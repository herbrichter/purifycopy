<?php

include_once('classes/Community.php');
include_once('classes/Media.php');
include_once('classes/CountryState.php');
include_once('classes/Type.php');
include_once('classes/SubType.php');
include_once('classes/MetaType.php');

if(isset($_GET['uid'])){
	$uid = $_GET['uid'];	
}

	$objCommunity=new Community();
	$userInfo=$objCommunity->getUserInfo($uid);
	//print_r($userInfo);
	
	$communityInfo=$objCommunity->getCommunityInfo($userInfo['community_id']);
	//print_r($communityInfo);
	
	$communityProfileInfo=$objCommunity->getCommunityProfileInfo($userInfo['community_id']);
	//print_r($communityProfileInfo);

	$objMedia=new Media();	
	$mediaData = $objMedia->searchMediaByProfileId($userInfo['community_id'],'community');
	//echo "<pre>";print_r($mediaData);
	
	$objCountryState=new CountryState();
	$state=$objCountryState->getStateName($communityInfo['state_id']);
	$country=$objCountryState->getCountryName($communityInfo['country_id']);
	//print_r( $state);
	
	$type=new Type();
	$subtype=new SubType();
	$metatype=new MetaType();
	
	$galleryImagesData = "";
	$galleryImagesDataIndex = 0;
	$thumbPath = "./uploads/thumb/";
	$orgPath = "./uploads/gallery/";



?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Purify Entertainment: About</title>
<link rel="stylesheet" type="text/css" media="screen" href="includes/purify.css" />
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.js"></script>
<script src="includes/functions.js" type="text/javascript"></script>
<link href="galleryfiles/gallery.css" rel="stylesheet"/>
<script src="galleryfiles/jquery.easing.1.3.js"></script>

<script src="includes/mediaelement-and-player.min.js"></script>
<link rel="stylesheet" href="includes/mediaelementplayer.min.css" />

<link rel="stylesheet" href="lightbox/prettyPhoto.css" type="text/css" media="screen" title="prettyPhoto main stylesheet" charset="utf-8" />
<script src="lightbox/jquery.prettyPhoto.js" type="text/javascript" charset="utf-8"></script>

</head>
<body onload="startTab();">
<div id="outerContainer">
  <div id="purifyMasthead">
    <div id="purifyLogo"><a href="index.php"><img src="images/4.gif" alt="Purify Entertainment" width="302" height="53" border="0" /></a></div>
    <div id="miscNav">
      <div id="loggedin">
        <h2>Logged in as <a href="#"><?php echo ucwords($userInfo['fname'].' '.$userInfo['lname']); ?></a></h2>
        <p><a href="profileedit_community.html">Edit Profile</a><br />
        <a href="Design/demoindex.php">Logout</a></p>
      </div>
    </div>
  </div>
  <div id="contentContainer">
    <!-- PROFILE START -->
    <div id="profileHeader">
      <div id="profilePhoto"><img src="uploads/profile_pic/450/<?php echo $communityInfo['image_name']; ?>" width="450" height="450" /></div>
      <div id="profileInfo">
        <h2><?php echo $type->getTypeById($communityProfileInfo['type_id']);?></h2>
        <div id="profileFeatured">
			<div class="fmedia">
				featured Media
			</div>
			<a href=""><img src="images/profile/play.png" width="35" height="23" onmouseover="this.src='images/profile/play-over.png'" onmouseout="this.src='images/profile/play.png'" /></a>
			<a href=""><img src="images/profile/add.png" width="27" height="23" onmouseover="this.src='images/profile/add-over.png'" onmouseout="this.src='images/profile/add.png'" /></a>
		</div>

        <h1><?php  echo ucwords($communityInfo['name']/*$userInfo['fname'].' '.$userInfo['lname']*/); ?></h1>
        <h3>Where:<?php echo ucwords($communityInfo['city'].', '.$state['state_name'].', '.$country['country_name']); ?> <br/>
          Subtype: <?php if($communityProfileInfo['subtype_id']!=0)echo $subtype->getSubTypeById($communityProfileInfo['subtype_id']); 
		  if($communityProfileInfo['metatype_id']!=0)echo " /".$metatype->getmetaTypeById($communityProfileInfo['metatype_id']); 
		  
		  ?>		  
		  <br/>
		  Email: <?php echo $userInfo['email']; ?></h3>
        <p></p>
      </div>
      <div id="profileStatsBlock">
        <div id="profileStats">
          <p>Fans: 765</p>
          <p>Total Downloads: 21,304</p>
          <p>Profile Views: 121,140</p>
        </div>
        <div id="profileButtons"> <a href="#">Become A Fan</a> <a href="#">Subscribe to </a> <a target="_blank" href="<?php echo $communityInfo['homepage']; ?>">Visit Homepage</a> </div>
      </div>
    </div>
    <div id="profileTabs">
      <ul>
	  <?php if($mediaData[0][0]=="gallery"){ ?>
        <li><a href="javascript:showTab('<?php echo $mediaData[0];?>');" class="profileTab3" id="tab<?php echo $mediaData[0];?>">Galleries</a></li>		
	  <?php }else if($mediaData[1][0]=="audio"){ ?>
        <li><a href="javascript:showTab('<?php echo $mediaData[0];?>');" class="profileTab2" id="tab<?php echo $mediaData[0];?>">Songs</a></li>
	  <?php }else if($mediaData[2][0]=="video"){ ?>
        <li><a href="javascript:showTab('<?php echo $mediaData[0];?>');" class="profileTab1" id="tab<?php echo $mediaData[0];?>">Videos</a></li>
 	  <?php }else if($mediaData[3][0]=="projects"){ ?>
       <li><a href="javascript:showTab('<?php echo $mediaData[0];?>');" class="profileTab4" id="tab<?php echo $mediaData[0];?>">Projects</a></li>
 	  <?php }else if($mediaData[4][0]=="events"){ ?>
       <li><a href="javascript:showTab('<?php echo $mediaData[0];?>');" class="profileTab5" id="tab<?php echo $mediaData[0];?>">Events</a></li>
 	  <?php }else if($mediaData[5][0]=="friends"){ ?>
       <li><a href="javascript:showTab('<?php echo $mediaData[0];?>');" class="profileTab6" id="tab<?php echo $mediaData[0];?>">Friends</a></li>
 	  <?php }?>
     </ul>
    </div>

<?php if($mediaData[0][0]=="gallery"){ ?>
     <div id="projectsTab" class="hiddenBlock<?php echo $mediaData[0];?>">
      <h2><a href="#">Play All Galleries</a></h2>
      <div class="rowItem">
<?php	
		$galleryList = $mediaData[0][1];
		for($g=0;$g<count($galleryList);$g++){
			$galleryData = $galleryList[$g][0];
			$mediaTitle = $galleryData['gallery_title'];
			$mediaSrc = $galleryData['image_name'];
			$filePath = "./uploads/thumb/".$mediaSrc;
			$galleryImagesData[$galleryImagesDataIndex] = $galleryList[$g][1];

			$galleryImagesDataIndex++;
?>
        <div class="tabItem">
			<img src="<?php echo $filePath;?>" width="200" height="175" />
			<h3><?php echo $mediaTitle; ?></h3>
			<div class="player">
				<a href="javascript:void(0);" onclick="checkload('<?php echo $mediaSrc;?>','<?php echo $galleryImagesDataIndex-1; ?>');">
					<img src="images/profile/play.gif" width="27" height="23" />
				</a>
				<img src="images/profile/add.gif" width="35" height="23" />
				<img src="images/profile/download.gif" width="28" height="23" />
			</div>
        </div>
<?php	} ?>
      </div>
    </div>

<?php }else if($mediaData[1][0]=="audio"){ ?>
    
    <div id="eventsTab" class="hiddenBlock<?php echo $mediaData[0];?>">
		<div id="profileFeatured">
		<div class="playall">
			Play All
		</div>
		<a href=""><img src="images/profile/play.png" width="35" height="23" onmouseover="this.src='images/profile/play-over.png'" onmouseout="this.src='images/profile/play.png'" /></a>
		<a href=""><img src="images/profile/add.png" width="27" height="23" onmouseover="this.src='images/profile/add-over.png'" onmouseout="this.src='images/profile/add.png'" /></a>
	</div>
      <h2><a href="#">Play All Songs</a></h2>
      <div class="rowItem">
	<?php
		$audioList = $mediaData[1][1];
		//print_r($audioList);
		for($a=0;$a<count($audioList);$a++){
			$audioTitle = $audioList[$a]['audio_name'];
			if($audioList[$a]['audio_file_name']!=""){
				$mediaFile = $audioList[$a]['audio_file_name'];
				$filePath = "./uploads/audio/".$mediaFile;
?>
        <div class="tabItem"> <img src="uploads/profile_pic/200/<?php echo $communityInfo['image_name']; ?>" width="200" height="175" />
          <h3><?php echo $audioTitle;?></h3>
          <div class="player">
			<a href="#inline_demo" rel="prettyAudio[inline]">
				<img src="images/profile/play.gif" width="27" height="23" />
			</a>
			<img src="images/profile/add.gif" width="35" height="23" />
			<img src="images/profile/download.gif" width="28" height="23" /></div>
          </div>
		  <div id="inline_demo" style="visibility:hidden;">
			<div style="padding:20px 0 20px 50px;">
				<audio type="audio/mp3" src="<?php echo $filePath; ?>" id="player2"></audio>
			</div>
		  </div>
	<?php }	}	?>
      </div>      
    </div>
<?php }else if($mediaData[2][0]=="video"){ ?>

	<div id="mediaTab" class="hiddenBlock<?php echo $mediaData[0];?>">
      <div id="profileFeatured">
		<div class="playall">
			Play All
		</div>
		<a href=""><img src="images/profile/play.png" width="35" height="23" onmouseover="this.src='images/profile/play-over.png'" onmouseout="this.src='images/profile/play.png'" /></a>
		<a href=""><img src="images/profile/add.png" width="27" height="23" onmouseover="this.src='images/profile/add-over.png'" onmouseout="this.src='images/profile/add.png'" /></a>
	  </div>
      <h2><a href="#">Play All Videos</a></h2>
<?php
		$videoList = $mediaData[2][1];
		//print_r($videoList);
		for($v=0;$v<count($videoList);$v++){
			$videoTitle = $videoList[$v]['video_name'];
			if($videoList[$v]['video_link']!=""){
				$mediaFile = $videoList[$v]['video_link'];
				preg_match("#(?<=v=)[a-zA-Z0-9-]+(?=&)|(?<=v\/)[^&\n]+(?=\?)|(?<=v=)[^&\n]+|(?<=youtu.be/)[^&\n]+#", $mediaFile, $matches);
				$videoId = $matches[0];
?>
      <div class="rowItem">
        <div class="tabItem"> <img src="http://img.youtube.com/vi/<?php echo $videoId;?>/0.jpg" width="200" height="175" />
          <h3><?php echo $videoTitle; ?></h3>
          <div class="player">
		  <a href="http://www.youtube.com/watch?v=<?php echo $videoId;?>" rel="prettyPhoto" title="">
			<img src="images/profile/play.gif" width="27" height="23" />
		  </a>
		  <img src="images/profile/add.gif" width="35" height="23" />
		  <img src="images/profile/download.gif" width="28" height="23" /></div>
        </div>
      </div>      
<?php } }?>
    </div>    
<?php }else if($mediaData[3][0]=="projects"){ ?>   
    <div id="servicesTab" class="hiddenBlock<?php echo $mediaData[0];?>">
      <h2>Projects</h2>
      <div class="rowItem">
        <div class="tabItem"> <img src="images/profile/icon1.jpg" width="200" height="175" />
          <h3>Project Title</h3>
          <div class="player"><img src="images/profile/play.gif" width="27" height="23" /><img src="images/profile/add.gif" width="35" height="23" /><img src="images/profile/download.gif" width="28" height="23" /></div>
        </div>
        <div class="tabItem"> <img src="images/profile/icon2.jpg" width="200" height="175" />
          <h3>Project Title</h3>
          <div class="player"><img src="images/profile/play.gif" width="27" height="23" /><img src="images/profile/add.gif" width="35" height="23" /><img src="images/profile/download.gif" width="28" height="23" /></div>
        </div>
        <div class="tabItem"> <img src="images/profile/icon3.jpg" width="200" height="175" />
          <h3>Project Title</h3>
          <div class="player"><img src="images/profile/play.gif" width="27" height="23" /><img src="images/profile/add.gif" width="35" height="23" /><img src="images/profile/download.gif" width="28" height="23" /></div>
        </div>
        <div class="tabItem"> <img src="images/profile/icon4.jpg" width="200" height="175" />
          <h3>Project Title</h3>
          <div class="player"><img src="images/profile/play.gif" width="27" height="23" /><img src="images/profile/add.gif" width="35" height="23" /><img src="images/profile/download.gif" width="28" height="23" /></div>
        </div>
        <div class="tabItem"> <img src="images/profile/icon5.jpg" width="200" height="175" />
          <h3>Project Title</h3>
          <div class="player"><img src="images/profile/play.gif" width="27" height="23" /><img src="images/profile/add.gif" width="35" height="23" /><img src="images/profile/download.gif" width="28" height="23" /></div>
        </div>
        <div class="tabItem"> <img src="images/profile/icon6.jpg" width="200" height="175" />
          <h3>Project Title</h3>
          <div class="player"><img src="images/profile/play.gif" width="27" height="23" /><img src="images/profile/add.gif" width="35" height="23" /><img src="images/profile/download.gif" width="28" height="23" /></div>
        </div>
      </div>
    </div>
<?php }else if($mediaData[4][0]=="events"){ ?>   
    <div id="friendsTab" class="hiddenBlock<?php echo $mediaData[0];?>">
      <h2>Upcoming Events</h2>
      <div class="rowItem">
        <div class="tabItem"> <img src="images/profile/icon1.jpg" width="200" height="175" />
          <h3>Event Title</h3>
          <div class="player"><img src="images/profile/play.gif" width="27" height="23" /><img src="images/profile/add.gif" width="35" height="23" /><img src="images/profile/download.gif" width="28" height="23" /></div>
        </div>
        <div class="tabItem"> <img src="images/profile/icon2.jpg" width="200" height="175" />
          <h3>Event Title</h3>
          <div class="player"><img src="images/profile/play.gif" width="27" height="23" /><img src="images/profile/add.gif" width="35" height="23" /><img src="images/profile/download.gif" width="28" height="23" /></div>
        </div>
        <div class="tabItem"> <img src="images/profile/icon3.jpg" width="200" height="175" />
          <h3>Event Title</h3>
          <div class="player"><img src="images/profile/play.gif" width="27" height="23" /><img src="images/profile/add.gif" width="35" height="23" /><img src="images/profile/download.gif" width="28" height="23" /></div>
        </div>
        <div class="tabItem"> <img src="images/profile/icon4.jpg" width="200" height="175" />
          <h3>Event Title</h3>
          <div class="player"><img src="images/profile/play.gif" width="27" height="23" /><img src="images/profile/add.gif" width="35" height="23" /><img src="images/profile/download.gif" width="28" height="23" /></div>
        </div>
        <div class="tabItem"> <img src="images/profile/icon5.jpg" width="200" height="175" />
          <h3>Event Title</h3>
          <div class="player"><img src="images/profile/play.gif" width="27" height="23" /><img src="images/profile/add.gif" width="35" height="23" /><img src="images/profile/download.gif" width="28" height="23" /></div>
        </div>
        <div class="tabItem"> <img src="images/profile/icon6.jpg" width="200" height="175" />
          <h3>Event Title</h3>
          <div class="player"><img src="images/profile/play.gif" width="27" height="23" /><img src="images/profile/add.gif" width="35" height="23" /><img src="images/profile/download.gif" width="28" height="23" /></div>
        </div>
      </div>
      <h2>Recorded Events</h2>
      <div class="rowItem">
        <div class="tabItem"> <img src="images/profile/icon1.jpg" width="200" height="175" />
          <h3>Event Title</h3>
          <div class="player"><img src="images/profile/play.gif" width="27" height="23" /><img src="images/profile/add.gif" width="35" height="23" /><img src="images/profile/download.gif" width="28" height="23" /></div>
        </div>
        <div class="tabItem"> <img src="images/profile/icon2.jpg" width="200" height="175" />
          <h3>Event Title</h3>
          <div class="player"><img src="images/profile/play.gif" width="27" height="23" /><img src="images/profile/add.gif" width="35" height="23" /><img src="images/profile/download.gif" width="28" height="23" /></div>
        </div>
        <div class="tabItem"> <img src="images/profile/icon3.jpg" width="200" height="175" />
          <h3>Event Title</h3>
          <div class="player"><img src="images/profile/play.gif" width="27" height="23" /><img src="images/profile/add.gif" width="35" height="23" /><img src="images/profile/download.gif" width="28" height="23" /></div>
        </div>
        <div class="tabItem"> <img src="images/profile/icon4.jpg" width="200" height="175" />
          <h3>Event Title</h3>
          <div class="player"><img src="images/profile/play.gif" width="27" height="23" /><img src="images/profile/add.gif" width="35" height="23" /><img src="images/profile/download.gif" width="28" height="23" /></div>
        </div>
        <div class="tabItem"> <img src="images/profile/icon5.jpg" width="200" height="175" />
          <h3>Event Title</h3>
          <div class="player"><img src="images/profile/play.gif" width="27" height="23" /><img src="images/profile/add.gif" width="35" height="23" /><img src="images/profile/download.gif" width="28" height="23" /></div>
        </div>
        <div class="tabItem"> <img src="images/profile/icon6.jpg" width="200" height="175" />
          <h3>Event Title</h3>
          <div class="player"><img src="images/profile/play.gif" width="27" height="23" /><img src="images/profile/add.gif" width="35" height="23" /><img src="images/profile/download.gif" width="28" height="23" /></div>
        </div>
      </div>
    </div>
<?php }else if($mediaData[5][0]=="friends"){ ?>    
    <div id="promotionsTab" class="hiddenBlock<?php echo $mediaData[0];?>">
      <h2>Artists</h2>
      <div class="rowItem">
        <div class="tabItem"> <img src="images/profile/icon1.jpg" width="200" height="175" />
          <h3>Artist Name</h3>
          <div class="player"><img src="images/profile/play.gif" width="27" height="23" /><img src="images/profile/add.gif" width="35" height="23" /><img src="images/profile/download.gif" width="28" height="23" /></div>
        </div>
        <div class="tabItem"> <img src="images/profile/icon2.jpg" width="200" height="175" />
          <h3>Artist Name</h3>
          <div class="player"><img src="images/profile/play.gif" width="27" height="23" /><img src="images/profile/add.gif" width="35" height="23" /><img src="images/profile/download.gif" width="28" height="23" /></div>
        </div>
        <div class="tabItem"> <img src="images/profile/icon3.jpg" width="200" height="175" />
          <h3>Artist Name</h3>
          <div class="player"><img src="images/profile/play.gif" width="27" height="23" /><img src="images/profile/add.gif" width="35" height="23" /><img src="images/profile/download.gif" width="28" height="23" /></div>
        </div>
        <div class="tabItem"> <img src="images/profile/icon4.jpg" width="200" height="175" />
          <h3>Artist Name</h3>
          <div class="player"><img src="images/profile/play.gif" width="27" height="23" /><img src="images/profile/add.gif" width="35" height="23" /><img src="images/profile/download.gif" width="28" height="23" /></div>
        </div>
        <div class="tabItem"> <img src="images/profile/icon5.jpg" width="200" height="175" />
          <h3>Artist Name</h3>
          <div class="player"><img src="images/profile/play.gif" width="27" height="23" /><img src="images/profile/add.gif" width="35" height="23" /><img src="images/profile/download.gif" width="28" height="23" /></div>
        </div>
        <div class="tabItem"> <img src="images/profile/icon6.jpg" width="200" height="175" />
          <h3>Artist Name</h3>
          <div class="player"><img src="images/profile/play.gif" width="27" height="23" /><img src="images/profile/add.gif" width="35" height="23" /><img src="images/profile/download.gif" width="28" height="23" /></div>
        </div>
      </div>
      <h2>Community</h2>
      <div class="rowItem">
        <div class="tabItem"> <img src="images/profile/icon1.jpg" width="200" height="175" />
          <h3>Community Name</h3>
          <div class="player"><img src="images/profile/play.gif" width="27" height="23" /><img src="images/profile/add.gif" width="35" height="23" /><img src="images/profile/download.gif" width="28" height="23" /></div>
        </div>
        <div class="tabItem"> <img src="images/profile/icon2.jpg" width="200" height="175" />
          <h3>Community Name</h3>
          <div class="player"><img src="images/profile/play.gif" width="27" height="23" /><img src="images/profile/add.gif" width="35" height="23" /><img src="images/profile/download.gif" width="28" height="23" /></div>
        </div>
        <div class="tabItem"> <img src="images/profile/icon3.jpg" width="200" height="175" />
          <h3>Community Name</h3>
          <div class="player"><img src="images/profile/play.gif" width="27" height="23" /><img src="images/profile/add.gif" width="35" height="23" /><img src="images/profile/download.gif" width="28" height="23" /></div>
        </div>
        <div class="tabItem"> <img src="images/profile/icon4.jpg" width="200" height="175" />
          <h3>Community Name</h3>
          <div class="player"><img src="images/profile/play.gif" width="27" height="23" /><img src="images/profile/add.gif" width="35" height="23" /><img src="images/profile/download.gif" width="28" height="23" /></div>
        </div>
        <div class="tabItem"> <img src="images/profile/icon5.jpg" width="200" height="175" />
          <h3>Community Name</h3>
          <div class="player"><img src="images/profile/play.gif" width="27" height="23" /><img src="images/profile/add.gif" width="35" height="23" /><img src="images/profile/download.gif" width="28" height="23" /></div>
        </div>
        <div class="tabItem"> <img src="images/profile/icon6.jpg" width="200" height="175" />
          <h3>Community Name</h3>
          <div class="player"><img src="images/profile/play.gif" width="27" height="23" /><img src="images/profile/add.gif" width="35" height="23" /><img src="images/profile/download.gif" width="28" height="23" /></div>
        </div>
      </div>
    </div>
<?php }?>    
    
    <!-- PROFILE END -->
  </div>
  <div id="footer"> <strong>&copy; 2010 Purify Entertainment</strong> This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/3.0/">Creative Commons Attribution-Share Alike 3.0 Unported License</a>.<br />
    <a href="privacypolicy.html">Privacy Policy</a> / <a href="useragreement.html">User Agreement</a> / <a href="originalityagreement.html">Originality Agreement</a> / <a href="busplan.html">Business Plan</a> </div>
  <!-- end of main container -->
</div>

  <!-- Gallery -->

<div id="light-gal" class="gallery-wrapper">
	<div id="bg">
		<a href="#" class="nextImageBtn" title="next"></a>
		<a href="#" class="prevImageBtn" title="previous"></a>
		<img width="1680" src="./uploads/gallery/1335074394-7032884797_475ce129ca.jpg" height="1050" alt="" title="" id="bgimg" />
	</div>
	<div id="preloader"><img src="./galleryfiles/ajax-loader_dark.gif" width="32" height="32" /></div>
	<div id="img_title"></div>
	<div id="toolbar">
		<a href="#" title="Maximize" id="maximizeImg" onClick="ImageViewMode('full');return false"><img src="./galleryfiles/toolbar_fs_icon.png" width="50" height="50"  /></a>
		<a href="#" title="Close" id="closeGal" onClick="CloseGallery();return false"><img src="./galleryfiles/close.png" width="50" height="50"  /></a>
	</div>
	<div id="thumbnails_wrapper">
	<div id="outer_container">
	<div class="thumbScroller">
		<div class="container" id="gallery-container">
			
		</div>
	</div>
	</div>
	</div>
</div>
</body>
</html>
<script type="text/javascript" src="./galleryfiles/jqgal.js"></script>
<script>

function checkload(defImg,gIndex){
	var arr = '<?php if($galleryImagesData!=""){ echo json_encode($galleryImagesData);}?>';
	var data = eval('('+arr+')');
	//alert(data[gIndex].toSource());
	data = data[gIndex];
	if(data.length>0){
		$("#gallery-container").html('');
	}
	$("#bgimg").attr('src','<?php echo $orgPath;?>'+defImg);
/*	var the1stImg = new Image();
	the1stImg.onload = CreateDelegate(the1stImg, theNewImg_onload);
	the1stImg.src = '<?php echo $orgPath;?>'+defImg;
	
	$("#bg").append('<img width="1680" src="<?php echo $orgPath;?>'+defImg+'" height="1050" alt="Denebola" title="Denebola" id="bgimg" />');
*/
	for(var i=0;i<data.length;i++){
//		alert(data[i].image_title);
		$("#gallery-container").append('<div class="content"><div><a href="<?php echo $orgPath;?>'+data[i].image_name+'"><img src="<?php echo $thumbPath;?>'+data[i].image_name+'" title="'+data[i].image_title+'" alt="'+data[i].image_title+'" class="thumb" /></a></div></div>');
	}
	setTimeout(function(){	loadGallery();	$("#light-gal").css('visibility','visible');	},500);
	
}
	$(document).ready(function(){
		// Load the classic theme
		//Galleria.loadTheme('./galleria/themes/classic/galleria.classic.min.js');
		//Galleria.loadTheme('../javascripts/themes/classic/galleria.classic.min.js');

		// Initialize Galleria
		//Galleria.run('#galleria10');

		
		$("a[rel^='prettyPhoto']").prettyPhoto({social_tools:''});
		$("a[rel^='prettyAudio']").prettyPhoto({social_tools:'',
			changepicturecallback: function(){ $('audio').mediaelementplayer(); $(".mejs-container").css('margin-left',0); }	
		});

		//$("a[rel^='prettyAudio']").prettyPhoto();
	});
</script>
