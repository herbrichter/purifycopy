<?php
    ob_start();
	include_once('login_includes.php');
	include_once('classes/Community.php');
	
	$verification_no=$_GET['id'];
	
	$obj=new Community();
	if($obj->checkVerificationNo($verification_no))
	{
		$obj->updateVerification($verification_no);
		
		//The line below is added just temporarily to divert user to demo site. This should be removed when actual site is ready.
		header('location:http://www.purifyentertainment.net/Design/index.php');
		//-----------------------------------------------------------------------------------------------------------------------
				
		$msg="Thank you for registering on our network. When your user account is activated we will send you an email with a link to login and start building your profile.";
		$reg_status="Complete";
	}
	else
	{
		$msg="Error in registration. Either your registration request is already in progress or the link is bad.";
		$reg_status="Failed";
	}
?>

<?php include_once('includes/header.php'); ?>
<title>Purify Entertainment: Six Feature</title>

  <div id="contentContainer">
    <h1>Registration <?php echo $reg_status; ?></h1>
    <p><?php echo $msg; ?></p>
  </div>
  
<?php include_once('includes/footer.php'); ?>