<?php

/******************************************************************************
* Copyright (c) 2009, http://www.webdigi.co.uk 
* Website Change Detection system
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*     * Redistributions of source code must retain the above copyright
*       notice, this list of conditions and the following disclaimer.
*     * Redistributions in binary form must reproduce the above copyright
*       notice, this list of conditions and the following disclaimer in the
*       documentation and/or other materials provided with the distribution.
*     * Neither the name of the <organization> nor the
*       names of its contributors may be used to endorse or promote products
*       derived from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY http://www.webdigi.co.uk ''AS IS'' AND ANY
* EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
* WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
* DISCLAIMED. IN NO EVENT SHALL http://www.webdigi.co.uk  BE LIABLE FOR ANY
* DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
* (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
* LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
* ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
* (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
* SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*******************************************************************************/

/*************************************
 * Settings - Please EDIT values below
 **************************************/
/**
 * Directory to scan.
 */
$dir = '.';
/**
 * Hash filename - The name of the file where the MD5 hash will be stored.
 */
$hashFilename = 'hash.md5';
/**
 * Log filename - The name of the file where the log should be written.
 */
$logFilename = 'hashlog';
/** 
 * Scan Password - This value has to be sent each time to run the code.
 * Please change from the default password to anything you like
 */ 
$scanPassword = "Simpsons2015!";

/** 
 * Exclude File List - Important do not exclude files like PHP, ASP, JS, HTML etc
 * Seperate each entry with a semicolon ; 
 * Full filename including extension. [CASE INSENSITIVE]
 */ 
$excludeFileList = "error_log;backup.zip;$hashFilename;$logFilename"; 

/** 
 * Exclude Folder List
 * Seperate each entry with a semicolon ; 
 * [CASE INSENSITIVE]
 */ 
$excludeFolderList = "cache;backup";

/** 
 * Exclude Extension List - Important do not exclude file extensions like PHP, ASP, JS, HTML etc
 * Seperate each entry with a semicolon ; 
 * Only extension type. [CASE INSENSITIVE]
 */ 
$excludeExtensionList = "flv;log;txt;jpg;mp3;mp4;jpeg;xml;png;wav;zip";

/** 
 * Set emailAddressToAlert variable if you want an email alert from the server.
 */ 
$emailAddressToAlert = "wasihacked@tmssys.com";
$emailSubject = "IMPORTANT! Files in the server have changed";
$emailBody = "Hash value has changed. Please note that your hash value is no longer the same on server";

//On earlier than PHP5 will use code below to declare stripos function
if (!function_exists("stripos")) {
  function stripos($str,$needle,$offset=0)
  {
      return strpos(strtolower($str),strtolower($needle),$offset);
  }
}

function traverseDirectoryStructure($dir) {
	global $excludeFolderList, $excludeFileList, $excludeExtensionList;
	if (!is_dir($dir) || !is_readable($dir)) {
		print "Dir $dir not readable.";
		return false;
	}

	$fileList = array ();
	$d = dir($dir);
	
	while (false !== ($entry = $d->read())) {
		if ($entry != '.' && $entry != '..') {
			if (is_dir($dir . '/' . $entry)) {
				if (stripos( $excludeFolderList, $entry ) === false) {
					$newFiles = traverseDirectoryStructure($dir . '/' . $entry, $excludeFileList, $excludeExtensionList);
					$fileList = array_merge($fileList,$newFiles);
				}
			} else {
				//dont scan files in exclude list
				if (stripos($excludeFileList, $entry) === false) {
					$extension = end(explode('.', $entry)); //get the file extension
					//dont scan extensions in exclude list
					if (stripos($excludeExtensionList, $extension) === false) {
						$fileList[] = $dir . '/' . $entry; //Prepare list to MD5 only allowed
					}
				}
			}
		}
	}

	$d->close();

	return $fileList;
}

function makeHashingStructure($files) {
	$master = array();
	foreach ($files as $file) {
		$master[$file] = md5_file($file);
	}
	return $master;
}

function saveMasterStructure($structure, $filepath) {
	$fh = fopen($filepath, 'w');
	fwrite($fh, serialize($structure));
	fclose($fh);
}

function loadMasterStructure($filepath) {
	return unserialize(file_get_contents($filepath));
}

function compareStructures($master, $new) {
	$diffs1 = array_diff_assoc($master,$new);
	$diffs2 = array_diff_assoc($new,$master);
	return array_merge($diffs1,$diffs2);
}

function deleteFile ($filepath) {

    $searchstring="/home/purifycopy/public_html/./";    
    
    $filename = substr($filepath, strlen($searchstring));

    //print "<li>File being deleted:    $filename\n</li>";
    
    if (file_exists($filename)) {
        unlink ($filename);
        print "<li>File deleted:    $filename\n</li>";
    }

}

/**********************************************************
 * Start with logic of scanning and checking the code files
 ***********************************************************/

//Key steps in scan 
//STEP 1  - Check if the password is OK
	if (strcmp ( $_REQUEST["password"], $scanPassword )  != 0 )
	{
		echo "Incorrect password!";
		exit(0);
	}

//Make absolute directory path.
$dir = dirname(__FILE__) . '/' . $dir;

// Open log file
$logFile = dirname(__FILE__) . "/" . $logFilename;
$logfh = fopen($logFile, 'a');

// Make hash file path absolute
$hashFile = dirname(__FILE__) . "/" . $hashFilename;

$files = traverseDirectoryStructure($dir, $excludeFileList, $excludeExtensionList);
$structure = makeHashingStructure($files);

//STEP 2  - Check if user has sent the myhash (otherwise treat as first run and send hash to user)
	if( !file_exists($hashFile) )
	{
		//make the hash and send to user
		saveMasterStructure($structure,$hashFile);
		echo '<h1>New master hash structure was created.</h1>';
		exit(0);			
	}


//STEP 3 - If user has sent hash then compare with a new hash 
//         If the values are different then raise an ALARM

	$masterStructure = loadMasterStructure($hashFile);

	if( ($diffs = compareStructures($masterStructure,$structure)) != null )
	{
	
		//ALERT. CODE FILES IN THE SERVER HAVE CHANGED.
		//PERFORM WHATEVER ALERT HERE YOU WANT. EMAIL, SMS !!
		
		//A. MAIL
		//if($emailAddressToAlert <> ""){
		//	
		//	//Add the new hash value to the email
		//	$emailBody = $emailBody;
		//
		//	mail($emailAddressToAlert, $emailSubject, $emailBody); //Simple mail function for alert.
		//
		//}
		echo "<h1>IMPORTANT!</h1>HASH CHECK has failed as the codebase has been altered.<br />";
		print "Diffs:\n<br /><ul>";
		foreach($diffs as $key => $value) {
			if (isset($masterStructure[$key]) && !isset($structure[$key])) {
				print "<li>File missing:  $key\n</li>";
			} elseif (isset($masterStructure[$key]) && isset($structure[$key])) {
				print "<li>File modified: $key ({$masterStructure[$key]} => {$structure[$key]})\n</li>";
			} elseif (!isset($masterStructure[$key]) && isset($structure[$key])) {
				print "<li>File added:    $key\n</li>";
                deleteFile($key);
			}
		}
		echo '</ul>';
		//B. RESPOND ON SCREEN TO USER
			
	}else{
	
		//HASH VALUE IS OK
			echo "<BR>All good! No change to the codebase is detected";
	
	}
	
?>