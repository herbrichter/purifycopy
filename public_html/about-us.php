<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "//www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="//www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Purify Art: About</title>

<?php include("includes/header.php");
//header('Content-type: video/mp4');
if(isset($_GET))
{
	if(isset($_GET['e']) || isset($_GET['a']))
	{
		?>
			<script>
				$("document").ready(function(){
					window.scroll(0,1286);
				});
			</script>
		<?php
	}
	elseif(isset($_GET['c']))
	{
		?>
			<script>
				$("document").ready(function(){
					window.scroll(0,1780);
				});
			</script>
		<?php
	}
	elseif(isset($_GET['t']))
	{
		?>
			<script>
				$("document").ready(function(){
					window.scroll(0,2236);
				});
			</script>
		<?php
	}
	elseif(isset($_GET['s']))
	{
		?>
			<script>
				$("document").ready(function(){
					window.scroll(0,2858);
				});
			</script>
		<?php
	}
}
?>
<script src="jwplayer/jwplayer.js"></script>
<script>jwplayer.key="e4OUSwhwX7jbBT3WDdQYuboL1dYW9b29pGxAHA=="</script>
<div id="contentContainer" >
	<div class="wrapper">
		<div class="inCont">
			<div class="pagetititle">
				Art with Integrity<br /><span class="subtitle">Share and promote art, music, and video on our ad free platform</span>
			</div>
			<div class="video">
				<div class="video" id='my-video'></div>
			</div>
				<script type='text/javascript'>
					jwplayer('my-video').setup({
						flashplayer: 'jwplayer/jwplayer.flash.swf',
						file: 'hmvideo/Purify_Intro_V3_HD.mp4',
						image: 'hmvideo/screenshot.jpg',
						skin: "jwplayer/jwplayerskins/beelden.xml",
						width: '655',
						height: '368',
					});
				</script>
			
			
			<div class="inBlk">
				<div class="topborder"></div>
				<div class="blkTitle">Mission</div>
				<div class="topborder"></div>
				<div class="blkcont">To create a network of web pages and sites with a common search engine, media player, and digital store all ad free that easily and cost effectively enables Internet users with e-commerce, online marketing, art and music discovery, and social sharing tools.</div>
			</div>
			<div class="inBlk">
				<div class="topborder"></div>
				<div class="blkTitle">Goals</div>
				<div class="topborder"></div>
				<div class="blkcont">1. To maximize the revenue media creators receive from online services.<br />2. To provide media viewers one affordable, complete, and interactive online service.<br />3. To protect the users privacy and the content creators IP rights.</div>
			</div> 
			<div class="inBlk">
				<div class="topborder"></div>
				<div class="blkTitle">Timeline</div>
				<div class="topborder"></div>
				<div class="blkcont"><div class="timline"><img src="images/timline.png" /></div></div>
			</div>    
			 <div class="inBlk">
				<div class="topborder"></div>
				<div class="blkTitle">Services</div>
				<div class="topborder"></div>
				<div class="servcont">
					<div class="icon"><img src="images/ico-network.png" /></div>
					<div class="intitle">Network</div>
					<p>The Purify Art network connects artists, fans, organizations and businesses in the arts industry and provides online tools that integrate audio, video, and visual art on one ad-free media platform. This network is driven by a media player that allows users to create custom playlists from their personal media libraries. Purify Art enables people to collaborate, promote, perform, share and make secure transactions with one another.</p>
					<div class="subtitle">Search and Discover: </div>
					<p>Use our advanced search engine to find and discover Artists, Companies, Events, Projects, and media across the network. Use the location fields to search within a specific country, state, and/or city.</p>
					<div class="subtitle">Ad-Free Profile Pages: </div>
					<p>Artists and Companies can share information and media through their profiles pages equipped with a media player and store. Users can build profile pages for Events and Projects such as bands, albums, collaborations, and more.</p>
					<div class="seperator"></div>
				</div>
				
				<div class="servcont">
					<div class="icon"><img src="images/icon-cloud.png" /></div>
					<div class="intitle">Cloud Media Library and Player</div>
					<p>All profiles and search engines contain media buttons for users to build custom ad free channels on the media player while browsing the site. The media player integrates audio, video, and visual into one art media experience.</p>
					<div class="subtitle">Uninterrupted Play: </div>
					<p>The media player allows the user to browse profiles and pages while building custom channels that play without interruption.</p>
					<div class="subtitle">Audio/Visual Integration: </div>
					<p>When uploading songs, musicians can tag visual art galleries so that audio and visual are played simultaneously within the media player. This gives musicians and visual artists the ability to cross promote one another.</p>
					<div class="seperator"></div>
				</div>
				
				<div class="servcont">
					<div class="icon"><img src="images/icon-promotion.png" /></div>
					<div class="intitle">Promotional Tools</div>
					<p>Purify Art offers a suite of promotional tools that continues to expand. Using the Purify Update function you can send emails, post to your news feed, and share on Facebook, with the click of a single button. We aim to provide one complete, professional platform for your promotions, improving your online presence and saving you time.</p>
					<div class="subtitle">Contacts:  </div>
					<p>Organize all of your contacts in one address book and build your list every time you are followed, share a free download, or make a sale.</p>
					<div class="subtitle">Emails:  </div>
					<p>Automatically, email lists will be created for each Artist/Company profile you create and for each Digital Fan Club you set-up making it easy to send email promotions to your entire list or to specific recipients.</p>
					<div class="subtitle">News Feeds: </div>
					<p>Share posts with your Followers and Friends to keep them updated on your latest media, Events, and Projects.</p>
					<div class="subtitle">Facebook:</div>
					<p>Sync your Purify Art posts and promotions with Facebook.</p>
				</div>
				
				<div class="servcont">
					<div class="icon"><img src="images/icon-ecommerce.png" /></div>
					<div class="intitle">eCommerce Tools</div>
					<p>Purify Art offers a suite of eCommerce tools for selling, distributing, and publishing music. Developments on the way include tools for visual artists to sell prints and digital files direct to fans, submit files to stock photo services, and pay-per-view and subscriptions to videos. Purify Art aims to provide one complete professional platform for monetizing creative content for musicians, visual artists, and video producers.</p>
					<div class="subtitle">Direct to Fan Sales: </div>
					<p>Artist and Company Members have the ability to make sales through the online digital store and receive 95% of profits from their sales. Purify Art charges 5% to provide the hosting and programming that enables artists to sell directly to their fans. <a href="">Learn More</a></p>
					<div class="subtitle">Fan Club Memberships: </div>
					<p>Artist and Company Members can offer annual Fan Club Memberships for any designated price. These memberships, available only to registered users, give access to exclusive media.</p>
					<div class="subtitle">Distribution and Publishing: </div>
					<p>Artist and Company Members can sign their songs and albums with Purify Art's full service global distribution and publishing service. No per-song or per-album fees, with thousands of stores and streaming outlets. <a href="">Learn More</a></p>
				</div>
				
				<div class="servcont">
					<div class="icon"><img src="images/icon-membership.png" /></div>
					<div class="intitle">Purify Art Memberships</div>
					<p>Registering with Purify Art is free. Once registered, users can purchase an annual Purify Art Membership for $25 and receive our premium services listed below:</p>
					<div class="subtitle">Custom Playlists:  </div>
					<p>Purify Art Members can create custom playlists using songs and videos in their Media Library that were either free, purchased, or gained through a Fan Club Membership. Ad-free playlists provide easy access to hours of your favorite songs and videos.</p>
					<div class="subtitle">eCommerce Tools: </div>
					<p>Purify Art Members can utilize Purify Art's suite of eCommerce tools including Direct to Fan Sales, Fan Club Memberships, and Distribution.</p>
				</div>
				
				<div class="servcont_last">
					<div class="icon"><img src="images/icon-pro.png" /></div>
					<div class="intitle">Pro Services</div>
					<p>Hire the team that built PurifyArt.com.  We are here to build any idea, vision, or online need you have. Our team works with all levels of projects, from low budget and simple to customized and complex. By offering web development, online marketing services, graphic design, and video production, we can completely integrate your marketing efforts through all mediums of communication to reinforce your business identity. While meeting your creative production and marketing needs, we bring you professional designs and tools with friendly customer care.</p>
                    <div style="border-radius:5px; -webkit-border-radius:5px; -moz-border-radius:5px; -ms-border-radius:5px; -o-border-radius:5px; background:#000; width:190px; height:40px; float:left; text-align:center; margin: 10px 0 10px 273px;">
                    	<a href="http://purifyproservices.com/request-a-quote/" target="_blank" style="float:left; width:190px; height:40px; line-height:40px; font-size:14px; color:#fff; text-decoration:none;">Request a Quote</a>
                    </div>
				</div>
			</div>   

		</div>
		<?php include_once("displayfooter.php"); ?>		
	</div>
</div>
<style>
	#miniFooter{ clear: both; margin: 20px 0 auto; float: left; }
	p#copyright { margin: 0; padding-top: 10px; }
</style>
</body>
</html>
