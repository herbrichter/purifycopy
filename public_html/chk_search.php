<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "//www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="//www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Purify Art</title>
<link rel="stylesheet" type="text/css" media="screen" href="includes/purify.css" />
<link rel="stylesheet" href="engine/css/vlightbox.css" type="text/css" />
<link rel="stylesheet" href="engine/css/visuallightbox.css" type="text/css" media="screen" />
<link rel="stylesheet" href="css/jquery.autocomplete.css" type="text/css" />
<link rel="stylesheet" href="includes/jquery.ui.all.css">
<link  rel="stylesheet" type="text/css" media="screen" href="css/jquery.ui.autocomplete.css"></link>


 <script type="text/javascript" src="javascripts/country_state.js"></script>
<script src="javascripts/jquery-1.2.6.js"></script>
<script type="text/javascript" src="javascripts/flowplayer-3.1.4.min.js"></script>
<script type="text/javascript" src="javascripts/audio-player.js"></script>
<style type="text/css">#vlightbox a#vlb{display:none}</style>
<script src="engine/js/jquery.min.js" type="text/javascript"></script>
<script src="javascripts/jquery-1.2.6.js"></script>
<script src="ui/jquery-1.7.2.js"></script>
<script src="includes/jquery.ui.core.js"></script>
<script src="includes/jquery.ui.widget.js"></script>
<script src="includes/jquery.ui.datepicker.js"></script>
<script type="text/javascript" src="javascripts/jquery.ui.autocomplete.js"></script>
<script type="text/javascript" src="javascripts/jquery.ui.position.js"></script>
<script type="text/javascript" src="javascripts/checkForm.js"></script>
<style>
	.ui-autocomplete-loading { background: white url('css/ui-anim_basic_16x16.gif') right center no-repeat; }
</style>
</head>
<body>
<input style="width:177px !important;" name="general_search" id="general_search" type="text" class="searchField" value="Search" onfocus="if(this.value == 'Search'){this.value = '';}" onblur="if(this.value == ''){this.value='Search';}" />
</body>
<script type="text/javascript">
$("#general_search").click(function(){
window.onkeypress = function(evt) {
		evt = evt || window.event;
		var charCode = evt.keyCode || evt.which;
		if(charCode==13){
			$("#searchlink").click();
		}
	};
});

 $(function() {
		$( "#general_search" ).autocomplete({
			source: "suggest_search.php",
			minLength: 1,
			select: function( event, ui ) {
			document.getElementById("table_name").value = ui.item.value1;
			document.getElementById("table_id").value = ui.item.value2;
			},
			open:function(event,ui){ 
				var maxListLength=10;
				var ul = jQuery( "#general_search" ).autocomplete("widget")[0];
				//alert(ul.offsetWidth);
				var len_org = ul.childNodes.length;
				while(ul.childNodes.length > maxListLength)
				{
					  ul.removeChild(ul.childNodes[ul.childNodes.length-1]);
				}
					if(ul.childNodes.length > maxListLength-1)
					{
						$('<li id="ac-add-venue" style="text-align: center;"><a href="search.php?alpha='+$("#general_search").val()+'" style="float:left; width:100%; background: none repeat scroll 0% 0% #808080;">See More...</a></li>').appendTo('ul.ui-autocomplete');
						$('<li id="ac-add-venue" style="text-align: center;">Displaying Top 10 of '+ len_org +' Results.</li>').appendTo('ul.ui-autocomplete');
					}
				}
		});
	});
</script>
</html>