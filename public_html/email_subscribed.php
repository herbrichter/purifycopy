<?php
	include("classes/ProfileDisplay.php");
	include("classes/GetSubscriptionInfo.php");
	include("newsmtp/PHPMailer/index.php");
	include_once('sendgrid/SendGrid_loader.php');
	session_start();
    $domainname=$_SERVER['SERVER_NAME'];
?>
<link rel="stylesheet" type="text/css" media="screen" href="includes/purify.css" />
<!--<script src='https://ajax.googleapis.com/ajax/libs/jquery/1.6.1/jquery.min.js'></script>-->
<script>
	function validate_fields_sub()
	{
		var sub_email = document.getElementById("sub_email").value;
		var sub_name = document.getElementById("sub_name").value;
		var atpos = sub_email.indexOf("@");
		var dotpos = sub_email.lastIndexOf(".");
		
		$("#member_email_new").css({"display":"block"});
		var check_vid_sub = $("#member_email_new").html();
		
		if(check_vid_sub == "You have already subscribed to this user." || check_vid_sub == "You have already subscribed to this event." || check_vid_sub == "You have already subscribed to this project.")
		{
			alert(check_vid_sub);
			return false;
		}
		
		if(check_vid_sub == "Please register first to subscribe.")
		{
			alert("Please register first to subscribe.");
			return false;
		}
	}
	$("document").ready(function(){
		document.getElementById("sub_email").value = '<?php if(isset($_SESSION['login_email'])){ echo $_SESSION['login_email']; }?>';
		
	});
	$("document").ready(function(){
			//$("#sub_email").focusout(function(){
				var text_value = document.getElementById("sub_email").value;
			//alert(text_value);
				var match_db_id = document.getElementById("rel_id_sub").value;
				var match_db_text = document.getElementById("rel_type_sub").value;
				//alert(match_db_text);
				var uurls = parent.document.URL;
				var text_data = 'reg_sub='+ text_value+'&'+'match_id_sub='+ match_db_id+'&'+'match_text_sub='+ match_db_text+'&'+'match_uuls='+ uurls;
				 
				$.ajax({	 
					type: 'POST',
					url : 'SubscriptionAjaxMembership.php',
					data: text_data,
					success: function(data) 
					{
						$("#member_email_new").html(data);
						
						if(data=="Please register first to subscribe.")
						{
							$("#links").css({"display":"block"});
						}
						else if(data == "same")
						{
							$("#member_email_same").css({"display":"block"});
						}
						else
						{
							$("#links").css({"display":"none"});
						}
					}
				});
			//});
		});
		
		function link_register()
		{
			window.parent.location = "https://www.<?php echo $domainname;?>";
			parent.jQuery.fancybox.close();
		}
		function link_after_subscription(link)
		{
			window.parent.location = link;
			parent.jQuery.fancybox.close();
		}
		
		function on_feeds(id)
		{
			var dataString_news = 'feeds=on&ids='+id;
				
			$.ajax({
				type: 'POST',
				url : 'SubscriptionAjaxRadio.php',
				data: dataString_news,
				success: function(data) 
				{
					alert("Your settings are saved.");
				}
			});
		}
		
		function off_feeds(id)
		{
			var dataString_news = 'feeds=off&ids='+id;
				
			$.ajax({
				type: 'POST',
				url : 'SubscriptionAjaxRadio.php',
				data: dataString_news,
				success: function(data) 
				{
					alert("Your settings are saved.");
				}
			});
		}
		
		function on_emails(id,from,to,type)
		{
			var dataString_news = 'emails=on&ids='+id+'&from='+from+'&to='+to+'&type='+type;
				
			$.ajax({
				type: 'POST',
				url : 'SubscriptionAjaxRadio.php',
				data: dataString_news,
				success: function(data) 
				{
					alert("Your settings are saved.");
				}
			});
		}
		
		function off_emails(id,from,to,type)
		{
			var dataString_news = 'emails=off&ids='+id+'&from='+from+'&to='+to+'&type='+type;
				
			$.ajax({
				type: 'POST',
				url : 'SubscriptionAjaxRadio.php',
				data: dataString_news,
				success: function(data) 
				{
					alert("Your settings are saved.");
				}
			});
		}
</script>
<?php
$sendgrid = new SendGrid('','');
$mail_class_chk = new GetSubscriptionInfo();
if(!isset($_GET['pre_url']) && ($_GET['pre_url'] == null || $_GET['pre_url']=="")) //if($_GET['pre_url'] == null || $_GET['pre_url']=="")
{	
	if(isset($_SERVER["HTTP_REFERER"])){ $url = $_SERVER["HTTP_REFERER"];}
	else{ $url = "";}
	$url_sep = explode('/',$url);
	$match = $url_sep[3];
}else{
$match = $_GET['pre_url'];
}
//echo $match;
$display = new ProfileDisplay();
$subscription_become = new GetSubscriptionInfo();

$artist_check = $display->artistCheck($match);
$community_check = $display->communityCheck($match);

$community_event_check = $display->communityEventCheck($match);
$community_project_check = $display->communityProjectCheck($match);

$artist_event_check = $display->artistEventCheck($match);
$artist_project_check = $display->artistProjectCheck($match);


if($artist_check!=0)
{
	$get_user_Info = $artist_check;
	$get_rel_id_sub=$get_user_Info['artist_id'];
	$get_rel_type_sub="artist";
	//$get_fan_Info = $get_fan_club->get_price_info($artist_check['artist_id'],"artist");
	$find_member_sub = $subscription_become->find_member($artist_check['artist_id'],"artist");
	$url_popup = $display->profile_url_pop("artist",$artist_check['artist_id']);
	//var_dump($find_member);
}

if($community_check!=0)
{
	$get_user_Info = $community_check;
	$get_rel_id_sub = $get_user_Info['community_id'];
	$get_rel_type_sub = "community";
	//$get_fan_Info = $get_fan_club->get_price_info($community_check['community_id'],"community");
	$find_member_sub = $subscription_become->find_member($community_check['community_id'],"community");
	$url_popup = $display->profile_url_pop("community",$community_check['community_id']);
}

if($artist_event_check!=0)
{
	//$sql_rel_art = $display->eventrel_art_comm('artist',$artist_event_check['artist_id']);
	//var_dump($sql_rel_art);
	//die;
	$get_user_Info = $display->eventrel_art_comm('artist',$artist_event_check['artist_id']);
	$get_rel_id_sub = $artist_event_check['artist_id'].'_'.$artist_event_check['id'];
	$get_rel_type_sub = "artist_event";
	//$get_fan_Info = $get_fan_club->get_price_info($artist_check['artist_id'],"artist");
	$find_member_sub = $subscription_become->find_member($artist_event_check['artist_id'],"artist");
	$url_popup = $display->profileURLpop_eve_pro("artist_event",$artist_event_check['id']);
}

if($community_event_check!=0)
{
	//$sql_rel_art = $display->eventrel_art_comm('artist',$community_event_check['artist_id']);
	//var_dump($sql_rel_art);
	//die;
	$get_user_Info = $display->eventrel_art_comm('community',$community_event_check['community_id']);
	$get_rel_id_sub = $community_event_check['community_id'].'_'.$community_event_check['id'];
	$get_rel_type_sub = "community_event";
	//$get_fan_Info = $get_fan_club->get_price_info($artist_check['artist_id'],"artist");
	$find_member_sub = $subscription_become->find_member($community_event_check['community_id'],"community");
	$url_popup = $display->profileURLpop_eve_pro("community_event",$community_event_check['id']);
}

if($community_project_check!=0)
{
	//$sql_rel_art = $display->eventrel_art_comm('artist',$community_event_check['artist_id']);
	//var_dump($sql_rel_art);
	//die;
	$get_user_Info = $display->eventrel_art_comm('community',$community_project_check['community_id']);
	$get_rel_id_sub = $community_project_check['community_id'].'_'.$community_project_check['id'];
	$get_rel_type_sub = "community_project";
	//$get_fan_Info = $get_fan_club->get_price_info($artist_check['artist_id'],"artist");
	$find_member_sub = $subscription_become->find_member($community_project_check['community_id'],"community");
	$url_popup = $display->profileURLpop_eve_pro("community_project",$community_project_check['id']);
}

if($artist_project_check!=0)
{
	//$sql_rel_art = $display->eventrel_art_comm('artist',$community_event_check['artist_id']);
	//var_dump($sql_rel_art);
	//die;
	$get_user_Info = $display->eventrel_art_comm('artist',$artist_project_check['artist_id']);
	$get_rel_id_sub = $artist_project_check['artist_id'].'_'.$artist_project_check['id'];
	$get_rel_type_sub = "artist_project";
	//$get_fan_Info = $get_fan_club->get_price_info($artist_check['artist_id'],"artist");
	$find_member_sub = $subscription_become->find_member($artist_project_check['artist_id'],"artist");
	$url_popup = $display->profileURLpop_eve_pro("artist_project",$artist_project_check['id']);
}
//echo "%s &copy %s";
//var_dump($_POST);
//var_dump($find_member_sub);
//die;
	$present_sub_sets = array();
	$date = date('Y-m-d');
	if(isset($_SESSION['login_email'])){ $session_email = $_SESSION['login_email']; }else{ $session_email = ""; }
	$sql_email_check_db = $mail_class_chk->ajax_mail_chk($session_email,$get_rel_id_sub,$get_rel_type_sub);
	$sql_email_check_db = mysql_query($sql_email_check_db);
	if(mysql_num_rows($sql_email_check_db)>0)
	{
		$row_db = mysql_fetch_assoc($sql_email_check_db);
		if($row_db['expiry_date']>$date)
		{
			$present_sub_sets = $row_db;
		}
		// else
		// {
			// echo "You may proceed.";
		// }
	}
	
if($_POST == null)
{
	//echo "hi";
	//if($find_member_sub['count(*)']>0)
	//{
		?>
		<div class="fancy_header">
			Follow
		</div>
		<div class="email_subscribed_pop_content">
		<?php
		if($artist_event_check!=0 || $community_event_check!=0)
		{
		?>
			<p style="font-size:13px;">Edit your subscription settings of <?php if($artist_event_check!=0){ echo $artist_event_check['title']; $get_user_info_pro=$artist_event_check['title'];}elseif($community_event_check!=0) { echo $community_event_check['title']; $get_user_info_pro=$community_event_check['title']; } ?> using the following options: <?php //echo "Purify Cost"; ?></p>
		<?php
		}
		if($community_check!=0 || $artist_check!=0)
		{
		?>
			<p style="font-size:13px;">Edit your subscription settings of <?php echo $get_user_Info['name']; ?> using the following options: <?php //echo "Purify Cost"; ?></p>
		<?php
		}
		if($artist_project_check!=0 || $community_project_check!=0)
		{
		?>
			<p style="font-size:13px;">Edit your subscription settings of <?php if($artist_project_check!=0){ echo $artist_project_check['title']; $get_user_info_pro = $artist_project_check['title']; }elseif($community_project_check!=0) { echo $community_project_check['title']; $get_user_info_pro = $community_project_check['title']; } ?> using the following options: <?php //echo "Purify Cost"; ?></p>
		<?php
		}
		?>
		<div style="float: left;">
			<div>
				<form action="email_subscription.php?pre_url=<?php echo $url_sep[3];?>" method="POST" onSubmit="return validate_fields_sub()">
					<div class="fieldCont" style="display:none;">
						<div class="fieldTitle">Email</div>
						<input type="hidden" name="sub_email" id="sub_email" class="fieldText" />
					</div>
					<div class="hintR" id="member_email_new" style="display:none;"></div><a style="display:none; font-size:smaller; text-decoration:underline;" id="links" href="#" onClick="link_register()">To Register Click Here.</a>
					<div class="hintR" style="display:none;" id="member_email_same">You Cannot Subscribe to your own profile.</div>
					<div class="fieldCont" style="display:none;">
						<div class="fieldTitle">Name</div>
						<input type="hidden" name="sub_name" id="sub_name" class="fieldText" />
						<input type="hidden" name="profile_title" id="profile_title" class="fieldText" value="<?php echo $get_user_info_pro;?>"/>
					</div>
						<input type="hidden" name="rel_id_sub" id="rel_id_sub" class="fieldText" value="<?php echo $get_rel_id_sub; ?>" />
						<input type="hidden" name="rel_type_sub" id="rel_type_sub" class="fieldText" value="<?php echo $get_rel_type_sub; ?>" />
						<input type="hidden" name="subscribed_to_name" id="subscribed_to_name" class="fieldText" value="<?php if($artist_event_check!=0){ echo $artist_event_check['title']; }elseif($community_event_check!=0) { echo $community_event_check['title']; }elseif($artist_project_check!=0){ echo $artist_project_check['title']; }elseif($community_project_check!=0) { echo $community_project_check['title']; }elseif($community_check!=0 || $artist_check!=0){ echo $get_user_Info['name']; }  ?>" />
					<div class="affiBlock" style="width:380px;">
                        <div class="RowCont" style="width:400px;">
                            <div class="sideL">News Feed :</div>
                            <div class="sideR" style="float:left;width:150px;">
                            	
								<?php
									if(isset($present_sub_sets['news_feed']) && $present_sub_sets['news_feed']=='1')
									{
								?>
										<div class="chkCont">
											<input onClick="on_feeds('<?php echo $present_sub_sets['id']; ?>')" checked type="radio" name="feed_chck" class="chk" id="yes_feed" value="yes"/>
											<div class="radioTitle">On</div>
										</div>
										<div class="chkCont">
											<input onClick="off_feeds('<?php echo $present_sub_sets['id']; ?>')" type="radio" name="feed_chck" class="chk" id="no_feed" value="no"/>
											<div class="radioTitle">Off</div>
										</div>
								<?php
									}
									else
									{
								?>
										<div class="chkCont">
											<input onClick="on_feeds('<?php if(isset($present_sub_sets['id'])){ echo $present_sub_sets['id']; } ?>')" type="radio" name="feed_chck" class="chk" id="yes_feed" value="yes"/>
											<div class="radioTitle">On</div>
										</div>
										<div class="chkCont">
											<input onClick="off_feeds('<?php if(isset($present_sub_sets['id'])){ echo $present_sub_sets['id']; } ?>')" checked type="radio" name="feed_chck" class="chk" id="no_feed" value="no"/>
											<div class="radioTitle">Off</div>
										</div>
								<?php
									}
								?>
                            </div>
                        </div>
                        <div class="RowCont" style="width:400px;">
                            <div class="sideL">Email Update :</div>
                            <div class="sideR" style="float:left;width:150px;">
							<?php
								$type = '';
								if(isset($present_sub_sets['related_type']) && ($present_sub_sets['related_type']=='artist' || $present_sub_sets['related_type']=='artist_project' || $present_sub_sets['related_type']=='artist_event'))
								{
									$type = 'artist';
								}
								elseif(isset($present_sub_sets['related_type']) && ($present_sub_sets['related_type']=='community' || $present_sub_sets['related_type']=='community_project' || $present_sub_sets['related_type']=='community_event'))
								{
									$type = 'community';
								}
													
								if(isset($present_sub_sets['email_update']) && $present_sub_sets['email_update']=='1')
								{
							?>
									<div class="chkCont">
										<input onClick="on_emails('<?php echo $present_sub_sets['id']; ?>','<?php echo $_SESSION['login_email']; ?>','<?php echo $present_sub_sets['subscribe_to_id']; ?>','<?php echo $type; ?>')" checked type="radio" name="email_chck" class="chk" id="yes" value="yes"/>
										<div class="radioTitle">On</div>
									</div>
									<div class="chkCont">
										<input onClick="off_emails('<?php echo $present_sub_sets['id']; ?>','<?php echo $_SESSION['login_email']; ?>','<?php echo $present_sub_sets['subscribe_to_id']; ?>','<?php echo $type; ?>')" type="radio" name="email_chck" class="chk" id="no" value="no"/>
										<div class="radioTitle">Off</div>
									</div>
							<?php
								}
								else
								{
							?>
									<div class="chkCont">
										<input onClick="on_emails('<?php if(isset($present_sub_sets['id'])){ echo $present_sub_sets['id']; } ?>','<?php if(isset($_SESSION['login_email'])){ echo $_SESSION['login_email']; } ?>','<?php if(isset($present_sub_sets['general_user_id'])){ echo $present_sub_sets['general_user_id']; } ?>','<?php echo $type; ?>')" type="radio" name="email_chck" class="chk" id="yes" value="yes"/>
										<div class="radioTitle">On</div>
									</div>
									<div class="chkCont">
										<input onClick="off_emails('<?php if(isset($present_sub_sets['id'])){ echo $present_sub_sets['id']; } ?>','<?php if(isset($_SESSION['login_email'])){ echo $_SESSION['login_email']; } ?>','<?php if(isset($present_sub_sets['general_user_id'])){ echo $present_sub_sets['general_user_id']; } ?>','<?php echo $type; ?>')" checked type="radio" name="email_chck" class="chk" id="no" value="no"/>
										<div class="radioTitle">Off</div>
									</div>
							<?php
								}
							?>
                            </div>
                        </div>
                        <div class="RowCont" style="display:none;">
                            <div class="sideL">RSS :</div>
                            <div class="sideR"><?php echo 'www.'.$domainname.'/'.$url_popup['profile_url']; ?></div>
                        </div>
                    </div>
					<!--<div class="fieldCont" style="width:300px;">
						<div class="fieldTitle" style="width:0px;"></div>
						<input type="submit" value="Follow" id="sub_button" style="color: rgb(255, 255, 255); font-family: 'futura-pt',sans-serif; font-size: 0.825em; font-weight: bold; padding: 6px 10px; float: left; border: 0px none; background-color: rgb(0, 0, 0);"/>
					</div>-->
				</form>
			</div>	
		</div>
		</div>
		<?php
}
?>
