<?php session_start(); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "//www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="//www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<title>Purify Art: About: Contacts</title>
<?php 
include_once('sendgrid/SendGrid_loader.php');
include("includes/header.php");

$sendgrid = new SendGrid('','');
?>

<script>
	function validate_contct()
	{
		var name =  $("#contct_name").val();
		var email =  $("#contct_email").val();
		var msg =  $("#contct_msg").val();
		var atpos = email.indexOf("@");
		var dotpos = email.lastIndexOf(".");
		
		if(name=="")
		{
			alert("Please enter your name.");
			return false;
		}
		
		if(email=="")
		{
			alert("Please enter your email.");
			return false;
		}
		else if (atpos<1 || dotpos<atpos+2 || dotpos+2>=email.lenght || email=="" || email==null)
		{
			alert("Please enter a valid email address.");
			return false;
		}
		
		if(msg=="")
		{
			alert("Please enter appropriate message.");
			return false;
		}
		/*if($("#captcha").val()==""){
			alert("Please enter security code.");
			return false;
		}*/
	}
</script>

<div id="outerContainer"></div>
  <div id="contentContainer" >
    <div id="actualContent" class="sidepages">
      <h1>Contact</h1>
		<?php
			if(isset($_POST))
			{	
				if(!empty($_POST))
				{
					if($_POST['contct_name']!="" && $_POST['contct_email']!="" && $_POST['contct_msg']!="")
					{
						echo "<p>Your message has been sent successfully to contact@purifyart.com.</p>";
					}
				}
			}
		?>
		<form action="" method="POST" onsubmit="return validate_contct()">
			<div class="fieldCont">
				<div class="fieldTitle" title="Name">Name</div>
				<input type="text" class="fieldText" name="contct_name" id="contct_name" />
			</div>
			
			<div class="fieldCont">
				<div class="fieldTitle" title="Email">Email</div>
				<input type="text" class="fieldText" name="contct_email" id="contct_email" />
			</div>
			
			<div class="fieldCont">
				<div class="fieldTitle" title="type">Type</div>
				<select title="Type" class="dropdown" id="type_contct" name="type_contct">
					<option value="General Questions">General Questions</option> 
					<option value="Press Inquiry">Press Inquiry</option>
					<option value="Business Inquiry">Business Inquiry</option>
				</select>
			</div>
			
			<div class="fieldCont">
				<div class="fieldTitle" title="Message">Message</div>
				<textarea class="fieldText" style="height:auto;" cols="23" rows="6" id="contct_msg" name="contct_msg"></textarea>
			</div>
			<div class="fieldCont" style="margin:0;">
				<div class="fieldTitle" title="Message">Are you human?</div>
				<p style="float:left;margin-bottom:10px;">Only enter the 3 <b>black</b> characters:<br/>
					<img src="captcha.php" alt="captcha image" id="captcha" style="margin-right:15px;"/>
					<a href="#" onclick="document.getElementById('captcha').src = 'captcha.php?' + Math.random(); return false">Reload Image</a>
					<br />
					<input type="text" id="captcha" class="fieldText" name="captcha" size="3" maxlength="3"/>
				</p>
			</div>
			<?php
	if(isset($_POST))
	{	
		if(!empty($_POST))
		{
			if($_POST['contct_name']!="" && $_POST['contct_email']!="" && $_POST['contct_msg']!="")
			{
				if($_SESSION["captcha"]!==$_POST["captcha"])
				{
					echo '<div class="fieldCont"><div class="fieldTitle" title="Message"> </div><p style="float: left; margin: 0px; position: relative;">Characters do not match the black characters on the image.</p></div>';
				} else {
					$mail_grid = new SendGrid\Mail();
					$url = "admin/newsletter/"; 
					$myFile = $url . "other_templates.txt";
					$fh = fopen($myFile, 'r');
					$theData = fread($fh, filesize($myFile));
					fclose($fh);
					
					$data_email = explode('|~|', $theData);
					
					$ns_user_n = str_replace('{::message::}', $_POST['contct_msg'], $data_email['1']);
					
					$body_email = $data_email['0'] . $ns_user_n . $data_email['2'];
					$mail_grid->addTo('contact@purifyart.net')->
						setFromName($_POST['contct_email'])->
						setFrom('no-reply@purifyart.com')->
						setSubject($_POST['type_contct'])->
						setHtml($body_email);
						
						$sendgrid -> smtp -> send($mail_grid); 
				}
				
			}
		}
	}
?>
			<div class="fieldCont">
				<div class="fieldTitle"></div>
				<input type="submit" value="Submit" class="register" style="position:relative; font-size: 12px; "/>
			</div>
		</form>
		
      <!--<p><strong>General Questions: </strong>contact@purifyart.com</p>-->
      <!--<p><strong>Office Phone: </strong>650.262.6603</p>
      <p><strong>Mailing Address:</strong><br />
        Purify Art<br />
        PO Box 333<br />
        Half Moon Bay, CA 94019</p>
      <br />-->
    </div>
    <div id="snipeSidebar">
    	<div class="snipe">
      	<h2 >ART WITH INTEGRITY</h2>
         <p>Find and promote music, art and video on our ad-free platform. <a href="about.php" style="padding-left: 5px;">More></a></p>
      </div>
    </div>
    
    <div class="clearMe"></div>
</div>

<!-- end of main container -->
</div><?php include("displayfooter.php");?>
</body>
</html>

