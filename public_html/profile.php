<?php session_start();
error_reporting(0);
include_once("commons/db.php");
include_once('classes/Artist.php');
include_once('classes/Media.php');
include_once('classes/CountryState.php');
include_once('classes/Type.php');
include_once('classes/SubType.php');
include_once('classes/MetaType.php');
include_once('classes/ProfileDisplay.php');
include_once('classes/Commontabs.php');
include_once('classes/Addfriend.php');
include_once("AjaxButtonsTest.php");
include_once("AjaxButtonsTestCom.php");
include_once("classes/ViewArtistProfileURL.php");
include_once("classes/ViewCommunityProfileURL.php");
include_once("classes/viewCommunityEventURL.php");
include_once("classes/viewArtistEventURL.php");
include_once("classes/viewArtistProjectURL.php");
include_once("classes/viewCommunityProjectURL.php");
include_once("classes/DisplayStatistics.php");

include_once("classes/PopularityMeter.php");
require_once('vimeo-vimeo-php-lib/vimeo.php');

$all_stats = new DisplayStatistics();
$friend_obj_new = new Addfriend();
$button_test = new AjaxButtonsTest();
$button_test_com = new AjaxButtonsTestCom();
$popular_obj = new PopularityMeter();

$objArtist=new Artist();
$objMedia=new Media();	
$logedinFlag = FALSE;

$key = '6b55800e7503e2cb8d69f93d2f7bda0df9fac434';
$secret = 'f924407f6edf2e3398092516363de7b562a9d4a5';
$vimeo = new phpVimeo($key, $secret);
//var_dump($_GET);
//if(isset($_GET['uid'])){
	//$uid = $_GET['uid'];	
//}
$down_button = "";
if($_REQUEST['username'])
{
	$username=$_REQUEST['username'];
	$userpass=$_REQUEST['userpass'];
	$flag=$objMedia->checkUserLogin($username,$userpass);
	if($flag !="")
	{
		$_SESSION['userInfo'] = $username;
		$_SESSION['gen_user_id'] = $flag['general_user_id'];
		$_SESSION['login_email']=$username;
		$_SESSION['login_id']=$flag['general_user_id'];
		$_SESSION['name']=$flag['fname'].$flag['lname'];
		$_SESSION['artist_id']=$flag['artist_id'];
		$_SESSION['community_id']=$flag['community_id'];
		$_SESSION['member_id']=$flag['member_id'];
		$_SESSION['team_id']=$flag['team_id'];
		$logedinFlag=TRUE;
	}	
}

if(isset($_SESSION['userInfo']))
{
	$logedinFlag = TRUE;
}

if(isset($_SESSION['login_email']))
{
	$_SESSION['gen_user_id'] = $_SESSION['login_id'];
	$logedinFlag = TRUE;
}

//***************************************View as per Profile URL***********************************************//
$url = $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
//echo $url;
$url_sep = explode('/',$url);
$url_sep1 = explode('&',$url_sep[1]);
$match = $url_sep1[0];

$display = new ProfileDisplay();
$artist_check = $display->artistCheck($match);
$community_check = $display->communityCheck($match);

$community_event_check = $display->communityEventCheck($match);
$community_project_check = $display->communityProjectCheck($match);

$artist_event_check = $display->artistEventCheck($match);
$artist_project_check = $display->artistProjectCheck($match);

$imports = 0;
$bio_change_range = 0;

if($artist_check!=0)
{
	//echo "artist";
	//echo "hii";
	$new_profile_class_obj = new ViewArtistProfileURL();
	$getgeneral =  $new_profile_class_obj->get_user_info($artist_check['artist_id']);
	
	$get_user_delete_or_not = $new_profile_class_obj->chk_user_delete($artist_check['artist_id']);
	$get_common_id=$artist_check['artist_id'];
	
	//var_dump($getgeneral);
	$getuser = $new_profile_class_obj->get_user_artist_profile($artist_check['artist_id']);
	$getcountry = $new_profile_class_obj->get_user_country($artist_check['artist_id']);
	$getstate = $new_profile_class_obj->get_user_state($artist_check['artist_id']);
	$gettype = $new_profile_class_obj->get_artist_user_type($artist_check['artist_id']);
	$get_type_dis = $new_profile_class_obj->get_artist_user($artist_check['artist_id']);
	
	$get_subtype = $new_profile_class_obj->get_artist_user_subtype($artist_check['artist_id']);
	$get_subtype_dis = $new_profile_class_obj->get_artist_user_subs($artist_check['artist_id']);
	
	//var_dump($get_subtype);
	$get_metatype = $new_profile_class_obj->get_artist_user_metatype($artist_check['artist_id']);
	$src = "http://artjcropprofile.s3.amazonaws.com/";
	$src_list = "http://artjcropthumb.s3.amazonaws.com/";
	$src1 = "../";
	
	$project_exp = explode(',',$getuser['taggedprojects']);
	if(isset($project_exp) && count($project_exp)>0 && !empty($project_exp))
	{
		for($p=0;$p<count($project_exp);$p++)
		{
			$project_exp_n = explode('~',$project_exp[$p]);
			if($project_exp_n[1]=='art')
			{
				$get_project[$p] = $new_profile_class_obj->get_artist_project($project_exp_n[0]);
			}
			elseif($project_exp_n[1]=='com')
			{
				$get_project[$p] = $new_profile_class_obj->get_community_project($project_exp_n[0]);
			}
		}
	}
	
	//var_dump($get_project);
	//die;
	$get_media_Info = $new_profile_class_obj->get_artist_Info($artist_check['artist_id']);
	$date=date('y-m-d');
	
	$event_exp_rec = explode(',',$getuser['taggedrecordedevents']);
	
	if(isset($event_exp_rec) && count($event_exp_rec)>0)
	{
		for($i=0;$i<count($event_exp_rec);$i++)
		{
			if($event_exp_rec[$i]==""){continue;}
			else
			{
				$act_id = explode('~',$event_exp_rec[$i]);
				
				if($act_id[1]=='art')
				{
					$get_recorded_event[$i] = $new_profile_class_obj->recordedArtistEvents($act_id[0],$date);
				}
				elseif($act_id[1]=='com')
				{
					$get_recorded_event[$i] = $new_profile_class_obj->recordedComEvents($act_id[0],$date);
				}
			}
		}
	}
	else{
		$noevent_rec =1;
	}
	$event_exp_up = explode(',',$getuser['taggedupcomingevents']);

	if(isset($event_exp_up) && count($event_exp_up)>0)
	{
		for($i=0;$i<count($event_exp_up);$i++)
		{
			if($event_exp_up[$i]==""){continue;}
			else
			{
				$act_id = explode('~',$event_exp_up[$i]);
				
				if($act_id[1]=='art')
				{
					$get_upcoming_event[$i] = $new_profile_class_obj->upcomingArtistEvents($act_id[0],$date);
				}
				elseif($act_id[1]=='com')
				{
					$get_upcoming_event[$i] = $new_profile_class_obj->upcomingComEvents($act_id[0],$date);
				}
			}
		}
	}
	else{
	$noevent_up =1;
	}

	$get_song=explode(',',$get_media_Info['taggedsongs']);
	$get_video_pro_uniq_tabs_chk = explode(',',$get_media_Info['taggedvideos']);
	$get_gal=explode(',',$get_media_Info['taggedgalleries']);
	
	$sql_general = mysql_query("SELECT * FROM general_user WHERE artist_id='".$getuser['artist_id']."'");
	$ans_sql = mysql_fetch_assoc($sql_general);
	
	$get_permission = $new_profile_class_obj->get_all_permission($getgeneral['general_user_id']);
	$get_user_id = $new_profile_class_obj->get_loggedin_user_id($_SESSION['login_email']);
	$find_member = $new_profile_class_obj->get_member_or_not($getgeneral['general_user_id']);
	//var_dump($get_permission);
	//var_dump($get_user_id);
	
	if($_SESSION['login_email']!=NULL)
	{
		$down_button = $button_test->Artist_project($get_user_id['general_user_id']);
	}
	
	if($get_media_Info['imported']==1)
	{
		$imports = 1;
	}
}

if($community_check!=0)
{
	//echo "community";
	$new_profile_class_obj=new ViewCommunityProfileURL();
	$getgeneral =  $new_profile_class_obj->get_user_info($community_check['community_id']);
	$get_user_delete_or_not =  $new_profile_class_obj->chk_user_delete($community_check['community_id']);
	$get_common_id=$community_check['community_id'];
	
	$getuser=$new_profile_class_obj->get_user_community_profile($community_check['community_id']);
	$getcountry=$new_profile_class_obj->get_user_country($community_check['community_id']);
	$getstate=$new_profile_class_obj->get_user_state($community_check['community_id']);
	$gettype=$new_profile_class_obj->get_community_user_type($community_check['community_id']);
	$get_subtype=$new_profile_class_obj->get_community_user_subtype($community_check['community_id']);
	
	$get_type_dis = $new_profile_class_obj->get_community_user($community_check['community_id']);
	$get_subtype_dis = $new_profile_class_obj->get_community_user_subs($community_check['community_id']);
	
	$get_metatype=$new_profile_class_obj->get_artist_user_metatype($community_check['community_id']);
	$src = "http://comjcropprofile.s3.amazonaws.com/"; 
	$src_list = "http://comjcropthumb.s3.amazonaws.com/";
	$src1 = "../";

	$project_exp = explode(',',$getuser['taggedprojects']);
	if(isset($project_exp) && count($project_exp)>0)
	{
		for($p=0;$p<count($project_exp);$p++)
		{
			$project_exp_n = explode('~',$project_exp[$p]);
			if($project_exp_n[1]=='art')
			{
				$get_project[$p] = $new_profile_class_obj->get_artist_projects($project_exp_n[0]);
			}
			elseif($project_exp_n[1]=='com')
			{
				$get_project[$p] = $new_profile_class_obj->get_community_projects($project_exp_n[0]);
			}
		}
	}
	
	//$get_project = $new_profile_class_obj->get_community_projects($community_check['community_id']);
	$get_media_Info = $new_profile_class_obj->get_community_Info($community_check['community_id']);
	$date=date('y-m-d');
	
	$event_exp_rec = explode(',',$getuser['taggedrecordedevents']);
	if(isset($event_exp_rec) && count($event_exp_rec)>0)
	{
		for($i=0;$i<count($event_exp_rec);$i++)
		{
			if($event_exp_rec[$i]==""){continue;}
			else
			{
				$act_id = explode('~',$event_exp_rec[$i]);
				
				if($act_id[1]=='com')
				{
					$get_recorded_event[$i] = $new_profile_class_obj->recordedEvents($act_id[0],$date);
				}
				elseif($act_id[1]=='art')
				{
					$get_recorded_event[$i] = $new_profile_class_obj->recordedArtEvents($act_id[0],$date);
				}
			}
		}
	}
	else{
	$noevent_rec =1;
	}
	
	$event_exp_up = explode(',',$getuser['taggedupcomingevents']);
	if(isset($event_exp_up) && count($event_exp_up)>0)
	{
		for($i=0;$i<count($event_exp_up);$i++)
		{
			if($event_exp_up[$i]==""){continue;}
			else
			{
				$act_id = explode('~',$event_exp_up[$i]);
				
				if($act_id[1]=='com')
				{
					$get_upcoming_event[$i] = $new_profile_class_obj->upcomingEvents($act_id[0],$date);
				}
				elseif($act_id[1]=='art')
				{
					$get_upcoming_event[$i] = $new_profile_class_obj->upcomingArtEvents($act_id[0],$date);
				}
			}
		}
	}
	else{
	$noevent_up =1;
	}
	
	//$get_recorded_event = $new_profile_class_obj->recordedEvents($community_check['community_id'],$date);
	//$get_upcoming_event = $new_profile_class_obj->upcomingEvents($community_check['community_id'],$date);
	$get_song=explode(',',$get_media_Info['taggedsongs']);
	$get_video_pro_uniq_tabs_chk=explode(',',$get_media_Info['taggedvideos']);
	$get_gal=explode(',',$get_media_Info['taggedgalleries']);
	
	$get_permission = $new_profile_class_obj->get_all_permission($getgeneral['general_user_id']);
	$get_user_id = $new_profile_class_obj->get_loggedin_user_id($_SESSION['login_email']);
	$find_member = $new_profile_class_obj->get_member_or_not($getgeneral['general_user_id']);
	//var_dump($get_permission);
	
	if($_SESSION['login_email']!=NULL)
	{
		$down_button = $button_test_com->Community_project($get_user_id['general_user_id']);
	}
}

if($community_event_check!=0)
{
	//echo "communityevent";
	$new_profile_class_obj = new viewCommunityEventURL();
	$id = $community_event_check['id'];
	$art_id = $community_event_check['community_id'];
	$get_common_id = $community_event_check['community_id'];
	$getgeneral =  $new_profile_class_obj->get_generalUser($art_id);
	$get_user_delete_or_not =  $new_profile_class_obj->chk_user_delete($art_id);
	$getuser = $new_profile_class_obj->get_CommunityEvent($art_id,$id);
	$Find_Artist = $new_profile_class_obj->chk_for_artist_tab($getuser['tagged_user_email']);
	$Find_Company = $new_profile_class_obj->chk_for_company_tab($getuser['tagged_user_email']);
	$all_artist = explode(",",$getuser['tagged_user_email']);
	$find_delete_or_not = $new_profile_class_obj->find_delete_or_not($art_id,$id);
	
	$get_project = $new_profile_class_obj->get_project_count($getuser[0]);
	$get_user_exp = explode(',',$getuser['tagged_projects']);
	$upcom_event = explode(',',$getuser['taggedupcomingevents']);
	$rec_event = explode(',',$getuser['taggedrecordedevents']);

	$date=date('y-m-d');
	$exps = explode('~',$rec_event[0]);
	if($exps[1]=='art')
	{
		$get_recorded_event = $new_profile_class_obj->recordedArtEvents($date,$exps[0]);
	}
	elseif($exps[1]=='com')
	{
		$get_recorded_event = $new_profile_class_obj->recordedComEvents($date,$exps[0]);
	}
	
	$exps_1 = explode('~',$upcom_event[0]);
	if($exps_1[1]=='art')
	{
		$get_upcoming_event = $new_profile_class_obj->upcomingArtEvents($date,$exps_1[0]);
	}
	elseif($exps_1[1]=='com')
	{
		$get_upcoming_event = $new_profile_class_obj->upcomingComEvents($date,$exps_1[0]);
	}
	
	$getstate = $new_profile_class_obj->getState($art_id,$id);
	$getcountry = $new_profile_class_obj->getCountry($art_id,$id);
	$gettype = $new_profile_class_obj->getType($id);
	$get_subtype = $new_profile_class_obj->getSubType($id);
	
	$get_type_dis = $new_profile_class_obj->getMinType($id);
	$get_subtype_dis = $new_profile_class_obj->getMinType_subs($id);
	
	$get_metatype = $new_profile_class_obj->getMetaType($id);
	$src = ""; 
	//$src1 = "../community_event_jcrop/croppedFiles/profile";
	$get_media_Info = $new_profile_class_obj->get_All_Media($community_event_check['id']);
	//var_dump($get_media_Info);
	//die;
	$date=date('y-m-d');
	$get_song=explode(',',$get_media_Info['tagged_songs']);
	$get_video_pro_uniq_tabs_chk=explode(',',$get_media_Info['tagged_videos']);
	$get_gal=explode(',',$get_media_Info['tagged_galleries']);
	$all_art = explode(',',$get_media_Info['artist_view_selected']);
	$table_type = "community";
	//echo $getgeneral['general_user_id'];
	
	$get_permission = $new_profile_class_obj->get_all_permission($getgeneral['general_user_id']);
	$get_user_id = $new_profile_class_obj->get_loggedin_user_id($_SESSION['login_email']);
	$find_member = $new_profile_class_obj->get_member_or_not($getgeneral['general_user_id']);
//	$src_list = "https://comjcropthumb.s3.amazonaws.com/";
	//var_dump($get_permission);
	
	if($_SESSION['login_email']!=NULL)
	{
		$down_button = $button_test_com->Community_project($get_user_id['general_user_id']);
		//var_dump($donw_button);
	}
}

if($artist_event_check!=0)
{
	//echo "artistevent";
	$new_profile_class_obj = new viewArtistEventURL();
	$id = $artist_event_check['id'];
	$art_id = $artist_event_check['artist_id'];
	$get_common_id = $artist_event_check['artist_id'];
	$getgeneral =  $new_profile_class_obj->get_generalUser($art_id);
	$get_user_delete_or_not =  $new_profile_class_obj->chk_user_delete($art_id);
	$getuser = $new_profile_class_obj->get_ArtistEvent($art_id,$id);
	$Find_Artist = $new_profile_class_obj->chk_for_artist_tab($getuser['tagged_user_email']);
	$Find_Company = $new_profile_class_obj->chk_for_company_tab($getuser['tagged_user_email']);
	$all_artist = explode(",",$getuser['tagged_user_email']);
	$find_delete_or_not = $new_profile_class_obj->find_delete_or_not($art_id,$id);
	$get_user_exp = explode(',',$getuser['tagged_projects']);
	$get_project = $new_profile_class_obj->get_project_count($getuser[0]);
	//var_dump($getuser);
	$upcom_event = explode(',',$getuser['taggedupcomingevents']);
	$rec_event = explode(',',$getuser['taggedrecordedevents']);

	$date = date('y-m-d');
	$exps = explode('~',$rec_event[0]);
	if($exps[1]=='art')
	{
		$get_recorded_event = $new_profile_class_obj->recordedArtEvents($date,$exps[0]);
	}
	elseif($exps[1]=='com')
	{
		$get_recorded_event = $new_profile_class_obj->recordedComEvents($date,$exps[0]);
	}
	
	$exps_1 = explode('~',$upcom_event[0]);
	if($exps_1[1]=='art')
	{
		$get_upcoming_event = $new_profile_class_obj->upcomingArtEvents($date,$exps_1[0]);
	}
	elseif($exps_1[1]=='com')
	{
		$get_upcoming_event = $new_profile_class_obj->upcomingComEvents($date,$exps_1[0]);
	}
	
	$getstate = $new_profile_class_obj->getState($art_id,$id);
	$getcountry = $new_profile_class_obj->getCountry($art_id,$id);
	$gettype = $new_profile_class_obj->getType($id);
	$get_subtype = $new_profile_class_obj->getSubType($id);
	
	$get_type_dis = $new_profile_class_obj->getMinType($id);
	$get_subtype_dis = $new_profile_class_obj->getMinType($id);
	
	$get_metatype = $new_profile_class_obj->getMetaType($id);
	$src = "";
	//$src1 = "../artist_event_jcrop/croppedFiles/profile";
	$get_media_Info = $new_profile_class_obj->get_All_Media($artist_event_check['id']);
	//var_dump($get_media_Info);
	//die;
	$date=date('y-m-d');
	$get_song=explode(',',$get_media_Info['tagged_songs']);
	$get_video_pro_uniq_tabs_chk=explode(',',$get_media_Info['tagged_videos']);
	$get_gal=explode(',',$get_media_Info['tagged_galleries']);
	$all_art = explode(',',$get_media_Info['artist_view_selected']);
	$table_type = "artist";
	
	$get_permission = $new_profile_class_obj->get_all_permission($getgeneral['general_user_id']);
	$get_user_id = $new_profile_class_obj->get_loggedin_user_id($_SESSION['login_email']);
	$find_member = $new_profile_class_obj->get_member_or_not($getgeneral['general_user_id']);
//	$src_list = "https://artjcropthumb.s3.amazonaws.com/";

	if($_SESSION['login_email']!=NULL)
	{
		$down_button = $button_test->Artist_project($get_user_id['general_user_id']);
	}
}

if($artist_project_check!=0)
{
	//echo "artistproject";
	$new_profile_class_obj = new viewArtistProjectURL();
	$id = $artist_project_check['id'];
	$art_id = $artist_project_check['artist_id'];
	$get_common_id = $artist_project_check['artist_id'];
	$getgeneral =  $new_profile_class_obj->get_generalUser($art_id);
	$get_user_delete_or_not =  $new_profile_class_obj->chk_user_delete($art_id);
	$getuser = $new_profile_class_obj->get_ArtistProject($art_id,$id);
	$Find_Artist = $new_profile_class_obj->chk_for_artist_tab($getuser['tagged_user_email']);
	$Find_Company = $new_profile_class_obj->chk_for_company_tab($getuser['tagged_user_email']);
	$all_artist = explode(",",$getuser['tagged_user_email']);
	$find_delete_or_not = $new_profile_class_obj->find_delete_or_not($art_id,$id);
	//var_dump($getuser);	
	
	/*$project_exp = explode(',',$getuser['taggedprojects']);
	if(isset($project_exp) && count($project_exp)>0)
	{
		for($p=0;$p<count($project_exp);$p++)
		{
			$get_project[$p] = $new_profile_class_obj->get_project_count($project_exp[$p]);
		}
	}*/
	
	$get_project = $new_profile_class_obj->get_project_count($getuser[0]);
	$get_user_exp = explode(',',$getuser['tagged_projects']);
	$upcom_event = explode(',',$getuser['taggedupcomingevents']);
	//var_dump($upcom_event);
	$rec_event = explode(',',$getuser['taggedrecordedevents']);

	$date=date('y-m-d');
	//var_dump($upcom_event);
	$exps = explode('~',$rec_event[0]);
	if($exps[1]=='art')
	{
		$get_recorded_event = $new_profile_class_obj->recordedArtEvents($date,$exps[0]);
	}
	elseif($exps[1]=='com')
	{
		$get_recorded_event = $new_profile_class_obj->recordedComEvents($date,$exps[0]);
	}
	
	$exps_1 = explode('~',$upcom_event[0]);
	if($exps_1[1]=='art')
	{
		$get_upcoming_event = $new_profile_class_obj->upcomingArtEvents($date,$exps_1[0]);
	}
	elseif($exps_1[1]=='com')
	{
		$get_upcoming_event = $new_profile_class_obj->upcomingComEvents($date,$exps_1[0]);
	}	
	
	$getstate = $new_profile_class_obj->getState($art_id,$id);
	$getcountry = $new_profile_class_obj->getCountry($art_id,$id);
	$gettype = $new_profile_class_obj->getType($id);
	$get_subtype = $new_profile_class_obj->getSubType($id);
	
	$get_type_dis = $new_profile_class_obj->getMinType($id);
	$get_subtype_dis = $new_profile_class_obj->getMinType_subs($id);
	
	$get_metatype = $new_profile_class_obj->getMetaType($id);
	$all_media = $new_profile_class_obj->get_all_project_media($id);
	$src = "";
	//$src1 = "../artist_project_jcrop/croppedFiles/profile";
	$get_media_Info = $new_profile_class_obj->get_all_project_media($artist_project_check['id']);
	//var_dump($get_media_Info);
	//die;
	$date=date('y-m-d');
	$get_song=explode(',',$get_media_Info['tagged_songs']);
	$get_video_pro_uniq_tabs_chk=explode(',',$get_media_Info['tagged_videos']);
	$get_gal=explode(',',$get_media_Info['tagged_galleries']);
	$all_art = explode(',',$get_media_Info['artist_view_selected']);
	$table_type = "artist";
	
	$get_permission = $new_profile_class_obj->get_all_permission($getgeneral['general_user_id']);
	$get_user_id = $new_profile_class_obj->get_loggedin_user_id($_SESSION['login_email']);
	$find_member = $new_profile_class_obj->get_member_or_not($getgeneral['general_user_id']);
//	$src_list = "https://artjcropthumb.s3.amazonaws.com/";

	if($_SESSION['login_email']!=NULL)
	{
		$down_button = $button_test->Artist_project($get_user_id['general_user_id']);
		//var_dump($down_button);
	}
	
	$exp_crt_dummy_pros_chk = explode('|',$getuser['creators_info']);
	if($exp_crt_dummy_pros_chk[0]=='general_artist')
	{
		$sql_art_dummy_chk = mysql_query("SELECT * FROM general_artist WHERE artist_id='".$exp_crt_dummy_pros_chk[1]."'");
		$ans_art_dummy_chk = mysql_fetch_assoc($sql_art_dummy_chk);
		if($ans_art_dummy_chk['imported']==1)
		{
			$imports = 1;
		}
	}
	//if()
}

if($community_project_check!=0)
{
	//echo "communityproject";
	$new_profile_class_obj = new viewCommunityProjectURL();
	$id = $community_project_check['id'];
	$art_id = $community_project_check['community_id'];
	$get_common_id = $community_project_check['community_id'];
	$getgeneral =  $new_profile_class_obj->get_generalUser($art_id);
	$get_user_delete_or_not =  $new_profile_class_obj->chk_user_delete($art_id);
	$getuser = $new_profile_class_obj->get_CommunityProject($art_id,$id);
	
	$Find_Artist = $new_profile_class_obj->chk_for_artist_tab($getuser['tagged_user_email']);
	$Find_Company = $new_profile_class_obj->chk_for_company_tab($getuser['tagged_user_email']);
	$all_artist = explode(",",$getuser['tagged_user_email']);
 	$find_delete_or_not = $new_profile_class_obj->find_delete_or_not($art_id,$id);
	
	$get_project = $new_profile_class_obj->get_project_count($getuser[0]);
	$get_user_exp = explode(',',$getuser['tagged_projects']);
	$upcom_event = explode(',',$getuser['taggedupcomingevents']);
	$rec_event = explode(',',$getuser['taggedrecordedevents']);

	$date=date('y-m-d');

	$exps = explode('~',$rec_event[0]);
	if($exps[1]=='art')
	{
		$get_recorded_event = $new_profile_class_obj->recordedArtEvents($date,$exps[0]);
	}
	elseif($exps[1]=='com')
	{
		$get_recorded_event = $new_profile_class_obj->recordedComEvents($date,$exps[0]);
	}
	
	$exps_1 = explode('~',$upcom_event[0]);
	if($exps_1[1]=='art')
	{
		$get_upcoming_event = $new_profile_class_obj->upcomingArtEvents($date,$exps_1[0]);
	}
	elseif($exps_1[1]=='com')
	{
		$get_upcoming_event = $new_profile_class_obj->upcomingComEvents($date,$exps_1[0]);
	}
	
	$getstate = $new_profile_class_obj->getState($art_id,$id);
	$getcountry = $new_profile_class_obj->getCountry($art_id,$id);
	$gettype = $new_profile_class_obj->getType($id);
	$get_subtype = $new_profile_class_obj->getSubType($id);
	
	$get_type_dis = $new_profile_class_obj->getMinType($id);
	$get_subtype_dis = $new_profile_class_obj->getMinType_subs($id);	
	
	$get_metatype = $new_profile_class_obj->getMetaType($id);
	$src = ""; 
	//$src1 = "../community_project_jcrop/croppedFiles/profile";
	$get_media_Info = $new_profile_class_obj->get_all_media($community_project_check['id']);
	//var_dump($get_media_Info);
	//die;
	$date=date('y-m-d');
	$get_song=explode(',',$get_media_Info['tagged_songs']);
	$get_video_pro_uniq_tabs_chk=explode(',',$get_media_Info['tagged_videos']);
	$get_gal=explode(',',$get_media_Info['tagged_galleries']);
	$all_art = explode(',',$get_media_Info['artist_view_selected']);
	$table_type = "community";
	
	$get_permission = $new_profile_class_obj->get_all_permission($getgeneral['general_user_id']);
	$get_user_id = $new_profile_class_obj->get_loggedin_user_id($_SESSION['login_email']);
	$find_member = $new_profile_class_obj->get_member_or_not($getgeneral['general_user_id']);
//	$src_list = "https://comjcropthumb.s3.amazonaws.com/";

if($_SESSION['login_email']!=NULL)
	{
		$down_button = $button_test_com->Community_project($get_user_id['general_user_id']);
		//var_dump($donw_button);
	}
}

//******************************END OF View as per Profile URL*******************************//

	$userInfo=$objArtist->getUserInfo($uid);
	//print_r($userInfo);
	
	$artistInfo=$objArtist->getArtistInfo($userInfo['artist_id']);
	//print_r($artistInfo);
	
	$artistProfileInfo=$objArtist->getArtistProfileInfo($userInfo['artist_id']);
	//print_r($artistProfileInfo);

	$mediaData = $objMedia->searchMediaByProfileId($userInfo['artist_id'],'artist');
	//echo "<pre>";print_r($mediaData);
	
	$objCountryState=new CountryState();
	$state=$objCountryState->getStateName($artistInfo['state_id']);
	$country=$objCountryState->getCountryName($artistInfo['country_id']);
	//print_r( $state);
	
	$type=new Type();
	$subtype=new SubType();
	$metatype=new MetaType();
	
	$galleryImagesData = "";
	$galleryImagesDataIndex = 0;
	$thumbPath = "https://medgalthumb.s3.amazonaws.com/";
	$orgPath = "https://medgalhighres.s3.amazonaws.com/";

//print_r($mediaData);

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "https://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="https://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php 
//header("Content-Type: text/javascript; charset=UTF-8");
if($getuser['name']!="" || $getuser['title']!="")
{
?>
<meta property="og:title" content="<?php if($getuser['name']!="") { echo $getuser['name']; } else { echo $getuser['title']; } ?>"/>
<?php
}
if($src.$getuser['image_name']!="")
{
?>
<meta property="og:image" content="<?php if($src.$getuser['image_name']!="") { echo $src.$getuser['image_name']; } else { echo "http://artjcropthumb.s3.amazonaws.com/Noimage.png"; } ?>"/>
<?php
}
if($getuser['bio']!="" || $getuser['description']!="")
{
?>
<meta property="og:description" content="<?php if($getuser['bio']!="") { echo strip_tags($getuser['bio']); } elseif($getuser['description']!="") { echo strip_tags($getuser['description']); }  ?>"/>
<?php
}

//header("Content-Type: text/html; charset=UTF-8");
?>
<!--<meta property="og:message" content="<?php //echo "hii"; ?>"/>
<div id="outerContainer">-->
<?php
if($artist_check!=0){
?><title><?php echo $artist_check['name'];?></title>
<?php }
if($community_check!=0){
?><title><?php echo $community_check['name'];?></title>
<?php }
if($artist_event_check!=0){
?><title><?php echo $artist_event_check['title'];?></title>
<?php }
if($artist_project_check!=0){
?><title><?php echo $artist_project_check['title'];?></title>
<?php }
if($community_event_check!=0){
?><title><?php echo $community_event_check['title'];?></title>
<?php }
if($community_project_check!=0){
?><title><?php echo $community_project_check['title'];?></title>
<?php }
?>
<!--</div>-->

<?php
if(isset($_SESSION['login_email']))
{
$common_chk = new Commontabs();
$chks_se = $common_chk->tabs();
if(!empty($chks_se))
{
?>
	<script>

	$("document").ready(function(){
	if(<?php echo $artist_check ;?> != 0)
	{
		<?php if($artist_check['artist_id'] == $chks_se['artist_id']) 
		{ 
		?>
		
			$("#edit_profile_page").attr("href", "profileedit_artist.php");
		<?php
		}
		?>
	}
	else if(<?php echo $community_check ;?> != 0)
	{
		<?php if($community_check['community_id'] == $chks_se['community_id'])
		{ 
		?>
			$("#edit_profile_page").attr("href", "profileedit_community.php");
		<?php
		}
		?>
	}
	else if(<?php echo $community_event_check ;?> != 0)
	{
		<?php if($community_event_check['community_id'] == $chks_se['community_id'])
		{
		?>
			$("#edit_profile_page").attr("href", "add_community_event.php?id=<?php echo $community_event_check['id']?>");
		<?php
		}
		?>
	}
	else if(<?php echo $community_project_check ;?> != 0)
	{
		<?php if($community_project_check['community_id'] == $chks_se['community_id']) 
		{
		?>
			$("#edit_profile_page").attr("href", "add_community_project.php?id=<?php echo $community_project_check['id']?>");
		<?php
		}
		?>
	}
	else if(<?php echo $artist_event_check ;?> != 0)
	{
		<?php if($artist_event_check['artist_id'] == $chks_se['artist_id'])
		{
		?>
			$("#edit_profile_page").attr("href", "add_artist_event.php?id=<?php echo $artist_event_check['id']?>");
		<?php
		}
		?>
	}
	else if(<?php echo $artist_project_check ;?> != 0)
	{
		<?php if($artist_project_check['artist_id'] == $chks_se['artist_id'])
		{
		?>
			$("#edit_profile_page").attr("href", "add_artist_project.php?id=<?php echo $artist_project_check['id']?>");
		<?php
		}
		?>
	}
	});
	</script>
<?php
	}
}
if($community_event_check!=0)
{
	if($get_permission['community_event'] == "private" && $_SESSION['login_email'] != $getgeneral['email'])
	{
		echo "<div style='height:200px;'><h4 style='text-align:center;'>This Page Is Only For Private Users</h4></div>";
		include_once("displayfooter.php");
		die;
	}
	else if($get_permission['community_event'] == "public" && !isset($_SESSION['login_email']))
	{
		echo "<div style='height:200px;'><h4 style='text-align:center;'>This Page Is Only For Registered Users</h4></div>";
		include_once("displayfooter.php");
		die;
	}
	else if($get_permission['community_event'] == "members" && ($find_member['expiry_date']<$date || $find_member['lifetime']!=1))
	{
		echo "<div style='height:200px;'><h4 style='text-align:center;'>This Page Is Only For Purify Members</h4></div>";
		include_once("displayfooter.php");
		die;
	}
	else if(mysql_num_rows($find_delete_or_not)>0)
	{
		echo "<div style='height:200px;'><h4 style='text-align:center;'>This Event Has Been Deleted By It's Creator.</h4></div>";
		include_once("displayfooter.php");
		die;
	}
	
}

if($community_project_check!=0)
{
	if($get_permission['community_project'] == "private" && $_SESSION['login_email'] != $getgeneral['email'])
	{
		echo "<div style='height:200px;'><h4 style='text-align:center;'>This Page Is Only For Private Users</h4></div>";
		include_once("displayfooter.php");
		die;
	}
	else if($get_permission['community_project'] == "public" && !isset($_SESSION['login_email']))
	{
		echo "<div style='height:200px;'><h4 style='text-align:center;'>This Page Is Only For Registered Users</h4></div>";
		include_once("displayfooter.php");
		die;
	}
	else if($get_permission['community_project'] == "members" && ($find_member['expiry_date']<$date || $find_member['lifetime']!=1))
	{
		echo "<div style='height:200px;'><h4 style='text-align:center;'>This Page Is Only For Purify Members</h4></div>";
		include_once("displayfooter.php");
		die;
	}
	else if(mysql_num_rows($find_delete_or_not)>0)
	{
		echo "<div style='height:200px;'><h4 style='text-align:center;'>This Project Has Been Deleted By It's Creator.</h4></div>";
		include_once("displayfooter.php");
		die;
	}
}

if($artist_event_check!=0)
{
	if($get_permission['artist_event'] == "private" && $_SESSION['login_email'] != $getgeneral['email'])
	{
		echo "<div style='height:200px;'><h4 style='text-align:center;'>This Page Is Only For Private Users</h4></div>";
		include_once("displayfooter.php");
		die;
	}
	else if($get_permission['artist_event'] == "public" && !isset($_SESSION['login_email']))
	{
		echo "<div style='height:200px;'><h4 style='text-align:center;'>This Page Is Only For Registered Users</h4></div>";
		include_once("displayfooter.php");
		die;
	}
	else if($get_permission['artist_event'] == "members" && ($find_member['expiry_date']<$date || $find_member['lifetime']!=1))
	{
		echo "<div style='height:200px;'><h4 style='text-align:center;'>This Page Is Only For Purify Members</h4></div>";
		include_once("displayfooter.php");
		die;
	}
	else if(mysql_num_rows($find_delete_or_not)>0)
	{
		echo "<div style='height:200px;'><h4 style='text-align:center;'>This Event Has Been Deleted By It's Creator.</h4></div>";
		include_once("displayfooter.php");
		die;
	}
}

if($artist_project_check!=0)
{
	if($get_permission['artist_project'] == "private" && $_SESSION['login_email'] != $getgeneral['email'])
	{
		echo "<div style='height:200px;'><h4 style='text-align:center;'>This Page Is Only For Private Users</h4></div>";
		include_once("displayfooter.php");
		die;
	}
	else if($get_permission['artist_project'] == "public" && !isset($_SESSION['login_email']))
	{
		echo "<div style='height:200px;'><h4 style='text-align:center;'>This Page Is Only For Registered Users</h4></div>";
		include_once("displayfooter.php");
		die;
	}
	else if($get_permission['artist_project'] == "members" && ($find_member['expiry_date']<$date || $find_member['lifetime']!=1))
	{
		echo "<div style='height:200px;'><h4 style='text-align:center;'>This Page Is Only For Purify Members</h4></div>";
		include_once("displayfooter.php");
		die;
	}
	else if(mysql_num_rows($find_delete_or_not)>0)
	{
		echo "<div style='height:200px;'><h4 style='text-align:center;'>This Project Has Been Deleted By It's Creator.</h4></div>";
		include_once("displayfooter.php");
		die;
	}
}



if($artist_check!=0)
{
	if($get_user_delete_or_not['delete_status'] ==1)
	{
		echo "<div style='height:200px;'><h4 style='text-align:center;'>This User Has Been Deleted By It Self.</h4></div>";
		include_once("displayfooter.php");
		die;
	}
	if($getuser['active']==1)
	{
		echo "<div style='height:200px;'><h4 style='text-align:center;'>This Profile Currently Deactivated By Admin.</h4></div>";
		include_once("displayfooter.php");
		die;
	}
}

if($community_check!=0)
{
	if($get_user_delete_or_not['delete_status'] ==1)
	{
		echo "<div style='height:200px;'><h4 style='text-align:center;'>This User Has Been Deleted By It Self.</h4></div>";
		include_once("displayfooter.php");
		die;
	}
	if($getuser['active']==1)
	{
		echo "<div style='height:200px;'><h4 style='text-align:center;'>This Profile Currently Deactivated By Admin.</h4></div>";
		include_once("displayfooter.php");
		die;
	}
}
if($community_event_check!=0)
{
	if($get_user_delete_or_not['delete_status'] ==1)
	{
		echo "<div style='height:200px;'><h4 style='text-align:center;'>This User Has Been Deleted By It Self.</h4></div>";
		include_once("displayfooter.php");
		die;
	}
	if($getuser['active'] ==1)
	{
		echo "<div style='height:200px;'><h4 style='text-align:center;'>This Profile Currently Deactivated By Admin.</h4></div>";
		include_once("displayfooter.php");
		die;
	}
}

if($community_project_check!=0)
{
	if($get_user_delete_or_not['delete_status'] ==1)
	{
		echo "<div style='height:200px;'><h4 style='text-align:center;'>This User Has Been Deleted By It Self.</h4></div>";
		include_once("displayfooter.php");
		die;
	}
	if($getuser['active'] ==1)
	{
		echo "<div style='height:200px;'><h4 style='text-align:center;'>This Profile Currently Deactivated By Admin.</h4></div>";
		include_once("displayfooter.php");
		die;
	}
}

if($artist_event_check!=0)
{
	if($get_user_delete_or_not['delete_status'] ==1)
	{
		echo "<div style='height:200px;'><h4 style='text-align:center;'>This User Has Been Deleted By It Self.</h4></div>";
		include_once("displayfooter.php");
		die;
	}
	if($getuser['active'] ==1)
	{
		echo "<div style='height:200px;'><h4 style='text-align:center;'>This Profile Currently Deactivated By Admin.</h4></div>";
		include_once("displayfooter.php");
		die;
	}
}

if($artist_project_check!=0)
{
	if($get_user_delete_or_not['delete_status'] ==1)
	{
		echo "<div style='height:200px;'><h4 style='text-align:center;'>This User Has Been Deleted By It Self.</h4></div>";
		include_once("displayfooter.php");
		die;
	}
	if($getuser['active'] ==1)
	{
		echo "<div style='height:200px;'><h4 style='text-align:center;'>This Profile Currently Deactivated By Admin.</h4></div>";
		include_once("displayfooter.php");
		die;
	}
}
?>

<!--<link rel="stylesheet" type="text/css" media="screen" href="../includes/purify.css" />
<script src="../js/jquery.min.js"></script>-->
<link href="../galleryfiles/gallery.css" rel="stylesheet"/>
<script src='http://ajax.googleapis.com/ajax/libs/jquery/1.6.1/jquery.min.js'></script>
<script src="../includes/jquery.js"></script>

<script src="../includes/functionsmedia.js" type="text/javascript"></script>
<script src="../includes/functionsURL.js" type="text/javascript"></script>
<script src="../includes/mediaelement-and-player.min.js"></script>
<link rel="stylesheet" href="../includes/mediaelementplayer.min.css" />

<!--<script type="text/javascript" src="./fancybox/jquery.fancybox-1.3.4.pack.js"></script>
<link rel="stylesheet" type="text/css" href="./fancybox/jquery.fancybox-1.3.4.css" media="screen" />-->

<script>
	var logedinFlag='<?php echo $logedinFlag;?>';
</script>
<script type="text/javascript">
	function shw_messages()
	{
		/* if(logedinFlag =="")
		{
			$("#contactArea").html("Messaging is for Registered Users Only.");
			centerPopup();	loadPopup(); return;
		}
		else
		{ */
			document.getElementById("message").click();
		//}
	}
</script>
<script type="text/javascript">
	//!window.jQuery && document.write('<script src="./fancybox/jquery-1.4.3.min.js"><\/script>');

	$(document).ready(function() {
		$("#fan_club").fancybox({
		'autoScale'			: true,
		'transitionIn'		: 'none',
		'transitionOut'		: 'none',
		'type'				: 'iframe'
		});
	});
	
	/* $(document).ready(function() {
		$("#more_link").fancybox({
		'width'				: '52%',
		'height'			: '75%',
		'transitionIn'		: 'none',
		'transitionOut'		: 'none',
		'type'				: 'iframe'
		});
	}); */
	
	function fancy_login()
	{
		if(logedinFlag!="")
		{
			$("#fan_club_pro").click();
		}
		else if(logedinFlag=="")	
		{
			var url_sec = document.getElementById("logged_chk_user");
			url_sec.href = "handle_login.php?fan&title="+encodeURIComponent(document.title);
			$("#logged_chk_user").click();
		}
	}
	
	function fancy_login_single(url)
	{
		if(logedinFlag!="")
		{
			$("#fan_club_pro"+url).click();
		}
		else if(logedinFlag=="")	
		{
			var url_sec = document.getElementById("logged_chk_user");
			url_sec.href = "handle_login.php?fan&datas="+url;
			$("#logged_chk_user").click();
		}
	}
	
	function click_fancy_login_down_single(url)
	{
		$("#fan_club_pro"+url).click();
	}
	
	function click_fancy_login_down()
	{
		$("#fan_club_pro").click();
	}
		
	$(document).ready(function() {
		$("#fan_club_pro").fancybox({
			helpers : {
				media : {},
				buttons : {},
				title : null
			}
		});
	});
	
	$(document).ready(function() {
		$("#email_subscription").fancybox({
			helpers : {
				media : {},
				buttons : {},
				title : null
			}
		});
	});
	
	$(document).ready(function() {
		$("#email_subscribed").fancybox({
			helpers : {
				media : {},
				buttons : {},
				title : null
			}
		});
	});
	/*'onComplete' : function(){
            $('#fancybox-content').removeAttr('style').css({ 'height' : $(window).height()-100, 'margin' : '0 auto', 'width' : $(window).width()-550 });
            $('#fancybox-wrap').removeAttr('style').css({ 'height' : $(window).height()-100, 'margin' : '0 auto', 'width' : $(window).width()-550 }).show();
            $.fancybox.resize();
            $.fancybox.center();
        }*/
	
	$(document).ready(function() {
		//$("#message").bind("click", $.fancybox.onUpdate);
		
		$("#message").fancybox({
			helpers : {
				media : {},
				buttons : {},
				title : null,
			}
		});
		
		/*$("#message")
		.attr('rel', 'media-gallery')
		.fancybox({
			openEffect : 'none',
			closeEffect : 'none',
			prevEffect : 'none',
			nextEffect : 'none',
			height:100,
			width:300,
			arrows : false,
			helpers : {
				media : {},
				buttons : {},
				title : null
			}
		});*/
		
		
		/* $("#message").attr('rel', 'media-gallery').fancybox({
			//'width'				: '68%',
			//'height'			: '100%',
			openEffect 		: 'fade',
			closeEffect 	: 'none',
			type			: 'iframe',
			prevEffect 		: 'none',
			nextEffect 		: 'none',
			autoSize    	: false,
			autoScale   	: false,
			fixed			: false,
			//autoWidth   	: false,
			autoDimensions  : false,
			scrolling		: false,
			arrows 			: false,
			helpers 		: { media : {}, buttons : {}, title : null },
			afterLoad		: function(){
								this.width = $('.fancybox-iframe').contents().find('#contents_frames').width() + 30;
								this.height = $('.fancybox-iframe').contents().find('#contents_frames').height() + 32;
							},
			onUpdate		: function(){
								var wseight = $('.fancybox-iframe').contents().find('#contents_frames').width() + 30;
								var wseight_wrap = wseight + 30;
								var hseight = $('.fancybox-iframe').contents().find('#contents_frames').height() + 32;
								//$(".fancybox-inner").css({"width":wides});
								$(".fancybox-inner").css({"width":wseight,"height":hseight});
								$(".fancybox-wrap").css({"width":wseight_wrap});
								$.fancybox.reposition();
							}
		}); */
		
	});
		
	$(document).ready(function() {
		$("#email_subscription_eve").fancybox({
			helpers : {
				media : {},
				buttons : {},
				title : null
			}
		});
	});
	
	$(document).ready(function() {
		$("#email_subscribed_eve").fancybox({
			helpers : {
				media : {},
				buttons : {},
				title : null
			}
		});
	});
	
</script>
<?php
include_once('includes/header.php');
?>
	<link rel="stylesheet" href="../lightbox/prettyPhoto.css" type="text/css" media="screen" title="prettyPhoto main stylesheet" charset="utf-8" />
<script src="../lightbox/jquery.prettyPhoto.js" type="text/javascript" charset="utf-8"></script>
<script src="../javascripts/popup.js" type="text/javascript" charset="utf-8"></script>
<?php
//var_dump($_SESSION);
//die;
if(isset($_SESSION['login_email']))
{
?>
<script>
function confirmdelete(name,femail)
{
	var con_del = confirm("Are You Sure You Want To Become Friend of  "+ name +"?");
	if(con_del == false)
	{
		return false;
	}
	else if(con_del == true) 
	{
		location.href = "add_friends.php?log_id="+ femail +"&friend="+ "<?php echo $match; ?>";
	}
}

function confirmaccept(f_id,g_id,type)
{
	var con_del = confirm("Are You Sure You Want To Confirm Friend Request?");
	if(con_del == false)
	{
		return false;
	}
	else if(con_del == true)
	{
		location.href = "add_friends.php?confirm_friend_id="+ f_id +"&confirm_gen="+ g_id + "&type="+ type;
	}
}

function confirmignore(f_id,g_id,type)
{
	var con_del = confirm("Are You Sure You Want To Ignore Friend Request?");
	if(con_del == false)
	{
		return false;
	}
	else if(con_del == true)
	{
		location.href = "add_friends.php?ignore_friend_id="+ f_id +"&ignore_gen="+ g_id + "&type="+ type;
	}
}
</script>
<?php
}
	if(isset($_SESSION['name']) && isset($_SESSION['rel_type']))
	{
?>
		<script>
		$("document").ready(function(){
			//alert("hii");
			var url = document.getElementById("fan_club_pro");
			url.href = "fan_club.php?success=1";
			url.click();
		});
		</script>
<?php
	}
?>
<!--<body onload="startTab();">-->
<div id="outerContainer">
 <!-- <div id="purifyMasthead" style="border-top:none;">
    <div id="purifyLogo"><a href="index.php"><img src="../images/4.gif" alt="Purify Entertainment" width="302" height="53" border="0" /></a></div>-->
    <!--<div id="miscNav">
      <div id="loggedin">
        <h2><?php //echo ucwords($getgeneral['fname'].' '.$getgeneral['lname']); ?></h2>
        <p><a href="profileedit_artist.php">Edit Profile</a><br />
        <a href="../index.php">Logout</a></p>
      </div>
    </div>-->
  </div>
  <div id="loaders">
	<img src="images/ajax_loader_large.gif" style="height: 35px;
    left: 444px;
    margin-top: 153px;
    position: relative;">
  </div>
  <div id="similar_as_below" style="display:none;">
  <div id="contentContainer" >
    <!-- PROFILE START -->
    <div id="profileHeader">
		<div id="profilePhoto" style="position:relative;">
			<?php
				$org_pros_names = basename($getuser['image_name']);
				if($org_pros_names=="")
				{
			?>
					<img src="https://artjcropprofile.s3.amazonaws.com/Noimage.png" width="450" height="450" />
			<?php			
				}
				else
				{
			?>
					<img src="<?php echo $src.$getuser['image_name'];?>" width="450" height="450" />
			<?php
				}
			?>	
		</div>
      <div id="profileInfo">
        <h2><?php if(isset($gettype['name']) && !empty($gettype['name'])) { echo $gettype['name']; }
				  if(isset($get_subtype['name']) && !empty($get_subtype['name'])) { if(!empty($gettype['name'])) { echo '; '.$get_subtype['name']; } else{ echo $get_subtype['name']; }  }  ?></h2>
		<div id="profileFeatured">
		<?php
		if(isset($_SESSION['login_email']) && ($artist_check !=0 || $community_check !=0))
			{
				$chks_app = 0;
				
				$chk_art_com = $friend_obj_new->general_user_sec($_SESSION['login_id']);
				if($chk_art_com['artist_id']!=0 || $chk_art_com['community_id']!=0)
				{
					if($chk_art_com['artist_id']!=0)
					{
						$sq = mysql_query("SELECT * FROM general_artist WHERE artist_id='".$chk_art_com['artist_id']."'");
						$ans_sq = mysql_fetch_assoc($sq);
						if($ans_sq['status']==0 && $ans_sq['del_status']==0 && $ans_sq['active']==0)
						{
							$chks_app = 1;
						}
					}
					elseif($chk_art_com['community_id']!=0)
					{
						$sq = mysql_query("SELECT * FROM general_community WHERE community_id='".$chk_art_com['community_id']."'");
						$ans_sq = mysql_fetch_assoc($sq);
						if($ans_sq['status']==0 && $ans_sq['del_status']==0 && $ans_sq['active']==0)
						{
							$chks_app = 1;
						}
					}
					if($chks_app==1)
				{
					$present_friend_m = $friend_obj_new->find_friend($match);
					if($present_friend_m=='a')
					{
						$present_friend_aid = $friend_obj_new->found_artist($match);
						$present_friend_ac_id = $friend_obj_new->found_ageneral($present_friend_aid['artist_id']);
					}
					elseif($present_friend_m=='c')
					{
						$present_friend_cid = $friend_obj_new->found_community($match);
						$present_friend_ac_id = $friend_obj_new->found_cgeneral($present_friend_cid['community_id']);
					}
				
					//if($artist_check !=0)
					//{
						$present_friend = $friend_obj_new->friend_apresent($_SESSION['login_id'],$present_friend_ac_id['general_user_id']);
						//var_dump($present_friend);
						if($present_friend_ac_id['general_user_id']!=$_SESSION['login_id'])
						{
							if($present_friend=='not')
							{
								$present_ac_i_friend = $friend_obj_new->confirm_ignore_a($_SESSION['login_id'],$present_friend_ac_id['general_user_id']);
								if($present_ac_i_friend=='confirm')
								{
			?>
									<input id="add_friends" onClick="return confirmaccept('<?php echo $_SESSION['login_id']; ?>','<?php echo $present_friend_ac_id['general_user_id']; ?>','artist')" type="submit" value="Confirm" style="background-color: #000000;border: 0 none;color: #FFFFFF;float: left;font-family: futura-pt,sans-serif;font-size: 0.825em;font-weight: bold;margin-right: 5px;padding: 8px 15px;text-decoration: none; border-radius:5px; -webkit-border-radius:5px; -moz-border-radius:5px; -ms-border-radius:5px; -o-border-radius:5px; " />
									
									<input id="add_friends" onClick="return confirmignore('<?php echo $_SESSION['login_id']; ?>','<?php echo $present_friend_ac_id['general_user_id']; ?>','artist')" type="submit" value="Ignore" style="background-color: #000000;border: 0 none;color: #FFFFFF;float: left;font-family: futura-pt,sans-serif;font-size: 0.825em;font-weight: bold;margin-right: 5px;padding: 8px 15px;text-decoration: none; border-radius:5px; -webkit-border-radius:5px; -moz-border-radius:5px; -ms-border-radius:5px; -o-border-radius:5px; " />
			<?php
								}
								else
								{
			?>
									<input id="add_friends" onClick="return confirmdelete('<?php if($getuser['name']!="") { echo mysql_real_escape_string($getuser['name']); }?>','<?php echo $_SESSION['login_id']; ?>')" type="submit" value="Add Friend" style="background-color: #000000;border: 0 none;color: #FFFFFF;float: left;font-family: futura-pt,sans-serif;font-size: 0.825em;font-weight: bold;margin-right: 5px;padding: 8px 15px;text-decoration: none; border-radius:5px; -webkit-border-radius:5px; -moz-border-radius:5px; -ms-border-radius:5px; -o-border-radius:5px; " />
			<?php
								}
							}
							elseif($present_friend=='pending_present')
							{
			?>
								<input id="add_friends" type="submit" value="Friend Request Sent" style="background-color: #000000;border: 0 none;color: #FFFFFF;float: left;font-family: futura-pt,sans-serif;font-size: 0.825em;font-weight: bold;margin-right: 5px;padding: 8px 15px;text-decoration: none; border-radius:5px; -webkit-border-radius:5px; -moz-border-radius:5px; -ms-border-radius:5px; -o-border-radius:5px; " />
			<?php
							}
							elseif($present_friend=='accept_present')
							{
			?>
								<input id="add_friends" type="submit" value="Friends" style="background-color: #000000;border: 0 none;color: #FFFFFF;float: left;font-family: futura-pt,sans-serif;font-size: 0.825em;font-weight: bold;margin-right: 5px;padding: 8px 15px;text-decoration: none; border-radius:5px; -webkit-border-radius:5px; -moz-border-radius:5px; -ms-border-radius:5px; -o-border-radius:5px; " />
			<?php
							}
						}
					/*}
					elseif($community_check !=0)
					{
						$present_friend = $friend_obj_new->friend_cpresent($_SESSION['login_id'],$present_friend_c_id['general_user_id'],$present_friend_cid['community_id']);
						if($present_friend_c_id['general_user_id']!=$_SESSION['login_id'])
						{
							if($present_friend=='not')
							{
								$present_cc_i_friend = $friend_obj_new->confirm_ignore_c($_SESSION['login_id'],$present_friend_a_id['general_user_id']);
								if($present_cc_i_friend=='confirm')
								{
			?>
									<input id="add_friends" onClick="return confirmaccept('<?php echo $_SESSION['login_id']; ?>','<?php echo $present_friend_a_id['general_user_id']; ?>','community')" type="submit" value="Confirm" style="background-color: #000000;border: 0 none;color: #FFFFFF;float: left;font-family: futura-pt,sans-serif;font-size: 0.825em;font-weight: bold;margin-right: 5px;padding: 8px 15px;text-decoration: none;" />
									<input id="add_friends" onClick="return confirmignore('<?php echo $_SESSION['login_id']; ?>','<?php echo $present_friend_a_id['general_user_id']; ?>','community')" type="submit" value="Ignore" style="background-color: #000000;border: 0 none;color: #FFFFFF;float: left;font-family: futura-pt,sans-serif;font-size: 0.825em;font-weight: bold;margin-right: 5px;padding: 8px 15px;text-decoration: none;" />
			<?php
								}
								else
								{
			?>
									<input id="add_friends" onClick="return confirmdelete('<?php if($getuser['name']!="") { echo $getuser['name']; }?>','<?php echo $_SESSION['login_id']; ?>')" type="submit" value="Add Friend" style="background-color: #000000;border: 0 none;color: #FFFFFF;float: left;font-family: futura-pt,sans-serif;font-size: 0.825em;font-weight: bold;margin-right: 5px;padding: 8px 15px;text-decoration: none;" />
			<?php
								}
							}
							elseif($present_friend=='pending_present')
							{
			?>
								<input id="add_friends" type="submit" value="Friend Request Sent" style="background-color: #000000;border: 0 none;color: #FFFFFF;float: left;font-family: futura-pt,sans-serif;font-size: 0.825em;font-weight: bold;margin-right: 5px;padding: 8px 15px;text-decoration: none;" />
			<?php
							}
							elseif($present_friend=='accept_present')
							{
			?>
								<input id="add_friends" type="submit" value="Friends" style="background-color: #000000;border: 0 none;color: #FFFFFF;float: left;font-family: futura-pt,sans-serif;font-size: 0.825em;font-weight: bold;margin-right: 5px;padding: 8px 15px;text-decoration: none;" />
			<?php
							}
						}
					}*/
				}
				}
			}
			?>
			</div>
        <!--<div id="profileFeatured">
			<div class="fmedia">
				featured Media
			</div>
			<?php //echo $getuser['featured_media'].$getuser['media_id'];
			
			if($artist_check !=0 || $artist_event_check !=0 || $artist_project_check !=0  )
			{
				$table_type = "artist";
			}
			if($community_check !=0 || $community_event_check !=0 || $community_project_check !=0  )
			{
				$table_type = "community";
			}
			if($getuser['featured_media'] == "Gallery" && $getuser['media_id']!=0)
			{
				$find_whether_regis=$new_profile_class_obj->get_count_Gallery_at_register($getuser["$table_type".'_id'],$getuser['media_id']);
				//var_dump($find_whether_regis);
				//if($find_whether_regis["count(*)"]>1)
				if($getuser['media_id']!=0 && ($getuser['featured_media_table']=='general_community_gallery_list' || $getuser['featured_media_table']=='general_artist_gallery_list'))
				{
					$getimages=$new_profile_class_obj->get_featured_media_gallery_at_register($getuser['media_id'],$table_type);
					$feature_image_name = array();
					$feature_image_title = array();
					$feature_count = 0;
					while($get_image = mysql_fetch_assoc($getimages))
					{
						$feature_image_name[$feature_count] = $get_image['image_name'];
						$feature_image_title[$feature_count] = $get_image['image_title'];
						$feature_count++;
					}
					$feature_big_img = $feature_image_name[0];
					$featured_gal_img = implode(",", $feature_image_name);
					$featured_gal_img_title = implode(",", $feature_image_title);
					$feature_org_path = "https://reggallery.s3.amazonaws.com/";
					$feature_thum_path = "https://reggalthumb.s3.amazonaws.com/";
				}
				elseif($getuser['media_id']!=0 && ($getuser['featured_media_table']=='general_media' || $getuser['featured_media_table']=='general_media'))
				{
					$getimages=$new_profile_class_obj->get_featured_media_gallery($getuser['media_id']);
					$feature_image_name = array();
					$feature_image_title = array();
					$feature_count = 0;
					while($get_image = mysql_fetch_assoc($getimages))
					{
						$feature_image_name[$feature_count] = $get_image['image_name'];
						$feature_image_title[$feature_count] = $get_image['image_title'];
						$feature_count++;
					}
					$feature_big_img = $feature_image_name[0];
					$featured_gal_img = implode(",", $feature_image_name);
					$featured_gal_img_title = implode(",", $feature_image_title);
					$feature_org_path = "https://medgallery.s3.amazonaws.com/";
					$feature_thum_path = "https://medgalthumb.s3.amazonaws.com/";
				}
			?>
				<a href="javascript:void(0);"><img src="../images/profile/play.png" width="35" height="23" onmouseover="this.src='../images/profile/play-over.png'" onmouseout="this.src='../images/profile/play.png'" onclick="checkload('<?php echo $feature_big_img;?>','<?php echo $featured_gal_img; ?>','<?php echo $feature_thum_path; ?>','<?php echo $feature_org_path; ?>','<?php echo $featured_gal_img_title; ?>')" /></a>
			<?php
			}
			if($getuser['featured_media'] == "Song" && $getuser['media_id']!=0)
			{
			?>
				<a href="javascript:void(0);"><img src="../images/profile/play.png" width="35" height="23" onmouseover="this.src='../images/profile/play-over.png'" onmouseout="this.src='../images/profile/play.png'" onclick="showPlayer('a','<?php echo $getuser['media_id'];?>','play')" /></a>
			<?php
			}
			
			if($getuser['featured_media'] == "Video" && $getuser['media_id']!=0)
			{
			?>
				<a href="javascript:void(0);"><img src="../images/profile/play.png" width="35" height="23" onmouseover="this.src='../images/profile/play-over.png'" onmouseout="this.src='../images/profile/play.png'" onclick="showPlayer('v','<?php echo $getuser['media_id'];?>','playlist')" /></a>
			<?php
			}
			?>
			<a href=""><img src="../images/profile/add.png" width="27" height="23" onmouseover="this.src='../images/profile/add-over.png'" onmouseout="this.src='../images/profile/add.png'" /></a>
		</div>-->
        
        <h1 style="margin: 0 0 3px;"><?php if($getuser['name']!="") { echo $getuser['name']; } else { echo $getuser['title']; }?></h1>

        <h3 style="margin: 0 0 6px;"><?php if($artist_check!=0 || $community_check!=0) { 
		
		if((isset($getuser['city']) &&  !empty($getuser['city'])) || (isset($getstate['state_name']) && !empty($getstate['state_name'])) || (isset($getcountry['country_name']) && !empty($getcountry['country_name'])))
			{ ?><!-- Where: --> <?php
		if(isset($getuser['city']) && !empty($getuser['city']))
		{
			echo $getuser['city'];
		}
		if(isset($getstate['state_name']) && !empty($getstate['state_name']))
		{
			if(!empty($getuser['city']))
			{
				echo ', '.$getstate['state_name'];
			}
			else
			{
				echo $getstate['state_name'];
			}
		}
		if(isset($getcountry['country_name']) && !empty($getcountry['country_name']))
		{
			if(!empty($getuser['city']) || !empty($getstate['state_name']))
			{
				echo ', '.$getcountry['country_name'];
			}
			else
			{
				echo $getcountry['country_name'];
			}
		}?><br/>
		<?php
		}
		
			
		}
		if($artist_event_check!=0 || $community_event_check!=0)
		{
			if($getuser['venue_name']!="" && !empty($getuser['venue_name']))
			{
				if($artist_event_check!=0)
				{
					$types = 'artist';
					$sql_gens = $display->general_acusers($types,$artist_event_check['artist_id']);
				}
				if($community_event_check!=0)
				{
					$types = 'community';
					$sql_gens = $display->general_acusers($types,$community_event_check['community_id']);
				}
				
				//if(isset($sql_gens))
				//{
					//if(!empty($sql_gens))
					//{
						/*if($sql_gens['profile_url']!="")
						{
		?>
							Venue: <?php echo '<a href="'.$sql_gens['profile_url'].'">'.$getuser['venue_name'].'</a>';
						}*/
						if($getuser['state_id']!="" && $getuser['state_id']!=0)
						{
							$sql_state = mysql_query("SELECT * FROM state WHERE state_id='".$getuser['state_id']."' AND status=1");
							if($sql_state!="" && $sql_state!=Null)
							{
								if(mysql_num_rows($sql_state)>0)
								{
									$ans_state = mysql_fetch_assoc($sql_state);
								}
							}
						}
						if($getuser['venue_website']!="" && $getuser['venue_website']!="http://" && $getuser['venue_website']!="https://")
						{
							$end_p = $getuser['venue_name'];
							
							if($getuser['city']!="" && !empty($getuser['city']))
							{
								$end_p .= ', '.$getuser['city'];
							}
							
							if(isset($ans_state))
							{
								$end_p .= ', '.$ans_state['state_name'];
							}
		?>
							Venue: <?php echo '<a target="_blank" href="'.$getuser['venue_website'].'">'.$end_p.'</a>';
						}
						else
						{
							$end_p = $getuser['venue_name'];
							
							if($getuser['city']!="" && !empty($getuser['city']))
							{
								$end_p .= ', '.$getuser['city'];
							}
							
							if(isset($ans_state))
							{
								$end_p .= ', '.$ans_state['state_name'];
							}
		?>
							Venue: <?php echo $end_p;
						}
					//}
				//}
				/*else
				//{
		?>
					Venue: <?php echo $getuser['venue_name'];
				}*/
			} 
			if($getuser['date']!="" && !empty($getuser['date']))
			{ ?><br/>
			When: <?php if($getuser['start_time']!="" && $getuser['end_time']!="") { echo $getuser['start_time'].$getuser['start_timeap'].' to '.$getuser['end_time'].$getuser['end_timeap']; } $newDate = date("M d, Y", strtotime($getuser['date'])); echo ' on '.$newDate;  } ?><br/>
		<?php
		}
		if($artist_project_check!=0 || $community_project_check!=0)
		{
			/* if(!empty($getuser['creators_info']) && $getuser['creators_info']!="")
			{
				$exp_cpr = explode('|',$getuser['creators_info']);
				if($exp_cpr[0]=='general_artist')
				{
					$sql_cp_ar = mysql_query("SELECT * FROM general_artist WHERE artist_id='".$exp_cpr[1]."'");
				}
				if($exp_cpr[0]=='general_community')
				{
					$sql_cp_ar = mysql_query("SELECT * FROM general_community WHERE community_id='".$exp_cpr[1]."'");
				}
				if($exp_cpr[0]=='artist_project' || $exp_cpr[0]=='community_project' || $exp_cpr[0]=='community_event' || $exp_cpr[0]=='artist_event')
				{
					$sql_cp_ar = mysql_query("SELECT * FROM $exp_cpr[0] WHERE id='".$exp_cpr[1]."'");
				}
				
				if(isset($sql_cp_ar))
				{
					if(mysql_num_rows($sql_cp_ar)>0)
					{
						$ans_cp_ar = mysql_fetch_assoc($sql_cp_ar);
						if($ans_cp_ar['profile_url']!="" && !empty($ans_cp_ar['profile_url']))
						{
							?> Creator: <a href="<?php echo $ans_cp_ar['profile_url']; ?>"><?php echo $getuser['creator']; ?></a><br/> <?php
						}
						elseif($getuser['creator']!="" && !empty($getuser['creator']))
						{
							?> Creator: <?php echo $getuser['creator']; ?><br/> <?php
						}
					}
					elseif($getuser['creator']!="" && !empty($getuser['creator']))
					{
						?> Creator: <?php echo $getuser['creator']; ?><br/> <?php
					}
				}
				elseif($getuser['creator']!="" && !empty($getuser['creator']))
				{
					?> Creator: <?php echo $getuser['creator']; ?><br/> <?php
				}
			}
			elseif($getuser['creator']!="" && !empty($getuser['creator']))
			{
				?> Creator: <?php echo $getuser['creator']; ?><br/> <?php
			} */
		}
		
       if(isset($get_metatype) && $get_metatype!=null) 
		{
		?>
			<!-- Type: -->
	    <?php
			if(isset($get_metatype[0]['name']) && !empty($get_metatype[0]['name'])) { echo $get_metatype[0]['name']; }
			if(isset($get_metatype[1]['name']) && !empty($get_metatype[1]['name'])) { if(!empty($get_metatype[0]['name'])) { echo ', '.$get_metatype[1]['name']; } else { echo $get_metatype[1]['name']; } }
			if(isset($get_metatype[2]['name']) && !empty($get_metatype[2]['name'])) {  if(!empty($get_metatype[0]['name']) || !empty($get_metatype[1]['name'])) { echo ', '.$get_metatype[2]['name']; } else { echo $get_metatype[2]['name']; } }
		?> <br/>
		<?php
		} 
		if($artist_project_check!=0 || $community_project_check!=0)
		{
			if((isset($getuser['city']) &&  !empty($getuser['city'])) || (isset($getstate['state_name']) && !empty($getstate['state_name'])) || (isset($getcountry['country_name']) && !empty($getcountry['country_name'])))
			{
		?><!-- Where: --> <?php
		if(isset($getuser['city']) &&  !empty($getuser['city']))
		{
			echo $getuser['city'];
		}
		if(isset($getstate['state_name']) && !empty($getstate['state_name']))
		{
			if(!empty($getuser['city']))
			{
				echo ', '.$getstate['state_name'];
			}
			else
			{
				echo $getstate['state_name'];
			}
		}
		if(isset($getcountry['country_name']) && !empty($getcountry['country_name']))
		{
			if(!empty($getuser['city']) || !empty($getstate['state_name']))
			{
				echo ', '.$getcountry['country_name'];
			}
			else
			{
				echo $getcountry['country_name'];
			}
		}
		?><br/>
		<?php
		}
		}
		?>
		</h3>
		<?php
		if($artist_check !=0 || $community_check !=0)
			{
				$data = explode("/",$_SERVER['REQUEST_URI']); //echo $data[1]; 
				if($find_member != null && $find_member['general_user_id'] != null && ($find_member['expiry_date'] >$date || $find_member['lifetime']==1))
				{
					if($getuser['type']=="fan_club_yes" || $getuser['type']=="for_sale") {
							$bio_change_range = 1;
					?>
						<div class="sign_bio_up_buts" >
							<a style="display:none;" href="fan_club.php?data=<?php echo $data[1];?>" id="fan_club_pro" class="fancybox fancybox.ajax">
							<?php if($getuser['type']=="fan_club_yes") { echo "Fan Club"; } elseif($getuser['type']=="for_sale") { echo "Download"; } ?></a>
							<a style="font-size: 21px; padding: 8px 15px; background: none repeat scroll 0 0 #333333; border: 1px solid #8A8A8A;" onmouseover="this.style.background='none repeat scroll 0 0 #000000';" onmouseout="this.style.background='none repeat scroll 0 0 #333333';" title="<?php if($getuser['type']=="fan_club_yes") { echo "Purchase a fan club membership to this users exclusive media."; } elseif($getuser['type']=="for_sale") { echo "Purchase this projects media."; } ?>" href="javascript:void(0);" onClick="fancy_login()" ><?php if($getuser['type']=="fan_club_yes") { echo "Fan Club"; } elseif($getuser['type']=="for_sale") { echo "Download"; } ?></a>
						</div>
					<?php 
					}	
				}
			}
			elseif($community_project_check!=0 || $artist_project_check!=0)
			{
				$data = explode("/",$_SERVER['REQUEST_URI']);
				if($find_member != null && $find_member['general_user_id'] != null && ($find_member['expiry_date'] >$date || $find_member['lifetime']==1))
				{
					if($getuser['type']=="for_sale") {
					include_once("findproject_sale.php");
					if(isset($artist_project_check) && $artist_project_check!=0){
						$match_id = $artist_project_check['id']."~art";
					}
					if(isset($community_project_check) && $community_project_check!=0){
						$match_id = $community_project_check['id']."~com";
					}
					$stats = 0;
					if($_SESSION['login_email']!=NULL)
					{
						if(isset($artist_project_check) && $artist_project_check!=0)
						{						
							$stats = $display->chkdownloaded_pro($artist_project_check['id'],$_SESSION['login_id'],'artist_project');
						}
						if(isset($community_project_check) && $community_project_check!=0)
						{
							$stats = $display->chkdownloaded_pro($community_project_check['id'],$_SESSION['login_id'],'community_project');
						}
					}
					else
					{
						if($getuser['type']=="for_sale" || $getuser['type']=="free_club")
						{
							if(isset($artist_project_check) && $artist_project_check!=0)
							{						
								$stats = $display->chkdownloaded_pro_not_log($artist_project_check['id'],'artist_project');
							}
							if(isset($community_project_check) && $community_project_check!=0)
							{
								$stats = $display->chkdownloaded_pro_not_log($community_project_check['id'],'community_project');
							}
						}
					}
					
					if($stats==1)
					{
						if($getuser['type']!="fan_club")
						{
							$free_down_project_rel = "yes";
						}
					}
					else
					{
						for($pro_sale=0;$pro_sale<=count($final_prosale_arrs);$pro_sale++)
						{
							if($final_prosale_arrs[$pro_sale]['id']==$match_id){
								$free_down_project_rel = "yes";
							}
							else{
								if($free_down_project_rel =="" || $free_down_project_rel =="no"){
									$free_down_project_rel = "no";
								}
							}
						}
					}
					?>
					<a  title="Purchase a fan club membership to this projects exclusive media." href="fan_club.php?data=<?php echo $data[1];?>" id="fan_club_pro" class="fancybox fancybox.ajax" style="display:none;" ><?php if($getuser['type']=="fan_club") { echo "Fan Club"; } elseif($getuser['type']=="for_sale") { echo "Buy Now"; } elseif($getuser['type']=="free_club") { echo "Free Download"; } ?></a>
					<?php
					if($getuser['type']=="for_sale" && $free_down_project_rel=="yes"){
						if($stats==1)
						{
							$bio_change_range = 1;
							if(isset($getuser['artist_id']))
							{
								$type_zip_down = 'art';
							}
							else
							{
								$type_zip_down = 'com';
							}
					?>
							<div class="sign_bio_up_buts" >
								<a style="font-size: 21px; padding: 8px 15px; background: none repeat scroll 0 0 #333333; border: 1px solid #8A8A8A;" onmouseout="this.style.background='none repeat scroll 0 0 #333333';" onmouseover="this.style.background='none repeat scroll 0 0 #000000';" title="Purchase this projects media." href="javascript:void(0);" onClick="download_directly_project('<?php echo $getuser['sale_song'];?>','<?php echo $getuser['title'];?>','<?php echo $getuser['creator'];?>','<?php echo $type_zip_down; ?>','<?php echo $getuser['id']; ?>')">Download</a>
							</div>
					<?php
						}
						else
						{
							$bio_change_range = 1;
							if(isset($getuser['artist_id']))
							{
								$type_zip_down = 'art';
							}
							else
							{
								$type_zip_down = 'com';
							}
					?>
							<div class="sign_bio_up_buts" >
								<a style="font-size: 21px; padding: 8px 15px; background: none repeat scroll 0 0 #333333; border: 1px solid #8A8A8A;" onmouseout="this.style.background='none repeat scroll 0 0 #333333';" onmouseover="this.style.background='none repeat scroll 0 0 #000000';" title="Purchase this projects media." href="javascript:void(0);" onClick="download_directly_project('<?php echo $getuser['sale_song'];?>','<?php echo $getuser['title'];?>','<?php echo $getuser['creator'];?>','<?php echo $type_zip_down; ?>','<?php echo $getuser['id']; ?>')">Download</a>
							</div>
					<?php		
						}
					}else{
							$bio_change_range = 1;
					?>
						<div class="sign_bio_up_buts" >
							<a style="font-size: 21px; padding: 8px 15px; background: none repeat scroll 0 0 #333333; border: 1px solid #8A8A8A;" onmouseout="this.style.background='none repeat scroll 0 0 #333333';" onmouseover="this.style.background='none repeat scroll 0 0 #000000';" title="<?php if($getuser['type']=="fan_club") { echo "Purchase a fan club membership to this users exclusive media."; } elseif($getuser['type']=="for_sale") { echo "Purchase this projects media."; } ?>" href="javascript:void(0);" onClick="<?php if($getuser['type']=="fan_club") { echo "fancy_login()"; } elseif($getuser['type']=="for_sale") { echo "click_fancy_login_down()"; } ?>"><?php if($getuser['type']=="fan_club") { echo "Fan Club"; } elseif($getuser['type']=="for_sale") { echo "Buy Now"; } ?></a>
						</div>
					<?php }
					
					}
					
					if($getuser['type']=="free_club") {
					include_once("findproject_sale.php");
					if(isset($artist_project_check) && $artist_project_check!=0){
						$match_id = $artist_project_check['id']."~art";
					}
					if(isset($community_project_check) && $community_project_check!=0){
						$match_id = $community_project_check['id']."~com";
					}
					$stats = 0;
					if($_SESSION['login_email']!=NULL)
					{
						if(isset($artist_project_check) && $artist_project_check!=0)
						{						
							$stats = $display->chkdownloaded_pro($artist_project_check['id'],$_SESSION['login_id'],'artist_project');
						}
						if(isset($community_project_check) && $community_project_check!=0)
						{
							$stats = $display->chkdownloaded_pro($community_project_check['id'],$_SESSION['login_id'],'community_project');
						}
					}
					else
					{
						if($getuser['type']=="for_sale" || $getuser['type']=="free_club")
						{
							if(isset($artist_project_check) && $artist_project_check!=0)
							{						
								$stats = $display->chkdownloaded_pro_not_log($artist_project_check['id'],'artist_project');
							}
							if(isset($community_project_check) && $community_project_check!=0)
							{
								$stats = $display->chkdownloaded_pro_not_log($community_project_check['id'],'community_project');
							}
						}
					}
					
					if($stats==1)
					{
						if($getuser['type']!="fan_club")
						{
							$free_down_project_rel = "yes";
						}
					}
					else
					{
						for($pro_sale=0;$pro_sale<=count($final_prosale_arrs);$pro_sale++)
						{
							if($final_prosale_arrs[$pro_sale]['id']==$match_id){
								$free_down_project_rel = "yes";
							}
							else{
								if($free_down_project_rel =="" || $free_down_project_rel =="no"){
									$free_down_project_rel = "no";
								}
							}
						}
					}
					?>
					<a  title="Purchase a fan club membership to this projects exclusive media." href="fan_club.php?data=<?php echo $data[1];?>" id="fan_club_pro" class="fancybox fancybox.ajax" style="display:none;" ><?php if($getuser['type']=="fan_club") { echo "Fan Club"; } elseif($getuser['type']=="for_sale") { echo "Buy Now"; } ?></a>
					<?php
					if($free_down_project_rel=="yes"){
						if($stats==1)
						{
							$bio_change_range = 1;
							if(isset($getuser['artist_id']))
							{
								$type_zip_down = 'art';
							}
							else
							{
								$type_zip_down = 'com';
							}
					?>
							<div class="sign_bio_up_buts" >
								<a style="font-size: 21px; padding: 8px 15px; background: none repeat scroll 0 0 #333333; border: 1px solid #8A8A8A;" onmouseout="this.style.background='none repeat scroll 0 0 #333333';" onmouseover="this.style.background='none repeat scroll 0 0 #000000';" title="Purchase this projects media." href="javascript:void(0);" onClick="download_directly_project('<?php echo $getuser['free_club_song'];?>','<?php echo $getuser['title'];?>','<?php echo $getuser['creator'];?>','<?php echo $type_zip_down; ?>','<?php echo $getuser['id']; ?>')">Download</a>
							</div>
					<?php
						}
						else
						{
							$bio_change_range = 1;
							if(isset($getuser['artist_id']))
							{
								$type_zip_down = 'art';
							}
							else
							{
								$type_zip_down = 'com';
							}
					?>
							<div class="sign_bio_up_buts" >
								<a style="font-size: 21px; padding: 8px 15px; background: none repeat scroll 0 0 #333333; border: 1px solid #8A8A8A;" onmouseout="this.style.background='none repeat scroll 0 0 #333333';" onmouseover="this.style.background='none repeat scroll 0 0 #000000';" title="Purchase this projects media." href="javascript:void(0);" onClick="download_directly_project('<?php echo $getuser['free_club_song'];?>','<?php echo $getuser['title'];?>','<?php echo $getuser['creator'];?>','<?php echo $type_zip_down; ?>','<?php echo $getuser['id']; ?>')">Download</a>
							</div>
					<?php		
						}
					}else{
						if(isset($community_project_check) && $community_project_check!=0)
						{
							$tip_add_aftr_detils = $display->get_tip_details_for_every($getuser['id'],'community_project');
						}
						elseif(isset($artist_project_check) && $artist_project_check!=0)
						{
							$tip_add_aftr_detils = $display->get_tip_details_for_every($getuser['id'],'artist_project');
						}
						
						if(isset($getuser['artist_id']))
						{
							$type_zip_down = 'art';
						}
						else
						{
							$type_zip_down = 'com';
						}
						
						$bio_change_range = 1;
					?>
						<div class="sign_bio_up_buts" >
							<a style="font-size: 21px; padding: 8px 15px; background: none repeat scroll 0 0 #333333; border: 1px solid #8A8A8A;" onmouseout="this.style.background='none repeat scroll 0 0 #333333';" onmouseover="this.style.background='none repeat scroll 0 0 #000000';" title="<?php if($getuser['type']=="fan_club") { echo "Purchase a fan club membership to this users exclusive media."; } elseif($getuser['type']=="for_sale") { echo "Purchase this projects media."; } ?>" href="javascript:void(0);" onClick="<?php if($getuser['type']=="fan_club") { echo "fancy_login()"; } elseif($getuser['type']=="for_sale") { echo "click_fancy_login_down()"; } elseif($getuser['type']=="free_club") { if($getuser['ask_tip']=='0') { echo "download_directly_project('".$getuser['free_club_song']."','".$getuser['title']."','".$getuser['creator']."','".$type_zip_down."','".$getuser['id']."')"; } else { echo "ask_to_donate_pro('".mysql_real_escape_string($getuser['title'])."','".mysql_real_escape_string($getuser['creator'])."','". $getuser['free_club_song']."','fan_club_pro','".$tip_add_aftr_detils."','".$type_zip_down."','".$getuser['id']."')"; } } ?>"><?php if($getuser['type']=="fan_club") { echo "Fan Club"; } elseif($getuser['type']=="for_sale") { echo "Buy Now"; } elseif($getuser['type']=="free_club") { echo "Free Download"; } ?></a>
						</div>
					<?php }
					}
				}
				
				if($find_member != null && $find_member['general_user_id'] != null && ($find_member['expiry_date'] >$date || $find_member['lifetime']==1))
				{
					if($getuser['type']=="fan_club") {
					include_once("findproject_sale.php");
					if(isset($artist_project_check) && $artist_project_check!=0){
						$match_id = $artist_project_check['id']."~art";
					}
					if(isset($community_project_check) && $community_project_check!=0){
						$match_id = $community_project_check['id']."~com";
					}
					$stats = 0;
					if($_SESSION['login_email']!=NULL)
					{
						if(isset($artist_project_check) && $artist_project_check!=0)
						{						
							$stats = $display->chkdownloaded_pro($artist_project_check['id'],$_SESSION['login_id'],'artist_project');
						}
						if(isset($community_project_check) && $community_project_check!=0)
						{
							$stats = $display->chkdownloaded_pro($community_project_check['id'],$_SESSION['login_id'],'community_project');
						}
					}
					else
					{
						if($getuser['type']=="for_sale" || $getuser['type']=="free_club")
						{
							if(isset($artist_project_check) && $artist_project_check!=0)
							{						
								$stats = $display->chkdownloaded_pro_not_log($artist_project_check['id'],'artist_project');
							}
							if(isset($community_project_check) && $community_project_check!=0)
							{
								$stats = $display->chkdownloaded_pro_not_log($community_project_check['id'],'community_project');
							}
						}
					}
					
					if($stats==1)
					{
						if($getuser['type']!="fan_club")
						{
							$free_down_project_rel = "yes";
						}
					}
					else
					{
						for($pro_sale=0;$pro_sale<=count($final_prosale_arrs);$pro_sale++)
						{
							if($final_prosale_arrs[$pro_sale]['id']==$match_id){
								$free_down_project_rel = "yes";
							}
							else{
								if($free_down_project_rel =="" || $free_down_project_rel =="no"){
									$free_down_project_rel = "no";
								}
							}
						}
					}
					?>
					<a  title="Purchase a fan club membership to this projects exclusive media." href="fan_club.php?data=<?php echo $data[1];?>" id="fan_club_pro" class="fancybox fancybox.ajax" style="display:none;" ><?php if($getuser['type']=="fan_club") { echo "Fan Club"; } elseif($getuser['type']=="for_sale") { echo "Buy Now"; } elseif($getuser['type']=="free_club") { echo "Free Download"; } ?></a>
					<?php
					if($free_down_project_rel=="yes"){
						if($stats==1)
						{
							$bio_change_range = 1;
							if(isset($getuser['artist_id']))
							{
								$type_zip_down = 'art';
							}
							else
							{
								$type_zip_down = 'com';
							}
					?>
							<div class="sign_bio_up_buts" >
								<a style="font-size: 21px; padding: 8px 15px; background: none repeat scroll 0 0 #333333; border: 1px solid #8A8A8A;" onmouseout="this.style.background='none repeat scroll 0 0 #333333';" onmouseover="this.style.background='none repeat scroll 0 0 #000000';" title="Purchase this projects media." href="javascript:void(0);" onClick="download_directly_project('<?php echo $getuser['fan_song'];?>','<?php echo $getuser['title'];?>','<?php echo $getuser['creator'];?>','<?php echo $type_zip_down; ?>','<?php echo $getuser['id']; ?>')">Download</a>
							</div>
					<?php
						}
						else
						{
							$bio_change_range = 1;
							if(isset($getuser['artist_id']))
							{
								$type_zip_down = 'art';
							}
							else
							{
								$type_zip_down = 'com';
							}
					?>
							<div class="sign_bio_up_buts" >
								<a style="font-size: 21px; padding: 8px 15px; background: none repeat scroll 0 0 #333333; border: 1px solid #8A8A8A;" onmouseout="this.style.background='none repeat scroll 0 0 #333333';" onmouseover="this.style.background='none repeat scroll 0 0 #000000';" title="Purchase this projects media." href="javascript:void(0);" onClick="download_directly_project('<?php echo $getuser['fan_song'];?>','<?php echo $getuser['title'];?>','<?php echo $getuser['creator'];?>','<?php echo $type_zip_down; ?>','<?php echo $getuser['id']; ?>')">Download</a>
							</div>
					<?php		
						}
					}else{
						$bio_change_range = 1;
					?>
						<div class="sign_bio_up_buts" >
							<a style="font-size: 21px; padding: 8px 15px; background: none repeat scroll 0 0 #333333; border: 1px solid #8A8A8A;" onmouseout="this.style.background='none repeat scroll 0 0 #333333';" onmouseover="this.style.background='none repeat scroll 0 0 #000000';" title="<?php if($getuser['type']=="fan_club") { echo "Purchase a fan club membership to this users exclusive media."; } elseif($getuser['type']=="for_sale") { echo "Purchase this projects media."; } ?>" href="javascript:void(0);" onClick="<?php if($getuser['type']=="fan_club") { echo "fancy_login()"; } elseif($getuser['type']=="for_sale") { echo "click_fancy_login_down()"; } ?>"><?php if($getuser['type']=="fan_club") { echo "Fan Club"; } elseif($getuser['type']=="for_sale") { echo "Buy Now"; } elseif($getuser['type']=="free_club") { echo "Free Download"; } ?></a>
						</div>
					<?php }
					}
				}
			}
			//if($artist_project_check==0 && $community_project_check==0)
			//{
			?>
		  <!--Email:--> <?php //echo $getgeneral['email']; } ?>
		  <script>
		  $("document").ready(function(){
			$("#contentContainer p").css({"width":"437px"});
		  });
		  </script>
        <p style="width:463px; font-size:14px;"><?php
			if($bio_change_range==1)
			{
				$range_chars = 430;
				$range_brs = 8;
			}
			else
			{
				$range_chars = 510;
				$range_brs = 9;
			}
			if($artist_check !=0)
			{
				if(isset($get_permission) && !empty($get_permission))
				{
					if($get_permission['artist_biography'] == "open")
					{
						if($getuser['bio']!="") 
						{ 
							$exp_br_out = explode("<br>",$getuser['bio']);
							if(strlen($getuser['bio'])>$range_chars || count($exp_br_out) > $range_brs)
							{
								$exp_br = explode("<br>",$getuser['bio']);
								if(count($exp_br) > $range_brs){
									$all_bio="";
									$array_count = 7;
									for($coun_br=0;$coun_br<$array_count;$coun_br++){
										if(strlen($all_bio)<$range_chars){
											$str_len = strlen($exp_br[$coun_br]);
											$newvar = $str_len/70;
											if($newvar>0){
												$array_count = $array_count - floor($newvar);
											}
											if($array_count<$coun_br){
												//$all_bio = $all_bio . $exp_br[$coun_br];
												if($coun_br==1){
													$all_bio = $all_bio . substr($exp_br[$coun_br],0,$range_chars);
												}else{
													$all_bio = $all_bio . substr($exp_br[$coun_br],0,120);
												}
											}else{
												if($array_count==$coun_br){
													$all_bio = $all_bio . substr($exp_br[$coun_br],0,120);
												}else{
													$all_bio = $all_bio . $exp_br[$coun_br]."<br>";
												}
											}
										}
									}
									echo substr($all_bio,0,$range_chars).' '.'<a href="javascript:void(0);" onClick="open_desc()" style="text-decoration:underline;" id="more_link" class="fancybox fancybox.ajax" name="more_link">More></a>'; 
								}
								else{
									echo substr($getuser['bio'],0,$range_chars).' '.'<a href="javascript:void(0);" onClick="open_desc()" style="text-decoration:underline;" class="fancybox fancybox.ajax" id="more_link" name="more_link">More></a>'; 
								}
							}
							else
							{
								echo $getuser['bio']; 
							}
						}
						elseif($getuser['description']!="")
						{
							$exp_br_out = explode("<br>",$getuser['bio']);
							if(strlen($getuser['description'])>$range_chars || count($exp_br_out) > $range_brs)
							{
								$exp_br = explode("<br>",$getuser['description']);
								if(count($exp_br) > $range_brs){
									$all_bio="";
									$array_count = 7;
									for($coun_br=0;$coun_br<$array_count;$coun_br++){
										if(strlen($all_bio)<$range_chars){
											$str_len = strlen($exp_br[$coun_br]);
											$newvar = $str_len/70;
											if($newvar>0){
												$array_count = $array_count - floor($newvar);
											}
											if($array_count<$coun_br){
												//$all_bio = $all_bio . $exp_br[$coun_br];
												if($coun_br==1){
													$all_bio = $all_bio . substr($exp_br[$coun_br],0,$range_chars);
												}else{
													$all_bio = $all_bio . substr($exp_br[$coun_br],0,120);
												}
											}else{
												if($array_count==$coun_br){
													$all_bio = $all_bio . substr($exp_br[$coun_br],0,120);
												}else{
													$all_bio = $all_bio . $exp_br[$coun_br]."<br>";
												}
											}
										}
									}
									echo substr($all_bio,0,$range_chars).' '.'<a href="javascript:void(0);" onClick="open_desc()" style="text-decoration:underline;" id="more_link" class="fancybox fancybox.ajax" name="more_link">More></a>'; 
								}else{
									echo substr($getuser['description'],0,$range_chars).' '.'<a href="javascript:void(0);" onClick="open_desc()" style="text-decoration:underline;" class="fancybox fancybox.ajax" id="more_link" name="more_link">More></a>';
								}
							}
							else
							{
								echo $getuser['description'];
							}
						}
					}
					else if($get_permission['artist_biography'] == "private" && $_SESSION['login_email'] == $getgeneral['email'])
					{
						if($getuser['bio']!="") 
						{ 
							$exp_br_out = explode("<br>",$getuser['bio']);
							if(strlen($getuser['bio'])>$range_chars || count($exp_br_out) > $range_brs)
							{
								$exp_br = explode("<br>",$getuser['bio']);
								if(count($exp_br) > $range_brs){
									$all_bio="";
									$array_count = 7;
									for($coun_br=0;$coun_br<$array_count;$coun_br++){
										if(strlen($all_bio)<$range_chars){
											$str_len = strlen($exp_br[$coun_br]);
											$newvar = $str_len/70;
											if($newvar>0){
												$array_count = $array_count - floor($newvar);
											}
											if($array_count<$coun_br){
												//$all_bio = $all_bio . $exp_br[$coun_br];
												if($coun_br==1){
													$all_bio = $all_bio . substr($exp_br[$coun_br],0,$range_chars);
												}else{
													$all_bio = $all_bio . substr($exp_br[$coun_br],0,120);
												}
											}else{
												if($array_count==$coun_br){
													$all_bio = $all_bio . substr($exp_br[$coun_br],0,120);
												}else{
													$all_bio = $all_bio . $exp_br[$coun_br]."<br>";
												}
											}
										}
									}
									echo substr($all_bio,0,$range_chars).' '.'<a href="javascript:void(0);" onClick="open_desc()" style="text-decoration:underline;" id="more_link" class="fancybox fancybox.ajax" name="more_link">More></a>'; 
								}else{
									echo substr($getuser['bio'],0,$range_chars).' '.'<a href="javascript:void(0);" onClick="open_desc()" style="text-decoration:underline;" class="fancybox fancybox.ajax" id="more_link" name="more_link">More></a>';
								}
							}
							else
							{
								echo $getuser['bio']; 
							}
						}
						elseif($getuser['description']!="")
						{
							$exp_br_out = explode("<br>",$getuser['bio']);
							if(strlen($getuser['description'])>$range_chars || count($exp_br_out) > $range_brs)
							{
								$exp_br = explode("<br>",$getuser['description']);
								if(count($exp_br) > $range_brs){
									$all_bio="";
									$array_count = 7;
									for($coun_br=0;$coun_br<$array_count;$coun_br++){
										if(strlen($all_bio)<$range_chars){
											$str_len = strlen($exp_br[$coun_br]);
											$newvar = $str_len/70;
											if($newvar>0){
												$array_count = $array_count - floor($newvar);
											}
											if($array_count<$coun_br){
												//$all_bio = $all_bio . $exp_br[$coun_br];
												if($coun_br==1){
													$all_bio = $all_bio . substr($exp_br[$coun_br],0,$range_chars);
												}else{
													$all_bio = $all_bio . substr($exp_br[$coun_br],0,120);
												}
											}else{
												if($array_count==$coun_br){
													$all_bio = $all_bio . substr($exp_br[$coun_br],0,120);
												}else{
													$all_bio = $all_bio . $exp_br[$coun_br]."<br>";
												}
											}
										}
									}
									echo substr($all_bio,0,$range_chars).' '.'<a href="javascript:void(0);" onClick="open_desc()" style="text-decoration:underline;" id="more_link" class="fancybox fancybox.ajax" name="more_link">More></a>'; 
								}else{
									echo substr($getuser['description'],0,$range_chars).' '.'<a href="javascript:void(0);" onClick="open_desc()" style="text-decoration:underline;" class="fancybox fancybox.ajax" id="more_link" name="more_link">More></a>';
								}
							}
							else
							{
								echo $getuser['description'];
							}
						}
					}
					else if($get_permission['artist_biography'] == "public" && isset($_SESSION['login_email']))
					{
						if($getuser['bio']!="") 
						{ 
							$exp_br_out = explode("<br>",$getuser['bio']);
							if(strlen($getuser['bio'])>$range_chars || count($exp_br_out) > $range_brs)
							{
								$exp_br = explode("<br>",$getuser['bio']);
								if(count($exp_br) > $range_brs){
									$all_bio="";
									$array_count = 7;
									for($coun_br=0;$coun_br<$array_count;$coun_br++){
										if(strlen($all_bio)<$range_chars){
											$str_len = strlen($exp_br[$coun_br]);
											$newvar = $str_len/70;
											if($newvar>0){
												$array_count = $array_count - floor($newvar);
											}
											if($array_count<$coun_br){
												//$all_bio = $all_bio . $exp_br[$coun_br];
												if($coun_br==1){
													$all_bio = $all_bio . substr($exp_br[$coun_br],0,$range_chars);
												}else{
													$all_bio = $all_bio . substr($exp_br[$coun_br],0,120);
												}
											}else{
												if($array_count==$coun_br){
													$all_bio = $all_bio . substr($exp_br[$coun_br],0,120);
												}else{
													$all_bio = $all_bio . $exp_br[$coun_br]."<br>";
												}
											}
										}
									}
									echo substr($all_bio,0,$range_chars).' '.'<a href="javascript:void(0);" onClick="open_desc()" style="text-decoration:underline;" id="more_link" class="fancybox fancybox.ajax" name="more_link">More></a>'; 
								}else{
									echo substr($getuser['bio'],0,$range_chars).' '.'<a href="javascript:void(0);" onClick="open_desc()" style="text-decoration:underline;" class="fancybox fancybox.ajax" id="more_link" name="more_link">More></a>';
								}
							}
							else
							{
								echo $getuser['bio']; 
							}
						}
						elseif($getuser['description']!="")
						{
							$exp_br_out = explode("<br>",$getuser['bio']);
							if(strlen($getuser['description'])>$range_chars || count($exp_br_out) > $range_brs)
							{
								$exp_br = explode("<br>",$getuser['description']);
								if(count($exp_br) > $range_brs){
									$all_bio="";
									$array_count = 7;
									for($coun_br=0;$coun_br<$array_count;$coun_br++){
										if(strlen($all_bio)<$range_chars){
											$str_len = strlen($exp_br[$coun_br]);
											$newvar = $str_len/70;
											if($newvar>0){
												$array_count = $array_count - floor($newvar);
											}
											if($array_count<$coun_br){
												//$all_bio = $all_bio . $exp_br[$coun_br];
												if($coun_br==1){
													$all_bio = $all_bio . substr($exp_br[$coun_br],0,$range_chars);
												}else{
													$all_bio = $all_bio . substr($exp_br[$coun_br],0,120);
												}
											}else{
												if($array_count==$coun_br){
													$all_bio = $all_bio . substr($exp_br[$coun_br],0,120);
												}else{
													$all_bio = $all_bio . $exp_br[$coun_br]."<br>";
												}
											}
										}
									}
									echo substr($all_bio,0,$range_chars).' '.'<a href="javascript:void(0);" onClick="open_desc()" style="text-decoration:underline;" id="more_link" class="fancybox fancybox.ajax" name="more_link">More></a>'; 
								}else{
									echo substr($getuser['description'],0,$range_chars).' '.'<a href="javascript:void(0);" onClick="open_desc()" style="text-decoration:underline;" class="fancybox fancybox.ajax" id="more_link" name="more_link">More></a>';
								}
							}
							else
							{
								echo $getuser['description'];
							}
						}
					}
					else if($get_permission['artist_biography'] == "members" && ($find_member['expiry_date']>$date || $find_member['lifetime']==1))
					{
						if($getuser['bio']!="") 
						{ 
							$exp_br_out = explode("<br>",$getuser['bio']);
							if(strlen($getuser['bio'])>$range_chars || count($exp_br_out) > $range_brs)
							{
								$exp_br = explode("<br>",$getuser['bio']);
								if(count($exp_br) > $range_brs){
									$all_bio="";
									$array_count = 7;
									for($coun_br=0;$coun_br<$array_count;$coun_br++){
										if(strlen($all_bio)<$range_chars){
											$str_len = strlen($exp_br[$coun_br]);
											$newvar = $str_len/70;
											if($newvar>0){
												$array_count = $array_count - floor($newvar);
											}
											if($array_count<$coun_br){
												//$all_bio = $all_bio . $exp_br[$coun_br];
												if($coun_br==1){
													$all_bio = $all_bio . substr($exp_br[$coun_br],0,$range_chars);
												}else{
													$all_bio = $all_bio . substr($exp_br[$coun_br],0,120);
												}
											}else{
												if($array_count==$coun_br){
													$all_bio = $all_bio . substr($exp_br[$coun_br],0,120);
												}else{
													$all_bio = $all_bio . $exp_br[$coun_br]."<br>";
												}
											}
										}
									}
									echo substr($all_bio,0,$range_chars).' '.'<a href="javascript:void(0);" onClick="open_desc()" style="text-decoration:underline;" id="more_link" class="fancybox fancybox.ajax" name="more_link">More></a>'; 
								}else{
									echo substr($getuser['bio'],0,$range_chars).' '.'<a href="javascript:void(0);" onClick="open_desc()" style="text-decoration:underline;" class="fancybox fancybox.ajax" id="more_link" name="more_link">More></a>';
								}
							}
							else
							{
								echo $getuser['bio']; 
							}
						}
						elseif($getuser['description']!="")
						{
							$exp_br_out = explode("<br>",$getuser['bio']);
							if(strlen($getuser['description'])>$range_chars || count($exp_br_out) > $range_brs)
							{
								$exp_br = explode("<br>",$getuser['description']);
								if(count($exp_br) > $range_brs){
									$all_bio="";
									$array_count = 7;
									for($coun_br=0;$coun_br<$array_count;$coun_br++){
										if(strlen($all_bio)<$range_chars){
											$str_len = strlen($exp_br[$coun_br]);
											$newvar = $str_len/70;
											if($newvar>0){
												$array_count = $array_count - floor($newvar);
											}
											if($array_count<$coun_br){
												//$all_bio = $all_bio . $exp_br[$coun_br];
												if($coun_br==1){
													$all_bio = $all_bio . substr($exp_br[$coun_br],0,$range_chars);
												}else{
													$all_bio = $all_bio . substr($exp_br[$coun_br],0,120);
												}
											}else{
												if($array_count==$coun_br){
													$all_bio = $all_bio . substr($exp_br[$coun_br],0,120);
												}else{
													$all_bio = $all_bio . $exp_br[$coun_br]."<br>";
												}
											}
										}
									}
									echo substr($all_bio,0,$range_chars).' '.'<a href="javascript:void(0);" onClick="open_desc()" style="text-decoration:underline;" id="more_link" class="fancybox fancybox.ajax" name="more_link">More></a>'; 
								}else{
									echo substr($getuser['description'],0,$range_chars).' '.'<a href="javascript:void(0);" onClick="open_desc()" style="text-decoration:underline;" class="fancybox fancybox.ajax" id="more_link" name="more_link">More></a>';
								}
							}
							else
							{
								echo $getuser['description'];
							}
						}
					}
					else if($_SESSION['login_email'] == $getgeneral['email'])
					{
						if($getuser['bio']!="") 
						{ 
							$exp_br_out = explode("<br>",$getuser['bio']);
							if(strlen($getuser['bio'])>$range_chars || count($exp_br_out) > $range_brs)
							{
								$exp_br = explode("<br>",$getuser['bio']);
								if(count($exp_br) > $range_brs){
									$all_bio="";
									$array_count = 7;
									for($coun_br=0;$coun_br<$array_count;$coun_br++){
										if(strlen($all_bio)<$range_chars){
											$str_len = strlen($exp_br[$coun_br]);
											$newvar = $str_len/70;
											if($newvar>0){
												$array_count = $array_count - floor($newvar);
											}
											if($array_count<$coun_br){
												//$all_bio = $all_bio . $exp_br[$coun_br];
												if($coun_br==1){
													$all_bio = $all_bio . substr($exp_br[$coun_br],0,$range_chars);
												}else{
													$all_bio = $all_bio . substr($exp_br[$coun_br],0,120);
												}
											}else{
												if($array_count==$coun_br){
													$all_bio = $all_bio . substr($exp_br[$coun_br],0,120);
												}else{
													$all_bio = $all_bio . $exp_br[$coun_br]."<br>";
												}
											}
										}
									}
									echo substr($all_bio,0,$range_chars).' '.'<a href="javascript:void(0);" onClick="open_desc()" style="text-decoration:underline;" id="more_link" class="fancybox fancybox.ajax" name="more_link">More></a>'; 
								}else{
									echo substr($getuser['bio'],0,$range_chars).' '.'<a href="javascript:void(0);" onClick="open_desc()" style="text-decoration:underline;" class="fancybox fancybox.ajax" id="more_link" name="more_link">More></a>';
								}
							}
							else
							{
								echo $getuser['bio']; 
							}
						}
						elseif($getuser['description']!="")
						{
							$exp_br_out = explode("<br>",$getuser['bio']);
							if(strlen($getuser['description'])>$range_chars || count($exp_br_out) > $range_brs)
							{
								$exp_br = explode("<br>",$getuser['description']);
								if(count($exp_br) > $range_brs){
									$all_bio="";
									$array_count = 7;
									for($coun_br=0;$coun_br<$array_count;$coun_br++){
										if(strlen($all_bio)<$range_chars){
											$str_len = strlen($exp_br[$coun_br]);
											$newvar = $str_len/70;
											if($newvar>0){
												$array_count = $array_count - floor($newvar);
											}
											if($array_count<$coun_br){
												//$all_bio = $all_bio . $exp_br[$coun_br];
												if($coun_br==1){
													$all_bio = $all_bio . substr($exp_br[$coun_br],0,$range_chars);
												}else{
													$all_bio = $all_bio . substr($exp_br[$coun_br],0,120);
												}
											}else{
												if($array_count==$coun_br){
													$all_bio = $all_bio . substr($exp_br[$coun_br],0,120);
												}else{
													$all_bio = $all_bio . $exp_br[$coun_br]."<br>";
												}
											}
										}
									}
									echo substr($all_bio,0,$range_chars).' '.'<a href="javascript:void(0);" onClick="open_desc()" style="text-decoration:underline;" id="more_link" class="fancybox fancybox.ajax" name="more_link">More></a>'; 
								}else{
									echo substr($getuser['description'],0,$range_chars).' '.'<a href="javascript:void(0);" onClick="open_desc()" style="text-decoration:underline;" class="fancybox fancybox.ajax" id="more_link" name="more_link">More></a>';
								}
							}
							else
							{
								echo $getuser['description'];
							}
						}
					}
				}
				else
				{
					if($getuser['bio']!="") 
						{ 
							$exp_br_out = explode("<br>",$getuser['bio']);
							if(strlen($getuser['bio'])>$range_chars || count($exp_br_out) > $range_brs)
							{
								$exp_br = explode("<br>",$getuser['bio']);
								if(count($exp_br) > $range_brs){
									$all_bio="";
									$array_count = 7;
									for($coun_br=0;$coun_br<$array_count;$coun_br++){
										if(strlen($all_bio)<$range_chars){
											$str_len = strlen($exp_br[$coun_br]);
											$newvar = $str_len/70;
											if($newvar>0){
												$array_count = $array_count - floor($newvar);
											}
											if($array_count<$coun_br){
												//$all_bio = $all_bio . $exp_br[$coun_br];
												if($coun_br==1){
													$all_bio = $all_bio . substr($exp_br[$coun_br],0,$range_chars);
												}else{
													$all_bio = $all_bio . substr($exp_br[$coun_br],0,120);
												}
											}else{
												if($array_count==$coun_br){
													$all_bio = $all_bio . substr($exp_br[$coun_br],0,120);
												}else{
													$all_bio = $all_bio . $exp_br[$coun_br]."<br>";
												}
											}
										}
									}
									echo substr($all_bio,0,$range_chars).' '.'<a href="javascript:void(0);" onClick="open_desc()" style="text-decoration:underline;" id="more_link" class="fancybox fancybox.ajax" name="more_link">More></a>'; 
								}else{
									echo substr($getuser['bio'],0,$range_chars).' '.'<a href="javascript:void(0);" onClick="open_desc()" style="text-decoration:underline;" class="fancybox fancybox.ajax" id="more_link" name="more_link">More></a>';
								}	
							}
							else
							{
								echo $getuser['bio']; 
							}
						}
						elseif($getuser['description']!="")
						{
							$exp_br_out = explode("<br>",$getuser['bio']);
							if(strlen($getuser['description'])>$range_chars || count($exp_br_out) > $range_brs)
							{
								$exp_br = explode("<br>",$getuser['description']);
								if(count($exp_br) > $range_brs){
									$all_bio="";
									$array_count = 7;
									for($coun_br=0;$coun_br<$array_count;$coun_br++){
										if(strlen($all_bio)<$range_chars){
											$str_len = strlen($exp_br[$coun_br]);
											$newvar = $str_len/70;
											if($newvar>0){
												$array_count = $array_count - floor($newvar);
											}
											if($array_count<$coun_br){
												//$all_bio = $all_bio . $exp_br[$coun_br];
												if($coun_br==1){
													$all_bio = $all_bio . substr($exp_br[$coun_br],0,$range_chars);
												}else{
													$all_bio = $all_bio . substr($exp_br[$coun_br],0,120);
												}
											}else{
												if($array_count==$coun_br){
													$all_bio = $all_bio . substr($exp_br[$coun_br],0,120);
												}else{
													$all_bio = $all_bio . $exp_br[$coun_br]."<br>";
												}
											}
										}
									}
									echo substr($all_bio,0,$range_chars).' '.'<a href="javascript:void(0);" onClick="open_desc()" style="text-decoration:underline;" id="more_link" class="fancybox fancybox.ajax" name="more_link">More></a>'; 
								}else{
									echo substr($getuser['description'],0,$range_chars).' '.'<a href="javascript:void(0);" onClick="open_desc()" style="text-decoration:underline;" class="fancybox fancybox.ajax" id="more_link" name="more_link">More></a>';
								}
							}
							else
							{
								echo $getuser['description'];
							}
						}
				}
			}
			else if($community_check !=0)
			//else if($community_check !=0 || $community_event_check !=0 || $community_project_check !=0)
			{
			
				if(isset($get_permission) && !empty($get_permission))
				{
					if($get_permission['community_biography'] == "open")
					{
						if($getuser['bio']!="") 
						{ 
							$exp_br_out = explode("<br>",$getuser['bio']);
							if(strlen($getuser['bio'])>$range_chars || count($exp_br_out) > $range_brs)
							{
								$exp_br = explode("<br>",$getuser['bio']);
								if(count($exp_br) > $range_brs){
									$all_bio="";
									$array_count = 7;
									for($coun_br=0;$coun_br<$array_count;$coun_br++){
										if(strlen($all_bio)<$range_chars){
											$str_len = strlen($exp_br[$coun_br]);
											$newvar = $str_len/70;
											if($newvar>0){
												$array_count = $array_count - floor($newvar);
											}
											if($array_count<$coun_br){
												//$all_bio = $all_bio . $exp_br[$coun_br];
												if($coun_br==1){
													$all_bio = $all_bio . substr($exp_br[$coun_br],0,$range_chars);
												}else{
													$all_bio = $all_bio . substr($exp_br[$coun_br],0,120);
												}
											}else{
												if($array_count==$coun_br){
													$all_bio = $all_bio . substr($exp_br[$coun_br],0,120);
												}else{
													$all_bio = $all_bio . $exp_br[$coun_br]."<br>";
												}
											}
										}
									}
									echo substr($all_bio,0,$range_chars).' '.'<a href="javascript:void(0);" onClick="open_desc()" style="text-decoration:underline;" id="more_link" class="fancybox fancybox.ajax" name="more_link">More></a>'; 
								}else{
									echo substr($getuser['bio'],0,$range_chars).' '.'<a href="javascript:void(0);" onClick="open_desc()" style="text-decoration:underline;" class="fancybox fancybox.ajax" id="more_link" name="more_link">More></a>';
								}
							}
							else
							{
								echo $getuser['bio']; 
							}
						}
						elseif($getuser['description']!="")
						{
							$exp_br_out = explode("<br>",$getuser['bio']);
							if(strlen($getuser['description'])>$range_chars || count($exp_br_out) > $range_brs)
							{
								$exp_br = explode("<br>",$getuser['description']);
								if(count($exp_br) > $range_brs){
									$all_bio="";
									$array_count = 7;
									for($coun_br=0;$coun_br<$array_count;$coun_br++){
										if(strlen($all_bio)<$range_chars){
											$str_len = strlen($exp_br[$coun_br]);
											$newvar = $str_len/70;
											if($newvar>0){
												$array_count = $array_count - floor($newvar);
											}
											if($array_count<$coun_br){
												//$all_bio = $all_bio . $exp_br[$coun_br];
												if($coun_br==1){
													$all_bio = $all_bio . substr($exp_br[$coun_br],0,$range_chars);
												}else{
													$all_bio = $all_bio . substr($exp_br[$coun_br],0,120);
												}
											}else{
												if($array_count==$coun_br){
													$all_bio = $all_bio . substr($exp_br[$coun_br],0,120);
												}else{
													$all_bio = $all_bio . $exp_br[$coun_br]."<br>";
												}
											}
										}
									}
									echo substr($all_bio,0,$range_chars).' '.'<a href="javascript:void(0);" onClick="open_desc()" style="text-decoration:underline;" id="more_link" class="fancybox fancybox.ajax" name="more_link">More></a>'; 
								}else{
									echo substr($getuser['description'],0,$range_chars).' '.'<a href="javascript:void(0);" onClick="open_desc()" style="text-decoration:underline;" class="fancybox fancybox.ajax" id="more_link" name="more_link">More></a>';
								}
							}
							else
							{
								echo $getuser['description'];
							}
						}
					}
					else if($get_permission['community_biography'] == "private" && $_SESSION['login_email'] == $getgeneral['email'])
					{
						if($getuser['bio']!="") 
						{ 
							$exp_br_out = explode("<br>",$getuser['bio']);
							if(strlen($getuser['bio'])>$range_chars || count($exp_br_out) > $range_brs)
							{
								$exp_br = explode("<br>",$getuser['bio']);
								if(count($exp_br) > $range_brs){
									$all_bio="";
									$array_count = 7;
									for($coun_br=0;$coun_br<$array_count;$coun_br++){
										if(strlen($all_bio)<$range_chars){
											$str_len = strlen($exp_br[$coun_br]);
											$newvar = $str_len/70;
											if($newvar>0){
												$array_count = $array_count - floor($newvar);
											}
											if($array_count<$coun_br){
												//$all_bio = $all_bio . $exp_br[$coun_br];
												if($coun_br==1){
													$all_bio = $all_bio . substr($exp_br[$coun_br],0,$range_chars);
												}else{
													$all_bio = $all_bio . substr($exp_br[$coun_br],0,120);
												}
											}else{
												if($array_count==$coun_br){
													$all_bio = $all_bio . substr($exp_br[$coun_br],0,120);
												}else{
													$all_bio = $all_bio . $exp_br[$coun_br]."<br>";
												}
											}
										}
									}
									echo substr($all_bio,0,$range_chars).' '.'<a href="javascript:void(0);" onClick="open_desc()" style="text-decoration:underline;" id="more_link" class="fancybox fancybox.ajax" name="more_link">More></a>'; 
								}else{
									echo substr($getuser['bio'],0,$range_chars).' '.'<a href="javascript:void(0);" onClick="open_desc()" style="text-decoration:underline;" class="fancybox fancybox.ajax" id="more_link" name="more_link">More></a>';
								}
							}
							else
							{
								echo $getuser['bio']; 
							}
						}
						elseif($getuser['description']!="")
						{
							$exp_br_out = explode("<br>",$getuser['bio']);
							if(strlen($getuser['description'])>$range_chars || count($exp_br_out) > $range_brs)
							{
								$exp_br = explode("<br>",$getuser['description']);
								if(count($exp_br) > $range_brs){
									$all_bio="";
									$array_count = 7;
									for($coun_br=0;$coun_br<$array_count;$coun_br++){
										if(strlen($all_bio)<$range_chars){
											$str_len = strlen($exp_br[$coun_br]);
											$newvar = $str_len/70;
											if($newvar>0){
												$array_count = $array_count - floor($newvar);
											}
											if($array_count<$coun_br){
												//$all_bio = $all_bio . $exp_br[$coun_br];
												if($coun_br==1){
													$all_bio = $all_bio . substr($exp_br[$coun_br],0,$range_chars);
												}else{
													$all_bio = $all_bio . substr($exp_br[$coun_br],0,120);
												}
											}else{
												if($array_count==$coun_br){
													$all_bio = $all_bio . substr($exp_br[$coun_br],0,120);
												}else{
													$all_bio = $all_bio . $exp_br[$coun_br]."<br>";
												}
											}
										}
									}
									echo substr($all_bio,0,$range_chars).' '.'<a href="javascript:void(0);" onClick="open_desc()" style="text-decoration:underline;" id="more_link" class="fancybox fancybox.ajax" name="more_link">More></a>'; 
								}else{
									echo substr($getuser['description'],0,$range_chars).' '.'<a href="javascript:void(0);" onClick="open_desc()" style="text-decoration:underline;" class="fancybox fancybox.ajax" id="more_link" name="more_link">More></a>';
								}
							}
							else
							{
								echo $getuser['description'];
							}
						}
					}
					else if($get_permission['community_biography'] == "public" && isset($_SESSION['login_email']))
					{
						if($getuser['bio']!="") 
						{ 
							$exp_br_out = explode("<br>",$getuser['bio']);
							if(strlen($getuser['bio'])>$range_chars || count($exp_br_out) > $range_brs)
							{
								$exp_br = explode("<br>",$getuser['bio']);
								if(count($exp_br) > $range_brs){
									$all_bio="";
									$array_count = 7;
									for($coun_br=0;$coun_br<$array_count;$coun_br++){
										if(strlen($all_bio)<$range_chars){
											$str_len = strlen($exp_br[$coun_br]);
											$newvar = $str_len/70;
											if($newvar>0){
												$array_count = $array_count - floor($newvar);
											}
											if($array_count<$coun_br){
												//$all_bio = $all_bio . $exp_br[$coun_br];
												if($coun_br==1){
													$all_bio = $all_bio . substr($exp_br[$coun_br],0,$range_chars);
												}else{
													$all_bio = $all_bio . substr($exp_br[$coun_br],0,120);
												}
											}else{
												if($array_count==$coun_br){
													$all_bio = $all_bio . substr($exp_br[$coun_br],0,120);
												}else{
													$all_bio = $all_bio . $exp_br[$coun_br]."<br>";
												}
											}
										}
									}
									echo substr($all_bio,0,$range_chars).' '.'<a href="javascript:void(0);" onClick="open_desc()" style="text-decoration:underline;" id="more_link" class="fancybox fancybox.ajax" name="more_link">More></a>'; 
								}else{
									echo substr($getuser['bio'],0,$range_chars).' '.'<a href="javascript:void(0);" onClick="open_desc()" style="text-decoration:underline;" class="fancybox fancybox.ajax" id="more_link" name="more_link">More></a>';
								}
							}
							else
							{
								echo $getuser['bio']; 
							}
						}
						elseif($getuser['description']!="")
						{
							$exp_br_out = explode("<br>",$getuser['bio']);
							if(strlen($getuser['description'])>$range_chars || count($exp_br_out) > $range_brs)
							{
								$exp_br = explode("<br>",$getuser['description']);
								if(count($exp_br) > $range_brs){
									$all_bio="";
									$array_count = 7;
									for($coun_br=0;$coun_br<$array_count;$coun_br++){
										if(strlen($all_bio)<$range_chars){
											$str_len = strlen($exp_br[$coun_br]);
											$newvar = $str_len/70;
											if($newvar>0){
												$array_count = $array_count - floor($newvar);
											}
											if($array_count<$coun_br){
												//$all_bio = $all_bio . $exp_br[$coun_br];
												if($coun_br==1){
													$all_bio = $all_bio . substr($exp_br[$coun_br],0,$range_chars);
												}else{
													$all_bio = $all_bio . substr($exp_br[$coun_br],0,120);
												}
											}else{
												if($array_count==$coun_br){
													$all_bio = $all_bio . substr($exp_br[$coun_br],0,120);
												}else{
													$all_bio = $all_bio . $exp_br[$coun_br]."<br>";
												}
											}
										}
									}
									echo substr($all_bio,0,$range_chars).' '.'<a href="javascript:void(0);" onClick="open_desc()" style="text-decoration:underline;" id="more_link" class="fancybox fancybox.ajax" name="more_link">More></a>'; 
								}else{
									echo substr($getuser['description'],0,$range_chars).' '.'<a href="javascript:void(0);" onClick="open_desc()" style="text-decoration:underline;" class="fancybox fancybox.ajax" id="more_link" name="more_link">More></a>';
								}
							}
							else
							{
								echo $getuser['description'];
							}
						}
					}
					else if($get_permission['community_biography'] == "members" && ($find_member['expiry_date']>$date || $find_member['lifetime']==1))
					{
						if($getuser['bio']!="") 
						{ 
							$exp_br_out = explode("<br>",$getuser['bio']);
							if(strlen($getuser['bio'])>$range_chars || count($exp_br_out) > $range_brs)
							{
								$exp_br = explode("<br>",$getuser['bio']);
								if(count($exp_br) > $range_brs){
									$all_bio="";
									$array_count = 7;
									for($coun_br=0;$coun_br<$array_count;$coun_br++){
										if(strlen($all_bio)<$range_chars){
											$str_len = strlen($exp_br[$coun_br]);
											$newvar = $str_len/70;
											if($newvar>0){
												$array_count = $array_count - floor($newvar);
											}
											if($array_count<$coun_br){
												//$all_bio = $all_bio . $exp_br[$coun_br];
												if($coun_br==1){
													$all_bio = $all_bio . substr($exp_br[$coun_br],0,$range_chars);
												}else{
													$all_bio = $all_bio . substr($exp_br[$coun_br],0,120);
												}
											}else{
												if($array_count==$coun_br){
													$all_bio = $all_bio . substr($exp_br[$coun_br],0,120);
												}else{
													$all_bio = $all_bio . $exp_br[$coun_br]."<br>";
												}
											}
										}
									}
									echo substr($all_bio,0,$range_chars).' '.'<a href="javascript:void(0);" onClick="open_desc()" style="text-decoration:underline;" id="more_link" class="fancybox fancybox.ajax" name="more_link">More></a>'; 
								}else{
									echo substr($getuser['bio'],0,$range_chars).' '.'<a href="javascript:void(0);" onClick="open_desc()" style="text-decoration:underline;" class="fancybox fancybox.ajax" id="more_link" name="more_link">More></a>';
								}	
							}
							else
							{
								echo $getuser['bio']; 
							}
						}
						elseif($getuser['description']!="")
						{
							$exp_br_out = explode("<br>",$getuser['bio']);
							if(strlen($getuser['description'])>$range_chars || count($exp_br_out) > $range_brs)
							{
								$exp_br = explode("<br>",$getuser['description']);
								if(count($exp_br) > $range_brs){
									$all_bio="";
									$array_count = 7;
									for($coun_br=0;$coun_br<$array_count;$coun_br++){
										if(strlen($all_bio)<$range_chars){
											$str_len = strlen($exp_br[$coun_br]);
											$newvar = $str_len/70;
											if($newvar>0){
												$array_count = $array_count - floor($newvar);
											}
											if($array_count<$coun_br){
												//$all_bio = $all_bio . $exp_br[$coun_br];
												if($coun_br==1){
													$all_bio = $all_bio . substr($exp_br[$coun_br],0,$range_chars);
												}else{
													$all_bio = $all_bio . substr($exp_br[$coun_br],0,120);
												}
											}else{
												if($array_count==$coun_br){
													$all_bio = $all_bio . substr($exp_br[$coun_br],0,120);
												}else{
													$all_bio = $all_bio . $exp_br[$coun_br]."<br>";
												}
											}
										}
									}
									echo substr($all_bio,0,$range_chars).' '.'<a href="javascript:void(0);" onClick="open_desc()" style="text-decoration:underline;" id="more_link" class="fancybox fancybox.ajax" name="more_link">More></a>'; 
								}else{
									echo substr($getuser['description'],0,$range_chars).' '.'<a href="javascript:void(0);" onClick="open_desc()" style="text-decoration:underline;" class="fancybox fancybox.ajax" id="more_link" name="more_link">More></a>';
								}
							}
							else
							{
								echo $getuser['description'];
							}
						}
					}
					else if($_SESSION['login_email'] == $getgeneral['email'])
					{
						if($getuser['bio']!="") 
						{ 
							$exp_br_out = explode("<br>",$getuser['bio']);
							if(strlen($getuser['bio'])>$range_chars || count($exp_br_out) > $range_brs)
							{
								$exp_br = explode("<br>",$getuser['bio']);
								if(count($exp_br) > $range_brs){
									$all_bio="";
									$array_count = 7;
									for($coun_br=0;$coun_br<$array_count;$coun_br++){
										if(strlen($all_bio)<$range_chars){
											$str_len = strlen($exp_br[$coun_br]);
											$newvar = $str_len/70;
											if($newvar>0){
												$array_count = $array_count - floor($newvar);
											}
											if($array_count<$coun_br){
												//$all_bio = $all_bio . $exp_br[$coun_br];
												if($coun_br==1){
													$all_bio = $all_bio . substr($exp_br[$coun_br],0,$range_chars);
												}else{
													$all_bio = $all_bio . substr($exp_br[$coun_br],0,120);
												}
											}else{
												if($array_count==$coun_br){
													$all_bio = $all_bio . substr($exp_br[$coun_br],0,120);
												}else{
													$all_bio = $all_bio . $exp_br[$coun_br]."<br>";
												}
											}
										}
									}
									echo substr($all_bio,0,$range_chars).' '.'<a href="javascript:void(0);" onClick="open_desc()" style="text-decoration:underline;" id="more_link" class="fancybox fancybox.ajax" name="more_link">More></a>'; 
								}else{
									echo substr($getuser['bio'],0,$range_chars).' '.'<a href="javascript:void(0);" onClick="open_desc()" style="text-decoration:underline;" class="fancybox fancybox.ajax" id="more_link" name="more_link">More></a>';
								}
							}
							else
							{
								echo $getuser['bio']; 
							}
						}
						elseif($getuser['description']!="")
						{
							$exp_br_out = explode("<br>",$getuser['bio']);
							if(strlen($getuser['description'])>$range_chars || count($exp_br_out) > $range_brs)
							{
								$exp_br = explode("<br>",$getuser['description']);
								if(count($exp_br) > $range_brs){
									$all_bio="";
									$array_count = 7;
									for($coun_br=0;$coun_br<$array_count;$coun_br++){
										if(strlen($all_bio)<$range_chars){
											$str_len = strlen($exp_br[$coun_br]);
											$newvar = $str_len/70;
											if($newvar>0){
												$array_count = $array_count - floor($newvar);
											}
											if($array_count<$coun_br){
												//$all_bio = $all_bio . $exp_br[$coun_br];
												if($coun_br==1){
													$all_bio = $all_bio . substr($exp_br[$coun_br],0,$range_chars);
												}else{
													$all_bio = $all_bio . substr($exp_br[$coun_br],0,120);
												}
											}else{
												if($array_count==$coun_br){
													$all_bio = $all_bio . substr($exp_br[$coun_br],0,120);
												}else{
													$all_bio = $all_bio . $exp_br[$coun_br]."<br>";
												}
											}
										}
									}
									echo substr($all_bio,0,$range_chars).' '.'<a href="javascript:void(0);" onClick="open_desc()" style="text-decoration:underline;" id="more_link" class="fancybox fancybox.ajax" name="more_link">More></a>'; 
								}else{
									echo substr($getuser['description'],0,$range_chars).' '.'<a href="javascript:void(0);" onClick="open_desc()" style="text-decoration:underline;" class="fancybox fancybox.ajax" id="more_link" name="more_link">More></a>';
								}
							}
							else
							{
								echo $getuser['description'];
							}
						}
					}
				}
				else{
					if($getuser['bio']!="") 
						{ 
							$exp_br_out = explode("<br>",$getuser['bio']);
							if(strlen($getuser['bio'])>$range_chars || count($exp_br_out) > $range_brs)
							{
								$exp_br = explode("<br>",$getuser['bio']);
								if(count($exp_br) > $range_brs){
									$all_bio="";
									$array_count = 7;
									for($coun_br=0;$coun_br<$array_count;$coun_br++){
										if(strlen($all_bio)<$range_chars){
											$str_len = strlen($exp_br[$coun_br]);
											$newvar = $str_len/70;
											if($newvar>0){
												$array_count = $array_count - floor($newvar);
											}
											if($array_count<$coun_br){
												//$all_bio = $all_bio . $exp_br[$coun_br];
												if($coun_br==1){
													$all_bio = $all_bio . substr($exp_br[$coun_br],0,$range_chars);
												}else{
													$all_bio = $all_bio . substr($exp_br[$coun_br],0,120);
												}
											}else{
												if($array_count==$coun_br){
													$all_bio = $all_bio . substr($exp_br[$coun_br],0,120);
												}else{
													$all_bio = $all_bio . $exp_br[$coun_br]."<br>";
												}
											}
										}
									}
									echo substr($all_bio,0,$range_chars).' '.'<a href="javascript:void(0);" onClick="open_desc()" style="text-decoration:underline;" id="more_link" class="fancybox fancybox.ajax" name="more_link">More></a>'; 
								}else{
									echo substr($getuser['bio'],0,$range_chars).' '.'<a href="javascript:void(0);" onClick="open_desc()" style="text-decoration:underline;" id="more_link" name="more_link" class="fancybox fancybox.ajax">More></a>';
								}
							}
							else
							{
								echo $getuser['bio']; 
							}
						}
						elseif($getuser['description']!="")
						{
							$exp_br_out = explode("<br>",$getuser['bio']);
							if(strlen($getuser['description'])>$range_chars || count($exp_br_out) > $range_brs)
							{
								$exp_br = explode("<br>",$getuser['description']);
								if(count($exp_br) > $range_brs){
									$all_bio="";
									$array_count = 7;
									for($coun_br=0;$coun_br<$array_count;$coun_br++){
										if(strlen($all_bio)<$range_chars){
											$str_len = strlen($exp_br[$coun_br]);
											$newvar = $str_len/70;
											if($newvar>0){
												$array_count = $array_count - floor($newvar);
											}
											if($array_count<$coun_br){
												//$all_bio = $all_bio . $exp_br[$coun_br];
												if($coun_br==1){
													$all_bio = $all_bio . substr($exp_br[$coun_br],0,$range_chars);
												}else{
													$all_bio = $all_bio . substr($exp_br[$coun_br],0,120);
												}
											}else{
												if($array_count==$coun_br){
													$all_bio = $all_bio . substr($exp_br[$coun_br],0,120);
												}else{
													$all_bio = $all_bio . $exp_br[$coun_br]."<br>";
												}
											}
										}
									}
									echo substr($all_bio,0,$range_chars).' '.'<a href="javascript:void(0);" onClick="open_desc()" style="text-decoration:underline;" id="more_link" class="fancybox fancybox.ajax" name="more_link">More></a>'; 
								}else{
									echo substr($getuser['description'],0,$range_chars).' '.'<a href="javascript:void(0);" onClick="open_desc()" style="text-decoration:underline;" class="fancybox fancybox.ajax" id="more_link" name="more_link">More></a>';
								}
							}
							else
							{
								echo $getuser['description'];
							}
						}
				}
				
			}
			else
			{
				if($getuser['bio']!="") 
						{ 
							$exp_br_out = explode("<br>",$getuser['bio']);
							if(strlen($getuser['bio'])>$range_chars || count($exp_br_out) > $range_brs)
							{

								$exp_br = explode("<br>",$getuser['bio']);
								if(count($exp_br) > $range_brs){
									$all_bio="";
									$array_count = 7;
									for($coun_br=0;$coun_br<$array_count;$coun_br++){
										if(strlen($all_bio)<$range_chars){
											$str_len = strlen($exp_br[$coun_br]);
											$newvar = $str_len/70;
											if($newvar>0){
												$array_count = $array_count - floor($newvar);
											}
											if($array_count<$coun_br){
												//$all_bio = $all_bio . $exp_br[$coun_br];
												if($coun_br==1){
													$all_bio = $all_bio . substr($exp_br[$coun_br],0,$range_chars);
												}else{
													$all_bio = $all_bio . substr($exp_br[$coun_br],0,120);
												}
											}else{
												if($array_count==$coun_br){
													$all_bio = $all_bio . substr($exp_br[$coun_br],0,120);
												}else{
													$all_bio = $all_bio . $exp_br[$coun_br]."<br>";
												}
											}
										}
									}
									echo substr($all_bio,0,$range_chars).' '.'<a href="javascript:void(0);" onClick="open_desc()" style="text-decoration:underline;" id="more_link" class="fancybox fancybox.ajax" name="more_link">More></a>'; 
								}else{
									echo substr($getuser['bio'],0,$range_chars).' '.'<a href="javascript:void(0);" onClick="open_desc()" style="text-decoration:underline;" class="fancybox fancybox.ajax" id="more_link" name="more_link">More></a>';
								}
							}
							else
							{
								echo $getuser['bio']; 
							}
						}
						elseif($getuser['description']!="")
						{
							$exp_br_out = explode("<br>",$getuser['bio']);
							if(strlen($getuser['description'])>$range_chars || count($exp_br_out) > $range_brs)
							{
								$exp_br = explode("<br>",$getuser['description']);
								if(count($exp_br) > $range_brs){
									$all_bio="";
									$array_count = 7;
									for($coun_br=0;$coun_br<$array_count;$coun_br++){
										if(strlen($all_bio)<$range_chars){
											$str_len = strlen($exp_br[$coun_br]);
											$newvar = $str_len/70;
											if($newvar>0){
												$array_count = $array_count - floor($newvar);
											}
											if($array_count<$coun_br){
												//$all_bio = $all_bio . $exp_br[$coun_br];
												if($coun_br==1){
													$all_bio = $all_bio . substr($exp_br[$coun_br],0,$range_chars);
												}else{
													$all_bio = $all_bio . substr($exp_br[$coun_br],0,120);
												}
											}else{
												if($array_count==$coun_br){
													$all_bio = $all_bio . substr($exp_br[$coun_br],0,120);
												}else{
													$all_bio = $all_bio . $exp_br[$coun_br]."<br>";
												}
											}
										}
									}
									echo substr($all_bio,0,$range_chars).' '.'<a href="javascript:void(0);" onClick="open_desc()" class="fancybox fancybox.ajax" style="text-decoration:underline;" id="more_link" name="more_link">More></a>'; 
								}else{
									echo substr($getuser['description'],0,$range_chars).' '.'<a href="javascript:void(0);" class="fancybox fancybox.ajax" onClick="open_desc()" style="text-decoration:underline;" id="more_link" name="more_link">More></a>';
								}
							}
							else
							{
								echo $getuser['description'];
							}
						}
			}
		
		?></p>
      </div>
	  <?php
		if($community_check!=0 || $artist_check!=0)
		{
	  ?>
		  <div id="profileStatsBlock">
			<?php
			if($artist_check!=0)
			{
				$asc_id = $artist_check['artist_id'].'~artist';
				$counts_views = $artist_check['page_views'] + 1;
				
				$sql_up = mysql_query("UPDATE general_artist SET page_views='".$counts_views."' WHERE artist_id='".$artist_check['artist_id']."'");
				
				$change_popular = $popular_obj->updateviews($counts_views,'general_artist',$artist_check['artist_id']);
			}
			if($community_check!=0)
			{
				$asc_id = $community_check['community_id'].'~community';
				
				$counts_views_c = $community_check['page_views'] + 1;
				
				$sql_up = mysql_query("UPDATE general_community SET page_views='".$counts_views_c."' WHERE community_id='".$community_check['community_id']."'");
				
				$change_popular = $popular_obj->updateviews($counts_views_c,'general_community',$community_check['community_id']);
			}
			$members = $all_stats->get__membs($asc_id);
			
			$subscribers = $all_stats->get__subcs($asc_id);
			
			if($artist_check !=0)
				{
				?>
					<div id="profileButtons">
						<a href="search.php?type_dis=artist&pritype_dis=<?php echo $get_type_dis['type_id']; ?>&subtype_dis=<?php echo $get_subtype_dis['subtype_id']; ?>" title="Search for other profiles within the same general categories.">Discover</a>
						
					<?php
						if(isset($get_permission) && !empty($get_permission))
						{
							if($get_permission['artist_homepage'] == "open")
							{
								if($getuser['homepage']!="")
								{
									if(strpos($getuser['homepage'],':')>0)
									{
							  ?>
										<a title="Go to the official website for this profile." href="<?php echo $getuser['homepage']; ?>" target="_blank">Website</a>
							  <?php
									}
									else
									{
								?>
										<a title="Go to the official website for this profile." href="//<?php echo $getuser['homepage']; ?>" target="_blank">Website</a>
							  <?php	
									}
								}
							}
							else if($get_permission['artist_homepage'] == "private" && $_SESSION['login_email'] == $getgeneral['email'])
							{
								if($getuser['homepage']!="")
								{
									if(strpos($getuser['homepage'],':')>0)
									{
							  ?>
										<a title="Go to the official website for this profile." href="<?php echo $getuser['homepage']; ?>" target="_blank">Website</a>
							  <?php
									}
									else
									{
								?>
										<a title="Go to the official website for this profile." href="//<?php echo $getuser['homepage']; ?>" target="_blank">Website</a>
							  <?php	
									}
								}
							}
							else if($get_permission['artist_homepage'] == "public" && isset($_SESSION['login_email']))
							{
								if($getuser['homepage']!="")
								{
									if(strpos($getuser['homepage'],':')>0)
									{
							  ?>
										<a title="Go to the official website for this profile." href="<?php echo $getuser['homepage']; ?>" target="_blank">Website</a>
							  <?php
									}
									else
									{
								?>
										<a title="Go to the official website for this profile." href="//<?php echo $getuser['homepage']; ?>" target="_blank">Website</a>
							  <?php	
									}
								}
							}
							else if($get_permission['artist_homepage'] == "members" && ($find_member['expiry_date']>$date || $find_member['lifetime']==1))
							{
								if($getuser['homepage']!="")
								{
									if(strpos($getuser['homepage'],':')>0)
									{
							  ?>
										<a title="Go to the official website for this profile." href="<?php echo $getuser['homepage']; ?>" target="_blank">Website</a>
							  <?php
									}
									else
									{
								?>
										<a title="Go to the official website for this profile." href="//<?php echo $getuser['homepage']; ?>" target="_blank">Website</a>
							  <?php	
									}
								}
							}
							else if($_SESSION['login_email'] == $getgeneral['email'])
							{
								if($getuser['homepage']!="")
								{
									if(strpos($getuser['homepage'],':')>0)
									{
								?>
									<a title="Go to the official website for this profile." href="<?php echo $getuser['homepage']; ?>" target="_blank">Website</a>
							  <?php
									}
									else
									{
								?>
									<a title="Go to the official website for this profile." href="//<?php echo $getuser['homepage']; ?>" target="_blank">Website</a>
							  <?php	
									}
								}
							}
						}
						else
						{
							if($getuser['homepage']!="")
							{
								if(strpos($getuser['homepage'],':')>0)
								{
						  ?>
									<a title="Go to the official website for this profile." href="<?php echo $getuser['homepage']; ?>" target="_blank">Website</a>
						  <?php
								}
								else
								{
							?>
									<a title="Go to the official website for this profile." href="//<?php echo $getuser['homepage']; ?>" target="_blank">Website</a>
							<?php
								}
							}
						}	
					?>
					</div>
					
					<div id="profileButtons">
						<a class="fancybox fancybox.ajax" title="Follow this profiles social networks and/or share this profile with your social networks." id="social_share" href="share_social.php?type=general_artist&match=<?php echo $match; ?>&fb=<?php echo $getuser['fb_share']; ?>&twit_share=<?php echo $getuser['twit_share']; ?>&gplus_share=<?php echo $getuser['gplus_share']; ?>&tubm_share=<?php echo $getuser['tubm_share']; ?>&stbu_share=<?php echo $getuser['stbu_share']; ?>&pin_share=<?php echo $getuser['pin_share']; ?>&you_share=<?php echo $getuser['you_share']; ?>&vimeo_share=<?php echo $getuser['vimeo_share']; ?>&sdcl_share=<?php echo $getuser['sdcl_share']; ?>&ints_share=<?php echo $getuser['ints_share']; ?>&subjects=<?php echo urlencode($getuser['name']);  ?>" >Social</a>
			<?php			
						if($imports!=1)
						{
						?> 
							<a title="Send a message to the creator of this page." href="javascript:void(0);" onclick="shw_messages()">Message</a>
						<?php
						}
							if($artist_check !=0)
							{
								if($imports!=1)
								{
						?>
									<a title="Send a message to the creator of this page." href="message.php?artist&urls=<?php echo $match; ?>" id="message" class="fancybox fancybox.ajax" style="display:none;">Message</a>
						<?php
								}
							}
							elseif($community_check!=0)
							{
						?>
								<a title="Send a message to the creator of this page." href="message.php?community&urls=<?php echo $match; ?>" id="message" class="fancybox fancybox.ajax" style="display:none;">Message</a>
						<?php
							}
						
					/* if($artist_check['page_views']!=0)
					{
						?><p title="The amount of times this page has been viewed.">Page Views: <?php echo $artist_check['page_views']; ?></p><?php
					} */
			?>
				</div>	
			<?php		
				}
				else if($community_check !=0)
				{
			?>
					<div id="profileButtons">
						<a href="search.php?type_dis=community&pritype_dis=<?php echo $get_type_dis['type_id']; ?>&subtype_dis=<?php echo $get_subtype_dis['subtype_id']; ?>" title="Search for other profiles within the same general categories.">Discover</a>
					<?php
					if(isset($get_permission) && !empty($get_permission))
					{
						if($get_permission['community_homepage'] == "open")
						{
							if($getuser['homepage']!="")
							{
								if(strpos($getuser['homepage'],':')>0)
								{
						  ?>
									<a title="Go to the official website for this profile." href="<?php echo $getuser['homepage']; ?>" target="_blank">Website</a>
						  <?php
								}
								else
								{
							?>
									<a title="Go to the official website for this profile." href="//<?php echo $getuser['homepage']; ?>" target="_blank">Website</a>
						  <?php	
								}
							}
						}
						else if($get_permission['community_homepage'] == "private" && $_SESSION['login_email'] == $getgeneral['email'])
						{
							if($getuser['homepage']!="")
							{
								if(strpos($getuser['homepage'],':')>0)
								{
						  ?>
									<a title="Go to the official website for this profile." href="<?php echo $getuser['homepage']; ?>" target="_blank">Website</a>
						  <?php
								}
								else
								{
							?>
									<a title="Go to the official website for this profile." href="//<?php echo $getuser['homepage']; ?>" target="_blank">Website</a>
						  <?php	
								}
							}
						}
						else if($get_permission['community_homepage'] == "public" && isset($_SESSION['login_email']))
						{
							if($getuser['homepage']!="")
							{
								if(strpos($getuser['homepage'],':')>0)
								{
						  ?>
									<a title="Go to the official website for this profile." href="<?php echo $getuser['homepage']; ?>" target="_blank">Website</a>
						  <?php	
								}
								else
								{
							?>
									<a title="Go to the official website for this profile." href="//<?php echo $getuser['homepage']; ?>" target="_blank">Website</a>
						  <?php		
								}
							}
						}
						else if($get_permission['community_homepage'] == "members" && ($find_member['expiry_date']>$date || $find_member['lifetime']==1))
						{
							if($getuser['homepage']!="")
							{
								if(strpos($getuser['homepage'],':')>0)
								{
						  ?>
									<a title="Go to the official website for this profile." href="<?php echo $getuser['homepage']; ?>" target="_blank">Website</a>
						  <?php
								}
								else
								{
							?>
									<a title="Go to the official website for this profile." href="//<?php echo $getuser['homepage']; ?>" target="_blank">Website</a>
						  <?php	
								}
							}
						}
						else if($_SESSION['login_email'] == $getgeneral['email'])
						{
							if($getuser['homepage']!="")
							{
								if(strpos($getuser['homepage'],':')>0)
								{
						  ?>
									<a title="Go to the official website for this profile." href="<?php echo $getuser['homepage']; ?>" target="_blank">Website</a>
						  <?php
								}
								else
								{
							?>
									<a title="Go to the official website for this profile." href="//<?php echo $getuser['homepage']; ?>" target="_blank">Website</a>
						  <?php		
								}
							}
						}
					}
					else
					{
						if($getuser['homepage']!="")
						{
							if(strpos($getuser['homepage'],':')>0)
							{
					  ?>
								<a title="Go to the official website for this profile." href="<?php echo $getuser['homepage']; ?>" target="_blank">Website</a>
					  <?php
							}
							else
							{
						?>
								<a title="Go to the official website for this profile." href="//<?php echo $getuser['homepage']; ?>" target="_blank">Website</a>
					  <?php	
							}
						}
					}	
				?>		
					</div>
					
					<div id="profileButtons">
						<a class="fancybox fancybox.ajax" title="Follow this profiles social networks and/or share this profile with your social networks." id="social_share" href="share_social.php?type=general_community&match=<?php echo $match; ?>&fb=<?php echo $getuser['fb_share']; ?>&twit_share=<?php echo $getuser['twit_share']; ?>&gplus_share=<?php echo $getuser['gplus_share']; ?>&tubm_share=<?php echo $getuser['tubm_share']; ?>&stbu_share=<?php echo $getuser['stbu_share']; ?>&pin_share=<?php echo $getuser['pin_share']; ?>&you_share=<?php echo $getuser['you_share']; ?>&vimeo_share=<?php echo $getuser['vimeo_share']; ?>&sdcl_share=<?php echo $getuser['sdcl_share']; ?>&ints_share=<?php echo $getuser['ints_share']; ?>&subjects=<?php echo urlencode($getuser['name']);  ?>" >Social</a>
			<?php
					if($imports!=1)
					{
					?> 
						<a title="Send a message to the creator of this page." href="javascript:void(0);" onclick="shw_messages()">Message</a>
					<?php
					}
						if($artist_check !=0)
						{
							if($imports!=1)
							{
					?>
								<a title="Send a message to the creator of this page." href="message.php?artist&urls=<?php echo $match; ?>" id="message" class="fancybox fancybox.ajax" style="display:none;">Message</a>
					<?php
							}
						}
						elseif($community_check!=0)
						{
					?>
							<a title="Send a message to the creator of this page." href="message.php?community&urls=<?php echo $match; ?>" id="message" class="fancybox fancybox.ajax" style="display:none;">Message</a>
					<?php
						}
					/* if($community_check['page_views']!=0)
					{
						?><p title="The amount of times this page has been viewed.">Page Views: <?php echo $community_check['page_views']; ?></p><?php
					} */
			?>
				</div>
			<?php
				}
			
			/* if($subscribers!='0')
			{
			?>
				<p title="The amount of users that have subscribed to this user.">Subscribers: <?php echo $subscribers; ?></p>
			<?php
			}
			 if($members!='0')
			{
			?>
				<p>Fans: <?php echo $members; ?></p>
			<?php
			} */
				
				/* else
				{
					if($getuser['homepage']!="")
					{
				  ?>
						<a href="<?php echo $getuser['homepage']; ?>" target="_blank"><p>Website</p></a>
				  <?php
					}
				} */
				
				  ?>
			
			<?php
				$ret_ans = "absent";
				if(isset($_SESSION))
				{
					if(isset($_SESSION['login_email']))
					{
						if($_SESSION['login_email']!="")
						{
							if($artist_check !=0)
							{
								$ret_ans = $display->email_subscription_schek($_SESSION['login_email'],$artist_check['artist_id'],'artist');
							}
							elseif($community_check !=0)
							{
								$ret_ans = $display->email_subscription_schek($_SESSION['login_email'],$community_check['community_id'],'community');
							}
						}
					}
				}
?>
				<div id="profileButtons">
<?php
				if($ret_ans=='present')
				{
			?>
					<div id="profileButtons_acts">
						<a title="Follow this profile to receive updates from them via your feed and/or email." href="javascript:void(0);" onclick="return login_subscribed();">Following</a>
					</div>
			<?php
				}
				else
				{
?>
					<a title="Follow this profile to receive updates from them via your feed and/or email." href="javascript:void(0);" onclick="return login_subscription();">Follow</a>
<?php
				}
			?>
			<a href="email_subscription.php" id="email_subscription" class="fancybox fancybox.ajax" title="Follow this profile to receive updates from them via your feed and/or email." style="display:none;">Follow</a>
			<a  href="email_subscribed.php" id="email_subscribed" class="fancybox fancybox.ajax" title="Follow this profile to receive updates from them via your feed and/or email." style="display:none;">Follow</a>
			<!--<a href="fan_club.php" id="fan_club">Fan Club</a>-->
			<?php $data = explode("/",$_SERVER['REQUEST_URI']); //echo $data[1]; 
			/* if($find_member != null && $find_member['general_user_id'] != null && ($find_member['expiry_date'] >$date || $find_member['lifetime']==1))
			{
				if($getuser['type']=="fan_club_yes" || $getuser['type']=="for_sale") {
				?> <a style="display:none;" href="fan_club.php?data=<?php echo $data[1];?>" id="fan_club_pro" class="fancybox fancybox.ajax">
				<?php if($getuser['type']=="fan_club_yes") { echo "Fan Club"; } elseif($getuser['type']=="for_sale") { echo "Download"; } ?></a>
				<a title="<?php if($getuser['type']=="fan_club_yes") { echo "Purchase a fan club membership to this users exclusive media."; } elseif($getuser['type']=="for_sale") { echo "Purchase this projects media."; } ?>" href="javascript:void(0);" onClick="fancy_login()" ><?php if($getuser['type']=="fan_club_yes") { echo "Fan Club"; } elseif($getuser['type']=="for_sale") { echo "Download"; } ?></a><?php }
			} */
			
			if($artist_check!=0)
			{
		?>
				<a class="fancybox fancybox.ajax" id="statistics_share" href="statistics_profs.php?views=<?php echo $artist_check['page_views']; ?>&subs=<?php echo $subscribers; ?>&type=general_artist" title="View stats on this profile page.">Stats</a>
		<?php
			}
			elseif($community_check!=0)
			{
		?>
				<a class="fancybox fancybox.ajax" id="statistics_share" href="statistics_profs.php?views=<?php echo $community_check['page_views']; ?>&subs=<?php echo $subscribers; ?>&type=general_community" title="View stats on this profile page.">Stats</a>
		<?php
			}
			?>
			</div>
		  </div>
	 <?php
		}
	if($community_event_check!=0 || $artist_event_check!=0)
	{
	?>
		<div id="profileStatsBlock">
			<?php
			if($artist_event_check!=0)
			{
				$te_count = 0;
				$tv_count = 0;
				
				if(!empty($artist_event_check['creators_info']) && $artist_event_check['creators_info']!="")
				{
					$a_eve = mysql_query("SELECT * FROM artist_event WHERE creators_info='".$artist_event_check['creators_info']."' AND del_status=0 AND active=0");
					if($a_eve!="" && $a_eve!=Null)
					{
						$ae_count = mysql_num_rows($a_eve);
					}
					
					$c_eve = mysql_query("SELECT * FROM community_event WHERE creators_info='".$artist_event_check['creators_info']."' AND del_status=0 AND active=0");
					if($c_eve!="" && $c_eve!=Null)
					{
						$ce_count = mysql_num_rows($c_eve);
					}
					
					$te_count = $ae_count + $ce_count;
					
					/* if($te_count>0)
					{
				?>
						<p>Events by Promoter: <?php echo $te_count; ?></p>
				<?php
					} */
				}
				
				if(!empty($artist_event_check['venue_name']) && $artist_event_check['venue_name']!="")
				{
					$a_ven = mysql_query("SELECT * FROM artist_event WHERE venue_name='".$artist_event_check['venue_name']."' AND del_status=0 AND active=0");
					if($a_ven!="" && $a_ven!=Null)
					{
						$av_count = mysql_num_rows($a_ven);
					}
					
					$c_ven = mysql_query("SELECT * FROM community_event WHERE venue_name='".$artist_event_check['venue_name']."' AND del_status=0 AND active=0");
					if($c_ven!="" && $c_ven!=Null)
					{
						$cv_count = mysql_num_rows($c_ven);
					}
					
					$tv_count = $av_count + $cv_count;
					
					/* if($tv_count>0)
					{
				?>
						<p> Events by Venue: <?php echo $tv_count; ?> </p>
				<?php
					} */
				}
		?>
				<div id="profileButtons">
					<a href="search.php?type_dis=event&pritype_dis=<?php echo $get_type_dis['type_id']; ?>&subtype_dis=<?php echo $get_subtype_dis['subtype_id']; ?>" title="Search for other profiles within the same general categories.">Discover</a>
						
					<a class="fancybox fancybox.ajax" id="statistics_share" href="statistics_profs.php?views=<?php echo $artist_event_check['page_views']; ?>&ebp=<?php echo $te_count; ?>&ebv=<?php echo $tv_count; ?>&type=artist_event" title="View stats on this profile page.">Stats</a>
				</div>
				
			<div id="profileButtons">
				<a class="fancybox fancybox.ajax" title="Follow this profiles social networks and/or share this profile with your social networks." id="social_share" href="share_social.php?type=artist_event&match=<?php echo $match; ?>&fb=<?php echo $artist_event_check['fb_share']; ?>&twit_share=<?php echo $artist_event_check['twit_share']; ?>&gplus_share=<?php echo $artist_event_check['gplus_share']; ?>&tubm_share=<?php echo $artist_event_check['tubm_share']; ?>&stbu_share=<?php echo $artist_event_check['stbu_share']; ?>&pin_share=<?php echo $artist_event_check['pin_share']; ?>&you_share=<?php echo $artist_event_check['you_share']; ?>&vimeo_share=<?php echo $artist_event_check['vimeo_share']; ?>&sdcl_share=<?php echo $artist_event_check['sdcl_share']; ?>&ints_share=<?php echo $artist_event_check['ints_share']; ?>&subjects=<?php echo urlencode($artist_event_check['title']);  ?>" >Social</a>
		<?php			
				$counts_views_ae = $artist_event_check['page_views'] + 1;
				
				$sql_up = mysql_query("UPDATE artist_event SET page_views='".$counts_views_ae."' WHERE id='".$artist_event_check['id']."'");
				
				$change_popular = $popular_obj->updateviews($counts_views_ae,'artist_event',$artist_event_check['id']);
				
				/* if($artist_event_check['page_views']!=0)
				{
				?>
					<p title="The amount of times this page has been viewed.">Page Views: <?php echo $artist_event_check['page_views']; ?></p>
				<?php
				} */
		?>
			</div>
		<?php
			}
			
			if($community_event_check!=0)
			{
				$te_count = 0;
				$tv_count = 0;
				
				if(!empty($community_event_check['creators_info']) && $community_event_check['creators_info']!="")
				{
					$a_eve = mysql_query("SELECT * FROM artist_event WHERE creators_info='".$community_event_check['creators_info']."' AND del_status=0 AND active=0");
					if($a_eve!="" && $a_eve!=Null)
					{
						$ae_count = mysql_num_rows($a_eve);
					}
					
					$c_eve = mysql_query("SELECT * FROM community_event WHERE creators_info='".$community_event_check['creators_info']."' AND del_status=0 AND active=0");
					if($c_eve!="" && $c_eve!=Null)
					{
						$ce_count = mysql_num_rows($c_eve);
					}
					
					$te_count = $ae_count + $ce_count;
					
					/* if($te_count>0)
					{
				?>
						<p>Events by Promoter: <?php echo $te_count; ?></p>
				<?php
					} */
				}
				
				if(!empty($community_event_check['venue_name']) && $community_event_check['venue_name']!="")
				{
					$a_ven = mysql_query("SELECT * FROM artist_event WHERE venue_name='".$community_event_check['venue_name']."' AND del_status=0 AND active=0");
					if($a_ven!="" && $a_ven!=Null)
					{
						$av_count = mysql_num_rows($a_ven);
					}
					
					$c_ven = mysql_query("SELECT * FROM community_event WHERE venue_name='".$community_event_check['venue_name']."' AND del_status=0 AND active=0");
					if($c_ven!="" && $c_ven!=Null)
					{
						$cv_count = mysql_num_rows($c_ven);
					}
					
					$tv_count = $av_count + $cv_count;
					
					/* if($tv_count>0)
					{
				?>
						<p> Events by Venue: <?php echo $tv_count; ?> </p>
				<?php
					} */
				}
				
				?>
				<div id="profileButtons">
					<a href="search.php?type_dis=event&pritype_dis=<?php echo $get_type_dis['type_id']; ?>&subtype_dis=<?php echo $get_subtype_dis['subtype_id']; ?>" title="Search for other profiles within the same general categories.">Discover</a>
						
					<a class="fancybox fancybox.ajax" id="statistics_share" href="statistics_profs.php?views=<?php echo $community_event_check['page_views']; ?>&ebp=<?php echo $te_count; ?>&ebv=<?php echo $tv_count; ?>&type=community_event" title="View stats on this profile page.">Stats</a>
				</div>
				
			<div id="profileButtons">
				<a class="fancybox fancybox.ajax" title="Follow this profiles social networks and/or share this profile with your social networks." id="social_share" href="share_social.php?type=community_event&match=<?php echo $match; ?>&fb=<?php echo $community_event_check['fb_share']; ?>&twit_share=<?php echo $community_event_check['twit_share']; ?>&gplus_share=<?php echo $community_event_check['gplus_share']; ?>&tubm_share=<?php echo $community_event_check['tubm_share']; ?>&stbu_share=<?php echo $community_event_check['stbu_share']; ?>&pin_share=<?php echo $community_event_check['pin_share']; ?>&you_share=<?php echo $community_event_check['you_share']; ?>&vimeo_share=<?php echo $community_event_check['vimeo_share']; ?>&sdcl_share=<?php echo $community_event_check['sdcl_share']; ?>&ints_share=<?php echo $community_event_check['ints_share']; ?>&subjects=<?php echo urlencode($community_event_check['title']);  ?>" >Social</a>
		<?php
				$counts_views_ce = $community_event_check['page_views'] + 1;
				
				$sql_up = mysql_query("UPDATE community_event SET page_views='".$counts_views_ce."' WHERE id='".$community_event_check['id']."'");
				
				$change_popular = $popular_obj->updateviews($counts_views_ce,'community_event',$community_event_check['id']);
				
				/* if($community_event_check['page_views']!=0)
				{
				?>
					<p title="The amount of times this page has been viewed.">Page Views: <?php echo $community_event_check['page_views']; ?></p>
				<?php
				} */
		?>
			</div>
		<?php
			}
			
			?>
			  <!--<p>Tickets Remaining: N/A</p>-->
			
			<div id="profileButtons">
<?php
			$ret_ans = "absent";
			if(isset($_SESSION))
			{
				if(isset($_SESSION['login_email']))
				{
					if($_SESSION['login_email']!="")
					{
						if($artist_event_check !=0)
						{
							$ret_ans = $display->email_subscription_schek($_SESSION['login_email'],$artist_event_check['artist_id'].'_'.$artist_event_check['id'],'artist_event');
						}
						elseif($community_event_check !=0)
						{
							$ret_ans = $display->email_subscription_schek($_SESSION['login_email'],$community_event_check['community_id'].'_'.$community_event_check['id'],'community_event');
						}
					}
				}
			}
			
			if($ret_ans=='present')
			{
?>
				<a style="background: url('/images/profile/btn_over.gif') no-repeat scroll 0 0 rgba(0, 0, 0, 0);" title="Follow this profile to receive updates from them via your feed and/or email." href="javascript:void(0);" onclick="return login_subscribed_event();">Following</a>
<?php
			}
			else
			{
?>
				<a title="Follow this profile to receive updates from them via your feed and/or email." href="javascript:void(0);" onclick="return login_subscription_event();">Follow</a>
<?php
			}
?>
			<a href="email_subscription.php" id="email_subscription_eve" class="fancybox fancybox.ajax" title="Follow this profile to receive updates from them via your feed and/or email." style="display:none;">Follow</a>
			<a  href="email_subscribed.php" id="email_subscribed_eve" class="fancybox fancybox.ajax" title="Follow this profile to receive updates from them via your feed and/or email." style="display:none;">Follow</a>
			<!--<a href="#">Tickets</a>--> 
			<?php
			if($artist_event_check!=0)
			{
				if(!empty($artist_event_check['creators_info']) && $artist_event_check['creators_info']!="")
				{
					$exp_urls = explode('|',$artist_event_check['creators_info']);
					if($exp_urls[0]=='general_artist')
					{
						$sql_query = mysql_query("SELECT * FROM general_artist WHERE artist_id='".$exp_urls[1]."' AND active=0 AND status=0 AND del_status=0");
					}
					
					if($exp_urls[0]=='general_community')
					{
						$sql_query = mysql_query("SELECT * FROM general_community WHERE community_id='".$exp_urls[1]."' AND active=0 AND status=0 AND del_status=0");
					}
					
					if($exp_urls[0]=='artist_project' || $exp_urls[0]=='community_project')
					{
						$sql_query = mysql_query("SELECT * FROM $exp_urls[0] WHERE id='".$exp_urls[1]."' AND active=0 AND del_status=0");
					}
					if(isset($sql_query))
					{
						if($sql_query!="" && $sql_query!=Null)
						{
							if(mysql_num_rows($sql_query)>0)
							{
								$ans = mysql_fetch_assoc($sql_query);
								if($ans['profile_url']!="" && !empty($ans['profile_url']))
								{
					?>
									<a title="Go to the profile page for the promoter of the event." href="<?php echo $ans['profile_url']; ?>">Promoter</a>
					<?php
								}
							}
						}
					}
				}
			}
			
			if($community_event_check!=0)
			{
				if(!empty($community_event_check['creators_info']) && $community_event_check['creators_info']!="")
				{
					$exp_urls = explode('|',$community_event_check['creators_info']);
					if($exp_urls[0]=='general_artist')
					{
						$sql_query = mysql_query("SELECT * FROM general_artist WHERE artist_id='".$exp_urls[1]."' AND active=0 AND status=0 AND del_status=0");
					}
					
					if($exp_urls[0]=='general_community')
					{
						$sql_query = mysql_query("SELECT * FROM general_community WHERE community_id='".$exp_urls[1]."' AND active=0 AND status=0 AND del_status=0");
					}
					
					if($exp_urls[0]=='artist_project' || $exp_urls[0]=='community_project')
					{
						$sql_query = mysql_query("SELECT * FROM $exp_urls[0] WHERE id='".$exp_urls[1]."' AND active=0 AND del_status=0");
					}
					
					if(isset($sql_query))
					{
						if($sql_query!="" && $sql_query!=Null)
						{
							if(mysql_num_rows($sql_query)>0)
							{
								$ans = mysql_fetch_assoc($sql_query);
								if($ans['profile_url']!="" && !empty($ans['profile_url']))
								{
					?>
									<a title="Go to the profile page for the promoter of the event." href="<?php echo $ans['profile_url']; ?>">Promoter</a>
					<?php
								}
							}
						}
					}
				}
			}
			?>
			</div>
		</div>
	<?php
	}
	 if($community_project_check!=0 || $artist_project_check!=0)
	{
		//if($artist_check !=0 || $artist_project_check!=0)
		//{
		?>
			<div id="profileStatsBlock">
			<?php
			if($artist_project_check!=0)
			{
				$asc_id = $artist_project_check['artist_id'].'_'.$artist_project_check['artist_id'].'~artist_project';
				$subscribers = $all_stats->get__subcs($asc_id);
		?>
			<div id="profileButtons">
					<a href="search.php?type_dis=project&pritype_dis=<?php echo $get_type_dis['type_id']; ?>&subtype_dis=<?php echo $get_subtype_dis['subtype_id']; ?>" title="Search for other profiles within the same general categories.">Discover</a>
					
				<?php
					if(!empty($artist_project_check['homepage']))
					{
						if(strpos($artist_project_check['homepage'],':')>0)
						{
					?>
							<a title="Go to the official website for this profile." href="<?php echo $artist_project_check['homepage']; ?>" target="_blank">Website</a>
					<?php
						}
						else
						{
					?>
							<a title="Go to the official website for this profile." href="//<?php echo $artist_project_check['homepage']; ?>" target="_blank">Website</a>
					<?php	
						}
					}		
				?>	
				</div>
				
				<div id="profileButtons">
					<a class="fancybox fancybox.ajax" title="Follow this profiles social networks and/or share this profile with your social networks." id="social_share" href="share_social.php?type=artist_project&match=<?php echo $match; ?>&fb=<?php echo $artist_project_check['fb_share']; ?>&twit_share=<?php echo $artist_project_check['twit_share']; ?>&gplus_share=<?php echo $artist_project_check['gplus_share']; ?>&tubm_share=<?php echo $artist_project_check['tubm_share']; ?>&stbu_share=<?php echo $artist_project_check['stbu_share']; ?>&pin_share=<?php echo $artist_project_check['pin_share']; ?>&you_share=<?php echo $artist_project_check['you_share']; ?>&vimeo_share=<?php echo $artist_project_check['vimeo_share']; ?>&sdcl_share=<?php echo $artist_project_check['sdcl_share']; ?>&ints_share=<?php echo $artist_project_check['ints_share']; ?>&subjects=<?php echo urlencode($artist_project_check['title']);  ?>" >Social</a>
		<?php
				
					if($imports!=1)
					{
				?>
					
						<a title="Send a message to the creator of this page." href="javascript:void(0);" onclick="shw_messages()">Message</a>
				<?php
					}
					
					if($imports!=1)
					{
						if($artist_project_check !=0)
						{
				?>
							<a title="Send a message to the creator of this page." href="message.php?artist_pro&urls=<?php echo $match; ?>" id="message" class="fancybox fancybox.ajax" style="display:none;">Message</a>
					<?php
						}
						elseif($community_project_check!=0)
						{
					?>
							<a title="Send a message to the creator of this page." href="message.php?community_pro&urls=<?php echo $match; ?>" id="message" class="fancybox fancybox.ajax" style="display:none;">Message</a>
					<?php
						}
					}
				
				$count_vies = $artist_project_check['page_views'] + 1;
				$sql_views = mysql_query("UPDATE artist_project SET page_views='".$count_vies."' WHERE id='".$artist_project_check['id']."'");
				
				$change_popular = $popular_obj->updateviews($count_vies,'artist_project',$artist_project_check['id']);
				
				/* if($artist_project_check['page_views']!=0)
				{
				?>
					<p title="The amount of times this page has been viewed.">Page Views: <?php echo $artist_project_check['page_views']; ?></p>
				<?php
				} */
		?>
			</div>
		<?php
			}
			
			if($community_project_check!=0)
			{
				$asc_id = $community_project_check['community_id'].'_'.$community_project_check['community_id'].'~community_project';
				
				$subscribers = $all_stats->get__subcs($asc_id);
		?>
			<div id="profileButtons">
					<a href="search.php?type_dis=project&pritype_dis=<?php echo $get_type_dis['type_id']; ?>&subtype_dis=<?php echo $get_subtype_dis['subtype_id']; ?>" title="Search for other profiles within the same general categories.">Discover</a>
						
				<?php				
					if(!empty($community_project_check['homepage']))
					{
						if(strpos($community_project_check['homepage'],':')>0)
						{
					?>
							<a title="Go to the official website for this profile." href="<?php echo $community_project_check['homepage']; ?>" target="_blank">Website</a>
					<?php
						}
						else
						{
					?>
							<a title="Go to the official website for this profile." href="//<?php echo $community_project_check['homepage']; ?>" target="_blank">Website</a>
					<?php	
						}
					}	
				?>
				</div>
				
				<div id="profileButtons">
					<a class="fancybox fancybox.ajax" title="Follow this profiles social networks and/or share this profile with your social networks." id="social_share" href="share_social.php?type=general_community&match=<?php echo $match; ?>&fb=<?php echo $community_project_check['fb_share']; ?>&twit_share=<?php echo $community_project_check['twit_share']; ?>&gplus_share=<?php echo $community_project_check['gplus_share']; ?>&tubm_share=<?php echo $community_project_check['tubm_share']; ?>&stbu_share=<?php echo $community_project_check['stbu_share']; ?>&pin_share=<?php echo $community_project_check['pin_share']; ?>&you_share=<?php echo $community_project_check['you_share']; ?>&vimeo_share=<?php echo $community_project_check['vimeo_share']; ?>&sdcl_share=<?php echo $community_project_check['sdcl_share']; ?>&ints_share=<?php echo $community_project_check['ints_share']; ?>&subjects=<?php echo urlencode($community_project_check['title']);  ?>" >Social</a>
			<?php		
					if($imports!=1)
					{
				?>
					
						<a title="Send a message to the creator of this page." href="javascript:void(0);" onclick="shw_messages()">Message</a>
				<?php
					}
					
					if($imports!=1)
					{
						if($artist_project_check !=0)
						{
				?>
							<a title="Send a message to the creator of this page." href="message.php?artist_pro&urls=<?php echo $match; ?>" id="message" class="fancybox fancybox.ajax" style="display:none;">Message</a>
					<?php
						}
						elseif($community_project_check!=0)
						{
					?>
							<a title="Send a message to the creator of this page." href="message.php?community_pro&urls=<?php echo $match; ?>" id="message" class="fancybox fancybox.ajax" style="display:none;">Message</a>
					<?php
						}
					}
				
				$count_vies_p = $community_project_check['page_views'] + 1;
				$sql_views = mysql_query("UPDATE community_project SET page_views='".$count_vies_p."' WHERE id='".$community_project_check['id']."'");
				
				$change_popular = $popular_obj->updateviews($count_vies_p,'community_project',$community_project_check['id']);
				
				/* if($community_project_check['page_views']!=0)
				{
				?>
					<p title="The amount of times this page has been viewed.">Page Views: <?php echo $community_project_check['page_views']; ?></p>
				<?php
				} */
		?>
			</div>
		<?php
			}
			$members = $all_stats->get__membs($asc_id);
			
			/* if($subscribers!='0')
			{
			?>
				<p title="The amount of users that have subscribed to this project.">Subscribers: <?php echo $subscribers; ?></p>
			<?php
			} */
			/* if($members!='0')
			{
			?>
				<p>Fans: <?php echo $members; ?></p>
			<?php
			} */
			
			/*if($get_permission['artist_homepage'] == "open")
			{
				if($getuser['homepage']!="")
				{
			  ?>
					<a href="<?php echo $getuser['homepage']; ?>"><p>Homepage</p></a>
			  <?php
				}
			}
			else if($get_permission['artist_homepage'] == "private" && $_SESSION['login_email'] == $getgeneral['email'])
			{
				if($getuser['homepage']!="")
				{
			  ?>
					<a href="<?php echo $getuser['homepage']; ?>"><p>Homepage</p></a>
			  <?php
				}
			}
			else if($get_permission['artist_homepage'] == "public" && isset($_SESSION['login_email']))
			{
				if($getuser['homepage']!="")
				{
			  ?>
					<a href="<?php echo $getuser['homepage']; ?>"><p>Homepage</p></a>
			  <?php
				}
			}
			else if($get_permission['artist_homepage'] == "members" && $find_member['expiry_date']>$date)
			{
				if($getuser['homepage']!="")
				{
			  ?>
					<a href="<?php echo $getuser['homepage']; ?>"><p>Homepage</p></a>
			  <?php
				}
			}
			else if($_SESSION['login_email'] == $getgeneral['email'])
			{
				if($getuser['homepage']!="")
				{
			  ?>
					<a href="<?php echo $getuser['homepage']; ?>"><p>Homepage</p></a>
			  <?php
				}
			}
		}
		else if($community_check !=0 || $community_project_check !=0)
		{
			if($get_permission['community_homepage'] == "open")
			{
				if($getuser['homepage']!="")
				{
			  ?>
					<a href="<?php echo $getuser['homepage']; ?>"><p>Homepage</p></a>
			  <?php
				}
			}
			else if($get_permission['community_homepage'] == "private" && $_SESSION['login_email'] == $getgeneral['email'])
			{
				if($getuser['homepage']!="")
				{
			  ?>
					<a href="<?php echo $getuser['homepage']; ?>"><p>Homepage</p></a>
			  <?php
				}
			}
			else if($get_permission['community_homepage'] == "public" && isset($_SESSION['login_email']))
			{
				if($getuser['homepage']!="")
				{
			  ?>
					<a href="<?php echo $getuser['homepage']; ?>"><p>Homepage</p></a>
			  <?php
				}
			}
			else if($get_permission['community_homepage'] == "members" && $find_member['expiry_date']>$date)
			{
				if($getuser['homepage']!="")
				{
			  ?>
					<a href="<?php echo $getuser['homepage']; ?>"><p>Homepage</p></a>
			  <?php
				}
			}
			else if($_SESSION['login_email'] == $getgeneral['email'])
			{
				if($getuser['homepage']!="")
				{
			  ?>
					<a href="<?php echo $getuser['homepage']; ?>"><p>Homepage</p></a>
			  <?php
				}
			}
		}*/
		//echo $_SERVER['REQUEST_URI'];
	?>
			<?php $data = explode("/",$_SERVER['REQUEST_URI']); //echo $data[1]; ?>
				<div id="profileButtons">
				<?php
				/* if($find_member != null && $find_member['general_user_id'] != null && ($find_member['expiry_date'] >$date || $find_member['lifetime']==1))
				{
					if($getuser['type']=="for_sale") {
					include_once("findproject_sale.php");
					if(isset($artist_project_check) && $artist_project_check!=0){
						$match_id = $artist_project_check['id']."~art";
					}
					if(isset($community_project_check) && $community_project_check!=0){
						$match_id = $community_project_check['id']."~com";
					}
					$stats = 0;
					if($_SESSION['login_email']!=NULL)
					{
						if(isset($artist_project_check) && $artist_project_check!=0)
						{						
							$stats = $display->chkdownloaded_pro($artist_project_check['id'],$_SESSION['login_id']);
						}
						if(isset($community_project_check) && $community_project_check!=0)
						{
							$stats = $display->chkdownloaded_pro($community_project_check['id'],$_SESSION['login_id']);
						}
					}
					
					if($stats==1)
					{
						$free_down_project_rel = "yes";
					}
					else
					{
						for($pro_sale=0;$pro_sale<=count($final_prosale_arrs);$pro_sale++)
						{
							if($final_prosale_arrs[$pro_sale]['id']==$match_id){
								$free_down_project_rel = "yes";
							}
							else{
								if($free_down_project_rel =="" || $free_down_project_rel =="no"){
									$free_down_project_rel = "no";
								}
							}
						}
					}
					?>
					<a  title="Purchase a fan club membership to this projects exclusive media." href="fan_club.php?data=<?php echo $data[1];?>" id="fan_club_pro" class="fancybox fancybox.ajax" style="display:none;" ><?php if($getuser['type']=="fan_club") { echo "Fan Club"; } elseif($getuser['type']=="for_sale") { echo "Buy Now"; } elseif($getuser['type']=="free_club") { echo "Download"; } ?></a>
					<?php
					if($getuser['type']=="for_sale" && $free_down_project_rel=="yes"){
						if($stats==1)
						{
					?>
							<a title="Purchase this projects media." href="javascript:void(0);" onClick="download_directly_project('<?php echo $getuser['sale_song'];?>','<?php echo $getuser['title'];?>','<?php echo $getuser['creator'];?>')">Download</a>
					<?php
						}
						else
						{
					?>
							<a title="Purchase this projects media." href="javascript:void(0);" onClick="download_directly_project('<?php echo $getuser['sale_song'];?>','<?php echo $getuser['title'];?>','<?php echo $getuser['creator'];?>')">Download</a>
					<?php		
						}
					}else{
					?>
					<a  title="<?php if($getuser['type']=="fan_club") { echo "Purchase a fan club membership to this users exclusive media."; } elseif($getuser['type']=="for_sale") { echo "Purchase this projects media."; } ?>" href="javascript:void(0);" onClick="<?php if($getuser['type']=="fan_club") { echo "fancy_login()"; } elseif($getuser['type']=="for_sale") { echo "click_fancy_login_down()"; } ?>"><?php if($getuser['type']=="fan_club") { echo "Fan Club"; } elseif($getuser['type']=="for_sale") { echo "Buy Now"; } ?></a>
					<?php }
					
					}
					
					if($getuser['type']=="free_club") {
					include_once("findproject_sale.php");
					if(isset($artist_project_check) && $artist_project_check!=0){
						$match_id = $artist_project_check['id']."~art";
					}
					if(isset($community_project_check) && $community_project_check!=0){
						$match_id = $community_project_check['id']."~com";
					}
					$stats = 0;
					if($_SESSION['login_email']!=NULL)
					{
						if(isset($artist_project_check) && $artist_project_check!=0)
						{						
							$stats = $display->chkdownloaded_pro($artist_project_check['id'],$_SESSION['login_id']);
						}
						if(isset($community_project_check) && $community_project_check!=0)
						{
							$stats = $display->chkdownloaded_pro($community_project_check['id'],$_SESSION['login_id']);
						}
					}
					
					if($stats==1)
					{
						$free_down_project_rel = "yes";
					}
					else
					{
						for($pro_sale=0;$pro_sale<=count($final_prosale_arrs);$pro_sale++)
						{
							if($final_prosale_arrs[$pro_sale]['id']==$match_id){
								$free_down_project_rel = "yes";
							}
							else{
								if($free_down_project_rel =="" || $free_down_project_rel =="no"){
									$free_down_project_rel = "no";
								}
							}
						}
					}
					?>
					<a  title="Purchase a fan club membership to this projects exclusive media." href="fan_club.php?data=<?php echo $data[1];?>" id="fan_club_pro" class="fancybox fancybox.ajax" style="display:none;" ><?php if($getuser['type']=="fan_club") { echo "Fan Club"; } elseif($getuser['type']=="for_sale") { echo "Buy Now"; } ?></a>
					<?php
					if($free_down_project_rel=="yes"){
						if($stats==1)
						{
					?>
							<a title="Purchase this projects media." href="javascript:void(0);" onClick="download_directly_project('<?php echo $getuser['free_club_song'];?>','<?php echo $getuser['title'];?>','<?php echo $getuser['creator'];?>')">Download</a>
					<?php
						}
						else
						{
					?>
							<a title="Purchase this projects media." href="javascript:void(0);" onClick="download_directly_project('<?php echo $getuser['free_club_song'];?>','<?php echo $getuser['title'];?>','<?php echo $getuser['creator'];?>')">Download</a>
					<?php		
						}
					}else{
					?>
					<a  title="<?php if($getuser['type']=="fan_club") { echo "Purchase a fan club membership to this users exclusive media."; } elseif($getuser['type']=="for_sale") { echo "Purchase this projects media."; } ?>" href="javascript:void(0);" onClick="<?php if($getuser['type']=="fan_club") { echo "fancy_login()"; } elseif($getuser['type']=="for_sale") { echo "click_fancy_login_down()"; } elseif($getuser['type']=="free_club") { if($getuser['ask_tip']=='0') { echo "download_directly_project('".$getuser['free_club_song']."','".$getuser['title']."','".$getuser['creator']."')"; } else { echo "ask_to_donate_pro('".str_replace("'","\'",$getuser['title'])."','".str_replace("'","\'",$getuser['creator'])."','". $getuser['free_club_song']."','fan_club_pro')"; } } ?>"><?php if($getuser['type']=="fan_club") { echo "Fan Club"; } elseif($getuser['type']=="for_sale") { echo "Buy Now"; } elseif($getuser['type']=="free_club") { echo "Download"; } ?></a>
					<?php }
					}
				} */
				
				$ret_ans = "absent";
				if(isset($_SESSION))
				{
					if(isset($_SESSION['login_email']))
					{
						if($_SESSION['login_email']!="")
						{
							if($artist_project_check !=0)
							{
								$ret_ans = $display->email_subscription_schek($_SESSION['login_email'],$artist_project_check['artist_id'].'_'.$artist_project_check['id'],'artist_project');
							}
							elseif($community_project_check !=0)
							{
								$ret_ans = $display->email_subscription_schek($_SESSION['login_email'],$community_project_check['community_id'].'_'.$community_project_check['id'],'community_project');
							}
						}
					}
				}
				
				if($ret_ans=='present')
				{
?>
					<div id="profileButtons_acts">
						<a title="Follow this profile to receive updates from them via your feed and/or email." href="javascript:void(0);" onclick="return login_subscribed();">Following</a>
					</div>
<?php
				}
				else
				{
?>
					<a title="Follow this profile to receive updates from them via your feed and/or email." href="javascript:void(0);" onclick="return login_subscription();">Follow</a>
<?php				
				}
?>
				<a  href="email_subscription.php" id="email_subscription" class="fancybox fancybox.ajax" title="Follow this profile to receive updates from them via your feed and/or email." style="display:none;">Follow</a>
				<a  href="email_subscribed.php" id="email_subscribed" class="fancybox fancybox.ajax" title="Follow this profile to receive updates from them via your feed and/or email." style="display:none;">Follow</a>
				<?php
				/* if($find_member != null && $find_member['general_user_id'] != null && ($find_member['expiry_date'] >$date || $find_member['lifetime']==1))
				{
					if($getuser['type']=="fan_club") {
					include_once("findproject_sale.php");
					if(isset($artist_project_check) && $artist_project_check!=0){
						$match_id = $artist_project_check['id']."~art";
					}
					if(isset($community_project_check) && $community_project_check!=0){
						$match_id = $community_project_check['id']."~com";
					}
					$stats = 0;
					if($_SESSION['login_email']!=NULL)
					{
						if(isset($artist_project_check) && $artist_project_check!=0)
						{						
							$stats = $display->chkdownloaded_pro($artist_project_check['id'],$_SESSION['login_id']);
						}
						if(isset($community_project_check) && $community_project_check!=0)
						{
							$stats = $display->chkdownloaded_pro($community_project_check['id'],$_SESSION['login_id']);
						}
					}
					
					if($stats==1)
					{
						$free_down_project_rel = "yes";
					}
					else
					{
						for($pro_sale=0;$pro_sale<=count($final_prosale_arrs);$pro_sale++)
						{
							if($final_prosale_arrs[$pro_sale]['id']==$match_id){
								$free_down_project_rel = "yes";
							}
							else{
								if($free_down_project_rel =="" || $free_down_project_rel =="no"){
									$free_down_project_rel = "no";
								}
							}
						}
					}
					?>
					<a  title="Purchase a fan club membership to this projects exclusive media." href="fan_club.php?data=<?php echo $data[1];?>" id="fan_club_pro" class="fancybox fancybox.ajax" style="display:none;" ><?php if($getuser['type']=="fan_club") { echo "Fan Club"; } elseif($getuser['type']=="for_sale") { echo "Buy Now"; } elseif($getuser['type']=="free_club") { echo "Download"; } ?></a>
					<?php
					if($free_down_project_rel=="yes"){
						if($stats==1)
						{
					?>
							<a title="Purchase this projects media." href="javascript:void(0);" onClick="download_directly_project('<?php echo $getuser['fan_song'];?>','<?php echo $getuser['title'];?>','<?php echo $getuser['creator'];?>')">Download</a>
					<?php
						}
						else
						{
					?>
							<a title="Purchase this projects media." href="javascript:void(0);" onClick="download_directly_project('<?php echo $getuser['fan_song'];?>','<?php echo $getuser['title'];?>','<?php echo $getuser['creator'];?>')">Download</a>
					<?php		
						}
					}else{
					?>
					<a  title="<?php if($getuser['type']=="fan_club") { echo "Purchase a fan club membership to this users exclusive media."; } elseif($getuser['type']=="for_sale") { echo "Purchase this projects media."; } ?>" href="javascript:void(0);" onClick="<?php if($getuser['type']=="fan_club") { echo "fancy_login()"; } elseif($getuser['type']=="for_sale") { echo "click_fancy_login_down()"; } ?>"><?php if($getuser['type']=="fan_club") { echo "Fan Club"; } elseif($getuser['type']=="for_sale") { echo "Buy Now"; } elseif($getuser['type']=="free_club") { echo "Download"; } ?></a>
					<?php }
					
					}
				} */
				if($artist_project_check !=0)
				{
				?>
					<a class="fancybox fancybox.ajax" id="statistics_share" href="statistics_profs.php?views=<?php echo $artist_project_check['page_views']; ?>&subs=<?php echo $subscribers; ?>&type=artist_project" title="View stats on this profile page.">Stats</a>
				<?php
				}
				elseif($community_project_check !=0)
				{
				?>
					<a class="fancybox fancybox.ajax" id="statistics_share" href="statistics_profs.php?views=<?php echo $community_project_check['page_views']; ?>&subs=<?php echo $subscribers; ?>&type=community_project" title="View stats on this profile page.">Stats</a>
				<?php
				}					
				?>
			</div>
		</div>
	<?php
	}
	 ?>
    </div>
	
    <div id="profileTabs">
      <ul>
	  
	  <?php 
	  /*if($artist_event_check !=0 || $community_event_check !=0 || $community_project_check !=0 || $artist_project_check !=0)
	  {
			 if($get_media_Info['artist_view_selected']!="")
			 {
			 ?>  
				 <li><a href="javascript:tabSix();" class="profileTab6" id="tabSix">Artist</a></li>
			 <?php
			 }
	  }*/	
	if($get_media_Info['taggedvideos']!="")
	{
		$exp_vids = explode(',',$get_media_Info['taggedvideos']);
		$tab_chk = 0;
		$val_chk = 0;
		for($v_e=0;$v_e<count($exp_vids);$v_e++)
		{
			if($exp_vids[$v_e]!="")
			{
				$val_chk = $display->check_vids($exp_vids[$v_e]);
			}
			if(isset($val_chk))
			{
				if($val_chk==1)
				{
					$tab_chk = 1;
					break;
				}
				else
				{
					continue;
				}
			}
		}
		if($tab_chk==1)
		{
?>
			<li><a href="javascript:tabOne();" class="profileTab1" id="tabOne">Videos</a></li>
<?php
		}
	}
	if($get_media_Info['tagged_videos']!="")
	{
		$exp_vids = explode(',',$get_media_Info['tagged_videos']);
		$tab_chk = 0;
		$val_chk = 0;
		
		for($v_e=0;$v_e<count($exp_vids);$v_e++)
		{
			if($exp_vids[$v_e]!="")
			{
				$val_chk = $display->check_vids($exp_vids[$v_e]);
			}
			if(isset($val_chk))
			{
				if($val_chk==1)
				{
					$tab_chk = 1;
					break;
				}
				else
				{
					continue;
				}
			}
		}
		if($tab_chk==1)
		{
?>
			<li><a href="javascript:tabOne();" class="profileTab1" id="tabOne">Videos</a></li>
<?php
		}
	}
	if($get_media_Info['taggedsongs']!="")
	{
		$exp_vids = explode(',',$get_media_Info['taggedsongs']);
		$tab_chk = 0;
		$val_chk = 0;
		for($v_e=0;$v_e<count($exp_vids);$v_e++)
		{
			if($exp_vids[$v_e]!="")
			{
				$val_chk = $display->check_vids($exp_vids[$v_e]);
			}
			if(isset($val_chk))
			{
				if($val_chk==1)
				{
					$tab_chk = 1;
					break;
				}
				else
				{
					continue;
				}
			}
		}
		if($tab_chk==1)
		{
?>
			<li><a href="javascript:tabTwo();" class="profileTab2" id="tabTwo">Songs</a></li>
<?php
		}
	}
	if($get_media_Info['tagged_songs']!="")
	{
		$exp_vids = explode(',',$get_media_Info['tagged_songs']);
		$tab_chk = 0;
		$val_chk = 0;
		for($v_e=0;$v_e<count($exp_vids);$v_e++)
		{
			if($exp_vids[$v_e]!="")
			{
				$val_chk = $display->check_vids($exp_vids[$v_e]);
			}
			if(isset($val_chk))
			{
				if($val_chk==1)
				{
					$tab_chk = 1;
					break;
				}
				else
				{
					continue;
				}
			}
			
		}
		if($tab_chk==1)
		{
?>
			<li><a href="javascript:tabTwo();" class="profileTab2" id="tabTwo">Songs</a></li>
<?php
		}
	}
	if($get_media_Info['taggedgalleries']!="")
	{
		$exp_vids = explode(',',$get_media_Info['taggedgalleries']);
		$tab_chk = 0;
		$val_chk = 0;
		for($v_e=0;$v_e<count($exp_vids);$v_e++)
		{
			if($exp_vids[$v_e]!="")
			{
				$val_chk = $display->check_vids($exp_vids[$v_e]);
			}
			if(isset($val_chk))
			{
				if($val_chk==1)
				{
					$tab_chk = 1;
					break;
				}
				else
				{
					continue;
				}
			}
		}
		if($tab_chk==1)
		{
?>
			<li><a href="javascript:tabThree();" class="profileTab3" id="tabThree">Galleries</a></li>
<?php
		}
	}
	if($get_media_Info['tagged_galleries']!="")
	{
		$exp_vids = explode(',',$get_media_Info['tagged_galleries']);
		$tab_chk = 0;
		$val_chk = 0;
		for($v_e=0;$v_e<count($exp_vids);$v_e++)
		{
			if($exp_vids[$v_e]!="")
			{
				$val_chk = $display->check_vids($exp_vids[$v_e]);
			}
			if(isset($val_chk))
			{
				if($val_chk==1)
				{
					$tab_chk = 1;
					break;
				}
				else
				{
					continue;
				}
			}
		}
		if($tab_chk==1)
		{
?>
			<li><a href="javascript:tabThree();" class="profileTab3" id="tabThree">Galleries</a></li>
<?php
		}
	}
	if($artist_project_check !=0 || $community_project_check !=0)
	{
		if(($Find_Artist == "artist" && !empty($all_artist)) || (!empty($getuser['creators_info']) && !empty($getuser['creator'])))
		{
		?>
		<li><a href="javascript:tabFour();" class="profileTab4" id="tabFour">Artists</a></li>
		<?php
		}
	}
	elseif($artist_event_check !=0 || $community_event_check !=0)
	{
		if(($Find_Artist == "artist" && !empty($all_artist)) || (!empty($getuser['creators_info']) && !empty($getuser['creator'])))
		{
		?>
		<li><a href="javascript:tabFour();" class="profileTab4" id="tabFour">Artists</a></li>
		<?php
		}
	}
	 
	  //if($artist_event_check==0 && $community_event_check==0 && $community_project_check==0 && $artist_project_check==0)
		//{
		
		if($artist_check!=0 || $community_check!=0)
		{
			if(isset($get_project[0])){
			if($get_project[0]!="" && $get_project[0]!=NULL)
			{
				if(mysql_num_rows($get_project[0])> 0 || $getuser['tagged_projects'] !=null || $getuser['taggedprojects'] !=null)
				{
					if($artist_check!=0)
					{
						if(!empty($get_permission))
						{
							if($get_permission['artist_project'] == "open")
							{
								 ?>
									<li><a href="javascript:tabFour();" class="profileTab4" id="tabFour">Projects</a></li>
								  <?php
							}
							else if($get_permission['artist_project'] == "private" && $_SESSION['login_email'] == $getgeneral['email'])
							{
								?>
									<li><a href="javascript:tabFour();" class="profileTab4" id="tabFour">Projects</a></li>
								  <?php
							}
							else if($get_permission['artist_project'] == "public" && isset($_SESSION['login_email']))
							{
								?>
									<li><a href="javascript:tabFour();" class="profileTab4" id="tabFour">Projects</a></li>
								  <?php
							}
							else if($get_permission['artist_project'] == "members" && ($find_member['expiry_date']>$date || $find_member['lifetime']==1))
							{
								?>
									<li><a href="javascript:tabFour();" class="profileTab4" id="tabFour">Projects</a></li>
								  <?php
							}
							else if($_SESSION['login_email'] == $getgeneral['email'])
							{
								?>
									<li><a href="javascript:tabFour();" class="profileTab4" id="tabFour">Projects</a></li>
								  <?php
							}
						}
						else
						{
							?>
							<li><a href="javascript:tabFour();" class="profileTab4" id="tabFour">Projects</a></li>
						  <?php
						}
					}
					else if($community_check!=0)
					{	
						if(!empty($get_permission))
						{
							if($get_permission['community_project'] == "open")
							{
								?>
									<li><a href="javascript:tabFour();" class="profileTab4" id="tabFour">Projects</a></li>
								  <?php
							}
							else if($get_permission['community_project'] == "private" && $_SESSION['login_email'] == $getgeneral['email'])
							{
								?>
									<li><a href="javascript:tabFour();" class="profileTab4" id="tabFour">Projects</a></li>
								  <?php
							}
							else if($get_permission['community_project'] == "public" && isset($_SESSION['login_email']))
							{
								?>
									<li><a href="javascript:tabFour();" class="profileTab4" id="tabFour">Projects</a></li>
								  <?php
							}
							else if($get_permission['community_project'] == "members" && ($find_member['expiry_date']>$date || $find_member['lifetime']==1))
							{
								?>
									<li><a href="javascript:tabFour();" class="profileTab4" id="tabFour">Projects</a></li>
								  <?php
							}
							else if($_SESSION['login_email'] == $getgeneral['email'])
							{
								?>
									<li><a href="javascript:tabFour();" class="profileTab4" id="tabFour">Projects</a></li>
								  <?php
							}
						}else
						{
							?>
							<li><a href="javascript:tabFour();" class="profileTab4" id="tabFour">Projects</a></li>
						  <?php
						}
					}
					else
					{
						?>
							<li><a href="javascript:tabFour();" class="profileTab4" id="tabFour">Projects</a></li>
						<?php
					}
				}
			}
			}
		}
		else
		{
			if($getuser['tagged_projects'] !=null)
			{
  ?>
				<li><a href="javascript:tabFive();" class="profileTab5" id="tabFive">Projects</a></li>
<?php
			}
		}
		  //if((isset($get_upcoming_event[0]) && $get_upcoming_event[0]!="" ) || (isset($get_recorded_event[0]) && $get_recorded_event[0]!="") || (mysql_num_rows($get_upcoming_event)>0 || mysql_num_rows($get_recorded_event)>0))
		  if(isset($get_upcoming_event) || isset($get_recorded_event))
		  {
				if($artist_check!=0)
				{	
					if(!empty($get_permission))
					{
						if($get_permission['artist_event'] == "open")
						{
							 ?>
								<li><a href="javascript:tabFive();" class="profileTab5" id="tabFive">Events</a></li>
							  <?php
						}
						else if($get_permission['artist_event'] == "private" && $_SESSION['login_email'] == $getgeneral['email'])
						{
							?>
								<li><a href="javascript:tabFive();" class="profileTab5" id="tabFive">Events</a></li>
							  <?php
						}
						else if($get_permission['artist_event'] == "public" && isset($_SESSION['login_email']))
						{
							?>
								<li><a href="javascript:tabFive();" class="profileTab5" id="tabFive">Events</a></li>
							  <?php
						}
						else if($get_permission['artist_event'] == "members" && ($find_member['expiry_date']>$date || $find_member['lifetime']==1))
						{
							?>
								<li><a href="javascript:tabFive();" class="profileTab5" id="tabFive">Events</a></li>
							  <?php
						}
						else if($_SESSION['login_email'] == $getgeneral['email'])
						{
							?>
								<li><a href="javascript:tabFive();" class="profileTab5" id="tabFive">Events</a></li>
							  <?php
						}
					}else{
					?>
						<li><a href="javascript:tabFive();" class="profileTab5" id="tabFive">Events</a></li>
					  <?php
					}
				}
				else if($community_check!=0)
				{
					if(!empty($get_permission)){
						if($get_permission['community_event'] == "open")
						{
							?>
								<li><a href="javascript:tabFive();" class="profileTab5" id="tabFive">Events</a></li>
							  <?php
						}
						else if($get_permission['community_event'] == "private" && $_SESSION['login_email'] == $getgeneral['email'])
						{
							?>
								<li><a href="javascript:tabFive();" class="profileTab5" id="tabFive">Events</a></li>
							  <?php
						}
						else if($get_permission['community_event'] == "public" && isset($_SESSION['login_email']))
						{
							?>
								<li><a href="javascript:tabFive();" class="profileTab5" id="tabFive">Events</a></li>
							  <?php
						}
						else if($get_permission['community_event'] == "members" && ($find_member['expiry_date']>$date || $find_member['lifetime']==1))
						{
							?>
								<li><a href="javascript:tabFive();" class="profileTab5" id="tabFive">Events</a></li>
							  <?php
						}
						else if($_SESSION['login_email'] == $getgeneral['email'])
						{
							?>
								<li><a href="javascript:tabFive();" class="profileTab5" id="tabFive">Events</a></li>
							  <?php
						}
					}else{
					?>
						<li><a href="javascript:tabFive();" class="profileTab5" id="tabFive">Events</a></li>
					  <?php
					}
				}
				else if($community_project_check!=0 || $community_event_check!=0 || $artist_project_check!=0 || $artist_event_check!=0)
				{	
					if($get_upcoming_event!=="" && $get_upcoming_event!=Null)
					{
						if(mysql_num_rows($get_upcoming_event)>0){
						?>
							<li><a href="javascript:tabSeven();" class="profileTab7" id="tabSeven">Events</a></li>
						 <?php
						}
					}
					elseif($get_recorded_event!="" && $get_recorded_event!=Null)
					{
						if(mysql_num_rows($get_recorded_event)>0){
						?>
							<li><a href="javascript:tabSeven();" class="profileTab7" id="tabSeven">Events</a></li>
						 <?php
						}
					}
				}
			}
			if($artist_check !=0 || $community_check !=0)
			{
				if($get_media_Info['artist_view_selected'] !="" || $get_media_Info['community_view_selected'] !="")
				{
					$frd_tb_hk = $friend_obj_new->get_all_friends($getgeneral['general_user_id']);
					if($frd_tb_hk!="")
					{
						if(mysql_num_rows($frd_tb_hk)>0)
						{
							//echo "1";
				?>
							<li><a href="javascript:tabSeven();" class="profileTab7" id="tabSeven">Friends</a></li>
				<?php
						}
						elseif($artist_check !=0)
						{
							$sql_com_khs = mysql_query("SELECT *,gu.general_user_id as general_user_id FROM `general_user` gu LEFT JOIN `general_community` ga on gu.community_id = ga.community_id WHERE gu.general_user_id=".$getgeneral['general_user_id']." AND gu.active=0 AND gu.delete_status=0 AND gu.status=0 AND ga.active=0 AND ga.del_status=0");
							if($sql_com_khs!="")
							{
								if(mysql_num_rows($sql_com_khs)>0)
								{
									//echo "2";
					?>
									<li><a href="javascript:tabSeven();" class="profileTab7" id="tabSeven">Friends</a></li>
					<?php
								}
							}
						}
						elseif($community_check !=0)
						{
							$sql_art_khs = mysql_query("SELECT *,gu.general_user_id as general_user_id FROM `general_user` gu LEFT JOIN `general_artist` ga on gu.artist_id = ga.artist_id WHERE gu.general_user_id=".$getgeneral['general_user_id']." AND gu.active=0 AND gu.delete_status=0 AND gu.status=0 AND ga.active=0 AND ga.del_status=0");
							if($sql_art_khs!="")
							{
								if(mysql_num_rows($sql_art_khs)>0)
								{
									//echo "3";
					?>
									<li><a href="javascript:tabSeven();" class="profileTab7" id="tabSeven">Friends</a></li>
					<?php
								}
							}
						}
					}
					elseif($artist_check !=0)
					{
						$sql_com_khs = mysql_query("SELECT *,gu.general_user_id as general_user_id FROM `general_user` gu LEFT JOIN `general_community` ga on gu.community_id = ga.community_id WHERE gu.general_user_id=".$getgeneral['general_user_id']." AND gu.active=0 AND gu.delete_status=0 AND gu.status=0 AND ga.active=0 AND ga.del_status=0");
						if($sql_com_khs!="")
						{
							if(mysql_num_rows($sql_com_khs)>0)
							{
								//echo "4";
				?>
								<li><a href="javascript:tabSeven();" class="profileTab7" id="tabSeven">Friends</a></li>
				<?php
							}
						}
					}
					elseif($community_check !=0)
					{
						$sql_art_khs = mysql_query("SELECT *,gu.general_user_id as general_user_id FROM `general_user` gu LEFT JOIN `general_artist` ga on gu.artist_id = ga.artist_id WHERE gu.general_user_id=".$getgeneral['general_user_id']." AND gu.active=0 AND gu.delete_status=0 AND gu.status=0 AND ga.active=0 AND ga.del_status=0");
						if($sql_art_khs!="")
						{
							if(mysql_num_rows($sql_art_khs)>0)
							{
								//echo "5";
				?>
								<li><a href="javascript:tabSeven();" class="profileTab7" id="tabSeven">Friends</a></li>
				<?php
							}
						}
					}
				}
				elseif($artist_check !=0)
					{
						$sql_com_khs = mysql_query("SELECT *,gu.general_user_id as general_user_id FROM `general_user` gu LEFT JOIN `general_community` ga on gu.community_id = ga.community_id WHERE gu.general_user_id=".$getgeneral['general_user_id']." AND gu.active=0 AND gu.delete_status=0 AND gu.status=0 AND ga.active=0 AND ga.del_status=0");
						if($sql_com_khs!="")
						{
							if(mysql_num_rows($sql_com_khs)>0)
							{
								//echo "6";
								$frd_tb_hk = $friend_obj_new->get_all_friends($getgeneral['general_user_id']);
								if($frd_tb_hk!="" && mysql_num_rows($frd_tb_hk)>0){
				?>
								<li><a href="javascript:tabSeven();" class="profileTab7" id="tabSeven">Friends</a></li>
				<?php
								}
							}
						}
					}
					elseif($community_check !=0)
					{
						$sql_art_khs = mysql_query("SELECT *,gu.general_user_id as general_user_id FROM `general_user` gu LEFT JOIN `general_artist` ga on gu.artist_id = ga.artist_id WHERE gu.general_user_id=".$getgeneral['general_user_id']." AND gu.active=0 AND gu.delete_status=0 AND gu.status=0 AND ga.active=0 AND ga.del_status=0");
						if($sql_art_khs!="")
						{
							if(mysql_num_rows($sql_art_khs)>0)
							{
								//echo "7";
								$frd_tb_hk = $friend_obj_new->get_all_friends($getgeneral['general_user_id']);
								if($frd_tb_hk!="" && mysql_num_rows($frd_tb_hk)>0){
				?>
								<li><a href="javascript:tabSeven();" class="profileTab7" id="tabSeven">Friends</a></li>
				<?php
								}
							}
						}
					}
			}
	
		?>
      </ul>
    </div>	
<?php

if($get_media_Info['taggedgalleries']!="" || $get_media_Info['tagged_galleries']!="")
	{
	?>
	
     <div id="projectsTab" class="hiddenBlock3">
		<!--<div id="profileFeatured">
		
			<div class="playall">
				Play All
			</div>
			<?php
				if($artist_check !=0 || $community_check !=0)
				{
			?>
					<a title="Play all of the media on this profile page." href=""><img src="../images/profile/play.png" width="35" height="23" onmouseover="this.src='../images/profile/play-over.png'" onmouseout="this.src='../images/profile/play.png'" /></a>
			<?php
				}
				elseif($artist_event_check !=0 || $community_event_check !=0)
				{
			?>
					<a title="Play all of the media on this events page." href=""><img src="../images/profile/play.png" width="35" height="23" onmouseover="this.src='../images/profile/play-over.png'" onmouseout="this.src='../images/profile/play.png'" /></a>
			<?php
				}
				elseif($artist_project_check !=0 || $community_project_check !=0)
				{
			?>
					<a title="Play all of the media on this projects page." href=""><img src="../images/profile/play.png" width="35" height="23" onmouseover="this.src='../images/profile/play-over.png'" onmouseout="this.src='../images/profile/play.png'" /></a>
			<?php			
				}
			
			if($artist_check !=0 || $community_check !=0)
			{
			?>
				<a title="Add all of the media on this profile page to your playlist without stopping your current file." href=""><img src="../images/profile/add.png" width="27" height="23" onmouseover="this.src='../images/profile/add-over.png'" onmouseout="this.src='../images/profile/add.png'" /></a>
			<?php
			}
			elseif($artist_event_check !=0 || $community_event_check !=0)
			{
			?>
				<a title="Add all of the media on this events page to your playlist without stopping your current file." href=""><img src="../images/profile/add.png" width="27" height="23" onmouseover="this.src='../images/profile/add-over.png'" onmouseout="this.src='../images/profile/add.png'" /></a>
			<?php
			}
			elseif($artist_project_check !=0 || $community_project_check !=0)
			{
			?>
				<a title="Add all of the media on this projects page to your playlist without stopping your current file." href=""><img src="../images/profile/add.png" width="27" height="23" onmouseover="this.src='../images/profile/add-over.png'" onmouseout="this.src='../images/profile/add.png'" /></a>
			<?php
			}
			?>
			
		</div>-->
		<div id="profileFeatured">
			
			<?php //echo $getuser['featured_media'].$getuser['media_id'];
			
			if($artist_check !=0 || $artist_event_check !=0 || $artist_project_check !=0)
			{
				$table_type = "artist";
			}
			if($community_check !=0 || $community_event_check !=0 || $community_project_check !=0)
			{
				$table_type = "community";
			}
			if($getuser['featured_media'] == "Gallery" && $getuser['media_id']!=0)
			{
				$find_whether_regis=$new_profile_class_obj->get_count_Gallery_at_register($getuser["$table_type".'_id'],$getuser['media_id']);
				//var_dump($find_whether_regis);
				//if($find_whether_regis["count(*)"]>1)
				if($getuser['media_id']!=0 && ($getuser['featured_media_table']=='general_community_gallery_list' || $getuser['featured_media_table']=='general_artist_gallery_list'))
				{
					$getimages=$new_profile_class_obj->get_featured_media_gallery_at_register($getuser['media_id'],$table_type);
					$feature_image_name = array();
					$feature_image_title = array();
					$feature_count = 0;
					while($get_image = mysql_fetch_assoc($getimages))
					{
						$feature_image_name[$feature_count] = $get_image['image_name'];
						$feature_image_title[$feature_count] = $get_image['image_title'];
						$feature_count++;
					}
					$feature_big_img = $feature_image_name[0];
					$featured_gal_img = implode(",", $feature_image_name);
					$featured_gal_img_title = implode(",", $feature_image_title);
					$feature_org_path = "https://reggallery.s3.amazonaws.com/";
					$feature_thum_path = "https://reggalthumb.s3.amazonaws.com/";
				}
				elseif($getuser['media_id']!=0 && $getuser['featured_media_table']=='general_media')
				{
					$getimages=$new_profile_class_obj->get_featured_media_gallery($getuser['media_id']);
					$feature_image_name = array();
					$feature_image_title = array();
					$feature_count = 0;
					while($get_image = mysql_fetch_assoc($getimages))
					{
						$feature_image_name[$feature_count] = $get_image['image_name'];
						$feature_image_title[$feature_count] = $get_image['image_title'];
						$feature_count++;
					}
					$feature_big_img = $feature_image_name[0];
					$featured_gal_img = implode(",", $feature_image_name);
					$featured_gal_img_title = implode(",", $feature_image_title);
					$feature_org_path = "https://medgallery.s3.amazonaws.com/";
					$feature_thum_path = "https://medgalthumb.s3.amazonaws.com/";
				}
			?>
				<div class="fmedia">featured Media</div>
				<?php
				if($artist_check !=0 || $community_check !=0)
				{
				?>
					<a title="Play this artists featured media." href="javascript:void(0);"><img src="../images/profile/play.png" width="35" height="23" onmouseover="this.src='../images/profile/play-over.png'" onmouseout="this.src='../images/profile/play.png'" onclick="checkload('<?php echo $feature_big_img;?>','<?php echo $featured_gal_img; ?>','<?php echo $feature_thum_path; ?>','<?php echo $feature_org_path; ?>','<?php echo $featured_gal_img_title; ?>')" /></a>
					<a title="Add this artists featured media to your playlist without stopping your current file." href="javascript:void(0);"><img src="../images/profile/add.png" width="27" height="23" onmouseover="this.src='../images/profile/add-over.png'" onmouseout="this.src='../images/profile/add.png'" /></a>
				<?php
				}
				elseif($artist_event_check !=0 || $community_event_check !=0)
				{
				?>
					<a title="Play this events featured media." href="javascript:void(0);"><img src="../images/profile/play.png" width="35" height="23" onmouseover="this.src='../images/profile/play-over.png'" onmouseout="this.src='../images/profile/play.png'" onclick="checkload('<?php echo $feature_big_img;?>','<?php echo $featured_gal_img; ?>','<?php echo $feature_thum_path; ?>','<?php echo $feature_org_path; ?>','<?php echo $featured_gal_img_title; ?>')" /></a>
					<a title="Add this events featured media to your playlist without stopping your current file." href="javascript:void(0);"><img src="../images/profile/add.png" width="27" height="23" onmouseover="this.src='../images/profile/add-over.png'" onmouseout="this.src='../images/profile/add.png'" /></a>
				<?php
				}
				elseif($artist_project_check !=0 || $community_project_check !=0)
				{
				?>
					<a title="Play this projects featured media." href="javascript:void(0);"><img src="../images/profile/play.png" width="35" height="23" onmouseover="this.src='../images/profile/play-over.png'" onmouseout="this.src='../images/profile/play.png'" onclick="checkload('<?php echo $feature_big_img;?>','<?php echo $featured_gal_img; ?>','<?php echo $feature_thum_path; ?>','<?php echo $feature_org_path; ?>','<?php echo $featured_gal_img_title; ?>')" /></a>
					<a title="Add this projects featured media to your playlist without stopping your current file." href="javascript:void(0);"><img src="../images/profile/add.png" width="27" height="23" onmouseover="this.src='../images/profile/add-over.png'" onmouseout="this.src='../images/profile/add.png'" /></a>
				<?php
				}
			
			}
			if($getuser['featured_media'] == "Song" && $getuser['media_id']!=0)
			{
			?>
				<div class="fmedia">featured Media</div>
				<?php
				if($artist_check !=0 || $community_check !=0)
				{
				?>
					<a title="Play this artists featured media." href="javascript:void(0);"><img src="../images/profile/play.png" width="35" height="23" onmouseover="this.src='../images/profile/play-over.png'" onmouseout="this.src='../images/profile/play.png'" onclick="showPlayer('a','<?php echo $getuser['media_id'];?>','play')" /></a>
					<a title="Add this artists featured media to your playlist without stopping your current file." href="javascript:void(0);"><img src="../images/profile/add.png" width="27" height="23" onmouseover="this.src='../images/profile/add-over.png'" onmouseout="this.src='../images/profile/add.png'" onclick="showPlayer('a','<?php echo $getuser['media_id'];?>','add')" /></a>
			<?php
				}
				elseif($artist_event_check !=0 || $community_event_check !=0)
				{
			?>
					<a title="Play this events featured media." href="javascript:void(0);"><img src="../images/profile/play.png" width="35" height="23" onmouseover="this.src='../images/profile/play-over.png'" onmouseout="this.src='../images/profile/play.png'" onclick="showPlayer('a','<?php echo $getuser['media_id'];?>','play')" /></a>
					<a title="Add this events featured media to your playlist without stopping your current file." href="javascript:void(0);"><img src="../images/profile/add.png" width="27" height="23" onmouseover="this.src='../images/profile/add-over.png'" onmouseout="this.src='../images/profile/add.png'" onclick="showPlayer('a','<?php echo $getuser['media_id'];?>','add')" /></a>
			<?php
				}
				elseif($artist_project_check !=0 || $community_project_check !=0)
				{
			?>
					<a title="Play this projects featured media." href="javascript:void(0);"><img src="../images/profile/play.png" width="35" height="23" onmouseover="this.src='../images/profile/play-over.png'" onmouseout="this.src='../images/profile/play.png'" onclick="showPlayer('a','<?php echo $getuser['media_id'];?>','play')" /></a>
					<a title="Add this projects featured media to your playlist without stopping your current file." href="javascript:void(0);"><img src="../images/profile/add.png" width="27" height="23" onmouseover="this.src='../images/profile/add-over.png'" onmouseout="this.src='../images/profile/add.png'" onclick="showPlayer('a','<?php echo $getuser['media_id'];?>','add')" /></a>
			<?php
				}
			}
			?>
			<?php
			if($getuser['featured_media'] == "Video" && $getuser['media_id']!=0)
			{
			?>
				<div class="fmedia">featured Media</div>
			<?php
			if($artist_check !=0 || $community_check !=0)
			{
			?>
					<a title="Play this artists featured media." href="javascript:void(0);"><img src="../images/profile/play.png" width="35" height="23" onmouseover="this.src='../images/profile/play-over.png'" onmouseout="this.src='../images/profile/play.png'" onclick="showPlayer('v','<?php echo $getuser['media_id'];?>','playlist')" /></a>
					<a title="Add this artists featured media to your playlist without stopping your current file." href="javascript:void(0);"><img src="../images/profile/add.png" width="27" height="23" onmouseover="this.src='../images/profile/add-over.png'" onmouseout="this.src='../images/profile/add.png'" onclick="showPlayer('v','<?php echo $getuser['media_id'];?>','addvi')"/></a>
			<?php
				}
				elseif($artist_event_check !=0 || $community_event_check !=0)
				{
			?>
					<a title="Play this events featured media." href="javascript:void(0);"><img src="../images/profile/play.png" width="35" height="23" onmouseover="this.src='../images/profile/play-over.png'" onmouseout="this.src='../images/profile/play.png'" onclick="showPlayer('v','<?php echo $getuser['media_id'];?>','playlist')" /></a>
					<a title="Add this events featured media to your playlist without stopping your current file." href="javascript:void(0);"><img src="../images/profile/add.png" width="27" height="23" onmouseover="this.src='../images/profile/add-over.png'" onmouseout="this.src='../images/profile/add.png'" onclick="showPlayer('v','<?php echo $getuser['media_id'];?>','addvi')"/></a>
			<?php
				}
				elseif($artist_project_check !=0 || $community_project_check !=0)
				{
			?>
					<a title="Play this projects featured media." href="javascript:void(0);"><img src="../images/profile/play.png" width="35" height="23" onmouseover="this.src='../images/profile/play-over.png'" onmouseout="this.src='../images/profile/play.png'" onclick="showPlayer('v','<?php echo $getuser['media_id'];?>','playlist')" /></a>
					<a title="Add this projects featured media to your playlist without stopping your current file." href="javascript:void(0);"><img src="../images/profile/add.png" width="27" height="23" onmouseover="this.src='../images/profile/add-over.png'" onmouseout="this.src='../images/profile/add.png'" onclick="showPlayer('v','<?php echo $getuser['media_id'];?>','addvi')"/></a>
			<?php
				}
			}
			?>
			
		</div>
      <!--<h2>Play All Galleries</h2>-->
	  <h2></h2>
      <div class="rowItem">
	 <?php
		
		$get_Galsort = array();
		for($i=0;$i<count($get_gal);$i++)
		{
			$get_Galsort[] =$new_profile_class_obj->get_gallery($get_gal[$i]);
		}
		
		$sort = array();
		foreach($get_Galsort as $k=>$v)
		{		
			$sort['play_count'][$k] = $v['play_count'];
			//$sort['from'][$k] = $end_f;
			//$sort['track'][$k] = $v['track'];
			//$sort['title'][$k] = $v['title'];
		}
		if(!empty($sort))
		{
			array_multisort($sort['play_count'], SORT_DESC,$get_Galsort);
		}
		
		$dummy_gp = array();
		$galleryImagesDataIndex = 0;
		$galleryImagesData = "";
		
		for($g=0;$g<count($get_Galsort);$g++)
		{
			if($get_Galsort[$g]['id']=="")
			{
				continue;
			}
			else
			{
				if(in_array($get_Galsort[$g]['id'],$dummy_gp))
				{}
				else
				{
				$dummy_gp[] = $get_Galsort[$g]['id'];
				//echo $get_common_id;
				$Get_count_img_at_reg=$new_profile_class_obj->get_count_Gallery_at_register($get_common_id,$get_Galsort[$g]['id']);
				if($Get_count_img_at_reg['count(*)']>0)
				{
					$Get_img_at_reg=$new_profile_class_obj->get_Gallery_at_register($get_common_id,$get_Galsort[$g]['id']);
					$Get_medi_cover_pic="";
					$play_array = array();
						if($Get_img_at_reg!="" && $Get_img_at_reg!=Null)
						{
							if(mysql_num_rows($Get_img_at_reg)>0)
							{
								while($row = mysql_fetch_assoc($Get_img_at_reg))
								{
									$play_array[] = $row;
								}
							
								${'orgPath'.$g}="https://reggallery.s3.amazonaws.com/";
								${'thumbPath'.$g}="https://reggalthumb.s3.amazonaws.com/";
								$register_cover_pic=$play_array[0]['cover_pic'];
								$register_No_pic=$play_array[0]['image_name'];
								$get_gallery=$new_profile_class_obj->get_Gallery_title_at_register($play_array[0]['gallery_id']);
								$galleryImagesDataIndex="";
								for($l=0;$l<count($play_array);$l++)
								{
									$mediaSrc = $play_array[0]['image_name'];
									$filePath = "https://reggallery.s3.amazonaws.com/".$mediaSrc;
									${'galleryImagesData'.$g}[$galleryImagesDataIndex]="";
									${'galleryImagesData'.$g}[$galleryImagesDataIndex] = $play_array[$l]['image_name'];
									${'galleryImagestitle'.$g}[$galleryImagesDataIndex] = $play_array[$l]['image_title'];
									$galleryImagesDataIndex++;
								}
							}
						}
				}
				else
				{
					$get_gallery=$new_profile_class_obj->get_gallery($get_Galsort[$g]['id']);
					$Get_medi_cover_pic=$new_profile_class_obj->get_media_cover_pic($get_gallery['id']);
					//$register_cover_pic="";
					//$register_No_pic="";
					$sql_play1 = mysql_query("SELECT * FROM media_images WHERE media_id='".$get_Galsort[$g]['id']."' AND gallery_id!=0");
						
						$play_array = array();
						if($sql_play1!="" && $sql_play1!=Null)
						{
							if(mysql_num_rows($sql_play1)>0)
							{
								while($row = mysql_fetch_assoc($sql_play1))
								{
									$play_array[] = $row;
								}
								${'orgPath'.$g}="https://medgallery.s3.amazonaws.com/";
								${'thumbPath'.$g}="https://medgalthumb.s3.amazonaws.com/";
								for($l=0;$l<count($play_array);$l++)
								{
									$mediaSrc = $play_array[0]['image_name'];
									$filePath = "https://medgallery.s3.amazonaws.com/".$mediaSrc;
									
									
									${'galleryImagesData'.$g}[$galleryImagesDataIndex] = $play_array[$l]['image_name'];
									${'galleryImagestitle'.$g}[$galleryImagesDataIndex] = $play_array[$l]['image_title'];
									//echo ${'galleryImagesData'.$g}[$galleryImagesDataIndex];
									$galleryImagesDataIndex++;
								}
								
							}
							else
							{
								${'orgPath'.$g}="https://medsingleimage.s3.amazonaws.com/";
								${'thumbPath'.$g}="https://medgalthumb.s3.amazonaws.com/";
								
								$sql_play1 = mysql_query("SELECT * FROM media_images WHERE media_id='".$get_Galsort[$g]['id']."' AND gallery_id=0");
								$play_array = array();
								if($sql_play1!="" && $sql_play1!=Null)
								{
									if(mysql_num_rows($sql_play1)>0)
									{
										while($row = mysql_fetch_assoc($sql_play1))
										{
											$play_array[] = $row;
										}
										
										for($l=0;$l<count($play_array);$l++)
										{
											$mediaSrc = $play_array[0]['image_name'];
											if($play_array[0]['for_sale']==1)
											{
												$filePath = "https://medgalhighres.s3.amazonaws.com/".$mediaSrc;
												${'orgPath'.$g}="https://medgalhighres.s3.amazonaws.com/";
												${'thumbPath'.$g}="https://medgalhighres.s3.amazonaws.com/";
											}
											else
											{
												$filePath = "https://medsingleimage.s3.amazonaws.com/".$mediaSrc;
											}
											
											${'galleryImagesData'.$g}[$galleryImagesDataIndex] = $play_array[$l]['image_name'];
											${'galleryImagestitle'.$g}[$galleryImagesDataIndex] = $play_array[$l]['image_title'];
											//echo $play_array[$l]['image_title'];
											//die;
											//echo ${'galleryImagesData'.$g}[$galleryImagesDataIndex];
											$galleryImagesDataIndex++;
											//echo $galleryImagesDataIndex;
										}
									}
								}
							}
						}
						else
						{
							${'orgPath'.$g}="https://medsingleimage.s3.amazonaws.com/";
							${'thumbPath'.$g}="https://medgalthumb.s3.amazonaws.com/";
							
							$sql_play1 = mysql_query("SELECT * FROM media_images WHERE media_id='".$get_Galsort[$g]['id']."' AND gallery_id=0");
							$play_array = array();
							if($sql_play1!="" && $sql_play1!=Null)
							{
								if(mysql_num_rows($sql_play1)>0)
								{
									while($row = mysql_fetch_assoc($sql_play1))
									{
										$play_array[] = $row;
									}
									
									for($l=0;$l<count($play_array);$l++)
									{
										$mediaSrc = $play_array[0]['image_name'];
										if($play_array[0]['for_sale']==1)
										{
											$filePath = "https://medgalhighres.s3.amazonaws.com/".$mediaSrc;
											${'orgPath'.$g}="https://medgalhighres.s3.amazonaws.com/";
											${'thumbPath'.$g}="https://medgalhighres.s3.amazonaws.com/";
										}
										else
										{
											$filePath = "https://medsingleimage.s3.amazonaws.com/".$mediaSrc;
										}
										
										${'galleryImagesData'.$g}[$galleryImagesDataIndex] = $play_array[$l]['image_name'];
										${'galleryImagestitle'.$g}[$galleryImagesDataIndex] = $play_array[$l]['image_title'];
										//echo $play_array[$l]['image_title'];
										//die;
										//echo ${'galleryImagesData'.$g}[$galleryImagesDataIndex];
										$galleryImagesDataIndex++;
										//echo $galleryImagesDataIndex;
									}
								}
							}
						}
						//die;
					//}
			}
	if($get_gallery['gallery_title'] !="" || $get_gallery['title']!="")
	{
		if(isset(${'galleryImagesData'.$g}))
		{
			${'galleryImagesData'.$g} = implode(",", ${'galleryImagesData'.$g});
			${'galleryImagestitle'.$g} = implode(",", ${'galleryImagestitle'.$g});
		}
	  ?>
        <div class="tabItem">
		<div style="position:relative; float:left; height: 200px;">
		<a href="javascript:void(0);" onclick="checkload('<?php echo $mediaSrc;?>','<?php echo ${'galleryImagesData'.$g}; ?>','<?php echo ${'thumbPath'.$g}; ?>','<?php echo ${'orgPath'.$g}; ?>','<?php echo ${'galleryImagestitle'.$g}; ?>','<?php echo $get_gallery['id'];?>')">
		<img onMouseOver="newfunction('<?php echo $get_gallery['id']; ?>')" onMouseOut="newfunctionout('<?php echo $get_gallery['id']; ?>')" class="imgs_pro" src="<?php if(isset($Get_medi_cover_pic) && $Get_medi_cover_pic!=""){
				if($Get_medi_cover_pic['profile_image'] !="" && $Get_medi_cover_pic['profile_image'] !=null )
				{
					echo $Get_medi_cover_pic['profile_image'];
					$gallery_image_name_cart = "https://medgalthumb.s3.amazonaws.com/".$Get_medi_cover_pic['profile_image'];
				}
				else{
					echo "https://medgalthumb.s3.amazonaws.com/Noimage.png";
					$gallery_image_name_cart = "https://medgalthumb.s3.amazonaws.com/Noimage.png";
				}
			
				/*if($Get_medi_cover_pic['cover_pic']=="" && $Get_medi_cover_pic['for_sale']==0)
				{
					//echo "https://medgalthumb.s3.amazonaws.com/".$Get_medi_cover_pic['image_name'];
					//$gallery_image_name_cart = "https://medgalthumb.s3.amazonaws.com/".$Get_medi_cover_pic['image_name'];
					echo $Get_medi_cover_pic['profile_image'];
					$gallery_image_name_cart = $Get_medi_cover_pic['profile_image'];
				}
				else if($Get_medi_cover_pic['for_sale']==1)
				{
					echo "https://medgalhighres.s3.amazonaws.com/".$Get_medi_cover_pic['image_name'];
					$gallery_image_name_cart ="https://medgalhighres.s3.amazonaws.com/".$Get_medi_cover_pic['image_name'];
				}
				else
				{
					echo "https://medgalthumb.s3.amazonaws.com/Noimage.png";
					$gallery_image_name_cart = "https://medgalthumb.s3.amazonaws.com/Noimage.png";
				}*/
			}
			else
			{
				if($register_cover_pic=="")
				{
					echo "https://reggalthumb.s3.amazonaws.com/".$register_No_pic;
					$gallery_image_name_cart = "https://reggalthumb.s3.amazonaws.com/".$register_No_pic;
				}
				else
				{
					echo "https://reggalthumb.s3.amazonaws.com/".$register_cover_pic;
					$gallery_image_name_cart ="https://reggalthumb.s3.amazonaws.com/".$register_cover_pic;
				}
			}
			
		?>" width="200" height="200" /></a>
		<div class="navButton">
			<div class="playMedia">
				<a class="list_play" href="javascript:void(0);"><img onMouseOver="newfunction('<?php echo $get_gallery['id']; ?>')" onMouseOut="newfunctionout('<?php echo $get_gallery['id']; ?>')" src="/images/listing/play_youtube.png" onclick="checkload('<?php echo $mediaSrc;?>','<?php echo ${'galleryImagesData'.$g}; ?>','<?php echo ${'thumbPath'.$g}; ?>','<?php echo ${'orgPath'.$g}; ?>','<?php echo ${'galleryImagestitle'.$g}; ?>','<?php echo $get_gallery['id'];?>')" id="play_video_image<?php echo $get_gallery['id'];?>" title="Add file to the bottom of your playlist without stopping current media." /></a>
			</div>
		</div>
		</div>
		<!--<img id="play_video_image<?php echo $get_gallery['id'];?>" onclick="checkload('<?php echo $mediaSrc;?>','<?php echo ${'galleryImagesData'.$g}; ?>','<?php echo ${'thumbPath'.$g}; ?>','<?php echo ${'orgPath'.$g}; ?>','<?php echo ${'galleryImagestitle'.$g}; ?>','<?php echo $get_gallery['id'];?>')" style="position: relative; bottom: 128px; left: 70px;" src="/images/profile/play_youtube.png" onMouseOver="newfunction('<?php echo $get_gallery['id'];?>')" onMouseOut="newfunctionout('<?php echo $get_gallery['id'];?>')" />-->
		<div class="both_wrap_per">
        <!--<div class="player">		 
			<a href="javascript:void(0);"><img src="../images/profile/play.gif" onmouseover="this.src='../images/profile/play-over.gif'" onclick="checkload('<?php /*echo $mediaSrc;?>','<?php echo ${'galleryImagesData'.$g}; ?>','<?php echo ${'thumbPath'.$g}; ?>','<?php echo ${'orgPath'.$g}; ?>','<?php echo ${'galleryImagestitle'.$g}; ?>','<?php echo $get_gallery['id'];?>')" onmouseout="this.src='../images/profile/play.gif'" /></a>
			<a href=""><img src="../images/profile/add.gif"  onmouseover="this.src='../images/profile/add-over.gif'" onmouseout="this.src='../images/profile/add.gif'"/></a>
			<a href="javascript:void(0);" onclick="down_gallery_pop('<?php if($Get_medi_cover_pic['for_sale']!=1){echo "nosale";}else{echo $g;}*/?>')"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" /></a>-->
			<?php 
			if($Get_medi_cover_pic['for_sale']==1)
			{
			?>
			<a href="addtocart.php?type=gallery&seller_id=<?php echo $get_gallery['general_user_id'];?>&media_id=<?php echo $get_gallery['id'];?>&buyer_id=<?php echo $_SESSION['gen_user_id'];?>&title=<?php echo $gallery_title_cart;?>&image_name=<?php echo $gallery_image_name_cart;?>" id="download_gallery<?php echo $g;?>" style="display:none;"></a>
				<script type="text/javascript">
				$(document).ready(function() {
						$("#download_gallery<?php echo $g;?>").fancybox({
						'width'				: '55%',
						'height'			: '75%',
						'transitionIn'		: 'none',
						'transitionOut'		: 'none',
						'type'				: 'iframe'
						});
				});
				</script>
		  <?php
		  }
		  ?>
		  <!--</div>-->
		  <div class="type_subtype">
		  <a href="javascript:void(0);" onclick="checkload('<?php echo $mediaSrc;?>','<?php echo ${'galleryImagesData'.$g}; ?>','<?php echo ${'thumbPath'.$g}; ?>','<?php echo ${'orgPath'.$g}; ?>','<?php echo ${'galleryImagestitle'.$g}; ?>','<?php echo $get_gallery['id'];?>')">
		  <?php if(isset($get_gallery['gallery_title'])){
					if(strlen($get_gallery['gallery_title'])>26){
						echo substr($get_gallery['gallery_title'],0,22)."...<br/>";
					}else{
						echo $get_gallery['gallery_title']."<br/>";
					}
					$gallery_title_cart =$get_gallery['gallery_title'];
				
					$by_cret = $display->get_creator_by($get_gallery['id']);
				}else{
					if(strlen($get_gallery['title'])>26){
						echo substr($get_gallery['title'],0,22)."...<br/>";
					}else{
						echo $get_gallery['title']."<br/>";
					}
					$gallery_title_cart = $get_gallery['title'];
					
					$by_cret = $display->get_creator_by($get_gallery['id']);
				}
				
				if($by_cret[1]!="")
				{ 
					$purl_ps = $by_cret[1]; 
				}				
				else
				{ 
					$purl_ps = 'javascript:void(0)';
				}
														
			?></a>
				<div class="by_details">
			<?php	
				if($by_cret[0]!="")
				{
					if(strlen($by_cret[0])>=19)
					{
						echo "by <a href='".$purl_ps."'>".substr($by_cret[0],0,15)."</a>...";
					}
					else
					{
						echo "by <a href='".$purl_ps."'>".$by_cret[0]."</a>";
					}
				}
		?></div><?php $get_profile_type = $display->types_of_search('media',$get_gallery['id']);
						if(!empty($get_profile_type) && $get_profile_type!=Null)
						{
							$res_profile_type = mysql_fetch_assoc($get_profile_type);
							if($res_profile_type['type_name']!=""){
				?>
								<span style="font-size:13px; font-weight:normal;">								
				<?php 									
								if(strlen($res_profile_type['subtype_name'])>26){
									echo substr($res_profile_type['subtype_name'],0,24)."...";
								}else{
									echo $res_profile_type['subtype_name'];
								}
								if($res_profile_type['metatype_name']!="" && strlen($res_profile_type['subtype_name']<26)){
									$type_length = strlen($res_profile_type['subtype_name']);
									if($type_length + strlen($res_profile_type['metatype_name'])>22){
										echo ", ".substr($res_profile_type['metatype_name'],0,22 - $type_length)."...";
									}else{
										echo ", ".$res_profile_type['metatype_name'];
									}							
								}									
				?>
								</span>
				<?php
							}
						} ?></div></div>
        </div>
	<?php
		}
		}
	}
	}
	?>
       
      </div>      
    </div>
    <?php
	}
	else
	{
	?>
		<div id="projectsTab" class="hiddenBlock3"></div>
	<?php
	}


	if($get_media_Info['taggedsongs']!="" || $get_media_Info['tagged_songs']!="")
	{
	?>
    <div id="eventsTab" class="hiddenBlock2">
		<div id="profileFeatured">
			<div class="playall">
				Play All
			</div>
			<?php
				if($artist_check !=0 || $community_check !=0)
			{
			?>
				<a title="Play all of the media on this profile page." href="javascript:void(0);"><img src="../images/profile/play.png" width="35" height="23" onmouseover="this.src='../images/profile/play-over.png'" onmouseout="this.src='../images/profile/play.png'" onclick="PlayAll('a','all_song_id')" /></a>
				<a title="Add all of the media on this profile page to your playlist without stopping your current file." href="javascript:void(0);"><img src="../images/profile/add.png" width="27" height="23" onmouseover="this.src='../images/profile/add-over.png'" onmouseout="this.src='../images/profile/add.png'" onclick="PlayAll('a','all_song_id')" /></a>
			<?php
			}
			elseif($artist_event_check !=0 || $community_event_check !=0)
			{
			?>
				<a title="Play all of the media on this events page." href="javascript:void(0);"><img src="../images/profile/play.png" width="35" height="23" onmouseover="this.src='../images/profile/play-over.png'" onmouseout="this.src='../images/profile/play.png'" onclick="PlayAll('a','all_song_id')" /></a>
				<a title="Add all of the media on this events page to your playlist without stopping your current file." href="javascript:void(0);"><img src="../images/profile/add.png" width="27" height="23" onmouseover="this.src='../images/profile/add-over.png'" onmouseout="this.src='../images/profile/add.png'" onclick="PlayAll('a','all_song_id')" /></a>
			<?php
			}
			elseif($artist_project_check !=0 || $community_project_check !=0)
			{
			?>
				<a title="Play all of the media on this projects page." href="javascript:void(0);"><img src="../images/profile/play.png" width="35" height="23" onmouseover="this.src='../images/profile/play-over.png'" onmouseout="this.src='../images/profile/play.png'" onclick="PlayAll('a','all_song_id')" /></a>
				<a title="Add all of the media on this projects page to your playlist without stopping your current file." href="javascript:void(0);"><img src="../images/profile/add.png" width="27" height="23" onmouseover="this.src='../images/profile/add-over.png'" onmouseout="this.src='../images/profile/add.png'" onclick="PlayAll('a','all_song_id')" /></a>
			<?php
			}
			?>
		</div>
		<div id="profileFeatured">
			
			<?php //echo $getuser['featured_media'].$getuser['media_id'];
			
			if($artist_check !=0 || $artist_event_check !=0 || $artist_project_check !=0  )
			{
				$table_type = "artist";
			}
			if($community_check !=0 || $community_event_check !=0 || $community_project_check !=0  )
			{
				$table_type = "community";
			}
			if($getuser['featured_media'] == "Gallery" && $getuser['media_id']!=0)
			{
				$find_whether_regis=$new_profile_class_obj->get_count_Gallery_at_register($getuser["$table_type".'_id'],$getuser['media_id']);
				if($find_whether_regis["count(*)"]>1)
				{
					$getimages=$new_profile_class_obj->get_featured_media_gallery_at_register($getuser['media_id'],$table_type);
					$feature_image_name = array();
					$feature_image_title = array();
					$feature_count = 0;
					while($get_image = mysql_fetch_assoc($getimages))
					{
						$feature_image_name[$feature_count] = $get_image['image_name'];
						$feature_image_title[$feature_count] = $get_image['image_title'];
						$feature_count++;
					}
					$feature_big_img = $feature_image_name[0];
					$featured_gal_img = implode(",", $feature_image_name);
					$featured_gal_img_title = implode(",", $feature_image_title);
					$feature_org_path = "https://reggallery.s3.amazonaws.com/";
					$feature_thum_path = "https://reggalthumb.s3.amazonaws.com/";
				}
				else
				{
					$getimages=$new_profile_class_obj->get_featured_media_gallery($getuser['media_id']);
					$feature_image_name = array();
					$feature_image_title = array();
					$feature_count = 0;
					while($get_image = mysql_fetch_assoc($getimages))
					{
						$feature_image_name[$feature_count] = $get_image['image_name'];
						$feature_image_title[$feature_count] = $get_image['image_title'];
						$feature_count++;
					}
					$feature_big_img = $feature_image_name[0];
					$featured_gal_img = implode(",", $feature_image_name);
					$featured_gal_img_title = implode(",", $feature_image_title);
					$feature_org_path = "https://medgallery.s3.amazonaws.com/";
					$feature_thum_path = "https://medgalthumb.s3.amazonaws.com/";
				}
			?>
				<div class="fmedia">featured Media</div>
				<?php
				if($artist_check !=0 || $community_check !=0)
				{
				?>
					<a title="Play this artists featured media." href="javascript:void(0);"><img src="../images/profile/play.png" width="35" height="23" onmouseover="this.src='../images/profile/play-over.png'" onmouseout="this.src='../images/profile/play.png'" onclick="checkload('<?php echo $feature_big_img;?>','<?php echo $featured_gal_img; ?>','<?php echo $feature_thum_path; ?>','<?php echo $feature_org_path; ?>','<?php echo $featured_gal_img_title; ?>')" /></a>
					<a title="Add this artists featured media to your playlist without stopping your current file." href="javascript:void(0);"><img src="../images/profile/add.png" width="27" height="23" onmouseover="this.src='../images/profile/add-over.png'" onmouseout="this.src='../images/profile/add.png'" /></a>
			<?php
				}
				elseif($artist_event_check !=0 || $community_event_check !=0)
				{
			?>
					<a title="Play this events featured media." href="javascript:void(0);"><img src="../images/profile/play.png" width="35" height="23" onmouseover="this.src='../images/profile/play-over.png'" onmouseout="this.src='../images/profile/play.png'" onclick="checkload('<?php echo $feature_big_img;?>','<?php echo $featured_gal_img; ?>','<?php echo $feature_thum_path; ?>','<?php echo $feature_org_path; ?>','<?php echo $featured_gal_img_title; ?>')" /></a>
					<a title="Add this events featured media to your playlist without stopping your current file." href="javascript:void(0);"><img src="../images/profile/add.png" width="27" height="23" onmouseover="this.src='../images/profile/add-over.png'" onmouseout="this.src='../images/profile/add.png'" /></a>
			<?php
				}
				elseif($artist_project_check !=0 || $community_project_check !=0)
				{
			?>
					<a title="Play this projects featured media." href="javascript:void(0);"><img src="../images/profile/play.png" width="35" height="23" onmouseover="this.src='../images/profile/play-over.png'" onmouseout="this.src='../images/profile/play.png'" onclick="checkload('<?php echo $feature_big_img;?>','<?php echo $featured_gal_img; ?>','<?php echo $feature_thum_path; ?>','<?php echo $feature_org_path; ?>','<?php echo $featured_gal_img_title; ?>')" /></a>
					<a title="Add this projects featured media to your playlist without stopping your current file." href="javascript:void(0);"><img src="../images/profile/add.png" width="27" height="23" onmouseover="this.src='../images/profile/add-over.png'" onmouseout="this.src='../images/profile/add.png'" /></a>
			<?php
				}
			}
			if($getuser['featured_media'] == "Song" && $getuser['media_id']!=0)
			{
			?>
				<div class="fmedia">featured Media</div>
				<?php
				if($artist_check !=0 || $community_check !=0)
				{
				?>
					<a title="Play this artists featured media." href="javascript:void(0);"><img src="../images/profile/play.png" width="35" height="23" onmouseover="this.src='../images/profile/play-over.png'" onmouseout="this.src='../images/profile/play.png'" onclick="showPlayer('a','<?php echo $getuser['media_id'];?>','play')" /></a>
					<a title="Add this artists featured media to your playlist without stopping your current file." href="javascript:void(0);"><img src="../images/profile/add.png" width="27" height="23" onmouseover="this.src='../images/profile/add-over.png'" onmouseout="this.src='../images/profile/add.png'" onclick="showPlayer('a','<?php echo $getuser['media_id'];?>','add')" /></a>
			<?php
				}
				elseif($artist_event_check !=0 || $community_event_check !=0)
				{
			?>
					<a title="Play this events featured media." href="javascript:void(0);"><img src="../images/profile/play.png" width="35" height="23" onmouseover="this.src='../images/profile/play-over.png'" onmouseout="this.src='../images/profile/play.png'" onclick="showPlayer('a','<?php echo $getuser['media_id'];?>','play')" /></a>
					<a title="Add this events featured media to your playlist without stopping your current file." href="javascript:void(0);"><img src="../images/profile/add.png" width="27" height="23" onmouseover="this.src='../images/profile/add-over.png'" onmouseout="this.src='../images/profile/add.png'" onclick="showPlayer('a','<?php echo $getuser['media_id'];?>','add')" /></a>
			<?php
				}
				elseif($artist_project_check !=0 || $community_project_check !=0)
				{
			?>
					<a title="Play this projects featured media." href="javascript:void(0);"><img src="../images/profile/play.png" width="35" height="23" onmouseover="this.src='../images/profile/play-over.png'" onmouseout="this.src='../images/profile/play.png'" onclick="showPlayer('a','<?php echo $getuser['media_id'];?>','play')" /></a>
					<a title="Add this projects featured media to your playlist without stopping your current file." href="javascript:void(0);"><img src="../images/profile/add.png" width="27" height="23" onmouseover="this.src='../images/profile/add-over.png'" onmouseout="this.src='../images/profile/add.png'" onclick="showPlayer('a','<?php echo $getuser['media_id'];?>','add')" /></a>
			<?php
				}
			}
			?>
			<?php
			if($getuser['featured_media'] == "Video" && $getuser['media_id']!=0)
			{
			?>
				<div class="fmedia">featured Media</div>
				<?php
				if($artist_check !=0 || $community_check !=0)
				{
				?>
					<a title="Play this artists featured media." href="javascript:void(0);"><img src="../images/profile/play.png" width="35" height="23" onmouseover="this.src='../images/profile/play-over.png'" onmouseout="this.src='../images/profile/play.png'" onclick="showPlayer('v','<?php echo $getuser['media_id'];?>','playlist')" /></a>
					<a title="Add this artists featured media to your playlist without stopping your current file." href="javascript:void(0);"><img src="../images/profile/add.png" width="27" height="23" onmouseover="this.src='../images/profile/add-over.png'" onmouseout="this.src='../images/profile/add.png'" onclick="showPlayer('v','<?php echo $getuser['media_id'];?>','addvi')" /></a>
			<?php
				}
				elseif($artist_event_check !=0 || $community_event_check !=0)
				{
			?>
					<a title="Play this events featured media." href="javascript:void(0);"><img src="../images/profile/play.png" width="35" height="23" onmouseover="this.src='../images/profile/play-over.png'" onmouseout="this.src='../images/profile/play.png'" onclick="showPlayer('v','<?php echo $getuser['media_id'];?>','playlist')" /></a>
					<a title="Add this events featured media to your playlist without stopping your current file." href="javascript:void(0);"><img src="../images/profile/add.png" width="27" height="23" onmouseover="this.src='../images/profile/add-over.png'" onmouseout="this.src='../images/profile/add.png'" onclick="showPlayer('v','<?php echo $getuser['media_id'];?>','addvi')" /></a>
			<?php
				}
				elseif($artist_project_check !=0 || $community_project_check !=0)
				{
			?>
					<a title="Play this projects featured media." href="javascript:void(0);"><img src="../images/profile/play.png" width="35" height="23" onmouseover="this.src='../images/profile/play-over.png'" onmouseout="this.src='../images/profile/play.png'" onclick="showPlayer('v','<?php echo $getuser['media_id'];?>','playlist')" /></a>
					<a title="Add this projects featured media to your playlist without stopping your current file." href="javascript:void(0);"><img src="../images/profile/add.png" width="27" height="23" onmouseover="this.src='../images/profile/add-over.png'" onmouseout="this.src='../images/profile/add.png'" onclick="showPlayer('v','<?php echo $getuser['media_id'];?>','addvi')" /></a>
			<?php
				}
			}
			?>
			
		</div>
      <!--<h2>Play All Songs</h2>-->
	  <h2 style="margin: 0 0 3px 16px;"></h2>
     <div class="rowItem">
	   <?php
		$get_sort_songs = array();
		for($acc_song1=0;$acc_song1<count($get_song);$acc_song1++)
		{
			if($get_song[$acc_song1]!="" && $get_song[$acc_song1]!=0)
			{
				if($artist_check !=0 || $community_check!=0){
					$get_sort_songs[$acc_song1] = $new_profile_class_obj->get_Song_title($get_song[$acc_song1]);
					$get_artist_song1 = $new_profile_class_obj->get_Song($get_song[$acc_song1]);
					//$getsong1 = mysql_fetch_assoc($get_artist_song1);
					//var_dump($getsong1);
					$get_sort_songs[$acc_song1]['track'] = $get_artist_song1['track'];
				}
				else if($artist_check ==0 && $community_check ==0)
				{
					$get_sort_songs[$acc_song1] = $new_profile_class_obj->get_Song($get_song[$acc_song1]);
					$get_artist_song1 = $new_profile_class_obj->get_Song_disp($get_song[$acc_song1]);
					//$getsong1 = mysql_fetch_assoc($get_artist_song1);
					$get_sort_songs[$acc_song1]['track'] = $get_artist_song1['track'];
				}
			}
		}
		
		$sort = array();
		foreach($get_sort_songs as $k=>$v)
		{	
			//$create_c = strpos($v['creator'],'(');
			//$end_c1 = substr($v['creator'],0,$create_c);
			$end_c1 = $v['creator'];
			$end_c = trim($end_c1);
			
			//$create_f = strpos($v['from'],'(');
			//$end_f1 = substr($v['from'],0,$create_f);
			$end_f1 = $v['from'];
			$end_f = trim($end_f1);
			
			$sort['creator'][$k] = strtolower($end_c);
			$sort['from'][$k] = strtolower($end_f);
			$sort['track'][$k] = $v['track'];
			$sort['title'][$k] = strtolower($v['title']);
		}
		//var_dump($sort);
		if(!empty($sort))
		{
			array_multisort($sort['creator'], SORT_ASC, $sort['from'], SORT_ASC, $sort['track'], SORT_NUMERIC,SORT_ASC,$sort['title'], SORT_ASC,$get_sort_songs);
		}
		$dummy_sp = array();
		//var_dump($get_sort_songs);
		include_once("findsale_song.php");
		//var_dump($get_songs_pnew_w);
		for($i=0;$i<count($get_sort_songs);$i++)
		{
			//echo $get_song[$i];
			if(in_array($get_sort_songs[$i]['id'],$dummy_sp))
			{}
			else
			{
				$dummy_sp[] = $get_sort_songs[$i]['id'];
				$get_Song = $new_profile_class_obj->get_Song($get_sort_songs[$i]['id']);
				if(isset($get_Song))
				{
					$src_list ="https://comprothumjcrop.s3.amazonaws.com/";
					$from_list_pic_song ="";
					$creator_list_pic_song ="";
					if($get_Song['from_info']!="")
					{
						$expl_from_in =explode("|",$get_Song['from_info']);
						$get_from_list_pic = $new_profile_class_obj->get_from_list_pic($expl_from_in[0],$expl_from_in[1]);
						if($get_from_list_pic['listing_image_name'] =="")
						{
							$from_list_pic_song="https://artjcropthumb.s3.amazonaws.com/Noimage.png";
						}
						else
						{
							$from_list_pic_song = $get_from_list_pic['listing_image_name'];
						}
					}
					if($get_Song['from_info'] =="" && $get_Song['creator_info']!="")
					{
						$expl_creator_in =explode("|",$get_Song['creator_info']);
						if($expl_creator_in[0] == "general_community")
						{
							$col_name = "community_id";
							$creator_src = "https://comjcropthumb.s3.amazonaws.com/";
						}
						else if($expl_creator_in[0] == "general_artist")
						{
							$col_name = "artist_id";
							$creator_src = "https://artjcropthumb.s3.amazonaws.com/";
						}
						else
						{
							$col_name = "id";
							$creator_src = "";
						}
						$get_creator_list_pic = $new_profile_class_obj->get_creator_list_pic($expl_creator_in[0],$expl_creator_in[1],$col_name);
						if($get_creator_list_pic['listing_image_name'] =="")
						{
							$creator_list_pic_song = $creator_src."Noimage.png";
						}
						else
						{
							$creator_list_pic_song = $creator_src.$get_creator_list_pic['listing_image_name'];
						}
					}
				}
				if($get_media_Info['taggedsongs']!="")
				{
					//echo "azhar";
					$get_Song_title = $new_profile_class_obj->get_Song_title($get_sort_songs[$i]['id']);
					
					$from_list_pic_song ="";
					$creator_list_pic_song ="";
					if($get_Song_title['from_info']!="")
					{
						$expl_from_in =explode("|",$get_Song_title['from_info']);
						$get_from_list_pic = $new_profile_class_obj->get_from_list_pic($expl_from_in[0],$expl_from_in[1]);
						if($get_from_list_pic['listing_image_name'] =="")
						{
							$from_list_pic_song="https://artjcropthumb.s3.amazonaws.com/Noimage.png";
						}
						else
						{
							$from_list_pic_song = $get_from_list_pic['listing_image_name'];
						}
					}
					if($get_Song_title['from_info'] =="" && $get_Song_title['creator_info']!="")
					{
						$expl_creator_in =explode("|",$get_Song_title['creator_info']);
						if($expl_creator_in[0] == "general_community")
						{
							$col_name = "community_id";
							$creator_src = "https://comjcropthumb.s3.amazonaws.com/";
						}
						else if($expl_creator_in[0] == "general_artist")
						{
							$col_name = "artist_id";
							$creator_src = "https://artjcropthumb.s3.amazonaws.com/";
						}
						else
						{
							$col_name = "id";
							$creator_src = "";
						}
						$get_creator_list_pic = $new_profile_class_obj->get_creator_list_pic($expl_creator_in[0],$expl_creator_in[1],$col_name);
						if($get_creator_list_pic['listing_image_name'] =="")
						{
							$creator_list_pic_song = $creator_src."Noimage.png";
						}
						else
						{
							$creator_list_pic_song = $creator_src.$get_creator_list_pic['listing_image_name'];
						}
					}
				}
				$get_Song_regis = $new_profile_class_obj->get_Song_regis($get_sort_songs[$i]['id']);
				$get_artist_Song=$new_profile_class_obj->get_Song_disp($get_sort_songs[$i]['id']);
				
				//var_dump($get_artist_Song['gallery_id']);
				if($get_artist_Song['gallery_id']!=0 && $get_artist_Song['gallery_at']=='general_artist') 
				{
					$Get_gal_img_for_song1_artist=$new_profile_class_obj->get_artist_Gallery_at_register_for_song($get_artist_Song['gallery_id']);
					if($Get_gal_img_for_song1_artist!="" && $Get_gal_img_for_song1_artist!=Null)
					{
						if(mysql_num_rows($Get_gal_img_for_song1_artist)>0)
						{
							$Get_gal_img_for_song=mysql_fetch_assoc($Get_gal_img_for_song1_artist);
							//echo $Get_gal_img_for_song['image_name']."reg_art</br>";
							$starting_path="https://reggalthumb.s3.amazonaws.com/";
						}
					}
				}
				
				if($get_artist_Song['gallery_id']!=0 && $get_artist_Song['gallery_at']=='general_community') 
				{
					$Get_gal_img_for_song1_comm=$new_profile_class_obj->get_community_Gallery_at_register_for_song($get_artist_Song['gallery_id']);
					if($Get_gal_img_for_song1_comm!="" && $Get_gal_img_for_song1_comm!=Null)
					{
						if(mysql_num_rows($Get_gal_img_for_song1_comm)>0)
						{			
							$Get_gal_img_for_song=mysql_fetch_assoc($Get_gal_img_for_song1_comm);
							//echo $Get_gal_img_for_song['image_name']."reg_comm</br>";
							$starting_path="https://reggalthumb.s3.amazonaws.com/";
						}
					}
				}
				
				if($get_artist_Song['gallery_id']!=0 && $get_artist_Song['gallery_at']=='general_media') 
				{
					$Get_gal_img_for_song1_media=$new_profile_class_obj->get_media_Gallery($get_artist_Song['gallery_id']);
					if($Get_gal_img_for_song1_media!="" && $Get_gal_img_for_song1_media!=Null)
					{
						if(mysql_num_rows($Get_gal_img_for_song1_media)>0)
						{			
							$Get_gal_img_for_song=mysql_fetch_assoc($Get_gal_img_for_song1_media);
							//echo $Get_gal_img_for_song['cover_pic']."gen_media</br>";
							$starting_path="https://medgalthumb.s3.amazonaws.com/";
						}
					}
				}
				if($Get_gal_img_for_song['cover_pic'] =="" && $Get_gal_img_for_song['image_name'] =="")
				{
					if($getgeneral['general_user_id'] != $get_Song_title['general_user_id'])
					{
						if($artist_check !=0)
						{
							$get_tag_listing_pic =$new_profile_class_obj->get_tagged_song_list_pic($get_Song_title['general_user_id']);
						}
					}
				}
		
		if($get_Song_title['title'] !="" || $get_Song_regis['audio_name'] !="" || $get_Song['title'] !="")
		{
		  ?>
			<div class="tabItem" style="float:none; height:42px; margin-top:20px; width:892px; <?php if((count($get_sort_songs)-1) != $i){ echo "border-bottom:1px solid #DDDDDD;"; }?>"><!--<a href="javascript:void(0);" onclick="showPlayer('a','<?php if($get_Song_title['id']!=""){echo $get_Song_title['id'];}else{echo $get_Song['id'];}?>','play');"><img  src="<?php
			/*if($get_artist_Song['gallery_id']!="" && $get_artist_Song['gallery_id'] !=0 && $get_artist_Song['gallery_id'] != null)
			{
				//echo $starting_path;
				if($Get_gal_img_for_song['cover_pic'] =="" && $Get_gal_img_for_song['image_name'] =="")
				{
					//if($getgeneral['general_user_id'] != $get_Song_title['general_user_id']){
						//echo $src_list.$get_tag_listing_pic['listing_image_name'];}
					//else{
						echo $src_list."Noimage.png";
						$song_image_name_cart = $src_list."Noimage.png";
						//}
				}
				else
				{
					if(isset($Get_gal_img_for_song['cover_pic']) && $Get_gal_img_for_song['cover_pic']!=""){
						echo $Get_gal_img_for_song['cover_pic'];
						$song_image_name_cart = $starting_path.$Get_gal_img_for_song['cover_pic'];
						}
					else{
						echo $Get_gal_img_for_song['image_name'];
						$song_image_name_cart = $starting_path.$Get_gal_img_for_song['image_name'];
						}
				}
				$starting_path ="";
				$Get_gal_img_for_song ="";
			}
			else
			{
				if(isset($from_list_pic_song) && $from_list_pic_song!=""){
					echo $from_list_pic_song;
					$song_image_name_cart = $from_list_pic_song;}
				else if(isset($creator_list_pic_song) && $creator_list_pic_song!=""){
					echo $creator_list_pic_song;
					$song_image_name_cart =$creator_list_pic_song;}
				else{
				echo $src_list."Noimage.png";
				$song_image_name_cart =$src_list."Noimage.png";}
			}*/
			if(isset($get_Song_title) && $get_Song_title!= null){
				$song_image_name_cart = $new_profile_class_obj->get_audio_img($get_Song_title['id']);
			}else{
				$song_image_name_cart = $new_profile_class_obj->get_audio_img($get_Song['id']);
			}
			//var_dump($song_image_name_cart);
			
			?>" width="200" height="200" /></a>-->
			<div class="player" style="width: 82px;">
				<a href="javascript:void(0);"><img src="../images/profile/play.gif"  onmouseover="this.src='../images/profile/play-over.gif'" onmouseout="this.src='../images/profile/play.gif'" onclick="showPlayer('a','<?php if($get_Song_title['id']!=""){echo $get_Song_title['id'];}else{echo $get_Song['id'];}?>','play');" title="Play file now." /></a>
				<a href="javascript:void(0);"><img src="../images/profile/add.gif"  onmouseover="this.src='../images/profile/add-over.gif'" onmouseout="this.src='../images/profile/add.gif'" onclick="showPlayer('a','<?php if($get_Song_title['id'] !=""){ echo $get_Song_title['id'];}else{ echo $get_Song['id']; }?>','add');" title="Add file to the bottom of your playlist without stopping current media."/></a>
				<?php
				//var_dump($get_Song_title);
				
				if($get_Song_title['sharing_preference']==5 || $get_Song_title['sharing_preference']==2 || $get_Song['sharing_preference']==2 || $get_Song['sharing_preference']==5){
					$free_down_rel ="";
					
					$stats = 0;
					if($_SESSION['login_email']!=NULL)
					{
						if($get_Song_title['id']!="")
						{
							$stats = $display->checkdownloaded($get_Song_title['id'],$_SESSION['login_id']);
						}
						else
						{
							$stats = $display->checkdownloaded($get_Song['id'],$_SESSION['login_id']);
						}
					}
					else
					{	
						if($get_Song_title['id']!="")
						{
							$stats = $display->checkdownloaded_not_login($get_Song_title['id']);
						}
						else
						{
							$stats = $display->checkdownloaded_not_login($get_Song['id']);
						}
					}
					
					if($stats==1)
					{
						$free_down_rel = "yes";
					}
					else
					{	
						//echo "some";
						for($find_down=0;$find_down<=count($get_songs_pnew_w);$find_down++)
						{
							//echo $get_songs_pnew_w[$find_down]['id'] ."==". $get_Song_title['id']."</br>";
							
							if(isset($get_Song_title) && $get_Song_title != null)
							{
								//var_dump($get_songs_pnew_w);
								if($get_songs_pnew_w[$find_down]['id']==$get_Song_title['id']){
								$free_down_rel = "yes";
								}
								else{
									if($free_down_rel =="" || $free_down_rel =="no"){
										$free_down_rel = "no";
									}
								}
							}else if(isset($get_Song) && $get_Song != null)
							{
								//echo "else";
								if($get_songs_pnew_w[$find_down]['id']==$get_Song['id']){
								$free_down_rel = "yes";
								}
								else{
									if($free_down_rel =="" || $free_down_rel =="no"){
										$free_down_rel = "no";
									}
								}
							}
							
						}
					}
					//echo $free_down_rel;
					//echo $get_Song_title['id'];
					if($free_down_rel == "yes"){
				?>
					<a href="javascript:void(0);" onclick="download_directly_song('<?php if($get_Song_title['id']!=""){echo $get_Song_title['id'];}else{echo $get_Song['id'];}?>','<?php if($get_Song_title['title']!=""){echo str_replace("'","\'",$get_Song_title['title']);}else{echo str_replace("'","\'",$get_Song['title']);}?>','<?php if($get_Song_title['title']!=""){echo str_replace("'","\'",$get_Song_title['creator']);}else{echo str_replace("'","\'",$get_Song['creator']);}?>','<?php if($get_Song_title['title']!=""){echo str_replace("'","\'",$get_Song_title['from']);}else{echo str_replace("'","\'",$get_Song['from']);}?>')"><img src="../images/profile/download_down.gif" onmouseover="this.src='../images/profile/download-over_down.gif'" onmouseout="this.src='../images/profile/download_down.gif'" title="Add media to your cart for free download or purchase." /></a>
				<?php
					}
					else if($get_Song_title['sharing_preference']==2  || $get_Song['sharing_preference']==2){
						if($get_Song_title['id']!=""){
							if($get_Song_title['sharing_preference']==2)
							{
								if($get_Song_title['ask_tip']==0)
								{
					?>
							<a href="javascript:void(0);" onclick="download_directly_song('<?php if($get_Song_title['id']!=""){echo $get_Song_title['id'];}else{echo $get_Song['id'];}?>','<?php if($get_Song_title['title']!=""){echo str_replace("'","\'",$get_Song_title['title']);}else{echo str_replace("'","\'",$get_Song['title']);}?>','<?php if($get_Song_title['title']!=""){echo str_replace("'","\'",$get_Song_title['creator']);}else{echo str_replace("'","\'",$get_Song['creator']);}?>','<?php if($get_Song_title['title']!=""){echo str_replace("'","\'",$get_Song_title['from']);}else{echo str_replace("'","\'",$get_Song['from']);}?>')"><img src="../images/profile/download_down.gif" onmouseover="this.src='../images/profile/download-over_down.gif'" onmouseout="this.src='../images/profile/download_down.gif'" title="Add media to your cart for free download or purchase." /></a>
					<?php
								}
								else
								{
									$tip_add_aftr_detils = $display->get_tip_details_for_every($get_Song_title['id'],'media');
					?>
							<a href="javascript:void(0);" onclick="ask_to_donate('<?php echo mysql_real_escape_string($get_Song_title['title']);?>','<?php echo mysql_real_escape_string($get_Song_title['creator']);?>','<?php echo $get_Song_title['id'];?>','download_song<?php echo $i;?>','<?php echo $get_Song_title['general_user_id']; ?>','<?php echo $tip_add_aftr_detils; ?>')"><img src="../images/profile/download_down.gif" onmouseover="this.src='../images/profile/download-over_down.gif'" onmouseout="this.src='../images/profile/download_down.gif'" title="Add media to your cart for free download or purchase." /></a>
					<?php
								}
							}
							elseif($get_Song_title['sharing_preference']==5)
							{
								$tip_add_aftr_detils = $display->get_tip_details_for_every($get_Song_title['id'],'media');
							?>
					<a href="javascript:void(0);" onclick="ask_to_donate('<?php echo mysql_real_escape_string($get_Song_title['title']);?>','<?php echo mysql_real_escape_string($get_Song_title['creator']);?>','<?php echo $get_Song_title['id'];?>','download_song<?php echo $i;?>','<?php echo $get_Song_title['general_user_id']; ?>','<?php echo $tip_add_aftr_detils; ?>')"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>
					<?php
							}
						
						}else{
							if($get_Song['sharing_preference']==2)
							{
								if($get_Song['ask_tip']==0)
								{
					?>
									<a href="javascript:void(0);" onclick="download_directly_song('<?php if($get_Song_title['id']!=""){echo $get_Song_title['id'];}else{echo $get_Song['id'];}?>','<?php if($get_Song_title['title']!=""){echo str_replace("'","\'",$get_Song_title['title']);}else{echo str_replace("'","\'",$get_Song['title']);}?>','<?php if($get_Song_title['title']!=""){echo str_replace("'","\'",$get_Song_title['creator']);}else{echo str_replace("'","\'",$get_Song['creator']);}?>','<?php if($get_Song_title['title']!=""){echo str_replace("'","\'",$get_Song_title['from']);}else{echo str_replace("'","\'",$get_Song['from']);}?>')"><img src="../images/profile/download_down.gif" onmouseover="this.src='../images/profile/download-over_down.gif'" onmouseout="this.src='../images/profile/download_down.gif'" title="Add media to your cart for free download or purchase." /></a>
					<?php
								}
								else
								{
									$tip_add_aftr_detils = $display->get_tip_details_for_every($get_Song['id'],'media');
								?>
					<a href="javascript:void(0);" onclick="ask_to_donate('<?php echo mysql_real_escape_string($get_Song['title']);?>','<?php echo mysql_real_escape_string($get_Song['creator']);?>','<?php echo $get_Song['id'];?>','download_song<?php echo $i;?>','<?php echo $get_Song['general_user_id']; ?>','<?php echo $tip_add_aftr_detils; ?>')"><img src="../images/profile/download_down.gif" onmouseover="this.src='../images/profile/download-over_down.gif'" onmouseout="this.src='../images/profile/download_down.gif'" title="Add media to your cart for free download or purchase." /></a>
					<?php
								}
							}
							elseif($get_Song['sharing_preference']==5)
							{
								$tip_add_aftr_detils = $display->get_tip_details_for_every($get_Song['id'],'media');
							?>
					<a href="javascript:void(0);" onclick="ask_to_donate('<?php echo mysql_real_escape_string($get_Song['title']);?>','<?php echo mysql_real_escape_string($get_Song['creator']);?>','<?php echo $get_Song['id'];?>','download_song<?php echo $i;?>','<?php echo $get_Song['general_user_id']; ?>','<?php echo $tip_add_aftr_detils; ?>')"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>
					<?php
							}
						}
					}
					else{
				?>
					<a href="javascript:void(0);" onclick="down_song_pop(<?php echo $i;?>)"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>
				<?php
					}
				}else{
					
				?>	
				<!--<img src="../images/profile/download-over.gif" />-->
				<?php
				}
				?>
				<a href="addtocart.php?seller_id=<?php if(isset($get_Song_title) && $get_Song_title!= null){ echo $get_Song_title['general_user_id'];}else{ echo $get_Song['general_user_id']; }?>&media_id=<?php if(isset($get_Song_title) && $get_Song_title!=null){ echo $get_Song_title['id']; }else{ echo $get_Song['id']; }?>&buyer_id=<?php echo $_SESSION['gen_user_id'];?>&title=<?php if(isset($get_Song_title) && $get_Song_title!=null){ echo str_replace("'","\'",urlencode($get_Song_title['title']));}else{ echo str_replace("'","\'",urlencode($get_Song['title']));}?>&image_name=<?php echo urlencode($song_image_name_cart); ?>" id="download_song<?php echo $i;?>" class="fancybox fancybox.ajax" style="display:none;"></a>
					<script type="text/javascript">
					$(document).ready(function() {
							$("#download_song<?php echo $i;?>").fancybox({
								helpers : {
									media : {},
									buttons : {},
									title : null
								}
							/*'width'				: '75%',
							'height'			: '75%',
							'transitionIn'		: 'none',
							'transitionOut'		: 'none',
							'type'				: 'iframe'*/
							});
					});
					</script>
			  
			  </div>
			  
			  <div style="float:left; margin-left:20px;"><h3><a href="javascript:void(0);" onclick="showPlayer('a','<?php if($get_Song_title['id']!=""){echo $get_Song_title['id'];}else{echo $get_Song['id'];}?>','play');"><?php if($get_media_Info['taggedsongs']!="")
				{
					if($get_Song_title['title']=="")
					{
						echo $get_Song_regis['audio_name'];
						$song_title_cart =$get_Song_regis['audio_name'];
					} 
					else 
					{
						//$create_c = strpos($get_Song_title['creator'],'(');
						//$end = substr($get_Song_title['creator'],0,$create_c);
						$end = $get_Song_title['creator'];

						//$create_f = strpos($get_Song_title['from'],'(');
						//$end_f = substr($get_Song_title['from'],0,$create_f);
						$end_f = $get_Song_title['from'];
						
						$exchos = "";
						if($get_Song['track']!="" && !empty($get_Song['track']))
						{
							$exchos .= "<b>".$get_Song['track']."</b>  ";
						}
						
						if(strlen($get_Song_title['title'])>97)
						{
							$exchos .= "<b>".stripslashes(substr($get_Song_title['title'],0,100))."...</b></a>  ";
						}
						else
						{
							$exchos .= "<b>".stripslashes($get_Song_title['title'])."</b></a>  ";
						}
						
						$total_title_lens = strlen($get_Song_title['title']);
						$ends_lens = 0;
						
						if($total_title_lens<97)
						{
							if($end!="" && !empty($end) && $get_Song_title['creator_info']!="")
							{
								$exp_cr = explode('|',$get_Song_title['creator_info']);
								if($exp_cr[0]=='general_artist')
								{
									$sql_c_ar = mysql_query("SELECT * FROM general_artist WHERE artist_id='".$exp_cr[1]."'");
								}
								if($exp_cr[0]=='general_community')
								{
									$sql_c_ar = mysql_query("SELECT * FROM general_community WHERE community_id='".$exp_cr[1]."'");
								}
								if($exp_cr[0]=='artist_project' || $exp_cr[0]=='community_project' || $exp_cr[0]=='community_event' || $exp_cr[0]=='artist_event')
								{
									$sql_c_ar = mysql_query("SELECT * FROM $exp_cr[0] WHERE id='".$exp_cr[1]."'");
								}
								
								if(isset($sql_c_ar))
								{
									if($sql_c_ar!="" && $sql_c_ar!=Null)
									{
										if(mysql_num_rows($sql_c_ar)>0)
										{
											$ans_c_ar = mysql_fetch_assoc($sql_c_ar);
											if($ans_c_ar['profile_url']!="" && !empty($ans_c_ar['profile_url']))
											{
												$ends_lens = strlen($end);
												if(($total_title_lens + strlen($end))>97)
												{
													$exchos .= "By  <a href='/".$ans_c_ar['profile_url']."'><b>".substr($end,0,98 - $total_title_lens)."...</b></a>  ";
												}
												else
												{
													$exchos .= "By  <a href='/".$ans_c_ar['profile_url']."'><b>".$end."</b></a>  ";
												}
											}
											else
											{
												$ends_lens = strlen($end);
												if(($total_title_lens + strlen($end))>97)
												{
													$exchos .= "By  <b>".substr($end,0,98 - $total_title_lens)."...</b>  ";
												}
												else
												{
													$exchos .= "By  <b>".$end."</b>  ";
												}
											}
										}
										else
										{
											$ends_lens = strlen($end);
											if(($total_title_lens + strlen($end))>97)
											{
												$exchos .= "By  <b>".substr($end,0,98 - $total_title_lens)."...</b>  ";
											}
											else
											{
												$exchos .= "By  <b>".$end."</b>  ";
											}
										}
									}else{
										$ends_lens = strlen($end);
										if(($total_title_lens + strlen($end))>97)
										{
											$exchos .= "By  <b>".substr($end,0,98 - $total_title_lens)."...</b>  ";
										}
										else
										{
											$exchos .= "By  <b>".$end."</b>  ";
										}
									}
								}
								else
								{
									$ends_lens = strlen($end);
									if(($total_title_lens + strlen($end))>97)
									{
										$exchos .= "By  <b>".substr($end,0,98 - $total_title_lens)."...</b>  ";
									}
									else
									{
										$exchos .= "By  <b>".$end."</b>  ";
									}
								}
							}
							elseif($end!="" && !empty($end))
							{
								$ends_lens = strlen($end);
								if($total_title_lens + strlen($end)>97)
								{
									$exchos .= "By  <b>".substr($end,0,98 - $total_title_lens)."...</b>  ";
								}
								else
								{
									$exchos .= "By  <b>".$end."</b>  ";
								}
							}
							
							$totals_tens_lens = $ends_lens + $total_title_lens;
							
							if($ends_lens + $total_title_lens<97)
							{
								if($end_f!="" && !empty($end_f) && $get_Song_title['from_info']!="")
								{
									$exp_fr = explode('|',$get_Song_title['from_info']);
									if($exp_fr[0]=='general_artist')
									{
										$sql_f_ar = mysql_query("SELECT * FROM general_artist WHERE artist_id='".$exp_fr[1]."'");
									}
									if($exp_fr[0]=='general_community')
									{
										$sql_f_ar = mysql_query("SELECT * FROM general_community WHERE community_id='".$exp_fr[1]."'");
									}
									if($exp_fr[0]=='artist_project' || $exp_fr[0]=='community_project' || $exp_fr[0]=='community_event' || $exp_fr[0]=='artist_event')
									{
										$sql_f_ar = mysql_query("SELECT * FROM $exp_fr[0] WHERE id='".$exp_fr[1]."'");
									}
									
									if(isset($sql_f_ar))
									{
										if($sql_f_ar!="" && $sql_f_ar!=Null)
										{
											if(mysql_num_rows($sql_f_ar)>0)
											{
												$ans_f_ar = mysql_fetch_assoc($sql_f_ar);
												
												if($ans_f_ar['profile_url']!="" && !empty($ans_f_ar['profile_url']))
												{
													if(($totals_tens_lens + strlen($end_f))>97)
													{
														$exchos .= "From  <a href='/".$ans_f_ar['profile_url']."'><b>".substr($end_f,0,98 - $totals_tens_lens)."...</b></a>";
													}
													else
													{
														$exchos .= "From  <a href='/".$ans_f_ar['profile_url']."'><b>".$end_f."</b></a>";
													}
												}
												else
												{
													if(($totals_tens_lens + strlen($end_f))>97)
													{
														$exchos .= "From  <b>".substr($end_f,0,98 - $totals_tens_lens)."...</b>";
													}
													else
													{
														$exchos .= "From  <b>".$end_f."</b>";
													}
												}
											}
											else
											{
												if(($totals_tens_lens + strlen($end_f))>97)
												{
													$exchos .= "From  <b>".substr($end_f,0,98 - $totals_tens_lens)."...</b>";
												}
												else
												{
													$exchos .= "From  <b>".$end_f."</b>";
												}
											}
										}else
										{
											if(($totals_tens_lens + strlen($end_f))>97)
											{
												$exchos .= "From  <b>".substr($end_f,0,98 - $totals_tens_lens)."...</b>";
											}
											else
											{
												$exchos .= "From  <b>".$end_f."</b>";
											}
										}
									}
									else
									{
										if(($totals_tens_lens + strlen($end_f))>97)
										{
											$exchos .= "From  <b>".substr($end_f,0,98 - $totals_tens_lens)."...</b>";
										}
										else
										{
											$exchos .= "From  <b>".$end_f."</b>";
										}
									}
								}
								elseif($end_f!="" && !empty($end_f))
								{
									if(($totals_tens_lens + strlen($end_f))>97)
									{
										$exchos .= "From  <b>".substr($end_f,0,98 - $totals_tens_lens)."...</b>";
									}
									else
									{
										$exchos .= "From  <b>".$end_f."</b>";
									}
								}
							}
						}
						echo $exchos;
						$song_title_cart =stripslashes($get_Song_title['title']);
					} 
				}
				else
				{
					//$create_c = strpos($get_Song['creator'],'(');
					//$end = substr($get_Song['creator'],0,$create_c);
					$end = $get_Song['creator'];
					
					//$create_f = strpos($get_Song['from'],'(');
					//$end_f = substr($get_Song['from'],0,$create_f);
					$end_f = $get_Song['from'];
					
					$sql_query = mysql_query("SELECT * FROM media_songs WHERE media_id='".$get_Song['id']."'");
					if($sql_query!="" && $sql_query!=Null)
					{
						if(mysql_num_rows($sql_query)>0)
						{
							$res13=mysql_fetch_assoc($sql_query);
						}
					}
					$exchos_1 = "";
					
					if(isset($res13) && !empty($res13) && $res13['track']!="")
					{
						$exchos_1 .= "<b>".$res13['track']."</b>  ";
					}
					
					if(strlen($get_Song['title'])>97)
					{
						$exchos_1 .= "<b>".stripslashes(substr($get_Song['title'],0,98))."...</b></a>  ";
					}
					else
					{
						$exchos_1 .= "<b>".stripslashes($get_Song['title'])."</b></a>  ";
					}
					
					$total_son_len = strlen($get_Song['title']);
					$ends_sonl = 0;
					
					if($total_son_len<97)
					{
						if($end!="" && !empty($end) && $get_Song['creator_info']!="")
						{
							$exp_cr = explode('|',$get_Song['creator_info']);
							if($exp_cr[0]=='general_artist')
							{
								$sql_c_ar = mysql_query("SELECT * FROM general_artist WHERE artist_id='".$exp_cr[1]."'");
							}
							if($exp_cr[0]=='general_community')
							{
								$sql_c_ar = mysql_query("SELECT * FROM general_community WHERE community_id='".$exp_cr[1]."'");
							}
							if($exp_cr[0]=='artist_project' || $exp_cr[0]=='community_project' || $exp_cr[0]=='community_event' || $exp_cr[0]=='artist_event')
							{
								$sql_c_ar = mysql_query("SELECT * FROM $exp_cr[0] WHERE id='".$exp_cr[1]."'");
							}
							if($sql_c_ar!="" && $sql_c_ar!=Null)
							{
								if(mysql_num_rows($sql_c_ar)>0)
								{
									$ans_c_ar = mysql_fetch_assoc($sql_c_ar);
									if($ans_c_ar['profile_url']!="" && !empty($ans_c_ar['profile_url']))
									{
										$ends_sonl = strlen($end);
										if(($total_son_len + $ends_sonl)>97)
										{
											$exchos_1 .= "By  <a href='/".$ans_c_ar['profile_url']."'><b>".substr($end,0,98 - $total_son_len)."...</b></a>  ";
										}
										else
										{
											$exchos_1 .= "By  <a href='/".$ans_c_ar['profile_url']."'><b>".$end."</b></a>  ";
										}
									}
									else
									{
										$ends_sonl = strlen($end);
										if(($total_son_len + $ends_sonl)>97)
										{
											$exchos_1 .= "By  <b>".substr($end,0,98 - $total_son_len)."...</b>  ";
										}
										else
										{
											$exchos_1 .= "By  <b>".$end."</b>  ";
										}
									}
								}
								else
								{
									$ends_sonl = strlen($end);
									if(($total_son_len + $ends_sonl)>97)
									{
										$exchos_1 .= "By  <b>".substr($end,0,98 - $total_son_len)."...</b>  ";
									}
									else
									{
										$exchos_1 .= "By  <b>".$end."</b>  ";
									}
								}
							}else
							{
								$ends_sonl = strlen($end);
								if(($total_son_len + $ends_sonl)>97)
								{
									$exchos_1 .= "By  <b>".substr($end,0,98 - $total_son_len)."...</b>  ";
								}
								else
								{
									$exchos_1 .= "By  <b>".$end."</b>  ";
								}
							}
						}
						elseif($end!="" && !empty($end))
						{
							$ends_sonl = strlen($end);
							if(($total_son_len + $ends_sonl)>97)
							{
								$exchos_1 .= "By  <b>".substr($end,0,98 - $total_son_len)."...</b>  ";
							}
							else
							{
								$exchos_1 .= "By  <b>".$end."</b>  ";
							}
						}
						
						$total_endl_t = $total_son_len + $ends_sonl;
						
						if(($total_son_len + $ends_sonl)<97)
						{
							if($end_f!="" && !empty($end_f) && $get_Song['from_info']!="")
							{
								$exp_fr = explode('|',$get_Song['from_info']);
								if($exp_fr[0]=='general_artist')
								{
									$sql_f_ar = mysql_query("SELECT * FROM general_artist WHERE artist_id='".$exp_fr[1]."'");
								}
								if($exp_fr[0]=='general_community')
								{
									$sql_f_ar = mysql_query("SELECT * FROM general_community WHERE community_id='".$exp_fr[1]."'");
								}
								if($exp_fr[0]=='artist_project' || $exp_fr[0]=='community_project' || $exp_fr[0]=='community_event' || $exp_fr[0]=='artist_event')
								{
									$sql_f_ar = mysql_query("SELECT * FROM $exp_fr[0] WHERE id='".$exp_fr[1]."'");
								}
								if($sql_f_ar!="" && $sql_f_ar!=Null)
								{
									if(mysql_num_rows($sql_f_ar)>0)
									{
										$ans_f_ar = mysql_fetch_assoc($sql_f_ar);
										if($ans_f_ar['profile_url']!="" && !empty($ans_f_ar['profile_url']))
										{
											if(($total_endl_t + strlen($end_f))>97)
											{
												$exchos_1 .= "From  <a href='/".$ans_f_ar['profile_url']."'><b>".substr($end_f,0,98 - $total_endl_t)."...</b></a>";
											}
											else
											{
												$exchos_1 .= "From  <a href='/".$ans_f_ar['profile_url']."'><b>".$end_f."</b></a>";
											}
										}
										else
										{
											if(($total_endl_t + strlen($end_f))>97)
											{
												$exchos_1 .= "From  <b>".substr($end_f,0,98 - $total_endl_t)."...</b>";
											}
											else
											{
												$exchos_1 .= "From  <b>".$end_f."</b>";
											}
										}
									}
									else
									{
										if(($total_endl_t + strlen($end_f))>97)
										{
											$exchos_1 .= "From  <b>".substr($end_f,0,98 - $total_endl_t)."...</b>";
										}
										else
										{
											$exchos_1 .= "From  <b>".$end_f."</b>";
										}
									}
								}
								else
								{
									if(($total_endl_t + strlen($end_f))>97)
									{
										$exchos_1 .= "From  <b>".substr($end_f,0,98 - $total_endl_t)."...</b>";
									}
									else
									{
										$exchos_1 .= "From  <b>".$end_f."</b>";
									}
								}
							}
							elseif($end_f!="" && !empty($end_f))
							{
								if(($total_endl_t + strlen($end_f))>97)
								{
									$exchos_1 .= "From  <b>".substr($end_f,0,98 - $total_endl_t)."...</b>";
								}
								else
								{
									$exchos_1 .= "From  <b>".$end_f."</b>";
								}
							}
						}
					}
					echo $exchos_1;
				}?></h3>
				<?php /* $get_profile_type = $display->types_of_search('media',$get_Song['id']);
												if(!empty($get_profile_type) && $get_profile_type!=Null)
												{
													$res_profile_type = mysql_fetch_assoc($get_profile_type);
													if($res_profile_type['type_name']!=""){
				?>
														<br/><span style="font-size:13px; font-weight:normal;">
				<?php 
														echo $res_profile_type['type_name'];
														if($res_profile_type['subtype_name']!=""){
															echo ", ".$res_profile_type['subtype_name'];
														}
				?>
														</span>
				<?php
													}
												} */ ?> </div>
			  
			</div>
		<?php
				if($get_Song_title['id']!="")
				{
					$get_all_id_song = $get_all_id_song.",".$get_Song_title['id'];
				}
				else{
				$get_all_id_song = $get_all_id_song.",".$get_Song['id'];
				}				
			}
		}
	}
	
	?><input type="hidden" id="all_song_id" value ="<?php echo $get_all_id_song;?>"></input>
      </div>      
    </div>
    <?php
	
	}
	else
	{
	?>
	    <div id="eventsTab" class="hiddenBlock2"></div>
	<?php
	}
	
	if($get_media_Info['taggedvideos']!="" || $get_media_Info['tagged_videos']!="")
	{
		
		//else
		//{
		?>
			<div id="mediaTab" class="hiddenBlock1">
		<?php
		//}
	?>
      <div id="profileFeatured">
		<div class="playall">
			Play All
		</div>
		<?php
		if($artist_check !=0 || $community_check !=0)
		{
		?>
			<a title="Play all of the media on this profile page." href="javascript:void(0);"><img src="../images/profile/play.png" width="35" height="23" onmouseover="this.src='../images/profile/play-over.png'" onmouseout="this.src='../images/profile/play.png'" onclick="PlayAll('v','play_all_video_id')" /></a>
			<a title="Add all of the media on this profile page to your playlist without stopping your current file." href="javascript:void(0);"><img src="../images/profile/add.png" width="27" height="23" onmouseover="this.src='../images/profile/add-over.png'" onmouseout="this.src='../images/profile/add.png'" onclick="PlayAll('v','play_all_video_id')" /></a>
		<?php
		}
		elseif($artist_event_check !=0 || $community_event_check !=0)
		{
		?>
			<a title="Play all of the media on this events page." href="javascript:void(0);"><img src="../images/profile/play.png" width="35" height="23" onmouseover="this.src='../images/profile/play-over.png'" onmouseout="this.src='../images/profile/play.png'" onclick="PlayAll('v','play_all_video_id')" /></a>
			<a title="Add all of the media on this events page to your playlist without stopping your current file." href="javascript:void(0);"><img src="../images/profile/add.png" width="27" height="23" onmouseover="this.src='../images/profile/add-over.png'" onmouseout="this.src='../images/profile/add.png'" onclick="PlayAll('v','play_all_video_id')" /></a>
		<?php
		}
		elseif($artist_project_check !=0 || $community_project_check !=0)
		{
		?>
			<a title="Play all of the media on this projects page." href="javascript:void(0);"><img src="../images/profile/play.png" width="35" height="23" onmouseover="this.src='../images/profile/play-over.png'" onmouseout="this.src='../images/profile/play.png'" onclick="PlayAll('v','play_all_video_id')" /></a>
			<a title="Add all of the media on this projects page to your playlist without stopping your current file." href="javascript:void(0);"><img src="../images/profile/add.png" width="27" height="23" onmouseover="this.src='../images/profile/add-over.png'" onmouseout="this.src='../images/profile/add.png'" onclick="PlayAll('v','play_all_video_id')" /></a>
		<?php
		}
		?>
	</div>
	<div id="profileFeatured">
			
			<?php //echo $getuser['featured_media'].$getuser['media_id'];
			
			if($artist_check !=0 || $artist_event_check !=0 || $artist_project_check !=0  )
			{
				$table_type = "artist";
			}
			if($community_check !=0 || $community_event_check !=0 || $community_project_check !=0  )
			{
				$table_type = "community";
			}
			if($getuser['featured_media'] != 'nodisplay')
			{
				if($getuser['featured_media'] == "Gallery" && $getuser['media_id']!=0)
				{
					$find_whether_regis=$new_profile_class_obj->get_count_Gallery_at_register($getuser["$table_type".'_id'],$getuser['media_id']);
					//var_dump($find_whether_regis);
					if($find_whether_regis["count(*)"]>1)
					{
						$getimages=$new_profile_class_obj->get_featured_media_gallery_at_register($getuser['media_id'],$table_type);
						$feature_image_name = array();
						$feature_image_title = array();
						$feature_count = 0;
						while($get_image = mysql_fetch_assoc($getimages))
						{
							$feature_image_name[$feature_count] = $get_image['image_name'];
							$feature_image_title[$feature_count] = $get_image['image_title'];
							$feature_count++;
						}
						$feature_big_img = $feature_image_name[0];
						$featured_gal_img = implode(",", $feature_image_name);
						$featured_gal_img_title = implode(",", $feature_image_title);
						$feature_org_path = "https://reggallery.s3.amazonaws.com/";
						$feature_thum_path = "https://reggalthumb.s3.amazonaws.com/";
					}
					else
					{
						$getimages=$new_profile_class_obj->get_featured_media_gallery($getuser['media_id']);
						$feature_image_name = array();
						$feature_image_title = array();
						$feature_count = 0;
						while($get_image = mysql_fetch_assoc($getimages))
						{
							$feature_image_name[$feature_count] = $get_image['image_name'];
							$feature_image_title[$feature_count] = $get_image['image_title'];
							$feature_count++;
						}
						$feature_big_img = $feature_image_name[0];
						$featured_gal_img = implode(",", $feature_image_name);
						$featured_gal_img_title = implode(",", $feature_image_title);
						$feature_org_path = "https://medgallery.s3.amazonaws.com/";
						$feature_thum_path = "https://medgalthumb.s3.amazonaws.com/";
					}
				?>
					<div class="fmedia">featured Media</div>
					<?php
					if($artist_check !=0 || $community_check !=0)
					{
					?>
						<a title="Play this artists featured media." href="javascript:void(0);"><img src="../images/profile/play.png" width="35" height="23" onmouseover="this.src='../images/profile/play-over.png'" onmouseout="this.src='../images/profile/play.png'" onclick="checkload('<?php echo $feature_big_img;?>','<?php echo $featured_gal_img; ?>','<?php echo $feature_thum_path; ?>','<?php echo $feature_org_path; ?>','<?php echo $featured_gal_img_title; ?>')" /></a>
						<a title="Add this artists featured media to your playlist without stopping your current file." href="javascript:void(0);"><img src="../images/profile/add.png" width="27" height="23" onmouseover="this.src='../images/profile/add-over.png'" onmouseout="this.src='../images/profile/add.png'" /></a>
					<?php
					}
					elseif($artist_event_check !=0 || $community_event_check !=0)
					{
					?>
						<a title="Play this events featured media." href="javascript:void(0);"><img src="../images/profile/play.png" width="35" height="23" onmouseover="this.src='../images/profile/play-over.png'" onmouseout="this.src='../images/profile/play.png'" onclick="checkload('<?php echo $feature_big_img;?>','<?php echo $featured_gal_img; ?>','<?php echo $feature_thum_path; ?>','<?php echo $feature_org_path; ?>','<?php echo $featured_gal_img_title; ?>')" /></a>
						<a title="Add this events featured media to your playlist without stopping your current file." href="javascript:void(0);"><img src="../images/profile/add.png" width="27" height="23" onmouseover="this.src='../images/profile/add-over.png'" onmouseout="this.src='../images/profile/add.png'" /></a>
					<?php
					}
					elseif($artist_project_check !=0 || $community_project_check !=0)
					{
					?>
						<a title="Play this projects featured media." href="javascript:void(0);"><img src="../images/profile/play.png" width="35" height="23" onmouseover="this.src='../images/profile/play-over.png'" onmouseout="this.src='../images/profile/play.png'" onclick="checkload('<?php echo $feature_big_img;?>','<?php echo $featured_gal_img; ?>','<?php echo $feature_thum_path; ?>','<?php echo $feature_org_path; ?>','<?php echo $featured_gal_img_title; ?>')" /></a>
						<a title="Add this projects featured media to your playlist without stopping your current file." href="javascript:void(0);"><img src="../images/profile/add.png" width="27" height="23" onmouseover="this.src='../images/profile/add-over.png'" onmouseout="this.src='../images/profile/add.png'" /></a>
					<?php
					}
					?>
					
				<?php
				}
				if($getuser['featured_media'] == "Song" && $getuser['media_id']!=0)
				{
				?>
					<div class="fmedia">featured Media</div>
					<?php
					if($artist_check !=0 || $community_check !=0)
					{
					?>
						<a title="Play this artists featured media." href="javascript:void(0);"><img src="../images/profile/play.png" width="35" height="23" onmouseover="this.src='../images/profile/play-over.png'" onmouseout="this.src='../images/profile/play.png'" onclick="showPlayer('a','<?php echo $getuser['media_id'];?>','play')" /></a>
						<a title="Add this artists featured media to your playlist without stopping your current file." href="javascript:void(0);"><img src="../images/profile/add.png" width="27" height="23" onmouseover="this.src='../images/profile/add-over.png'" onmouseout="this.src='../images/profile/add.png'" onclick="showPlayer('a','<?php echo $getuser['media_id'];?>','add')" /></a>
				<?php
					}
					elseif($artist_event_check !=0 || $community_event_check !=0)
					{
				?>
						<a title="Play this events featured media." href="javascript:void(0);"><img src="../images/profile/play.png" width="35" height="23" onmouseover="this.src='../images/profile/play-over.png'" onmouseout="this.src='../images/profile/play.png'" onclick="showPlayer('a','<?php echo $getuser['media_id'];?>','play')" /></a>
						<a title="Add this events featured media to your playlist without stopping your current file." href="javascript:void(0);"><img src="../images/profile/add.png" width="27" height="23" onmouseover="this.src='../images/profile/add-over.png'" onmouseout="this.src='../images/profile/add.png'" onclick="showPlayer('a','<?php echo $getuser['media_id'];?>','add')" /></a>
				<?php
					}
					elseif($artist_project_check !=0 || $community_project_check !=0)
					{
				?>
						<a title="Play this projects featured media." href="javascript:void(0);"><img src="../images/profile/play.png" width="35" height="23" onmouseover="this.src='../images/profile/play-over.png'" onmouseout="this.src='../images/profile/play.png'" onclick="showPlayer('a','<?php echo $getuser['media_id'];?>','play')" /></a>
						<a title="Add this projects featured media to your playlist without stopping your current file." href="javascript:void(0);"><img src="../images/profile/add.png" width="27" height="23" onmouseover="this.src='../images/profile/add-over.png'" onmouseout="this.src='../images/profile/add.png'" onclick="showPlayer('a','<?php echo $getuser['media_id'];?>','add')" /></a>
				<?php
					}
				}
				?>
				<?php
				if($getuser['featured_media'] == "Video" && $getuser['media_id']!=0)
				{
				?>
					<div class="fmedia">featured Media</div>
					<?php
					if($artist_check !=0 || $community_check !=0)
					{
					?>
						<a title="Play this artists featured media." href="javascript:void(0);"><img src="../images/profile/play.png" width="35" height="23" onmouseover="this.src='../images/profile/play-over.png'" onmouseout="this.src='../images/profile/play.png'" onclick="showPlayer('v','<?php echo $getuser['media_id'];?>','playlist')" /></a>
						<a title="Add this artists featured media to your playlist without stopping your current file." href="javascript:void(0);"><img src="../images/profile/add.png" width="27" height="23" onmouseover="this.src='../images/profile/add-over.png'" onmouseout="this.src='../images/profile/add.png'" onclick="showPlayer('v','<?php echo $getuser['media_id'];?>','addvi')" /></a>
				<?php
					}
					elseif($artist_event_check !=0 || $community_event_check !=0)
					{
				?>
						<a title="Play this events featured media." href="javascript:void(0);"><img src="../images/profile/play.png" width="35" height="23" onmouseover="this.src='../images/profile/play-over.png'" onmouseout="this.src='../images/profile/play.png'" onclick="showPlayer('v','<?php echo $getuser['media_id'];?>','playlist')" /></a>
						<a title="Add this events featured media to your playlist without stopping your current file." href="javascript:void(0);"><img src="../images/profile/add.png" width="27" height="23" onmouseover="this.src='../images/profile/add-over.png'" onmouseout="this.src='../images/profile/add.png'" onclick="showPlayer('v','<?php echo $getuser['media_id'];?>','addvi')" /></a>
				<?php
					}
					elseif($artist_project_check !=0 || $community_project_check !=0)
					{
				?>
						<a title="Play this projects featured media." href="javascript:void(0);"><img src="../images/profile/play.png" width="35" height="23" onmouseover="this.src='../images/profile/play-over.png'" onmouseout="this.src='../images/profile/play.png'" onclick="showPlayer('v','<?php echo $getuser['media_id'];?>','playlist')" /></a>
						<a title="Add this projects featured media to your playlist without stopping your current file." href="javascript:void(0);"><img src="../images/profile/add.png" width="27" height="23" onmouseover="this.src='../images/profile/add-over.png'" onmouseout="this.src='../images/profile/add.png'" onclick="showPlayer('v','<?php echo $getuser['media_id'];?>','addvi')" /></a>
				<?php
					}
				}
			}
			?>
			
		</div>
      <!--<h2><a href="#">Play All Videos</a></h2>-->
	  <h2></h2>
      <div class="rowItem">
	  <?php		
		$get_Videosort = array();
		for($i=0;$i<count($get_video_pro_uniq_tabs_chk);$i++)
		{
			if($artist_check !=0 || $community_check!=0)
			{
				$get_Videosort[] =$new_profile_class_obj->get_Video_title($get_video_pro_uniq_tabs_chk[$i]);
			}
			elseif($artist_check ==0 && $community_check==0)
			{
				$get_Videosort[] =$new_profile_class_obj->get_Video($get_video_pro_uniq_tabs_chk[$i]);
			}
			
		}
		
		$sort = array();
		foreach($get_Videosort as $k=>$v)
		{		
			$sort['play_count'][$k] = $v['play_count'];
			//$sort['from'][$k] = $end_f;
			//$sort['track'][$k] = $v['track'];
			//$sort['title'][$k] = $v['title'];
		}
		//var_dump($sort);
		if(!empty($sort))
		{
			array_multisort($sort['play_count'], SORT_DESC,$get_Videosort);
		}
		
		$dummy_vp = array();
		include_once("findsale_video.php");
		for($i=0;$i<count($get_Videosort);$i++)
		{
			if(in_array($get_Videosort[$i]['id'],$dummy_vp))
			{}
			else
			{
				$dummy_vp[] = $get_Videosort[$i]['id'];
			$get_Video=$new_profile_class_obj->get_Video($get_Videosort[$i]['id']);
			if(isset($get_Video))
			{
				$src_list ="https://comprothumjcrop.s3.amazonaws.com/";
				$from_list_pic ="";
				$creator_list_pic ="";
				if($get_Video['from_info']!="")
				{
					$expl_from_in =explode("|",$get_Video['from_info']);
					$get_from_list_pic = $new_profile_class_obj->get_from_list_pic($expl_from_in[0],$expl_from_in[1]);
					if($get_from_list_pic['listing_image_name'] =="")
					{
						$from_list_pic="https://artjcropthumb.s3.amazonaws.com/Noimage.png";
					}
					else
					{
						$from_list_pic = $get_from_list_pic['listing_image_name'];
					}
				}
				if($get_Video['from_info'] =="" && $get_Video['creator_info']!="")
				{
					$expl_creator_in =explode("|",$get_Video['creator_info']);
					if($expl_creator_in[0] == "general_community")
					{
						$col_name = "community_id";
						$creator_src = "https://comjcropthumb.s3.amazonaws.com/";
					}
					else if($expl_creator_in[0] == "general_artist")
					{
						$col_name = "artist_id";
						$creator_src = "https://artjcropthumb.s3.amazonaws.com/";
					}
					else
					{
						$col_name = "id";
						$creator_src = "";
					}
					$get_creator_list_pic = $new_profile_class_obj->get_creator_list_pic($expl_creator_in[0],$expl_creator_in[1],$col_name);
					if($get_creator_list_pic['listing_image_name'] =="")
					{
						$creator_list_pic = $creator_src."Noimage.png";
					}
					else
					{
						$creator_list_pic = $creator_src.$get_creator_list_pic['listing_image_name'];
					}
				}
			}
			if($get_media_Info['taggedvideos']!="")
			{
				$get_Video_title=$new_profile_class_obj->get_Video_title($get_Videosort[$i]['id']);
				$from_list_pic ="";
				$creator_list_pic ="";

				if($get_Video_title['from_info']!="")
				{
					$expl_from_in =explode("|",$get_Video_title['from_info']);
					$get_from_list_pic = $new_profile_class_obj->get_from_list_pic($expl_from_in[0],$expl_from_in[1]);
					if($get_from_list_pic['listing_image_name'] =="")
					{
						$from_list_pic="https://artjcropthumb.s3.amazonaws.com/Noimage.png";
					}
					else
					{
						$from_list_pic = $get_from_list_pic['listing_image_name'];
					}
				}
				if($get_Video_title['from_info'] =="" && $get_Video_title['creator_info']!="")
				{
					$expl_creator_in =explode("|",$get_Video_title['creator_info']);
					if($expl_creator_in[0] == "general_community")
					{
						$col_name = "community_id";
						$creator_src = "https://comjcropthumb.s3.amazonaws.com/";
					}
					else if($expl_creator_in[0] == "general_artist")
					{
						$col_name = "artist_id";
						$creator_src = "https://artjcropthumb.s3.amazonaws.com/";
					}
					else
					{
						$col_name = "id";
						$creator_src = "";
					}
					$get_creator_list_pic = $new_profile_class_obj->get_creator_list_pic($expl_creator_in[0],$expl_creator_in[1],$col_name);
					if($get_creator_list_pic['listing_image_name'] =="")
					{
						$creator_list_pic = $creator_src."Noimage.png";
					}
					else
					{
						$creator_list_pic = $creator_src.$get_creator_list_pic['listing_image_name'];
					}
				}
			}
	if($get_Video_title['title'] !="" || $get_Video['title'] !="") 
	{
	//var_dump($get_Video_title);
	//echo $get_Video['id'];
	  ?>
	  
        <div class="tabItem">
		<div style="position:relative; float:left; height: 200px;">
		<a href="javascript:void(0);" onclick="showPlayer('v','<?php if($get_Video_title['id']!=""){echo $get_Video_title['id'];}else{ echo $get_Video['id'];}?>','playlist');" >
		<!--<img src="<?php /*
		if(isset($from_list_pic) && $from_list_pic!="")
		{
			echo $from_list_pic;
			$image_name_cart = $from_list_pic;
		}
		else if(isset($creator_list_pic) && $creator_list_pic!="")
		{
			echo $creator_list_pic;
			$image_name_cart =$creator_list_pic;
		}
		else
		{
		echo $src_list."Noimage.png";
		$image_name_cart = $src_list."Noimage.png";
		}*/?>" width="200" height="200" />
		Old Image
		-->
		<?php if($get_Video_title['id']!=""){ 
				$media_id = $get_Video_title['id'];
			}else{ 
				$media_id = $get_Video['id'];
			}
			
			$get_video_image = $new_profile_class_obj->getvideoimage($media_id);
			
			if(!empty($get_video_image))
			{
				if($get_video_image[1]=='youtube')
				{
?>
						<img onMouseOver="newfunction('<?php if($get_Video_title['id']!=""){echo $get_Video_title['id'];}else{ echo $get_Video['id']; } ?>')" onMouseOut="newfunctionout('<?php if($get_Video_title['id']!=""){echo $get_Video_title['id'];}else{ echo $get_Video['id']; } ?>')" class="imgs_pro" src="https://img.youtube.com/vi/<?php echo $get_video_image[0]; ?>/default.jpg" width="200" height="200"  title="Add file to the bottom of your playlist without stopping current media."/>
					</a>
<?php
				}
				elseif($get_video_image[1]=='vimeo' && $get_video_image[0]!=NULL)
				{
					/* $url = 'http://vimeo.com/api/v2/video/'.$get_video_image[0].'.php';
					$contents = file_get_contents($url);
					$thumb = unserialize(trim($contents));
					$image_name_cart = $thumb[0]['thumbnail_medium']; */
					
					$video = $vimeo->call('vimeo.videos.getInfo', array('video_id' => $get_video_image[0]));
					$image_name_cart = $video->video[0]->thumbnails->thumbnail[1]->_content;
?>
						<img onMouseOver="newfunction('<?php if($get_Video_title['id']!=""){echo $get_Video_title['id'];}else{ echo $get_Video['id']; } ?>')" onMouseOut="newfunctionout('<?php if($get_Video_title['id']!=""){echo $get_Video_title['id'];}else{ echo $get_Video['id']; } ?>')" class="imgs_pro" src="<?php echo $image_name_cart; ?>" width="200" height="200"  title="Add file to the bottom of your playlist without stopping current media."/>
					</a>
<?php
				}
				else
				{
?>
						<img onMouseOver="newfunction('<?php if($get_Video_title['id']!=""){echo $get_Video_title['id'];}else{ echo $get_Video['id']; } ?>')" onMouseOut="newfunctionout('<?php if($get_Video_title['id']!=""){echo $get_Video_title['id'];}else{ echo $get_Video['id']; } ?>')" class="imgs_pro" src="<?php echo 'https://comjcropprofile.s3.amazonaws.com/Noimage.png'; ?>" width="200" height="200"  title="Add file to the bottom of your playlist without stopping current media."/>
					</a>
<?php
				}
			}
			else
			{
?>
					<img onMouseOver="newfunction('<?php if($get_Video_title['id']!=""){echo $get_Video_title['id'];}else{ echo $get_Video['id']; } ?>')" onMouseOut="newfunctionout('<?php if($get_Video_title['id']!=""){echo $get_Video_title['id'];}else{ echo $get_Video['id']; } ?>')" class="imgs_pro" src="<?php echo 'https://comjcropprofile.s3.amazonaws.com/Noimage.png'; ?>" width="200" height="200"  title="Add file to the bottom of your playlist without stopping current media."/>
				</a>
<?php
			}
		?>
		<div class="navButton">
			<div class="playMedia">
				<a class="list_play" href="javascript:void(0);"><img onMouseOver="newfunction('<?php if($get_Video_title['id']!=""){echo $get_Video_title['id'];}else{ echo $get_Video['id']; } ?>')" onMouseOut="newfunctionout('<?php if($get_Video_title['id']!=""){echo $get_Video_title['id'];}else{ echo $get_Video['id']; } ?>')" src="/images/listing/play_youtube.png" onclick="showPlayer('v','<?php if($get_Video_title['id']!=""){echo $get_Video_title['id'];}else{ echo $get_Video['id'];}?>','playlist');" id="play_video_image<?php if($get_Video_title['id']!=""){echo $get_Video_title['id'];}else{ echo $get_Video['id']; } ?>" title="Add file to the bottom of your playlist without stopping current media." /></a>
			</div>
		</div>
		</div>
		<div class="both_wrap_per">
			<div class="player" >
				<a href="javascript:void(0);" ><img src="../images/profile/play.gif"  onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'"  onclick="showPlayer('v','<?php if($get_Video_title['id']!=""){echo $get_Video_title['id'];}else{ echo $get_Video['id'];}?>','playlist');" title="Play file now."/></a>
				<a href="javascript:void(0);"><img src="../images/profile/add.gif"  onmouseover="this.src='../images/profile/add-over.gif'" onmouseout="this.src='../images/profile/add.gif'" onclick="showPlayer('v','<?php if($get_Video_title['id']!=""){echo $get_Video_title['id'];}else{ echo $get_Video['id'];}?>','addvi');" title="Add file to the bottom of your playlist without stopping current media."/></a>
				<?php
				if($get_Video_title['sharing_preference'] == 5 || $get_Video_title['sharing_preference'] == 2 || $get_Video['sharing_preference'] == 5 || $get_Video['sharing_preference'] == 2)
				{
					$free_down_rel_video="";
					for($find_down=0;$find_down<=count($get_vids_pnew_w);$find_down++)
					{
						if(isset($get_Video_title) && $get_Video_title != null){
							if($get_vids_pnew_w[$find_down]['id']==$get_Video_title['id']){
								$free_down_rel_video = "yes";
							}
							else{
								if($free_down_rel_video =="" || $free_down_rel_video =="no"){
									$free_down_rel_video = "no";
								}
							}
						}else if(isset($get_Video) && $get_Video != null){
							if($get_vids_pnew_w[$find_down]['id']==$get_Video['id']){
								$free_down_rel_video = "yes";
							}
							else{
								if($free_down_rel_video =="" || $free_down_rel_video =="no"){
									$free_down_rel_video = "no";
								}
							}
						}
					}
					if($free_down_rel_video == "yes"){
					 ?>
					<!-- <a href="javascript:void(0);" onclick="download_directly_song('<?php if($get_Video_title['id']!=""){echo $get_Video_title['id'];}else{ echo $get_Video['id'];}?>','<?php if($get_Video_title['id']!=""){echo str_replace("'","\'",$get_Video_title['title']);}else{ echo str_replace("'","\'",$get_Video['title']);}?>','<?php if($get_Video_title['id']!=""){echo str_replace("'","\'",$get_Video_title['creator']);}else{ echo str_replace("'","\'",$get_Video['creator']);}?>','<?php if($get_Video_title['id']!=""){echo str_replace("'","\'",$get_Video_title['from']);}else{ echo str_replace("'","\'",$get_Video['from']);}?>')"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>
					<img src="../images/profile/download-over.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download-over.gif'" title="Add media to your cart for free download or purchase." />-->
					 
				 <?php
					}
					else if($get_Video_title['sharing_preference']==2  || $get_Video['sharing_preference']==2){
						if($get_Video_title['id']!=""){
						?>
					<!--	<a href="javascript:void(0);" onclick="ask_to_donate('<?php echo str_replace("'","\'",$get_Video_title['title']);?>','<?php echo str_replace("'","\'",$get_Video_title['creator']);?>','<?php echo $get_Video_title['id'];?>','download_song<?php echo $i;?>')"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>
					<img src="../images/profile/download-over.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download-over.gif'" title="Add media to your cart for free download or purchase." />-->
					<?php
						}else{
						?>
					<!--	<a href="javascript:void(0);" onclick="ask_to_donate('<?php echo str_replace("'","\'",$get_Video['title']);?>','<?php echo str_replace("'","\'",$get_Video['creator']);?>','<?php echo $get_Video['id'];?>','download_song<?php echo $i;?>')"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>
						<img src="../images/profile/download-over.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download-over.gif'" title="Add media to your cart for free download or purchase." />-->
					<?php
						}
					}
					else{
				?>
				<!--	<a href="javascript:void(0);" onclick="down_video_pop(<?php echo $i;?>)"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>
				<img src="../images/profile/download-over.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download-over.gif'" title="Add media to your cart for free download or purchase." />-->
				<?php
					}
				}else{
				?>
					<!--<img src="../images/profile/download-over.gif" />-->
				<?php
				}
				?>
				
				<a href="addtocart.php?seller_id=<?php if(isset($get_Video_title) && $get_Video_title != null){ echo $get_Video_title['general_user_id'];}else { echo $get_Video['general_user_id']; }?>&media_id=<?php if(isset($get_Video_title) && $get_Video_title!= null){ echo $get_Video_title['id']; }else{ echo $get_Video['id']; }?>&buyer_id=<?php echo $_SESSION['gen_user_id'];?>&title=<?php echo str_replace("'","\'",$title_cart);?>&image_name=<?php echo $image_name_cart;?>" id="download_video<?php echo $i;?>" style="display:none;"></a>
				<script type="text/javascript">
				$(document).ready(function() {
						$("#download_video<?php echo $i;?>").fancybox({
						'height'			: '75%',
						'width'				: '69%',
						'transitionIn'		: 'none',
						'transitionOut'		: 'none',
						'type'				: 'iframe'
						});
				});
				</script>
			  </div>
		  <div class="type_subtype"><a href="javascript:void(0);" onclick="showPlayer('v','<?php if($get_Video_title['id']!=""){ echo $get_Video_title['id'];}else{ echo $get_Video['id'];}?>','playlist');">
		  <?php if($get_media_Info['taggedvideos']!="") {
					if(strlen($get_Video_title['title'])>22){
						echo substr($get_Video_title['title'],0,19)."...<br/>";
					}else{
						echo $get_Video_title['title']."<br/>";
					}
					$title_cart= $get_Video_title['title'];
				
					$by_cret = $display->get_creator_by($get_Video_title['id']);
					
				} else { 
					if(strlen($get_Video['title'])>22){
						echo substr($get_Video['title'],0,19)."...<br/>";
					}else{
						echo $get_Video['title']."<br/>"; 
					}
					$title_cart=$get_Video['title']; 
				
					$by_cret = $display->get_creator_by($get_Video['id']);
				}
				
				if($by_cret[1]!="")
				{ 
					$purl_ps = $by_cret[1]; 
				}				
				else
				{ 
					$purl_ps = 'javascript:void(0)';
				}
				
				if($get_Video_title['id']!="")
				{ 
					$id_vids = $get_Video_title['id'];
				}
				else
				{ 
					$id_vids = $get_Video['id'];
				}
		?></a><div class="by_details"><?php		
				if($by_cret[0]!="")
				{
					if(strlen($by_cret[0])>=19)
					{
						echo "by <a href='".$purl_ps."'>".substr($by_cret[0],0,15)."</a>...";
					}
					else
					{
						echo "by <a href='".$purl_ps."'>".$by_cret[0]."</a>";
					}
				}
			?></div><?php $get_profile_type = $display->types_of_search('media',$id_vids);
												if(!empty($get_profile_type) && $get_profile_type!=Null)
												{
													$res_profile_type = mysql_fetch_assoc($get_profile_type);
													if($res_profile_type['type_name']!=""){
				?>
														<span style="font-size:13px; font-weight:normal;">
				<?php 
														if(strlen($res_profile_type['subtype_name'])>26){
															echo substr($res_profile_type['subtype_name'],0,24)."...";
														}else{
															echo $res_profile_type['subtype_name'];
														}
														if($res_profile_type['metatype_name']!="" && strlen($res_profile_type['subtype_name']<26)){
															$type_length = strlen($res_profile_type['subtype_name']);
															if($type_length + strlen($res_profile_type['metatype_name'])>22){
																echo ", ".substr($res_profile_type['metatype_name'],0,22 - $type_length)."...";
															}else{
																echo ", ".$res_profile_type['metatype_name'];
															}
															
														}
				?>
														</span>
				<?php
													}
												} ?></div>
												</div>
        </div>
		<?php
		if($get_Video_title['id']!=""){$play_all_video = $play_all_video.",".$get_Video_title['id'];}else{ $play_all_video = $play_all_video.",".$get_Video['id'];}
		
		//$get_Video_title ==""; $get_Video=="";
		}
		}
		}
		?>
		<input type="hidden" id="play_all_video_id" value="<?php echo $play_all_video;?>"/>
      </div>      
    </div>    
    <?php
	}
	else
	{
		
		//else
		//{
		?>
			<div id="mediaTab" class="hiddenBlock1"></div>
		<?php
		//}
	}
	
//if($artist_event_check==0 && $community_event_check==0)
//{
?>
<script>
function newfunction(id)
{
	//var image = document.getElementById("play_video_image"+id);
	//image.src="/images/profile/play_hover.png";
	$("#play_video_image"+id).css({"background":"none repeat scroll 0 0 #000000","border-radius":"6px 6px 6px 6px"});
}function newfunctionout(id)
{
	//var image = document.getElementById("play_video_image"+id);
	//image.src="/images/profile/play_youtube.png";
	$("#play_video_image"+id).css({"background":"none repeat scroll 0 0 #333333","border-radius":"6px 6px 6px 6px"});
}
</script>
	
	<?php
			if($artist_check !=0 || $community_check !=0)
			{
			?>
				<div id="servicesTab" class="hiddenBlock4">
					<div id="profileFeaturedproject" style="float: right;
    height: 55px;
    margin-right: 17px;
    position: relative;
    top: 19px;">
			<?php
			}
			else
			{
			?>
				<div id="friendsTab" class="hiddenBlock5">
					<div id="profileFeaturedproject" style="float: right;
    height: 55px;
    margin-right: 17px;
    position: relative;
    top: 19px;">
			<?php
			}
			?>			
		
			<div class="playall">
			Play All
			</div>
			<?php
			if($artist_check !=0 || $community_check !=0)
			{
			?>
				<a title="Play all of the media on this profile page." href="javascript:void(0);"><img src="../images/profile/play.png" width="35" height="23" onmouseover="this.src='../images/profile/play-over.png'" onmouseout="this.src='../images/profile/play.png'" onclick="<?php if($artist_event_check!=0 || $artist_project_check!=0 || $community_project_check!=0 || $community_event_check!=0){ echo "PlayAll_featured_media('song_type_value1','video_type_value1')";} else { echo "PlayAll_featured_media('song_type_value','video_type_value')";}?>" /></a>
				<a title="Add all of the media on this profile page to your playlist without stopping your current file." href="javascript:void(0);"><img src="../images/profile/add.png" width="27" height="23" onmouseover="this.src='../images/profile/add-over.png'" onmouseout="this.src='../images/profile/add.png'" onclick="<?php if($artist_event_check!=0 || $artist_project_check!=0 || $community_project_check!=0 || $community_event_check!=0){ echo "PlayAll_featured_media('song_type_value1','video_type_value1')";} else { echo "PlayAll_featured_media('song_type_value','video_type_value')";}?>" /></a>
			<?php
			}
			elseif($artist_event_check !=0 || $community_event_check !=0)
			{
			?>
				<a title="Play all of the media on this events page." href="javascript:void(0);"><img src="../images/profile/play.png" style="border: medium none; float: left;" width="35" height="23" onmouseover="this.src='../images/profile/play-over.png'" onmouseout="this.src='../images/profile/play.png'" onclick="<?php if($artist_event_check!=0 || $artist_project_check!=0 || $community_project_check!=0 || $community_event_check!=0){ echo "PlayAll_featured_media('song_type_value1','video_type_value1')";} else { echo "PlayAll_featured_media('song_type_value','video_type_value')";}?>" /></a>
				<a title="Add all of the media on this events page to your playlist without stopping your current file." href="javascript:void(0);"><img src="../images/profile/add.png" width="27" height="23" onmouseover="this.src='../images/profile/add-over.png'" onmouseout="this.src='../images/profile/add.png'" onclick="<?php if($artist_event_check!=0 || $artist_project_check!=0 || $community_project_check!=0 || $community_event_check!=0){ echo "PlayAll_featured_media('song_type_value1','video_type_value1')";} else { echo "PlayAll_featured_media('song_type_value','video_type_value')";}?>" /></a>
			<?php
			}
			elseif($artist_project_check !=0 || $community_project_check !=0)
			{
			?>
				<a title="Play all of the media on this projects page." href="javascript:void(0);"><img src="../images/profile/play.png" style="border: medium none; float: left;" width="35" height="23" onmouseover="this.src='../images/profile/play-over.png'" onmouseout="this.src='../images/profile/play.png'" onclick="<?php if($artist_event_check!=0 || $artist_project_check!=0 || $community_project_check!=0 || $community_event_check!=0){ echo "PlayAll_featured_media('song_type_value1','video_type_value1')";} else { echo "PlayAll_featured_media('song_type_value','video_type_value')";}?>" /></a>
				<a title="Add all of the media on this projects page to your playlist without stopping your current file." href="javascript:void(0);"><img src="../images/profile/add.png" width="27" height="23" onmouseover="this.src='../images/profile/add-over.png'" onmouseout="this.src='../images/profile/add.png'" onclick="<?php if($artist_event_check!=0 || $artist_project_check!=0 || $community_project_check!=0 || $community_event_check!=0){ echo "PlayAll_featured_media('song_type_value1','video_type_value1')";} else { echo "PlayAll_featured_media('song_type_value','video_type_value')";}?>" /></a>
			<?php
			}
			?>
		</div>
		<div id="profileFeatured">
			
			<?php //echo $getuser['featured_media'].$getuser['media_id'];
			
			if($artist_check !=0 || $artist_event_check !=0 || $artist_project_check !=0  )
			{
				$table_type = "artist";
			}
			if($community_check !=0 || $community_event_check !=0 || $community_project_check !=0  )
			{
				$table_type = "community";
			}
			if($getuser['featured_media'] == "Gallery" && $getuser['media_id']!=0)
			{
				$find_whether_regis=$new_profile_class_obj->get_count_Gallery_at_register($getuser["$table_type".'_id'],$getuser['media_id']);
				//var_dump($find_whether_regis);
				if($find_whether_regis["count(*)"]>1)
				{
					$getimages=$new_profile_class_obj->get_featured_media_gallery_at_register($getuser['media_id'],$table_type);
					$feature_image_name = array();
					$feature_image_title = array();
					$feature_count = 0;
					while($get_image = mysql_fetch_assoc($getimages))
					{
						$feature_image_name[$feature_count] = $get_image['image_name'];
						$feature_image_title[$feature_count] = $get_image['image_title'];
						$feature_count++;
					}
					$feature_big_img = $feature_image_name[0];
					$featured_gal_img = implode(",", $feature_image_name);
					$featured_gal_img_title = implode(",", $feature_image_title);
					$feature_org_path = "https://reggallery.s3.amazonaws.com/";
					$feature_thum_path = "https://reggalthumb.s3.amazonaws.com/";
				}
				else
				{
					$getimages=$new_profile_class_obj->get_featured_media_gallery($getuser['media_id']);
					$feature_image_name = array();
					$feature_image_title = array();
					$feature_count = 0;
					while($get_image = mysql_fetch_assoc($getimages))
					{
						$feature_image_name[$feature_count] = $get_image['image_name'];
						$feature_image_title[$feature_count] = $get_image['image_title'];
						$feature_count++;
					}
					$feature_big_img = $feature_image_name[0];
					$featured_gal_img = implode(",", $feature_image_name);
					$featured_gal_img_title = implode(",", $feature_image_title);
					$feature_org_path = "https://medgallery.s3.amazonaws.com/";
					$feature_thum_path = "https://medgalthumb.s3.amazonaws.com/";
				}
			?>
				<div class="fmedia">featured Media</div>
				<?php
				if($artist_check !=0 || $community_check !=0)
				{
				?>
					<a title="Play this artists featured media." href="javascript:void(0);"><img src="../images/profile/play.png" width="35" height="23" onmouseover="this.src='../images/profile/play-over.png'" onmouseout="this.src='../images/profile/play.png'" onclick="checkload('<?php echo $feature_big_img;?>','<?php echo $featured_gal_img; ?>','<?php echo $feature_thum_path; ?>','<?php echo $feature_org_path; ?>','<?php echo $featured_gal_img_title; ?>')" /></a>
					<a title="Add this artists featured media to your playlist without stopping your current file." href=""><img src="../images/profile/add.png" width="27" height="23" onmouseover="this.src='../images/profile/add-over.png'" onmouseout="this.src='../images/profile/add.png'" /></a>
			<?php
				}
				elseif($artist_event_check !=0 || $community_event_check !=0)
				{
			?>
					<a title="Play this events featured media." href="javascript:void(0);"><img src="../images/profile/play.png" width="35" height="23" onmouseover="this.src='../images/profile/play-over.png'" onmouseout="this.src='../images/profile/play.png'" onclick="checkload('<?php echo $feature_big_img;?>','<?php echo $featured_gal_img; ?>','<?php echo $feature_thum_path; ?>','<?php echo $feature_org_path; ?>','<?php echo $featured_gal_img_title; ?>')" /></a>
					<a title="Add this events featured media to your playlist without stopping your current file." href=""><img src="../images/profile/add.png" width="27" height="23" onmouseover="this.src='../images/profile/add-over.png'" onmouseout="this.src='../images/profile/add.png'" /></a>
			<?php
				}
				elseif($artist_project_check !=0 || $community_project_check !=0)
				{
			?>
					<a title="Play this projects featured media." href="javascript:void(0);"><img src="../images/profile/play.png" width="35" height="23" onmouseover="this.src='../images/profile/play-over.png'" onmouseout="this.src='../images/profile/play.png'" onclick="checkload('<?php echo $feature_big_img;?>','<?php echo $featured_gal_img; ?>','<?php echo $feature_thum_path; ?>','<?php echo $feature_org_path; ?>','<?php echo $featured_gal_img_title; ?>')" /></a>
					<a title="Add this projects featured media to your playlist without stopping your current file." href=""><img src="../images/profile/add.png" width="27" height="23" onmouseover="this.src='../images/profile/add-over.png'" onmouseout="this.src='../images/profile/add.png'" /></a>
			<?php
				}
			}
			if($getuser['featured_media'] == "Song" && $getuser['media_id']!=0)
			{
			?>
				<div class="fmedia">featured Media</div>
				<?php
				if($artist_check !=0 || $community_check !=0)
				{
				?>
					<a title="Play this artists featured media." href="javascript:void(0);"><img src="../images/profile/play.png" width="35" height="23" onmouseover="this.src='../images/profile/play-over.png'" onmouseout="this.src='../images/profile/play.png'" onclick="showPlayer('a','<?php echo $getuser['media_id'];?>','play')" /></a>
					<a title="Add this artists featured media to your playlist without stopping your current file." href=""><img src="../images/profile/add.png" width="27" height="23" onmouseover="this.src='../images/profile/add-over.png'" onmouseout="this.src='../images/profile/add.png'" onclick="showPlayer('a','<?php echo $getuser['media_id'];?>','add')" /></a>
			<?php
				}
				elseif($artist_event_check !=0 || $community_event_check !=0)
				{
			?>
					<a title="Play this events featured media." href="javascript:void(0);"><img src="../images/profile/play.png" width="35" height="23" onmouseover="this.src='../images/profile/play-over.png'" onmouseout="this.src='../images/profile/play.png'" onclick="showPlayer('a','<?php echo $getuser['media_id'];?>','play')" /></a>
					<a title="Add this events featured media to your playlist without stopping your current file." href=""><img src="../images/profile/add.png" width="27" height="23" onmouseover="this.src='../images/profile/add-over.png'" onmouseout="this.src='../images/profile/add.png'" onclick="showPlayer('a','<?php echo $getuser['media_id'];?>','add')" /></a>
			<?php
				}
				elseif($artist_project_check !=0 || $community_project_check !=0)
				{
			?>
					<a title="Play this projects featured media." href="javascript:void(0);"><img src="../images/profile/play.png" width="35" height="23" onmouseover="this.src='../images/profile/play-over.png'" onmouseout="this.src='../images/profile/play.png'" onclick="showPlayer('a','<?php echo $getuser['media_id'];?>','play')" /></a>
					<a title="Add this projects featured media to your playlist without stopping your current file." href=""><img src="../images/profile/add.png" width="27" height="23" onmouseover="this.src='../images/profile/add-over.png'" onmouseout="this.src='../images/profile/add.png'" onclick="showPlayer('a','<?php echo $getuser['media_id'];?>','add')" /></a>
			<?php
				}
			}
			?>
			<?php
			if($getuser['featured_media'] == "Video" && $getuser['media_id']!=0)
			{
			?>
				<div class="fmedia">featured Media</div>
				<?php
				if($artist_check !=0 || $community_check !=0)
				{
				?>
					<a title="Play this artists featured media." href="javascript:void(0);"><img src="../images/profile/play.png" width="35" height="23" onmouseover="this.src='../images/profile/play-over.png'" onmouseout="this.src='../images/profile/play.png'" onclick="showPlayer('v','<?php echo $getuser['media_id'];?>','playlist')" /></a>
					<a title="Add this artists featured media to your playlist without stopping your current file." href=""><img src="../images/profile/add.png" width="27" height="23" onmouseover="this.src='../images/profile/add-over.png'" onmouseout="this.src='../images/profile/add.png'" onclick="showPlayer('v','<?php echo $getuser['media_id'];?>','addvi')" /></a>
				
			<?php
				}
				elseif($artist_event_check !=0 || $community_event_check !=0)
				{
			?>
					<a title="Play this events featured media." href="javascript:void(0);"><img src="../images/profile/play.png" width="35" height="23" onmouseover="this.src='../images/profile/play-over.png'" onmouseout="this.src='../images/profile/play.png'" onclick="showPlayer('v','<?php echo $getuser['media_id'];?>','playlist')" /></a>
					<a title="Add this events featured media to your playlist without stopping your current file." href=""><img src="../images/profile/add.png" width="27" height="23" onmouseover="this.src='../images/profile/add-over.png'" onmouseout="this.src='../images/profile/add.png'" onclick="showPlayer('v','<?php echo $getuser['media_id'];?>','addvi')" /></a>
			<?php
				}
				elseif($artist_project_check !=0 || $community_project_check !=0)
				{
			?>
					<a title="Play this projects featured media." href="javascript:void(0);"><img src="../images/profile/play.png" width="35" height="23" onmouseover="this.src='../images/profile/play-over.png'" onmouseout="this.src='../images/profile/play.png'" onclick="showPlayer('v','<?php echo $getuser['media_id'];?>','playlist')" /></a>
					<a title="Add this projects featured media to your playlist without stopping your current file." href=""><img src="../images/profile/add.png" width="27" height="23" onmouseover="this.src='../images/profile/add-over.png'" onmouseout="this.src='../images/profile/add.png'" onclick="showPlayer('v','<?php echo $getuser['media_id'];?>','addvi')" /></a>
			<?php
				}
			}
			?>
			
		</div>
      <h2></h2>
	  <!--<h2>Projects</h2>-->
      <div class="rowItem">
	  <?php
		$pros_news = array();
		if($artist_event_check!=0 || $artist_project_check!=0 || $community_project_check!=0 || $community_event_check!=0)
		{
			for($eve_pro=0;$eve_pro<count($get_user_exp) ;$eve_pro++)
			{
				$get_user_exp_n = explode('~',$get_user_exp[$eve_pro]);
				if($get_user_exp_n[1]=='art')
				{
					$get_ev_project = $new_profile_class_obj->get_aproject_count($get_user_exp_n[0]);
				}
				elseif($get_user_exp_n[1]=='com')
				{
					$get_ev_project = $new_profile_class_obj->get_cproject_count($get_user_exp_n[0]);
				}
				if($get_ev_project!="" && $get_ev_project!=Null)
				{
					if(mysql_num_rows($get_ev_project)>0)
					{
						$row = mysql_fetch_assoc($get_ev_project);
						$pros_news[] = $row;
					}
				}
			}
		}
		include_once("findsale_song.php");
		include_once("findsale_video.php");
		if($artist_event_check!=0 || $artist_project_check!=0 || $community_project_check!=0 || $community_event_check!=0)
		{
			for($eve_pro=0;$eve_pro<count($pros_news) ;$eve_pro++)
			{
				/* $get_user_exp_n = explode('~',$get_user_exp[$eve_pro]);
				if($get_user_exp_n[1]=='art')
				{
					$get_ev_project = $new_profile_class_obj->get_aproject_count($get_user_exp_n[0]);
				}
				elseif($get_user_exp_n[1]=='com')
				{
					$get_ev_project = $new_profile_class_obj->get_cproject_count($get_user_exp_n[0]);
				}
				if(mysql_num_rows($get_ev_project)>0)
				{
					$row = mysql_fetch_assoc($get_ev_project); */
					if(isset($pros_news[$eve_pro]['artist_id']))
					{
						$link = "add_artist_project.php?id=";
						$find_creator = $new_profile_class_obj ->find_creator($pros_news[$eve_pro]['artist_id'],"artist");
					}
					else if(isset($pros_news[$eve_pro]['community_id']))
					{
						$link = "add_community_project.php?id=";
						$find_creator = $new_profile_class_obj ->find_creator($pros_news[$eve_pro]['community_id'],"community");
					}
					
					if(isset($pros_news[$eve_pro]['artist_id']))
					{
						$sql_butts = mysql_query("SELECT * FROM general_user WHERE artist_id='".$pros_news[$eve_pro]['artist_id']."'");
						$ans_butts_pros = mysql_fetch_assoc($sql_butts);
					}
					elseif(isset($pros_news[$eve_pro]['community_id']))
					{
						$sql_butts = mysql_query("SELECT * FROM general_user WHERE community_id='".$pros_news[$eve_pro]['community_id']."'");
						$ans_butts_pros = mysql_fetch_assoc($sql_butts);
					}
					
					$find_mem_butts_pro = $display->get_member_or_not($ans_butts_pros['general_user_id']);
					?>
						<div class="tabItem">
						<a href ="<?php echo $pros_news[$eve_pro]['profile_url'];?>" onclick ="return chk_for_creator('<?php echo $pros_news[$eve_pro]['profile_url'];?>')"><img class="imgs_pro" src="<?php if(isset($pros_news[$eve_pro]['image_name']) && ($pros_news[$eve_pro]['image_name']!="" || $pros_news[$eve_pro]['image_name']!=null)) { echo $pros_news[$eve_pro]['image_name']; } else { echo "https://artproprofile.s3.amazonaws.com/Noimage.png"; } ?>" width="200" height="200" /></a>
						<div class="both_wrap_per">
						  <div class="player">
						  <?php
							if($pros_news[$eve_pro]['featured_media']=="Gallery" && $pros_news[$eve_pro]['media_id']!=0)
							{
								$feature_reg_big_img = "";
								$all_register_images = "";
								$feature_reg_thum_path = "";
								$feature_reg_org_path = "";
								$featured_reg_gal_img_title = "";
									
								if($pros_news[$eve_pro]['featured_media_table']!="" &&($pros_news[$eve_pro]['featured_media_table']=="general_artist_gallery_list" || $pros_news[$eve_pro]['featured_media_table']=="general_community_gallery_list"))
								{
									$get_register_featured_gallery = $new_profile_class_obj ->get_register_featured_media_gallery($pros_news[$eve_pro]['media_id'],$pros_news[$eve_pro]['featured_media_table']);
									$all_register_images ="";
									$feature_reg_big_img ="";
									$featured_reg_gal_img_title ="";
									for($cou_i=0;$cou_i<count($get_register_featured_gallery);$cou_i++)
									{
										$feature_reg_big_img = $get_register_featured_gallery[0]['image_name'];
										$all_register_images = $all_register_images.",".$get_register_featured_gallery[$cou_i]['image_name'];
										$featured_reg_gal_img_title = $featured_reg_gal_img_title .",".$get_register_featured_gallery[$cou_i]['image_title'];
									}
									$all_register_images = ltrim($all_register_images,',');
									$featured_reg_gal_img_title = ltrim($featured_reg_gal_img_title,',');
									$feature_reg_org_path = "https://reggallery.s3.amazonaws.com/";
									$feature_reg_thum_path = "https://reggalthumb.s3.amazonaws.com/";
								}
								else if($pros_news[$eve_pro]['featured_media_table']!="" && $pros_news[$eve_pro]['featured_media_table']=="general_media"){
									$get_register_featured_gallery = $new_profile_class_obj ->get_featured_gallery_images($pros_news[$eve_pro]['media_id']);
									$all_register_images ="";
									$feature_reg_big_img ="";
									$featured_reg_gal_img_title ="";
									for($cou_i=0;$cou_i<count($get_register_featured_gallery);$cou_i++)
									{
										$feature_reg_big_img = $get_register_featured_gallery[0]['image_name'];
										$all_register_images = $all_register_images.",".$get_register_featured_gallery[$cou_i]['image_name'];
										$featured_reg_gal_img_title = $featured_reg_gal_img_title .",".$get_register_featured_gallery[$cou_i]['image_title'];
									}
									$all_register_images = ltrim($all_register_images,',');
									$featured_reg_gal_img_title = ltrim($featured_reg_gal_img_title,',');
									$feature_reg_org_path = "https://medgallery.s3.amazonaws.com/";
									$feature_reg_thum_path = "https://medgalthumb.s3.amazonaws.com/";
								}
							?>
							<a href="javascript:void(0);"><img src="../images/profile/play.gif" onmouseover="this.src='../images/profile/play-over.gif'" onmouseout="this.src='../images/profile/play.gif'" onclick="checkload('<?php echo $feature_reg_big_img;?>','<?php echo $all_register_images; ?>','<?php echo $feature_reg_thum_path; ?>','<?php echo $feature_reg_org_path; ?>','<?php echo $featured_reg_gal_img_title; ?>')" title="Play file now."/></a>
							<!--<a href="javascript:void(0);"><img src="../images/profile/add.gif"  onmouseover="this.src='../images/profile/add-over.gif'" onmouseout="this.src='../images/profile/add.gif'" title="Add file to the bottom of your playlist without stopping current media."/></a>-->
							
							<?php
							if($find_mem_butts_pro != null && $find_mem_butts_pro['general_user_id'] != null && ($find_mem_butts_pro['expiry_date'] >$date || $find_mem_butts_pro['lifetime']==1))
							{
								if($pros_news[$eve_pro]['type']=="for_sale" || $pros_news[$eve_pro]['type']=="fan_club" || $pros_news[$eve_pro]['type']=="free_club")
								{
									$chk_ac_ep = 0;
									$stats = 0;
									if($_SESSION['login_email']!=NULL)
									{
										if(isset($pros_news[$eve_pro]['artist_id']))
										{
											$stats = $display->chkdownloaded_pro($pros_news[$eve_pro]['id'],$_SESSION['login_id'],'artist_project');
										}
										elseif(isset($pros_news[$eve_pro]['community_id']))
										{
											$stats = $display->chkdownloaded_pro($pros_news[$eve_pro]['id'],$_SESSION['login_id'],'community_project');
										}
									}
									else
									{
										if($pros_news[$eve_pro]['type']=="for_sale" || $pros_news[$eve_pro]['type']=="free_club")
										{
											if(isset($pros_news[$eve_pro]['artist_id']))
											{						
												$stats = $display->chkdownloaded_pro_not_log($pros_news[$eve_pro]['id'],'artist_project');
											}
											if(isset($pros_news[$eve_pro]['community_id']))
											{
												$stats = $display->chkdownloaded_pro_not_log($pros_news[$eve_pro]['id'],'community_project');
											}
										}
									}
									
									if($stats==1)
									{
										if($pros_news[$eve_pro]['type']!="fan_club")
										{
											$chk_ac_ep = 1;
										}
									}
									else
									{
										if($pros_news[$eve_pro]['type']=="free_club")
										{
											//if($pros_news[$eve_pro]['ask_tip']=="0")
											//{	
												$chk_ac_ep = 1;
											//}
										}
										else
										{
											if($_SESSION['login_email']!=NULL)
											{
												for($set_alss=0;$set_alss<count($down_button);$set_alss++)
												{
													if($down_button[$set_alss] !="")
													{
														if(isset($down_button[$set_alss]['artist_id']) && isset($pros_news[$eve_pro]['artist_id']))
														{
															$org_exp_ids = explode('~',$down_button[$set_alss]['id']);
															
															if($org_exp_ids[0]==$pros_news[$eve_pro]['id'])
															{
																$chk_ac_ep = 1;
																break;
															}
														}
														elseif(isset($down_button[$set_alss]['community_id']) && isset($pros_news[$eve_pro]['community_id']))
														{
															$org_exp_ids = explode('~',$down_button[$set_alss]['id']);
															
															if($org_exp_ids[0]==$pros_news[$eve_pro]['id'])
															{
																$chk_ac_ep = 1;
																break;
															}
														}
													}
												}
											}
										}
									}
									if($chk_ac_ep==0)
									{
							?>
										<a href="javascript:void(0);" onClick="<?php if($pros_news[$eve_pro]['type']=="fan_club") { echo "fancy_login_single('".$pros_news[$eve_pro]['profile_url']."')"; } elseif($pros_news[$eve_pro]['type']=="for_sale") { echo "click_fancy_login_down_single('".$pros_news[$eve_pro]['profile_url']."')"; } ?>"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>
							<?php
									}
									else
									{
										if($pros_news[$eve_pro]['type']=="free_club" && $pros_news[$eve_pro]['ask_tip']=='1' && $stats!=1)
										{
											if(isset($pros_news[$eve_pro]['artist_id']))
											{
												$sql_gens_down_tip = mysql_query("SELECT * FROM general_user WHERE artist_id='".$pros_news[$eve_pro]['artist_id']."'");
												$ans_gens_down_tip = mysql_fetch_assoc($sql_gens_down_tip);
												
												$tip_add_aftr_detils = $display->get_tip_details_for_every($pros_news[$eve_pro]['id'],'artist_project');
												$type_zip_down = 'art';
											}
											elseif(isset($pros_news[$eve_pro]['community_id']))
											{
												$sql_gens_down_tip = mysql_query("SELECT * FROM general_user WHERE community_id='".$pros_news[$eve_pro]['community_id']."'");
												$ans_gens_down_tip = mysql_fetch_assoc($sql_gens_down_tip);
												
												$tip_add_aftr_detils = $display->get_tip_details_for_every($pros_news[$eve_pro]['id'],'community_project');
												$type_zip_down = 'com';
											}
							?>
											<a href="javascript:void(0);" onClick="<?php echo "ask_to_donate_alpro('".mysql_real_escape_string($pros_news[$eve_pro]['title'])."','".mysql_real_escape_string($pros_news[$eve_pro]['creator'])."','". $pros_news[$eve_pro]['free_club_song']."','".$pros_news[$eve_pro]['profile_url']."','".$ans_gens_down_tip['general_user_id']."','".$tip_add_aftr_detils."','".$type_zip_down."','".$pros_news[$eve_pro]['id']."')"; ?>"><img src="../images/profile/download_down.gif" onmouseover="this.src='../images/profile/download-over_down.gif'" onmouseout="this.src='../images/profile/download_down.gif'" title="Add media to your cart for free download or purchase." /></a>
							<?php
										}
										else
										{
											if(isset($pros_news[$eve_pro]['artist_id']))
											{
												$type_zip_down = 'art';
											}
											else
											{
												$type_zip_down = 'com';
											}
								?>									
											<a href="javascript:void(0);" onClick="download_directly_project('<?php if($pros_news[$eve_pro]['type']=="fan_club") { echo $pros_news[$eve_pro]['fan_song']; } elseif($pros_news[$eve_pro]['type']=="for_sale") { echo $pros_news[$eve_pro]['sale_song']; } elseif($pros_news[$eve_pro]['type']=="free_club") { echo $pros_news[$eve_pro]['free_club_song']; } ?>','<?php echo $pros_news[$eve_pro]['title']; ?>','<?php echo $pros_news[$eve_pro]['creator'];?>','<?php echo $type_zip_down; ?>','<?php echo $pros_news[$eve_pro]['id']; ?>')"><img src="../images/profile/download_down.gif" onmouseover="this.src='../images/profile/download-over_down.gif'" onmouseout="this.src='../images/profile/download_down.gif'" title="Add media to your cart for free download or purchase." /></a>
							<?php	
										}
									}
								}
							}
								?>
								<a class="fancybox fancybox.ajax" href="fan_club.php?data=<?php echo $pros_news[$eve_pro]['profile_url'];?>" id="fan_club_pro<?php echo $pros_news[$eve_pro]['profile_url'];?>" style="display:none;" ><?php if($pros_news[$eve_pro]['type']=="fan_club") { echo "Fan Club"; } elseif($pros_news[$eve_pro]['type']=="for_sale") { echo "Purchase"; } elseif($pros_news[$eve_pro]['type']=="free_club") { echo "Free"; } ?></a>
											<script type="text/javascript">
											$(document).ready(function() {
													/* $("#fan_club_pro<?php echo $pros_news[$eve_pro]['profile_url'];?>").fancybox({
													'width'				: '69%',
													'height'			: '80%',
													'transitionIn'		: 'none',
													'transitionOut'		: 'none',
													'type'				: 'iframe'
													}); */
													
													$("#fan_club_pro<?php echo $pros_news[$eve_pro]['profile_url'];?>").fancybox({
														helpers : {
															media : {},
															buttons : {},
															title : null
														}
													});
											});
											</script>
								<?php
							}
							else if($pros_news[$eve_pro]['featured_media']=="Song" && $pros_news[$eve_pro]['media_id']!=0)
							{
								if($pros_news[$eve_pro]['featured_media_table']=="general_artist_audio" || $pros_news[$eve_pro]['featured_media_table']=="general_community_audio")
								{
									?>
										<a href="javascript:void(0);"><img src="../images/profile/play.gif" onmouseover="this.src='../images/profile/play-over.gif'" onmouseout="this.src='../images/profile/play.gif'" onclick="showPlayer_reg('a','<?php echo $pros_news[$eve_pro]['media_id'];?>','play_reg','<?php echo $pros_news[$eve_pro]['featured_media_table'];?>')" title="Play file now."/></a>
										<a href="javascript:void(0);"><img src="../images/profile/add.gif"  onmouseover="this.src='../images/profile/add-over.gif'" onmouseout="this.src='../images/profile/add.gif'" onclick="showPlayer_reg('a','<?php echo $pros_news[$eve_pro]['media_id'];?>','add_reg','<?php echo $pros_news[$eve_pro]['featured_media_table'];?>')" title="Add file to the bottom of your playlist without stopping current media."/></a>
									<?php
								}
								else{
									?>
										<a href="javascript:void(0);"><img src="../images/profile/play.gif" onmouseover="this.src='../images/profile/play-over.gif'" onmouseout="this.src='../images/profile/play.gif'" onclick="showPlayer('a','<?php echo $pros_news[$eve_pro]['media_id'];?>','play')" title="Play file now."/></a>
										<a href="javascript:void(0);"><img src="../images/profile/add.gif"  onmouseover="this.src='../images/profile/add-over.gif'" onmouseout="this.src='../images/profile/add.gif'" onclick="showPlayer('a','<?php echo $pros_news[$eve_pro]['media_id'];?>','add')" title="Add file to the bottom of your playlist without stopping current media."/></a>
									<?php
								}
								if($find_mem_butts_pro != null && $find_mem_butts_pro['general_user_id'] != null && ($find_mem_butts_pro['expiry_date'] >$date || $find_mem_butts_pro['lifetime']==1))
							{
								if($pros_news[$eve_pro]['type']=="for_sale" || $pros_news[$eve_pro]['type']=="fan_club" || $pros_news[$eve_pro]['type']=="free_club")
								{
									$chk_ac_ep = 0;
									
									$stats = 0;
									if($_SESSION['login_email']!=NULL)
									{
										if(isset($pros_news[$eve_pro]['artist_id']))
										{
											$stats = $display->chkdownloaded_pro($pros_news[$eve_pro]['id'],$_SESSION['login_id'],'artist_project');
										}
										elseif(isset($pros_news[$eve_pro]['community_id']))
										{
											$stats = $display->chkdownloaded_pro($pros_news[$eve_pro]['id'],$_SESSION['login_id'],'community_project');
										}
									}
									else
									{
										if($pros_news[$eve_pro]['type']=="for_sale" || $pros_news[$eve_pro]['type']=="free_club")
										{
											if(isset($pros_news[$eve_pro]['artist_id']))
											{						
												$stats = $display->chkdownloaded_pro_not_log($pros_news[$eve_pro]['id'],'artist_project');
											}
											if(isset($pros_news[$eve_pro]['community_id']))
											{
												$stats = $display->chkdownloaded_pro_not_log($pros_news[$eve_pro]['id'],'community_project');
											}
										}
									}
									
									if($stats==1)
									{
										if($pros_news[$eve_pro]['type']!="fan_club")
										{
											$chk_ac_ep = 1;
										}
									}
									else
									{
										if($pros_news[$eve_pro]['type']=="free_club")
										{
											//if($pros_news[$eve_pro]['ask_tip']=="0")
											//{
												$chk_ac_ep = 1;
											//}
										}
										else
										{
											if($_SESSION['login_email']!=NULL)
											{
												for($set_alss=0;$set_alss<count($down_button);$set_alss++)
												{
													if($down_button[$set_alss] !="")
													{
														if(isset($down_button[$set_alss]['artist_id']) && isset($pros_news[$eve_pro]['artist_id']))
														{
															$org_exp_ids = explode('~',$down_button[$set_alss]['id']);
															
															if($org_exp_ids[0]==$pros_news[$eve_pro]['id'])
															{
																$chk_ac_ep = 1;
																break;
															}
														}
														elseif(isset($down_button[$set_alss]['community_id']) && isset($pros_news[$eve_pro]['community_id']))
														{
															$org_exp_ids = explode('~',$down_button[$set_alss]['id']);
															
															if($org_exp_ids[0]==$pros_news[$eve_pro]['id'])
															{
																$chk_ac_ep = 1;
																break;
															}
														}
													}
												}
											}
										}
									}
									if($chk_ac_ep==0)
									{
							?>
										<a href="javascript:void(0);" onClick="<?php if($pros_news[$eve_pro]['type']=="fan_club") { echo "fancy_login_single('".$pros_news[$eve_pro]['profile_url']."')"; } elseif($pros_news[$eve_pro]['type']=="for_sale") { echo "click_fancy_login_down_single('".$pros_news[$eve_pro]['profile_url']."')"; } ?>"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>
							<?php
									}
									else
									{
										if($pros_news[$eve_pro]['type']=="free_club" && $pros_news[$eve_pro]['ask_tip']=='1' && $stats!=1)
										{
											if(isset($pros_news[$eve_pro]['artist_id']))
											{
												$sql_gens_down_tip = mysql_query("SELECT * FROM general_user WHERE artist_id='".$pros_news[$eve_pro]['artist_id']."'");
												$ans_gens_down_tip = mysql_fetch_assoc($sql_gens_down_tip);
												
												$tip_add_aftr_detils = $display->get_tip_details_for_every($pros_news[$eve_pro]['id'],'artist_project');
												$type_zip_down = 'art';
											}
											elseif(isset($pros_news[$eve_pro]['community_id']))
											{
												$sql_gens_down_tip = mysql_query("SELECT * FROM general_user WHERE community_id='".$pros_news[$eve_pro]['community_id']."'");
												$ans_gens_down_tip = mysql_fetch_assoc($sql_gens_down_tip);
												
												$tip_add_aftr_detils = $display->get_tip_details_for_every($pros_news[$eve_pro]['id'],'community_project');
												$type_zip_down = 'com';
											}
							?>
											<a href="javascript:void(0);" onClick="<?php echo "ask_to_donate_alpro('".mysql_real_escape_string($pros_news[$eve_pro]['title'])."','".mysql_real_escape_string($pros_news[$eve_pro]['creator'])."','". $pros_news[$eve_pro]['free_club_song']."','".$pros_news[$eve_pro]['profile_url']."','".$ans_gens_down_tip['general_user_id']."','".$tip_add_aftr_detils."','".$type_zip_down."','".$pros_news[$eve_pro]['id']."')"; ?>"><img src="../images/profile/download_down.gif" onmouseover="this.src='../images/profile/download-over_down.gif'" onmouseout="this.src='../images/profile/download_down.gif'" title="Add media to your cart for free download or purchase." /></a>
							<?php
										}
										else
										{
											if(isset($pros_news[$eve_pro]['artist_id']))
											{
												$type_zip_down = 'art';
											}
											else
											{
												$type_zip_down = 'com';
											}
								?>									
											<a href="javascript:void(0);" onClick="download_directly_project('<?php if($pros_news[$eve_pro]['type']=="fan_club") { echo $pros_news[$eve_pro]['fan_song']; } elseif($pros_news[$eve_pro]['type']=="for_sale") { echo $pros_news[$eve_pro]['sale_song']; } elseif($pros_news[$eve_pro]['type']=="free_song") { echo $pros_news[$eve_pro]['free_club_song']; } elseif($pros_news[$eve_pro]['type']=="free_club") { echo $pros_news[$eve_pro]['free_club_song']; } ?>','<?php echo $pros_news[$eve_pro]['title']; ?>','<?php echo $pros_news[$eve_pro]['creator'];?>','<?php echo $type_zip_down; ?>','<?php echo $pros_news[$eve_pro]['id']; ?>')"><img src="../images/profile/download_down.gif" onmouseover="this.src='../images/profile/download-over_down.gif'" onmouseout="this.src='../images/profile/download_down.gif'" title="Add media to your cart for free download or purchase." /></a>
							<?php		
										}
									}
								}
							}
								$songtype1 = "s";
								if($pros_news[$eve_pro]['media_id'] !="" && $pros_news[$eve_pro]['media_id']!=0){
									$songtype_value1 = $songtype_value1 .",".$pros_news[$eve_pro]['media_id'];
								}
								?>
								<a class="fancybox fancybox.ajax" href="fan_club.php?data=<?php echo $pros_news[$eve_pro]['profile_url'];?>" id="fan_club_pro<?php echo $pros_news[$eve_pro]['profile_url'];?>" style="display:none;" ><?php if($pros_news[$eve_pro]['type']=="fan_club") { echo "Fan Club"; } elseif($pros_news[$eve_pro]['type']=="for_sale") { echo "Purchase"; } elseif($pros_news[$eve_pro]['type']=="free_club") { echo "Free"; } ?></a>
											<script type="text/javascript">
											$(document).ready(function() {
													/* $("#fan_club_pro<?php echo $pros_news[$eve_pro]['profile_url'];?>").fancybox({
													'width'				: '69%',
													'height'			: '80%',
													'transitionIn'		: 'none',
													'transitionOut'		: 'none',
													'type'				: 'iframe'
													}); */
													
													$("#fan_club_pro<?php echo $pros_news[$eve_pro]['profile_url'];?>").fancybox({
														helpers : {
															media : {},
															buttons : {},
															title : null
														}
													});
											});
											</script>
								<?php
							}
							else if($pros_news[$eve_pro]['featured_media']=="Video" && $pros_news[$eve_pro]['media_id']!=0)
							{
								if($pros_news[$eve_pro]['featured_media_table']=="general_artist_video" || $pros_news[$eve_pro]['featured_media_table']=="general_community_video")
								{
									?>
										<a href="javascript:void(0);"><img src="../images/profile/play.gif" onmouseover="this.src='../images/profile/play-over.gif'" onmouseout="this.src='../images/profile/play.gif'" onclick="showPlayer_reg('v','<?php echo $pros_news[$eve_pro]['media_id'];?>','playlist_reg','<?php echo $pros_news[$eve_pro]['featured_media_table'];?>')" title="Play file now."/></a>
										<a href="javascript:void(0);"><img src="../images/profile/add.gif"  onmouseover="this.src='../images/profile/add-over.gif'" onmouseout="this.src='../images/profile/add.gif'" onclick="showPlayer_reg('v','<?php echo $pros_news[$eve_pro]['media_id'];?>','addvi_reg','<?php echo $pros_news[$eve_pro]['featured_media_table'];?>')" title="Add file to the bottom of your playlist without stopping current media."/></a>
									<?php
								}else{
									?>
										<a href="javascript:void(0);"><img src="../images/profile/play.gif" onmouseover="this.src='../images/profile/play-over.gif'" onmouseout="this.src='../images/profile/play.gif'" onclick="showPlayer('v','<?php echo $pros_news[$eve_pro]['media_id'];?>','playlist')" title="Play file now."/></a>
										<a href="javascript:void(0);"><img src="../images/profile/add.gif"  onmouseover="this.src='../images/profile/add-over.gif'" onmouseout="this.src='../images/profile/add.gif'" onclick="showPlayer('v','<?php echo $pros_news[$eve_pro]['media_id'];?>','addvi')" title="Add file to the bottom of your playlist without stopping current media."/></a>
									<?php
								}
								if($find_mem_butts_pro != null && $find_mem_butts_pro['general_user_id'] != null && ($find_mem_butts_pro['expiry_date'] >$date || $find_mem_butts_pro['lifetime']==1))
							{
								if($pros_news[$eve_pro]['type']=="for_sale" || $pros_news[$eve_pro]['type']=="fan_club" || $pros_news[$eve_pro]['type']=="free_club")
								{
									$chk_ac_ep = 0;
									$stats = 0;
									if($_SESSION['login_email']!=NULL)
									{
										if(isset($pros_news[$eve_pro]['artist_id']))
										{
											$stats = $display->chkdownloaded_pro($pros_news[$eve_pro]['id'],$_SESSION['login_id'],'artist_project');
										}
										elseif(isset($pros_news[$eve_pro]['community_id']))
										{
											$stats = $display->chkdownloaded_pro($pros_news[$eve_pro]['id'],$_SESSION['login_id'],'community_project');
										}
									}
									else
									{
										if($pros_news[$eve_pro]['type']=="for_sale" || $pros_news[$eve_pro]['type']=="free_club")
										{
											if(isset($pros_news[$eve_pro]['artist_id']))
											{						
												$stats = $display->chkdownloaded_pro_not_log($pros_news[$eve_pro]['id'],'artist_project');
											}
											if(isset($pros_news[$eve_pro]['community_id']))
											{
												$stats = $display->chkdownloaded_pro_not_log($pros_news[$eve_pro]['id'],'community_project');
											}
										}
									}
									
									if($stats==1)
									{
										if($pros_news[$eve_pro]['type']!="fan_club")
										{
											$chk_ac_ep = 1;
										}
									}
									else
									{
										if($pros_news[$eve_pro]['type']=="free_club")
										{
											//if($pros_news[$eve_pro]['ask_tip']=="0")
											//{	
												$chk_ac_ep = 1;
											//}
										}
										else
										{
											if($_SESSION['login_email']!=NULL)
											{
												for($set_alss=0;$set_alss<count($down_button);$set_alss++)
												{
													if($down_button[$set_alss] !="")
													{
														if(isset($down_button[$set_alss]['artist_id']) && isset($pros_news[$eve_pro]['artist_id']))
														{
															$org_exp_ids = explode('~',$down_button[$set_alss]['id']);
															
															if($org_exp_ids[0]==$pros_news[$eve_pro]['id'])
															{
																$chk_ac_ep = 1;
																break;
															}
														}
														elseif(isset($down_button[$set_alss]['community_id']) && isset($pros_news[$eve_pro]['community_id']))
														{
															$org_exp_ids = explode('~',$down_button[$set_alss]['id']);
															
															if($org_exp_ids[0]==$pros_news[$eve_pro]['id'])
															{
																$chk_ac_ep = 1;
																break;
															}
														}
													}
												}
											}
										}
									}
									if($chk_ac_ep==0)
									{
										
							?>
										<a href="javascript:void(0);" onClick="<?php if($pros_news[$eve_pro]['type']=="fan_club") { echo "fancy_login_single('".$pros_news[$eve_pro]['profile_url']."')"; } elseif($pros_news[$eve_pro]['type']=="for_sale") { echo "click_fancy_login_down_single('".$pros_news[$eve_pro]['profile_url']."')"; } ?>"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>
							<?php
									}
									else
									{
										if($pros_news[$eve_pro]['type']=="free_club" && $pros_news[$eve_pro]['ask_tip']=='1' && $stats!=1)
										{
											if(isset($pros_news[$eve_pro]['artist_id']))
											{
												$sql_gens_down_tip = mysql_query("SELECT * FROM general_user WHERE artist_id='".$pros_news[$eve_pro]['artist_id']."'");
												$ans_gens_down_tip = mysql_fetch_assoc($sql_gens_down_tip);
												
												$tip_add_aftr_detils = $display->get_tip_details_for_every($pros_news[$eve_pro]['id'],'artist_project');
												$type_zip_down = 'art';
											}
											elseif(isset($pros_news[$eve_pro]['community_id']))
											{
												$sql_gens_down_tip = mysql_query("SELECT * FROM general_user WHERE community_id='".$pros_news[$eve_pro]['community_id']."'");
												$ans_gens_down_tip = mysql_fetch_assoc($sql_gens_down_tip);
												
												$tip_add_aftr_detils = $display->get_tip_details_for_every($pros_news[$eve_pro]['id'],'community_project');
												$type_zip_down = 'com';
											}
							?>
											<a href="javascript:void(0);" onClick="<?php echo "ask_to_donate_alpro('".mysql_real_escape_string($pros_news[$eve_pro]['title'])."','".mysql_real_escape_string($pros_news[$eve_pro]['creator'])."','". $pros_news[$eve_pro]['free_club_song']."','".$pros_news[$eve_pro]['profile_url']."','".$ans_gens_down_tip['general_user_id']."','".$tip_add_aftr_detils."','".$type_zip_down."','".$pros_news[$eve_pro]['id']."')"; ?>"><img src="../images/profile/download_down.gif" onmouseover="this.src='../images/profile/download-over_down.gif'" onmouseout="this.src='../images/profile/download_down.gif'" title="Add media to your cart for free download or purchase." /></a>
							<?php
										}
										else
										{
											if(isset($pros_news[$eve_pro]['artist_id']))
											{
												$type_zip_down = 'art';
											}
											else
											{
												$type_zip_down = 'com';
											}
								?>									
											<a href="javascript:void(0);" onClick="download_directly_project('<?php if($pros_news[$eve_pro]['type']=="fan_club") { echo $pros_news[$eve_pro]['fan_song']; } elseif($pros_news[$eve_pro]['type']=="for_sale") { echo $pros_news[$eve_pro]['sale_song']; } elseif($pros_news[$eve_pro]['type']=="free_club") { echo $pros_news[$eve_pro]['free_club_song']; } ?>','<?php echo $pros_news[$eve_pro]['title']; ?>','<?php echo $pros_news[$eve_pro]['creator'];?>','<?php echo $type_zip_down; ?>','<?php echo $pros_news[$eve_pro]['id']; ?>')"><img src="../images/profile/download_down.gif" onmouseover="this.src='../images/profile/download-over_down.gif'" onmouseout="this.src='../images/profile/download_down.gif'" title="Add media to your cart for free download or purchase." /></a>
							<?php		
										}
									}
								}
							}
								$videotype1 = "v";
								if($pros_news[$eve_pro]['media_id']!="" && $pros_news[$eve_pro]['media_id']!=0){
									$videotype_value1 = $videotype_value1 .",".$pros_news[$eve_pro]['media_id'];
								}
								?>
								<a class="fancybox fancybox.ajax" href="fan_club.php?data=<?php echo $pros_news[$eve_pro]['profile_url'];?>" id="fan_club_pro<?php echo $pros_news[$eve_pro]['profile_url'];?>" style="display:none;" ><?php if($pros_news[$eve_pro]['type']=="fan_club") { echo "Fan Club"; } elseif($pros_news[$eve_pro]['type']=="for_sale") { echo "Purchase"; } elseif($pros_news[$eve_pro]['type']=="free_club") { echo "Free"; } ?></a>
											<script type="text/javascript">
											$(document).ready(function() {
													/* $("#fan_club_pro<?php echo $pros_news[$eve_pro]['profile_url'];?>").fancybox({
													'width'				: '69%',
													'height'			: '80%',
													'transitionIn'		: 'none',
													'transitionOut'		: 'none',
													'type'				: 'iframe'
													}); */
													
													$("#fan_club_pro<?php echo $pros_news[$eve_pro]['profile_url'];?>").fancybox({
														helpers : {
															media : {},
															buttons : {},
															title : null
														}
													});
											});
											</script>
								<?php
							}
							else{
							if($find_mem_butts_pro != null && $find_mem_butts_pro['general_user_id'] != null && ($find_mem_butts_pro['expiry_date'] >$date || $find_mem_butts_pro['lifetime']==1))
							{
								if($pros_news[$eve_pro]['type']=="for_sale" || $pros_news[$eve_pro]['type']=="fan_club" || $pros_news[$eve_pro]['type']=="free_club")
								{
									$stats = 0;
									if($_SESSION['login_email']!=NULL)
									{
										if(isset($pros_news[$eve_pro]['artist_id']))
										{
											$stats = $display->chkdownloaded_pro($pros_news[$eve_pro]['id'],$_SESSION['login_id'],'artist_project');
										}
										elseif(isset($pros_news[$eve_pro]['community_id']))
										{
											$stats = $display->chkdownloaded_pro($pros_news[$eve_pro]['id'],$_SESSION['login_id'],'community_project');
										}
									}
									else
									{
										if($pros_news[$eve_pro]['type']=="for_sale" || $pros_news[$eve_pro]['type']=="free_club")
										{
											if(isset($pros_news[$eve_pro]['artist_id']))
											{						
												$stats = $display->chkdownloaded_pro_not_log($pros_news[$eve_pro]['id'],'artist_project');
											}
											if(isset($pros_news[$eve_pro]['community_id']))
											{
												$stats = $display->chkdownloaded_pro_not_log($pros_news[$eve_pro]['id'],'community_project');
											}
										}
									}
									
									if($stats==1)
									{
										if($pros_news[$eve_pro]['type']!="fan_club")
										{
											$chk_ac_ep = 1;
										}
									}
									else
									{
										$chk_ac_ep = 0;
										if($pros_news[$eve_pro]['type']=="free_club")
										{
											//if($pros_news[$eve_pro]['ask_tip']=="0")
											//{
												$chk_ac_ep = 1;
											//}
										}
										else
										{
											if($_SESSION['login_email']!=NULL)
											{
												for($set_alss=0;$set_alss<count($down_button);$set_alss++)
												{
													if($down_button[$set_alss] !="")
													{
														if(isset($down_button[$set_alss]['artist_id']) && isset($pros_news[$eve_pro]['artist_id']))
														{
															$org_exp_ids = explode('~',$down_button[$set_alss]['id']);
															
															if($org_exp_ids[0]==$pros_news[$eve_pro]['id'])
															{
																$chk_ac_ep = 1;
																break;
															}
														}
														elseif(isset($down_button[$set_alss]['community_id']) && isset($pros_news[$eve_pro]['community_id']))
														{
															$org_exp_ids = explode('~',$down_button[$set_alss]['id']);
															
															if($org_exp_ids[0]==$pros_news[$eve_pro]['id'])
															{
																$chk_ac_ep = 1;
																break;
															}
														}
													}
												}
											}
										}
									}
									if($chk_ac_ep==0)
									{
								?>
										<a href="javascript:void(0);" onClick="<?php if($pros_news[$eve_pro]['type']=="fan_club") { echo "fancy_login_single('".$pros_news[$eve_pro]['profile_url']."')"; } elseif($pros_news[$eve_pro]['type']=="for_sale") { echo "click_fancy_login_down_single('".$pros_news[$eve_pro]['profile_url']."')"; } ?>"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>
							<?php
									}
									else
									{
										if($pros_news[$eve_pro]['type']=="free_club" && $pros_news[$eve_pro]['ask_tip']=='1' && $stats!=1)
										{
											if(isset($pros_news[$eve_pro]['artist_id']))
											{
												$sql_gens_down_tip = mysql_query("SELECT * FROM general_user WHERE artist_id='".$pros_news[$eve_pro]['artist_id']."'");
												$ans_gens_down_tip = mysql_fetch_assoc($sql_gens_down_tip);
												
												$tip_add_aftr_detils = $display->get_tip_details_for_every($pros_news[$eve_pro]['id'],'artist_project');
												$type_zip_down = 'art';
											}
											elseif(isset($pros_news[$eve_pro]['community_id']))
											{
												$sql_gens_down_tip = mysql_query("SELECT * FROM general_user WHERE community_id='".$pros_news[$eve_pro]['community_id']."'");
												$ans_gens_down_tip = mysql_fetch_assoc($sql_gens_down_tip);
												
												$tip_add_aftr_detils = $display->get_tip_details_for_every($pros_news[$eve_pro]['id'],'community_project');
												$type_zip_down = 'com';
											}
							?>
											<a href="javascript:void(0);" onClick="<?php echo "ask_to_donate_alpro('".mysql_real_escape_string($pros_news[$eve_pro]['title'])."','".mysql_real_escape_string($pros_news[$eve_pro]['creator'])."','". $pros_news[$eve_pro]['free_club_song']."','".$pros_news[$eve_pro]['profile_url']."','".$ans_gens_down_tip['general_user_id']."','".$tip_add_aftr_detils."','".$type_zip_down."','".$pros_news[$eve_pro]['id']."')"; ?>"><img src="../images/profile/download_down.gif" onmouseover="this.src='../images/profile/download-over_down.gif'" onmouseout="this.src='../images/profile/download_down.gif'" title="Add media to your cart for free download or purchase." /></a>
							<?php
										}
										else
										{
											if(isset($pros_news[$eve_pro]['artist_id']))
											{
												$type_zip_down = 'art';
											}
											else
											{
												$type_zip_down = 'com';
											}
								?>									
											<a href="javascript:void(0);" onClick="download_directly_project('<?php if($pros_news[$eve_pro]['type']=="fan_club") { echo $pros_news[$eve_pro]['fan_song']; } elseif($pros_news[$eve_pro]['type']=="for_sale") { echo $pros_news[$eve_pro]['sale_song']; } elseif($pros_news[$eve_pro]['type']=="free_club") { echo $pros_news[$eve_pro]['free_club_song']; } ?>','<?php echo $pros_news[$eve_pro]['title']; ?>','<?php echo $pros_news[$eve_pro]['creator'];?>','<?php echo $type_zip_down; ?>','<?php echo $pros_news[$eve_pro]['id']; ?>')"><img src="../images/profile/download_down.gif" onmouseover="this.src='../images/profile/download-over_down.gif'" onmouseout="this.src='../images/profile/download_down.gif'" title="Add media to your cart for free download or purchase." /></a>
							<?php		
										}
									}
								}
							}
						  ?>
						  <a class="fancybox fancybox.ajax" href="fan_club.php?data=<?php echo $pros_news[$eve_pro]['profile_url'];?>" id="fan_club_pro<?php echo $pros_news[$eve_pro]['profile_url'];?>" style="display:none;" ><?php if($pros_news[$eve_pro]['type']=="fan_club") { echo "Fan Club"; } elseif($pros_news[$eve_pro]['type']=="for_sale") { echo "Purchase"; } elseif($pros_news[$eve_pro]['type']=="free_club") { echo "Free"; } ?></a>
											<script type="text/javascript">
											$(document).ready(function() {
													/* $("#fan_club_pro<?php echo $pros_news[$eve_pro]['profile_url'];?>").fancybox({
													'width'				: '69%',
													'height'			: '80%',
													'transitionIn'		: 'none',
													'transitionOut'		: 'none',
													'type'				: 'iframe'
													}); */
													
													$("#fan_club_pro<?php echo $pros_news[$eve_pro]['profile_url'];?>").fancybox({
														helpers : {
															media : {},
															buttons : {},
															title : null
														}
													});
											});
											</script>
							<!--<a href="javascript:void(0);"><img src="../images/profile/play.gif" onmouseover="this.src='../images/profile/play-over.gif'" onmouseout="this.src='../images/profile/play.gif'" /></a>
							<a href="javascript:void(0);"><img src="../images/profile/add.gif"  onmouseover="this.src='../images/profile/add-over.gif'" onmouseout="this.src='../images/profile/add.gif'"/></a>-->
							<?php
							}
							?>
							<!--<a href=""><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" /></a>-->
						  </div>
						  <div class="type_subtype"><a  href ="<?php echo $pros_news[$eve_pro]['profile_url'];?>" onclick ="return chk_for_creator('<?php echo $pros_news[$eve_pro]['profile_url'];?>')">
						  <?php
							if(strlen($pros_news[$eve_pro]['title'])>39){
								echo substr($pros_news[$eve_pro]['title'],0,37)."...";
							}else{
								echo $pros_news[$eve_pro]['title'];
							}
								?></a><?php if(isset($pros_news[$eve_pro]['artist_id']))
												{
													$get_profile_type = $display->types_of_search('artist_project',$pros_news[$eve_pro]['id']);
													if(!empty($get_profile_type) && $get_profile_type!=Null)
													{
														$res_profile_type = mysql_fetch_assoc($get_profile_type);
														if($res_profile_type['type_name']!=""){
														?>
															<br/><span style="font-size:13px; font-weight:normal;">
															<?php 
															
															if(strlen($res_profile_type['type_name'])>26){
																echo substr($res_profile_type['type_name'],0,24)."...";
															}else{
																echo $res_profile_type['type_name'];
															}
															if($res_profile_type['subtype_name']!="" && strlen($res_profile_type['type_name']<26)){
																$type_length = strlen($res_profile_type['type_name']);
																if($type_length + strlen($res_profile_type['subtype_name'])>22){
																	echo ", ".substr($res_profile_type['subtype_name'],0,22 - $type_length)."...";
																}else{
																	echo ", ".$res_profile_type['subtype_name'];
																}
																
															}
															?>
															</span>
													<?php
														}
													}
												}
												elseif(isset($pros_news[$eve_pro]['community_id']))
												{
													$get_profile_type = $display->types_of_search('community_project',$pros_news[$eve_pro]['id']);
													if(!empty($get_profile_type) && $get_profile_type!=Null)
													{
														$res_profile_type = mysql_fetch_assoc($get_profile_type);
														if($res_profile_type['type_name']!=""){
														?>
															<br/><span style="font-size:13px; font-weight:normal;">
															<?php 
															if(strlen($res_profile_type['type_name'])>26){
																echo substr($res_profile_type['type_name'],0,24)."...";
															}else{
																echo $res_profile_type['type_name'];
															}
															if($res_profile_type['subtype_name']!="" && strlen($res_profile_type['type_name']<26)){
																$type_length = strlen($res_profile_type['type_name']);
																if($type_length + strlen($res_profile_type['subtype_name'])>22){
																	echo ", ".substr($res_profile_type['subtype_name'],0,22 - $type_length)."...";
																}else{
																	echo ", ".$res_profile_type['subtype_name'];
																}
																
															}
															?>
															</span>
													<?php
														}
													}
												} ?> </div></div>
						</div>
					<?php
				//}
			}
			?>
		<!--<input type="hidden" id="video_type1" value="<?php //echo $videotype1;?>"/>-->
		<input type="hidden" id="video_type_value1" value="<?php echo $videotype_value1;?>"/>
		<!--<input type="hidden" id="song_type1" value="<?php //echo $songtype1;?>"/>-->
		<input type="hidden" id="song_type_value1" value="<?php echo $songtype_value1;?>"/>
		<script type="text/javascript">
		$("document").ready(function(){
			var a = document.getElementById("video_type_value1").value;
			var b = document.getElementById("song_type_value1").value;
			if((a =="" || a ==" ") && (b=="" || b==" "))
			{
				$("#profileFeaturedproject").css({"display":"none"});
			}
		});
		
		</script>
		<?php
		}
		else
		{
		$project_new = array();
		for($new_co=0;$new_co<count($project_exp);$new_co++ )
		{
			while($row = mysql_fetch_assoc($get_project[$new_co]))
			{
				$project_new[] = $row;
			}
		}
		
		$sort = array();
		foreach($project_new as $k=>$v)
		{		
			$sort['page_views'][$k] = $v['page_views'];
			//$sort['from'][$k] = $end_f;
			//$sort['track'][$k] = $v['track'];
			//$sort['title'][$k] = $v['title'];
		}
		//var_dump($sort);
		if(!empty($sort))
		{
			array_multisort($sort['page_views'], SORT_DESC,$project_new);
		}
		
		for($new_co=0;$new_co<count($project_new);$new_co++ )
		{
			//while($row=mysql_fetch_assoc($get_project[$new_co]))
			//{
					//var_dump($row);
					if(isset($project_new[$new_co]['artist_id']))
					{
						$link = "add_artist_project.php?id=";
						$find_creator = $new_profile_class_obj ->find_creator($project_new[$new_co]['artist_id'],"artist");
					}
					else if(isset($project_new[$new_co]['community_id']))
					{
						$link = "add_community_project.php?id=";
						$find_creator = $new_profile_class_obj ->find_creator($project_new[$new_co]['community_id'],"community");
					}
					
					if(isset($project_new[$new_co]['artist_id']))
					{
						$sql_butts = mysql_query("SELECT * FROM general_user WHERE artist_id='".$project_new[$new_co]['artist_id']."'");
						$ans_butts_pros = mysql_fetch_assoc($sql_butts);
					}
					elseif(isset($project_new[$new_co]['community_id']))
					{
						$sql_butts = mysql_query("SELECT * FROM general_user WHERE community_id='".$project_new[$new_co]['community_id']."'");
						$ans_butts_pros = mysql_fetch_assoc($sql_butts);
					}
					
					$find_mem_butts_pro = $display->get_member_or_not($ans_butts_pros['general_user_id']);
					
				  ?>
					<div class="tabItem">
					<a  href ="<?php echo $project_new[$new_co]['profile_url'];?>" onclick ="return chk_for_creator('<?php echo $project_new[$new_co]['profile_url'];?>')"><img class="imgs_pro" src="<?php if(isset($project_new[$new_co]['image_name']) && ($project_new[$new_co]['image_name']!="" || $project_new[$new_co]['image_name']!=null)) { echo $project_new[$new_co]['image_name']; } else { echo "https://artproprofile.s3.amazonaws.com/Noimage.png"; } ?>" width="200" height="200" /></a>
					  <div class="both_wrap_per">
					  <div class="player">
					  <?php
							if($project_new[$new_co]['featured_media']=="Gallery" && $project_new[$new_co]['media_id']!=0)
							{
								$feature_reg_big_img = "";
								$all_register_images = "";
								$feature_reg_thum_path = "";
								$feature_reg_org_path = "";
								$featured_reg_gal_img_title = "";
									
								if($project_new[$new_co]['featured_media_table']!="" &&($project_new[$new_co]['featured_media_table']=="general_artist_gallery_list" || $project_new[$new_co]['featured_media_table']=="general_community_gallery_list"))
								{
									$get_register_featured_gallery = $new_profile_class_obj ->get_register_featured_media_gallery($project_new[$new_co]['media_id'],$project_new[$new_co]['featured_media_table']);
									$all_register_images ="";
									$feature_reg_big_img ="";
									$featured_reg_gal_img_title ="";
									for($cou_i=0;$cou_i<count($get_register_featured_gallery);$cou_i++)
									{
										$feature_reg_big_img = $get_register_featured_gallery[0]['image_name'];
										$all_register_images = $all_register_images.",".$get_register_featured_gallery[$cou_i]['image_name'];
										$featured_reg_gal_img_title = $featured_reg_gal_img_title .",".$get_register_featured_gallery[$cou_i]['image_title'];
									}
									$all_register_images = ltrim($all_register_images,',');
									$featured_reg_gal_img_title = ltrim($featured_reg_gal_img_title,',');
									$feature_reg_org_path = "https://reggallery.s3.amazonaws.com/";
									$feature_reg_thum_path = "https://reggalthumb.s3.amazonaws.com/";
								}
								else if($project_new[$new_co]['featured_media_table']!="" && $project_new[$new_co]['featured_media_table']=="general_media"){
									$get_register_featured_gallery = $new_profile_class_obj ->get_featured_gallery_images($project_new[$new_co]['media_id']);
									$all_register_images ="";
									$feature_reg_big_img ="";
									$featured_reg_gal_img_title ="";
									for($cou_i=0;$cou_i<count($get_register_featured_gallery);$cou_i++)
									{
										$feature_reg_big_img = $get_register_featured_gallery[0]['image_name'];
										$all_register_images = $all_register_images.",".$get_register_featured_gallery[$cou_i]['image_name'];
										$featured_reg_gal_img_title = $featured_reg_gal_img_title .",".$get_register_featured_gallery[$cou_i]['image_title'];
									}
									$all_register_images = ltrim($all_register_images,',');
									$featured_reg_gal_img_title = ltrim($featured_reg_gal_img_title,',');
									$feature_reg_org_path = "https://medgallery.s3.amazonaws.com/";
									$feature_reg_thum_path = "https://medgalthumb.s3.amazonaws.com/";
								}
							?>
							<a href="javascript:void(0);"><img src="../images/profile/play.gif" onmouseover="this.src='../images/profile/play-over.gif'" onmouseout="this.src='../images/profile/play.gif'" onclick="checkload('<?php echo $feature_reg_big_img;?>','<?php echo $all_register_images; ?>','<?php echo $feature_reg_thum_path; ?>','<?php echo $feature_reg_org_path; ?>','<?php echo $featured_reg_gal_img_title; ?>')"/></a>
							<!--<a href="javascript:void(0);"><img src="../images/profile/add.gif"  onmouseover="this.src='../images/profile/add-over.gif'" onmouseout="this.src='../images/profile/add.gif'" /></a>-->
								<?php
								$chk_ac_ep = 0;
								if($find_mem_butts_pro != null && $find_mem_butts_pro['general_user_id'] != null && ($find_mem_butts_pro['expiry_date'] >$date || $find_mem_butts_pro['lifetime']==1))
							{
								if($project_new[$new_co]['type']=="for_sale" || $project_new[$new_co]['type']=="fan_club" || $project_new[$new_co]['type']=="free_club")
								{
									$stats = 0;
									if($_SESSION['login_email']!=NULL)
									{
										if(isset($project_new[$new_co]['artist_id']))
										{
											$stats = $display->chkdownloaded_pro($project_new[$new_co]['id'],$_SESSION['login_id'],'artist_project');
										}
										elseif(isset($project_new[$new_co]['community_id']))
										{
											$stats = $display->chkdownloaded_pro($project_new[$new_co]['id'],$_SESSION['login_id'],'community_project');
										}
									}
									else
									{
										if($project_new[$new_co]['type']=="for_sale" || $project_new[$new_co]['type']=="free_club")
										{
											if(isset($project_new[$new_co]['artist_id']))
											{						
												$stats = $display->chkdownloaded_pro_not_log($project_new[$new_co]['id'],'artist_project');
											}
											if(isset($project_new[$new_co]['community_id']))
											{
												$stats = $display->chkdownloaded_pro_not_log($project_new[$new_co]['id'],'community_project');
											}
										}
									}
									
									if($stats==1)
									{
										if($project_new[$new_co]['type']!="fan_club")
										{
											$chk_ac_ep = 1;
										}
									}
									else
									{
										if($project_new[$new_co]['type']=="free_club")
										{
											//if($project_new[$new_co]['ask_tip']=="0")
											//{
												$chk_ac_ep = 1;
											//}
										}
										else
										{
											if($_SESSION['login_email']!=NULL)
											{
												for($set_alss=0;$set_alss<count($down_button);$set_alss++)
												{
													if($down_button[$set_alss] !="")
													{
														if(isset($down_button[$set_alss]['artist_id']) && isset($project_new[$new_co]['artist_id']))
														{
															$org_exp_ids = explode('~',$down_button[$set_alss]['id']);
															
															if($org_exp_ids[0]==$project_new[$new_co]['id'])
															{
																$chk_ac_ep = 1;
																break;
															}
														}
														elseif(isset($down_button[$set_alss]['community_id']) && isset($project_new[$new_co]['community_id']))
														{
															$org_exp_ids = explode('~',$down_button[$set_alss]['id']);
															
															if($org_exp_ids[0]==$project_new[$new_co]['id'])
															{
																$chk_ac_ep = 1;
																break;
															}
														}
													}
												}
											}
										}
									}
									if($chk_ac_ep==0)
									{
										if(isset($project_new[$new_co]['artist_id']))
										{
											$sql_gens_down_tip = mysql_query("SELECT * FROM general_user WHERE artist_id='".$project_new[$new_co]['artist_id']."'");
											$ans_gens_down_tip = mysql_fetch_assoc($sql_gens_down_tip);
											
											$tip_add_aftr_detils = $display->get_tip_details_for_every($project_new[$new_co]['id'],'artist_project');
											$type_zip_down = 'art';
										}
										elseif(isset($project_new[$new_co]['community_id']))
										{
											$sql_gens_down_tip = mysql_query("SELECT * FROM general_user WHERE community_id='".$project_new[$new_co]['community_id']."'");
											$ans_gens_down_tip = mysql_fetch_assoc($sql_gens_down_tip);
											
											$tip_add_aftr_detils = $display->get_tip_details_for_every($project_new[$new_co]['id'],'community_project');
											$type_zip_down = 'com';
										}
							?>
										<a href="javascript:void(0);" onClick="<?php if($project_new[$new_co]['type']=="fan_club") { echo "fancy_login_single('".$project_new[$new_co]['profile_url']."')"; } elseif($project_new[$new_co]['type']=="for_sale") { echo "click_fancy_login_down_single('".$project_new[$new_co]['profile_url']."')"; } elseif($project_new[$new_co]['type']=="free_club") { if($project_new[$new_co]['ask_tip']=='0') { echo "download_directly_project('".$project_new[$new_co]['free_club_song']."','".$project_new[$new_co]['title']."','".$project_new[$new_co]['creator']."','".$type_zip_down."','".$project_new[$new_co]['id']."')"; } else { echo "ask_to_donate_alpro('".mysql_real_escape_string($project_new[$new_co]['title'])."','".mysql_real_escape_string($project_new[$new_co]['creator'])."','". $project_new[$new_co]['free_club_song']."','".$project_new[$new_co]['profile_url']."','".$ans_gens_down_tip['general_user_id']."','".$tip_add_aftr_detils."','".$type_zip_down."','".$project_new[$new_co]['id']."')"; } } ?>"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>
							<?php
									}
									else
									{
										if($project_new[$new_co]['type']=="free_club" && $project_new[$new_co]['ask_tip']=='1' && $stats!=1)
										{
											if(isset($project_new[$new_co]['artist_id']))
											{
												$sql_gens_down_tip = mysql_query("SELECT * FROM general_user WHERE artist_id='".$project_new[$new_co]['artist_id']."'");
												$ans_gens_down_tip = mysql_fetch_assoc($sql_gens_down_tip);
												
												$tip_add_aftr_detils = $display->get_tip_details_for_every($project_new[$new_co]['id'],'artist_project');
												$type_zip_down = 'art';
											}
											elseif(isset($project_new[$new_co]['community_id']))
											{
												$sql_gens_down_tip = mysql_query("SELECT * FROM general_user WHERE community_id='".$project_new[$new_co]['community_id']."'");
												$ans_gens_down_tip = mysql_fetch_assoc($sql_gens_down_tip);
												
												$tip_add_aftr_detils = $display->get_tip_details_for_every($project_new[$new_co]['id'],'community_project');
												$type_zip_down = 'com';
											}
							?>
											<a href="javascript:void(0);" onClick="<?php echo "ask_to_donate_alpro('".mysql_real_escape_string($project_new[$new_co]['title'])."','".mysql_real_escape_string($project_new[$new_co]['creator'])."','". $project_new[$new_co]['free_club_song']."','".$project_new[$new_co]['profile_url']."','".$ans_gens_down_tip['general_user_id']."','".$tip_add_aftr_detils."','".$type_zip_down."','".$project_new[$new_co]['id']."')"; ?>"><img src="../images/profile/download_down.gif" onmouseover="this.src='../images/profile/download-over_down.gif'" onmouseout="this.src='../images/profile/download_down.gif'" title="Add media to your cart for free download or purchase." /></a>
							<?php
										}
										else
										{
											if(isset($project_new[$new_co]['artist_id']))
											{
												$type_zip_down = 'art';
											}
											else
											{
												$type_zip_down = 'com';
											}
								?>									
											<a href="javascript:void(0);" onClick="download_directly_project('<?php if($project_new[$new_co]['type']=="fan_club") { echo $project_new[$new_co]['fan_song']; } elseif($project_new[$new_co]['type']=="for_sale") { echo $project_new[$new_co]['sale_song']; } elseif($project_new[$new_co]['type']=="free_club") { echo $project_new[$new_co]['free_club_song']; } ?>','<?php echo $project_new[$new_co]['title']; ?>','<?php echo $project_new[$new_co]['creator'];?>','<?php echo $type_zip_down; ?>','<?php echo $project_new[$new_co]['id']; ?>')"><img src="../images/profile/download_down.gif" onmouseover="this.src='../images/profile/download-over_down.gif'" onmouseout="this.src='../images/profile/download_down.gif'" title="Add media to your cart for free download or purchase." /></a>
							<?php		
										}
									}
								}
							}
								?>
								<a class="fancybox fancybox.ajax" href="fan_club.php?data=<?php echo $project_new[$new_co]['profile_url'];?>" id="fan_club_pro<?php echo $project_new[$new_co]['profile_url'];?>" style="display:none;" ><?php if($project_new[$new_co]['type']=="fan_club") { echo "Fan Club"; } elseif($project_new[$new_co]['type']=="for_sale") { echo "Purchase"; } elseif($project_new[$new_co]['type']=="free_club") { echo "Free"; } ?></a>
											<script type="text/javascript">
											$(document).ready(function() {
												/* $("#fan_club_pro<?php echo $project_new[$new_co]['profile_url'];?>").fancybox({
												'width'				: '69%',
												'height'			: '80%',
												'transitionIn'		: 'none',
												'transitionOut'		: 'none',
												'type'				: 'iframe'
												}); */
												$("#fan_club_pro<?php echo $project_new[$new_co]['profile_url'];?>").fancybox({
													helpers : {
														media : {},
														buttons : {},
														title : null
													}
												});
											});
											</script>
								<?php
							}
							else if($project_new[$new_co]['featured_media']=="Song" && $project_new[$new_co]['media_id']!=0)
							{
								if($project_new[$new_co]['featured_media_table']=="general_artist_audio" || $project_new[$new_co]['featured_media_table']=="general_community_audio")
								{
									?>
										<a href="javascript:void(0);"><img src="../images/profile/play.gif" onmouseover="this.src='../images/profile/play-over.gif'" onmouseout="this.src='../images/profile/play.gif'" onclick="showPlayer_reg('a','<?php echo $project_new[$new_co]['media_id'];?>','play_reg','<?php echo $project_new[$new_co]['featured_media_table'];?>')" title="Play file now."/></a>
										<a href="javascript:void(0);"><img src="../images/profile/add.gif"  onmouseover="this.src='../images/profile/add-over.gif'" onmouseout="this.src='../images/profile/add.gif'" onclick="showPlayer_reg('a','<?php echo $project_new[$new_co]['media_id'];?>','add_reg','<?php echo $project_new[$new_co]['featured_media_table'];?>')" title="Add file to the bottom of your playlist without stopping current media."/></a>
									<?php
								}
								else{
									?>
										<a href="javascript:void(0);"><img src="../images/profile/play.gif" onmouseover="this.src='../images/profile/play-over.gif'" onmouseout="this.src='../images/profile/play.gif'" onclick="showPlayer('a','<?php echo $project_new[$new_co]['media_id'];?>','play')" title="Play file now."/></a>
										<a href="javascript:void(0);"><img src="../images/profile/add.gif"  onmouseover="this.src='../images/profile/add-over.gif'" onmouseout="this.src='../images/profile/add.gif'" onclick="showPlayer('a','<?php echo $project_new[$new_co]['media_id'];?>','add')" title="Add file to the bottom of your playlist without stopping current media."/></a>
									<?php
								}

								$song_image_name_cartproject ="";
								$chk_ac_ep = 0;
								if($find_mem_butts_pro != null && $find_mem_butts_pro['general_user_id'] != null && ($find_mem_butts_pro['expiry_date'] >$date || $find_mem_butts_pro['lifetime']==1))
							{
								if($project_new[$new_co]['type']=="for_sale" || $project_new[$new_co]['type']=="fan_club" || $project_new[$new_co]['type']=="free_club")
								{
									$stats = 0;
									if($_SESSION['login_email']!=NULL)
									{
										if(isset($project_new[$new_co]['artist_id']))
										{
											$stats = $display->chkdownloaded_pro($project_new[$new_co]['id'],$_SESSION['login_id'],'artist_project');
										}
										elseif(isset($project_new[$new_co]['community_id']))
										{
											$stats = $display->chkdownloaded_pro($project_new[$new_co]['id'],$_SESSION['login_id'],'community_project');
										}
									}
									else
									{
										if($project_new[$new_co]['type']=="for_sale" || $project_new[$new_co]['type']=="free_club")
										{
											if(isset($project_new[$new_co]['artist_id']))
											{						
												$stats = $display->chkdownloaded_pro_not_log($project_new[$new_co]['id'],'artist_project');
											}
											if(isset($project_new[$new_co]['community_id']))
											{
												$stats = $display->chkdownloaded_pro_not_log($project_new[$new_co]['id'],'community_project');
											}
										}
									}
									
									if($stats==1)
									{
										if($project_new[$new_co]['type']!="fan_club")
										{
											$chk_ac_ep = 1;
										}
									}
									else
									{
										if($project_new[$new_co]['type']=="free_club")
										{
											//if($project_new[$new_co]['ask_tip']=="0")
											//{
												$chk_ac_ep = 1;
											//}
										}
										else
										{
											if($_SESSION['login_email']!=NULL)
											{
												for($set_alss=0;$set_alss<count($down_button);$set_alss++)
												{
													if($down_button[$set_alss] !="")
													{
														if(isset($down_button[$set_alss]['artist_id']) && isset($project_new[$new_co]['artist_id']))
														{	
															$org_exp_ids = explode('~',$down_button[$set_alss]['id']);
															
															if($org_exp_ids[0]==$project_new[$new_co]['id'])
															{
																$chk_ac_ep = 1;
																break;
															}
														}
														elseif(isset($down_button[$set_alss]['community_id']) && isset($project_new[$new_co]['community_id']))
														{
															$org_exp_ids = explode('~',$down_button[$set_alss]['id']);
															
															if($org_exp_ids[0]==$project_new[$new_co]['id'])
															{
																$chk_ac_ep = 1;
																break;
															}
														}
													}
												}
											}
										}
									}
									if($chk_ac_ep==0)
									{
										if(isset($project_new[$new_co]['artist_id']))
										{
											$sql_gens_down_tip = mysql_query("SELECT * FROM general_user WHERE artist_id='".$project_new[$new_co]['artist_id']."'");
											$ans_gens_down_tip = mysql_fetch_assoc($sql_gens_down_tip);
											
											$tip_add_aftr_detils = $display->get_tip_details_for_every($project_new[$new_co]['id'],'artist_project');
											$type_zip_down = 'art';
										}
										elseif(isset($project_new[$new_co]['community_id']))
										{
											$sql_gens_down_tip = mysql_query("SELECT * FROM general_user WHERE community_id='".$project_new[$new_co]['community_id']."'");
											$ans_gens_down_tip = mysql_fetch_assoc($sql_gens_down_tip);
											
											$tip_add_aftr_detils = $display->get_tip_details_for_every($project_new[$new_co]['id'],'community_project');
											$type_zip_down = 'com';
										}
							?>
										<a href="javascript:void(0);" onClick="<?php if($project_new[$new_co]['type']=="fan_club") { echo "fancy_login_single('".$project_new[$new_co]['profile_url']."')"; } elseif($project_new[$new_co]['type']=="for_sale") { echo "click_fancy_login_down_single('".$project_new[$new_co]['profile_url']."')"; } elseif($project_new[$new_co]['type']=="free_club") { if($project_new[$new_co]['ask_tip']=='0') { echo "download_directly_project('".$project_new[$new_co]['free_club_song']."','".$project_new[$new_co]['title']."','".$project_new[$new_co]['creator']."','".$type_zip_down."','".$project_new[$new_co]['id']."')"; } else { echo "ask_to_donate_alpro('".mysql_real_escape_string($project_new[$new_co]['title'])."','".mysql_real_escape_string($project_new[$new_co]['creator'])."','". $project_new[$new_co]['free_club_song']."','".$project_new[$new_co]['profile_url']."','".$ans_gens_down_tip['general_user_id']."','".$tip_add_aftr_detils."','".$type_zip_down."','".$project_new[$new_co]['id']."')"; } } ?>"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>
							<?php
									}
									else
									{
										if($project_new[$new_co]['type']=="free_club" && $project_new[$new_co]['ask_tip']=='1' && $stats!=1)
										{
											if(isset($project_new[$new_co]['artist_id']))
											{
												$sql_gens_down_tip = mysql_query("SELECT * FROM general_user WHERE artist_id='".$project_new[$new_co]['artist_id']."'");
												$ans_gens_down_tip = mysql_fetch_assoc($sql_gens_down_tip);
												
												$tip_add_aftr_detils = $display->get_tip_details_for_every($project_new[$new_co]['id'],'artist_project');
												$type_zip_down = 'art';
											}
											elseif(isset($project_new[$new_co]['community_id']))
											{
												$sql_gens_down_tip = mysql_query("SELECT * FROM general_user WHERE community_id='".$project_new[$new_co]['community_id']."'");
												$ans_gens_down_tip = mysql_fetch_assoc($sql_gens_down_tip);
												
												$tip_add_aftr_detils = $display->get_tip_details_for_every($project_new[$new_co]['id'],'community_project');
												$type_zip_down = 'com';
											}
							?>
											
											<a href="javascript:void(0);" onClick="<?php echo "ask_to_donate_alpro('".mysql_real_escape_string($project_new[$new_co]['title'])."','".mysql_real_escape_string($project_new[$new_co]['creator'])."','". $project_new[$new_co]['free_club_song']."','".$project_new[$new_co]['profile_url']."','".$ans_gens_down_tip['general_user_id']."','".$tip_add_aftr_detils."','".$type_zip_down."','".$project_new[$new_co]['id']."')"; ?>"><img src="../images/profile/download_down.gif" onmouseover="this.src='../images/profile/download-over_down.gif'" onmouseout="this.src='../images/profile/download_down.gif'" title="Add media to your cart for free download or purchase." /></a>
							<?php
										}
										else
										{
											if(isset($project_new[$new_co]['artist_id']))
											{
												$type_zip_down = 'art';
											}
											else
											{
												$type_zip_down = 'com';
											}
								?>									
											<a href="javascript:void(0);" onClick="download_directly_project('<?php if($project_new[$new_co]['type']=="fan_club") { echo $project_new[$new_co]['fan_song']; } elseif($project_new[$new_co]['type']=="for_sale") { echo $project_new[$new_co]['sale_song']; } elseif($project_new[$new_co]['type']=="free_club") { echo $project_new[$new_co]['free_club_song']; } ?>','<?php echo $project_new[$new_co]['title']; ?>','<?php echo $project_new[$new_co]['creator'];?>','<?php echo $type_zip_down; ?>','<?php echo $project_new[$new_co]['id']; ?>')"><img src="../images/profile/download_down.gif" onmouseover="this.src='../images/profile/download-over_down.gif'" onmouseout="this.src='../images/profile/download_down.gif'" title="Add media to your cart for free download or purchase." /></a>
							<?php	
										}
									}
								}
							}
								?>
									<a class="fancybox fancybox.ajax" href="fan_club.php?data=<?php echo $project_new[$new_co]['profile_url'];?>" id="fan_club_pro<?php echo $project_new[$new_co]['profile_url'];?>" style="display:none;" ><?php if($project_new[$new_co]['type']=="fan_club") { echo "Fan Club"; } elseif($project_new[$new_co]['type']=="for_sale") { echo "Purchase"; } elseif($project_new[$new_co]['type']=="free_club") { echo "Free"; } ?></a>
											<script type="text/javascript">
											$(document).ready(function() {
													/* $("#fan_club_pro<?php echo $project_new[$new_co]['profile_url'];?>").fancybox({
													'width'				: '69%',
													'height'			: '80%',
													'transitionIn'		: 'none',
													'transitionOut'		: 'none',
													'type'				: 'iframe'
													});  */
													$("#fan_club_pro<?php echo $project_new[$new_co]['profile_url'];?>").fancybox({
														helpers : {
															media : {},
															buttons : {},
															title : null
														}
													});
											});
											</script>
								<?php
								$songtype2 = "v";
								if($project_new[$new_co]['media_id']!="" && $project_new[$new_co]['media_id']!=0){
									$songtype_value2 = $songtype_value2 .",".$project_new[$new_co]['media_id'];
								}
								
							}
							else if($project_new[$new_co]['featured_media']=="Video" && $project_new[$new_co]['media_id']!=0)
							{
								if($project_new[$new_co]['featured_media_table']=="general_artist_video" || $project_new[$new_co]['featured_media_table']=="general_community_video")
								{
									?>
										<a href="javascript:void(0);"><img src="../images/profile/play.gif" onmouseover="this.src='../images/profile/play-over.gif'" onmouseout="this.src='../images/profile/play.gif'" onclick="showPlayer_reg('v','<?php echo $project_new[$new_co]['media_id'];?>','playlist_reg','<?php echo $project_new[$new_co]['featured_media_table'];?>')" title="Play file now."/></a>
										<a href="javascript:void(0);"><img src="../images/profile/add.gif"  onmouseover="this.src='../images/profile/add-over.gif'" onmouseout="this.src='../images/profile/add.gif'" onclick="showPlayer_reg('v','<?php echo $project_new[$new_co]['media_id'];?>','addvi_reg','<?php echo $project_new[$new_co]['featured_media_table'];?>')" title="Add file to the bottom of your playlist without stopping current media."/></a>
									<?php
								}else{
									?>
										<a href="javascript:void(0);"><img src="../images/profile/play.gif" onmouseover="this.src='../images/profile/play-over.gif'" onmouseout="this.src='../images/profile/play.gif'" onclick="showPlayer('v','<?php echo $project_new[$new_co]['media_id'];?>','playlist')" title="Play file now."/></a>
										<a href="javascript:void(0);"><img src="../images/profile/add.gif"  onmouseover="this.src='../images/profile/add-over.gif'" onmouseout="this.src='../images/profile/add.gif'" onclick="showPlayer('v','<?php echo $project_new[$new_co]['media_id'];?>','addvi')" title="Add file to the bottom of your playlist without stopping current media."/></a>
									<?php
								}
								$image_name_cartproject = "";
								$chk_ac_ep = 0;
								if($find_mem_butts_pro != null && $find_mem_butts_pro['general_user_id'] != null && ($find_mem_butts_pro['expiry_date'] >$date || $find_mem_butts_pro['lifetime']==1))
							{
								if($project_new[$new_co]['type']=="for_sale" || $project_new[$new_co]['type']=="fan_club" || $project_new[$new_co]['type']=="free_club")
								{
									$stats = 0;
									if($_SESSION['login_email']!=NULL)
									{
										if(isset($project_new[$new_co]['artist_id']))
										{
											$stats = $display->chkdownloaded_pro($project_new[$new_co]['id'],$_SESSION['login_id'],'artist_project');
										}
										elseif(isset($project_new[$new_co]['community_id']))
										{
											$stats = $display->chkdownloaded_pro($project_new[$new_co]['id'],$_SESSION['login_id'],'community_project');
										}
									}
									else
									{
										if($project_new[$new_co]['type']=="for_sale" || $project_new[$new_co]['type']=="free_club")
										{
											if(isset($project_new[$new_co]['artist_id']))
											{						
												$stats = $display->chkdownloaded_pro_not_log($project_new[$new_co]['id'],'artist_project');
											}
											if(isset($project_new[$new_co]['community_id']))
											{
												$stats = $display->chkdownloaded_pro_not_log($project_new[$new_co]['id'],'community_project');
											}
										}
									}
									
									if($stats==1)
									{
										if($project_new[$new_co]['type']!="fan_club")
										{
											$chk_ac_ep = 1;
										}
									}
									else
									{
										if($project_new[$new_co]['type']=="free_club")
										{
											//if($project_new[$new_co]['ask_tip']=="0")
											//{
												$chk_ac_ep = 1;
											//}
										}
										else
										{
											if($_SESSION['login_email']!=NULL)
											{
												for($set_alss=0;$set_alss<count($down_button);$set_alss++)
												{
													if($down_button[$set_alss] !="")
													{
														if(isset($down_button[$set_alss]['artist_id']) && isset($project_new[$new_co]['artist_id']))
														{	
															$org_exp_ids = explode('~',$down_button[$set_alss]['id']);
															
															if($org_exp_ids[0]==$project_new[$new_co]['id'])
															{
																$chk_ac_ep = 1;
																break;
															}
														}
														elseif(isset($down_button[$set_alss]['community_id']) && isset($project_new[$new_co]['community_id']))
														{
															$org_exp_ids = explode('~',$down_button[$set_alss]['id']);
															
															if($org_exp_ids[0]==$project_new[$new_co]['id'])
															{
																$chk_ac_ep = 1;
																break;
															}
														}
													}
												}
											}
										}
									}
									if($chk_ac_ep==0)
									{
							?>
										<a href="javascript:void(0);" onClick="<?php if($project_new[$new_co]['type']=="fan_club") { echo "fancy_login_single('".$project_new[$new_co]['profile_url']."')"; } elseif($project_new[$new_co]['type']=="for_sale") { echo "click_fancy_login_down_single('".$project_new[$new_co]['profile_url']."')"; } ?>"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>
							<?php
									}
									else
									{
										if($project_new[$new_co]['type']=="free_club" && $project_new[$new_co]['ask_tip']=='1' && $stats!=1)
										{
											if(isset($project_new[$new_co]['artist_id']))
											{
												$sql_gens_down_tip = mysql_query("SELECT * FROM general_user WHERE artist_id='".$project_new[$new_co]['artist_id']."'");
												$ans_gens_down_tip = mysql_fetch_assoc($sql_gens_down_tip);
												
												$tip_add_aftr_detils = $display->get_tip_details_for_every($project_new[$new_co]['id'],'artist_project');
												$type_zip_down = 'art';
											}
											elseif(isset($project_new[$new_co]['community_id']))
											{
												$sql_gens_down_tip = mysql_query("SELECT * FROM general_user WHERE community_id='".$project_new[$new_co]['community_id']."'");
												$ans_gens_down_tip = mysql_fetch_assoc($sql_gens_down_tip);
												
												$tip_add_aftr_detils = $display->get_tip_details_for_every($project_new[$new_co]['id'],'community_project');
												$type_zip_down = 'com';
											}
							?>
											<a href="javascript:void(0);" onClick="<?php echo "ask_to_donate_alpro('".mysql_real_escape_string($project_new[$new_co]['title'])."','".mysql_real_escape_string($project_new[$new_co]['creator'])."','". $project_new[$new_co]['free_club_song']."','".$project_new[$new_co]['profile_url']."','".$ans_gens_down_tip['general_user_id']."','".$tip_add_aftr_detils."','".$type_zip_down."','".$project_new[$new_co]['id']."')"; ?>"><img src="../images/profile/download_down.gif" onmouseover="this.src='../images/profile/download-over_down.gif'" onmouseout="this.src='../images/profile/download_down.gif'" title="Add media to your cart for free download or purchase." /></a>
							<?php
										}
										else
										{
											if(isset($project_new[$new_co]['artist_id']))
											{
												$type_zip_down = 'art';
											}
											else
											{
												$type_zip_down = 'com';
											}
								?>									
											<a href="javascript:void(0);" onClick="download_directly_project('<?php if($project_new[$new_co]['type']=="fan_club") { echo $project_new[$new_co]['fan_song']; } elseif($project_new[$new_co]['type']=="for_sale") { echo $project_new[$new_co]['sale_song']; } elseif($project_new[$new_co]['type']=="free_club") { echo $project_new[$new_co]['free_club_song']; } ?>','<?php echo $project_new[$new_co]['title']; ?>','<?php echo $project_new[$new_co]['creator'];?>','<?php echo $type_zip_down; ?>','<?php echo $project_new[$new_co]['id']; ?>')"><img src="../images/profile/download_down.gif" onmouseover="this.src='../images/profile/download-over_down.gif'" onmouseout="this.src='../images/profile/download_down.gif'" title="Add media to your cart for free download or purchase." /></a>
							<?php		
										}
									}
								}
							}
								?>
									<a class="fancybox fancybox.ajax" href="fan_club.php?data=<?php echo $project_new[$new_co]['profile_url'];?>" id="fan_club_pro<?php echo $project_new[$new_co]['profile_url'];?>" style="display:none;" ><?php if($project_new[$new_co]['type']=="fan_club") { echo "Fan Club"; } elseif($project_new[$new_co]['type']=="for_sale") { echo "Purchase"; } elseif($project_new[$new_co]['type']=="free_club") { echo "Free"; } ?></a>
											<script type="text/javascript">
											$(document).ready(function() {
													/* $("#fan_club_pro<?php echo $project_new[$new_co]['profile_url'];?>").fancybox({
													'width'				: '69%',
													'height'			: '80%',
													'transitionIn'		: 'none',
													'transitionOut'		: 'none',
													'type'				: 'iframe'
													}); */
													
													$("#fan_club_pro<?php echo $project_new[$new_co]['profile_url'];?>").fancybox({
														helpers : {
															media : {},
															buttons : {},
															title : null
														}
													});
											});
											</script>
								<?php
								$videotype2 = "v";
								if($project_new[$new_co]['media_id']!="" && $project_new[$new_co]['media_id']!=0){
									$videotype_value2 = $videotype_value2 .",".$project_new[$new_co]['media_id'];
								}
								
							}
							else{
							
								$chk_ac_ep = 0;
								if($find_mem_butts_pro != null && $find_mem_butts_pro['general_user_id'] != null && ($find_mem_butts_pro['expiry_date'] >$date || $find_mem_butts_pro['lifetime']==1))
							{
								if($project_new[$new_co]['type']=="for_sale" || $project_new[$new_co]['type']=="fan_club" || $project_new[$new_co]['type']=="free_club")
								{
									$stats = 0;
									if($_SESSION['login_email']!=NULL)
									{
										if(isset($project_new[$new_co]['artist_id']))
										{
											$stats = $display->chkdownloaded_pro($project_new[$new_co]['id'],$_SESSION['login_id'],'artist_project');
										}
										elseif(isset($project_new[$new_co]['community_id']))
										{
											$stats = $display->chkdownloaded_pro($project_new[$new_co]['id'],$_SESSION['login_id'],'community_project');
										}
									}
									else
									{
										if($project_new[$new_co]['type']=="for_sale" || $project_new[$new_co]['type']=="free_club")
										{
											if(isset($project_new[$new_co]['artist_id']))
											{						
												$stats = $display->chkdownloaded_pro_not_log($project_new[$new_co]['id'],'artist_project');
											}
											if(isset($project_new[$new_co]['community_id']))
											{
												$stats = $display->chkdownloaded_pro_not_log($project_new[$new_co]['id'],'community_project');
											}
										}
									}
									
									if($stats==1)
									{
										if($project_new[$new_co]['type']!="fan_club")
										{
											$chk_ac_ep = 1;
										}
									}
									else
									{
										if($project_new[$new_co]['type']=="free_club")
										{
											//if($project_new[$new_co]['ask_tip']=='0')
											//{
												$chk_ac_ep = 1;
											//}
										}
										else
										{
											if($_SESSION['login_email']!=NULL)
											{
												for($set_alss=0;$set_alss<count($down_button);$set_alss++)
												{
													if($down_button[$set_alss] !="")
													{
														if(isset($down_button[$set_alss]['artist_id']) && isset($project_new[$new_co]['artist_id']))
														{	
															$org_exp_ids = explode('~',$down_button[$set_alss]['id']);
															
															if($org_exp_ids[0]==$project_new[$new_co]['id'])
															{
																$chk_ac_ep = 1;
																break;
															}
														}
														elseif(isset($down_button[$set_alss]['community_id']) && isset($project_new[$new_co]['community_id']))
														{
															$org_exp_ids = explode('~',$down_button[$set_alss]['id']);
															
															if($org_exp_ids[0]==$project_new[$new_co]['id'])
															{
																$chk_ac_ep = 1;
																break;
															}
														}
													}
												}
											}
										}
									}
									if($chk_ac_ep==0)
									{
							?>
										<a href="javascript:void(0);" onClick="<?php if($project_new[$new_co]['type']=="fan_club") { echo "fancy_login_single('".$project_new[$new_co]['profile_url']."')"; } elseif($project_new[$new_co]['type']=="for_sale") { echo "click_fancy_login_down_single('".$project_new[$new_co]['profile_url']."')"; } ?>"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>
										
							<?php
									}
									else
									{
										if($project_new[$new_co]['type']=="free_club" && $project_new[$new_co]['ask_tip']=='1' && $stats!=1)
										{
											if(isset($project_new[$new_co]['artist_id']))
											{
												$sql_gens_down_tip = mysql_query("SELECT * FROM general_user WHERE artist_id='".$project_new[$new_co]['artist_id']."'");
												$ans_gens_down_tip = mysql_fetch_assoc($sql_gens_down_tip);
												
												$tip_add_aftr_detils = $display->get_tip_details_for_every($project_new[$new_co]['id'],'artist_project');
												$type_zip_down = 'art';
											}
											elseif(isset($project_new[$new_co]['community_id']))
											{
												$sql_gens_down_tip = mysql_query("SELECT * FROM general_user WHERE community_id='".$project_new[$new_co]['community_id']."'");
												$ans_gens_down_tip = mysql_fetch_assoc($sql_gens_down_tip);
												
												$tip_add_aftr_detils = $display->get_tip_details_for_every($project_new[$new_co]['id'],'community_project');
												$type_zip_down = 'com';
											}
							?>
											<a href="javascript:void(0);" onClick="<?php echo "ask_to_donate_alpro('".mysql_real_escape_string($project_new[$new_co]['title'])."','".mysql_real_escape_string($project_new[$new_co]['creator'])."','". $project_new[$new_co]['free_club_song']."','".$project_new[$new_co]['profile_url']."','".$ans_gens_down_tip['general_user_id']."','".$tip_add_aftr_detils."','".$type_zip_down."','".$project_new[$new_co]['id']."')"; ?>"><img src="../images/profile/download_down.gif" onmouseover="this.src='../images/profile/download-over_down.gif'" onmouseout="this.src='../images/profile/download_down.gif'" title="Add media to your cart for free download or purchase." /></a>
							<?php
										}
										else
										{
											if(isset($project_new[$new_co]['artist_id']))
											{
												$type_zip_down = 'art';
											}
											else
											{
												$type_zip_down = 'com';
											}
								?>									
											<a href="javascript:void(0);" onClick="download_directly_project('<?php if($project_new[$new_co]['type']=="fan_club") { echo $project_new[$new_co]['fan_song']; } elseif($project_new[$new_co]['type']=="for_sale") { echo $project_new[$new_co]['sale_song']; } elseif($project_new[$new_co]['type']=="free_club") { echo $project_new[$new_co]['free_club_song']; } ?>','<?php echo $project_new[$new_co]['title']; ?>','<?php echo $project_new[$new_co]['creator'];?>','<?php echo $type_zip_down; ?>','<?php echo $project_new[$new_co]['id']; ?>')"><img src="../images/profile/download_down.gif" onmouseover="this.src='../images/profile/download-over_down.gif'" onmouseout="this.src='../images/profile/download_down.gif'" title="Add media to your cart for free download or purchase." /></a>
							<?php		
										}
									}
								}
							}
						  ?>
						  <a class="fancybox fancybox.ajax" href="fan_club.php?data=<?php echo $project_new[$new_co]['profile_url'];?>" id="fan_club_pro<?php echo $project_new[$new_co]['profile_url'];?>" style="display:none;" ><?php if($project_new[$new_co]['type']=="fan_club") { echo "Fan Club"; } elseif($project_new[$new_co]['type']=="for_sale") { echo "Purchase"; } elseif($project_new[$new_co]['type']=="free_club") { echo "Free"; } ?></a>
							<script type="text/javascript">
								$(document).ready(function() {
										/* $("#fan_club_pro<?php echo $project_new[$new_co]['profile_url'];?>").fancybox({
										'width'				: '69%',
										'height'			: '80%',
										'transitionIn'		: 'none',
										'transitionOut'		: 'none',
										'type'				: 'iframe'
										}); */
										
										$("#fan_club_pro<?php echo $project_new[$new_co]['profile_url'];?>").fancybox({
														helpers : {
															media : {},
															buttons : {},
															title : null
														}
													});
								});
							</script>
						<!--<a href="javascript:void(0);"><img src="../images/profile/play.gif" onmouseover="this.src='../images/profile/play-over.gif'" onmouseout="this.src='../images/profile/play.gif'" /></a>
						<a href="javascript:void(0);"><img src="../images/profile/add.gif"  onmouseover="this.src='../images/profile/add-over.gif'" onmouseout="this.src='../images/profile/add.gif'"/></a>-->
						<?php
						}
						?>
						<!--<a href=""><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" /></a>-->
						
					  </div>
					  <div class="type_subtype"><a  href ="<?php echo $project_new[$new_co]['profile_url'];?>" onclick ="return chk_for_creator('<?php echo $project_new[$new_co]['profile_url'];?>')">
					  <?php 
						if(strlen($project_new[$new_co]['title'])>39){
							echo  substr($project_new[$new_co]['title'],0,37)."...";
						}else{
							echo $project_new[$new_co]['title'];
						}
					  ?></a><?php if(isset($project_new[$new_co]['artist_id']))
												{
													$get_profile_type = $display->types_of_search('artist_project',$project_new[$new_co]['id']);
													if(!empty($get_profile_type) && $get_profile_type!=Null)
													{
														$res_profile_type = mysql_fetch_assoc($get_profile_type);
														if($res_profile_type['type_name']!=""){
														?>
															<br/><span style="font-size:13px; font-weight:normal;">
															<?php 
															
															if(strlen($res_profile_type['type_name'])>26){
																echo substr($res_profile_type['type_name'],0,24)."...";
															}else{
																echo $res_profile_type['type_name'];
															}
															if($res_profile_type['subtype_name']!="" && strlen($res_profile_type['type_name']<26)){
																$type_length = strlen($res_profile_type['type_name']);
																if($type_length + strlen($res_profile_type['subtype_name'])>22){
																	echo ", ".substr($res_profile_type['subtype_name'],0,22 - $type_length)."...";
																}else{
																	echo ", ".$res_profile_type['subtype_name'];
																}
																
															}
															?>
															</span>
													<?php
														}
													}
												}
												elseif(isset($project_new[$new_co]['community_id']))
												{
													$get_profile_type = $display->types_of_search('community_project',$project_new[$new_co]['id']);
													if(!empty($get_profile_type) && $get_profile_type!=Null)
													{
														$res_profile_type = mysql_fetch_assoc($get_profile_type);
														if($res_profile_type['type_name']!=""){
														?>
															<br/><span style="font-size:13px; font-weight:normal;">
															<?php 
															if(strlen($res_profile_type['type_name'])>26){
																echo substr($res_profile_type['type_name'],0,24)."...";
															}else{
																echo $res_profile_type['type_name'];
															}
															if($res_profile_type['subtype_name']!="" && strlen($res_profile_type['type_name']<26)){
																$type_length = strlen($res_profile_type['type_name']);
																if($type_length + strlen($res_profile_type['subtype_name'])>22){
																	echo ", ".substr($res_profile_type['subtype_name'],0,22 - $type_length)."...";
																}else{
																	echo ", ".$res_profile_type['subtype_name'];
																}
																
															}
															/*echo $res_profile_type['type_name'];
															if($res_profile_type['subtype_name']!=""){
																echo ", ".$res_profile_type['subtype_name'];
															}*/
															?>
															</span>
													<?php
														}
													}
												} ?> </div></div>
					</div>
				<?php
			//}
		}
		?>
		<!--<input type="hidden" id="video_type" value="<?php //echo $videotype2;?>"/>-->
		<input type="hidden" id="video_type_value" value="<?php echo $videotype_value2;?>"/>
		<!--<input type="hidden" id="song_type" value="<?php //echo $songtype2;?>"/>-->
		<input type="hidden" id="song_type_value" value="<?php echo $songtype_value2;?>"/>
		<script type="text/javascript">
		$("document").ready(function(){
			var a = document.getElementById("video_type_value").value;
			var b = document.getElementById("song_type_value").value;
			if((a =="" || a ==" ") && (b=="" || b==" "))
			{
				$("#profileFeaturedproject").css({"display":"none"});
			}
		});
		
		</script>
		
		<?php
		//echo $type2 = "v";
		//echo $type_value2
	}
	?>
	 </div>
    </div>
<?php
//}


//if($artist_event_check==0 && $community_event_check==0)
//{
?>
<script>
function chk_for_creator(s)
{
	if(s == "")
	{
		alert("Profile URL Is Not Set For This Project.");
		return false;
	}
}
</script>
<?php
if($artist_check !=0 || $community_check !=0)
			{
			?>
 <div id="friendsTab" class="hiddenBlock5">
 <?php
	}
	else
	{
	?>
		<div id="promotionsTab" class="hiddenBlock7">
	<?php
	}
 ?>
		<div id="profileFeaturedevent" style="height:55px; position:relative; top:19px; margin-right: 17px; float: right;">
			<div class="playall">
				Play All
			</div>
			<?php
			if($artist_check !=0 || $community_check !=0)
			{
			?>
				<a title="Play all of the media on this profile page." href="javascript:void(0)"><img src="../images/profile/play.png" width="35" height="23" onmouseover="this.src='../images/profile/play-over.png'" onmouseout="this.src='../images/profile/play.png'" onclick="<?php if($artist_event_check !=0 || $community_event_check !=0 || $artist_project_check !=0 || $community_project_check !=0){ echo "PlayAll_featured_media_events('upcoming_event_song2','upcoming_event_video2','recorded_event_song2','recorded_event_video2')"; }else{ echo "PlayAll_featured_media_events('upcoming_event_song1','upcoming_event_video1','recorded_event_song1','recorded_event_video1')";} ?>"/></a>
				<a title="Add all of the media on this profile page to your playlist without stopping your current file." href="javascript:void(0)"><img src="../images/profile/add.png" width="27" height="23" onmouseover="this.src='../images/profile/add-over.png'" onmouseout="this.src='../images/profile/add.png'" onclick="<?php if($artist_event_check !=0 || $community_event_check !=0 || $artist_project_check !=0 || $community_project_check !=0){ echo "PlayAll_featured_media_events('upcoming_event_song2','upcoming_event_video2','recorded_event_song2','recorded_event_video2')"; }else{ echo "PlayAll_featured_media_events('upcoming_event_song1','upcoming_event_video1','recorded_event_song1','recorded_event_video1')";} ?>"/></a>
			<?php
			}
			elseif($artist_event_check !=0 || $community_event_check !=0)
			{
			?>
				<a title="Play all of the media on this events page." href="javascript:void(0)"><img src="../images/profile/play.png" width="35" height="23" onmouseover="this.src='../images/profile/play-over.png'" onmouseout="this.src='../images/profile/play.png'" onclick="<?php if($artist_event_check !=0 || $community_event_check !=0 || $artist_project_check !=0 || $community_project_check !=0){ echo "PlayAll_featured_media_events('upcoming_event_song2','upcoming_event_video2','recorded_event_song2','recorded_event_video2')"; }else{ echo "PlayAll_featured_media_events('upcoming_event_song1','upcoming_event_video1','recorded_event_song1','recorded_event_video1')";} ?>"/></a>
				<a title="Add all of the media on this events page to your playlist without stopping your current file." href="javascript:void(0)"><img src="../images/profile/add.png" width="27" height="23" onmouseover="this.src='../images/profile/add-over.png'" onmouseout="this.src='../images/profile/add.png'" onclick="<?php if($artist_event_check !=0 || $community_event_check !=0 || $artist_project_check !=0 || $community_project_check !=0){ echo "PlayAll_featured_media_events('upcoming_event_song2','upcoming_event_video2','recorded_event_song2','recorded_event_video2')"; }else{ echo "PlayAll_featured_media_events('upcoming_event_song1','upcoming_event_video1','recorded_event_song1','recorded_event_video1')";} ?>"/></a>
			<?php
			}
			elseif($artist_project_check !=0 || $community_project_check !=0)
			{
			?>
				<a title="Play all of the media on this projects page." href="javascript:void(0)"><img src="../images/profile/play.png" width="35" height="23" onmouseover="this.src='../images/profile/play-over.png'" onmouseout="this.src='../images/profile/play.png'" onclick="<?php if($artist_event_check !=0 || $community_event_check !=0 || $artist_project_check !=0 || $community_project_check !=0){ echo "PlayAll_featured_media_events('upcoming_event_song2','upcoming_event_video2','recorded_event_song2','recorded_event_video2')"; }else{ echo "PlayAll_featured_media_events('upcoming_event_song1','upcoming_event_video1','recorded_event_song1','recorded_event_video1')";} ?>"/></a>
				<a title="Add all of the media on this projects page to your playlist without stopping your current file." href="javascript:void(0)"><img src="../images/profile/add.png" width="27" height="23" onmouseover="this.src='../images/profile/add-over.png'" onmouseout="this.src='../images/profile/add.png'" onclick="<?php if($artist_event_check !=0 || $community_event_check !=0 || $artist_project_check !=0 || $community_project_check !=0){ echo "PlayAll_featured_media_events('upcoming_event_song2','upcoming_event_video2','recorded_event_song2','recorded_event_video2')"; }else{ echo "PlayAll_featured_media_events('upcoming_event_song1','upcoming_event_video1','recorded_event_song1','recorded_event_video1')";} ?>"/></a>
			<?php
			}
			?>
		</div>
		<div id="profileFeatured">
			
			<?php //echo $getuser['featured_media'].$getuser['media_id'];
			
			if($artist_check !=0 || $artist_event_check !=0 || $artist_project_check !=0  )
			{
				$table_type = "artist";
			}
			if($community_check !=0 || $community_event_check !=0 || $community_project_check !=0  )
			{
				$table_type = "community";
			}
			if($getuser['featured_media'] == "Gallery" && $getuser['media_id']!=0)
			{
				$find_whether_regis=$new_profile_class_obj->get_count_Gallery_at_register($getuser["$table_type".'_id'],$getuser['media_id']);
				//var_dump($find_whether_regis);
				if($find_whether_regis["count(*)"]>1)
				{
					$getimages=$new_profile_class_obj->get_featured_media_gallery_at_register($getuser['media_id'],$table_type);
					$feature_image_name = array();
					$feature_image_title = array();
					$feature_count = 0;
					while($get_image = mysql_fetch_assoc($getimages))
					{
						$feature_image_name[$feature_count] = $get_image['image_name'];
						$feature_image_title[$feature_count] = $get_image['image_title'];
						$feature_count++;
					}
					$feature_big_img = $feature_image_name[0];
					$featured_gal_img = implode(",", $feature_image_name);
					$featured_gal_img_title = implode(",", $feature_image_title);
					$feature_org_path = "https://reggallery.s3.amazonaws.com/";
					$feature_thum_path = "https://reggalthumb.s3.amazonaws.com/";
				}
				else
				{
					$getimages=$new_profile_class_obj->get_featured_media_gallery($getuser['media_id']);
					$feature_image_name = array();
					$feature_image_title = array();
					$feature_count = 0;
					while($get_image = mysql_fetch_assoc($getimages))
					{
						$feature_image_name[$feature_count] = $get_image['image_name'];
						$feature_image_title[$feature_count] = $get_image['image_title'];
						$feature_count++;
					}
					$feature_big_img = $feature_image_name[0];
					$featured_gal_img = implode(",", $feature_image_name);
					$featured_gal_img_title = implode(",", $feature_image_title);
					$feature_org_path = "https://medgallery.s3.amazonaws.com/";
					$feature_thum_path = "https://medgalthumb.s3.amazonaws.com/";
				}
			?>
				<div class="fmedia">featured Media</div>
				<?php
				if($artist_check !=0 || $community_check !=0)
				{
				?>
					<a title="Play this artists featured media." href="javascript:void(0);"><img src="../images/profile/play.png" width="35" height="23" onmouseover="this.src='../images/profile/play-over.png'" onmouseout="this.src='../images/profile/play.png'" onclick="checkload('<?php echo $feature_big_img;?>','<?php echo $featured_gal_img; ?>','<?php echo $feature_thum_path; ?>','<?php echo $feature_org_path; ?>','<?php echo $featured_gal_img_title; ?>')" /></a>
					<a title="Add this artists featured media to your playlist without stopping your current file." href="javascript:void(0);"><img src="../images/profile/add.png" width="27" height="23" onmouseover="this.src='../images/profile/add-over.png'" onmouseout="this.src='../images/profile/add.png'" /></a>
				
			<?php
				}
				elseif($artist_event_check !=0 || $community_event_check !=0)
				{
			?>
					<a title="Play this events featured media." href="javascript:void(0);"><img src="../images/profile/play.png" width="35" height="23" onmouseover="this.src='../images/profile/play-over.png'" onmouseout="this.src='../images/profile/play.png'" onclick="checkload('<?php echo $feature_big_img;?>','<?php echo $featured_gal_img; ?>','<?php echo $feature_thum_path; ?>','<?php echo $feature_org_path; ?>','<?php echo $featured_gal_img_title; ?>')" /></a>
					<a title="Add this events featured media to your playlist without stopping your current file." href="javascript:void(0);"><img src="../images/profile/add.png" width="27" height="23" onmouseover="this.src='../images/profile/add-over.png'" onmouseout="this.src='../images/profile/add.png'" /></a>
			<?php
				}
				elseif($artist_project_check !=0 || $community_project_check !=0)
				{
			?>
					<a title="Play this projects featured media." href="javascript:void(0);"><img src="../images/profile/play.png" width="35" height="23" onmouseover="this.src='../images/profile/play-over.png'" onmouseout="this.src='../images/profile/play.png'" onclick="checkload('<?php echo $feature_big_img;?>','<?php echo $featured_gal_img; ?>','<?php echo $feature_thum_path; ?>','<?php echo $feature_org_path; ?>','<?php echo $featured_gal_img_title; ?>')" /></a>
					<a title="Add this projects featured media to your playlist without stopping your current file." href="javascript:void(0);"><img src="../images/profile/add.png" width="27" height="23" onmouseover="this.src='../images/profile/add-over.png'" onmouseout="this.src='../images/profile/add.png'" /></a>
			<?php
				}
			}
			if($getuser['featured_media'] == "Song" && $getuser['media_id']!=0)
			{
			?>
				<div class="fmedia">featured Media</div>
				<?php
				if($artist_check !=0 || $community_check !=0)
				{
				?>
					<a title="Play this artists featured media." href="javascript:void(0);"><img src="../images/profile/play.png" width="35" height="23" onmouseover="this.src='../images/profile/play-over.png'" onmouseout="this.src='../images/profile/play.png'" onclick="showPlayer('a','<?php echo $getuser['media_id'];?>','play')" /></a>
					<a title="Add this artists featured media to your playlist without stopping your current file." href="javascript:void(0);"><img src="../images/profile/add.png" width="27" height="23" onmouseover="this.src='../images/profile/add-over.png'" onmouseout="this.src='../images/profile/add.png'" onclick="showPlayer('a','<?php echo $getuser['media_id'];?>','add')"/></a>
			<?php
				}
				elseif($artist_event_check !=0 || $community_event_check !=0)
				{
			?>
					<a title="Play this events featured media." href="javascript:void(0);"><img src="../images/profile/play.png" width="35" height="23" onmouseover="this.src='../images/profile/play-over.png'" onmouseout="this.src='../images/profile/play.png'" onclick="showPlayer('a','<?php echo $getuser['media_id'];?>','play')" /></a>
					<a title="Add this events featured media to your playlist without stopping your current file." href="javascript:void(0);"><img src="../images/profile/add.png" width="27" height="23" onmouseover="this.src='../images/profile/add-over.png'" onmouseout="this.src='../images/profile/add.png'" onclick="showPlayer('a','<?php echo $getuser['media_id'];?>','add')"/></a>
			<?php
				}
				elseif($artist_project_check !=0 || $community_project_check !=0)
				{
			?>
					<a title="Play this projects featured media." href="javascript:void(0);"><img src="../images/profile/play.png" width="35" height="23" onmouseover="this.src='../images/profile/play-over.png'" onmouseout="this.src='../images/profile/play.png'" onclick="showPlayer('a','<?php echo $getuser['media_id'];?>','play')" /></a>
					<a title="Add this projects featured media to your playlist without stopping your current file." href="javascript:void(0);"><img src="../images/profile/add.png" width="27" height="23" onmouseover="this.src='../images/profile/add-over.png'" onmouseout="this.src='../images/profile/add.png'" onclick="showPlayer('a','<?php echo $getuser['media_id'];?>','add')"/></a>
			<?php
				}
			}
			?>
			<?php
			if($getuser['featured_media'] == "Video" && $getuser['media_id']!=0)
			{
			?>
				<div class="fmedia">featured Media</div>
				<?php
				if($artist_check !=0 || $community_check !=0)
				{
				?>
					<a title="Play this artists featured media." href="javascript:void(0);"><img src="../images/profile/play.png" width="35" height="23" onmouseover="this.src='../images/profile/play-over.png'" onmouseout="this.src='../images/profile/play.png'" onclick="showPlayer('v','<?php echo $getuser['media_id'];?>','playlist')" /></a>
					<a title="Add this artists featured media to your playlist without stopping your current file." href="javascript:void(0);"><img src="../images/profile/add.png" width="27" height="23" onmouseover="this.src='../images/profile/add-over.png'" onmouseout="this.src='../images/profile/add.png'" onclick="showPlayer('v','<?php echo $getuser['media_id'];?>','addvi')" /></a>
			<?php
				}
				elseif($artist_event_check !=0 || $community_event_check !=0)
				{
			?>
					<a title="Play this events featured media." href="javascript:void(0);"><img src="../images/profile/play.png" width="35" height="23" onmouseover="this.src='../images/profile/play-over.png'" onmouseout="this.src='../images/profile/play.png'" onclick="showPlayer('v','<?php echo $getuser['media_id'];?>','playlist')" /></a>
					<a title="Add this events featured media to your playlist without stopping your current file." href="javascript:void(0);"><img src="../images/profile/add.png" width="27" height="23" onmouseover="this.src='../images/profile/add-over.png'" onmouseout="this.src='../images/profile/add.png'" onclick="showPlayer('v','<?php echo $getuser['media_id'];?>','addvi')" /></a>
			<?php
				}
				elseif($artist_project_check !=0 || $community_project_check !=0)
				{
			?>
					<a title="Play this projects featured media." href="javascript:void(0);"><img src="../images/profile/play.png" width="35" height="23" onmouseover="this.src='../images/profile/play-over.png'" onmouseout="this.src='../images/profile/play.png'" onclick="showPlayer('v','<?php echo $getuser['media_id'];?>','playlist')" /></a>
					<a title="Add this projects featured media to your playlist without stopping your current file." href="javascript:void(0);"><img src="../images/profile/add.png" width="27" height="23" onmouseover="this.src='../images/profile/add-over.png'" onmouseout="this.src='../images/profile/add.png'" onclick="showPlayer('v','<?php echo $getuser['media_id'];?>','addvi')" /></a>
			<?php
				}
			}
			?>
			
		</div>
		<?php
		include_once("findsale_song.php");
		include_once("findsale_video.php");
		if($artist_event_check !=0 || $community_event_check !=0 || $artist_project_check !=0 || $community_project_check !=0)
		{
			//var_dump(mysql_num_rows($get_upcoming_event));
			if(isset($get_upcoming_event)){
			if($get_upcoming_event!="" && $get_upcoming_event!=Null){
			if(count($upcom_event) && mysql_num_rows($get_upcoming_event)>0)
			{
				?>
			  <h2>Upcoming Events</h2>
			  <div class="rowItem">
				<?php
				for($exp_up_ev =0;$exp_up_ev < count($upcom_event);$exp_up_ev++)
				{
					$act_exp_com = explode('~',$upcom_event[$exp_up_ev]);
					if($act_exp_com[1]=='art')
					{	
						$get_upcoming_event_of_art = $new_profile_class_obj->upcomingArtEvents($date,$act_exp_com[0]);
					}
					if($act_exp_com[1]=='com')
					{
						$get_upcoming_event_of_art = $new_profile_class_obj->upcomingComEvents($date,$act_exp_com[0]);
					}
					
					while($get_upcoming_event1=mysql_fetch_assoc($get_upcoming_event_of_art))
					{
					$upeventcounter = $upeventcounter +1;
					?>
					<div class="tabItem">
					<a href = "/<?php echo $get_upcoming_event1['profile_url'];?>" onclick="return chk_for_creator('<?php echo $get_upcoming_event1['profile_url'];?>')">
						<img class="imgs_pro" src="<?php if($get_upcoming_event1['image_name']==""){ echo "https://arteventprofile.s3.amazonaws.com/Noimage.png"; } else { echo $get_upcoming_event1['image_name']; } ?>" width="200" height="200" />
					</a>	
					<div class="both_wrap_per">
					  <div class="player">
						<?php
						if($get_upcoming_event1['featured_media']=="Gallery" && $get_upcoming_event1['media_id']!=0)
						{
							$feature_reg_big_img = "";
							$all_register_images = "";
							$feature_reg_thum_path = "";
							$feature_reg_org_path = "";
							$featured_reg_gal_img_title = "";
									
							if($get_upcoming_event1['featured_media_table']!="" &&($get_upcoming_event1['featured_media_table']=="general_artist_gallery_list" || $get_upcoming_event1['featured_media_table']=="general_community_gallery_list"))
							{
								$get_register_featured_gallery = $new_profile_class_obj ->get_register_featured_media_gallery($get_upcoming_event1['media_id'],$get_upcoming_event1['featured_media_table']);
								$all_register_images ="";
								$feature_reg_big_img ="";
								$featured_reg_gal_img_title ="";
								for($cou_i=0;$cou_i<count($get_register_featured_gallery);$cou_i++)
								{
									$feature_reg_big_img = $get_register_featured_gallery[0]['image_name'];
									$all_register_images = $all_register_images.",".$get_register_featured_gallery[$cou_i]['image_name'];
									$featured_reg_gal_img_title = $featured_reg_gal_img_title .",".$get_register_featured_gallery[$cou_i]['image_title'];
								}
								$all_register_images = ltrim($all_register_images,',');
								$featured_reg_gal_img_title = ltrim($featured_reg_gal_img_title,',');
								$feature_reg_org_path = "https://reggallery.s3.amazonaws.com/";
								$feature_reg_thum_path = "https://reggalthumb.s3.amazonaws.com/";
							}
							else if($get_upcoming_event1['featured_media_table']!="" && $get_upcoming_event1['featured_media_table']=="general_media"){
								$get_register_featured_gallery = $new_profile_class_obj ->get_featured_gallery_images($get_upcoming_event1['media_id']);
								$all_register_images ="";
								$feature_reg_big_img ="";
								$featured_reg_gal_img_title ="";
								for($cou_i=0;$cou_i<count($get_register_featured_gallery);$cou_i++)
								{
									$feature_reg_big_img = $get_register_featured_gallery[0]['image_name'];
									$all_register_images = $all_register_images.",".$get_register_featured_gallery[$cou_i]['image_name'];
									$featured_reg_gal_img_title = $featured_reg_gal_img_title .",".$get_register_featured_gallery[$cou_i]['image_title'];
								}
								$all_register_images = ltrim($all_register_images,',');
								$featured_reg_gal_img_title = ltrim($featured_reg_gal_img_title,',');
								$feature_reg_org_path = "https://medgallery.s3.amazonaws.com/";
								$feature_reg_thum_path = "https://medgalthumb.s3.amazonaws.com/";
							}
						?>
						<a href="javascript:void(0);"><img src="../images/profile/play.gif" onmouseover="this.src='../images/profile/play-over.gif'" onmouseout="this.src='../images/profile/play.gif'" onclick="checkload('<?php echo $feature_reg_big_img;?>','<?php echo $all_register_images; ?>','<?php echo $feature_reg_thum_path; ?>','<?php echo $feature_reg_org_path; ?>','<?php echo $featured_reg_gal_img_title; ?>')" title="Play file now."/></a>
						<!--<a href="javascript:void(0);"><img src="../images/profile/add.gif"  onmouseover="this.src='../images/profile/add-over.gif'" onmouseout="this.src='../images/profile/add.gif'" title="Add file to the bottom of your playlist without stopping current media."/></a>-->
						<?php
							if($get_upcoming_event1['featured_media_table']!="" && $get_upcoming_event1['featured_media_table']=="general_media")
							{
								$get_gallery_infoevent = $new_profile_class_obj ->get_fetatured_media_info($get_upcoming_event1['media_id']);
								$get_gal_imageevent  = $new_profile_class_obj ->get_gallery_info($get_upcoming_event1['media_id']);
								if($get_gallery_infoevent['sharing_preference']==2){
								?>
								<!--	<a href="javascript:void(0);" onclick="ask_to_donate('<?php echo str_replace("'","\'",$get_gallery_infoevent['title']);?>','<?php echo str_replace("'","\'",$get_gallery_infoevent['creator']);?>','<?php echo $get_gallery_infoevent['id'];?>','download_galevent<?php echo $exp_up_ev;?>')"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>
								<img src="../images/profile/download-over.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download-over.gif'" title="Add media to your cart for free download or purchase." />-->
								 <?php
								}else{
								?>
								<!--<img src="../images/profile/download-over.gif" />-->
								<?php
								}
							}else{
							?>
							<!--<img src="../images/profile/download-over.gif" title="Add media to your cart for free download or purchase."/>-->
							<?php
							}
							?>
							<a href="addtocart.php?seller_id=<?php echo $get_gallery_infoevent['general_user_id'];?>&media_id=<?php echo $get_gallery_infoevent['id'];?>&buyer_id=<?php echo $_SESSION['gen_user_id'];?>&title=<?php echo str_replace("'","\'",$get_gallery_infoevent['title']);?>&image_name=<?php echo $get_gal_imageevent['profile_image'];?>" id="download_galevent<?php echo $exp_up_ev;?>" style="display:none;"></a>
								<script type="text/javascript">
								$(document).ready(function() {
										$("#download_galevent<?php echo $exp_up_ev;?>").fancybox({
										'width'				: '75%',
										'height'			: '75%',
										'transitionIn'		: 'none',
										'transitionOut'		: 'none',
										'type'				: 'iframe'
										});
								});
								</script>
							<?php	
						}
						else if($get_upcoming_event1['featured_media']=="Song" && $get_upcoming_event1['media_id']!=0)
						{
							if($get_upcoming_event1['featured_media_table']=="general_artist_audio" || $get_upcoming_event1['featured_media_table']=="general_community_audio")
							{
								?>
									<a href="javascript:void(0);"><img src="../images/profile/play.gif" onmouseover="this.src='../images/profile/play-over.gif'" onmouseout="this.src='../images/profile/play.gif'" onclick="showPlayer_reg('a','<?php echo $get_upcoming_event1['media_id'];?>','play_reg','<?php echo $get_upcoming_event1['featured_media_table'];?>')" title="Play file now."/></a>
									<a href="javascript:void(0);"><img src="../images/profile/add.gif"  onmouseover="this.src='../images/profile/add-over.gif'" onmouseout="this.src='../images/profile/add.gif'" onclick="showPlayer_reg('a','<?php echo $get_upcoming_event1['media_id'];?>','add_reg','<?php echo $get_upcoming_event1['featured_media_table'];?>')" title="Add file to the bottom of your playlist without stopping current media."/></a>
								<?php
							}
							else{
								?>
									<a href="javascript:void(0);"><img src="../images/profile/play.gif" onmouseover="this.src='../images/profile/play-over.gif'" onmouseout="this.src='../images/profile/play.gif'" onclick="showPlayer('a','<?php echo $get_upcoming_event1['media_id'];?>','play')" title="Play file now."/></a>
									<a href="javascript:void(0);"><img src="../images/profile/add.gif"  onmouseover="this.src='../images/profile/add-over.gif'" onmouseout="this.src='../images/profile/add.gif'" onclick="showPlayer('a','<?php echo $get_upcoming_event1['media_id'];?>','add')" title="Add file to the bottom of your playlist without stopping current media."/></a>
								<?php
							}
							if($get_upcoming_event1['featured_media_table']!="general_artist_audio" || $get_upcoming_event1['featured_media_table']!="general_community_audio")
							{
								$get_media_songinfo_event = $new_profile_class_obj->get_fetatured_media_info($get_upcoming_event1['media_id']);
								if($get_media_songinfo_event['sharing_preference']==5 || $get_media_songinfo_event['sharing_preference']==2)
								{
									$song_image_name_cartevent = $new_profile_class_obj->get_audio_img($get_media_songinfo_event['id']);
									$free_down_rel ="";
									for($find_down=0;$find_down<=count($get_songs_pnew_w);$find_down++)
									{
										if($get_songs_pnew_w[$find_down]['id']==$get_media_songinfo_event['id']){
										$free_down_rel = "yes";
										}
										else{
											if($free_down_rel =="" || $free_down_rel =="no"){
												$free_down_rel = "no";
											}
										}
									}
									if($free_down_rel == "yes"){
									 ?>
									<!-- <a href="javascript:void(0);" onclick="download_directly_song('<?php echo $get_media_songinfo_event['id'];?>','<?php echo str_replace("'","\'",$get_media_songinfo_event['title']);?>','<?php echo str_replace("'","\'",$get_media_songinfo_event['creator']);?>','<?php echo str_replace("'","\'",$get_media_songinfo_event['from']);?>')"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>
									<img src="../images/profile/download-over.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download-over.gif'" title="Add media to your cart for free download or purchase." />-->
									 <?php
									}
									else if($get_media_songinfo_event['sharing_preference']==2){
									 ?>
								<!--	<a href="javascript:void(0);" onclick="ask_to_donate('<?php echo str_replace("'","\'",$get_media_songinfo_event['title']);?>','<?php echo str_replace("'","\'",$get_media_songinfo_event['creator']);?>','<?php echo $get_media_songinfo_event['id'];?>','download_songevent<?php echo $exp_up_ev;?>')"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>
									<img src="../images/profile/download-over.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download-over.gif'" title="Add media to your cart for free download or purchase." />-->
								 <?php
									}
									else{
									?>
									<!--	<a href="javascript:void(0);" onclick="down_song_pop_event(<?php echo $exp_up_ev;?>)"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>
										<img src="../images/profile/download-over.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download-over.gif'" title="Add media to your cart for free download or purchase." />-->
									<?php
									}
								}
								else{
								?>	
								<img src="../images/profile/download-over.gif" />
								<?php
								}
							}
							else{
							?>	
							<img src="../images/profile/download-over.gif" />
							<?php
							}
							if($get_upcoming_event1['media_id']!="" && $get_upcoming_event1['media_id']!=0 && $get_upcoming_event1['media_id']!=" "){
								$upcoming_event_song2 = $upcoming_event_song2 .",". $get_upcoming_event1['media_id'];
							}
							?>
							<a href="addtocart.php?seller_id=<?php echo $get_media_songinfo_event['general_user_id'];?>&media_id=<?php echo $get_media_songinfo_event['id'];?>&buyer_id=<?php echo $_SESSION['gen_user_id'];?>&title=<?php echo str_replace("'","\'",$get_media_songinfo_event['title']);?>&image_name=<?php echo $song_image_name_cartevent;?>" id="download_songevent<?php echo $exp_up_ev;?>" style="display:none;"></a>
							<script type="text/javascript">
							$(document).ready(function() {
									$("#download_songevent<?php echo $exp_up_ev;?>").fancybox({
									'height'			: '75%',
									'width'				: '69%',
									'transitionIn'		: 'none',
									'transitionOut'		: 'none',
									'type'				: 'iframe'
									});
							});
							</script>
							<?php
						}
						else if($get_upcoming_event1['featured_media']=="Video" && $get_upcoming_event1['media_id']!=0)
						{
							if($get_upcoming_event1['featured_media_table']=="general_artist_video" || $get_upcoming_event1['featured_media_table']=="general_community_video")
							{
								?>
									<a href="javascript:void(0);"><img src="../images/profile/play.gif" onmouseover="this.src='../images/profile/play-over.gif'" onmouseout="this.src='../images/profile/play.gif'" onclick="showPlayer_reg('v','<?php echo $get_upcoming_event1['media_id'];?>','playlist_reg','<?php echo $get_upcoming_event1['featured_media_table'];?>')" title="Play file now."/></a>
									<a href="javascript:void(0);"><img src="../images/profile/add.gif"  onmouseover="this.src='../images/profile/add-over.gif'" onmouseout="this.src='../images/profile/add.gif'" onclick="showPlayer_reg('v','<?php echo $get_upcoming_event1['media_id'];?>','addvi_reg','<?php echo $get_upcoming_event1['featured_media_table'];?>')" title="Add file to the bottom of your playlist without stopping current media."/></a>
								<?php
							}else{
								?>
									<a href="javascript:void(0);"><img src="../images/profile/play.gif" onmouseover="this.src='../images/profile/play-over.gif'" onmouseout="this.src='../images/profile/play.gif'" onclick="showPlayer('v','<?php echo $get_upcoming_event1['media_id'];?>','playlist')" title="Play file now."/></a>
									<a href="javascript:void(0);"><img src="../images/profile/add.gif"  onmouseover="this.src='../images/profile/add-over.gif'" onmouseout="this.src='../images/profile/add.gif'" onclick="showPlayer('v','<?php echo $get_upcoming_event1['media_id'];?>','addvi')" title="Add file to the bottom of your playlist without stopping current media."/></a>
								<?php
							}
							if($get_upcoming_event1['featured_media_table']!="general_artist_audio" || $get_upcoming_event1['featured_media_table']!="general_community_audio")
							{
								$get_media_videoinfo_event = $new_profile_class_obj->get_fetatured_media_info($get_upcoming_event1['media_id']);
								if($get_media_videoinfo_event['sharing_preference']==5 || $get_media_videoinfo_event['sharing_preference']==2)
								{
									$get_video_image = $new_profile_class_obj->getvideoimage($get_media_info_down_gal['id']);
									$image_name_cartevent = "https://img.youtube.com/vi/".$get_video_image."/default.jpg";
									$free_down_rel ="";
									for($find_down=0;$find_down<=count($get_vids_pnew_w);$find_down++)
									{
										if($get_vids_pnew_w[$find_down]['id']==$get_media_videoinfo_event['id']){
										$free_down_rel = "yes";
										}
										else{
											if($free_down_rel =="" || $free_down_rel =="no"){
												$free_down_rel = "no";
											}
										}
									}
									if($free_down_rel == "yes"){
									 ?>
									<!-- <a href="javascript:void(0);" onclick="download_directly_song('<?php echo $get_media_videoinfo_event['id'];?>','<?php echo str_replace("'","\'",$get_media_videoinfo_event['title']);?>','<?php echo str_replace("'","\'",$get_media_videoinfo_event['creator']);?>','<?php echo str_replace("'","\'",$get_media_videoinfo_event['from']);?>')"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>
									<img src="../images/profile/download-over.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download-over.gif'" title="Add media to your cart for free download or purchase." />-->
									 <?php
									}
									else if($get_media_videoinfo_event['sharing_preference']==2){
									 ?>
								<!--	<a href="javascript:void(0);" onclick="ask_to_donate('<?php echo str_replace("'","\'",$get_media_videoinfo_event['title']);?>','<?php echo str_replace("'","\'",$get_media_videoinfo_event['creator']);?>','<?php echo $get_media_videoinfo_event['id'];?>','download_videoevent<?php echo $exp_up_ev;?>')"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>
									<img src="../images/profile/download-over.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download-over.gif'" title="Add media to your cart for free download or purchase." />-->
								 <?php
									}
									else{
									?>
									<!--	<a href="javascript:void(0);" onclick="down_video_pop_event(<?php echo $exp_up_ev;?>)"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>
										<img src="../images/profile/download-over.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download-over.gif'" title="Add media to your cart for free download or purchase." />-->
									<?php
									}
								}
								else{
								?>	
								<img src="../images/profile/download-over.gif" />
								<?php
								}
							}
							else{
							?>	
							<img src="../images/profile/download-over.gif" />
							<?php
							}
							if($get_upcoming_event1['media_id']!="" && $get_upcoming_event1['media_id']!=0 && $get_upcoming_event1['media_id']!=" "){
								$upcoming_event_video2 = $upcoming_event_video2 .",". $get_upcoming_event1['media_id'];
							}
							?>
							<a href="addtocart.php?seller_id=<?php echo $get_media_videoinfo_event['general_user_id'];?>&media_id=<?php echo $get_media_videoinfo_event['id'];?>&buyer_id=<?php echo $_SESSION['gen_user_id'];?>&title=<?php echo str_replace("'","\'",$get_media_videoinfo_event['title']);?>&image_name=<?php echo $image_name_cartevent;?>" id="download_videoevent<?php echo $exp_up_ev;?>" style="display:none;"></a>
							<script type="text/javascript">
							$(document).ready(function() {
									$("#download_videoevent<?php echo $exp_up_ev;?>").fancybox({
									'height'			: '75%',
									'width'				: '69%',
									'transitionIn'		: 'none',
									'transitionOut'		: 'none',
									'type'				: 'iframe'
									});
							});
							</script>
							<?php
						}
						else{
					  ?>
						<!--<a href="javascript:void(0);"><img src="../images/profile/play.gif" onmouseover="this.src='../images/profile/play-over.gif'" onmouseout="this.src='../images/profile/play.gif'" /></a>
						<a href="javascript:void(0);"><img src="../images/profile/add.gif"  onmouseover="this.src='../images/profile/add-over.gif'" onmouseout="this.src='../images/profile/add.gif'"/></a>-->
						<?php
						}
						?>
						<!--<a href=""><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" /></a>-->
					  </div>
					  <div class="type_subtype"><a href = "/<?php echo $get_upcoming_event1['profile_url'];?>" onclick="return chk_for_creator('<?php echo $get_upcoming_event1['profile_url'];?>')">
					  <?php if(strlen($get_upcoming_event1['title'])>=22)
						{
							echo substr($get_upcoming_event1['title'],0,19)."...";
						}else{
							echo $get_upcoming_event1['title'];
						}
					  ?></a><?php if(isset($get_upcoming_event1['artist_id']))
											{
												?>
											<div class="by_details">
					<?php
												$type_eves = 'artist';
												$event_name = "";
												if($get_upcoming_event1['state_id']!=0)
												{
													$event_name = $display->state_event_names($get_upcoming_event1['id'],$type_eves);
												}
												
												if(($get_upcoming_event1['date']!="" && $get_upcoming_event1['date']!='0000-00-00') || ($get_upcoming_event1['state_id']!="" && $get_upcoming_event1['state_id']!=0))
												{
													if($get_upcoming_event1['date']!="" && $get_upcoming_event1['date']!='0000-00-00')
													{
														if($event_name!="")
														{
															if(strlen($event_name)>=9)
															{
																echo date('m.d.y',strtotime($get_upcoming_event1['date'])).', '.substr($event_name,0,6)."...";
															}
															else
															{
																echo date('m.d.y',strtotime($get_upcoming_event1['date'])).', '.$event_name;
															}
														}
														else
														{
															echo date('m.d.y',strtotime($get_upcoming_event1['date']));
														}
													}
													else
													{
														if($event_name!="")
														{
															if(strlen($event_name)>=19)
															{
																echo substr($event_name,0,16)."...";
															}
															else
															{
																echo $event_name;
															}
														}
													}
												}					
			?>
											</div>
			<?php
												$get_profile_type = $display->types_of_search('artist_event',$get_upcoming_event1['id']);
												if(!empty($get_profile_type) && $get_profile_type!=Null)
												{
													$res_profile_type = mysql_fetch_assoc($get_profile_type);
													if($res_profile_type['type_name']!=""){
													?>
														<span style="font-size:13px; font-weight:normal;">
														<?php 
														if(strlen($res_profile_type['type_name'])>26){
															echo substr($res_profile_type['type_name'],0,24)."...";
														}else{
															echo $res_profile_type['type_name'];
														}
														if($res_profile_type['subtype_name']!="" && strlen($res_profile_type['type_name']<26)){
															$type_length = strlen($res_profile_type['type_name']);
															if($type_length + strlen($res_profile_type['subtype_name'])>22){
																echo ", ".substr($res_profile_type['subtype_name'],0,22 - $type_length)."...";
															}else{
																echo ", ".$res_profile_type['subtype_name'];
															}
															
														}
														/*echo $res_profile_type['type_name'];
														if($res_profile_type['subtype_name']!=""){
															echo ", ".$res_profile_type['subtype_name'];
														}*/
														?>
														</span>
												<?php
													}
												}
											}
											elseif(isset($get_upcoming_event1['community_id']))
											{
											?>
											<div class="by_details">
					<?php
												$type_eves = 'community';
												$event_name = "";
												if($get_upcoming_event1['state_id']!=0)
												{
													$event_name = $display->state_event_names($get_upcoming_event1['id'],$type_eves);
												}
												
												if(($get_upcoming_event1['date']!="" && $get_upcoming_event1['date']!='0000-00-00') || ($get_upcoming_event1['state_id']!="" && $get_upcoming_event1['state_id']!=0))
												{
													if($get_upcoming_event1['date']!="" && $get_upcoming_event1['date']!='0000-00-00')
													{
														if($event_name!="")
														{
															if(strlen($event_name)>=9)
															{
																echo date('m.d.y',strtotime($get_upcoming_event1['date'])).', '.substr($event_name,0,6)."...";
															}
															else
															{
																echo date('m.d.y',strtotime($get_upcoming_event1['date'])).', '.$event_name;
															}
														}
														else
														{
															echo date('m.d.y',strtotime($get_upcoming_event1['date']));
														}
													}
													else
													{
														if($event_name!="")
														{
															if(strlen($event_name)>=19)
															{
																echo substr($event_name,0,16)."...";
															}
															else
															{
																echo $event_name;
															}
														}
													}
												}					
			?>
											</div>
			<?php
												$get_profile_type = $display->types_of_search('community_event',$get_upcoming_event1['id']);
												if(!empty($get_profile_type) && $get_profile_type!=Null)
												{
													$res_profile_type = mysql_fetch_assoc($get_profile_type);
													if($res_profile_type['type_name']!=""){
													?>
														<span style="font-size:13px; font-weight:normal;">
														<?php 
														if(strlen($res_profile_type['type_name'])>26){
															echo substr($res_profile_type['type_name'],0,24)."...";
														}else{
															echo $res_profile_type['type_name'];
														}
														if($res_profile_type['subtype_name']!="" && strlen($res_profile_type['type_name']<26)){
															$type_length = strlen($res_profile_type['type_name']);
															if($type_length + strlen($res_profile_type['subtype_name'])>22){
																echo ", ".substr($res_profile_type['subtype_name'],0,22 - $type_length)."...";
															}else{
																echo ", ".$res_profile_type['subtype_name'];
															}
															
														}
														/*echo $res_profile_type['type_name'];
														if($res_profile_type['subtype_name']!=""){
															echo ", ".$res_profile_type['subtype_name'];
														}*/
														?>
														</span>
												<?php
													}
												}
											} ?> </div></div>
					</div>
					<?php
					}
				 }
			?>
				<input type="hidden" id="upcoming_event_video2" value="<?php echo $upcoming_event_video2;?>"/>
				<input type="hidden" id="upcoming_event_song2" value="<?php echo $upcoming_event_song2;?>"/>
				<script type="text/javascript">
				$("document").ready(function(){
					var a = document.getElementById("upcoming_event_video2").value;
					var b = document.getElementById("upcoming_event_song2").value;
					if((a =="" || a ==" ") && (b=="" || b==" "))
					{
						$("#profileFeaturedevent").css({"display":"none"});
					}
				});
				
				</script>
		  </div>
		  <?php
		  }
		  }}
		}
		else
		{
			if(isset($get_upcoming_event)){
			if($get_upcoming_event[0] !="" && $get_upcoming_event[0]!=Null){
			if( mysql_num_rows($get_upcoming_event[0])>0)
			{
			?>
			<h2>Upcoming Events</h2>
		  <div class="rowItem">
			<?php
			foreach($get_upcoming_event as $get_upcoming_event2)
			{
			$upcounter = $upcounter+1;
			while($get_upcoming_event1=mysql_fetch_assoc($get_upcoming_event2))
			{
			?>
			<div class="tabItem">
			<a href = "/<?php echo $get_upcoming_event1['profile_url'];?>" onclick="return chk_for_creator('<?php echo $get_upcoming_event1['profile_url'];?>')">
				<img class="imgs_pro" src="<?php if($get_upcoming_event1['image_name']==""){ echo "https://arteventprofile.s3.amazonaws.com/Noimage.png"; } else { echo $get_upcoming_event1['image_name']; } ?>" width="200" height="200" />
			</a>
			<div class="both_wrap_per">
			<div class="player">
				<?php
				if($get_upcoming_event1['featured_media']=="Gallery" && $get_upcoming_event1['media_id']!=0)
				{	
					$feature_reg_big_img = "";
					$all_register_images = "";
					$feature_reg_thum_path = "";
					$feature_reg_org_path = "";
					$featured_reg_gal_img_title = "";
					
					if($get_upcoming_event1['featured_media_table']!="" &&($get_upcoming_event1['featured_media_table']=="general_artist_gallery_list" || $get_upcoming_event1['featured_media_table']=="general_community_gallery_list"))
					{
						$get_register_featured_gallery = $new_profile_class_obj ->get_register_featured_media_gallery($get_upcoming_event1['media_id'],$get_upcoming_event1['featured_media_table']);
						$all_register_images ="";
						$feature_reg_big_img ="";
						$featured_reg_gal_img_title ="";
						for($cou_i=0;$cou_i<count($get_register_featured_gallery);$cou_i++)
						{
							$feature_reg_big_img = $get_register_featured_gallery[0]['image_name'];
							$all_register_images = $all_register_images.",".$get_register_featured_gallery[$cou_i]['image_name'];
							$featured_reg_gal_img_title = $featured_reg_gal_img_title .",".$get_register_featured_gallery[$cou_i]['image_title'];
						}
						$all_register_images = ltrim($all_register_images,',');
						$featured_reg_gal_img_title = ltrim($featured_reg_gal_img_title,',');
						$feature_reg_org_path = "https://reggallery.s3.amazonaws.com/";
						$feature_reg_thum_path = "https://reggalthumb.s3.amazonaws.com/";
					}
					else if($get_upcoming_event1['featured_media_table']!="" && $get_upcoming_event1['featured_media_table']=="general_media"){
						$get_register_featured_gallery = $new_profile_class_obj ->get_featured_gallery_images($get_upcoming_event1['media_id']);
						$all_register_images ="";
						$feature_reg_big_img ="";
						$featured_reg_gal_img_title ="";
						for($cou_i=0;$cou_i<count($get_register_featured_gallery);$cou_i++)
						{
							$feature_reg_big_img = $get_register_featured_gallery[0]['image_name'];
							$all_register_images = $all_register_images.",".$get_register_featured_gallery[$cou_i]['image_name'];
							$featured_reg_gal_img_title = $featured_reg_gal_img_title .",".$get_register_featured_gallery[$cou_i]['image_title'];
						}
						$all_register_images = ltrim($all_register_images,',');
						$featured_reg_gal_img_title = ltrim($featured_reg_gal_img_title,',');
						$feature_reg_org_path = "https://medgallery.s3.amazonaws.com/";
						$feature_reg_thum_path = "https://medgalthumb.s3.amazonaws.com/";
					}
				?>
				<a href="javascript:void(0);"><img src="../images/profile/play.gif" onmouseover="this.src='../images/profile/play-over.gif'" onmouseout="this.src='../images/profile/play.gif'" onclick="checkload('<?php echo $feature_reg_big_img;?>','<?php echo $all_register_images; ?>','<?php echo $feature_reg_thum_path; ?>','<?php echo $feature_reg_org_path; ?>','<?php echo $featured_reg_gal_img_title; ?>')" title="Play file now."/></a>
				<?php
				if($get_upcoming_event1['featured_media_table']!="" && $get_upcoming_event1['featured_media_table']=="general_media")
				{
					$get_gallery_infoevent = $new_profile_class_obj ->get_fetatured_media_info($get_upcoming_event1['media_id']);
					$get_gal_imageevent  = $new_profile_class_obj ->get_gallery_info($get_upcoming_event1['media_id']);
					if($get_gallery_infoevent['sharing_preference']==2){
					?>
					<!--	<a href="javascript:void(0);" onclick="ask_to_donate('<?php echo str_replace("'","\'",$get_gallery_infoevent['title']);?>','<?php echo str_replace("'","\'",$get_gallery_infoevent['creator']);?>','<?php echo $get_gallery_infoevent['id'];?>','download_galevent<?php echo $upcounter;?>')"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>
					<img src="../images/profile/download-over.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download-over.gif'" title="Add media to your cart for free download or purchase." />-->
					 <?php
					}else{
					?>
					<!--<img src="../images/profile/download-over.gif" />-->
					<?php
					}
				}else{
				?>
				<!--<img src="../images/profile/download-over.gif" title="Add media to your cart for free download or purchase."/>-->
				<?php
				}
				?>
				<a href="addtocart.php?seller_id=<?php echo $get_gallery_infoevent['general_user_id'];?>&media_id=<?php echo $get_gallery_infoevent['id'];?>&buyer_id=<?php echo $_SESSION['gen_user_id'];?>&title=<?php echo str_replace("'","\'",$get_gallery_infoevent['title']);?>&image_name=<?php echo $get_gal_imageevent['profile_image'];?>" id="download_galevent<?php echo $upcounter;?>" style="display:none;"></a>
					<script type="text/javascript">
					$(document).ready(function() {
							$("#download_galevent<?php echo $upcounter;?>").fancybox({
							'width'				: '75%',
							'height'			: '75%',
							'transitionIn'		: 'none',
							'transitionOut'		: 'none',
							'type'				: 'iframe'
							});
					});
					</script>
				<?php
				}
				else if($get_upcoming_event1['featured_media']=="Song" && $get_upcoming_event1['media_id']!=0)
				{
					if($get_upcoming_event1['featured_media_table']=="general_artist_audio" || $get_upcoming_event1['featured_media_table']=="general_community_audio")
					{
						?>
							<a href="javascript:void(0);"><img src="../images/profile/play.gif" onmouseover="this.src='../images/profile/play-over.gif'" onmouseout="this.src='../images/profile/play.gif'" onclick="showPlayer_reg('a','<?php echo $get_upcoming_event1['media_id'];?>','play_reg','<?php echo $get_upcoming_event1['featured_media_table'];?>')" title="Play file now."/></a>
							<a href="javascript:void(0);"><img src="../images/profile/add.gif"  onmouseover="this.src='../images/profile/add-over.gif'" onmouseout="this.src='../images/profile/add.gif'" onclick="showPlayer_reg('a','<?php echo $get_upcoming_event1['media_id'];?>','add_reg','<?php echo $get_upcoming_event1['featured_media_table'];?>')" title="Add file to the bottom of your playlist without stopping current media."/></a>
						<?php
					}
					else{
						?>
							<a href="javascript:void(0);"><img src="../images/profile/play.gif" onmouseover="this.src='../images/profile/play-over.gif'" onmouseout="this.src='../images/profile/play.gif'" onclick="showPlayer('a','<?php echo $get_upcoming_event1['media_id'];?>','play')" title="Play file now."/></a>
							<a href="javascript:void(0);"><img src="../images/profile/add.gif"  onmouseover="this.src='../images/profile/add-over.gif'" onmouseout="this.src='../images/profile/add.gif'" onclick="showPlayer('a','<?php echo $get_upcoming_event1['media_id'];?>','add')" title="Add file to the bottom of your playlist without stopping current media."/></a>
						<?php
					}
					if($get_upcoming_event1['featured_media_table']!="general_artist_audio" || $get_upcoming_event1['featured_media_table']!="general_community_audio")
					{
						$get_media_songinfo_event = $new_profile_class_obj->get_fetatured_media_info($get_upcoming_event1['media_id']);
						if($get_media_songinfo_event['sharing_preference']==5 || $get_media_songinfo_event['sharing_preference']==2)
						{
							$song_image_name_cartevent = $new_profile_class_obj->get_audio_img($get_media_songinfo_event['id']);
							$free_down_rel ="";
							for($find_down=0;$find_down<=count($get_songs_pnew_w);$find_down++)
							{
								if($get_songs_pnew_w[$find_down]['id']==$get_media_songinfo_event['id']){
								$free_down_rel = "yes";
								}
								else{
									if($free_down_rel =="" || $free_down_rel =="no"){
										$free_down_rel = "no";
									}
								}
							}
							if($free_down_rel == "yes"){
							 ?>
							<!-- <a href="javascript:void(0);" onclick="download_directly_song('<?php echo $get_media_songinfo_event['id'];?>','<?php echo str_replace("'","\'",$get_media_songinfo_event['title']);?>','<?php echo str_replace("'","\'",$get_media_songinfo_event['creator']);?>','<?php echo str_replace("'","\'",$get_media_songinfo_event['from']);?>')"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>
							<img src="../images/profile/download-over.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download-over.gif'" title="Add media to your cart for free download or purchase." />-->
							 <?php
							}
							else if($get_media_songinfo_event['sharing_preference']==2){
							 ?>
							<!--<a href="javascript:void(0);" onclick="ask_to_donate('<?php echo str_replace("'","\'",$get_media_songinfo_event['title']);?>','<?php echo str_replace("'","\'",$get_media_songinfo_event['creator']);?>','<?php echo $get_media_songinfo_event['id'];?>','download_songevent<?php echo $upcounter;?>')"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>
							<img src="../images/profile/download-over.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download-over.gif'" title="Add media to your cart for free download or purchase." />-->
						 <?php
							}
							else{
							?>
							<!--	<a href="javascript:void(0);" onclick="down_song_pop_event(<?php echo $upcounter;?>)"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>
							<img src="../images/profile/download-over.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download-over.gif'" title="Add media to your cart for free download or purchase." />-->
							<?php
							}
						}
						else{
						?>	
						<!--<img src="../images/profile/download-over.gif" />-->
						<?php
						}
					}
					else{
					?>	
					<!--<img src="../images/profile/download-over.gif" />-->
					<?php
					}
					if($get_upcoming_event1['media_id']!="" && $get_upcoming_event1['media_id']!=0 && $get_upcoming_event1['media_id']!=" "){					
					$up_all_song = $up_all_song .",".$get_upcoming_event1['media_id'];
					}
					?>
					<a href="addtocart.php?seller_id=<?php echo $get_media_songinfo_event['general_user_id'];?>&media_id=<?php echo $get_media_songinfo_event['id'];?>&buyer_id=<?php echo $_SESSION['gen_user_id'];?>&title=<?php echo str_replace("'","\'",$get_media_songinfo_event['title']);?>&image_name=<?php echo $song_image_name_cartevent;?>" id="download_songevent<?php echo $upcounter;?>" style="display:none;"></a>
					<script type="text/javascript">
					$(document).ready(function() {
							$("#download_songevent<?php echo $upcounter;?>").fancybox({
							'height'			: '75%',
							'width'				: '69%',
							'transitionIn'		: 'none',
							'transitionOut'		: 'none',
							'type'				: 'iframe'
							});
					});
					</script>
					<?php
				}
				else if($get_upcoming_event1['featured_media']=="Video" && $get_upcoming_event1['media_id']!=0)
				{
					if($get_upcoming_event1['featured_media_table']=="general_artist_video" || $get_upcoming_event1['featured_media_table']=="general_community_video")
					{
						?>
							<a href="javascript:void(0);"><img src="../images/profile/play.gif" onmouseover="this.src='../images/profile/play-over.gif'" onmouseout="this.src='../images/profile/play.gif'" onclick="showPlayer_reg('v','<?php echo $get_upcoming_event1['media_id'];?>','playlist_reg','<?php echo $get_upcoming_event1['featured_media_table'];?>')" title="Play file now."/></a>
							<a href="javascript:void(0);"><img src="../images/profile/add.gif"  onmouseover="this.src='../images/profile/add-over.gif'" onmouseout="this.src='../images/profile/add.gif'" onclick="showPlayer_reg('v','<?php echo $get_upcoming_event1['media_id'];?>','addvi_reg','<?php echo $get_upcoming_event1['featured_media_table'];?>')" title="Add file to the bottom of your playlist without stopping current media."/></a>
						<?php
					}else{
						?>
							<a href="javascript:void(0);"><img src="../images/profile/play.gif" onmouseover="this.src='../images/profile/play-over.gif'" onmouseout="this.src='../images/profile/play.gif'" onclick="showPlayer('v','<?php echo $get_upcoming_event1['media_id'];?>','playlist')" title="Play file now."/></a>
							<a href="javascript:void(0);"><img src="../images/profile/add.gif"  onmouseover="this.src='../images/profile/add-over.gif'" onmouseout="this.src='../images/profile/add.gif'" onclick="showPlayer('v','<?php echo $get_upcoming_event1['media_id'];?>','addvi')" title="Add file to the bottom of your playlist without stopping current media."/></a>
						<?php
					}
					if($get_upcoming_event1['featured_media_table']!="general_artist_audio" || $get_upcoming_event1['featured_media_table']!="general_community_audio")
					{
						$get_media_videoinfo_event = $new_profile_class_obj->get_fetatured_media_info($get_upcoming_event1['media_id']);
						if($get_media_videoinfo_event['sharing_preference']==5 || $get_media_videoinfo_event['sharing_preference']==2)
						{
							$get_video_image = $new_profile_class_obj->getvideoimage($get_media_info_down_gal['id']);
							$image_name_cartevent = "https://img.youtube.com/vi/".$get_video_image."/default.jpg";
							$free_down_rel ="";
							for($find_down=0;$find_down<=count($get_vids_pnew_w);$find_down++)
							{
								if($get_vids_pnew_w[$find_down]['id']==$get_media_videoinfo_event['id']){
								$free_down_rel = "yes";
								}
								else{
									if($free_down_rel =="" || $free_down_rel =="no"){
										$free_down_rel = "no";
									}
								}
							}
							if($free_down_rel == "yes"){
							 ?>
						<!--	 <a href="javascript:void(0);" onclick="download_directly_song('<?php echo $get_media_videoinfo_event['id'];?>','<?php echo str_replace("'","\'",$get_media_videoinfo_event['title']);?>','<?php echo str_replace("'","\'",$get_media_videoinfo_event['creator']);?>','<?php echo str_replace("'","\'",$get_media_videoinfo_event['from']);?>')"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>
						<img src="../images/profile/download-over.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download-over.gif'" title="Add media to your cart for free download or purchase." />-->
							 <?php
							}
							else if($get_media_videoinfo_event['sharing_preference']==2){
							 ?>
							<!--<a href="javascript:void(0);" onclick="ask_to_donate('<?php echo str_replace("'","\'",$get_media_videoinfo_event['title']);?>','<?php echo str_replace("'","\'",$get_media_videoinfo_event['creator']);?>','<?php echo $get_media_videoinfo_event['id'];?>','download_videoevent<?php echo $upcounter;?>')"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>
							<img src="../images/profile/download-over.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download-over.gif'" title="Add media to your cart for free download or purchase." />-->
						 <?php
							}
							else{
							?>
							<!--	<a href="javascript:void(0);" onclick="down_video_pop_event(<?php echo $upcounter;?>)"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>
							<img src="../images/profile/download-over.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download-over.gif'" title="Add media to your cart for free download or purchase." />-->
							<?php
							}
						}
						else{
						?>	
						<!--<img src="../images/profile/download-over.gif" />-->
						<?php
						}
					}
					else{
					?>	
					<!--<img src="../images/profile/download-over.gif" />-->
					<?php
					}
					if($get_upcoming_event1['media_id']!="" && $get_upcoming_event1['media_id']!=0 && $get_upcoming_event1['media_id']!=" "){
					$up_all_video = $up_all_video .",".$get_upcoming_event1['media_id'];
					}
					?>
					<a href="addtocart.php?seller_id=<?php echo $get_media_videoinfo_event['general_user_id'];?>&media_id=<?php echo $get_media_videoinfo_event['id'];?>&buyer_id=<?php echo $_SESSION['gen_user_id'];?>&title=<?php echo str_replace("'","\'",$get_media_videoinfo_event['title']);?>&image_name=<?php echo $image_name_cartevent;?>" id="download_videoevent<?php echo $upcounter;?>" style="display:none;"></a>
					<script type="text/javascript">
					$(document).ready(function() {
							$("#download_videoevent<?php echo $upcounter;?>").fancybox({
							'height'			: '75%',
							'width'				: '69%',
							'transitionIn'		: 'none',
							'transitionOut'		: 'none',
							'type'				: 'iframe'
							});
					});
					</script>
					<?php
				}
				else{
			  ?>
				<!--<a href="javascript:void(0);"><img src="../images/profile/play.gif" onmouseover="this.src='../images/profile/play-over.gif'" onmouseout="this.src='../images/profile/play.gif'" /></a>
				<a href="javascript:void(0);"><img src="../images/profile/add.gif"  onmouseover="this.src='../images/profile/add-over.gif'" onmouseout="this.src='../images/profile/add.gif'"/></a>-->
				<?php
				}
				?>
				<!--<a href=""><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" /></a>-->
				
			</div>
			<div class="type_subtype"><a href = "/<?php echo $get_upcoming_event1['profile_url'];?>" onclick="return chk_for_creator('<?php echo $get_upcoming_event1['profile_url'];?>')">
			  <?php if(strlen($get_upcoming_event1['title'])>=22){ 
				echo substr($get_upcoming_event1['title'],0,19)."...";
			  }else{
				echo $get_upcoming_event1['title'];
				}
			?>
			</a><?php if(isset($get_upcoming_event1['artist_id']))
											{
												$get_profile_type = $display->types_of_search('artist_event',$get_upcoming_event1['id']);
												if(!empty($get_profile_type) && $get_profile_type!=Null)
												{												
					?>
											<div class="by_details">
					<?php
												$type_eves = 'artist';
												$event_name = "";
												if($get_upcoming_event1['state_id']!=0)
												{
													$event_name = $display->state_event_names($get_upcoming_event1['id'],$type_eves);
												}
												
												if(($get_upcoming_event1['date']!="" && $get_upcoming_event1['date']!='0000-00-00') || ($get_upcoming_event1['state_id']!="" && $get_upcoming_event1['state_id']!=0))
												{
													if($get_upcoming_event1['date']!="" && $get_upcoming_event1['date']!='0000-00-00')
													{
														if($event_name!="")
														{
															if(strlen($event_name)>=9)
															{
																echo date('m.d.y',strtotime($get_upcoming_event1['date'])).', '.substr($event_name,0,6)."...";
															}
															else
															{
																echo date('m.d.y',strtotime($get_upcoming_event1['date'])).', '.$event_name;
															}
														}
														else
														{
															echo date('m.d.y',strtotime($get_upcoming_event1['date']));
														}
													}
													else
													{
														if($event_name!="")
														{
															if(strlen($event_name)>=19)
															{
																echo substr($event_name,0,16)."...";
															}
															else
															{
																echo $event_name;
															}
														}
													}
												}					
			?>
											</div>
			<?php
													$res_profile_type = mysql_fetch_assoc($get_profile_type);
													if($res_profile_type['type_name']!=""){
													?>
														<span style="font-size:13px; font-weight:normal;">
														<?php 
														if(strlen($res_profile_type['type_name'])>26){
															echo substr($res_profile_type['type_name'],0,24)."...";
														}else{
															echo $res_profile_type['type_name'];
														}
														if($res_profile_type['subtype_name']!="" && strlen($res_profile_type['type_name']<26)){
															$type_length = strlen($res_profile_type['type_name']);
															if($type_length + strlen($res_profile_type['subtype_name'])>22){
																echo ", ".substr($res_profile_type['subtype_name'],0,22 - $type_length)."...";
															}else{
																echo ", ".$res_profile_type['subtype_name'];
															}
															
														}
														/*echo $res_profile_type['type_name'];
														if($res_profile_type['subtype_name']!=""){
															echo ", ".$res_profile_type['subtype_name'];
														}*/
														?>
														</span>
												<?php
													}
												}
											}
											elseif(isset($get_upcoming_event1['community_id']))
											{
												$get_profile_type = $display->types_of_search('community_event',$get_upcoming_event1['id']);
												if(!empty($get_profile_type) && $get_profile_type!=Null)
												{
												?>
											<div class="by_details">
					<?php
												$type_eves = 'community';
												$event_name = "";
												if($get_upcoming_event1['state_id']!=0)
												{
													$event_name = $display->state_event_names($get_upcoming_event1['id'],$type_eves);
												}
												
												if(($get_upcoming_event1['date']!="" && $get_upcoming_event1['date']!='0000-00-00') || ($get_upcoming_event1['state_id']!="" && $get_upcoming_event1['state_id']!=0))
												{
													if($get_upcoming_event1['date']!="" && $get_upcoming_event1['date']!='0000-00-00')
													{
														if($event_name!="")
														{
															if(strlen($event_name)>=9)
															{
																echo date('m.d.y',strtotime($get_upcoming_event1['date'])).', '.substr($event_name,0,6)."...";
															}
															else
															{
																echo date('m.d.y',strtotime($get_upcoming_event1['date'])).', '.$event_name;
															}
														}
														else
														{
															echo date('m.d.y',strtotime($get_upcoming_event1['date']));
														}
													}
													else
													{
														if($event_name!="")
														{
															if(strlen($event_name)>=19)
															{
																echo substr($event_name,0,16)."...";
															}
															else
															{
																echo $event_name;
															}
														}
													}
												}					
			?>
											</div>
			<?php
													$res_profile_type = mysql_fetch_assoc($get_profile_type);
													if($res_profile_type['type_name']!=""){
													?>
														<span style="font-size:13px; font-weight:normal;">
														<?php 
														
														if(strlen($res_profile_type['type_name'])>26){
															echo substr($res_profile_type['type_name'],0,24)."...";
														}else{
															echo $res_profile_type['type_name'];
														}
														if($res_profile_type['subtype_name']!="" && strlen($res_profile_type['type_name']<26)){
															$type_length = strlen($res_profile_type['type_name']);
															if($type_length + strlen($res_profile_type['subtype_name'])>22){
																echo ", ".substr($res_profile_type['subtype_name'],0,22 - $type_length)."...";
															}else{
																echo ", ".$res_profile_type['subtype_name'];
															}
															
														}
														/*echo $res_profile_type['type_name'];
														if($res_profile_type['subtype_name']!=""){
															echo ", ".$res_profile_type['subtype_name'];
														}*/
														?>
														</span>
												<?php
													}
												}
											} ?> </div></div>
			</div>
			<?php
			} }
			?>
			<input type="hidden" id="upcoming_event_video1" value="<?php echo $up_all_video;?>"/>
			<input type="hidden" id="upcoming_event_song1" value="<?php echo $up_all_song;?>"/>
			<script type="text/javascript">
				$("document").ready(function(){
					var a = document.getElementById("upcoming_event_video1").value;
					var b = document.getElementById("upcoming_event_song1").value;
					if((a =="" || a ==" ") && (b=="" || b==" "))
					{
						$("#profileFeaturedevent").css({"display":"none"});
					}
				});
				
			</script>
		  </div>
		  <?php
		  }}}
		}
	  ?>
	  <?php
	  if($artist_event_check !=0 || $community_event_check !=0 || $artist_project_check !=0 || $community_project_check !=0)
		{
			//echo mysql_num_rows($get_recorded_event);
			if(isset($get_recorded_event)){
			if($get_recorded_event !="" && $get_recorded_event!=Null){
			if(mysql_num_rows($get_recorded_event)>0)
			{
			?>
				<h2>Recorded Events</h2>
				<div class="rowItem">
				<?php
				for($exp_rec_ev =0;$exp_rec_ev < count($rec_event);$exp_rec_ev++)
				{
					$act_com_id = explode('~',$rec_event[$exp_rec_ev]);
					if($act_com_id[1]=='art')
					{
						$get_rec_event_of_art = $new_profile_class_obj->recordedArtEvents($date,$act_com_id[0]);
					}
					elseif($act_com_id[1]=='com')
					{
						$get_rec_event_of_art = $new_profile_class_obj->recordedComEvents($date,$act_com_id[0]);
					}
					while($get_recorded_event1=mysql_fetch_assoc($get_rec_event_of_art))
					{
					?>
					<div class="tabItem"> 
					<a href = "/<?php echo $get_recorded_event1['profile_url'];?>" onclick="return chk_for_creator('<?php echo $get_recorded_event1['profile_url'];?>')">
						<img class="imgs_pro" src="<?php if($get_recorded_event1['image_name']==""){ echo "https://arteventprofile.s3.amazonaws.com/Noimage.png"; } else { echo $get_recorded_event1['image_name']; } ?>" width="200" height="200" />
					</a>
					<div class="both_wrap_per">
					<div class="player">
						<?php
						if($get_recorded_event1['featured_media']=="Gallery" && $get_recorded_event1['media_id']!=0)
						{
							$feature_reg_big_img = "";
							$all_register_images = "";
							$feature_reg_thum_path = "";
							$feature_reg_org_path = "";
							$featured_reg_gal_img_title = "";
									
							if($get_recorded_event1['featured_media_table']!="" &&($get_recorded_event1['featured_media_table']=="general_artist_gallery_list" || $get_recorded_event1['featured_media_table']=="general_community_gallery_list"))
							{
								$get_register_featured_gallery = $new_profile_class_obj ->get_register_featured_media_gallery($get_recorded_event1['media_id'],$get_recorded_event1['featured_media_table']);
								$all_register_images ="";
								$feature_reg_big_img ="";
								$featured_reg_gal_img_title ="";
								for($cou_i=0;$cou_i<count($get_register_featured_gallery);$cou_i++)
								{
									$feature_reg_big_img = $get_register_featured_gallery[0]['image_name'];
									$all_register_images = $all_register_images.",".$get_register_featured_gallery[$cou_i]['image_name'];
									$featured_reg_gal_img_title = $featured_reg_gal_img_title .",".$get_register_featured_gallery[$cou_i]['image_title'];
								}
								$all_register_images = ltrim($all_register_images,',');
								$featured_reg_gal_img_title = ltrim($featured_reg_gal_img_title,',');
								$feature_reg_org_path = "https://reggallery.s3.amazonaws.com/";
								$feature_reg_thum_path = "https://reggalthumb.s3.amazonaws.com/";
							}
							else if($get_recorded_event1['featured_media_table']!="" && $get_recorded_event1['featured_media_table']=="general_media"){
								$get_register_featured_gallery = $new_profile_class_obj ->get_featured_gallery_images($get_recorded_event1['media_id']);
								$all_register_images ="";
								$feature_reg_big_img ="";
								$featured_reg_gal_img_title ="";
								for($cou_i=0;$cou_i<count($get_register_featured_gallery);$cou_i++)
								{
									$feature_reg_big_img = $get_register_featured_gallery[0]['image_name'];
									$all_register_images = $all_register_images.",".$get_register_featured_gallery[$cou_i]['image_name'];
									$featured_reg_gal_img_title = $featured_reg_gal_img_title .",".$get_register_featured_gallery[$cou_i]['image_title'];
								}
								$all_register_images = ltrim($all_register_images,',');
								$featured_reg_gal_img_title = ltrim($featured_reg_gal_img_title,',');
								$feature_reg_org_path = "https://medgallery.s3.amazonaws.com/";
								$feature_reg_thum_path = "https://medgalthumb.s3.amazonaws.com/";
							}
						?>
						<a href="javascript:void(0);"><img src="../images/profile/play.gif" onmouseover="this.src='../images/profile/play-over.gif'" onmouseout="this.src='../images/profile/play.gif'" onclick="checkload('<?php echo $feature_reg_big_img; ?>','<?php echo $all_register_images; ?>','<?php echo $feature_reg_thum_path; ?>','<?php echo $feature_reg_org_path; ?>','<?php echo $featured_reg_gal_img_title; ?>')" title="Play file now."/></a>
						<!--<a href="javascript:void(0);"><img src="../images/profile/add.gif"  onmouseover="this.src='../images/profile/add-over.gif'" onmouseout="this.src='../images/profile/add.gif'" title="Add file to the bottom of your playlist without stopping current media."/></a>-->
						<?php
							if($get_recorded_event1['featured_media_table']!="" && $get_recorded_event1['featured_media_table']=="general_media"){
								$get_gal_imagerecevent  = $new_profile_class_obj ->get_gallery_info($get_recorded_event1['media_id']);
								$get_media_info_down_gal = $new_profile_class_obj ->get_fetatured_media_info($get_recorded_event1['media_id']);
								if($get_media_info_down_gal['sharing_preference']==2){
								?>
								<!--	<a href="javascript:void(0);" onclick="ask_to_donate('<?php echo str_replace("'","\'",$get_media_info_down_gal['title']);?>','<?php echo str_replace("'","\'",$get_media_info_down_gal['creator']);?>','<?php echo $get_media_info_down_gal['id'];?>','download_galrecevent<?php echo $exp_rec_ev;?>')"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>-->
								<!--<img src="../images/profile/download-over.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download-over.gif'" title="Add media to your cart for free download or purchase." />-->
								 <?php
								}else{
								?>
								<!--<img src="../images/profile/download-over.gif" />-->
								<?php
								}
							}else{
							?>
							<!--<img src="../images/profile/download-over.gif" />-->
							<?php
							}
							?>
							<a href="addtocart.php?seller_id=<?php echo $get_media_info_down_gal['general_user_id'];?>&media_id=<?php echo $get_media_info_down_gal['id'];?>&buyer_id=<?php echo $_SESSION['gen_user_id'];?>&title=<?php echo str_replace("'","\'",$get_media_info_down_gal['title']);?>&image_name=<?php echo $get_gal_imagerecevent['profile_image'];?>" id="download_galrecevent<?php echo $exp_rec_ev;?>" style="display:none;"></a>
								<script type="text/javascript">
								$(document).ready(function() {
										$("#download_galrecevent<?php echo $exp_rec_ev;?>").fancybox({
										'width'				: '75%',
										'height'			: '75%',
										'transitionIn'		: 'none',
										'transitionOut'		: 'none',
										'type'				: 'iframe'
										});
								});
								</script>
							<?php
						}
						else if($get_recorded_event1['featured_media']=="Song" && $get_recorded_event1['media_id']!=0)
						{
							if($get_recorded_event1['featured_media_table']=="general_artist_audio" || $get_recorded_event1['featured_media_table']=="general_community_audio")
							{
								?>
									<a href="javascript:void(0);"><img src="../images/profile/play.gif" onmouseover="this.src='../images/profile/play-over.gif'" onmouseout="this.src='../images/profile/play.gif'" onclick="showPlayer_reg('a','<?php echo $get_recorded_event1['media_id'];?>','play_reg','<?php echo $get_recorded_event1['featured_media_table'];?>')" title="Play file now."/></a>
									<a href="javascript:void(0);"><img src="../images/profile/add.gif"  onmouseover="this.src='../images/profile/add-over.gif'" onmouseout="this.src='../images/profile/add.gif'" onclick="showPlayer_reg('a','<?php echo $get_recorded_event1['media_id'];?>','add_reg','<?php echo $get_recorded_event1['featured_media_table'];?>')" title="Add file to the bottom of your playlist without stopping current media."/></a>
								<?php
							}
							else{
								?>
									<a href="javascript:void(0);"><img src="../images/profile/play.gif" onmouseover="this.src='../images/profile/play-over.gif'" onmouseout="this.src='../images/profile/play.gif'" onclick="showPlayer('a','<?php echo $get_recorded_event1['media_id'];?>','play')" title="Play file now."/></a>
									<a href="javascript:void(0);"><img src="../images/profile/add.gif"  onmouseover="this.src='../images/profile/add-over.gif'" onmouseout="this.src='../images/profile/add.gif'" onclick="showPlayer('a','<?php echo $get_recorded_event1['media_id'];?>','add')" title="Add file to the bottom of your playlist without stopping current media."/></a>
								<?php
							}
							if($get_recorded_event1['featured_media_table']!="general_artist_audio" || $get_recorded_event1['featured_media_table']!="general_community_audio")
							{
								$get_media_info_receve = $new_profile_class_obj ->get_fetatured_media_info($get_recorded_event1['media_id']);
								if($get_media_info_receve['sharing_preference']==5 || $get_media_info_receve['sharing_preference']==2)
								{
									$song_image_name_cartrecevent = $new_profile_class_obj->get_audio_img($get_media_info_receve['id']);
									$free_down_rel ="";
									for($find_down=0;$find_down<=count($get_songs_pnew_w);$find_down++)
									{
										if($get_songs_pnew_w[$find_down]['id']==$get_media_info_receve['id']){
										$free_down_rel = "yes";
										}
										else{
											if($free_down_rel =="" || $free_down_rel =="no"){
												$free_down_rel = "no";
											}
										}
									}
									if($free_down_rel == "yes"){
									 ?>
								<!--	 <a href="javascript:void(0);" onclick="download_directly_song('<?php echo $get_media_info_receve['id'];?>','<?php echo str_replace("'","\'",$get_media_info_receve['title']);?>','<?php echo str_replace("'","\'",$get_media_info_receve['creator']);?>','<?php echo str_replace("'","\'",$get_media_info_receve['from']);?>')"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>-->
								<!--<img src="../images/profile/download-over.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download-over.gif'" title="Add media to your cart for free download or purchase." />-->
									 <?php
									}
									else if($get_media_info_receve['sharing_preference']==2){
									 ?>
									<!--<a href="javascript:void(0);" onclick="ask_to_donate('<?php echo str_replace("'","\'",$get_media_info_receve['title']);?>','<?php echo str_replace("'","\'",$get_media_info_receve['creator']);?>','<?php echo $get_media_info_receve['id'];?>','download_songrecevent<?php echo $exp_rec_ev;?>')"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>-->
									<!--<img src="../images/profile/download-over.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download-over.gif'" title="Add media to your cart for free download or purchase." />-->
								 <?php
									}
									else{
									?>
									<!--	<a href="javascript:void(0);" onclick="down_song_pop_recevent(<?php echo $exp_rec_ev;?>)"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>-->
										<!--<img src="../images/profile/download-over.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download-over.gif'" title="Add media to your cart for free download or purchase." />-->
									<?php
									}
								}
								else{
								?>
								<!--<img src="../images/profile/download-over.gif" />-->
								<?php
								}
							if($get_recorded_event1['media_id']!="" && $get_recorded_event1['media_id']!=" " && $get_recorded_event1['media_id']!=0){
								$recorded_event_song2 = $recorded_event_song2 .",". $get_recorded_event1['media_id'];
							}
							?>
							<a href="addtocart.php?seller_id=<?php echo $get_media_info_receve['general_user_id'];?>&media_id=<?php echo $get_media_info_receve['id'];?>&buyer_id=<?php echo $_SESSION['gen_user_id'];?>&title=<?php echo str_replace("'","\'",$get_media_info_receve['title']);?>&image_name=<?php echo $song_image_name_cartrecevent;?>" id="download_songrecevent<?php echo $exp_rec_ev;?>" style="display:none;"></a>
							<script type="text/javascript">
							$(document).ready(function() {
									$("#download_songrecevent<?php echo $exp_rec_ev;?>").fancybox({
									'height'			: '75%',
									'width'				: '69%',
									'transitionIn'		: 'none',
									'transitionOut'		: 'none',
									'type'				: 'iframe'
									});
							});
							</script>
							<?php
						}
						else{
							?>
							<!--<img src="../images/profile/download-over.gif" />-->
							<?php
							}
						}
						else if($get_recorded_event1['featured_media']=="Video" && $get_recorded_event1['media_id']!=0)
						{
							if($get_recorded_event1['featured_media_table']=="general_artist_video" || $get_recorded_event1['featured_media_table']=="general_community_video")
							{
								?>
									<a href="javascript:void(0);"><img src="../images/profile/play.gif" onmouseover="this.src='../images/profile/play-over.gif'" onmouseout="this.src='../images/profile/play.gif'" onclick="showPlayer_reg('v','<?php echo $get_recorded_event1['media_id'];?>','playlist_reg','<?php echo $get_recorded_event1['featured_media_table'];?>')" title="Play file now."/></a>
									<a href="javascript:void(0);"><img src="../images/profile/add.gif"  onmouseover="this.src='../images/profile/add-over.gif'" onmouseout="this.src='../images/profile/add.gif'" onclick="showPlayer_reg('v','<?php echo $get_recorded_event1['media_id'];?>','addvi_reg','<?php echo $get_recorded_event1['featured_media_table'];?>')" title="Add file to the bottom of your playlist without stopping current media."/></a>
								<?php
							}else{
								?>
									<a href="javascript:void(0);"><img src="../images/profile/play.gif" onmouseover="this.src='../images/profile/play-over.gif'" onmouseout="this.src='../images/profile/play.gif'" onclick="showPlayer('v','<?php echo $get_recorded_event1['media_id'];?>','playlist')" title="Play file now."/></a>
									<a href="javascript:void(0);"><img src="../images/profile/add.gif"  onmouseover="this.src='../images/profile/add-over.gif'" onmouseout="this.src='../images/profile/add.gif'" onclick="showPlayer('v','<?php echo $get_recorded_event1['media_id'];?>','addvi')" title="Add file to the bottom of your playlist without stopping current media."/></a>
								<?php
							}
							if($get_recorded_event1['featured_media_table']!="general_artist_video" || $get_recorded_event1['featured_media_table']!="general_community_video")
							{
								$get_media_info_down_gal = $new_profile_class_obj ->get_fetatured_media_info($get_recorded_event1['media_id']);
								if($get_media_info_down_gal['sharing_preference']==5 || $get_media_info_down_gal['sharing_preference']==2)
								{
									$get_video_image = $new_profile_class_obj->getvideoimage($get_media_info_down_gal['id']);
									$image_name_cartrecevent = "https://img.youtube.com/vi/".$get_video_image."/default.jpg";
									$free_down_rel ="";
									for($find_down=0;$find_down<=count($get_vids_pnew_w);$find_down++)
									{
										if($get_vids_pnew_w[$find_down]['id']==$get_media_info_down_gal['id']){
										$free_down_rel = "yes";
										}
										else{
											if($free_down_rel =="" || $free_down_rel =="no"){
												$free_down_rel = "no";
											}
										}
									}
									if($free_down_rel == "yes"){
									 ?>
									 <!--<a href="javascript:void(0);" onclick="download_directly_song('<?php echo $get_media_info_down_gal['id'];?>','<?php echo str_replace("'","\'",$get_media_info_down_gal['title']);?>','<?php echo str_replace("'","\'",$get_media_info_down_gal['creator']);?>','<?php echo str_replace("'","\'",$get_media_info_down_gal['from']);?>')"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>-->
									 <!--<img src="../images/profile/download-over.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download-over.gif'" title="Add media to your cart for free download or purchase." />-->
									 <?php
									}
									else if($get_media_info_down_gal['sharing_preference']==2){
									 ?>
								<!--	<a href="javascript:void(0);" onclick="ask_to_donate('<?php echo str_replace("'","\'",$get_media_info_down_gal['title']);?>','<?php echo str_replace("'","\'",$get_media_info_down_gal['creator']);?>','<?php echo $get_media_info_down_gal['id'];?>','download_videorecevent<?php echo $exp_rec_ev;?>')"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>-->
									<!--<img src="../images/profile/download-over.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download-over.gif'" title="Add media to your cart for free download or purchase." />-->
								 <?php
									}
									else{
									?>
								<!--		<a href="javascript:void(0);" onclick="down_video_pop_recevent(<?php echo $exp_rec_ev;?>)"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>-->
										<!--<img src="../images/profile/download-over.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download-over.gif'" title="Add media to your cart for free download or purchase." />-->
									<?php
									}
								}
								else{
								?>
								<!--<img src="../images/profile/download-over.gif" />-->
								<?php
								}
							}
							else{
							?>
							<!--<img src="../images/profile/download-over.gif" />-->
							<?php
							}
							if($get_recorded_event1['media_id']!="" && $get_recorded_event1['media_id']!=" " && $get_recorded_event1['media_id']!=0){
								$recorded_event_video2 = $recorded_event_video2 .",". $get_recorded_event1['media_id'];
							}
							?>
							<a href="addtocart.php?seller_id=<?php echo $get_media_info_receve['general_user_id'];?>&media_id=<?php echo $get_media_info_receve['id'];?>&buyer_id=<?php echo $_SESSION['gen_user_id'];?>&title=<?php echo str_replace("'","\'",$get_media_info_receve['title']);?>&image_name=<?php echo $image_name_cartrecevent;?>" id="download_videorecevent<?php echo $exp_rec_ev;?>" style="display:none;"></a>
							<script type="text/javascript">
							$(document).ready(function() {
									$("#download_videorecevent<?php echo $exp_rec_ev;?>").fancybox({
									'height'			: '75%',
									'width'				: '69%',
									'transitionIn'		: 'none',
									'transitionOut'		: 'none',
									'type'				: 'iframe'
									});
							});
							</script>
							<?php
						}
						else{
					  ?>
						<!--<a href="javascript:void(0);"><img src="../images/profile/play.gif" onmouseover="this.src='../images/profile/play-over.gif'" onmouseout="this.src='../images/profile/play.gif'" /></a>
						<a href="javascript:void(0);"><img src="../images/profile/add.gif"  onmouseover="this.src='../images/profile/add-over.gif'" onmouseout="this.src='../images/profile/add.gif'"/></a>-->
						<?php
						}
						?>
						<!--<a href=""><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" /></a>-->
					  </div>
					  <div class="type_subtype">
					  <a href = "/<?php echo $get_recorded_event1['profile_url'];?>" onclick="return chk_for_creator('<?php echo $get_recorded_event1['profile_url'];?>')">
					  <?php
						if($get_recorded_event1['date']!="")
						{
							$new_dt = date("m.d.y", strtotime($get_recorded_event1['date']));
							//$exp_dt = explode('.',$new_dt);
							//$exp_yr = substr($exp_dt[2],-2);
							if(strlen($get_recorded_event1['title'])>=22){
								echo substr($get_recorded_event1['title'],0,19)."...";
							}else{
								echo $get_recorded_event1['title'];
							}
							?>
<?php
						}
						else
						{
							if(strlen($get_recorded_event1['title'])>=22){
								echo substr($get_recorded_event1['title'],0,19)."...";
							}else{
								echo $get_recorded_event1['title'];
							}
							?>
<?php
						}
?>
					</a><?php if(isset($get_recorded_event1['artist_id']))
											{
											?>
											<div class="by_details">
					<?php
												$type_eves = 'artist';
												$event_name = "";
												if($get_recorded_event1['state_id']!=0)
												{
													$event_name = $display->state_event_names($get_recorded_event1['id'],$type_eves);
												}
												
												if(($get_recorded_event1['date']!="" && $get_recorded_event1['date']!='0000-00-00') || ($get_recorded_event1['state_id']!="" && $get_recorded_event1['state_id']!=0))
												{
													if($get_recorded_event1['date']!="" && $get_recorded_event1['date']!='0000-00-00')
													{
														if($event_name!="")
														{
															if(strlen($event_name)>=9)
															{
																echo date('m.d.y',strtotime($get_recorded_event1['date'])).', '.substr($event_name,0,6)."...";
															}
															else
															{
																echo date('m.d.y',strtotime($get_recorded_event1['date'])).', '.$event_name;
															}
														}
														else
														{
															echo date('m.d.y',strtotime($get_recorded_event1['date']));
														}
													}
													else
													{
														if($event_name!="")
														{
															if(strlen($event_name)>=19)
															{
																echo substr($event_name,0,16)."...";
															}
															else
															{
																echo $event_name;
															}
														}
													}
												}					
			?>
											</div>
			<?php
												$get_profile_type = $display->types_of_search('artist_event',$get_recorded_event1['id']);
												if(!empty($get_profile_type) && $get_profile_type!=Null)
												{
													$res_profile_type = mysql_fetch_assoc($get_profile_type);
													if($res_profile_type['type_name']!=""){
													?>
														<span style="font-size:13px; font-weight:normal;">
														<?php 
														if(strlen($res_profile_type['type_name'])>26){
															echo substr($res_profile_type['type_name'],0,24)."...";
														}else{
															echo $res_profile_type['type_name'];
														}
														if($res_profile_type['subtype_name']!="" && strlen($res_profile_type['type_name']<26)){
															$type_length = strlen($res_profile_type['type_name']);
															if($type_length + strlen($res_profile_type['subtype_name'])>22){
																echo ", ".substr($res_profile_type['subtype_name'],0,22 - $type_length)."...";
															}else{
																echo ", ".$res_profile_type['subtype_name'];
															}
														}
														/*echo $res_profile_type['type_name'];
														if($res_profile_type['subtype_name']!=""){
															echo ", ".$res_profile_type['subtype_name'];
														}*/
														?>
														</span>
												<?php
													}
												}
											}
											elseif(isset($get_recorded_event1['community_id']))
											{
											?>
											<div class="by_details">
					<?php
												$type_eves = 'community';
												$event_name = "";
												if($get_recorded_event1['state_id']!=0)
												{
													$event_name = $display->state_event_names($get_recorded_event1['id'],$type_eves);
												}
												
												if(($get_recorded_event1['date']!="" && $get_recorded_event1['date']!='0000-00-00') || ($get_recorded_event1['state_id']!="" && $get_recorded_event1['state_id']!=0))
												{
													if($get_recorded_event1['date']!="" && $get_recorded_event1['date']!='0000-00-00')
													{
														if($event_name!="")
														{
															if(strlen($event_name)>=9)
															{
																echo date('m.d.y',strtotime($get_recorded_event1['date'])).', '.substr($event_name,0,6)."...";
															}
															else
															{
																echo date('m.d.y',strtotime($get_recorded_event1['date'])).', '.$event_name;
															}
														}
														else
														{
															echo date('m.d.y',strtotime($get_recorded_event1['date']));
														}
													}
													else
													{
														if($event_name!="")
														{
															if(strlen($event_name)>=19)
															{
																echo substr($event_name,0,16)."...";
															}
															else
															{
																echo $event_name;
															}
														}
													}
												}					
			?>
											</div>
			<?php
												$get_profile_type = $display->types_of_search('community_event',$get_recorded_event1['id']);
												if(!empty($get_profile_type) && $get_profile_type!=Null)
												{
													$res_profile_type = mysql_fetch_assoc($get_profile_type);
													if($res_profile_type['type_name']!=""){
													?>
														<span style="font-size:13px; font-weight:normal;">
														<?php 
														if(strlen($res_profile_type['type_name'])>26){
															echo substr($res_profile_type['type_name'],0,24)."...";
														}else{
															echo $res_profile_type['type_name'];
														}
														if($res_profile_type['subtype_name']!="" && strlen($res_profile_type['type_name']<26)){
															$type_length = strlen($res_profile_type['type_name']);
															if($type_length + strlen($res_profile_type['subtype_name'])>22){
																echo ", ".substr($res_profile_type['subtype_name'],0,22 - $type_length)."...";
															}else{
																echo ", ".$res_profile_type['subtype_name'];
															}
															
														}
														/*echo $res_profile_type['type_name'];
														if($res_profile_type['subtype_name']!=""){
															echo ", ".$res_profile_type['subtype_name'];
														}*/
														?>
														</span>
												<?php
													}
												}
											} ?> </div></div>
					</div>
					<?php
					}
				 }
			?>
				<input type="hidden" id="recorded_event_video2" value="<?php echo $recorded_event_video2;?>"/>
				<input type="hidden" id="recorded_event_song2" value="<?php echo $recorded_event_song2;?>"/>
				<script type="text/javascript">
				$("document").ready(function(){
					var a = document.getElementById("recorded_event_video2").value;
					var b = document.getElementById("recorded_event_song2").value;
					if((a =="" || a ==" ") && (b=="" || b==" "))
					{
						$("#profileFeaturedevent").css({"display":"none"});
					}
				});
				
				</script>
		  </div>
		  <?php
		}}}
		}
		else
		{
			if(isset($get_recorded_event) && !empty($get_recorded_event))
			{
			   // if(mysql_num_rows($get_recorded_event[0])>0)
			 // {
			  ?>
			  <h2>Recorded Events</h2>
			  <div class="rowItem">
			  <?php
				foreach($get_recorded_event as $get_recorded_event2)
				{
					$reccounter = $reccounter + 1;
					while($get_recorded_event1=mysql_fetch_assoc($get_recorded_event2))
					{
					?>
					<div class="tabItem"> 
					<a href = "/<?php echo $get_recorded_event1['profile_url'];?>" onclick="return chk_for_creator('<?php echo $get_recorded_event1['profile_url'];?>')">
						<img class="imgs_pro" src="<?php if($get_recorded_event1['image_name']==""){echo "https://arteventprofile.s3.amazonaws.com/Noimage.png"; } else { echo $get_recorded_event1['image_name']; } ?>" width="200" height="200" />
					</a>
					<div class="both_wrap_per">
					<div class="player">
					 <?php
						if($get_recorded_event1['featured_media']=="Gallery" && $get_recorded_event1['media_id']!=0)
						{
							$feature_reg_big_img = "";
							$all_register_images = "";
							$feature_reg_thum_path = "";
							$feature_reg_org_path = "";
							$featured_reg_gal_img_title = "";
								
							if($get_recorded_event1['featured_media_table']!="" &&($get_recorded_event1['featured_media_table']=="general_artist_gallery_list" || $get_recorded_event1['featured_media_table']=="general_community_gallery_list"))
							{								
								$get_register_featured_gallery = "";
								$get_register_featured_gallery = $new_profile_class_obj ->get_register_featured_media_gallery($get_recorded_event1['media_id'],$get_recorded_event1['featured_media_table']);
								$all_register_images ="";
								$feature_reg_big_img ="";
								$featured_reg_gal_img_title ="";
								for($cou_i=0;$cou_i<count($get_register_featured_gallery);$cou_i++)
								{
									$feature_reg_big_img = $get_register_featured_gallery[0]['image_name'];
									$all_register_images = $all_register_images.",".$get_register_featured_gallery[$cou_i]['image_name'];
									$featured_reg_gal_img_title = $featured_reg_gal_img_title .",".$get_register_featured_gallery[$cou_i]['image_title'];
								}
								$all_register_images = ltrim($all_register_images,',');
								$featured_reg_gal_img_title = ltrim($featured_reg_gal_img_title,',');
								$feature_reg_org_path = "https://reggallery.s3.amazonaws.com/";
								$feature_reg_thum_path = "https://reggalthumb.s3.amazonaws.com/";
							}
							else if($get_recorded_event1['featured_media_table']!="" && $get_recorded_event1['featured_media_table']=="general_media"){
								$get_register_featured_gallery = $new_profile_class_obj ->get_featured_gallery_images($get_recorded_event1['media_id']);
								$all_register_images = "";
								$feature_reg_big_img = "";
								$featured_reg_gal_img_title ="";
								$feature_reg_thum_path = "";
								$feature_reg_org_path = "";
								for($cou_i=0;$cou_i<count($get_register_featured_gallery);$cou_i++)
								{
									$feature_reg_big_img = $get_register_featured_gallery[0]['image_name'];
									$all_register_images = $all_register_images.",".$get_register_featured_gallery[$cou_i]['image_name'];
									$featured_reg_gal_img_title = $featured_reg_gal_img_title .",".$get_register_featured_gallery[$cou_i]['image_title'];
								}
								$all_register_images = ltrim($all_register_images,',');
								$featured_reg_gal_img_title = ltrim($featured_reg_gal_img_title,',');
								$feature_reg_org_path = "https://medgallery.s3.amazonaws.com/";
								$feature_reg_thum_path = "https://medgalthumb.s3.amazonaws.com/";
							}
						?>
						<a href="javascript:void(0);"><img src="../images/profile/play.gif" onmouseover="this.src='../images/profile/play-over.gif'" onmouseout="this.src='../images/profile/play.gif'" onclick="checkload('<?php echo $feature_reg_big_img;?>','<?php echo $all_register_images; ?>','<?php echo $feature_reg_thum_path; ?>','<?php echo $feature_reg_org_path; ?>','<?php echo $featured_reg_gal_img_title; ?>')" title="Play file now."/></a>
						<!--<a href="javascript:void(0);"><img src="../images/profile/add.gif"  onmouseover="this.src='../images/profile/add-over.gif'" onmouseout="this.src='../images/profile/add.gif'" title="Add file to the bottom of your playlist without stopping current media."/></a>-->
						<?php
							if($get_recorded_event1['featured_media_table']!="" && $get_recorded_event1['featured_media_table']=="general_media"){
								$get_gal_imagerecevent  = $new_profile_class_obj ->get_gallery_info($get_recorded_event1['media_id']);
								$get_media_info_down_gal = $new_profile_class_obj ->get_fetatured_media_info($get_recorded_event1['media_id']);
								if($get_media_info_down_gal['sharing_preference']==5 || $get_media_info_down_gal['sharing_preference']==2){
								?>
									<!--<a href="javascript:void(0);" onclick="ask_to_donate('<?php echo str_replace("'","\'",$get_media_info_down_gal['title']);?>','<?php echo str_replace("'","\'",$get_media_info_down_gal['creator']);?>','<?php echo $get_media_info_down_gal['id'];?>','download_galrecevent<?php echo $reccounter;?>')"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>
									<img src="../images/profile/download-over.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download-over.gif'" title="Add media to your cart for free download or purchase." />-->
								 <?php
								}else{
								?>
									<!--<img src="../images/profile/download-over.gif" />-->
								<?php
								}
							}else{
							?>
								<!--<img src="../images/profile/download-over.gif" />-->
							<?php
							}
							?>
							<a href="addtocart.php?seller_id=<?php echo $get_media_info_down_gal['general_user_id'];?>&media_id=<?php echo $get_media_info_down_gal['id'];?>&buyer_id=<?php echo $_SESSION['gen_user_id'];?>&title=<?php echo str_replace("'","\'",$get_media_info_down_gal['title']);?>&image_name=<?php echo $get_gal_imagerecevent['profile_image'];?>" id="download_galrecevent<?php echo $reccounter;?>" style="display:none;"></a>
								<script type="text/javascript">
								$(document).ready(function() {
										$("#download_galrecevent<?php echo $reccounter;?>").fancybox({
										'width'				: '75%',
										'height'			: '75%',
										'transitionIn'		: 'none',
										'transitionOut'		: 'none',
										'type'				: 'iframe'
										});
								});
								</script>
							<?php
						}
						else if($get_recorded_event1['featured_media']=="Song" && $get_recorded_event1['media_id']!=0)
						{
							if($get_recorded_event1['featured_media_table']=="general_artist_audio" || $get_recorded_event1['featured_media_table']=="general_community_audio")
							{
								?>
									<a href="javascript:void(0);"><img src="../images/profile/play.gif" onmouseover="this.src='../images/profile/play-over.gif'" onmouseout="this.src='../images/profile/play.gif'" onclick="showPlayer_reg('a','<?php echo $get_recorded_event1['media_id'];?>','play_reg','<?php echo $get_recorded_event1['featured_media_table'];?>')" title="Play file now."/></a>
									<a href="javascript:void(0);"><img src="../images/profile/add.gif"  onmouseover="this.src='../images/profile/add-over.gif'" onmouseout="this.src='../images/profile/add.gif'" onclick="showPlayer_reg('a','<?php echo $get_recorded_event1['media_id'];?>','add_reg','<?php echo $get_recorded_event1['featured_media_table'];?>')" title="Add file to the bottom of your playlist without stopping current media."/></a>
								<?php
							}
							else{
								?>
									<a href="javascript:void(0);"><img src="../images/profile/play.gif" onmouseover="this.src='../images/profile/play-over.gif'" onmouseout="this.src='../images/profile/play.gif'" onclick="showPlayer('a','<?php echo $get_recorded_event1['media_id'];?>','play')" title="Play file now."/></a>
									<a href="javascript:void(0);"><img src="../images/profile/add.gif"  onmouseover="this.src='../images/profile/add-over.gif'" onmouseout="this.src='../images/profile/add.gif'" onclick="showPlayer('a','<?php echo $get_recorded_event1['media_id'];?>','add')" title="Add file to the bottom of your playlist without stopping current media."/></a>
								<?php
							}
							if($get_recorded_event1['featured_media_table']!="general_artist_audio" || $get_recorded_event1['featured_media_table']!="general_community_audio")
							{
								$get_media_info_down_gal = $new_profile_class_obj ->get_fetatured_media_info($get_recorded_event1['media_id']);
								if($get_media_info_down_gal['sharing_preference']==5 || $get_media_info_down_gal['sharing_preference']==2)
								{
								$song_image_name_cartrecevent = $new_profile_class_obj->get_audio_img($get_media_info_down_gal['id']);
								$free_down_rel ="";
								for($find_down=0;$find_down<=count($get_songs_pnew_w);$find_down++)
								{
									if($get_songs_pnew_w[$find_down]['id']==$get_media_info_down_gal['id']){
									$free_down_rel = "yes";
									}
									else{
										if($free_down_rel =="" || $free_down_rel =="no"){
											$free_down_rel = "no";
										}
									}
								}
								if($free_down_rel == "yes"){
								 ?>
							<!--	 <a href="javascript:void(0);" onclick="download_directly_song('<?php echo $get_media_info_down_gal['id'];?>','<?php echo str_replace("'","\'",$get_media_info_down_gal['title']);?>','<?php echo str_replace("'","\'",$get_media_info_down_gal['creator']);?>','<?php echo str_replace("'","\'",$get_media_info_down_gal['from']);?>')"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>-->
								 <?php
								}
								else if($get_media_info_down_gal['sharing_preference']==2){
								 ?>
								<!--<a href="javascript:void(0);" onclick="ask_to_donate('<?php echo str_replace("'","\'",$get_media_info_down_gal['title']);?>','<?php echo str_replace("'","\'",$get_media_info_down_gal['creator']);?>','<?php echo $get_media_info_down_gal['id'];?>','download_songrecevent<?php echo $reccounter;?>')"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>
								<img src="../images/profile/download-over.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download-over.gif'" title="Add media to your cart for free download or purchase." />-->
							 <?php
								}
								else{
								?>
								<!--	<a href="javascript:void(0);" onclick="down_song_pop_recevent(<?php echo $reccounter;?>)"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>
									<img src="../images/profile/download-over.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download-over.gif'" title="Add media to your cart for free download or purchase." />-->
								<?php
								}
							}else{
							?>
								<!--<img src="../images/profile/download-over.gif" />-->
							<?php
							}
							if($get_recorded_event1['media_id']!="" && $get_recorded_event1['media_id']!=" " && $get_recorded_event1['media_id']!=0){
								$recorded_event_song1 = $recorded_event_song1 .",". $get_recorded_event1['media_id'];
							}
							?>
							<a href="addtocart.php?seller_id=<?php echo $get_media_info_down_gal['general_user_id'];?>&media_id=<?php echo $get_media_info_down_gal['id'];?>&buyer_id=<?php echo $_SESSION['gen_user_id'];?>&title=<?php echo str_replace("'","\'",$get_media_info_down_gal['title']);?>&image_name=<?php echo $song_image_name_cartrecevent;?>" id="download_songrecevent<?php echo $reccounter;?>" style="display:none;"></a>
							<script type="text/javascript">
							$(document).ready(function() {
									$("#download_songrecevent<?php echo $reccounter;?>").fancybox({
									'height'			: '75%',
									'width'				: '69%',
									'transitionIn'		: 'none',
									'transitionOut'		: 'none',
									'type'				: 'iframe'
									});
							});
							</script>
							<?php
							}	
							else{
							?>
								<!--<img src="../images/profile/download-over.gif" />-->
							<?php
							}
						}
						else if($get_recorded_event1['featured_media']=="Video" && $get_recorded_event1['media_id']!=0)
						{
							if($get_recorded_event1['featured_media_table']=="general_artist_video" || $get_recorded_event1['featured_media_table']=="general_community_video")
							{
								?>
									<a href="javascript:void(0);"><img src="../images/profile/play.gif" onmouseover="this.src='../images/profile/play-over.gif'" onmouseout="this.src='../images/profile/play.gif'" onclick="showPlayer_reg('v','<?php echo $get_recorded_event1['media_id'];?>','playlist_reg','<?php echo $get_recorded_event1['featured_media_table'];?>')" title="Play file now."/></a>
									<a href="javascript:void(0);"><img src="../images/profile/add.gif"  onmouseover="this.src='../images/profile/add-over.gif'" onmouseout="this.src='../images/profile/add.gif'" onclick="showPlayer_reg('v','<?php echo $get_recorded_event1['media_id'];?>','addvi_reg','<?php echo $get_recorded_event1['featured_media_table'];?>')" title="Add file to the bottom of your playlist without stopping current media."/></a>
								<?php
							}else{
								?>
									<a href="javascript:void(0);"><img src="../images/profile/play.gif" onmouseover="this.src='../images/profile/play-over.gif'" onmouseout="this.src='../images/profile/play.gif'" onclick="showPlayer('v','<?php echo $get_recorded_event1['media_id'];?>','playlist')" title="Play file now."/></a>
									<a href="javascript:void(0);"><img src="../images/profile/add.gif"  onmouseover="this.src='../images/profile/add-over.gif'" onmouseout="this.src='../images/profile/add.gif'" onclick="showPlayer('v','<?php echo $get_recorded_event1['media_id'];?>','addvi')" title="Add file to the bottom of your playlist without stopping current media."/></a>
								<?php
							}
							if($get_recorded_event1['featured_media_table']!="general_artist_audio" || $get_recorded_event1['featured_media_table']!="general_community_audio"){
								$get_media_info_down_gal = $new_profile_class_obj ->get_fetatured_media_info($get_recorded_event1['media_id']);
								if($get_media_info_down_gal['sharing_preference']==5 || $get_media_info_down_gal['sharing_preference']==2){
								$get_video_image = $new_profile_class_obj->getvideoimage($get_media_info_down_gal['id']);
								$image_name_cartrecevent = "https://img.youtube.com/vi/".$get_video_image."/default.jpg";
								$free_down_rel ="";
								for($find_down=0;$find_down<=count($get_vids_pnew_w);$find_down++)
								{
									if($get_vids_pnew_w[$find_down]['id']==$get_media_info_down_gal['id']){
									$free_down_rel = "yes";
									}
									else{
										if($free_down_rel =="" || $free_down_rel =="no"){
											$free_down_rel = "no";
										}
									}
								}
								if($free_down_rel == "yes"){
								 ?>
								<!-- <a href="javascript:void(0);" onclick="download_directly_song('<?php echo $get_media_info_down_gal['id'];?>','<?php echo str_replace("'","\'",$get_media_info_down_gal['title']);?>','<?php echo str_replace("'","\'",$get_media_info_down_gal['creator']);?>','<?php echo str_replace("'","\'",$get_media_info_down_gal['from']);?>')"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>-->
								<!--<img src="../images/profile/download-over.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download-over.gif'" title="Add media to your cart for free download or purchase." />-->
								 <?php
								}
								else if($get_media_info_down_gal['sharing_preference']==2){
								 ?>
								<!--<a href="javascript:void(0);" onclick="ask_to_donate('<?php echo str_replace("'","\'",$get_media_info_down_gal['title']);?>','<?php echo str_replace("'","\'",$get_media_info_down_gal['creator']);?>','<?php echo $get_media_info_down_gal['id'];?>','download_videorecevent<?php echo $reccounter;?>')"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>-->
								<!--<img src="../images/profile/download-over.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download-over.gif'" title="Add media to your cart for free download or purchase." />-->
							 <?php
								}
								else{
								?>
								<!--	<a href="javascript:void(0);" onclick="down_video_pop_recevent(<?php echo $reccounter;?>)"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>-->
									<!--<img src="../images/profile/download-over.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download-over.gif'" title="Add media to your cart for free download or purchase." />-->
								<?php
								}
							}else{
							?>
								<!--<img src="../images/profile/download-over.gif" />-->
							<?php
							}
							if($get_recorded_event1['media_id']!="" && $get_recorded_event1['media_id']!=" " && $get_recorded_event1['media_id']!=0){
							$recorded_event_video1 = $recorded_event_video1 .",". $get_recorded_event1['media_id'];
							}
							?>
							<a href="addtocart.php?seller_id=<?php echo $get_media_info_down_gal['general_user_id'];?>&media_id=<?php echo $get_media_info_down_gal['id'];?>&buyer_id=<?php echo $_SESSION['gen_user_id'];?>&title=<?php echo str_replace("'","\'",$get_media_info_down_gal['title']);?>&image_name=<?php echo $image_name_cartrecevent;?>" id="download_videorecevent<?php echo $reccounter;?>" style="display:none;"></a>
							<script type="text/javascript">
							$(document).ready(function() {
									$("#download_videorecevent<?php echo $reccounter;?>").fancybox({
									'height'			: '75%',
									'width'				: '69%',
									'transitionIn'		: 'none',
									'transitionOut'		: 'none',
									'type'				: 'iframe'
									});
							});
							</script>
							<?php
							}
							else{
							?>
								<!--<img src="../images/profile/download-over.gif" />-->
							<?php
							}
						}
						else{
					  ?>
						<!--<a href="javascript:void(0);"><img src="../images/profile/play.gif" onmouseover="this.src='../images/profile/play-over.gif'" onmouseout="this.src='../images/profile/play.gif'" /></a>
						<a href="javascript:void(0);"><img src="../images/profile/add.gif"  onmouseover="this.src='../images/profile/add-over.gif'" onmouseout="this.src='../images/profile/add.gif'"/></a>-->
						<?php
						}
						?>
						<!--<a href=""><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" /></a>-->
						
					  </div>
					  <div class="type_subtype"><a href = "/<?php echo $get_recorded_event1['profile_url'];?>" onclick="return chk_for_creator('<?php echo $get_recorded_event1['profile_url'];?>')">
						<?php
						if($get_recorded_event1['date']!="")
						{
							$new_dt = date("m.d.y", strtotime($get_recorded_event1['date']));
							//$exp_dt = explode('.',$new_dt);
							//$exp_yr = substr($exp_dt[2],-2);
							if(strlen($get_recorded_event1['title'])>=22)
							{
								echo substr($get_recorded_event1['title'],0,19)."...";
							}else{
								echo $get_recorded_event1['title'];
							}	
							?>
<?php
						}
						else
						{
							if(strlen($get_recorded_event1['title'])>=22){
								echo substr($get_recorded_event1['title'],0,19)."...";
							}else{
								echo $get_recorded_event1['title'];
							}
							?>
<?php
						}
?>
					</a><?php if(isset($get_recorded_event1['artist_id']))
											{
											?>
											<div class="by_details">
					<?php
												$type_eves = 'artist';
												$event_name = "";
												if($get_recorded_event1['state_id']!=0)
												{
													$event_name = $display->state_event_names($get_recorded_event1['id'],$type_eves);
												}
												
												if(($get_recorded_event1['date']!="" && $get_recorded_event1['date']!='0000-00-00') || ($get_recorded_event1['state_id']!="" && $get_recorded_event1['state_id']!=0))
												{
													if($get_recorded_event1['date']!="" && $get_recorded_event1['date']!='0000-00-00')
													{
														if($event_name!="")
														{
															if(strlen($event_name)>=9)
															{
																echo date('m.d.y',strtotime($get_recorded_event1['date'])).', '.substr($event_name,0,6)."...";
															}
															else
															{
																echo date('m.d.y',strtotime($get_recorded_event1['date'])).', '.$event_name;
															}
														}
														else
														{
															echo date('m.d.y',strtotime($get_recorded_event1['date']));
														}
													}
													else
													{
														if($event_name!="")
														{
															if(strlen($event_name)>=19)
															{
																echo substr($event_name,0,16)."...";
															}
															else
															{
																echo $event_name;
															}
														}
													}
												}					
			?>
											</div>
			<?php
												$get_profile_type = $display->types_of_search('artist_event',$get_recorded_event1['id']);
												if(!empty($get_profile_type) && $get_profile_type!=Null)
												{
													$res_profile_type = mysql_fetch_assoc($get_profile_type);
													if($res_profile_type['type_name']!=""){
													?>
														<span style="font-size:13px; font-weight:normal;">
														<?php 
														if(strlen($res_profile_type['type_name'])>26){
															echo substr($res_profile_type['type_name'],0,24)."...";
														}else{
															echo $res_profile_type['type_name'];
														}
														if($res_profile_type['subtype_name']!="" && strlen($res_profile_type['type_name']<26)){
															$type_length = strlen($res_profile_type['type_name']);
															if($type_length + strlen($res_profile_type['subtype_name'])>22){
																echo ", ".substr($res_profile_type['subtype_name'],0,22 - $type_length)."...";
															}else{
																echo ", ".$res_profile_type['subtype_name'];
															}
															
														}
														/*echo $res_profile_type['type_name'];
														if($res_profile_type['subtype_name']!=""){
															echo ", ".$res_profile_type['subtype_name'];
														}*/
														?>
														</span>
												<?php
													}
												}
											}
											elseif(isset($get_recorded_event1['community_id']))
											{
											?>
											<div class="by_details">
					<?php
												$type_eves = 'community';
												$event_name = "";
												if($get_recorded_event1['state_id']!=0)
												{
													$event_name = $display->state_event_names($get_recorded_event1['id'],$type_eves);
												}
												
												if(($get_recorded_event1['date']!="" && $get_recorded_event1['date']!='0000-00-00') || ($get_recorded_event1['state_id']!="" && $get_recorded_event1['state_id']!=0))
												{
													if($get_recorded_event1['date']!="" && $get_recorded_event1['date']!='0000-00-00')
													{
														if($event_name!="")
														{
															if(strlen($event_name)>=9)
															{
																echo date('m.d.y',strtotime($get_recorded_event1['date'])).', '.substr($event_name,0,6)."...";
															}
															else
															{
																echo date('m.d.y',strtotime($get_recorded_event1['date'])).', '.$event_name;
															}
														}
														else
														{
															echo date('m.d.y',strtotime($get_recorded_event1['date']));
														}
													}
													else
													{
														if($event_name!="")
														{
															if(strlen($event_name)>=19)
															{
																echo substr($event_name,0,16)."...";
															}
															else
															{
																echo $event_name;
															}
														}
													}
												}					
			?>
											</div>
			<?php
												$get_profile_type = $display->types_of_search('community_event',$get_recorded_event1['id']);
												if(!empty($get_profile_type) && $get_profile_type!=Null)
												{
													$res_profile_type = mysql_fetch_assoc($get_profile_type);
													if($res_profile_type['type_name']!=""){
													?>
														<span style="font-size:13px; font-weight:normal;">
														<?php 
														if(strlen($res_profile_type['type_name'])>26){
															echo substr($res_profile_type['type_name'],0,24)."...";
														}else{
															echo $res_profile_type['type_name'];
														}
														if($res_profile_type['subtype_name']!="" && strlen($res_profile_type['type_name']<26)){
															$type_length = strlen($res_profile_type['type_name']);
															if($type_length + strlen($res_profile_type['subtype_name'])>22){
																echo ", ".substr($res_profile_type['subtype_name'],0,22 - $type_length)."...";
															}else{
																echo ", ".$res_profile_type['subtype_name'];
															}
															
														}
														/*echo $res_profile_type['type_name'];
														if($res_profile_type['subtype_name']!=""){
															echo ", ".$res_profile_type['subtype_name'];
														}*/
														?>
														</span>
												<?php
													}
												}
											} ?> </div></div>
					</div>
					<?php
					}
					
					}
					?>
					<input type="hidden" id="recorded_event_video1" value="<?php echo $recorded_event_video1;?>"/>
					<input type="hidden" id="recorded_event_song1" value="<?php echo $recorded_event_song1;?>"/>
					<script type="text/javascript">
					$("document").ready(function(){
						var a = document.getElementById("recorded_event_video1").value;
						var b = document.getElementById("recorded_event_song1").value;
						if((a =="" || a ==" ") && (b=="" || b==" "))
						{
							$("#profileFeaturedevent").css({"display":"none"});
						}
					});
					
					</script>
				  </div>
				<?php
				//}
				
			}
		}
		?>
    </div>
<?php 
//}

 //if($mediaData[5][0]=="friends"){ ?>    
   <!-- <div id="promotionsTab" class="hiddenBlock<?php //echo $mediaData[5][0];?>">-->
<!--<div id="promotionsTab" class="hiddenBlock6">
	<?php
		/*if($get_media_Info['artist_view_selected'] !="")
		{
		?>
			<div id="mediaTab" class="hiddenBlock6">
				<div id="profileFeatured">
					<div class="playall">
						Play All
					</div>
					<a href="">
						<img src="images/profile/play.png" width="35" height="23" onmouseover="this.src='images/profile/play-over.png'" onmouseout="this.src='images/profile/play.png'" />
					</a>
					<a href="">
						<img src="images/profile/add.png" width="27" height="23" onmouseover="this.src='images/profile/add-over.png'" onmouseout="this.src='images/profile/add.png'" />
					</a>
				</div>
				<h2><a href="#">Artists</a></h2>
				<div class="rowItem">
					<?php
						for($exn_pr=0;$exn_pr<= count($all_art);$exn_pr++)
						{
							$get_artists = $new_profile_class_obj->get_All_Artist($all_art[$exn_pr]);
							while($rowa_pro=mysql_fetch_assoc($get_artists))
							{
					?>
								<div class="tabItem"> 
									<img src="<?php if(isset($rowa_pro['image_name']) && $rowa_pro['image_name']!="") { echo 'https://artjcropprofile.s3.amazonaws.com/'.$rowa_pro['image_name'] ; } else { echo 'https://artjcropprofile.s3.amazonaws.com/Noimage.png'; } ?>" width="200" height="200" />
									<h3><?php echo $rowa_pro['name']; ?></h3>
									<div class="player">
										<img src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" />
										<img src="images/profile/add.gif"  onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'"/>
										<img src="images/profile/download.gif" onmouseover="this.src='images/profile/download-over.gif'" onmouseout="this.src='images/profile/download.gif'" />
									</div>
								</div>
					<?php
							}
						}
					?>
				</div>
			</div>    
		<?php
		}
		else
		{
		?>
			<div id="mediaTab" class="hiddenBlock6">
			</div>
		<?php
		}*/
		?>
</div>-->
<?php //}
	if($artist_check !=0 || $community_check !=0)
	{
?>
		<div id="promotionsTab" class="hiddenBlock7">
<?php
	}
	else
	{
?>
		<div id="servicesTab" class="hiddenBlock4">
<?php
	}

	if($artist_check !=0 || $community_check !=0)
	{
		if($get_media_Info['artist_view_selected'] !="" || $get_media_Info['community_view_selected'] !="")
		{

				if($artist_check !=0 || $community_check !=0)
				{
			?>
					<div id="promotionsTab" class="hiddenBlock7">
			<?php
				}
				else
				{
			?>
					<div id="servicesTab" class="hiddenBlock4">
			<?php
				}
			?> 
				<div id="profileFeaturedfriend">
					<div class="playall">
						Play All
					</div>
					<?php
					if($artist_check !=0 || $community_check !=0)
					{
					?>
						<a title="Play all of the media on this profile page." href="javascript:void(0);">
							<img src="images/profile/play.png" width="35" height="23" onmouseover="this.src='images/profile/play-over.png'" onmouseout="this.src='images/profile/play.png'" onclick="PlayAll_featured_media_events('art_friend_song','art_friend_video','com_friend_song','com_friend_video')"/>
						</a>
						<a title="Add all of the media on this profile page to your playlist without stopping your current file." href="javascript:void(0);">
							<img src="images/profile/add.png" width="27" height="23" onmouseover="this.src='images/profile/add-over.png'" onmouseout="this.src='images/profile/add.png'" onclick="PlayAll_featured_media_events('art_friend_song','art_friend_video','com_friend_song','com_friend_video')"/>
						</a>
					<?php
					}
					elseif($artist_event_check !=0 || $community_event_check !=0)
					{
					?>
						<a title="Play all of the media on this events page." href="javascript:void(0);">
							<img src="images/profile/play.png" width="35" height="23" onmouseover="this.src='images/profile/play-over.png'" onmouseout="this.src='images/profile/play.png'" onclick="PlayAll_featured_media_events('art_friend_song','art_friend_video','com_friend_song','com_friend_video')"/>
						</a>
						<a title="Add all of the media on this events page to your playlist without stopping your current file." href="javascript:void(0);">
							<img src="images/profile/add.png" width="27" height="23" onmouseover="this.src='images/profile/add-over.png'" onmouseout="this.src='images/profile/add.png'" onclick="PlayAll_featured_media_events('art_friend_song','art_friend_video','com_friend_song','com_friend_video')"/>
						</a>
					<?php
					}
					elseif($artist_project_check !=0 || $community_project_check !=0)
					{
					?>
						<a title="Play all of the media on this projects page." href="javascript:void(0);">
							<img src="images/profile/play.png" width="35" height="23" onmouseover="this.src='images/profile/play-over.png'" onmouseout="this.src='images/profile/play.png'" onclick="PlayAll_featured_media_events('art_friend_song','art_friend_video','com_friend_song','com_friend_video')"/>
						</a>
						<a title="Add all of the media on this projects page to your playlist without stopping your current file." href="javascript:void(0);">
							<img src="images/profile/add.png" width="27" height="23" onmouseover="this.src='images/profile/add-over.png'" onmouseout="this.src='images/profile/add.png'" onclick="PlayAll_featured_media_events('art_friend_song','art_friend_video','com_friend_song','com_friend_video')" />
						</a>
					<?php
					}
					?>
				</div>
				<div id="profileFeatured">
			
			<?php //echo $getuser['featured_media'].$getuser['media_id'];
			
			if($artist_check !=0 || $artist_event_check !=0 || $artist_project_check !=0  )
			{
				$table_type = "artist";
			}
			if($community_check !=0 || $community_event_check !=0 || $community_project_check !=0  )
			{
				$table_type = "community";
			}
			if($getuser['featured_media'] == "Gallery" && $getuser['media_id']!=0)
			{
				$find_whether_regis=$new_profile_class_obj->get_count_Gallery_at_register($getuser["$table_type".'_id'],$getuser['media_id']);
				//var_dump($find_whether_regis);
				if($find_whether_regis["count(*)"]>1)
				{
					$getimages=$new_profile_class_obj->get_featured_media_gallery_at_register($getuser['media_id'],$table_type);
					$feature_image_name = array();
					$feature_image_title = array();
					$feature_count = 0;
					while($get_image = mysql_fetch_assoc($getimages))
					{
						$feature_image_name[$feature_count] = $get_image['image_name'];
						$feature_image_title[$feature_count] = $get_image['image_title'];
						$feature_count++;
					}
					$feature_big_img = $feature_image_name[0];
					$featured_gal_img = implode(",", $feature_image_name);
					$featured_gal_img_title = implode(",", $feature_image_title);
					$feature_org_path = "https://reggallery.s3.amazonaws.com/";
					$feature_thum_path = "https://reggalthumb.s3.amazonaws.com/";
				}
				else
				{
					$getimages=$new_profile_class_obj->get_featured_media_gallery($getuser['media_id']);
					$feature_image_name = array();
					$feature_image_title = array();
					$feature_count = 0;
					while($get_image = mysql_fetch_assoc($getimages))
					{
						$feature_image_name[$feature_count] = $get_image['image_name'];
						$feature_image_title[$feature_count] = $get_image['image_title'];
						$feature_count++;
					}
					$feature_big_img = $feature_image_name[0];
					$featured_gal_img = implode(",", $feature_image_name);
					$featured_gal_img_title = implode(",", $feature_image_title);
					$feature_org_path = "https://medgallery.s3.amazonaws.com/";
					$feature_thum_path = "https://medgalthumb.s3.amazonaws.com/";
				}
			?>
				<div class="fmedia">featured Media</div>
				<?php
				if($artist_check !=0 || $community_check !=0)
				{
				?>
					<a title="Play this artists featured media." href="javascript:void(0);"><img src="../images/profile/play.png" width="35" height="23" onmouseover="this.src='../images/profile/play-over.png'" onmouseout="this.src='../images/profile/play.png'" onclick="checkload('<?php echo $feature_big_img;?>','<?php echo $featured_gal_img; ?>','<?php echo $feature_thum_path; ?>','<?php echo $feature_org_path; ?>','<?php echo $featured_gal_img_title; ?>')" /></a>
					<a title="Add this artists featured media to your playlist without stopping your current file." href="javascript:void(0);"><img src="../images/profile/add.png" width="27" height="23" onmouseover="this.src='../images/profile/add-over.png'" onmouseout="this.src='../images/profile/add.png'" /></a>
			<?php
				}
				elseif($artist_event_check !=0 || $community_event_check !=0)
				{
			?>
					<a title="Play this events featured media." href="javascript:void(0);"><img src="../images/profile/play.png" width="35" height="23" onmouseover="this.src='../images/profile/play-over.png'" onmouseout="this.src='../images/profile/play.png'" onclick="checkload('<?php echo $feature_big_img;?>','<?php echo $featured_gal_img; ?>','<?php echo $feature_thum_path; ?>','<?php echo $feature_org_path; ?>','<?php echo $featured_gal_img_title; ?>')" /></a>
					<a title="Add this events featured media to your playlist without stopping your current file." href="javascript:void(0);"><img src="../images/profile/add.png" width="27" height="23" onmouseover="this.src='../images/profile/add-over.png'" onmouseout="this.src='../images/profile/add.png'" /></a>
			<?php
				}
				elseif($artist_project_check !=0 || $community_project_check !=0)
				{
			?>
					<a title="Play this projects featured media." href="javascript:void(0);"><img src="../images/profile/play.png" width="35" height="23" onmouseover="this.src='../images/profile/play-over.png'" onmouseout="this.src='../images/profile/play.png'" onclick="checkload('<?php echo $feature_big_img;?>','<?php echo $featured_gal_img; ?>','<?php echo $feature_thum_path; ?>','<?php echo $feature_org_path; ?>','<?php echo $featured_gal_img_title; ?>')" /></a>
					<a title="Add this projects featured media to your playlist without stopping your current file." href="javascript:void(0);"><img src="../images/profile/add.png" width="27" height="23" onmouseover="this.src='../images/profile/add-over.png'" onmouseout="this.src='../images/profile/add.png'" /></a>
			<?php
				}
			}
			if($getuser['featured_media'] == "Song" && $getuser['media_id']!=0)
			{
			?>
				<div class="fmedia">featured Media</div>
				<?php
				if($artist_check !=0 || $community_check !=0)
				{
				?>
					<a title="Play this artists featured media." href="javascript:void(0);"><img src="../images/profile/play.png" width="35" height="23" onmouseover="this.src='../images/profile/play-over.png'" onmouseout="this.src='../images/profile/play.png'" onclick="showPlayer('a','<?php echo $getuser['media_id'];?>','play')" /></a>
					<a title="Add this artists featured media to your playlist without stopping your current file." href="javascript:void(0);"><img src="../images/profile/add.png" width="27" height="23" onmouseover="this.src='../images/profile/add-over.png'" onmouseout="this.src='../images/profile/add.png'" onclick="showPlayer('a','<?php echo $getuser['media_id'];?>','add')" /></a>
			<?php
				}
				elseif($artist_event_check !=0 || $community_event_check !=0)
				{
			?>
					<a title="Play this events featured media." href="javascript:void(0);"><img src="../images/profile/play.png" width="35" height="23" onmouseover="this.src='../images/profile/play-over.png'" onmouseout="this.src='../images/profile/play.png'" onclick="showPlayer('a','<?php echo $getuser['media_id'];?>','play')" /></a>
					<a title="Add this events featured media to your playlist without stopping your current file." href="javascript:void(0);"><img src="../images/profile/add.png" width="27" height="23" onmouseover="this.src='../images/profile/add-over.png'" onmouseout="this.src='../images/profile/add.png'" onclick="showPlayer('a','<?php echo $getuser['media_id'];?>','add')" /></a>
			<?php
				}
				elseif($artist_project_check !=0 || $community_project_check !=0)
				{
			?>
					<a title="Play this projects featured media." href="javascript:void(0);"><img src="../images/profile/play.png" width="35" height="23" onmouseover="this.src='../images/profile/play-over.png'" onmouseout="this.src='../images/profile/play.png'" onclick="showPlayer('a','<?php echo $getuser['media_id'];?>','play')" /></a>
					<a title="Add this projects featured media to your playlist without stopping your current file." href="javascript:void(0);"><img src="../images/profile/add.png" width="27" height="23" onmouseover="this.src='../images/profile/add-over.png'" onmouseout="this.src='../images/profile/add.png'" onclick="showPlayer('a','<?php echo $getuser['media_id'];?>','add')" /></a>
			<?php
				}
			}
			?>
			<?php
			if($getuser['featured_media'] == "Video" && $getuser['media_id']!=0)
			{
			?>
				<div class="fmedia">featured Media</div>
				<?php
				if($artist_check !=0 || $community_check !=0)
				{
				?>
					<a title="Play this artists featured media." href="javascript:void(0);"><img src="../images/profile/play.png" width="35" height="23" onmouseover="this.src='../images/profile/play-over.png'" onmouseout="this.src='../images/profile/play.png'" onclick="showPlayer('v','<?php echo $getuser['media_id'];?>','playlist')" /></a>
					<a title="Add this artists featured media to your playlist without stopping your current file." href="javascript:void(0);"><img src="../images/profile/add.png" width="27" height="23" onmouseover="this.src='../images/profile/add-over.png'" onmouseout="this.src='../images/profile/add.png'" onclick="showPlayer('v','<?php echo $getuser['media_id'];?>','addvi')" /></a>
			<?php
				}
				elseif($artist_event_check !=0 || $community_event_check !=0)
				{
			?>
					<a title="Play this events featured media." href="javascript:void(0);"><img src="../images/profile/play.png" width="35" height="23" onmouseover="this.src='../images/profile/play-over.png'" onmouseout="this.src='../images/profile/play.png'" onclick="showPlayer('v','<?php echo $getuser['media_id'];?>','playlist')" /></a>
					<a title="Add this events featured media to your playlist without stopping your current file." href="javascript:void(0);"><img src="../images/profile/add.png" width="27" height="23" onmouseover="this.src='../images/profile/add-over.png'" onmouseout="this.src='../images/profile/add.png'" onclick="showPlayer('v','<?php echo $getuser['media_id'];?>','addvi')" /></a>
			<?php
				}
				elseif($artist_project_check !=0 || $community_project_check !=0)
				{
			?>
					<a title="Play this projects featured media." href="javascript:void(0);"><img src="../images/profile/play.png" width="35" height="23" onmouseover="this.src='../images/profile/play-over.png'" onmouseout="this.src='../images/profile/play.png'" onclick="showPlayer('v','<?php echo $getuser['media_id'];?>','playlist')" /></a>
					<a title="Add this projects featured media to your playlist without stopping your current file." href="javascript:void(0);"><img src="../images/profile/add.png" width="27" height="23" onmouseover="this.src='../images/profile/add-over.png'" onmouseout="this.src='../images/profile/add.png'" onclick="showPlayer('v','<?php echo $getuser['media_id'];?>','addvi')" /></a>
			<?php
				}
			}
			?>
			
		</div>
				<?php
				if(!empty($get_media_Info['artist_view_selected']) && $get_media_Info['artist_view_selected'] !='0')
				{
					$flag_af_1 = 0;
					$flag_af_2 = 0;
					
					$src_list = "https://artjcropthumb.s3.amazonaws.com/";
					
					$exp_art_id = explode(",",$get_media_Info['artist_view_selected']);
					
					if($community_check!=0)
					{
						$sql = mysql_query("SELECT * FROM general_user WHERE community_id='".$community_check['community_id']."'");
						$ans = mysql_fetch_assoc($sql);
						
						if($ans['artist_id']!=0)
						{
							//echo "SELECT * FROM general_artist WHERE artist_id='".$ans['artist_id']."' AND status=0 AND active=0 AND del_status=0";
							$sql_c = mysql_query("SELECT * FROM general_artist WHERE artist_id='".$ans['artist_id']."' AND status=0 AND active=0 AND del_status=0");
							if($sql_c!="" && $sql_c!=Null)
							{
								if(mysql_num_rows($sql_c)>0)
								{
									$exp_art_id[] = $ans['artist_id'];
									$arts_ids = $ans['artist_id'];
								}
							}
						}
					}
					//var_dump($exp_art_id);
					//$exp_art_id = array_unique($exp_art_id);
					
					$find_frd = $friend_obj_new->find_friend($match);
					if($find_frd=='a')
					{
						$p_f_aid = $friend_obj_new->found_artist($match);
						$p_f_ac_id = $friend_obj_new->found_ageneral($p_f_aid['artist_id']);
					}
					elseif($find_frd=='c')
					{
						$p_f_cid = $friend_obj_new->found_community($match);
						$p_f_ac_id = $friend_obj_new->found_cgeneral($p_f_cid['community_id']);
					}
					
					for($art_count = 0;$art_count< count($exp_art_id); $art_count++)
					{
						$chk_art_f = "";
						$chk_art_gf = $friend_obj_new->general_user_sec_art($exp_art_id[$art_count]);
						
						if(!empty($chk_art_gf))
						{
							if($p_f_ac_id['general_user_id']==$chk_art_gf['general_user_id'])
							{
								$chk_art_f = 'accept_present';
							}
							else
							{
								$chk_art_f = $friend_obj_new->friend_apresent($p_f_ac_id['general_user_id'],$chk_art_gf['general_user_id']);
							}
						}
						
						if($exp_art_id[$art_count]=="" || empty($chk_art_f) || $chk_art_f!='accept_present')
						{
							continue;
						}
						else
						{
							$get_friend_artist = $new_profile_class_obj->get_artist_friends($exp_art_id[$art_count]);
							if($get_friend_artist!="" && $get_friend_artist!=Null)
							{
								if(mysql_num_rows($get_friend_artist)>0 && $flag_af_1==0)
								{
									$art_tab_chk = $friend_obj_new->get_all_friends($p_f_ac_id['general_user_id']);
									if($community_check!=0)
									{
										if($art_tab_chk!="")
										{
											if(mysql_num_rows($art_tab_chk)>0)
											{
								?>
												<h2><a href="#">Artists</a></h2>
												<div class="rowItem">
								<?php
												$flag_af_1 = 1;
											}
											elseif(isset($arts_ids))
											{
								?>
												<h2><a href="#">Artists</a></h2>
												<div class="rowItem">
								<?php
												$flag_af_1 = 1;			
											}
										}
										elseif(isset($arts_ids))
										{
								?>
											<h2><a href="#">Artists</a></h2>
											<div class="rowItem">
								<?php
											$flag_af_1 = 1;			
										}
									}
									elseif($artist_check!=0 && $art_tab_chk!="")
									{
										if(mysql_num_rows($art_tab_chk)>0)
										{
								?>
										<h2><a href="#">Artists</a></h2>
											<div class="rowItem">
								<?php
										$flag_af_1 = 1;	
										}
									}
								}
							}
						}
					}
					/* end($exp_art_id);
					$last_id = key($exp_art_id);
					$org_counts = count($exp_art_id); */
					
					$dum_arts_view = array();
					
					$find_frd = $friend_obj_new->find_friend($match);
					if($find_frd=='a')
					{
						$p_f_aid = $friend_obj_new->found_artist($match);
						$p_f_ac_id = $friend_obj_new->found_ageneral($p_f_aid['artist_id']);
					}
					elseif($find_frd=='c')
					{
						$p_f_cid = $friend_obj_new->found_community($match);
						$p_f_ac_id = $friend_obj_new->found_cgeneral($p_f_cid['community_id']);
					}
						
					for($art_count = 0;$art_count<count($exp_art_id);$art_count++)
					{
						$chk_art_f = "";
						$chk_art_gf = $friend_obj_new->general_user_sec_art($exp_art_id[$art_count]);
						
						if(!empty($chk_art_gf))
						{
							if($p_f_ac_id['general_user_id']==$chk_art_gf['general_user_id'])
							{
								$chk_art_f = 'accept_present';
							}
							else
							{
								$chk_art_f = $friend_obj_new->friend_apresent($p_f_ac_id['general_user_id'],$chk_art_gf['general_user_id']);
							}
						}
						
						//if($exp_art_id[$art_count]=="")
						if($exp_art_id[$art_count]=="" || in_array($exp_art_id[$art_count],$dum_arts_view) || empty($chk_art_f) || $chk_art_f!='accept_present')
						{
							//continue;
							/* $art_count = $art_count + 1;
							if($exp_art_id[$art_count]=="")
							{
								$org_counts = $org_counts + 1;
							}
							else
							{
								$art_count = $art_count - 1;
								
							}
							echo $art_count; */
						}
						else
						{
							$dum_arts_view[] = $exp_art_id[$art_count];
							$get_friend_artist = $new_profile_class_obj->get_artist_friends($exp_art_id[$art_count]);
							$find_mem_butts_frd = $display->get_member_or_not($chk_art_gf['general_user_id']);
							while($friend_art = mysql_fetch_assoc($get_friend_artist))
							{
							?>
								<div class="tabItem"><a href="/<?php echo $friend_art['profile_url'];?>"><img class="imgs_pro" width="200" height="200" src="<?php echo $src_list . $friend_art['listing_image_name'];?>"/></a>
								<div class="both_wrap_per">
								<div class="player">
									<?php
									if($friend_art['featured_media']=="Gallery" && $friend_art['media_id']!=0)
									{
										$feature_reg_big_img = "";
										$all_register_images = "";
										$feature_reg_thum_path = "";
										$feature_reg_org_path = "";
										$featured_reg_gal_img_title = "";
							
										if($friend_art['featured_media_table']!="" &&($friend_art['featured_media_table']=="general_artist_gallery_list" || $friend_art['featured_media_table']=="general_community_gallery_list"))
										{
											$get_register_featured_gallery = $new_profile_class_obj ->get_register_featured_media_gallery($friend_art['media_id'],$friend_art['featured_media_table']);
											$all_register_images ="";
											$feature_reg_big_img ="";
											$featured_reg_gal_img_title ="";
											for($cou_i=0;$cou_i<count($get_register_featured_gallery);$cou_i++)
											{
												$feature_reg_big_img = $get_register_featured_gallery[0]['image_name'];
												$all_register_images = $all_register_images.",".$get_register_featured_gallery[$cou_i]['image_name'];
												$featured_reg_gal_img_title = $featured_reg_gal_img_title .",".$get_register_featured_gallery[$cou_i]['image_title'];
											}
											$all_register_images = ltrim($all_register_images,',');
											$featured_reg_gal_img_title = ltrim($featured_reg_gal_img_title,',');
											$feature_reg_org_path = "https://reggallery.s3.amazonaws.com/";
											$feature_reg_thum_path = "https://reggalthumb.s3.amazonaws.com/";
										}
										else if($friend_art['featured_media_table']!="" && $friend_art['featured_media_table']=="general_media"){
											$get_register_featured_gallery = $new_profile_class_obj ->get_featured_gallery_images($friend_art['media_id']);
											$all_register_images ="";
											$feature_reg_big_img ="";
											$featured_reg_gal_img_title ="";
											for($cou_i=0;$cou_i<count($get_register_featured_gallery);$cou_i++)
											{
												$feature_reg_big_img = $get_register_featured_gallery[0]['image_name'];
												$all_register_images = $all_register_images.",".$get_register_featured_gallery[$cou_i]['image_name'];
												$featured_reg_gal_img_title = $featured_reg_gal_img_title .",".$get_register_featured_gallery[$cou_i]['image_title'];
											}
											$all_register_images = ltrim($all_register_images,',');
											$featured_reg_gal_img_title = ltrim($featured_reg_gal_img_title,',');
											$feature_reg_org_path = "https://medgallery.s3.amazonaws.com/";
											$feature_reg_thum_path = "https://medgalthumb.s3.amazonaws.com/";
										}
									?>
									<a href="javascript:void(0);"><img src="../images/profile/play.gif" onmouseover="this.src='../images/profile/play-over.gif'" onmouseout="this.src='../images/profile/play.gif'" onclick="checkload('<?php echo $feature_reg_big_img;?>','<?php echo $all_register_images; ?>','<?php echo $feature_reg_thum_path; ?>','<?php echo $feature_reg_org_path; ?>','<?php echo $featured_reg_gal_img_title; ?>')" title="Play file now."/></a>
									<!--<a href="javascript:void(0);"><img src="../images/profile/add.gif"  onmouseover="this.src='../images/profile/add-over.gif'" onmouseout="this.src='../images/profile/add.gif'" title="Add file to the bottom of your playlist without stopping current media."/></a>-->
									<?php
									if($find_mem_butts_frd != null && $find_mem_butts_frd['general_user_id'] != null && ($find_mem_butts_frd['expiry_date'] >$date || $find_mem_butts_frd['lifetime']==1))
									{
										if($friend_art['type']=='fan_club_yes')
										{
											if($_SESSION['login_email']==NULL || $_SESSION['login_id']!=$chk_art_gf['general_user_id'])
											{
									?>
											<a class="fancybox fancybox.ajax" title="<?php if($friend_art['type']=="fan_club_yes") { echo "Purchase a fan club membership to this users exclusive media."; } elseif($friend_art['type']=="for_sale") { echo "Purchase this projects media."; } ?>" href="javascript:void(0);" onClick="fancy_login_art_coms('<?php echo $friend_art['profile_url']; ?>')" ><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>
									<?php
											}
										}
									}
										?>
											<a class="fancybox fancybox.ajax" style="display:none;" href="fan_club.php?data=<?php echo $friend_art['profile_url']; ?>" id="fan_club_pro_ca<?php echo $friend_art['profile_url']; ?>">
											<?php if($friend_art['type']=="fan_club_yes") { echo "Fan Club"; } elseif($friend_art['type']=="for_sale") { echo "Download"; } ?></a>
											<script type="text/javascript">
											$(document).ready(function() {
													$("#fan_club_pro_ca<?php echo $friend_art['profile_url']; ?>").fancybox({
													helpers : {
															media : {},
															buttons : {},
															title : null,
														},
													});
											});
											</script>
										<?php
									}
									else if($friend_art['featured_media']=="Song" && $friend_art['media_id']!=0)
									{
										if($friend_art['featured_media_table']=="general_artist_audio" || $friend_art['featured_media_table']=="general_community_audio")
										{
											?>
												<a href="javascript:void(0);"><img src="../images/profile/play.gif" onmouseover="this.src='../images/profile/play-over.gif'" onmouseout="this.src='../images/profile/play.gif'" onclick="showPlayer_reg('a','<?php echo $friend_art['media_id'];?>','play_reg','<?php echo $friend_art['featured_media_table'];?>')" title="Play file now."/></a>
												<a href="javascript:void(0);"><img src="../images/profile/add.gif"  onmouseover="this.src='../images/profile/add-over.gif'" onmouseout="this.src='../images/profile/add.gif'" onclick="showPlayer_reg('a','<?php echo $friend_art['media_id'];?>','add_reg','<?php echo $friend_art['featured_media_table'];?>')" title="Add file to the bottom of your playlist without stopping current media."/></a>
											<?php
										}
										else{
											?>
												<a href="javascript:void(0);"><img src="../images/profile/play.gif" onmouseover="this.src='../images/profile/play-over.gif'" onmouseout="this.src='../images/profile/play.gif'" onclick="showPlayer('a','<?php echo $friend_art['media_id'];?>','play')" title="Play file now."/></a>
												<a href="javascript:void(0);"><img src="../images/profile/add.gif"  onmouseover="this.src='../images/profile/add-over.gif'" onmouseout="this.src='../images/profile/add.gif'" onclick="showPlayer('a','<?php echo $friend_art['media_id'];?>','add')" title="Add file to the bottom of your playlist without stopping current media."/></a>
											<?php
										}
										if($find_mem_butts_frd != null && $find_mem_butts_frd['general_user_id'] != null && ($find_mem_butts_frd['expiry_date'] >$date || $find_mem_butts_frd['lifetime']==1))
									{
										if($friend_art['type']=='fan_club_yes')
										{	
											if($_SESSION['login_email']==NULL || $_SESSION['login_id']!=$chk_art_gf['general_user_id'])
											{
												
									?>
											<a title="<?php if($friend_art['type']=="fan_club_yes") { echo "Purchase a fan club membership to this users exclusive media."; } elseif($friend_art['type']=="for_sale") { echo "Purchase this projects media."; } ?>" href="javascript:void(0);" onClick="fancy_login_art_coms('<?php echo $friend_art['profile_url']; ?>')" ><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>
									<?php
											}
										}
									}
									?>
									<a class="fancybox fancybox.ajax" style="display:none;" href="fan_club.php?data=<?php echo $friend_art['profile_url']; ?>" id="fan_club_pro_ca<?php echo $friend_art['profile_url']; ?>">
											<?php if($friend_art['type']=="fan_club_yes") { echo "Fan Club"; } elseif($friend_art['type']=="for_sale") { echo "Download"; } ?></a>
											<script type="text/javascript">
											$(document).ready(function() {
													$("#fan_club_pro_ca<?php echo $friend_art['profile_url']; ?>").fancybox({
													helpers : {
															media : {},
															buttons : {},
															title : null,
														},
													});
											});
											</script>
											<?php
										if($friend_art['media_id']!="" && $friend_art['media_id']!=" " && $friend_art['media_id']!=0){
											$art_friend_song = $art_friend_song .",". $friend_art['media_id'];
										}
										?>
										<a href="addtocart.php?seller_id=<?php echo $get_media_info_down_gal['general_user_id'];?>&media_id=<?php echo $get_media_info_down_gal['id'];?>&buyer_id=<?php echo $_SESSION['gen_user_id'];?>&title=<?php echo str_replace("'","\'",$get_media_info_down_gal['title']);?>&image_name=<?php echo $song_image_name_songartist_fri;?>" id="download_songartistfri<?php echo $art_count;?>" style="display:none;"></a>
										<script type="text/javascript">
										$(document).ready(function() {
												$("#download_songartistfri<?php echo $art_count;?>").fancybox({
												'height'			: '75%',
												'width'				: '69%',
												'transitionIn'		: 'none',
												'transitionOut'		: 'none',
												'type'				: 'iframe'
												});
										});
										</script>
										<?php
									}
									else if($friend_art['featured_media']=="Video" && $friend_art['media_id']!=0)
									{
										if($friend_art['featured_media_table']=="general_artist_video" || $friend_art['featured_media_table']=="general_community_video")
										{
											?>
												<a href="javascript:void(0);"><img src="../images/profile/play.gif" onmouseover="this.src='../images/profile/play-over.gif'" onmouseout="this.src='../images/profile/play.gif'" onclick="showPlayer_reg('v','<?php echo $friend_art['media_id'];?>','playlist_reg','<?php echo $friend_art['featured_media_table'];?>')" title="Play file now."/></a>
												<a href="javascript:void(0);"><img src="../images/profile/add.gif"  onmouseover="this.src='../images/profile/add-over.gif'" onmouseout="this.src='../images/profile/add.gif'" onclick="showPlayer_reg('v','<?php echo $friend_art['media_id'];?>','addvi_reg','<?php echo $friend_art['featured_media_table'];?>')" title="Add file to the bottom of your playlist without stopping current media."/></a>
											<?php
										}else{
											?>
												<a href="javascript:void(0);"><img src="../images/profile/play.gif" onmouseover="this.src='../images/profile/play-over.gif'" onmouseout="this.src='../images/profile/play.gif'" onclick="showPlayer('v','<?php echo $friend_art['media_id'];?>','playlist')" title="Play file now."/></a>
												<a href="javascript:void(0);"><img src="../images/profile/add.gif"  onmouseover="this.src='../images/profile/add-over.gif'" onmouseout="this.src='../images/profile/add.gif'" onclick="showPlayer('v','<?php echo $friend_art['media_id'];?>','addvi')" title="Add file to the bottom of your playlist without stopping current media."/></a>
											<?php
										}
										if($find_mem_butts_frd != null && $find_mem_butts_frd['general_user_id'] != null && ($find_mem_butts_frd['expiry_date'] >$date || $find_mem_butts_frd['lifetime']==1))
									{
										if($friend_art['type']=='fan_club_yes')
										{	
											if($_SESSION['login_email']==NULL || $_SESSION['login_id']!=$chk_art_gf['general_user_id'])
											{
									?>
											<a title="<?php if($friend_art['type']=="fan_club_yes") { echo "Purchase a fan club membership to this users exclusive media."; } elseif($friend_art['type']=="for_sale") { echo "Purchase this projects media."; } ?>" href="javascript:void(0);" onClick="fancy_login_art_coms('<?php echo $friend_art['profile_url']; ?>')" ><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>
									<?php
											}
										}
									}
									?>
									<a class="fancybox fancybox.ajax" style="display:none;" href="fan_club.php?data=<?php echo $friend_art['profile_url']; ?>" id="fan_club_pro_ca<?php echo $friend_art['profile_url']; ?>">
											<?php if($friend_art['type']=="fan_club_yes") { echo "Fan Club"; } elseif($friend_art['type']=="for_sale") { echo "Download"; } ?></a>
											<script type="text/javascript">
											$(document).ready(function() {
													$("#fan_club_pro_ca<?php echo $friend_art['profile_url']; ?>").fancybox({
													helpers : {
															media : {},
															buttons : {},
															title : null,
														},
													});
											});
											</script>
											<?php
										if($friend_art['media_id']!="" && $friend_art['media_id']!=" " && $friend_art['media_id']!=0){
											$art_friend_video = $art_friend_video .",". $friend_art['media_id'];
										}
										?>
										<a href="addtocart.php?seller_id=<?php echo $get_media_info_down_gal['general_user_id'];?>&media_id=<?php echo $get_media_info_down_gal['id'];?>&buyer_id=<?php echo $_SESSION['gen_user_id'];?>&title=<?php echo str_replace("'","\'",$get_media_info_down_gal['title']);?>&image_name=<?php echo $image_name_artist_friend;?>" id="download_videoartistfri<?php echo $art_count;?>" style="display:none;"></a>
										<script type="text/javascript">
										$(document).ready(function() {
												$("#download_videoartistfri<?php echo $art_count;?>").fancybox({
												'height'			: '75%',
												'width'				: '69%',
												'transitionIn'		: 'none',
												'transitionOut'		: 'none',
												'type'				: 'iframe'
												});
										});
										</script>
										<?php
									}
									else{
									if($find_mem_butts_frd != null && $find_mem_butts_frd['general_user_id'] != null && ($find_mem_butts_frd['expiry_date'] >$date || $find_mem_butts_frd['lifetime']==1))
									{
										if($friend_art['type']=='fan_club_yes')
										{	
											if($_SESSION['login_email']==NULL || $_SESSION['login_id']!=$chk_art_gf['general_user_id'])
											{
									?>
											<a title="<?php if($friend_art['type']=="fan_club_yes") { echo "Purchase a fan club membership to this users exclusive media."; } elseif($friend_art['type']=="for_sale") { echo "Purchase this projects media."; } ?>" href="javascript:void(0);" onClick="fancy_login_art_coms('<?php echo $friend_art['profile_url']; ?>')" ><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>
									<?php
											}
										}
									}
								  ?>
									<a class="fancybox fancybox.ajax" style="display:none;" href="fan_club.php?data=<?php echo $friend_art['profile_url']; ?>" id="fan_club_pro_ca<?php echo $friend_art['profile_url']; ?>">
											<?php if($friend_art['type']=="fan_club_yes") { echo "Fan Club"; } elseif($friend_art['type']=="for_sale") { echo "Download"; } ?></a>
											<script type="text/javascript">
											$(document).ready(function() {
													$("#fan_club_pro_ca<?php echo $friend_art['profile_url']; ?>").fancybox({
													helpers : {
															media : {},
															buttons : {},
															title : null,
														},
													});
											});
											</script>
									<!--<a href="javascript:void(0);"><img src="../images/profile/play.gif" onmouseover="this.src='../images/profile/play-over.gif'" onmouseout="this.src='../images/profile/play.gif'" /></a>
									<a href="javascript:void(0);"><img src="../images/profile/add.gif"  onmouseover="this.src='../images/profile/add-over.gif'" onmouseout="this.src='../images/profile/add.gif'"/></a>-->
									<?php
									}
									?>
									<!--<img onmouseout="this.src='images/profile/download.gif'" onmouseover="this.src='images/profile/download-over.gif'" src="images/profile/download.gif">-->
									
								  </div>
								 <div class="type_subtype"><a href="/<?php echo $friend_art['profile_url'];?>">
								 <?php if(strlen($friend_art['name']>39)){
									echo substr($friend_art['name'],0,37)."...";
								 }else { echo $friend_art['name']; } ?></a><?php if(isset($friend_art['artist_id']))
											{
												$get_profile_type = $display->types_of_search('artist',$friend_art['artist_id']);
												if(!empty($get_profile_type) && $get_profile_type!=Null)
												{
													$res_profile_type = mysql_fetch_assoc($get_profile_type);
													if($res_profile_type['type_name']!=""){
													?>
														<br/><span style="font-size:13px; font-weight:normal;">
														<?php 
														if(strlen($res_profile_type['type_name'])>26){
															echo substr($res_profile_type['type_name'],0,24)."...";
														}else{
															echo $res_profile_type['type_name'];
														}
														if($res_profile_type['subtype_name']!="" && strlen($res_profile_type['type_name']<26)){
															$type_length = strlen($res_profile_type['type_name']);
															if($type_length + strlen($res_profile_type['subtype_name'])>22){
																echo ", ".substr($res_profile_type['subtype_name'],0,22 - $type_length)."...";
															}else{
																echo ", ".$res_profile_type['subtype_name'];
															}
															
														}
														/*echo $res_profile_type['type_name'];
														if($res_profile_type['subtype_name']!=""){
															echo ", ".$res_profile_type['subtype_name'];
														}*/
														?>
														</span>
												<?php
													}
												}
											}
											elseif(isset($friend_art['community_id']))
											{
												$get_profile_type = $display->types_of_search('community',$friend_art['community_id']);
												if(!empty($get_profile_type) && $get_profile_type!=Null)
												{
													$res_profile_type = mysql_fetch_assoc($get_profile_type);
													if($res_profile_type['type_name']!=""){
													?>
														<br/><span style="font-size:13px; font-weight:normal;">
														<?php 
														if(strlen($res_profile_type['type_name'])>26){
															echo substr($res_profile_type['type_name'],0,24)."...";
														}else{
															echo $res_profile_type['type_name'];
														}
														if($res_profile_type['subtype_name']!="" && strlen($res_profile_type['type_name']<26)){
															$type_length = strlen($res_profile_type['type_name']);
															if($type_length + strlen($res_profile_type['subtype_name'])>22){
																echo ", ".substr($res_profile_type['subtype_name'],0,22 - $type_length)."...";
															}else{
																echo ", ".$res_profile_type['subtype_name'];
															}
															
														}
														/*echo $res_profile_type['type_name'];
														if($res_profile_type['subtype_name']!=""){
															echo ", ".$res_profile_type['subtype_name'];
														}*/
														?>
														</span>
												<?php
													}
												}
											} ?> </div></div>
								</div>
							<?php
							}
						}
					}
					?>
					<input type="hidden" id="art_friend_song" value="<?php echo $art_friend_song;?>"/>
					<input type="hidden" id="art_friend_video" value="<?php echo $art_friend_video;?>"/>
					<script type="text/javascript">
					$("document").ready(function(){
						var a = document.getElementById("art_friend_song").value;
						var b = document.getElementById("art_friend_video").value;
						if((a =="" || a ==" ") && (b=="" || b==" "))
						{
							$("#profileFeaturedfriend").css({"display":"none"});
						}
					});
					
					</script>
					<?php
					for($art_count = 0;$art_count< count($exp_art_id); $art_count++)
					{
						if($exp_art_id[$art_count]=="")
						{
							continue;
						}
						else
						{
							$get_friend_artist = $new_profile_class_obj->get_artist_friends($exp_art_id[$art_count]);
							if($get_friend_artist!="" && $get_friend_artist!=Null)
							{
								if(mysql_num_rows($get_friend_artist)>0 && $flag_af_2==0)
								{
								?>
										</div>
								<?php
									$flag_af_2 = 1;
								}
							}
						}
					}
				
				}
				if($get_media_Info['community_view_selected'] !="" && $get_media_Info['community_view_selected']!='0')
				{
					$flag_cf_1 = 0;
					$flag_cf_2 = 0;
					
					$src_list = "https://comjcropthumb.s3.amazonaws.com/";
					$exp_com_id = explode(",",$get_media_Info['community_view_selected']);
					
					if($artist_check!=0)
					{
						$sql = mysql_query("SELECT * FROM general_user WHERE artist_id='".$artist_check['artist_id']."'");
						$ans = mysql_fetch_assoc($sql);
						
						if($ans['community_id']!=0)
						{
							$sql_c = mysql_query("SELECT * FROM general_community WHERE community_id='".$ans['community_id']."' AND status=0 AND active=0 AND del_status=0");
							if($sql_c!="" && $sql_c!=Null)
							{
								if(mysql_num_rows($sql_c)>0)
								{
									$exp_com_id[] = $ans['community_id'];
									$coms_ids = $ans['community_id'];
								}
							}
						}
					}
					
					$find_frd = $friend_obj_new->find_friend($match);
					if($find_frd=='a')
					{
						$p_f_aid = $friend_obj_new->found_artist($match);
						$p_f_ac_id = $friend_obj_new->found_ageneral($p_f_aid['artist_id']);
					}
					elseif($find_frd=='c')
					{
						$p_f_cid = $friend_obj_new->found_community($match);
						$p_f_ac_id = $friend_obj_new->found_cgeneral($p_f_cid['community_id']);
					}
					
					//$exp_com_id = array_unique($exp_com_id);
					for($com_count = 0;$com_count< count($exp_com_id); $com_count++)
					{
						$chk_com_f = '';
						$chk_com_gf = $friend_obj_new->general_user_sec_com($exp_com_id[$com_count]);
						if(!empty($chk_com_gf))
						{
							if($p_f_ac_id['general_user_id']==$chk_com_gf['general_user_id'])
							{
								$chk_com_f = 'accept_present';
							}
							else
							{
								$chk_com_f = $friend_obj_new->friend_apresent($p_f_ac_id['general_user_id'],$chk_com_gf['general_user_id']);
							}
						}
						
						if($exp_com_id[$com_count]=="" || empty($chk_com_f) || $chk_com_f!='accept_present')
						{
							continue;
						}
						else
						{
							$get_friend_community = $new_profile_class_obj->get_community_friends($exp_com_id[$com_count]);
							if($get_friend_community!="" && $get_friend_community!=Null)
							{
								if(mysql_num_rows($get_friend_community)>0 && $flag_cf_1==0)
								{
									$art_tab_chk = $friend_obj_new->get_all_friends($p_f_ac_id['general_user_id']);
									if($artist_check!=0)
									{
										if($art_tab_chk!="")
										{
											if(mysql_num_rows($art_tab_chk)>0)
											{
							?>
												<h2><a href="#">Companies</a></h2>
												<div class="rowItem">
							<?php
												$flag_cf_1 = 1;
											}
											elseif(isset($coms_ids))
											{
							?>
												<h2><a href="#">Companies</a></h2>
												<div class="rowItem">
							<?php
												$flag_cf_1 = 1;			
											}
										}
										elseif(isset($coms_ids))
										{
							?>
											<h2><a href="#">Companies</a></h2>
											<div class="rowItem">
							<?php
											$flag_cf_1 = 1;			
										}
									}
									elseif($community_check!=0 && $art_tab_chk!="")
									{
										if(mysql_num_rows($art_tab_chk)>0)
										{
								?>
											<h2><a href="#">Companies</a></h2>
											<div class="rowItem">
							<?php
											$flag_cf_1 = 1;
										}
									}
								}
							}
						}
					}
					
					$find_frd = $friend_obj_new->find_friend($match);
					if($find_frd=='a')
					{
						$p_f_aid = $friend_obj_new->found_artist($match);
						$p_f_ac_id = $friend_obj_new->found_ageneral($p_f_aid['artist_id']);
					}
					elseif($find_frd=='c')
					{
						$p_f_cid = $friend_obj_new->found_community($match);
						$p_f_ac_id = $friend_obj_new->found_cgeneral($p_f_cid['community_id']);
					}
					
					$dump_coms_view = array();
					for($com_count = 0;$com_count< count($exp_com_id); $com_count++)
					{
						$chk_com_f = '';
						$chk_com_gf = $friend_obj_new->general_user_sec_com($exp_com_id[$com_count]);
						if(!empty($chk_com_gf))
						{
							if($p_f_ac_id['general_user_id']==$chk_com_gf['general_user_id'])
							{
								$chk_com_f = 'accept_present';
							}
							else
							{
								$chk_com_f = $friend_obj_new->friend_apresent($p_f_ac_id['general_user_id'],$chk_com_gf['general_user_id']);
							}
						} 
						
						if($exp_com_id[$com_count]=="" || in_array($exp_com_id[$com_count],$dump_coms_view) || empty($chk_com_f) || $chk_com_f!='accept_present')
						{
							//continue;
						}
						else
						{
							$dump_coms_view[] = $exp_com_id[$com_count];
							$get_friend_community = $new_profile_class_obj->get_community_friends($exp_com_id[$com_count]);
							$find_mem_butts_frd = $display->get_member_or_not($chk_com_gf['general_user_id']);
							while($friend_com = mysql_fetch_assoc($get_friend_community))
							{
							?>
								<div class="tabItem"><a href="/<?php echo $friend_com['profile_url'];?>"><img class="imgs_pro" width="200" height="200" src="<?php echo $src_list . $friend_com['listing_image_name'];?>"/></a>
								  <div class="both_wrap_per">
								<div class="player">
									<?php
									if($friend_com['featured_media']=="Gallery" && $friend_com['media_id']!=0)
									{
										$feature_reg_big_img = "";
										$all_register_images = "";
										$feature_reg_thum_path = "";
										$feature_reg_org_path = "";
										$featured_reg_gal_img_title = "";
							
										if($friend_com['featured_media_table']!="" &&($friend_com['featured_media_table']=="general_artist_gallery_list" || $friend_com['featured_media_table']=="general_community_gallery_list"))
										{
											$get_register_featured_gallery = $new_profile_class_obj ->get_register_featured_media_gallery($friend_com['media_id'],$friend_com['featured_media_table']);
											$all_register_images ="";
											$feature_reg_big_img ="";
											$featured_reg_gal_img_title ="";
											for($cou_i=0;$cou_i<count($get_register_featured_gallery);$cou_i++)
											{
												$feature_reg_big_img = $get_register_featured_gallery[0]['image_name'];
												$all_register_images = $all_register_images.",".$get_register_featured_gallery[$cou_i]['image_name'];
												$featured_reg_gal_img_title = $featured_reg_gal_img_title .",".$get_register_featured_gallery[$cou_i]['image_title'];
											}
											$all_register_images = ltrim($all_register_images,',');
											$featured_reg_gal_img_title = ltrim($featured_reg_gal_img_title,',');
											$feature_reg_org_path = "https://reggallery.s3.amazonaws.com/";
											$feature_reg_thum_path = "https://reggalthumb.s3.amazonaws.com/";
										}
										else if($friend_com['featured_media_table']!="" && $friend_com['featured_media_table']=="general_media"){
											$get_register_featured_gallery = $new_profile_class_obj ->get_featured_gallery_images($friend_com['media_id']);
											$all_register_images ="";
											$feature_reg_big_img ="";
											$featured_reg_gal_img_title ="";
											for($cou_i=0;$cou_i<count($get_register_featured_gallery);$cou_i++)
											{
												$feature_reg_big_img = $get_register_featured_gallery[0]['image_name'];
												$all_register_images = $all_register_images.",".$get_register_featured_gallery[$cou_i]['image_name'];
												$featured_reg_gal_img_title = $featured_reg_gal_img_title .",".$get_register_featured_gallery[$cou_i]['image_title'];
											}
											$all_register_images = ltrim($all_register_images,',');
											$featured_reg_gal_img_title = ltrim($featured_reg_gal_img_title,',');
											$feature_reg_org_path = "https://medgallery.s3.amazonaws.com/";
											$feature_reg_thum_path = "https://medgalthumb.s3.amazonaws.com/";
										}
									?>
									<a href="javascript:void(0);"><img src="../images/profile/play.gif" onmouseover="this.src='../images/profile/play-over.gif'" onmouseout="this.src='../images/profile/play.gif'" onclick="checkload('<?php echo $feature_reg_big_img;?>','<?php echo $all_register_images; ?>','<?php echo $feature_reg_thum_path; ?>','<?php echo $feature_reg_org_path; ?>','<?php echo $featured_reg_gal_img_title; ?>')" title="Play file now."/></a>
									<!--<a href="javascript:void(0);"><img src="../images/profile/add.gif"  onmouseover="this.src='../images/profile/add-over.gif'" onmouseout="this.src='../images/profile/add.gif'" title="Add file to the bottom of your playlist without stopping current media."/></a>-->
									<?php
									if($find_mem_butts_frd != null && $find_mem_butts_frd['general_user_id'] != null && ($find_mem_butts_frd['expiry_date'] >$date || $find_mem_butts_frd['lifetime']==1))
									{
										if($friend_com['type']=="fan_club_yes")
										{
											if($_SESSION['login_email']==NULL || $_SESSION['login_id']!=$chk_com_gf['general_user_id'])
											{
										?>
											<a title="<?php if($friend_com['type']=="fan_club_yes") { echo "Purchase a fan club membership to this users exclusive media."; } elseif($friend_com['type']=="for_sale") { echo "Purchase this projects media."; } ?>" href="javascript:void(0);" onClick="fancy_login_art_coms('<?php echo $friend_com['profile_url']; ?>')" ><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>
										<?php
											}
										}
									}
										 ?>
										<a class="fancybox fancybox.ajax" style="display:none;" href="fan_club.php?data=<?php echo $friend_com['profile_url']; ?>" id="fan_club_pro_ca<?php echo $friend_com['profile_url']; ?>">
											<?php if($friend_com['type']=="fan_club_yes") { echo "Fan Club"; } elseif($friend_com['type']=="for_sale") { echo "Download"; } ?></a>
											<script type="text/javascript">
											$(document).ready(function() {
													$("#fan_club_pro_ca<?php echo $friend_com['profile_url']; ?>").fancybox({
													helpers : {
															media : {},
															buttons : {},
															title : null,
														},
													});
											});
											</script>
										<?php	
									}
									else if($friend_com['featured_media']=="Song" && $friend_com['media_id']!=0)
									{
										if($friend_com['featured_media_table']=="general_artist_audio" || $friend_com['featured_media_table']=="general_community_audio")
										{
											?>
												<a href="javascript:void(0);"><img src="../images/profile/play.gif" onmouseover="this.src='../images/profile/play-over.gif'" onmouseout="this.src='../images/profile/play.gif'" onclick="showPlayer_reg('a','<?php echo $friend_com['media_id'];?>','play_reg','<?php echo $friend_com['featured_media_table'];?>')" title="Play file now."/></a>
												<a href="javascript:void(0);"><img src="../images/profile/add.gif"  onmouseover="this.src='../images/profile/add-over.gif'" onmouseout="this.src='../images/profile/add.gif'" onclick="showPlayer_reg('a','<?php echo $friend_com['media_id'];?>','add_reg','<?php echo $friend_com['featured_media_table'];?>')" title="Add file to the bottom of your playlist without stopping current media."/></a>
											<?php
										}
										else{
											?>
												<a href="javascript:void(0);"><img src="../images/profile/play.gif" onmouseover="this.src='../images/profile/play-over.gif'" onmouseout="this.src='../images/profile/play.gif'" onclick="showPlayer('a','<?php echo $friend_com['media_id'];?>','play')" title="Play file now."/></a>
												<a href="javascript:void(0);"><img src="../images/profile/add.gif"  onmouseover="this.src='../images/profile/add-over.gif'" onmouseout="this.src='../images/profile/add.gif'" onclick="showPlayer('a','<?php echo $friend_com['media_id'];?>','add')" title="Add file to the bottom of your playlist without stopping current media."/></a>
											<?php
										}
										if($find_mem_butts_frd != null && $find_mem_butts_frd['general_user_id'] != null && ($find_mem_butts_frd['expiry_date'] >$date || $find_mem_butts_frd['lifetime']==1))
									{
										if($friend_com['type']=="fan_club_yes")
										{
											if($_SESSION['login_email']==NULL || $_SESSION['login_id']!=$chk_com_gf['general_user_id'])
											{
										?>
											<a title="<?php if($friend_com['type']=="fan_club_yes") { echo "Purchase a fan club membership to this users exclusive media."; } elseif($friend_com['type']=="for_sale") { echo "Purchase this projects media."; } ?>" href="javascript:void(0);" onClick="fancy_login_art_coms('<?php echo $friend_com['profile_url']; ?>')" ><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>
										<?php
											}
										}
									}
										if($friend_com['media_id']!="" && $friend_com['media_id']!=" " && $friend_com['media_id']!=0){
											$com_friend_song = $com_friend_song .",".$friend_com['media_id'];
										}
										 ?>
										<a class="fancybox fancybox.ajax" style="display:none;" href="fan_club.php?data=<?php echo $friend_com['profile_url']; ?>" id="fan_club_pro_ca<?php echo $friend_com['profile_url']; ?>">
											<?php if($friend_com['type']=="fan_club_yes") { echo "Fan Club"; } elseif($friend_com['type']=="for_sale") { echo "Download"; } ?></a>
											<script type="text/javascript">
											$(document).ready(function() {
													$("#fan_club_pro_ca<?php echo $friend_com['profile_url']; ?>").fancybox({
													helpers : {
															media : {},
															buttons : {},
															title : null,
														},
													});
											});
											</script>
										<?php	
									}
									else if($friend_com['featured_media']=="Video" && $friend_com['media_id']!=0)
									{
										if($friend_com['featured_media_table']=="general_artist_video" || $friend_com['featured_media_table']=="general_community_video")
										{
											?>
												<a href="javascript:void(0);"><img src="../images/profile/play.gif" onmouseover="this.src='../images/profile/play-over.gif'" onmouseout="this.src='../images/profile/play.gif'" onclick="showPlayer_reg('v','<?php echo $friend_com['media_id'];?>','playlist_reg','<?php echo $friend_com['featured_media_table'];?>')" title="Play file now."/></a>
												<a href="javascript:void(0);"><img src="../images/profile/add.gif"  onmouseover="this.src='../images/profile/add-over.gif'" onmouseout="this.src='../images/profile/add.gif'" onclick="showPlayer_reg('v','<?php echo $friend_com['media_id'];?>','addvi_reg','<?php echo $friend_com['featured_media_table'];?>')" title="Add file to the bottom of your playlist without stopping current media."/></a>
											<?php
										}else{
											?>
												<a href="javascript:void(0);"><img src="../images/profile/play.gif" onmouseover="this.src='../images/profile/play-over.gif'" onmouseout="this.src='../images/profile/play.gif'" onclick="showPlayer('v','<?php echo $friend_com['media_id'];?>','playlist')" title="Play file now."/></a>
												<a href="javascript:void(0);"><img src="../images/profile/add.gif"  onmouseover="this.src='../images/profile/add-over.gif'" onmouseout="this.src='../images/profile/add.gif'" onclick="showPlayer('v','<?php echo $friend_com['media_id'];?>','addvi')" title="Add file to the bottom of your playlist without stopping current media."/></a>
											<?php
										}
										if($find_mem_butts_frd != null && $find_mem_butts_frd['general_user_id'] != null && ($find_mem_butts_frd['expiry_date'] >$date || $find_mem_butts_frd['lifetime']==1))
									{
										if($friend_com['type']=="fan_club_yes")
										{
											if($_SESSION['login_email']==NULL || $_SESSION['login_id']!=$chk_com_gf['general_user_id'])
											{
										?>
											<a title="<?php if($friend_com['type']=="fan_club_yes") { echo "Purchase a fan club membership to this users exclusive media."; } elseif($friend_com['type']=="for_sale") { echo "Purchase this projects media."; } ?>" href="javascript:void(0);" onClick="fancy_login_art_coms('<?php echo $friend_com['profile_url']; ?>')" ><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>
										<?php
											}
										}
									}
										if($friend_com['media_id']!="" && $friend_com['media_id']!=" " && $friend_com['media_id']!=0){
											$com_friend_video = $com_friend_video .",".$friend_com['media_id'];
										}
										?>
										<a class="fancybox fancybox.ajax" style="display:none;" href="fan_club.php?data=<?php echo $friend_com['profile_url']; ?>" id="fan_club_pro_ca<?php echo $friend_com['profile_url']; ?>">
											<?php if($friend_com['type']=="fan_club_yes") { echo "Fan Club"; } elseif($friend_com['type']=="for_sale") { echo "Download"; } ?></a>
											<script type="text/javascript">
											$(document).ready(function() {
													$("#fan_club_pro_ca<?php echo $friend_com['profile_url']; ?>").fancybox({
													helpers : {
															media : {},
															buttons : {},
															title : null,
														},
													});
											});
											</script>
										<?php
									}
									else{
									if($find_mem_butts_frd != null && $find_mem_butts_frd['general_user_id'] != null && ($find_mem_butts_frd['expiry_date'] >$date || $find_mem_butts_frd['lifetime']==1))
									{
										if($friend_com['type']=="fan_club_yes")
										{
											if($_SESSION['login_email']==NULL || $_SESSION['login_id']!=$chk_com_gf['general_user_id'])
											{
										?>
											<a title="<?php if($friend_com['type']=="fan_club_yes") { echo "Purchase a fan club membership to this users exclusive media."; } elseif($friend_com['type']=="for_sale") { echo "Purchase this projects media."; } ?>" href="javascript:void(0);" onClick="fancy_login_art_coms('<?php echo $friend_com['profile_url']; ?>')" ><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>
										<?php
											}
										}
									}
								  ?>
										<a class="fancybox fancybox.ajax" style="display:none;" href="fan_club.php?data=<?php echo $friend_com['profile_url']; ?>" id="fan_club_pro_ca<?php echo $friend_com['profile_url']; ?>">
											<?php if($friend_com['type']=="fan_club_yes") { echo "Fan Club"; } elseif($friend_com['type']=="for_sale") { echo "Download"; } ?></a>
											<script type="text/javascript">
											$(document).ready(function() {
													$("#fan_club_pro_ca<?php echo $friend_com['profile_url']; ?>").fancybox({
													helpers : {
															media : {},
															buttons : {},
															title : null,
														},
													});
											});
											</script>
									<!--<a href="javascript:void(0);"><img src="../images/profile/play.gif" onmouseover="this.src='../images/profile/play-over.gif'" onmouseout="this.src='../images/profile/play.gif'" /></a>
									<a href="javascript:void(0);"><img src="../images/profile/add.gif"  onmouseover="this.src='../images/profile/add-over.gif'" onmouseout="this.src='../images/profile/add.gif'"/></a>-->
									<?php
									}
									?>
									<!--<img onmouseout="this.src='images/profile/download.gif'" onmouseover="this.src='images/profile/download-over.gif'" src="images/profile/download.gif">-->
									
								  </div>
								  <div class="type_subtype"><a href="/<?php echo $friend_com['profile_url'];?>">
								  <?php if(strlen($friend_com['name'])>39){
										echo substr($friend_com['name'],0,37)."...";
								  }else { echo $friend_com['name']; }?></a><?php if(isset($friend_com['artist_id']))
											{
												$get_profile_type = $display->types_of_search('artist',$friend_com['artist_id']);
												if(!empty($get_profile_type) && $get_profile_type!=Null)
												{
													$res_profile_type = mysql_fetch_assoc($get_profile_type);
													if($res_profile_type['type_name']!=""){
													?>
														<br/><span style="font-size:13px; font-weight:normal;">
														<?php 
														if(strlen($res_profile_type['type_name'])>26){
															echo substr($res_profile_type['type_name'],0,24)."...";
														}else{
															echo $res_profile_type['type_name'];
														}
														if($res_profile_type['subtype_name']!="" && strlen($res_profile_type['type_name']<26)){
															$type_length = strlen($res_profile_type['type_name']);
															if($type_length + strlen($res_profile_type['subtype_name'])>22){
																echo ", ".substr($res_profile_type['subtype_name'],0,22 - $type_length)."...";
															}else{
																echo ", ".$res_profile_type['subtype_name'];
															}
															
														}
														/*echo $res_profile_type['type_name'];
														if($res_profile_type['subtype_name']!=""){
															echo ", ".$res_profile_type['subtype_name'];
														}*/
														?>
														</span>
												<?php
													}
												}
											}
											elseif(isset($friend_com['community_id']))
											{
												$get_profile_type = $display->types_of_search('community',$friend_com['community_id']);
												if(!empty($get_profile_type) && $get_profile_type!=Null)
												{
													$res_profile_type = mysql_fetch_assoc($get_profile_type);
													if($res_profile_type['type_name']!=""){
													?>
														<br/><span style="font-size:13px; font-weight:normal;">
														<?php 
														if(strlen($res_profile_type['type_name'])>26){
															echo substr($res_profile_type['type_name'],0,24)."...";
														}else{
															echo $res_profile_type['type_name'];
														}
														if($res_profile_type['subtype_name']!="" && strlen($res_profile_type['type_name']<26)){
															$type_length = strlen($res_profile_type['type_name']);
															if($type_length + strlen($res_profile_type['subtype_name'])>22){
																echo ", ".substr($res_profile_type['subtype_name'],0,22 - $type_length)."...";
															}else{
																echo ", ".$res_profile_type['subtype_name'];
															}
															
														}
														/*echo $res_profile_type['type_name'];
														if($res_profile_type['subtype_name']!=""){
															echo ", ".$res_profile_type['subtype_name'];
														}*/
														?>
														</span>
												<?php
													}
												}
											} ?> </div></div>
								</div>
							<?php
							}
								
						}
					}
					?>
					<input type="hidden" id="com_friend_song" value="<?php echo $com_friend_song;?>"/>
					<input type="hidden" id="com_friend_video" value="<?php echo $com_friend_video;?>"/>
					<script type="text/javascript">
					$("document").ready(function(){
						var a = document.getElementById("com_friend_song").value;
						var b = document.getElementById("com_friend_video").value;
						if((a =="" || a ==" ") && (b=="" || b==" "))
						{
							$("#profileFeaturedfriend").css({"display":"none"});
						}
					});
					
					</script>
					<?php
					for($com_count = 0;$com_count< count($exp_com_id); $com_count++)
					{
						if($exp_com_id[$com_count]=="")
						{
							continue;
						}
						else
						{
							$get_friend_community = $new_profile_class_obj->get_community_friends($exp_com_id[$com_count]);
							if($get_friend_community!="" && $get_friend_community!=Null)
							{
								if(mysql_num_rows($get_friend_community)>0 && $flag_cf_2==0)
								{
							?>
									</div>
							<?php
									$flag_cf_2 = 1;
								}
							}
						}
					}
				}
				?>
			</div>    
		<?php
		}
	}
	else if($community_event_check !=0 || $community_project_check !=0 || $artist_event_check !=0 || $artist_project_check !=0)
	{
		if(($artist_project_check !=0 || $community_project_check !=0 || $community_event_check !=0 || $artist_event_check !=0) && (!empty($getuser['creators_info']) && !empty($getuser['creator'])))
		{
			
			$exp_car = explode('|',$getuser['creators_info']);
			if($exp_car[0]=='general_artist')
			{
				$sql__ar = mysql_query("SELECT * FROM general_artist WHERE artist_id='".$exp_car[1]."'");
			}
			if($exp_car[0]=='general_community')
			{
				$sql__ar = mysql_query("SELECT * FROM general_community WHERE community_id='".$exp_car[1]."'");
			}
			if($exp_car[0]=='artist_project' || $exp_car[0]=='community_project')
			{
				$sql__ar = mysql_query("SELECT * FROM $exp_car[0] WHERE id='".$exp_car[1]."'");
			}
			if(isset($sql__ar)){
			if($sql__ar!="" && $sql__ar!=Null){
			if(mysql_num_rows($sql__ar)>0 || $Find_Artist == "artist")
			{
				if($artist_check !=0 || $community_check !=0)
				{
?>
					<div id="promotionsTab" class="hiddenBlock7">
						<div id="profileFeaturedproart" style="height:55px; position:relative; top:19px; margin-right: 17px; float: right;">
<?php
				}
				else
				{
?>
					<div id="servicesTab" class="hiddenBlock4">
						<div id="profileFeaturedproart" style="height:55px; position:relative; top:19px; margin-right: 17px; float: right;">
<?php
				}
?>
			
				<div class="playall">
					Play All
				</div>
				<?php
				if($artist_check !=0 || $community_check !=0)
				{
				?>
					<a title="Play all of the media on this profile page." href="javascript:void(0);">
						<img src="images/profile/play.png" width="35" height="23" onmouseover="this.src='images/profile/play-over.png'" onmouseout="this.src='images/profile/play.png'" onclick="PlayAll_featured_media('songtype_value_artist','videotype_value_artist')"/>
					</a>
					<a title="Add all of the media on this profile page to your playlist without stopping your current file." href="javascript:void(0);">
						<img src="images/profile/add.png" width="27" height="23" onmouseover="this.src='images/profile/add-over.png'" onmouseout="this.src='images/profile/add.png'" onclick="PlayAll_featured_media('songtype_value_artist','videotype_value_artist')"/>
					</a>
				<?php
				}
				elseif($artist_event_check !=0 || $community_event_check !=0)
				{
				?>
					<a title="Play all of the media on this events page." href="javascript:void(0);">
						<img src="images/profile/play.png"  style="border: medium none; float: left;" width="35" height="23" onmouseover="this.src='images/profile/play-over.png'" onmouseout="this.src='images/profile/play.png'" onclick="PlayAll_featured_media('songtype_value_artist','videotype_value_artist')"/>
					</a>
					<a title="Add all of the media on this events page to your playlist without stopping your current file." href="javascript:void(0);">
						<img src="images/profile/add.png" width="27" height="23" onmouseover="this.src='images/profile/add-over.png'" onmouseout="this.src='images/profile/add.png'" onclick="PlayAll_featured_media('songtype_value_artist','videotype_value_artist')"/>
					</a>
				<?php
				}
				elseif($artist_project_check !=0 || $community_project_check !=0)
				{
				?>
					<a title="Play all of the media on this projects page." href="javascript:void(0);">
						<img src="images/profile/play.png"  style="border: medium none; float: left;" width="35" height="23" onmouseover="this.src='images/profile/play-over.png'" onmouseout="this.src='images/profile/play.png'" onclick="PlayAll_featured_media('songtype_value_artist','videotype_value_artist')"/>
					</a>
					<a title="Add all of the media on this projects page to your playlist without stopping your current file." href="javascript:void(0);">
						<img src="images/profile/add.png" width="27" height="23" onmouseover="this.src='images/profile/add-over.png'" onmouseout="this.src='images/profile/add.png'" onclick="PlayAll_featured_media('songtype_value_artist','videotype_value_artist')" />
					</a>
				<?php
				}
				?>
			</div>
			<div id="profileFeatured">
			
			<?php //echo $getuser['featured_media'].$getuser['media_id'];
			
			if($artist_check !=0 || $artist_event_check !=0 || $artist_project_check !=0  )
			{
				$table_type = "artist";
			}
			if($community_check !=0 || $community_event_check !=0 || $community_project_check !=0  )
			{
				$table_type = "community";
			}
			if($getuser['featured_media'] == "Gallery" && $getuser['media_id']!=0)
			{
				$find_whether_regis=$new_profile_class_obj->get_count_Gallery_at_register($getuser["$table_type".'_id'],$getuser['media_id']);
				//var_dump($find_whether_regis);
				if($find_whether_regis["count(*)"]>1)
				{
					$getimages=$new_profile_class_obj->get_featured_media_gallery_at_register($getuser['media_id'],$table_type);
					$feature_image_name = array();
					$feature_image_title = array();
					$feature_count = 0;
					while($get_image = mysql_fetch_assoc($getimages))
					{
						$feature_image_name[$feature_count] = $get_image['image_name'];
						$feature_image_title[$feature_count] = $get_image['image_title'];
						$feature_count++;
					}
					$feature_big_img = $feature_image_name[0];
					$featured_gal_img = implode(",", $feature_image_name);
					$featured_gal_img_title = implode(",", $feature_image_title);
					$feature_org_path = "https://reggallery.s3.amazonaws.com/";
					$feature_thum_path = "https://reggalthumb.s3.amazonaws.com/";
				}
				else
				{
					$getimages=$new_profile_class_obj->get_featured_media_gallery($getuser['media_id']);
					$feature_image_name = array();
					$feature_image_title = array();
					$feature_count = 0;
					while($get_image = mysql_fetch_assoc($getimages))
					{
						$feature_image_name[$feature_count] = $get_image['image_name'];
						$feature_image_title[$feature_count] = $get_image['image_title'];
						$feature_count++;
					}
					$feature_big_img = $feature_image_name[0];
					$featured_gal_img = implode(",", $feature_image_name);
					$featured_gal_img_title = implode(",", $feature_image_title);
					$feature_org_path = "https://medgallery.s3.amazonaws.com/";
					$feature_thum_path = "https://medgalthumb.s3.amazonaws.com/";
				}
			?>
				<div class="fmedia">featured Media</div>
				<?php
				if($artist_check !=0 || $community_check !=0)
				{
				?>
					<a title="Play this artists featured media." href="javascript:void(0);"><img src="../images/profile/play.png" width="35" height="23" onmouseover="this.src='../images/profile/play-over.png'" onmouseout="this.src='../images/profile/play.png'" onclick="checkload('<?php echo $feature_big_img;?>','<?php echo $featured_gal_img; ?>','<?php echo $feature_thum_path; ?>','<?php echo $feature_org_path; ?>','<?php echo $featured_gal_img_title; ?>')" /></a>
					<a title="Add this artists featured media to your playlist without stopping your current file." href="javascript:void(0);"><img src="../images/profile/add.png" width="27" height="23" onmouseover="this.src='../images/profile/add-over.png'" onmouseout="this.src='../images/profile/add.png'" /></a>
			<?php
				}
				elseif($artist_event_check !=0 || $community_event_check !=0)
				{
			?>
					<a title="Play this events featured media." href="javascript:void(0);"><img src="../images/profile/play.png" width="35" height="23" onmouseover="this.src='../images/profile/play-over.png'" onmouseout="this.src='../images/profile/play.png'" onclick="checkload('<?php echo $feature_big_img;?>','<?php echo $featured_gal_img; ?>','<?php echo $feature_thum_path; ?>','<?php echo $feature_org_path; ?>','<?php echo $featured_gal_img_title; ?>')" /></a>
					<a title="Add this events featured media to your playlist without stopping your current file." href="javascript:void(0);"><img src="../images/profile/add.png" width="27" height="23" onmouseover="this.src='../images/profile/add-over.png'" onmouseout="this.src='../images/profile/add.png'" /></a>
			<?php
				}
				elseif($artist_project_check !=0 || $community_project_check !=0)
				{
			?>
					<a title="Play this projects featured media." href="javascript:void(0);"><img src="../images/profile/play.png" width="35" height="23" onmouseover="this.src='../images/profile/play-over.png'" onmouseout="this.src='../images/profile/play.png'" onclick="checkload('<?php echo $feature_big_img;?>','<?php echo $featured_gal_img; ?>','<?php echo $feature_thum_path; ?>','<?php echo $feature_org_path; ?>','<?php echo $featured_gal_img_title; ?>')" /></a>
					<a title="Add this projects featured media to your playlist without stopping your current file." href="javascript:void(0);"><img src="../images/profile/add.png" width="27" height="23" onmouseover="this.src='../images/profile/add-over.png'" onmouseout="this.src='../images/profile/add.png'" /></a>
			<?php
				}
			}
			if($getuser['featured_media'] == "Song" && $getuser['media_id']!=0)
			{
			?>
				<div class="fmedia">featured Media</div>
				<?php
				if($artist_check !=0 || $community_check !=0)
				{
				?>
					<a title="Play this artists featured media." href="javascript:void(0);"><img src="../images/profile/play.png" width="35" height="23" onmouseover="this.src='../images/profile/play-over.png'" onmouseout="this.src='../images/profile/play.png'" onclick="showPlayer('a','<?php echo $getuser['media_id'];?>','play')" /></a>
					<a title="Add this artists featured media to your playlist without stopping your current file." href="javascript:void(0);"><img src="../images/profile/add.png" width="27" height="23" onmouseover="this.src='../images/profile/add-over.png'" onmouseout="this.src='../images/profile/add.png'" onclick="showPlayer('a','<?php echo $getuser['media_id'];?>','add')" /></a>
			<?php
				}
				elseif($artist_event_check !=0 || $community_event_check !=0)
				{
			?>
					<a title="Play this events featured media." href="javascript:void(0);"><img src="../images/profile/play.png" width="35" height="23" onmouseover="this.src='../images/profile/play-over.png'" onmouseout="this.src='../images/profile/play.png'" onclick="showPlayer('a','<?php echo $getuser['media_id'];?>','play')" /></a>
					<a title="Add this events featured media to your playlist without stopping your current file." href="javascript:void(0);"><img src="../images/profile/add.png" width="27" height="23" onmouseover="this.src='../images/profile/add-over.png'" onmouseout="this.src='../images/profile/add.png'" onclick="showPlayer('a','<?php echo $getuser['media_id'];?>','add')" /></a>
			<?php
				}
				elseif($artist_project_check !=0 || $community_project_check !=0)
				{
			?>
					<a title="Play this projects featured media." href="javascript:void(0);"><img src="../images/profile/play.png" width="35" height="23" onmouseover="this.src='../images/profile/play-over.png'" onmouseout="this.src='../images/profile/play.png'" onclick="showPlayer('a','<?php echo $getuser['media_id'];?>','play')" /></a>
					<a title="Add this projects featured media to your playlist without stopping your current file." href="javascript:void(0);"><img src="../images/profile/add.png" width="27" height="23" onmouseover="this.src='../images/profile/add-over.png'" onmouseout="this.src='../images/profile/add.png'" onclick="showPlayer('a','<?php echo $getuser['media_id'];?>','add')" /></a>
			<?php
				}
			}
			?>
			<?php
			if($getuser['featured_media'] == "Video" && $getuser['media_id']!=0)
			{
			?>
				<div class="fmedia">featured Media</div>
				<?php
				if($artist_check !=0 || $community_check !=0)
				{
				?>
					<a title="Play this artists featured media." href="javascript:void(0);"><img src="../images/profile/play.png" width="35" height="23" onmouseover="this.src='../images/profile/play-over.png'" onmouseout="this.src='../images/profile/play.png'" onclick="showPlayer('v','<?php echo $getuser['media_id'];?>','playlist')" /></a>
					<a title="Add this artists featured media to your playlist without stopping your current file." href="javascript:void(0);"><img src="../images/profile/add.png" width="27" height="23" onmouseover="this.src='../images/profile/add-over.png'" onmouseout="this.src='../images/profile/add.png'" onclick="showPlayer('v','<?php echo $getuser['media_id'];?>','addvi')" /></a>
			<?php
				}
				elseif($artist_event_check !=0 || $community_event_check !=0)
				{
			?>
					<a title="Play this events featured media." href="javascript:void(0);"><img src="../images/profile/play.png" width="35" height="23" onmouseover="this.src='../images/profile/play-over.png'" onmouseout="this.src='../images/profile/play.png'" onclick="showPlayer('v','<?php echo $getuser['media_id'];?>','playlist')" /></a>
					<a title="Add this events featured media to your playlist without stopping your current file." href="javascript:void(0);"><img src="../images/profile/add.png" width="27" height="23" onmouseover="this.src='../images/profile/add-over.png'" onmouseout="this.src='../images/profile/add.png'" onclick="showPlayer('v','<?php echo $getuser['media_id'];?>','addvi')" /></a>
			<?php
				}
				elseif($artist_project_check !=0 || $community_project_check !=0)
				{
			?>
					<a title="Play this projects featured media." href="javascript:void(0);"><img src="../images/profile/play.png" width="35" height="23" onmouseover="this.src='../images/profile/play-over.png'" onmouseout="this.src='../images/profile/play.png'" onclick="showPlayer('v','<?php echo $getuser['media_id'];?>','playlist')" /></a>
					<a title="Add this projects featured media to your playlist without stopping your current file." href="javascript:void(0);"><img src="../images/profile/add.png" width="27" height="23" onmouseover="this.src='../images/profile/add-over.png'" onmouseout="this.src='../images/profile/add.png'" onclick="showPlayer('v','<?php echo $getuser['media_id'];?>','addvi')" /></a>
			<?php
				}
			}
			?>
			
		</div>
			<h2><a href="#">Artists</a></h2>
			
			<div class="rowItem">
<?php
			}}}
			
			if(isset($sql__ar))
			{
				if($sql__ar!="" && $sql__ar!=Null){
				if(mysql_num_rows($sql__ar)>0)
				{
					$ans_cp = mysql_fetch_assoc($sql__ar);
					
					if(isset($ans_cp['artist_id']))
					{
						$sql_butts = mysql_query("SELECT * FROM general_user WHERE artist_id='".$ans_cp['artist_id']."'");
						$ans_butts = mysql_fetch_assoc($sql_butts);
					}
					elseif(isset($ans_cp['community_id']))
					{
						$sql_butts = mysql_query("SELECT * FROM general_user WHERE community_id='".$ans_cp['community_id']."'");
						$ans_butts = mysql_fetch_assoc($sql_butts);
					}
					$find_mem_butts = $display->get_member_or_not($ans_butts['general_user_id']);
					if($exp_car[0]=='general_artist')
					{
					
		?>
						<div class="tabItem"> 
							<a href ="<?php if($ans_cp['profile_url']!="") { ?>/<?php echo $ans_cp['profile_url']; } ?>"><img class="imgs_pro" src="<?php if(isset($ans_cp['image_name']) && $ans_cp['image_name']!="") { echo 'https://artjcropprofile.s3.amazonaws.com/'.$ans_cp['image_name'] ; } else { echo 'https://artjcropprofile.s3.amazonaws.com/Noimage.png'; } ?>" width="200" height="200" /></a>
							<div class="both_wrap_per">
							<div class="player">
								<?php
								if($ans_cp['featured_media']=="Gallery" && $ans_cp['media_id']!=0)
								{
									$feature_reg_big_img = "";
									$all_register_images = "";
									$feature_reg_thum_path = "";
									$feature_reg_org_path = "";
									$featured_reg_gal_img_title = "";
							
									if($ans_cp['featured_media_table']!="" &&($ans_cp['featured_media_table']=="general_artist_gallery_list" || $ans_cp['featured_media_table']=="general_community_gallery_list"))
									{
										$get_register_featured_gallery = $new_profile_class_obj ->get_register_featured_media_gallery($ans_cp['media_id'],$ans_cp['featured_media_table']);
										$all_register_images ="";
										$feature_reg_big_img ="";
										$featured_reg_gal_img_title ="";
										for($cou_i=0;$cou_i<count($get_register_featured_gallery);$cou_i++)
										{
											$feature_reg_big_img = $get_register_featured_gallery[0]['image_name'];
											$all_register_images = $all_register_images.",".$get_register_featured_gallery[$cou_i]['image_name'];
											$featured_reg_gal_img_title = $featured_reg_gal_img_title .",".$get_register_featured_gallery[$cou_i]['image_title'];
										}
										$all_register_images = ltrim($all_register_images,',');
										$featured_reg_gal_img_title = ltrim($featured_reg_gal_img_title,',');
										$feature_reg_org_path = "https://reggallery.s3.amazonaws.com/";
										$feature_reg_thum_path = "https://reggalthumb.s3.amazonaws.com/";
									}
									else if($ans_cp['featured_media_table']!="" && $ans_cp['featured_media_table']=="general_media"){
										$get_register_featured_gallery = $new_profile_class_obj ->get_featured_gallery_images($ans_cp['media_id']);
										$all_register_images ="";
										$feature_reg_big_img ="";
										$featured_reg_gal_img_title ="";
										for($cou_i=0;$cou_i<count($get_register_featured_gallery);$cou_i++)
										{
											$feature_reg_big_img = $get_register_featured_gallery[0]['image_name'];
											$all_register_images = $all_register_images.",".$get_register_featured_gallery[$cou_i]['image_name'];
											$featured_reg_gal_img_title = $featured_reg_gal_img_title .",".$get_register_featured_gallery[$cou_i]['image_title'];
										}
										$all_register_images = ltrim($all_register_images,',');
										$featured_reg_gal_img_title = ltrim($featured_reg_gal_img_title,',');
										$feature_reg_org_path = "https://medgallery.s3.amazonaws.com/";
										$feature_reg_thum_path = "https://medgalthumb.s3.amazonaws.com/";
									}
									?>
									<a href="javascript:void(0);"><img src="../images/profile/play.gif" onmouseover="this.src='../images/profile/play-over.gif'" onmouseout="this.src='../images/profile/play.gif'" onclick="checkload('<?php echo $feature_reg_big_img;?>','<?php echo $all_register_images; ?>','<?php echo $feature_reg_thum_path; ?>','<?php echo $feature_reg_org_path; ?>','<?php echo $featured_reg_gal_img_title; ?>')" title="Play file now."/></a>
									<!--<a href="javascript:void(0);"><img src="../images/profile/add.gif"  onmouseover="this.src='../images/profile/add-over.gif'" onmouseout="this.src='../images/profile/add.gif'" title="Add file to the bottom of your playlist without stopping current media."/></a>-->
									<?php
									if($find_mem_butts != null && $find_mem_butts['general_user_id'] != null && ($find_mem_butts['expiry_date'] >$date || $find_mem_butts['lifetime']==1))
									{
										if($ans_cp['type']=='fan_club_yes')
										{
											if($_SESSION['login_email']==NULL || $_SESSION['login_email']!=$ans_butts['email'])
											{
										?>
											<a title="<?php if($ans_cp['type']=="fan_club_yes") { echo "Purchase a fan club membership to this users exclusive media."; } elseif($ans_cp['type']=="for_sale") { echo "Purchase this projects media."; } ?>" href="javascript:void(0);" onClick="fancy_login_art_coms('<?php echo $ans_cp['profile_url']; ?>')" ><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>
										<?php
											}
										}
									}
									?>
									<a class="fancybox fancybox.ajax" style="display:none;" href="fan_club.php?data=<?php echo $ans_cp['profile_url']; ?>" id="fan_club_pro_ca<?php echo $ans_cp['profile_url']; ?>">
											<?php if($ans_cp['type']=="fan_club_yes") { echo "Fan Club"; } elseif($ans_cp['type']=="for_sale") { echo "Download"; } ?></a>
											<script type="text/javascript">
											$(document).ready(function() {
													$("#fan_club_pro_ca<?php echo $ans_cp['profile_url']; ?>").fancybox({
													helpers : {
															media : {},
															buttons : {},
															title : null,
														},
													});
											});
											</script>
									<?php	
								}
								else if($ans_cp['featured_media']=="Song" && $ans_cp['media_id']!=0)
								{
									if($ans_cp['featured_media_table']=="general_artist_audio" || $ans_cp['featured_media_table']=="general_community_audio")
									{
										?>
											<a href="javascript:void(0);"><img src="../images/profile/play.gif" onmouseover="this.src='../images/profile/play-over.gif'" onmouseout="this.src='../images/profile/play.gif'" onclick="showPlayer_reg('a','<?php echo $ans_cp['media_id'];?>','play_reg','<?php echo $ans_cp['featured_media_table'];?>')" title="Play file now."/></a>
											<a href="javascript:void(0);"><img src="../images/profile/add.gif"  onmouseover="this.src='../images/profile/add-over.gif'" onmouseout="this.src='../images/profile/add.gif'" onclick="showPlayer_reg('a','<?php echo $ans_cp['media_id'];?>','add_reg','<?php echo $ans_cp['featured_media_table'];?>')" title="Add file to the bottom of your playlist without stopping current media."/></a>
										<?php
									}
									else{
										?>
											<a href="javascript:void(0);"><img src="../images/profile/play.gif" onmouseover="this.src='../images/profile/play-over.gif'" onmouseout="this.src='../images/profile/play.gif'" onclick="showPlayer('a','<?php echo $ans_cp['media_id'];?>','play')" title="Play file now."/></a>
											<a href="javascript:void(0);"><img src="../images/profile/add.gif"  onmouseover="this.src='../images/profile/add-over.gif'" onmouseout="this.src='../images/profile/add.gif'" onclick="showPlayer('a','<?php echo $ans_cp['media_id'];?>','add')" title="Add file to the bottom of your playlist without stopping current media."/></a>
										<?php
									}
									if($find_mem_butts != null && $find_mem_butts['general_user_id'] != null && ($find_mem_butts['expiry_date'] >$date || $find_mem_butts['lifetime']==1))
									{
										if($ans_cp['type']=='fan_club_yes')
										{
											if($_SESSION['login_email']==NULL || $_SESSION['login_email']!=$ans_butts['email'])
											{
										?>
											<a title="<?php if($ans_cp['type']=="fan_club_yes") { echo "Purchase a fan club membership to this users exclusive media."; } elseif($ans_cp['type']=="for_sale") { echo "Purchase this projects media."; } ?>" href="javascript:void(0);" onClick="fancy_login_art_coms('<?php echo $ans_cp['profile_url']; ?>')" ><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>
										<?php
											}
										}
									}
									if($ans_cp['media_id']!="" && $ans_cp['media_id']!=" " && $ans_cp['media_id']!=0){
										$songtype_value_artist = $songtype_value_artist .",".$ans_cp['media_id'];
									}
									?>
									<a class="fancybox fancybox.ajax" style="display:none;" href="fan_club.php?data=<?php echo $ans_cp['profile_url']; ?>" id="fan_club_pro_ca<?php echo $ans_cp['profile_url']; ?>">
											<?php if($ans_cp['type']=="fan_club_yes") { echo "Fan Club"; } elseif($ans_cp['type']=="for_sale") { echo "Download"; } ?></a>
											<script type="text/javascript">
											$(document).ready(function() {
													$("#fan_club_pro_ca<?php echo $ans_cp['profile_url']; ?>").fancybox({
													helpers : {
															media : {},
															buttons : {},
															title : null,
														},
													});
											});
											</script>
									<?php
								}
								else if($ans_cp['featured_media']=="Video" && $ans_cp['media_id']!=0)
								{
									if($ans_cp['featured_media_table']=="general_artist_video" || $ans_cp['featured_media_table']=="general_community_video")
									{
										?>
											<a href="javascript:void(0);"><img src="../images/profile/play.gif" onmouseover="this.src='../images/profile/play-over.gif'" onmouseout="this.src='../images/profile/play.gif'" onclick="showPlayer_reg('v','<?php echo $ans_cp['media_id'];?>','playlist_reg','<?php echo $ans_cp['featured_media_table'];?>')" title="Play file now."/></a>
											<a href="javascript:void(0);"><img src="../images/profile/add.gif"  onmouseover="this.src='../images/profile/add-over.gif'" onmouseout="this.src='../images/profile/add.gif'" onclick="showPlayer_reg('v','<?php echo $ans_cp['media_id'];?>','addvi_reg','<?php echo $ans_cp['featured_media_table'];?>')" title="Add file to the bottom of your playlist without stopping current media."/></a>
										<?php
									}else{
										?>
											<a href="javascript:void(0);"><img src="../images/profile/play.gif" onmouseover="this.src='../images/profile/play-over.gif'" onmouseout="this.src='../images/profile/play.gif'" onclick="showPlayer('v','<?php echo $ans_cp['media_id'];?>','playlist')" title="Play file now."/></a>
											<a href="javascript:void(0);"><img src="../images/profile/add.gif"  onmouseover="this.src='../images/profile/add-over.gif'" onmouseout="this.src='../images/profile/add.gif'" onclick="showPlayer('v','<?php echo $ans_cp['media_id'];?>','addvi')" title="Add file to the bottom of your playlist without stopping current media."/></a>
										<?php
									}
									if($find_mem_butts != null && $find_mem_butts['general_user_id'] != null && ($find_mem_butts['expiry_date'] >$date || $find_mem_butts['lifetime']==1))
									{
										if($ans_cp['type']=='fan_club_yes')
										{
											if($_SESSION['login_email']==NULL || $_SESSION['login_email']!=$ans_butts['email'])
											{
										?>
											<a title="<?php if($ans_cp['type']=="fan_club_yes") { echo "Purchase a fan club membership to this users exclusive media."; } elseif($ans_cp['type']=="for_sale") { echo "Purchase this projects media."; } ?>" href="javascript:void(0);" onClick="fancy_login_art_coms('<?php echo $ans_cp['profile_url']; ?>')" ><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>
										<?php
											}
										}
									}
									if($ans_cp['media_id']!="" && $ans_cp['media_id']!=" " && $ans_cp['media_id']!=0){
										$videotype_value_artist = $videotype_value_artist .",".$ans_cp['media_id'];
									}
									?>
									<a class="fancybox fancybox.ajax" style="display:none;" href="fan_club.php?data=<?php echo $ans_cp['profile_url']; ?>" id="fan_club_pro_ca<?php echo $ans_cp['profile_url']; ?>">
											<?php if($ans_cp['type']=="fan_club_yes") { echo "Fan Club"; } elseif($ans_cp['type']=="for_sale") { echo "Download"; } ?></a>
											<script type="text/javascript">
											$(document).ready(function() {
													$("#fan_club_pro_ca<?php echo $ans_cp['profile_url']; ?>").fancybox({
													helpers : {
															media : {},
															buttons : {},
															title : null,
														},
													});
											});
											</script>
									<?php
								}
								else{
								if($find_mem_butts != null && $find_mem_butts['general_user_id'] != null && ($find_mem_butts['expiry_date'] >$date || $find_mem_butts['lifetime']==1))
									{
									if($ans_cp['type']=='fan_club_yes')
										{
											if($_SESSION['login_email']==NULL || $_SESSION['login_email']!=$ans_butts['email'])
											{
										?>
											<a title="<?php if($ans_cp['type']=="fan_club_yes") { echo "Purchase a fan club membership to this users exclusive media."; } elseif($ans_cp['type']=="for_sale") { echo "Purchase this projects media."; } ?>" href="javascript:void(0);" onClick="fancy_login_art_coms('<?php echo $ans_cp['profile_url']; ?>')" ><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>
										<?php
											}
										}
									}
							  ?>
							  <a  class="fancybox fancybox.ajax" style="display:none;" href="fan_club.php?data=<?php echo $ans_cp['profile_url']; ?>" id="fan_club_pro_ca<?php echo $ans_cp['profile_url']; ?>">
											<?php if($ans_cp['type']=="fan_club_yes") { echo "Fan Club"; } elseif($ans_cp['type']=="for_sale") { echo "Download"; } ?></a>
											<script type="text/javascript">
											$(document).ready(function() {
													$("#fan_club_pro_ca<?php echo $ans_cp['profile_url']; ?>").fancybox({
													helpers : {
															media : {},
															buttons : {},
															title : null,
														},
													});
											});
											</script>
								<!--<a href="javascript:void(0);"><img src="../images/profile/play.gif" onmouseover="this.src='../images/profile/play-over.gif'" onmouseout="this.src='../images/profile/play.gif'" /></a>
								<a href="javascript:void(0);"><img src="../images/profile/add.gif"  onmouseover="this.src='../images/profile/add-over.gif'" onmouseout="this.src='../images/profile/add.gif'"/></a>-->
								<?php
								}
								?>
								<!--<img src="images/profile/download.gif" onmouseover="this.src='images/profile/download-over.gif'" onmouseout="this.src='images/profile/download.gif'" />-->
								
								
							</div>
							<div class="type_subtype"><a href ="<?php if($ans_cp['profile_url']!="") { ?>/<?php echo $ans_cp['profile_url']; } ?>">
							<?php if(strlen($ans_cp['name'])>39){
								echo substr($ans_cp['name'],0,37)."...";
							}else{
								echo $ans_cp['name'];
							} ?></a> <?php if(isset($ans_cp['artist_id']))
											{
												$get_profile_type = $display->types_of_search('artist',$ans_cp['artist_id']);
												if(!empty($get_profile_type) && $get_profile_type!=Null)
												{
													$res_profile_type = mysql_fetch_assoc($get_profile_type);
													if($res_profile_type['type_name']!=""){
													?>
														<br/><span style="font-size:13px; font-weight:normal;">
														<?php 
														if(strlen($res_profile_type['type_name'])>26){
															echo substr($res_profile_type['type_name'],0,24)."...";
														}else{
															echo $res_profile_type['type_name'];
														}
														if($res_profile_type['subtype_name']!="" && strlen($res_profile_type['type_name']<26)){
															$type_length = strlen($res_profile_type['type_name']);
															if($type_length + strlen($res_profile_type['subtype_name'])>22){
																echo ", ".substr($res_profile_type['subtype_name'],0,22 - $type_length)."...";
															}else{
																echo ", ".$res_profile_type['subtype_name'];
															}
															
														}
														/*echo $res_profile_type['type_name'];
														if($res_profile_type['subtype_name']!=""){
															echo ", ".$res_profile_type['subtype_name'];
														}*/
														?>
														</span>
												<?php
													}
												}
											}
											elseif(isset($ans_cp['community_id']))
											{
												$get_profile_type = $display->types_of_search('community',$ans_cp['community_id']);
												if(!empty($get_profile_type) && $get_profile_type!=Null)
												{
													$res_profile_type = mysql_fetch_assoc($get_profile_type);
													if($res_profile_type['type_name']!=""){
													?>
														<br/><span style="font-size:13px; font-weight:normal;">
														<?php 
														if(strlen($res_profile_type['type_name'])>26){
															echo substr($res_profile_type['type_name'],0,24)."...";
														}else{
															echo $res_profile_type['type_name'];
														}
														if($res_profile_type['subtype_name']!="" && strlen($res_profile_type['type_name']<26)){
															$type_length = strlen($res_profile_type['type_name']);
															if($type_length + strlen($res_profile_type['subtype_name'])>22){
																echo ", ".substr($res_profile_type['subtype_name'],0,22 - $type_length)."...";
															}else{
																echo ", ".$res_profile_type['subtype_name'];
															}
															
														}
														/*echo $res_profile_type['type_name'];
														if($res_profile_type['subtype_name']!=""){
															echo ", ".$res_profile_type['subtype_name'];
														}*/
														?>
														</span>
												<?php
													}
												}
											} ?> </div></div>
						</div>
		<?php	
					}
					if($exp_car[0]=='general_community')
					{
		?>
						<div class="tabItem"> 
							<a href ="<?php if($ans_cp['profile_url']!="") { ?>/<?php echo $ans_cp['profile_url']; } ?>"><img class="imgs_pro" src="<?php if(isset($ans_cp['image_name']) && $ans_cp['image_name']!="") { echo 'https://comjcropprofile.s3.amazonaws.com/'.$ans_cp['image_name'] ; } else { echo 'https://comjcropprofile.s3.amazonaws.com/Noimage.png'; } ?>" width="200" height="200" /></a>
							<div class="both_wrap_per">
							<div class="player">
								<?php
								if($ans_cp['featured_media']=="Gallery" && $ans_cp['media_id']!=0)
								{
									$feature_reg_big_img = "";
									$all_register_images = "";
									$feature_reg_thum_path = "";
									$feature_reg_org_path = "";
									$featured_reg_gal_img_title = "";
							
									if($ans_cp['featured_media_table']!="" &&($ans_cp['featured_media_table']=="general_artist_gallery_list" || $ans_cp['featured_media_table']=="general_community_gallery_list"))
									{
										$get_register_featured_gallery = $new_profile_class_obj ->get_register_featured_media_gallery($ans_cp['media_id'],$ans_cp['featured_media_table']);
										$all_register_images ="";
										$feature_reg_big_img ="";
										$featured_reg_gal_img_title ="";
										for($cou_i=0;$cou_i<count($get_register_featured_gallery);$cou_i++)
										{
											$feature_reg_big_img = $get_register_featured_gallery[0]['image_name'];
											$all_register_images = $all_register_images.",".$get_register_featured_gallery[$cou_i]['image_name'];
											$featured_reg_gal_img_title = $featured_reg_gal_img_title .",".$get_register_featured_gallery[$cou_i]['image_title'];
										}
										$all_register_images = ltrim($all_register_images,',');
										$featured_reg_gal_img_title = ltrim($featured_reg_gal_img_title,',');
										$feature_reg_org_path = "https://reggallery.s3.amazonaws.com/";
										$feature_reg_thum_path = "https://reggalthumb.s3.amazonaws.com/";
									}
									else if($ans_cp['featured_media_table']!="" && $ans_cp['featured_media_table']=="general_media"){
										$get_register_featured_gallery = $new_profile_class_obj ->get_featured_gallery_images($ans_cp['media_id']);
										$all_register_images ="";
										$feature_reg_big_img ="";
										$featured_reg_gal_img_title ="";
										for($cou_i=0;$cou_i<count($get_register_featured_gallery);$cou_i++)
										{
											$feature_reg_big_img = $get_register_featured_gallery[0]['image_name'];
											$all_register_images = $all_register_images.",".$get_register_featured_gallery[$cou_i]['image_name'];
											$featured_reg_gal_img_title = $featured_reg_gal_img_title .",".$get_register_featured_gallery[$cou_i]['image_title'];
										}
										$all_register_images = ltrim($all_register_images,',');
										$featured_reg_gal_img_title = ltrim($featured_reg_gal_img_title,',');
										$feature_reg_org_path = "https://medgallery.s3.amazonaws.com/";
										$feature_reg_thum_path = "https://medgalthumb.s3.amazonaws.com/";
									}
									?>
									<a href="javascript:void(0);"><img src="../images/profile/play.gif" onmouseover="this.src='../images/profile/play-over.gif'" onmouseout="this.src='../images/profile/play.gif'" onclick="checkload('<?php echo $feature_reg_big_img;?>','<?php echo $all_register_images; ?>','<?php echo $feature_reg_thum_path; ?>','<?php echo $feature_reg_org_path; ?>','<?php echo $featured_reg_gal_img_title; ?>')" title="Play file now."/></a>
									<!--<a href="javascript:void(0);"><img src="../images/profile/add.gif"  onmouseover="this.src='../images/profile/add-over.gif'" onmouseout="this.src='../images/profile/add.gif'" title="Add file to the bottom of your playlist without stopping current media."/></a>-->
									<?php
									if($find_mem_butts != null && $find_mem_butts['general_user_id'] != null && ($find_mem_butts['expiry_date'] >$date || $find_mem_butts['lifetime']==1))
									{
										if($ans_cp['type']=='fan_club_yes')
										{
											if($_SESSION['login_email']==NULL || $_SESSION['login_email']!=$ans_butts['email'])
											{
										?>
											<a title="<?php if($ans_cp['type']=="fan_club_yes") { echo "Purchase a fan club membership to this users exclusive media."; } elseif($ans_cp['type']=="for_sale") { echo "Purchase this projects media."; } ?>" href="javascript:void(0);" onClick="fancy_login_art_coms('<?php echo $ans_cp['profile_url']; ?>')" ><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>
										<?php
											}
										}
									}
									?>
									<a class="fancybox fancybox.ajax" style="display:none;" href="fan_club.php?data=<?php echo $ans_cp['profile_url']; ?>" id="fan_club_pro_ca<?php echo $ans_cp['profile_url']; ?>">
											<?php if($ans_cp['type']=="fan_club_yes") { echo "Fan Club"; } elseif($ans_cp['type']=="for_sale") { echo "Download"; } ?></a>
											<script type="text/javascript">
											$(document).ready(function() {
													$("#fan_club_pro_ca<?php echo $ans_cp['profile_url']; ?>").fancybox({
													helpers : {
															media : {},
															buttons : {},
															title : null,
														},
													});
											});
											</script>
									<?php
								}
								else if($ans_cp['featured_media']=="Song" && $ans_cp['media_id']!=0)
								{
									if($ans_cp['featured_media_table']=="general_artist_audio" || $ans_cp['featured_media_table']=="general_community_audio")
									{
										?>
											<a href="javascript:void(0);"><img src="../images/profile/play.gif" onmouseover="this.src='../images/profile/play-over.gif'" onmouseout="this.src='../images/profile/play.gif'" onclick="showPlayer_reg('a','<?php echo $ans_cp['media_id'];?>','play_reg','<?php echo $ans_cp['featured_media_table'];?>')" title="Play file now."/></a>
											<a href="javascript:void(0);"><img src="../images/profile/add.gif"  onmouseover="this.src='../images/profile/add-over.gif'" onmouseout="this.src='../images/profile/add.gif'" onclick="showPlayer_reg('a','<?php echo $ans_cp['media_id'];?>','add_reg','<?php echo $ans_cp['featured_media_table'];?>')" title="Add file to the bottom of your playlist without stopping current media."/></a>
										<?php
									}
									else{
										?>
											<a href="javascript:void(0);"><img src="../images/profile/play.gif" onmouseover="this.src='../images/profile/play-over.gif'" onmouseout="this.src='../images/profile/play.gif'" onclick="showPlayer('a','<?php echo $ans_cp['media_id'];?>','play')" title="Play file now."/></a>
											<a href="javascript:void(0);"><img src="../images/profile/add.gif"  onmouseover="this.src='../images/profile/add-over.gif'" onmouseout="this.src='../images/profile/add.gif'" onclick="showPlayer('a','<?php echo $ans_cp['media_id'];?>','add')" title="Add file to the bottom of your playlist without stopping current media."/></a>
										<?php
									}
									if($find_mem_butts != null && $find_mem_butts['general_user_id'] != null && ($find_mem_butts['expiry_date'] >$date || $find_mem_butts['lifetime']==1))
									{
										if($ans_cp['type']=='fan_club_yes')
										{
											if($_SESSION['login_email']==NULL || $_SESSION['login_email']!=$ans_butts['email'])
											{
										?>
											<a title="<?php if($ans_cp['type']=="fan_club_yes") { echo "Purchase a fan club membership to this users exclusive media."; } elseif($ans_cp['type']=="for_sale") { echo "Purchase this projects media."; } ?>" href="javascript:void(0);" onClick="fancy_login_art_coms('<?php echo $ans_cp['profile_url']; ?>')" ><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>
										<?php
											}
										}
									}
									if($ans_cp['media_id']!="" && $ans_cp['media_id']!=" " && $ans_cp['media_id']!=0){
									$songtype_value_artist = $songtype_value_artist .",".$ans_cp['media_id'];
									}
									?>
									<a class="fancybox fancybox.ajax" style="display:none;" href="fan_club.php?data=<?php echo $ans_cp['profile_url']; ?>" id="fan_club_pro_ca<?php echo $ans_cp['profile_url']; ?>">
											<?php if($ans_cp['type']=="fan_club_yes") { echo "Fan Club"; } elseif($ans_cp['type']=="for_sale") { echo "Download"; } ?></a>
											<script type="text/javascript">
											$(document).ready(function() {
													$("#fan_club_pro_ca<?php echo $ans_cp['profile_url']; ?>").fancybox({
													helpers : {
															media : {},
															buttons : {},
															title : null,
														},
													});
											});
											</script>
									<?php
								}
								else if($ans_cp['featured_media']=="Video" && $ans_cp['media_id']!=0)
								{
									if($ans_cp['featured_media_table']=="general_artist_video" || $ans_cp['featured_media_table']=="general_community_video")
									{
										?>
											<a href="javascript:void(0);"><img src="../images/profile/play.gif" onmouseover="this.src='../images/profile/play-over.gif'" onmouseout="this.src='../images/profile/play.gif'" onclick="showPlayer_reg('v','<?php echo $ans_cp['media_id'];?>','playlist_reg','<?php echo $ans_cp['featured_media_table'];?>')" title="Play file now."/></a>
											<a href="javascript:void(0);"><img src="../images/profile/add.gif"  onmouseover="this.src='../images/profile/add-over.gif'" onmouseout="this.src='../images/profile/add.gif'" onclick="showPlayer_reg('v','<?php echo $ans_cp['media_id'];?>','addvi_reg','<?php echo $ans_cp['featured_media_table'];?>')" title="Add file to the bottom of your playlist without stopping current media."/></a>
										<?php
									}else{
										?>
											<a href="javascript:void(0);"><img src="../images/profile/play.gif" onmouseover="this.src='../images/profile/play-over.gif'" onmouseout="this.src='../images/profile/play.gif'" onclick="showPlayer('v','<?php echo $ans_cp['media_id'];?>','playlist')" title="Play file now."/></a>
											<a href="javascript:void(0);"><img src="../images/profile/add.gif"  onmouseover="this.src='../images/profile/add-over.gif'" onmouseout="this.src='../images/profile/add.gif'" onclick="showPlayer('v','<?php echo $ans_cp['media_id'];?>','addvi')" title="Add file to the bottom of your playlist without stopping current media."/></a>
										<?php
									}
									if($find_mem_butts != null && $find_mem_butts['general_user_id'] != null && ($find_mem_butts['expiry_date'] >$date || $find_mem_butts['lifetime']==1))
									{
										if($ans_cp['type']=='fan_club_yes')
										{
											if($_SESSION['login_email']==NULL || $_SESSION['login_email']!=$ans_butts['email'])
											{
										?>
											<a title="<?php if($ans_cp['type']=="fan_club_yes") { echo "Purchase a fan club membership to this users exclusive media."; } elseif($ans_cp['type']=="for_sale") { echo "Purchase this projects media."; } ?>" href="javascript:void(0);" onClick="fancy_login_art_coms('<?php echo $ans_cp['profile_url']; ?>')" ><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>
										<?php
											}
										}
									}
									if($ans_cp['media_id']!="" && $ans_cp['media_id']!=" " && $ans_cp['media_id']!=0){
										$videotype_value_artist = $videotype_value_artist .",".$ans_cp['media_id'];
									}
									?>
										<a class="fancybox fancybox.ajax" style="display:none;" href="fan_club.php?data=<?php echo $ans_cp['profile_url']; ?>" id="fan_club_pro_ca<?php echo $ans_cp['profile_url']; ?>">
											<?php if($ans_cp['type']=="fan_club_yes") { echo "Fan Club"; } elseif($ans_cp['type']=="for_sale") { echo "Download"; } ?></a>
											<script type="text/javascript">
											$(document).ready(function() {
													$("#fan_club_pro_ca<?php echo $ans_cp['profile_url']; ?>").fancybox({
													helpers : {
															media : {},
															buttons : {},
															title : null,
														},
													});
											});
											</script>
									<?php
								}
								else{
								if($find_mem_butts != null && $find_mem_butts['general_user_id'] != null && ($find_mem_butts['expiry_date'] >$date || $find_mem_butts['lifetime']==1))
									{
									if($ans_cp['type']=='fan_club_yes')
										{
											if($_SESSION['login_email']==NULL || $_SESSION['login_email']!=$ans_butts['email'])
											{
										?>
											<a title="<?php if($ans_cp['type']=="fan_club_yes") { echo "Purchase a fan club membership to this users exclusive media."; } elseif($ans_cp['type']=="for_sale") { echo "Purchase this projects media."; } ?>" href="javascript:void(0);" onClick="fancy_login_art_coms('<?php echo $ans_cp['profile_url']; ?>')" ><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>
										<?php
											}
										}
									}
							  ?>
							  <a class="fancybox fancybox.ajax" style="display:none;" href="fan_club.php?data=<?php echo $ans_cp['profile_url']; ?>" id="fan_club_pro_ca<?php echo $ans_cp['profile_url']; ?>">
											<?php if($ans_cp['type']=="fan_club_yes") { echo "Fan Club"; } elseif($ans_cp['type']=="for_sale") { echo "Download"; } ?></a>
											<script type="text/javascript">
											$(document).ready(function() {
													$("#fan_club_pro_ca<?php echo $ans_cp['profile_url']; ?>").fancybox({
													helpers : {
															media : {},
															buttons : {},
															title : null,
														},
													});
											});
											</script>
								<!--<a href="javascript:void(0);"><img src="../images/profile/play.gif" onmouseover="this.src='../images/profile/play-over.gif'" onmouseout="this.src='../images/profile/play.gif'" /></a>
								<a href="javascript:void(0);"><img src="../images/profile/add.gif"  onmouseover="this.src='../images/profile/add-over.gif'" onmouseout="this.src='../images/profile/add.gif'"/></a>-->
								<?php
								}
								?>
								<!--<img src="images/profile/download.gif" onmouseover="this.src='images/profile/download-over.gif'" onmouseout="this.src='images/profile/download.gif'" />-->
							</div>
							<div class="type_subtype"><a href ="<?php if($ans_cp['profile_url']!="") { ?>/<?php echo $ans_cp['profile_url']; } ?>">
							<?php if(strlen($ans_cp['name'])>39){
								echo substr($ans_cp['name'],0,37)."...";
							}else{
								echo $ans_cp['name'];
							} ?></a><?php if(isset($ans_cp['artist_id']))
											{
												$get_profile_type = $display->types_of_search('artist',$ans_cp['artist_id']);
												if(!empty($get_profile_type) && $get_profile_type!=Null)
												{
													$res_profile_type = mysql_fetch_assoc($get_profile_type);
													if($res_profile_type['type_name']!=""){
													?>
														<br/><span style="font-size:13px; font-weight:normal;">
														<?php 
														if(strlen($res_profile_type['type_name'])>26){
															echo substr($res_profile_type['type_name'],0,24)."...";
														}else{
															echo $res_profile_type['type_name'];
														}
														if($res_profile_type['subtype_name']!="" && strlen($res_profile_type['type_name']<26)){
															$type_length = strlen($res_profile_type['type_name']);
															if($type_length + strlen($res_profile_type['subtype_name'])>22){
																echo ", ".substr($res_profile_type['subtype_name'],0,22 - $type_length)."...";
															}else{
																echo ", ".$res_profile_type['subtype_name'];
															}
															
														}
														/*echo $res_profile_type['type_name'];
														if($res_profile_type['subtype_name']!=""){
															echo ", ".$res_profile_type['subtype_name'];
														}*/
														?>
														</span>
												<?php
													}
												}
											}
											elseif(isset($ans_cp['community_id']))
											{
												$get_profile_type = $display->types_of_search('community',$ans_cp['community_id']);
												if(!empty($get_profile_type) && $get_profile_type!=Null)
												{
													$res_profile_type = mysql_fetch_assoc($get_profile_type);
													if($res_profile_type['type_name']!=""){
													?>
														<br/><span style="font-size:13px; font-weight:normal;">
														<?php 
														if(strlen($res_profile_type['type_name'])>26){
															echo substr($res_profile_type['type_name'],0,24)."...";
														}else{
															echo $res_profile_type['type_name'];
														}
														if($res_profile_type['subtype_name']!="" && strlen($res_profile_type['type_name']<26)){
															$type_length = strlen($res_profile_type['type_name']);
															if($type_length + strlen($res_profile_type['subtype_name'])>22){
																echo ", ".substr($res_profile_type['subtype_name'],0,22 - $type_length)."...";
															}else{
																echo ", ".$res_profile_type['subtype_name'];
															}
															
														}
														/*echo $res_profile_type['type_name'];
														if($res_profile_type['subtype_name']!=""){
															echo ", ".$res_profile_type['subtype_name'];
														}*/
														?>
														</span>
												<?php
													}
												}
											} ?> </div></div>
						</div>
		<?php	
					}
					if($exp_car[0]=='artist_project' || $exp_car[0]=='community_project')
					{
						$ans_cret = $ans_cp['creators_info'];
						
						$exp1_car = explode('|',$ans_cret);
						if($ans_cret!="")
						{
							if($exp1_car[0]=='general_artist')
							{
								$sql1__ar = mysql_query("SELECT * FROM general_artist WHERE artist_id='".$exp1_car[1]."'");
							}
							if($exp1_car[0]=='general_community')
							{
								$sql1__ar = mysql_query("SELECT * FROM general_community WHERE community_id='".$exp1_car[1]."'");
							}
							if($exp1_car[0]=='artist_project' || $exp1_car[0]=='community_project')
							{
								$sql1__ar = mysql_query("SELECT * FROM $exp1_car[0] WHERE id='".$exp1_car[1]."'");
							}
						}
						
						if(isset($sql1__ar))
						{
							if($sql1__ar!="" && $sql1__ar!=Null)
							{
								if(mysql_num_rows($sql1__ar)>0)
								{
									$ans1_cp = mysql_fetch_assoc($sql1__ar);
								}
							}
						}
						
						if(!isset($ans1_cp))
						{
							$ans1_cp['profile_url'] = $ans_cp['profile_url'];
						}
						
						if($ans_cp['profile_url']==$ans1_cp['profile_url'])
						{
							
		?>
						<div class="tabItem"> 
							<a href ="<?php if($ans_cp['profile_url']!="") { ?>/<?php echo $ans_cp['profile_url']; } ?>"><img class="imgs_pro" src="<?php if(isset($ans_cp['image_name']) && $ans_cp['image_name']!="") { echo $ans_cp['image_name'] ; } else { echo 'https://artjcropprofile.s3.amazonaws.com/Noimage.png'; } ?>" width="200" height="200" /></a>
							<div class="both_wrap_per">
							<div class="player">
								<?php
								if($ans_cp['featured_media']=="Gallery" && $ans_cp['media_id']!=0)
								{
									$feature_reg_big_img = "";
									$all_register_images = "";
									$feature_reg_thum_path = "";
									$feature_reg_org_path = "";
									$featured_reg_gal_img_title = "";
									
									if($ans_cp['featured_media_table']!="" &&($ans_cp['featured_media_table']=="general_artist_gallery_list" || $ans_cp['featured_media_table']=="general_community_gallery_list"))
									{
										$get_register_featured_gallery = $new_profile_class_obj ->get_register_featured_media_gallery($ans_cp['media_id'],$ans_cp['featured_media_table']);
										$all_register_images ="";
										$feature_reg_big_img ="";
										$featured_reg_gal_img_title ="";
										for($cou_i=0;$cou_i<count($get_register_featured_gallery);$cou_i++)
										{
											$feature_reg_big_img = $get_register_featured_gallery[0]['image_name'];
											$all_register_images = $all_register_images.",".$get_register_featured_gallery[$cou_i]['image_name'];
											$featured_reg_gal_img_title = $featured_reg_gal_img_title .",".$get_register_featured_gallery[$cou_i]['image_title'];
										}
										$all_register_images = ltrim($all_register_images,',');
										$featured_reg_gal_img_title = ltrim($featured_reg_gal_img_title,',');
										$feature_reg_org_path = "https://reggallery.s3.amazonaws.com/";
										$feature_reg_thum_path = "https://reggalthumb.s3.amazonaws.com/";
									}
									else if($ans_cp['featured_media_table']!="" && $ans_cp['featured_media_table']=="general_media"){
										$get_register_featured_gallery = $new_profile_class_obj ->get_featured_gallery_images($ans_cp['media_id']);
										$all_register_images ="";
										$feature_reg_big_img ="";
										$featured_reg_gal_img_title ="";
										for($cou_i=0;$cou_i<count($get_register_featured_gallery);$cou_i++)
										{
											$feature_reg_big_img = $get_register_featured_gallery[0]['image_name'];
											$all_register_images = $all_register_images.",".$get_register_featured_gallery[$cou_i]['image_name'];
											$featured_reg_gal_img_title = $featured_reg_gal_img_title .",".$get_register_featured_gallery[$cou_i]['image_title'];
										}
										$all_register_images = ltrim($all_register_images,',');
										$featured_reg_gal_img_title = ltrim($featured_reg_gal_img_title,',');
										$feature_reg_org_path = "https://medgallery.s3.amazonaws.com/";
										$feature_reg_thum_path = "https://medgalthumb.s3.amazonaws.com/";
									}
									?>
									<a href="javascript:void(0);"><img src="../images/profile/play.gif" onmouseover="this.src='../images/profile/play-over.gif'" onmouseout="this.src='../images/profile/play.gif'" onclick="checkload('<?php echo $feature_reg_big_img;?>','<?php echo $all_register_images; ?>','<?php echo $feature_reg_thum_path; ?>','<?php echo $feature_reg_org_path; ?>','<?php echo $featured_reg_gal_img_title; ?>')" title="Play file now."/></a>
									<!--<a href="javascript:void(0);"><img src="../images/profile/add.gif"  onmouseover="this.src='../images/profile/add-over.gif'" onmouseout="this.src='../images/profile/add.gif'" title="Add file to the bottom of your playlist without stopping current media."/></a>-->
									<?php
									if($find_mem_butts != null && $find_mem_butts['general_user_id'] != null && ($find_mem_butts['expiry_date'] >$date || $find_mem_butts['lifetime']==1))
									{
										if($ans_cp['type']=="fan_club" || $ans_cp['type']=="for_sale" || $ans_cp['type']=="fan_club_yes")
										{
											if($_SESSION['login_email']==NULL || $_SESSION['login_email']!=$ans_butts['email'])
											{
										?>
											
											<a href="javascript:void(0);" onClick="<?php if($ans_cp['type']=="fan_club") { echo "fancy_login_single('".$ans_cp['profile_url']."')"; } elseif($ans_cp['type']=="for_sale") { echo "click_fancy_login_down_single('".$ans_cp['profile_url']."')"; } ?>"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>
										<?php
											}
										}
									}
									?>
									 <a href="fan_club.php?data=<?php echo $ans_cp['profile_url'];?>" id="fan_club_pro<?php echo $ans_cp['profile_url'];?>" style="display:none;" ><?php if($ans_cp['type']=="fan_club") { echo "Fan Club"; } elseif($ans_cp['type']=="for_sale") { echo "Purchase"; } ?></a>
											<script type="text/javascript">
											$(document).ready(function() {
													$("#fan_club_pro<?php echo $ans_cp['profile_url'];?>").fancybox({
													'width'				: '69%',
													'height'			: '80%',
													'transitionIn'		: 'none',
													'transitionOut'		: 'none',
													'type'				: 'iframe'
													});
											});
											</script>
									<?php	
								}
								else if($ans_cp['featured_media']=="Song" && $ans_cp['media_id']!=0)
								{
									if($ans_cp['featured_media_table']=="general_artist_audio" || $ans_cp['featured_media_table']=="general_community_audio")
									{
										?>
											<a href="javascript:void(0);"><img src="../images/profile/play.gif" onmouseover="this.src='../images/profile/play-over.gif'" onmouseout="this.src='../images/profile/play.gif'" onclick="showPlayer_reg('a','<?php echo $ans_cp['media_id'];?>','play_reg','<?php echo $ans_cp['featured_media_table'];?>')" title="Play file now."/></a>
											<a href="javascript:void(0);"><img src="../images/profile/add.gif"  onmouseover="this.src='../images/profile/add-over.gif'" onmouseout="this.src='../images/profile/add.gif'" onclick="showPlayer_reg('a','<?php echo $ans_cp['media_id'];?>','add_reg','<?php echo $ans_cp['featured_media_table'];?>')" title="Add file to the bottom of your playlist without stopping current media."/></a>
										<?php
									}
									else{
										?>
											<a href="javascript:void(0);"><img src="../images/profile/play.gif" onmouseover="this.src='../images/profile/play-over.gif'" onmouseout="this.src='../images/profile/play.gif'" onclick="showPlayer('a','<?php echo $ans_cp['media_id'];?>','play')" title="Play file now."/></a>
											<a href="javascript:void(0);"><img src="../images/profile/add.gif"  onmouseover="this.src='../images/profile/add-over.gif'" onmouseout="this.src='../images/profile/add.gif'" onclick="showPlayer('a','<?php echo $ans_cp['media_id'];?>','add')" title="Add file to the bottom of your playlist without stopping current media."/></a>
										<?php
									}
									if($find_mem_butts != null && $find_mem_butts['general_user_id'] != null && ($find_mem_butts['expiry_date'] >$date || $find_mem_butts['lifetime']==1))
									{
										if($ans_cp['type']=="fan_club" || $ans_cp['type']=="for_sale" || $ans_cp['type']=="fan_club_yes")
										{
											if($_SESSION['login_email']==NULL || $_SESSION['login_email']!=$ans_butts['email'])
											{
										?>
											<a href="javascript:void(0);" onClick="<?php if($ans_cp['type']=="fan_club") { echo "fancy_login_single('".$ans_cp['profile_url']."')"; } elseif($ans_cp['type']=="for_sale") { echo "click_fancy_login_down_single('".$ans_cp['profile_url']."')"; } ?>"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>
										<?php
											}
										}
									}
									if($ans_cp['media_id']!="" && $ans_cp['media_id']!=" " && $ans_cp['media_id']!=0){
										$songtype_value_artist = $songtype_value_artist .",".$ans_cp['media_id'];
									}
									?>
									 <a href="fan_club.php?data=<?php echo $ans_cp['profile_url'];?>" id="fan_club_pro<?php echo $ans_cp['profile_url'];?>" style="display:none;" ><?php if($ans_cp['type']=="fan_club") { echo "Fan Club"; } elseif($ans_cp['type']=="for_sale") { echo "Purchase"; } ?></a>
											<script type="text/javascript">
											$(document).ready(function() {
													$("#fan_club_pro<?php echo $ans_cp['profile_url'];?>").fancybox({
													'width'				: '69%',
													'height'			: '80%',
													'transitionIn'		: 'none',
													'transitionOut'		: 'none',
													'type'				: 'iframe'
													});
											});
											</script>
									<?php
								}
								else if($ans_cp['featured_media']=="Video" && $ans_cp['media_id']!=0)
								{
									if($ans_cp['featured_media_table']=="general_artist_video" || $ans_cp['featured_media_table']=="general_community_video")
									{
										?>
											<a href="javascript:void(0);"><img src="../images/profile/play.gif" onmouseover="this.src='../images/profile/play-over.gif'" onmouseout="this.src='../images/profile/play.gif'" onclick="showPlayer_reg('v','<?php echo $ans_cp['media_id'];?>','playlist_reg','<?php echo $ans_cp['featured_media_table'];?>')" title="Play file now."/></a>
											<a href="javascript:void(0);"><img src="../images/profile/add.gif"  onmouseover="this.src='../images/profile/add-over.gif'" onmouseout="this.src='../images/profile/add.gif'" onclick="showPlayer_reg('v','<?php echo $ans_cp['media_id'];?>','addvi_reg','<?php echo $ans_cp['featured_media_table'];?>')" title="Add file to the bottom of your playlist without stopping current media."/></a>
										<?php
									}else{
										?>
											<a href="javascript:void(0);"><img src="../images/profile/play.gif" onmouseover="this.src='../images/profile/play-over.gif'" onmouseout="this.src='../images/profile/play.gif'" onclick="showPlayer('v','<?php echo $ans_cp['media_id'];?>','playlist')" title="Play file now."/></a>
											<a href="javascript:void(0);"><img src="../images/profile/add.gif"  onmouseover="this.src='../images/profile/add-over.gif'" onmouseout="this.src='../images/profile/add.gif'" onclick="showPlayer('v','<?php echo $ans_cp['media_id'];?>','addvi')" title="Add file to the bottom of your playlist without stopping current media."/></a>
										<?php
									}
									if($find_mem_butts != null && $find_mem_butts['general_user_id'] != null && ($find_mem_butts['expiry_date'] >$date || $find_mem_butts['lifetime']==1))
									{
										if($ans_cp['type']=="fan_club" || $ans_cp['type']=="for_sale" || $ans_cp['type']=="fan_club_yes")
										{
											if($_SESSION['login_email']==NULL || $_SESSION['login_email']!=$ans_butts['email'])
											{
										?>
											<a href="javascript:void(0);" onClick="<?php if($ans_cp['type']=="fan_club") { echo "fancy_login_single('".$ans_cp['profile_url']."')"; } elseif($ans_cp['type']=="for_sale") { echo "click_fancy_login_down_single('".$ans_cp['profile_url']."')"; } ?>"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>
										<?php
											}
										}
									}
									if($ans_cp['media_id']!="" && $ans_cp['media_id']!=" " && $ans_cp['media_id']!=0){
										$videotype_value_artist = $videotype_value_artist .",".$ans_cp['media_id'];
									}
									?>
									<a href="fan_club.php?data=<?php echo $ans_cp['profile_url'];?>" id="fan_club_pro<?php echo $ans_cp['profile_url'];?>" style="display:none;" ><?php if($ans_cp['type']=="fan_club") { echo "Fan Club"; } elseif($ans_cp['type']=="for_sale") { echo "Purchase"; } ?></a>
											<script type="text/javascript">
											$(document).ready(function() {
													$("#fan_club_pro<?php echo $ans_cp['profile_url'];?>").fancybox({
													'width'				: '69%',
													'height'			: '80%',
													'transitionIn'		: 'none',
													'transitionOut'		: 'none',
													'type'				: 'iframe'
													});
											});
											</script>
									<?php
								}
								else{
								if($find_mem_butts != null && $find_mem_butts['general_user_id'] != null && ($find_mem_butts['expiry_date'] >$date || $find_mem_butts['lifetime']==1))
									{
									if($ans_cp['type']=="fan_club" || $ans_cp['type']=="for_sale" || $ans_cp['type']=="fan_club_yes")
										{
											if($_SESSION['login_email']==NULL || $_SESSION['login_email']!=$ans_butts['email'])
											{
										?>
											<a href="javascript:void(0);" onClick="<?php if($ans_cp['type']=="fan_club") { echo "fancy_login_single('".$ans_cp['profile_url']."')"; } elseif($ans_cp['type']=="for_sale") { echo "click_fancy_login_down_single('".$ans_cp['profile_url']."')"; } ?>"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>
										<?php
											}
										}
									}
							  ?>
							  <a href="fan_club.php?data=<?php echo $ans_cp['profile_url'];?>" id="fan_club_pro<?php echo $ans_cp['profile_url'];?>" style="display:none;" ><?php if($ans_cp['type']=="fan_club") { echo "Fan Club"; } elseif($ans_cp['type']=="for_sale") { echo "Purchase"; } ?></a>
											<script type="text/javascript">
											$(document).ready(function() {
													$("#fan_club_pro<?php echo $ans_cp['profile_url'];?>").fancybox({
													'width'				: '69%',
													'height'			: '80%',
													'transitionIn'		: 'none',
													'transitionOut'		: 'none',
													'type'				: 'iframe'
													});
											});
											</script>
								<!--<a href="javascript:void(0);"><img src="../images/profile/play.gif" onmouseover="this.src='../images/profile/play-over.gif'" onmouseout="this.src='../images/profile/play.gif'" /></a>
								<a href="javascript:void(0);"><img src="../images/profile/add.gif"  onmouseover="this.src='../images/profile/add-over.gif'" onmouseout="this.src='../images/profile/add.gif'"/></a>-->
								<?php
								}
								?>
								<!--<img src="images/profile/download.gif" onmouseover="this.src='images/profile/download-over.gif'" onmouseout="this.src='images/profile/download.gif'" />-->
							</div>
							<div class="type_subtype"><a href ="<?php if($ans_cp['profile_url']!="") { ?>/<?php echo $ans_cp['profile_url']; } ?>">
							<?php if(strlen($ans_cp['title'])>39){
								echo substr($ans_cp['title'],0,37)."...";
							}else{
								echo $ans_cp['title'];
							} ?></a><?php if(isset($ans_cp['artist_id']))
												{
													$get_profile_type = $display->types_of_search('artist_project',$ans_cp['id']);
													if(!empty($get_profile_type) && $get_profile_type!=Null)
													{
														$res_profile_type = mysql_fetch_assoc($get_profile_type);
														if($res_profile_type['type_name']!=""){
														?>
															<br/><span style="font-size:13px; font-weight:normal;">
															<?php 
															if(strlen($res_profile_type['type_name'])>26){
																echo substr($res_profile_type['type_name'],0,24)."...";
															}else{
																echo $res_profile_type['type_name'];
															}
															if($res_profile_type['subtype_name']!="" && strlen($res_profile_type['type_name']<26)){
																$type_length = strlen($res_profile_type['type_name']);
																if($type_length + strlen($res_profile_type['subtype_name'])>22){
																	echo ", ".substr($res_profile_type['subtype_name'],0,22 - $type_length)."...";
																}else{
																	echo ", ".$res_profile_type['subtype_name'];
																}
																
															}
															/*echo $res_profile_type['type_name'];
															if($res_profile_type['subtype_name']!=""){
																echo ", ".$res_profile_type['subtype_name'];
															}*/
															?>
															</span>
													<?php
														}
													}
												}
												elseif(isset($ans_cp['community_id']))
												{
													$get_profile_type = $display->types_of_search('community_project',$ans_cp['id']);
													if(!empty($get_profile_type) && $get_profile_type!=Null)
													{
														$res_profile_type = mysql_fetch_assoc($get_profile_type);
														if($res_profile_type['type_name']!=""){
														?>
															<br/><span style="font-size:13px; font-weight:normal;">
															<?php 
															if(strlen($res_profile_type['type_name'])>26){
																echo substr($res_profile_type['type_name'],0,24)."...";
															}else{
																echo $res_profile_type['type_name'];
															}
															if($res_profile_type['subtype_name']!="" && strlen($res_profile_type['type_name']<26)){
																$type_length = strlen($res_profile_type['type_name']);
																if($type_length + strlen($res_profile_type['subtype_name'])>22){
																	echo ", ".substr($res_profile_type['subtype_name'],0,22 - $type_length)."...";
																}else{
																	echo ", ".$res_profile_type['subtype_name'];
																}
																
															}
															/*echo $res_profile_type['type_name'];
															if($res_profile_type['subtype_name']!=""){
																echo ", ".$res_profile_type['subtype_name'];
															}*/
															?>
															</span>
													<?php
														}
													}
												} ?> </div></div>
						</div>
		<?php
						}
						else
						{
		?>
							<div class="tabItem"> 
								<a href ="<?php if($ans_cp['profile_url']!="") { ?>/<?php echo $ans_cp['profile_url']; } ?>"><img class="imgs_pro" src="<?php if(isset($ans_cp['image_name']) && $ans_cp['image_name']!="") { echo $ans_cp['image_name'] ; } else { echo 'https://artjcropprofile.s3.amazonaws.com/Noimage.png'; } ?>" width="200" height="200" /></a>
								<div class="both_wrap_per">
								<div class="player">
									<?php
									if($ans_cp['featured_media']=="Gallery" && $ans_cp['media_id']!=0)
									{
										$feature_reg_big_img = "";
										$all_register_images = "";
										$feature_reg_thum_path = "";
										$feature_reg_org_path = "";
										$featured_reg_gal_img_title = "";
									
										if($ans_cp['featured_media_table']!="" &&($ans_cp['featured_media_table']=="general_artist_gallery_list" || $ans_cp['featured_media_table']=="general_community_gallery_list"))
										{
											$get_register_featured_gallery = $new_profile_class_obj ->get_register_featured_media_gallery($ans_cp['media_id'],$ans_cp['featured_media_table']);
											$all_register_images ="";
											$feature_reg_big_img ="";
											$featured_reg_gal_img_title ="";
											for($cou_i=0;$cou_i<count($get_register_featured_gallery);$cou_i++)
											{
												$feature_reg_big_img = $get_register_featured_gallery[0]['image_name'];
												$all_register_images = $all_register_images.",".$get_register_featured_gallery[$cou_i]['image_name'];
												$featured_reg_gal_img_title = $featured_reg_gal_img_title .",".$get_register_featured_gallery[$cou_i]['image_title'];
											}
											$all_register_images = ltrim($all_register_images,',');
											$featured_reg_gal_img_title = ltrim($featured_reg_gal_img_title,',');
											$feature_reg_org_path = "https://reggallery.s3.amazonaws.com/";
											$feature_reg_thum_path = "https://reggalthumb.s3.amazonaws.com/";
										}
										else if($ans_cp['featured_media_table']!="" && $ans_cp['featured_media_table']=="general_media"){
											$get_register_featured_gallery = $new_profile_class_obj ->get_featured_gallery_images($ans_cp['media_id']);
											$all_register_images ="";
											$feature_reg_big_img ="";
											$featured_reg_gal_img_title ="";
											for($cou_i=0;$cou_i<count($get_register_featured_gallery);$cou_i++)
											{
												$feature_reg_big_img = $get_register_featured_gallery[0]['image_name'];
												$all_register_images = $all_register_images.",".$get_register_featured_gallery[$cou_i]['image_name'];
												$featured_reg_gal_img_title = $featured_reg_gal_img_title .",".$get_register_featured_gallery[$cou_i]['image_title'];
											}
											$all_register_images = ltrim($all_register_images,',');
											$featured_reg_gal_img_title = ltrim($featured_reg_gal_img_title,',');
											$feature_reg_org_path = "https://medgallery.s3.amazonaws.com/";
											$feature_reg_thum_path = "https://medgalthumb.s3.amazonaws.com/";
										}
									?>
									<a href="javascript:void(0);"><img src="../images/profile/play.gif" onmouseover="this.src='../images/profile/play-over.gif'" onmouseout="this.src='../images/profile/play.gif'" onclick="checkload('<?php echo $feature_reg_big_img;?>','<?php echo $all_register_images; ?>','<?php echo $feature_reg_thum_path; ?>','<?php echo $feature_reg_org_path; ?>','<?php echo $featured_reg_gal_img_title; ?>')" title="Play file now."/></a>
									<!--<a href="javascript:void(0);"><img src="../images/profile/add.gif"  onmouseover="this.src='../images/profile/add-over.gif'" onmouseout="this.src='../images/profile/add.gif'" title="Add file to the bottom of your playlist without stopping current media."/></a>-->
									<?php
									if($find_mem_butts != null && $find_mem_butts['general_user_id'] != null && ($find_mem_butts['expiry_date'] >$date || $find_mem_butts['lifetime']==1))
									{
											if($ans_cp['type']=="fan_club" || $ans_cp['type']=="for_sale" || $ans_cp['type']=="fan_club_yes")
										{
											if($_SESSION['login_email']==NULL || $_SESSION['login_email']!=$ans_butts['email'])
											{
										?>
											<a href="javascript:void(0);" onClick="<?php if($ans_cp['type']=="fan_club") { echo "fancy_login_single('".$ans_cp['profile_url']."')"; } elseif($ans_cp['type']=="for_sale") { echo "click_fancy_login_down_single('".$ans_cp['profile_url']."')"; } ?>"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>
										<?php
											}
										}
									}
										?>
										 <a href="fan_club.php?data=<?php echo $ans_cp['profile_url'];?>" id="fan_club_pro<?php echo $ans_cp['profile_url'];?>" style="display:none;" ><?php if($ans_cp['type']=="fan_club") { echo "Fan Club"; } elseif($ans_cp['type']=="for_sale") { echo "Purchase"; } ?></a>
											<script type="text/javascript">
											$(document).ready(function() {
													$("#fan_club_pro<?php echo $ans_cp['profile_url'];?>").fancybox({
													'width'				: '69%',
													'height'			: '80%',
													'transitionIn'		: 'none',
													'transitionOut'		: 'none',
													'type'				: 'iframe'
													});
											});
											</script>
										<?php
									}
									else if($ans_cp['featured_media']=="Song" && $ans_cp['media_id']!=0)
									{
										if($ans_cp['featured_media_table']=="general_artist_audio" || $ans_cp['featured_media_table']=="general_community_audio")
										{
											?>
												<a href="javascript:void(0);"><img src="../images/profile/play.gif" onmouseover="this.src='../images/profile/play-over.gif'" onmouseout="this.src='../images/profile/play.gif'" onclick="showPlayer_reg('a','<?php echo $ans_cp['media_id'];?>','play_reg','<?php echo $ans_cp['featured_media_table'];?>')" title="Play file now."/></a>
												<a href="javascript:void(0);"><img src="../images/profile/add.gif"  onmouseover="this.src='../images/profile/add-over.gif'" onmouseout="this.src='../images/profile/add.gif'" onclick="showPlayer_reg('a','<?php echo $ans_cp['media_id'];?>','add_reg','<?php echo $ans_cp['featured_media_table'];?>')" title="Add file to the bottom of your playlist without stopping current media."/></a>
											<?php
										}
										else{
											?>
												<a href="javascript:void(0);"><img src="../images/profile/play.gif" onmouseover="this.src='../images/profile/play-over.gif'" onmouseout="this.src='../images/profile/play.gif'" onclick="showPlayer('a','<?php echo $ans_cp['media_id'];?>','play')" title="Play file now."/></a>
												<a href="javascript:void(0);"><img src="../images/profile/add.gif"  onmouseover="this.src='../images/profile/add-over.gif'" onmouseout="this.src='../images/profile/add.gif'" onclick="showPlayer('a','<?php echo $ans_cp['media_id'];?>','add')" title="Add file to the bottom of your playlist without stopping current media."/></a>
											<?php
										}
										if($find_mem_butts != null && $find_mem_butts['general_user_id'] != null && ($find_mem_butts['expiry_date'] >$date || $find_mem_butts['lifetime']==1))
									{
											if($ans_cp['type']=="fan_club" || $ans_cp['type']=="for_sale" || $ans_cp['type']=="fan_club_yes")
										{
											if($_SESSION['login_email']==NULL || $_SESSION['login_email']!=$ans_butts['email'])
											{
										?>
											<a href="javascript:void(0);" onClick="<?php if($ans_cp['type']=="fan_club") { echo "fancy_login_single('".$ans_cp['profile_url']."')"; } elseif($ans_cp['type']=="for_sale") { echo "click_fancy_login_down_single('".$ans_cp['profile_url']."')"; } ?>"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>
										<?php
											}
										}
									}
										if($ans_cp['media_id']!="" && $ans_cp['media_id']!=" " && $ans_cp['media_id']!=0)
										{
											$songtype_value_artist = $songtype_value_artist .",".$ans_cp['media_id'];
										}
										?>
										 <a href="fan_club.php?data=<?php echo $ans_cp['profile_url'];?>" id="fan_club_pro<?php echo $ans_cp['profile_url'];?>" style="display:none;" ><?php if($ans_cp['type']=="fan_club") { echo "Fan Club"; } elseif($ans_cp['type']=="for_sale") { echo "Purchase"; } ?></a>
											<script type="text/javascript">
											$(document).ready(function() {
													$("#fan_club_pro<?php echo $ans_cp['profile_url'];?>").fancybox({
													'width'				: '69%',
													'height'			: '80%',
													'transitionIn'		: 'none',
													'transitionOut'		: 'none',
													'type'				: 'iframe'
													});
											});
											</script>
										<?php
									}
									else if($ans_cp['featured_media']=="Video" && $ans_cp['media_id']!=0)
									{
										if($ans_cp['featured_media_table']=="general_artist_video" || $ans_cp['featured_media_table']=="general_community_video")
										{
											?>
												<a href="javascript:void(0);"><img src="../images/profile/play.gif" onmouseover="this.src='../images/profile/play-over.gif'" onmouseout="this.src='../images/profile/play.gif'" onclick="showPlayer_reg('v','<?php echo $ans_cp['media_id'];?>','playlist_reg','<?php echo $ans_cp['featured_media_table'];?>')" title="Play file now."/></a>
												<a href="javascript:void(0);"><img src="../images/profile/add.gif"  onmouseover="this.src='../images/profile/add-over.gif'" onmouseout="this.src='../images/profile/add.gif'" onclick="showPlayer_reg('v','<?php echo $ans_cp['media_id'];?>','addvi_reg','<?php echo $ans_cp['featured_media_table'];?>')" title="Add file to the bottom of your playlist without stopping current media."/></a>
											<?php
										}else{
											?>
												<a href="javascript:void(0);"><img src="../images/profile/play.gif" onmouseover="this.src='../images/profile/play-over.gif'" onmouseout="this.src='../images/profile/play.gif'" onclick="showPlayer('v','<?php echo $ans_cp['media_id'];?>','playlist')" title="Play file now."/></a>
												<a href="javascript:void(0);"><img src="../images/profile/add.gif"  onmouseover="this.src='../images/profile/add-over.gif'" onmouseout="this.src='../images/profile/add.gif'" onclick="showPlayer('v','<?php echo $ans_cp['media_id'];?>','addvi')" title="Add file to the bottom of your playlist without stopping current media."/></a>
											<?php
										}
										if($find_mem_butts != null && $find_mem_butts['general_user_id'] != null && ($find_mem_butts['expiry_date'] >$date || $find_mem_butts['lifetime']==1))
									{
											if($ans_cp['type']=="fan_club" || $ans_cp['type']=="for_sale" || $ans_cp['type']=="fan_club_yes")
										{
											if($_SESSION['login_email']==NULL || $_SESSION['login_email']!=$ans_butts['email'])
											{
										?>
											<a href="javascript:void(0);" onClick="<?php if($ans_cp['type']=="fan_club") { echo "fancy_login_single('".$ans_cp['profile_url']."')"; } elseif($ans_cp['type']=="for_sale") { echo "click_fancy_login_down_single('".$ans_cp['profile_url']."')"; } ?>"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>
										<?php
											}
										}
									}
										if($ans_cp['media_id']!="" && $ans_cp['media_id']!=" " && $ans_cp['media_id']!=0){
										$videotype_value_artist = $videotype_value_artist .",".$ans_cp['media_id'];
										}
										?>
										<a href="fan_club.php?data=<?php echo $ans_cp['profile_url'];?>" id="fan_club_pro<?php echo $ans_cp['profile_url'];?>" style="display:none;" ><?php if($ans_cp['type']=="fan_club") { echo "Fan Club"; } elseif($ans_cp['type']=="for_sale") { echo "Purchase"; } ?></a>
											<script type="text/javascript">
											$(document).ready(function() {
													$("#fan_club_pro<?php echo $ans_cp['profile_url'];?>").fancybox({
													'width'				: '69%',
													'height'			: '80%',
													'transitionIn'		: 'none',
													'transitionOut'		: 'none',
													'type'				: 'iframe'
													});
											});
											</script>
										<?php
									}
									else{
									if($find_mem_butts != null && $find_mem_butts['general_user_id'] != null && ($find_mem_butts['expiry_date'] >$date || $find_mem_butts['lifetime']==1))
									{
										if($ans_cp['type']=="fan_club" || $ans_cp['type']=="for_sale" || $ans_cp['type']=="fan_club_yes")
										{
											if($_SESSION['login_email']==NULL || $_SESSION['login_email']!=$ans_butts['email'])
											{
										?>
											<a href="javascript:void(0);" onClick="<?php if($ans_cp['type']=="fan_club") { echo "fancy_login_single('".$ans_cp['profile_url']."')"; } elseif($ans_cp['type']=="for_sale") { echo "click_fancy_login_down_single('".$ans_cp['profile_url']."')"; } ?>"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>
										<?php
											}
										}
									}
								  ?>
								  <a href="fan_club.php?data=<?php echo $ans_cp['profile_url'];?>" id="fan_club_pro<?php echo $ans_cp['profile_url'];?>" style="display:none;" ><?php if($ans_cp['type']=="fan_club") { echo "Fan Club"; } elseif($ans_cp['type']=="for_sale") { echo "Purchase"; } ?></a>
											<script type="text/javascript">
											$(document).ready(function() {
													$("#fan_club_pro<?php echo $ans_cp['profile_url'];?>").fancybox({
													'width'				: '69%',
													'height'			: '80%',
													'transitionIn'		: 'none',
													'transitionOut'		: 'none',
													'type'				: 'iframe'
													});
											});
											</script>
									<!--<a href="javascript:void(0);"><img src="../images/profile/play.gif" onmouseover="this.src='../images/profile/play-over.gif'" onmouseout="this.src='../images/profile/play.gif'" /></a>
									<a href="javascript:void(0);"><img src="../images/profile/add.gif"  onmouseover="this.src='../images/profile/add-over.gif'" onmouseout="this.src='../images/profile/add.gif'"/></a>-->
									<?php
									}
									?>
									<!--<img src="images/profile/download.gif" onmouseover="this.src='images/profile/download-over.gif'" onmouseout="this.src='images/profile/download.gif'" />-->
								</div>
								<div class="type_subtype"><a href ="<?php if($ans_cp['profile_url']!="") { ?>/<?php echo $ans_cp['profile_url']; } ?>">
								<?php if(strlen($ans_cp['title'])>39){
									echo substr($ans_cp['title'],0,37)."...";
								}else{
									echo $ans_cp['title'];
								} ?></a><?php if(isset($ans_cp['artist_id']))
												{
													$get_profile_type = $display->types_of_search('artist_project',$ans_cp['id']);
													if(!empty($get_profile_type) && $get_profile_type!=Null)
													{
														$res_profile_type = mysql_fetch_assoc($get_profile_type);
														if($res_profile_type['type_name']!=""){
														?>
															<br/><span style="font-size:13px; font-weight:normal;">
															<?php 
															if(strlen($res_profile_type['type_name'])>26){
																echo substr($res_profile_type['type_name'],0,24)."...";
															}else{
																echo $res_profile_type['type_name'];
															}
															if($res_profile_type['subtype_name']!="" && strlen($res_profile_type['type_name']<26)){
																$type_length = strlen($res_profile_type['type_name']);
																if($type_length + strlen($res_profile_type['subtype_name'])>22){
																	echo ", ".substr($res_profile_type['subtype_name'],0,22 - $type_length)."...";
																}else{
																	echo ", ".$res_profile_type['subtype_name'];
																}
																
															}
															/*echo $res_profile_type['type_name'];
															if($res_profile_type['subtype_name']!=""){
																echo ", ".$res_profile_type['subtype_name'];
															}*/
															?>
															</span>
													<?php
														}
													}
												}
												elseif(isset($ans_cp['community_id']))
												{
													$get_profile_type = $display->types_of_search('community_project',$ans_cp['id']);
													if(!empty($get_profile_type) && $get_profile_type!=Null)
													{
														$res_profile_type = mysql_fetch_assoc($get_profile_type);
														if($res_profile_type['type_name']!=""){
														?>
															<br/><span style="font-size:13px; font-weight:normal;">
															<?php 
															if(strlen($res_profile_type['type_name'])>26){
																echo substr($res_profile_type['type_name'],0,24)."...";
															}else{
																echo $res_profile_type['type_name'];
															}
															if($res_profile_type['subtype_name']!="" && strlen($res_profile_type['type_name']<26)){
																$type_length = strlen($res_profile_type['type_name']);
																if($type_length + strlen($res_profile_type['subtype_name'])>22){
																	echo ", ".substr($res_profile_type['subtype_name'],0,22 - $type_length)."...";
																}else{
																	echo ", ".$res_profile_type['subtype_name'];
																}
																
															}
															/*echo $res_profile_type['type_name'];
															if($res_profile_type['subtype_name']!=""){
																echo ", ".$res_profile_type['subtype_name'];
															}*/
															?>
															</span>
													<?php
														}
													}
												} ?> </div></div>
							</div>
	<?php
							
							if($exp1_car[0]=='general_artist')
							{
								if(isset($ans1_cp['artist_id']))
								{
									$sql_butts = mysql_query("SELECT * FROM general_user WHERE artist_id='".$ans1_cp['artist_id']."'");
									$ans_butts = mysql_fetch_assoc($sql_butts);
								}
								elseif(isset($ans1_cp['community_id']))
								{
									$sql_butts = mysql_query("SELECT * FROM general_user WHERE community_id='".$ans1_cp['community_id']."'");
									$ans_butts = mysql_fetch_assoc($sql_butts);
								}
								
		?>
								<div class="tabItem"> 
							<a href ="<?php if($ans1_cp['profile_url']!="") { ?>/<?php echo $ans1_cp['profile_url']; } ?>"><img class="imgs_pro" src="<?php if(isset($ans1_cp['image_name']) && $ans1_cp['image_name']!="") { echo 'https://artjcropprofile.s3.amazonaws.com/'.$ans1_cp['image_name'] ; } else { echo 'https://artjcropprofile.s3.amazonaws.com/Noimage.png'; } ?>" width="200" height="200" /></a>
							<div class="both_wrap_per">
							<div class="player">
								<?php
								if($ans1_cp['featured_media']=="Gallery" && $ans1_cp['media_id']!=0)
								{
									$feature_reg_big_img = "";
									$all_register_images = "";
									$feature_reg_thum_path = "";
									$feature_reg_org_path = "";
									$featured_reg_gal_img_title = "";
									
									if($ans1_cp['featured_media_table']!="" &&($ans1_cp['featured_media_table']=="general_artist_gallery_list" || $ans1_cp['featured_media_table']=="general_community_gallery_list"))
									{
										$get_register_featured_gallery = $new_profile_class_obj ->get_register_featured_media_gallery($ans1_cp['media_id'],$ans1_cp['featured_media_table']);
										$all_register_images ="";
										$feature_reg_big_img ="";
										$featured_reg_gal_img_title ="";
										for($cou_i=0;$cou_i<count($get_register_featured_gallery);$cou_i++)
										{
											$feature_reg_big_img = $get_register_featured_gallery[0]['image_name'];
											$all_register_images = $all_register_images.",".$get_register_featured_gallery[$cou_i]['image_name'];
											$featured_reg_gal_img_title = $featured_reg_gal_img_title .",".$get_register_featured_gallery[$cou_i]['image_title'];
										}
										$all_register_images = ltrim($all_register_images,',');
										$featured_reg_gal_img_title = ltrim($featured_reg_gal_img_title,',');
										$feature_reg_org_path = "https://reggallery.s3.amazonaws.com/";
										$feature_reg_thum_path = "https://reggalthumb.s3.amazonaws.com/";
									}
									else if($ans1_cp['featured_media_table']!="" && $ans1_cp['featured_media_table']=="general_media"){
										$get_register_featured_gallery = $new_profile_class_obj ->get_featured_gallery_images($ans1_cp['media_id']);
										$all_register_images ="";
										$feature_reg_big_img ="";
										$featured_reg_gal_img_title ="";
										for($cou_i=0;$cou_i<count($get_register_featured_gallery);$cou_i++)
										{
											$feature_reg_big_img = $get_register_featured_gallery[0]['image_name'];
											$all_register_images = $all_register_images.",".$get_register_featured_gallery[$cou_i]['image_name'];
											$featured_reg_gal_img_title = $featured_reg_gal_img_title .",".$get_register_featured_gallery[$cou_i]['image_title'];
										}
										$all_register_images = ltrim($all_register_images,',');
										$featured_reg_gal_img_title = ltrim($featured_reg_gal_img_title,',');
										$feature_reg_org_path = "https://medgallery.s3.amazonaws.com/";
										$feature_reg_thum_path = "https://medgalthumb.s3.amazonaws.com/";
									}
								?>
								<a href="javascript:void(0);"><img src="../images/profile/play.gif" onmouseover="this.src='../images/profile/play-over.gif'" onmouseout="this.src='../images/profile/play.gif'" onclick="checkload('<?php echo $feature_reg_big_img;?>','<?php echo $all_register_images; ?>','<?php echo $feature_reg_thum_path; ?>','<?php echo $feature_reg_org_path; ?>','<?php echo $featured_reg_gal_img_title; ?>')" title="Play file now."/></a>
								<!--<a href="javascript:void(0);"><img src="../images/profile/add.gif"  onmouseover="this.src='../images/profile/add-over.gif'" onmouseout="this.src='../images/profile/add.gif'" title="Add file to the bottom of your playlist without stopping current media."/></a>-->
								<?php
								if($find_mem_butts != null && $find_mem_butts['general_user_id'] != null && ($find_mem_butts['expiry_date'] >$date || $find_mem_butts['lifetime']==1))
									{
									if($ans1_cp['type']=="fan_club" || $ans1_cp['type']=="for_sale" || $ans1_cp['type']=="fan_club_yes")
											{
											if($_SESSION['login_email']==NULL || $_SESSION['login_email']!=$ans_butts['email'])
											{
										?>
											<a title="<?php if($ans1_cp['type']=="fan_club_yes") { echo "Purchase a fan club membership to this users exclusive media."; } elseif($ans1_cp['type']=="for_sale") { echo "Purchase this projects media."; } ?>" href="javascript:void(0);" onClick="fancy_login_art_coms('<?php echo $ans1_cp['profile_url']; ?>')" ><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>
										<?php
											}
										}
									}
								?>
								<a class="fancybox fancybox.ajax" style="display:none;" href="fan_club.php?data=<?php echo $ans1_cp['profile_url']; ?>" id="fan_club_pro_ca<?php echo $ans1_cp['profile_url']; ?>">
											<?php if($ans1_cp['type']=="fan_club_yes") { echo "Fan Club"; } elseif($ans1_cp['type']=="for_sale") { echo "Download"; } ?></a>
											<script type="text/javascript">
											$(document).ready(function() {
													$("#fan_club_pro_ca<?php echo $ans1_cp['profile_url']; ?>").fancybox({
													helpers : {
															media : {},
															buttons : {},
															title : null,
														},
													});
											});
											</script>
								<?php
								}
								else if($ans1_cp['featured_media']=="Song" && $ans1_cp['media_id']!=0)
								{
									if($ans1_cp['featured_media_table']=="general_artist_audio" || $ans1_cp['featured_media_table']=="general_community_audio")
									{
										?>
											<a href="javascript:void(0);"><img src="../images/profile/play.gif" onmouseover="this.src='../images/profile/play-over.gif'" onmouseout="this.src='../images/profile/play.gif'" onclick="showPlayer_reg('a','<?php echo $ans1_cp['media_id'];?>','play_reg','<?php echo $ans1_cp['featured_media_table'];?>')" title="Play file now."/></a>
											<a href="javascript:void(0);"><img src="../images/profile/add.gif"  onmouseover="this.src='../images/profile/add-over.gif'" onmouseout="this.src='../images/profile/add.gif'" onclick="showPlayer_reg('a','<?php echo $ans1_cp['media_id'];?>','add_reg','<?php echo $ans1_cp['featured_media_table'];?>')" title="Add file to the bottom of your playlist without stopping current media."/></a>
										<?php
									}
									else{
										?>
											<a href="javascript:void(0);"><img src="../images/profile/play.gif" onmouseover="this.src='../images/profile/play-over.gif'" onmouseout="this.src='../images/profile/play.gif'" onclick="showPlayer('a','<?php echo $ans1_cp['media_id'];?>','play')" title="Play file now."/></a>
											<a href="javascript:void(0);"><img src="../images/profile/add.gif"  onmouseover="this.src='../images/profile/add-over.gif'" onmouseout="this.src='../images/profile/add.gif'" onclick="showPlayer('a','<?php echo $ans1_cp['media_id'];?>','add')" title="Add file to the bottom of your playlist without stopping current media."/></a>
										<?php
									}
									if($find_mem_butts != null && $find_mem_butts['general_user_id'] != null && ($find_mem_butts['expiry_date'] >$date || $find_mem_butts['lifetime']==1))
									{
									if($ans1_cp['type']=="fan_club" || $ans1_cp['type']=="for_sale" || $ans1_cp['type']=="fan_club_yes")
										{
											if($_SESSION['login_email']==NULL || $_SESSION['login_email']!=$ans_butts['email'])
											{
										?>
											<a title="<?php if($ans1_cp['type']=="fan_club_yes") { echo "Purchase a fan club membership to this users exclusive media."; } elseif($ans1_cp['type']=="for_sale") { echo "Purchase this projects media."; } ?>" href="javascript:void(0);" onClick="fancy_login_art_coms('<?php echo $ans1_cp['profile_url']; ?>')" ><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>
										<?php
											}
										}
									}
									if($ans1_cp['media_id']!="" && $ans1_cp['media_id']!=" " && $ans1_cp['media_id']!=0){
										$songtype_value_artist = $songtype_value_artist .",".$ans1_cp['media_id'];
									}
									?>
									<a class="fancybox fancybox.ajax" style="display:none;" href="fan_club.php?data=<?php echo $ans1_cp['profile_url']; ?>" id="fan_club_pro_ca<?php echo $ans1_cp['profile_url']; ?>">
											<?php if($ans1_cp['type']=="fan_club_yes") { echo "Fan Club"; } elseif($ans1_cp['type']=="for_sale") { echo "Download"; } ?></a>
											<script type="text/javascript">
											$(document).ready(function() {
													$("#fan_club_pro_ca<?php echo $ans1_cp['profile_url']; ?>").fancybox({
													helpers : {
															media : {},
															buttons : {},
															title : null,
														},
													});
											});
											</script>
									<?php
								}
								else if($ans1_cp['featured_media']=="Video" && $ans1_cp['media_id']!=0)
								{
									if($ans1_cp['featured_media_table']=="general_artist_video" || $ans1_cp['featured_media_table']=="general_community_video")
									{
										?>
											<a href="javascript:void(0);"><img src="../images/profile/play.gif" onmouseover="this.src='../images/profile/play-over.gif'" onmouseout="this.src='../images/profile/play.gif'" onclick="showPlayer_reg('v','<?php echo $ans1_cp['media_id'];?>','playlist_reg','<?php echo $ans1_cp['featured_media_table'];?>')" title="Play file now."/></a>
											<a href="javascript:void(0);"><img src="../images/profile/add.gif"  onmouseover="this.src='../images/profile/add-over.gif'" onmouseout="this.src='../images/profile/add.gif'" onclick="showPlayer_reg('v','<?php echo $ans1_cp['media_id'];?>','addvi_reg','<?php echo $ans1_cp['featured_media_table'];?>')" title="Add file to the bottom of your playlist without stopping current media."/></a>
										<?php
									}else{
										?>
											<a href="javascript:void(0);"><img src="../images/profile/play.gif" onmouseover="this.src='../images/profile/play-over.gif'" onmouseout="this.src='../images/profile/play.gif'" onclick="showPlayer('v','<?php echo $ans1_cp['media_id'];?>','playlist')" title="Play file now."/></a>
											<a href="javascript:void(0);"><img src="../images/profile/add.gif"  onmouseover="this.src='../images/profile/add-over.gif'" onmouseout="this.src='../images/profile/add.gif'" onclick="showPlayer('v','<?php echo $ans1_cp['media_id'];?>','addvi')" title="Add file to the bottom of your playlist without stopping current media."/></a>
										<?php
									}
									if($find_mem_butts != null && $find_mem_butts['general_user_id'] != null && ($find_mem_butts['expiry_date'] >$date || $find_mem_butts['lifetime']==1))
									{
									if($ans1_cp['type']=="fan_club" || $ans1_cp['type']=="for_sale" || $ans1_cp['type']=="fan_club_yes")
										{
											if($_SESSION['login_email']==NULL || $_SESSION['login_email']!=$ans_butts['email'])
											{
										?>
											<a title="<?php if($ans1_cp['type']=="fan_club_yes") { echo "Purchase a fan club membership to this users exclusive media."; } elseif($ans1_cp['type']=="for_sale") { echo "Purchase this projects media."; } ?>" href="javascript:void(0);" onClick="fancy_login_art_coms('<?php echo $ans1_cp['profile_url']; ?>')" ><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>
										<?php
											}
										}
									}
									if($ans1_cp['media_id']!="" && $ans1_cp['media_id']!=" " && $ans1_cp['media_id']!=0){
									$videotype_value_artist = $videotype_value_artist .",".$ans1_cp['media_id'];
									}
									?>
									<a class="fancybox fancybox.ajax" style="display:none;" href="fan_club.php?data=<?php echo $ans1_cp['profile_url']; ?>" id="fan_club_pro_ca<?php echo $ans1_cp['profile_url']; ?>">
											<?php if($ans1_cp['type']=="fan_club_yes") { echo "Fan Club"; } elseif($ans1_cp['type']=="for_sale") { echo "Download"; } ?></a>
											<script type="text/javascript">
											$(document).ready(function() {
													$("#fan_club_pro_ca<?php echo $ans1_cp['profile_url']; ?>").fancybox({
													helpers : {
															media : {},
															buttons : {},
															title : null,
														},
													});
											});
											</script>
									<?php
								}
								else{
								if($find_mem_butts != null && $find_mem_butts['general_user_id'] != null && ($find_mem_butts['expiry_date'] >$date || $find_mem_butts['lifetime']==1))
									{
									if($ans1_cp['type']=="fan_club" || $ans1_cp['type']=="for_sale" || $ans1_cp['type']=="fan_club_yes")
										{
											if($_SESSION['login_email']==NULL || $_SESSION['login_email']!=$ans_butts['email'])
											{
										?>
											<a title="<?php if($ans1_cp['type']=="fan_club_yes") { echo "Purchase a fan club membership to this users exclusive media."; } elseif($ans1_cp['type']=="for_sale") { echo "Purchase this projects media."; } ?>" href="javascript:void(0);" onClick="fancy_login_art_coms('<?php echo $ans1_cp['profile_url']; ?>')" ><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>
										<?php
											}
										}
									}
							  ?>
							  <a class="fancybox fancybox.ajax" style="display:none;" href="fan_club.php?data=<?php echo $ans1_cp['profile_url']; ?>" id="fan_club_pro_ca<?php echo $ans1_cp['profile_url']; ?>">
											<?php if($ans1_cp['type']=="fan_club_yes") { echo "Fan Club"; } elseif($ans1_cp['type']=="for_sale") { echo "Download"; } ?></a>
											<script type="text/javascript">
											$(document).ready(function() {
													$("#fan_club_pro_ca<?php echo $ans1_cp['profile_url']; ?>").fancybox({
													helpers : {
															media : {},
															buttons : {},
															title : null,
														},
													});
											});
											</script>
								<!--<a href="javascript:void(0);"><img src="../images/profile/play.gif" onmouseover="this.src='../images/profile/play-over.gif'" onmouseout="this.src='../images/profile/play.gif'" /></a>
								<a href="javascript:void(0);"><img src="../images/profile/add.gif"  onmouseover="this.src='../images/profile/add-over.gif'" onmouseout="this.src='../images/profile/add.gif'"/></a>-->
								<?php
								}
								?>
								<!--<img src="images/profile/download.gif" onmouseover="this.src='images/profile/download-over.gif'" onmouseout="this.src='images/profile/download.gif'" />-->
								
								
							</div>
							<div class="type_subtype"><a href ="<?php if($ans1_cp['profile_url']!="") { ?>/<?php echo $ans1_cp['profile_url']; } ?>">
							<?php if(strlen($ans1_cp['name'])>39){
								echo substr($ans1_cp['name'],0,37)."...";
							}else{
								echo $ans1_cp['name'];
							} ?></a><?php if(isset($ans1_cp['artist_id']))
											{	
												if(empty($ans1_cp['id']) || $ans1_cp['id']==Null){
													$get_profile_type = $display->types_of_search('artist',$ans1_cp['artist_id']);
												}else{
													$get_profile_type = $display->types_of_search('artist_event',$ans1_cp['id']);
												}
												if(!empty($get_profile_type) && $get_profile_type!=Null)
												{
													$res_profile_type = mysql_fetch_assoc($get_profile_type);
													if($res_profile_type['type_name']!=""){
													?>
														<br/><span style="font-size:13px; font-weight:normal;">
														<?php 
														if(strlen($res_profile_type['type_name'])>26){
															echo substr($res_profile_type['type_name'],0,24)."...";
														}else{
															echo $res_profile_type['type_name'];
														}
														if($res_profile_type['subtype_name']!="" && strlen($res_profile_type['type_name']<26)){
															$type_length = strlen($res_profile_type['type_name']);
															if($type_length + strlen($res_profile_type['subtype_name'])>22){
																echo ", ".substr($res_profile_type['subtype_name'],0,22 - $type_length)."...";
															}else{
																echo ", ".$res_profile_type['subtype_name'];
															}
															
														}
														/*echo $res_profile_type['type_name'];
														if($res_profile_type['subtype_name']!=""){
															echo ", ".$res_profile_type['subtype_name'];
														}*/
														?>
														</span>
												<?php
													}
												}
											} ?> </div></div>
						</div>
		<?php	
							}
							if($exp1_car[0]=='artist_project' || $exp1_car[0]=='community_project')
							{
								if(isset($ans1_cp['artist_id']))
								{
									$sql_butts = mysql_query("SELECT * FROM general_user WHERE artist_id='".$ans1_cp['artist_id']."'");
									$ans_butts = mysql_fetch_assoc($sql_butts);
								}
								elseif(isset($ans1_cp['community_id']))
								{
									$sql_butts = mysql_query("SELECT * FROM general_user WHERE community_id='".$ans1_cp['community_id']."'");
									$ans_butts = mysql_fetch_assoc($sql_butts);
								}
		?>
							<div class="tabItem"> 
								<a href ="<?php if($ans1_cp['profile_url']!="") { ?>/<?php echo $ans1_cp['profile_url']; } ?>"><img class="imgs_pro" src="<?php if(isset($ans1_cp['image_name']) && $ans1_cp['image_name']!="") { echo $ans1_cp['image_name'] ; } else { echo 'https://artjcropprofile.s3.amazonaws.com/Noimage.png'; } ?>" width="200" height="200" /></a>
								<div class="both_wrap_per">
								<div class="player">
									<?php
									if($ans1_cp['featured_media']=="Gallery" && $ans1_cp['media_id']!=0)
									{
										$feature_reg_big_img = "";
										$all_register_images = "";
										$feature_reg_thum_path = "";
										$feature_reg_org_path = "";
										$featured_reg_gal_img_title = "";
									
										if($ans1_cp['featured_media_table']!="" &&($ans1_cp['featured_media_table']=="general_artist_gallery_list" || $ans1_cp['featured_media_table']=="general_community_gallery_list"))
										{
											$get_register_featured_gallery = $new_profile_class_obj ->get_register_featured_media_gallery($ans1_cp['media_id'],$ans1_cp['featured_media_table']);
											$all_register_images ="";
											$feature_reg_big_img ="";
											$featured_reg_gal_img_title ="";
											for($cou_i=0;$cou_i<count($get_register_featured_gallery);$cou_i++)
											{
												$feature_reg_big_img = $get_register_featured_gallery[0]['image_name'];
												$all_register_images = $all_register_images.",".$get_register_featured_gallery[$cou_i]['image_name'];
												$featured_reg_gal_img_title = $featured_reg_gal_img_title .",".$get_register_featured_gallery[$cou_i]['image_title'];
											}
											$all_register_images = ltrim($all_register_images,',');
											$featured_reg_gal_img_title = ltrim($featured_reg_gal_img_title,',');
											$feature_reg_org_path = "https://reggallery.s3.amazonaws.com/";
											$feature_reg_thum_path = "https://reggalthumb.s3.amazonaws.com/";
										}
										else if($ans1_cp['featured_media_table']!="" && $ans1_cp['featured_media_table']=="general_media"){
											$get_register_featured_gallery = $new_profile_class_obj ->get_featured_gallery_images($ans1_cp['media_id']);
											$all_register_images ="";
											$feature_reg_big_img ="";
											$featured_reg_gal_img_title ="";
											for($cou_i=0;$cou_i<count($get_register_featured_gallery);$cou_i++)
											{
												$feature_reg_big_img = $get_register_featured_gallery[0]['image_name'];
												$all_register_images = $all_register_images.",".$get_register_featured_gallery[$cou_i]['image_name'];
												$featured_reg_gal_img_title = $featured_reg_gal_img_title .",".$get_register_featured_gallery[$cou_i]['image_title'];
											}
											$all_register_images = ltrim($all_register_images,',');
											$featured_reg_gal_img_title = ltrim($featured_reg_gal_img_title,',');
											$feature_reg_org_path = "https://medgallery.s3.amazonaws.com/";
											$feature_reg_thum_path = "https://medgalthumb.s3.amazonaws.com/";
										}
										?>
										<a href="javascript:void(0);"><img src="../images/profile/play.gif" onmouseover="this.src='../images/profile/play-over.gif'" onmouseout="this.src='../images/profile/play.gif'" onclick="checkload('<?php echo $feature_reg_big_img;?>','<?php echo $all_register_images; ?>','<?php echo $feature_reg_thum_path; ?>','<?php echo $feature_reg_org_path; ?>','<?php echo $featured_reg_gal_img_title; ?>')" title="Play file now."/></a>
										<!--<a href="javascript:void(0);"><img src="../images/profile/add.gif"  onmouseover="this.src='../images/profile/add-over.gif'" onmouseout="this.src='../images/profile/add.gif'" title="Add file to the bottom of your playlist without stopping current media."/></a>-->
										<?php
										if($find_mem_butts != null && $find_mem_butts['general_user_id'] != null && ($find_mem_butts['expiry_date'] >$date || $find_mem_butts['lifetime']==1))
										{
											if($ans1_cp['type']=="fan_club" || $ans1_cp['type']=="for_sale" || $ans1_cp['type']=="fan_club_yes")
											{
												if($_SESSION['login_email']==NULL || $_SESSION['login_email']!=$ans_butts['email'])
												{
											?>
												<a href="javascript:void(0);" onClick="<?php if($ans1_cp['type']=="fan_club") { echo "fancy_login_single('".$ans1_cp['profile_url']."')"; } elseif($ans1_cp['type']=="for_sale") { echo "click_fancy_login_down_single('".$ans1_cp['profile_url']."')"; } ?>"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>
											<?php
												}
											}
										}
										?>
										<a href="fan_club.php?data=<?php echo $ans1_cp['profile_url'];?>" id="fan_club_pro<?php echo $ans1_cp['profile_url'];?>" style="display:none;" ><?php if($ans1_cp['type']=="fan_club") { echo "Fan Club"; } elseif($ans1_cp['type']=="for_sale") { echo "Purchase"; } ?></a>
											<script type="text/javascript">
											$(document).ready(function() {
													$("#fan_club_pro<?php echo $ans1_cp['profile_url'];?>").fancybox({
													'width'				: '69%',
													'height'			: '80%',
													'transitionIn'		: 'none',
													'transitionOut'		: 'none',
													'type'				: 'iframe'
													});
											});
											</script>
										<?php	
									}
									else if($ans1_cp['featured_media']=="Song" && $ans1_cp['media_id']!=0)
									{
										if($ans1_cp['featured_media_table']=="general_artist_audio" || $ans1_cp['featured_media_table']=="general_community_audio")
										{
											?>
												<a href="javascript:void(0);"><img src="../images/profile/play.gif" onmouseover="this.src='../images/profile/play-over.gif'" onmouseout="this.src='../images/profile/play.gif'" onclick="showPlayer_reg('a','<?php echo $ans1_cp['media_id'];?>','play_reg','<?php echo $ans1_cp['featured_media_table'];?>')" title="Play file now."/></a>
												<a href="javascript:void(0);"><img src="../images/profile/add.gif"  onmouseover="this.src='../images/profile/add-over.gif'" onmouseout="this.src='../images/profile/add.gif'" onclick="showPlayer_reg('a','<?php echo $ans1_cp['media_id'];?>','add_reg','<?php echo $ans1_cp['featured_media_table'];?>')" title="Add file to the bottom of your playlist without stopping current media."/></a>
											<?php
										}
										else{
											?>
												<a href="javascript:void(0);"><img src="../images/profile/play.gif" onmouseover="this.src='../images/profile/play-over.gif'" onmouseout="this.src='../images/profile/play.gif'" onclick="showPlayer('a','<?php echo $ans1_cp['media_id'];?>','play')" title="Play file now."/></a>
												<a href="javascript:void(0);"><img src="../images/profile/add.gif"  onmouseover="this.src='../images/profile/add-over.gif'" onmouseout="this.src='../images/profile/add.gif'" onclick="showPlayer('a','<?php echo $ans1_cp['media_id'];?>','add')" title="Add file to the bottom of your playlist without stopping current media."/></a>
											<?php
										}
										if($find_mem_butts != null && $find_mem_butts['general_user_id'] != null && ($find_mem_butts['expiry_date'] >$date || $find_mem_butts['lifetime']==1))
									{
											if($ans1_cp['type']=="fan_club" || $ans1_cp['type']=="for_sale" || $ans1_cp['type']=="fan_club_yes")
											{
												if($_SESSION['login_email']==NULL || $_SESSION['login_email']!=$ans_butts['email'])
												{
											?>
												<a href="javascript:void(0);" onClick="<?php if($ans1_cp['type']=="fan_club") { echo "fancy_login_single('".$ans1_cp['profile_url']."')"; } elseif($ans1_cp['type']=="for_sale") { echo "click_fancy_login_down_single('".$ans1_cp['profile_url']."')"; } ?>"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>
											<?php
												}
											}
										}
										if($ans1_cp['media_id']!="" && $ans1_cp['media_id']!=" " && $ans1_cp['media_id']!=0){
											$songtype_value_artist = $songtype_value_artist .",".$ans1_cp['media_id'];
										}
										?>
										<a href="fan_club.php?data=<?php echo $ans1_cp['profile_url'];?>" id="fan_club_pro<?php echo $ans1_cp['profile_url'];?>" style="display:none;" ><?php if($ans1_cp['type']=="fan_club") { echo "Fan Club"; } elseif($ans1_cp['type']=="for_sale") { echo "Purchase"; } ?></a>
											<script type="text/javascript">
											$(document).ready(function() {
													$("#fan_club_pro<?php echo $ans1_cp['profile_url'];?>").fancybox({
													'width'				: '69%',
													'height'			: '80%',
													'transitionIn'		: 'none',
													'transitionOut'		: 'none',
													'type'				: 'iframe'
													});
											});
											</script>
										<?php
									}
									else if($ans1_cp['featured_media']=="Video" && $ans1_cp['media_id']!=0)
									{
										if($ans1_cp['featured_media_table']=="general_artist_video" || $ans1_cp['featured_media_table']=="general_community_video")
										{
											?>
												<a href="javascript:void(0);"><img src="../images/profile/play.gif" onmouseover="this.src='../images/profile/play-over.gif'" onmouseout="this.src='../images/profile/play.gif'" onclick="showPlayer_reg('v','<?php echo $ans1_cp['media_id'];?>','playlist_reg','<?php echo $ans1_cp['featured_media_table'];?>')" title="Play file now."/></a>
												<a href="javascript:void(0);"><img src="../images/profile/add.gif"  onmouseover="this.src='../images/profile/add-over.gif'" onmouseout="this.src='../images/profile/add.gif'" onclick="showPlayer_reg('v','<?php echo $ans1_cp['media_id'];?>','addvi_reg','<?php echo $ans1_cp['featured_media_table'];?>')" title="Add file to the bottom of your playlist without stopping current media."/></a>
											<?php
										}else{
											?>
												<a href="javascript:void(0);"><img src="../images/profile/play.gif" onmouseover="this.src='../images/profile/play-over.gif'" onmouseout="this.src='../images/profile/play.gif'" onclick="showPlayer('v','<?php echo $ans1_cp['media_id'];?>','playlist')" title="Play file now."/></a>
												<a href="javascript:void(0);"><img src="../images/profile/add.gif"  onmouseover="this.src='../images/profile/add-over.gif'" onmouseout="this.src='../images/profile/add.gif'" onclick="showPlayer('v','<?php echo $ans1_cp['media_id'];?>','addvi')" title="Add file to the bottom of your playlist without stopping current media."/></a>
											<?php
										}
										if($find_mem_butts != null && $find_mem_butts['general_user_id'] != null && ($find_mem_butts['expiry_date'] >$date || $find_mem_butts['lifetime']==1))
										{
											if($ans1_cp['type']=="fan_club" || $ans1_cp['type']=="for_sale" || $ans1_cp['type']=="fan_club_yes")
											{
												if($_SESSION['login_email']==NULL || $_SESSION['login_email']!=$ans_butts['email'])
												{
											?>
												<a href="javascript:void(0);" onClick="<?php if($ans1_cp['type']=="fan_club") { echo "fancy_login_single('".$ans1_cp['profile_url']."')"; } elseif($ans1_cp['type']=="for_sale") { echo "click_fancy_login_down_single('".$ans1_cp['profile_url']."')"; } ?>"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>
											<?php
												}
											}
										}
										if($ans1_cp['media_id']!="" && $ans1_cp['media_id']!=" " && $ans1_cp['media_id']!=0){
											$videotype_value_artist = $videotype_value_artist .",".$ans1_cp['media_id'];
										}
										?>
										<a href="fan_club.php?data=<?php echo $ans1_cp['profile_url'];?>" id="fan_club_pro<?php echo $ans1_cp['profile_url'];?>" style="display:none;" ><?php if($ans1_cp['type']=="fan_club") { echo "Fan Club"; } elseif($ans1_cp['type']=="for_sale") { echo "Purchase"; } ?></a>
											<script type="text/javascript">
											$(document).ready(function() {
													$("#fan_club_pro<?php echo $ans1_cp['profile_url'];?>").fancybox({
													'width'				: '69%',
													'height'			: '80%',
													'transitionIn'		: 'none',
													'transitionOut'		: 'none',
													'type'				: 'iframe'
													});
											});
											</script>
										<?php
									}
									else{
									if($find_mem_butts != null && $find_mem_butts['general_user_id'] != null && ($find_mem_butts['expiry_date'] >$date || $find_mem_butts['lifetime']==1))
									{
										if($ans1_cp['type']=="fan_club" || $ans1_cp['type']=="for_sale" || $ans1_cp['type']=="fan_club_yes")
										{
											if($_SESSION['login_email']==NULL || $_SESSION['login_email']!=$ans_butts['email'])
											{
										?>
											<a href="javascript:void(0);" onClick="<?php if($ans1_cp['type']=="fan_club") { echo "fancy_login_single('".$ans1_cp['profile_url']."')"; } elseif($ans1_cp['type']=="for_sale") { echo "click_fancy_login_down_single('".$ans1_cp['profile_url']."')"; } ?>"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>
										<?php
											}
										}
									}
								  ?>
								  <a href="fan_club.php?data=<?php echo $ans1_cp['profile_url'];?>" id="fan_club_pro<?php echo $ans1_cp['profile_url'];?>" style="display:none;" ><?php if($ans1_cp['type']=="fan_club") { echo "Fan Club"; } elseif($ans1_cp['type']=="for_sale") { echo "Purchase"; } ?></a>
											<script type="text/javascript">
											$(document).ready(function() {
													$("#fan_club_pro<?php echo $ans1_cp['profile_url'];?>").fancybox({
													'width'				: '69%',
													'height'			: '80%',
													'transitionIn'		: 'none',
													'transitionOut'		: 'none',
													'type'				: 'iframe'
													});
											});
											</script>
									<!--<a href="javascript:void(0);"><img src="../images/profile/play.gif" onmouseover="this.src='../images/profile/play-over.gif'" onmouseout="this.src='../images/profile/play.gif'" /></a>
									<a href="javascript:void(0);"><img src="../images/profile/add.gif"  onmouseover="this.src='../images/profile/add-over.gif'" onmouseout="this.src='../images/profile/add.gif'"/></a>-->
									<?php
									}
									?>
									<!--<img src="images/profile/download.gif" onmouseover="this.src='images/profile/download-over.gif'" onmouseout="this.src='images/profile/download.gif'" />-->
								</div>
								<div class="type_subtype"><a href ="<?php if($ans1_cp['profile_url']!="") { ?>/<?php echo $ans1_cp['profile_url']; } ?>">
								<?php if(strlen($ans1_cp['title'])>39){
									echo substr($ans1_cp['title'],0,37)."...";
								}else{
									echo $ans1_cp['title'];
								} ?></a><?php if(isset($ans1_cp['artist_id']))
												{
													$get_profile_type = $display->types_of_search('artist_project',$ans1_cp['id']);
													if(!empty($get_profile_type) && $get_profile_type!=Null)
													{
														$res_profile_type = mysql_fetch_assoc($get_profile_type);
														if($res_profile_type['type_name']!=""){
														?>
															<br/><span style="font-size:13px; font-weight:normal;">
															<?php 
															if(strlen($res_profile_type['type_name'])>26){
																echo substr($res_profile_type['type_name'],0,24)."...";
															}else{
																echo $res_profile_type['type_name'];
															}
															if($res_profile_type['subtype_name']!="" && strlen($res_profile_type['type_name']<26)){
																$type_length = strlen($res_profile_type['type_name']);
																if($type_length + strlen($res_profile_type['subtype_name'])>22){
																	echo ", ".substr($res_profile_type['subtype_name'],0,22 - $type_length)."...";
																}else{
																	echo ", ".$res_profile_type['subtype_name'];
																}
																
															}
															/*echo $res_profile_type['type_name'];
															if($res_profile_type['subtype_name']!=""){
																echo ", ".$res_profile_type['subtype_name'];
															}*/
															?>
															</span>
													<?php
														}
													}
												}
												elseif(isset($ans1_cp['community_id']))
												{
													$get_profile_type = $display->types_of_search('community_project',$ans1_cp['id']);
													if(!empty($get_profile_type) && $get_profile_type!=Null)
													{
														$res_profile_type = mysql_fetch_assoc($get_profile_type);
														if($res_profile_type['type_name']!=""){
														?>
															<br/><span style="font-size:13px; font-weight:normal;">
															<?php 
															if(strlen($res_profile_type['type_name'])>26){
																echo substr($res_profile_type['type_name'],0,24)."...";
															}else{
																echo $res_profile_type['type_name'];
															}
															if($res_profile_type['subtype_name']!="" && strlen($res_profile_type['type_name']<26)){
																$type_length = strlen($res_profile_type['type_name']);
																if($type_length + strlen($res_profile_type['subtype_name'])>22){
																	echo ", ".substr($res_profile_type['subtype_name'],0,22 - $type_length)."...";
																}else{
																	echo ", ".$res_profile_type['subtype_name'];
																}
																
															}
															/*echo $res_profile_type['type_name'];
															if($res_profile_type['subtype_name']!=""){
																echo ", ".$res_profile_type['subtype_name'];
															}*/
															?>
															</span>
													<?php
														}
													}
												} ?></div></div>
							</div>
		<?php
					}
						}
					}
				}
				}
			}
		}
		//var_dump($exp1_car);
		//var_dump($exp_car);
		if($Find_Artist == "artist" || $Find_Company == "company" || isset($exp1_car))
		{
			
			if(isset($exp_car))
			{
				if($exp_car[0]=='artist_project' || $exp_car[0]=='community_project')
				{
					$ans_tgd = explode(',',$ans_cp['tagged_user_email']);
				}
			}
			
			if(isset($ans_tgd))
			{
				for($tg=0;$tg<count($ans_tgd);$tg++)
				{
					$all_artist[] = $ans_tgd[$tg];
				}
			}

						$dummy_1vp = array();
						for($count_exp_art=0;$count_exp_art<=count($all_artist);$count_exp_art++)
						{
							if($all_artist[$count_exp_art] !="")
							{
								if(isset($exp_car[0]))
								{
									if($exp_car[0]=='general_artist')
									{
										
										$sql = mysql_query("SELECT * FROM general_user WHERE artist_id='".$ans_cp['artist_id']."'");
										if($sql!="" && $sql!=Null)
										{
											if(mysql_num_rows($sql)>0)
											{
												$ans = mysql_fetch_assoc($sql);
											}
										}
									}
								}
								
								if(in_array($all_artist[$count_exp_art],$dummy_1vp) && $ans['email']!=$all_artist[$count_exp_art])
								{}
								else
								{
										
									$dummy_1vp[] = $all_artist[$count_exp_art];
									$get_artist = $new_profile_class_obj->get_all_artist_info($all_artist[$count_exp_art]);
									if(!empty($get_artist))
									{
									/*$sql_buttds = mysql_query("SELECT * FROM general_user WHERE artist_id='".$get_artist['artist_id']."'");
									if($sql_buttds!="" && $sql_buttds!=Null)
									{
										if(mysql_num_rows($sql_buttds)>0)
										{
											$ans_buttds = mysql_fetch_assoc($sql_buttds);
										}
									}*/
									
									
									if(!empty($get_artist)){
										$img_path_tag = 'https://artjcropprofile.s3.amazonaws.com/';
										$sql_buttds = mysql_query("SELECT * FROM general_user WHERE artist_id='".$get_artist['artist_id']."'");
										if($sql_buttds!="" && $sql_buttds!=Null)
										{
											if(mysql_num_rows($sql_buttds)>0)
											{
												$ans_buttds = mysql_fetch_assoc($sql_buttds);
											}
										}
									}else if(empty($get_artist)){
										$img_path_tag = 'https://comjcropprofile.s3.amazonaws.com/';
										$get_artist = $new_profile_class_obj->get_all_company_info($all_artist[$count_exp_art]);
										$sql_buttds = mysql_query("SELECT * FROM general_user WHERE community_id='".$get_artist['community_id']."'");
										if($sql_buttds!="" && $sql_buttds!=Null)
										{
											if(mysql_num_rows($sql_buttds)>0)
											{
												$ans_buttds = mysql_fetch_assoc($sql_buttds);
											}
										}
									}
									
									
									$find_mem_butts = $display->get_member_or_not($ans_buttds['general_user_id']);
									//echo $get_artist['profile_url'].':'.$get_artist['featured_media'];
					?>
								<div class="tabItem">
									<a href ="/<?php echo $get_artist['profile_url'];?>"><img class="imgs_pro" src="<?php if(isset($get_artist['image_name']) && $get_artist['image_name']!="") { echo $img_path_tag.$get_artist['image_name'] ; } else { echo 'https://artjcropprofile.s3.amazonaws.com/Noimage.png'; } ?>" width="200" height="200" /></a>
									<div class="both_wrap_per">
									<div class="player">
										<?php
											if($get_artist['featured_media']=="Gallery" && $get_artist['media_id']!=0)
											{
												$feature_reg_big_img = "";
												$all_register_images = "";
												$feature_reg_thum_path = "";
												$feature_reg_org_path = "";
												$featured_reg_gal_img_title = "";
									
												if($get_artist['featured_media_table']!="" &&($get_artist['featured_media_table']=="general_artist_gallery_list" || $get_artist['featured_media_table']=="general_community_gallery_list"))
												{
													$get_register_featured_gallery = $new_profile_class_obj ->get_register_featured_media_gallery($get_artist['media_id'],$get_artist['featured_media_table']);
													$all_register_images ="";
													$feature_reg_big_img ="";
													$featured_reg_gal_img_title ="";
													for($cou_i=0;$cou_i<count($get_register_featured_gallery);$cou_i++)
													{
														$feature_reg_big_img = $get_register_featured_gallery[0]['image_name'];
														$all_register_images = $all_register_images.",".$get_register_featured_gallery[$cou_i]['image_name'];
														$featured_reg_gal_img_title = $featured_reg_gal_img_title .",".$get_register_featured_gallery[$cou_i]['image_title'];
													}
													$all_register_images = ltrim($all_register_images,',');
													$featured_reg_gal_img_title = ltrim($featured_reg_gal_img_title,',');
													$feature_reg_org_path = "https://reggallery.s3.amazonaws.com/";
													$feature_reg_thum_path = "https://reggalthumb.s3.amazonaws.com/";
												}
												else if($get_artist['featured_media_table']!="" && $get_artist['featured_media_table']=="general_media"){
													$get_register_featured_gallery = $new_profile_class_obj ->get_featured_gallery_images($get_artist['media_id']);
													$all_register_images ="";
													$feature_reg_big_img ="";
													$featured_reg_gal_img_title ="";
													for($cou_i=0;$cou_i<count($get_register_featured_gallery);$cou_i++)
													{
														$feature_reg_big_img = $get_register_featured_gallery[0]['image_name'];
														$all_register_images = $all_register_images.",".$get_register_featured_gallery[$cou_i]['image_name'];
														$featured_reg_gal_img_title = $featured_reg_gal_img_title .",".$get_register_featured_gallery[$cou_i]['image_title'];
													}
													$all_register_images = ltrim($all_register_images,',');
													$featured_reg_gal_img_title = ltrim($featured_reg_gal_img_title,',');
													$feature_reg_org_path = "https://medgallery.s3.amazonaws.com/";
													$feature_reg_thum_path = "https://medgalthumb.s3.amazonaws.com/";
												}
												?>
												<a href="javascript:void(0);"><img src="../images/profile/play.gif" onmouseover="this.src='../images/profile/play-over.gif'" onmouseout="this.src='../images/profile/play.gif'" onclick="checkload('<?php echo $feature_reg_big_img;?>','<?php echo $all_register_images; ?>','<?php echo $feature_reg_thum_path; ?>','<?php echo $feature_reg_org_path; ?>','<?php echo $featured_reg_gal_img_title; ?>')" title="Play file now."/></a>
												<!--<a href="javascript:void(0);"><img src="../images/profile/add.gif"  onmouseover="this.src='../images/profile/add-over.gif'" onmouseout="this.src='../images/profile/add.gif'" title="Add file to the bottom of your playlist without stopping current media." /></a>-->
												<?php
												if($find_mem_butts != null && $find_mem_butts['general_user_id'] != null && ($find_mem_butts['expiry_date'] >$date || $find_mem_butts['lifetime']==1))
												{
													if($get_artist['type']=='fan_club_yes')
													{
														if($_SESSION['login_email']==NULL || $ans_buttds['email']!=$_SESSION['login_email'])
														{
													?>
														<a title="<?php if($get_artist['type']=="fan_club_yes") { echo "Purchase a fan club membership to this users exclusive media."; } elseif($get_artist['type']=="for_sale") { echo "Purchase this projects media."; } ?>" href="javascript:void(0);" onClick="fancy_login_art_coms('<?php echo $get_artist['profile_url']; ?>')" ><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>
													<?php
														}
													}
												}
												?>
												<a class="fancybox fancybox.ajax" style="display:none;" href="fan_club.php?data=<?php echo $get_artist['profile_url']; ?>" id="fan_club_pro_ca<?php echo $get_artist['profile_url']; ?>">
												<?php if($get_artist['type']=="fan_club_yes") { echo "Fan Club"; } elseif($get_artist['type']=="for_sale") { echo "Download"; } ?></a>
												<script type="text/javascript">
												$(document).ready(function() {
														$("#fan_club_pro_ca<?php echo $get_artist['profile_url']; ?>").fancybox({
														helpers : {
															media : {},
															buttons : {},
															title : null,
														},
														});
												});
												</script>
												<?php
											}
											else if($get_artist['featured_media']=="Song" && $get_artist['media_id']!=0)
											{
												if($get_artist['featured_media_table']=="general_artist_audio" || $get_artist['featured_media_table']=="general_community_audio")
												{
													?>
														<a href="javascript:void(0);"><img src="../images/profile/play.gif" onmouseover="this.src='../images/profile/play-over.gif'" onmouseout="this.src='../images/profile/play.gif'" onclick="showPlayer_reg('a','<?php echo $get_artist['media_id'];?>','play_reg','<?php echo $get_artist['featured_media_table'];?>')" title="Play file now."/></a>
														<a href="javascript:void(0);"><img src="../images/profile/add.gif"  onmouseover="this.src='../images/profile/add-over.gif'" onmouseout="this.src='../images/profile/add.gif'" onclick="showPlayer_reg('a','<?php echo $get_artist['media_id'];?>','add_reg','<?php echo $get_artist['featured_media_table'];?>')" title="Add file to the bottom of your playlist without stopping current media."/></a>
													<?php
												}
												else{
													?>
														<a href="javascript:void(0);"><img src="../images/profile/play.gif" onmouseover="this.src='../images/profile/play-over.gif'" onmouseout="this.src='../images/profile/play.gif'" onclick="showPlayer('a','<?php echo $get_artist['media_id'];?>','play')" title="Play file now."/></a>
														<a href="javascript:void(0);"><img src="../images/profile/add.gif"  onmouseover="this.src='../images/profile/add-over.gif'" onmouseout="this.src='../images/profile/add.gif'" onclick="showPlayer('a','<?php echo $get_artist['media_id'];?>','add')" title="Add file to the bottom of your playlist without stopping current media."/></a>
													<?php
												}
												if($find_mem_butts != null && $find_mem_butts['general_user_id'] != null && ($find_mem_butts['expiry_date'] >$date || $find_mem_butts['lifetime']==1))
												{
													if($get_artist['type']=='fan_club_yes')
													{
														if($_SESSION['login_email']==NULL || $ans_buttds['email']!=$_SESSION['login_email'])
														{
													?>
														<a title="<?php if($get_artist['type']=="fan_club_yes") { echo "Purchase a fan club membership to this users exclusive media."; } elseif($get_artist['type']=="for_sale") { echo "Purchase this projects media."; } ?>" href="javascript:void(0);" onClick="fancy_login_art_coms('<?php echo $get_artist['profile_url']; ?>')" ><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>
													<?php
														}
													}
												}
											
												if($get_artist['media_id']!="" && $get_artist['media_id']!=" " && $get_artist['media_id']!=0){
													$songtype_value_artist = $songtype_value_artist .",".$get_artist['media_id'];
												}
												?>
												<a class="fancybox fancybox.ajax" style="display:none;" href="fan_club.php?data=<?php echo $get_artist['profile_url']; ?>" id="fan_club_pro_ca<?php echo $get_artist['profile_url']; ?>">
												<?php if($get_artist['type']=="fan_club_yes") { echo "Fan Club"; } elseif($get_artist['type']=="for_sale") { echo "Download"; } ?></a>
												<script type="text/javascript">
												$(document).ready(function() {
														$("#fan_club_pro_ca<?php echo $get_artist['profile_url']; ?>").fancybox({
														helpers : {
															media : {},
															buttons : {},
															title : null,
														},
														});
												});
												</script>
												<?php
											}
											else if($get_artist['featured_media']=="Video" && $get_artist['media_id']!=0)
											{
												if($get_artist['featured_media_table']=="general_artist_video" || $get_artist['featured_media_table']=="general_community_video")
												{
													?>
														<a href="javascript:void(0);"><img src="../images/profile/play.gif" onmouseover="this.src='../images/profile/play-over.gif'" onmouseout="this.src='../images/profile/play.gif'" onclick="showPlayer_reg('v','<?php echo $get_artist['media_id'];?>','playlist_reg','<?php echo $get_artist['featured_media_table'];?>')" title="Play file now."/></a>
														<a href="javascript:void(0);"><img src="../images/profile/add.gif"  onmouseover="this.src='../images/profile/add-over.gif'" onmouseout="this.src='../images/profile/add.gif'" onclick="showPlayer_reg('v','<?php echo $get_artist['media_id'];?>','addvi_reg','<?php echo $get_artist['featured_media_table'];?>')" title="Add file to the bottom of your playlist without stopping current media."/></a>
													<?php
												}else{
													?>
														<a href="javascript:void(0);"><img src="../images/profile/play.gif" onmouseover="this.src='../images/profile/play-over.gif'" onmouseout="this.src='../images/profile/play.gif'" onclick="showPlayer('v','<?php echo $get_artist['media_id'];?>','playlist')" title="Play file now."/></a>
														<a href="javascript:void(0);"><img src="../images/profile/add.gif"  onmouseover="this.src='../images/profile/add-over.gif'" onmouseout="this.src='../images/profile/add.gif'" onclick="showPlayer('v','<?php echo $get_artist['media_id'];?>','addvi')" title="Add file to the bottom of your playlist without stopping current media."/></a>
													<?php
												}
												if($find_mem_butts != null && $find_mem_butts['general_user_id'] != null && ($find_mem_butts['expiry_date'] >$date || $find_mem_butts['lifetime']==1))
												{
													if($get_artist['type']=='fan_club_yes')
													{
														if($_SESSION['login_email']==NULL || $ans_buttds['email']!=$_SESSION['login_email'])
														{
													?>
														<a title="<?php if($get_artist['type']=="fan_club_yes") { echo "Purchase a fan club membership to this users exclusive media."; } elseif($get_artist['type']=="for_sale") { echo "Purchase this projects media."; } ?>" href="javascript:void(0);" onClick="fancy_login_art_coms('<?php echo $get_artist['profile_url']; ?>')" ><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>
													<?php
														}
													}
												}
												if($get_artist['media_id']!="" && $get_artist['media_id']!=" " && $get_artist['media_id']!=0){
												$videotype_value_artist = $videotype_value_artist .",".$get_artist['media_id'];
												}
												?>
												<a class="fancybox fancybox.ajax" style="display:none;" href="fan_club.php?data=<?php echo $get_artist['profile_url']; ?>" id="fan_club_pro_ca<?php echo $get_artist['profile_url']; ?>">
												<?php if($get_artist['type']=="fan_club_yes") { echo "Fan Club"; } elseif($get_artist['type']=="for_sale") { echo "Download"; } ?></a>
												<script type="text/javascript">
												$(document).ready(function() {
														$("#fan_club_pro_ca<?php echo $get_artist['profile_url']; ?>").fancybox({
														helpers : {
															media : {},
															buttons : {},
															title : null,
														},
														});
												});
												</script>
												<?php
											}
											else{
											if($find_mem_butts != null && $find_mem_butts['general_user_id'] != null && ($find_mem_butts['expiry_date'] >$date || $find_mem_butts['lifetime']==1))
												{
												if($get_artist['type']=='fan_club_yes')
												{
													if($_SESSION['login_email']==NULL || $ans_buttds['email']!=$_SESSION['login_email'])
													{
												?>
													<a title="<?php if($get_artist['type']=="fan_club_yes") { echo "Purchase a fan club membership to this users exclusive media."; } elseif($get_artist['type']=="for_sale") { echo "Purchase this projects media."; } ?>" href="javascript:void(0);" onClick="fancy_login_art_coms('<?php echo $get_artist['profile_url']; ?>')" ><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>
												<?php
													}
												}
											}
										  ?>
										  <a class="fancybox fancybox.ajax" style="display:none;" href="fan_club.php?data=<?php echo $get_artist['profile_url']; ?>" id="fan_club_pro_ca<?php echo $get_artist['profile_url']; ?>">
												<?php if($get_artist['type']=="fan_club_yes") { echo "Fan Club"; } elseif($get_artist['type']=="for_sale") { echo "Download"; } ?></a>
												<script type="text/javascript">
												$(document).ready(function() {
														$("#fan_club_pro_ca<?php echo $get_artist['profile_url']; ?>").fancybox({
														helpers : {
															media : {},
															buttons : {},
															title : null,
														},
														});
												});
												</script>
											<!--<a href="javascript:void(0);"><img src="../images/profile/play.gif" onmouseover="this.src='../images/profile/play-over.gif'" onmouseout="this.src='../images/profile/play.gif'" /></a>
											<a href="javascript:void(0);"><img src="../images/profile/add.gif"  onmouseover="this.src='../images/profile/add-over.gif'" onmouseout="this.src='../images/profile/add.gif'"/></a>-->
											<?php
											}
											?>
											<!--<img src="images/profile/download.gif" onmouseover="this.src='images/profile/download-over.gif'" onmouseout="this.src='images/profile/download.gif'" />-->
									</div>
									<div class="type_subtype"><a href ="/<?php echo $get_artist['profile_url'];?>">
									<?php 
									if(strlen($get_artist['name'])>39){
										echo substr($get_artist['name'],0,37)."...";
									}else{
										echo $get_artist['name'];
									}
									 ?></a><?php if(isset($get_artist['artist_id']))
											{
												$get_profile_type = $display->types_of_search('artist',$get_artist['artist_id']);
												if(!empty($get_profile_type) && $get_profile_type!=Null)
												{
													$res_profile_type = mysql_fetch_assoc($get_profile_type);
													if($res_profile_type['type_name']!=""){
													?>
														<br/><span style="font-size:13px; font-weight:normal;">
														<?php 
														if(strlen($res_profile_type['type_name'])>26){
															echo substr($res_profile_type['type_name'],0,24)."...";
														}else{
															echo $res_profile_type['type_name'];
														}
														if($res_profile_type['subtype_name']!="" && strlen($res_profile_type['type_name']<26)){
															$type_length = strlen($res_profile_type['type_name']);
															if($type_length + strlen($res_profile_type['subtype_name'])>22){
																echo ", ".substr($res_profile_type['subtype_name'],0,22 - $type_length)."...";
															}else{
																echo ", ".$res_profile_type['subtype_name'];
															}
															
														}
														/*echo $res_profile_type['type_name'];
														if($res_profile_type['subtype_name']!=""){
															echo ", ".$res_profile_type['subtype_name'];
														}*/
														?>
														</span>
												<?php
													}
												}
											}
											elseif(isset($get_artist['community_id']))
											{
												$get_profile_type = $display->types_of_search('community',$get_artist['community_id']);
												if(!empty($get_profile_type) && $get_profile_type!=Null)
												{
													$res_profile_type = mysql_fetch_assoc($get_profile_type);
													if($res_profile_type['type_name']!=""){
													?>
														<br/><span style="font-size:13px; font-weight:normal;">
														<?php 
														if(strlen($res_profile_type['type_name'])>26){
															echo substr($res_profile_type['type_name'],0,24)."...";
														}else{
															echo $res_profile_type['type_name'];
														}
														if($res_profile_type['subtype_name']!="" && strlen($res_profile_type['type_name']<26)){
															$type_length = strlen($res_profile_type['type_name']);
															if($type_length + strlen($res_profile_type['subtype_name'])>22){
																echo ", ".substr($res_profile_type['subtype_name'],0,22 - $type_length)."...";
															}else{
																echo ", ".$res_profile_type['subtype_name'];
															}
															
														}
														/*echo $res_profile_type['type_name'];
														if($res_profile_type['subtype_name']!=""){
															echo ", ".$res_profile_type['subtype_name'];
														}*/
														?>
														</span>
												<?php
													}
												}
											} ?></div></div>
								</div>
					<?php
									}
								}
							}	
						}
		}
		?>
		<input type="hidden" id="videotype_value_artist" value="<?php echo $videotype_value_artist;?>"/>
		<input type="hidden" id="songtype_value_artist" value="<?php echo $songtype_value_artist;?>"/>
		<script type="text/javascript">
		$("document").ready(function(){
			var a = document.getElementById("videotype_value_artist").value;
			var b = document.getElementById("songtype_value_artist").value;
			if((a =="" || a ==" ") && (b=="" || b==" "))
			{
				$("#profileFeaturedproart").css({"display":"none"});
			}
		});
		
		</script>
			</div>
		</div>
		<?php
	}
	else
	{
		if($artist_check !=0 || $community_check !=0)
				{
			?>
					<div id="promotionsTab" class="hiddenBlock7">
					</div>
			<?php
				}
				else
				{
			?>
					<div id="servicesTab" class="hiddenBlock4">
					</div>
			<?php
				}
			?> 
		<!--<div id="mediaTab" class="hiddenBlock1">-->
		
	<?php
	}
	?>
</div>




    <!-- PROFILE END -->
  <!--</div>-->
</div>
</div>
<?php include_once("displayfooter.php"); ?>
  <!-- Gallery -->

<div id="light-gal" class="gallery-wrapper">
	<div id="bg">
		<a href="#" class="nextImageBtn" title="next"></a>
		<a href="#" class="prevImageBtn" title="previous"></a>
		<img width="1680" src="" height="1050" alt="" title="" id="bgimg" />
	</div>
	<div id="preloader"><img src=".././galleryfiles/ajax-loader_dark.gif" width="32" height="32" /></div>
	<div id="img_title"></div>
	<div id="toolbar">
		<a href="#" title="Pause" id="pauseImg" onClick="setimage('pause')"><img src=".././galleryfiles/pause.png" width="50" height="50"  /></a>
		<a href="#" title="Maximize" id="maximizeImg" onClick="ImageViewMode('full');return false"><img src=".././galleryfiles/toolbar_fs_icon.png" width="50" height="50"  /></a>
		<script type="text/javascript">
		<!--[if IE]>
		$("#maximizeImg").css({"display":"none"});
		<![endif]-->
		</script>
		<a href="#" title="Close" id="closeGal" onClick="CloseGallery();return false"><img src=".././galleryfiles/close.png" width="50" height="50"  /></a>
	</div>
	<div id="thumbnails_wrapper">
	<div id="outer_container">
	<div class="thumbScroller">
		<div class="container" id="gallery-container">
			
		</div>
	</div>
	</div>
	</div>
</div>

<!-- Profile Login Required Box -->
<div id="popupContact">
		<a style="cursor:pointer;" id="popupContactClose">x</a>
		<h1 class="title_light_boxes">Register <a href="registration_up.php">Here</a>, It's Free!</h1><br />
		<div class="light_fancies">
			<p id="contactArea">
				Only registered users can view media. 
			</p>
			<p>Already Registered ? Login below.</p>
			<form name="checkLoginFrm" method="post">
				<div class="formCont">
					<div class="formFieldCont">
					<div class="fieldTitle">Username</div>
					<input type="text" class="textfield" name="username" value="Email" onfocus="if(this.value == 'Email'){this.value = '';}" onblur="if(this.value == ''){this.value='Email';}" />
				</div>
					<div class="formFieldCont">
					<div class="fieldTitle">Password</div>
					<input name="password_fake2" id="password_fake2" onfocus="myfunc2()" value="Password" type="text" class="textfield" />
					<input name="userpass" id="userpass" onblur="myfuncs2()" style="display:none;"  type="password" class="textfield" />
				</div>
					<div class="formFieldCont">
					<div class="fieldTitle"></div>
					<input type="image" class="login" src="../images/profile/login.png" style="width:47px;" width="47" height="23" />
				</div>
			</form>
		</div>
        </div>
	</div>
	
	<div id="zipes_desc" style="display:none;">
		<div class="fancy_header">
			<?php if($getuser['name']!="") { echo $getuser['name']; } else { echo $getuser['title']; }?>
		</div>
		<div id="contents_frames" style="float:left; width:450px;">
			<p style="margin: 0px;">
	<?php
				if($getuser['bio']!="")
				{
					echo $getuser['bio'];
				}
				elseif($getuser['description']!="")
				{
					echo $getuser['description'];
				}
	?>
			</p>
		</div>
	</div>
		
    <div id="backgroundPopup"></div>
<!-- Login Box END -->
<a href="asktodonate.php" class="fancybox fancybox.ajax" id="get_donationpop" style="display:none">fs</a>
<a href="asktodonate_pro.php" class="fancybox fancybox.ajax" id="get_donationpop_pro" style="display:none">fs</a>
<a href="asktodonate_pro.php" class="fancybox fancybox.ajax" id="get_donationpop_alpro" style="display:none">fs</a>
<a id="ask_name_email" style="display:none" class="fancybox fancybox.ajax" name="ask_name_email" href="ask_name_aftr_don.php">ask</a>
<a id="ask_name_email_single" style="display:none" class="fancybox fancybox.ajax" name="ask_name_email_single" href="ask_single_name_aftr_don.php">ask</a>
<a href="handle_login.php" style="display:none;" class="fancybox fancybox.ajax" id="logged_chk_user"></a>

<a id="ask_name_alemail" style="display:none" class="fancybox fancybox.ajax" name="ask_name_alemail" href="ask_name_aftr_don.php">ask</a>
<input type="hidden" id="curr_playing_playlist" name="curr_playing_playlist">
</body>
<script src="../galleryfiles/jquery.easing.1.3.js"></script>
<script type="text/javascript" src="../galleryfiles/jqgal.js"></script>
</html>

<script>

//function checkload(defImg,gIndex){
function checkload(defImg,Images,orgpath,thumbpath,title,media_id){
	var arr_sep = Images.split(',');
	var title_sep = null;
	title_sep = title.split(',');
	//alert(title_sep);
	var k;
	var data = new Array();
	for(k=0;k<arr_sep.length;k++)
	{
		//arr_sep[k] = arr_sep[k].replace("\"","");
		arr_sep[k] = arr_sep[k].replace(/"/g, '');
		arr_sep[k] = arr_sep[k].replace("[","");
		arr_sep[k] = arr_sep[k].replace("]","");
		data[k] = arr_sep[k];
	}
	
	var q;
	var data1 = new Array();
	for(q=0;q<title_sep.length;q++)
	{
		//arr_sep[k] = arr_sep[k].replace("\"","");
		title_sep[q] = title_sep[q].replace(/"/g, '');
		title_sep[q] = title_sep[q].replace("[","");
		title_sep[q] = title_sep[q].replace("]","");
		data1[q] = title_sep[q];
	}
	
	if(data.length>0){
		$("#gallery-container").html('');
	}
	//$("#bgimg").attr('src','<?php echo $orgPath;?>'+defImg);
	//alert(orgpath+defImg);
	$("#bgimg").attr('src',thumbpath+defImg);
	

	for(var i=0;i<data.length;i++){

		$("#gallery-container").append("<div class='content'><div><a href='"+thumbpath+data[i]+"'><img  src='"+orgpath+data[i]+"' title='"+data1[i]+"' alt='"+data1[i]+"' class='thumb' /></a></div></div>");
	}
	//setTimeout(function(){	loadGallery();	$("#light-gal").css('visibility','visible');	},500);
	loadGallery();
	$("#light-gal").css('visibility','visible');
	$("#miniFooter").css('display','none');
	$("#profileHeader").css('display','none');
	$("#contentContainer").css('display','none');
	
	//$("#mediaTab").css('display','none');
	$(".accountSettings").css('z-index','1');

	document.getElementById("gallery-container").style.left = "0px";
	$.ajax({
			type: "POST",
			url: 'update_media.php',
			data: { "media_id":media_id },
			success: function(data){
			}
	});
}
	$(document).ready(function(){
		
		$("a[rel^='prettyPhoto']").prettyPhoto({social_tools:''});
		$("a[rel^='prettyAudio']").prettyPhoto({social_tools:'',
			changepicturecallback: function(){ $('audio').mediaelementplayer(); $(".mejs-container").css('margin-left',0); }	
		});
		
		$("#logged_chk_user").fancybox({
				helpers : {
					media : {},
					buttons : {},
					title : null
				}
			});
	});

	$(".login").click(function(){
		checkLogin();//document.checkLoginFrm.submit();
	});

function PlayAll(what,id)
{
	var get_id = document.getElementById(id).value;
	var ex_id =get_id.split(",");
	var type;
	var counter =1;
	function next()
	{
		 if (counter < ex_id.length)
		 {
			if(what == "v")
			{
				if($("#curr_playing_playlist").val()!="")
				{
					type = "addvi";
				}else{
					if(counter ==1)
					{
						type = "playlist";
					}
					else
					{
						type = "addvi";
					}
				}
			}
			else
			{
				if($("#curr_playing_playlist").val()!="")
				{
					type = "add";
				}else{
					if(counter ==1)
					{
						type = "play";
					}
					else
					{
						type = "add";
					}
				}
			}
			//document.getElementById(""id).value;
			  showPlayer_playall(what,ex_id[counter],type);
			  setTimeout(next, 2000);
			  counter++;
		  }
	}
	  next();
}

function PlayAll_featured_media(ids,idv)
{
	var get_video_id = document.getElementById(idv).value;
	var ex_video_id =get_video_id.split(",");
	var get_song_id = document.getElementById(ids).value;
	var ex_song_id =get_song_id.split(",");
	var type;
	var counter_video =1;
	var counter_song =1;
	function nextvideo()
	{
		what = "v";
		if (counter_video < ex_video_id.length)
		{
			if($("#curr_playing_playlist").val()!="")
			{
				type = "addvi";
			}else{
				if(counter_video ==1)
				{
					type = "playlist";
				}
				else
				{
					type = "addvi";
				}
			}
			if(ex_video_id[counter_video] != 0){
				showPlayer_playall(what,ex_video_id[counter_video],type);
			}
			setTimeout(nextvideo, 2000);
			counter_video++;
		}
	}
	nextvideo();
	function nextsong()
	{
		what = "a";
		if (counter_song < ex_song_id.length)
		{
			if($("#curr_playing_playlist").val()!="")
			{
				type = "add";
			}else{
				if(counter_song ==1)
				{
					type = "play";
				}
				else
				{
					type = "add";
				}
			}
			if(ex_song_id[counter_song] != 0){
				showPlayer_playall(what,ex_song_id[counter_song],type);
			}
			setTimeout(nextsong, 2000);
			counter_song++;
		}
	}
	nextsong();
}
function PlayAll_featured_media_events(ids_up,idv_up,ids_rec,idv_rec)
{
	var type;
	var counter_video_upcom =1;
	var counter_song_upcom =1;
	var counter_video_rec =1;
	var counter_song_rec =1;
	if(document.getElementById(idv_up)!= null){
		var get_video_id_upcom = document.getElementById(idv_up).value;
		var ex_video_id_upcom =get_video_id_upcom.split(",");
		function nextvideo_upcom()
		{
			what = "v";
			if (counter_video_upcom < ex_video_id_upcom.length)
			{
				if($("#curr_playing_playlist").val()!="")
				{
					type = "addvi";
				}else{
					if(counter_video_upcom ==1)
					{
						type = "playlist";
					}
					else
					{
						type = "addvi";
					}
				}
				if(ex_video_id_upcom[counter_video_upcom] != 0){
					showPlayer_playall(what,ex_video_id_upcom[counter_video_upcom],type);
				}
				setTimeout(nextvideo_upcom, 2000);
				counter_video_upcom++;
			}
		}
		nextvideo_upcom();
	}if(document.getElementById(ids_up)!=null){
		var get_song_id_upcom = document.getElementById(ids_up).value;
		var ex_song_id_upcom =get_song_id_upcom.split(",");
		function nextsong_upcom()
		{
			what = "a";
			if (counter_song_upcom < ex_song_id_upcom.length)
			{
				if($("#curr_playing_playlist").val()!="")
				{
					type = "add";
				}else{
					if(counter_song_upcom ==1)
					{
						type = "play";
					}
					else
					{
						type = "add";
					}
				}
				if(ex_song_id_upcom[counter_song_upcom] != 0){
					showPlayer_playall(what,ex_song_id_upcom[counter_song_upcom],type);
				}
				setTimeout(nextsong_upcom, 2000);
				counter_song_upcom++;
			}
		}
		nextsong_upcom();
	}
	
	if(document.getElementById(idv_rec)!= null){
		var get_video_id_rec = document.getElementById(idv_rec).value;
		var ex_video_id_rec =get_video_id_rec.split(",");
		function nextvideo_rec()
		{
			what = "v";
			if (counter_video_rec < ex_video_id_rec.length)
			{
				if($("#curr_playing_playlist").val()!="")
				{
					type = "addvi";
				}else{
					if(counter_video_rec ==1)
					{
						type = "playlist";
					}
					else
					{
						type = "addvi";
					}
				}
				if(ex_video_id_rec[counter_video_rec] != 0){
					showPlayer_playall(what,ex_video_id_rec[counter_video_rec],type);
				}
				setTimeout(nextvideo_rec, 2000);
				counter_video_rec++;
			}
		}
		nextvideo_rec();
	}if(document.getElementById(ids_rec)!=null){
		var get_song_id_rec = document.getElementById(ids_rec).value;
		var ex_song_id_rec =get_song_id_rec.split(",");
		function nextsong_rec()
		{
			what = "a";
			if (counter_song_rec < ex_song_id_rec.length)
			{
				if($("#curr_playing_playlist").val()!="")
				{
					type = "add";
				}else{
					if(counter_song_rec ==1)
					{
						type = "play";
					}
					else
					{
						type = "add";
					}
				}
				if(ex_song_id_rec[counter_song_rec] != 0){
					showPlayer_playall(what,ex_song_id_rec[counter_song_rec],type);
				}
				setTimeout(nextsong_rec, 2000);
				counter_song_rec++;
			}
		}
		nextsong_rec();
	}
}
</script>
<script type="text/javascript">
function down_video_pop(id)
{
	
	/*if(logedinFlag =="")
	{
		$("#contactArea").html("Please login to use This Facility.");
		centerPopup();
		loadPopup();
		return false;
	}
	else
	{*/
		var Dvideolink = document.getElementById("download_video"+id);
		Dvideolink.click();
	//}
}
function down_video_pop_project(id)
{
	var Dvideolink = document.getElementById("download_videoproject"+id);
	Dvideolink.click();
}
function down_video_pop_event(id)
{
	var Dvideolink = document.getElementById("download_videoevent"+id);
	Dvideolink.click();
}
function down_video_pop_recevent(id)
{
	var Dvideolink = document.getElementById("download_videorecevent"+id);
	Dvideolink.click();
}
function down_song_pop(id)
{
	
	/*if(logedinFlag =="")
	{
		$("#contactArea").html("Please login to use This Facility.");
		centerPopup();
		loadPopup();
		return false;
	}
	else
	{*/
		var Dvideolink = document.getElementById("download_song"+id);
		Dvideolink.click();
	//}
}
function down_song_pop_project(id)
{
	var Dvideolink = document.getElementById("download_songproject"+id);
	Dvideolink.click();
}
function down_song_pop_event(id)
{
	var Dvideolink = document.getElementById("download_songevent"+id);
	Dvideolink.click();
}
function down_song_pop_recevent(id)
{
	var Dvideolink = document.getElementById("download_songrecevent"+id);
	Dvideolink.click();
}
function down_song_art_friend(id){
	var Dvideolink = document.getElementById("download_songartfri"+id);
	Dvideolink.click();}
function down_song_com_friend(id){
	var Dvideolink = document.getElementById("download_songcomfri"+id);
	Dvideolink.click();}
function down_song_artpro_friend1(id){
	var Dvideolink = document.getElementById("download_songartpro1"+id);
	Dvideolink.click();}
function down_song_artpro_friend2(id){
	var Dvideolink = document.getElementById("download_songfriartpro2"+id);
	Dvideolink.click();}
function down_song_genartfriend(id){
	var Dvideolink = document.getElementById("download_songgenartfri"+id);
	Dvideolink.click();}
function down_song_artpro_friend3(id){
	var Dvideolink = document.getElementById("download_songartprofri3"+id);
	Dvideolink.click();}
function down_song_artpro_friend4(id){
	var Dvideolink = document.getElementById("download_songartprofr4"+id);
	Dvideolink.click();}


function down_video_art_friend(id){
	var Dvideolink = document.getElementById("download_videoartfri"+id);
	Dvideolink.click();}
function down_video_com_friend(id){
	var Dvideolink = document.getElementById("download_videocomfri"+id);
	Dvideolink.click();}
function down_video_artpro_friend1(id){
	var Dvideolink = document.getElementById("download_videoartpro1"+id);
	Dvideolink.click();}
function down_video_artpro_friend2(id){
	var Dvideolink = document.getElementById("download_videofriartpro2"+id);
	Dvideolink.click();}
function down_video_genartfriend(id){
	var Dvideolink = document.getElementById("download_videogenartfri"+id);
	Dvideolink.click();}
function down_video_artpro_friend3(id){
	var Dvideolink = document.getElementById("download_videoartprofri3"+id);
	Dvideolink.click();}
function down_video_artpro_friend4(id){
	var Dvideolink = document.getElementById("download_videoartprofri4"+id);
	Dvideolink.click();}

function down_song_artist_friend(id){
	var Dvideolink = document.getElementById("download_songartistfri"+id);
	Dvideolink.click();}
function down_video_artist_friend(id){
	var Dvideolink = document.getElementById("download_videoartistfri"+id);
	Dvideolink.click();}
function down_song_community_friend(id){
	var Dvideolink = document.getElementById("download_songcomfriend"+id);
	Dvideolink.click();}
function down_video_community_friend(id){
	var Dvideolink = document.getElementById("download_videocomfriend"+id);
	Dvideolink.click();}



function down_gallery_pop(id)
{
	if(id == "nosale")
	{
		alert("This Gallery Is Not For Sale");
		return false;
	}
	else if(logedinFlag =="")
	{
		$("#logged_chk_user").click();
	}
	else
	{
		var Dgallink = document.getElementById("download_gallery"+id);
		Dgallink.click();
	}
}

function autoRotate(){
	if(setTimer==1){
		$("a.nextImageBtn").click();
	}
}

function setimage(playMode){
	if(playMode=="pause"){
		$("#pauseImg").find("img").attr("src", ".././galleryfiles/play.png");
		$("#pauseImg").attr("onClick", "ImagePlayMode('play');return false").attr("title", "Play");
		setTimer=0;
		$rotate_val =1;
		GalleryAutoPlayTimer = null;
		//window.clearInterval(GalleryAutoPlayTimer);
	} else {
		$("#pauseImg").find("img").attr("src", ".././galleryfiles/pause.png");
		$("#pauseImg").attr("onClick", "ImagePlayMode('pause');return false").attr("title", "Pause");
		setTimer=1;
		GalleryAutoPlayTimer=self.setInterval("autoRotate()",4000);
	}

}




/////// Related Player	
//var win_obj = $("#curr_playing_playlist").val();
//if(win_obj==""){
var windowRef=false;
//}
var profileName = "Mithesh";
function showPlayer(tp,ref,act){
//alert(act);
//alert('<?php echo $ans_sql['general_user_id'] ;?>');
	//var windowRef = window.open('player/?tp='+tp+'&ref='+ref+'&source='+source);
	if(logedinFlag =="")
	{
		//$("#contactArea").html("Only registered users can view media.");
		//centerPopup();
		//loadPopup();
		//return false;
	}

	var pl = $("#curr_playing_playlist").val();
	$.ajax({
		type: "POST",
		url: 'player/getMediaInfo.php',
		data: { "tp":tp, "ref":ref, "source":"<?php echo $ans_sql['general_user_id']; ?>", "act":act, "pl":pl, "creator":profileName, "update":"update" },
		success: function(data){
			get_cookie(data);
			//var get_obj = getCookieValue_for_obj("windowobj");
			//alert(get_obj);
			/*if(get_obj=="")
			{
				alert("inif");
				
				get_cookie_obj(windowRef);
			}else{
			alert("inelse");
				windowRef = get_obj;
				//windowRef.focus();
				//alert(get_obj.location.href);
				alert(windowRef.location.);
			}
			alert(windowRef.location);*/
			var is_chrome = navigator.userAgent.toLowerCase().indexOf('chrome') > -1;
			if(is_chrome ==true){
				WinHeight = 579;
			}else{
				WinHeight = 580;
			}
			windowRef = window.open('', 'formpopup', 'height='+ WinHeight +',width=580,scrollbars');
			if(windowRef == null || typeof(windowRef) == "undefined") {
			   alert("The media player is a pop up player to give you uninterrupted streaming while browsing the site. To view it you must allow pop ups from Purify Art in the settings of your browser.");
			   return false;
			}
			
			if(windowRef.location == "about:blank" || (act!="add" && act!="addvi")){
			//alert("hey");
				windowRef.location.href='combinePlayer/index.php#tp='+tp+'&ref='+ref+'&pl='+data+'&buyer_id='+'<?php echo $_SESSION['gen_user_id'];?>'+'&act=play';
				//jQuery(window).blur();
				windowRef.focus();
			}
			else{
				//alert("yo");
				windowRef.location.href='combinePlayer/index.php#tp='+tp+'&ref='+ref+'&pl='+data+'&buyer_id='+'<?php echo $_SESSION['gen_user_id'];?>';
				windowRef.focus();
			}
		}
	});
}

function showPlayer_playall(tp,ref,act){
	var pl = $("#curr_playing_playlist").val();
	$.ajax({
		type: 'POST',
		url: '/player/getMediaInfo.php',
		data: { "tp":tp, "ref":ref, "source":"<?php echo $ans_sql['general_user_id']; ?>", "act":act, "pl":pl, "creator":profileName, "update":"update" },
		success: function(data){
			get_cookie(data);
			var is_chrome = navigator.userAgent.toLowerCase().indexOf('chrome') > -1;
			if(is_chrome ==true){
				WinHeight = 579;
			}else{
				WinHeight = 580;
			}
			windowRef = window.open('', 'formpopup', 'height='+WinHeight+',width=587,resizable=no,scrollbars');
			if(windowRef == null || typeof(windowRef) == "undefined") {
			   alert("The media player is a pop up player to give you uninterrupted streaming while browsing the site. To view it you must allow pop ups from Purify Art in the settings of your browser.");
			   return false;
			}
			if(windowRef.location == "about:blank" || (act!="add" && act!="addvi")){
				windowRef.location.href='combinePlayer/index.php#tp='+tp+'&ref='+ref+'&pl='+data+'&buyer_id='+'<?php echo $_SESSION['gen_user_id'];?>'+'&act=play'+'&chan=channel';
				//jQuery(window).blur();
				windowRef.focus();
			}
			else{
				windowRef.location.href='combinePlayer/index.php#tp='+tp+'&ref='+ref+'&pl='+data+'&buyer_id='+'<?php echo $_SESSION['gen_user_id'];?>'+'&chan=channel';
				//windowRef.focus();
			}
		}, 
		error: function(jqXHR, textStatus, errorThrown) 
          {
              //alert(errorThrown);
           }
	});
}

function showPlayer_reg(tp,ref,act,tbl){
	if(logedinFlag =="")
	{
		//$("#contactArea").html("Only registered users can view media.");
		//centerPopup();
		//loadPopup();
		//return false;
	}

	var pl = $("#curr_playing_playlist").val();
	$.ajax({
		type: "POST",
		url: 'player/getMediaInfo.php',
		data: { "tp":tp, "ref":ref, "tbl":tbl, "source":"<?php echo $ans_sql['general_user_id']; ?>", "act":act, "pl":pl, "creator":profileName, "update":"update" },
		success: function(data){
		//alert(data);
			//$("#curr_playing_playlist").val(data);
			get_cookie(data);
			var is_chrome = navigator.userAgent.toLowerCase().indexOf('chrome') > -1;
			if(is_chrome ==true){
				WinHeight = 579;
			}else{
				WinHeight = 580;
			}
			windowRef = window.open('', 'formpopup', 'height='+WinHeight+',width=580,resizable=no,scrollbars');
			if(windowRef == null || typeof(windowRef) == "undefined") {
			   alert("The media player is a pop up player to give you uninterrupted streaming while browsing the site. To view it you must allow pop ups from Purify Art in the settings of your browser.");
			   return false;
			}
			if(windowRef.location == "about:blank" || (act!="add" && act!="addvi" && act!="add_reg" && act!="addvi_reg")){
				windowRef.location.href='combinePlayer/index.php#tp='+tp+'&ref='+ref+'&pl='+data+'&buyer_id='+'<?php echo $_SESSION['gen_user_id'];?>'+'&act=play';
				//jQuery(window).blur();
				windowRef.focus();
			}
			else{
				windowRef.location.href='combinePlayer/index.php#tp='+tp+'&ref='+ref+'&pl='+data+'&buyer_id='+'<?php echo $_SESSION['gen_user_id'];?>';
				//jQuery(window).blur();
				windowRef.focus();
			}
		}
	});
}
function login_subscription()
{	
	if(logedinFlag =="")
	{
		var url_sec = document.getElementById("logged_chk_user");
		url_sec.href = "handle_login.php?foll&title="+encodeURIComponent(document.title);
		$("#logged_chk_user").click();
	}
	else
	{
		$("#email_subscription").click();
	}
}

function login_subscribed()
{	
	if(logedinFlag =="")
	{
		var url_sec = document.getElementById("logged_chk_user");
		url_sec.href = "handle_login.php?foll&title="+encodeURIComponent(document.title);
		$("#logged_chk_user").click();
	}
	else
	{
		$("#email_subscribed").click();
	}
}

function open_desc()
{
	/* $("#contactArea_desc").html("");
	centerPopup_desc();
	loadPopup_desc();
	return false; */
	
	$("#more_link").fancybox({
		content: $("#zipes_desc").html(),
		helpers : {
				media : {},
				buttons : {},
				title : null
			}
	});
}

function login_subscription_event()
{
	if(logedinFlag =="")
	{
		var url_sec = document.getElementById("logged_chk_user");
		url_sec.href = "handle_login.php?foll&title="+encodeURIComponent(document.title);
		$("#logged_chk_user").click();
	}
	else
	{
		$("#email_subscription_eve").click();
	}
}

function login_subscribed_event()
{
	if(logedinFlag =="")
	{
		var url_sec = document.getElementById("logged_chk_user");
		url_sec.href = "handle_login.php?foll&title="+encodeURIComponent(document.title);
		$("#logged_chk_user").click();
	}
	else
	{
		$("#email_subscribed_eve").click();
	}
}

function get_cookie(data)
{
	document.cookie="filename=" + data;
	getCookieValue("filename");
}
/*function get_cookie_obj(data)
{
	document.cookie="windowobj=" + data;
	getCookieValue_for_obj("windowobj");
}*/
function getCookieValue(key)
{
	currentcookie = document.cookie;
	if (currentcookie.length > 0)
	{
		firstidx = currentcookie.indexOf(key + "=");
		if (firstidx != -1)
		{
			firstidx = firstidx + key.length + 1;
			lastidx = currentcookie.indexOf(";",firstidx);
			if (lastidx == -1)
			{
				lastidx = currentcookie.length;
			}
			getdata = unescape(currentcookie.substring(firstidx, lastidx));
			$("#curr_playing_playlist").val(unescape(currentcookie.substring(firstidx, lastidx)));
			//return unescape(currentcookie.substring(firstidx, lastidx));
		}
		
	}
	return "";
}
/*function getCookieValue_for_obj(key)
{
	currentcookie = document.cookie;
	if (currentcookie.length > 0)
	{
		firstidx = currentcookie.indexOf(key + "=");
		if (firstidx != -1)
		{
			firstidx = firstidx + key.length + 1;
			lastidx = currentcookie.indexOf(";",firstidx);
			if (lastidx == -1)
			{
				lastidx = currentcookie.length;
			}
			return unescape(currentcookie.substring(firstidx, lastidx));
		}
		
	}
	return "";
}*/
//getCookieValue("filename");
$("document").ready(function(){
window.setInterval("getCookieValue('filename')",500);
});
function download_directly_song(id,t,c,f){
	var val = confirm("Are you sure you want to download "+t+" by "+c+" from "+f+"?");
	if(val==true){
	window.location = "downloads_ok_zip.php?download_meds_zip="+id+"&single_pocess=true";
	}
}
function download_directly_project(id,t,c,typ_zip,id_zip){
	var val = confirm("Are you sure you want to download "+t+" by "+c+"?");
	if(val==true){
	window.location = "downloads_ok_zip.php?download_meds_zip="+id_zip+"&prject_trues=false"+"&pro_typ_zip="+typ_zip;
	}
}
function myfunc2()
{
	
	$("#password_fake2").hide();
	$("#userpass").show().focus();
}

function myfuncs2()
{
	var pass_val = document.getElementById("userpass");
	if(pass_val.value=="")
	{
		$("#password_fake2").show();
		$("#userpass").hide();
	}
}
var $_returnvalue = "";
var $_returnvalue_tip_user = 0;
var $_returnclicktext ="";
var $_returnsong_id ="";

var $_returnvalue_pro = "";
var $_returnvalue_tip_user_pro = 0;
var $_returnclicktext_pro ="";
var $_returnsong_id_pro ="";

var $_retval_pro_tip_gen_id_don = 0;
var $_retval_single_tip_gen_id_don = 0;
var $_retval_single_tip_gen_ids_aldon = "";
var $_retval_single_tip_gen_ids_aldon_pro = "";

var $_retval_pro_name_aftr_don = "";
var $_retval_pro_email_aftr_don = "";
var $_returnsong_id_pro_type = "";
var $_returnsong_id_pro_ids_zip = "";

var $_retval_single_name_aftr_don = "";
var $_retval_single_email_aftr_don = "";

function ask_to_donate(name,creator,song_id,clicktext,down_tip_gen_id_sin,get_all_detils_tip_fr)
{
	$("#get_donationpop").attr("href", "asktodonate.php?name="+encodeURIComponent(name)+"&creator="+encodeURIComponent(creator)+"&clicktext="+encodeURIComponent(clicktext)+"&song_id="+song_id+"&gen_user_down_tip_id="+down_tip_gen_id_sin+"&simple_detals_fr_tip="+get_all_detils_tip_fr);
	$("#ask_name_email_single").attr("href", "ask_single_name_aftr_don.php?name="+encodeURIComponent(name)+"&creator="+encodeURIComponent(creator));
	$("#get_donationpop").click();
}

function ask_to_donate_pro(name,creator,song_id,clicktext,get_all_detils_tip_fr_pro,tpy_pro_zip,id_zip)
{
	$("#get_donationpop_pro").attr("href", "asktodonate_pro.php?name="+encodeURIComponent(name)+"&creator="+encodeURIComponent(creator)+"&clicktext="+encodeURIComponent(clicktext)+"&song_id="+encodeURIComponent(song_id)+"&simple_detals_fr_tip="+get_all_detils_tip_fr_pro+"&pro_type="+tpy_pro_zip+"&pro_type_zip_id="+id_zip);
	$("#ask_name_email").attr("href", "ask_name_aftr_don.php?name="+encodeURIComponent(name)+"&creator="+encodeURIComponent(creator));
	$("#get_donationpop_pro").click();
}

function ask_to_donate_alpro(name,creator,song_id,clicktext,down_tip_gen_id,get_all_detils_tip_fr_alpro,tpy_pro_zip,id_zip)
{
	$("#get_donationpop_alpro").attr("href", "asktodonate_pro.php?name="+encodeURIComponent(name)+"&creator="+encodeURIComponent(creator)+"&clicktext="+encodeURIComponent(clicktext)+"&song_id="+encodeURIComponent(song_id)+"&gen_user_down_tip_id="+down_tip_gen_id+"&simple_detals_fr_tip="+get_all_detils_tip_fr_alpro+"&pro_type="+tpy_pro_zip+"&pro_type_zip_id="+id_zip);
	$("#ask_name_alemail").attr("href", "ask_name_aftr_don.php?name="+encodeURIComponent(name)+"&creator="+encodeURIComponent(creator));
	$("#get_donationpop_alpro").click();
}

function ask_to_donate_result()
{
	if($_returnvalue == 1){
		$_returnvalue ="";
		var Dvideolink = "";
		var Dvideolink = document.getElementById($_returnclicktext);
		setTimeout(function(){
		//alert(Dvideolink.href);
		$("#"+$_returnclicktext).attr("href", Dvideolink.href + "&don_tips_al="+$_returnvalue_tip_user);
		//alert(Dvideolink.href);
		Dvideolink.click();
		}
		,2000);
	}
	else if($_returnvalue == 2){
	<?php
			if(isset($_SESSION['login_email']))
			{
				if($_SESSION['login_email']!="")
				{
		?>
					setTimeout(function(){
					$_returnvalue ="";
						window.location = "downloads_ok_zip.php?download_meds_zip="+$_returnsong_id+"&single_pocess=true&gen_down_tip_id="+$_retval_single_tip_gen_id_don+"&gen_tip_al_ids="+$_retval_single_tip_gen_ids_aldon;
					}
					,1000);
		<?php
				}
				else
				{
?>
					$_retval_single_name_aftr_don = "";
					$_retval_single_email_aftr_don = "";
					$("#ask_name_email_single").click();
<?php
				}
			}
			else
			{
?>
				$_retval_single_name_aftr_don = "";
				$_retval_single_email_aftr_don = "";
				$("#ask_name_email_single").click();
<?php
			}
		?>
	}
}

$("#ask_name_email_single").fancybox({
		helpers : {
			media : {},
			buttons : {},
			title : null,
		},
		afterClose : function(){
			if($_retval_single_name_aftr_don=="" || $_retval_single_email_aftr_don=="")
			{
				alert("Please give all your valid details.");
			}
			else
			{
				//call_single_download();	
			}
		}
	});
	
function call_single_download()
{
	setTimeout(function(){
		$_returnvalue ="";
			window.location = "downloads_ok_zip.php?download_meds_zip="+$_returnsong_id+"&single_pocess=true&sessio_oth_name="+encodeURIComponent($_retval_single_name_aftr_don)+"&sessio_oth_email="+encodeURIComponent($_retval_single_email_aftr_don)+"&gen_down_tip_id="+$_retval_single_tip_gen_id_don+"&gen_tip_al_ids="+$_retval_single_tip_gen_ids_aldon;
		}
		,1000);
}

function ask_to_donate_result_pro()
{
	if($_returnvalue_pro == 1){
		$_returnvalue_pro ="";
		var Dvideolink = "";
		var Dvideolink = document.getElementById($_returnclicktext_pro);
		setTimeout(function(){
		$("#"+$_returnclicktext_pro).attr("href", Dvideolink.href + "&don_tips_al_pro="+$_returnvalue_tip_user_pro);
		//alert(Dvideolink.href);
		Dvideolink.click();
		}
		,2000);
	}
	else if($_returnvalue_pro == 2){
		<?php
			if(isset($_SESSION['login_email']))
			{
				if($_SESSION['login_email']!="")
				{
		?>
					setTimeout(function(){
						$_returnvalue_pro ="";
							window.location = "downloads_ok_zip.php?download_meds_zip="+$_returnsong_id_pro_ids_zip+"&prject_trues=false&gen_down_tip_id="+<?php echo $getgeneral['general_user_id']; ?>+"&gen_tip_al_ids="+$_retval_single_tip_gen_ids_aldon_pro+"&pro_typ_zip="+$_returnsong_id_pro_type;
					},1000);
		<?php
				}
				else
				{
?>
					$_retval_pro_name_aftr_don = "";
					$_retval_pro_email_aftr_don = "";
					$("#ask_name_email").click();
<?php
				}
			}
			else
			{
?>
				$_retval_pro_name_aftr_don = "";
				$_retval_pro_email_aftr_don = "";
				$("#ask_name_email").click();
<?php
			}
		?>
	}
}

function call_download()
{
	setTimeout(function(){
		$_returnvalue_pro ="";
			window.location = "downloads_ok_zip.php?download_meds_zip="+$_returnsong_id_pro_ids_zip+"&prject_trues=false&sessio_oth_name="+encodeURIComponent($_retval_pro_name_aftr_don)+"&sessio_oth_email="+encodeURIComponent($_retval_pro_email_aftr_don)+"&gen_down_tip_id="+<?php echo $getgeneral['general_user_id']; ?>+"&gen_tip_al_ids="+$_retval_single_tip_gen_ids_aldon_pro+"&pro_typ_zip="+$_returnsong_id_pro_type;
	} ,1000);
}

function ask_to_donate_result_alpro()
{
	if($_returnvalue_pro == 1){
		$_returnvalue_pro ="";
		var Dvideolink = "";
		var Dvideolink = document.getElementById('fan_club_pro'+$_returnclicktext_pro);
		setTimeout(function(){
			$("#fan_club_pro"+$_returnclicktext_pro).attr("href", Dvideolink.href + "&don_tips_al_pro="+$_returnvalue_tip_user_pro);
			//alert(Dvideolink.href);
			Dvideolink.click();
		}
		,2000);
	}
	else if($_returnvalue_pro == 2){
<?php
			if(isset($_SESSION['login_email']))
			{
				if($_SESSION['login_email']!="")
				{
		?>
					setTimeout(function(){
					$_returnvalue_pro ="";
						window.location = "downloads_ok_zip.php?download_meds_zip="+$_returnsong_id_pro_ids_zip+"&prject_trues=false&gen_down_tip_id="+$_retval_pro_tip_gen_id_don+"&gen_tip_al_ids="+$_retval_single_tip_gen_ids_aldon_pro+"&pro_typ_zip="+$_returnsong_id_pro_type;
					}
					,1000);
		<?php
				}
				else
				{
?>
					$_retval_pro_name_aftr_don = "";
					$_retval_pro_email_aftr_don = "";
					$("#ask_name_alemail").click();
<?php
				}
			}
			else
			{
?>
				$_retval_pro_name_aftr_don = "";
				$_retval_pro_email_aftr_don = "";
				$("#ask_name_alemail").click();
<?php
			}
		?>
	}
}

function call_al_download()
{
	setTimeout(function(){
		$_returnvalue_pro ="";
			window.location = "downloads_ok_zip.php?download_meds_zip="+$_returnsong_id_pro_ids_zip+"&prject_trues=false&sessio_oth_name="+encodeURIComponent($_retval_pro_name_aftr_don)+"&sessio_oth_email="+encodeURIComponent($_retval_pro_email_aftr_don)+"&gen_down_tip_id="+$_retval_pro_tip_gen_id_don+"&gen_tip_al_ids="+$_retval_single_tip_gen_ids_aldon_pro+"&pro_typ_zip="+$_returnsong_id_pro_type;
		}
		,1000);
}

$(document).ready(function() {
	$("#get_donationpop").fancybox({
		helpers : {
			media : {},
			buttons : {},
			title : null,
		},
		afterClose : function(){
								ask_to_donate_result();	
							}
	});
});

/* $(document).ready(function() {
	$("#get_donationpop_pro").fancybox({
		'width'				: '35%',
		'height'			: '30%',
		'transitionIn'		: 'none',
		'transitionOut'		: 'none',
		'type'				: 'iframe',
		'onClosed'			: function(){
								ask_to_donate_result_pro();	
							}
	});
}); */

$(document).ready(function() {
	$("#get_donationpop_pro").fancybox({
		helpers : {
			media : {},
			buttons : {},
			title : null,
		},
		afterClose : function(){
						ask_to_donate_result_pro();	
					}
	});
	
	$("#ask_name_email").fancybox({
		helpers : {
			media : {},
			buttons : {},
			title : null,
		},
		afterClose : function(){
						if($_retval_pro_name_aftr_don=="" || $_retval_pro_email_aftr_don=="")
						{
							alert("Please give all your valid details.");
						}
						else
						{
							//call_download();	
						}
					}
	});
	
	$("#ask_name_alemail").fancybox({
		helpers : {
			media : {},
			buttons : {},
			title : null,
		},
		afterClose : function(){
						if($_retval_pro_name_aftr_don=="" || $_retval_pro_email_aftr_don=="")
						{
							alert("Please give all your valid details.");
						}
						else
						{
							//call_al_download();	
						}
					}
	});
	
});

$(document).ready(function() {
	$("#social_share").fancybox({
		helpers : {
			media : {},
			buttons : {},
			title : null,
		},
		afterClose : function() {
			location.reload();
		}
	});
});

$(document).ready(function() {
	$("#statistics_share").fancybox({
		helpers : {
			media : {},
			buttons : {},
			title : null,
		}
	});
});

$(document).ready(function() {
	$("#get_donationpop_alpro").fancybox({
		helpers : {
			media : {},
			buttons : {},
			title : null,
		},
		afterClose : function(){
						ask_to_donate_result_alpro();	
					}
	});
});

$(window).load(function() {
	$("#similar_as_below").css({"display":"block"});
	$("#loaders").css({"display":"none"});
	startTab();
});

function fancy_login_art_coms(url)
{
	if(logedinFlag!="")
	{
		$("#fan_club_pro_ca"+url).click();
	}
	else if(logedinFlag=="")	
	{
		var url_sec = document.getElementById("logged_chk_user");
		url_sec.href = "handle_login.php?fan&datas="+url;
		$("#logged_chk_user").click();
	}
}
</script>
<style>
.player{float: left; margin-bottom: 7px; width: 200px; height:25px; }
.type_subtype {font-size: 14px; font-weight: bold; line-height: 20px; width: 180px;height:60px;float:left;}
.tabItem{margin-bottom:20px;}
.imgs_pro { clear:both; margin-bottom: 0px !important; border-top: 1px solid #E8E8E8; border-left: 1px solid #E8E8E8; border-right: 1px solid #E8E8E8; }
#profileTabs { margin-top: 0; height: 42px; }
.both_wrap_per { background: none repeat scroll 0 0 #F0F0F0; height:84px; padding: 10px; width: 180px; border-bottom: 1px solid #E8E8E8; border-left: 1px solid #E8E8E8; border-right: 1px solid #E8E8E8; float:left; }
.by_details { font-weight: normal; }
.navButton { margin: 0 0 4px; width: 142px; float: left; position: absolute; left: 40%; top: 41%; }
.playMedia { float: left; margin-left: 1px; }
.playMedia a { height: inherit !important; left: 74px !important; width: inherit !important; border-radius: 6px 6px 6px 6px !important; bottom: 138px !important; clear: both; background: none repeat scroll 0 0 #333333; float:left; }
.playMedia a:hover { background:none repeat scroll 0 0 #000000; }
.playMedia .list_play img { margin-bottom: 0px !important; }
#profileFeaturedevent img { border: medium none; float: left; }
.sign_bio_up_buts { float:right; width:440px; height: 31px; margin-bottom: 8px; margin-top: 17px; }
#contents_frames{padding: 20px;background-color: #FFFFFF;border-radius:6px;}
</style>
<?php

if($community_project_check==0 && $artist_project_check==0 && $artist_event_check==0 && $community_event_check==0 && $community_check==0 && $artist_check==0)
{
	?>
	<script>
		//$(document).ready(function() {
			document.getElementById("searchlink").click();
		//});
	</script>
	<?php
}
?>