<?php include_once("commons/db.php");
	include_once("classes/News.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "//www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="//www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<title>Purify Art: About: News</title>
<?php
	include_once("includes/header.php"); 
?>

<div id="outerContainer"></div>
 <div id="contentContainer" >
    <div id="actualContent" class="sidepages">
      <h1>News</h1>
	<?php
		$obj_news = new News();
		$all_news = $obj_news->get_AllNews();
		if(count($all_news) && !empty($all_news))
		{
			for($n_i=0;$n_i<count($all_news);$n_i++)
			{
				if($all_news[$n_i]['posted_date']!="" && $all_news[$n_i]['title']!="")
				{
	?>
					<div class="newsPost">
						<h4><?php echo $all_news[$n_i]['posted_date']; ?></h4>
						<h3><?php echo $all_news[$n_i]['title']; ?></h3>
						<p><?php echo $all_news[$n_i]['paragraph']; ?></p>
						<!--<p> We're excited to be officially releasing PurifyArt.net on January 1st and bringing you this great lineup of artists to celebrate on January 5th. Come out and support! Check out the <a href="http://www.facebook.com/events/468025656568916/" target="_blank">event page</a> for details.</p>-->
					</div>
    <?php
				}
			}
		}
	?>
    </div>
    <div id="snipeSidebar">
    	<div class="snipe">
      	<h2>What is Purify Art?</h2>
        <p>Purify Art provides fans, artists, musicians, and businesses tools and opportunities to access the arts. <a href="about.php">More ></a></p>
      </div>
      <div class="snipe">
      	<h2>Register Now!</h2>
        <p>Interested in joining Purify Art? Sign up for free. <a href="index.php">More ></a></p>
      </div>
    </div>
    <div class="clearMe"></div>
</div>
<?php include("footer.php");?>
<!-- end of main container -->
</div>
</body>
</html>
