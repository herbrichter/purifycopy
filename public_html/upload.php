<?php
			$myBucket = "19rohit";
			//include the S3 class
            $root = realpath($_SERVER["DOCUMENT_ROOT"]);
            if (!class_exists('S3')) require_once ($root.'/S3.php');
			
			//AWS access info
			if (!defined('awsAccessKey')) define('awsAccessKey', '');
			if (!defined('awsSecretKey')) define('awsSecretKey', '');
			
			//instantiate the class
			$s3 = new S3(awsAccessKey, awsSecretKey);
			
			//check whether a form was submitted
			if(isset($_POST['register'])){
				echo "form submitted uploading media<br/>";
				//retreive post variables
				$fileName = $_FILES['image']['name'];
				$fileTempName = $_FILES['image']['tmp_name'];
				
				//create a new bucket
				$s3->putBucket($myBucket, S3::ACL_PUBLIC_READ);
				
				//move the file
				if ($s3->putObjectFile($fileTempName, $myBucket, $fileName, S3::ACL_PUBLIC_READ)) {
					echo "<strong>We successfully uploaded your file.</strong>";
				}else{
					echo "<strong>Something went wrong while uploading your file... sorry.</strong>";
				}
			}
		?>