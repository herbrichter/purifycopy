<?php
	include_once('commons/db.php');
	include_once('classes/Addfriend.php');
	include_once('classes/NewSuggestion.php');
	include_once('classes/Feeds_unsubcribe.php');
	include("newsmtp/PHPMailer/index.php");
	include_once('sendgrid/SendGrid_loader.php');
	session_start();
	$domainname=$_SERVER['SERVER_NAME'];
	$feeds_unsub = new Feeds_unsubcribe();
	$friend_noej = new Addfriend();
	$friend_newsj = new NewSuggestion();
	$sendgrid = new SendGrid('','');
	
	if(isset($_POST['acc_dec']))
	{
		$exp_acc_dec = explode('_',$_POST['acc_dec']);
		if($exp_acc_dec[0]=='aacc')
		{
			$sql_accept_up = $friend_noej->accept_friend_a($_SESSION['login_id'],$exp_acc_dec[1]); 
			echo "Accepted";
			
			$find_to = $friend_noej->general_user_sec($exp_acc_dec[1]);
			$find_from = $friend_noej->general_user_sec($_SESSION['login_id']);
			
			if(($find_from['artist_id']!=0 || $find_from['community_id']!=0) && $find_to['artist_id']!=0)
			{
				if($find_from['artist_id']!=0)
				{
					$sql_art_ag_ad = $friend_noej->friend_art($find_from['artist_id']);
					if(mysql_num_rows($sql_art_ag_ad)>0)
					{
						while($rg_aow_a_ad = mysql_fetch_assoc($sql_art_ag_ad))
						{
							if($rg_aow_a_ad['artist_view_selected']!="")
							{
								//$artist_view_exp = explode(',',$rg_aow_a_ad['artist_view_selected']);
								$artist_view_exp = $rg_aow_a_ad['artist_view_selected'].','.$find_to['artist_id'];
								$up_eve_art = $friend_noej->update_artist($artist_view_exp,$rg_aow_a_ad['artist_id']);
							}
							else
							{
								$artist_view_exp = ','.$find_to['artist_id'];
								$up_eve_art = $friend_noej->update_artist($artist_view_exp,$rg_aow_a_ad['artist_id']);
							}
						}
					}

					$sql_art_event_ad = $friend_noej->get_artisteve($find_from['artist_id']);
					if(mysql_num_rows($sql_art_event_ad)>0)
					{
						while($r_aow_e_ad = mysql_fetch_assoc($sql_art_event_ad))
						{
							if($r_aow_e_ad['artist_view_selected']!="")
							{
								//$artist_view_exp = explode(',',$r_aow_e_ad['artist_view_selected']);
								$artist_view_exp = $r_aow_e_ad['artist_view_selected'].','.$find_to['artist_id'];
								$up_eve_art = $friend_noej->update_artisteve($artist_view_exp,$r_aow_e_ad['id']);
							}
							else
							{
								$artist_view_exp = ','.$find_to['artist_id'];
								$up_eve_art = $friend_noej->update_artisteve($artist_view_exp,$r_aow_e_ad['id']);
							}
						}
					}
					
					$sql_art_project_ad = $friend_noej->get_artistpro($find_from['artist_id']);
					if(mysql_num_rows($sql_art_project_ad)>0)
					{
						while($r_aow_p_ad = mysql_fetch_assoc($sql_art_project_ad))
						{
							if($r_aow_p_ad['artist_view_selected']!="")
							{
								//$artist_view_exp = explode(',',$r_aow_p_ad['artist_view_selected']);
								$artist_view_exp = $r_aow_p_ad['artist_view_selected'].','.$find_to['artist_id'];
								$up_eve_art = $friend_noej->update_artistpro($artist_view_exp,$r_aow_p_ad['id']);
							}
							else
							{
								$artist_view_exp = ','.$find_to['artist_id'];
								$up_eve_art = $friend_noej->update_artistpro($artist_view_exp,$r_aow_p_ad['id']);
							}
						}
					}
				}
				
				if($find_from['community_id']!=0)
				{
					$sql_com_adc = $friend_noej->friend_com($find_from['community_id']);
					if(mysql_num_rows($sql_com_adc)>0)
					{
						while($r_aow_cc_ad = mysql_fetch_assoc($sql_com_adc))
						{
							if($r_aow_cc_ad['artist_view_selected']!="")
							{
								//$artist_view_exp = explode(',',$r_aow_cc_ad['artist_view_selected']);
								$artist_view_excp = $r_aow_cc_ad['artist_view_selected'].','.$find_to['artist_id'];
								$up_eve_art = $friend_noej->update_community($artist_view_excp,$r_aow_cc_ad['community_id']);
							}
							else
							{
								$artist_view_excp = ','.$find_to['artist_id'];
								$up_eve_art = $friend_noej->update_community($artist_view_excp,$r_aow_cc_ad['community_id']);
							}
						}
					}
					
					$sql_com_event_ad = $friend_noej->get_communityeve($find_from['community_id']);
					if(mysql_num_rows($sql_com_event_ad)>0)
					{
						while($r_aow_e_ad = mysql_fetch_assoc($sql_com_event_ad))
						{
							if($r_aow_e_ad['artist_view_selected']!="")
							{
								//$artist_view_exp = explode(',',$r_aow_e_ad['artist_view_selected']);
								$artist_view_exp = $r_aow_e_ad['artist_view_selected'].','.$find_to['artist_id'];
								$up_eve_art = $friend_noej->update_communityeve($artist_view_exp,$r_aow_e_ad['id']);
							}
							else
							{
								$artist_view_exp = ','.$find_to['artist_id'];
								$up_eve_art = $friend_noej->update_communityeve($artist_view_exp,$r_aow_e_ad['id']);
							}
						}
					}
					
					$sql_art_project_ad = $friend_noej->get_communitypro($find_from['community_id']);
					if(mysql_num_rows($sql_art_project_ad)>0)
					{
						while($r_aow_p_ad = mysql_fetch_assoc($sql_art_project_ad))
						{
							if($r_aow_p_ad['artist_view_selected']!="")
							{
								//$artist_view_exp = explode(',',$r_aow_p_ad['artist_view_selected']);
								$artist_view_exp = $r_aow_p_ad['artist_view_selected'].','.$find_to['artist_id'];
								$up_eve_art = $friend_noej->update_communitypro($artist_view_exp,$r_aow_p_ad['id']);
							}
							else
							{
								$artist_view_exp = ','.$find_to['artist_id'];
								$up_eve_art = $friend_noej->update_communitypro($artist_view_exp,$r_aow_p_ad['id']);
							}
						}
					}
				}
			}
			
			if(($find_from['artist_id']!=0 || $find_from['community_id']!=0) && $find_to['community_id']!=0)
			{
				if($find_from['artist_id']!=0)
				{
					$sqsl_art_ag_ad = $friend_noej->friend_art($find_from['artist_id']);
					if(mysql_num_rows($sqsl_art_ag_ad)>0)
					{	
						while($rg_aow_a_ad = mysql_fetch_assoc($sqsl_art_ag_ad))
						{
							if($rg_aow_a_ad['community_view_selected']!="")
							{
								//$artist_view_exp = explode(',',$rg_aow_a_ad['artist_view_selected']);
								$artist_view_exp = $rg_aow_a_ad['community_view_selected'].','.$find_to['community_id'];
								$up_eve_art = $friend_noej->update_artist_c($artist_view_exp,$rg_aow_a_ad['artist_id']);
							}
							else
							{
								$artist_view_exp = ','.$find_to['community_id'];
								$up_eve_art = $friend_noej->update_artist_c($artist_view_exp,$rg_aow_a_ad['artist_id']);
							}
						}
					}
					
					$sqls_art_event_ad = $friend_noej->get_artisteve($find_from['artist_id']);
					if(mysql_num_rows($sqls_art_event_ad)>0)
					{
						while($rce_aow_e_ad = mysql_fetch_assoc($sqls_art_event_ad))
						{
							if($rce_aow_e_ad['community_view_selected']!="")
							{
								//$artist_view_exp = explode(',',$rce_aow_e_ad['artist_view_selected']);
								$artist_view_exp = $rce_aow_e_ad['community_view_selected'].','.$find_to['community_id'];
								$up_eve_art = $friend_noej->update_artisteve_c($artist_view_exp,$rce_aow_e_ad['id']);
							}
							else
							{
								$artist_view_exp = ','.$find_to['community_id'];
								$up_eve_art = $friend_noej->update_artisteve_c($artist_view_exp,$rce_aow_e_ad['id']);
							}
						}
					}
					
					$sqdl_art_project_ad = $friend_noej->get_artistpro($find_from['artist_id']);
					if(mysql_num_rows($sqdl_art_project_ad)>0)
					{
						while($rcp_aow_p_ad = mysql_fetch_assoc($sqdl_art_project_ad))
						{
							if($rcp_aow_p_ad['community_view_selected']!="")
							{
								//$artist_view_exp = explode(',',$rcp_aow_p_ad['artist_view_selected']);
								$artist_view_exp = $rcp_aow_p_ad['community_view_selected'].','.$find_to['community_id'];
								$up_eve_art = $friend_noej->update_artistpro_c($artist_view_exp,$rcp_aow_p_ad['id']);
							}
							else
							{
								$artist_view_exp = ','.$find_to['community_id'];
								$up_eve_art = $friend_noej->update_artistpro_c($artist_view_exp,$rcp_aow_p_ad['id']);
							}
						}
					}
				}	
				
				if($find_from['community_id']!=0)
				{
					$sqls_com_adc = $friend_noej->friend_com($find_from['community_id']);
					if(mysql_num_rows($sqls_com_adc)>0)
					{
						while($r_aow_cc_ad = mysql_fetch_assoc($sqls_com_adc))
						{
							if($r_aow_cc_ad['community_view_selected']!="")
							{
								//$artist_view_exp = explode(',',$r_aow_cc_ad['community_view_selected']);
								$artist_view_excp = $r_aow_cc_ad['community_view_selected'].','.$find_to['community_id'];
								$up_eve_art = $friend_noej->update_community_c($artist_view_excp,$r_aow_cc_ad['community_id']);
							}
							else
							{
								$artist_view_excp = ','.$find_to['community_id'];
								$up_eve_art = $friend_noej->update_community_c($artist_view_excp,$r_aow_cc_ad['community_id']);
							}
						}
					}
					
					$sqls_com_event_ad = $friend_noej->get_communityeve($find_from['community_id']);
					if(mysql_num_rows($sqls_com_event_ad)>0)
					{
						while($rcde_aow_e_ad = mysql_fetch_assoc($sqls_com_event_ad))
						{
							if($rcde_aow_e_ad['community_view_selected']!="")
							{
								//$artist_view_exp = explode(',',$rcde_aow_e_ad['community_view_selected']);
								$artist_view_exp = $rcde_aow_e_ad['community_view_selected'].','.$find_to['community_id'];
								$up_eve_art = $friend_noej->update_communityeve_c($artist_view_exp,$rcde_aow_e_ad['id']);
							}
							else
							{
								$artist_view_exp = ','.$find_to['community_id'];
								$up_eve_art = $friend_noej->update_communityeve_c($artist_view_exp,$rcde_aow_e_ad['id']);
							}
						}
					}
					
					$sqsl_art_project_ad = $friend_noej->get_communitypro($find_from['community_id']);
					if(mysql_num_rows($sqsl_art_project_ad)>0)
					{
						while($rade_aow_p_ad = mysql_fetch_assoc($sqsl_art_project_ad))
						{
							if($rade_aow_p_ad['community_view_selected']!="")
							{
								//$artist_view_exp = explode(',',$rade_aow_p_ad['community_view_selected']);
								$artist_view_exp = $rade_aow_p_ad['community_view_selected'].','.$find_to['community_id'];
								$up_eve_art = $friend_noej->update_communitypro_c($artist_view_exp,$rade_aow_p_ad['id']);
							}
							else
							{
								$artist_view_exp = ','.$find_to['community_id'];
								$up_eve_art = $friend_noej->update_communitypro_c($artist_view_exp,$rade_aow_p_ad['id']);
							}
						}
					}
				}
			}
			//////////////////////////////////////////FROM//////////////////////////////////////////////
			
			
			/////////////////////////////////////////////////TO////////////////////////////////////////////////
			if(($find_to['community_id']!=0 || $find_to['artist_id']!=0) && $find_from['artist_id']!=0)
			{
				if($find_to['artist_id']!=0)
				{
					$sql_art_af_ad = $friend_noej->friend_art($find_to['artist_id']);
					if(mysql_num_rows($sql_art_af_ad)>0)
					{
						while($rf_aow_a_ad = mysql_fetch_assoc($sql_art_af_ad))
						{
							if($rf_aow_a_ad['artist_view_selected']!="")
							{
								//$artist_view_exp = explode(',',$rf_aow_a_ad['artist_view_selected']);
								$artist_view_exp = $rf_aow_a_ad['artist_view_selected'].','.$find_from['artist_id'];
								$up_eve_art = $friend_noej->update_artist($artist_view_exp,$rf_aow_a_ad['artist_id']);
							}
							else
							{
								$artist_view_exp = ','.$find_from['artist_id'];
								$up_eve_art = $friend_noej->update_artist($artist_view_exp,$rf_aow_a_ad['artist_id']);
							}
						}
					}
					
					$sql_art_event_ad = $friend_noej->get_artisteve($find_to['artist_id']);
					if(mysql_num_rows($sql_art_event_ad)>0)
					{
						while($r_aow_e_ad = mysql_fetch_assoc($sql_art_event_ad))
						{
							if($r_aow_e_ad['artist_view_selected']!="")
							{
								//$artist_view_exp = explode(',',$r_aow_e_ad['artist_view_selected']);
								$artist_view_exp = $r_aow_e_ad['artist_view_selected'].','.$find_from['artist_id'];
								$up_eve_art = $friend_noej->update_artisteve($artist_view_exp,$r_aow_e_ad['id']);
							}
							else
							{
								$artist_view_exp = ','.$find_from['artist_id'];
								$up_eve_art = $friend_noej->update_artisteve($artist_view_exp,$r_aow_e_ad['id']);
							}
						}
					}
					
					$sql_art_project_ad = $friend_noej->get_artistpro($find_to['artist_id']);
					if(mysql_num_rows($sql_art_project_ad)>0)
					{
						while($r_aow_p_ad = mysql_fetch_assoc($sql_art_project_ad))
						{
							if($r_aow_p_ad['artist_view_selected']!="")
							{
								//$artist_view_exp = explode(',',$r_aow_p_ad['artist_view_selected']);
								$artist_view_exp = $r_aow_p_ad['artist_view_selected'].','.$find_from['artist_id'];
								$up_eve_art = $friend_noej->update_artistpro($artist_view_exp,$r_aow_p_ad['id']);
							}
							else
							{
								$artist_view_exp = ','.$find_from['artist_id'];
								$up_eve_art = $friend_noej->update_artistpro($artist_view_exp,$r_aow_p_ad['id']);
							}
						}
					}
				}
				
				if($find_to['community_id']!=0)
				{
					$sql_com_adc = $friend_noej->friend_com($find_to['community_id']);
					if(mysql_num_rows($sql_com_adc)>0)
					{
						while($r_aow_cc_ad = mysql_fetch_assoc($sql_com_adc))
						{
							if($r_aow_cc_ad['artist_view_selected']!="")
							{
								//$artist_view_exp = explode(',',$r_aow_cc_ad['artist_view_selected']);
								$artist_view_excp = $r_aow_cc_ad['artist_view_selected'].','.$find_from['artist_id'];
								$up_eve_art = $friend_noej->update_community($artist_view_excp,$r_aow_cc_ad['community_id']);
							}
							else
							{
								$artist_view_excp = ','.$find_from['artist_id'];
								$up_eve_art = $friend_noej->update_community($artist_view_excp,$r_aow_cc_ad['community_id']);
							}
						}
					}
			
					$sql_com_event_ad = $friend_noej->get_communityeve($find_to['community_id']);
					if(mysql_num_rows($sql_com_event_ad)>0)
					{
						while($r_aow_e_ad = mysql_fetch_assoc($sql_com_event_ad))
						{
							if($r_aow_e_ad['artist_view_selected']!="")
							{
								//$artist_view_exp = explode(',',$r_aow_e_ad['artist_view_selected']);
								$artist_view_exp = $r_aow_e_ad['artist_view_selected'].','.$find_from['artist_id'];
								$up_eve_art = $friend_noej->update_communityeve($artist_view_exp,$r_aow_e_ad['id']);
							}
							else
							{
								$artist_view_exp = ','.$find_from['artist_id'];
								$up_eve_art = $friend_noej->update_communityeve($artist_view_exp,$r_aow_e_ad['id']);
							}
						}
					}
					
					$sql_art_project_ad = $friend_noej->get_communitypro($find_to['community_id']);
					if(mysql_num_rows($sql_art_project_ad)>0)
					{
						while($r_aow_p_ad = mysql_fetch_assoc($sql_art_project_ad))
						{
							if($r_aow_p_ad['artist_view_selected']!="")
							{
								//$artist_view_exp = explode(',',$r_aow_p_ad['artist_view_selected']);
								$artist_view_exp = $r_aow_p_ad['artist_view_selected'].','.$find_from['artist_id'];
								$up_eve_art = $friend_noej->update_communitypro($artist_view_exp,$r_aow_p_ad['id']);
							}
							else
							{
								$artist_view_exp = ','.$find_from['artist_id'];
								$up_eve_art = $friend_noej->update_communitypro($artist_view_exp,$r_aow_p_ad['id']);
							}
						}
					}
				}
			}
			
			if(($find_to['community_id']!=0 || $find_to['artist_id']!=0) && $find_from['community_id']!=0)
			{
				if($find_to['artist_id']!=0)
				{
					$sqlf_art_af_ad = $friend_noej->friend_art($find_to['artist_id']);
					if(mysql_num_rows($sqlf_art_af_ad)>0)
					{
						while($rf_aow_a_ad = mysql_fetch_assoc($sqlf_art_af_ad))
						{
							if($rf_aow_a_ad['community_view_selected']!="")
							{
								//$artist_view_exp = explode(',',$rf_aow_a_ad['community_view_selected']);
								$artist_view_exp = $rf_aow_a_ad['community_view_selected'].','.$find_from['community_id'];
								$up_eve_art = $friend_noej->update_artist_c($artist_view_exp,$rf_aow_a_ad['artist_id']);
							}
							else
							{
								$artist_view_exp = ','.$find_from['community_id'];
								$up_eve_art = $friend_noej->update_artist_c($artist_view_exp,$rf_aow_a_ad['artist_id']);
							}
						}
					}
				
					$sqls_art_event_ad = $friend_noej->get_artisteve($find_to['artist_id']);
					if(mysql_num_rows($sqls_art_event_ad)>0)
					{
						while($rce_aow_e_ad = mysql_fetch_assoc($sqls_art_event_ad))
						{
							if($rce_aow_e_ad['community_view_selected']!="")
							{
								//$artist_view_exp = explode(',',$rce_aow_e_ad['community_view_selected']);
								$artist_view_exp = $rce_aow_e_ad['community_view_selected'].','.$find_from['community_id'];
								$up_eve_art = $friend_noej->update_artisteve_c($artist_view_exp,$rce_aow_e_ad['id']);
							}
							else
							{
								$artist_view_exp = ','.$find_from['community_id'];
								$up_eve_art = $friend_noej->update_artisteve_c($artist_view_exp,$rce_aow_e_ad['id']);
							}
						}
					}
					
					$sqdl_art_project_ad = $friend_noej->get_artistpro($find_to['artist_id']);
					if(mysql_num_rows($sqdl_art_project_ad)>0)
					{
						while($rcp_aow_p_ad = mysql_fetch_assoc($sqdl_art_project_ad))
						{
							if($rcp_aow_p_ad['community_view_selected']!="")
							{
								//$artist_view_exp = explode(',',$rcp_aow_p_ad['community_view_selected']);
								$artist_view_exp = $rcp_aow_p_ad['community_view_selected'].','.$find_from['community_id'];
								$up_eve_art = $friend_noej->update_artistpro_c($artist_view_exp,$rcp_aow_p_ad['id']);
							}
							else
							{
								$artist_view_exp = ','.$find_from['community_id'];
								$up_eve_art = $friend_noej->update_artistpro_c($artist_view_exp,$rcp_aow_p_ad['id']);
							}
						}
					}
				}
					
				if($find_to['community_id']!=0)
				{
					$sqls_com_adc = $friend_noej->friend_com($find_to['community_id']);
					if(mysql_num_rows($sqls_com_adc)>0)
					{
						while($r_aow_cc_ad = mysql_fetch_assoc($sqls_com_adc))
						{
							if($r_aow_cc_ad['community_view_selected']!="")
							{
								//$artist_view_exp = explode(',',$r_aow_cc_ad['community_view_selected']);
								$artist_view_excp = $r_aow_cc_ad['community_view_selected'].','.$find_from['community_id'];
								$up_eve_art = $friend_noej->update_community_c($artist_view_excp,$r_aow_cc_ad['community_id']);
							}
							else
							{
								$artist_view_excp = ','.$find_from['community_id'];
								$up_eve_art = $friend_noej->update_community_c($artist_view_excp,$r_aow_cc_ad['community_id']);
							}
						}
					}
					
					$sqls_com_event_ad = $friend_noej->get_communityeve($find_to['community_id']);
					if(mysql_num_rows($sqls_com_event_ad)>0)
					{
						while($rcde_aow_e_ad = mysql_fetch_assoc($sqls_com_event_ad))
						{
							if($rcde_aow_e_ad['community_view_selected']!="")
							{
								//$artist_view_exp = explode(',',$rcde_aow_e_ad['community_view_selected']);
								$artist_view_exp = $rcde_aow_e_ad['community_view_selected'].','.$find_from['community_id'];
								$up_eve_art = $friend_noej->update_communityeve_c($artist_view_exp,$rcde_aow_e_ad['id']);
							}
							else
							{
								$artist_view_exp = ','.$find_from['community_id'];
								$up_eve_art = $friend_noej->update_communityeve_c($artist_view_exp,$rcde_aow_e_ad['id']);
							}
						}
					}
					
					$sqsl_art_project_ad = $friend_noej->get_communitypro($find_to['community_id']);
					if(mysql_num_rows($sqsl_art_project_ad)>0)
					{
						while($rade_aow_p_ad = mysql_fetch_assoc($sqsl_art_project_ad))
						{
							if($rade_aow_p_ad['community_view_selected']!="")
							{
								//$artist_view_exp = explode(',',$rade_aow_p_ad['community_view_selected']);
								$artist_view_exp = $rade_aow_p_ad['community_view_selected'].','.$find_from['community_id'];
								$up_eve_art = $friend_noej->update_communitypro_c($artist_view_exp,$rade_aow_p_ad['id']);
							}
							else
							{
								$artist_view_exp = ','.$find_from['community_id'];
								$up_eve_art = $friend_noej->update_communitypro_c($artist_view_exp,$rade_aow_p_ad['id']);
							}
						}
					}
				}
			}
			$header = "";
			$header .= "MIME-Version: 1.0\r\n";
			$header .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
			//$to = $find_to['email'];
			/* $mail->AddAddress($find_to['email']);
			//$subject = 'Friend Request Confirmed by '.$find_from['fname'].' '.$find_from['lname'];
			$mail->Subject = 'Friend Request Confirmed by '.$find_from['fname'].' '.$find_from['lname'];
			$mail_body = "<html><body>".$find_from['fname'].' '.$find_from['lname']." has accepted your friend request. You may now tag them in your media and profiles and they will receive updates you send. To send updates use your <a href='profileedit_artist.php#promotions'>promotions page</a> or add a <a href='profileedit.php'>news post</a>.</body></html>";
			$header .= "from: Purify Art < no-reply@purifyart.com>"."\r\n"; */
			//$sentmail = mail($to,$subject,$mail_body,$header);
			
			$mail_body = "<html><body>".$find_from['fname'].' '.$find_from['lname']." has accepted your friend request. You may now tag them in your media and profiles and they will receive updates you send. To send updates use your <a href='http://'.$domainname.'/profileedit_artist.php#promotions'>promotions page</a> or add a <a href='http://'.$domainname.'/profileedit.php'>news post</a>.</body></html>";
			
			$url = "admin/newsletter/"; 
			$myFile = $url . "other_templates.txt";
			$fh = fopen($myFile, 'r');
			$theData = fread($fh, filesize($myFile));
			fclose($fh);
			
			$data_email = explode('|~|', $theData);
			
			$ns_user_n = str_replace('{::message::}', $mail_body, $data_email['1']);
			
			$body_email = $data_email['0'] . $ns_user_n . $data_email['2'];
			
			$mail_grid = new SendGrid\Mail();
			
			$mail_grid->addTo($find_to['email'])->
					setFromName('Purify Art')->
					setFrom('no-reply@purifyart.com')->
					setSubject('Friend Request Confirmed by '.$find_from['fname'].' '.$find_from['lname'])->
					setHtml($body_email);
					
			//$sendgrid->web->send($mail_grid);
			
			$sendgrid -> smtp -> send($mail_grid); 
			
			//$mail->Body    = $mail_body;
			
			
			
			//if($mail->Send()){
			//	$sentmail = true;
			//}
		}
		
		if($exp_acc_dec[0]=='adec')
		{
			$sql_accept_up = $friend_noej->decline_friend_a($_SESSION['login_id'],$exp_acc_dec[1]);
			echo "Declined";
		}
		
		if($exp_acc_dec[0]=='cacc')
		{
			$sql_accept_up = $friend_noej->accept_friend_c($_SESSION['login_id'],$exp_acc_dec[1]);
			echo "Accepted";
			
			$find_to = $friend_noej->general_user_sec($exp_acc_dec[1]);
			$find_from = $friend_noej->general_user_sec($_SESSION['login_id']);
			$header = "";
			$header .= "MIME-Version: 1.0\r\n";
			$header .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
			//$to = $find_to['email'];
			/* $mail->AddAddress($find_to['email']);
			//$subject = 'Friend Request Confirmed by '.$find_from['fname'].' '.$find_from['lname'];
			$mail->Subject = 'Friend Request Confirmed by '.$find_from['fname'].' '.$find_from['lname'];
			//$mail_body = $find_from['fname'].' '.$find_from['lname']." has confirmed your friend request."."\r\n"."\r\n"."To view your updated profile please Log in to www.purifyart.com";
			$mail_body = "<html><body>".$find_from['fname'].' '.$find_from['lname']." has accepted your friend request. You may now tag them in your media and profiles and they will receive updates you send. To send updates use your <a href='profileedit_artist.php#promotions'>promotions page</a> or add a <a href='profileedit.php'>news post</a>.</body></html>";
			$header .= "from: Purify Art < no-reply@purifyart.com>"."\r\n";
			//$sentmail = mail($to,$subject,$mail_body,$header);
			$mail->Body    = $mail_body; */
			
			$mail_body = "<html><body>".$find_from['fname'].' '.$find_from['lname']." has accepted your friend request. You may now tag them in your media and profiles and they will receive updates you send. To send updates use your <a href='http://'.$domainname.'/profileedit_artist.php#promotions'>promotions page</a> or add a <a href='http://'.$domainname.'/profileedit.php'>news post</a>.</body></html>";
			
			$url = "admin/newsletter/"; 
			$myFile = $url . "other_templates.txt";
			$fh = fopen($myFile, 'r');
			$theData = fread($fh, filesize($myFile));
			fclose($fh);
			
			$data_email = explode('|~|', $theData);
			
			$ns_user_n = str_replace('{::message::}', $mail_body, $data_email['1']);
			
			$body_email = $data_email['0'] . $ns_user_n . $data_email['2'];
			
			$mail_grid = new SendGrid\Mail();
			
			$mail_grid->addTo($find_to['email'])->
					setFromName('Purify Art')->
					setFrom('no-reply@purifyart.com')->
					setSubject('Friend Request Confirmed by '.$find_from['fname'].' '.$find_from['lname'])->
					setHtml($body_email);
					
			//$sendgrid->web->send($mail_grid);
			
			$sendgrid -> smtp -> send($mail_grid); 
			
			/* if($mail->Send()){
				$sentmail = true;
			} */
		}
		
		if($exp_acc_dec[0]=='cdec')
		{
			$sql_accept_up = $friend_noej->decline_friend_c($_SESSION['login_id'],$exp_acc_dec[1]);
			echo "Declined";
		}
	}
	
	if(isset($_POST['hide_dis']))
	{
		$post_exp = explode('_',$_POST['hide_dis']);
		if($post_exp[0]=='a' || $post_exp[0]=='1a')
		{
			if($post_exp[4]!=0 || $post_exp[5]!=0)
			{
				if($post_exp[1]=='display')
				{
					if($post_exp[4]!=0)
					{
						$sql_art_a_ad = $friend_noej->friend_art($post_exp[4]);
						if(mysql_num_rows($sql_art_a_ad)>0)
						{
							while($r_aow_a_ad = mysql_fetch_assoc($sql_art_a_ad))
							{
								if($r_aow_a_ad['artist_view_selected']!="")
								{
									//$artist_view_exp = explode(',',$r_aow_a_ad['artist_view_selected']);
									$artist_view_exp = $r_aow_a_ad['artist_view_selected'].','.$post_exp[3];
									$up_eve_art = $friend_noej->update_artist($artist_view_exp,$r_aow_a_ad['artist_id']);
								}
								else
								{
									$artist_view_exp = ','.$post_exp[3];
									$up_eve_art = $friend_noej->update_artist($artist_view_exp,$r_aow_a_ad['artist_id']);
								}
							}
						}
						
						$sql_art_event_ad = $friend_noej->get_artisteve($post_exp[4]);
						if(mysql_num_rows($sql_art_event_ad)>0)
						{
							while($r_aow_e_ad = mysql_fetch_assoc($sql_art_event_ad))
							{
								if($r_aow_e_ad['artist_view_selected']!="")
								{
									//$artist_view_exp = explode(',',$r_aow_e_ad['artist_view_selected']);
									$artist_view_exp = $r_aow_e_ad['artist_view_selected'].','.$post_exp[3];
									$up_eve_art = $friend_noej->update_artisteve($artist_view_exp,$r_aow_e_ad['id']);
								}
								else
								{
									$artist_view_exp = ','.$post_exp[3];
									$up_eve_art = $friend_noej->update_artisteve($artist_view_exp,$r_aow_e_ad['id']);
								}
							}
						}
						
						$sql_art_project_ad = $friend_noej->get_artistpro($post_exp[4]);
						if(mysql_num_rows($sql_art_project_ad)>0)
						{
							while($r_aow_p_ad = mysql_fetch_assoc($sql_art_project_ad))
							{
								if($r_aow_p_ad['artist_view_selected']!="")
								{
									//$artist_view_exp = explode(',',$r_aow_p_ad['artist_view_selected']);
									$artist_view_exp = $r_aow_p_ad['artist_view_selected'].','.$post_exp[3];
									$up_eve_art = $friend_noej->update_artistpro($artist_view_exp,$r_aow_p_ad['id']);
								}
								else
								{
									$artist_view_exp = ','.$post_exp[3];
									$up_eve_art = $friend_noej->update_artistpro($artist_view_exp,$r_aow_p_ad['id']);
								}
							}
						}
					}
					
					if($post_exp[5]!=0)
					{
						$sql_com_ad = $friend_noej->friend_com($post_exp[5]);
						if(mysql_num_rows($sql_com_ad)>0)
						{
							while($r_aow_c_ad = mysql_fetch_assoc($sql_com_ad))
							{
								if($r_aow_c_ad['artist_view_selected']!="")
								{
									//$artist_view_exp = explode(',',$r_aow_c_ad['artist_view_selected']);
									$artist_view_exp = $r_aow_c_ad['artist_view_selected'].','.$post_exp[3];
									$up_eve_art = $friend_noej->update_community($artist_view_exp,$r_aow_c_ad['community_id']);
								}
								else
								{
									$artist_view_exp = ','.$post_exp[3];
									$up_eve_art = $friend_noej->update_community($artist_view_exp,$r_aow_c_ad['community_id']);
								}
							}
						}
						
						$sql_com_event_ad = $friend_noej->get_communityeve($post_exp[5]);
						if(mysql_num_rows($sql_com_event_ad)>0)
						{
							while($r_aow_e_ad = mysql_fetch_assoc($sql_com_event_ad))
							{
								if($r_aow_e_ad['artist_view_selected']!="")
								{
									//$artist_view_exp = explode(',',$r_aow_e_ad['artist_view_selected']);
									$artist_view_exp = $r_aow_e_ad['artist_view_selected'].','.$post_exp[3];
									$up_eve_art = $friend_noej->update_communityeve($artist_view_exp,$r_aow_e_ad['id']);
								}
								else
								{
									$artist_view_exp = ','.$post_exp[3];
									$up_eve_art = $friend_noej->update_communityeve($artist_view_exp,$r_aow_e_ad['id']);
								}
							}
						}
						
						$sql_art_project_ad = $friend_noej->get_communitypro($post_exp[5]);
						if(mysql_num_rows($sql_art_project_ad)>0)
						{
							while($r_aow_p_ad = mysql_fetch_assoc($sql_art_project_ad))
							{
								if($r_aow_p_ad['artist_view_selected']!="")
								{
									//$artist_view_exp = explode(',',$r_aow_p_ad['artist_view_selected']);
									$artist_view_exp = $r_aow_p_ad['artist_view_selected'].','.$post_exp[3];
									$up_eve_art = $friend_noej->update_communitypro($artist_view_exp,$r_aow_p_ad['id']);
								}
								else
								{
									$artist_view_exp = ','.$post_exp[3];
									$up_eve_art = $friend_noej->update_communitypro($artist_view_exp,$r_aow_p_ad['id']);
								}
							}
						}
					}
				}
				elseif($post_exp[1]=='hide')
				{
					if($post_exp[4]!=0)
					{
						$sql_art_a_ah = $friend_noej->friend_art($post_exp[4]);
						if(mysql_num_rows($sql_art_a_ah)>0)
						{
							while($r_aow_a_ah = mysql_fetch_assoc($sql_art_a_ah))
							{
								$artist_view_exp_n_ah = array();
								if($r_aow_a_ah['artist_view_selected']!="")
								{
									$artist_view_exp_ah = explode(',',$r_aow_a_ah['artist_view_selected']);
									for($i_im_ah=0;$i_im_ah<count($artist_view_exp_ah);$i_im_ah++)
									{
										if($artist_view_exp_ah[$i_im_ah]!=$post_exp[3])
										{
											$artist_view_exp_n_ah[] = $artist_view_exp_ah[$i_im_ah];
										}
									}
									$artist_view_exp_i_ah = implode(',',$artist_view_exp_n_ah);
									$up_art_ah = $friend_noej->update_artist($artist_view_exp_i_ah,$r_aow_a_ah['artist_id']);
								}
							}
						}
						
						$sql_art_event_ah = $friend_noej->get_artisteve($post_exp[4]);
						if(mysql_num_rows($sql_art_event_ah)>0)
						{
							while($r_aow_e_ah = mysql_fetch_assoc($sql_art_event_ah))
							{
								$artist_view_exp_e_ah = array();
								if($r_aow_e_ah['artist_view_selected']!="")
								{
									$artist_view_exp = explode(',',$r_aow_e_ah['artist_view_selected']);
									for($i_im_eah=0;$i_im_eah<count($artist_view_exp);$i_im_eah++)
									{
										if($artist_view_exp[$i_im_eah]!=$post_exp[3])
										{
											$artist_view_exp_e_ah[] = $artist_view_exp[$i_im_eah];
										}
									}
									$artist_view_exp_ie = implode(',',$artist_view_exp_e_ah);
									$up_eve_art = $friend_noej->update_artisteve($artist_view_exp_ie,$r_aow_e_ah['id']);
								}
							}
						}
						
						$sql_art_project = $friend_noej->get_artistpro($post_exp[4]);
						if(mysql_num_rows($sql_art_project)>0)
						{
							while($r_aow_p = mysql_fetch_assoc($sql_art_project))
							{
								$artist_view_exp_p = array();
								if($r_aow_p['artist_view_selected']!="")
								{
									$artist_view_exp = explode(',',$r_aow_p['artist_view_selected']);
									for($i_im_pah=0;$i_im_pah<count($artist_view_exp);$i_im_pah++)
									{
										if($artist_view_exp[$i_im_pah]!=$post_exp[3])
										{
											$artist_view_exp_p[] = $artist_view_exp[$i_im_pah];
										}
									}
									$artist_view_exp_ip = implode(',',$artist_view_exp_p);
									$up_eve_art = $friend_noej->update_artistpro($artist_view_exp_ip,$r_aow_p['id']);
								}
							}
						}
					}
					
					if($post_exp[5]!=0)
					{
						$sql_com_ah = $friend_noej->friend_com($post_exp[5]);
						if(mysql_num_rows($sql_com_ah)>0)
						{
							while($r_aow_c_ah = mysql_fetch_assoc($sql_com_ah))
							{
								$artist_view_exp_n = array();
								if($r_aow_c_ah['artist_view_selected']!="")
								{
									$artist_view_exp = explode(',',$r_aow_c_ah['artist_view_selected']);
									for($i_im_c_ah=0;$i_im_c_ah<count($artist_view_exp);$i_im_c_ah++)
									{
										if($artist_view_exp[$i_im_c_ah]!=$post_exp[3])
										{
											$artist_view_exp_n[] = $artist_view_exp[$i_im_c_ah];
										}
									}
									$artist_view_exp_i = implode(',',$artist_view_exp_n);
									$up_eve_art = $friend_noej->update_community($artist_view_exp_i,$r_aow_c_ah['community_id']);
								}
							}
						}
						
						$sql_com_event_ah = $friend_noej->get_communityeve($post_exp[5]);
						if(mysql_num_rows($sql_com_event_ah)>0)
						{
							while($r_aow_e_ah = mysql_fetch_assoc($sql_com_event_ah))
							{
								$artist_view_exp_e = array();
								if($r_aow_e_ah['artist_view_selected']!="")
								{
									$artist_view_exp = explode(',',$r_aow_e_ah['artist_view_selected']);
									for($i_im_e_ah=0;$i_im_e_ah<count($artist_view_exp);$i_im_e_ah++)
									{
										if($artist_view_exp[$i_im_e_ah]!=$post_exp[3])
										{
											$artist_view_exp_e[] = $artist_view_exp[$i_im_e_ah];
										}
									}
									$artist_view_exp_ie = implode(',',$artist_view_exp_e);
									$up_eve_art = $friend_noej->update_communityeve($artist_view_exp_ie,$r_aow_e_ah['id']);
								}
							}
						}
						
						$sql_com_project_ah = $friend_noej->get_communitypro($post_exp[5]);
						if(mysql_num_rows($sql_com_project_ah)>0)
						{
							while($r_aow_p_ah = mysql_fetch_assoc($sql_com_project_ah))
							{
								$artist_view_exp_p = array();
								if($r_aow_p_ah['artist_view_selected']!="")
								{
									$artist_view_exp = explode(',',$r_aow_p_ah['artist_view_selected']);
									for($i_im_p_ah=0;$i_im_p_ah<count($artist_view_exp);$i_im_p_ah++)
									{
										if($artist_view_exp[$i_im_p_ah]!=$post_exp[3])
										{
											$artist_view_exp_p[] = $artist_view_exp[$i_im_p_ah];
										}
									}
									$artist_view_exp_ip = implode(',',$artist_view_exp_p);
									$up_eve_art = $friend_noej->update_communitypro($artist_view_exp_ip,$r_aow_p_ah['id']);
								}
							}
						}
					}
				}
				elseif($post_exp[1]=='delete')
				{
					$art_remove_id = $post_exp[3];
					$sql_check_com = $friend_noej->general_user_sec_art($art_remove_id);
					if($sql_check_com['community_id']!=0)
					{
						$com_remove_id = $sql_check_com['community_id'];
					}
					
					if($post_exp[4]!=0)
					{
						$sql_art_a_ah = $friend_noej->friend_art($post_exp[4]);
						if(mysql_num_rows($sql_art_a_ah)>0)
						{
							while($r_aow_a_ah = mysql_fetch_assoc($sql_art_a_ah))
							{
								$artist_view_exp_n_ah = array();
								if($r_aow_a_ah['artist_view_selected']!="")
								{
									$artist_view_exp_ah = explode(',',$r_aow_a_ah['artist_view_selected']);
									for($i_im_ah=0;$i_im_ah<count($artist_view_exp_ah);$i_im_ah++)
									{
										if($artist_view_exp_ah[$i_im_ah]!=$art_remove_id)
										{
											$artist_view_exp_n_ah[] = $artist_view_exp_ah[$i_im_ah];
										}
									}
									$artist_view_exp_i_ah = implode(',',$artist_view_exp_n_ah);
									$up_art_ah = $friend_noej->update_artist($artist_view_exp_i_ah,$r_aow_a_ah['artist_id']);
								}
							}
						}
						
						$sql_art_event_ah = $friend_noej->get_artisteve($post_exp[4]);
						if(mysql_num_rows($sql_art_event_ah)>0)
						{
							while($r_aow_e_ah = mysql_fetch_assoc($sql_art_event_ah))
							{
								$artist_view_exp_e_ah = array();
								if($r_aow_e_ah['artist_view_selected']!="")
								{
									$artist_view_exp = explode(',',$r_aow_e_ah['artist_view_selected']);
									for($i_im_eah=0;$i_im_eah<count($artist_view_exp);$i_im_eah++)
									{
										if($artist_view_exp[$i_im_eah]!=$art_remove_id)
										{
											$artist_view_exp_e_ah[] = $artist_view_exp[$i_im_eah];
										}
									}
									$artist_view_exp_ie = implode(',',$artist_view_exp_e_ah);
									$up_eve_art = $friend_noej->update_artisteve($artist_view_exp_ie,$r_aow_e_ah['id']);
								}
							}
						}
						
						$sql_art_project = $friend_noej->get_artistpro($post_exp[4]);
						if(mysql_num_rows($sql_art_project)>0)
						{
							while($r_aow_p = mysql_fetch_assoc($sql_art_project))
							{
								$artist_view_exp_p = array();
								if($r_aow_p['artist_view_selected']!="")
								{
									$artist_view_exp = explode(',',$r_aow_p['artist_view_selected']);
									for($i_im_pah=0;$i_im_pah<count($artist_view_exp);$i_im_pah++)
									{
										if($artist_view_exp[$i_im_pah]!=$art_remove_id)
										{
											$artist_view_exp_p[] = $artist_view_exp[$i_im_pah];
										}
									}
									$artist_view_exp_ip = implode(',',$artist_view_exp_p);
									$up_eve_art = $friend_noej->update_artistpro($artist_view_exp_ip,$r_aow_p['id']);
								}
							}
						}
						
						if(isset($com_remove_id) && $com_remove_id!=0)
						{
							$sql_art = $friend_noej->friend_art($post_exp[4]);
							if(mysql_num_rows($sql_art)>0)
							{
								while($r_aow_a = mysql_fetch_assoc($sql_art))
								{
									$artist_view_exp_n = array();
									if($r_aow_a['community_view_selected']!="")
									{
										$artist_view_exp = explode(',',$r_aow_a['community_view_selected']);
										for($i_im=0;$i_im<count($artist_view_exp);$i_im++)
										{
											if($artist_view_exp[$i_im]!=$com_remove_id)
											{
												$artist_view_exp_n[] = $artist_view_exp[$i_im];
											}
										}
										$artist_view_exp_i = implode(',',$artist_view_exp_n);
										$up_eve_art = $friend_noej->update_artist_c($artist_view_exp_i,$r_aow_a['artist_id']);
									}
								}
							}
							
							$sql_art_event = $friend_noej->get_artisteve($post_exp[4]);
							if(mysql_num_rows($sql_art_event)>0)
							{
								while($r_aow_e = mysql_fetch_assoc($sql_art_event))
								{
									$artist_view_exp_e = array();
									if($r_aow_e['community_view_selected']!="")
									{
										$artist_view_exp = explode(',',$r_aow_e['community_view_selected']);
										for($i_im=0;$i_im<count($artist_view_exp);$i_im++)
										{
											if($artist_view_exp[$i_im]!=$com_remove_id)
											{
												$artist_view_exp_e[] = $artist_view_exp[$i_im];
											}
										}
										$artist_view_exp_ie = implode(',',$artist_view_exp_e);
										$up_eve_art = $friend_noej->update_artisteve_c($artist_view_exp_ie,$r_aow_e['id']);
									}
								}
							}
							
							$sql_art_project = $friend_noej->get_artistpro($post_exp[4]);
							if(mysql_num_rows($sql_art_project)>0)
							{
								while($r_aow_p = mysql_fetch_assoc($sql_art_project))
								{
									$artist_view_exp_p = array();
									if($r_aow_p['community_view_selected']!="")
									{
										$artist_view_exp = explode(',',$r_aow_p['community_view_selected']);
										for($i_im=0;$i_im<count($artist_view_exp);$i_im++)
										{
											if($artist_view_exp[$i_im]!=$com_remove_id)
											{
												$artist_view_exp_p[] = $artist_view_exp[$i_im];
											}
										}
										$artist_view_exp_ip = implode(',',$artist_view_exp_p);
										$up_eve_art = $friend_noej->update_artistpro_c($artist_view_exp_ip,$r_aow_p['id']);
									}
								}
							}
						}
					}
					
					if($post_exp[5]!=0)
					{
						$sql_com_ah = $friend_noej->friend_com($post_exp[5]);
						if(mysql_num_rows($sql_com_ah)>0)
						{
							while($r_aow_c_ah = mysql_fetch_assoc($sql_com_ah))
							{
								$artist_view_exp_n = array();
								if($r_aow_c_ah['artist_view_selected']!="")
								{
									$artist_view_exp = explode(',',$r_aow_c_ah['artist_view_selected']);
									for($i_im_c_ah=0;$i_im_c_ah<count($artist_view_exp);$i_im_c_ah++)
									{
										if($artist_view_exp[$i_im_c_ah]!=$art_remove_id)
										{
											$artist_view_exp_n[] = $artist_view_exp[$i_im_c_ah];
										}
									}
									$artist_view_exp_i = implode(',',$artist_view_exp_n);
									$up_eve_art = $friend_noej->update_community($artist_view_exp_i,$r_aow_c_ah['community_id']);
								}
							}
						}
						
						if(isset($com_remove_id) && $com_remove_id!=0)
						{
							$sql_art = $friend_noej->friend_com($post_exp[5]);
							if(mysql_num_rows($sql_art)>0)
							{
								while($r_aow_a = mysql_fetch_assoc($sql_art))
								{
									$artist_view_exp_n = array();
									if($r_aow_a['community_view_selected']!="")
									{
										$artist_view_exp = explode(',',$r_aow_a['community_view_selected']);
										for($i_im=0;$i_im<count($artist_view_exp);$i_im++)
										{
											if($artist_view_exp[$i_im]!=$com_remove_id)
											{
												$artist_view_exp_n[] = $artist_view_exp[$i_im];
											}
										}
										$artist_view_exp_i = implode(',',$artist_view_exp_n);
										$up_eve_art = $friend_noej->update_community_c($artist_view_exp_i,$r_aow_a['community_id']);
									}
								}
							}
							
							$sql_art_event = $friend_noej->get_communityeve($post_exp[5]);
							if(mysql_num_rows($sql_art_event)>0)
							{
								while($r_aow_e = mysql_fetch_assoc($sql_art_event))
								{
									$artist_view_exp_e = array();
									if($r_aow_e['community_view_selected']!="")
									{
										$artist_view_exp = explode(',',$r_aow_e['community_view_selected']);
										for($i_im=0;$i_im<count($artist_view_exp);$i_im++)
										{
											if($artist_view_exp[$i_im]!=$com_remove_id)
											{
												$artist_view_exp_e[] = $artist_view_exp[$i_im];
											}
										}
										$artist_view_exp_ie = implode(',',$artist_view_exp_e);
										$up_eve_art = $friend_noej->update_communityeve_c($artist_view_exp_ie,$r_aow_e['id']);
									}
								}
							}
							
							$sql_art_project = $friend_noej->get_communitypro($post_exp[5]);
							if(mysql_num_rows($sql_art_project)>0)
							{
								while($r_aow_p = mysql_fetch_assoc($sql_art_project))
								{
									$artist_view_exp_p = array();
									if($r_aow_p['community_view_selected']!="")
									{
										$artist_view_exp = explode(',',$r_aow_p['community_view_selected']);
										for($i_im=0;$i_im<count($artist_view_exp);$i_im++)
										{
											if($artist_view_exp[$i_im]!=$com_remove_id)
											{
												$artist_view_exp_p[] = $artist_view_exp[$i_im];
											}
										}
										$artist_view_exp_ip = implode(',',$artist_view_exp_p);
										$up_eve_art = $friend_noej->update_communitypro_c($artist_view_exp_ip,$r_aow_p['id']);
									}
								}
							}
						}
						
						$sql_com_event_ah = $friend_noej->get_communityeve($post_exp[5]);
						if(mysql_num_rows($sql_com_event_ah)>0)
						{
							while($r_aow_e_ah = mysql_fetch_assoc($sql_com_event_ah))
							{
								$artist_view_exp_e = array();
								if($r_aow_e_ah['artist_view_selected']!="")
								{
									$artist_view_exp = explode(',',$r_aow_e_ah['artist_view_selected']);
									for($i_im_e_ah=0;$i_im_e_ah<count($artist_view_exp);$i_im_e_ah++)
									{
										if($artist_view_exp[$i_im_e_ah]!=$art_remove_id)
										{
											$artist_view_exp_e[] = $artist_view_exp[$i_im_e_ah];
										}
									}
									$artist_view_exp_ie = implode(',',$artist_view_exp_e);
									$up_eve_art = $friend_noej->update_communityeve($artist_view_exp_ie,$r_aow_e_ah['id']);
								}
							}
						}
						
						$sql_com_project_ah = $friend_noej->get_communitypro($post_exp[5]);
						if(mysql_num_rows($sql_com_project_ah)>0)
						{
							while($r_aow_p_ah = mysql_fetch_assoc($sql_com_project_ah))
							{
								$artist_view_exp_p = array();
								if($r_aow_p_ah['artist_view_selected']!="")
								{
									$artist_view_exp = explode(',',$r_aow_p_ah['artist_view_selected']);
									for($i_im_p_ah=0;$i_im_p_ah<count($artist_view_exp);$i_im_p_ah++)
									{
										if($artist_view_exp[$i_im_p_ah]!=$art_remove_id)
										{
											$artist_view_exp_p[] = $artist_view_exp[$i_im_p_ah];
										}
									}
									$artist_view_exp_ip = implode(',',$artist_view_exp_p);
									$up_eve_art = $friend_noej->update_communitypro($artist_view_exp_ip,$r_aow_p_ah['id']);
								}
							}
						}
					}
					$delete_tag_data = $friend_noej->delete_tag($sql_check_com['general_user_id'],$_SESSION['login_id']);
					$sql_sub = $feeds_unsub->subscription($_SESSION['login_email'],$sql_check_com['general_user_id']);
					$sql_cnt = $feeds_unsub->remove_contact($_SESSION['login_id'],$sql_check_com['general_user_id']);
					$delete_sql_gen = $friend_noej->delete_friend($sql_check_com['general_user_id'],$_SESSION['login_id']);
				}
			}
		}
		if($post_exp[0]=='c' || $post_exp[0]=='1c')
		{
			if($post_exp[4]!=0 || $post_exp[5]!=0)
			{
				if($post_exp[1]=='display')
				{
					if($post_exp[4]!=0)
					{
						$sql_art = $friend_noej->friend_art($post_exp[4]);
						if(mysql_num_rows($sql_art)>0)
						{
							while($r_aow_a = mysql_fetch_assoc($sql_art))
							{
								if($r_aow_a['community_view_selected']!="")
								{
									//$artist_view_exp = explode(',',$r_aow_a['community_view_selected']);
									$artist_view_exp = $r_aow_a['community_view_selected'].','.$post_exp[3];
									$up_eve_art = $friend_noej->update_artist_c($artist_view_exp,$r_aow_a['artist_id']);
								}
								else
								{
									$artist_view_exp = ','.$post_exp[3];
									$up_eve_art = $friend_noej->update_artist_c($artist_view_exp,$r_aow_a['artist_id']);
								}
							}
						}
						
						$sql_art_event = $friend_noej->get_artisteve($post_exp[4]);
						if(mysql_num_rows($sql_art_event)>0)
						{
							while($r_aow_e = mysql_fetch_assoc($sql_art_event))
							{
								if($r_aow_e['community_view_selected']!="")
								{
									//$artist_view_exp = explode(',',$r_aow_e['community_view_selected']);
									$artist_view_exp = $r_aow_e['community_view_selected'].','.$post_exp[3];
									$up_eve_art = $friend_noej->update_artisteve_c($artist_view_exp,$r_aow_e['id']);
								}
								else
								{
									$artist_view_exp = ','.$post_exp[3];
									$up_eve_art = $friend_noej->update_artisteve_c($artist_view_exp,$r_aow_e['id']);
								}
							}
						}
						
						$sql_art_project = $friend_noej->get_artistpro($post_exp[4]);
						if(mysql_num_rows($sql_art_project)>0)
						{
							while($r_aow_p = mysql_fetch_assoc($sql_art_project))
							{
								if($r_aow_p['community_view_selected']!="")
								{
									//$artist_view_exp = explode(',',$r_aow_p['community_view_selected']);
									$artist_view_exp = $r_aow_p['community_view_selected'].','.$post_exp[3];
									$up_eve_art = $friend_noej->update_artistpro_c($artist_view_exp,$r_aow_p['id']);
								}
								else
								{
									$artist_view_exp = ','.$post_exp[3];
									$up_eve_art = $friend_noej->update_artistpro_c($artist_view_exp,$r_aow_p['id']);
								}
							}
						}
					}
					
					if($post_exp[5]!=0)
					{
						$sql_art = $friend_noej->friend_com($post_exp[5]);
						if(mysql_num_rows($sql_art)>0)
						{
							while($r_aow_a = mysql_fetch_assoc($sql_art))
							{
								if($r_aow_a['community_view_selected']!="")
								{
									//$artist_view_exp = explode(',',$r_aow_a['community_view_selected']);
									$artist_view_exp = $r_aow_a['community_view_selected'].','.$post_exp[3];
									$up_eve_art = $friend_noej->update_community_c($artist_view_exp,$r_aow_a['community_id']);
								}
								else
								{
									$artist_view_exp = ','.$post_exp[3];
									$up_eve_art = $friend_noej->update_community_c($artist_view_exp,$r_aow_a['community_id']);
								}
							}
						}
						
						$sql_art_event = $friend_noej->get_communityeve($post_exp[5]);
						if(mysql_num_rows($sql_art_event)>0)
						{
							while($r_aow_e = mysql_fetch_assoc($sql_art_event))
							{
								if($r_aow_e['community_view_selected']!="")
								{
									//$artist_view_exp = explode(',',$r_aow_e['community_view_selected']);
									$artist_view_exp = $r_aow_e['community_view_selected'].','.$post_exp[3];
									$up_eve_art = $friend_noej->update_communityeve_c($artist_view_exp,$r_aow_e['id']);
								}
								else
								{
									$artist_view_exp = ','.$post_exp[3];
									$up_eve_art = $friend_noej->update_communityeve_c($artist_view_exp,$r_aow_e['id']);
								}
							}
						}
						
						$sql_art_project = $friend_noej->get_communitypro($post_exp[5]);
						if(mysql_num_rows($sql_art_project)>0)
						{
							while($r_aow_p = mysql_fetch_assoc($sql_art_project))
							{
								if($r_aow_p['community_view_selected']!="")
								{
									//$artist_view_exp = explode(',',$r_aow_p['community_view_selected']);
									$artist_view_exp = $r_aow_p['community_view_selected'].','.$post_exp[3];
									$up_eve_art = $friend_noej->update_communitypro_c($artist_view_exp,$r_aow_p['id']);
								}
								else
								{
									$artist_view_exp = ','.$post_exp[3];
									$up_eve_art = $friend_noej->update_communitypro_c($artist_view_exp,$r_aow_p['id']);
								}
							}
						}
					}
				}
				elseif($post_exp[1]=='hide')
				{
					if($post_exp[4]!=0)
					{
						$sql_art = $friend_noej->friend_art($post_exp[4]);
						if(mysql_num_rows($sql_art)>0)
						{
							while($r_aow_a = mysql_fetch_assoc($sql_art))
							{
								$artist_view_exp_n = array();
								if($r_aow_a['community_view_selected']!="")
								{
									$artist_view_exp = explode(',',$r_aow_a['community_view_selected']);
									for($i_im=0;$i_im<count($artist_view_exp);$i_im++)
									{
										if($artist_view_exp[$i_im]!=$post_exp[3])
										{
											$artist_view_exp_n[] = $artist_view_exp[$i_im];
										}
									}
									$artist_view_exp_i = implode(',',$artist_view_exp_n);
									$up_eve_art = $friend_noej->update_artist_c($artist_view_exp_i,$r_aow_a['artist_id']);
								}
							}
						}
						
						$sql_art_event = $friend_noej->get_artisteve($post_exp[4]);
						if(mysql_num_rows($sql_art_event)>0)
						{
							while($r_aow_e = mysql_fetch_assoc($sql_art_event))
							{
								$artist_view_exp_e = array();
								if($r_aow_e['community_view_selected']!="")
								{
									$artist_view_exp = explode(',',$r_aow_e['community_view_selected']);
									for($i_im=0;$i_im<count($artist_view_exp);$i_im++)
									{
										if($artist_view_exp[$i_im]!=$post_exp[3])
										{
											$artist_view_exp_e[] = $artist_view_exp[$i_im];
										}
									}
									$artist_view_exp_ie = implode(',',$artist_view_exp_e);
									$up_eve_art = $friend_noej->update_artisteve_c($artist_view_exp_ie,$r_aow_e['id']);
								}
							}
						}
						
						$sql_art_project = $friend_noej->get_artistpro($post_exp[4]);
						if(mysql_num_rows($sql_art_project)>0)
						{
							while($r_aow_p = mysql_fetch_assoc($sql_art_project))
							{
								$artist_view_exp_p = array();
								if($r_aow_p['community_view_selected']!="")
								{
									$artist_view_exp = explode(',',$r_aow_p['community_view_selected']);
									for($i_im=0;$i_im<count($artist_view_exp);$i_im++)
									{
										if($artist_view_exp[$i_im]!=$post_exp[3])
										{
											$artist_view_exp_p[] = $artist_view_exp[$i_im];
										}
									}
									$artist_view_exp_ip = implode(',',$artist_view_exp_p);
									$up_eve_art = $friend_noej->update_artistpro_c($artist_view_exp_ip,$r_aow_p['id']);
								}
							}
						}
					}
					
					if($post_exp[5]!=0)
					{
						$sql_art = $friend_noej->friend_com($post_exp[5]);
						if(mysql_num_rows($sql_art)>0)
						{
							while($r_aow_a = mysql_fetch_assoc($sql_art))
							{
								$artist_view_exp_n = array();
								if($r_aow_a['community_view_selected']!="")
								{
									$artist_view_exp = explode(',',$r_aow_a['community_view_selected']);
									for($i_im=0;$i_im<count($artist_view_exp);$i_im++)
									{
										if($artist_view_exp[$i_im]!=$post_exp[3])
										{
											$artist_view_exp_n[] = $artist_view_exp[$i_im];
										}
									}
									$artist_view_exp_i = implode(',',$artist_view_exp_n);
									$up_eve_art = $friend_noej->update_community_c($artist_view_exp_i,$r_aow_a['community_id']);
								}
							}
						}
						
						$sql_art_event = $friend_noej->get_communityeve($post_exp[5]);
						if(mysql_num_rows($sql_art_event)>0)
						{
							while($r_aow_e = mysql_fetch_assoc($sql_art_event))
							{
								$artist_view_exp_e = array();
								if($r_aow_e['community_view_selected']!="")
								{
									$artist_view_exp = explode(',',$r_aow_e['community_view_selected']);
									for($i_im=0;$i_im<count($artist_view_exp);$i_im++)
									{
										if($artist_view_exp[$i_im]!=$post_exp[3])
										{
											$artist_view_exp_e[] = $artist_view_exp[$i_im];
										}
									}
									$artist_view_exp_ie = implode(',',$artist_view_exp_e);
									$up_eve_art = $friend_noej->update_communityeve_c($artist_view_exp_ie,$r_aow_e['id']);
								}
							}
						}
						
						$sql_art_project = $friend_noej->get_communitypro($post_exp[5]);
						if(mysql_num_rows($sql_art_project)>0)
						{
							while($r_aow_p = mysql_fetch_assoc($sql_art_project))
							{
								$artist_view_exp_p = array();
								if($r_aow_p['community_view_selected']!="")
								{
									$artist_view_exp = explode(',',$r_aow_p['community_view_selected']);
									for($i_im=0;$i_im<count($artist_view_exp);$i_im++)
									{
										if($artist_view_exp[$i_im]!=$post_exp[3])
										{
											$artist_view_exp_p[] = $artist_view_exp[$i_im];
										}
									}
									$artist_view_exp_ip = implode(',',$artist_view_exp_p);
									$up_eve_art = $friend_noej->update_communitypro_c($artist_view_exp_ip,$r_aow_p['id']);
								}
							}
						}
					}
				}
				elseif($post_exp[1]=='delete')
				{
					$com_remove_id = $post_exp[3];
					$sql_check_com = $friend_noej->general_user_sec_com($com_remove_id);
					if($sql_check_com['artist_id']!=0)
					{
						$art_remove_id = $sql_check_com['artist_id'];
					}
					
					if($post_exp[4]!=0)
					{
						if(isset($art_remove_id) && $art_remove_id!=0)
						{
							$sql_art_a_ah = $friend_noej->friend_art($post_exp[4]);
							if(mysql_num_rows($sql_art_a_ah)>0)
							{
								while($r_aow_a_ah = mysql_fetch_assoc($sql_art_a_ah))
								{
									$artist_view_exp_n_ah = array();
									if($r_aow_a_ah['artist_view_selected']!="")
									{
										$artist_view_exp_ah = explode(',',$r_aow_a_ah['artist_view_selected']);
										for($i_im_ah=0;$i_im_ah<count($artist_view_exp_ah);$i_im_ah++)
										{
											if($artist_view_exp_ah[$i_im_ah]!=$art_remove_id)
											{
												$artist_view_exp_n_ah[] = $artist_view_exp_ah[$i_im_ah];
											}
										}
										$artist_view_exp_i_ah = implode(',',$artist_view_exp_n_ah);
										$up_art_ah = $friend_noej->update_artist($artist_view_exp_i_ah,$r_aow_a_ah['artist_id']);
									}
								}
							}
							
							$sql_art_event_ah = $friend_noej->get_artisteve($post_exp[4]);
							if(mysql_num_rows($sql_art_event_ah)>0)
							{
								while($r_aow_e_ah = mysql_fetch_assoc($sql_art_event_ah))
								{
									$artist_view_exp_e_ah = array();
									if($r_aow_e_ah['artist_view_selected']!="")
									{
										$artist_view_exp = explode(',',$r_aow_e_ah['artist_view_selected']);
										for($i_im_eah=0;$i_im_eah<count($artist_view_exp);$i_im_eah++)
										{
											if($artist_view_exp[$i_im_eah]!=$art_remove_id)
											{
												$artist_view_exp_e_ah[] = $artist_view_exp[$i_im_eah];
											}
										}
										$artist_view_exp_ie = implode(',',$artist_view_exp_e_ah);
										$up_eve_art = $friend_noej->update_artisteve($artist_view_exp_ie,$r_aow_e_ah['id']);
									}
								}
							}
							
							$sql_art_project = $friend_noej->get_artistpro($post_exp[4]);
							if(mysql_num_rows($sql_art_project)>0)
							{
								while($r_aow_p = mysql_fetch_assoc($sql_art_project))
								{
									$artist_view_exp_p = array();
									if($r_aow_p['artist_view_selected']!="")
									{
										$artist_view_exp = explode(',',$r_aow_p['artist_view_selected']);
										for($i_im_pah=0;$i_im_pah<count($artist_view_exp);$i_im_pah++)
										{
											if($artist_view_exp[$i_im_pah]!=$art_remove_id)
											{
												$artist_view_exp_p[] = $artist_view_exp[$i_im_pah];
											}
										}
										$artist_view_exp_ip = implode(',',$artist_view_exp_p);
										$up_eve_art = $friend_noej->update_artistpro($artist_view_exp_ip,$r_aow_p['id']);
									}
								}
							}
						}
							
						$sql_art = $friend_noej->friend_art($post_exp[4]);
						if(mysql_num_rows($sql_art)>0)
						{
							while($r_aow_a = mysql_fetch_assoc($sql_art))
							{
								$artist_view_exp_n = array();
								if($r_aow_a['community_view_selected']!="")
								{
									$artist_view_exp = explode(',',$r_aow_a['community_view_selected']);
									for($i_im=0;$i_im<count($artist_view_exp);$i_im++)
									{
										if($artist_view_exp[$i_im]!=$com_remove_id)
										{
											$artist_view_exp_n[] = $artist_view_exp[$i_im];
										}
									}
									$artist_view_exp_i = implode(',',$artist_view_exp_n);
									$up_eve_art = $friend_noej->update_artist_c($artist_view_exp_i,$r_aow_a['artist_id']);
								}
							}
						}
						
						$sql_art_event = $friend_noej->get_artisteve($post_exp[4]);
						if(mysql_num_rows($sql_art_event)>0)
						{
							while($r_aow_e = mysql_fetch_assoc($sql_art_event))
							{
								$artist_view_exp_e = array();
								if($r_aow_e['community_view_selected']!="")
								{
									$artist_view_exp = explode(',',$r_aow_e['community_view_selected']);
									for($i_im=0;$i_im<count($artist_view_exp);$i_im++)
									{
										if($artist_view_exp[$i_im]!=$com_remove_id)
										{
											$artist_view_exp_e[] = $artist_view_exp[$i_im];
										}
									}
									$artist_view_exp_ie = implode(',',$artist_view_exp_e);
									$up_eve_art = $friend_noej->update_artisteve_c($artist_view_exp_ie,$r_aow_e['id']);
								}
							}
						}
						
						$sql_art_project = $friend_noej->get_artistpro($post_exp[4]);
						if(mysql_num_rows($sql_art_project)>0)
						{
							while($r_aow_p = mysql_fetch_assoc($sql_art_project))
							{
								$artist_view_exp_p = array();
								if($r_aow_p['community_view_selected']!="")
								{
									$artist_view_exp = explode(',',$r_aow_p['community_view_selected']);
									for($i_im=0;$i_im<count($artist_view_exp);$i_im++)
									{
										if($artist_view_exp[$i_im]!=$com_remove_id)
										{
											$artist_view_exp_p[] = $artist_view_exp[$i_im];
										}
									}
									$artist_view_exp_ip = implode(',',$artist_view_exp_p);
									$up_eve_art = $friend_noej->update_artistpro_c($artist_view_exp_ip,$r_aow_p['id']);
								}
							}
						}
					}
					
					if($post_exp[5]!=0)
					{
						if(isset($art_remove_id) && $art_remove_id!=0)
						{
							$sql_com_ah = $friend_noej->friend_com($post_exp[5]);
							if(mysql_num_rows($sql_com_ah)>0)
							{
								while($r_aow_c_ah = mysql_fetch_assoc($sql_com_ah))
								{
									$artist_view_exp_n = array();
									if($r_aow_c_ah['artist_view_selected']!="")
									{
										$artist_view_exp = explode(',',$r_aow_c_ah['artist_view_selected']);
										for($i_im_c_ah=0;$i_im_c_ah<count($artist_view_exp);$i_im_c_ah++)
										{
											if($artist_view_exp[$i_im_c_ah]!=$art_remove_id)
											{
												$artist_view_exp_n[] = $artist_view_exp[$i_im_c_ah];
											}
										}
										$artist_view_exp_i = implode(',',$artist_view_exp_n);
										$up_eve_art = $friend_noej->update_community($artist_view_exp_i,$r_aow_c_ah['community_id']);
									}
								}
							}
							
							$sql_com_event_ah = $friend_noej->get_communityeve($post_exp[5]);
							if(mysql_num_rows($sql_com_event_ah)>0)
							{
								while($r_aow_e_ah = mysql_fetch_assoc($sql_com_event_ah))
								{
									$artist_view_exp_e = array();
									if($r_aow_e_ah['artist_view_selected']!="")
									{
										$artist_view_exp = explode(',',$r_aow_e_ah['artist_view_selected']);
										for($i_im_e_ah=0;$i_im_e_ah<count($artist_view_exp);$i_im_e_ah++)
										{
											if($artist_view_exp[$i_im_e_ah]!=$art_remove_id)
											{
												$artist_view_exp_e[] = $artist_view_exp[$i_im_e_ah];
											}
										}
										$artist_view_exp_ie = implode(',',$artist_view_exp_e);
										$up_eve_art = $friend_noej->update_communityeve($artist_view_exp_ie,$r_aow_e_ah['id']);
									}
								}
							}
							
							$sql_com_project_ah = $friend_noej->get_communitypro($post_exp[5]);
							if(mysql_num_rows($sql_com_project_ah)>0)
							{
								while($r_aow_p_ah = mysql_fetch_assoc($sql_com_project_ah))
								{
									$artist_view_exp_p = array();
									if($r_aow_p_ah['artist_view_selected']!="")
									{
										$artist_view_exp = explode(',',$r_aow_p_ah['artist_view_selected']);
										for($i_im_p_ah=0;$i_im_p_ah<count($artist_view_exp);$i_im_p_ah++)
										{
											if($artist_view_exp[$i_im_p_ah]!=$art_remove_id)
											{
												$artist_view_exp_p[] = $artist_view_exp[$i_im_p_ah];
											}
										}
										$artist_view_exp_ip = implode(',',$artist_view_exp_p);
										$up_eve_art = $friend_noej->update_communitypro($artist_view_exp_ip,$r_aow_p_ah['id']);
									}
								}
							}
						}
						
						$sql_art = $friend_noej->friend_com($post_exp[5]);
						if(mysql_num_rows($sql_art)>0)
						{
							while($r_aow_a = mysql_fetch_assoc($sql_art))
							{
								$artist_view_exp_n = array();
								if($r_aow_a['community_view_selected']!="")
								{
									$artist_view_exp = explode(',',$r_aow_a['community_view_selected']);
									for($i_im=0;$i_im<count($artist_view_exp);$i_im++)
									{
										if($artist_view_exp[$i_im]!=$com_remove_id)
										{
											$artist_view_exp_n[] = $artist_view_exp[$i_im];
										}
									}
									$artist_view_exp_i = implode(',',$artist_view_exp_n);
									$up_eve_art = $friend_noej->update_community_c($artist_view_exp_i,$r_aow_a['community_id']);
								}
							}
						}
						
						$sql_art_event = $friend_noej->get_communityeve($post_exp[5]);
						if(mysql_num_rows($sql_art_event)>0)
						{
							while($r_aow_e = mysql_fetch_assoc($sql_art_event))
							{
								$artist_view_exp_e = array();
								if($r_aow_e['community_view_selected']!="")
								{
									$artist_view_exp = explode(',',$r_aow_e['community_view_selected']);
									for($i_im=0;$i_im<count($artist_view_exp);$i_im++)
									{
										if($artist_view_exp[$i_im]!=$com_remove_id)
										{
											$artist_view_exp_e[] = $artist_view_exp[$i_im];
										}
									}
									$artist_view_exp_ie = implode(',',$artist_view_exp_e);
									$up_eve_art = $friend_noej->update_communityeve_c($artist_view_exp_ie,$r_aow_e['id']);
								}
							}
						}
						
						$sql_art_project = $friend_noej->get_communitypro($post_exp[5]);
						if(mysql_num_rows($sql_art_project)>0)
						{
							while($r_aow_p = mysql_fetch_assoc($sql_art_project))
							{
								$artist_view_exp_p = array();
								if($r_aow_p['community_view_selected']!="")
								{
									$artist_view_exp = explode(',',$r_aow_p['community_view_selected']);
									for($i_im=0;$i_im<count($artist_view_exp);$i_im++)
									{
										if($artist_view_exp[$i_im]!=$com_remove_id)
										{
											$artist_view_exp_p[] = $artist_view_exp[$i_im];
										}
									}
									$artist_view_exp_ip = implode(',',$artist_view_exp_p);
									$up_eve_art = $friend_noej->update_communitypro_c($artist_view_exp_ip,$r_aow_p['id']);
								}
							}
						}
					}
					$delete_tag_data2 = $friend_noej->delete_tag($sql_check_com['general_user_id'],$_SESSION['login_id']);
					$sql_sub = $feeds_unsub->subscription($_SESSION['login_email'],$sql_check_com['general_user_id']);
					$sql_cnt = $feeds_unsub->remove_contact($_SESSION['login_id'],$sql_check_com['general_user_id']);
					$delete_sql_gen = $friend_noej->delete_friend($sql_check_com['general_user_id'],$_SESSION['login_id']);
					//echo "large";
				}
			}
		}
	}
	
?>
