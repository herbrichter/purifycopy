<?php
session_start();
include_once("commons/db.php");
include_once('classes/Commontabs.php');
include("classes/viewCommunityProject.php");
include("classes/ViewCommunityUserProfile.php");
include("classes/ViewArtistUserProfile.php");

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Purify Entertainment: About</title>
<!--<link rel="stylesheet" type="text/css" media="screen" href="includes/purify.css" />
<script src="includes/jquery.js" type="text/javascript"></script>
<script src="includes/functions.js" type="text/javascript"></script>

<link rel="stylesheet" type="text/css" media="screen" href="includes/purify.css" />
<!--<script src="includes/jquery.js" type="text/javascript"></script>
<script src="js/jquery.min.js"></script>
<script src="includes/functionsmedia.js" type="text/javascript"></script>
<script src="includes/functions.js" type="text/javascript"></script>
<link href="galleryfiles/gallery.css" rel="stylesheet"/>
<script src="galleryfiles/jquery.easing.1.3.js"></script>
<script src="includes/mediaelement-and-player.min.js"></script>
<link rel="stylesheet" href="includes/mediaelementplayer.min.css" />
<!--<script src="javascript/jquery-1.2.6.js" type="text/javascript"></script>
<script src="http://jqueryjs.googlecode.com/files/jquery-1.2.6.min.js" type="text/javascript"></script>
<link rel="stylesheet" href="lightbox/prettyPhoto.css" type="text/css" media="screen" title="prettyPhoto main stylesheet" charset="utf-8" />
<script src="lightbox/jquery.prettyPhoto.js" type="text/javascript" charset="utf-8"></script>
<!--<script src="includes/popup.js" type="text/javascript"></script>
<script src="javascripts/popup.js" type="text/javascript" charset="utf-8"></script>-->

<link rel="stylesheet" type="text/css" media="screen" href="includes/purify.css" />
<script src="js/jquery.min.js"></script>
<script src="includes/functionsmedia.js" type="text/javascript"></script>
<script src="includes/functions.js" type="text/javascript"></script>
<link href="galleryfiles/gallery.css" rel="stylesheet"/>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4/jquery.min.js"></script>
<script src="galleryfiles/jquery.easing.1.3.js"></script>
<link rel="stylesheet" href="lightbox/prettyPhoto.css" type="text/css" media="screen" title="prettyPhoto main stylesheet" charset="utf-8" />
<script src="lightbox/jquery.prettyPhoto.js" type="text/javascript" charset="utf-8"></script>

</head>
<?php
	$id = $_GET['id'];
	
	$newtab=new Commontabs();
	$new_profile_class_obj=new ViewCommunityUserProfile();
	$new_profile_class_obj_artist=new ViewArtistUserProfile();
	
	$newres1=$newtab->tabs();
	//$newres1=mysql_fetch_array($result);
	
	$project_info_obj = new viewCommunityProject();
	$getproject = $project_info_obj->get_CommunityProject($id);
	//var_dump($getproject);
	$state = $project_info_obj->getState($id);
	$country = $project_info_obj->getCountry($id);
	$type = $project_info_obj->getType($id);
	$sub = $project_info_obj->getSubType($id);
	$meta = $project_info_obj->getMetaType($id);
	
	$all_media = $project_info_obj->get_All_Media($id);
	$Find_Artist = $project_info_obj->chk_for_artist_tab($all_media['tagged_user_email']);
	//var_dump($all_media);
	$exp_upcom= explode(',',$all_media['taggedupcomingevents']);
	$exp_rec= explode(',',$all_media['taggedrecordedevents']);
	$exp_pro= explode(',',$all_media['tagged_projects']);
	$all_comm = explode(',',$all_media['community_view_selected']);
	$all_art = explode(',',$all_media['tagged_user_email']);
	
	$date=date('y-m-d');
	$video_id=explode(",",$all_media['tagged_videos']);
	if(isset($all_media['featured_media']) && $all_media['featured_media']=='Video' && $all_media['media_id']!="")
	{
		$new_array_video = array();
		$video_id[].= $all_media['media_id'];
		
		foreach ($video_id as $key3 => $value3) 
		{
			if(isset($new_array_video[$value3]))
			{
				$new_array_video[$value3] += 1;
			}
			else
				$new_array_video[$value3] = 1;
		}
		foreach ($new_array_video as $uid2 => $n) 
		{
			$ex_uid3=$ex_uid3.','.$uid2;
		}
		$video_id=explode(',',$ex_uid3);
	}
	
	$song_id=explode(",",$all_media['tagged_songs']);
	if(isset($all_media['featured_media']) && $all_media['featured_media']=='Song' && $all_media['media_id']!="")
	{
		$new_array_song = array();
		$song_id[].= $all_media['media_id'];
		
		foreach ($song_id as $key2 => $value2) 
		{
			if(isset($new_array_song[$value2]))
			{
				$new_array_song[$value2] += 1;
			}
			else
				$new_array_song[$value2] = 1;
		}
		foreach ($new_array_song as $uid1 => $n) 
		{
			$ex_uid2=$ex_uid2.','.$uid1;
		}
		$song_id=explode(',',$ex_uid2);
	}
	
	$gal_id=explode(",",$all_media['tagged_galleries']);
	if(isset($all_media['featured_media']) && $all_media['featured_media']=='Gallery' && $all_media['media_id']!="")
	{
		$new_array_gal = array();
		$gal_id[].= $all_media['media_id'];
		
		foreach ($gal_id as $key1 => $value1) 
		{
			if(isset($new_array_gal[$value1]))
			{
				$new_array_gal[$value1] += 1;
			}
			else
				$new_array_gal[$value1] = 1;
		}
		foreach ($new_array_gal as $uid => $n) 
		{
			$ex_uid1=$ex_uid1.','.$uid;
		}
		$gal_id=explode(',',$ex_uid1);
	}
	
	$pro_id=explode(",",$all_media['tagged_projects']);
	$event_id=explode(",",$all_media['tagged_events']);

?>
<script type="text/javascript">
$("document").ready(function(){
var imagename = "<?php echo $newres1['image_name']; ?>";
if(imagename=="")
{
	imagename="Noimage.png";
	$('#loggedin').css({
	backgroundImage : 'url(/uploads/profile_pic/'+ imagename +')',
	backgroundSize :'50px',
	backgroundRepeat: 'no-repeat',
	backgroundPosition: 'right top'
	});
}
else
{
	$('#loggedin').css({
	backgroundImage : 'url(general_jcrop/croppedFiles/thumb/'+ imagename +')',
//backgroundSize :'50px'
	backgroundRepeat: 'no-repeat',
	backgroundPosition: 'right top'
	});
}

});
</script>
<body onload="startTab();">
<div id="outerContainer">
<?php 

include("header.php");
?>
       <!-- <p><a href="profileedit_artist.php">Edit Profile</a><br />
        <a href="../logout.php">Logout</a></p>
      </div>
    </div>
  </div>
  </div>-->
  <div id="contentContainer">
    <!-- PROFILE START -->
    <div id="profileHeader">
      <div id="profilePhoto"><img src="<?php if($getproject['image_name']!="") { echo $getproject['image_name']; } else { echo "http://generalproimage.s3.amazonaws.com/Noimage.png"; } ?>" /></div>
      <div id="profileInfo">       
        <div id="profileFeatured">
			<div class="fmedia">
				featured Media
			</div>
			<a href=""><img src="images/profile/play.png" width="35" height="23" onmouseover="this.src='images/profile/play-over.png'" onmouseout="this.src='images/profile/play.png'" /></a>
			<a href=""><img src="images/profile/add.png" width="27" height="23" onmouseover="this.src='images/profile/add-over.png'" onmouseout="this.src='images/profile/add.png'" /></a>
		</div>
        <h2><?php if($type['name']!="") { echo $type['name'].','.$sub['name']; } ?></h2>
        <h1><? echo $getproject['title']; ?></h1>
        <h3>Creator: <? echo $getproject['creator']; ?><br/>         
          <?php if(isset($meta) && $meta!=null) 
			{
			?>
		   Type: 
		   <?php
				if(isset($meta[0]['name'])) echo $meta[0]['name']; ?>
				<?php if(isset($meta[1]['name'])) echo ', '. $meta[1]['name']; ?>
				<?php if(isset($meta[2]['name'])) echo ', '. $meta[2]['name'];
			?> <br/>
			<?php
			} ?>
		  Where: <? echo $getproject['city'].','.$state['state_name'].','.$country['country_name']; ?></h3>
       <? //echo $getproject['description']; ?>
	   
	    <p><?php
		$str_count = substr_count($getproject['description'],'<p>');
		//echo substr_count($getproject['description'],'<p>');
		$data_len = strlen($getproject['description']);
		//echo strlen($getproject['description']);
		//echo $data_len/75;
		//echo round($data_len/75);
		$st_pos=0;
		$en_pos=75;
		$get_data_len=round($data_len/75);
		if($str_count==1)
		{
			for($index=1;$index<=$get_data_len;$index++)
			{
				echo substr($getproject['description'],$st_pos,$en_pos)."</br>";
				$st_pos=$st_pos + $en_pos;
			}
		}
		else
		{
			echo $getproject['description'];
		}
		
		//echo $getuser['bio'];
		
		?></p>
	   
      </div>
      <div id="profileStatsBlock">
        <div id="profileStats">
          <p>Subscribers: 765</p>
          <p>Fans: 21,304</p>
          <p>Homepage</p>
        </div>
        <div id="profileButtons"><a href="#" >Subscribe</a>
		<a href="total_media.php?id=<?php echo $_GET['id'];?>" id="FanorDown"><?php if($getproject['type']=="fan_club"){echo "Fan Club";}else if($getproject['type']=="for_sale"){echo "Download";}?></a>

		<a href="#">Homepage</a></div>
      </div>
    </div>
    <div id="profileTabs">
      <ul>
	  <?php
	  if($Find_Artist =="artist")
	  {
	  ?>
        <li><a href="javascript:tabOne();" class="profileLit" id="tabOne">Artists</a></li>
        <?php
		}
		if($all_media['tagged_videos']!= "" || ($all_media['featured_media']=='Video' && $all_media['media_id']!=""))
		{
		?>
			<li><a href="javascript:tabTwo();" class="profileTab2" id="tabTwo" >Videos</a></li>
		<?php
		}
		if($all_media['tagged_songs']!= "" || ($all_media['featured_media']=='Song' && $all_media['media_id']!=""))
		{
		?>
			<li><a href="javascript:tabThree();" class="profileTab3" id="tabThree">Songs</a></li>
		<?php
		}
		
		if($all_media['tagged_galleries']!= "" || ($all_media['featured_media']=='Gallery' && $all_media['media_id']!=""))
		{
		?>
			<li><a href="javascript:tabFour();" class="profileTab4" id="tabFour">Galleries</a></li>
		<?php
		}
		
		if($all_media['tagged_projects']!="")
		{
		?>
			<li><a href="javascript:tabSix();" class="profileTab6" id="tabSix">Projects</a></li>
		<?php
		}
		if($all_media['taggedupcomingevents'] !="" || $all_media['taggedrecordedevents'] !="" ) 
		{
		?>
			<li><a href="javascript:tabFive();" class="profileTab5" id="tabFive">Events</a></li>
		<?php
		}
		?>
      </ul>
    </div>

	<?php
	if($Find_Artist =="artist")
	{
	?>
		<div id="mediaTab" class="hiddenBlock1">
			<div id="profileFeatured">
				<div class="playall">
					Play All
				</div>
				<a href=""><img src="images/profile/play.png" width="35" height="23" onmouseover="this.src='images/profile/play-over.png'" onmouseout="this.src='images/profile/play.png'" /></a>
				<a href=""><img src="images/profile/add.png" width="27" height="23" onmouseover="this.src='images/profile/add-over.png'" onmouseout="this.src='images/profile/add.png'" /></a>
			</div>
            <h2><a href="#">Artists</a></h2>
			<div class="rowItem">
			<?php
				for($exn_pr=0;$exn_pr<=count($all_art);$exn_pr++)
				{
					if($all_art[$exn_pr] !="")
					{
					$get_community = $project_info_obj->get_all_artist_info($all_art[$exn_pr]);
			?>
						<div class="tabItem"> 
							<img src="<?php if(isset($get_community['image_name']) && $get_community['image_name']!="") { echo 'http://artjcropprofile.s3.amazonaws.com/'.$get_community['image_name'] ; } else { echo 'http://artjcropprofile.s3.amazonaws.com/Noimage.png'; } ?>" width="200" height="175" />
							<h3><?php echo $get_community['name']; ?></h3>
							<div class="player">
								<img src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" />
								<img src="images/profile/add.gif"  onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'"/>
								<img src="images/profile/download.gif" onmouseover="this.src='images/profile/download-over.gif'" onmouseout="this.src='images/profile/download.gif'" />
							</div>
						</div>
		<?php
					}	
				}
		?>
			</div>
		</div> 
	<?php
	}
	else
	{
	?>
		<div id="mediaTab" class="hiddenBlock1">
		</div>
	<?php
	}
	?>
    
    
    
    <div id="eventsTab" class="hiddenBlock2">
		<div id="profileFeatured">
			<div class="playall">
				Play All
			</div>
			<a href=""><img src="images/profile/play.png" width="35" height="23" onmouseover="this.src='images/profile/play-over.png'" onmouseout="this.src='images/profile/play.png'" /></a>
			<a href=""><img src="images/profile/add.png" width="27" height="23" onmouseover="this.src='images/profile/add-over.png'" onmouseout="this.src='images/profile/add.png'" /></a>
		</div>
      <h2><a href="#">Videos</a></h2>
      <div class="rowItem">
	  <?php
		for($k=0;$k<count($video_id);$k++)
		{
			if($video_id[$k]=="")
			{
				continue;
			}
			else
			{
				$Video = $project_info_obj->get_Video($video_id[$k]);
				$src_list ="http://comprothumjcrop.s3.amazonaws.com/";
				$from_list_pic ="";
				$creator_list_pic ="";
				if($Video['from_info']!="")
				{
					$expl_from_in =explode("|",$Video['from_info']);
					$get_from_list_pic = $new_profile_class_obj->get_from_list_pic($expl_from_in[0],$expl_from_in[1]);
					if($get_from_list_pic['listing_image_name'] =="")
					{
						$from_list_pic="http://artjcropthumb.s3.amazonaws.com/Noimage.png";
					}
					else
					{
						$from_list_pic = $get_from_list_pic['listing_image_name'];
					}
				}
				if($Video['from_info'] =="" && $Video['creator_info']!="")
				{
					$expl_creator_in =explode("|",$Video['creator_info']);
					if($expl_creator_in[0] == "general_community")
					{
						$col_name = "community_id";
						$creator_src = "http://comjcropthumb.s3.amazonaws.com/";
					}
					else if($expl_creator_in[0] == "general_artist")
					{
						$col_name = "artist_id";
						$creator_src = "http://artjcropthumb.s3.amazonaws.com/";
					}
					else
					{
						$col_name = "id";
						$creator_src = "";
					}
					$get_creator_list_pic = $new_profile_class_obj->get_creator_list_pic($expl_creator_in[0],$expl_creator_in[1],$col_name);
					if($get_creator_list_pic['listing_image_name'] =="")
					{
						$creator_list_pic = $creator_src."Noimage.png";
					}
					else
					{
						$creator_list_pic = $creator_src.$get_creator_list_pic['listing_image_name'];
					}
				}
			}
		if($Video['title'] !="")
		{
			?>
			<div class="tabItem"> <img src="<?php 
			if(isset($from_list_pic) && $from_list_pic!="")
			{
				echo $from_list_pic;
			}
			else if(isset($creator_list_pic) && $creator_list_pic!="")
			{
				echo $creator_list_pic;
			}
			else
			{
			echo $src_list."Noimage.png";}?>" width="200" height="175" />
			  <h3><?php echo substr($Video['title'],0,29);?></h3>
			  <div class="player">
				<img src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" />
				<img src="images/profile/add.gif"  onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'"/>
				<img src="images/profile/download.gif" onmouseover="this.src='images/profile/download-over.gif'" onmouseout="this.src='images/profile/download.gif'" />
			  </div>
			</div>
			<?php
		}
		}
		?>
      </div>      
    </div>
    
    
    
    
    
     <div id="projectsTab" class="hiddenBlock3">
		<div id="profileFeatured">
			<div class="playall">
				Play All
			</div>
			<a href=""><img src="images/profile/play.png" width="35" height="23" onmouseover="this.src='images/profile/play-over.png'" onmouseout="this.src='images/profile/play.png'" /></a>
			<a href=""><img src="images/profile/add.png" width="27" height="23" onmouseover="this.src='images/profile/add-over.png'" onmouseout="this.src='images/profile/add.png'" /></a>
		</div>
      <h2><a href="#">Songs</a></h2>
      <div class="rowItem">
	  <?php
		for($s=0;$s<count($song_id);$s++)
		{
			if($song_id[$s]=="")
			{
				continue;
			}
			else
			{
				$Song = $project_info_obj->get_Songs_regis($song_id[$s]);
				if($Song['audio_name']=="")
				{
					$Song_media = $project_info_obj->get_Songs_media($song_id[$s]);
				}
				
				
				$get_community_Song=$new_profile_class_obj->get_community_Song($song_id[$s]);
				
				
				$src_list ="http://comprothumjcrop.s3.amazonaws.com/";
				$from_list_pic_song ="";
				$creator_list_pic_song ="";
				if($Song_media['from_info']!="")
				{
					$expl_from_in =explode("|",$Song_media['from_info']);
					$get_from_list_pic = $new_profile_class_obj->get_from_list_pic($expl_from_in[0],$expl_from_in[1]);
					if($get_from_list_pic['listing_image_name'] =="")
					{
						$from_list_pic_song="http://artjcropthumb.s3.amazonaws.com/Noimage.png";
					}
					else
					{
						$from_list_pic_song = $get_from_list_pic['listing_image_name'];
					}
				}
				if($Song_media['from_info'] =="" && $Song_media['creator_info']!="")
				{
					$expl_creator_in =explode("|",$Song_media['creator_info']);
					if($expl_creator_in[0] == "general_community")
					{
						$col_name = "community_id";
						$creator_src = "http://comjcropthumb.s3.amazonaws.com/";
					}
					else if($expl_creator_in[0] == "general_artist")
					{
						$col_name = "artist_id";
						$creator_src = "http://artjcropthumb.s3.amazonaws.com/";
					}
					else
					{
						$col_name = "id";
						$creator_src = "";
					}
					$get_creator_list_pic = $new_profile_class_obj->get_creator_list_pic($expl_creator_in[0],$expl_creator_in[1],$col_name);
					if($get_creator_list_pic['listing_image_name'] =="")
					{
						$creator_list_pic_song = $creator_src."Noimage.png";
					}
					else
					{
						$creator_list_pic_song = $creator_src.$get_creator_list_pic['listing_image_name'];
					}
				}
				/*$Get_gal_img_for_song1_artist=$new_profile_class_obj_artist->get_artist_Gallery_at_register_for_song($get_community_Song['gallery_id']);
				if(mysql_num_rows($Get_gal_img_for_song1_artist)>0)
				{
					$Get_gal_img_for_song=mysql_fetch_assoc($Get_gal_img_for_song1_artist);
					$starting_path="http://reggalthumb.s3.amazonaws.com/";
				}
				$Get_gal_img_for_song1_comm=$new_profile_class_obj->get_community_Gallery_at_register_for_song($get_community_Song['gallery_id']);
				if(mysql_num_rows($Get_gal_img_for_song1_comm)>0)
				{			
					$Get_gal_img_for_song=mysql_fetch_assoc($Get_gal_img_for_song1_comm);
					$starting_path="http://reggalthumb.s3.amazonaws.com/";
				}
				
				$Get_gal_img_for_song1_media=$new_profile_class_obj->get_media_Gallery($get_community_Song['gallery_id']);
				if(mysql_num_rows($Get_gal_img_for_song1_media)>0)
				{			
					$Get_gal_img_for_song=mysql_fetch_assoc($Get_gal_img_for_song1_media);
					$starting_path="http://medgalthumb.s3.amazonaws.com/";
				}*/
			}
		if($Song['audio_name'] !="" || $Song_media['title']!="")
		{
			?>
			<div class="tabItem"> <img src="<?php 
			if(isset($from_list_pic_song) && $from_list_pic_song!="")
			{
				echo $from_list_pic_song;
			}
			else if(isset($creator_list_pic_song) && $creator_list_pic_song!="")
			{
				echo $creator_list_pic_song;
			}
			else
			{
			echo $src_list."Noimage.png";}
			?>" width="200" height="175" />
			  <h3><?php if($Song['audio_name']=="") { echo substr($Song_media['title'],0,29);} else{ echo substr($Song['audio_name'],0,29);}?></h3>
			  <div class="player">
				<img src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" />
				<img src="images/profile/add.gif"  onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'"/>
				<img src="images/profile/download.gif" onmouseover="this.src='images/profile/download-over.gif'" onmouseout="this.src='images/profile/download.gif'" />
			  </div>
			</div>
			<?php
		}
		}
		?>
      
      </div>      
    </div>
    
    
    
    
    <div id="servicesTab" class="hiddenBlock4">
		<div id="profileFeatured">
			<div class="playall">
				Play All
			</div>
			<a href=""><img src="images/profile/play.png" width="35" height="23" onmouseover="this.src='images/profile/play-over.png'" onmouseout="this.src='images/profile/play.png'" /></a>
			<a href=""><img src="images/profile/add.png" width="27" height="23" onmouseover="this.src='images/profile/add-over.png'" onmouseout="this.src='images/profile/add.png'" /></a>
		</div>
      <h2><a href="#">Galleries</a></h2>
      <div class="rowItem">
	  <?php
		for($g=0;$g<count($gal_id);$g++)
		{
			if($gal_id[$g]=="" || $gal_id[$g]==0)
			{
				continue;
			}
			else
			{
				
				$Get_count_img_at_reg=$project_info_obj->get_count_community_pro_Gallery_at_register($gal_id[$g]);
				if($Get_count_img_at_reg['count(*)']>0)
				{

					$Get_img_at_reg=$project_info_obj->get_community_pro_Gallery_at_register($gal_id[$g]);
					$play_array = array();
					$Get_com_media_cover_pic="";
						if(mysql_num_rows($Get_img_at_reg)>0)
						{
							while($row = mysql_fetch_assoc($Get_img_at_reg))
							{
								$play_array[] = $row;
								//var_dump($row);
							}
						
							${'orgPath'.$g}="http://reggallery.s3.amazonaws.com/";
							${'thumbPath'.$g}="http://reggalthumb.s3.amazonaws.com/";
							$Gallery=$project_info_obj->get_community_pro_Gallery_title_at_register($play_array[0]['gallery_id']);
							//var_dump($get_community_gallery);
							for($l=0;$l<count($play_array);$l++)
							{
								$mediaSrc = $play_array[0]['image_name'];
								$filePath = "http://reggallery.s3.amazonaws.com/".$mediaSrc;
								$register_cover_pic=$play_array[0]['cover_pic'];
								$register_No_pic=$play_array[0]['image_name'];
								${'galleryImagesData'.$g}[$galleryImagesDataIndex] = $play_array[$l]['image_name'];
								${'galleryImagestitle'.$g}[$galleryImagesDataIndex] = $play_array[$l]['image_title'];
								$galleryImagesDataIndex++;
							}
						}
				}
				else
				{
				
					$Gallery = $project_info_obj->get_gallery($gal_id[$g]);
					$Get_com_media_cover_pic=$project_info_obj->get_media_cover_pic($Gallery['id']);
					//echo $gal_id[$i];
					$sql_play1 = mysql_query("SELECT * FROM media_images WHERE media_id='".$gal_id[$g]."' AND gallery_id!=0");
						$register_cover_pic="";
						$play_array = array();
						if(mysql_num_rows($sql_play1)>0)
						{
							while($row = mysql_fetch_assoc($sql_play1))
							{
								$play_array[] = $row;
							}
							
						
							${'orgPath'.$g}="http://medgallery.s3.amazonaws.com/";
							${'thumbPath'.$g}="http://medgalthumb.s3.amazonaws.com/";
							for($l=0;$l<count($play_array);$l++)
							{
								$mediaSrc = $play_array[0]['image_name'];
								$filePath = "http://medgalthumb.s3.amazonaws.com/".$mediaSrc;
								${'galleryImagesData'.$g}[$galleryImagesDataIndex] = $play_array[$l]['image_name'];
								${'galleryImagestitle'.$g}[$galleryImagesDataIndex] = $play_array[$l]['image_title'];
								//echo ${'galleryImagesData'.$g}[$galleryImagesDataIndex];
								$galleryImagesDataIndex++;
								
								
							}
						}
						else
						{
							${'orgPath'.$g}="http://medsingleimage.s3.amazonaws.com/";
							${'thumbPath'.$g}="http://medgalthumb.s3.amazonaws.com/";
							
							$sql_play1 = mysql_query("SELECT * FROM media_images WHERE media_id='".$gal_id[$g]."' AND gallery_id=0");
							$play_array = array();
							if(mysql_num_rows($sql_play1)>0)
							{
								while($row = mysql_fetch_assoc($sql_play1))
								{
									$play_array[] = $row;
								}
								
								
								for($l=0;$l<count($play_array);$l++)
								{
									$mediaSrc = $play_array[0]['image_name'];
									$filePath = "http://medsingleimage.s3.amazonaws.com/".$mediaSrc;
									
									${'galleryImagesData'.$g}[$galleryImagesDataIndex] = $play_array[$l]['image_name'];
									${'galleryImagestitle'.$g}[$galleryImagesDataIndex] = $play_array[$l]['image_title'];
									//echo ${'galleryImagesData'.$g}[$galleryImagesDataIndex];
									$galleryImagesDataIndex++;
									//echo $galleryImagesDataIndex;
								}
							}
						}
					}
			
		?>
		 
		  <?php
		  if(isset(${'galleryImagesData'.$g}))
		  {
				${'galleryImagesData'.$g} = implode(",", ${'galleryImagesData'.$g});
				${'galleryImagestitle'.$g} = implode(",", ${'galleryImagestitle'.$g});
		  }
	   
		if($Gallery['gallery_title'] !="" || $Gallery['title']!="")
		{
				  ?>
				 
				<div class="tabItem"><a href="javascript:void(0);"><img src="<?php if(isset($Get_com_media_cover_pic) && $Get_com_media_cover_pic!=""){
						if($Get_com_media_cover_pic['cover_pic']=="")
						{
							echo "http://medgalthumb.s3.amazonaws.com/".$Get_com_media_cover_pic['image_name'];
						}
						else
						{
							echo "http://medgalthumb.s3.amazonaws.com/".$Get_com_media_cover_pic['cover_pic'];
						}
					}
					else
					{
						if($register_cover_pic=="")
						{
							echo "http://reggalthumb.s3.amazonaws.com/".$register_No_pic;
						}
						else
						{
							echo "http://reggalthumb.s3.amazonaws.com/".$register_cover_pic;
						}
					}
					
				?>" width="200" height="175" onclick="checkload('<?php echo $mediaSrc;?>','<?php echo ${'galleryImagesData'.$g}; ?>','<?php echo ${'thumbPath'.$g}; ?>','<?php echo ${'orgPath'.$g}; ?>','<?php echo ${'galleryImagestitle'.$g}; ?>')" /></a>
				  <h3><?php if(isset($Gallery['gallery_title'])){echo substr($Gallery['gallery_title'],0,29);}else {echo substr($Gallery['title'],0,29);}?></h3>
				 <div class="player">
					<a href="javascript:void(0);"><img src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onclick="checkload('<?php echo $mediaSrc;?>','<?php echo ${'galleryImagesData'.$g}; ?>','<?php echo ${'thumbPath'.$g}; ?>','<?php echo ${'orgPath'.$g}; ?>','<?php echo ${'galleryImagestitle'.$g}; ?>')" onmouseout="this.src='images/profile/play.gif'" /></a>
					<img src="images/profile/add.gif"  onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'"/>
					<img src="images/profile/download.gif" onmouseover="this.src='images/profile/download-over.gif'" onmouseout="this.src='images/profile/download.gif'" />
					</div>
				</div>
				<?php
			}
			}
		}
		?>
        
      </div>
    </div>
    
    
    
    
    <div id="friendsTab" class="hiddenBlock5">
		<div id="profileFeatured">
			<div class="playall">
				Play All
			</div>
			<a href=""><img src="images/profile/play.png" width="35" height="23" onmouseover="this.src='images/profile/play-over.png'" onmouseout="this.src='images/profile/play.png'" /></a>
			<a href=""><img src="images/profile/add.png" width="27" height="23" onmouseover="this.src='images/profile/add-over.png'" onmouseout="this.src='images/profile/add.png'" /></a>
		</div>
		<?php
		if($all_media['taggedupcomingevents']!="")
		{
		?>
      <h2>Upcoming Events</h2>
      <div class="rowItem">
	  <?php
		for($ex_up=0;$ex_up<=count($exp_upcom);$ex_up++)
		{
			$get_upcoming_event = $project_info_obj->upcomingEvents($date,$exp_upcom[$ex_up]);
			while($row_up=mysql_fetch_assoc($get_upcoming_event))
			{
	  ?>
        <div class="tabItem">
		<a href="#"><img src="<?php if($row_up['image_name']=="") { echo "http://comeventprojcrop.s3.amazonaws.com/Noimage.png";} else { echo $row_up['image_name']; } ?>" width="200" height="175" />
          <h3><?php echo substr($row_up['title'],0,29);?></h3>
          <div class="player">
			<img src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" />
			<img src="images/profile/add.gif"  onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'"/>
			<img src="images/profile/download.gif" onmouseover="this.src='images/profile/download-over.gif'" onmouseout="this.src='images/profile/download.gif'" />
		  </div>
        </div>
		<?php
			}
		}
		?>
      </div>
	  <?php
	  }
	  if($all_media['taggedrecordedevents']!="")
	  {
	  ?>
      <h2>Recorded Events</h2>
      <div class="rowItem">
	  <?php
		for($ex_re=0;$ex_re<=count($exp_rec);$ex_re++)
		{
			$get_recorded_event= $project_info_obj->recordedEvents($date,$exp_rec[$ex_re]);
			while($row_re=mysql_fetch_assoc($get_recorded_event))
			{
	  ?>
        <div class="tabItem">
		<a href="#"><img src="<?php if($row_re['image_name']=="") { echo "http://comeventprojcrop.s3.amazonaws.com/Noimage.png"; } else { echo $row_re['image_name']; } ?>" width="200" height="175" />
          <h3><?php echo substr($row_re['title'],0,29);?></h3>
          <div class="player">
			<img src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" />
			<img src="images/profile/add.gif"  onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'"/>
			<img src="images/profile/download.gif" onmouseover="this.src='images/profile/download-over.gif'" onmouseout="this.src='images/profile/download.gif'" />
		  </div>
        </div>
        <?php
			}
		}
		?>
      </div>
	  <?php
	  }
	  ?>
    </div>
    
    
    
    <div id="promotionsTab" class="hiddenBlock6">
		<div id="profileFeatured">
			<div class="playall">
				Play All
			</div>
			<a href=""><img src="images/profile/play.png" width="35" height="23" onmouseover="this.src='images/profile/play-over.png'" onmouseout="this.src='images/profile/play.png'" /></a>
			<a href=""><img src="images/profile/add.png" width="27" height="23" onmouseover="this.src='images/profile/add-over.png'" onmouseout="this.src='images/profile/add.png'" /></a>
		</div>
      <h2>Projects</h2>
      <div class="rowItem">
	  <?php
		for($ex_pr=0;$ex_pr<=count($exp_pro);$ex_pr++)
		{
			$get_project = $project_info_obj->get_Project($exp_pro[$ex_pr]);
			while($row_pro=mysql_fetch_assoc($get_project))
			{
	  ?>
        <div class="tabItem">
		<a href="profile_community_project.php?id=<?php echo $row_pro['id'];?>"><img src="<?php if($row_pro['image_name']=="") { echo "http://comproprofjcrop.s3.amazonaws.com/Noimage.png"; } else { echo $row_pro['image_name']; } ?>" width="200" height="175" />
          <h3><?php echo substr($row_pro['title'],0,29);?></h3>
          <div class="player">
			<img src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" />
			<img src="images/profile/add.gif"  onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'"/>
			<img src="images/profile/download.gif" onmouseover="this.src='images/profile/download-over.gif'" onmouseout="this.src='images/profile/download.gif'" />
		  </div>
        </div>
		<?php
			}
		}
		?>
      </div>
    </div>
    
    
    
    <!-- PROFILE END -->
  </div>
 <?php include_once("displayfooter.php");?>
</div>
<script type="text/javascript">
$(document).ready(function() {
				$("#FanorDown").fancybox({
				'autoScale'			: true,
				'transitionIn'		: 'none',
				'transitionOut'		: 'none',
				'type'				: 'iframe'
			});
		});
</script>

<div id="light-gal" class="gallery-wrapper">
	<div id="bg">
		<a href="#" class="nextImageBtn" title="next"></a>
		<a href="#" class="prevImageBtn" title="previous"></a>
		<img width="1680" src="./uploads/gallery/1335074394-7032884797_475ce129ca.jpg" height="1050" alt="" title="" id="bgimg" />
	</div>
	<div id="preloader"><img src="./galleryfiles/ajax-loader_dark.gif" width="32" height="32" /></div>
	<div id="img_title"></div>
	<div id="toolbar">
		<a href="#" title="Maximize" id="maximizeImg" onClick="ImageViewMode('full');return false"><img src="./galleryfiles/toolbar_fs_icon.png" width="50" height="50"  /></a>
		<a href="#" title="Close" id="closeGal" onClick="CloseGallery();return false"><img src="./galleryfiles/close.png" width="50" height="50"  /></a>
	</div>
	<div id="thumbnails_wrapper">
	<div id="outer_container">
	<div class="thumbScroller">
		<div class="container" id="gallery-container">
			
		</div>
	</div>
	</div>
	</div>
</div>

</body>
</html>

<script type="text/javascript" src="galleryfiles/jqgal.js"></script>
<script>

//function checkload(defImg,gIndex){
function checkload(defImg,Images,orgpath,thumbpath,title){
	var arr_sep = Images.split(',');
	var k;
	var data = new Array();
	for(k=0;k<arr_sep.length;k++)
	{
		//arr_sep[k] = arr_sep[k].replace("\"","");
		arr_sep[k] = arr_sep[k].replace(/"/g, '');
		arr_sep[k] = arr_sep[k].replace("[","");
		arr_sep[k] = arr_sep[k].replace("]","");
		data[k] = arr_sep[k];
	}

	if(data.length>0){
		$("#gallery-container").html('');
	}
	
	var title_sep = title.split(',');
	var q;
	var data1 = new Array();
	for(q=0;q<title_sep.length;q++)
	{
		title_sep[q] = title_sep[q].replace(/"/g, '');
		title_sep[q] = title_sep[q].replace("[","");
		title_sep[q] = title_sep[q].replace("]","");
		data1[q] = title_sep[q];
	}


	$("#bgimg").attr('src',thumbpath+defImg);
	$("#bgimg").attr('title',data1[0]);
	$("#bgimg").attr('title',data1[0]);

	for(var i=0;i<data.length;i++){
		$("#gallery-container").append("<div class='content'><div><a href='"+thumbpath+data[i]+"'><img  src='"+orgpath+data[i]+"' title='"+data1[i]+"' class='thumb' /></a></div></div>");
	}
	//setTimeout(function(){	loadGallery();	$("#light-gal").css('visibility','visible');	},500);	
	loadGallery();
	$("#light-gal").css('visibility','visible');
	document.getElementById("gallery-container").style.left = "0px";
}
	$(document).ready(function(){

		
		$("a[rel^='prettyPhoto']").prettyPhoto({social_tools:''});
		$("a[rel^='prettyAudio']").prettyPhoto({social_tools:'',
			changepicturecallback: function(){ $('audio').mediaelementplayer(); $(".mejs-container").css('margin-left',0); }	
		});

		//$("a[rel^='prettyAudio']").prettyPhoto();
	});

/////// Related Player	
	var windowRef;
	function showPlayer(tp,ref,act){
		//var windowRef = window.open('player/?tp='+tp+'&ref='+ref+'&source='+source);
		if(logedinFlag==""){	centerPopup();	loadPopup(); return;	}
		var pl = $("#curr_playing_playlist").val();
		$.ajax({
			type: "POST",
			url: 'player/getMediaInfo.php',
			data: { "tp":tp, "ref":ref, "source":"<?php echo $_GET['id'] ;?>", "act":act, "pl":pl, "creator":profileName },
			success: function(data){
				$("#curr_playing_playlist").val(data);
				if(!windowRef){
					windowRef = window.open('combinePlayer/index.php?tp='+tp+'&ref='+ref+'&pl='+data, 'formpopup', 'width=580,height=730,resizeable,scrollbars');
				}
			}
		});
	}

	$(".login").click(function(){
		checkLogin();//document.checkLoginFrm.submit();
	});

	
</script>