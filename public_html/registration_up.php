<?php
	include_once('commons/db.php');
	include_once('commons/session_check.php');
	include_once('classes/News.php');
	include_once('classes/Quote.php');
	include_once('classes/User.php');
	include_once('classes/Features.php');
	include_once('classes/SubType.php');
	include_once('classes/Type.php');			
	include_once('classes/UserType.php');			
	include_once('classes/UserImg.php');			
	
	//session_start();
	//$username = $_SESSION['name'];
	// if(!$username == '')
	// {
		// $login_flag=1;
	// }
	//var_dump($_SESSION['login_email']);
	$login_flag="";
	if($login_flag) include_once('loggedin_includes.php');
	else include_once('login_includes.php');
	
	$objNews=new News();
	$row1=$objNews->getLatestNews();
	
	$objQuote=new Quote();
	$row2=$objQuote->getRandomQuote();	
	
	$pagenum=isset($_GET['pagenum']);
	if (!(isset($pagenum)))
	{
		$pagenum = 1;
	} 
	
	$userObj = new User();
	$username="";
	$user = $userObj->getUserInfo($username);
	$uid = $user['user_id'];
	if(!($uid))
	{
		if(isset($_SESSION['uid']))
			$uid = $_SESSION['uid'];
		else
		{
			$uid = rand(0000, 9999);
			$_SESSION['uid'] = $uid;
		}

	}
	
	$objFeatures=new Features();
	$no_next=$objFeatures->countNextFeatures();

	switch(isset($_GET['sel']))
	{
		case 'next':$prev_flag=1;
					$page_rows = 6;
					$last = ceil($no_next/$page_rows); 	
					if ($pagenum < 1)
					{
						$pagenum = 1;
					}
					elseif ($pagenum > $last)
					{
						$pagenum = $last;
					}

					$max = 'limit ' .($pagenum - 1) * $page_rows .',' .$page_rows;
					$rs3=$objFeatures->getNextFeatures($max);

					if ($pagenum == 1)
					{
						$prev_url=$_SERVER['PHP_SELF']; 
					}
					else
					{
						$prev_url=$_SERVER['PHP_SELF'].'?sel=next&&pagenum='.($pagenum-1);
					}

					if ($pagenum == $last)
					{
						$next_flag=0;
					}
					else
					{
						$next_flag=1;
					}
										
					$next_url=$_SERVER['PHP_SELF'].'?sel=next&&pagenum='.($pagenum+1);
					break;
					
		default: 	$prev_flag=0;
					if($no_next!=NULL)	$next_flag=1;
					$rs3=$objFeatures->getCurrentFeatures();
					$next_url=$_SERVER['PHP_SELF'].'?sel=next';
					break;
	}

	
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "//www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="//www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<div id="outerContainer"></div>
<head>
<?php
	//$url_par = 'purify.net/me#mithesh';
	//$urls_parse = parse_url($url_par);
	//echo $urls_parse['fragment'];
	if(isset($_GET['uris']))
	{
		if(isset($_GET['hashing']))
		{
			$urils = '/'.$_GET['uris'].'#'.$_GET['hashing'];
		}
		else
		{
			$urils = '/'.$_GET['uris'];
		}
	}
	else
	{
		$urils = "";
	}
	include_once('includes/header.php');
 ?>
	<script src="jwplayer/jwplayer.js"></script>
	<script>jwplayer.key="e4OUSwhwX7jbBT3WDdQYuboL1dYW9b29pGxAHA=="</script>

	<script>
		$(function() 
		{
			$("#bday").datepicker({
			dateFormat: 'yy-mm-dd',
			changeMonth: true,
			changeYear: true
			});
		});
	</script>
<!-- <script type="text/javascript">
	function MM_openBrWindow(uid) { //v2.0
	  window.open('mediaplayer_single.php?uid='+uid,"","toolbar=no,location=no,directories=no, status=no,menubar=no,scrollbars=no,resizable=no,copyhistory=no,width=425,height=150");
	}   
</script> -->

<script language="JavaScript">
	function MM_openBrWindow(uid) 
	{
		if (opener && !opener.closed)
		{
//			opener.focus();
			opener.close();
			var myWin =window.open('mediaplayer_single.php?uid='+uid,"","toolbar=no,location=no,directories=no, status=no,menubar=no,scrollbars=no,resizable=no,copyhistory=no,width=430,height=150");
			opener = myWin;
		}
		else 
		{
			var myWin = window.open('mediaplayer_single.php?uid='+uid,"","toolbar=no,location=no,directories=no, status=no,menubar=no,scrollbars=no,resizable=no,copyhistory=no,width=430,height=150");
			opener = myWin;
		}
	}
</script>
<!--Ajax To check email id already present or not -->

<script>

function CheckEmail() {
	var ajaxFile = 'emailexist.php';
	$.post(ajaxFile,{"email":$("#email").val()},function(data){
		if(data == "Fail")	{	
			$("#emailerror").val('Email id already present.');
		}
		else if(data == "Valid")	{
			$("#emailerror").html('').show();
		}
		return false;
	});
}
</script>

  <div id="contentContainer" class="container">  
    <!--<div id="homeLeftColumn">
      <div id="homemission">
        <p>Purify Art is a not for profit organization bringing together Artists, Fans, Organizations, and Businesses in the Art industry on one ad free platform. 
        <!--The mission of Purify Art is to purify the arts industry by offering low cost, ad free internet services, and donating profits to artists and art communities around the world.</p>
        <h5 style="margin:0;"><a href="about-us.php">More</a></h5>
      </div>
      <!--<div id="homeQuote">
        <h2>Quote of the Now</h2>
        <h3>"<?php /*echo $row2['quote_para']; ?>"</h3>
        <h4><a href="<?php echo $row2['source_link']; ?>" target="_blank" class="greyLink"><?php echo $row2['source']; ?></a>, <a href="<?php echo $row2['author_link']; ?>" target="_blank" class="greyLink"><?php echo $row2['author']; ?></a></h4>
        <p align="right"><a href="about_quotes.php">Suggest a Quote</a></p>
      </div>-->
      <div id="front_page_news">
        <h2>Latest News</h2>
        <h4><?php echo $row1['posted_date']; ?> </h4>
        <h3><?php echo $row1['title']; ?></h3>
        <p><?php echo $row1['paragraph'];*/ ?></p>
		<h5><a href="about_news.php">More News</a></h5>
      </div>
    </div>-->
	<div id="homeLeftColumn">
		<div id="frontPageVideo">
        <!--<iframe src="//player.vimeo.com/video/56815569" width="380" height="214" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>-->
		<div id='my-video'></div>
		<script type='text/javascript'>
			/* jwplayer('my-video').setup({
				file: 'hmvideo/Purify_Into_V3_Encoded.m4v',
				image: 'hmvideo/screenshot.jpg',
				width: '380',
				height: '214',
			}); */
		</script>
		
		<div class="banner" style="width:380px;  margin-bottom:8px; margin-top:0px;">
			<div id="wrapper">
				<div class="slider-wrapper theme-default">
					<div id="slider" class="nivoSlider">
					<?php
						if(isset($_SESSION['login_email']))
						{
					?>
							<img src="images/banner1_new.png" data-thumb="images/banner1_new.png" alt="" />
							<img src="images/banner2_new.png" data-thumb="images/banner2_new.png" alt=""  />
							<img src="images/banner3_new.png" data-thumb="images/banner3_new.png" alt="" data-transition="slideInLeft" />
							<img src="images/banner4_new.png" data-thumb="images/banner4_new.png" alt=""  />
							<img src="images/banner5_new.png" data-thumb="images/banner5_new.png" alt=""  />
					<?php
						}
						else
						{
					?>
							<img src="images/banner1_new.png" data-thumb="images/banner1_new.png" alt="" />
							<img src="images/banner2_new.png" data-thumb="images/banner2_new.png" alt=""  />
							<img src="images/banner3_new.png" data-thumb="images/banner3_new.png" alt="" data-transition="slideInLeft" />
							<img src="images/banner4_new.png" data-thumb="images/banner4_new.png" alt=""  />
							<img src="images/banner5_new.png" data-thumb="images/banner5_new.png" alt=""  />
					<?php
						}
					?>
					</div>
				</div>
			</div>
		</div>
        </div>
		<div id="homemission" style="float:left; margin-bottom:12px; margin-top:12px; padding:26px;">
		  <p>Find and promote music, art and video on our ad-free platform. <a href="about.php" style="padding-left: 5px;">More ></a></p>
		</div>
		<?php
			include_once('fblog_reg.php');
			include_once('google_login_reg.php');
		?>
    </div>
 <div id="homeRightColumn" style="float: right; padding-right: 30px; width: 478px;">
	
		<!--<h1>Opens Jan 5, 2013!</h1>-->
    	<h1>Join for Free!</h1>

        <form name="form" method="post" action="general_registration.php" onsubmit="return checkForm();"> 
        	<div class="fieldCont">
            	<div class="fieldTitle">* First Name</div>
                <input type="text"  name="Fname" id="Fname" class="fieldText" <?php if(isset($_SESSION['login_email'])){ echo "disabled"; }?>/>
            </div> 
            <div class="fieldCont">
            	<div class="fieldTitle">* Last Name</div>
                <input type="text"  name="Lname" class="fieldText" <?php if(isset($_SESSION['login_email'])){ echo "disabled"; }?> />
            </div>
			<div class="fieldCont">
            	<div class="fieldTitle">Birth Date</div>
                <input type="text"  name="bday" id="bday" class="fieldText"  <?php if(isset($_SESSION['login_email'])){ echo "disabled"; }?>/>
            </div>
            <div class="fieldCont">
            	<div class="fieldTitle">* Your Email</div>
                <input type="text" value="<?php 
				if(isset($_GET['send_as']))
				{
					if($_GET['send_as']!="")
					{
						echo $_GET['send_as'];
					}
				}
				elseif(isset($_GET['downloads_thnxs']))
				{
					if($_GET['downloads_thnxs']!="")
					{
						echo $_GET['downloads_thnxs'];
					}
				}
				elseif(isset($_GET['thnxs_download']))
				{
					if($_GET['thnxs_download']!="")
					{
						echo $_GET['thnxs_download'];
					}
				}
				?>"  name="email" class="fieldText" id="email" onblur="return CheckEmail()" <?php
				if(isset($_SESSION['login_email']))
				{
					echo "disabled";
				} /* elseif(isset($_GET['thnxs_download'])) { if($_GET['thnxs_download']!="")
					{
						echo "disabled";
					}  } */
				 ?> />
				<input type="hidden"  name="emailerror" class="fieldText" id="emailerror" />
				<!--<div id="emailerror" style="color:#FF0000; font-size:10px; margin-left:25px; width:300px; float:right;"></div>-->
            </div>
            <div class="fieldCont">
            	<div class="fieldTitle">* Re - enter Email</div>
                <input type="text" value="<?php if(isset($_GET['send_as'])) { if($_GET['send_as']!="") { echo $_GET['send_as']; } } elseif(isset($_GET['downloads_thnxs']))
				{
					if($_GET['downloads_thnxs']!="")
					{
						echo $_GET['downloads_thnxs'];
					}
				}
				elseif(isset($_GET['thnxs_download']))
				{
					if($_GET['thnxs_download']!="")
					{
						echo $_GET['thnxs_download'];
					}
				}
				?>" name="remail" class="fieldText" <?php if(isset($_SESSION['login_email'])){ echo "disabled"; }/*  elseif(isset($_GET['thnxs_download'])) { if($_GET['thnxs_download']!="")
					{
						echo "disabled";
					}  } */ ?> />
            </div> 
			<input type="hidden" id="download_meds_link" name="download_meds_link" value="<?php if(isset($_GET['thnxs_download']))
				{
					if($_GET['thnxs_download']!="")
					{
						echo "real";
					}
				}else { echo "fake"; } ?>" />
            <div class="fieldCont">
            	<div class="fieldTitle">* Password</div>
                <input type="password"  name="pass" class="fieldText" <?php if(isset($_SESSION['login_email'])){ echo "disabled"; }?> />
            </div> 
            <div class="fieldCont">
            	<div class="fieldTitle">* Confirm Password</div>
                <input type="password"  name="rpass" class="fieldText" <?php if(isset($_SESSION['login_email'])){ echo "disabled"; }?> />
            </div> 
		  <div class="fieldCont">
		  	<div class="chkCont">
            	<input type="checkbox" name="terms" class="chk" <?php if(isset($_SESSION['login_email'])){ echo "disabled"; }?> />
				<div class="chkTitle">I accept the <a href="about_agreements_user.php" target="_blank">Terms</a> &amp; <a href="about_agreements_privacy.php" target="_blank">Privacy</a> Agreements</div>
            </div>	 
			</div>
			<div class="fieldCont">
				<div class="regiCont">
                <input type="submit"  name="submit" class="register" value="Submit" <?php if(isset($_SESSION['login_email'])){ echo "disabled"; }?> />
                <!--<input type="reset" name="reset" class="register" value="Reset"/>-->
				</div>
            </div>
      	</form>
	</div>
    <div class="clearMe"></div>
    
  </div>
<?php include_once('displayfooter.php'); ?>