<?php
//ini_set('max_execution_time', 10000); //5 minutes
//phpinfo();
//die;
session_start();
//echo $_SESSION['audio_file_name'];
	include("commons/db.php");
	include_once('sendgrid/SendGrid_loader.php');
	include_once('classes/Commontabs.php');
	include_once('classes/UnregUser.php');
	include_once('classes/Artist.php');
	include_once('classes/UserType.php');
	include_once('classes/UserImg.php');
	include_once('classes/UserSubscription.php');
	include_once('classes/RegistrationEmail.php');
	include_once('classes/NotifyAdminEmail.php');
	//include_once('login_includes.php');
	
	include("classes/AddMedia.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "https://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="https://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<meta https-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Purify Art</title>

<!--<link rel="stylesheet" type="text/css" media="screen" href="includes/purify.css" />

<link rel="stylesheet" type="text/css" media="screen" href="includes/purify.css" />
<script src='https://ajax.googleapis.com/ajax/libs/jquery/1.6.1/jquery.min.js'></script>
<script src="includes/mediaorganictabs-jquery.js"></script>
<link rel="stylesheet" type="text/css" media="screen" href="includes/multiple_select.css"/>

<script type="text/javascript" src="uploadify/swfobject.js"></script>
<script type="text/javascript" src="uploadify/jquery.uploadify.v2.1.4.min.js"></script>
<link href="uploadify/uploadify.css" rel="stylesheet"/>-->

<?php
$newtab=new Commontabs();
include("header.php");
?>
<div id="outerContainer">

      <!--  <p>
          <a href="logout.php">Logout</a></p>
      </div>
    </div>
  </div>
 </div>-->
 <script>
	$(function() {

		//$("#mediaTab").organicTabs();
	});
	
	$(function()
{
	var config = {
		toolbar:
		[
			['Bold', 'Italic', '-', 'NumberedList', 'BulletedList', '-', 'Link', 'Unlink'],
			['UIColor']
		]
	};

	// Initialize the editor.
	// Callback function can be passed and executed after full instance creation.
	$('.jquery_ckeditor').ckeditor(config);
	config.forcePasteAsPlainText = true;
});
</script>

<script type="text/javascript">

function handleSelection(choice) {
//alert(choice);
//document.getElementById('select').disabled=true;
if(choice==113)
	{
	  document.getElementById(choice).style.display="block";
	  document.getElementById("type_association").style.display="block";
	  document.getElementById('nodisplay').style.display="none";
	}
	else
	{
	  document.getElementById(choice).style.display="block";
	  document.getElementById("type_association").style.display="block";
	  document.getElementById('113').style.display="none";
	}

if(choice==114)
	{
	  document.getElementById(choice).style.display="block";
	  document.getElementById("type_association").style.display="block";
	  document.getElementById('nodisplay').style.display="none";
	}
	else
	{
	  document.getElementById(choice).style.display="block";
	  document.getElementById("type_association").style.display="block";
	  document.getElementById('114').style.display="none";
	}

if(choice==115)
	{
	  document.getElementById(choice).style.display="block";
	  document.getElementById("type_association").style.display="block";
	  document.getElementById('nodisplay').style.display="none";
	}
	else
	{
	  document.getElementById(choice).style.display="block";
	  document.getElementById("type_association").style.display="block";
	  document.getElementById('115').style.display="none";
	}

if(choice==116)
	{
	  document.getElementById(choice).style.display="block";
	  document.getElementById("type_association").style.display="block";
	  document.getElementById('nodisplay').style.display="none";
	  $("#chk_terms").css({"display":"none"});
	}
	else
	{
	  document.getElementById(choice).style.display="block";
	  document.getElementById("type_association").style.display="block";
	  document.getElementById('116').style.display="none";
	  $("#chk_terms").css({"display":"block"});
	}

/*if(choice=='book')
	{
	  document.getElementById(choice).style.display="block";
	  document.getElementById('nodisplay').style.display="none";
	}
	else
	{
	  document.getElementById(choice).style.display="block";
	  document.getElementById('book').style.display="none";
	}
*/
}
/*$(document).ready(function(){
	$("#addimage").change(function(){
		if($("#addimage").val()=="multiple")
		{
			var di = document.getElementById("imgUploadDiv");
			di.style.display="block";
			var a =document.getElementById("singleimageupload");
			a.style.display="none";
		}
		if($("#addimage").val()=="one")
		{
			var di = document.getElementById("imgUploadDiv");
			di.style.display="none";
			var a =document.getElementById("singleimageupload");
			a.style.display="block";
		}
	});
});*/

function addtags()
{
	// Retrieve the elements from the document body
	var textbox = document.getElementById('taguser');
	var olds = $("#useremail_old");
	if(textbox.value=="")
	{
		alert("Please Enter Some Text");
		return false;
	}
	
	var listbox = document.getElementById('taggedusers[]');
	var rmv_button = document.getElementById("remove");
	var options = document.getElementById('taggedusers[]').options;
	var optiontext,text;
	for (i=0; i<options.length; i++)
	{
	//	optiontext=options[i].value.toLowerCase();
		if(options[i].value.toLowerCase()==textbox.value.toLowerCase())
		{
			alert(options[i].value + "  Already Exist.");
			return false;
		}
	}
	
	if(olds.val() != '' && olds.val() != ',')
	{
		document.getElementById("useremail").value = document.getElementById("useremail").value +","+ olds.val();
		document.getElementById("useremail_old").value="";
	}
	else
	{
		document.getElementById("useremail").value = document.getElementById("useremail").value;
		document.getElementById("useremail_old").value="";
	}
	// Now we need to create a new 'option' tag to add to MyListbox
	var newOption = document.createElement('option');
	newOption.value = textbox.value; // The value that this option will have
	newOption.innerHTML = textbox.value; // The displayed text inside of the <option> tags
	// Finally, add the new option to the listbox
	listbox.appendChild(newOption);
	textbox.value="";
	rmv_button.style.display="block";
}
function remove_tags()
{
	var listbox = document.getElementById('taggedusers[]');
    //iterate through each option of the listbox
    for(var count= listbox.options.length-1; count >= 0; count--)
	{
		var a = document.getElementById('useremail').value;
		var aexp = a.split(',');
         //if the option is selected, delete the option
        if(listbox.options[count].selected == true)
		{
            listbox.remove(count);
			var aexp2 = a.split(',');
			if(aexp2[count + 1] == aexp[count + 1])
			{
				var rem_email = document.getElementById('useremail_removed').value;
				if(rem_email != "")
				{
					var rem_email_split = rem_email.split(',');
					for(var rem_count=0;rem_count<rem_email_split.length;rem_count++)
					{
						if(rem_email_split[rem_count]==""){continue;}
						else{
							if(rem_email_split[rem_count] !=aexp2[count + 1])
							{
								document.getElementById('useremail_removed').value = document.getElementById('useremail_removed').value + "," + aexp2[count + 1];
							}
						}
					}
				}
				else
				{
					document.getElementById('useremail_removed').value = document.getElementById('useremail_removed').value + "," + aexp2[count + 1];
				}
				aexp2[count + 1] = "";
				document.getElementById('useremail').value = "";
				for(var xz=0;xz< aexp2.length;xz++ )
				{
					if(aexp2[xz] != "")
					{
						document.getElementById('useremail').value = document.getElementById('useremail').value + "," + aexp2[xz];
					}
				}
			}
        }
    }
	if(listbox.options.length==0 || listbox.options.length== "")
	{
		var rmv_button = document.getElementById("remove");
		rmv_button.style.display="none";
	}
}
function validate()
{
	/*var a = document.getElementById("selected-types");
	if(a.innerHTML =="" || a.innerHTML ==null)
	{
		alert("You must have at least one type selected.");
		return false;
	}*/
	var a = document.getElementById("selected-types");
	var sel = document.getElementById("type-select");
	var price = document.getElementById("songprice").value;
	var privacy_chk = document.getElementById("chk_privacy").checked;
	var title;
	if(document.getElementById("channeltitle")!=null){
		var chan = document.getElementById("channeltitle");
		if(chan.value !=" " && chan.value!=""){
		title = chan.value;}
	}if(document.getElementById("galleryFileName")!=null){
		var gal = document.getElementById("galleryFileName");
		if(gal.value !=" " && gal.value!=""){
		title = gal.value;}
	}if(document.getElementById("audioFileName")!=null){
		var audio1 = document.getElementById("audioFileName");
		if(audio1.value !=" " && audio1.value!=""){
		title = audio1.value;}
	}if(document.getElementById("audioLinkName")!=null){
		var audio2 = document.getElementById("audioLinkName");
		if(audio2.value !=" " && audio2.value!=""){
		title = audio2.value;}
	}if(document.getElementById("videoFileName")!=null){
		var video1 = document.getElementById("videoFileName");
		if(video1.value !=" " && video1.value!=""){
		title = video1.value;}
	}if(document.getElementById("videoLinkName")!=null){
		var video2 = document.getElementById("videoLinkName");
		if(video2.value !=" " && video2.value!=""){
		title = video2.value;}
	}
	if($('#upload_song_link').is(':checked')) { 
		if(document.getElementById("audioLinkUrl")!=null){
			var audiourl = document.getElementById("audioLinkUrl");
			if(audiourl.value ==" " || audiourl.value==""){
			alert("Please enter sound cloud URL.");
			return false;
			}
		}
	}
	if($('#upload_video_link').is(':checked')) { 
		if(document.getElementById("videoLinkUrl")!=null){
			var audiourl = document.getElementById("videoLinkUrl");
			if(audiourl.value ==" " || audiourl.value==""){
			alert("Please enter you tube video URL.");
			return false;
			}
		}
	}
	
	if($("#type-select").val()==114 && $("#sharingpreference").val()==5){
		if(price =="" || price ==null){
			alert("Please Enter Price.");
			return false;
		}
	}
	if(sel.value == 0){
		alert("Please select any media type.");
		return false;}
	
	else if(title =="" || title ==null){
		alert("Please Enter Title.");
		return false;
	}
	else if(a.innerHTML =="" || a.innerHTML ==null){
		alert("Please Select at least one sub type and click the add button.");
		return false;
	}	
	
	if(privacy_chk==false && sel.value!=116)
	{
		alert("You need to check the box showing you have read the Terms of Use and Intellectual Property Policy.");
		return false;
	}
}

function know_member_or_not(type)
{
	//alert("For Uploading Media More Than <?php echo $limit ;?> MB You Have To Be A Member Of Purify");
	if($("#sharingpreference").val() != 5){
	alert("You have reached your storage limit. To upload more media you can do any of the following: add an Amazon S3 account, delete files, add youtube/soundcloud links, or upload songs for sale (songs for sale do not apply toward your storage limit).");
	}
	if(type == "song")
	{				
		document.getElementById("upload_song_link1").checked = false;
	}
	if(type == "gallery")
	{			
		document.getElementById('uploadtopurifyno').checked=false;
	}
	if(type == "video")
	{				
		//document.getElementById("upload_video_link").click();
	}
	return false;
}

<?php
if(isset($_GET['edit']))
{
?>
$("document").ready(function(){
	var rmv_button = document.getElementById("remove");
	rmv_button.style.display="block";
});
<?php
}
?>
/*Script For Detecting Flash*/
$("document").ready(function(){
	var flashEnabled = !!(navigator.mimeTypes["application/x-shockwave-flash"] || window.ActiveXObject && new ActiveXObject('ShockwaveFlash.ShockwaveFlash'));
	if (!flashEnabled) { 
	$("#Flashnotfound").css({"display":"block","color":"red","float":"right","width":"300px","position":"relative","right":"84px"});
	$("#Flashnotfound_song").css({"display":"block","color":"red","float":"right","width":"300px","position":"relative","right":"84px"});
	$("#Flashnotfound_video").css({"display":"block","color":"red","float":"right","width":"300px","position":"relative","right":"84px"});
	}
});
/*Script For Detecting Flash Ends Here*/
</script>

<?php
$newclassobj=new AddMedia();
$getgallery=$newclassobj->get_user_gallery();
$getall_channel_song=$newclassobj->Get_all_my_song();
$getall_channel_video=$newclassobj->Get_all_my_video();
$find_member =$newclassobj->Find_member();

$get_S3_account =$newclassobj->getS3_account();

/*Code For Finding The max Upload Size*/

$Get_the_upload_limit =$newclassobj->get_media_storage();
//var_dump($Get_the_upload_limit);
if(!empty($Get_the_upload_limit) && $Get_the_upload_limit=="s3integrated"){
	$final_value = 0;
}else if(!empty($Get_the_upload_limit) && $Get_the_upload_limit=="adminset"){
	$final_value = 0;
}else{
	$Get_the_upload_val = strpos($Get_the_upload_limit,".");
	$Get_the_upload_val = $Get_the_upload_val + 2 ;
	$final_value = substr($Get_the_upload_limit,0,$Get_the_upload_val);
}

//echo $final_value =110;

/*Code For Finding The max Upload Size End Here*/

//echo mysql_num_rows($find_member);
/*if(mysql_num_rows($find_member) == 0)
{
	$get_size_gallery=$newclassobj->Get_size_all_media();
	$get_gene_info=$newclassobj->sel_general();

	if($get_gene_info['artist_id'] > 0)
	{
		$get_status_art=$newclassobj->Get_status_for_size("artist",$get_gene_info['artist_id']);
		if($get_status_art['status'] == 0)
		{
			//echo "hi";
			$get_art_regis_info=$newclassobj->Get_art_regis_info($get_gene_info['artist_id']);
			$limit =200;
		}
	}
	if($get_gene_info['community_id'] > 0)
	{
		$get_status_comm=$newclassobj->Get_status_for_size("community",$get_gene_info['community_id']);
		if($get_status_comm['status'] == 0)
		{
			//echo "</br>hicomm</br>";
			$get_comm_regis_info=$newclassobj->Get_comm_regis_info($get_gene_info['community_id']);
			if(isset($limit) && $limit !="")
			{
				$limit = $limit + $limit;
			}
			else
			{
				$limit = 200;
			}
		}
	}


	//echo $limit;
	while($res_row = mysql_fetch_assoc($get_size_gallery))
	{
		//var_dump($res_row)."</br>";
		if($res_row['media_type']==113)
		{
			$table_name = "media_images";
		}
		if($res_row['media_type']==114)
		{
			$table_name = "media_songs";
		}
		if($res_row['media_type']==115)
		{
			$table_name = "media_video";
		}
		$get_all_size =$newclassobj->Get_media($table_name,$res_row['id']);
		$total_size = $get_all_size + $total_size;
	}
	$total_size = $total_size + $get_art_regis_info + $get_comm_regis_info;
	var_dump($total_size)	;

	//if($total_size > $limit)
	//{
	?>
		
	<?php
	//}
}*/

//die;
$gettype=$newclassobj->get_media_types();

$get_artist_member=$newclassobj->Get_artist_member_Id();
$get_comm_member=$newclassobj->Get_comm_member_Id();
//var_dump($get_comm_member);
//var_dump($get_artist_member);


if(isset($_GET['edit']) && $_GET['edit']>0)
{
	$id=$_GET['edit'];
	$editmedia=$newclassobj->edit_media($id);

	//$tagged_user=explode(',',$editmedia['tagged_users']);
	$tagged_user=explode(',',$editmedia['tagged_user_email']);
	
	$get_edit_mediatype=$newclassobj->get_edit_media_type($id);

	if(isset($get_edit_mediatype['gallery_id']))
	{
		$get_media_gallery_name=$newclassobj->get_media_gallery_name($get_edit_mediatype['gallery_id']);
	}	
	$get_media_gallery_sale=$newclassobj->get_media_gallery_sales($id);
	
	$get_sales_song=$newclassobj->Get_song_sales($get_edit_mediatype['media_id']);

	
	$get_sales_video=$newclassobj->Get_video_sales($get_edit_mediatype['media_id']);

$getmeta=$newclassobj->sel_media_profile($id);

$arr=Array();
while($gmeta=mysql_fetch_assoc($getmeta))	
{
	//var_dump($gmeta);
	$arr[]=$gmeta;
}
//echo $editmedia['media_type'];
?>	
	<script type="text/javascript">
		$("document").ready(function(){
			handleSelection("<?php echo $editmedia['media_type'];?>");
			$("#type-select").change();
			if(document.getElementById("type-select").value==116)
			{
				$("#chk_terms").css({"display":"none"});
			}
		});
	</script>
<?php	
}

?>

<?php	
if(isset($_GET['edit']) && $_GET['edit']>0)
{
	//if($editmedia['media_type']==116)
	//{
	?>
	<script type="text/javascript">
$("document").ready(function(){
<?php 
for($i=0;$i<count($arr);$i++)
{
?>
if(!$("div").hasClass(<?php echo $arr[$i]['type_id'];?>))
{
	<?php $qu = mysql_query("select name from type where type_id='".$arr[$i]['type_id']."'");
	$name=mysql_fetch_array($qu);
	//$name=$newgeneral->seltype_name($arr);
	if($name[0]!="")
	{
	?>
		$("#selected-types").append('<div class="'
		+<?php echo $arr[$i]['type_id'];?>+' box1" id="'
		+<?php echo $arr[$i]['type_id'];?>+'">'
		+'<input type="checkbox" name="type_array['
		+<?php echo $arr[$i]['type_id'];?>+']" value="'
		+<?php echo $arr[$i]['type_id'];?>
		+'" checked onclick="$(this).parent().remove()" />'
		+"<?php echo $name[0];
		//echo $arr[$i]['type_id'];
		?>"
		+'<input type="hidden" name="typeValschannel[]" value="'
		+<?php echo $arr[$i]['type_id'];?>+'_0_0"/></div>');
	<?php
	}
	?>
}

if(!$("div").hasClass(<?php echo $arr[$i]['type_id'];?>+'_'+<?php echo $arr[$i]['subtype_id'];?>))
{
	<?php $qu = mysql_query("select name from subtype where subtype_id='".$arr[$i]['subtype_id']."'");
	$name=mysql_fetch_array($qu);
	if($name[0]!="")
	{
	?>
		 $("."+<?php echo $arr[$i]['type_id'];?>).append('<div id="'
		 +<?php echo $arr[$i]['subtype_id'];?>+'" class="'
		 +<?php echo $arr[$i]['type_id'];?>+'_'+<?php echo $arr[$i]['subtype_id'];?>
		 + ' box1A"><input type="checkbox" name="type_array['+<?php echo $arr[$i]['type_id'];?>
		 +']['+<?php echo $arr[$i]['subtype_id'];?>
		 +']" value="'+$("#showtype-select").val()
		 +'_'+<?php echo $arr[$i]['subtype_id'];?>
		 + '" checked  onclick="$(this).parent().remove()"/>'
		 +"<?php echo $name[0];
			//echo $arr[$i]['subtype_id'];
			?>"
		 +'<br>'+'<input type="hidden" name="subTypeValschannel[]" value="'
		 +<?php echo $arr[$i]['type_id'];?>+'_'+<?php echo $arr[$i]['subtype_id'];?>+'_0"/></div>');
	<?php
	}
	?>
}

if(!$("div").hasClass(<?php echo $arr[$i]['type_id'];?>+'_'+<?php echo $arr[$i]['subtype_id'];?>+'_'+<?php echo $arr[$i]['metatype_id'];?>))
{
<?php $qu = mysql_query("select name from meta_type where meta_id='".$arr[$i]['metatype_id']."'");
	$name=mysql_fetch_array($qu);
	if($name[0]!=null)
	{
	?>
		$("."+<?php echo $arr[$i]['type_id'];?>+'_'
		+<?php echo $arr[$i]['subtype_id'];?>).append('<div class="'
		+<?php echo $arr[$i]['type_id'];?>+'_'+<?php echo $arr[$i]['subtype_id'];?>
		+'_'+<?php echo $arr[$i]['metatype_id'];?>
		+ ' box1B"><input type="checkbox" name="metatype_checked[]" value="'
		+<?php echo $arr[$i]['type_id'];?>+'_'+<?php echo $arr[$i]['subtype_id'];?>
		+'_'+<?php echo $arr[$i]['metatype_id'];?>
		+'" checked  onclick="$(this).parent().remove()"/>'
		+"<?php echo $name[0];
			//echo $arr[$i]['metatype_id'];
			?>"
		+'<input type="hidden" name="metaTypeValschannel[]" value="'
		+<?php echo $arr[$i]['type_id'];?>+'_'+<?php echo $arr[$i]['subtype_id'];?>
		+'_'+<?php echo $arr[$i]['metatype_id'];?>+'"/></div>');
	<?php
	}
	?>
}
<?php
}
?>
});
</script>	
	<?php
	//}
}
?>
<script type="text/javascript">
var final_value = '<?php echo $final_value;?>';
</script>
  <div id="toplevelNav"></div>
  <div>
      <div id="profileTabs">
          <ul>
             <?php
		   //$check=new AddContact();
		   $new_artist=$newtab->artist_status();
		   $new_community=$newtab->community_status();
		   $new_team=$newtab->team_status();
		    $gen_det = mysql_query("SELECT * FROM general_user WHERE email='".$_SESSION['login_email']."'");
		   if(mysql_num_rows($gen_det)>0){
			$res_gen_det = mysql_fetch_assoc($gen_det);
		   }
		    $date = date("Y-m-d");
			$sql_member_chk = mysql_query("SELECT * FROM purify_membership WHERE general_user_id='".$res_gen_det['general_user_id']."' AND (expiry_date>'".$date."' OR lifetime=1)");
		   if(mysql_num_rows($sql_member_chk)>0)
		   {
				$member_var = $member_var + 1;
		   }
		   
		   if(($newres1['artist_id']==0 && $new_artist['status']==1) && $newres1['member_id']==0 && ($newres1['community_id']==0 && $new_community['status']==1) && ($newres1['team_id']==0 && $new_team['status']==1))
		  {		  
		  ?>
            <li class="active">PERSONAL</li>		
            <!--<li><a href="profileedit_support.php">SUPPORT</a></li>-->
		   <?php
		   }
		    elseif(($newres1['artist_id']==0 && ($new_artist['status']!=0 ||  $new_artist['status']=="")) && ($newres1['community_id']==0 && ($new_community['status']!=0 || $new_community['status']=="")) && ($newres1['team_id']==0 && ($new_team['status']!=0 || $new_team['status']=="")) && $member_var!=0)
		   {
		   ?>
			<li><a href="profileedit.php">PERSONAL</a></li>
			<li class="active">Media</li>
            <!--<li><a href="profileedit_support.php">SUPPORT</a></li>-->
		   <?php
		   }
		   elseif(($newres1['artist_id']!=0 && $new_artist['status']==0 && $new_artist['active']==0) && ($newres1['community_id']!=0 && $new_community['status']==0 && $new_community['active']==0) && ($newres1['team_id']!=0 && $new_team['status']==0))
		   {
		   ?>
		   <li><a href="profileedit.php">PERSONAL</a></li>	
		   <li class="active">Media</a></li>
		   <li><a href="profileedit_artist.php">ARTIST</a></li>
		   <!--<li><a href="profileedit_community.php">COMMUNITY</a></li>-->
		   <li><a href="profileedit_community.php">COMMUNITY</a></li>
		   <li><a href="profileedit_team.php">TEAM</a></li>
          <!--<li><a href="profileedit_support.php">SUPPORT</a></li>-->
		   
		   <?php
		   }
		   elseif(($newres1['artist_id']!=0 && $new_artist['status']==0 && $new_artist['active']==0) && ($newres1['community_id']!=0 && $new_community['status']==0 && $new_community['active']==0))
		   {
		   ?>
		   <li><a href="profileedit.php">PERSONAL</a></li>	
		   <li class="active">Media</a></li>		   
		   <li><a href="profileedit_artist.php">ARTIST</a></li>
		   <!--<li><a href="profileedit_community.php">COMMUNITY</a></li>-->
		   <li><a href="profileedit_community.php">COMMUNITY</a></li>
           <!--<li><a href="profileedit_support.php">SUPPORT</a></li>-->
		   <?php
		   }
		   elseif(($newres1['artist_id']!=0 && $new_artist['status']==0 && $new_artist['active']==0) && ($newres1['team_id']!=0 && $new_team['status']==0))
		   {
		   ?>
			<li><a href="profileedit.php">PERSONAL</a></li>	
		   <li class="active">Media</a></li>
		    <li><a href="profileedit_artist.php">ARTIST</a></li>
			<li><a href="profileedit_team.php">TEAM</a></li>
            <!--<li><a href="profileedit_support.php">SUPPORT</a></li>-->
		   <?php
		   }
		   elseif(($newres1['community_id']!=0 && $new_community['status']==0 && $new_community['active']==0) && ($newres1['team_id']!=0 && $new_team['status']==0))
		   {
		   ?>
		   <li><a href="profileedit.php">PERSONAL</a></li>	
		   <li class="active">Media</a></li>
		   <!--<li><a href="profileedit_community.php">COMMUNITY</a></li>-->
		   <li><a href="profileedit_community.php">COMMUNITY</a></li>
		   <li><a href="profileedit_team.php">TEAM</a></li>
          <!--<li><a href="profileedit_support.php">SUPPORT</a></li>-->
		   <?php
		   }
		   elseif($newres1['artist_id']!=0 && $new_artist['status']==0 && $new_artist['active']==0) 
		   {?>
		    <li><a href="profileedit.php">PERSONAL</a></li>	
		   <li class="active">Media</a></li>	
			<li><a href="profileedit_artist.php">ARTIST</a></li>
            <!--<li><a href="profileedit_support.php">SUPPORT</a></li>-->
		   <?php
		   }
		   elseif($newres1['member_id']!=0)
		   {
		   ?>
		   <?php
		   }
		   elseif($newres1['community_id']!=0 && $new_community['status']==0 && $new_community['active']==0)
		   {
		   ?>
		    <li><a href="profileedit.php">PERSONAL</a></li>	
		   <li class="active">Media</a></li>
			<!--<li>COMMUNITY</a></li>-->
			<li><a href="profileedit_community.php">COMMUNITY</a></li>
            <!--<li><a href="profileedit_support.php">SUPPORT</a></li>-->
		   <?php
		   }
		   elseif($newres1['team_id']!=0 && $new_team['status']==0)
		   {
		   ?>
		    <li><a href="profileedit.php">PERSONAL</a></li>	
		   <li class="active">Media</a></li>
			<li><a href="profileedit_team.php">TEAM</a></li>
            <!--<li><a href="profileedit_support.php">SUPPORT</a></li>-->
`		   <?php
		   }
		   else
		   {
		   ?>
		  <li class="active">PERSONAL</li>		
            <!--<li><a href="profileedit_support.php">SUPPORT</a></li>-->
		   <?php
		   }
			?>
          </ul>
      </div>
	<!-- Files adde for fancy box2-->
<script type="text/javascript" src="new_fancy_box/jquery.fancybox.js?v=2.1.4"></script>
<link rel="stylesheet" type="text/css" href="new_fancy_box/jquery.fancybox.css?v=2.1.4" media="screen" />
<script type="text/javascript" src="new_fancy_box/helpers/jquery.fancybox-media.js?v=1.0.5"></script>
<!-- Files adde for fancy box2 ends here-->
 	<!--<script type="text/javascript" src="./fancybox/jquery.fancybox-1.3.4.pack.js"></script>
	<link rel="stylesheet" type="text/css" href="./fancybox/jquery.fancybox-1.3.4.css" media="screen" />-->
<script type="text/javascript">
//!window.jQuery && document.write('<script src="./fancybox/jquery-1.4.3.min.js"><\/script>');

          
	$(document).ready(function() {
				$("#popup_gallery").fancybox({
				'width'				: '35%',
				'height'			: '30%',
				'autoScale'			: true,
				'transitionIn'		: 'none',
				'transitionOut'		: 'none',
				'type'				: 'iframe'
			});
		});
		
	$(document).ready(function() {
				$("#popup_song").fancybox({
				'width'				: '35%',
				'height'			: '30%',
				'autoScale'			: true,
				'transitionIn'		: 'none',
				'transitionOut'		: 'none',
				'type'				: 'iframe'
			});
		});
		
	$(document).ready(function() {
				$("#popup_video").fancybox({
				'width'				: '35%',
				'height'			: '30%',
				'autoScale'			: true,
				'transitionIn'		: 'none',
				'transitionOut'		: 'none',
				'type'				: 'iframe'
			});
		});
		$(document).ready(function() {
				$("#popup_s3_error").fancybox({
				'width'				: '75%',
				'height'			: '70%',
				'autoScale'			: true,
				'transitionIn'		: 'none',
				'transitionOut'		: 'none',
				'type'				: 'iframe'
			});
		});
	// $(document).ready(function() {
				// $("#take_S3_access_key").fancybox({
				// 'autoScale'			: true,
				// 'transitionIn'		: 'none',
				// 'transitionOut'		: 'none',
				// 'type'				: 'iframe'
			// });
		// });


function agreement_gallery()
{
	var link =document.getElementById("popup_gallery");
	link.click();
}

function agreement_song()
{
	var link =document.getElementById("popup_song");
	link.click();
}

function agreement_video()
{
	var link =document.getElementById("popup_video");
	link.click();
}

// function take_S3_access()
// {
	// var link =document.getElementById("take_S3_access_key");
	// link.click();
// }
function submit_form()
{
	var a = document.getElementById("selected-types");
	var sel = document.getElementById("type-select");
	var price = document.getElementById("songprice").value;
	var title = "";
	if(document.getElementById("channeltitle")!=null){
		var chan = document.getElementById("channeltitle");
		if(chan.value !=" " && chan.value!=""){
		title = chan.value;}
	}if(document.getElementById("galleryFileName")!=null){
		var gal = document.getElementById("galleryFileName");
		if(gal.value !=" " && gal.value!=""){
		title = gal.value;}
	}if(document.getElementById("audioFileName")!=null){
		var audio1 = document.getElementById("audioFileName");
		if(audio1.value !=" " && audio1.value!=""){
		title = audio1.value;}
	}if(document.getElementById("audioLinkName")!=null){
		var audio2 = document.getElementById("audioLinkName");
		if(audio2.value !=" " && audio2.value!=""){
		title = audio2.value;}
		
	}if(document.getElementById("videoFileName")!=null){
		var video1 = document.getElementById("videoFileName");
		if(video1.value !=" " && video1.value!=""){
		title = video1.value;}
	}if(document.getElementById("videoLinkName")!=null){
		var video2 = document.getElementById("videoLinkName");
		if(video2.value !=" " && video2.value!=""){
		title = video2.value;}
	}
	
	
	
	if($("#type-select").val()==114 && $("#sharingpreference").val()==5){
		if(price =="" || price ==null){
			alert("Please Enter Price.");
			return false;
		}
		else if(price==0)
		{
			alert('You can not have a price of $0, if you would like to offer this media for free than change the sharing preference to "Share with all users for streaming and downloading".');
			return false;
		}
	}
	if($('#upload_song_link').is(':checked')) {
		if(document.getElementById("audioLinkUrl")!=null){
			var audiourl = document.getElementById("audioLinkUrl");
			if(audiourl.value ==" " || audiourl.value==""){
			alert("Please enter sound cloud URL.")
			return false;
			}
		}
	}
	
	if($('#upload_video_link').is(':checked')) {
		if(document.getElementById("videoLinkUrl")!=null){
			var audiourl = document.getElementById("videoLinkUrl");
			if(audiourl.value ==" " || audiourl.value==""){
			alert("Please enter you tube video URL.")
			return false;
			}
		}
	}
	
	if(sel.value == 0){
		alert("Please select any media type.");
		return false;}
	else if(title =="" || title ==null){
		alert("Please Enter Title.");
		return false;
	}
	
	else if(a.innerHTML =="" || a.innerHTML ==null){
		alert("Please Select at least one sub type and click the add button.");
		return false;
	}else{
		document.getElementById("media_form").submit();
	}
}
</script>
 
          <div id="mediaTab">
            <div id="subNavigation" class="tabs">
              <ul class="nav">
                <li><a href="profileedit_media.php" class="current">Media Library</a></li>
				<li><a href="profileedit_media.php#s3account" class="current">S3 Account</a></li>
                <!--<li><a href="profileedit_media.php#storage">Storage</a></li>-->
				<!--<li><a href="profileedit_media.php#s3">S3 Library</a></li>-->
				<?php
				if(($newres1['artist_id']!=0 && $new_artist['status']==0 && $new_artist['active']==0) || ($newres1['community_id']!=0 && $new_community['status']==0 && $new_community['active']==0))
				{
				?>
				<li><a href="tagged_user.php">Tagged Media</a></li>
				<?php
				}
				?>
              </ul>
            </div>
            <div class="list-wrap">
				<div class="subTabs" id="library">
					<h1><?php if(isset($_GET['edit'])){echo "Edit".' "'.$editmedia['title'].'"';}else { echo "Add New Media";}?></h1>
					<div id="mediaContent">
                    	<div style="border-bottom:1px dotted #999;" class="topLinks">
                            <div style="float:left;" class="links">
								<ul>
									<li>
									<input type="button" id="save_form_top1" style="background-color: #000; border: 0 none; color: #FFFFFF; font-family: 'futura-pt',sans-serif; font-size: 0.825em; font-weight: bold; padding: 6px 10px; float:left;border-radius:5px;" onclick="submit_form()" value=" Save ">
									<input type="button" id="save_form_top2" style="display:none;background-color: #000; border: 0 none; color: #FFFFFF; font-family: 'futura-pt',sans-serif; font-size: 0.825em; font-weight: bold; padding: 6px 10px;border-radius:5px;float:left;" value=" Save ">
									</li>
								</ul>
							</div>
						</div>
					</div>
					<div style="width:665px; float:left;">
						<div id="actualContent">
						
						<div id="channel_pro_image" style="display:none;">
						<?php include('media_channel_jcrop/med_channel.php'); ?>
						</div>
						<!--<div id="gallery_pro_image" style="display:none;">
						<?php //include('media_channel_jcrop/med_gallery.php'); ?>
						</div>-->
						<?php 
						if(isset($_GET['edit']))
						{
						?>
							<form action="insertmedia.php?update=<?php echo $_GET['edit'];?>" id="media_form" method="POST" id="media_form" enctype="multipart/form-data" onsubmit="return validate()">
						<?php
						}
						else
						{
						?>
							<form action="insertmedia.php" method="POST" id="media_form" enctype="multipart/form-data" onsubmit="return validate()">
						<?php
						}
						?>
						
                        	<!--<div class="fieldCont">
                                <div class="fieldTitle">Title</div>
                                <input type="text" class="fieldText" name="title" id="title" value="<?php// if(isset($_GET['edit'])){echo $editmedia['title'];}?>" />
                            </div>-->
                        	
							<input type="text" id="med_channel_image" name="med_channel_image" value="<?php if(isset($_GET['edit'])){echo $get_edit_mediatype['profile_image'];}?>" style="display:none;"/>
							<input type="text" id="med_gallery_image" name="med_gallery_image" value="<?php if(isset($_GET['edit'])){echo $get_edit_mediatype['profile_image'];}?>" style="display:none;"/>
							<div class="fieldCont">
                            	<div title="Choose the type of media you would like to add." class="fieldTitle">*Media Type</div>
								<select title="Choose the type of media you would like to add." name="type-select" class="dropdown" id="type-select" onChange="handleSelection(value)">
									<option value="0" selected="selected">Select Type</option>
									<?php
									if(isset($_GET['edit']))
									{
										$arr =array();
										  $i=0;
										while($row=mysql_fetch_assoc($gettype))
										  {
											$arr[$i]=$row['type_id'];
												$i++;
												if($editmedia['media_type']==$row['type_id'])
												{
													if($editmedia['media_type']==116){
													?> 
													<option value="<?php echo $editmedia['media_type'];?>" selected chkval="<?php echo $row['name'];?>"><?php if(mysql_num_rows($find_member)==0){ echo $row['name']."(Members Only)";}else{ echo $row['name'];}?></option>
													<?php
													}else{
												?> 
													<option value="<?php echo $editmedia['media_type'];?>" selected chkval="<?php echo $row['name'];?>"><?php echo $row['name'];?></option>
												<?php
													}
												}
										  }
									}
									else
									{
										  $arr =array();
										  $i=0;
										  while($row=mysql_fetch_assoc($gettype))
										  {
												$arr[$i]=$row['type_id'];
												$i++;
												//var_dump($row);
												if($row['type_id']==117)
												{
													
												}
												elseif(($newres1['artist_id']==0 && ($new_artist['status']!=0 ||  $new_artist['status']=="")) && ($newres1['community_id']==0 && ($new_community['status']!=0 || $new_community['status']=="")) && ($newres1['team_id']==0 && ($new_team['status']!=0 || $new_team['status']=="")) && $member_var!=0)
												{
													if(mysql_num_rows($find_member) !=0 && $row['type_id']==116){
													?>
														<option value="<?php echo $row['type_id'];?>" chkval="<?php echo $row['name'];?>"><?php echo $row['name'];?></option>
													<?php
													}
												}
												else
												{
													
													if(mysql_num_rows($find_member) !=0 && $row['type_id']==116){
													?>
														<option value="<?php echo $row['type_id'];?>" chkval="<?php echo $row['name'];?>"><?php echo $row['name'];?></option>
													<?php
													}
													else{
														if($row['type_id']==116){
														?> 
														<option value="<?php echo $row['type_id'];?>" chkval="<?php echo $row['name'];?>" disabled><?php if(mysql_num_rows($find_member) ==0){ echo $row['name']."(Members Only)";}else{ echo $row['name'];}?></option>
														<?php
														}else{
													?> 
														<option value="<?php echo $row['type_id'];?>" chkval="<?php echo $row['name'];?>"><?php echo $row['name'];?></option>
												  <?php
														}
													}
												}
										  }
									 }
								  ?>
                              </select>

                               <!-- <select class="dropdown" id="select"  onChange="handleSelection(value)" id="mediatype" name="mediatype">
								<option  value="nodisplay">Please select one </option>
								<?php
								/*if(isset($_GET['edit']))
								{
									if($editmedia['media_type']=="channel")
									{
								?>
										<option value="channel" selected >Channel</option>
                                	<?php
									}
									elseif($editmedia['media_type']=="gallery")
									{
									?>
										<option value="gallery" selected >Gallery / Image</option>
                                    <?php
									}
									elseif($editmedia['media_type']=="song")
									{
									?>
										<option value="song" selected >Song</option>
                                    <?php
									}
									elseif($editmedia['media_type']=="video")
									{
									?>
                                    <option value="video" selected>Video</option>
								<?php
									}
								}
								else
								{
								?>
								 	<!--<option value="book">Book</option>-->
                                    <option value="channel">Channel</option>
                                    <option value="gallery">Gallery / Image</option>
                                    <option value="song">Song</option>
                                    <option value="video">Video</option>
								<?php
								}*/
								?>
                                </select>-->
                            </div>
							<div class="fieldCont" id="sharing_prefer_dd">
                              <div class="fieldTitle" title="Choose how you would like the media to be used on the site.">*Sharing Preference</div>
                                <select class="dropdown" title="Choose how you would like the media to be used on the site." id="sharingpreference" name="sharingpreference">
								
								<?php
								if(isset($_GET['edit']))
								{
								?>
									<option value="">Select from the list</option>
									<!--<option title="For your personal use only. May not be shared or sold." value="0" <?php //if($editmedia['sharing_preference']==0){ ?> selected <?php //} ?> >Personal use only</option>-->
                                    <option title="All users may stream media but not download. Cannot be sold." value="1" <?php if($editmedia['sharing_preference']==1){echo "selected";}?>>Share with all users for streaming</option>
									<?php
									if($editmedia['media_type']==115){
										$chk_sale = $newclassobj->get_media_video_info($editmedia['id']);
										if($chk_sale!=""){
										?>
										<option title="All users may stream and download media and will be asked to donate to you when downloading. Cannot be sold." value="2" <?php if($editmedia['sharing_preference']==2){echo "selected";}?>>Share with all users for streaming and downloading</option>
										<?php
										}
									}else if($editmedia['media_type']==114){
										$chk_sale_song = $newclassobj->get_media_song_info($editmedia['id']);
										if($chk_sale_song!=""){
										?>
										<option title="All users may stream and download media and will be asked to donate to you when downloading. Cannot be sold." value="2" <?php if($editmedia['sharing_preference']==2){echo "selected";}?>>Share with all users for streaming and downloading</option>
										<?php
										}
									}else{
									?>
                                    <option title="All users may stream and download media and will be asked to donate to you when downloading. Cannot be sold." value="2" <?php if($editmedia['sharing_preference']==2){echo "selected";}?>>Share with all users for streaming and downloading</option>
									<?php 
									}
									if(mysql_num_rows($find_member) !=0)
									{
									?>
										<!--<option value="3" <?php// if($editmedia['sharing_preference']==3){echo "selected";}?>>Share with fan club for streaming</option>
										<option value="4" <?php //if($editmedia['sharing_preference']==4){echo "selected";}?>>Share with fan club for streaming and downloading</option>-->
										<?php
										//if($editmedia['sharing_preference']==5){
										if($editmedia['media_type']==114){
											$chk_sale = $newclassobj->get_media_song_info($editmedia['id']);
											if($chk_sale!=""){
												?>
													<option title="Media can be sold and added to users media library or downloaded." value="5" <?php if($editmedia['sharing_preference']==5){echo "selected";}?>>For sale</option>
												<?php
											}/*else{
											?>
												<option title="Media can be sold and added to users media library or downloaded. Media cannot be streamed." value="5" >For sale</option>
											<?php
											}*/
										}
										else if($editmedia['media_type']==115){
											$chk_sale = $newclassobj->get_media_video_info($editmedia['id']);
											if($chk_sale!=""){
												?>
													<option title="Media can be sold and added to users media library or downloaded." value="5" <?php if($editmedia['sharing_preference']==5){echo "selected";}?>>For sale</option>
												<?php
											}
										}else if($editmedia['media_type']==113){
											$chk_sale = $newclassobj->get_media_gallery_info($editmedia['id']);
											if($chk_sale!=""){
												?>
													<option title="Media can be sold and added to users media library or downloaded." value="5" <?php if($editmedia['sharing_preference']==5){echo "selected";}?>>For sale</option>
												<?php
											}
										}
										//}
									}
								}
								else
								{
								?>
									<option value="">Select from the list</option>
                                    <!--<option title="For your personal use only. May not be shared or sold." value="0" >Personal use only</option>-->
                                    <option title="All users may stream media but not download. Cannot be sold." value="1">Share with all users for streaming</option>
                                    <option title="All users may stream and download media and will be asked to donate to you when downloading. Cannot be sold." value="2">Share with all users for streaming and downloading</option>
									<!--<option value="3">Share with fan club for streaming</option>
									<option value="4">Share with fan club for streaming and downloading</option>-->
									<option <?php if(mysql_num_rows($find_member)<=0){ echo "disabled";} ?> title="Media can be sold and added to users media library or downloaded." value="5"><?php if(mysql_num_rows($find_member)==0){ echo "For sale(Members Only)";} else{ echo "For sale";}?></option>
                                <?php
								}
								?>
								</select>
                            </div>
							<div id="mytext_tip" style="display:none;">
								<div id="mytext4">
									<div class="fieldCont">
										<div class="fieldTitle">Ask For Tip</div>
										<div class="chkCont">
											<?php
												if(isset($_GET['edit']))
												{
													if($editmedia['ask_tip']=='1')
													{
											?>
														<input title="" type="radio" name="free_tip" class="chk" value="free_tip_yes" checked id="free_tip_1" />
											<?php
													}
													else
													{
											?>
														<input title="" type="radio" name="free_tip" class="chk" value="free_tip_yes" id="free_tip_1" />
											<?php
													}
												}
												else
												{
											?>
													<input title="" type="radio" name="free_tip" class="chk" value="free_tip_yes" id="free_tip_1" />
											<?php
												}
											?>
											<div title="" class="radioTitle">Yes</div>
										</div>
										<div class="chkCont">
										<?php
												if(isset($_GET['edit']))
												{
													if($editmedia['ask_tip']=='0')
													{
										?>
														<input title="" type="radio" name="free_tip" class="chk" value="free_tip_no" checked id="free_tip_2"  />
										<?php
													}
													else
													{
										?>
														<input title="" type="radio" name="free_tip" class="chk" value="free_tip_no" id="free_tip_2"  />
										<?php
													}
												}
												else
												{
										?>
													<input title="" type="radio" name="free_tip" class="chk" value="free_tip_no" id="free_tip_2"  />
										<?php
												}
										?>
											<div title="" class="radioTitle">No</div>
										</div>
										<div class="hint" style="width:120px; margin-top:3px;">While being downloaded</div>
									</div>
								</div>
							</div>
							<div class="fieldCont" id="channel_title" style="display:none;">
                                <div class="fieldTitle">*Title</div>
                                <input type="text" class="fieldText" name="channeltitle" id="channeltitle" value="<?php if(isset($_GET['edit'])  && $editmedia['media_type']==116) { echo $editmedia['title']; } ?>" />
                            </div>
							<!-- File upload Field For Song -->
							<?php
							if(!isset($_GET['edit']))
							{
							?>
								<div class="fieldCont" id="audioUploadDiv" style="display:none;">
									<div class="fieldTitle" title="Choose how to add the media file.">File</div>
									<input type="radio" name="audiouploadoption" value="file" onclick="show_uploading_options()" id="upload_song_org" title="Upload to the Purify Server, size of file will go toward your account limit." /> <span title="Upload to the Purify Server, size of file will go toward your account limit.">Upload</span>
									<input type="radio" name="audiouploadoption" value="link" id="upload_song_link" title="Add a url for a Soundcloud file. You may get the url from the Soundcloud player on the Soundcloud site or embedded on other sites." /> <span title="Add a url for a Soundcloud file. You may get the url from the Soundcloud player on the Soundcloud site or embedded on other sites.">Soundcloud URL </span><br/>
								</div>
								<div class="fieldCont" id="select_upload_to" style="display:none;">
									<div class="fieldTitle"></div>
									<input type="radio" id="song_uploader" title="Upload to the Purify Server, size of file will go toward your account limit." name="audiouploadoption1" value="<?php echo "file"; ?>" onclick="<?php if($final_value > 100) {echo " return know_member_or_not('song')";}?>" ><span title="Upload to the Purify Server, size of file will go toward your account limit.">Upload to Purify</span>
									<!--<input type="radio" name="audiouploadoption1" value="file" id="upload_song_link1" title="Upload to your Amazon S3 server if you have set one up. There is no limit on your uploads or plays with this media and you will pay Amazon directly $.12/GB/month for storage and streaming." onclick="<?php /*if($get_S3_account=="blank"){ echo "find_s3_account('blank','song_uploader')"; }*/?>" /><span title="Upload to your Amazon S3 server if you have set one up. There is no limit on your uploads or plays with this media and you will pay Amazon directly $.12/GB/month for storage and streaming.">Upload to my S3 </span><br/>-->
								</div>
								<div style="display:none" id="sel_file">
									 <div class="fieldCont" >
									 <!--<h4 style= "margin:0;">Sale</h4>-->
										<div class="fieldTitle" title="Choose how to add the media file.">File</div>
										<div class="radioUp">
											<input type="radio" class="radio" name="song_select" title="Upload to the Purify Server, size of file will go toward your account limit." value="purify" id="songuploadtopurify" onclick="showsong_upload();" <?php if(isset($_GET['edit']) && $get_edit_mediatype['upload_to']=="purify"){echo "checked";} ?> />
											<div class="radioTitle" title="Upload to the Purify Server, size of file will go toward your account limit." >Upload to Purify</div>
										</div>
										<!--<div class="radioUp">
											<input type="radio" onclick="take_S3_access()" class="radio" name="song_select" value="s3" title="Upload to your Amazon S3 server if you have set one up. There is no limit on your uploads or plays with this media and you will pay Amazon directly $.12/GB/month for storage and streaming." id="songuploadtomys3" <?php /*if(isset($_GET['edit']) && $get_edit_mediatype['upload_to']=="s3"){echo "checked";} */?> />
											<div class="radioTitle" title="Upload to your Amazon S3 server if you have set one up. There is no limit on your uploads or plays with this media and you will pay Amazon directly $.12/GB/month for storage and streaming.">Upload to my S3</div>-->
											<!--<a href="uploadtomyS3.php" id="take_S3_access_key"></a>
										</div>-->
									</div>
									<!--<div class="fieldCont">
										<div class="fieldTitle">Title</div>
										<input type="text" class="fieldText" name="title" id="title" value="<?php //if(isset($_GET['edit'])){echo $editmedia['title'];}?>" />
									</div>-->
								</div>
								<div id="audiofileform" style="display:none;">
									<div class="fieldCont">
										<div class="fieldTitle">*Title</div>
										<input name="audioFileName" type="text" id="audioFileName" class="fieldText"/>
										<div id="Flashnotfound_song" style="display:none;">
											 Either your flash player plugin is disabled or if you don't have the flash player <a href="http://get.adobe.com/flashplayer/" target="_blank">Click Here</a> to install.
										</div>
										<div id="divAudioFileSize" style="display:none;"></div>
										<div class="hint" style="width:120px; margin-top:3px;">.mp3 files only</div>
									</div>
									<div class="fieldCont">
										<div class="fieldTitle"></div>
										<!--<form action="" methos="post" enctype="multipart/form-data">-->
										<input name="theFile" type="file" id="file_upload_audio" class="fieldText"/>
										<!--</form>-->
									</div>
								</div>
								
								<div id="audiolinkform" style="display:none;">
									<div class="fieldCont" >
										<div class="fieldTitle">*Title</div>
										<input name="audioLinkName" type="text" id="audioLinkName" class="fieldText"/>
									</div>
									<div class="fieldCont" >
										<div class="fieldTitle">*Link</div>
										<input name="audioLinkUrl" type="text" id="audioLinkUrl" class="fieldText" />
									</div>
								</div>
								<div class="fieldCont" id="audiotrackfield" style="display:none;">
									<div class="fieldTitle">Track #</div>
									<input type="hidden" class="fieldText"  id="songname" name="songname" />
									<input type="hidden" class="fieldText"  id="songsize" name="songsize" />
									<input type="text" class="fieldText" name="track" id="track" value="<?php if(isset($_GET['edit'])){echo $get_edit_mediatype['track'];}?>" />
								</div>
								<div style="display:none" id="sel_file">
									 <div class="fieldCont" >
									 <!--<h4 style= "margin:0;">Sale</h4>-->
										<div class="fieldTitle" title="Choose how to add the media file.">File</div>
										<div class="radioUp">
											<input type="radio" class="radio" title="Upload to the Purify Server, size of file will go toward your account limit." name="song_select" value="purify" id="songuploadtopurify" onclick="showsong_upload();" <?php if(isset($_GET['edit']) && $get_edit_mediatype['upload_to']=="purify"){echo "checked";} ?> />
											<div class="radioTitle" title="Upload to the Purify Server, size of file will go toward your account limit." >Upload</div>
										</div>
										<!--<div class="radioUp">
											<input type="radio" onclick="take_S3_access()" class="radio" name="song_select" value="s3" id="songuploadtomys3" <?php /*if(isset($_GET['edit']) && $get_edit_mediatype['upload_to']=="s3"){echo "checked";} */?> title="Upload to your Amazon S3 server if you have set one up. There is no limit on your uploads or plays with this media and you will pay Amazon directly $.12/GB/month for storage and streaming." />
											<div class="radioTitle" title="Upload to your Amazon S3 server if you have set one up. There is no limit on your uploads or plays with this media and you will pay Amazon directly $.12/GB/month for storage and streaming.">Upload to my S3</div>-->
											<!--<a href="uploadtomyS3.php" id="take_S3_access_key"></a>
										</div>-->
									</div>
									<div class="fieldCont">
										<div class="fieldTitle">*Title</div>
										<input type="text" class="fieldText" name="forsongtitle" id="forsongtitle" value="<?php //if(isset($_GET['edit'])){echo $editmedia['title'];}?>" />
									</div>
								</div>
								<?php
							}
							if(isset($_GET['edit']) && $editmedia['media_type']==114)
							{
								if($get_edit_mediatype['audiolinkname']=="" && $get_edit_mediatype['audiolinkurl']=="")
								{
							?>	
								 <div class="fieldCont">
									<input type="hidden" class="fieldText"  id="videoname" name="videoname" />
									<div class="fieldTitle">*Title</div>
									<input type="text" id="audioFileName" name="song_up_title" class="fieldText" value="<?php echo $editmedia['title']; ?>"/>
									<div class="hint" style="width:120px; margin-top:3px;">.mp3 files only</div>
								</div>
								<!--<div class="fieldCont">
									<input type="hidden" class="fieldText"  id="videoname" name="videoname" />
									<div class="fieldTitle"> Name</div>
									<input type="text" disabled class="fieldText" value="<?php// echo $get_edit_mediatype['song_name']; ?>"/>
								</div>-->
							<?php
								}
								else
								{
								?>
								<div class="fieldCont">
									<input type="hidden" class="fieldText"  id="videoname" name="videoname" />
									<div class="fieldTitle">*Title</div>
									<input type="text" id="audioLinkName" name="songlink_title" class="fieldText" value="<?php echo $editmedia['title']; ?>"/>
								</div>
								<!--<div class="fieldCont">
									<input type="hidden" class="fieldText"  id="videoname" name="videoname" />
									<div class="fieldTitle"> Link</div>
									<input type="text" disabled class="fieldText" value="<?php //echo $get_edit_mediatype['audiolinkurl']; ?>"/>
								</div>-->
								<?php
								}
								if(isset($_GET['edit']))
								{
								?>
									<div class="fieldCont" >
										<div class="fieldTitle">Track #</div>
										<input type="hidden" class="fieldText"  id="songname" name="songname" />
										<input type="hidden" class="fieldText"  id="songsize" name="songsize" />
										<input type="text" class="fieldText" name="track" id="track" value="<?php if(isset($_GET['edit'])){echo $get_edit_mediatype['track'];}?>" />
									</div>
								<?php	
								}
							}
							?>
							<!-- File upload Field For Song Ends Here -->
							<!-- File upload Field For Gallery  -->
							<?php
							if(!isset($_GET['edit']))
							{
							?>
                            	<div class="fieldCont" id = "forsale_yes" style="display:none;">
                                	<!--<h4 style="margin:0;">Sale</h4>-->
									<div class="fieldTitle" title="Choose how to add the media file.">File</div>
                                    <div class="radioUp">
                                        <input type="radio" class="radio" title="Upload to the Purify Server, size of file will go toward your account limit." name="select" value="purify" id="uploadtopurify" <?php if(isset($_GET['edit']) && $get_edit_mediatype['upload_to']=="purify"){echo "checked";} ?>/>
                                        <div class="radioTitle" title="Upload to the Purify Server, size of file will go toward your account limit.">Upload to Purify</div>
                                    </div>
                                    <!--<div class="radioUp" >
                                        <input type="radio" class="radio" name="select" value="s3" id="uploadtomys3" <?php/* if(isset($_GET['edit']) && $get_edit_mediatype['upload_to']=="s3"){echo "checked";} ?> onclick="<?php if($get_S3_account=="blank"){ echo "find_s3_account('blank','uploadtopurify')"; } else{ echo "showuploaders(1)";} */?>" title="Upload to your Amazon S3 server if you have set one up. There is no limit on your uploads or plays with this media and you will pay Amazon directly $.12/GB/month for storage and streaming."/>
                                        <div class="radioTitle" title="Upload to your Amazon S3 server if you have set one up. There is no limit on your uploads or plays with this media and you will pay Amazon directly $.12/GB/month for storage and streaming." >Upload to my S3</div>
                                    </div>-->
                                    
                                </div>
								<div class="fieldCont" id = "forsale_no" style="display:none;">
                                	<div class="fieldTitle" title="Choose how to add the media file.">File</div>
                                    <div class="radioUp">
                                        <input type="radio" class="radio" name="select" title="Upload to the Purify Server, size of file will go toward your account limit." value="purify" id="uploadtopurifyno" <?php if(isset($_GET['edit']) && $get_edit_mediatype['upload_to']=="purify"){echo "checked";} ?> onclick="<?php if($final_value > 100) {echo "know_member_or_not('gallery')";}else {echo "showuploaders('1')";}?> "/>
                                        <div class="radioTitle" title="Upload to the Purify Server, size of file will go toward your account limit.">Upload to Purify</div>
                                    </div>
                                    <!--<div class="radioUp" >
                                        <input type="radio" class="radio" name="select" value="s3" id="upload_gallery_link" <?php /*if(isset($_GET['edit']) && $get_edit_mediatype['upload_to']=="s3"){echo "checked";} ?>  onclick="<?php if($get_S3_account=="blank"){ echo "find_s3_account('blank','uploadtopurifyno')"; } else{ echo "showuploaders(1)";} */?>" title="Upload to your Amazon S3 server if you have set one up. There is no limit on your uploads or plays with this media and you will pay Amazon directly $.12/GB/month for storage and streaming."/>
                                        <div class="radioTitle" title="Upload to your Amazon S3 server if you have set one up. There is no limit on your uploads or plays with this media and you will pay Amazon directly $.12/GB/month for storage and streaming." >Upload to my S3</div>
                                    </div>-->
									<a href="add_user_s3.php" id="popup_s3_error"></a>
                                    
                                </div>
								<div id="mytext6" style="display:none;">
										<div class="fieldCont" id="upload_gallery" style="display:none;">
											<div class="fieldTitle" title="Select to upload multiple images which will be resized for streaming or single image to be uploaded at original size for downloading only.">Add Image</div>
											<select class="dropdown" name ="selectuploadtype" id="selectuploadtype" title="Select to upload multiple images which will be resized for streaming or single image to be uploaded at original size for downloading only.">
											<?php 
											if(isset($_GET['edit']))
											{
												?>
													<option value="0">Select Image</option>
												<?php
												if($get_edit_mediatype['gallery_id']>0)
												{
												?>
													<option value="Image" selected>Multiple</option>
												<?php	
												}
												else
												{
												?>
													<option value="one" selected>Single</option>
												<?php
												}
											}
											else
											{
											?>
												<option value="0">Select Image</option>
												<option value="Image">Multiple</option>
												<option value="one">Single</option>
											<?php
											}
											?>
											</select>
											<div class="hint">Single images will be high resolution.</div>
										</div>
										
									</div>
								<div id="singleimageupload" style="display:none">
									<div class="fieldCont" >
										<div class="fieldTitle">*Title</div>
										<input type="text" class="fieldText" name="forgallerytitle" id="forgallerytitle" />
									</div>
									<div class="fieldCont">
										<div class="fieldTitle">Add Image</div>
										<input type="file" name="gallerysingleimage" id="gallerysingleimage"/>
									</div>
								</div>
								<!-- Image upload Div -->
								<div id="imgUploadDiv" style="display:none;">
									<div class="fieldCont">
										<div class="fieldTitle">*Gallery Title</div>
										<input name="galleryFileName" type="text" id="galleryFileName" class="fieldText" />
										<div id="Flashnotfound" style="display:none;">
											Either your flash player plugin is disabled or if you don't have the flash player <a href="http://get.adobe.com/flashplayer/" target="_blank">Click Here</a> to install.
										</div>
										<div id="divGalleryFileSize" style="display:none;"></div>
									</div>	
									<div class="fieldCont">
										<div class="fieldTitle"></div>
										<!--<form action="" method="post" enctype="multipart/form-data">-->
											<input name="theFile" type="file" id="file_upload" />
										<!--</form>-->
										<!-- Image upload Div -->
									</div>
								</div>
								
								<input type="hidden" id="galleryimages[]" name="galleryimages[]" />
								<input type="hidden" id="galleryimagessize[]" name="galleryimagessize[]" />
								<div id="upload_URL" style="display:none;">
									<div class="fieldCont" >
										<div class="fieldTitle">*Title</div>
										<input type="text" class="fieldText" name="multipletitle" id="multipletitle" />
									</div>
									<div class="fieldCont" >
										<div class="fieldTitle">URL or File(s)</div>
										<input type="text" class="fieldText" name="imageurl" id="imageurl" value ="<?php if(isset($_GET['edit'])){echo $get_edit_mediatype['image_url'];}?>"/>
										<div class="hintUpload">Saved at 1500 x 1500 if file is larger. If it is smaller save at original size.</div>
									</div>
								</div>
								<?php 
									if(isset($_GET['edit']))
									{
									?>
									<div class="fieldCont">
                                      <div class="fieldTitle">High Res Upload</div>
									<input type="text" disabled class="fieldText" value="<?php echo $get_media_gallery_sale['high_res_upload'];?>"/>
									</div>
									<?php
									}
									else
									{
									?>
									<div id="gallery_sale" style="display:none;">
										<div class="fieldCont">
											<div class="fieldTitle">*Title</div>
											<input type="text" class="fieldText" name="galsaletitle" id="galsaletitle" value="<?php //if(isset($_GET['edit'])){echo $editmedia['title'];}?>" />
										</div>
										<div class="fieldCont" >
										  <div class="fieldTitle">High Res Upload</div>
											<input type="file" name="gallery_high_res_image" id="gallery_high_res_image"/>
											<!--<div class="hint">Used for Distribution or Sale.</div>-->
										</div>
									</div>
									<?php
									}
									?>
									
							<?php
							}
							if(isset($_GET['edit']) && $editmedia['media_type']==113)
							{
							?>
							<div class="fieldCont">
								<div class="fieldTitle">*Title </div>
								<?php 
								
									if($get_edit_mediatype['gallery_id']>0)
									{
									?>
										<input type="text" class="fieldText" name ="gallery_edit_title" id="galleryFileName" value="<?php echo $editmedia['title'];?>"/>
									<?php
									}
									else
									{
									?>
										<input type="text" class="fieldText" name ="gallery_sale_title" id="galleryFileName" value="<?php echo $editmedia['title'];?>"/>
										</div>
										<div class="fieldCont">
										<div class="fieldTitle"></div>
										<input type="text"  class="fieldText" disabled  value="<?php echo $get_edit_mediatype['image_name'];?>"/>
									<?php
									}
								?>
							</div>	
							<?php
							}
								?>
							<!-- File upload Field For Gallery Ends Here -->
							
							<!-- File upload Field For Video  -->
							<?php
							if(!isset($_GET['edit']))
							{
							?>
								<div class="fieldCont" id="display_file_at_yes" style="display:none">
								<!--<h4 style="margin:0;">Sale</h4>-->
									<div class="fieldTitle" title="Choose how to add the media file.">File</div>
									<div class="radioUp">
										<input type="radio" class="radio" title="Upload to the Purify Server, size of file will go toward your account limit." name="video_select" value="purify" id="video_select" <?php if(isset($_GET['edit']) && $get_edit_mediatype['upload_to']=="purify"){echo "checked";} ?> />
										<div class="radioTitle" title="Upload to the Purify Server, size of file will go toward your account limit." >Upload</div>
									</div>
									<!--<div class="radioUp">
										<input type="radio" class="radio" name="video_select" value="s3" id="video_select" <?php /*if(isset($_GET['edit']) && $get_edit_mediatype['upload_to']=="s3"){echo "checked";} */?> title="Upload to your Amazon S3 server if you have set one up. There is no limit on your uploads or plays with this media and you will pay Amazon directly $.12/GB/month for storage and streaming."/>
										<div class="radioTitle" title="Upload to your Amazon S3 server if you have set one up. There is no limit on your uploads or plays with this media and you will pay Amazon directly $.12/GB/month for storage and streaming.">Upload to my S3</div>
									</div>-->
								</div>	
								
								<!--<div class="fieldCont" id="display_file_at_no" style="display:none">
									<div class="fieldTitle">File</div>
									<div class="radioUp">
										<input type="radio" class="radio" name="video_select" value="purify" id="video_select" <?php //if(isset($_GET['edit']) && $get_edit_mediatype['upload_to']=="purify"){echo "checked";} ?>  onclick="<?php //if($final_value > 100) {echo "know_member_or_not()";}else {echo "showvideouploaders(0)";}?> "/>
										<div class="radioTitle" >Upload</div>
									</div>
									<div class="radioUp">
										<input type="radio" class="radio" name="video_select" value="addyoutubeurl" id="video_select" <?php //if(isset($_GET['edit']) && $get_edit_mediatype['upload_to']=="addyoutubeurl"){echo "checked";} ?> onclick="showvideouploaders(1)" />
										<div class="radioTitle">Add Youtube URL</div>
									</div>
								</div>	-->								
									
								<div class="fieldCont" id="videoUploadDiv" style="display:none;" >
									<div class="fieldTitle" title="Choose how to add the media file.">File</div>
									<input type="radio" name="videouploadoption1" value="file" id="video_upload_org" title="Upload to the Purify Server, size of file will go toward your account limit." onclick="show_video_uploading_options()" ><span title="Upload to the Purify Server, size of file will go toward your account limit."> Upload </span>
									<input type="radio" name="videouploadoption1" value="link" id="upload_video_link" title="Add a url for a Youtube file. You may get the url from the Youtube player on the Youtube site or embedded on other sites."><span title="Add a url for a Youtube file. You may get the url from the Youtube player on the Youtube site or embedded on other sites.">Youtube URL </span><br/>
								</div>
								<div class="fieldCont" id="videoUploadoptions" style="display:none;" >
									<div class="fieldTitle"></div>
									<input type="radio" name="videouploadoption" title="Upload to the Purify Server, size of file will go toward your account limit." id="video_uploader" value="<?php echo "file";?>" onclick="<?php if($final_value > 100) {echo " return know_member_or_not('video')";}?>" ><span title="Upload to the Purify Server, size of file will go toward your account limit."> Upload to Purify </span>
									<!--<input type="radio" name="videouploadoption" id="upload_s3_video" value="file" title="Upload to your Amazon S3 server if you have set one up. There is no limit on your uploads or plays with this media and you will pay Amazon directly $.12/GB/month for storage and streaming." onclick="<?php /*if($get_S3_account=="blank"){ echo "find_s3_account('blank','video_uploader')"; }*/?>" ><span title="Upload to your Amazon S3 server if you have set one up. There is no limit on your uploads or plays with this media and you will pay Amazon directly $.12/GB/month for storage and streaming.">Upload to my S3</span><br/>-->
								</div>
								<div id="videofileform" style="display:none;">
									<div class="fieldCont">
										<div class="fieldTitle">*Title</div> 
										<input name="videoFileName" type="text" id="videoFileName" class="fieldText" />
										<div id="Flashnotfound_video" style="display:none;">
											 Either your flash player plugin is disabled or if you don't have the flash player <a href="http://get.adobe.com/flashplayer/" target="_blank">Click Here</a> to install.
										</div>
										<div id="divVideoFileSize" style="display:none;"></div>
									</div>
									<div class="fieldCont">
										<div class="fieldTitle"></div> 
										<!--<form action="" methos="post" enctype="multipart/form-data">-->
										<input name="thevideoFile" type="file" id="file_upload_video" />
										<!--</form>-->
									</div>
								</div>
								<div id="videolinkform"  style="display:none;">
									<div class="fieldCont" >
										<div class="fieldTitle">*Title</div>
										<input name="videoLinkName" type="text" id="videoLinkName" class="fieldText" />
									</div>
									<div class="fieldCont" >
										<div class="fieldTitle">*Link</div>
										<input name="videoLinkUrl" type="text" id="videoLinkUrl" class="fieldText" />
										<div class="hint" style="padding-top:5px; width:154px;">Enter a Vimeo or Youtube link.</div>
									</div>
								</div>
								
										
								<?php
							}
							if(isset($_GET['edit']) && $editmedia['media_type']==115)
							{
								if($get_edit_mediatype['videotitle']=="" && $get_edit_mediatype['videolink']=="")
								{
							?>	
								 <div class="fieldCont">
									<input type="hidden" class="fieldText"  id="videoname" name="videoname" />
									
									<div class="fieldTitle">*Title</div>
									<input type="text" id="videoFileName" name="video_up_title" class="fieldText" value="<?php echo $editmedia['title']; ?>"/>
								</div>
								<!--<div class="fieldCont">
									<input type="hidden" class="fieldText"  id="videoname" name="videoname" />
									<div class="fieldTitle">Name</div>
									<input type="text" disabled class="fieldText" value="<?php //echo $get_edit_mediatype['video_name']; ?>"/>
								</div>-->
							<?php
								}
								else
								{
								?>
								<div class="fieldCont">
									<input type="hidden" class="fieldText"  id="videoname" name="videoname" />
									<div class="fieldTitle">*Title</div>
									<input type="text" class="fieldText" id="videoLinkName" name="videolink_title" value="<?php echo $editmedia['title']; ?>"/>
								</div>
								<!--<div class="fieldCont">
									<input type="hidden" class="fieldText"  id="videoname" name="videoname" />
									<div class="fieldTitle">Link</div>
									<input type="text" disabled class="fieldText" value="<?php //echo $get_edit_mediatype['videolink']; ?>"/>
								</div>-->
								<?php
								}
							}
							?>
							<!-- File upload Field For Video Ends Here -->
							<div class="fieldCont" id="media_tag_user">
                            	<div class="fieldTitle" title="Tag users who may use the media across the site but may not edit it.">Tag User</div>
                                <input type="text" id="taguser" name="taguser" class="fieldText" title="Tag users who may use the media across the site but may not edit it." />
								<div class="hint"><input type="button" value=" Add " id="addtag" onclick="addtags()" title="Press to add tagged user." /></div>
								<div class="hint" style="display:none;" id="user_error">No profiles found with that name.</div>
                            </div>
                            <div class="fieldCont" id="media_tagged_user">
                              <div class="fieldTitle" title="Users who have been tagged in the media." >Tagged Users</div>
                                <select size="5" multiple="multiple" class="list" id="taggedusers[]" name="taggedusers[]" title="Users who have been tagged in the media.">
                                   <?php
										if(isset($_GET['edit']))
										{
											for($i=0;$i<count($tagged_user);$i++)
											{
												$sql = $newclassobj->get_users_taggeds($tagged_user[$i]);
												if(empty($sql))
												{
													continue;
												}
											?>
												<option selected value="<?php echo $sql['fname'].' '.$sql['lname']; ?>"><?php echo $sql['fname'].' '.$sql['lname']; ?></option>
											<?php
											}
										}
									?>
                                </select>
								<input type="hidden" class="fieldText" id="useremail_old" name="useremail_old" value=""/>
								<input type="hidden" class="fieldText" id="useremail" name="useremail" value="<?php if(isset($_GET['edit'])){echo $editmedia['tagged_user_email']; }?>"/>
								<input type="hidden" class="fieldText" id="useremail_removed" name="useremail_removed" />
                            </div>
							<div class="hint"><input name="remove" id="remove" type="button"  value=" Remove " onclick="remove_tags()" style="display:none;left:-20px;position:relative;"/></div>
                            
                            
							<div class="fieldCont">
                            	<div class="fieldTitle" title="The registered user who created the media. If the creator does not display when typing than have them register as an artist or community user. ">Creator</div>
                                <input type="text" class="fieldText" id="creator" name="creator" value="<?php 
								if(isset($_GET['edit'])){
										if($editmedia['creator_info']!="") 
										{
											$exps = explode('|',$editmedia['creator_info']);
											$naam = $exps[0];
											$nam_id = $exps[1];
											
											if($naam=='general_artist')
											{
												$sql = mysql_query("SELECT * FROM general_artist WHERE artist_id='".$nam_id."'");
											}
											if($naam=='general_community')
											{
												$sql = mysql_query("SELECT * FROM general_community WHERE community_id='".$nam_id."'");
											}
											if($naam=='artist_project' || $naam=='community_project')
											{
												$sql = mysql_query("SELECT * FROM $naam WHERE id='".$nam_id."'");
											}
											
											if(isset($sql) && mysql_num_rows($sql)>0)
											{
												$ans = mysql_fetch_assoc($sql);
												if(isset($ans['name']))
												{
													echo $ans['name'];
												}
												elseif(isset($ans['title']))
												{
													echo $ans['title'];
												}
											}
										}
									}?>" title="The registered user who created the media. If the creator does not display when typing than have them register as an artist or community user. "/>
								<div class="hint" style="display:none" id="creator_error">No profiles found with that name.</div>
								<input type="hidden" class="fieldText" id="creator_info" name="creator_info" value="<?php if(isset($_GET['edit'])){echo $editmedia['creator_info']; }?>"/>
								<input type="hidden" class="fieldText" id="previous_creator_info" name="previous_creator_info" value="<?php if(isset($_GET['edit'])){echo $editmedia['creator_info']; }?>"/>
                            </div>
                            <div class="fieldCont">
                            	<div class="fieldTitle" title="The project or event profile that the media is from. If the project or event does not display when typing than add it in your artist or community profile.">From</div>
                                <input type="text" class="fieldText" id="from" name="from" value="<?php 
								if(isset($_GET['edit'])){
										if($editmedia['from_info']!="") 
										{
											$exps = explode('|',$editmedia['from_info']);
											$naam = $exps[0];
											$nam_id = $exps[1];
											
											if($naam=='artist_event' || $naam=='community_event')
											{
												$sql = mysql_query("SELECT * FROM $naam WHERE id='".$nam_id."'");
											}											
											if($naam=='artist_project' || $naam=='community_project')
											{
												$sql = mysql_query("SELECT * FROM $naam WHERE id='".$nam_id."'");
											}
											
											if(isset($sql) && mysql_num_rows($sql)>0)
											{
												$ans = mysql_fetch_assoc($sql);
												if(isset($ans['name']))
												{
													echo $ans['name'];
												}
												elseif(isset($ans['title']))
												{
													echo $ans['title'];
												}
											}
										} }?>" title="The project or event profile that the media is from. If the project or event does not display when typing than add it in your artist or community profile."/>
								<div class="hint" style="display:none" id="from_error">No profiles found with that name.</div>
								<input type="hidden" class="fieldText" id="from_info" name="from_info" value="<?php if(isset($_GET['edit'])){echo $editmedia['from_info']; }?>" />
								<input type="hidden" class="fieldText" id="previous_from_info" name="previous_from_info" value="<?php if(isset($_GET['edit'])){echo $editmedia['from_info']; }?>"/>
                            </div>
							
							<!--Song Gallery display Start here -->
							<?php
							if(!isset($_GET['edit']) )
							{
							//echo "ssss";
							?>
							<div class="fieldCont" id="song_gallery" style="display:none;">
								<div class="fieldTitle" title="Select a gallery you added or tagged in to display with your song as a slideshow in the media player.">Gallery</div>
								<select class="dropdown" name="gallery" id="gallery" title="Select a gallery you added or tagged in to display with your song as a slideshow in the media player.">
									<option value="multiple">Select from the list</option>
								<?php
									if(isset($_GET['edit']))
									{
										for($i=0;$i<count($getgallery);$i++)
										{
											if($get_edit_mediatype['gallery_id']==$getgallery[$i]['gallery_id'] || $get_edit_mediatype['gallery_id']==$getgallery[$i]['id'])
											{
										if($getgallery[$i]['gallery_title']!="")
											{
										?>
											<option selected value="<?php echo $getgallery[$i]['gallery_id'].'~'.$getgallery[$i]['table_name'];?>"><?php echo $getgallery[$i]['gallery_title'];?></option>
										<?php
											}
											if($getgallery[$i]['title']!="")
											{
										?>
											<option selected value="<?php echo $getgallery[$i]['id'].'~'.$getgallery[$i]['table_name'];?>"><?php echo $getgallery[$i]['title'];?></option>
										<?php
											}
											}	
										
											else
											{
											if($getgallery[$i]['gallery_title']!="")
											{
										?>
											<option value="<?php echo $getgallery[$i]['gallery_id'].'~'.$getgallery[$i]['table_name'];?>"><?php echo $getgallery[$i]['gallery_title'];?></option>
										<?php
											}
											if($getgallery[$i]['title']!="")
											{
										?>
											<option value="<?php echo $getgallery[$i]['id'].'~'.$getgallery[$i]['table_name'];?>"><?php echo $getgallery[$i]['title'];?></option>
										<?php
											}
											}
										}
									}
									else
									{
										for($i=0;$i<count($getgallery);$i++)
										{
											if($getgallery[$i]['gallery_title']!="")
											{
										?>
											<option value="<?php echo $getgallery[$i]['gallery_id'].'~'.$getgallery[$i]['table_name'];?>"><?php echo $getgallery[$i]['gallery_title'];?></option>
										<?php
											}
											if($getgallery[$i]['title']!="")
											{
										?>
											<option value="<?php echo $getgallery[$i]['id'].'~'.$getgallery[$i]['table_name'];?>"><?php echo $getgallery[$i]['title'];?></option>
										<?php
											}
										}
									}
								?>
									
								</select>
							</div>
							<?php
							}
							?>
							<!--Song Gallery display ends here -->
							<!--Song Gallery display in edit Starts  here -->
                        	<?php 
							if(isset($_GET['edit']) && $editmedia['media_type']==114)
							{
							?>
							<div class="fieldCont">
								<div class="fieldTitle" title="Select a gallery you added or tagged in to display with your song as a slideshow in the media player.">Gallery</div>
								<select class="dropdown" name="gallery" id="gallery" title="Select a gallery you added or tagged in to display with your song as a slideshow in the media player.">
									<option value="multiple">Select from the list</option>
								<?php
									if(isset($_GET['edit']))
									{
										for($i=0;$i<count($getgallery);$i++)
										{
											if($get_edit_mediatype['gallery_id']==$getgallery[$i]['gallery_id'] || $get_edit_mediatype['gallery_id']==$getgallery[$i]['id'])
											{
										if($getgallery[$i]['gallery_title']!="")
											{
										?>
											<option selected value="<?php echo $getgallery[$i]['gallery_id'].'~'.$getgallery[$i]['table_name'];?>"><?php echo $getgallery[$i]['gallery_title'];?></option>
										<?php
											}
											if($getgallery[$i]['title']!="")
											{
										?>
											<option selected value="<?php echo $getgallery[$i]['id'].'~'.$getgallery[$i]['table_name'];?>"><?php echo $getgallery[$i]['title'];?></option>
										<?php
											}
											}	
										
											else
											{
											if($getgallery[$i]['gallery_title']!="")
											{
										?>
											<option value="<?php echo $getgallery[$i]['gallery_id'].'~'.$getgallery[$i]['table_name'];?>"><?php echo $getgallery[$i]['gallery_title'];?></option>
										<?php
											}
											if($getgallery[$i]['title']!="")
											{
										?>
											<option value="<?php echo $getgallery[$i]['id'].'~'.$getgallery[$i]['table_name'];?>"><?php echo $getgallery[$i]['title'];?></option>
										<?php
											}
											}
										}
									}
									else
									{
										for($i=0;$i<count($getgallery);$i++)
										{
											if($getgallery[$i]['gallery_title']!="")
											{
										?>
											<option value="<?php echo $getgallery[$i]['gallery_id'].'~'.$getgallery[$i]['table_name'];?>"><?php echo $getgallery[$i]['gallery_title'];?></option>
										<?php
											}
											if($getgallery[$i]['title']!="")
											{
										?>
											<option value="<?php echo $getgallery[$i]['id'].'~'.$getgallery[$i]['table_name'];?>"><?php echo $getgallery[$i]['title'];?></option>
										<?php
											}
										}
									}
								?>
									
								</select>
							</div>
                            <?php
							}
							?>
							<!--Song Gallery display in edit ends here -->
							
							
                            <div id="nodisplay" style="display:none;">
                            </div>
							 
							<!--Gallery Div Starts Here
							<div id="<?php //echo $arr[3];?>" style="display:none;"> Back up
							-->
                            <div id="113" style="display:none;">
								<!--<div class="fieldCont">
                                  <div class="fieldTitle">Sub Type</div>
                                         <select name="subtype-select" class="dropdown" id="subtype-select">
                                            <option value="0" selected="selected">Select Subtype</option>					
                                        </select>
                    
                                  <!--<div class="hint"><input name="ADD" type="button" id="addType" value=" Add " /></div>-->
                               <!-- </div>
                                <div class="fieldCont">
                                <div class="fieldTitle">Meta Type</div>
                                    <select name="metatype-select" class="dropdown" id="metatype-select">
                                        <option value="0" selected="selected">Select Metatype</option>					
                    
                                    </select>
                                  
                                  <div class="hint"><input name="ADD" type="button" id="addTypegallery" value=" Add "></div>
                                </div>-->
                           
                                <script type="text/javascript">
									function showuploaders(num)
									{
										if(num==1)
										{
											if(($("#sharingpreference").val() ==4) && $("#type-select").val()==113)
											{
												document.getElementById('upload_gallery').style.display='block';
												document.getElementById('upload_URL').style.display='none';
											}
											else if(($("#sharingpreference").val() ==0 || $("#sharingpreference").val() ==1 || $("#sharingpreference").val() ==3 || $("#sharingpreference").val() ==2) && $("#type-select").val()==113)
											{
												Change_gallery();
												document.getElementById('imgUploadDiv').style.display='block';
												document.getElementById('upload_URL').style.display='none';
											}
										}
										else
										{
											document.getElementById('singleimageupload').style.display='none';
											document.getElementById('imgUploadDiv').style.display='none';
											document.getElementById('upload_gallery').style.display='none';
											document.getElementById('upload_URL').style.display='block';
										}
									}
								</script>
								<?php
								/*if(isset($_GET['edit']))
								{
								?>
									<script type="text/javascript">
										$(document).ready(function(){
										
										var a="<?php echo $get_edit_mediatype['for_sale']?>";
										//alert(a);
											if(a==1)
											{
												showText(0);
											}
											else
											{
												showText(1);
											}
										});
										
									</script>
							<?php
								}*/
							
							?>
							 <!-- <div id="mytext" style="display:none;">
                                	<div class="fieldCont"><b>Sales &amp; Distribution</b></div>
                                    <div class="fieldCont">
                                        <div class="fieldTitle">Price</div>
                                        <input type="text" class="fieldText" name="totalcost_gallery" id="totalcost_gallery" <?php if(isset($_GET['edit'])){ echo "disabled";}?> value="<?php if(isset($_GET['edit'])){ echo $get_media_gallery_sale['total_cost'];}?>"/>
                                    </div>
									
                                </div>-->
								
                                
								
							
                            </div>
							<?php
							if(isset($_GET['edit']) && $editmedia['media_type']==113 && $editmedia['sharing_preference ']==5)
							{
							?>
								<div class="fieldCont">
									<div class="fieldTitle">Price</div>
									<input type="text" class="fieldText" name="totalcost_gallery" id="totalcost_gallery" <?php if(isset($_GET['edit'])){ echo "disabled";}?> value="<?php if(isset($_GET['edit'])){ echo $get_media_gallery_sale['total_cost'];}?>"/>
								</div>
							<?php	
							}
							?>
							<!--Gallery Div Ends Here-->
							
							
							<!--Song Div Starts Here-->
                            <div id="114" style="display:none;">
									<!--<div class="fieldCont">
									  <div class="fieldTitle">Sub Type</div>
											 <select name="subtype-select3" class="dropdown" id="subtype-select3">
												<option value="0" selected="selected">Select Subtype</option>					
						
											</select>
						
									  <!--<div class="hint"><input name="ADD" type="button" id="addType" value=" Add " /></div>-->
									<!--</div>
									<div class="fieldCont">
									<div class="fieldTitle">Meta Type</div>
										<select name="metatype-select3" class="dropdown" id="metatype-select3">
											<option value="0" selected="selected">Select Metatype</option>					
						
										</select>
									  
									  <div class="hint"><input name="ADD" type="button" id="addTypesong" value=" Add "></div>
									</div>-->
									
									<script type="text/javascript">
											function find_member(type)
											{
												alert("You have to be member for using this facility.");
												if(type =="song")
												{
													document.getElementById("no_for_song").click();
												}
												if(type == "gallery")
												{
													document.getElementById("no_for_gallery").click();
												}
												if(type=="video")
												{
													document.getElementById("no_for_video").click();
												}
											}
											</script>
						        
								<!--<div class="fieldCont" style="display:none" id="sel_file_at_no">
								<div class="fieldTitle">File</div>
								     <div class="radioUp">
                                        <input type="radio" class="radio" name="song_select" value="souncloudurl" <?php //if(isset($_GET['edit']) && $get_edit_mediatype['upload_to']=="song_select"){echo "checked";} ?>/>
                                        <div class="radioTitle">Add Soundcloud URL</div>
                                    </div>
                                </div>-->
							
								<div id="mytext2" style="display:none;">
                                    <script type="text/javascript">function showText4(num){ 
										if(num==0){ 
											document.getElementById('mytext4').style.display='block';
											//document.getElementById('mytext5').style.display='none';
										}
										else 
										{
											document.getElementById('mytext4').style.display='none';
											//document.getElementById('mytext5').style.display='block';
										}
										return;	
										}
									</script>
                                    <div id="mytext4" >
									<!--<div class="fieldCont"><b>Sales &amp; Distribution</b></div>-->
                                    	<div class="fieldCont">
                                            <div class="fieldTitle">*Price</div>
                                            <input type="text" class="fieldText" name="songprice" id="songprice" value="<?php if(isset($_GET['edit'])){echo $get_edit_mediatype['price'];}?>" />
                                        </div>
										<!--<div class="fieldCont">
                                            <div class="fieldTitle">Format</div>
											<select onchange="Selection(value)" class="dropdown">
                                            	<option value="empty">Select Format</option>
                                                <option value="empty">Full Length Album (5 or more songs)</option>
                                                <option value="empty">EP Album (2  - 4 Songs)</option>
                                                <option value="songSingle">Single</option>
                                            </select>
										</div>
										 <div id="songSingle" style="display:none;">
                                            <div class="fieldCont">
                                                <div class="fieldTitle">Hight Res Image</div>
                                                <input type="text" class="fieldText" />
                                            </div>
                                        </div>
                                        <div class="fieldCont">
                                            <div class="fieldTitle">Upload WAV</div>
											<?php
												/*if(isset($_GET['edit']))
												{
												?>
													<input type="text" class="fieldText" value="<?php echo $get_sales_song['upload_wav'];?>" disabled />
												<?php	
												}
												else
												{
												?>
													<input type="file" class="fieldUpload" name="upload_wav" id="upload_wav" />
												<?php
												}*/
												?>
                                        </div>-->
                                    </div>
                                </div>
								
                                <!--<div id="general_field" style="display:none;">
									
									<div class="fieldCont">
										<div class="fieldTitle">Recorded At</div>
										<input type="text" class="fieldText" name="recordedat" id="recordedat" value="<?php //if(isset($_GET['edit'])){echo $get_edit_mediatype['recordedat'];}?>" />
									</div>
									<div class="fieldCont">
										<div class="fieldTitle">Mixed By</div>
										<input type="text" class="fieldText" name="mixedby" id="mixedby"  value="<?php// if(isset($_GET['edit'])){echo $get_edit_mediatype['mixedby'];}?>"/>
									</div>
									<div class="fieldCont">
										<div class="fieldTitle">Mastered By</div>
										<input type="text" class="fieldText" id="masteredby" name="masteredby" value="<?php //if(isset($_GET['edit'])){echo $get_edit_mediatype['masteredby'];}?>" />
									</div>
									
								</div>-->
                                
             				</div>
							<!--Song Div Ends  Here-->
							
							<!--Video Div Starts Here-->
                            <div id="115" style="display:none;">
								<!--<div class="fieldCont">
                                  <div class="fieldTitle">Sub Type</div>
                                         <select name="subtype-select2" class="dropdown" id="subtype-select2">
                                            <option value="0" selected="selected">Select Subtype</option>					
                    
                                        </select>
                    
                                  <!--<div class="hint"><input name="ADD" type="button" id="addType" value=" Add " /></div>-->
                                <!--</div>
                                <div class="fieldCont">
                                <div class="fieldTitle">Meta Type</div>
                                    <select name="metatype-select2" class="dropdown" id="metatype-select2">
                                        <option value="0" selected="selected">Select Metatype</option>					
                    
                                    </select>
                                  
                                  <div class="hint"><input name="ADD" type="button" id="addTypevideo" value=" Add "></div>
                                </div>-->
							<div id="display_at_no" style="display:none;">
							
		
                                
                                <div class="fieldCont">
								<input type="hidden" class="fieldText"  id="videoname" name="videoname" />
								<input type="hidden" class="fieldText"  id="videosize" name="videosize" />
                                    <div class="fieldTitle">Produced By</div>
                                    <input type="text" class="fieldText" name="producedby" id="producedby" value="<?php if(isset($_GET['edit'])){echo $get_edit_mediatype['produced_by'];} ?>" <?php //if(isset($_GET['edit'])){echo "disabled";} ?>/>
                                </div>
                                <div class="fieldCont">
                                    <div class="fieldTitle">Soundtrack By</div>
                                    <input type="text" class="fieldText" name="soundtrackby" id="soundtrackby" value="<?php if(isset($_GET['edit'])){echo $get_edit_mediatype['sound_track_by'];} ?>" <?php //if(isset($_GET['edit'])){echo "disabled";} ?> />
                                </div>
                                <div class="fieldCont">
                                    <div class="fieldTitle">Directed By</div>
                                    <input type="text" class="fieldText" name="directedby" id="directedby" value="<?php if(isset($_GET['edit'])){echo $get_edit_mediatype['directed_by'];} ?>" <?php// if(isset($_GET['edit'])){echo "disabled";} ?>/>
                                </div>
                                <!--<div class="fieldCont">
                                    <div class="fieldTitle">Profile Image</div>
                                    <input type="file" name="videoprofileimage" id="videoprofileimage" />
                                </div>-->
								<?php 
								/*if(isset($_GET['edit']))
								{
								?>
									<div class="fieldCont">
										<div class="fieldTitle"></div>
										<input type="text" class="fieldText" disabled  value="<?php echo $get_edit_mediatype['profile_image']; ?> "/>
									</div>
								<?php
								}*/
								?>
                                
                            </div>
							 <div id="mytext3" style="display:none;">
                                	<!--<div class="fieldCont"><b>Sales &amp; Distribution</b></div>-->
                                <div class="fieldCont">
                                	<div class="fieldTitle">Total Cost</div>
                                    <input type="text" class="fieldText" name="totalcost" id="totalcost" value="<?php if(isset($_GET['edit'])){echo $get_sales_video['total_cost'];}?> "  <?php if(isset($_GET['edit'])){echo "disabled";}?> />
                                </div>
                                <!--<div class="fieldCont">
                                	<div class="fieldTitle">Point Sharing</div>
                                    <select size="5" multiple="multiple" class="list" name="pointsharing" id="pointsharing">
                                        <option value="0">Select from the list</option>
                                    </select>
                                </div>
                                <div class="fieldCont">
                                	<div class="fieldTitle">File or URL</div>
									<?php
										/*if(isset($_GET['edit']))
										{
										?>
											<input type="text" class="fieldText" value="<?php echo $get_sales_video['file_or_url'];?>" disabled />
										<?php
										}
										else
										{
										?>
										<input type="file" class="fieldUpload" name="file_or_url" id="file_or_url" />
										<?php
										}*/
									?>
                                    
                                </div>
                                <div class="fieldCont">
                                	<div class="fieldTitle">High Res Upload</div>
									<?php
										/*if(isset($_GET['edit']))
										{
										?>
											<input type="text" class="fieldText" value="<?php echo $get_sales_video['high_res_upload'];?>" disabled />
										<?php
										}
										else
										{
										?>
										<input type="file" class="fieldUpload" name="high_res_upload" id="high_res_upload" />
										<?php
										}*/
										?>
                                </div>-->
                                </div>
						</div>
						
							<!--Video Div Ends Here-->
							
							<!--Channel Div Satrts Here-->
                            <div id="116" style="display:none;">
                            	<!--<div class="fieldCont">
                                  <div class="fieldTitle">Sub Type</div>
                                       <select name="subtype-select1" class="dropdown" id="subtype-select1">
											<option value="0" selected="selected">Select Subtype</option>
									   </select>
								</div>
                                <div class="fieldCont">
                                <div class="fieldTitle">Meta Type</div>
                                    <select name="metatype-select1" class="dropdown" id="metatype-select1">
                                        <option value="0" selected="selected">Select Metatype</option>					
                    
                                    </select>
                                  
                                  <div class="hint"><input name="ADD" type="button" id="addTypechannel" value=" Add "></div>
                                </div>-->
								
                                <div class="fieldCont">
                                    <div class="fieldTitle">Description</div>
									<textarea  class="jquery_ckeditor" cols="4" name="channeldescription" id="channeldescription" rows="10"><?php if(isset($_GET['edit'])){echo $get_edit_mediatype['description'];}?></textarea>
                                    <!--<input type="text" class="fieldText" name="channeldescription" id="channeldescription" value="<?php //if(isset($_GET['edit'])){echo $get_edit_mediatype['description'];} ?>" />-->
                                </div>
                               <!-- <div class="fieldCont">
                                    <div class="fieldTitle">Profile Image</div>
                                    <input type="file" name="channelprofileimage" id="channelprofileimage" />
                                </div>-->
								<?php
								/*if(isset($_GET['edit']))
								{
								?>
									<div class="fieldCont">
										<div class="fieldTitle"></div>
										<input type="text" name="channelprofileimagetext" id="channelprofileimagetext" class="fieldText" readonly value="<?php echo $get_edit_mediatype['profile_image']; ?>"/>
									</div>
								<?php
								}*/
								?>
                                <div class="fieldCont">
                                  <div class="fieldTitle" title="Select the songs to be added to this channel.">Songs</div>
                                   <select size="5" multiple="multiple" class="list" name="channelsongs[]" id="channelsongs[]" title="Select the songs to be added to this channel.">
										<?php
										
										/* while($get_mysong = mysql_fetch_assoc($getall_channel_song))
										{
											if(isset($_GET['edit']))
											{
												$get_sel_song =$newclassobj->get_sel_songs($_GET['edit']);
												$exp_song_id = explode(",",$get_sel_song['songs']);
												$yes =0;
												for($count_sel=0;$count_sel<=count($exp_song_id);$count_sel++)
												{
													if($exp_song_id[$count_sel]==""){continue;}
													else{
														if($exp_song_id[$count_sel] == $get_mysong['id'])
														{
															$yes = 1;
														?>
															<option value="<?php echo $get_mysong['id'];?>" selected><?php echo substr($get_mysong['creator'],0,20) ." : ". substr($get_mysong['from'],0,20) ." : ".  $get_mysong['title'];?></option>
														<?php
														}
													}
												}
												if($yes == 0)
												{
													
												?>
													<option value="<?php echo $get_mysong['id'];?>"><?php echo substr($get_mysong['creator'],0,20) ." : ". substr($get_mysong['from'],0,20) ." : ".  $get_mysong['title'];?></option>
												<?php
												}
											}
											else{
										?>
											<option value="<?php echo $get_mysong['id'];?>"><?php echo substr($get_mysong['creator'],0,20) ." : ". substr($get_mysong['from'],0,20) ." : ".  $get_mysong['title'];?></option>
										<?php
											}
										} */
										
										/* $get_gene_info =$newclassobj->sel_general();
										$get_tagged_id = explode(",",$get_gene_info['accepted_media_id']);
										if(!empty($get_tagged_id))
										{
											for($tag_count=0;$tag_count<count($get_tagged_id);$tag_count++)
											{
												if($get_tagged_id[$tag_count]==""){ continue; }else{
													$get_tag_song =$newclassobj->get_all_tagged_songs($get_tagged_id[$tag_count]);
													if(!empty($get_tag_song)){
														if(isset($_GET['edit']))
															{
																$get_sel_song_tag =$newclassobj->get_sel_songs($_GET['edit']);
																$exp_song_id_tag = explode(",",$get_sel_song_tag['songs']);
																$yes_tag =0;
																for($count_sel_tag=0;$count_sel_tag<=count($exp_song_id_tag);$count_sel_tag++)
																{
																	if($exp_song_id_tag[$count_sel_tag]==""){continue;}
																	else{
																		if($exp_song_id_tag[$count_sel_tag] == $get_tag_song['id'])
																		{
																			$yes_tag = 1;
																		?>
																			<option value="<?php echo $get_tag_song['id'];?>" selected><?php echo substr($get_tag_song['creator'],0,20) ." : ". substr($get_tag_song['from'],0,20) ." : ".  $get_tag_song['title'];?></option>
																		<?php
																		}
																	}
																}
																if($yes_tag == 0)
																{
																	
																?>
																	<option value="<?php echo $get_tag_song['id'];?>"><?php echo substr($get_tag_song['creator'],0,20) ." : ". substr($get_tag_song['from'],0,20) ." : ".  $get_tag_song['title'];?></option>
																<?php
																}
															}
															else{
																	
																?>
																	<option value="<?php echo $get_tag_song['id'];?>"><?php echo substr($get_tag_song['creator'],0,20) ." : ". substr($get_tag_song['from'],0,20) ." : ".  $get_tag_song['title'];?></option>
																<?php
																}
													}
												}
											}
										} */
										
										$get_mysong = array();
										while($get_mysongs = mysql_fetch_assoc($getall_channel_song))
										{
											$get_mysong[] = $get_mysongs;
										}
										
										$acc_son_med = array();
										$get_gene_info =$newclassobj->sel_general();
										$get_tagged_id = explode(",",$get_gene_info['accepted_media_id']);
										if(!empty($get_tagged_id))
										{
											for($tag_count=0;$tag_count<count($get_tagged_id);$tag_count++)
											{
												if($get_tagged_id[$tag_count]=="")
												{
													continue;
												}
												else
												{
													$get_tag_song = $newclassobj->get_all_tagged_songs($get_tagged_id[$tag_count]);
													if(!empty($get_tag_song))
													{
														$acc_son_med[] = $get_tag_song;
													}
												}
											}
										}
										
										$get_media_tag_art_pro = $newclassobj->get_artist_project_tag($_SESSION['login_email']);
										if(mysql_num_rows($get_media_tag_art_pro)>0)
										{
											$is = 0;
											while($art_pro_tag = mysql_fetch_assoc($get_media_tag_art_pro))
											{
												$get_songs = explode(",",$art_pro_tag['tagged_songs']);
												for($count_art_tag_song=0;$count_art_tag_song<count($get_songs);$count_art_tag_song++)
												{
													if($get_songs[$count_art_tag_song]!="")
													{
														$get_songs_new[$is] = $get_songs[$count_art_tag_song];
														$is = $is + 1;
													}
												}
											}
											$get_songs_n = array_unique($get_songs_new);
										}
										
										$get_media_tag_art_eve = $newclassobj->get_artist_event_tag($_SESSION['login_email']);
										if(mysql_num_rows($get_media_tag_art_eve)>0)
										{
											$is = 0;
											while($art_pro_tag = mysql_fetch_assoc($get_media_tag_art_eve))
											{
												$get_songs = explode(",",$art_pro_tag['tagged_songs']);
												for($count_art_tag_song=0;$count_art_tag_song<count($get_songs);$count_art_tag_song++)
												{
													if($get_songs[$count_art_tag_song]!="")
													{
														$get_evsongs_new[$is] = $get_songs[$count_art_tag_song];
														$is = $is + 1;
													}
												}
											}
											$get_songs_evn = array_unique($get_evsongs_new);
										}
										
										$get_media_tag_com_pro = $newclassobj->get_community_project_tag($_SESSION['login_email']);
										if(mysql_num_rows($get_media_tag_com_pro)>0)
										{
											$is_c = 0;
											while($com_pro_tag = mysql_fetch_assoc($get_media_tag_com_pro))
											{
												$get_com_songs = explode(",",$com_pro_tag['tagged_songs']);
												for($count_com_tag_song=0;$count_com_tag_song<count($get_com_songs);$count_com_tag_song++)
												{
													if($get_com_songs[$count_com_tag_song]!="")
													{
														$get_songs_new_c[$is_c] = $get_com_songs[$count_com_tag_song];
														$is_c = $is_c + 1;
													}
												}
											}
											$get_songs_n_c = array_unique($get_songs_new_c);
										}
										
										$get_media_tag_com_eve = $newclassobj->get_community_event_tag($_SESSION['login_email']);
										if(mysql_num_rows($get_media_tag_com_eve)>0)
										{
											$is_c = 0;
											while($com_pro_tag = mysql_fetch_assoc($get_media_tag_com_eve))
											{
												$get_com_songs = explode(",",$com_pro_tag['tagged_songs']);
												for($count_com_tag_song=0;$count_com_tag_song<count($get_com_songs);$count_com_tag_song++)
												{
													if($get_com_songs[$count_com_tag_song]!="")
													{
														$get_esongs_new_c[$is_c] = $get_com_songs[$count_com_tag_song];
														$is_c = $is_c + 1;
													}
												}
											}
											$get_songs_n_ec = array_unique($get_esongs_new_c);
										}
										
										$get_media_cre_art_pro = $newclassobj->get_project_create();
										if(!empty($get_media_cre_art_pro))
										{
											$is_p = 0;
											//while($art_pro_tag = mysql_fetch_assoc($get_media_cre_art_pro))
											for($cr_i=0;$cr_i<count($get_media_cre_art_pro);$cr_i++)
											{
												$get_csongs = explode(",",$get_media_cre_art_pro[$cr_i]['tagged_songs']);
												for($count_art_cr_song=0;$count_art_cr_song<count($get_csongs);$count_art_cr_song++)
												{
													if($get_csongs[$count_art_cr_song]!="")
													{
														$get_songs_pn[$is_p] = $get_csongs[$count_art_cr_song];
														$is_p = $is_p + 1;
													}
												}
											}
											$get_songs_p_new = array_unique($get_songs_pn);
										}
										/*This function will display the user in which profile he is creator and display media where that profile was creator*/
										$new_medi = $newclassobj->get_creator_heis();
										$new_creator_creator_media_array = array();
										if(!empty($new_medi))
										{
											for($count_cre_m=0;$count_cre_m<count($new_medi);$count_cre_m++)
											{
												if($new_medi[$count_cre_m]['media_type']==114){
													$new_creator_creator_media_array[] = $new_medi[$count_cre_m];
												}
											}
										}
										/*This function will display the user in which profile he is creator and display media where that profile was creator Ends here*/
										
										/*This function will display the user in which profile he is from and display media where that profile was creator*/
										$new_medi_from = $newclassobj->get_from_heis();
										$new_from_from_media_array = array();
										if(!empty($new_medi_from))
										{
											for($count_cre_m=0;$count_cre_m<count($new_medi_from);$count_cre_m++)
											{
												if($new_medi_from[$count_cre_m]['media_type']==114){
												$new_from_from_media_array[] = $new_medi_from[$count_cre_m];
												}
											}
										}
										/*This function will display the user in which profile he is from and display media where that profile was creator Ends here*/
										
										
										/*This function will display the user in which profile he is from or creator and tagged and display media where that profile was creator*/
										$new_medi_tagged = $newclassobj->get_the_user_tagin_project($_SESSION['login_email']);
										$new_tagged_from_media_array = array();
										if(!empty($new_medi_tagged))
										{
											for($count_cre_m=0;$count_cre_m<count($new_medi_tagged);$count_cre_m++)
											{
												if($new_medi_tagged[$count_cre_m]['media_type']==114){
												$new_tagged_from_media_array[] = $new_medi_tagged[$count_cre_m];
												}
											}
										}
										/*This function will display the user in which profile he is from or creator and tagged and display media where that profile was creator Ends here*/
										
										
										/*This function will display in which media the curent user was creator*/
										$get_where_creator_media = $newclassobj->get_media_where_creator_or_from();
										$new_creator_media_array = array();
										if(!empty($get_where_creator_media))
										{
											for($count_tag_m=0;$count_tag_m<count($get_where_creator_media);$count_tag_m++)
											{
												if($get_where_creator_media[$count_tag_m]['media_type']==114){
													$new_creator_media_array[] = $get_where_creator_media[$count_tag_m];
												}
											}
										}
										/*This function will display in which media the curent user was creator ends here*/
										
										if(!isset($get_songs_evn))
										{
											$get_songs_evn = array();
										}
										
										if(!isset($get_songs_n_ec))
										{
											$get_songs_n_ec = array();
										}
										
										if(!isset($get_songs_n))
										{
											$get_songs_n = array();
										}
										
										if(!isset($get_songs_n_c))
										{
											$get_songs_n_c = array();
										}
										
										if(!isset($get_songs_p_new))
										{
											$get_songs_p_new = array();
										}
										
										$get_songs_pnew_w = array_merge($get_songs_n,$get_songs_n_c,$get_songs_p_new,$get_songs_evn,$get_songs_n_ec);
										
										$cre_frm_med = array();
										$cre_frm_med = $newclassobj->get_media_create();
										
										$get_songs_pnew = array_unique($get_songs_pnew_w);
										$get_songs_final_ct = array();
										
										for($count_art_cr_song=0;$count_art_cr_song<count($get_songs_pnew);$count_art_cr_song++)
										{
											if($get_songs_pnew[$count_art_cr_song]!="")
											{
												$get_media_tag_art_pro_song = $newclassobj->get_tag_media_info($get_songs_pnew[$count_art_cr_song]);
												$get_songs_final_ct[] = $get_media_tag_art_pro_song;
											}
										}
										//var_dump($acc_son_med);
										//var_dump($get_mysong);
										//var_dump($get_songs_final_ct);
										//var_dump($new_creator_creator_media_array);
										//var_dump($new_from_from_media_array);
										//var_dump($new_tagged_from_media_array);
										//var_dump($new_creator_media_array);
										//var_dump($cre_frm_med);
										/*Get Subscription media*/
										$new_subscription_media = array();
										$get_subscription_media_res = $newclassobj->get_subscription_media();
										for($count_sub=0;$count_sub<=count($get_subscription_media_res);$count_sub++)
										{
											if($get_subscription_media_res[$count_sub]!="")
											{
												if($get_subscription_media_res[$count_sub]['media_type']==114)
												{
													/*$get_media_track_res = $newclassobj->get_media_track($get_subscription_media_res[$count_sub]['id']); 
													$get_subscription_media_res[$count_sub]['track'] = $get_media_track_res['track'];*/
													$new_subscription_media[] = $get_subscription_media_res[$count_sub];
												}
											}
										}
										
										$get_songs_pnew_w = array_merge($acc_son_med,$get_mysong,$get_songs_final_ct,$new_creator_creator_media_array,$new_from_from_media_array,$new_tagged_from_media_array,$new_creator_media_array,$cre_frm_med,$new_subscription_media);
										
										/* var_dump($acc_son_med);
										var_dump($get_songs_final_ct);
										var_dump($new_creator_creator_media_array);
										var_dump($new_from_from_media_array);
										var_dump($new_tagged_from_media_array);
										var_dump($new_creator_media_array);
										var_dump($cre_frm_med);
										var_dump($new_subscription_media); */						
										for($acc_song1=0;$acc_song1<count($get_songs_pnew_w);$acc_song1++)
										{
											if($get_songs_pnew_w[$acc_song1]['id']!="" && $get_songs_pnew_w[$acc_song1]['id']!=0)
											{
												$get_artist_song1 = $newclassobj->get_artist_song($get_songs_pnew_w[$acc_song1]['id']);
												$getsong1 = mysql_fetch_assoc($get_artist_song1);
												$ids = $get_songs_pnew_w[$acc_song1]['id'];
												$get_songs_pnew_w[$acc_song1]['track'] = $getsong1['track'];
											}
										}
										
										/*Code For taking songs Selected by User*/
											$get_selected_det = $newclassobj->get_selected_songs_videos_channel($_GET['edit']);
											$get_select_songs = explode(",",$get_selected_det['songs']);
										/*Code For taking songs Selected by User Ends Here*/
										
										$sort = array();
										foreach($get_songs_pnew_w as $k=>$v) {
										
											//$create_c = strpos($v['creator'],'(');
											//$end_c = substr($v['creator'],0,$create_c);
											$end_c = $v['creator'];
											
											//$create_f = strpos($v['from'],'(');
											//$end_f = substr($v['from'],0,$create_f);
											$end_f = $v['from'];
											
											$sort['creator'][$k] = $end_c;
											$sort['from'][$k] = $end_f;
											$sort['track'][$k] = $v['track'];
											$sort['title'][$k] = $v['title'];
										}
										//var_dump($sort);
										array_multisort($sort['creator'], SORT_ASC, $sort['from'], SORT_ASC, $sort['track'], SORT_ASC,$sort['title'], SORT_ASC,$get_songs_pnew_w);
										
										$dummy_s = array();
										for($acc_song=0;$acc_song<count($get_songs_pnew_w);$acc_song++)
										{
											if(isset($get_songs_pnew_w[$acc_song]['id']))
											{
												if(in_array($get_songs_pnew_w[$acc_song]['id'],$dummy_s))
												{
													
												}
												else
												{
													$dummy_s[] = $get_songs_pnew_w[$acc_song]['id'];
													if($get_songs_pnew_w[$acc_song]['id']!="" && $get_songs_pnew_w[$acc_song]['media_type']==114)
													{
														//$create_c = strpos($get_songs_pnew_w[$acc_song]['creator'],'(');
														//$end_c = substr($get_songs_pnew_w[$acc_song]['creator'],0,$create_c);
														$end_c = $get_songs_pnew_w[$acc_song]['creator'];
														
														//$create_f = strpos($get_songs_pnew_w[$acc_song]['from'],'(');
														//$end_f = substr($get_songs_pnew_w[$acc_song]['from'],0,$create_f);
														$end_f = $get_songs_pnew_w[$acc_song]['from'];
														
														$eceks = "";
														if($end_c!="")
														{
															$eceks = $end_c;
														}
														if($end_f!="")
														{
															if($end_c!="")
															{
																$eceks .= " : ".$end_f;
															}
															else
															{
																$eceks .= $end_f;
															}
														}
														if($get_songs_pnew_w[$acc_song]['track']!="")
														{
															if($end_c!="" || $end_f!="")
															{
																$eceks .= " : ".$get_songs_pnew_w[$acc_song]['track'];
															}
															else
															{
																$eceks .= $get_songs_pnew_w[$acc_song]['track'];
															}
														}
														if($get_songs_pnew_w[$acc_song]['title']!="")
														{
															if($end_c!="" || $end_f!="" || $get_songs_pnew_w[$acc_song]['track']!="")
															{
																$eceks .= " : ".$get_songs_pnew_w[$acc_song]['title'];
															}
															else
															{
																$eceks .= $get_songs_pnew_w[$acc_song]['title'];
															}
														}
														
														if($get_select_songs!="")
														{
															$get_select_songs = array_unique($get_select_songs);
															$yes_com_tag_pro_song =0;
															for($count_sel=0;$count_sel<=count($get_select_songs);$count_sel++)
															{
																if($get_select_songs[$count_sel]==""){continue;}
																else
																{
																	if($get_select_songs[$count_sel] == $get_songs_pnew_w[$acc_song]['id'])
																	{
																		$yes_com_tag_pro_song = 1;
										?>
																		<option value="<?php echo $get_songs_pnew_w[$acc_song]['id'];?>" selected><?php echo $eceks; ?></option>
										<?php
																	}
																}
															}
															if($yes_com_tag_pro_song == 0)
															{
										?>
																<option value="<?php echo $get_songs_pnew_w[$acc_song]['id'];?>"><?php echo $eceks; ?></option>
										<?php
															}
														}
														else
														{
			?>
															<option value="<?php echo $get_songs_pnew_w[$acc_song]['id'];?>" ><?php echo $eceks; ?></option>
			<?php
														}
													}
												}
											}
										}
										
										/* for($count_art_cr_song=0;$count_art_cr_song<count($get_songs_pnew);$count_art_cr_song++)
										{
											if($get_songs_pnew[$count_art_cr_song]!=""){
												
											$art_coms_a = '';
											if($get_media_cre_art_pro[$cr_i]['artist_id']!=0 && $get_media_cre_art_pro[$cr_i]['artist_id']!="")
											{
												$art_coms_a = 'artist~'.$get_media_cre_art_pro[$cr_i]['artist_id'];
											}
											elseif($get_media_cre_art_pro[$cr_i]['community_id']!=0 && $get_media_cre_art_pro[$cr_i]['community_id']!="")
											{
												$art_coms_a = 'community~'.$get_media_cre_art_pro[$cr_i]['community_id'];
											}
													
											$reg_meds = $newclassobj->get_media_reg_info($get_songs_pnew[$count_art_cr_song],$art_coms_a);
											if($reg_meds!=NULL && $reg_meds!='')
											{
												if($reg_meds=='general_artist_audio')
												{
													$sql_a = mysql_query("SELECT * FROM general_artist_audio WHERE audio_id='".$get_songs_pnew[$count_art_cr_song]."'");
													if(mysql_num_rows($sql_a)>0)
													{
														$ans_a = mysql_fetch_assoc($sql_a);
														$sql_gens = $newclassobj->sel_general_aid($get_media_cre_art_pro[$cr_i]['artist_id']);
															
														if($get_select_songs!="")
														{
															//$get_sel_song =$newclassobj->get_sel_songs($_GET['edit']);
															//$exp_song_id = explode(",",$get_sel_song['songs']);
															$yes_com_tag_pro_song =0;
															for($count_sela=0;$count_sela<=count($get_select_songs);$count_sela++)
															{
																if($get_select_songs[$count_sela]==""){continue;}
																else
																{
																	if($get_select_songs[$count_sela] == $ans_a['audio_id'])
																	{
																		$yes_com_tag_pro_song = 1;
					?>															
																		<option value="<?php echo $ans_a['audio_id'];?>" selected><?php echo substr($sql_gens['fname'] ." ". $sql_gens['lname'],0,20) ." : ".  $ans_a['audio_name'];?></option>
					<?php
																	}
																}
															}
															if($yes_com_tag_pro_song == 0)
															{												
					?>
																<option value="<?php echo $ans_a['audio_id'];?>"><?php echo substr($sql_gens['fname'] ." ". $sql_gens['lname'],0,20) ." : ".  $ans_a['audio_name'];?></option>
					<?php
															}
														}
														else
														{
					?>
															<option value="<?php echo $ans_a['audio_id'];?>"><?php echo substr($sql_gens['fname'] ." ". $sql_gens['lname'],0,20) ." : ".  $ans_a['audio_name'];?></option>
					<?php
														}
													}
												}
												if($reg_meds=='general_community_audio')
												{
													$sql_a = mysql_query("SELECT * FROM general_community_audio WHERE audio_id='".$get_songs_pnew[$count_art_cr_song]."'");
													if(mysql_num_rows($sql_a)>0)
													{
														$ans_a = mysql_fetch_assoc($sql_a);
														$sql_gens = $newclassobj->sel_general_cid($get_media_cre_art_pro[$cr_i]['community_id']);
															
														if($get_select_songs!="")
														{
															//$get_sel_song =$newclassobj->get_sel_songs($_GET['edit']);
															//$exp_song_id = explode(",",$get_sel_song['songs']);
															$yes_com_tag_pro_song =0;
															for($count_selc=0;$count_selc<=count($get_select_songs);$count_selc++)
															{
																if($get_select_songs[$count_selc]==""){continue;}
																else
																{
																	if($get_select_songs[$count_selc] == $ans_a['audio_id'])
																	{
																		$yes_com_tag_pro_song = 1;
					?>															
																		<option value="<?php echo $ans_a['audio_id'];?>" selected><?php echo substr($sql_gens['fname'] ." ". $sql_gens['lname'],0,20) ." : ".  $ans_a['audio_name'];?></option>
					<?php
																	}
																}
															}
															if($yes_com_tag_pro_song == 0)
															{												
					?>
																<option value="<?php echo $ans_a['audio_id'];?>"><?php echo substr($sql_gens['fname'] ." ". $sql_gens['lname'],0,20) ." : ".  $ans_a['audio_name'];?></option>
					<?php
															}
														}
														else
														{
					?>
															<option value="<?php echo $ans_a['audio_id'];?>"><?php echo substr($sql_gens['fname'] ." ". $sql_gens['lname'],0,20) ." : ".  $ans_a['audio_name'];?></option>
					<?php
														}
													}
												}
											}
											else
											{
												$get_media_tag_art_pro_song = $newclassobj->get_tag_media_info($get_songs_pnew[$count_art_cr_song]);
												if($get_select_songs!="")
												{
													//$get_sel_song =$newclassobj->get_sel_songs($_GET['edit']);
													//$exp_song_id = explode(",",$get_sel_song['songs']);
													$yes_com_tag_pro_song =0;
													for($count_sel=0;$count_sel<=count($get_select_songs);$count_sel++)
													{
														if($get_select_songs[$count_sel]==""){continue;}
														else
														{
															if($get_select_songs[$count_sel] == $get_media_tag_art_pro_song['id'])
															{
																$yes_com_tag_pro_song = 1;
					?>
																<option value="<?php echo $get_media_tag_art_pro_song['id'];?>" selected><?php echo substr($get_media_tag_art_pro_song['creator'],0,20) ." : ". substr($get_media_tag_art_pro_song['from'],0,20) ." : ".  $get_media_tag_art_pro_song['title'];?></option>
					<?php
															}
														}
													}
													if($yes_com_tag_pro_song == 0)
													{													
					?>
														<option value="<?php echo $get_media_tag_art_pro_song['id'];?>"><?php echo substr($get_media_tag_art_pro_song['creator'],0,20) ." : ". substr($get_media_tag_art_pro_song['from'],0,20) ." : ".  $get_media_tag_art_pro_song['title'];?></option>
					<?php
													}
												}
												else
												{
					?>
													<option value="<?php echo $get_media_tag_art_pro_song['id'];?>"><?php echo substr($get_media_tag_art_pro_song['creator'],0,20) ." : ". substr($get_media_tag_art_pro_song['from'],0,20) ." : ".  $get_media_tag_art_pro_song['title'];?></option>
					<?php
												}
											}
										}
									} */
										
									
										?>
                                        
                                    </select>
                                </div>
                                <div class="fieldCont">
                                  <div class="fieldTitle" title="Select the videos to be added to this channel.">Videos</div>
                                    <select size="5" multiple="multiple" class="list" name="channelvideos[]" id="channelvideos[]" title="Select the videos to be added to this channel.">
                                        <?php
										/* while($get_myvideo = mysql_fetch_assoc($getall_channel_video))
										{
											if(isset($_GET['edit']))
											{
												$get_sel_video =$newclassobj->get_sel_songs($_GET['edit']);
												$exp_video_id = explode(",",$get_sel_video['videos']);
												$yes_video =0;
												for($count_sel_video=0;$count_sel_video<=count($exp_video_id);$count_sel_video++)
												{
													if($exp_video_id[$count_sel_video]==""){continue;}
													else{
														if($exp_video_id[$count_sel_video] == $get_myvideo['id'])
														{
															$yes_video = 1;
														?>
															<option value="<?php echo $get_myvideo['id'];?>" selected><?php echo substr($get_myvideo['creator'],0,20) ." : ". substr($get_myvideo['from'],0,20) ." : ".  $get_myvideo['title'];?></option>
														<?php
														}
													}
												}
												if($yes_video == 0)
												{
												?>
													<option value="<?php echo $get_myvideo['id'];?>"><?php echo substr($get_myvideo['creator'],0,20) ." : ". substr($get_myvideo['from'],0,20) ." : ".  $get_myvideo['title'];?></option>
												<?php
												}
											}
											else{
												?>
													<option value="<?php echo $get_myvideo['id'];?>"><?php echo substr($get_myvideo['creator'],0,20) ." : ". substr($get_myvideo['from'],0,20) ." : ".  $get_myvideo['title'];?></option>
												<?php
											}
										} */
										
										/* $get_tagged_id_video = explode(",",$get_gene_info['accepted_media_id']);
										if(!empty($get_tagged_id_video))
										{
											for($tag_count_video=0;$tag_count_video<count($get_tagged_id_video);$tag_count_video++)
											{
												if($get_tagged_id_video[$tag_count_video]==""){ continue; }else{
													$get_tag_video =$newclassobj->get_all_tagged_video($get_tagged_id_video[$tag_count_video]);
													if(!empty($get_tag_video)){
														if(isset($_GET['edit']))
														{
															$get_sel_video_tag =$newclassobj->get_sel_songs($_GET['edit']);
															$exp_video_id_tag = explode(",",$get_sel_video_tag['videos']);
															$yes_tag_video =0;
															for($count_sel_tag_video=0;$count_sel_tag_video<=count($exp_video_id_tag);$count_sel_tag_video++)
															{
																if($exp_video_id_tag[$count_sel_tag_video]==""){continue;}
																else{
																	if($exp_video_id_tag[$count_sel_tag_video] == $get_tag_video['id'])
																	{
																		$yes_tag_video = 1;
																	?>
																		<option value="<?php echo $get_tag_video['id'];?>" selected><?php echo substr($get_tag_video['creator'],0,20) ." : ". substr($get_tag_video['from'],0,20) ." : ".  $get_tag_video['title'];?></option>
																	<?php
																	}
																}
															}
															if($yes_tag_video == 0)
															{
																
															?>
																<option value="<?php echo $get_tag_video['id'];?>"><?php echo substr($get_tag_video['creator'],0,20) ." : ". substr($get_tag_video['from'],0,20) ." : ".  $get_tag_video['title'];?></option>
															<?php
															}
														}
														else{
															?>
																<option value="<?php echo $get_tag_video['id'];?>"><?php echo substr($get_tag_video['creator'],0,20) ." : ". substr($get_tag_video['from'],0,20) ." : ".  $get_tag_video['title'];?></option>
															<?php
															}
													}
												}
											}
										} */
										
										$get_myvideo = array();
										while($get_myvideos = mysql_fetch_assoc($getall_channel_video))
										{
											$get_myvideo[] = $get_myvideos;
										}
										
										$get_vids_acc = array();
										$get_tagged_id_video = explode(",",$get_gene_info['accepted_media_id']);
										if(!empty($get_tagged_id_video))
										{
											for($tag_count_video=0;$tag_count_video<count($get_tagged_id_video);$tag_count_video++)
											{
												if($get_tagged_id_video[$tag_count_video]=="")
												{
													continue;
												}
												else
												{
													$get_tag_video =$newclassobj->get_all_tagged_video($get_tagged_id_video[$tag_count_video]);
													if(!empty($get_tag_video))
													{
														$get_vids_acc[] = $get_tag_video;
													}
												}
											}
										}
										
										$get_media_tag_art_pro = $newclassobj->get_artist_project_tag($_SESSION['login_email']);
										if(mysql_num_rows($get_media_tag_art_pro)>0)
										{
											$is_v = 0;
											while($art_pro_tag = mysql_fetch_assoc($get_media_tag_art_pro))
											{
												$get_videos = explode(",",$art_pro_tag['tagged_videos']);
												for($count_art_tag_video=0;$count_art_tag_video<count($get_videos);$count_art_tag_video++)
												{
													if($get_videos[$count_art_tag_video]!="")
													{
														$get_videos_new[$is_v] = $get_videos[$count_art_tag_video];
														$is_v = $is_v + 1;
													}
												}
											}
											$get_videos_n = array_unique($get_videos_new);
										}
										
										$get_media_tag_art_eve = $newclassobj->get_artist_event_tag($_SESSION['login_email']);
										if(mysql_num_rows($get_media_tag_art_eve)>0)
										{
											$is = 0;
											while($art_pro_tag = mysql_fetch_assoc($get_media_tag_art_eve))
											{
												$get_songs = explode(",",$art_pro_tag['tagged_videos']);
												for($count_art_tag_song=0;$count_art_tag_song<count($get_songs);$count_art_tag_song++)
												{
													if($get_songs[$count_art_tag_song]!="")
													{
														$get_cvsongs_new[$is] = $get_songs[$count_art_tag_song];
														$is = $is + 1;
													}
												}
											}
											$get_songs_yvn = array_unique($get_cvsongs_new);
										}
										
										$get_media_tag_com_pro = $newclassobj->get_community_project_tag($_SESSION['login_email']);
										if(mysql_num_rows($get_media_tag_com_pro)>0)
										{
											$is_vc = 0;
											while($com_pro_tag = mysql_fetch_assoc($get_media_tag_com_pro))
											{
												$get_com_videos = explode(",",$com_pro_tag['tagged_videos']);
												for($count_com_tag_video=0;$count_com_tag_video<count($get_com_videos);$count_com_tag_video++)
												{
													if($get_com_videos[$count_com_tag_video]!="")
													{
														$get_videos_c_new[$is_vc] = $get_com_videos[$count_com_tag_video];
														$is_vc = $is_vc + 1;
													}
												}
											}
											$get_videos_c_n = array_unique($get_videos_c_new);
										}
										
										$get_media_tag_com_eve = $newclassobj->get_community_event_tag($_SESSION['login_email']);
										if(mysql_num_rows($get_media_tag_com_eve)>0)
										{
											$is_c = 0;
											while($com_pro_tag = mysql_fetch_assoc($get_media_tag_com_eve))
											{
												$get_com_songs = explode(",",$com_pro_tag['tagged_videos']);
												for($count_com_tag_song=0;$count_com_tag_song<count($get_com_songs);$count_com_tag_song++)
												{
													if($get_com_songs[$count_com_tag_song]!="")
													{
														$get_vsongs_new_c[$is_c] = $get_com_songs[$count_com_tag_song];
														$is_c = $is_c + 1;
													}
												}
											}
											$get_songs_n_vc = array_unique($get_vsongs_new_c);
										}
										
										$get_media_cre_art_pro = $newclassobj->get_project_create();
										if(!empty($get_media_cre_art_pro))
										{
											$is_v_c = 0;
											//while($art_pro_tag = mysql_fetch_assoc($get_media_cre_art_pro))
											for($cr_i=0;$cr_i<count($get_media_cre_art_pro);$cr_i++)
											{
												$get_cvideos = explode(",",$get_media_cre_art_pro[$cr_i]['tagged_videos']);

												for($count_art_cr_video=0;$count_art_cr_video<count($get_cvideos);$count_art_cr_video++)
												{
													if($get_cvideos[$count_art_cr_video]!="")
													{
														$get_videos__new[$is_v_c] = $get_cvideos[$count_art_cr_video];
														$is_v_c = $is_v_c + 1;
													}
												}
											}
											$get_videos__n = array_unique($get_videos__new);
										}
										
										/*This function will display the user in which profile he is creator and display media where that profile was creator*/
										$new_medi = $newclassobj->get_creator_heis();
										$new_creator_creator_media_array = array();
										if(!empty($new_medi))
										{
											for($count_cre_m=0;$count_cre_m<count($new_medi);$count_cre_m++)
											{
												if($new_medi[$count_cre_m]['media_type']==115){
													$new_creator_creator_media_array[] = $new_medi[$count_cre_m];
												}
											}
										}
										/*This function will display the user in which profile he is creator and display media where that profile was creator Ends here*/
										
										/*This function will display the user in which profile he is from and display media where that profile was creator*/
										$new_medi_from = $newclassobj->get_from_heis();
										$new_from_from_media_array = array();
										if(!empty($new_medi_from))
										{
											for($count_cre_m=0;$count_cre_m<count($new_medi_from);$count_cre_m++)
											{
												if($new_medi_from[$count_cre_m]['media_type']==115){
												$new_from_from_media_array[] = $new_medi_from[$count_cre_m];
												}
											}
										}

										/*This function will display the user in which profile he is from and display media where that profile was creator Ends here*/
										
										
										/*This function will display the user in which profile he is from or creator and tagged and display media where that profile was creator*/
										$new_medi_tagged = $newclassobj->get_the_user_tagin_project($_SESSION['login_email']);
										$new_tagged_from_media_array = array();
										if(!empty($new_medi_tagged))
										{
											for($count_cre_m=0;$count_cre_m<count($new_medi_tagged);$count_cre_m++)
											{
												if($new_medi_tagged[$count_cre_m]['media_type']==115){
												$new_tagged_from_media_array[] = $new_medi_tagged[$count_cre_m];
												}
											}
										}

										/*This function will display the user in which profile he is from or creator and tagged and display media where that profile was creator Ends here*/
										
										
										/*This function will display in which media the curent user was creator*/
										$get_where_creator_media = $newclassobj->get_media_where_creator_or_from();
										$new_creator_media_array = array();
										if(!empty($get_where_creator_media))
										{
											for($count_tag_m=0;$count_tag_m<count($get_where_creator_media);$count_tag_m++)
											{
												if($get_where_creator_media[$count_tag_m]['media_type']==115){
													$new_creator_media_array[] = $get_where_creator_media[$count_tag_m];
												}
											}
										}
										/*This function will display in which media the curent user was creator ends here*/
										
										
										if(!isset($get_songs_yvn))
										{
											$get_songs_yvn = array();
										}
										
										if(!isset($get_songs_n_vc))
										{
											$get_songs_n_vc = array();
										}
										
										if(!isset($get_videos_n))
										{
											$get_videos_n = array();
										}
										
										if(!isset($get_videos_c_n))
										{
											$get_videos_c_n = array();
										}
										
										if(!isset($get_videos__n))
										{
											$get_videos__n = array();
										}
										
										$get_videos_n_n = array_merge($get_videos_n,$get_videos_c_n,$get_videos__n,$get_songs_yvn,$get_songs_n_vc);
										
										$get_videos_n_new = array_unique($get_videos_n_n);
										$get_vids_final_ct = array();

										
										for($count_art_cr_video=0;$count_art_cr_video<count($get_videos_n_new);$count_art_cr_video++)
										{
											if($get_videos_n_new[$count_art_cr_video]!="")
											{
												$get_media_tag_art_pro_video = $newclassobj->get_tag_media_info($get_videos_n_new[$count_art_cr_video]);
												$get_vids_final_ct[] = $get_media_tag_art_pro_video;
											}
										}
										
										$new_subscription_media = array();
										$get_subscription_media_res = $newclassobj->get_subscription_media();
										for($count_sub=0;$count_sub<=count($get_subscription_media_res);$count_sub++)
										{
											if($get_subscription_media_res[$count_sub]!="")
											{
												//$get_media_type = $newclassobj->sel_type_of_tagged_media($get_subscription_media_res[$count_sub]['media_type']);
												if($get_subscription_media_res[$count_sub]['media_type']==115)
												{
													//$get_subscription_media_res[$count_sub]['name'] = $get_media_type['name'];
													$new_subscription_media[] = $get_subscription_media_res[$count_sub];
												}
											}
										}
										
										$get_vids_pnew_w = array_merge($get_vids_acc,$get_myvideo,$get_vids_final_ct,$new_creator_creator_media_array,$new_from_from_media_array,$new_tagged_from_media_array,$new_creator_media_array,$new_subscription_media);
										
										/* for($acc_song1=0;$acc_song1<count($get_vids_pnew_w);$acc_song1++)
										{
											if($get_vids_pnew_w[$acc_song1]['id']!="" && $get_vids_pnew_w[$acc_song1]['id']!=0)
											{
												$get_artist_song1 = $newclassobj->get_artist_song($get_vids_pnew_w[$acc_song1]['id']);
												$getsong1 = mysql_fetch_assoc($get_artist_song1);
												$ids = $get_vids_pnew_w[$acc_song1]['id'];
												$get_vids_pnew_w[$acc_song1]['track'] = $getsong1['track'];
											}
										} */

										/*Code For taking songs Selected by User*/
											$get_selected_det = $newclassobj->get_selected_songs_videos_channel($_GET['edit']);
											$get_select_videos = explode(",",$get_selected_det['videos']);
										/*Code For taking songs Selected by User Ends Here*/
										
										$sort = array();
										foreach($get_vids_pnew_w as $k=>$v) {
										
											//$create_c = strpos($v['creator'],'(');
											//$end_c = substr($v['creator'],0,$create_c);
											$end_c = $v['creator'];
											
											//$create_f = strpos($v['from'],'(');
											//$end_f = substr($v['from'],0,$create_f);
											$end_f = $v['from'];
											
											$sort['creator'][$k] = $end_c;
											$sort['from'][$k] = $end_f;
											//$sort['track'][$k] = $v['track'];
											$sort['title'][$k] = $v['title'];
										}
										//var_dump($sort);
										array_multisort($sort['creator'], SORT_ASC, $sort['from'], SORT_ASC, $sort['title'], SORT_ASC,$get_vids_pnew_w);
										
										$dummy_v = array();
										for($acc_vide=0;$acc_vide<count($get_vids_pnew_w);$acc_vide++)
										{
											if(isset($get_vids_pnew_w[$acc_vide]['id']))
											{
												if(in_array($get_vids_pnew_w[$acc_vide]['id'],$dummy_v))
												{

												}
												else
												{
													$dummy_v[] = $get_vids_pnew_w[$acc_vide]['id'];
													if($get_vids_pnew_w[$acc_vide]['id']!="" && $get_vids_pnew_w[$acc_vide]['media_type']==115)
													{
														//$create_c = strpos($get_vids_pnew_w[$acc_vide]['creator'],'(');
														//$end_c = substr($get_vids_pnew_w[$acc_vide]['creator'],0,$create_c);
														$end_c = $get_vids_pnew_w[$acc_vide]['creator'];
														
														//$create_f = strpos($get_vids_pnew_w[$acc_vide]['from'],'(');
														//$end_f = substr($get_vids_pnew_w[$acc_vide]['from'],0,$create_f);
														$end_f = $get_vids_pnew_w[$acc_vide]['from'];
														
														$eceksv = "";
														if($end_c!="")
														{
															$eceksv = $end_c;
														}
														if($end_f!="")
														{
															if($end_c!="")
															{
																$eceksv .= " : ".$end_f;
															}
															else
															{
																$eceksv .= $end_f;
															}
														}
														if($get_vids_pnew_w[$acc_vide]['title']!="")
														{
															if($end_c!="" || $end_f!="")
															{
																$eceksv .= " : ".$get_vids_pnew_w[$acc_vide]['title'];
															}
															else
															{
																$eceksv .= $get_vids_pnew_w[$acc_vide]['title'];
															}
														}
														
														if($get_select_videos!="")
														{
															$get_select_videos = array_unique($get_select_videos);
															$yes_com_tag_pro_song =0;
															for($count_sel=0;$count_sel<=count($get_select_videos);$count_sel++)
															{
																if($get_select_videos[$count_sel]==""){continue;}
																else
																{
																	if($get_select_videos[$count_sel] == $get_vids_pnew_w[$acc_vide]['id'])
																	{
																		$yes_com_tag_pro_song = 1;
										?>
																		<option value="<?php echo $get_vids_pnew_w[$acc_vide]['id'];?>" selected><?php echo $eceksv; ?></option>
										<?php
																	}
																}
															}
															if($yes_com_tag_pro_song == 0)
															{													
										?>
																<option value="<?php echo $get_vids_pnew_w[$acc_vide]['id'];?>"><?php echo $eceksv; ?></option>
										<?php
															}
														}
														else
														{
			?>
															<option value="<?php echo $get_vids_pnew_w[$acc_vide]['id'];?>" ><?php echo $eceksv; ?></option>
			<?php
														}
													}
												}
											}
										}

											/* for($count_art_cr_video=0;$count_art_cr_video<count($get_videos_n_new);$count_art_cr_video++)
												{
													if($get_videos_n_new[$count_art_cr_video]!=""){
													
													$art_coms_v = '';
													if($get_media_cre_art_pro[$cr_i]['artist_id']!=0 && $get_media_cre_art_pro[$cr_i]['artist_id']!="")
													{
														$art_coms_v = 'artist~'.$get_media_cre_art_pro[$cr_i]['artist_id'];
													}
													elseif($get_media_cre_art_pro[$cr_i]['community_id']!=0 && $get_media_cre_art_pro[$cr_i]['community_id']!="")
													{
														$art_coms_v = 'community~'.$get_media_cre_art_pro[$cr_i]['community_id'];
													}
													
													$reg_meds = $newclassobj->get_media_reg_info($get_videos_n_new[$count_art_cr_video],$art_coms_v);
													if($reg_meds!=NULL && $reg_meds!='')
													{
														if($reg_meds=='general_artist_video')
														{
															$sql_a = mysql_query("SELECT * FROM general_artist_video WHERE video_id='".$get_videos_n_new[$count_art_cr_video]."'");
															if(mysql_num_rows($sql_a)>0)
															{
																$ans_a = mysql_fetch_assoc($sql_a);
																$sql_gens = $newclassobj->sel_general_aid($get_media_cre_art_pro[$cr_i]['artist_id']);
																
																if($get_select_videos!="")
																{
																	//$get_sel_song =$newclassobj->get_sel_songs($_GET['edit']);
																	//$exp_song_id = explode(",",$get_sel_song['songs']);
																	$yes_com_tag_pro_song =0;
																	for($count_selv=0;$count_selv<=count($get_select_videos);$count_selv++)
																	{
																		if($get_select_videos[$count_selv]==""){continue;}
																		else{
																			if($get_select_videos[$count_selv] == $ans_a['video_id'])
																			{
																				$yes_com_tag_pro_song = 1;
																			?>															
																				<option value="<?php echo $ans_a['video_id'];?>" selected><?php echo substr($sql_gens['fname'] ." ". $sql_gens['lname'],0,20) ." : ".  $ans_a['video_name'];?></option>
																			<?php
																			}
																		}
																	}
																	if($yes_com_tag_pro_song == 0)
																	{
																		
																	?>
																		<option value="<?php echo $ans_a['video_id'];?>"><?php echo substr($sql_gens['fname'] ." ". $sql_gens['lname'],0,20) ." : ".  $ans_a['video_name'];?></option>
																	<?php
																	}
																}
																else{
																?>
																	<option value="<?php echo $ans_a['video_id'];?>"><?php echo substr($sql_gens['fname'] ." ". $sql_gens['lname'],0,20) ." : ".  $ans_a['video_name'];?></option>
																<?php
																	}
															}
														}
														if($reg_meds=='general_community_video')
														{
															$sql_a = mysql_query("SELECT * FROM general_community_video WHERE video_id='".$get_videos_n_new[$count_art_cr_video]."'");
															if(mysql_num_rows($sql_a)>0)
															{
																$ans_a = mysql_fetch_assoc($sql_a);
																$sql_gens = $newclassobj->sel_general_cid($get_media_cre_art_pro[$cr_i]['community_id']);
																
																if($get_select_videos!="")
																{
																	//$get_sel_song =$newclassobj->get_sel_songs($_GET['edit']);
																	//$exp_song_id = explode(",",$get_sel_song['songs']);
																	$yes_com_tag_pro_song =0;
																	for($count_selvc=0;$count_selvc<=count($get_select_videos);$count_selvc++)
																	{
																		if($get_select_videos[$count_selvc]==""){continue;}
																		else{
																			if($get_select_videos[$count_selvc] == $ans_a['video_id'])
																			{
																				$yes_com_tag_pro_song = 1;
																			?>															
																				<option value="<?php echo $ans_a['video_id'];?>" selected><?php echo substr($sql_gens['fname'] ." ". $sql_gens['lname'],0,20) ." : ".  $ans_a['video_name'];?></option>
																			<?php
																			}
																		}
																	}
																	if($yes_com_tag_pro_song == 0)
																	{
																		
																	?>
																		<option value="<?php echo $ans_a['video_id'];?>"><?php echo substr($sql_gens['fname'] ." ". $sql_gens['lname'],0,20) ." : ".  $ans_a['video_name'];?></option>
																	<?php
																	}
																}
																else{
																?>
																	<option value="<?php echo $ans_a['video_id'];?>"><?php echo substr($sql_gens['fname'] ." ". $sql_gens['lname'],0,20) ." : ".  $ans_a['video_name'];?></option>
																<?php
																	}
															}
														}
													}
													else
													{
														$get_media_tag_art_pro_video = $newclassobj->get_tag_media_info($get_videos_n_new[$count_art_cr_video]);
														if($get_select_videos!="")
														{
															//$get_sel_song =$newclassobj->get_sel_songs($_GET['edit']);
															//$exp_video_id = explode(",",$get_sel_video['taggedvideos']);
															$yes_tag_pro_video =0;
															for($count_sel=0;$count_sel<=count($get_select_videos);$count_sel++)
															{
																if($get_select_videos[$count_sel]==""){continue;}
																else{
																	if($get_select_videos[$count_sel] == $get_media_tag_art_pro_video['id'])
																	{
																		$yes_tag_pro_video = 1;
																	?>
																		<option value="<?php echo $get_media_tag_art_pro_video['id'];?>" selected><?php echo substr($get_media_tag_art_pro_video['creator'],0,20) ." : ". substr($get_media_tag_art_pro_video['from'],0,20) ." : ".  $get_media_tag_art_pro_video['title'];?></option>
																	<?php
																	}
																}
															}
															if($yes_tag_pro_video == 0)
															{
																
															?>
																<option value="<?php echo $get_media_tag_art_pro_video['id'];?>"><?php echo substr($get_media_tag_art_pro_video['creator'],0,20) ." : ". substr($get_media_tag_art_pro_video['from'],0,20) ." : ".  $get_media_tag_art_pro_video['title'];?></option>
															<?php
															}
														}
														else{
														?>
															<option value="<?php echo $get_media_tag_art_pro_video['id'];?>"><?php echo substr($get_media_tag_art_pro_video['creator'],0,20) ." : ". substr($get_media_tag_art_pro_video['from'],0,20) ." : ".  $get_media_tag_art_pro_video['title'];?></option>
														<?php
															}
														}
													}
												} */
										?>
                                    </select>
                                </div>
                            </div>
							<div class="fieldCont" id="type_association" style="display:none;">
								<h4>Type Association</h4>
								
								<div style="font-size:12px;" class="fieldCont">Select type associations to represent your profile. Your first selections will display on your profile page, all others will be used for search engines. If you would like to change your display than remove all types and select your desired display type first.<br><br>Suggest a new type or subtype by emailing info@purifyart.com</div>
								<div class="fieldCont">
                                  <div class="fieldTitle">*Sub Type</div>
                                       <select name="subtype-select1[]" class="dropdown" id="subtype-select1">
											<option value="0" selected="selected">Select Subtype</option>
									   </select>
								</div>
                                <div class="fieldCont">
                                <div class="fieldTitle">Meta Type</div>
                                    <select name="metatype-select1[]" class="dropdown" id="metatype-select1">
                                        <option value="0" selected="selected">Select Metatype</option>					
                    
                                    </select>
                                  
                                  <div class="hint"><input name="ADD" type="button" id="addTypechannel" value=" Add "></div>
                                </div>
							</div>
							<div  id="selected-types" style="float:right;"></div>
							<?php
							if(!isset($_GET['edit']))
							{
							?>
								<div class="fieldCont" id="chk_terms">
									<div class="fieldTitle"></div>
									<input type="checkbox" style="width:14px;" class="fieldText" name="chk_privacy" id="chk_privacy" value="" />&nbsp; I have read the <a href="about_agreements_user.php" target="_blank">Terms of Use</a> and <a target="_blank" href="about_report_copyright_infringement.php">Intellectual Property Policy</a>.
								</div>
							<?php
							}
							else
							{
							?>
								<!--<div class="fieldCont" id="chk_terms" style="display:none;">
									<div class="fieldTitle"></div>
									<input type="checkbox" checked style="width:0;" class="fieldText" name="chk_privacy" id="chk_privacy" value="" />&nbsp;  I have read the Terms of Use and Intellectual Property Policy. 
								</div>-->
							<?php
							}
							?>
							
								<div class="fieldCont">
                                    <div class="fieldTitle"></div>
                                    <input type="submit" class="register" value="Save" id="insert_new_media" />
                                </div>
								
								</form>
						</div>
					</div>
				</div>
                
                
                
                
                <div class="subTabs" id="storage" style="display:none;"><h1>Storage</h1><br /><p><strong>Add File: </strong><em>If you are uploading multiple files, compress them into a single file using zip or rar compression.</em></p>
      <p><strong>Add User who may access file: </strong><em>List registered user </em></p>
      <p><strong> List of Users who may access file: </strong>Kaz, Granny Beats </p>
      <p>UPLOAD / DELETE </p>
      <p><strong>Total Library Space: </strong>5,000 MB </p>
      <p><strong>Library Space Left: </strong>3,054 MB </p>
      <p><strong>Stored Files</strong></p>
      <p><a href="#">02.12.09, 12:43, 243 MB, LeeRick_Untitiled_01.zip</a> (edit/download) <br />
        <a href="#">02.14.09, 12:45, 192 MB, LeeRick_Untitiled_02.zip</a> (edit/download) <br />
        <a href="#">02.19.09, 12:47, 262 MB, LeeRick_Untitiled_03.zip</a> (edit/download) <br />
        <a href="#">02.22.09, 12:49, 302 MB, LeeRick_Untitiled_04.zip</a> (edit/download) <br />
        <a href="#">03.02.09, 12:52, 212 MB, LeeRick_Untitiled_05.zip</a> (edit/download) </p></div>
                
                
                
                
               </div>
          </div>  
          
           
          
          <p></p>
   </div>
</div>
  <!-- end of main container -->
</div><?php include_once("displayfooter.php"); ?>
<script>
var container = $('#container');
</script>

<!--files for search technique
<script src="ui/jquery-1.7.2.js"></script>
<script src="ui/jquery.ui.core.js"></script>
<script src="ui/jquery.ui.widget.js"></script>
<script type="text/javascript" src="javascripts/jquery.ui.position.js"></script>	
<script type="text/javascript" src="javascripts/jquery.ui.autocomplete.js"></script>
<link  rel="stylesheet" type="text/css" media="screen" href="css/jquery.ui.autocomplete.css"></link>
<link rel="stylesheet" href="includes/jquery.ui.all.css">-->
<!--<link rel="stylesheet" href="css/jquery.autocomplete.css" type="text/css" />-->

<!--files for search technique ends here 


<style>
	.ui-autocomplete-loading { background: white url('css/ui-anim_basic_16x16.gif') right center no-repeat; }
</style>-->
<script type="text/javascript">
	$(function() {
		$( "#taguser" ).autocomplete({
			source: function(request, response) {
				$.ajax({
				  url: "usersuggestion.php",
				  dataType: "json",
				  data: request,                    
				  success: function (data) {
					// No matching result
					if (data.length == 0) {
					  document.getElementById("taguser").value ="";
						$(".ui-autocomplete-loading").css({"background" : "none"});
						$("#user_error").css({"display" : "block","color" : "red","width":"200px","position":"relative","left":"107px"});
					}
					else {
					  response(data);
					  $("#user_error").css({"display" : "none"});
					}
				  }});
				},
			minLength: 1,
			select: function( event, ui ) {
			document.getElementById("useremail_old").value = ui.item.value1;
			$("#user_error").css({"display" : "none"});
			//document.getElementById("venuewebsite").value=ui.item.value2;
			},
			change: function(event, ui) {
				console.log(this.value);
				if (ui.item == null) {
					document.getElementById("taguser").value="";
					$("#user_error").css({"display" : "block","color" : "red","width":"200px","position":"relative","left":"107px"});
				} 
			}
		});
	});
	
	$(function() {
		$( "#creator" ).autocomplete({
			source: function(request, response) {
				$.ajax({
				  url: "usersuggestion_Creator.php",
				  dataType: "json",
				  data: request,                    
				  success: function (data) {
					// No matching result
					if (data.length == 0) {
						if($("#sharingpreference").val()!=0)
						{
							document.getElementById("creator").value ="";
							$(".ui-autocomplete-loading").css({"background" : "none"});
							$("#creator_error").css({"display" : "block","color" : "red","width":"200px","position":"relative","left":"107px"});
						}
						else
						{
							document.getElementById("creator").value ="";
							$(".ui-autocomplete-loading").css({"background" : "none"});
							$("#creator_error").css({"display" : "block","color" : "red","width":"200px","position":"relative","left":"107px"});
						}
					}
					else {
						$(".ui-autocomplete-loading").css({"background" : "none"});
					  response(data);
					}
				  }});
				},
			minLength: 1,
			select: function( event, ui ) {
				$("#creator_error").css({"display" : "none"});
				document.getElementById("creator_info").value = ui.item.value2;
			},
			change: function(event, ui) {
				console.log(this.value);
				if (ui.item == null) {
					document.getElementById("creator").value="";
					$("#creator_error").css({"display" : "block","color" : "red","width":"200px","position":"relative","left":"107px"});
				} 
			}
		});
	});
	$(function() {
		$( "#from" ).autocomplete({
			source: function(request, response) {
				$.ajax({
				  url: "usersuggestion_From.php",
				  dataType: "json",
				  data: request,                    
				  success: function (data) {
					if (data.length == 0) {
						if($("#sharingpreference").val()!=0)
						{
							document.getElementById("from").value ="";
							$(".ui-autocomplete-loading").css({"background" : "none"});
							$("#from_error").css({"display" : "block","color" : "red","width":"200px","position":"relative","left":"107px"});
						}
						else
						{
							document.getElementById("from").value ="";
							$(".ui-autocomplete-loading").css({"background" : "none"});
							$("#from_error").css({"display" : "block","color" : "red","width":"200px","position":"relative","left":"107px"});
						}
					}
					else {
						$(".ui-autocomplete-loading").css({"background" : "none"});
					  response(data);
					}
				  }});
				},
			minLength: 1,
			select: function( event, ui ) {
				$("#from_error").css({"display" : "none"});
				document.getElementById("from_info").value = ui.item.value2;
			},
			change: function(event, ui) {
				console.log(this.value);
				if (ui.item == null) {
					document.getElementById("from").value="";
					$("#from_error").css({"display" : "block","color" : "red","width":"200px","position":"relative","left":"107px"});
				} 
			}
		});
	});
	
function showsong_upload(){
	document.getElementById("song_uploader").click();
}function show_uploading_options(){
	//document.getElementById("select_upload_to").style.display="block";
	//document.getElementById('song_uploader').click();
}function show_video_uploading_options(){
	//document.getElementById("videoUploadoptions").style.display="block";
}

function find_s3_account(detail,id)
{
	if(detail == "blank"){
		document.getElementById("popup_s3_error").click();
		if(document.getElementById("upload_song_link1")!=null){
			document.getElementById("upload_song_link1").checked = false;
		}if(document.getElementById("upload_s3_video")!=null){
			document.getElementById("upload_s3_video").checked = false;
		}if(document.getElementById("upload_gallery_link")!=null){
			document.getElementById("upload_gallery_link").checked = false;
		}
		
		if(final_value < 100){
		document.getElementById(id).click();
		}
	}
}
$("#upload_song_link").click(function(){
document.getElementById("song_uploader").checked = false;
});
$("#upload_video_link").click(function(){
document.getElementById("video_uploader").checked = false;
});
</script>				
</body>
<script src="mediatype.js" type="text/javascript"></script>

<?php
if(isset($_GET['edit']))
{
?>
  <script type="text/javascript">
		$(document).ready(function() {  
			$('#type-select').val('<?php echo $editmedia['media_type'];?>');
			$('#type-select').change();
		});
  </script>
<?php
}
if(isset($_GET['adds']))
{
	if($_GET['adds']!="")
	{
?>
		<script type="text/javascript">
			$("document").ready(function(){
				document.getElementById("type-select").value = <?php echo $_GET['adds'] ?>;
				$("#type-select").change();
				if(document.getElementById("type-select").value==116)
				{
					$("#chk_terms").css({"display":"none"});
				}
			});
		</script>
<?php
	}
}
?>
</html>