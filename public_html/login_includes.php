<?php
	include_once('commons/session_check.php');
	include_once('classes/User.php');
	include_once('classes/UserSubscription.php');
	
	//session_start();
	// $username = $_SESSION['username'];
	// if(!$username == '')
	// {
		// header('Location:index.php');
	// }
	if(isset($_POST['go']))
	{
		$username=$_POST['username'];
		$password=$_POST['password'];
		$obj=new User();
		if($obj->verify($username,$password))
		{
			$row=$obj->getUserInfo($username);
			if(($row['user_type']=='fan')&&($row['status']=='0'))
			{	
				$obj2=new UserSubscription();
				$rs2=$obj2->getUserSubscription($row['user_id']);
				$row2=mysql_fetch_assoc($rs2);
				if($row2['payment_mode']=="Google Checkout")
					header('location:registration_fan_incomplete_gc.php?uid='.$row['user_id'].'&&sid='.$row2['subscription_id']);
				else if($row2['payment_mode']=="PayPal")
					header('location:registration_fan_incomplete_pp.php?uid='.$row['user_id'].'&&sid='.$row2['subscription_id']);
				else
					$invalid_user="Invalid username or password.";
			}
			else if(($row['status']=='1')&&($row['verified']=='0'))
			{
				$invalid_user="Verify email address first.";
			}
			else
			{
				$_SESSION['username']=$username;
				header('location:index.php');
			}
		}
		else
		{
			$invalid_user="Invalid username or password.";
			//echo $invalid_user;
		}
	}