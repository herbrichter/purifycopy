<?php 
	//Included files
	include_once('../commons/db.php');
	include('classes/GeneralArtist.php');
	include_once('../sendgrid/SendGrid_loader.php');
	
	$user_id = $_REQUEST['guid'];
	$artist_id = $_REQUEST['gaid'];
	$status = $_REQUEST['status'];
	$sendgrid = new SendGrid('','');
	
	echo $user_id." ".$artist_id." ".$status;
	
	//Object created to display data from from Manage class
	$obj= new GeneralArtist();
	$reply=$obj->setStatus($artist_id,$status);
	$userInfo=$obj->generalUserByArtistId($artist_id);
	$row = mysql_fetch_assoc($userInfo);
	if($reply){
		// Mail sent for aprove
		$to=$row['email'];
		$subject="You artist profile was approved";
		$header="from: Purify Art < no-reply@purifyart.com>"."\r\n";
		$message="You artist profile was approved. Login to begin using it."; 
		// send email
		//$sentmail = mail($to,$subject,$message,$header);
		
		$url = "admin/newsletter/"; 
			$myFile = $url . "other_templates.txt";
			$fh = fopen($myFile, 'r');
			$theData = fread($fh, filesize($myFile));
			fclose($fh);
			
			$data_email = explode('|~|', $theData);
			
			$ns_user_n = str_replace('{::message::}', $message, $data_email['1']);
			
			$body_email = $data_email['0'] . $ns_user_n . $data_email['2'];
		
		$mail_grid = new SendGrid\Mail();

		$mail_grid->addTo($to)->
			   setFromName('Purify Art')->
				setFrom('no-reply@purifyart.com')->
			   setSubject($subject)->
			   setHtml($body_email);
		//$sendgrid->web->send($mail_grid);
		$sendgrid -> smtp -> send($mail_grid); 
		
	}else{
		// Mail sent for Deny
		$to=$row['email'];
		$subject="You artist profile was denied";
		$header="from: Purify Art < no-reply@purifyart.com>"."\r\n";
		$message="Your artist profile request was denied because your uploaded media and/or links were not deemed a high enough quality or they were not related to \"Art\". Please register again if you feel this was in error or email info@purifyart.com";
		// send email
		//$sentmail = mail($to,$subject,$message,$header);
		$url = "admin/newsletter/"; 
			$myFile = $url . "other_templates.txt";
			$fh = fopen($myFile, 'r');
			$theData = fread($fh, filesize($myFile));
			fclose($fh);
			
			$data_email = explode('|~|', $theData);
			
			$ns_user_n = str_replace('{::message::}', $message, $data_email['1']);
			
			$body_email = $data_email['0'] . $ns_user_n . $data_email['2'];
			
		$mail_grid = new SendGrid\Mail();

		$mail_grid->addTo($to)->
			   setFromName('Purify Art')->
				setFrom('no-reply@purifyart.com')->
			   setSubject($subject)->
			   setHtml($body_email);
		//$sendgrid->web->send($mail_grid);
		$sendgrid -> smtp -> send($mail_grid); 
	}
	
	echo $reply
?>
