<?php 
include_once('../../commons/db.php');

class Media
{

	function addGalleryImagesEntry($title,$files,$profile_id,$profile_type)
	{
		$query="insert into general_".$profile_type."_gallery_list (`gallery_title`,`created_date`) values ('".$title."',CURDATE())";
		$rs=mysql_query($query) or die(mysql_error());
		$gallery_id=mysql_insert_id();
		if($gallery_id){
			$fileList = explode("|",$files);
			for($i=0;$i<count($fileList);$i++){
				if($fileList[$i]!=""){
					$query="insert into general_".$profile_type."_gallery (`profile_id`,`gallery_id`,`image_name`,`created_date`) values ('$profile_id','".$gallery_id."','".$fileList[$i]."',CURDATE())";
					$rs=mysql_query($query) or die(mysql_error());
				}
			}
		}
	}

	function addMediaAudiosEntry($title,$files,$profile_id,$profile_type)
	{
		$fileList = explode("|",$files);
		for($i=0;$i<count($fileList);$i++){
			if($fileList[$i]!=""){
				$query="insert into general_".$profile_type."_audio (`profile_id`,`audio_name`,`audio_file_name`,`created_date`) values ('$profile_id','".$title."','".$fileList[$i]."',CURDATE())";
				$rs=mysql_query($query) or die(mysql_error());
			}
		}
	}

	function addMediaAudioLinkEntry($title,$link,$profile_id,$profile_type)
	{
		if($link!=""){
			$query="insert into general_".$profile_type."_audio (`profile_id`,`audio_name`,`audio_link`,`created_date`) values ('$profile_id','".$title."','".$link."',CURDATE())";
			$rs=mysql_query($query) or die(mysql_error());
		}
	}

	function addMediaVideoLinkEntry($title,$link,$profile_id,$profile_type)
	{
		if($link!=""){
			$query="insert into general_".$profile_type."_video (`profile_id`,`video_name`,`video_link`,`created_date`) values ('$profile_id','".$title."','".$link."',CURDATE())";
			$rs=mysql_query($query) or die(mysql_error());
		}
	}

	function getGalleryImagesByProfileId($profile_id,$profile_type)
	{
		$sql="select image_name from general_".$profile_type."_gallery where profile_id='".$profile_id."'";
		$rs=mysql_query($sql) or die(mysql_error());
		return mysql_fetch_assoc($rs);	  	
	}  

	function searchMediaByProfileId($profile_id,$profile_type){
		$mediaType = "";
		$resultMediaArray = Array();
		$sql="SELECT L.*,G.image_name,G.image_title FROM general_".$profile_type."_gallery as G, general_".$profile_type."_gallery_list as L 
			WHERE L.gallery_id=G.gallery_id 
			AND G.profile_id='".$profile_id."' GROUP BY L.gallery_id";
		$rs=mysql_query($sql) or die(mysql_error());
		if(mysql_num_rows($rs)){
			$mediaType = "gallery";
			$resultArray = Array();
			$gCount = 0;
			while ($row = mysql_fetch_assoc($rs)) {
				$resultArray[$gCount][0] = $row;
				$imagesArr = Array();
				$sql1="SELECT G.* FROM general_".$profile_type."_gallery as G, general_".$profile_type."_gallery_list as L 
					WHERE L.gallery_id=G.gallery_id 
					AND G.profile_id='".$profile_id."' AND L.gallery_id='".$row['gallery_id']."'";
				$rs1=mysql_query($sql1) or die(mysql_error());
				if(mysql_num_rows($rs1)){
					while ($row1 = mysql_fetch_assoc($rs1)) {						
						$imagesArr[] = $row1;						
					}
					$resultArray[$gCount][1] = $imagesArr;
					$gCount++;
				}
			}

			$resultMediaArray[0][0] = $mediaType;
			$resultMediaArray[0][1] = $resultArray;
		}
		
		$sql="SELECT * FROM general_".$profile_type."_audio WHERE profile_id='".$profile_id."'";
		$rs=mysql_query($sql) or die(mysql_error());
		if(mysql_num_rows($rs)){
			$mediaType = "audio";
			$resultArray = Array();
			while ($row = mysql_fetch_assoc($rs)) {
				$resultArray[] = $row;
			}			
			$resultMediaArray[1][0] = $mediaType;
			$resultMediaArray[1][1] = $resultArray;
		}
				
		$sql="SELECT * FROM general_".$profile_type."_video WHERE profile_id='".$profile_id."'";
		$rs=mysql_query($sql) or die(mysql_error());
		if(mysql_num_rows($rs)){
			$mediaType = "video";
			$resultArray = Array();
			while ($row = mysql_fetch_assoc($rs)) {
				$resultArray[] = $row;
			}			
			$resultMediaArray[2][0] = $mediaType;
			$resultMediaArray[2][1] = $resultArray;
		}
				
		return $resultMediaArray;
	}
	
	function getMediaById($type,$media_id,$profile_id,$profile_type){
		switch($type){
			case "video":
			case "v":
						$sql="SELECT * FROM general_".$profile_type."_video WHERE video_id='".$media_id."'";
						$rs=mysql_query($sql) or die(mysql_error());
						if(mysql_num_rows($rs)){
							$resultArray = Array();
							while ($row = mysql_fetch_assoc($rs)) {
								$resultArray[] = $row;
							}			
							return $resultArray;
						}
					break;
			case "audio":
			case "a":
						$sql="SELECT * FROM general_".$profile_type."_audio WHERE audio_id='".$media_id."'";
						$rs=mysql_query($sql) or die(mysql_error());
						if(mysql_num_rows($rs)){
							$resultArray = Array();
							while ($row = mysql_fetch_assoc($rs)) {
								if($row['gallery_relation']>0){
									$sql1 = "SELECT G.*,GL.gallery_title, 'gallery' as image_type FROM general_".$profile_type."_gallery as G, general_".$profile_type."_gallery_list as GL WHERE G.gallery_id=GL.gallery_id AND  G.gallery_id='".$row['gallery_relation']."'";
									$rs1=mysql_query($sql1) or die(mysql_error());
									if(mysql_num_rows($rs1)){
										$galleryArray = Array();
										while ($row1 = mysql_fetch_assoc($rs1)) {
											$galleryArray[] = $row1;
										}
									}
								}else{
									$sql1 = "SELECT '1' as img_id, image_name, name as image_title, 'profile_pic' as image_type FROM general_".$profile_type." WHERE artist_id='".$profile_id."'";
									$rs1=mysql_query($sql1) or die(mysql_error());
									if(mysql_num_rows($rs1)){
										$galleryArray = Array();
										while ($row1 = mysql_fetch_assoc($rs1)) {
											$galleryArray[] = $row1;
										}
									}
								}
								$row['gallery_files'] = $galleryArray;
								$resultArray[] = $row;
							}			
							return $resultArray;
						}
					break;
		}
		
	}

	function getUserBucket($user_id){
		$sql="select bucket_name from general_user where general_user_id='".$user_id."'";
		$rs=mysql_query($sql) or die(mysql_error());
		$row = mysql_fetch_assoc($rs);	  			
		return $row['bucket_name'];
	}

	function getMediaListByArtistId($type,$artist_id,$profile_type){
		switch($type){
			case "audio":
			case "a":
						$sql="SELECT * FROM general_".$profile_type."_audio WHERE profile_id='".$artist_id."'";
						$rs=mysql_query($sql) or die(mysql_error());
						if(mysql_num_rows($rs)){
							$resultArray = Array();
							while ($row = mysql_fetch_assoc($rs)) {
								if($row['gallery_relation']>0){
									$sql1 = "SELECT *, 'gallery' as image_type FROM general_".$profile_type."_gallery WHERE gallery_id='".$row['gallery_relation']."'";
									$rs1=mysql_query($sql1) or die(mysql_error());
									if(mysql_num_rows($rs1)){
										$galleryArray = Array();
										while ($row1 = mysql_fetch_assoc($rs1)) {
											$galleryArray[] = $row1;
										}
									}
								}else{
									$sql1 = "SELECT '1' as img_id, image_name, name as image_title, 'profile_pic' as image_type FROM general_".$profile_type." ";
									$rs1=mysql_query($sql1) or die(mysql_error());
									if(mysql_num_rows($rs1)){
										$galleryArray = Array();
										while ($row1 = mysql_fetch_assoc($rs1)) {
											$galleryArray[] = $row1;
										}
									}
								}
								$row['gallery_files'] = $galleryArray;
								$resultArray[] = $row;
							}			
							return $resultArray;
						}
				break;
			case "video":
			case "v":
						$sql="SELECT * FROM general_".$profile_type."_video WHERE profile_id='".$artist_id."'";
						$rs=mysql_query($sql) or die(mysql_error());
						if(mysql_num_rows($rs)){
							$resultArray = Array();
							while ($row = mysql_fetch_assoc($rs)) {
								$resultArray[] = $row;
							}			
							return $resultArray;
						}
				break;
		}
	}

	function checkUserLogin($uname,$upass){ //Check for general user & common user login
		$sql="SELECT * FROM general_user WHERE email='".mysql_real_escape_string($uname)."' AND pass='".mysql_real_escape_string($upass)."'";
		$rs=mysql_query($sql) or die(mysql_error());
		if(mysql_num_rows($rs))
			return TRUE;
		else
			return FALSE;
	}

}
?>