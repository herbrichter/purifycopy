<?php
	
	class RegistrationEmail
	{
		function sendArtist()
		{
			$this->from="web@purifyentertainment.net";
			$this->subject="Thank you for registering with Purify Entertainment!";
			$this->header="From: Purify Entertainment <".$this->from.">";
			$this->msg=<<<EOD
Hello $this->name,
	
Thank you for registering with Purify Entertainment. Follow the link below to verify your email address.

$this->elink 

After your account has been activated we will email you with notification that your account is ready for login.

Username: $this->username
Reference no.: $this->ref_no

Refer new users and receive Purify Points. Have your friends and family enter your reference number when registering or purchasing a membership. For every Member you refer, you will receive 10 Points in your account.

------------------------------------------------------------------------------------------------------------------------------------------
Purify Entertainment
EOD;
			
			mail($this->recipient,$this->subject,$this->msg,$this->header);
		}
		
		function sendCommunity()
		{
			$this->from="web@purifyentertainment.net";
			$this->subject="Thank you for registering with Purify Entertainment!";
			$this->header="From: Purify Entertainment <".$this->from.">";
			$this->msg=<<<EOD
Hello $this->name,
	
Thank you for registering with Purify Entertainment. Follow the link below to verify your email address.

$this->elink

After your account has been activated we will email you with notification that your account is ready for login.

Username: $this->username
Reference no.: $this->ref_no

Refer new users and receive Purify Points. Have your friends and family enter your reference number when registering or purchasing a membership. For every Member you refer, you will receive 10 Points in your account.

------------------------------------------------------------------------------------------------------------------------------------------
Purify Entertainment
EOD;
			
			mail($this->recipient,$this->subject,$this->msg,$this->header);
		}
		
		function sendTeam()
		{
			$this->from="web@purifyentertainment.net";
			$this->subject="Thank you for registering with Purify Entertainment!";
			$this->header="From: Purify Entertainment <".$this->from.">";
			/*switch($category_id)
			{
				case 1:	$this->category="Director";
						break;
				case 2: $this->category="Team Programmer";
						break;
				case 3: $this->category="Team Promoter";
						break;
			}*/
			$this->msg=<<<EOD
Hello $this->name,
	
Thank you for registering with Purify Entertainment. Follow the link below to verify your email address.

$this->elink 

After your account has been activated we will email you with notification that your account is ready for login.

Username: $this->username
Reference no.: $this->ref_no

Refer new users and receive Purify Points. Have your friends and family enter your reference number when registering or purchasing a membership. For every Member you refer, you will receive 10 Points in your account.

------------------------------------------------------------------------------------------------------------------------------------------
Purify Entertainment
EOD;
			
			mail($this->recipient,$this->subject,$this->msg,$this->header);
		}
		
		function sendFan($subscription_id)
		{
			$this->from="web@purifyentertainment.net";
			$this->subject="Thank you for registering with Purify Entertainment!";
			$this->header="From: Purify Entertainment <".$this->from.">";
			switch($subscription_id)
			{
				case 1:	$this->subscription="Purify Founder";
						break;
				case 2: $this->subscription="Life Time Subscriber";
						break;
				case 3: $this->subscription="Purify Fan";
						break;
			}
			$this->msg=<<<EOD
Hello $this->name,
	
Thank you for registering with Purify Entertainment. Follow the link below to verify your email address.

$this->elink 

After your account has been activated we will email you with notification that your account is ready for login.

Username: $this->username
Reference no.: $this->ref_no

Refer new users and receive Purify Points. Have your friends and family enter your reference number when registering or purchasing a membership. For every Member you refer, you will receive 10 Points in your account.

------------------------------------------------------------------------------------------------------------------------------------------
Purify Entertainment
EOD;
			
			mail($this->recipient,$this->subject,$this->msg,$this->header);
		}
	}
?>