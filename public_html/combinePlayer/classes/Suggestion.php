<?php
include_once('commons/db.php');

class Suggestion
{
	function addSuggestion($name,$email,$sugg)
	{
		$posted_date = date("F j, Y, g:i a");
		$sql="insert into suggestion (`name`,`email`,`sugg`,`posted_date`) values ('$name','$email','$sugg','$posted_date')";
		$rs=mysql_query($sql) or die(mysql_error());
		return mysql_insert_id();
	}
	function countUnread()
	{
		$sql="SELECT * FROM suggestion WHERE seen='0'";
		$rs=mysql_query($sql) or die(mysql_error());
		return mysql_num_rows($rs);
	}
	function countRead()
	{
		$sql="SELECT * FROM suggestion WHERE seen='1'";
		$rs=mysql_query($sql) or die(mysql_error());
		return mysql_num_rows($rs);
	}	
	function countSuggestionsByCriteria($search_key,$criteria)
	{
		if(!$search_key)
		{
			if($criteria=='read') $sql="SELECT * FROM suggestion WHERE seen='1'";
			else if($criteria=='unread') $sql="SELECT * FROM suggestion WHERE seen='0'";
			else $sql="SELECT * FROM suggestion";		
		}
		else
		{
			if($criteria=='read') $sql="SELECT * FROM suggestion WHERE seen='1' && posted_date like '%$search_key%'";
			else if($criteria=='unread') $sql="SELECT * FROM suggestion WHERE seen='0' && posted_date like '%$search_key%'";
			else $sql="SELECT * FROM suggestion WHERE posted_date like '%$search_key%'";
		}
			
		$rs=mysql_query($sql) or die(mysql_error());
		return mysql_num_rows($rs);		
	}
	function sortByCriteria($search_key,$max,$criteria,$orderby,$dir)
	{
		if(!$search_key)
		{
			if($criteria=='read') $sql="SELECT * FROM suggestion WHERE seen='1' ORDER BY $orderby $dir".$max;
			else if($criteria=='unread') $sql="SELECT * FROM suggestion WHERE seen='0' ORDER BY $orderby $dir".$max;
			else $sql="SELECT * FROM suggestion ORDER BY $orderby $dir".$max;		
		}
		else
		{
			if($criteria=='read') $sql="SELECT * FROM suggestion WHERE seen='1' && posted_date like '%$search_key%' ORDER BY $orderby $dir".$max;
			else if($criteria=='unread') $sql="SELECT * FROM suggestion WHERE seen='0' && posted_date like '%$search_key%' ORDER BY $orderby $dir".$max;
			else $sql="SELECT * FROM suggestion WHERE posted_date like '%$search_key%' ORDER BY $orderby $dir".$max;
		}
			
		$rs=mysql_query($sql) or die(mysql_error());
		return($rs);
	}
	function mark_as_read($read_sugg_id)
	{
		$sql="UPDATE suggestion SET seen='1' WHERE sugg_id='$read_sugg_id'";
		$rs=mysql_query($sql) or die(mysql_error());
	}
	
	function notifyAdmin()
	{
		$this->recipient="purifyentertainment@gmail.com";
		$this->subject="New Suggestion";
		$this->header="From: purifyentertainment.net";
		$this->msg=<<<EOD

Hello Administrator,
--------------------------
You have a new Suggestion. Kindly login to accept/reject it.
Thanks.
------------------------------------------------------------------------------------------------------------------------------------------
EOD;
			
		mail($this->recipient,$this->subject,$this->msg,$this->header);		
	}
}

?>