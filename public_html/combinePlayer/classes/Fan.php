<?php
include_once('User.php');
include_once('PasswordHash.php');

class Fan extends User
{
  function Fan()
  {
  	$this->user_type='fan';
  }
  
  function addUser($username,$password,$email,$verification_no)
  {
    $t_hasher = new PasswordHash(8, FALSE);
	$hash = $t_hasher->HashPassword($password);
	$now = date("F j, Y, g:i a");
	$query="insert into user (`username`,`password`,`email`,`verification_no`,`status`,`create_date`,`update_date`,`subscribed`) values ('$username','$hash','$email','$verification_no','0','$now','$now','1')";
	$rs=mysql_query($query) or die(mysql_error());
	$id=mysql_insert_id();
	return($id);
  }
  
  function addParentEntry($user_id)
  {
  	$query="insert into user_parent (`user_id`,`user_type`,`featured_parent`) values ('$user_id','$this->user_type','0')";
	$rs=mysql_query($query) or die(mysql_error());
	$id=mysql_insert_id();
	return($id);
  }
  
  function updateVerification($verification_no)
  {
  	$query="UPDATE user SET verified='1' WHERE verification_no='$verification_no'";
	$rs=mysql_query($query) or die(mysql_error());
  }
}
?>