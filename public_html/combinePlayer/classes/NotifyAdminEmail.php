<?php
	class NotifyAdminEmail
	{
		function notifyAdminEmail()
		{
			$this->recipient="purifyentertainment@gmail.com";
			$this->subject="New user registered.";
			$this->header="From: Purifyentertainment.net";
			$this->msg=<<<EOD

Hello Administrator,
--------------------------
You have a new user registered. Kindly login to accept/reject it.
Thanks.
------------------------------------------------------------------------------------------------------------------------------------------
EOD;
			
			mail($this->recipient,$this->subject,$this->msg,$this->header);		
		}
	}

?>