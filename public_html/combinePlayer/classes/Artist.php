<?php
include_once('User.php');
include_once('PasswordHash.php');

class Artist extends User
{
  function Artist()
  {
  	$this->user_type='artist';
  }
  
  function addUser($username,$password,$name,$email,$country,$state_or_province,$city,$homepage,$verification_no)
  {
    $t_hasher = new PasswordHash(8, FALSE);
	$hash = $t_hasher->HashPassword($password);
	$now = date("F j, Y, g:i a");
	if($homepage=='http://')
	{
		$homepage='';
	}	
	$query="insert into user (`username`,`password`,`name`,`email`,`country`,`state_or_province`,`city`,`homepage`,`ref_no`,`ref_by`,`verification_no`,`status`,`create_date`,`update_date`,`subscribed`,`featured`) values ('$username','$hash','$name','$email','$country','$state_or_province','$city','$homepage','$ref_no','$ref_by','$verification_no','0','$now','$now','1','1')";
	$rs=mysql_query($query) or die(mysql_error());
	$id=mysql_insert_id();
	return($id);
  }
  
  function addParentEntry($user_id)
  {
  	$query="insert into user_parent (`user_id`,`user_type`,`featured_parent`) values ('$user_id','$this->user_type','1')";
	$rs=mysql_query($query) or die(mysql_error());
	$id=mysql_insert_id();
	return($id);
  }
  
  function updateUpload($user_id,$upload_type,$upload_name,$upload_path)
  {
  	$query="UPDATE user SET upload_type='$upload_type', upload_name='$upload_name', upload_path='$upload_path' WHERE user_id='$user_id'";
	$rs=mysql_query($query) or die(mysql_error());
  }
  
  function updateVerification($verification_no)
  {
  	$query="UPDATE user SET verified='1' WHERE verification_no='$verification_no'";
	$rs=mysql_query($query) or die(mysql_error());
  }
  
  function unlinkUser($user_id)
  {
  	$query="SELECT * FROM user WHERE user_id='$user_id'";
	$rs=mysql_query($query) or die(mysql_error());  	
	$row=mysql_fetch_assoc($rs);
	unlink($row['upload_path']);
  }

  function getUserInfo($uid=''){
	$sql="SELECT * FROM general_user WHERE general_user_id='".$uid."'";
	$rs=mysql_query($sql) or die(mysql_error());
	if(mysql_num_rows($rs)){
		return mysql_fetch_assoc($rs);	
	}
  }
  
	function getArtistInfo($artist_id=''){
		$sql="SELECT * FROM general_artist WHERE artist_id='".$artist_id."'";
		$rs=mysql_query($sql) or die(mysql_error());
		if(mysql_num_rows($rs)){
			return mysql_fetch_assoc($rs);	
		}
	}
	
	function getArtistProfileInfo($artist_id=''){
	$sql="SELECT * FROM general_artist_profiles WHERE artist_id='".$artist_id."'";
	$rs=mysql_query($sql) or die(mysql_error());
	if(mysql_num_rows($rs)){
		return mysql_fetch_assoc($rs);	
	}
  }

}
?>