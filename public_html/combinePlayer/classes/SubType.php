<?php
include_once('commons/db.php');

class SubType
{
  function listSubTypes($type_id)
  {
	$data = mysql_query("SELECT * FROM subtype WHERE type_id='$type_id' AND name!='None' ORDER BY name") or die(mysql_error()); 
	return($data);
  }
  function listSubTypesEx($type_id,$subtype_id)
  {
	$data = mysql_query("SELECT * FROM subtype WHERE type_id='$type_id' AND name!='None' AND subtype_id!='$subtype_id' ORDER BY name") or die(mysql_error()); 
	return($data);
  }  
  function getNone($type_id)
  {
	$data = mysql_query("SELECT * FROM subtype WHERE type_id='$type_id' AND name='None'") or die(mysql_error()); 
	return($data);
  }
  function getSubTypeById($subtype_id)
  {
	$data = mysql_query("SELECT * FROM subtype WHERE subtype_id='$subtype_id'") or die(mysql_error()); 
	$row=mysql_fetch_assoc($data);  
	return($row['name']);
  }
  function getSubtype($subtype_id)
  {
	$data = mysql_query("SELECT * FROM subtype WHERE subtype_id='$subtype_id'") or die(mysql_error()); 
	$row=mysql_fetch_assoc($data);  
	return($row);  
  }
  function addSubType($newsubtype,$type_id)
  {
	$data = mysql_query("INSERT INTO subtype (`name`,`type_id`) values ('$newsubtype','$type_id')") or die(mysql_error()); 
	return mysql_insert_id();     
  }
  function removeSubType($subtype_id)
  {
  	$sql="DELETE FROM subtype WHERE subtype_id='$subtype_id'";
	$rs=mysql_query($sql) or die(mysql_error());
	return $rs;  	
  }
  function removeSubTypeByType($type_id)
  {
  	$sql="DELETE FROM subtype WHERE type_id='$type_id'";
	$rs=mysql_query($sql) or die(mysql_error());
	return $rs;  	
  }  
}
?>