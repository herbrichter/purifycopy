<?php
class ViewArtistProfileURL
{
	function get_user_info($id)
	{
		$sql=mysql_query("select * from general_user where artist_id='".$id."' AND delete_status=0");
		$res=mysql_fetch_assoc($sql);
		return($res);
	}
	function get_user_artist_profile($id)
	{
		$res=$this->get_user_info($id);
		$sql4=mysql_query("select * from general_artist where artist_id='".$res['artist_id']."' AND del_status=0");
		$res4=mysql_fetch_assoc($sql4);
		return($res4);
	}
	function get_user_country($id)
	{
		$res=$this->get_user_artist_profile($id);
		$sql2=mysql_query("select * from country where country_id='".$res['country_id']."'");
		$res2=mysql_fetch_assoc($sql2);
		return($res2);
	}
	function get_user_state($id)
	{
		$res=$this->get_user_artist_profile($id);
		$sql3=mysql_query("select * from state where state_id='".$res['state_id']."'");
		$res3=mysql_fetch_assoc($sql3);
		return($res3);
	}
	function get_artist_user($id)
	{
		$res=$this->get_user_info($id);
		$sql5=mysql_query("select MIN(general_artist_profiles_id) from general_artist_profiles where artist_id='".$res['artist_id']."'");
		$res5=mysql_fetch_assoc($sql5);

		$sql6=mysql_query("select * from general_artist_profiles where general_artist_profiles_id='".$res5['MIN(general_artist_profiles_id)']."'");
		$res6=mysql_fetch_assoc($sql6);
		return($res6);
	}
	function get_artist_user_type($id)
	{
		$res1=$this->get_artist_user($id);
		$sql7=mysql_query("select * from type where type_id='".$res1['type_id']."'");
		$res7=mysql_fetch_assoc($sql7);
		return($res7);
	}
	function get_artist_user_subtype($id)
	{
		$res1=$this->get_artist_user($id);
		$sql8=mysql_query("select * from subtype where subtype_id='".$res1['subtype_id']."'");
		$res8=mysql_fetch_assoc($sql8);
		return($res8);
	}
	function get_artist_user_metatype($id)
	{
		$res=$this->get_user_info($id);
		$sql16=mysql_query("select DISTINCT(metatype_id) from general_artist_profiles where artist_id='".$res['artist_id']."' AND metatype_id!=0 LIMIT 3");
		$new_ans = array();
		$new_ans1 = array();
		if(mysql_num_rows($sql16)>0)
		{
			while($row = mysql_fetch_assoc($sql16))
			{
				$new_ans[] = $row; 
			}
			
			for($i=0;$i<count($new_ans);$i++)
			{
				$sql17 = mysql_query("SELECT name FROM meta_type WHERE meta_id='".$new_ans[$i]['metatype_id']."'");
				$new_ans1[$i] = mysql_fetch_assoc($sql17);
			}
		}
		return($new_ans1);
	}
	
	function get_artist_project($id)
	{
		$sql9=mysql_query("select * from artist_project where id='".$id."' AND del_status=0 AND active=0");
		return($sql9);
	}
	
	function get_community_project($id)
	{
		$sql9=mysql_query("select * from community_project where id='".$id."' AND del_status=0 AND active=0");
		return($sql9);
	}
	
	/********Function for Displaying video and songs*****/
	
	function get_artist_Info($id)
	{
		$res=$this->get_user_info($id);
		$sql10=mysql_query("select * from general_artist where artist_id='".$res['artist_id']."' AND del_status=0");
		$res10=mysql_fetch_assoc($sql10);
		return($res10);
	}
	function get_Video($tag_video)
	{
		$sql11=mysql_query("select * from media_video where media_id='".$tag_video."'");
		$res11=mysql_fetch_assoc($sql11);
		return($res11);
	}
	function get_Video_title($tag_video)
	{
		$sql12=mysql_query("select * from general_media where id='".$tag_video."' AND delete_status=0");
		$res12=mysql_fetch_assoc($sql12);
		return($res12);
	}
	function get_Song($tag_song)
	{
		$sql13=mysql_query("select * from media_songs where media_id='".$tag_song."'");
		$res13=mysql_fetch_assoc($sql13);
		return($res13);
	}
	function get_Song_title($tag_song)
	{
		$sql14=mysql_query("select * from general_media where id='".$tag_song."' AND media_type=114 AND delete_status=0");
		$res14=mysql_fetch_assoc($sql14);
		return($res14);
	}
	function get_Song_regis($tag_song)
	{
		$sql15=mysql_query("select * from general_artist_audio where audio_id='".$tag_song."'");
		$res15=mysql_fetch_assoc($sql15);
		return($res15);
	}
	
	function recordedComEvents($id,$date)
	{
		$res=$this->get_user_info($id);
		
		$sql_recorded_artist=mysql_query("SELECT * FROM community_event WHERE id='".$id."' AND del_status=0 AND active=0");
		return($sql_recorded_artist);
	}
	
	function upcomingComEvents($id,$date)
	{
		$res=$this->get_user_info($id);
		$sql_upcoming_artist=mysql_query("SELECT * FROM community_event WHERE id='".$id."' AND del_status=0 AND active=0");
		return($sql_upcoming_artist);
	}
	
	function recordedArtistEvents($id,$date)
	{
		$res=$this->get_user_info($id);
		
		$sql_recorded_artist=mysql_query("SELECT * FROM artist_event WHERE id='".$id."' AND del_status=0 AND active=0");
		return($sql_recorded_artist);
	}
	
	function upcomingArtistEvents($id,$date)
	{
		$res=$this->get_user_info($id);
		$sql_upcoming_artist=mysql_query("SELECT * FROM artist_event WHERE id='".$id."' AND del_status=0 AND active=0");
		return($sql_upcoming_artist);
	}
	
	function get_Gallery_title($tag_gal)
	{
		$sql15=mysql_query("select * from general_media where id='".$tag_gal."' AND delete_status=0");
		$res15=mysql_fetch_assoc($sql15);
		return($res15);
	}
	function get_gallery($tag_gal)
	{
		$sql16=mysql_query("select * from general_media where id='".$tag_gal."' AND media_type=113 AND delete_status=0");
		$res16=mysql_fetch_assoc($sql16);
		return($res16);
	}
	
	
	
	function get_media_cover_pic($media_id)
	{
		$sql16=mysql_query("select * from media_images where media_id='".$media_id."'");
		$res16=mysql_fetch_assoc($sql16);
		return($res16);
	}
	function get_Gallery_at_register($id,$tag_gal)
	{
		$res=$this->get_user_info($id);
		$get_gal=mysql_query("select * from general_artist_gallery where gallery_id='".$tag_gal."' AND profile_id='".$res['artist_id']."'");
		return($get_gal);
	}
	function get_count_Gallery_at_register($id,$tag_gal)
	{
		$res=$this->get_user_info($id);
		$get_gal=mysql_query("select count(*) from general_artist_gallery where gallery_id='".$tag_gal."' AND profile_id='".$res['artist_id']."'");
		$res_gal=mysql_fetch_assoc($get_gal);
		return($res_gal);
	}
	function get_Gallery_title_at_register($tag_gal)
	{
		$get_gal_title=mysql_query("select * from general_artist_gallery_list where gallery_id='".$tag_gal."'");
		$res_gal_title=mysql_fetch_assoc($get_gal_title);
		return($res_gal_title);
	}
	
	/*****************************Functions For Displaying song pic ****************************/
	
	function get_artist_Gallery_at_register_for_song($gallery_id)
	{
		$get_gal=mysql_query("select * from general_artist_gallery where gallery_id='".$gallery_id."' ");
		return($get_gal);
	}
	
	function get_media_Gallery($gallery_id)
	{
		$get_gal=mysql_query("select * from media_images where media_id='".$gallery_id."'");
		return($get_gal);
	}
	function get_community_Gallery_at_register_for_song($tag_gal)
	{
		$get_gal=mysql_query("select * from general_community_gallery where gallery_id='".$tag_gal."'");
		return($get_gal);
	}
	function get_Song_disp($tag_song)
	{
		$sql13=mysql_query("select * from media_songs where media_id='".$tag_song."'");
		$res13=mysql_fetch_assoc($sql13);
		return($res13);
	}
	
	function get_all_permission($gen_id)
	{
		$get_per=mysql_query("select * from permissions where general_user_id='".$gen_id."'");
		$res_per=mysql_fetch_assoc($get_per);
		return($res_per);
	}
	function get_loggedin_user_id($email)
	{
		$get_id=mysql_query("select * from general_user where email='".$email."' AND delete_status=0");
		$res_id=mysql_fetch_assoc($get_id);
		return($res_id);
	}
	
	function get_member_or_not($id)
	{
		$get_member=mysql_query("select * from purify_membership where general_user_id='".$id."'");
		$res_member=mysql_fetch_assoc($get_member);
		return($res_member);
	}
	
	
	function get_tagged_song_list_pic($id)
	{
		$get_art_id=mysql_query("select * from general_user where general_user_id='".$id."' AND delete_status=0" );
		$res = mysql_fetch_assoc($get_art_id);
		$get_list_pic=mysql_query("select * from general_artist where artist_id='".$res['artist_id']."' AND del_status=0");
		$res_list_pic=mysql_fetch_assoc($get_list_pic);
		return($res_list_pic);
	}
	
	function get_featured_media_gallery($id)
	{
		$get_feature_media_images=mysql_query("select * from media_images where media_id='".$id."'");
		return($get_feature_media_images);
	}
	function get_featured_media_gallery_at_register($id,$type)
	{
		$get_feature_media_images=mysql_query("select * from general_".$type."_gallery where gallery_id='".$id."'");
		return($get_feature_media_images);
	}
	function get_artist_friends($art_id)
	{
		$get_art = mysql_query("select * from general_artist where artist_id = '".$art_id."' AND del_status=0 AND status=0 AND active=0");
		return($get_art);
	}
	function get_community_friends($com_id)
	{
		$get_com = mysql_query("select * from general_community where community_id = '".$com_id."' AND del_status=0 AND status=0 AND active=0");
		return($get_com);
	}
	
	function get_from_list_pic($tbl_name,$id)
	{
		$get_list = mysql_query("select * from ".$tbl_name." where id = '".$id."'");
		$res_list = mysql_fetch_assoc($get_list);
		return($res_list);
	}
	function get_creator_list_pic($tbl_name,$id,$id_name)
	{
		$get_list = mysql_query("select * from ".$tbl_name." where ".$id_name." = '".$id."'");
		$res_list = mysql_fetch_assoc($get_list);
		return($res_list);
	}
	function chk_user_delete($id)
	{
		$get_user = mysql_query("select * from general_user where artist_id = '".$id."'");
		$res_user = mysql_fetch_assoc($get_user);
		return($res_user);
	}
	/*****************************Functions For Displaying song pic  Ends here****************************/
	function find_creator($id,$protype)
	{
		if(isset($_SESSION['login_email']))
		{
			$sql = mysql_query("select * from general_user where email = '".$_SESSION['login_email']."'");
			if(mysql_num_rows($sql)>0)
			{
				$res = mysql_fetch_assoc($sql);
				if($res[$protype."_id"]>0)
				{
					if($res[$protype.'_id'] == $id)
					{
						return ("creator");
					}
					else
					{
						return ("other");
					}
				}
				else
				{
					return ("other");
				}
			}
		}
		else
		{
			return ("other");
		}
	}
	function getvideoimage($media_id)
	{
		$sql = mysql_query("select * from media_video where media_id ='".$media_id."'");
		if(mysql_num_rows($sql)>0)
		{
			$res = mysql_fetch_assoc($sql);
			preg_match("#(?<=v=)[a-zA-Z0-9-]+(?=&)|(?<=v\/)[^&\n]+(?=\?)|(?<=v=)[^&\n]+|(?<=youtu.be/)[^&\n]+#", $res['videolink'], $matches);
			$videoId = $matches[0];
			return($videoId);
		}
	}
	function get_register_featured_media_gallery($media_id,$table_name)
	{
		if($table_name == "general_artist_gallery_list"){ $tbl_name = "general_artist_gallery";}
		if($table_name == "general_community_gallery_list"){ $tbl_name = "general_community_gallery";}
		$query = mysql_query("select * from ".$tbl_name." where gallery_id='".$media_id."'");
		if(mysql_num_rows($query)>0)
		{
			while($res = mysql_fetch_assoc($query))
			{
				$new_array[] = $res;
			}
			return($new_array);
		}
	}
	function get_featured_gallery_images($media_id)
	{
		$query = mysql_query("select * from media_images where media_id='".$media_id."'");
		if(mysql_num_rows($query)>0)
		{
			while($res = mysql_fetch_assoc($query))
			{
				$new_array[] = $res;
			}
			return($new_array);
		}
	}
	function get_audio_img($audio_id)
	{
		$image_name ="";
		$sql_media_img = mysql_query("SELECT * FROM general_media WHERE id='".$audio_id."' AND delete_status=0 AND media_status=0");
		if(mysql_num_rows($sql_media_img)>0)
		{
			$res_media_img = mysql_fetch_assoc($sql_media_img);
			if($res_media_img['from_info']!="")
			{
				$exp_from_info = explode("|",$res_media_img['from_info']);
				$get_img = mysql_query("select * from ".$exp_from_info[0]." where id=".$exp_from_info[1]."");
				if(mysql_num_rows($get_img)>0)
				{
					$res_img = mysql_fetch_assoc($get_img);
					$image_name = $res_img['image_name'];
					
				}
			}
			if($res_media_img['from_info'] =="" && $res_media_img['creator_info']!="")
			{
				$exp_creator_info = explode("|",$res_media_img['creator_info']);
				if($exp_creator_info[0]=="general_artist"){$id_name ="artist_id";
				$image_name ="http://artjcropprofile.s3.amazonaws.com/";
				}
				if($exp_creator_info[0]=="general_community"){$id_name ="community_id";
				$image_name ="http://comjcropprofile.s3.amazonaws.com/";
				}
				if($exp_creator_info[0]=="community_project" || $exp_creator_info[0]=="artist_project"){$id_name ="id";
				$image_name ="";
				}
				
				$get_img = mysql_query("select * from ".$exp_creator_info[0]." where ".$id_name."=".$exp_creator_info[1]."");
				if(mysql_num_rows($get_img)>0)
				{
					$res_img = mysql_fetch_assoc($get_img);
					$image_name = $image_name .$res_img['image_name'];
				}
			}
			if($res_media_img['from_info'] =="" && $res_media_img['creator_info'] =="")
			{
				$get_gen_det = mysql_query("select * from general_user where general_user_id='".$res_media_img['general_user_id']."'");
				if(mysql_num_rows($get_gen_det)>0){
					$res_gen_det = mysql_fetch_assoc($get_gen_det);
					if($res_gen_det['artist_id']!="" && $res_gen_det['artist_id']!=0){
						$get_art_info = mysql_query("select * from general_artist where artist_id='".$res_gen_det['artist_id']."'");
						if(mysql_num_rows($get_art_info)>0){
							$image_name ="http://artjcropprofile.s3.amazonaws.com/";
							$res_art_info = mysql_fetch_assoc($get_art_info);
							$image_name = $image_name .$res_art_info['image_name'];
						}
					}
					if($res_gen_det['community_id']!="" && $res_gen_det['community_id']!=0){
						$get_com_info = mysql_query("select * from general_community where community_id='".$res_gen_det['community_id']."'");
						if(mysql_num_rows($get_com_info)>0){
							$image_name ="http://comjcropprofile.s3.amazonaws.com/";
							$res_com_info = mysql_fetch_assoc($get_com_info);
							$image_name = $image_name .$res_com_info['image_name'];
						}
					}
				}
			}
		}
		return($image_name);
	}
	function get_fetatured_media_info($id)
	{
		$sql = mysql_query("select * from general_media where id='".$id."'");
		if(mysql_num_rows($sql)>0){
			$res = mysql_fetch_assoc($sql);
			return($res);
		}
	}
	function get_gallery_info($id){
		$query = mysql_query("select * from media_images where media_id='".$id."'");
		if(mysql_num_rows($query)>0){
		$res = mysql_fetch_assoc($query);
		}
		return($res);
	}
}
?>