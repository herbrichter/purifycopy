<?php
	include_once('classes/User.php');
	
	session_start();
	$username = $_SESSION['username'];
	$obj=new User();
	$row=$obj->getUserInfo($username);	
?>          
          
          <div id="edit-box" class="edit-text">
            <form action="" method="post" id="location_form">
            <table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" class="edit-text">
                <tr>
                  <td width="100">Country</td>
                  <td width="402">
                    <input name="country" type="text" class="regularField2" id="country" value="<?php echo $row['country']; ?>" />
                  </td>
                  <td width="148" align="left"><div class="hint"></div></td>
                </tr>
                <tr>
                  <td colspan="3"><img src="images/spacer.gif" alt=" " width="10" height="10" /></td>
                </tr>
                <tr>
                  <td width="100">State/Province</td>
                  <td width="402">
                    <input name="state_or_province" type="text" class="regularField2" id="state_or_province" value="<?php echo $row['state_or_province']; ?>" />
                  </td>
                  <td width="148" align="left"><div class="hint"></div></td>
                </tr>
                <tr>
                  <td colspan="3"><img src="images/spacer.gif" alt=" " width="10" height="10" /></td>
                </tr>
                <tr>
                  <td width="100">City</td>
                  <td width="402">
                    <input name="city" type="text" class="regularField2" id="city" value="<?php echo $row['city']; ?>" />
                  </td>
                  <td width="148" align="left"><div class="hint"></div></td>
                </tr>
                <tr>
                  <td colspan="3"><img src="images/spacer.gif" alt=" " width="10" height="10" /></td>
                </tr>                                         
                <tr>
                  <td width="100" valign="top"></td>
                  <td width="402">
                    <input type="submit" name="location" id="location" value=" Save " />
                  </td>
                  <td width="148" align="left"><div class="hint"></div></td>
                </tr>
              </table>
              </form>          
          </div>