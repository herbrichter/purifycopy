<?php 
	ob_start();
	include_once('classes/UnregUser.php');
	include_once('classes/Artist.php');
	include_once('classes/UserType.php');
	include_once('classes/UserImg.php');
	include_once('classes/RegistrationEmail.php');
	include_once('recaptchalib.php');
	include_once('login_includes.php');
    
    $domainname=$_SERVER['SERVER_NAME'];
	
	$publickey = "6Lduu-sSAAAAAHbUkh7_9mmfg3zQucvRxtr2gvbA";
	$privatekey = "6Lduu-sSAAAAALD2YftpY41sm3a1CtIpHeymu3TH";
	$parent='Artist';
	
	# the response from reCAPTCHA
	$resp = null;
	# the error code from reCAPTCHA, if any
	$error = null;
 
	if($uemail=$_POST['uemail'])
	{
		$uobj=new UnregUser();
		if($uobj->addUser($parent,$uemail))
		{
			$uflag=1;
		}
	}
	
	# are we submitting the page?
	if(isset($_POST['register']))
	{
		$resp = recaptcha_check_answer ($privatekey,
        		                        $_SERVER["REMOTE_ADDR"],
                		                $_POST["recaptcha_challenge_field"],
                        		        $_POST["recaptcha_response_field"]);
		$email=$_POST['email'];
		$firstname=$_POST['fname'];
		$lastname=$_POST['lname'];
		$country=$_POST['country'];
		$state_or_province=$_POST['state'];
		$city=$_POST['city'];
		$homepage=$_POST['homepage'];
		$username=$_POST['username'];
		$password=$_POST['password'];
		$ref_by=$_POST['referred_by'];
		
		if(isset($_POST['type_array']))
		{
			$flag=1;
		}
		else
		{
			$flag=0;
			$msg2="<div align='right' class='hintRed'>Please select atleast one type-subtype set.</div>";
		}

		if(($_FILES["upload"]["type"]=="image/jpeg")||($_FILES["upload"]["type"]=="audio/mpeg"))
		{	
			if ($_FILES["upload"]["error"] > 0)
    		{
				$flag=0;
    			$msg3="Error: ".$_FILES["upload"]["error"];
		    }
			else if(($_FILES["upload"]["type"]=="image/jpeg")&&$_FILES["upload"]["size"]>1000000)
			{
				$flag=0;
    			$msg3="<div align='right' class='hintRed'>Please select a .jpg file(upto 1MB) or a .mp3 file(upto 10MB).</div>";
			}
			else if(($_FILES["upload"]["type"]=="audio/mpeg")&&$_FILES["upload"]["size"]>10000000)
			{
				$flag=0;
    			$msg3="<div align='right' class='hintRed'>Please select a .jpg file(upto 1MB) or a .mp3 file(upto 10MB).</div>";
			}
			else
			{	
				switch($_FILES["upload"]["type"])
				{
					case "image/jpeg": 	$upload_type=0;
										$dir="images";
										$ext=".jpg";
										break;
					case "audio/mpeg": 	$upload_type=1;
										$dir="mp3s";
										$ext=".mp3";
										break;
				}
				$flag2=1;
			}
		}
		else
		{
			$flag2=0;
			$msg3="<div align='right' class='hintRed'>Please select a .jpg file(upto 1MB) or a .mp3 file(upto 10MB).</div>";
		}

		if($flag==1&&$flag2==1)
		{
			if ($resp->is_valid)
			{
				$obj=new Artist();
				$obj2=new UserType();
				
				if($ref_by&&!$obj->checkRefNo($ref_by))
				{
					$msg4="<div align='right' class='hintRed'>User with this reference no. does not exist.</div>";
					$ref_flag=0;
				}
				else
				{
					$ref_flag=1;
				}
				if($obj->checkAvailability($username))
				{
					$msg1="<div align='right' class='hintRed'>Username you selected already exists, please select a new one.</div>";
					$avail_flag=0;
				}
				else
				{
					$avail_flag=1;
				}
				
				if($ref_flag==1&&$avail_flag==1)
				{	
					do
					{
						$verification_no = rand(1000000000,9999999999);	
					}while($obj->checkVerificationNo($verification_no));
					
					do
					{
						$ref_no = rand(10000,99999);
						$ref_id=$_POST['fname'].$ref_no;
					}while($obj->checkRefNo($ref_id));
					
					if($user_id=$obj->addUser($username,$password,$firstname,$lastname,$email,$country,$state_or_province,$city,$homepage,$verification_no,$ref_id,$ref_by))
					{
						$obj->updateReference($ref_by);
						$obj->addParentEntry($user_id);
						foreach($_POST['type_array'] as $type_id => $sub_type_array)
						{
							foreach($sub_type_array as $key=>$subtype_id)
							{
								$obj2->addEntry($user_id,$type_id,$subtype_id);
							}
						}
						
						//path where uploaded files are stored.
						$upload_name=$_FILES["upload"]["name"];
						$upload_path="uploads/".$dir."/".$user_id."__upload".$ext; 
						move_uploaded_file($_FILES["upload"]["tmp_name"],$upload_path);
						$obj->updateUpload($user_id,$upload_type,$upload_name,$upload_path);
						require('pp_upload_inc.php');						
						require("pp_upload.php");
						if(!$msg5)
						{
							$objImg=new UserImg();
							$objImg->addImg($user_id,$thumb_image_location,$thumb_image_location2,$thumb_image_location3);
							
							$objReg=new RegistrationEmail();
							$objReg->name=$firstname;
							$objReg->elink="http://www.purifyentertainment.net/Design/registration_artist_verify.php?id=".$verification_no;
							$objReg->recipient=$email;
							$objReg->username=$username;
							$objReg->ref_no=$ref_id;
							$objReg->sendArtist();
							header('location:registration_artist_done.php?ref='.$ref_id);
						}
						else
						{
							$obj->unlinkUser($user_id);
							$obj->abandonUser($user_id);
							$obj->abandonUserParent($user_id);
							$obj2->abandonUserType($user_id);
						}
					}	
				}
			} 
			else 
			{
				# set the error code so that we can display it. You could also use
				# die ("reCAPTCHA failed"), but using the error message is
				# more user friendly
				$error = $resp->error;
			}
		}
	}
?>

<?php include_once('includes/header.php'); ?>
<title></title>
<link rel="stylesheet" type="text/css" media="screen" href="includes/multiple_select.css"/>
<script>
	$(document).ready(
						function()
						{
    						$("#addType").click(
													function () 
													{
														//alert($("#type-select").val().attr("key"));
														//alert($("#type-select option:selected").attr("key"));
														if ($("div").hasClass($("#type-select").val())) 
														{
															//alert("hi");
															if(!$("div").hasClass($("#type-select").val()+$("#subtype-select").val()))
															{
																$("."+$("#type-select").val()).append('<div class="' +$("#type-select").val()+$("#subtype-select").val() + ' box1A"><input type="checkbox" name="type_array['+$("#type-select").val() +']['+$("#subtype-select").val()+']" value="'+$("#subtype-select").val()+'" checked onclick=" $(this).parent().remove()"/>'+$("#subtype-select option:selected").attr("chkval")+'</div>');
															}
														}
														else
														{
															$("#selected-types").append('<div class="'+$("#type-select").val()+' box1">'+'<input type="checkbox" name="type_array['+$("#type-select").val() +']" value="'+$("#type-select").val()+'" checked onclick="$(this).parent().remove()" />'+$("#type-select option:selected").attr("chkval")+'<br>'+'<div class="'+$("#type-select").val()+$("#subtype-select").val() + ' box1A"><input type="checkbox" name="type_array['+$("#type-select").val() +']['+$("#subtype-select").val()+']" value="'+$("#subtype-select").val()+'" checked  onclick="$(this).parent().remove()"/>'+$("#subtype-select option:selected").attr("chkval")+'</div></div>');
														}													
													}
												);
							$("#type-select").change(
														function ()
														{
															//alert($("#type-select").val());
															$.post("registration_block_subtype.php", { type_id:$("#type-select").val() },
  															function(data)
															{
    														//alert("Data Loaded: " + data);
																$("#subtype-select").replaceWith(data);
  															}
													);
											}
										)
										.change();
							$("#chk_avail_link").click(
															function()
															{
																$.post("registration_user_availablity.php", { name:$("#username").val() },
																function(data)
																{
																	$("#user_name_msg").html(data);
																	$("#register").focus();	
																	$("#username").focus();																	
																}
																);
							
															}
							);			
		
					}
				);
</script>
<script type="text/javascript" language="javascript">
	function validate()
	{
		var username=$('#username');
		var password=$('#password');
		var password2=$('#password2');
		var firstname=$('#fname');
		var lastname=$('#lname');
		var country=$('#country');
		var state_or_province=$('#state');
		var city=$('#city');
		var email=$('#email');
		var upload=$('#upload');
		var imgupload=$('#imgupload');
		var frm=$('#registration_form');
		var email_val = /^([a-zA-Z0-9_.-])+@(([a-zA-Z0-9-])+.)+([a-zA-Z])+$/;
		
		if(username.val()=='' || password.val()=='' || password2.val()=='' || firstname.val()=='' || lastname.val()=='' || email.val()=='' || upload.val()=='' || imgupload.val()=='')
		{
			alert("Fields marked with (*) are compulsory.");
		}
		else
		{
			if(password.val().length<5)
			{
				alert("Password must be at least 5 characters");
				document.registration_form.password.focus();
			}
			else if(password.val()!=password2.val())
			{
				alert("Passwords do not match.");
				document.registration_form.password.focus();
			}
      		else if(!email_val.test(email.val()))
      		{
          		alert("Error: Email id not in correct format.");
		  		document.registration_form.email.focus();
     		}
			else if(!document.registration_form.uaccept.checked)
			{
				alert("Please read and accept the Originality Agreement.");
		  		document.registration_form.uaccept.focus();
			}
			else if(!document.registration_form.accept.checked)
			{
				alert("Please read and accept the User Agreement & Privacy Policy.");
		  		document.registration_form.accept.focus();
			}
			else
			{
				frm.submit();
			}
		}
	}
	
	function unreg_submit()
	{
		var frm=document.getElementById('unreg_form');
		var uemail=$('#uemail');
		var email_val = /^([a-zA-Z0-9_.-])+@(([a-zA-Z0-9-])+.)+([a-zA-Z])+$/;
		
		if(!email_val.test(uemail.val()))
		{
			alert("Error: Email id not in correct format.");
			document.unreg_form.uemail.focus();
		}
		else
		{
			frm.submit();
		}
	}
	
	function clearme(theText)
	{
		if (theText.value == theText.defaultValue) {
         theText.value = ""
     	}		 
	}	
</script>
	
  <div id="contentContainer">
    <div id="unregistered">
  	<?php if(!$uflag) include('unreg_form.php'); ?>
    </div>
    <h1>Purify Member Registration</h1>
    USER TYPE: ARTIST, COMMUNITY, FAN, or TEAM 
    <p>&nbsp;</p>
    <p>(NOTHING WILL DISPLAY UNTIL THE USER HAS CHOSEN A USER TYPE, THEN THE FOLLOWING TEXT WILL DISPLAY ABOVE THE APPLICABLE FORM ACCORDING TO THE USER TYPE)</p>
    <h3>ARTISTS</h3>
    <p style="font-weight: bold">Artists who have original digital content to share may register.</p>
    <p style="font-weight: bold">As a  Purify Artist you can: </p>
    <ul id="biglist">
      <li>Store and share your original media </li>
      <li>Represent yourself and promote online </li>
      <li>Sell subscriptions to your art and services and receive 100% of your sales</li>
    </ul>
    <h3>COMMUNITY</h3>
    <p style="font-weight: bold">Businesses and Organizations in the arts may also register, as a Community Member you can: </p>
    <ul id="biglist">
      <li>Host and share your media </li>
      <li>Represent yourself and promote online </li>
      <li>Sell subscriptions to your media and services and receive 100% of your sales </li>
    </ul>
    <h3>FANS</h3>
    <p style="font-weight: bold">Any Internet user may browse</p>
    <p style="font-weight: bold">the listings, as a registered  Fan you  may: </p>
    <ul id="biglist">
      <li>Store and view your favorite media </li>
      <li>Subscribe to both free and exclusive media </li>
      <li>Use the Purify Player via the web</li>
    </ul>
    <h3>TEAM</h3>
    <p style="font-weight: bold">Purify  is always looking for volunteers and potential contractors. Programmers,  Promoters, and Directors may register and help Purify the Entertainment  Industry. Once registered you will be eligible to receive a Purify Work  Contract.</p>
    <p></p>
    <form id="registration_form" name="registration_form" method="post" action="" enctype="multipart/form-data">
      <div style="width:650px;float:left;">
      <input type="hidden" name="type" value="1" />
      <table width="650" border="0" cellpadding="0" cellspacing="0">
        <tr>
          <td width="100">*Email</td>
          <td width="402"><input type="text" name="email" class="regularField" id="email" value="<?php echo $email; ?>" /></td>
          <td width="148" align="left"><div class="hint">Will stay private.</div></td>
        </tr>
        <tr>
          <td colspan="3"><img src="images/spacer.gif" alt=" " width="10" height="10" /></td>
        </tr>
        <tr>
          <td>*First Name</td>
          <td><input name="fname" type="text" class="regularField" id="fname" value="<?php echo $firstname; ?>" /></td>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td colspan="3"><img src="images/spacer.gif" alt=" " width="10" height="10" /></td>
        </tr>
        <tr>
          <td>*Last Name</td>
          <td><input name="lname" type="text" class="regularField" id="lname" value="<?php echo $lastname; ?>"/></td>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td colspan="3"><img src="images/spacer.gif" alt=" " width="10" height="10" /></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td>
            <?php echo $msg2; ?>          </td>
          <td>&nbsp;</td>
        </tr>
		<?php include_once('registration_block_type.php'); ?>
        <tr>
          <td colspan="3"><img src="images/spacer.gif" alt=" " width="10" height="10" /></td>
        </tr>
		<tr id="subtype_form">
          <td>Subtype</td>
          <td>
        <?php include_once('registration_block_subtype.php'); ?>		  </td>
          <td><input name="ADD" type="button" id="addType" value=" Add " /></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td><p>Suggest a new type or subtype by emailing info@purifyentertainment.net</p></td>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td>Country</td>
          <td><input name="country" type="text" class="regularField" id="country" value="<?php echo $country; ?>" /></td>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td colspan="3"><img src="images/spacer.gif" alt=" " width="10" height="10" /></td>
        </tr>
        <tr>
          <td>State/Province</td>
          <td><input name="state" type="text" class="regularField" id="state" value="<?php echo $state_or_province; ?>" /></td>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td colspan="3"><img src="images/spacer.gif" alt=" " width="10" height="10" /></td>
        </tr>
        <tr>
          <td>City</td>
          <td><input name="city" type="text" class="regularField" id="city" value="<?php echo $city; ?>" /></td>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td colspan="3"><img src="images/spacer.gif" alt=" " width="10" height="10" /></td>
        </tr>
        <tr>
          <td>Homepage</td>
          <td><input name="homepage" type="text" class="regularField" id="homepage" value="http://<?php echo $homepage; ?>" /></td>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td colspan="3"><img src="images/spacer.gif" alt=" " width="10" height="10" /></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td>
              <?php echo $msg4; ?>          </td>
          <td>&nbsp;</td>
        </tr>        
        <tr>
          <td>Referred by</td>
          <td><input name="referred_by" type="text" class="regularField" id="homepage" value="<?php echo $ref_by; ?>" /></td>
          <td valign="bottom"><div class="hint">Reference no.</div></td>
        </tr>
        <tr>
          <td colspan="3"><img src="images/spacer.gif" alt=" " width="10" height="10" /></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td>
            <?php echo $msg5; ?>          </td>
          <td>&nbsp;</td>
        </tr>        
        <tr>
          <td>*Upload Profile Picture</td>
          <td><input name="image" type="file" class="regularField" id="imgupload" /></td>
          <td><div class="hint">Upload a .jpg/.png/.gif image only.</div></td>
        </tr>
		<tr>
          <td>&nbsp;</td>
          <td><span class="hintp">For best results upload an image of the resolution 400x400 or higher.</span></td>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td>
            <div id="user_name_msg" class="hint">
              <?php echo $msg1; ?>            </div>          </td>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td>*Username</td>
          <td><input name="username" id="username" type="text" class="regularField" value="<?php echo $username; ?>" /></td>
          <td><a href="#" id="chk_avail_link" class="hint">Check availability</a></td>
        </tr>
        <tr>
          <td colspan="3"><img src="images/spacer.gif" alt=" " width="10" height="10" /></td>
        </tr>
        <tr>
          <td>*Password</td>
          <td><input name="password" type="password" class="regularField" id="password" /></td>
          <td><div class="hint">Must be at least 5 characters.</div></td>
        </tr>
        <tr>
          <td colspan="3"><img src="images/spacer.gif" alt=" " width="10" height="10" /></td>
        </tr>
        <tr>
          <td>*Confirm</td>
          <td><input name="password2" type="password" class="regularField" id="password2" /></td>
          <td><div class="hint">Confirm  password.</div></td>
        </tr>
		<tr>
          <td colspan="3"><img src="images/spacer.gif" alt=" " width="10" height="10" /></td>
        </tr>
		<tr>
          <td>&nbsp;</td>
          <td>
            <?php echo $msg3; ?>          </td>
          <td>&nbsp;</td>
        </tr>        
        <tr>
          <td>*Upload media</td>
          <td><input name="upload" type="file" class="regularField" id="upload" /></td>
          <td><div class="hint">Upload a .jpg or .mp3 file.</div></td>
        </tr>
		<tr>
          <td>&nbsp;</td>
          <td><p>Upload an image or mp3 you have permission to share. Your account's activation will be determined by the artistic integrity of your media.</p></td>
          <td>&nbsp;</td>
        </tr>
        <tr>        
        <tr>
          <td>Are you human?</td>
          <td><?php echo recaptcha_get_html($publickey, $error); ?></td>
          <td width="148" align="left"><div class="hint">Helps stop spam</div></td>
        </tr>
		<tr>
          <td colspan="3"><img src="images/spacer.gif" alt=" " width="10" height="10" /></td>
        </tr>
        <tr>
          <td align="right"><input id="uaccept" name="uaccept" type="checkbox" validate="required:true" /></td>
          <td colspan="2" class="hint">I agree that the content uploaded is original and accept the <a href="originalityagreement.php" target="_blank">Originality Agreement</a>.</td>
        </tr>
        <tr>
          <td colspan="3"><img src="images/spacer.gif" alt=" " width="10" height="10" /></td>
        </tr>
        <tr>
          <td align="right"><input id="accept" name="accept" type="checkbox" validate="required:true" /></td>
          <td colspan="2" class="hint">I accept and agree to the Purify Entertainment <a href="about_agreements_user.php" target="_blank">User Agreement</a> and <a href="about_agreements_privacy.php" target="_blank">Privacy Policy</a>.</td>
        </tr>
        <tr>
          <td colspan="3"><img src="images/spacer.gif" alt=" " width="10" height="10" /></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td align="right"><input type="button" name="register" id="register" value=" Register " onclick="validate();" /><input type="hidden" name="register" style="border-radius:5px; -webkit-border-radius:5px; -moz-border-radius:5px; -ms-border-radius:5px; -o-border-radius:5px;" /></td>
          <td align="right">&nbsp;</td>
        </tr>
		<tr>
          <td colspan="3"><img src="images/spacer.gif" alt=" " width="10" height="10" /></td>
        </tr>
      </table>
      </div>
	  <div style="width:255px;float:right;" id="selected-types" >	  </div>
    </form>
  </div>
  
<?php include_once('includes/footer.php'); ?>