<?php
$result = array();
$errorflag='no';

$errormsg=array();
$inputparams=array();
$username='nouser';
$userpass='nopass';
$listartists='kaz,leerick';
$format='json';

$data = file_get_contents("php://input");
$dataary=json_decode($data,true);

if (isset($dataary['format'])){    
    $format = strtolower($dataary['format']);
}
if(isset($dataary['listartists'])) {
    $listartists=$dataary['listartists'];
}else {
    $errormessage='missing profile';
    $errormsg[]=array('errormsg'=>$errormessage);
    $result[] = array('error'=>$errormsg);
    $inputparams[] = array('listartists'=>$listartists);
    $inputparams[] = array('format'=>$format);
    $inputparams[] = array('post'=>$dataary);
    $result[] = array('inputparams'=>$inputparams);
    $errorflag='yes';
}

if ($errorflag='no'){
    include_once("commons/db.php");
    include_once("classes/ViewArtistProfileURL.php");

    // add user name to bucketname in s3 url
    $root = realpath($_SERVER["DOCUMENT_ROOT"]);
    if (!class_exists('S3')) require_once ($root.'/S3.php');    
    $s3 = new S3("", "");
    
    $new_profile_class_obj = new ViewArtistProfileURL();
    
    $foundeurls=",";
    
    $listartists_exp = explode(',',$listartists);

    if(isset($listartists_exp) && count($listartists_exp)>0 && !empty($listartists_exp))
    {
        $tagged_profileurls="";
        for($j=0;$j<count($listartists_exp);$j++)
        {               
            //echo '<br>getting profileurl='.$listartists_exp[$j].' <br>';     
            $pos=strpos($foundeurls,$listartists_exp[$j]);
            if ($pos===false)
            {                            
                $foundeurls=$foundeurls.','.$listartists_exp[$j];                           
                if($listartists_exp[$j]!="")
                { 
                    //echo '<br>getting artists for profileurl='.$listartists_exp[$j].' <br>'; 
                    
                    $get_all_user_artists_for_profileurl[$j] =$new_profile_class_obj->get_user_artist_profileurl($listartists_exp[$j]);
                    
                    if($get_all_user_artists_for_profileurl[$j]["featured_media"]=="Song"){
                        $mediaid=$get_all_user_artists_for_profileurl[$j]["media_id"];
                        $get_featured_media_for_artist[$mediaid]=$new_profile_class_obj->get_Media_Song($mediaid);
                        $generaluserid= $get_featured_media_for_artist[$mediaid]["general_user_id"];
                        //echo '<br>generaluserid='.$generaluserid;
                        $s3->resetAuth("", "",false,$generaluserid);
                        $medaudioURI=$s3->getNewURI("medaudio"); 
                        $songname=$get_featured_media_for_artist[$mediaid]["song_name"];
                        $songimageurl=$new_profile_class_obj->get_audio_img($mediaid);
                        $songinfo[$mediaid]["song_title"]=$get_featured_media_for_artist[$mediaid]["songtitle"];
                        $songinfo[$mediaid]["song_url"]=$medaudioURI.$songname;
                        $songinfo[$mediaid]["creator_name"]=$get_featured_media_for_artist[$mediaid]["creator"];
                        $songinfo[$mediaid]["from_name"]=$get_featured_media_for_artist[$mediaid]["from"];
                        $songinfo[$mediaid]["song_image_url"]=$s3->replaceURI($songimageurl);
                    }
                    //print_r($get_all_user_artists_for_profileurl); 
                    //echo '<br>emails in project index ='.$j.' <br>';
                }
                //echo '<br>project index ='.$p.' <br>';
            } // skipping this profileurl already processed
        } // end of get artists for profileurl
        
    }    

//print ( '<pre>' );
//print_r($get_all_user_artists_for_profileurl);
//print ( '</pre>' );    
//die;

  
    // add alluserartistsforproject to result array
    $result[] = array('alluserartistsforprofileurl'=>$get_all_user_artists_for_profileurl);
    $result[] = array('featuredsonginfo'=>$songinfo);
    

} //end of if error == no

/* output in necessary format */
if($format == 'json') {
    header('Content-type: application/json');
    echo json_encode($result);
    //echo json_encode(array('artistprofile'=>$result));
    //echo array_to_json($result);
}
else {
    if ($format == 'xml'){
        header('Content-type: text/xml');
        echo '<result>';
        foreach($result as $index => $resultentry) {
            if(is_array($resultentry)) {
                foreach($resultentry as $key => $value) {
                    echo '<',$key,'>';
                    if(is_array($value)) {
                        foreach($value as $tag => $val) {
                            echo '<',$tag,'>',htmlentities($val),'</',$tag,'>';
                        }
                    }
                    echo '</',$key,'>';
                }
            }
        }
        echo '</result>';
    }
}

function array_to_json( $array ){

    if( !is_array( $array ) ){
        return false;
    }

    $associative = count( array_diff( array_keys($array), array_keys( array_keys( $array )) ));
    if( $associative ){

        $construct = array();
        foreach( $array as $key => $value ){

            // We first copy each key/value pair into a staging array,
            // formatting each key and value properly as we go.

            // Format the key:
            if( is_numeric($key) ){
                $key = "key_$key";
            }
            $key = "\"".addslashes($key)."\"";

            // Format the value:
            if( is_array( $value )){
                $value = array_to_json( $value );
            } else if( !is_numeric( $value ) || is_string( $value ) ){
                $value = "\"".addslashes($value)."\"";
            }

            // Add to staging array:
            $construct[] = "$key: $value";
        }

        // Then we collapse the staging array into the JSON form:
        $result = "{ " . implode( ", ", $construct ) . " }";

    } else { // If the array is a vector (not associative):

        $construct = array();
        foreach( $array as $value ){

            // Format the value:
            if( is_array( $value )){
                $value = array_to_json( $value );
            } else if( !is_numeric( $value ) || is_string( $value ) ){
                $value = "'".addslashes($value)."'";
            }

            // Add to staging array:
            $construct[] = $value;
        }

        // Then we collapse the staging array into the JSON form:
        $result = "[ " . implode( ", ", $construct ) . " ]";
    }
	
    return $result;
}

?>