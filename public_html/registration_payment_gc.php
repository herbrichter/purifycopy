<?php 
	include_once('login_includes.php');
	include_once('classes/Subscription.php');
	
	$user_id=$_GET['uid'];
	$subscription_id=$_GET['sid'];
	
	$obj=new Subscription();
	$row=$obj->getSubscriptionById($subscription_id);
	$subscription=$row['name'];
	$desc=$row['desc'];
	$cost=$row['cost'];

?>

<?php include_once('includes/header.php'); ?>
<title>Purify Entertainment: Six Feature</title>

  <div id="contentContainer">
    <h1>Purify Membership</h1>
    <p><a href="register.php">Back to All User Sign Up Page</a></p>
    <table width="100%" border="0">
      <tr>
        <td width="83%"><p>Confirm your membership below  using Google Checkout.</p></td>
        <td width="17%"></td>
      </tr>
    </table>
  </div>
	
  <h5 class="withLine">Your Membership:</h5>
    <div id="featuredHeads">
      <div class="featuredItemStackMid">
      <div id="featureHead"></div><div class="featuredItemLeftMid">
      <div class="featuredContentWide">
        <h3><?php echo $subscription; ?> Membership</h3>
        <p><?php echo $desc; ?><br />
        <a>USD <?php echo $cost; ?>.00</a></p>
      </div>
      <div class="clearMe"></div>
    </div>
    <div class="featuredItemRightMid">
      <div class="featuredButton">
         <form action="https://checkout.google.com/api/checkout/v2/checkoutForm/Merchant/515045437312695" id="BB_BuyButtonForm" method="post" name="BB_BuyButtonForm">
			<input alt="" src="https://checkout.google.com/buttons/buy.gif?merchant_id=515045437312695&amp;w=121&amp;h=44&amp;style=white&amp;variant=text&amp;loc=en_US" type="image"/>
            <input name="item_name_1" type="hidden" value="<?php echo $subscription; ?> Membership"/>
            <input name="item_description_1" type="hidden" value="<?php echo $desc; ?>"/>
            <input name="item_quantity_1" type="hidden" value="1"/>
            <input name="item_price_1" type="hidden" value="<?php echo $cost; ?>.0"/>
            <input name="item_currency_1" type="hidden" value="USD"/>
            <input name="_charset_" type="hidden" value="utf-8"/>
        </form>
      </div>
      <div class="clearMe"></div>
    </div>
</div>

<?php include_once('includes/footer.php'); ?>