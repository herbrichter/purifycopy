<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<?php	
session_start();
include('commons/db.php');
include('classes/AddToCart.php');
$add_to_cart_obj = new AddToCart();
$total=""; $sub_total="";

if(isset($_SESSION['guest_email']))
{
	if($_SESSION['guest_email']!=NULL)
	{
		unset($_SESSION['guest_email']);
		unset($_SESSION['guest_name']);
	}
}
if(isset($_GET['from_url'])){
	$back_to_url = $_GET['from_url'];
}else{
	if(isset($_SERVER['HTTP_REFERER'])){
		$back_to_url = $_SERVER['HTTP_REFERER']; }else{
		$back_to_url = ""; }
}
if(isset($_GET['viewbuyer_id']))
{
	$get_all_data = $add_to_cart_obj->Get_all_cart_data($_GET['viewbuyer_id']);
}
else
{
	if(isset($_GET['remove']) && !isset($_GET['remove_type']))
	{
		$delete_data = $add_to_cart_obj->Delete_Data($_GET['remove']);
	}
	else if(isset($_GET['removeall']) && $_GET['removeall']==1)
	{
		if($_GET['notlogin']=="yes"){
			$delete_data = $add_to_cart_obj->removeall_cart_data_notlogin($_GET['buyer_id']);
		}else{
			$delete_data = $add_to_cart_obj->removeall_cart_data_login($_GET['buyer_id']);
		}
	}
	else if(isset($_GET['remove']) && isset($_GET['remove_type']))
	{
		$delete_data = $add_to_cart_obj->Delete_Fan_Data($_GET['remove']);
	}
	else if(isset($_GET['play']) && isset($_GET['media_id']))
	{
		if(!isset($_GET['buyer_id']) || $_GET['buyer_id'] ==" " || $_GET['buyer_id'] == null){
			$add = $add_to_cart_obj->add_to_cart_play_not_login($_GET['media_id'],$_SERVER["REMOTE_ADDR"]);
			
			$res_data_tip = "";
			$get_data_tip = mysql_query("select * from general_media where id='".$_GET['media_id']."'");
			if($get_data_tip!="")
			{
				if(mysql_num_rows($get_data_tip)>0)
				{
					$res_data_tip = mysql_fetch_assoc($get_data_tip);
				}
			}
			
			if(!empty($res_data_tip))
			{
				if($res_data_tip['general_user_id']!="")
				{
					$_GET['seller_id'] = $res_data_tip['general_user_id'];
			
					if(isset($_GET['don_tips_al']) && isset($_GET['media_id']))
					{
						if($_GET['don_tips_al']!=0 && $_GET['media_id']!='' && $res_data_tip['general_user_id']!="")
						{
							$update_cart = $add_to_cart_obj->update_tip_from_own_not($_GET['don_tips_al'],$_GET['media_id'],$res_data_tip['general_user_id'],$_SERVER["REMOTE_ADDR"]);
							
							$add = $add_to_cart_obj->add_to_cart_play_not_login($_GET['media_id'],$_SERVER["REMOTE_ADDR"]);
						}
					}
				}
			}			
		}else{
			$add_play = $add_to_cart_obj->add_to_cart_play($_GET['media_id'],$_GET['buyer_id']);
			
			$res_data_tip = "";
			$get_data_tip = mysql_query("select * from general_media where id='".$_GET['media_id']."'");
			if($get_data_tip!="")
			{
				if(mysql_num_rows($get_data_tip)>0)
				{
					$res_data_tip = mysql_fetch_assoc($get_data_tip);
				}
			}
			
			if(!empty($res_data_tip))
			{
				if($res_data_tip['general_user_id']!="")
				{
					$_GET['seller_id'] = $res_data_tip['general_user_id'];
					
					if(isset($_GET['don_tips_al']) && isset($_GET['media_id']) && isset($_GET['buyer_id']))
					{
						if($_GET['don_tips_al']!=0 && $_GET['media_id']!='' && $res_data_tip['general_user_id']!="" && $_GET['buyer_id']!="")
						{
							$update_cart = $add_to_cart_obj->update_tip_from_own($_GET['don_tips_al'],$_GET['media_id'],$res_data_tip['general_user_id'],$_GET['buyer_id']);
							
							$add_play = $add_to_cart_obj->add_to_cart_play($_GET['media_id'],$_GET['buyer_id']);
						}
					}
				}
			}
		}
	}
	else if(isset($_GET['type']))
	{
		$add_gallery = $add_to_cart_obj->gallery_add_to_cart($_GET['seller_id'],$_GET['buyer_id'],$_GET['media_id'],$_GET['image_name'],$_GET['title']);
	}
	else if(isset($_GET['update_id']) && isset($_GET['update_val']))
	{
		$update_cart = $add_to_cart_obj->update_cart($_GET['update_id'],$_GET['update_val'],$_GET['table_name']);
	}
	else
	{
		if(($_GET['buyer_id'] ==" " || $_GET['buyer_id'] == null) && ($_GET['donate_purify'] =="" && !isset($_GET['display_cart']))){
			$add = $add_to_cart_obj->add_to_cart_not_login($_GET['seller_id'],$_GET['buyer_id'],$_GET['media_id'],$_GET['image_name'],$_GET['title'],0,0,0,$_SERVER["REMOTE_ADDR"]);
		}else{
			if(!isset($_GET['display_cart']) && !isset($_GET['donate_purify'])){
				$add = $add_to_cart_obj->add_to_cart($_GET['seller_id'],$_GET['buyer_id'],$_GET['media_id'],$_GET['image_name'],$_GET['title'],0,0,0);
			}
		}
	}
	if(isset($_GET['donate_purify']) && $_GET['donate_purify'] !="")
	{
		$donate_purify = $add_to_cart_obj->donate_to_purify($_GET['amount']);
	}
	
	if(!isset($_GET['buyer_id']) || $_GET['buyer_id'] ==" " || $_GET['buyer_id'] == null || !isset($_SESSION['login_email']) || $_SESSION['login_email']==null)
	{
		$get_all_data_fan = $add_to_cart_obj->get_fan_detail_notlogin();
		$get_all_data = $add_to_cart_obj->Get_all_cart_data_not_login($_SERVER["REMOTE_ADDR"]);
	}else{
		$get_all_data_fan = $add_to_cart_obj->get_Cart($_GET['buyer_id']);
		$get_all_data = $add_to_cart_obj->Get_all_cart_data($_GET['buyer_id']);
	}
	$purify_donate = $add_to_cart_obj->get_purify_donation();
}
?>
<html xmlns="http://www.w3.org/1999/xhtml" style="height: 100%;">
<head>
<link href="../includes/purify.css" rel="stylesheet" />
<link href="../sample.css" rel="stylesheet" type="text/css" />
</head>
<body >
	<div>
		<div class="fancy_header">
			Shopping Cart
		</div>
		<div id="cartWrapper" class="cartWrapperwhole">
			
				<div id="tempd_media" style="margin: 0px;">
					<div id="playlistView">
						<!--<div class="logo" style="float:left;"><img src="images/miniHeader-NewLogo.gif" /></div>-->
						<div style="width:260px; float:left;">
							<div id="totalCont_media">
								<div class="subtotal_media">
<?php 
								if(!isset($_SESSION['login_email']) || $_SESSION['login_email']=="" || $_SESSION['login_email']==null){
?>
									<div class="subtotal_media">
										<div class="title_media">Name:</div>
										<div class="cart_doll">&nbsp;</div>
										<div class="amount_media">
										<input type="text" class="field" id="user_name_notlog" value="<?php if(isset($_SESSION['guest_name'])){ echo $_SESSION['guest_name']; } ?>" style="width:100px; height:12px; font-size:11px;"/>
										</div>
									</div>
									<div class="subtotal_media">
										<div class="title_media">Email :</div>
										<div class="cart_doll">&nbsp;</div>
										<div class="amount_media">
										<input type="text" class="field" id="email_notlog" value="<?php if(isset($_SESSION['guest_email'])){ echo $_SESSION['guest_email']; }?>" style="width:100px; height:12px; font-size:11px;"/>
										</div>
									</div>
<?php
								}
									if(mysql_num_rows($get_all_data_fan)>0){
										while($get_cart = mysql_fetch_assoc($get_all_data_fan)){
											$total  = $total + $get_cart['price'] + $get_cart['donate'];
										}
									}
									if(mysql_num_rows($get_all_data)>0){
										while($ans_alls = mysql_fetch_assoc($get_all_data)){
											$total  = $total + $ans_alls['price'] + $ans_alls['donate'];
										}
									}
?>
									<div class="title_media">Subtotal:</div>
									<div class="cart_doll" id="totle_sin_rows1">$<?php echo $total;?></div>
									<div class="amount_media"> </div>
								</div>
								<div class="subtotal_media">
									<div class="title_media">Donate to Purify Art:</div>
									 <div class="cart_doll">$</div>
									<div class="amount_media"><input type="text" class="field" value="<?php if(isset($purify_donate['amount'])){ echo $purify_donate['amount']; }?>" id="donate_purify" style="width:75px; height:12px; font-size:11px;"/>
									<a onclick="donate_to_purify()" href="javascript:void(0);">Update</a>
									
									</div>
									<!--<div class="donate_update">
									<a href="javascript:void(0);" onclick="update_cart()" >Update</a>
									</div>-->
								</div>
							</div>
							<div class="total_amount_Cont_media">
								<div class="subtotal_media">
									<?php
										$total = $total + $purify_donate['amount'];
									?>
									<div class="title_media">Total:</div>
									<div class="cart_doll" id="totle_sin_rows2">$<?php echo $total;?></div>
									 <input type="hidden" id="grand_total" value="<?php echo $total;?>"/>
									<div class="amount_media"></div>
								</div>
							</div>
						</div>
						<div style="width:140px; float:left;">
							<div id="totalCont_new_media">
								<div class="shopping_media">
									<input type="button" style="border-radius:5px;margin-bottom:10px;" value="Continue Shopping" class="button_media" onclick="continue_shopping()">
									<input type="button" style="border-radius:5px;float:right;" onclick="get_payment_form()" value="Checkout" class="button_media">
								</div>
							</div>
						</div>
					</div>
        
				<!-- PlayList Coding -->
				<div id="playlistView">
					<div id="listFiles_media" style="height:32px;">
						<div class="titleCont_media">
							<div class="blkB_media">Title</div>
							<div class="blkC_media">Price</div>
							<div class="blkD_media">Tip</div>
							<div class="blkE_media">Total</div>
							<div class="blkF_media">-</div>
						</div>
					</div>
					<div id="listFiles_media" style="height:65px;">
					
						<?php
						
						if(!isset($_GET['buyer_id']) || $_GET['buyer_id'] ==" " || $_GET['buyer_id'] == null || !isset($_SESSION['login_email']) || $_SESSION['login_email']==null)
						{
							$get_all_data = $add_to_cart_obj->Get_all_cart_data_not_login($_SERVER["REMOTE_ADDR"]);
							$get_all_data_fan = $add_to_cart_obj->get_fan_detail_notlogin();
						}else{
							$get_all_data = $add_to_cart_obj->Get_all_cart_data($_GET['buyer_id']);
							$get_all_data_fan = $add_to_cart_obj->get_Cart($_GET['buyer_id']);
						}
						while($get_cart_fan = mysql_fetch_assoc($get_all_data_fan))
						{
							$get_seller_name = $add_to_cart_obj->Get_seller_name($get_cart_fan['seller_id']);
							if($get_cart_fan['table_name']=="general_community"){$id_name= "community_id";}
							if($get_cart_fan['table_name']=="community_project"){$id_name= "id";}
							if($get_cart_fan['table_name']=="general_artist"){$id_name= "artist_id";}
							if($get_cart_fan['table_name']=="artist_project"){$id_name= "id";}
							$get_cart_data = $add_to_cart_obj->get_cart_data($get_cart_fan['table_name'],$id_name,$get_cart_fan['type_id']);
							if($get_cart_fan['table_name']=="general_community"){$image_name = "http://comjcropthumb.s3.amazonaws.com/".$get_cart_data['listing_image_name'];$title = $get_cart_data['name'];$creator_disp = $get_cart_data['name'];$creator_url = $get_cart_data['profile_url'];}
							if($get_cart_fan['table_name']=="community_project"){$image_name = $get_cart_data['listing_image_name'];$title = $get_cart_data['title'];
								if($get_cart_data['type']=="for_sale"){
									$creator_disp = $get_cart_data['creator'];
									$exp_creator_info = explode("|",$get_cart_data['creators_info']);
									$get_creator_url = $add_to_cart_obj->get_creator_profile_url($exp_creator_info[0],$exp_creator_info[1]);
									$creator_url = $get_creator_url['profile_url'];
									$title_url = $get_cart_data['profile_url'];
								}else{
									$creator_disp = $get_cart_data['title'];
									$creator_url = $get_cart_data['profile_url'];
								}
								
							}
							if($get_cart_fan['table_name']=="general_artist"){$image_name = "http://artjcropthumb.s3.amazonaws.com/".$get_cart_data['listing_image_name'];$title = $get_cart_data['name'];$creator_disp = $get_cart_data['name'];$creator_url = $get_cart_data['profile_url'];}
							if($get_cart_fan['table_name']=="artist_project"){$image_name = $get_cart_data['listing_image_name'];$title = $get_cart_data['title'];
								if($get_cart_data['type']=="for_sale"){
									$exp_creator_info = explode("|",$get_cart_data['creators_info']);
									$get_creator_url = $add_to_cart_obj->get_creator_profile_url($exp_creator_info[0],$exp_creator_info[1]);
									$creator_url = $get_creator_url['profile_url'];
									$creator_disp = $get_cart_data['creator'];
									$title_url = $get_cart_data['profile_url'];
								}else{
									$creator_disp = $get_cart_data['title'];
									$creator_url = $get_cart_data['profile_url'];
								}
								
							}
							$get_cart_price = $add_to_cart_obj->get_cart_price($get_cart_fan['seller_id'],$get_cart_fan['buyer_id'],$get_cart_fan['type_id'],$get_cart_fan['table_name']);
							$title = str_replace("\'","'",$title);
						?>
							<div class="tableCont_media">
								<div id="<?php echo $get_cart_fan['id'];?>">
									<div class="blkB_media"><img src="<?php echo $image_name;?>" width="50" height="50" /><?php if($get_cart_data['type']=="for_sale"){ if(strlen($title)>26){echo "<a href='javascript:void(0)' onclick=click_profile_url('$title_url')>".substr($title,0,26)."..</a>";}else{ echo "<a href='javascript:void(0)' onclick=click_profile_url('$title_url')>".$title."</a>"; } }else{ $title = $title ." Fan Club Membership"; if(strlen($title)>26){ echo substr($title,0,26)."..";}else{ echo $title;} }?><br /><a href="javascript:void(0)" onclick="click_profile_url('<?php echo $creator_url;?>')"><?php echo $creator_disp;?></a></div>
									<div class="blkC_media">$<?php echo $get_cart_fan['price'];?></div>
									<div class="blkD_media">$<input type="text" id="donate<?php echo $get_cart_fan['id'];?>" value="<?php echo $get_cart_fan['donate'];?>" class="field" onblur="chk_for_update('donate<?php echo $get_cart_fan['id'];?>','<?php echo $get_cart_fan['id'];?>','add_to_cart_fan_pro')" /></div>
									<div class="blkE_media" id="totle_sin_rows_<?php echo $get_cart_fan['id'];?>">$<?php echo $get_cart_fan['total'];?></div>
									<div class="blkF_media"><a href="javascript:void(0);" onclick="update_cart('<?php echo $get_cart_fan['id'];?>','add_to_cart_fan_pro','<?php echo $get_cart_fan['price']; ?>')" style="width:20px; height:12px; font-size:11px;" >Update</a> | <a href="javascript:void(0);" onclick ="confirm_remove('<?php echo $get_cart_fan['id'];?>','<?php echo $_GET['buyer_id'];?>','fan_club','<?php echo $get_cart_fan['total'];?>')">Remove</a></div>
								</div>
								<!--<span style="color:red;display:none;font-size:9px;font-weight:bold;" id="error<?php //echo $get_cart_fan['id'];?>">Item is not updated.</span>-->
							</div>
						<?php
						//$sub_total = $sub_total + $get_cart_data['price'] + $get_cart_data['donate'];
						//$sub_total = $sub_total + $get_cart_price;
						
						}
						
						while($get_cart = mysql_fetch_assoc($get_all_data))
						{
							//$get_seller_name = $add_to_cart_obj->Get_seller_name($get_cart['seller_id']);
							$get_seller_name = $add_to_cart_obj->Get_media_creator($get_cart['media_id']);
							$exp_creator_info = explode("|",$get_seller_name['creator_info']);
							$get_creator_url = $add_to_cart_obj->get_creator_profile_url($exp_creator_info[0],$exp_creator_info[1]);
							$get_cart['title'] = str_replace("\'","'",$get_cart['title']);
						?>
							<div class="tableCont_media">
								<div id="<?php echo $get_cart['id'];?>">
									<div class="blkB_media"><img src="<?php echo $get_cart['image_name'];?>" width="50" height="50" /><?php if(strlen($get_cart['title'])>26){ echo substr($get_cart['title'],0,26)."..";}else{ echo $get_cart['title']; }?><br /><a href="javascript:void(0)" onclick="click_profile_url('<?php echo $get_creator_url['profile_url'];?>')"><?php echo $get_seller_name['creator'];?></a></div>
									<div class="blkC_media">$<?php echo $get_cart['price'];?></div>
									<div class="blkD_media">$<input type="text" id="donate_cart<?php echo $get_cart['id'];?>" value="<?php echo $get_cart['donate'];?>" class="field" onblur="chk_for_update('donate_cart<?php echo $get_cart['id'];?>','<?php echo $get_cart['id'];?>','addtocart')" style="width:20px; height:12px; font-size:11px;" /></div>
									<div class="blkE_media" id="totle_sin_rows_<?php echo $get_cart['id'];?>">$<?php echo $get_cart['price'] + $get_cart['donate'];?></div>
									<div class="blkF_media"><a href="javascript:void(0);" onclick="update_donate_cart('<?php echo $get_cart['id'];?>','addtocart','<?php echo $get_cart['price']; ?>')" >Update</a> | <a href="javascript:void(0);" onclick ="confirm_remove('<?php echo $get_cart['id'];?>','<?php if(isset($_GET['buyer_id'])){ echo $_GET['buyer_id']; } ?>','song','<?php echo $get_cart['price'] + $get_cart['donate'];?>')">Remove</a>
									<!--<span style="color:red;display:none;font-size:9px;font-weight:bold;" id="error<?php //echo $get_cart['id'];?>">Item is not updated.</span>-->
									</div>
								</div>
							</div>
						<?php
						$sub_total = $sub_total + $get_cart['price'] + $get_cart['donate'];
						}
						?>
					   
					</div>
					
				</div>
			</div>
		</div>
	</div>
<input type="hidden" id="idandvalue"/>
</body>
<script type="text/javascript" src="javascripts/jquery.js"></script>
<script type="text/javascript">
function get_payment_form()
{
	var donate = document.getElementById("idandvalue").value;

	var donate_puri = document.getElementById("donate_purify").value;
	var cart_items = document.getElementById("grand_total").value;
	var expdonate = donate.split("|");
	var expdonate2 = expdonate[0].split(",");
	if(donate_puri>0 || cart_items>0 || expdonate2[1]>0){
		<?php 
		if(!isset($_SESSION['login_email']) || $_SESSION['login_email']==null){
		?>
			var notlog_user = document.getElementById("user_name_notlog").value;
			var notlog_email = document.getElementById("email_notlog").value;
			if(notlog_user == "" || notlog_email == "" || !IsValidEmail(notlog_email))
			{
				alert("You need to provide you name and email address so we can contact you.");
			}else{
				if(donate_puri !="" || donate_puri !=0 || donate_puri >0){
					openurl = "../updatecart.php?alldata="+donate+"&buyer_id="+'<?php if(isset($_GET['buyer_id'])){ echo $_GET['buyer_id']; } ?>'+"&from_url="+'<?php echo $back_to_url; ?>&donate_purify=1&amount='+donate_puri+'&notlog_user_name='+notlog_user+'&notlog_user_email='+notlog_email;
				}else{
					openurl = "../updatecart.php?alldata="+donate+"&buyer_id="+'<?php if(isset($_GET['buyer_id'])){ echo $_GET['buyer_id']; } ?>'+"&from_url="+'<?php echo $back_to_url; ?>'+'&notlog_user_name='+notlog_user+'&notlog_user_email='+notlog_email;
				}
				window.parent.open(openurl, '_blank');
			}
		<?php
		}else{
		?>
		if(donate_puri !="" || donate_puri !=0 || donate_puri >0){
			openurl = "../updatecart.php?alldata="+donate+"&buyer_id="+'<?php echo $_GET['buyer_id']; ?>'+"&from_url="+'<?php echo $back_to_url; ?>&donate_purify=1&amount='+donate_puri;
		}else{
			openurl = "../updatecart.php?alldata="+donate+"&buyer_id="+'<?php echo $_GET['buyer_id']; ?>'+"&from_url="+'<?php echo $back_to_url; ?>';
		}
		window.parent.open(openurl, '_blank');
		<?php
		}
		?>

	}else{
		alert("There are no items in your cart to checkout.");
	}
}
function update_cart(id,tbl_name,actprice)
{
	var donate = document.getElementById("donate"+id).value;
	//var donate_val = donate.split("$");

	$.ajax({
		type: "GET",
		url: '../addtocart_media.php',
		data: { "update_id":id, "update_val":donate, "buyer_id":'<?php if(isset($_GET['buyer_id'])){ echo $_GET['buyer_id']; } ?>', "table_name":tbl_name, "from_url":'<?php echo $back_to_url;?>' },
		success: function(data){
			var spls_item_grd = $("#totle_sin_rows_"+id).html().split('$');
			
			var new_gr_totls_item = parseFloat(actprice) + parseFloat(donate);
			var old_gr_totls_item = parseFloat(spls_item_grd[1]);
			var diff_old_new = new_gr_totls_item - old_gr_totls_item;
			
			var spls_item_grd_upes = $("#totle_sin_rows1").html().split('$');
			
			if(diff_old_new>0)
			{
				var gr_totls_ups = parseFloat(document.getElementById("grand_total").value) + parseFloat(diff_old_new);
				var ahg_upes_totls_ups = parseFloat(spls_item_grd_upes[1])+parseFloat(diff_old_new);
				//document.getElementById("total_pass").value = gr_totls_ups;
				document.getElementById("grand_total").value = gr_totls_ups;
				$("#totle_sin_rows1").html('$'+ahg_upes_totls_ups);
				$("#totle_sin_rows2").html('$'+gr_totls_ups);
				$("#totle_sin_rows_"+id).html('$'+new_gr_totls_item);
			}
			else if(diff_old_new<0)
			{
				var gr_totls_ups = parseFloat(document.getElementById("grand_total").value) + parseFloat(diff_old_new);
				var ahg_upes_totls_ups = parseFloat(spls_item_grd_upes[1])+parseFloat(diff_old_new);
				//document.getElementById("total_pass").value = gr_totls_ups;
				document.getElementById("grand_total").value = gr_totls_ups;
				$("#totle_sin_rows1").html('$'+ahg_upes_totls_ups);
				$("#totle_sin_rows2").html('$'+gr_totls_ups);
				$("#totle_sin_rows_"+id).html('$'+new_gr_totls_item);
			}
		}
	});
}
function donate_to_purify()
{
	var donate = document.getElementById("donate_purify").value;

	$.ajax({
		type: "GET",
		url: '../addtocart_media.php',
		data: { "donate_purify":'1', "amount":donate, "seller_id":'<?php if(isset($_GET['seller_id'])){ echo $_GET['seller_id']; } ?>', "media_id":'<?php if(isset($_GET['media_id'])){ echo $_GET['media_id']; } ?>', "buyer_id":'<?php if(isset($_GET['buyer_id'])){ echo $_GET['buyer_id']; } ?>', "title":'<?php if(isset($_GET['title'])){ echo urlencode($_GET['title']); } ?>' },
		success: function(data){
			var spls_item_grd_upes = $("#totle_sin_rows1").html().split('$');
			var gr_totls_ups = parseFloat(spls_item_grd_upes[1]) + parseFloat(donate);
			//var gr_totls_ups = parseFloat(act_totls) + parseFloat(donate);
			//document.getElementById("total_pass").value = gr_totls_ups;
			document.getElementById("grand_total").value = gr_totls_ups;
			//$("#totle_sin_rows1").html('$'+gr_totls_ups);
			$("#totle_sin_rows2").html('$'+gr_totls_ups);
		}
	});
}
function update_donate_cart(id,tbl_name,actprice)
{
	var donate = document.getElementById("donate_cart"+id).value;

	$.ajax({
		type: "GET",
		url: '../addtocart_media.php',
		data: { "update_id":id, "update_val":donate, "buyer_id":'<?php if(isset($_GET['buyer_id'])){ echo $_GET['buyer_id']; } ?>', "table_name":tbl_name, "from_url":'<?php echo $back_to_url;?>' },
		success: function(data){
		
			var spls_item_grd = $("#totle_sin_rows_"+id).html().split('$');
			
			var new_gr_totls_item = parseFloat(actprice) + parseFloat(donate);
			var old_gr_totls_item = parseFloat(spls_item_grd[1]);
			var diff_old_new = new_gr_totls_item - old_gr_totls_item;
			
			var spls_item_grd_upes = $("#totle_sin_rows1").html().split('$');
			if(diff_old_new>0)
			{
				//var gr_totls_ups_fr_single = parseFloat(document.getElementById("total_pass").value) + parseFloat(diff_old_new);
				var gr_totls_ups = parseFloat(document.getElementById("grand_total").value) + parseFloat(diff_old_new);
				var ahg_upes_totls_ups = parseFloat(spls_item_grd_upes[1])+parseFloat(diff_old_new);
				
				//document.getElementById("total_pass").value = gr_totls_ups_fr_single;
				$("#totle_sin_rows_"+id).html('$'+new_gr_totls_item);
				
				document.getElementById("grand_total").value = gr_totls_ups;
				$("#totle_sin_rows1").html('$'+ahg_upes_totls_ups);
				$("#totle_sin_rows2").html('$'+gr_totls_ups);
				
			}
			else if(diff_old_new<0)
			{
				//var gr_totls_ups_fr_single = parseFloat(document.getElementById("total_pass").value) + parseFloat(diff_old_new);
				var gr_totls_ups = parseFloat(document.getElementById("grand_total").value) + parseFloat(diff_old_new);				
				var ahg_upes_totls_ups = parseFloat(spls_item_grd_upes[1])+parseFloat(diff_old_new);
				
				//document.getElementById("total_pass").value = gr_totls_ups_fr_single;
				$("#totle_sin_rows_"+id).html('$'+new_gr_totls_item);
				
				document.getElementById("grand_total").value = gr_totls_ups;
				$("#totle_sin_rows1").html('$'+ahg_upes_totls_ups);
				$("#totle_sin_rows2").html('$'+gr_totls_ups);				
			}
		}
	});
}
function confirm_remove(ids,buys_ids,tiyp,totals_of_al)
{
	var conf = confirm("Are You Sure You Want To Remove This Item From Cart.");
	if(conf == false)
	{
		return false;
	}
	else if (conf == true)
	{
		var donate = document.getElementById("donate_purify").value;
		
		if(tiyp=='song')
		{
			$.ajax({
				type: "GET",
				url: '../addtocart_media.php',
				data: { "remove":ids, "buyer_id":buys_ids, "from_url":'<?php echo $back_to_url;?>' },
				success: function(data){
					var gr_total = parseFloat(document.getElementById("grand_total").value) - parseFloat(totals_of_al);
					
					var spls_item_grd = $("#totle_sin_rows1").html().split('$');
					var new_gr_totls_item = parseFloat(spls_item_grd[1]) - parseFloat(totals_of_al);
					
					document.getElementById("grand_total").value = gr_total;
					$("#totle_sin_rows2").html('$'+gr_total);				
					$("#totle_sin_rows1").html('$'+new_gr_totls_item);
					
					if(tiyp=='song')
					{
						//var single_song_total = parseFloat(document.getElementById("total_pass").value) - parseFloat(totals_of_al);
						//document.getElementById("total_pass").value = single_song_total;
					}
					$("#listFiles_media .tableCont_media #"+ids).html('');
				}
			});
		}
		else if(tiyp=='fan_club')
		{
			$.ajax({
				type: "GET",
				url: '../addtocart_media.php',
				data: { "remove":ids, "remove_type":'fan_club', "buyer_id":buys_ids, "from_url":'<?php echo $back_to_url;?>' },
				success: function(data){
					var gr_total = parseFloat(document.getElementById("grand_total").value) - parseFloat(totals_of_al);
					
					var spls_item_grd = $("#totle_sin_rows1").html().split('$');
					var new_gr_totls_item = parseFloat(spls_item_grd[1]) - parseFloat(totals_of_al);
					
					document.getElementById("grand_total").value = gr_total;
					$("#totle_sin_rows2").html('$'+gr_total);				
					$("#totle_sin_rows1").html('$'+new_gr_totls_item);
					
					if(tiyp=='song')
					{
						//var single_song_total = parseFloat(document.getElementById("total_pass").value) - parseFloat(totals_of_al);
						//document.getElementById("total_pass").value = single_song_total;
					}
					$("#listFiles_media .tableCont_media #"+ids).html('');
				}
			});
		}
	}
}
function confirmdelete()
{
	var conf = confirm("Are you sure you want to remove all items from your cart.");
	if(conf == false)
	{
		return false;
	}
}
function chk_for_update(id,val,tblname)
{
	var a = document.getElementById(id);
	var sendval = document.getElementById("idandvalue");
	if(a.defaultValue==0 && a.value>0){
		sendval.value = sendval.value +","+ a.value +"|"+ val +"|"+tblname;
	}else if(a.defaultValue != a.value){
		sendval.value = sendval.value +","+ a.value +"|"+ val +"|"+tblname;
	}
}
function alerttoupdate(){
alert("Please update first.");
}
function continue_shopping(){
	var donate = document.getElementById("idandvalue").value;
	var donate_puri = document.getElementById("donate_purify").value;
	var spl_hashed = location.hash.split('#');
	
	if(donate_puri !="" || donate_puri !=0 || donate_puri >0){
		$.ajax({
			type : "GET",
			url : "../updatecart.php?alldata="+donate+"&buyer_id="+'<?php if(isset($get_buyer['general_user_id'])){ echo $get_buyer['general_user_id']; } ?>'+"&back_url="+'<?php echo $back_to_url; ?>&donate_purify=1&amount='+donate_puri+"&hash_changes_media="+encodeURIComponent(spl_hashed[1]),
			success : function(){
				$.fancybox.close();
			}
		});

	}else{
		$.ajax({
			type : "GET",
			url : "../updatecart.php?alldata="+donate+"&buyer_id="+'<?php if(isset($get_buyer['general_user_id'])){ echo $get_buyer['general_user_id']; } ?>'+"&back_url="+'<?php echo $back_to_url; ?>'+"&hash_changes_media="+encodeURIComponent(spl_hashed[1]),
			success : function(){
				$.fancybox.close();
			}
		});

	}
//parent.jQuery.fancybox.close();
}

function click_profile_url(url)
{
	openurl = "/"+url;
	//window.parent.open(openurl, '_self');
	window.parent.open(openurl, '_blank');
}

function IsValidEmail(strValue)
{
 var objRegExp  = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
 return objRegExp.test(strValue);
}


/*$("document").ready(function(){
$("#fancybox-content").css({"width":"623px"});
});*/
</script>
</html>
