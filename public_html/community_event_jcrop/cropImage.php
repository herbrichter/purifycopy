<?php
include_once("../commons/db.php");
error_reporting(0);
/**
 * Jcrop image cropping plugin for jQuery
 * Example cropping script
 * @copyright 2008-2009 Kelly Hallman
 * More info: http://deepliquid.com/content/Jcrop_Implementation_Theory.html
 */
if ($_REQUEST['action'] == 'crop')
{
	$filename = basename($_REQUEST['srcImg']);
	$path_to_image_directory = "./croppedFiles/";
	if($_REQUEST['resize']=='350')
	{
		$path_to_image_directory = "./croppedFiles/thumb/";
		$targ_w = 200;
		$targ_h = 200;
		$filename = 'top_thumb_'.$filename;
		
		$getid=explode('_',$filename);
		//$sql="update general_community set listing_image_name='".$filename."' where community_id='".$getid['2']."'";
		//echo $sql;
		//mysql_query($sql);
		$jpeg_quality = 100;
		$src = $_REQUEST['srcImg'];//'demo_files/pool.jpg';
		/*$src = "../".$src;
		if(!file_exists($_REQUEST['srcImg']))
		{
			//echo $src;
			//echo "file not exists";
		}*/
		$img_r = imagecreatefromjpeg($src);
		//print_r($img_r);
		$dst_r = ImageCreateTrueColor( $targ_w, $targ_h );

		imagecopyresampled($dst_r,$img_r,0,0,$_POST['x'],$_POST['y'],$targ_w,$targ_h,$_POST['w'],$_POST['h']);

	//	header('Content-type: image/jpeg');
		imagejpeg($dst_r,$path_to_image_directory.$filename,$jpeg_quality);
		
		//echo $dst_r;
        $root = realpath($_SERVER["DOCUMENT_ROOT"]);
        if (!class_exists('S3')) require_once ($root.'/S3.php');
		if (!defined('awsAccessKey')) define('awsAccessKey', '');
		if (!defined('awsSecretKey')) define('awsSecretKey', '');
		$s3 = new S3(awsAccessKey, awsSecretKey);
		$bucket_name = "comeventhumjcrop";
		$s3->putObject(S3::inputFile($path_to_image_directory.$filename),$bucket_name,$filename, S3::ACL_PUBLIC_READ);
		unlink($path_to_image_directory.$filename);
		echo "http://comeventhumjcrop.s3.amazonaws.com/" .$filename;
		//echo 'community_event_jcrop/'.$path_to_image_directory.$filename;
	}
	else
	{
		$path_to_image_directory = "./croppedFiles/profile/";
		$targ_w = $targ_h = 450;
		$filename = 'thumbnail_'.$filename;
		
		$getid=explode('_',$filename);
		//$sql=mysql_query("update general_community set image_name='".$filename."' where community_id='".$getid['1']."'");
	
		$jpeg_quality = 100;
		$src = $_REQUEST['srcImg'];//'demo_files/pool.jpg';
		//$src = "../".$src;
		//if(!file_exists($_REQUEST['srcImg']))
		//{
			//echo $src;
			//echo "file not exists";
		//}
		$img_r = imagecreatefromjpeg($src);
		//print_r($img_r);
		$dst_r = ImageCreateTrueColor( $targ_w, $targ_h );

		imagecopyresampled($dst_r,$img_r,0,0,$_POST['x'],$_POST['y'],$targ_w,$targ_h,$_POST['w'],$_POST['h']);

	//	header('Content-type: image/jpeg');
		imagejpeg($dst_r,$path_to_image_directory.$filename,$jpeg_quality);
		
		//echo $dst_r;
        $root = realpath($_SERVER["DOCUMENT_ROOT"]);
        if (!class_exists('S3')) require_once ($root.'/S3.php');
		if (!defined('awsAccessKey')) define('awsAccessKey', '');
		if (!defined('awsSecretKey')) define('awsSecretKey', '');
		$s3 = new S3(awsAccessKey, awsSecretKey);
		$bucket_name = "comeventprojcrop";
		$s3->putObject(S3::inputFile($path_to_image_directory.$filename),$bucket_name,$filename, S3::ACL_PUBLIC_READ);
		unlink($path_to_image_directory.$filename);
		echo "http://comeventprojcrop.s3.amazonaws.com/" .$filename;
	}
}

// If not a POST request, display page below:

?>