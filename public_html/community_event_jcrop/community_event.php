    <!--<script src="community_community_community_community_jcrop/js/jquery.min.js" type="text/javascript"></script>-->
    <script src="community_event_jcrop/js/jquery.Jcrop.js" type="text/javascript"></script>
    <link rel="stylesheet" href="community_event_jcrop/css/jquery.Jcrop.css" type="text/css" />
    <link rel="stylesheet" href="community_event_jcrop/demo_files/demos.css" type="text/css" />

	<!--<script src="community_event_jcrop/js/popup.js" type="text/javascript"></script>
    <link rel="stylesheet" href="community_event_jcrop/css/popup.css" type="text/css" />-->

	<script type="text/javascript" src="uploadify/swfobject.js"></script>
	<script type="text/javascript" src="uploadify/jquery.uploadify.v2.1.4.min.js"></script>
	<link href="uploadify/uploadify.css" rel="stylesheet"/>

	<?php
		/*if(isset($_SESSION['login_email']))
		{
			$sql = mysql_query("select * from  general_user where email='".$_SESSION['login_email']."'");
			$res=mysql_fetch_assoc($sql);
			//var_dump($res);

			$sql1=mysql_query("select * from  general_community where community_id = '".$res['community_id']."'");
			$res1=mysql_fetch_assoc($sql1);
		}*/
		$random_number = rand(1,1000000);
	?>
	<script type="text/javascript">
	var myUploader = null;
	var chk_uploader = 0;
	var iMaxUploadSize = 10485760; //10MB
	var IMG_UPLOAD_PATH = "community_event_jcrop/croppingFiles/";
	// Create variables (in this scope) to hold the API and image size
	var community_event_jcrop_api, boundx, boundy;
	var allowResizeCropper = false;
    jQuery(function($){
		$("#cropBtn").click(function(){
			if($('#x').val()!="" && $("#target").attr('src')!=""){
				$.ajax({
					type: "POST",
					url: 'community_event_jcrop/cropImage.php',
					data: { "action":'crop',"resize":allowResizeCropper,"x":$('#x').val(), "y":$('#y').val(), "w":$('#w').val(), "h":$('#h').val(), "srcImg":$("#target").attr('src')},
					success: function(data){
						//alert($("#target").attr('src'));
						//disablePopup();
						//window.location.href="profileedit_community.php";
						//window.location.reload();
						parent.jQuery.fancybox.close();
						if(allowResizeCropper=="350")
						{
							var cepro_img= new Date();
							//alert(a.getTime());
							data3 = data + "?ret=" + cepro_img.getTime();
							
							$("#resultImgThumb").attr('src',data3);
							$("#resultImgThumb").show();
							$("#Noimagediv_listing").hide();
							$("#stored_listing_image").hide();
							var pro_pic_thum=window.parent.document.getElementById("listing_image");
							pro_pic_thum.value=data;
						}
						else
						{
							var cpro_img= new Date();
							//alert(a.getTime());
							data4 = data + "?tet=" + cpro_img.getTime();
							
							$("#resultImgProfile").attr('src',data4);
							$("#resultImgProfile").show();
							var pro_pic=window.parent.document.getElementById("profile_image");
							pro_pic.value=data;
							$("#Noimagediv").hide();
							$("#stored_profile_image").hide();
							//var diff=data.split("/");
							//alert(diff['4']);
							//window.parent.document.getElementById("pro_pic").value = diff['4'];
							//alert(a);
							//a = diff['4'];
							//alert(a);
						}	
					}
				});
			}
		});
    });


	function generateUploader(){
		//alert(myUploader);
		if(chk_uploader==0){
			myUploader = {
				uploadify : function(){
					$('#file_upload').uploadify({
						'uploader'  : 'uploadify/uploadify.swf',
						'script'    : 'community_event_jcrop/uploadFiles.php?id=<?php echo $random_number; ?>',
						'cancelImg' : 'uploadify/cancel.png',
						'folder'    : 'community_event_jcrop/croppingFiles/',
						'auto'      : true,
						'multi'		: true,
						'removeCompleted' : true,
						'wmode'		: 'transparent',
						'buttonText': 'Upload Image',
						'fileExt'     : '*.jpg;*.gif;*.png',
						'fileDesc'    : 'Image Files',
						'simUploadLimit' : 1,
						'sizeLimit'	: iMaxUploadSize, //10 MB size
						'onComplete': function(event, ID, fileObj, response, data) {
							// On File Upload Completion			
							//location = 'uploadtos3.php?uploads=complete';
							//$("#uploadedImages").val($("#uploadedImages").val() + response + "|");
							if(response=="invalid"){
								$("#errMsg").html("Invalid image size.");
							}else{
								$("#errMsg").html('');
								$("#cropperSpace").show();
								//alert(IMG_UPLOAD_PATH);
								//alert(response);
								generateCropperObj(response);
							}
							setTimeout(function(){
								$.fancybox.update();
							},1000);
						},
						'onSelect' : function (event, ID, fileObj){
						},
						'onSelectOnce' : function(event, data)
						{
							/*iTotFileSize = data.allBytesTotal;
							if(iTotFileSize >= iMaxUploadSize){
								var iOverSize = (iTotFileSize - iMaxUploadSize) / 1024 / 1024; // Size, in MB
								$("#divGalleryFileSize").show().html("Maximum 10MB allowed. Total queue size approximately: <font color='red'>" + Math.ceil(iTotFileSize/1024/1024) + " (MB)</font>");
								$('#file_upload').uploadifyClearQueue();
							}
							else
							{
							   $("#divGalleryFileSize").hide() 
							}*/
						},
						'onOpen'	: function() {
							//hide overly
						}
						/*,
						'onError'     : function (event,ID,fileObj,errorObj) {
						  alert(errorObj.type + ' Error: ' + errorObj.info);
						},
						'onProgress'  : function(event,ID,fileObj,data) {
						  var bytes = Math.round(data.bytesLoaded / 1024);
						  $('#containerResult').html(' - ' + bytes + 'KB Uploaded');
						  return false;
						}*/

						});
					}
				};
				myUploader.uploadify();
				
				chk_uploader = 1;
		}
	}

function generateCropperObj(img){
//alert(img);
      if(community_event_jcrop_api){ 
		community_event_jcrop_api.destroy(); 
	  }
	  
		$("#targetTD").html('<img id="target">');
		$("#target").attr('src',img);
		$("#preview").attr('src',img);

      $('#target').Jcrop({
        onChange: updatePreview,
        onSelect: updateCoords,
		allowResize: allowResizeCropper,
		allowSelect: allowResizeCropper,
        aspectRatio: 1
      },function(){
        // Use the API to get the real image size
        var bounds = this.getBounds();
        boundx = bounds[0];
        boundy = bounds[1];
        community_event_jcrop_api = this;
		if(!allowResizeCropper){
			community_event_jcrop_api.animateTo([0,0,450,450]);
			community_event_jcrop_api.setOptions({ allowSelect: false });
			community_event_jcrop_api.setOptions({ allowResize: true });
			 community_event_jcrop_api.setOptions(true? {
            minSize: [ 450, 450 ],
            maxSize: [ 0, 0 ]
          }: {
            minSize: [ 0, 0 ],
            maxSize: [ 0, 0 ]
          });
		  $("#hinttext").text("Image size should be more than 450x450.");
		}
		if(allowResizeCropper=="350"){
			community_event_jcrop_api.animateTo([0,0,200,200]);
			community_event_jcrop_api.setOptions({ allowSelect: false });
			community_event_jcrop_api.setOptions({ allowResize: true });
			community_event_jcrop_api.setOptions(true? {
            minSize: [ 200, 200 ],
            maxSize: [ 0, 0 ]
          }: {
            minSize: [ 0, 0 ],
            maxSize: [ 0, 0 ]
          });
		  $("#hinttext").text("Image size should be more than 450x450.");
		}
		
        // Store the API in the community_event_jcrop_api variable
      });

	function updateCoords(c)
	{
		$('#x').val(c.x);
		$('#y').val(c.y);
		$('#w').val(c.w);
		$('#h').val(c.h);
		updatePreview(c);
	};

      function updatePreview(c)
      {
        if (parseInt(c.w) > 0)
        {
          var rx = 100 / c.w;
          var ry = 100 / c.h;

          $('#preview').css({
            width: Math.round(rx * boundx) + 'px',
            height: Math.round(ry * boundy) + 'px',
            marginLeft: '-' + Math.round(rx * c.x) + 'px',
            marginTop: '-' + Math.round(ry * c.y) + 'px'
          });
        }
      };
}

function showCropper(resizeFlag){
	allowResizeCropper = resizeFlag;
	if(allowResizeCropper=="350" && myUploader=="null")
	{
		$("#cropperSpace").hide();
	}
	else
	{
		$("#cropperSpace").show();
	}
	//centerPopup();	loadPopup(); 
	generateUploader(); //generateCropperObj();
}

function get_profile_pic(newresizeFlag)
{
	$("#community_event_header").html("Profile Picture");
	showCropper(newresizeFlag);
	updateCoords_manually();
	/*var str ="<?php echo $getdetail['image_name']; ?>";
	alert(str);
	if(str!="")
	{
		var as = str.split("/");
		var image_name = as[3].substr(10);
		var response = "https://comeventjcrop.s3.amazonaws.com/" + image_name;
	}
	else
	{
		response="";
	}
	if(response=="" || response==null)
	{*/
		var pro=document.getElementById("profile_image");
		if(pro.value!="")
		{
			response =pro.value;
			var img=response.split("/");
			var image_name = img[3].substr(10);
			var response = "https://comeventjcrop.s3.amazonaws.com/" + image_name;
			
			$("#cropperSpace").show();
			allowResizeCropper=false;
			generateCropperObj(response);
		}
		else
		{
			return;
		}
	/*}
	else
	{
		$("#cropperSpace").show();
		allowResizeCropper=false;
		alert(response);
		generateCropperObj(response);
	}*/
}
function get_listing_pic(newresizeFlag)
{
	$("#community_event_header").html("Listing Picture");
	showCropper(newresizeFlag);
	updateCoords_manually_thumb();
	/*var str ="<?php echo $getdetail['listing_image_name']; ?>";
	alert(str)
	if(str!="")
	{
		var as = str.split("/");
		var image_name = as[3].substr(10);
		var response = "https://comeventjcrop.s3.amazonaws.com/" + image_name;
		alert(response);
	}
	else
	{
		response="";
	}
	if(response=="" || response==null)
	{*/
		var list_pro= document.getElementById("listing_image");
		if(list_pro.value!="")
		{
			response =list_pro.value;
			var img=response.split("/");
			var image_name = img[3].substr(10);
			var response = "https://comeventjcrop.s3.amazonaws.com/" + image_name;
			
			$("#cropperSpace").show();
			allowResizeCropper="350";
			generateCropperObj(response);
			myUploader= "Notnull";
			return;
		}
		else
		{
			var list_preview= document.getElementById("preview");
			list_preview.src=" " ;
			myUploader="null";
			return;
		}
	/*}
	else
	{
		$("#cropperSpace").show();
		allowResizeCropper="350";
		generateCropperObj(response);
	}*/
}
$("document").ready(function(){
if(!allowResizeCropper)
{
	$("#resultImgProfile").hide();
	$("#resultImgThumb").hide();
}
});

function updateCoords_manually_thumb()
{
	$('#x').val(0);
	$('#y').val(0);
	$('#w').val(200);
	$('#h').val(200);
}
function updateCoords_manually()
{
	$('#x').val(0);
	$('#y').val(0);
	$('#w').val(450);
	$('#h').val(450);
}

/*Script For Detecting Flash*/
$("document").ready(function(){
	var flashEnabled = !!(navigator.mimeTypes["application/x-shockwave-flash"] || window.ActiveXObject && new ActiveXObject('ShockwaveFlash.ShockwaveFlash'));
	if (!flashEnabled) { 
		$("#Flashnotfound_img_comeve").css({"display":"block","color":"red","float":"left","width":"300px","position":"relative","left":"40px"});
	}
});
/*Script For Detecting Flash Ends Here*/
  </script>

<div class="fieldCont">
<div title="Select an image to crop and display on the events profile page." class="fieldTitle">*Profile Picture</div>
<?php
if(isset($_GET['id']) && $getdetail['image_name']!="")
{
?>
<img width="100" id="stored_profile_image" src="<?php if(isset($_GET['id'])) echo $getdetail['image_name']."?id=".time();?>"  />
<img id="resultImgProfile"  width="100"/>
<?php
}
else
{
?>
<img id="resultImgProfile" width="100"/>
<img id="Noimagediv" src="https://regprofilepic.s3.amazonaws.com/Noimage.png" width="100"/>
<?php
}
?>
<!--<a title="Select an image to crop and display on the events profile page." href="javascript:showCropper(false);" onclick="get_profile_pic()" class="hint" style="text-decoration:none;"><input type="button" value=" Change "/></a>-->
<a href="#popupContact_community_event" title="Select an image to crop and display on the events profile page." style="text-decoration:none;" class="fancybox_community_event" onClick="get_profile_pic(false)"><input class="blackhint" type="button" value=" Change "/></a>
</div>

<div class="fieldCont">
<div title="Select an image to crop and display on when the events profile is listed on other profile pages and in the search engine." class="fieldTitle">*Listing Picture</div>
<?php
if(isset($_GET['id']) && $getdetail['listing_image_name']!="")
{
?>
<img src="<?php if(isset($_GET['id'])) echo $getdetail['listing_image_name']."?id=".time();?>" id="stored_listing_image" width="100" />
<img id="resultImgThumb"  width="100"/>
<?php
}
else
{
?>
<img id="resultImgThumb"  width="100"/>
<img id="Noimagediv_listing" src="https://regprofilepic.s3.amazonaws.com/Noimage.png" width="100" />
<?php
}
?>
<!--<a title="Select an image to crop and display on when the events profile is listed on other profile pages and in the search engine." href="javascript:showCropper(350);" class="hint" onclick="get_listing_pic()" style="text-decoration:none;"><input type="button" value=" Change "/></a>-->
<a href="#popupContact_community_event" title="Select an image to crop and display on when the events profile is listed on other profile pages and in the search engine." style="text-decoration:none;" class="fancybox_community_event" onClick="get_listing_pic(350)"><input class="blackhint" type="button" value=" Change "/></a>
</div>


<!--<div style="margin:50px;">
	
	
</div>-->
<script type="text/javascript">
	$(".fancybox_community_event").fancybox({
		helpers : {
			media : {},
			buttons : {},
			title : null
		},
		afterShow: function(){
			var resize = setTimeout(function(){
			$.fancybox.update();
			},1000);
		}
	});
</script>
<!-- POPUP BOX START -->
<div id="popupContact_community_event" style="display:none">
	<div class="fancy_header" id="community_event_header">
	</div>
	<div class="pop_whole_image_content" >
		<div class="popup_image_preview_box">
			Preview
			<div class="popup_image_preview">
				<img src="" id="preview" alt="Preview" class="community_event_jcrop-preview" />
			</div>
		</div>
		<div id="Flashnotfound_img_comeve" style="display:none;">
			Either your flash player plugin is disabled or if you don't have the flash player <a href="http://get.adobe.com/flashplayer/" target="_blank">Click Here</a> to install.
		</div>
		<div class="pop_image_function_button">
			<form action="" methos="post" enctype="multipart/form-data" class="pop_image_form">
				<div class="pop_image_upload_button"><input name="theFile" type="file" id="file_upload" /></div>
				<div id="hinttext" class="pop_image_upload_hinttext">Image size should be more than 450x450.</div>
			</form>
			<div class="pop_image_crop_button">	 
				<input type="button" value="Crop Image" id="cropBtn" style=" background-color: #404040;border-radius: 5px;color: white;cursor: pointer;padding: 7px 24px;border:none;"/>
				<input type="hidden" id="x" name="x" />
				<input type="hidden" id="y" name="y" />
				<input type="hidden" id="w" name="w" />
				<input type="hidden" id="h" name="h" />
			</div>
		</div>
		<div style="clear:both"></div>
		<div id="errMsg" style="color:red;text-align:center;"></div>
		<table id="cropperSpace" style="display:none;  bottom: 100px;" class="big_image">
			<tr>
				<td rowspan="2" id="targetTD">
					<img src="" style="max-width:400px" id="target"/>				
				</td>
			</tr>
			<!--<tr>
				<td>Result
					<div style="width:100px;height:100px;overflow:hidden;">
						<img  id="result" alt="Result" class="community_event_jcrop-result" style="width:100px;height:100px;" />
					</div>
				</td>
			</tr>-->
		</table>
	</div>
</div>
<div id="backgroundPopup"></div>
<!-- POPUP BOX END -->