
    <!--<script src="jcrop/js/jquery.min.js" type="text/javascript"></script>-->
    <script src="jcrop/js/jquery.Jcrop.js" type="text/javascript"></script>
    <link rel="stylesheet" href="jcrop/css/jquery.Jcrop.css" type="text/css" />
    <link rel="stylesheet" href="jcrop/demo_files/demos.css" type="text/css" />

	<script src="jcrop/js/popup.js" type="text/javascript"></script>
    <link rel="stylesheet" href="jcrop/css/popup.css" type="text/css" />

	<script type="text/javascript" src="uploadify/swfobject.js"></script>
	<script type="text/javascript" src="uploadify/jquery.uploadify.v2.1.4.min.js"></script>
	<link href="uploadify/uploadify.css" rel="stylesheet"/>

	<script type="text/javascript">
	var myUploader = null;
	var iMaxUploadSize = 10485760; //10MB
	var IMG_UPLOAD_PATH = "jcrop/croppingFiles/";
	// Create variables (in this scope) to hold the API and image size
	var jcrop_api, boundx, boundy;
	var allowResizeCropper = false;
    jQuery(function($){
		$("#cropBtn").click(function(){
			if($('#x').val()!="" && $("#target").attr('src')!=""){
				$.ajax({
					type: "POST",
					url: 'jcrop/cropImage.php',
					data: { "action":'crop',"resize":allowResizeCropper,"x":$('#x').val(), "y":$('#y').val(), "w":$('#w').val(), "h":$('#h').val(), "srcImg":$("#target").attr('src')},
					success: function(data){
						alert($("#target").attr('src'));
						disablePopup();
						if(allowResizeCropper)
							$("#resultImgThumb").attr('src',data);
						else
							$("#resultImgProfile").attr('src',data);
					}
				});
			}
		});
    });


	function generateUploader(){
		if(!myUploader){
			myUploader = {
				uploadify : function(){
					$('#file_upload').uploadify({
						'uploader'  : 'uploadify/uploadify.swf',
						'script'    : 'jcrop/uploadFiles.php',
						'cancelImg' : 'uploadify/cancel.png',
						'folder'    : 'jcrop/croppingFiles/',
						'auto'      : true,
						'multi'		: true,
						'removeCompleted' : true,
						'wmode'		: 'transparent',
						'buttonText': 'Upload Image',
						'fileExt'     : '*.jpg;*.gif;*.png',
						'fileDesc'    : 'Image Files',
						'simUploadLimit' : 1,
						'sizeLimit'	: iMaxUploadSize, //10 MB size
						'onComplete': function(event, ID, fileObj, response, data) {
							// On File Upload Completion			
							//location = 'uploadtos3.php?uploads=complete';
							//$("#uploadedImages").val($("#uploadedImages").val() + response + "|");
							if(response=="invalid"){
								$("#errMsg").html("Invalid image size.");
							}else{
								$("#errMsg").html('');
								$("#cropperSpace").show();
								generateCropperObj(IMG_UPLOAD_PATH+response);
							}
						},
						'onSelect' : function (event, ID, fileObj){
						},
						'onSelectOnce' : function(event, data)
						{
							/*iTotFileSize = data.allBytesTotal;
							if(iTotFileSize >= iMaxUploadSize){
								var iOverSize = (iTotFileSize - iMaxUploadSize) / 1024 / 1024; // Size, in MB
								$("#divGalleryFileSize").show().html("Maximum 10MB allowed. Total queue size approximately: <font color='red'>" + Math.ceil(iTotFileSize/1024/1024) + " (MB)</font>");
								$('#file_upload').uploadifyClearQueue();
							}
							else
							{
							   $("#divGalleryFileSize").hide() 
							}*/
						},
						'onOpen'	: function() {
							//hide overly
						}
						/*,
						'onError'     : function (event,ID,fileObj,errorObj) {
						  alert(errorObj.type + ' Error: ' + errorObj.info);
						},
						'onProgress'  : function(event,ID,fileObj,data) {
						  var bytes = Math.round(data.bytesLoaded / 1024);
						  $('#containerResult').html(' - ' + bytes + 'KB Uploaded');
						  return false;
						}*/

						});
					}
				};
				myUploader.uploadify();
		}
	}

function generateCropperObj(img){
      if(jcrop_api){ 
		jcrop_api.destroy(); 
	  }
		$("#targetTD").html('<img id="target">');
		$("#target").attr('src',img);
		$("#preview").attr('src',img);

      $('#target').Jcrop({
        onChange: updatePreview,
        onSelect: updateCoords,
		allowResize: allowResizeCropper,
		allowSelect: allowResizeCropper,
        aspectRatio: 1
      },function(){
        // Use the API to get the real image size
        var bounds = this.getBounds();
        boundx = bounds[0];
        boundy = bounds[1];
        jcrop_api = this;
		if(!allowResizeCropper){
			jcrop_api.animateTo([0,0,450,450]);
		}
        // Store the API in the jcrop_api variable
      });

	function updateCoords(c)
	{
		$('#x').val(c.x);
		$('#y').val(c.y);
		$('#w').val(c.w);
		$('#h').val(c.h);
		updatePreview(c);
	};

      function updatePreview(c)
      {
        if (parseInt(c.w) > 0)
        {
          var rx = 100 / c.w;
          var ry = 100 / c.h;

          $('#preview').css({
            width: Math.round(rx * boundx) + 'px',
            height: Math.round(ry * boundy) + 'px',
            marginLeft: '-' + Math.round(rx * c.x) + 'px',
            marginTop: '-' + Math.round(ry * c.y) + 'px'
          });
        }
      };
}

function showCropper(resizeFlag){
	allowResizeCropper = resizeFlag;
	$("#cropperSpace").hide();
	centerPopup();	loadPopup(); generateUploader(); //generateCropperObj();
}
  </script>

<a href="javascript:showCropper(true);">Create Thumbnail</a><br/><br/>
<a href="javascript:showCropper(false);">Create Profile Image</a><br/><br/>
<div style="margin:50px;">
	<img id="resultImgThumb" style="vertical-align:top;"/>
	<img id="resultImgProfile"/>
</div>

<!-- POPUP BOX START -->
<div id="popupContact">
		<a id="popupContactClose">x</a>
		<p id="contactArea"><b>Image Cropper</b></p>
		<form action="" methos="post" enctype="multipart/form-data">
			<div style="width:auto;float:left;"><input name="theFile" type="file" id="file_upload" /></div>
			<div style="width:400px;float:left;color:grey;line-height:30px;">Image size should be more than 600x600.</div>
		</form>
		<div style="clear:both"></div>
        <div id="errMsg" style="color:red;text-align:center;"></div>
			<table id="cropperSpace" style="display:none;">
			  <tr>
				<td rowspan="2" id="targetTD">
				  <img src="croppingFiles/1340562846-7.jpg" style="max-width:400px" id="target"/>				
				</td>
				<td>Preview
					<div style="width:100px;height:100px;overflow:hidden;">
						<img src="croppingFiles/1340562846-7.jpg" id="preview" alt="Preview" class="jcrop-preview" />
					</div>
				</td>
			  </tr>
			  <tr>
			    <td>Result
					<div style="width:100px;height:100px;overflow:hidden;">
						<img  id="result" alt="Result" class="jcrop-result" style="width:100px;height:100px;" />
					</div>
				</td>
		      </tr>
			  <tr>
				<td colspan="2">
					<input type="button" value="Crop Image" id="cropBtn">
					<input type="hidden" id="x" name="x" />
					<input type="hidden" id="y" name="y" />
					<input type="hidden" id="w" name="w" />
					<input type="hidden" id="h" name="h" />
				</td>
			  </tr>
			</table>
		<br>
</div>
<div id="backgroundPopup"></div>
<!-- POPUP BOX END -->
