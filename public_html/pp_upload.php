<?php
	//Get the file information
	$userfile_name = $_FILES['image']['name'];
	$userfile_tmp = $_FILES['image']['tmp_name'];
	$userfile_size = $_FILES['image']['size'];
	$userfile_type = $_FILES['image']['type'];
	$filename = basename($_FILES['image']['name']);
	$file_ext = strtolower(substr($filename, strrpos($filename, '.') + 1));
	
	//Only process if the file is a JPG, PNG or GIF and below the allowed limit
	if((!empty($_FILES["image"])) && ($_FILES['image']['error'] == 0)) {
		
		foreach ($allowed_image_types as $mime_type => $ext) {
			//loop through the specified image types and if they match the extension then break out
			//everything is ok so go and check file size
			if($file_ext==$ext && $userfile_type==$mime_type){
				$msg5 = "";
				break;
			}else{
				$msg5 = "<div align='right' class='hintRed'>Only ".$image_ext." images accepted for upload.</div>";
			}
		}
		//check if the file size is above the allowed limit
		if ($userfile_size > ($max_file*1048576)) {
			$msg5.= "<div align='right' class='hintRed'>Images must be under ".$max_file."MB in size.</div>";
		}
		
	}else{
		if($parent!='Team')
			$msg5= "<div align='right' class='hintRed'>Select an image for upload.</div>";
		else
			$team_pp_upload_flag=1;
	}
	//Everything is ok, so we can upload the image.
	if (strlen($msg5)==0){
		
		if (isset($_FILES['image']['name'])){
			//this file could now has an unknown file extension (we hope it's one of the ones set above!)
			$large_image_location = $large_image_location.".".$file_ext;
			$thumb_image_location = $thumb_image_location.".".$file_ext;
			$thumb_image_location2 = $thumb_image_location2.".".$file_ext;
			$thumb_image_location3 = $thumb_image_location3.".".$file_ext;
			
			//put the file ext in the session so we know what file to look for once its uploaded
			$_SESSION['user_file_ext']=".".$file_ext;
			
			move_uploaded_file($userfile_tmp, $large_image_location);
			chmod($large_image_location, 0777);
			
			$width = getWidth($large_image_location);
			$height = getHeight($large_image_location);
			//Scale the image if it is greater than the width set above
			
			$w = 400;
			$h = 400;
			if($width>$height)
			{
				if ($width > $max_width){
					$scale = $max_width/$width;
					$uploaded = resizeImage($large_image_location,$width,$height,$scale);
				}else if($width < $min_width){
					$scale = $max_width/$width;
					$uploaded = resizeImage($large_image_location,$width,$height,$scale);
				}else{
					$scale = 1;
					$uploaded = resizeImage($large_image_location,$width,$height,$scale);
				}				
				
				//FOR CROPPING IMAGE
				
				$width=400;
				$height=ceil(($scale*$height));
								
				$x1 = 0;
				$y1 = 0;
				$x2 = 400;
				$y2 = $height;

				$scale = 400/$w;
				$cropped = resizeThumbnailImage($thumb_image_location, $large_image_location,$width,$height,$x1,$y1,$scale);
				$scale2 = 100/$w;
				$cropped = resizeThumbnailImage($thumb_image_location2, $large_image_location,$width,$height,$x1,$y1,$scale2);
				$scale3 = 50/$w;
				$cropped = resizeThumbnailImage($thumb_image_location3, $large_image_location,$width,$height,$x1,$y1,$scale3);
			}
			else 
			{
				if ($height > $max_height){
					$scale = $max_height/$height;
					$uploaded = resizeImage($large_image_location,$width,$height,$scale);
				}else if($height < $min_height){
					$scale = $max_height/$height;
					$uploaded = resizeImage($large_image_location,$width,$height,$scale);
				}else{
					$scale = 1;
					$uploaded = resizeImage($large_image_location,$width,$height,$scale);
				}

				//FOR CROPPING IMAGE
				
				$width=ceil(($scale*$width));
				$height=400;
				
				$x1 = 0;
				$y1 = 0;
				$x2 = $width;
				$y2 = 400;

				$scale = 400/$w;
				$cropped = resizeThumbnailImage($thumb_image_location, $large_image_location,$width,$height,$x1,$y1,$scale);
				$scale2 = 100/$w;
				$cropped = resizeThumbnailImage($thumb_image_location2, $large_image_location,$width,$height,$x1,$y1,$scale2);
				$scale3 = 50/$w;
				$cropped = resizeThumbnailImage($thumb_image_location3, $large_image_location,$width,$height,$x1,$y1,$scale3);
			}
		}
		unlink($large_image_location);
	}		
?>