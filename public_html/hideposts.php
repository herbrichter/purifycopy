<?php
include_once('commons/db.php');
include("newsmtp/PHPMailer/index.php");
session_start();
//echo $_SESSION['login_email'];
include_once('classes/Hide_Posts.php');
include_once('classes/PersonalWallDisplayFeeds.php');
include_once('sendgrid/SendGrid_loader.php');

$hide_post = new Hide_Posts();
$sql_feeds = new PersonalWallDisplayFeeds();
$sendgrid = new SendGrid('','');

$get_general_info = $hide_post->get_general_user();
//var_dump($get_general_info);
if(isset($_GET['id']))
{
	$hidethis_post = $hide_post->hide_post($get_general_info['general_user_id'],$_GET['id']);
}
elseif(isset($_GET['report_id']))
{
	$hidethis_post = $hide_post->hide_post($get_general_info['general_user_id'],$_GET['report_id']);
	
	$get_media_to_info = $hide_post->reported_spam($_GET['report_id']);
	if($get_media_to_info['profile_type']=='songs_post' || $get_media_to_info['profile_type']=='videos_post' || $get_media_to_info['profile_type']=='images_post')
	{
		$exp_meds = explode('~',$get_media_to_info['media']);
		
		if($exp_meds[1]=='reg_med')
		{
			$sql_meds = $sql_feeds->get_mediaSelected($exp_meds[0]);
		}
		elseif(($exp_meds[1]=='reg_art' || $exp_meds[1]=='reg_com') && $get_media_to_info['profile_type']=='images_post')
		{
			if($exp_meds[1]=='reg_art')
			{
				$reg_table_type = 'general_artist_gallery_list';
			}
			elseif($exp_meds[1]=='reg_com')
			{
				$reg_table_type = 'general_community_gallery_list';
			}
			
			$sql_meds = $sql_feeds->get_galleryData($reg_table_type,$exp_meds[0]);
		}
		elseif(($exp_meds[1]=='reg_art' || $exp_meds[1]=='reg_com') && $get_media_to_info=='videos_post')
		{
			if($exp_meds[1]=='reg_art')
			{
				$reg_table_type = 'general_artist_video';
			}
			elseif($exp_meds[1]=='reg_com')
			{
				$reg_table_type = 'general_community_video';
			}
							
			$sql_meds = $sql_feeds->get_MediaReg($reg_table_type,$exp_meds[0]);
		}
		elseif(($exp_meds[1]=='reg_art' || $exp_meds[1]=='reg_com') && $get_media_to_info=='songs_post')
		{
			if($exp_meds[1]=='reg_art')
			{
				$reg_table_type = 'general_artist_audio';
			}
			elseif($exp_meds[1]=='reg_com')
			{
				$reg_table_type = 'general_community_audio';
			}
							
			$sql_meds = $sql_feeds->get_MediaReg($reg_table_type,$exp_meds[0]);
		}
	}
	elseif($get_media_to_info['profile_type']=='projects_post' || $get_media_to_info['profile_type']=='events_post')
	{
		$exp_meds = explode('~',$get_media_to_info['media']);
		$sql_meds = $sql_feeds->featured_pro_eve($exp_meds[1],$exp_meds[0]);
	}
	
	if($exp_meds[1]=='reg_med' || $get_media_to_info['profile_type']=='projects_post' || $get_media_to_info['profile_type']=='events_post')
	{
		if($get_media_to_info['profile_type']=='projects_post' || $get_media_to_info['profile_type']=='events_post')
		{
			if($sql_meds['profile_url']!="")
			{
				$titles_mails = '<a href=purifyart.com/'.$sql_meds['profile_url'].'>'.$sql_meds['title'].'</a>';
			}
			else
			{
				$titles_mails = $sql_meds['title'];
			}
		}
		elseif($exp_meds[1]=='reg_med')
		{
			if($sql_meds['profile_url']!="")
			{
				$titles_mails = '<a href=purifyart.com/'.$sql_meds['profile_url'].'>'.$sql_meds['title'].'</a>';
			}
			else
			{
				$titles_mails = $sql_meds['title'];
			}
			/* if($sql_meds['creator']!="")
			{
				$ecehos .= ' By '.$sql_meds['creator'];
			}
			if($sql_meds['from']!="")
			{
				$ecehos .= ' From '.$sql_meds['from'];
			}
			echo $ecehos; */
		}
	}
	elseif($exp_meds[1]=='reg_art' || $exp_meds[1]=='reg_com')
	{
		if($get_media_to_info['profile_type']=='images_post')
		{
			$titles_mails = $sql_meds['gallery_title'];
		}
		elseif($get_media_to_info['profile_type']=='songs_post')
		{
			$titles_mails = $sql_meds['audio_name'];
		}
		elseif($get_media_to_info['profile_type']=='videos_post')
		{
			$titles_mails = $sql_meds['video_name'];
		}
	}
	
	$get_general_to_info = $hide_post->get_user_to($get_media_to_info['general_user_id']);
	
	$headers = "";
	$headers .= "MIME-Version: 1.0\r\n";
	$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
	//$def_to = '<bryan@purifyart.com>';
	//$def_to = '<mayur.purify@gmail.com>';
	//$def_to = "<mithesh@remotedataexchange.com>";
	//$def_to = "<info@purifyart.com>";
	$def_to = 'info@purifyart.com';
	//$mail->AddAddress($def_to);
	//$subject = 'Reported Spam';
	//$mail->Subject = 'Reported Spam';
	$message = 'Spam Reported By :- '. $_SESSION['name'].'<br/>';
	$message .= 'Spam Reported User :- '. $get_general_to_info['fname'].' '.$get_general_to_info['lname'].'<br/>';
	if(isset($titles_mails))
	{
		$message .= 'Spam Reported :- '. $titles_mails.'<br/>';
		$message .= $get_media_to_info['description'];
	}
	$headers .= "from: Purify Art < no-reply@purifyart.com>"."\r\n";
	
	//mail($def_to,$subject,$message,$headers);
	//$mail->Body    = $message;
	$url = "admin/newsletter/"; 
			$myFile = $url . "other_templates.txt";
			$fh = fopen($myFile, 'r');
			$theData = fread($fh, filesize($myFile));
			fclose($fh);
			
			$data_email = explode('|~|', $theData);
			
			$ns_user_n = str_replace('{::message::}', $message, $data_email['1']);
			
			$body_email = $data_email['0'] . $ns_user_n . $data_email['2'];
			
	$mail_grid = new SendGrid\Mail();

		$mail_grid->addTo($def_to)->
			   setFromName('Purify Art')->
				setFrom('no-reply@purifyart.com')->
			   setSubject('Reported Spam')->
			   setHtml($body_email);
		//$sendgrid->web->send($mail_grid);
		$sendgrid -> smtp -> send($mail_grid); 
		
	/* if($mail->Send()){
		$sentmail = true;
	} */
	
	//info@purifyart.com
}
header("Location: ".$_SERVER['HTTP_REFERER']);
?>
