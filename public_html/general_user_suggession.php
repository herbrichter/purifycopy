<?php
include_once('commons/db.php');
include('classes/AddArtistEvent.php');
session_start();

$newclassobj = new AddArtistEvent();
$q = strtolower($_GET["term"]);
if(isset($_GET["sam"]))
{
	$match = $_GET["sam"];
	$tk_email=explode('(',$match);
	for($i=0;$i<count($tk_email);$i++)
	{
		$en_pos=strpos($tk_email[$i],')');
		$email = substr($tk_email[$i],0,$en_pos);
	}
}

if (!$q) return;

	//$cou_gt=$newclassobj->CountNameSuggession($q);
	$rs=$newclassobj->suggest_general_user($q);
	//if($cou_gt[0]>0)
	//{
	//echo $rs;
	
		if(isset($_GET["sam"]))
		{
			$result = array();	
			if(mysql_num_rows($rs) == 0)
			{
				continue;
			}
			else
			{
				while ($row = mysql_fetch_array($rs)) 
				{
					if($row["email"]==$email)
					{
						continue;
					}
					if($row["email"]==$_SESSION['login_email'])
					{
						continue;
					}
					else
					{
						//echo $row["fname"]." ".$row["lname"]." (".$row["email"].")\n";
						array_push($result, array("id"=>$row["fname"]." ".$row["lname"]." (".$row["city"].")", "label"=>$row["fname"]." ".$row["lname"]." (".$row["city"].")", "value" => $row["fname"]." ".$row["lname"]." (".$row["city"].")", "value1" => $row["email"]));
					}
				}
				echo array_to_json($result);
			}
		}
		else
		{
			/*if(mysql_num_rows($rs) == 0)
			{
				continue;
			}
			else
			{*/
				$result = array();	
				while ($row = mysql_fetch_array($rs)) 
				{
						//echo $row["fname"]." ".$row["lname"]." (".$row["email"].")\n";
						array_push($result, array("id"=>$row["fname"]." ".$row["lname"]." (".$row["city"].")", "label"=>$row["fname"]." ".$row["lname"]." (".$row["city"].")", "value" => $row["fname"]." ".$row["lname"]." (".$row["city"].")", "value1" => $row["email"]));
				}
				echo array_to_json($result);
			//}
		}
	//}



	
function array_to_json( $array ){

    if( !is_array( $array ) ){
        return false;
    }

    $associative = count( array_diff( array_keys($array), array_keys( array_keys( $array )) ));
    if( $associative ){

        $construct = array();
        foreach( $array as $key => $value ){

            // We first copy each key/value pair into a staging array,
            // formatting each key and value properly as we go.

            // Format the key:
            if( is_numeric($key) ){
                $key = "key_$key";
            }
            $key = "\"".addslashes($key)."\"";

            // Format the value:
            if( is_array( $value )){
                $value = array_to_json( $value );
            } else if( !is_numeric( $value ) || is_string( $value ) ){
                $value = "\"".addslashes($value)."\"";
            }

            // Add to staging array:
            $construct[] = "$key: $value";
        }

        // Then we collapse the staging array into the JSON form:
        $result = "{ " . implode( ", ", $construct ) . " }";

    } else { // If the array is a vector (not associative):

        $construct = array();
        foreach( $array as $value ){

            // Format the value:
            if( is_array( $value )){
                $value = array_to_json( $value );
            } else if( !is_numeric( $value ) || is_string( $value ) ){
                $value = "'".addslashes($value)."'";
            }

            // Add to staging array:
            $construct[] = $value;
        }

        // Then we collapse the staging array into the JSON form:
        $result = "[ " . implode( ", ", $construct ) . " ]";
    }

    return $result;
}	
?>