
    <!--<script src="jcrop/js/jquery.min.js" type="text/javascript"></script>-->
    <script src="jcrop/js/jquery.Jcrop.js" type="text/javascript"></script>
    <link rel="stylesheet" href="jcrop/css/jquery.Jcrop.css" type="text/css" />
    <link rel="stylesheet" href="jcrop/demo_files/demos.css" type="text/css" />

	<script src="jcrop/js/popup.js" type="text/javascript"></script>
    <link rel="stylesheet" href="jcrop/css/popup.css" type="text/css" />

	<script type="text/javascript" src="uploadify/swfobject.js"></script>
	<script type="text/javascript" src="uploadify/jquery.uploadify.v2.1.4.min.js"></script>
	<link href="uploadify/uploadify.css" rel="stylesheet"/>

	<?php
		if(isset($_SESSION['login_email']))
		{
			$sql = mysql_query("select * from  general_user where email='".$_SESSION['login_email']."'");
			$res=mysql_fetch_assoc($sql);
			//var_dump($res);

		}
	?>
	<script type="text/javascript">
	var myUploader = null;
	var iMaxUploadSize = 10485760; //10MB
	var IMG_UPLOAD_PATH = "https://teamjcrop.s3.amazonaws.com/";
	// Create variables (in this scope) to hold the API and image size
	var jcrop_api, boundx, boundy;
	var allowResizeCropper = false;
    jQuery(function($){
		$("#cropBtn").click(function(){
			if($('#x').val()!="" && $("#target").attr('src')!=""){
			var name_split = $("#target").attr('src').split('/');
			///alert(name_split[1]);
			//alert(name_split[2]);
			if(name_split[0]=='uploads')
			{
				var slash = '../' + $("#target").attr('src');
				
			}
			else
			{
				var slash = $("#target").attr('src');
				//alert(slash);
			}
				$.ajax({
					type: "POST",
					url: 'teamjcrop/cropImage.php',
					data: { "action":'crop',"resize":allowResizeCropper,"x":$('#x').val(), "y":$('#y').val(), "w":$('#w').val(), "h":$('#h').val(), "srcImg":slash},
					success: function(data){
						//alert($("#target").attr('src'));
						disablePopup();
						//window.location.href="profileedit_artist.php";
						window.location.reload();
						if(allowResizeCropper)
							$("#resultImgThumb").attr('src',data);
						else
							$("#resultImgProfile").attr('src',data);
							var diff=data.split("/");
							//alert(diff['4']);
							//window.parent.document.getElementById("pro_pic").value = diff['4'];
							//alert(a);
							//a = diff['4'];
							//alert(a);
					}
				});
			}
		});
    });


	function generateUploader(){
		if(!myUploader){
			myUploader = {
				uploadify : function(){
					$('#file_upload').uploadify({
						'uploader'  : 'uploadify/uploadify.swf',
						'script'    : 'teamjcrop/uploadFiles.php?id=<?php echo $res['team_id']; ?>',
						'cancelImg' : 'uploadify/cancel.png',
						'folder'    : 'teamjcrop/croppingFiles/',
						'auto'      : true,
						'multi'		: true,
						'removeCompleted' : true,
						'wmode'		: 'transparent',
						'buttonText': 'Upload Image',
						'fileExt'     : '*.jpg;*.gif;*.png',
						'fileDesc'    : 'Image Files',
						'simUploadLimit' : 1,
						'sizeLimit'	: iMaxUploadSize, //10 MB size
						'onComplete': function(event, ID, fileObj, response, data) {
							// On File Upload Completion			
							//location = 'uploadtos3.php?uploads=complete';
							//$("#uploadedImages").val($("#uploadedImages").val() + response + "|");
							if(response=="invalid")
							{
								$("#errMsg").html("Invalid image size.");
							}
							else
							{
								$("#errMsg").html('');
								$("#cropperSpace").show();
								//alert(IMG_UPLOAD_PATH);
								//alert(response + "Artist");
								generateCropperObj(response);
							}
						},
						'onSelect' : function (event, ID, fileObj){
						},
						'onSelectOnce' : function(event, data)
						{
							/*iTotFileSize = data.allBytesTotal;
							if(iTotFileSize >= iMaxUploadSize){
								var iOverSize = (iTotFileSize - iMaxUploadSize) / 1024 / 1024; // Size, in MB
								$("#divGalleryFileSize").show().html("Maximum 10MB allowed. Total queue size approximately: <font color='red'>" + Math.ceil(iTotFileSize/1024/1024) + " (MB)</font>");
								$('#file_upload').uploadifyClearQueue();
							}
							else
							{
							   $("#divGalleryFileSize").hide() 
							}*/
						},
						'onOpen'	: function() {
							//hide overly
						}
						/*,
						'onError'     : function (event,ID,fileObj,errorObj) {
						  alert(errorObj.type + ' Error: ' + errorObj.info);
						},
						'onProgress'  : function(event,ID,fileObj,data) {
						  var bytes = Math.round(data.bytesLoaded / 1024);
						  $('#containerResult').html(' - ' + bytes + 'KB Uploaded');
						  return false;
						}*/

						});
					}
				};
				myUploader.uploadify();
		}
	}

function generateCropperObj(img){
      if(jcrop_api){ 
		jcrop_api.destroy(); 
	  }
		$("#targetTD").html('<img id="target">');
		$("#target").attr('src',img);
		$("#preview").attr('src',img);

      $('#target').Jcrop({
        onChange: updatePreview,
        onSelect: updateCoords,
		allowResize: allowResizeCropper,
		allowSelect: allowResizeCropper,
        aspectRatio: 1
      },function(){
        // Use the API to get the real image size
        var bounds = this.getBounds();
        boundx = bounds[0];
        boundy = bounds[1];
        jcrop_api = this;
		if(!allowResizeCropper){
		$("#hinttext").text("Image size should be more than 450x450.");
		//alert(allowResizeCropper);
			jcrop_api.setOptions({ allowSelect: true });
			jcrop_api.setOptions({ allowResize: true });
		}
		else
		{
			$("#hinttext").text("Image size should be more than 450x450.");
		}
        // Store the API in the jcrop_api variable
      });

	function updateCoords(c)
	{
		$('#x').val(c.x);
		$('#y').val(c.y);
		$('#w').val(c.w);
		$('#h').val(c.h);
		updatePreview(c);
	};

      function updatePreview(c)
      {
        if (parseInt(c.w) > 0)
        {
          var rx = 100 / c.w;
          var ry = 100 / c.h;

          $('#preview').css({
            width: Math.round(rx * boundx) + 'px',
            height: Math.round(ry * boundy) + 'px',
            marginLeft: '-' + Math.round(rx * c.x) + 'px',
            marginTop: '-' + Math.round(ry * c.y) + 'px'
          });
        }
      };
}

function showCropper(resizeFlag){
	allowResizeCropper = resizeFlag;
	/*this line is commented as per our requirement*/
		
		//$("#cropperSpace").hide();
		
	/*this line is commented as per our requirement*/
	centerPopup();	loadPopup(); generateUploader(); //generateCropperObj();
}

function create()
{
	
	//alert("<?php echo $res1['image_name']; ?>");
	var str ="<?php echo $res1['image_name']; ?>";
	var image_name = str.substr(10);
	var response = image_name;
	//alert(response);
	if(response=="" || response==null)
	{
		return;
	}
	else
	{
		$("#cropperSpace").show();
		allowResizeCropper=false;
		//community_jcrop_api.destroy(); 
		if(jcrop_api)
		{ 
			jcrop_api.destroy(); 
		}
		<?php 
			$name_image = $res1['image_name'];
			$new_image1 = strpos($name_image,'bnail');
			if($new_image1==0)
			{
		?>
				var response = "<?php echo $res1['image_name']; ?>";
				var IMG_UPLOAD_PATH = "https://regteampro.s3.amazonaws.com/";
		<?php
				$source = "https://regteampro.s3.amazonaws.com/".$res1['image_name'];
				$dest = "teamjcrop/croppingFiles/".$res1['image_name'];
				if(copy($source,$dest))
				{
		?>			
					//alert("in if");
		<?php
				}
				$path = $_SERVER['DOCUMENT_ROOT']."/teamjcrop/croppingFiles/".$res1['image_name'];
                $root = realpath($_SERVER["DOCUMENT_ROOT"]);
                if (!class_exists('S3')) require_once ($root.'/S3.php');
		        if (!defined('awsAccessKey')) define('awsAccessKey', '');
		        if (!defined('awsSecretKey')) define('awsSecretKey', '');
				$s3 = new S3(awsAccessKey, awsSecretKey);
				$bucket_name = "teamjcrop";
				$s3->putObject(S3::inputFile($path),$bucket_name,$res1['image_name'], S3::ACL_PUBLIC_READ);
				unlink($path);
			}
			else
			{
		?>
				var check_split = response.split('_');
				if(check_split[1]=='thumbnail')
				{
					 var i;
					 var response = "";
					 for(i=2;i<check_split.length;i++)
					 {
						if(i!=2)
						{
							var response = response +'_'+ check_split[i] ;
						}
						else
						{
							var response = response + check_split[i] ;
						}
					}
					var IMG_UPLOAD_PATH = "https://regteampro.s3.amazonaws.com/";
				}
				else
				{
					var IMG_UPLOAD_PATH = "https://teamjcrop.s3.amazonaws.com/";
				}
		<?php
			}
		?>
		generateCropperObj(IMG_UPLOAD_PATH+response);
	}
}
  </script>


<!--<a href="javascript:showCropper(true);">Create Thumbnail</a><br/><br/>-->
<a href="javascript:showCropper(false);" style="text-decoration:none;" class="hint" onClick="create()"><input type="button" value=" Change "/></a>
<!--<div style="margin:50px;">
	<img id="resultImgThumb" style="vertical-align:top;"/>
	<img id="resultImgProfile"/>
</div>-->

<!-- POPUP BOX START -->
<div id="popupContact">
		<a id="popupContactClose">x</a>
        
		<div style=" float: left; width: 100px;right:100px;">
			Preview
			<div style="width:100px;height:100px;overflow:hidden;">
				<img src="" id="preview" alt="Preview" class="jcrop-preview" />
			</div>
		</div>
		<form action="" methos="post" enctype="multipart/form-data" style="width:75%; float:left; margin:19px 0 0 20px;">
			<div style="width:120px;float:left;"><input name="theFile" type="file" id="file_upload" /></div>
			<div id="hinttext" style="width:400px;float:left;color:grey;line-height:30px; position:relative; left:20px;">Image size should be more than 450x450.</div>
		</form>
        <div style="width:75%; float:left; margin-left:18px;">
			<input type="button" value="Crop Image" id="cropBtn" style=" background-color: #404040;border-radius: 3px 3px 3px 3px;color: white;cursor: pointer;padding: 7px 27px;border:none;margin-top:15px;"/>
			<input type="hidden" id="x" name="x" />
			<input type="hidden" id="y" name="y" />
			<input type="hidden" id="w" name="w" />
			<input type="hidden" id="h" name="h" />
		</div>
		<div style="clear:both"></div>
        <div id="errMsg" style="color:red;text-align:center;"></div>
			
			
		
		<div style="float:left; clear:both;">	
			
			<table id="cropperSpace" style="display:none;  bottom: 100px;">
			<tr>
			<td rowspan="2" id="targetTD">
				  <img src="" style="max-width:400px" id="target"/>				
				</td>
				
			  </tr>
			  <!--<tr>
			    <td>Result
					<div style="width:100px;height:100px;overflow:hidden;">
						<img  id="result" alt="Result" class="jcrop-result" style="width:100px;height:100px;" />
					</div>
				</td>
		      </tr>-->
			  
			</table>
        </div>
		<br>
</div>
<div id="backgroundPopup"></div>
<!-- POPUP BOX END -->