<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "//www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="//www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<title>Purify Art: Agreements: Intellectual Propoerty</title>
<?php include("includes/header.php");?>
<div id="outerContainer"></div>
  <div id="contentContainer" >    
    <div id="actualContent" class="sidepages">
      <h1>Intellectual Property Policy</h1>
      <p>Last Updated  May 17, 2012. </p>
      <h5>Upload Notice</h5>
      <p>Purify Entertainment  respects the intellectual property of others, and we ask our users to do the  same. Each user is responsible for ensuring that the Materials they upload to  the Purify Entertainment Site do not infringe any third party copyright.</p>
      <p>Purify Entertainment will  promptly remove Materials from the Purify Entertainment Site in accordance with  the Digital Millennium Copyright Act (&quot;DMCA&quot;) if properly notified  that the materials infringe a third party's copyright. In addition, Purify Entertainment  reserves the right to, in appropriate circumstances, terminate the accounts of  repeat (??) copyright infringers.</p>
      <p>DO NOT  UPLOAD MUSIC UNLESS YOU OWN THE MUSIC OR HAVE WRITTEN PERMISSION FROM THE  WRITERS, PRODUCERS AND PERFORMERS OF THE MUSIC (&ldquo;ARTISTS&rdquo;).  By uploading music to the Site, the user  warrants and represents that he/she/it has  obtained and hold all rights, approvals, consents, licenses and/or permissions,  in proper legal form, necessary to submit Materials on the terms provided herein;  that no other rights are required; that the Materials are original or that  rights have been obtained from the owners of the non-original Material.</p>
      <p>User should  clear the rights in all Material to ensure that it owns or control the  distribution rights from all creative contributors to the Material, (e.g., the  writer(s) of the lyrics, the writer(s) of the score, the singers and musicians  whose performance are embodied on the recording, and/or the producer).   </p>
      <p>Music is protected by copyright. The unauthorized  downloading or uploading of music is actionable as copyright infringement, even  if not done for monetary or financial or commercial gain. Copyright  infringement is against the law and may subject you to civil and criminal  liability. Uploading music you did not create or do not have the right to  redistribute from the copyright owner is against the law and is a violation of  Purify Entertainment&rsquo;s Terms of Use Agreement. </p>
      <p>Owning a  copy of music (you bought a CD or downloaded it from an Internet service), does  not make you the copyright owner or give you permission to upload music to a  Purify Entertainment Profile. If you are not the Artist(s) who created the  media or do not have written permission from the creators of the copyrighted  content/media, do not upload the file. If you violate this rule, your Profile  will be suspended and/or deleted. If you attempt to upload copyright protected  music and are not cleared for uploading, you will be blocked from further use  of your account until …... If you distribute your music commercially, your  record label and/or publisher may have already registered your music to prevent  copyright infringement. If you are blocked during an upload, contact your  record label representative to be cleared to upload your music to your Purify  Entertainment Profile.</p>
    </div>
    <div id="snipeSidebar">
    	<div class="snipe">
      	<h2>Questions?</h2>
        <p>If you have general questions about our services, check out our <a href="help_faq.php">FAQ ></a></p>
      </div>      
    </div>
    
    <div class="clearMe"></div>
    
  </div>
<?php include("footer.php");?>
</div>
</body>
</html>