<head>
	<title>Purify Entertainment</title>
	<script type="text/javascript" src="js/jquery-1.3.2.min.js"></script>
	<script type='text/javascript' src="js/flowplayer-3.1.4.min.js"></script>
	<script type="text/javascript" src="js/playlist-video.js"></script>
	<link rel="stylesheet" href="js/style.css" type="text/css" />
	<link rel="stylesheet" href="js/style-video.css" type="text/css" />
</head>

<div id="wrapper">

	<div id="playerwrapper">
		<div id="player"></div>
		
		
	</div>
    <div id="playlistbg">
    <div id="controls">
			<a onClick="player_previous()"><img src="js/previous.gif" width="20" height="20"/></a>
			<a onClick="player_repeat()" id="repeat"><img src="js/repeat.gif" width="20" height="20"/></a>
<!--			<a onClick="player_shuffle()" id="shuffle"><img src="js/shuffle.gif" width="20" height="20"/></a>-->
                        <a onClick="player_shuffle()" id="shuffle"><img src="js/shuffle.gif" width="20" height="20"/></a>
			<a onClick="player_next()"><img src="js/next.gif" width="20" height="20"/></a>

		</div>
		
		<div id="controls2">
<!--                    <a onClick="playlist_randomize()">All</a>-->
                    <a onClick="Loadaudio()">All Audio</a> |
                    <a  onClick="Loadvideo()">All Video</a> |
                    <a onClick="playlist_delete()">Clear</a>
			
		</div>
        <div style="clear:both"></div>

	<div id="playlist"></div>
    </div>
	

</div>
<div style="clear:both"></div>

<script type="text/javascript">

      var is_chrome = navigator.userAgent.toLowerCase().indexOf('chrome') > -1;

    $('#wrapper').append('<div style="display:none" id="__curhash">'+window.location+'</div>')

                function Loadaudio()
                {
                    playlist_delete();
                    $.ajax({
                        url: 'playlist.php?allmp3=yes&userid='+getUid(),
                        success: function(data) {
                            load_existing_playlist(getUid());
                        }
                    })
                }
                function Loadvideo()
                {
                    playlist_delete();
                    $.ajax({
                        url: 'playlist.php?allvideo=yes&userid='+getUid(),
                        success: function(data) {
                            load_existing_playlist(getUid());
                        }
                    })
                }
                function playlist_delete()
                {
                    playlist_clear(getUid());
                }

                function getUid()
                {
                    var url = window.location.toString();
                    
                    var params = url.split("&");
                    
                    var split = params[1].split("=");
                    
                    var uid = split[1];
                    
                    return uid;
                }
                function updatePlaylist(hash)
                {

                    //get currhash
                    currhash = $("#__curhash").html();
                    currhash = currhash.replace(/&amp;/g,'&');

                    if(currhash != window.location)
                    {
                        if(window.location.hash == "#DeleteCall")
                            {
                                window.setTimeout(function() {updatePlaylist(location.hash)},1000);
                            }
                            else{
                        uid = getUid();
                        $.ajax({
                            url: 'playlist.php?ajaxadd=yes&userid='+uid+'&'+hash.replace('#',''),
                            success: function(data) {
                                    var song_index= window.location.toString().substr(window.location.toString().indexOf("#id=") + 4, (window.location.toString().indexOf("&uid") - window.location.toString().indexOf("#id=") - 4));
                                    var addplay = window.location.toString().substr(window.location.toString().indexOf("addplay") + 8 );
                                    var p = window.location.toString().substr(window.location.toString().indexOf("&path=") + 6 ,( (window.location.toString().indexOf("&profileurl") - 6 )- (window.location.toString().indexOf("&path="))) );
                                    while( p.indexOf('%20' )+1) p=p.replace(/%20/,' ');
                                    var currentSong = p;
                                    var playIndex = false;
                                    var playSong = null;
                                    function playAddSong()
                                    {
                                            for(var i=0; i<playlist.length; i++)
                                            {
                                                if(playlist[i]["url"]== currentSong)
                                                    {
                                                       playIndex = true;
                                                       playSong = i;
                                                    }
//                                                alert(playlist[i]["url"] +" == "+currentSong);
                                            }
                                            //alert(playIndex);
                                            if(!playIndex)
                                            {
                                                if (addplay == "add")
                                                        {
                                                            add_clip(song_index);
//                                                            alert('$f().isPlaying()')
//                                                    alert("PlayIndex False Else Play");
                                                            if(!$f().isPlaying())
                                                            {
                                                                $f().stop();
                                                            }
                                                        }
                                                if (addplay == "play")
                                                {
//                                                    alert("PlayIndex False Else Play");
                                                    var index = add_clip(song_index);
                                                    if(is_chrome)
                                                    {
                                                    window.setTimeout(function() {play_clip(index-1)},1000);
                                                    }
                                                    else
                                                    {
                                                    window.setTimeout(function() {play_clip(index-1)},1000);
                                                    }
                                                }
                                            }
                                                else
                                                {
//                                                    alert(playSong);
                                                    if (addplay == "play")
                                                    {
//                                                        alert("PlayIndex true Else Play");
                                                        if(is_chrome)
                                                        {
                                                        window.setTimeout(function() {play_clip(playSong)},1000);
                                                        }
                                                        else
                                                        {
                                                        window.setTimeout(function() {play_clip(playSong)},1000);
                                                        }
                                                    }
                                                    else if(addplay == "add")
                                                        {
//                                                            alert("PlayIndex true Else Add");
                                                            add_clip(song_index);
                                                            if(!$f().isPlaying())
                                                            {
                                                                $f().stop();
                                                            }
                                                        }
                                                }
                                        }
                                        window.setTimeout(function() {playAddSong()},1000);
                            }
                        })

                        }
                        //set currhash
                        $("#__curhash").remove();
                        $('#wrapper').append('<div style="display:none" id="__curhash">'+window.location+'</div>')
                    }
                    window.setTimeout(function() {updatePlaylist(location.hash)},1000);
                }
                
                window.setTimeout(function() {updatePlaylist(location.hash)},1000);
                window.setTimeout(function(){
                    uid = getUid();
                    $.ajax({
                        url: 'playlist.php?ajaxadd=yes&userid='+uid+'&'+location.hash.replace('#',''),
                        success: function(data) {
                            load_existing_playlist(uid);
                            var song_index= window.location.toString().substr(window.location.toString().indexOf("#id=") + 4, (window.location.toString().indexOf("&uid") - window.location.toString().indexOf("#id=") - 4));
                            var addplay = window.location.toString().substr(window.location.toString().indexOf("addplay") + 8 );

                            if (window.ActiveXObject)
                            var xmlDoc = new ActiveXObject("Microsoft.XMLDOM");
                            else if (document.implementation && document.implementation.createDocument)
                            var xmlDoc= document.implementation.createDocument("","doc",null);

                            xmlDoc.load("playlist"+uid+".xml");
                            var msgobj=xmlDoc.getElementsByTagName("song");
                            function playExistingSong()
                            {

                                var count = 0;
                                var play = false;
                                var songIndex = null;
                                for (count = 0; count<msgobj.length; count++)
                                {
                                     if(parseInt($(msgobj[count]).attr("id")) == parseInt(song_index))
                                         {
                                             play = true;
                                             songIndex = count;
                                         }
                                }
    //                            alert("SongIndex = "+songIndex);
                                 if(play)
                                     {
//                                        window.setTimeout(function() {play_clip(songIndex)},1500);
    //                                    alert("Inside Pageload Play");

                                                if (addplay == "add")
                                                {
                                                    $f().stop();
                                                }
                                                if (addplay == "play")
                                                {
                                                    if(is_chrome)
                                                    {
                                                    window.setTimeout(function() {play_clip(songIndex)},1500);
                                                    }
                                                    else
                                                    {
                                                    window.setTimeout(function() {play_clip(songIndex)},1500);
                                                    }
                                                }
                                     }
                                 else
                                    {
                                       // alert("Inside Pageload Else");
                                        window.setTimeout(function() {play_clip(playlist.length-1)},1500);
                                    }
                            }
                            window.setTimeout(function() {playExistingSong()},1000);

                        }
                    })}, 700);
                var height = 730;
                var width = 580;
                if(is_chrome)
                {
                    height:700;
                    width:580;
                }
                if (window.ActiveXObject) {
                    height = 760;
                    width = 580;
                }
                window.resizeTo(width, height);
                $(window).resize(function(){window.resizeTo(width, height);});
                window.moveTo((screen.width-580)/2,0);
                window.focus();
</script>
