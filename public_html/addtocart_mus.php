<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<?php
session_start();
include('commons/db.php');
include('classes/AddToCart.php');
$add_to_cart_obj = new AddToCart();

/*if(isset($_SESSION['guest_email']))
{
	if($_SESSION['guest_email']!=NULL)
	{
		//session_destroy();
		unset($_SESSION['guest_email']);
		unset($_SESSION['guest_name']);
		unset($_SESSION['guest_lname']);
	}
}*/
if(isset($_GET['buyer_id'])){
	$buyer_id = $_GET['buyer_id'];
}else{
	$buyer_id ="";
}
if(isset($_GET['image_name'])){ $my_img_name = $_GET['image_name']; }else { $my_img_name = ""; }

if(isset($_GET['from_url'])){
	$back_to_url = $_GET['from_url'];
}else{
	if(isset($_SERVER['HTTP_REFERER']) && $_SERVER['HTTP_REFERER']!="")
	{
		$back_to_url = $_SERVER['HTTP_REFERER'];
	}
	else
	{
		$back_to_url = "";
	}
}

if(isset($_GET['ShowCartVersion']))
{
    $ShowCartVersion=$_GET['ShowCartVersion'];
} else 
{
    $ShowCartVersion='1';
}

if(isset($_GET['viewbuyer_id']))
{
	$get_all_data = $add_to_cart_obj->Get_all_cart_data($_GET['viewbuyer_id']);
}
else
{	
	if(isset($_GET['remove']) && !isset($_GET['remove_type']))
	{
		$delete_data = $add_to_cart_obj->Delete_Data($_GET['remove']);
	}
	else if(isset($_GET['removeall']) && $_GET['removeall']==1)
	{
		if($_GET['notlogin']=="yes"){
			$delete_data = $add_to_cart_obj->removeall_cart_data_notlogin($buyer_id);
		}else{
			$delete_data = $add_to_cart_obj->removeall_cart_data_login($buyer_id);
		}
	}
	else if(isset($_GET['remove']) && isset($_GET['remove_type']))
	{
		$delete_data = $add_to_cart_obj->Delete_Fan_Data($_GET['remove']);
	}
	else if(isset($_GET['play']) && isset($_GET['media_id']))
	{
		$add_play = $add_to_cart_obj->add_to_cart_play($_GET['media_id'],$buyer_id);
	}
	else if(isset($_GET['type']))
	{
		$add_gallery = $add_to_cart_obj->gallery_add_to_cart($_GET['seller_id'],$buyer_id,$_GET['media_id'],$my_img_name,$_GET['title']);
	}
	else if(isset($_GET['update_id']) && isset($_GET['update_val']))
	{
		$update_cart = $add_to_cart_obj->update_cart($_GET['update_id'],$_GET['update_val'],$_GET['table_name']);
	}
	else
	{
		if(!isset($_GET['donate_purify']))
		{
			$_GET['donate_purify'] = "";
		}
		
		if(($buyer_id ==" " || $buyer_id == null) && ($_GET['donate_purify'] =="" && !isset($_GET['display_cart'])) && (!isset($_GET['remove_donate_purify']) || $_GET['remove_donate_purify']=="")){
			$add = $add_to_cart_obj->add_to_cart_not_login($_GET['seller_id'],$buyer_id,$_GET['media_id'],$my_img_name,$_GET['title'],0,0,0,$_SERVER["REMOTE_ADDR"]);
			
			if(isset($_GET['don_tips_al']) && isset($_GET['media_id']) && isset($_GET['seller_id']))
			{
				if($_GET['don_tips_al']!=0 && $_GET['media_id']!='' && $_GET['seller_id']!="")
				{
					$update_cart = $add_to_cart_obj->update_tip_from_own_not($_GET['don_tips_al'],$_GET['media_id'],$_GET['seller_id'],$_SERVER["REMOTE_ADDR"]);
					
					$add = $add_to_cart_obj->add_to_cart_not_login($_GET['seller_id'],$buyer_id,$_GET['media_id'],$my_img_name,$_GET['title'],0,0,0,$_SERVER["REMOTE_ADDR"]);
				}
			}
			
		}else{
			if((!isset($_GET['display_cart']) || $_GET['display_cart']=="") && (!isset($_GET['donate_purify']) || $_GET['donate_purify']=="") && (!isset($_GET['remove_donate_purify']) || $_GET['remove_donate_purify']=="")){
				$add = $add_to_cart_obj->add_to_cart($_GET['seller_id'],$buyer_id,$_GET['media_id'],$my_img_name,$_GET['title'],0,0,0);
				
				if(isset($_GET['don_tips_al']) && isset($_GET['media_id']) && isset($_GET['seller_id']) && isset($buyer_id))
				{
					if($_GET['don_tips_al']!=0 && $_GET['media_id']!='' && $_GET['seller_id']!="" && $buyer_id!="")
					{
						$update_cart = $add_to_cart_obj->update_tip_from_own($_GET['don_tips_al'],$_GET['media_id'],$_GET['seller_id'],$buyer_id);
						
						$add = $add_to_cart_obj->add_to_cart($_GET['seller_id'],$buyer_id,$_GET['media_id'],$my_img_name,$_GET['title'],0,0,0);
					}
				}
			}
		}
	}
	
	if(isset($_GET['donate_purify']) && $_GET['donate_purify'] !="")
	{
		$donate_purify = $add_to_cart_obj->donate_to_purify($_GET['amount']);
	}
	
	if(isset($_GET['remove_donate_purify']) && $_GET['remove_donate_purify'] !="")
	{
		$removedonate_purify = $add_to_cart_obj->remove_donate_to_purify();
	}
	
	if($buyer_id ==" " || $buyer_id == null || !isset($_SESSION['login_email']))//$_SESSION['login_email']==null
	{
		$get_all_data_fan = $add_to_cart_obj->get_fan_detail_notlogin();
		$get_all_data = $add_to_cart_obj->Get_all_cart_data_not_login($_SERVER["REMOTE_ADDR"]);
	}else{
		$get_all_data_fan = $add_to_cart_obj->get_Cart($buyer_id);
		$get_all_data = $add_to_cart_obj->Get_all_cart_data($buyer_id);
	}
	
	$purify_donate = $add_to_cart_obj->get_purify_donation();
}
?>
<html xmlns="http://www.w3.org/1999/xhtml" style="height: 100%;">
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<head>
<link href="includes/purify.css" rel="stylesheet" />
<link href="sample.css" rel="stylesheet" type="text/css" />

</head>

	
    <?php
    if ($ShowCartVersion=="2")
    {?>
    <body style="background-color: #FFFFFF;">
    <div style="width:100%; float:left;">
		<div class="fancy_header2" style="color: #000000; font-size: 20px; height: 24px; margin-bottom: 20px;">
			Shopping Cart
		</div>
        
	<?php } else { ?>
    <body >
    <div style="width:100%; float:left;">
		<div class="fancy_header">
			Shopping Cart
		</div>
        
    <?php } ?>
       
        <div id="cartWrapper" class="mus-cartWrapperwhole" style="width: 587px;" >
        
			<div id="tempd" >
				<div id="playlistView">
					<div id="mus-totalCont" style="border-bottom:none; border-top:none;">
						<div class="subtotal" style="">
<?php 
						if(!isset($_SESSION['login_email']) && !isset($_SESSION['guest_name']))
						{
							//if($_SESSION['login_email']=="" || $_SESSION['login_email']==null)
							//{
	?>
								<div class="subtotal">
									<div class="title">First Name:</div>
									<!--<div class="cart_doll">&nbsp;</div>-->
									<div class="amount" style="width:150px;">
									<!--<input type="text" class="field" id="user_name_notlog" value="//echo $_SESSION['guest_name']; " style="width:100px; height:12px; font-size:11px;"/>-->
									<input type="text" class="field" id="user_name_notlog" value="" style="width:100px; height:12px; font-size:11px;"/>
									</div>
								</div>
								<div class="subtotal">
									<div class="title">Last Name:</div>
									<!--<div class="cart_doll">&nbsp;</div>-->
									<div class="amount" style="width:150px;">
									<!--<input type="text" class="field" id="user_lname_notlog" value="// echo $_SESSION['guest_lname'];" style="width:100px; height:12px; font-size:11px;"/>-->
									<input type="text" class="field" id="user_lname_notlog" value="" style="width:100px; height:12px; font-size:11px;"/>
									</div>
								</div>
								<div class="subtotal" style="margin-bottom:0px;">
									<div class="title">Email :</div>
									<!--<div class="cart_doll">&nbsp;</div>-->
									<div class="amount" style="width:150px;">
									<!--<input type="text" class="field" id="email_notlog" value=" //echo $_SESSION['guest_email'];" style="width:100px; height:12px; font-size:11px;"/>-->
									<input type="text" class="field" id="email_notlog" value="" style="width:100px; height:12px; font-size:11px;"/>
									</div>
								</div>
	<?php
							//}
						}
						else if(isset($_SESSION['guest_name']) && !empty($_SESSION['guest_name']) && !isset($_SESSION['login_email']))
						{
	?>
							<div class="subtotal">
								<div class="title">First Name:</div>
								<!--<div class="cart_doll">&nbsp;</div>-->
								<div class="amount" style="width:150px;">
								<!--<input type="text" class="field" id="user_name_notlog" value="//echo $_SESSION['guest_name']; " style="width:100px; height:12px; font-size:11px;"/>-->
								<input type="text" class="field" id="user_name_notlog" value="<?php echo $_SESSION['guest_name']; ?>" style="width:100px; height:12px; font-size:11px;"/>
								</div>
							</div>
							<div class="subtotal">
								<div class="title">Last Name:</div>
								<!--<div class="cart_doll">&nbsp;</div>-->
								<div class="amount" style="width:150px;">
								<!--<input type="text" class="field" id="user_lname_notlog" value="// echo $_SESSION['guest_lname'];" style="width:100px; height:12px; font-size:11px;"/>-->
								<input type="text" class="field" id="user_lname_notlog" value="<?php echo $_SESSION['guest_lname'];?>" style="width:100px; height:12px; font-size:11px;"/>
								</div>
							</div>
							<div class="subtotal" style="margin-bottom:0px;">
								<div class="title">Email :</div>
								<!--<div class="cart_doll">&nbsp;</div>-->
								<div class="amount" style="width:150px;">
								<!--<input type="text" class="field" id="email_notlog" value=" //echo $_SESSION['guest_email'];" style="width:100px; height:12px; font-size:11px;"/>-->
								<input type="text" class="field" id="email_notlog" value="<?php echo $_SESSION['guest_email'];?>" style="width:100px; height:12px; font-size:11px;"/>
								</div>
							</div>
	<?php
						}
						else if (isset($_SESSION['login_email']))
						{
							if($_SESSION['login_email']=="" || $_SESSION['login_email']==null && isset($_SESSION['guest_name']))
							{
	?>
								<div class="subtotal">
									<div class="title">First Name:</div>
									<!--<div class="cart_doll">&nbsp;</div>-->
									<div class="amount" style="width:150px;">
									<!--<input type="text" class="field" id="user_name_notlog" value="//echo $_SESSION['guest_name'];" style="width:100px; height:12px; font-size:11px;"/>-->
									<input type="text" class="field" id="user_name_notlog" value="" style="width:100px; height:12px; font-size:11px;"/>
									</div>
								</div>
								<div class="subtotal">
									<div class="title">Last Name:</div>
									<!--<div class="cart_doll">&nbsp;</div>-->
									<div class="amount" style="width:150px;">
									<!--<input type="text" class="field" id="user_lname_notlog" value=" //echo $_SESSION['guest_lname'];" style="width:100px; height:12px; font-size:11px;"/>-->
									<input type="text" class="field" id="user_lname_notlog" value="" style="width:100px; height:12px; font-size:11px;"/>
									</div>
								</div>
								<div class="subtotal" style="margin-bottom:0px;">
									<div class="title">Email :</div>
									<!--<div class="cart_doll">&nbsp;</div>-->
									<div class="amount" style="width:150px;">
									<!--<input type="text" class="field" id="email_notlog" value="// echo $_SESSION['guest_email'];" style="width:100px; height:12px; font-size:11px;"/>-->
									<input type="text" class="field" id="email_notlog" value="" style="width:100px; height:12px; font-size:11px;"/>
									</div>
								</div>
	<?php
							}
						}
								$total = 0;
								if(mysql_num_rows($get_all_data_fan)>0){
									while($get_cart = mysql_fetch_assoc($get_all_data_fan)){
										$total  = $total + $get_cart['price'] + $get_cart['donate'];
									}
								}
								if(mysql_num_rows($get_all_data)>0){
									while($ans_alls = mysql_fetch_assoc($get_all_data)){
										$total  = $total + $ans_alls['price'] + $ans_alls['donate'];
									}
								}
								?>
						</div>
						<div class="cart_doll" id="totle_sin_rows1" style="display:none">$<?php echo $total;?></div>
<?php
							if(!isset($_SESSION['login_email']))
							//if($_SESSION['login_email']=="" || $_SESSION['login_email']==null)
							{
?>
								<!--<div class="subtotal" style="padding-top: 5px; border-top: 1px solid #CCCCCC;">
<?php
							}
							else if ($_SESSION['login_email']=="" || $_SESSION['login_email']==null)
							{
?>
								<div class="subtotal" style="padding-top: 5px; border-top: 1px solid #CCCCCC;">
<?php
							}
							else
							{
?>
								<div class="subtotal" style="padding-top: 5px;">
<?php
							}

								if(mysql_num_rows($get_all_data_fan)>0){
									while($get_cart = mysql_fetch_assoc($get_all_data_fan)){
										$total  = $total + $get_cart['price'] + $get_cart['donate'];
									}
								}
								if(mysql_num_rows($get_all_data)>0){
									while($ans_alls = mysql_fetch_assoc($get_all_data)){
										$total  = $total + $ans_alls['price'] + $ans_alls['donate'];
									}
								}
								?>
							<div class="title">Subtotal:</div>
							<div class="cart_doll" id="totle_sin_rows1">$<?php echo $total;?></div>
							<div class="amount"> </div>
						</div>-->
						<!--<div class="subtotal">
							<div class="title">Donate to Purify Art:</div>
							<div class="cart_doll">$</div>
							<div class="amount" style="width:200px;">
								<input type="text" class="field" value="<?php if(isset($purify_donate['amount'])) { echo $purify_donate['amount']; } ?>" id="donate_purify"  style="width:100px; height:12px; font-size:11px;"/>
								<!--<input type="button" value="Update" onclick="donate_to_purify()"/>
								<a onclick="donate_to_purify(<?php echo $total;?>)" href="javascript:void(0);">Update </a>
								<a onclick="remove_donate_purify(<?php echo $total;?>)" href="javascript:void(0);">| Remove</a>
							</div>
						</div>-->
						<div class="total_amount_Cont" style="border-top:1px solid #CCCCCC;height:35px;width:400px; clear:both;">
							<div class="subtotal">
								<?php
									if(isset($purify_donate['amount']))
									{
										$total = $total + $purify_donate['amount'];
									}
									else
									{
										$total = $total;
									}
								?>
								<div class="title">Total:</div>
								 <div class="cart_doll" id="totle_sin_rows2">$<?php echo $total;?></div>
								 <input type="hidden" id="grand_total" value="<?php echo $total;?>"/>
								<div class="amount"></div>
							</div>
						</div>
					</div>
					<!--</div>-->
					<!--<div style="width:185px; float:left;">-->
					
					<!--</div>-->
					<?php 
						if($buyer_id == "" || !isset($_SESSION['login_email']))//$_SESSION['login_email']==null
						{ 
							$rem_alls_ids = $_SERVER['REMOTE_ADDR'];
							$not = '1';
						}
						else{ 
							$rem_alls_ids = $buyer_id;
							$not = '0';
						}
					?>
					
					<div id="mus-listFiles">
						<div class="titleCont">
							<div class="blkB">Title</div>
							<div class="blkC">Price</div>
							<div class="blkD">Tip</div>
							<div class="blkE">Total</div>
							<div class="blkF"><a href="javascript:void(0);" onclick="confirmdelete('<?php echo $rem_alls_ids; ?>','<?php echo $not; ?>')">Remove All</a></div>
						</div>
					
						
						<?php
						
						if($buyer_id ==" " || $buyer_id == null || !isset($_SESSION['login_email']))//$_SESSION['login_email']==null
						{
							$get_all_data = $add_to_cart_obj->Get_all_cart_data_not_login($_SERVER["REMOTE_ADDR"]);
							$get_all_data_fan = $add_to_cart_obj->get_fan_detail_notlogin();
						}else{
							$get_all_data = $add_to_cart_obj->Get_all_cart_data($buyer_id);
							$get_all_data_fan = $add_to_cart_obj->get_Cart($buyer_id);
						}
						while($get_cart_fan = mysql_fetch_assoc($get_all_data_fan))
						{
							$get_seller_name = $add_to_cart_obj->Get_seller_name($get_cart_fan['seller_id']);
							if($get_cart_fan['table_name']=="general_community"){$id_name= "community_id";}
							if($get_cart_fan['table_name']=="community_project"){$id_name= "id";}
							if($get_cart_fan['table_name']=="general_artist"){$id_name= "artist_id";}
							if($get_cart_fan['table_name']=="artist_project"){$id_name= "id";}
							$get_cart_data = $add_to_cart_obj->get_cart_data($get_cart_fan['table_name'],$id_name,$get_cart_fan['type_id']);
							if($get_cart_fan['table_name']=="general_community"){$image_name = "http://comjcropthumb.s3.amazonaws.com/".$get_cart_data['listing_image_name'];$title = $get_cart_data['name'];$creator_disp = $get_cart_data['name'];$creator_url = $get_cart_data['profile_url'];}
							if($get_cart_fan['table_name']=="community_project"){$image_name = $get_cart_data['listing_image_name'];$title = $get_cart_data['title'];
								if($get_cart_data['type']=="for_sale"){
									$creator_disp = $get_cart_data['creator'];
									$exp_creator_info = explode("|",$get_cart_data['creators_info']);
									$get_creator_url = $add_to_cart_obj->get_creator_profile_url($exp_creator_info[0],$exp_creator_info[1]);
									$creator_url = $get_creator_url['profile_url'];
									$title_url = $get_cart_data['profile_url'];
								}
								elseif($get_cart_data['type']=="free_club")
								{
									$exp_creator_info = explode("|",$get_cart_data['creators_info']);
									$get_creator_url = $add_to_cart_obj->get_creator_profile_url($exp_creator_info[0],$exp_creator_info[1]);
									$creator_url = $get_creator_url['profile_url'];
									$creator_disp = $get_cart_data['creator'];
									$title_url = $get_cart_data['profile_url'];
								}
								else{
									$creator_disp = $get_cart_data['title'];
									$creator_url = $get_cart_data['profile_url'];
								}
								
							}
							if($get_cart_fan['table_name']=="general_artist"){$image_name = "http://artjcropthumb.s3.amazonaws.com/".$get_cart_data['listing_image_name'];$title = $get_cart_data['name'];$creator_disp = $get_cart_data['name'];$creator_url = $get_cart_data['profile_url'];}
							if($get_cart_fan['table_name']=="artist_project"){$image_name = $get_cart_data['listing_image_name'];$title = $get_cart_data['title'];
								if($get_cart_data['type']=="for_sale"){
									$exp_creator_info = explode("|",$get_cart_data['creators_info']);
									$get_creator_url = $add_to_cart_obj->get_creator_profile_url($exp_creator_info[0],$exp_creator_info[1]);
									$creator_url = $get_creator_url['profile_url'];
									$creator_disp = $get_cart_data['creator'];
									$title_url = $get_cart_data['profile_url'];
								}
								elseif($get_cart_data['type']=="free_club")
								{
									$exp_creator_info = explode("|",$get_cart_data['creators_info']);
									$get_creator_url = $add_to_cart_obj->get_creator_profile_url($exp_creator_info[0],$exp_creator_info[1]);
									$creator_url = $get_creator_url['profile_url'];
									$creator_disp = $get_cart_data['creator'];
									$title_url = $get_cart_data['profile_url'];
								}
								else{
									$creator_disp = $get_cart_data['title'];
									$creator_url = $get_cart_data['profile_url'];
								}
								
							}
							$get_cart_price = $add_to_cart_obj->get_cart_price($get_cart_fan['seller_id'],$get_cart_fan['buyer_id'],$get_cart_fan['type_id'],$get_cart_fan['table_name']);
						?>
							<div class="tableCont">
								<div id="<?php echo $get_cart_fan['id'];?>">
									<div class="blkB"><img src="<?php echo $image_name;?>" width="50" height="50" /><?php if($get_cart_data['type']=="for_sale"){ if(strlen($title)>26){echo "<a href='javascript:void(0)' onclick=click_profile_url('$title_url')>".substr(str_replace("\'","'",$title),0,26)."..</a>";}else{ echo "<a href='javascript:void(0)' onclick=click_profile_url('$title_url')>".str_replace("\'","'",$title)."</a>"; } }elseif($get_cart_data['type']=="free_club"){ $title = $title; if(strlen($title)>26){ echo substr(str_replace("\'","'",$title),0,26)."..";}else{ echo str_replace("\'","'",$title);} }else{ $title = $title ." Fan Club Membership"; if(strlen($title)>26){ echo substr(str_replace("\'","'",$title),0,26)."..";}else{ echo str_replace("\'","'",$title);} }?><br /><a href="javascript:void(0)" onclick="click_profile_url('<?php echo $creator_url;?>')"><?php echo $creator_disp;?></a></div>
									<div class="blkC"><span class="hide"><strong>Price</strong></span> $<?php echo $get_cart_fan['price'];?></div>
									<div class="blkD"><span class="hide"><strong>Tip</strong></span> $<input type="text" id="donate<?php echo $get_cart_fan['id'];?>" value="<?php echo $get_cart_fan['donate'];?>" class="field" onblur="chk_for_update('donate<?php echo $get_cart_fan['id'];?>','<?php echo $get_cart_fan['id'];?>','add_to_cart_fan_pro')" /></div>
									<div class="blkE" id="totle_sin_rows_<?php echo $get_cart_fan['id'];?>"><span class="hide"><strong>Total</strong></span> $<?php echo $get_cart_fan['total'];?></div>
									<div class="blkF"><a href="javascript:void(0);" onclick="update_cart('<?php echo $get_cart_fan['id'];?>','add_to_cart_fan_pro','<?php echo $get_cart_fan['price']; ?>')" >Update</a> | <a href="javascript:void(0);" onclick ="confirm_remove('<?php echo $get_cart_fan['id'];?>','<?php echo $buyer_id;?>','fan_club','<?php echo $get_cart_fan['total'];?>')">Remove</a></div>
									
								</div>
							</div>
						<?php
						//$sub_total = $sub_total + $get_cart_data['price'] + $get_cart_data['donate'];
						//$sub_total = $sub_total + $get_cart_price;
						
						}
						$sub_total = 0;
						$all_tip_id ="";
						while($get_cart = mysql_fetch_assoc($get_all_data))
						{
							//$get_seller_name = $add_to_cart_obj->Get_seller_name($get_cart['seller_id']);
							$get_seller_name = $add_to_cart_obj->Get_media_creator($get_cart['media_id']);
							$exp_creator_info = explode("|",$get_seller_name['creator_info']);
							if(isset($exp_creator_info[0]) && isset($exp_creator_info[1]))
							{
								$get_creator_url = $add_to_cart_obj->get_creator_profile_url($exp_creator_info[0],$exp_creator_info[1]);
							?>
								<div class="tableCont">
									<div id="<?php echo $get_cart['id']; ?>">
										<div class="blkB"><img src="<?php echo $get_cart['image_name'];?>" width="50" height="50" /><?php if(strlen($get_cart['title'])>26){ echo substr(str_replace("\'","'",$get_cart['title']),0,26)."..";}else{ echo str_replace("\'","'",$get_cart['title']); }?><br /><a href="javascript:void(0)" onclick="click_profile_url('<?php echo $get_creator_url['profile_url'];?>')"><?php echo $get_seller_name['creator'];?></a></div>
										<div class="blkC"><span class="hide"><strong>Price</strong></span> $<?php echo $get_cart['price'];?></div>
										<div class="blkD"><span class="hide"><strong>Tip</strong></span> $<input type="text" id="donate_cart<?php echo $get_cart['id'];?>" value="<?php echo $get_cart['donate'];?>" class="field" onblur="chk_for_update('donate_cart<?php echo $get_cart['id'];?>','<?php echo $get_cart['id'];?>','addtocart')" /></div>
										<div class="blkE" id="totle_sin_rows_<?php echo $get_cart['id'];?>"><span class="hide"><strong>Total</strong></span> $<?php echo $get_cart['price'] + $get_cart['donate'];?></div>
										<div class="blkF"><a href="javascript:void(0);" onclick="update_donate_cart('<?php echo $get_cart['id'];?>','addtocart','<?php echo $get_cart['price']; ?>')" >Update</a> | <a href="javascript:void(0);" onclick ="confirm_remove('<?php echo $get_cart['id'];?>','<?php echo $buyer_id;?>','song','<?php echo $get_cart['price'] + $get_cart['donate'];?>')">Remove</a>
										<?php $all_tip_id = $get_cart['id'].",".$all_tip_id;?>
										<!--<span style="color:red;display:none;font-size:9px;font-weight:bold;" id="error<?php //echo $get_cart['id'];?>">Item is not updated.</span>-->
										</div>
									</div>
								</div>
						<?php
							}
						$sub_total = $sub_total + $get_cart['price'] + $get_cart['donate'];
						}
						?>
						<input type="text" id="chk_for_tip_link" value="<?php echo $all_tip_id;?>" style="display:none" />
					   <input type="hidden" value="<?php echo $sub_total; ?>" id="total_pass" name="total_pass" />
					</div>
					<div id="mus-totalCont_new">
						<div class="shopping">
							<input type="button" style="border-radius:5px;margin-bottom:10px;" value="Continue Shopping" class="button" onclick="continue_shopping()"/>
							<input type="button" id="chk_out_org" style="border-radius:5px;float:right;" onclick="get_payment_form()" value="Checkout" class="button">
							<!--<input type="button" id="chk_out_dup" style="float:right;display:none" onclick="alerttoupdate()" value="Checkout" class="button">-->
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
    </div>

<input type="hidden" id="idandvalue"/>
</body>
<script type="text/javascript" src="javascripts/jquery.js"></script>
<script type="text/javascript">
function get_payment_form()
{
	var get_ids = document.getElementById("chk_for_tip_link").value;
	get_ids = get_ids.split(",");
	for(var id_count=0;id_count<=get_ids.length;id_count++){
		if(document.getElementById("donate_cart"+get_ids[id_count])!=null){
			var donate = document.getElementById("donate_cart"+get_ids[id_count]).value;
			if(donate < 0){
				alert("Tip must be greater than or equal to zero.");
				return false;
			}
		}
	}
	if(document.getElementById("idandvalue")){
		var donate = document.getElementById("idandvalue").value;
	}else{
		var donate = "";
	}
	if(document.getElementById("donate_purify")){ var donate_puri = document.getElementById("donate_purify").value; }else{ var donate_puri = ""; }
	if(document.getElementById("grand_total")){ var cart_items = document.getElementById("grand_total").value; }else{ var cart_items = ""; }
	var expdonate = donate.split("|");
	var expdonate2 = expdonate[0].split(",");
	if(donate_puri>0 || donate_puri.trim()==""){
		if(donate_puri>0 || cart_items>0 || expdonate2[1]>0){
			<?php 
			//if($_SESSION['login_email']==null){
			if(!isset($_SESSION['login_email'])){
			?>
				var notlog_user = document.getElementById("user_name_notlog").value;
				var notlog_userl = document.getElementById("user_lname_notlog").value;
				var notlog_email = document.getElementById("email_notlog").value;
				if(notlog_user == "" || notlog_userl =="" || notlog_email == "" || !IsValidEmail(notlog_email))
				{
					alert("You need to provide you first name, last name and email address so we can contact you.");
				}else{
					if(donate_puri !="" || donate_puri !=0 || donate_puri >0){
						openurl = "updatecart.php?alldata="+donate+"&buyer_id="+'<?php echo $buyer_id; ?>'+"&from_url="+'<?php echo $back_to_url; ?>&donate_purify=1&amount='+donate_puri+'&notlog_user_name='+notlog_user+'&notlog_user_lname='+notlog_userl+'&notlog_user_email='+notlog_email;
					}else{
						openurl = "updatecart.php?alldata="+donate+"&buyer_id="+'<?php echo $buyer_id; ?>'+"&from_url="+'<?php echo $back_to_url; ?>'+'&notlog_user_name='+notlog_user+'&notlog_user_lname='+notlog_userl+'&notlog_user_email='+notlog_email;
					}
					window.parent.open(openurl, '_blank');
				}
			<?php
			}else{
			?>
			if(donate_puri !="" || donate_puri !=0 || donate_puri >0){
				openurl = "updatecart.php?alldata="+donate+"&buyer_id="+'<?php echo $buyer_id; ?>'+"&from_url="+'<?php echo $back_to_url; ?>&donate_purify=1&amount='+donate_puri;
			}else{
				openurl = "updatecart.php?alldata="+donate+"&buyer_id="+'<?php echo $buyer_id; ?>'+"&from_url="+'<?php echo $back_to_url; ?>';
			}
			window.parent.open(openurl, '_blank');
			<?php
			}
			?>
		}else{
			alert("There are no items in your cart to checkout.");
		}
	}else{
		alert("You may not enter $0 in the Donate to Purify Art field, either enter nothing or a value greater than 0.");
	}
}

function donate_to_purify(act_totls)
{
	if(document.getElementById("donate_purify")){
		var donate = document.getElementById("donate_purify").value;
	}else{
		var donate = "";
	}
	
	if(donate >0 && isNaN(donate)==false){
		$.ajax({
			type: "GET",
			url: 'addtocart.php',
			data: { "donate_purify":'1', "amount":donate, "seller_id":'<?php if(isset($_GET['seller_id'])) { echo $_GET['seller_id']; } ?>', "media_id":'<?php if(isset($_GET['media_id'])) { echo $_GET['media_id']; } ?>', "buyer_id":'<?php echo $buyer_id; ?>', "title":'<?php if(isset($_GET['title'])) { echo urlencode($_GET['title']); } ?>' },
			success: function(data){
				var spls_item_grd_upes = $("#totle_sin_rows1").html().split('$');
				var gr_totls_ups = parseFloat(spls_item_grd_upes[1]) + parseFloat(donate);
				//var gr_totls_ups = parseFloat(act_totls) + parseFloat(donate);
				//document.getElementById("total_pass").value = gr_totls_ups;
				document.getElementById("grand_total").value = gr_totls_ups;
				//$("#totle_sin_rows1").html('$'+gr_totls_ups);
				$("#totle_sin_rows2").html('$'+gr_totls_ups);
			}
		});
	}else{
		alert("You may not enter $0 in the Donate to Purify Art field, either enter nothing or a value greater than 0.");
	}
}

function remove_donate_purify(act_totls)
{
	var x= confirm("Are you sure you want to remove donation?");
	if(x==true)
	{
		if(document.getElementById("donate_purify")){
			var donate = document.getElementById("donate_purify").value;
		}else{
			var donate = "";
		}
		
		if(donate >0 || donate.trim()!=""){
			$.ajax({
				type: "GET",
				url: 'addtocart.php',
				data: { "remove_donate_purify":'1', "amount":donate, "seller_id":'<?php if(isset($_GET['seller_id'])) { echo $_GET['seller_id']; } ?>', "media_id":'<?php if(isset($_GET['media_id'])) { echo $_GET['media_id']; } ?>', "buyer_id":'<?php echo $buyer_id; ?>', "title":'<?php if(isset($_GET['title'])) { echo urlencode($_GET['title']); } ?>' },
				success: function(data){
					var spls_item_grd_upes = $("#totle_sin_rows1").html().split('$');
					var gr_totls_ups = (parseFloat(spls_item_grd_upes[1]) + parseFloat(donate)) - parseFloat(donate);
					document.getElementById("grand_total").value = gr_totls_ups;
					$("#totle_sin_rows2").html('$'+gr_totls_ups);
					document.getElementById("donate_purify").value="";
				}
			});
		}else{
			alert("There is nothing to remove.");
		}
	}
}

function update_cart(id,tbl_name,actprice)
{
	if(document.getElementById("donate"+id)){
		var donate = document.getElementById("donate"+id).value;
	}else{
		var donate = "";
	}
	
	//var donate_val = donate.split("$");
	$.ajax({
		type: "GET",
		url: 'addtocart.php',
		data: { "update_id":id, "update_val":donate, "buyer_id":'<?php echo $buyer_id; ?>', "table_name":tbl_name, "from_url":'<?php echo $back_to_url;?>' },
		success: function(data){
			var spls_item_grd = $("#totle_sin_rows_"+id).html().split('$');
			
			var new_gr_totls_item = parseFloat(actprice) + parseFloat(donate);
			var old_gr_totls_item = parseFloat(spls_item_grd[1]);
			var diff_old_new = new_gr_totls_item - old_gr_totls_item;
			
			var spls_item_grd_upes = $("#totle_sin_rows1").html().split('$');
			
			if(diff_old_new>0)
			{
				var gr_totls_ups = parseFloat(document.getElementById("grand_total").value) + parseFloat(diff_old_new);
				var ahg_upes_totls_ups = parseFloat(spls_item_grd_upes[1])+parseFloat(diff_old_new);
				//document.getElementById("total_pass").value = gr_totls_ups;
				document.getElementById("grand_total").value = gr_totls_ups;
				$("#totle_sin_rows1").html('$'+ahg_upes_totls_ups);
				$("#totle_sin_rows2").html('$'+gr_totls_ups);
				$("#totle_sin_rows_"+id).html('$'+new_gr_totls_item);
			}
			else if(diff_old_new<0)
			{
				var gr_totls_ups = parseFloat(document.getElementById("grand_total").value) + parseFloat(diff_old_new);
				var ahg_upes_totls_ups = parseFloat(spls_item_grd_upes[1])+parseFloat(diff_old_new);
				//document.getElementById("total_pass").value = gr_totls_ups;
				document.getElementById("grand_total").value = gr_totls_ups;
				$("#totle_sin_rows1").html('$'+ahg_upes_totls_ups);
				$("#totle_sin_rows2").html('$'+gr_totls_ups);
				$("#totle_sin_rows_"+id).html('$'+new_gr_totls_item);
			}
		}
	});
}

function update_donate_cart(id,tbl_name,actprice)
{
	if(document.getElementById("donate_cart"+id)){
		var donate = document.getElementById("donate_cart"+id).value;
	}else{
		var donate = "";
	}
	

	if(donate >=0 || donate.trim()==""){
		$.ajax({
			type: "GET",
			url: 'addtocart.php',
			data: { "update_id":id, "update_val":donate, "buyer_id":'<?php echo $buyer_id; ?>', "table_name":tbl_name, "from_url":'<?php echo $back_to_url;?>' },
			success: function(data){
			
				var spls_item_grd = $("#totle_sin_rows_"+id).html().split('$');
				
				var new_gr_totls_item = parseFloat(actprice) + parseFloat(donate);
				var old_gr_totls_item = parseFloat(spls_item_grd[1]);
				var diff_old_new = new_gr_totls_item - old_gr_totls_item;
				
				var spls_item_grd_upes = $("#totle_sin_rows1").html().split('$');
				if(diff_old_new>0)
				{
					var gr_totls_ups_fr_single = parseFloat(document.getElementById("total_pass").value) + parseFloat(diff_old_new);
					var gr_totls_ups = parseFloat(document.getElementById("grand_total").value) + parseFloat(diff_old_new);
					var ahg_upes_totls_ups = parseFloat(spls_item_grd_upes[1])+parseFloat(diff_old_new);
					
					document.getElementById("total_pass").value = gr_totls_ups_fr_single;
					$("#totle_sin_rows_"+id).html('$'+new_gr_totls_item);
					
					document.getElementById("grand_total").value = gr_totls_ups;
					$("#totle_sin_rows1").html('$'+ahg_upes_totls_ups);
					$("#totle_sin_rows2").html('$'+gr_totls_ups);
					
				}
				else if(diff_old_new<0)
				{
					var gr_totls_ups_fr_single = parseFloat(document.getElementById("total_pass").value) + parseFloat(diff_old_new);
					var gr_totls_ups = parseFloat(document.getElementById("grand_total").value) + parseFloat(diff_old_new);				
					var ahg_upes_totls_ups = parseFloat(spls_item_grd_upes[1])+parseFloat(diff_old_new);
					
					document.getElementById("total_pass").value = gr_totls_ups_fr_single;
					$("#totle_sin_rows_"+id).html('$'+new_gr_totls_item);
					
					document.getElementById("grand_total").value = gr_totls_ups;
					$("#totle_sin_rows1").html('$'+ahg_upes_totls_ups);
					$("#totle_sin_rows2").html('$'+gr_totls_ups);				
				}
			}
		});
	}else{
		alert("Tip must be greater than or equal to zero.");
	}
}
function confirm_remove(ids,buys_ids,tiyp,totals_of_al)
{
	var conf = confirm("Are You Sure You Want To Remove This Item From Cart.");
	if(conf == false)
	{
		return false;
	}
	else if (conf == true)
	{
		if(document.getElementById("donate_purify")){
			var donate = document.getElementById("donate_purify").value;
		}else{
			var donate = "";
		}
		
		
		if(tiyp=='song')
		{
			$.ajax({
				type: "GET",
				url: 'addtocart.php',
				data: { "remove":ids, "buyer_id":buys_ids, "from_url":'<?php echo $back_to_url;?>' },
				success: function(data){
					var gr_total = parseFloat(document.getElementById("grand_total").value) - parseFloat(totals_of_al);
					
					var spls_item_grd = $("#totle_sin_rows1").html().split('$');
					var new_gr_totls_item = parseFloat(spls_item_grd[1]) - parseFloat(totals_of_al);
					
					document.getElementById("grand_total").value = gr_total;
					$("#totle_sin_rows2").html('$'+gr_total);				
					$("#totle_sin_rows1").html('$'+new_gr_totls_item);
					
					if(tiyp=='song')
					{
						var single_song_total = parseFloat(document.getElementById("total_pass").value) - parseFloat(totals_of_al);
						document.getElementById("total_pass").value = single_song_total;
					}
					$("#mus-listFiles .tableCont #"+ids).html('');
				}
			});
		}
		else if(tiyp=='fan_club')
		{
			$.ajax({
				type: "GET",
				url: 'addtocart.php',
				data: { "remove":ids, "remove_type":'fan_club', "buyer_id":buys_ids, "from_url":'<?php echo $back_to_url;?>' },
				success: function(data){
					var gr_total = parseFloat(document.getElementById("grand_total").value) - parseFloat(totals_of_al);
					
					var spls_item_grd = $("#totle_sin_rows1").html().split('$');
					var new_gr_totls_item = parseFloat(spls_item_grd[1]) - parseFloat(totals_of_al);
					
					document.getElementById("grand_total").value = gr_total;
					$("#totle_sin_rows2").html('$'+gr_total);				
					$("#totle_sin_rows1").html('$'+new_gr_totls_item);
					
					if(tiyp=='song')
					{
						var single_song_total = parseFloat(document.getElementById("total_pass").value) - parseFloat(totals_of_al);
						document.getElementById("total_pass").value = single_song_total;
					}
					$("#mus-listFiles .tableCont #"+ids).html('');
				}
			});
		}
	}
}
function confirmdelete(buyer_ids,not_yes)
{
	var conf = confirm("Are you sure you want to remove all items from your cart.");
	if(conf == false)
	{
		return false;
	}
	else if (conf == true)
	{
		var donate = document.getElementById("donate_purify").value;
		if(not_yes=='1')
		{
			var send_not_yes_val = 'yes';
		}
		else
		{
			var send_not_yes_val = 'no';
		}
		
		$.ajax({
			type: "GET",
			url: 'addtocart.php',
			data: { "removeall":'1', "buyer_id":buyer_ids, "from_url":'<?php echo $back_to_url;?>', "notlogin":send_not_yes_val },
			success: function(data){
				if(donate=="" || donate==0)
				{
					document.getElementById("grand_total").value = 0;
					$("#totle_sin_rows2").html('$0');
				}
				else
				{
					document.getElementById("grand_total").value = donate;
					$("#totle_sin_rows2").html('$'+donate);
				}
				$("#mus-listFiles .tableCont").html('');
				document.getElementById("total_pass").value = "";
				$("#totle_sin_rows1").html('$');
			}
		});
	}
}
function chk_for_update(id,val,tblname)
{
	var a = document.getElementById(id);
	var sendval = document.getElementById("idandvalue");
	if(a.defaultValue==0 && a.value>0){
		sendval.value = sendval.value +","+ a.value +"|"+ val +"|"+tblname;
	}else if(a.defaultValue != a.value){
		sendval.value = sendval.value +","+ a.value +"|"+ val +"|"+tblname;
	}
}
function alerttoupdate(){
alert("Please update first.");
}
function continue_shopping(){
	if(document.getElementById("idandvalue")){ var donate = document.getElementById("idandvalue").value; }else{ var donate = ""; }
	if(document.getElementById("donate_purify")){ var donate_puri = document.getElementById("donate_purify").value; }else{ var donate_puri = ""; }
	
	var notlog_user = "";
	var notlog_userl = "";
	var notlog_email = "";
	<?php 
	if(!isset($_SESSION['login_email'])){
	?>
		var notlog_user = document.getElementById("user_name_notlog").value;
		var notlog_userl = document.getElementById("user_lname_notlog").value;
		var notlog_email = document.getElementById("email_notlog").value;
	<?php
	}?>
	
	if(donate_puri !="" || donate_puri !=0 || donate_puri >0){
		window.parent.location.href ="updatecart.php?alldata="+donate+"&buyer_id="+'<?php if(isset($get_buyer)) { echo $get_buyer['general_user_id']; } ?>'+"&back_url="+'<?php echo $back_to_url; ?>&donate_purify=1&amount='+donate_puri+'&notlog_user_name='+notlog_user+'&notlog_user_lname='+notlog_userl+'&notlog_user_email='+notlog_email;
	}else{
		window.parent.location.href ="updatecart.php?alldata="+donate+"&buyer_id="+'<?php if(isset($get_buyer)) { echo $get_buyer['general_user_id']; } ?>'+"&back_url="+'<?php echo $back_to_url; ?>'+'&notlog_user_name='+notlog_user+'&notlog_user_lname='+notlog_userl+'&notlog_user_email='+notlog_email;
	}
//parent.jQuery.fancybox.close();
}

function click_profile_url(url)
{
	openurl = "/"+url;
	window.parent.open(openurl, '_self');
}

function IsValidEmail(strValue)
{
 var objRegExp  = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
 return objRegExp.test(strValue);
}


/*$("document").ready(function(){
$("#fancybox-content").css({"width":"623px"});
});*/
</script>
</html>
