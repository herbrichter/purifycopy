<html>
<head>
<script language="javascript" type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
<script type="text/javascript" src="ckeditor.js"></script>
<script type="text/javascript" src="jquery.js"></script>
<script src="sample.js" type="text/javascript"></script>
<link href="sample.css" rel="stylesheet" type="text/css" />
<link href="includes/purify.css" rel="stylesheet" type="text/css" />
</head>
<body>
<?php
include_once('commons/db.php');
include_once('classes/EditPosts.php');
$pos_obj = new EditPosts();
$get_post_data = $pos_obj -> selectpost_data(mysql_real_escape_string($_GET['ed_post']));
?>
	<div>
		<div class="mine_loader" id="mine_loader" style="display:none">
			<img style="height: 35px; top: 50%; position: relative;" src="images/ajax_loader_large.gif"/>
		</div>
		<div class="fancy_header">
			Edit Post
		</div>
		<div id="actualContent" style="width: 95%;" class="cartWrapperwhole">
			<form method="POST" action="edit_post.php" onSubmit="return validate_post_first()" id="news_first_form">
			<input type="hidden"  value="<?php echo $_GET['ed_post'];?>" name="edit_post_id"/>
				<div class="fieldCont">
					<div class="fieldTitle" title="Select a topic to post about.">Topic</div>
					<select title="Select a topic to post about." class="dropdown" id="post_list_first" name="post_list_first">
						<option value="images_post"   <?php if($get_post_data['profile_type']=="images_post"){ echo "selected"; } ?> >Images</option>
						<option value="songs_post"    <?php if($get_post_data['profile_type']=="songs_post"){ echo "selected"; } ?>>Songs</option>
						<option value="videos_post"   <?php if($get_post_data['profile_type']=="videos_post"){ echo "selected"; } ?>>Videos</option>
						<option value="events_post"   <?php if($get_post_data['profile_type']=="events_post"){ echo "selected"; } ?>>Events</option>
						<option value="projects_post" <?php if($get_post_data['profile_type']=="projects_post"){ echo "selected"; } ?>>Projects</option>
					</select>
					
					
				</div>
				
				<div id="loader_first" style="display:none;">
					<img src="images/ajax_loader_large.gif" style="height: 25px; position: relative; right: 285px; top: 38px; width: 25px;">
				</div>
				
				<div class="fieldCont" id="fieldCont_media_first">
				</div>
				
				<div class="fieldCont">
					<textarea class="jquery_ckeditor" id="description_post_first" name="description_post_first">
					<?php echo $get_post_data['description']; ?>
					</textarea>
				</div>
				
				<div class="fieldContainer" style="right:90px;">
					<div class="rightSide">
						<a href="#newsfeeds" onmouseover="mopen('m_post_first')" onmouseout="mclosetime()">
						<input id="allow_to_first" name="allow_to_first" type="button" value="All" style="border-radius:5px; -webkit-border-radius:5px; -moz-border-radius:5px; -ms-border-radius:5px; -o-border-radius:5px; background:black; border: medium none;color: white;font-family: futura-pt,sans-serif;font-size: 0.825em;font-weight: bold;padding: 8px 18px;" /></a>
						<div style="visibility:hidden; border: 1px solid #777777; width: 150px; text-decoration:none; height:76px;" id="m_post_first" class="popUp" onmouseover="mcancelclosetime()" onmouseout="mclosetime()">
							<ul style="list-style: none outside none;">
								<li><a id="1_post" style="cursor:pointer;" onclick="allowpost_first('1_post')">Fans</a></li>
								<li><a id="2_post" style="cursor:pointer;" onclick="allowpost_first('2_post')">Friends</a></li>
								<li><a id="3_post" style="cursor:pointer;" onclick="allowpost_first('3_post')">Subscribers</a></li>
							</ul>
						</div>
					</div>
				</div>		
				
				<div class="fieldCont">
					<div class="fieldTitle"></div>
					<input type="hidden" id="allowh_post_first" name="allowh_post_first" value="All" />
					<input type="submit" value="Save" class="register" style="position:relative;float:right;left:45px;border-radius:5px; -webkit-border-radius:5px; -moz-border-radius:5px; -ms-border-radius:5px; -o-border-radius:5px;"/>
				</div>
			</form>
		</div>
	</div>
</body>
<style>
.fancybox-inner{overflow:none !important;}
</style>
</html>
<script>
$(function()
{
	var config = {
		toolbar:
		[
			['Bold', 'Italic', '-', 'NumberedList', 'BulletedList', '-', 'Link', 'Unlink'],
			['UIColor']
		],
		enterMode : CKEDITOR.ENTER_BR
	};

	// Initialize the editor.
	// Callback function can be passed and executed after full instance creation.
	$('.jquery_ckeditor').ckeditor(config);
});
function validate_post_first()
{
	var list_post_first = document.getElementById("list_epm_first[]").value;
	if(list_post_first=="" || list_post_first==null)
	{
		alert("Please select any Media to Post.");
		return false;
	}else{
		$("#mine_loader").css({"display":"block"});
	}
}
$(function()
{
	$("#post_list_first").change(function() 
	{
		$("#loader_first").show();
		$("#fieldCont_media_first").hide();
		$.post("PostList_First.php", { type_val:$("#post_list_first").val() },
		function(data)
		{
			$("#fieldCont_media_first").html(data);
			$("#loader_first").hide();
			$("#fieldCont_media_first").show();
		});
	});
	$('.list-wrap').css({"height":""});
	get_list_in_edit();
});
function get_list_in_edit(){
	$("#loader_first").show();
	$("#fieldCont_media_first").hide();
	$.post("PostList_First_Edit.php", {
		type_val:$("#post_list_first").val(),
		media_value : "<?php $data = explode("~",$get_post_data['media']); echo $data[0];?>"
	},
	function(data)
	{
		$("#fieldCont_media_first").html(data);
		$("#loader_first").hide();
		$("#fieldCont_media_first").show();
	});
}

var timeout	= 500;
var closetimer	= 0;
var ddmenuitem	= 0;

// open hidden layer
function mopen(id)
{	
	// cancel close timer
	mcancelclosetime();

	// close old layer
	if(ddmenuitem) ddmenuitem.style.visibility = 'hidden';

	// get new layer and show it
	ddmenuitem = document.getElementById(id);
	ddmenuitem.style.visibility = 'visible';

}
// close showed layer
function mclose()
{
	if(ddmenuitem) ddmenuitem.style.visibility = 'hidden';
}

// go close timer
function mclosetime()
{
	closetimer = window.setTimeout(mclose, timeout);
}

// cancel close timer
function mcancelclosetime()
{
	if(closetimer)
	{
		window.clearTimeout(closetimer);
		closetimer = null;
	}
}

// close layer when click-out
document.onclick = mclose; 

function allowpost_first(ids)
{
	document.getElementById('allowh_post_first').value = document.getElementById(ids).innerHTML;
	document.getElementById('allow_to_first').value = document.getElementById('allowh_post_first').value;
	//alert(data);
	if(document.getElementById('allow_to_first').value=='All')
	{
		document.getElementById("1_post").innerHTML = 'Fans';
		document.getElementById("2_post").innerHTML = 'Friends';
		document.getElementById("3_post").innerHTML = 'Subscribers';
		$('#allow_to_first').css({"background-position":"32px"});
		$('#actualContent .fieldContainer').css({"float": "right","margin-bottom": "20px","position": "relative","width":"52px"});
	}
	else if(document.getElementById('allow_to_first').value=='Fans')
	{
		document.getElementById("1_post").innerHTML = 'All';
		document.getElementById("2_post").innerHTML = 'Friends';
		document.getElementById("3_post").innerHTML = 'Subscribers';
		$('#allow_to_first').css({"background-position":"40px"});
		$('#actualContent .fieldContainer').css({"float": "right","margin-bottom": "20px","position": "relative","width":"59px"});
	}
	else if(document.getElementById('allow_to_first').value=='Friends')
	{
		document.getElementById("1_post").innerHTML = 'All';
		document.getElementById("2_post").innerHTML = 'Fans';
		document.getElementById("3_post").innerHTML = 'Subscribers';
		$('#allow_to_first').css({"background-position":"53px"});
		$('#actualContent .fieldContainer').css({"float": "right","margin-bottom": "20px","position": "relative","width":"72px"});
	}
	else if(document.getElementById('allow_to_first').value=='Subscribers')
	{
		document.getElementById("1_post").innerHTML = 'All';
		document.getElementById("2_post").innerHTML = 'Fans';
		document.getElementById("3_post").innerHTML = 'Friends';
		$('#allow_to_first').css({"background-position":"78px"});
		$('#actualContent .fieldContainer').css({"float": "right","margin-bottom": "20px","position": "relative","width":"97px"});
	}
}
</script>