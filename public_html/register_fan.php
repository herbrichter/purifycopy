<?php 
	ob_start();
	include_once('sendgrid/SendGrid_loader.php');
	include_once('classes/UnregUser.php');
	include_once('classes/Fan.php');
	include_once('classes/UserSubscription.php');
	//include_once('classes/RegistrationEmail.php');
	include_once('classes/NotifyAdminEmail.php');
	include_once('recaptchalib.php');
	if($multiple_user_flag != 1)	
	{
		include_once('login_includes.php');
	}
	
	$publickey = "6Lduu-sSAAAAAHbUkh7_9mmfg3zQucvRxtr2gvbA";
	$privatekey = "6Lduu-sSAAAAALD2YftpY41sm3a1CtIpHeymu3TH";
	$parent=$_POST['member_type'];
	
	# the response from reCAPTCHA
	$resp = null;
	# the error code from reCAPTCHA, if any
	$error = null;
	
	
	# are we submitting the page?
	if(isset($_POST['register']))
	{
		$resp = recaptcha_check_answer ($privatekey,
        		                        $_SERVER["REMOTE_ADDR"],
                		                $_POST["recaptcha_challenge_field"],
                        		        $_POST["recaptcha_response_field"]);
		$email=$_POST['email'];
		$username=$_POST['username'];
		$password=$_POST['password'];
		$subscription_id=$_POST['select-subscription'];
		$payment_mode_id=$_POST['select-payment-mode'];
/*		$ref_by=$_POST['referred_by'];
		$country=$_POST['country'];
		$state_or_province=$_POST['state'];
		$city=$_POST['city'];
*/			
		if ($resp->is_valid)
		{
			$obj=new Fan();
			$obj2=new UserSubscription();
			
			/*if($ref_by&&!$obj->checkRefNo($ref_by))
			{
				$msg4="<div align='right' class='hintRed'>User with this reference no. does not exist.</div>";
				$ref_flag=0;
			}
			else
			{*/
				//$ref_flag=1;
			//}
			
			if($obj->checkEmail_fan($email))
			{
				#$msg01="<div align='right' class='hintRed'>Email ID you selected already exists, please select a new one.</div>";
				$msg01="Email ID you selected already exists, please select a new one.";
				$avail_flag1=0;
				header("location:register.php?f=2&e=".$msg01);
			}
			else
			{
				$avail_flag1=1;
			}
			
			if($obj->checkAvailability($username))
			{
				#$msg1="<div align='right' class='hintRed'>Username you selected already exists, please select a new one.</div>";
				$msg1="Username you selected already exists, please select a new one.";
				$avail_flag=0;
				header("location:register.php?f=2&e=".$msg1);
			}
			else
			{
				$avail_flag=1;
			}
			
			if($avail_flag==1&&$avail_flag1=1)
			{	
				do
				{
					$verification_no = rand(1000000000,9999999999);	
				}while($obj->checkVerificationNo($verification_no));
				
/*				do
				{
					$ref_no = rand(10000,99999);
					$ref_id=$_POST['fname'].$ref_no;
				}while($obj->checkRefNo($ref_id));				
*/				
				if($user_id=$obj->addUser($username,$password,$email,$verification_no))
				{	
					//$obj->updateReference($ref_by);
					$obj->addParentEntry($user_id);
					
					
					
					
					$obj2->addEntry($user_id,$subscription_id,$payment_mode_id);
					switch($payment_mode_id)
					{
						case 1: header('location:registration_payment_gc.php?uid='.$user_id.'&&sid='.$subscription_id.'');
								break;
						case 2: header('location:registration_payment_pp.php?uid='.$user_id.'&&sid='.$subscription_id.'');
								break;
						case 3: header('location:registration_fan_cashcheck_complete.php');
								break;
						default:header('location:registration_payment.php?uid='.$user_id.'&&sid='.$subscription_id.'');
								break;
					}
					$objAdmin=new NotifyAdminEmail();
					$objAdmin->notifyAdminEmail(); 
					header("Location:register.php");
				}	
			}
		} 
		else 
		{
		    # set the error code so that we can display it. You could also use
    		# die ("reCAPTCHA failed"), but using the error message is
		    # more user friendly
    		$error = $resp->error;
			header("location:register.php?f=2&e=Incorrect reCAPTCHA code. Try again");
  		}
	}
?>


<script>
	$(document).ready(
						function()
						{
							$("#chk_avail_link").click(
															function()
															{
																$.post("registration_user_availablity.php", { name:$("#username").val() },
																function(data)
																{
																	$("#user_name_msg").html(data);
																	$("#register").focus();	
																	$("#username").focus();																	
																}
																);
							
															}
							);	
							$("#chk_avail_link1").click(
															function()
															{
																$.post("registration_fan_email.php", { email:$("#email").val() },
																function(data)
																{
																	$("#user_email_msg").html(data);
																	$("#register").focus();	
																	$("#email").focus();																	
																}
																);
							
															}
															
							);		
		
					}
				);
</script>
<script type="text/javascript" language="javascript">
	function validate()
	{
		var username=$('#username');
		var password=$('#password');
		var password2=$('#password2');
		var email=$('#email');
		var frm=$('#registration_form');
		var email_val = /^([a-zA-Z0-9_.-])+@(([a-zA-Z0-9-])+.)+([a-zA-Z])+$/;
			
		if(username.val()=='' || password.val()=='' || password2.val()=='' || email.val()=='')
		{
			alert("Fields marked with (*) are compulsory.");
		}
		else
		{
			if(password.val().length<5)
			{
				alert("Password must be at least 5 characters");
				document.registration_form.password.focus();
			}
			else if(password.val()!=password2.val())
			{
				alert("Passwords do not match.");
				document.registration_form.password.focus();
			}
      		else if(!email_val.test(email.val()))
      		{
          		alert("Error: Email id not in correct format.");
		  		document.registration_form.email.focus();
     		}
			else if(!document.registration_form.accept.checked)
			{
				alert("Please read and accept the User Agreement & Privacy Policy.");
		  		document.registration_form.accept.focus();
			}			
			else
			{
				frm.submit();
			}
		}
	}
	
	function unreg_submit()
	{
		var frm=document.getElementById('unreg_form');
		var uname=$('#uname');
		var uemail=$('#uemail');
		var email_val = /^([a-zA-Z0-9_.-])+@(([a-zA-Z0-9-])+.)+([a-zA-Z])+$/;
		
		/*if(uname.val()==''||uname.val()=='Name')
		{
			alert("Please enter your name.");
			document.unreg_form.uname.focus();
		}
		else*/ if(!email_val.test(uemail.val()))
		{
			alert("Error: Email id not in correct format.");
			document.unreg_form.uemail.focus();
		}
		else
		{
			frm.submit();
		}
	}
	
	function clearme(theText)
	{
		if (theText.value == theText.defaultValue) {
         theText.value = ""
     	}		 
	}	
</script>

<p></p>
	<h1>Fan Membership</h1>

    <form id="registration_form" name="registration_form" method="post" action="register_fan.php" enctype="multipart/form-data">
      <input type="hidden" name="type" value="2" />
      <table width="650" border="0" cellpadding="0" cellspacing="0">
        <tr>
          <td>&nbsp;</td>
          <td>
            <div id="user_email_msg" class="hint" style="color:#FF0000">
              <?php echo $error_flag; ?>            
            </div>          
          </td>
          <td>&nbsp;</td>
        </tr>
		
		<tr>
          <td width="100">*Email</td>
          <td width="402"><input name="email" type="text" class="regularField" id="email" value="<?php echo $email; ?>" /></td>
          <td width="148" align="left"><div class="hint">Will stay private.</div></td>
		  <td><a href="#" id="chk_avail_link1" class="hint"></a></td>
        </tr>
        <tr>
          <td colspan="3"><img src="images/spacer.gif" alt=" " width="10" height="10" /></td>
        </tr>
        <tr>
          <td>*Username</td>
          <td><input name="username" type="text" class="regularField" id="username" value="<?php echo $username; ?>" /></td>
          <td><a href="#" id="chk_avail_link" class="hint">Check availability</a></td>
        </tr>
        <tr>
          <td colspan="3"><img src="images/spacer.gif" alt=" " width="10" height="10" /></td>
        </tr>
		<?php include_once('registration_block_subscription_fan.php'); ?>
        <tr>
          <td colspan="3"><img src="images/spacer.gif" alt=" " width="10" height="10" /></td>
        </tr>
		 <tr>
          <td>Payment mode</td>
          <td>
          	<select id="select-payment-mode" name="select-payment-mode" class="pulldownField">
              <option value="1">Google Checkout</option>
              <option value="2">PayPal</option>
              <option value="3">Cash/Check</option>
            </select>
          </td>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td colspan="3"><img src="images/spacer.gif" alt=" " width="10" height="10" /></td>
        </tr>
		<tr>
          <td>*Password</td>
          <td><input name="password" type="password" class="regularField" id="password" /></td>
          <td><div class="hint">Must be at least 5 characters..</div></td>
        </tr>
        <tr>
          <td colspan="3"><img src="images/spacer.gif" alt=" " width="10" height="10" /></td>
        </tr>
        <tr>
			<td></td>
			<td><span class="hintp">Passwords must be the same for every account using the same email address.</span></td>
			<td></td>
		</tr>
		<tr>
          <td>*Confirm</td>
          <td><input name="password2" type="password" class="regularField" id="password2" /></td>
          <td><div class="hint">Confirm  password.</div></td>
        </tr>
		<tr>
          <td colspan="3"><img src="images/spacer.gif" alt=" " width="10" height="10" /></td>
        </tr>
		
		
       
		<?php #include_once('registration_block_subscription_fan.php'); ?>
        
        
        <tr>
          <td>Are you human?</td>
          <td><?php echo recaptcha_get_html($publickey, $error); ?></td>
          <td width="148" align="left"><div class="hint">Helps stop spam</div></td>
        </tr>
        <tr>
          <td colspan="3"><img src="images/spacer.gif" alt=" " width="10" height="10" /></td>
        </tr>
        <tr>
          <td align="right"><input id="accept" name="accept" type="checkbox" validate="required:true" /></td>
          <td colspan="2" class="hint">I accept and agree to the Purify Entertainment <a href="about_agreements_user.php" target="_blank">User Agreement</a> and <a href="about_agreements_privacy.php" target="_blank">Privacy Policy</a>.</td>
        </tr>
        <tr>
          <td colspan="3"><img src="images/spacer.gif" alt=" " width="10" height="10" /></td>
        </tr>
        <tr>        
        <tr>
          <td>&nbsp;</td>
          <td align="right"><input type="button" name="register" id="register" value=" Register " onclick="validate();" /><input type="hidden" name="register" style="border-radius:5px; -webkit-border-radius:5px; -moz-border-radius:5px; -ms-border-radius:5px; -o-border-radius:5px;"  /></td>
          <td align="right">&nbsp;</td>
        </tr>
        <tr>
          <td colspan="3"><img src="images/spacer.gif" alt=" " width="10" height="10" /></td>
        </tr>        
      </table>
    </form>
