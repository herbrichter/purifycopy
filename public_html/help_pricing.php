<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "//www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="//www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<title>Purify Art: Help: Pricing</title>

<?php include("includes/header.php");?>
<div id="outerContainer"></div>
	<div id="contentContainer" > 

    <div id="actualContent" class="pricing">
      <h1>Pricing</h1>
      <h4><a href="index.php">Register</a> for FREE!</h4>
      <div id="pricingInfo">
        <div id="purchase">
          <h4 style="float:none">Once registered, <br />
            Users may <strong>Purchase</strong>:</h4>
          <ul>
            <li><strong>Annual Memberships</strong> for premium <a href="about_services.php">services</a> ($25/year)</li>
            <li><strong>Media</strong> available for sale (Price designated by Artist or Company Member)</li>
          </ul>
        </div>
        <div id="sell">
          <h4 style="float:none">Artist and  Company <br />
            Members can <strong>Sell</strong>:</h4>
          <ul>
            <li><strong>Songs, Videos, or Images</strong> at PurifyArt.com (Receive 95% of the profit from sales)</li>
            <li><strong>Fan Club Memberships</strong> offering exclusive media. (Receive 95% of the profit from sales)</li>
          </ul>
        </div>
        <div id="getPaid">
          <h4 style="float:none">Artist and  Company <br />
            Members <strong>Get Paid</strong> for:</h4>
          <ul>
            <li><strong>Sales through PurifyArt.com</strong> from media sales, Fan club Memberships, and donations (Service charge of $0*-$10 depending upon method of payment, allow 30 days)</li>
          </ul>
        </div>
      </div>
    </div>
    <table border="0" cellpadding="5" cellspacing="0" id="pricingTable">
      <tbody>
        <!--<tr style="border:none; background-color:#fff; margin:0; padding:0;height:2em;">
      <td style="height:2em;"></td>
      <td style="height:2em;"></td>
      <td style="height:2em;"></td>
      <td style="height:2em;"></td>
      <td style="height:2em;"></td>
      <td style="height:2em;"></td>
    </tr> -->
        <tr align="center" id="headerCell">
          <td width="194"></td>
          <td width="121" align="center">NON REGISTERED</td>
          <td width="122" align="center" class="pricingCells">GENERAL</td>
          <td width="123" align="center" class="pricingCells"> MEMBER</td>
          <td width="120" align="center" class="pricingCells"> ARTIST & COMPANY</td>
          <td width="170" align="center" class="pricingCells" style="border-right: none;"> ARTIST & COMAPNY MEMBERS</td>
        </tr>
        <tr style="background-color: rgba(255,255,255,1);">
          <td class="firstCol" style="text-align:left; font-weight:bold;">Price</td>
          <td align="center" class="pricingCells">Free</td>
          <td align="center" class="pricingCells">Free</td>
          <td align="center" class="pricingCells">$25/Year</td>
          <td align="center" class="pricingCells">Free</td>
          <td align="center" class="pricingCells" style="border-right: none;">$25/Year</td>
        </tr>
        <tr>
          <td class="firstCol" style="text-align:left; font-weight:bold;">Requirements</td>
          <td align="center" class="pricingCells">None</td>
          <td align="center" class="pricingCells">None</td>
          <td align="center" class="pricingCells">General Registration</td>
          <td align="center" class="pricingCells">Share 1 media file</td>
          <td align="center" class="pricingCells" style="border-right: none;">General Registration</td>
        </tr>
        <tr style="background-color: rgba(255,255,255,1);">
          <td class="firstCol" style="text-align:left; font-weight:bold;">View Profiles and Demo</td>
          <td align="center" class="pricingCells"><img src="images/round_bullet-pricingTable.png" width="12" height="12" alt="X" /></td>
          <td align="center" class="pricingCells"><img src="images/round_bullet-pricingTable.png" width="12" height="12" alt="X" /></td>
          <td align="center" class="pricingCells"><img src="images/round_bullet-pricingTable.png" width="12" height="12" alt="X" /></td>
          <td align="center" class="pricingCells"><img src="images/round_bullet-pricingTable.png" width="12" height="12" alt="X" /></td>
          <td align="center" class="pricingCells" style="border-right: none;"><img src="images/round_bullet-pricingTable.png" width="12" height="12" alt="X" /></td>
        </tr>
        <tr>
          <td class="firstCol" style="text-align:left; font-weight:bold;">Search Engine</td>
          <td align="center" class="pricingCells"><img src="images/round_bullet-pricingTable.png" width="12" height="12" alt="X" /></td>
          <td align="center" class="pricingCells"><img src="images/round_bullet-pricingTable.png" width="12" height="12" alt="X" /></td>
          <td align="center" class="pricingCells"><img src="images/round_bullet-pricingTable.png" width="12" height="12" alt="X" /></td>
          <td align="center" class="pricingCells"><img src="images/round_bullet-pricingTable.png" width="12" height="12" alt="X" /></td>
          <td align="center" class="pricingCells" style="border-right: none;"><img src="images/round_bullet-pricingTable.png" width="12" height="12" alt="X" /></td>
        </tr>
        <tr  style="background-color: rgba(255,255,255,1);">
          <td class="firstCol" style="text-align:left; font-weight:bold;">Download Free Media</td>
          <td align="center" class="pricingCells"><img src="images/round_bullet-pricingTable.png" width="12" height="12" alt="X" /></td>
          <td align="center" class="pricingCells"><img src="images/round_bullet-pricingTable.png" width="12" height="12" alt="X" /></td>
          <td align="center" class="pricingCells"><img src="images/round_bullet-pricingTable.png" width="12" height="12" alt="X" /></td>
          <td align="center" class="pricingCells"><img src="images/round_bullet-pricingTable.png" width="12" height="12" alt="X" /></td>
          <td align="center" class="pricingCells" style="border-right: none;"><img src="images/round_bullet-pricingTable.png" width="12" height="12" alt="X" /></td>
        </tr>
        <tr>
          <td class="firstCol" style="text-align:left; font-weight:bold;">Purchase Media</td>
          <td align="center" class="pricingCells"><img src="images/round_bullet-pricingTable.png" width="12" height="12" alt="X" /></td>
          <td align="center" class="pricingCells"><img src="images/round_bullet-pricingTable.png" width="12" height="12" alt="X" /></td>
          <td align="center" class="pricingCells"><img src="images/round_bullet-pricingTable.png" width="12" height="12" alt="X" /></td>
          <td align="center" class="pricingCells"><img src="images/round_bullet-pricingTable.png" width="12" height="12" alt="X" /></td>
          <td align="center" class="pricingCells" style="border-right: none;"><img src="images/round_bullet-pricingTable.png" width="12" height="12" alt="X" /></td>
        </tr>
        <tr  style="background-color: rgba(255,255,255,1);">
          <td class="firstCol" style="text-align:left; font-weight:bold;">Purchase Fan Club Memberships</td>
          <td align="center" class="pricingCells"></td>
          <td align="center" class="pricingCells"><img src="images/round_bullet-pricingTable.png" width="12" height="12" alt="X" /></td>
          <td align="center" class="pricingCells"><img src="images/round_bullet-pricingTable.png" width="12" height="12" alt="X" /></td>
          <td align="center" class="pricingCells"><img src="images/round_bullet-pricingTable.png" width="12" height="12" alt="X" /></td>
          <td align="center" class="pricingCells" style="border-right: none;"><img src="images/round_bullet-pricingTable.png" width="12" height="12" alt="X" /></td>
        </tr>
        <tr>
          <td class="firstCol" style="text-align:left; font-weight:bold;">View Media</td>
          <td align="center" class="pricingCells"></td>
          <td align="center" class="pricingCells"><img src="images/round_bullet-pricingTable.png" width="12" height="12" alt="X" /></td>
          <td align="center" class="pricingCells"><img src="images/round_bullet-pricingTable.png" width="12" height="12" alt="X" /></td>
          <td align="center" class="pricingCells"><img src="images/round_bullet-pricingTable.png" width="12" height="12" alt="X" /></td>
          <td align="center" class="pricingCells" style="border-right: none;"><img src="images/round_bullet-pricingTable.png" width="12" height="12" alt="X" /></td>
        </tr>
        <tr  style="background-color: rgba(255,255,255,1);">
          <td class="firstCol" style="text-align:left; font-weight:bold; width:251px;">Subscribe for wall and email updates</td>
          <td align="center" class="pricingCells"></td>
          <td align="center" class="pricingCells"><img src="images/round_bullet-pricingTable.png" width="12" height="12" alt="X" /></td>
          <td align="center" class="pricingCells"><img src="images/round_bullet-pricingTable.png" width="12" height="12" alt="X" /></td>
          <td align="center" class="pricingCells"><img src="images/round_bullet-pricingTable.png" width="12" height="12" alt="X" /></td>
          <td align="center" class="pricingCells" style="border-right: none;"><img src="images/round_bullet-pricingTable.png" width="12" height="12" alt="X" /></td>
        </tr>
        <tr>
          <td class="firstCol" style="text-align:left; font-weight:bold;">Register as Artist, Company, Team</td>
          <td align="center" class="pricingCells"></td>
          <td align="center" class="pricingCells"><img src="images/round_bullet-pricingTable.png" width="12" height="12" alt="X" /></td>
          <td align="center" class="pricingCells"><img src="images/round_bullet-pricingTable.png" width="12" height="12" alt="X" /></td>
          <td align="center" class="pricingCells"><img src="images/round_bullet-pricingTable.png" width="12" height="12" alt="X" /></td>
          <td align="center" class="pricingCells" style="border-right: none;"><img src="images/round_bullet-pricingTable.png" width="12" height="12" alt="X" /></td>
        </tr>
        <tr  style="background-color: rgba(255,255,255,1);">
          <td class="firstCol" style="text-align:left; font-weight:bold;">Create Playlists</td>
          <td align="center" class="pricingCells"></td>
          <td align="center" class="pricingCells"></td>
          <td align="center" class="pricingCells"><img src="images/round_bullet-pricingTable.png" width="12" height="12" alt="X" /></td>
          <td align="center" class="pricingCells"><img src="images/round_bullet-pricingTable.png" width="12" height="12" alt="X" /></td>
          <td align="center" class="pricingCells" style="border-right: none;"><img src="images/round_bullet-pricingTable.png" width="12" height="12" alt="X" /></td>
        </tr>
        <tr>
          <td class="firstCol" style="text-align:left; font-weight:bold;">Email Support</td>
          <td align="center" class="pricingCells"></td>
          <td align="center" class="pricingCells"></td>
          <td align="center" class="pricingCells"><img src="images/round_bullet-pricingTable.png" width="12" height="12" alt="X" /></td>
          <td align="center" class="pricingCells"><img src="images/round_bullet-pricingTable.png" width="12" height="12" alt="X" /></td>
          <td align="center" class="pricingCells" style="border-right: none;"><img src="images/round_bullet-pricingTable.png" width="12" height="12" alt="X" /></td>
        </tr>
        <tr style="background-color: rgba(255,255,255,1);">       
          <td class="firstCol" style="text-align:left; font-weight:bold;">Artist/Company Profile</td>
          <td align="center" class="pricingCells"></td>
          <td align="center" class="pricingCells"></td>
          <td align="center" class="pricingCells"></td>
          <td align="center" class="pricingCells"><img src="images/round_bullet-pricingTable.png" width="12" height="12" alt="X" /></td>
          <td align="center" class="pricingCells" style="border-right: none;"><img src="images/round_bullet-pricingTable.png" width="12" height="12" alt="X" /></td>
        </tr>
        <tr>
          <tr >
          <td class="firstCol" style="text-align:left; font-weight:bold;">Event and Project Profiles</td>
          <td align="center" class="pricingCells"></td>
          <td align="center" class="pricingCells"></td>
          <td align="center" class="pricingCells"></td>
          <td align="center" class="pricingCells"><img src="images/round_bullet-pricingTable.png" width="12" height="12" alt="X" /></td>
          <td align="center" class="pricingCells" style="border-right: none;"><img src="images/round_bullet-pricingTable.png" width="12" height="12" alt="X" /></td>
        </tr>
        <tr  style="background-color: rgba(255,255,255,1);">       
          <td class="firstCol" style="text-align:left; font-weight:bold;">Promote to subscribers & email list</td>
          <td align="center" class="pricingCells"></td>
          <td align="center" class="pricingCells"></td>
          <td align="center" class="pricingCells"></td>
          <td align="center" class="pricingCells"><img src="images/round_bullet-pricingTable.png" width="12" height="12" alt="X" /></td>
          <td align="center" class="pricingCells" style="border-right: none;"><img src="images/round_bullet-pricingTable.png" width="12" height="12" alt="X" /></td>
        </tr>
        <tr>
        <tr >        
          <td class="firstCol" style="text-align:left; font-weight:bold;">Facebook integration</td>
          <td align="center" class="pricingCells"></td>
          <td align="center" class="pricingCells"></td>
          <td align="center" class="pricingCells"></td>
          <td align="center" class="pricingCells"><img src="images/round_bullet-pricingTable.png" width="12" height="12" alt="X" /></td>
          <td align="center" class="pricingCells" style="border-right: none;"><img src="images/round_bullet-pricingTable.png" width="12" height="12" alt="X" /></td>
        </tr>
        
        <tr>
        <!--<tr style="background-color: rgba(255,255,255,1);">        
          <td class="firstCol">Add Amazon S3 account</td>
          <td align="center" class="pricingCells"></td>
          <td align="center" class="pricingCells"></td>
          <td align="center" class="pricingCells">$50/One Time</td>
          <td align="center" class="pricingCells"></td>
          <td align="center" class="pricingCells" style="border-right: none;">$50/One Time</td>
        </tr>-->
		<tr style="background-color: rgba(255,255,255,1);">
          <!--<td class="firstCol">Sell Music or High Res Image</td>-->
		  <td class="firstCol" style="text-align:left; font-weight:bold;">Sell Music</td>
          <td align="center" class="pricingCells"></td>
          <td align="center" class="pricingCells"></td>
          <td align="center" class="pricingCells"></td>
          <td align="center" class="pricingCells"></td>
          <td align="center" class="pricingCells" style="border-right: none;"><img src="images/round_bullet-pricingTable.png" width="12" height="12" alt="X" /></td>
		  </tr>
        </tr>
        <tr>
        <tr style="">        
          <td class="firstCol" style="text-align:left; font-weight:bold;">Sell Fan Club Memberships</td>
          <td align="center" class="pricingCells"></td>
          <td align="center" class="pricingCells"></td>
          <td align="center" class="pricingCells"></td>
          <td align="center" class="pricingCells"></td>
          <td align="center" class="pricingCells" style="border-right: none;"><img src="images/round_bullet-pricingTable.png" width="12" height="12" alt="X" /></td>
        </tr>
      </tbody>
    </table>
 
    <div class="clearMe"></div>
</div>  

</div>
<?php include_once("displayfooter.php"); ?>
</body>
</html>
