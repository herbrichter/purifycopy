<?php
	session_start();
	error_reporting(0);
	ob_start();
	include_once('commons/db.php');
	include_once('loggedin_includes.php');
	include_once('classes/ProfileType.php');
	include_once('classes/Commontabs.php');
	include_once('classes/CountryState.php');
	include_once('classes/SuggestSearch.php');
	//include('recaptchalib.php');
	include_once('classes/Media.php');
	include_once('classes/ProfileDisplay.php');
	include_once("AjaxButtonsTestCom.php");
	
	$display = new ProfileDisplay();
	$search_criteria = new SuggestSearch();
	$button_test_com = new AjaxButtonsTestCom();
	
	$down_button = "";
	if($_SESSION['login_email']!=NULL)
	{
		$down_button = $button_test_com->Community_project($_SESSION['login_id']);
	}
	//var_dump($_GET);
	//die;
	if($_GET['q_match']=='Search for Artists, Media, Events etc' || $_GET['q_match']=='Search%2520for%2520Artists,%2520Media,%2520Events%2520etc' || $_GET['q_match']=='Search%20for%20Artists,%20Media,%20Events%20etc')
	{
		$_GET['q_match'] = 'Search';
	}
	
	if(isset($_GET['type_dis']))
	{
		if($_GET['type_dis']!="")
		{
			$_POST['profile_type_search'] = $_GET['type_dis'];
			
			if($_GET['pritype_dis']!="")
			{
				$_POST['select-category-artist'] = $_GET['pritype_dis'];				
			}
			if($_GET['subtype_dis']!="")
			{
				$_POST['select-category-artist-subtype'] = $_GET['subtype_dis'];
			}
		}
		$_GET['q_match'] = 'Search';
		$_GET['q_table'] = "";
		$_GET['q_id'] = "";
	}
	
	
	if(isset($_GET['tab_name']))
	{
		$_GET['q_match'] = 'Search';
		$_GET['q_table'] = "";
		$_GET['q_id'] = "";
		
		if($_GET['tab_name']!="")
		{
			if($_GET['tab_name']!='recent' && $_GET['tab_name']!='popular')
			{
				if($_GET['tab_name']=='companie')
				{
					$_POST['profile_type_search'] = 'community';
				}
				elseif($_GET['tab_name']=='song' || $_GET['tab_name']=='video' || $_GET['tab_name']=='gallery' || $_GET['tab_name']=='playlist')
				{
					$_POST['profile_type_search'] = 'media';
					if($_GET['tab_name']=='song')
					{
						$_POST['select-category-artist'] = '114';
					}
					elseif($_GET['tab_name']=='video')
					{
						$_POST['select-category-artist'] = '115';
					}
					elseif($_GET['tab_name']=='gallery')
					{
						$_POST['select-category-artist'] = '113';
					}
					elseif($_GET['tab_name']=='playlist')
					{
						$_POST['select-category-artist'] = '116';
					}
				}
				else
				{
					$_POST['profile_type_search'] = $_GET['tab_name'];
				}
			}
		}
	}
	
$objMedia=new Media();	
if(isset($_REQUEST['username']))
{
	if($_REQUEST['username'])
	{
		$username=$_REQUEST['username'];
		$userpass=$_REQUEST['userpass'];
		$flag=$objMedia->checkUserLogin($username,$userpass);
		if($flag !="")
		{
			$_SESSION['userInfo'] = $username;
			$_SESSION['gen_user_id'] = $flag['general_user_id'];
			$_SESSION['login_email']=$username;
			$_SESSION['login_id']=$flag['general_user_id'];
			$_SESSION['name']=$flag['fname'].$flag['lname'];
			$_SESSION['artist_id']=$flag['artist_id'];
			$_SESSION['community_id']=$flag['community_id'];
			$_SESSION['member_id']=$flag['member_id'];
			$_SESSION['team_id']=$flag['team_id'];
			$logedinFlag=TRUE;
		}	
	}
}

if(isset($_SESSION['userInfo']))
{
	$logedinFlag = TRUE;
}

if(isset($_SESSION['login_email']))
{
	$_SESSION['gen_user_id'] = $_SESSION['login_id'];
	$logedinFlag = TRUE;
}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Purify Art</title>
<!--<link rel="shortcut icon" href="" />-->
<link rel="stylesheet" type="text/css" media="screen" href="includes/purify.css" />
<script src="http://jqueryjs.googlecode.com/files/jquery-1.2.6.min.js" type="text/javascript"></script>
<script src="includes/popup.js" type="text/javascript"></script>

<?php 
	$newtab=new Commontabs();
	include_once("includes/header.php");
	//var_dump($_POST);
?>
<!--<div id="loaders" style="margin: 0 auto; overflow: hidden; width: 940px; text-align: center;">
	<img src="images/ajax_loader_large.gif" style="height: 35px; margin: 70px; position: relative;">
</div>-->
<div id="outerContainer" style="display: none;">

<script type="text/javascript">
function newfunction(id)
{
	//var image = document.getElementById("play_video_image"+id);
	//image.src="/images/profile/play_hover.png";
	$("#play_video_image"+id).css({"background":"none repeat scroll 0 0 #000000","border-radius":"6px 6px 6px 6px"});
}function newfunctionout(id)
{
	//var image = document.getElementById("play_video_image"+id);
	//image.src="/images/profile/play_youtube.png";
	$("#play_video_image"+id).css({"background":"none repeat scroll 0 0 #333333","border-radius":"6px 6px 6px 6px"});
}
</script>
<script type="text/javascript">
$(document).ready(function(){	
	
	///////////////////////////////////////////State changes Starts///////////////////////////////////
	<?php
		if(isset($_POST) && !empty($_POST) && $_POST['stateSelect']!='' && $_GET['q_match']=='Search')
		{
	?>
			//alert($("#countrySelect_artist").val());
			$.post("state_search.php", { country_id:$("#countrySelect").val(),state_ids:<?php echo $_POST['stateSelect']; ?> },
				function(data)
				{
					//alert("Data Loaded: " + data);											
					$("#stateSelect").html(data);
				}
			);
	<?php
		}
		else
		{
	?>
			$("#countrySelect").change(
			function ()
			{
				//alert($("#countrySelect_artist").val());
				$.post("state_search.php", { country_id:$("#countrySelect").val() },
					function(data)
					{
					//alert("Data Loaded: " + data);											
						$("#stateSelect").html(data);
					}
				);
			});
	<?php
		}
	?>
	
	$("#countrySelect").change(
		function ()
		{
			//alert($("#countrySelect_artist").val());
			$.post("state_search.php", { country_id:$("#countrySelect").val() },
				function(data)
				{
				//alert("Data Loaded: " + data);											
					$("#stateSelect").html(data);
				}
			);
		});
	///////////////////////////////////////////State changes Ends///////////////////////////////////

	//////////////////////////////////////////Sub-Type Changes Starts///////////////////////////
	<?php
		if(isset($_POST) && !empty($_POST) && isset($_POST['select-category-artist-subtype']) && $_POST['select-category-artist-subtype']!='' && $_GET['q_match']=='Search')
		{
	?>
			//alert($("#type-select").val());
			$.post("search_registration_block_subtype.php", { type_id:<?php echo $_POST['select-category-artist']; ?>,subtype_ids:<?php echo $_POST['select-category-artist-subtype']; ?> },
				function(data)
				{
					//alert("Data Loaded: " + data);
					//$("#metatype-select").html("<option value='0' selected='selected'>Select Metatype</option>");
					//$("#subtype-select").html(data);
					if(data == " " || data == null || data == "")
					{
						document.getElementById("subtype-select").disabled=true;
					}else{
						document.getElementById("subtype-select").disabled=false;
						$("#subtype-select").html(data);
					}
				}
			);
	<?php
		}
		else
		{
	?>
			$("#type-select").change(
				function ()
				{
					//alert($("#type-select").val());
					$.post("search_registration_block_subtype.php", { type_id:$("#type-select").val() },
					function(data)
					{
						//alert("Data Loaded: " + data);
						//$("#metatype-select").html("<option value='0' selected='selected'>Select Metatype</option>");
						//$("#subtype-select").html(data);
						if(data == " " || data == null || data == "" || $("#type-select").val() ==0)
						{
							document.getElementById("subtype-select").disabled=true;
							$("#subtype-select").html("");
						}else{
							document.getElementById("subtype-select").disabled=false;
							$("#subtype-select").html(data);
						}
					}
					);
				}
			);

	<?php
		}
	?>
		
	$("#type-select").change(
		function ()
		{
			//alert($("#type-select").val());
			$.post("search_registration_block_subtype.php", { type_id:$("#type-select").val() },
			function(data)
			{
				//alert("Data Loaded: " + data);
				//$("#metatype-select").html("<option value='0' selected='selected'>Select Metatype</option>");
				//$("#subtype-select").html(data);
				
				if(data == " " || data == null || data == "" || $("#type-select").val() ==0)
				{
					document.getElementById("subtype-select").disabled=true;
					$("#subtype-select").html("");
				}else{
					document.getElementById("subtype-select").disabled=false;
					$("#subtype-select").html(data);
				}
			}
			);
		}
	);
		
	//////////////////////////////////////////Sub-Type Changes Ends///////////////////////////
	
	
	//////////////////////////////////////////Meta-Type Changes Starts///////////////////////////
	<?php
		if(isset($_POST) && !empty($_POST) && isset($_POST['select-category-artist-metatype']) && $_POST['select-category-artist-metatype']!='' && $_GET['q_match']=='Search')
		{
	?>
			//alert($("#subtype-select").val());
			$.post("search_registration_block_metatype.php", { subtype_id:<?php echo $_POST['select-category-artist-subtype']; ?>,metatypes_id:<?php echo $_POST['select-category-artist-metatype']; ?> },
				function(data)
				{
					//alert("Data Loaded: " + data);
					//$("#metatype-select").html(data);
					if(data == " " || data == null || data == "")
					{
						document.getElementById("metatype-select").disabled=true;
					}else{
						document.getElementById("metatype-select").disabled=false;
						$("#metatype-select").html(data);
					}
				});
	<?php
		}
		else
		{
	?>
			$("#subtype-select").change(
				function ()
				{
					//alert($("#subtype-select").val());
					$.post("search_registration_block_metatype.php", { subtype_id:$("#subtype-select").val() },
					function(data)
					{
						//alert("Data Loaded: " + data);
						//$("#metatype-select").html(data);
						if(data == " " || data == null || data == "" || data == "Select Metatype" || $("#subtype-select").val() ==0)
						{
							document.getElementById("metatype-select").disabled=true;
							$("#metatype-select").html("");
						}else{
							document.getElementById("metatype-select").disabled=false;
							$("#metatype-select").html(data);
						}
					}
					);
				}	
			);
	<?php
		}
	?>
	
	$("#subtype-select").change(
		function ()
		{
			//alert($("#subtype-select").val());
			$.post("search_registration_block_metatype.php", { subtype_id:$("#subtype-select").val() },
			function(data)
			{
				//alert("Data Loaded: " + data);
				//$("#metatype-select").html(data);
				if(data == " " || data == null || data == "" || data == "Select Metatype" || $("#subtype-select").val() ==0)
				{
					document.getElementById("metatype-select").disabled=true;
					$("#metatype-select").html("");
				}else{
					document.getElementById("metatype-select").disabled=false;
					$("#metatype-select").html(data);
				}
			});
		}	
	);
	//////////////////////////////////////////Meta-Type Changes Ends///////////////////////////
	
	////////////////////////////////////////Type Changes Starts/////////////////////////////////////////////
	<?php	
		if(isset($_POST) && !empty($_POST) && isset($_POST['select-category-artist']) && $_POST['select-category-artist']!='' && $_GET['q_match']=='Search')
		{
	?>
			/*$.ajax({
			type: "POST",
			url: 'search_type.php',
			data: { "profile_type":$("#profile_type_search").val(), "type_ids":<?php echo $_POST['select-category-artist']; ?> },
			success: function(data){
				$("#type-select").html(data);
			}
		});*/
	
	
			$.post("search_type.php", { profile_type:$("#profile_type_search").val(),type_ids:<?php echo $_POST['select-category-artist']; ?> },
				function(data)
				{
					$("#type-select").html(data);
					document.getElementById("type-select").disabled=false;
				}
			);
	<?php
		}
		else
		{
	?>
			$("#profile_type_search").change(function ()
			{
				$.post("search_type.php", { profile_type:$("#profile_type_search").val() },
					function(data)
					{
						document.getElementById("type-select").disabled=false;
						$("#type-select").html(data);
					}
				);
			});
	<?php
		}
	?>
		
		
			$("#profile_type_search").change(function ()
			{
				//alert("hii");
				if($("#profile_type_search").val()=='select' && $("#type-select").val()!=0)
				{
				}
				else
				{
					$.post("search_type.php", { profile_type:$("#profile_type_search").val() },
						function(data)
						{
							document.getElementById("type-select").disabled=false;
							document.getElementById("subtype-select").disabled=true;
							document.getElementById("metatype-select").disabled=true;
							//$("#subtype-select").html("<option value='0' selected='selected'>Select Subtype</option>");
							//$("#metatype-select").html("<option value='0' selected='selected'>Select Metatype</option>");
							$("#type-select").html(data);
							$("#subtype-select").html("");
							$("#metatype-select").html("");
						}
					);
				}
			});
	/////////////////////////////////Type Changes Ends//////////////////////////////////
	
	});
/*function down_song_pop(id,name,creator)
{
	var val = confirm("would you like to donate :"+name+" ("+creator+")");
	if(val== true){
		var Dvideolink = document.getElementById("download_song"+id);
		Dvideolink.click();
	}
	else{
		window.location = "zipes_1.php?download_meds="+id;
	}
}
function down_video_pop(id,name,creator)
{
	var val = confirm("would you like to donate :"+name+" ("+creator+")");
	if(val== true){
		var Dvideolink = document.getElementById("download_video"+id);
		Dvideolink.click();
	}
	else{
		window.location = "zipes_1.php?download_meds="+id;
	}
}*/
function down_song_pop(id)
{
	var Dvideolink = document.getElementById("download_song"+id);
	Dvideolink.click();
	setTimeout(function(){
		jQuery.fancybox.close();
		$("#showcart").click();
	},1000);
}
function down_video_pop(id)
{
	var Dvideolink = document.getElementById("download_video"+id);
	Dvideolink.click();
}
</script>
				<!--<p><a href="../logout.php">Logout</a> </p>
			</div>
		</div>
	</div> 
</div>-->
  <div id="toplevelNav"></div>
  <div>
  		<div id="mediaTab">
            <!--<h1>Advance Search</h1>-->
                <form method="POST" name="serach_form" id="search_form">
					<div id="searchContent">
						<div class="fieldCont">
							<div title="Select the type of categories of users, profiles, and media you would like to search for." class="fieldTitle">Profile Type </div>
							<?php
								
								if(isset($_POST) && !empty($_POST) && $_POST['profile_type_search']!='select' && $_POST['profile_type_search']!='' && $_GET['q_match']=='Search')
								{
									if($_POST['profile_type_search'] == 'artist')
									{
							?>
										<select title="Select the type of categories of users, profiles, and media you would like to search for." class="dropdown" id="profile_type_search" name="profile_type_search">
											<option value="select">Select</option>  
											<option value="artist" selected>Artists</option>
											<option value="community">Companies</option>
											<option value="event">Events</option>
											<option value="media">Media</option>
											<option value="project">Projects</option>
											<!--<option value="services">Services</option>-->
										</select>
							<?php
									}
									elseif($_POST['profile_type_search'] == 'community')
									{
							?>
										<select title="Select the type of categories of users, profiles, and media you would like to search for." class="dropdown" id="profile_type_search" name="profile_type_search">
											<option value="select">Select</option>  
											<option value="artist">Artists</option>
											<option value="community" selected>Companies</option>
											<option value="event">Events</option>
											<option value="media">Media</option>
											<option value="project">Projects</option>
											<!--<option value="services">Services</option>-->
										</select>
							<?php
									}
									elseif($_POST['profile_type_search'] == 'event')
									{
							?>
										<select title="Select the type of categories of users, profiles, and media you would like to search for." class="dropdown" id="profile_type_search" name="profile_type_search">
											<option value="select">Select</option>  
											<option value="artist">Artists</option>
											<option value="community">Companies</option>
											<option value="event" selected>Events</option>
											<option value="media">Media</option>
											<option value="project">Projects</option>
											<!--<option value="services">Services</option>-->
										</select>
							<?php
									}
									elseif($_POST['profile_type_search'] == 'media')
									{
							?>
										<select title="Select the type of categories of users, profiles, and media you would like to search for." class="dropdown" id="profile_type_search" name="profile_type_search">
											<option value="select">Select</option>  
											<option value="artist">Artists</option>
											<option value="community">Companies</option>
											<option value="event">Events</option>
											<option value="media" selected>Media</option>
											<option value="project">Projects</option>
											<!--<option value="services">Services</option>-->
										</select>
							<?php
									}
									elseif($_POST['profile_type_search'] == 'project')
									{
							?>
										<select title="Select the type of categories of users, profiles, and media you would like to search for." class="dropdown" id="profile_type_search" name="profile_type_search">
											<option value="select">Select</option>  
											<option value="artist">Artists</option>
											<option value="community">Companies</option>
											<option value="event">Events</option>
											<option value="media">Media</option>
											<option value="project" selected>Projects</option>
											<!--<option value="services">Services</option>-->
										</select>
							<?php
									}
									elseif($_POST['profile_type_search'] == 'services')
									{
							?>
										<select title="Select the type of categories of users, profiles, and media you would like to search for." class="dropdown" id="profile_type_search" name="profile_type_search">
											<option value="select">Select</option>  
											<option  value="artist">Artists</option>
											<option value="community">Companies</option>
											<option value="event">Events</option>
											<option value="media">Media</option>
											<option value="project">Projects</option>
											<!--<option value="services" selected>Services</option>-->
										</select>
							<?php
									}
								}
								else
								{
									if($_GET['q_match']=='Search' && empty($_POST) && $_GET['tab_name']!='recent' && $_GET['tab_name']!='popular')
									{
							?>
										<select title="Select the type of categories of users, profiles, and media you would like to search for." class="dropdown" id="profile_type_search" name="profile_type_search">
											<option value="select">Select</option>  
											<option value="artist">Artists</option>
											<option value="community">Companies</option>
											<option value="event">Events</option>
											<option value="media">Media</option>
											<option value="project">Projects</option>
											<!--<option value="services">Services</option>-->
										</select>
							<?php
									}
									else
									{
							?>
										<select title="Select the type of categories of users, profiles, and media you would like to search for." class="dropdown" id="profile_type_search" name="profile_type_search">
											<option value="select">Select</option>  
											<option value="artist">Artists</option>
											<option value="community">Companies</option>
											<option value="event">Events</option>
											<option value="media">Media</option>
											<option value="project">Projects</option>
											<!--<option value="services">Services</option>-->
										</select>
							<?php
									}
								}
							?>
							<script>
								/* $(document).ready(function(){			
									$("#profile_type_search").change();
								}); */
							</script>
						</div>
						<div class="nav" id="index_navs">
							<ul>
							<?php
								if(isset($_GET['tab_name']))
								{
									if($_GET['tab_name']=='recent')
									{
							?>
										<li><a href="javascript:void(0);" onclick="change_Viewby('popular');" id="populars" >Popular</a></li>
										<li><a href="javascript:void(0);" onclick="change_Viewby('recent');" style="background:#e8e8e8;" id="recents" >Recent</a></li>
							<?php
									}
									elseif($_GET['tab_name']=='popular')
									{
							?>
										<li><a href="javascript:void(0);" onclick="change_Viewby('popular');" style="background:#e8e8e8;" id="populars" >Popular</a></li>
										<li><a href="javascript:void(0);" onclick="change_Viewby('recent');"  id="recents" >Recent</a></li>
							<?php		
									}
									else
									{
							?>
										<li><a href="javascript:void(0);" onclick="change_Viewby('popular');" style="background:#e8e8e8;" id="populars" >Popular</a></li>
										<li><a href="javascript:void(0);" onclick="change_Viewby('recent');"  id="recents" >Recent</a></li>
							<?php		
									}
								}
								else
								{
							?>
									<li><a href="javascript:void(0);" onclick="change_Viewby('popular');" style="background:#e8e8e8;" id="populars" >Popular</a></li>
									<li><a href="javascript:void(0);" onclick="change_Viewby('recent');"  id="recents" >Recent</a></li>
							<?php		
								}
							?>
							</ul>
						</div>
					</div>
					<div id="searchContent">
						<div class="fieldCont">
							<div title="Select the type of categories of users, profiles, and media you would like to search for." class="fieldTitle">Sub Type</div>
							<?php
								if(isset($_POST) && !empty($_POST) && isset($_POST['select-category-artist']) && $_POST['select-category-artist']!='' && $_GET['q_match']=='Search')
								{
							?>
									<select title="Select the type of categories of users, profiles, and media you would like to search for." name="select-category-artist" class="dropdown" id="type-select"  disabled>
										<!--<option value="0">Select Type</option>-->
									</select>
							<?php
								}
								else
								{
							?>
									<select title="Select the type of categories of users, profiles, and media you would like to search for." name="select-category-artist" class="dropdown" id="type-select" disabled>
										<!--<option value="0" selected="selected">Select Type</option>-->
									</select>
							<?php
								}
							?>
						</div>
						<div class="fieldCont">
							<div title="Select the type of categories of users, profiles, and media you would like to search for." class="fieldTitle"></div>
							<?php
								if(isset($_POST) && !empty($_POST) && isset($_POST['select-category-artist-subtype']) && $_POST['select-category-artist-subtype']!='' && $_GET['q_match']=='Search')
								{
							?>
									<select title="Select the type of categories of users, profiles, and media you would like to search for." name="select-category-artist-subtype" class="dropdown" id="subtype-select" disabled>
										<!--<option value="0">Select Subtype</option>-->
									</select>
							<?php
								}
								else
								{
							?>
									<select title="Select the type of categories of users, profiles, and media you would like to search for." name="select-category-artist-subtype" class="dropdown" id="subtype-select" disabled>
										<!--<option value="0" selected="selected">Select Subtype</option>-->
									</select>
							<?php
								}
							?>
						</div>
						<div class="fieldCont">
							<div title="Select the type of categories of users, profiles, and media you would like to search for." class="fieldTitle"></div>
								<select title="Select the type of categories of users, profiles, and media you would like to search for." name="select-category-artist-metatype" class="dropdown" id="metatype-select" disabled>
									<!--<option value="0" selected="selected">Select Metatype</option>-->
								</select>
						</div>
					</div>
					<div id="searchContent">
						<div class="fieldCont">
							<div title="Select the location you would like to search for users, profiles, and media in." class="fieldTitle">Country</div>
							<?php
								if(isset($_POST) && !empty($_POST) && $_GET['q_match']=='Search')
								{
							?>
									<select title="Select the location you would like to search for users, profiles, and media in." id='countrySelect' name='countrySelect'  class="dropdown">
											<option value="0">Select Country</option>					

									  <?php include('country_search.php'); ?>
									</select>
							<?php
								}
								else
								{
							?>
									<select title="Select the location you would like to search for users, profiles, and media in." id='countrySelect' name='countrySelect'  class="dropdown">
											<option value="0" selected="selected">Select Country</option>					

									  <?php include('country_search.php'); ?>
									</select>
							<?php
								}
							?>
						</div>
						<div class="fieldCont">
							<div title="Select the location you would like to search for users, profiles, and media in." class="fieldTitle">State/Province</div>
							<select title="Select the location you would like to search for users, profiles, and media in." id='stateSelect' name='stateSelect' class="dropdown">
							</select>
						</div>
						<div class="fieldCont">
							<div title="Select the location you would like to search for users, profiles, and media in." class="fieldTitle">City</div>
							<?php
								if(isset($_POST) && !empty($_POST) && $_POST['city_search']!='' && $_GET['q_match']=='Search')
								{
							?>
									<input title="Select the location you would like to search for users, profiles, and media in." id="city_search" name="city_search" type="text" class="fieldText" value="<?php echo $_POST['city_search']; ?>" />
							<?php
								}
								else
								{
							?>
									<input title="Select the location you would like to search for users, profiles, and media in." id="city_search" name="city_search" type="text" class="fieldText" />
							<?php
								}
							?>
						</div>
						<div class="buttonCont">
							<input style="border-radius:5px;" type="button" value="Search" class="button" onClick="sendString()" />
							<!--<input type="button" value="Cancel" />-->
						</div>
					</div>
					
					<div id="popupContact">
						<a id="popupContactClose">x</a>
						<p id="contactArea">
							Only registered users can view media. 
						</p>
						<h1>Register <a href="index.php">Here</a>, It's Free!</h1><br />
						<p>Already Registered ? Login below.</p>
							<div class="formCont">
								<div class="formFieldCont">
								<div class="fieldTitle">Username</div>
								<input type="text" class="textfield" name="username" />
							</div>
								<div class="formFieldCont">
								<div class="fieldTitle">Password</div>
								<input type="password" class="textfield" name="userpass" />
							</div>
								<div class="formFieldCont">
								<div class="fieldTitle"></div>
								<input type="image" class="login" src="../images/profile/login.png" width="47" height="23" />
							</div>
						</div>
					</div>
				</form>
		</div>
         
		<?php
		//var_dump($_POST);
			if(!isset($_GET['alpha']))
			{
				//echo "in 1";
				//$_GET['alpha'] = "";
				$alphs = "";
			}
			
			if(!isset($_GET['q_match']))
			{
				//$_GET['q_match'] = "Search";
				$q_match = "Search";
			}
			elseif(isset($_GET['q_match']))
			{
				$q_match = mysql_real_escape_string($_GET['q_match']);
			}
			if(!isset($_GET['q_table']))
			{
				$_GET['q_table'] = "";
			}
			if(!isset($_GET['q_id']))
			{
				$_GET['q_id'] = "";
			}

			if($_GET['q_match']=='Search' && empty($_POST) && $_GET['tab_name']!='recent' && $_GET['tab_name']!='popular')
			{
				//$_POST['profile_type_search'] = 'artist';
				$_POST['profile_type_search'] = 'select';
				$_GET['tab_name'] = 'popular';
				$_POST['select-category-artist'] = 0;
				$_POST['select-category-artist-subtype'] = 0;
				$_POST['select-category-artist-metatype'] = 0;
				$_POST['countrySelect'] = 0;
				$_POST['stateSelect'] = 0;
				$_POST['city_search'] = '';
				//echo "its not here";
			}
			if(isset($_GET['q_match']) && $_GET['q_match']!='Search' && isset($_GET['q_table']) && $_GET['q_table']=="" && isset($_GET['q_id']) && $_GET['q_id']=="")
			{
				//echo "in 2";
				$alphs = mysql_real_escape_string($_GET['q_match']);
				$alphs =str_replace("%20"," ",$alphs);
				$q_match = "Search";
				$_POST['profile_type_search'] = 'select';
				$_POST['select-category-artist'] = 0;
				$_POST['select-category-artist-subtype'] = 0;
				$_POST['select-category-artist-metatype'] = 0;
				$_POST['countrySelect'] = 0;
				$_POST['stateSelect'] = 0;
				$_POST['city_search'] = '';
			}
			if(isset($_GET['q_match']) && $_GET['q_match']!='Search' && isset($_GET['q_table']) && $_GET['q_table']!="" && isset($_GET['q_id']) && $_GET['q_id']!="")
			{
				$_POST['profile_type_search'] = 'select';
				$_POST['select-category-artist'] = 0;
				$_POST['select-category-artist-subtype'] = 0;
				$_POST['select-category-artist-metatype'] = 0;
				$_POST['countrySelect'] = 0;
				$_POST['stateSelect'] = 0;
				$_POST['city_search'] = '';
				//echo "in 3";
			}
			if(isset($_GET['alpha']))
			{
				//echo "in 4";
				$alphs = $_GET['alpha'];
				$_POST['profile_type_search'] = 'select';
				$_POST['select-category-artist'] = 0;
				$_POST['select-category-artist-subtype'] = 0;
				$_POST['select-category-artist-metatype'] = 0;
				$_POST['countrySelect'] = 0;
				$_POST['stateSelect'] = 0;
				$_POST['city_search'] = '';
			}
			if((isset($_POST) && $_POST!=null) || $q_match!='Search' || isset($alphs)) 
			{
				//var_dump($_POST);
				//var_dump($_GET['q_match']);
				
				//var_dump($_POST['profile_type_search']);
				//echo $alphs;
				//echo $q_match;
				
				// var_dump($_POST['select-category-artist-subtype']);
				
				// var_dump($_POST['select-category-artist-metatype']);
				
				// var_dump($_POST['select-category-artist']);
				
				// var_dump($_POST['stateSelect']);
				
				// var_dump($_POST['countrySelect']);
				
				// var_dump($_POST['city_search']);
				
				// die;
				if($_POST['stateSelect']==0 || $_POST['stateSelect']=="")
				{
					$_POST['stateSelect'] = 0;
				}
				if($_POST['select-category-artist']==0 || $_POST['select-category-artist']=="")
				{
					$_POST['select-category-artist'] = 0;
				}
				if($_GET['tab_name']!='recent' && $_GET['tab_name']!='popular')
				{
					$_GET['tab_name'] = '';
				}
				
				if($_POST['profile_type_search']!='select' || $_POST['select-category-artist']!=0 || $_POST['select-category-artist-subtype']!=0 || $_POST['select-category-artist-metatype']!=0 || $_POST['countrySelect']!=0 || $_POST['stateSelect']!=0 || $_POST['city_search']!='' || $q_match!="Search" || $alphs!="" || $_GET['tab_name']=='recent' || $_GET['tab_name']=='popular')
				{
					
					$profile_search = $search_criteria->searching($_POST['profile_type_search'],$_POST['select-category-artist'],$_POST['select-category-artist-subtype'],$_POST['select-category-artist-metatype'],$_POST['countrySelect'],$_POST['stateSelect'],$_POST['city_search'],$q_match,$_GET['q_table'],$_GET['q_id'],$alphs,$_GET['tab_name']);
					
				/* 	var_dump($_POST['profile_type_search']);
					var_dump($_POST['select-category-artist']);
					var_dump($_POST['select-category-artist-subtype']);
					var_dump($_POST['select-category-artist-metatype']);
					var_dump($_POST['countrySelect']);
					var_dump($_POST['stateSelect']);
					var_dump($_POST['city_search']);
					var_dump($q_match);
					var_dump($_GET['q_table']);
					var_dump($_GET['q_id']); */
					//var_dump($profile_search);
					//var_dump($_POST['countrySelect']);
					//echo count($profile_search);
					//echo $_GET['alpha'];
					//var_dump($profile_search);
					//var_dump($_POST['select-category-artist']);
					
					//die;
				}
				else
				{
					?>
						<script>
							alert("Please Enter some criteria for Search.");
						</script>
					<?php
				}
		?>
				<input type="hidden" value="19" id="scroll_values" name="scroll_values" />
				<input type="hidden" value="<?php echo count($profile_search); ?>" id="data_scroll_values" name="data_scroll_values" />
				<input type="hidden" value="<?php if(isset($_GET['tab_name']) && !empty($_GET['tab_name'])){ echo $_GET['tab_name']; }else{ echo "popular"; }?>" id="type_scroll_values" name="type_scroll_values" />
				
				<div id="mediaTab" style="border-top:1px solid #cbcaca; padding-top:20px;">
					<div id="loader_type" style="display:none; text-align:center;">
						<img src="images/ajax_loader_large.gif" style="height: 35px; margin-bottom: 33px; margin-top: 31px; position: relative; width: 35px;">
					</div>
					<div class="rowItem" id="change_type_view">
								<script>
									$("document").ready(function() {								
										$("#loaders_end_data").css({"display":"none"});
										$("#loaders_end").css({"display":"block"});
										
										var type_view = document.getElementById("type_scroll_values").value;
										
										$.ajax({
											type: "POST",
											url: 'getdata_scroll.php',
											data: { "view_type":type_view, "profile_type_search":'<?php echo $_POST['profile_type_search'] ?>', "select-category-artist":'<?php echo $_POST['select-category-artist'] ?>', "select-category-artist-subtype":'<?php echo $_POST['select-category-artist-subtype'] ?>', "select-category-artist-metatype":'<?php echo $_POST['select-category-artist-metatype'] ?>', "countrySelect":'<?php echo $_POST['countrySelect'] ?>', "stateSelect":'<?php echo $_POST['stateSelect'] ?>', "city_search":'<?php echo $_POST['city_search'] ?>', "q_match":'<?php echo $q_match ?>', "q_table":'<?php echo $_GET['q_table'] ?>', "q_id":'<?php echo $_GET['q_id'] ?>', "alphs":'<?php echo $alphs ?>', "tab_name":'<?php echo $_GET['tab_name'] ?>', "counts_scroll":19 },
											success: function(data){
												//$("#change_type_view").html(data);
												//$("#loader_type").hide();
												//$("#change_type_view").show();
												//document.getElementById("change_type_view").innerHTML = data;
												$("#change_type_view").html(data);
												$("#loaders_end").css({"display":"none"});
											}
										});
										
										$(window).scroll(function(){ /* window on scroll run the function using jquery and ajax */
																			
											var WindowHeight = $(window).height(); /* get the window height */
											if($(window).scrollTop() +1 >= $(document).height() - WindowHeight){ /* check is that user scrolls down to the bottom of the page */
											$("#loaders_end").css({"display":"block"});
											var chng_scr_vals = parseInt(document.getElementById("scroll_values").value);
											var chng_data_vals = parseInt(document.getElementById("data_scroll_values").value);
											
											var chng_scr_vals_i = document.getElementById("scroll_values").value;
											var chng_data_vals_i = document.getElementById("data_scroll_values").value;
											
											var type_view = document.getElementById("type_scroll_values").value;
												 /* create a variable that containing the url parameters which want to post to getdata.php file */
												if(chng_scr_vals_i!='NOMORE')
												{
													$("#loaders_end_data").css({"display":"none"});
													
													$.ajax({ /* post the values using AJAX */
													type: "POST",
													url: "getdata_scroll.php",
													data: { "view_type":type_view, "profile_type_search":'<?php echo $_POST['profile_type_search'] ?>', "select-category-artist":'<?php echo $_POST['select-category-artist'] ?>', "select-category-artist-subtype":'<?php echo $_POST['select-category-artist-subtype'] ?>', "select-category-artist-metatype":'<?php echo $_POST['select-category-artist-metatype'] ?>', "countrySelect":'<?php echo $_POST['countrySelect'] ?>', "stateSelect":'<?php echo $_POST['stateSelect'] ?>', "city_search":'<?php echo $_POST['city_search'] ?>', "q_match":'<?php echo $q_match ?>', "q_table":'<?php echo $_GET['q_table'] ?>', "q_id":'<?php echo $_GET['q_id'] ?>', "alphs":'<?php echo $alphs ?>', "tab_name":'<?php echo $_GET['tab_name'] ?>', "counts_scroll":chng_scr_vals + 20 },
													cache: false,
														success: function(data){
															if(chng_data_vals>chng_scr_vals)
															{
																//document.getElementById("change_type_view").innerHTML = data;
																$("#change_type_view").html(data);
																$("#loaders_end").css({"display":"none"});
															
																document.getElementById("scroll_values").value = chng_scr_vals + 20;
															}
															else
															{
																//document.getElementById("change_type_view").innerHTML = data;
																$("#change_type_view").html(data);
																document.getElementById("scroll_values").value = 'NOMORE';
																$("#loaders_end").css({"display":"none"});
															}
														}
													});
												}
												else
												{
													$("#loaders_end").css({"display":"none"});
													$("#loaders_end_data").css({"display":"block"});
												}
											}
										});
									});
								</script>
			<?php
							
		?>
						
				</div>
				</div>
		<?php
			}
		?>
          
   </div>
</div>
<div style="margin: 0px auto; overflow: hidden; width: 940px; text-align: center; display:none;" id="loaders_end">
	<img style="height: 35px; margin: 70px; position: relative;" src="images/ajax_loader_large.gif">
</div>
<div id="loaders_end_data" style="margin: 0px auto; overflow: hidden; width: 940px; text-align: center; display: none;">
	<p style="font-weight: bold;"></p>
</div>
  <?php //include_once("footer.php"); 
  include_once("displayfooter.php"); ?>
  <a href="asktodonate.php" class="fancybox fancybox.ajax" id="get_donationpop" style="display:none">fs</a>
  <a href="asktodonate_pro.php" class="fancybox fancybox.ajax" id="get_donationpop_alpro" style="display:none">fs</a>
  <a id="ask_name_alemail" class="fancybox fancybox.ajax" style="display:none" name="ask_name_alemail" href="ask_name_aftr_don.php">ask</a>
  <a id="ask_name_email_single" style="display:none" class="fancybox fancybox.ajax" name="ask_name_email_single" href="ask_single_name_aftr_don.php">ask</a>
  <a href="handle_login.php" style="display:none;" class="fancybox fancybox.ajax" id="logged_chk_user"></a>
</div>
</div> 
<link href="../galleryfiles/gallery.css" rel="stylesheet"/>
<script src="../galleryfiles/jquery.easing.1.3.js"></script>
<script src="../includes/functionsmedia.js" type="text/javascript"></script>
<script src="../includes/functionsURL.js" type="text/javascript"></script>
<script src="../includes/mediaelement-and-player.min.js"></script>
<link rel="stylesheet" href="../includes/mediaelementplayer.min.css" />

<link rel="stylesheet" href="../lightbox/prettyPhoto.css" type="text/css" media="screen" title="prettyPhoto main stylesheet" charset="utf-8" />
<script src="../lightbox/jquery.prettyPhoto.js" type="text/javascript" charset="utf-8"></script>
<script src="../javascripts/popup.js" type="text/javascript" charset="utf-8"></script>

<div id="light-gal" style="z-index:99999;" class="gallery-wrapper">
	<div id="bg">
		<a href="#" class="nextImageBtn" title="next"></a>
		<a href="#" class="prevImageBtn" title="previous"></a>
		<img width="1680" src="" height="1050" alt="" title="" id="bgimg" />
	</div>
	<div id="preloader"><img src=".././galleryfiles/ajax-loader_dark.gif" width="32" height="32" /></div>
	<div id="img_title"></div>
	<div id="toolbar">
		<a href="#" title="Pause" id="pauseImg" onClick="setimage('pause')"><img src=".././galleryfiles/pause.png" width="50" height="50"  /></a>
		<a href="#" title="Maximize" id="maximizeImg" onClick="ImageViewMode('full');return false"><img src=".././galleryfiles/toolbar_fs_icon.png" width="50" height="50"  /></a>
		<script type="text/javascript">
		<!--[if IE]>
		$("#maximizeImg").css({"display":"none"});
		<![endif]-->
		</script>
		<a href="#" title="Close" id="closeGal" onClick="CloseGallery();return false"><img src=".././galleryfiles/close.png" width="50" height="50"  /></a>
	</div>
	<div id="thumbnails_wrapper">
	<div id="outer_container">
	<div class="thumbScroller">
		<div class="container" id="gallery-container">
			
		</div>
	</div>
	</div>
	</div>
</div>
    <div id="backgroundPopup"></div>
	<input type="hidden" id="curr_playing_playlist" name="curr_playing_playlist">
</body>
<style>
.player{ float: left; margin-bottom: 0px; width: 172px; top:0px; }
.type_subtype{  font-size: 14px; font-weight: bold; line-height: 20px; width: 172px;  }
.tabItem {height: 335px;}
.imgs_pro { clear:both; margin-bottom: 0px !important; border-top: 1px solid #E8E8E8; border-left: 1px solid #E8E8E8; border-right: 1px solid #E8E8E8; }
#index_navs {border-bottom: medium none; float: left; height: 44px; margin-top: 81px; width: 939px; }
.both_wrap_per { background: none repeat scroll 0 0 #F0F0F0; height:84px; padding: 10px; width: 180px; border-bottom: 1px solid #E8E8E8; border-left: 1px solid #E8E8E8; border-right: 1px solid #E8E8E8; float:left; }
.by_details { font-weight: normal; }
.navButton { margin: 0 0 4px; width: 142px; float: left; }
.playMedia { float: left; margin-left: 1px; }
.playMedia a { height: inherit !important; left: 74px !important; position: relative !important; width: inherit !important; border-radius: 6px 6px 6px 6px !important; bottom: 138px !important; clear: both; background: none repeat scroll 0 0 #333333; float:left; }
.playMedia .list_play img { margin-bottom: 0px !important; }
.playMedia a:hover { background:none repeat scroll 0 0 #000000; }
</style>
<script type="text/javascript" src="/galleryfiles/jqgal.js"></script>
<script>
/*Code For Playing Channel All media*/
function PlayAll(videoid,songid)
{
	var get_song_id = document.getElementById(songid).value;
	var get_video_id = document.getElementById(videoid).value;
	var ex_song_id =get_song_id.split(",");
	var ex_video_id =get_video_id.split(",");
	var type;
	var counter_song =0;
	var counter_video =0;
	function next()
	{
		if (counter_video < ex_video_id.length)
		{
			if($("#curr_playing_playlist").val()!="")
			{
				type = "addvi";
			}else{
				if(counter_video ==1)
				{
					type = "playlist";
				}
				else
				{
					type = "addvi";
				}
			}
			what = "v";
			showPlayer_channel(what,ex_video_id[counter_video],type);
			setTimeout(next, 2000);
			counter_video++;
		}
		if(counter_video == ex_video_id.length)
		{
			if(counter_song < ex_song_id.length)
			{
				if($("#curr_playing_playlist").val()!="")
				{
					type = "add";
				}else{
					if(counter_song ==1)
					{
						type = "play";
					}
					else
					{
						type = "add";
					}
				}
				what = "a";
				showPlayer_channel(what,ex_song_id[counter_song],type);
				setTimeout(next, 2000);
				counter_song++;
			}
		}
	}
	  next();
}
/*Code For Playing Channel All media Ends Here*/

$(".login").click(function(){
//window.location.reload();
	document.search_form.submit();
	
});

$("document").ready(function() {
	$("#outerContainer").css({"display":"block"});
	//$("#loaders").css({"display":"none"});
	
	$("#logged_chk_user").fancybox({
				helpers : {
					media : {},
					buttons : {},
					title : null
				}
			});
});

function checkload(defImg,Images,orgpath,thumbpath,title,media_id){
	var arr_sep = Images.split(',');
	var title_sep = null;
	title_sep = title.split(',');
	var k;
	var data = new Array();
	for(k=0;k<arr_sep.length;k++)
	{
		if(arr_sep[k]!="" && arr_sep[k]!=" "){
			//arr_sep[k] = arr_sep[k].replace("\"","");
			arr_sep[k] = arr_sep[k].replace(/"/g, '');
			arr_sep[k] = arr_sep[k].replace("[","");
			arr_sep[k] = arr_sep[k].replace("]","");
			data[k] = arr_sep[k];
		}
	}
	
	var q;
	var data1 = new Array();
	for(q=0;q<title_sep.length;q++)
	{
		//arr_sep[k] = arr_sep[k].replace("\"","");
		title_sep[q] = title_sep[q].replace(/"/g, '');
		title_sep[q] = title_sep[q].replace("[","");
		title_sep[q] = title_sep[q].replace("]","");
		data1[q] = title_sep[q];
	}
	
	if(data.length>0){
		$("#gallery-container").html('');
	}
	//$("#bgimg").attr('src','<?php echo $orgPath;?>'+defImg);
	//alert(orgpath+defImg);
	$("#bgimg").attr('src',thumbpath+defImg);
	

	for(var i=0;i<data.length;i++){

		$("#gallery-container").append("<div class='content'><div><a href='"+thumbpath+data[i]+"'><img  src='"+orgpath+data[i]+"' title='"+data1[i]+"' alt='"+data1[i]+"' class='thumb' /></a></div></div>");
	}
	//setTimeout(function(){	loadGallery();	$("#light-gal").css('visibility','visible');	},500);
	loadGallery();
	$("#light-gal").css('visibility','visible');
	$(".rowItem").css({"display":"none"});
	$(".accountSettings").css('z-index','1');
	document.getElementById("gallery-container").style.left = "0px";
	$.ajax({
			type: "POST",
			url: 'update_media.php',
			data: { "media_id":media_id },
			success: function(data){
			}
	});
}

function autoRotate(){
	if(setTimer==1){
		$("a.nextImageBtn").click();
	}
}

function setimage(playMode){
	if(playMode=="pause"){
		$("#pauseImg").find("img").attr("src", ".././galleryfiles/play.png");
		$("#pauseImg").attr("onClick", "ImagePlayMode('play');return false").attr("title", "Play");
		setTimer=0;
		$rotate_val =1;
		GalleryAutoPlayTimer = null;
		//window.clearInterval(GalleryAutoPlayTimer);
	} else {
		$("#pauseImg").find("img").attr("src", ".././galleryfiles/pause.png");
		$("#pauseImg").attr("onClick", "ImagePlayMode('pause');return false").attr("title", "Pause");
		setTimer=1;
		GalleryAutoPlayTimer=self.setInterval("autoRotate()",4000);
	}

}



var logedinFlag='<?php echo $logedinFlag;?>';
var windowRef=false;
var profileName = "Mithesh";
function showPlayer(tp,ref,act){
	if(logedinFlag =="")
	{
		//$("#contactArea").html("Only registered users can view media.");
		//centerPopup();
		//loadPopup();
		//return false;
	}

	var pl = $("#curr_playing_playlist").val();
	$.ajax({
		type: "POST",
		url: 'player/getMediaInfo.php',
		data: { "tp":tp, "ref":ref, "source":"<?php echo $ans_sql['general_user_id']; ?>", "act":act, "pl":pl, "creator":profileName, "update":"update" },
		success: function(data){
			//$("#curr_playing_playlist").val(data);
			get_cookie(data);
			windowRef = window.open('', 'formpopup', 'height=580,width=587,resizeable,scrollbars');
			if(windowRef == null || typeof(windowRef) == "undefined") {
			   alert("The media player is a pop up player to give you uninterrupted streaming while browsing the site. To view it you must allow pop ups from Purify Art in the settings of your browser.");
			   return false;
			}
			if(windowRef.location == "about:blank" || (act!="add" && act!="addvi")){
				windowRef.location.href='combinePlayer/index.php#tp='+tp+'&ref='+ref+'&pl='+data+'&buyer_id='+'<?php echo $_SESSION['gen_user_id'];?>'+'&act=play';
				//jQuery(window).blur();
				windowRef.focus();
			}
			else{
				windowRef.location.href='combinePlayer/index.php#tp='+tp+'&ref='+ref+'&pl='+data+'&buyer_id='+'<?php echo $_SESSION['gen_user_id'];?>';
				//jQuery(window).blur();
				windowRef.focus();
			}
		}
	});
}

function showPlayer_channel(tp,ref,act){
	var pl = $("#curr_playing_playlist").val();
	$.ajax({
		type: 'POST',
		url: '/player/getMediaInfo.php',
		data: { "tp":tp, "ref":ref, "source":"<?php echo $ans_sql['general_user_id']; ?>", "act":act, "pl":pl, "creator":profileName, "update":"update" },
		success: function(data){
			get_cookie(data);
			var is_chrome = navigator.userAgent.toLowerCase().indexOf('chrome') > -1;
			if(is_chrome ==true){
				WinHeight = 579;
			}else{
				WinHeight = 580;
			}
			windowRef = window.open('', 'formpopup', 'height='+WinHeight+',width=587,resizable=no,scrollbars');
			if(windowRef == null || typeof(windowRef) == "undefined") {
			   alert("The media player is a pop up player to give you uninterrupted streaming while browsing the site. To view it you must allow pop ups from Purify Art in the settings of your browser.");
			   return false;
			}
			if(windowRef.location == "about:blank" || (act!="add" && act!="addvi")){
				windowRef.location.href='combinePlayer/index.php#tp='+tp+'&ref='+ref+'&pl='+data+'&buyer_id='+'<?php echo $_SESSION['gen_user_id'];?>'+'&act=play'+'&chan=channel';
				//jQuery(window).blur();
				windowRef.focus();
			}
			else{
				windowRef.location.href='combinePlayer/index.php#tp='+tp+'&ref='+ref+'&pl='+data+'&buyer_id='+'<?php echo $_SESSION['gen_user_id'];?>'+'&chan=channel';
				//windowRef.focus();
			}
		}, 
		error: function(jqXHR, textStatus, errorThrown) 
          {
             // alert(errorThrown);
           }
	});
}

function showPlayer_reg(tp,ref,act,tbl){
	if(logedinFlag =="")
	{
		//$("#contactArea").html("Only registered users can view media.");
		//centerPopup();
		//loadPopup();
		//return false;
	}

	var pl = $("#curr_playing_playlist").val();
	$.ajax({
		type: "POST",
		url: 'player/getMediaInfo.php',
		data: { "tp":tp, "ref":ref, "tbl":tbl, "source":"<?php echo $ans_sql['general_user_id']; ?>", "act":act, "pl":pl, "creator":profileName, "update":"update" },
		success: function(data){
		//alert(data);
			//$("#curr_playing_playlist").val(data);
			get_cookie(data);
			var is_chrome = navigator.userAgent.toLowerCase().indexOf('chrome') > -1;
			if(is_chrome ==true){
				WinHeight = 579;
			}else{
				WinHeight = 580;
			}
			windowRef = window.open('', 'formpopup', 'height='+WinHeight+',width=587,resizeable,scrollbars');
			if(windowRef == null || typeof(windowRef) == "undefined") {
			   alert("The media player is a pop up player to give you uninterrupted streaming while browsing the site. To view it you must allow pop ups from Purify Art in the settings of your browser.");
			   return false;
			}
			if(windowRef.location == "about:blank" || (act!="add" && act!="addvi" && act!="add_reg" && act!="addvi_reg")){
				windowRef.location.href='combinePlayer/index.php#tp='+tp+'&ref='+ref+'&pl='+data+'&buyer_id='+'<?php echo $_SESSION['gen_user_id'];?>'+'&act=play';
				//jQuery(window).blur();
				windowRef.focus();
			}
			else{
				windowRef.location.href='combinePlayer/index.php#tp='+tp+'&ref='+ref+'&pl='+data+'&buyer_id='+'<?php echo $_SESSION['gen_user_id'];?>';
				//jQuery(window).blur();
				windowRef.focus();
			}
		}
	});
}
function get_cookie(data)
{
	document.cookie="filename=" + data;
	getCookieValue("filename");
}
function getCookieValue(key)
{
	currentcookie = document.cookie;
	if (currentcookie.length > 0)
	{
		firstidx = currentcookie.indexOf(key + "=");
		if (firstidx != -1)
		{
			firstidx = firstidx + key.length + 1;
			lastidx = currentcookie.indexOf(";",firstidx);
			if (lastidx == -1)
			{
				lastidx = currentcookie.length;
			}
			getdata = unescape(currentcookie.substring(firstidx, lastidx));
			$("#curr_playing_playlist").val(unescape(currentcookie.substring(firstidx, lastidx)));
			//return unescape(currentcookie.substring(firstidx, lastidx));
		}
	}
	return "";
}

function change_Viewby(view_type)
{
	var divs = view_type + 's';
	$(".nav ul li a").css({"background":""});
	$("#"+divs).css({"background":"#e8e8e8"});
	
	$("#type_scroll_values").val(view_type);
	$("#scroll_values").val(19);
	
	
	$("#loaders_end_data").css({"display":"none"});
	//$("#loader_type").show();
	$("#loaders_end").css({"display":"block"});
	$("#change_type_view").hide();
	
	$.ajax({
		type: "POST",
		url: 'getdata_scroll.php',
		data: { "view_type":view_type, "profile_type_search":'<?php echo $_POST['profile_type_search'] ?>', "select-category-artist":'<?php echo $_POST['select-category-artist'] ?>', "select-category-artist-subtype":'<?php echo $_POST['select-category-artist-subtype'] ?>', "select-category-artist-metatype":'<?php echo $_POST['select-category-artist-metatype'] ?>', "countrySelect":'<?php echo $_POST['countrySelect'] ?>', "stateSelect":'<?php echo $_POST['stateSelect'] ?>', "city_search":'<?php echo $_POST['city_search'] ?>', "q_match":'<?php echo $q_match ?>', "q_table":'<?php echo $_GET['q_table'] ?>', "q_id":'<?php echo $_GET['q_id'] ?>', "alphs":'<?php echo $alphs ?>', "tab_name":'<?php echo $_GET['tab_name'] ?>', "counts_scroll":19 },
		success: function(data){
			$("#change_type_view").html(data);
			//$("#loader_type").hide();
			$("#loaders_end").css({"display":"none"});
			$("#change_type_view").show();
		}
	});
}
/* getCookieValue("filename");
function download_directly_song(id,t,c,f){
	var val = confirm("Are you sure you want to download "+t+" by "+c+" from "+f+"?");
	if(val==true){
	window.location = "zipes_1.php?download_meds="+id;
	}
} */
$("document").ready(function(){
window.setInterval("getCookieValue('filename')",500);
});
function download_directly_song(id,t,c,f){
	var val = confirm("Are you sure you want to download "+t+" by "+c+" from "+f+"?");
	if(val==true){
	window.location = "downloads_ok_zip.php?download_meds_zip="+id+"&single_pocess=true";
	}
}

var $_returnvalue ="";
var $_returnvalue_tip_user = 0;
var $_returnclicktext ="";
var $_returnsong_id ="";

var $_returnvalue_pro = "";
var $_returnvalue_tip_user_pro = 0;
var $_returnclicktext_pro ="";
var $_returnsong_id_pro ="";

var $_retval_pro_name_aftr_don = "";
var $_retval_pro_email_aftr_don = "";

var $_retval_pro_tip_gen_id_don = 0;
var $_retval_single_tip_gen_id_don = 0;
var $_retval_single_tip_gen_ids_aldon = "";
var $_retval_single_tip_gen_ids_aldon_pro = "";
var $_returnsong_id_pro_type = "";
var $_returnsong_id_pro_ids_zip = "";

var $_retval_single_name_aftr_don = "";
var $_retval_single_email_aftr_don = "";

/* function ask_to_donate(name,creator,song_id,clicktext)
{
	$("#get_donationpop").attr("href", "asktodonate.php?name="+name+"&creator="+creator+"&clicktext="+clicktext+"&song_id="+song_id);
	$("#get_donationpop").click();
} */
function ask_to_donate(name,creator,song_id,clicktext,down_tip_gen_id_sin,get_all_detils_tip_fr)
{
	$("#get_donationpop").attr("href", "asktodonate.php?name="+encodeURIComponent(name)+"&creator="+encodeURIComponent(creator)+"&clicktext="+encodeURIComponent(clicktext)+"&song_id="+song_id+"&gen_user_down_tip_id="+down_tip_gen_id_sin+"&simple_detals_fr_tip="+get_all_detils_tip_fr);
	$("#ask_name_email_single").attr("href", "ask_single_name_aftr_don.php?name="+encodeURIComponent(name)+"&creator="+encodeURIComponent(creator));
	$("#get_donationpop").click();
}

function ask_to_donate_alpro(name,creator,song_id,clicktext,down_tip_gen_id,get_all_detils_tip_fr_alpro,tpy_pro_zip,id_zip)
{
	$("#get_donationpop_alpro").attr("href", "asktodonate_pro.php?name="+encodeURIComponent(name)+"&creator="+encodeURIComponent(creator)+"&clicktext="+encodeURIComponent(clicktext)+"&song_id="+encodeURIComponent(song_id)+"&gen_user_down_tip_id="+down_tip_gen_id+"&simple_detals_fr_tip="+get_all_detils_tip_fr_alpro+"&pro_type="+tpy_pro_zip+"&pro_type_zip_id="+id_zip);
	$("#ask_name_alemail").attr("href", "ask_name_aftr_don.php?name="+encodeURIComponent(name)+"&creator="+encodeURIComponent(creator));
	$("#get_donationpop_alpro").click();
}

function ask_to_donate_result_alpro()
{
	if($_returnvalue_pro == 1){
		$_returnvalue_pro ="";
		var Dvideolink = "";
		var Dvideolink = document.getElementById('fan_club_pro'+$_returnclicktext_pro);
		setTimeout(function(){
			$("#fan_club_pro"+$_returnclicktext_pro).attr("href", Dvideolink.href + "&don_tips_al_pro="+$_returnvalue_tip_user_pro);
			//alert(Dvideolink.href);
			Dvideolink.click();
		}
		,2000);
	}
	else if($_returnvalue_pro == 2){
<?php
			if(isset($_SESSION['login_email']))
			{
				if($_SESSION['login_email']!="")
				{
		?>
					setTimeout(function(){
					$_returnvalue_pro ="";
						window.location = "downloads_ok_zip.php?download_meds_zip="+$_returnsong_id_pro_ids_zip+"&prject_trues=false&gen_down_tip_id="+$_retval_pro_tip_gen_id_don+"&gen_tip_al_ids="+$_retval_single_tip_gen_ids_aldon_pro+"&pro_typ_zip="+$_returnsong_id_pro_type;
					}
					,1000);
		<?php
				}
				else
				{
?>
					$_retval_pro_name_aftr_don = "";
					$_retval_pro_email_aftr_don = "";
					$("#ask_name_alemail").click();
<?php
				}
			}
			else
			{
?>
				$_retval_pro_name_aftr_don = "";
				$_retval_pro_email_aftr_don = "";
				$("#ask_name_alemail").click();
<?php
			}
		?>		
	}
}

function call_al_download()
{
	setTimeout(function(){
		$_returnvalue_pro ="";
			window.location ="downloads_ok_zip.php?download_meds_zip="+$_returnsong_id_pro_ids_zip+"&prject_trues=false&sessio_oth_name="+encodeURIComponent($_retval_pro_name_aftr_don)+"&sessio_oth_email="+encodeURIComponent($_retval_pro_email_aftr_don)+"&gen_down_tip_id="+$_retval_pro_tip_gen_id_don+"&gen_tip_al_ids="+$_retval_single_tip_gen_ids_aldon_pro+"&pro_typ_zip="+$_returnsong_id_pro_type;
		}
		,1000);
}

$(document).ready(function() {
	$("#get_donationpop_alpro").fancybox({
		helpers : {
			media : {},
			buttons : {},
			title : null,
		},
		afterClose : function(){
						ask_to_donate_result_alpro();	
					}
	});
	
	$("#ask_name_alemail").fancybox({
		helpers : {
			media : {},
			buttons : {},
			title : null,
		},
		afterClose : function(){
						if($_retval_pro_name_aftr_don=="" || $_retval_pro_email_aftr_don=="")
						{
							alert("Please give all your valid details.");
						}
						else
						{
							//call_al_download();	
						}
					}
	});
});
/*
function ask_to_donate(name,creator,song_id,clicktext)
{
	var val = confirm("would you like to donate :"+name+" ("+creator+")");
	if(val== true){
		var Dvideolink = document.getElementById(clicktext);
		Dvideolink.click();
	}
	else{
		window.location = "zipes_1.php?download_meds="+song_id;
	}
}*/
/* function ask_to_donate_result()
{
	if($_returnvalue == 1){
		$_returnvalue ="";
		var Dvideolink = document.getElementById($_returnclicktext);
		setTimeout(function(){
		Dvideolink.click();
		}
		,2000);
	}
	else if($_returnvalue == 2){
		setTimeout(function(){
		$_returnvalue ="";
		window.location = "zipes_1.php?download_meds="+$_returnsong_id;
		}
		,1000);
	}
} */
function ask_to_donate_result()
{
	if($_returnvalue == 1){
		$_returnvalue ="";
		var Dvideolink = document.getElementById($_returnclicktext);
		setTimeout(function(){
		$("#"+$_returnclicktext).attr("href", Dvideolink.href + "&don_tips_al="+$_returnvalue_tip_user);
		Dvideolink.click();
		}
		,2000);
	}
	else if($_returnvalue == 2){
	<?php
			if(isset($_SESSION['login_email']))
			{
				if($_SESSION['login_email']!="")
				{
		?>
					setTimeout(function(){
					$_returnvalue ="";
						window.location = "downloads_ok_zip.php?download_meds_zip="+$_returnsong_id+"&single_pocess=true&gen_down_tip_id="+$_retval_single_tip_gen_id_don+"&gen_tip_al_ids="+$_retval_single_tip_gen_ids_aldon;
					}
					,1000);
		<?php
				}
				else
				{
?>
					$_retval_single_name_aftr_don = "";
					$_retval_single_email_aftr_don = "";
					$("#ask_name_email_single").click();
<?php
				}
			}
			else
			{
?>
				$_retval_single_name_aftr_don = "";
				$_retval_single_email_aftr_don = "";
				$("#ask_name_email_single").click();
<?php
			}
		?>
	}
}

$("#ask_name_email_single").fancybox({
		helpers : {
			media : {},
			buttons : {},
			title : null,
		},
		afterClose : function(){
						if($_retval_single_name_aftr_don=="" || $_retval_single_email_aftr_don=="")
						{
							alert("Please give all your valid details.");
						}
						else
						{
							//call_single_download();	
						}
					}
	});
	
function call_single_download()
{
	setTimeout(function(){
		$_returnvalue ="";
			window.location = "downloads_ok_zip.php?download_meds_zip="+$_returnsong_id+"&single_pocess=true&sessio_oth_name="+encodeURIComponent($_retval_single_name_aftr_don)+"&sessio_oth_email="+encodeURIComponent($_retval_single_email_aftr_don)+"&gen_down_tip_id="+$_retval_single_tip_gen_id_don+"&gen_tip_al_ids="+$_retval_single_tip_gen_ids_aldon;
		}
		,1000);
}

$(document).ready(function() {
	$("#get_donationpop").fancybox({
		helpers : {
			media : {},
			buttons : {},
			title : null,
		},
		afterClose : function(){
								ask_to_donate_result();	
							}
	});
});

	function click_fancy_login_down(url)
	{
		url = url.replace("(","\\(");
		url = url.replace(")","\\)");
		$("#fan_club_pro"+url).click();
	}
		
	function fancy_login(url)
	{
		if(logedinFlag!="")
		{
			$("#fan_club_pro"+url).click();
		}
		else if(logedinFlag=="")	
		{
			var url_sec = document.getElementById("logged_chk_user");
			url_sec.href = "handle_login.php?fan&datas="+url;
			$("#logged_chk_user").click();
		}
	}
	
	function fancy_login_art_coms(url)
	{
		if(logedinFlag!="")
		{
			$("#fan_club_pro_ca"+url).click();
		}
		else if(logedinFlag=="")	
		{
			var url_sec = document.getElementById("logged_chk_user");
			url_sec.href = "handle_login.php?fan&datas="+url;
			$("#logged_chk_user").click();
		}
	}
	
	function download_directly_project(id,t,c,typ_zip,id_zip){
		var val = confirm("Are you sure you want to download "+t+" by "+c+"?");
		if(val==true){
		window.location = "downloads_ok_zip.php?download_meds_zip="+id_zip+"&prject_trues=false"+"&pro_typ_zip="+typ_zip;
		}
	}
</script>
</html>