<?php
	session_start();
	//include("includes/header.php");
	include("commons/db.php");
	include_once('classes/Commontabs.php');
	include("classes/GetFanClubInfo.php");
	$get_fan_club = new GetFanClubInfo();
    $domainname=$_SERVER['SERVER_NAME'];
?>
	<link rel="stylesheet" type="text/css" media="screen" href="includes/purify.css" />
	<!--<link rel="stylesheet" type="text/css" href="new_fancy_box/jquery.fancybox.css?v=2.1.4" media="screen" />-->
	<link href="sample.css" rel="stylesheet" type="text/css" />
<?php
	if(isset($_GET))
	{
		if($_GET['SECURETOKEN']!="" || $_GET['SECURETOKENID'])
		{
			$token_chk = $get_fan_club->check_token($_GET['SECURETOKEN']);
			if($token_chk=="1")
			{
				if($_GET['DESCRIPTION']=='chkout_membership')
				{
?>
					<div>
						<div class="fancy_header">
							Download Expired
						</div>
						<div id="contents_frames">
							<p style="position:relative; text-align:center; line-height:45px">You have already renewed your Membership. If you want to renew again please follow the link, <a style="text-decoration: underline;" href="profileedit.php#Become_a_member">Become a Member.</a></p>
						</div>
					</div>
<?php	
				}
				else
				{
					$token_chk_mailg = $get_fan_club->check_token_gmail($_GET['SECURETOKEN']);
					
					if(isset($_SESSION['login_email']))
					{
						if($_SESSION['login_email']!="")
						{
?>
							<div>
								<div class="fancy_header">
									Download Expired
								</div>
								<div id="contents_frames">
									<p style="position:relative; text-align:center; line-height:45px">This download link has expired, you may download your purchased and fan club media at any time in your <a style="text-decoration: underline;" href="profileedit_media.php">media library.</a></p>
								</div>
							</div>
<?php
						}
						else
						{
?>
							<div>
								<div class="fancy_header">
									Download Expired
								</div>
								<div id="contents_frames">
									<p style="position:relative; text-align:center; line-height:45px">Your download is expired as it has already been attempted. To access your download you will need to register and then go to your Media Library. If you have any questions contact sales@purifyart.net.<br/>
										<a style='background-color: rgb(0, 0, 0); border: 0px none; color: rgb(255, 255, 255); font-weight: 700; text-decoration: none; position: relative; margin-right: 0px; padding: 8px 19px; font-size: 15px;' href='registration_up.php?thnxs_download=<?php echo $token_chk_mailg['email_accs']; ?>'>Register</a>
									</p>
								</div>
							</div>
<?php
						}
					}
					else
					{
?>
						<div>
							<div class="fancy_header">
								Download Expired
							</div>
							<div id="contents_frames">
								<p style="position:relative; text-align:center; line-height:45px">Your download is expired as it has already been attempted. To access your download you will need to register and then go to your Media Library. If you have any questions contact sales@purifyart.net.<br/>
									<a style='background-color: rgb(0, 0, 0); border: 0px none; color: rgb(255, 255, 255); font-weight: 700; text-decoration: none; position: relative; margin-right: 0px; padding: 8px 19px; font-size: 15px;' href='registration_up.php?thnxs_download=<?php echo $token_chk_mailg['email_accs']; ?>'>Register</a>
								</p>
							</div>
						</div>
<?php
					}
				}
				//die;
			}
			else
			{
				$check_prefs_ever = $get_fan_club->check_secure_ids($_GET['SECURETOKENID']);
				if($check_prefs_ever=='1')
				{
					if(isset($_SESSION['login_email']))
					{
						if($_SESSION['login_email']!="")
						{
?>
							<div>
								<div class="fancy_header">
									Download Expired
								</div>
								<div id="contents_frames">
									<p style="position:relative; text-align:center; line-height:45px">This download link has expired, you may download your purchased and fan club media at any time in your <a style="text-decoration: underline;" href="profileedit_media.php">media library.</a></p>
								</div>
							</div>
<?php
						}
						else
						{
?>
							<div>
								<div class="fancy_header">
									Download Expired
								</div>
								<div id="contents_frames">
									<p style="position:relative; text-align:center; line-height:45px">Your download is expired as it has already been attempted. To access your download you will need to register and then go to your Media Library. If you have any questions contact sales@purifyart.net.<br/>
										<a style='background-color: rgb(0, 0, 0); border: 0px none; color: rgb(255, 255, 255); font-weight: 700; text-decoration: none; position: relative; margin-right: 0px; padding: 8px 19px; font-size: 15px;' href='registration_up.php?thnxs_download=<?php echo $_SESSION['guest_email']; ?>'>Register</a>
									</p>
								</div>
							</div>
<?php
						}
					}
					else
					{
?>
						<div>
							<div class="fancy_header">
								Download Expired
							</div>
							<div id="contents_frames">
								<p style="position:relative; text-align:center; line-height:45px">Your download is expired as it has already been attempted. To access your download you will need to register and then go to your Media Library. If you have any questions contact sales@purifyart.net.<br/>
									<a style='background-color: rgb(0, 0, 0); border: 0px none; color: rgb(255, 255, 255); font-weight: 700; text-decoration: none; position: relative; margin-right: 0px; padding: 8px 19px; font-size: 15px;' href='registration_up.php?thnxs_download=<?php echo $_SESSION['guest_email']; ?>'>Register</a>
								</p>
							</div>
						</div>
<?php
					}
				}
			}
		}
		else
		{
?>
			<div>
				<div class="fancy_header">
					Download Expired
				</div>
				<div id="contents_frames">
					<p style="position:relative; text-align:center; line-height:45px">Your download is expired as it has already been attempted. To access your download you will need to register and then go to your Media Library. If you have any questions contact sales@purifyart.net.<br/>
						<a style='background-color: rgb(0, 0, 0); border: 0px none; color: rgb(255, 255, 255); font-weight: 700; text-decoration: none; position: relative; margin-right: 0px; padding: 8px 19px; font-size: 15px;' href='registration_up.php'>Register</a>
					</p>
				</div>
			</div>
<?php	
		}
	}
?>
<style>
	#contents_frames {padding: 20px;background-color: #FFFFFF;border-radius:6px; width: 506px;}
	#contents_frames p { margin:0; }
</style>