<?php
$result = array();
$errorflag='no';

$errormsg=array();
$inputparams=array();
$username='nouser';
$userpass='nopass';
$listartists='kaz,leerick';
$format='json';

$data = file_get_contents("php://input");
$dataary=json_decode($data,true);

if (isset($dataary['format'])){    
    $format = strtolower($dataary['format']);
}
if(isset($dataary['listartists'])) {
    $listartists=$dataary['listartists'];
}else {
    $errormessage='missing profile';
    $errormsg[]=array('errormsg'=>$errormessage);
    $result[] = array('error'=>$errormsg);
    $inputparams[] = array('listartists'=>$listartists);
    $inputparams[] = array('format'=>$format);
    $inputparams[] = array('post'=>$dataary);
    $result[] = array('inputparams'=>$inputparams);
    $errorflag='yes';
}

if ($errorflag='no'){
    include_once("commons/db.php");
    include_once("classes/ViewArtistProfileURL.php");
    include_once("classes/viewArtistProjectURL.php");
    include_once("classes/PersonalWallDisplayFeeds.php");
    include_once('classes/ProfileDisplay.php');   
        
    // add user name to bucketname in s3 url
    $root = realpath($_SERVER["DOCUMENT_ROOT"]);
    if (!class_exists('S3')) require_once ($root.'/S3.php');    
    $s3 = new S3("", "");
    
    $foundeurls=",";
    $date = date('Y-m-d H:i:s');
    $galleryImagesDataIndex=0;
    $get_artist_event[0]=array();
    //$get_community_event[0]=array();
    //$get_project[0]=array();
    
    $listartists_exp = explode(',',$listartists);
    $k=0;
    
    if(isset($listartists_exp) && count($listartists_exp)>0 && !empty($listartists_exp))
    {
        for($j=0;$j<count($listartists_exp);$j++)
        {               
            //echo '<br>getting profileurl='.$listartists_exp[$j].' <br>';     
            $pos=strpos($foundeurls,$listartists_exp[$j]);
            if ($pos===false)
            {                            
                $foundeurls=$foundeurls.','.$listartists_exp[$j];                           
                if($listartists_exp[$j]!="")
                {   
                    $display = new ProfileDisplay();
                    $artist_check = $display->artistCheck($listartists_exp[$j]);
                    $get_common_id=$artist_check['artist_id'];
                    //echo '<br> artist id = '.$artist_check['artist_id'].' for profileurl= '.$profileurl.'<br>';
                    $newPersonalWallDisplayFeeds = new PersonalWallDisplayFeeds();
                    $new_profile_class_obj = new ViewArtistProfileURL();
                    $new_project_class_obj = new viewArtistProjectURL();
                    $getgeneral =  $new_profile_class_obj->get_user_info($artist_check['artist_id']);
                    //echo '<br> general user id = '.$getgeneral['general_user_id'].' for artist id= '.$artist_check['artist_id'].'<br>';
      
                                        
                    $get_posts_for_artist =$newPersonalWallDisplayFeeds->mypostsandgeneraluser($getgeneral['general_user_id']);   
                    
	                if($get_posts_for_artist!="" && $get_posts_for_artist!=Null)
	                {
		                if(mysql_num_rows($get_posts_for_artist)>0)
		                {
                            while($row = mysql_fetch_assoc($get_posts_for_artist))
	                        {
		                        //print_r($row);
                                $get_posts_for_all_artists[$k] = $row;
                                $profiletype=$get_posts_for_all_artists[$k]['profile_type'];
                                $media=$get_posts_for_all_artists[$k]['media'];
                                $generaluserid=$get_posts_for_all_artists[$k]['general_user_id'];
                                $generalartist[$generaluserid]= $new_profile_class_obj->get_artist_Info($generaluserid);
                                
                                switch ($profiletype)
                                {
                                    case 'projects_post':
                                    
                                        $project_exp_n = explode('~',$media);
                                        
                                        if ($project_exp_n[1]=="artist_project")
                                        {
                                            $artistprojectid=$project_exp_n[0];
                                            $get_project[$artistprojectid] = mysql_fetch_assoc($new_project_class_obj->get_aproject_count($artistprojectid));                                        
                                            //echo '<br>artist project here <br>';
                                            //print_r($get_project[$artistprojectid]);
                                            
                                            $AlbumProfileURL=$get_project[$artistprojectid]['profile_url'];
                                            $featuredMediaAlbum[$AlbumProfileURL]['featured_media']=$get_project[$artistprojectid]['featured_media'];
                                            $featuredMediaAlbum[$AlbumProfileURL]['media_id']=$get_project[$artistprojectid]['media_id'];
    
                                            if($featuredMediaAlbum[$AlbumProfileURL]['featured_media']=="Song"){
                                                $mediaid=$featuredMediaAlbum[$AlbumProfileURL]['media_id'];
                                                $get_media_song=$new_profile_class_obj->get_Media_Song($mediaid);
                                                $generaluserid= $get_media_song["general_user_id"];
                                                //echo '<br>generaluserid='.$generaluserid;
                                                $s3->resetAuth("", "",false,$generaluserid);
                                                $medaudioURI=$s3->getNewURI("medaudio"); 
                                                $songname=$get_media_song["song_name"];
                                                $songimageurl=$new_profile_class_obj->get_audio_img($mediaid);
                                                $featuredMediaAlbum[$AlbumProfileURL]["song_title"]=$get_media_song["songtitle"];
                                                $featuredMediaAlbum[$AlbumProfileURL]["song_url"]=$medaudioURI.$songname;
                                                $featuredMediaAlbum[$AlbumProfileURL]["creator_name"]=$get_media_song["creator"];
                                                $featuredMediaAlbum[$AlbumProfileURL]["from_name"]=$get_media_song["from"];
                                                $featuredMediaAlbum[$AlbumProfileURL]["song_image_url"]=$s3->replaceURI($songimageurl);
                                                $featuredMediaAlbum[$AlbumProfileURL]["for_sale"]=$get_media_song["for_sale"];
                                                $featuredMediaAlbum[$AlbumProfileURL]["price"]=$get_media_song["price"];                                                
                                            } else {
                                                if($featuredMediaAlbum[$AlbumProfileURL]['featured_media']=="Video"){
                                                    $mediaid=$featuredMediaAlbum[$AlbumProfileURL]['media_id'];
                                                    $get_media_video=$new_profile_class_obj->get_Media_Video($mediaid); 
                                                    $generaluserid= $get_media_video["general_user_id"];
                                                    //echo '<br>generaluserid='.$generaluserid;
                                                    $s3->resetAuth("", "",false,$generaluserid);
                                                    $medaudioURI=$s3->getNewURI("medaudio"); 
                                                    $featuredMediaAlbum[$AlbumProfileURL]["videotitle"]=$get_media_video["videotitle"];
                                                    $featuredMediaAlbum[$AlbumProfileURL]["videolink"]=$get_media_video["videolink"];
                                                }
                                            }                                
                                            
                                            
                                        }
                                        else
                                        {  // community project
                                            $artistprojectid=$project_exp_n[0];
                                            $get_project[$artistprojectid] = mysql_fetch_assoc($new_project_class_obj->get_cproject_count($artistprojectid));                                                                               
                                        }

                                        break;
                                    
                                    case 'events_post':
                                        $post_exp = explode('~',$media);
                                        
                                        if ($post_exp[1]=="artist_event")
                                        {
                                            $artistid=$post_exp[0];
                                            $get_artist_event[$artistid]=mysql_fetch_assoc($new_profile_class_obj->upcomingArtistEvents($artistid,$date));

                                            $new_profile_class_obj1 = new ViewArtistProfileURL();
                                            $AlbumProfileURL=$get_artist_event[$artistid]['profile_url'];
                                            $featuredMediaAlbum[$AlbumProfileURL]['featured_media']=$get_artist_event[$artistid]['featured_media'];
                                            $featuredMediaAlbum[$AlbumProfileURL]['media_id']=$get_artist_event[$artistid]['media_id'];
    
                                            if($featuredMediaAlbum[$AlbumProfileURL]['featured_media']=="Song"){
                                                $mediaid=$featuredMediaAlbum[$AlbumProfileURL]['media_id'];
                                                $get_media_song=$new_profile_class_obj1->get_Media_Song($mediaid);
                                                $generaluserid= $get_media_song["general_user_id"];
                                                //echo '<br>generaluserid='.$generaluserid;
                                                $s3->resetAuth("", "",false,$generaluserid);
                                                $medaudioURI=$s3->getNewURI("medaudio"); 
                                                $songname=$get_media_song["song_name"];
                                                $songimageurl=$new_profile_class_obj1->get_audio_img($mediaid);
                                                $featuredMediaAlbum[$AlbumProfileURL]["song_title"]=$get_media_song["songtitle"];
                                                $featuredMediaAlbum[$AlbumProfileURL]["song_url"]=$medaudioURI.$songname;
                                                $featuredMediaAlbum[$AlbumProfileURL]["creator_name"]=$get_media_song["creator"];
                                                $featuredMediaAlbum[$AlbumProfileURL]["from_name"]=$get_media_song["from"];
                                                $featuredMediaAlbum[$AlbumProfileURL]["song_image_url"]=$s3->replaceURI($songimageurl);
                                                $featuredMediaAlbum[$AlbumProfileURL]["for_sale"]=$get_media_song["for_sale"];
                                                $featuredMediaAlbum[$AlbumProfileURL]["price"]=$get_media_song["price"];                                                 
                                            } else {
                                                if($featuredMediaAlbum[$AlbumProfileURL]['featured_media']=="Video"){
                                                    $mediaid=$featuredMediaAlbum[$AlbumProfileURL]['media_id'];
                                                    $get_media_video=$new_profile_class_obj1->get_Media_Video($mediaid); 
                                                    $generaluserid= $get_media_video["general_user_id"];
                                                    //echo '<br>generaluserid='.$generaluserid;
                                                    $s3->resetAuth("", "",false,$generaluserid);
                                                    $medaudioURI=$s3->getNewURI("medaudio"); 
                                                    $featuredMediaAlbum[$AlbumProfileURL]["videotitle"]=$get_media_video["videotitle"];
                                                    $featuredMediaAlbum[$AlbumProfileURL]["videolink"]=$get_media_video["videolink"];
                                                }
                                            }
                                            
                                            //$get_artist_event[$artistid] = mysql_fetch_assoc($new_profile_class_obj->recordedArtistEvents($artistid,$date));
                                        }
                                        else
                                        { // community event
                                            $communityid=$post_exp[0];
                                            //echo '<br>community id= '.$communityid.' <br>';
                                            
                                            //echo '<br>community event info here <br>';
                                            $get_community_event[$communityid] = mysql_fetch_assoc($new_profile_class_obj->upcomingComEvents($communityid,$date)); 

                                            
                                            $new_profile_class_obj1 = new ViewArtistProfileURL();
                                            $AlbumProfileURL=$get_community_event[$communityid]['profile_url'];
                                            $featuredMediaAlbum[$AlbumProfileURL]['featured_media']=$get_community_event[$communityid]['featured_media'];
                                            $featuredMediaAlbum[$AlbumProfileURL]['media_id']=$get_community_event[$communityid]['media_id'];
    
                                            if($featuredMediaAlbum[$AlbumProfileURL]['featured_media']=="Song"){
                                                $mediaid=$featuredMediaAlbum[$AlbumProfileURL]['media_id'];
                                                $get_media_song=$new_profile_class_obj1->get_Media_Song($mediaid);
                                                $generaluserid= $get_media_song["general_user_id"];
                                                //echo '<br>generaluserid='.$generaluserid;
                                                $s3->resetAuth("", "",false,$generaluserid);
                                                $medaudioURI=$s3->getNewURI("medaudio"); 
                                                $songname=$get_media_song["song_name"];
                                                $songimageurl=$new_profile_class_obj1->get_audio_img($mediaid);
                                                $featuredMediaAlbum[$AlbumProfileURL]["song_title"]=$get_media_song["songtitle"];
                                                $featuredMediaAlbum[$AlbumProfileURL]["song_url"]=$medaudioURI.$songname;
                                                $featuredMediaAlbum[$AlbumProfileURL]["creator_name"]=$get_media_song["creator"];
                                                $featuredMediaAlbum[$AlbumProfileURL]["from_name"]=$get_media_song["from"];
                                                $featuredMediaAlbum[$AlbumProfileURL]["song_image_url"]=$s3->replaceURI($songimageurl);
                                                $featuredMediaAlbum[$AlbumProfileURL]["for_sale"]=$get_media_song["for_sale"];
                                                $featuredMediaAlbum[$AlbumProfileURL]["price"]=$get_media_song["price"];                                                
                                            } else {
                                                if($featuredMediaAlbum[$AlbumProfileURL]['featured_media']=="Video"){
                                                    $mediaid=$featuredMediaAlbum[$AlbumProfileURL]['media_id'];
                                                    $get_media_video=$new_profile_class_obj1->get_Media_Video($mediaid); 
                                                    $generaluserid= $get_media_video["general_user_id"];
                                                    //echo '<br>generaluserid='.$generaluserid;
                                                    $s3->resetAuth("", "",false,$generaluserid);
                                                    $medaudioURI=$s3->getNewURI("medaudio"); 
                                                    $featuredMediaAlbum[$AlbumProfileURL]["videotitle"]=$get_media_video["videotitle"];
                                                    $featuredMediaAlbum[$AlbumProfileURL]["videolink"]=$get_media_video["videolink"];
                                                }
                                            }                                            
                                            
                                            
                                            
                                            //$get_community_event[$communityid] = mysql_fetch_assoc($new_profile_class_obj->recordedComEvents($communityid,$date));                                            
                                        }                                        
                                    
                                        break;
                                    
                                    case 'videos_post':
                                    
                                        $video_exp = explode('~',$media);
                                        $videoid=$video_exp[0];    
                                        $exp_tagged_videos_info[$videoid] = $new_profile_class_obj->get_Media_video($videoid);
                                                                        
			                            $get_video_image = $new_profile_class_obj->getvideoimage($videoid);
                                    
			                            if(!empty($get_video_image))
			                            {
				                            if($get_video_image[1]=='youtube')
				                            {
                                                $imagename="https://img.youtube.com/vi/".$get_video_image[0]."/default.jpg";
                                            }                                     
				                            elseif($get_video_image[1]=='vimeo' && $get_video_image[0]!=NULL)
				                            {					
					                            $video = $vimeo->call('vimeo.videos.getInfo', array('video_id' => $get_video_image[0]));
					                            $imagename = $video->video[0]->thumbnails->thumbnail[1]->_content; 
                                            }
                                        }else
                                        {
                                            $imagename ="https://comjcropprofile.s3.amazonaws.com/Noimage.png";
                                        }
                                    
                                        $videoimagefile[$videoid]=$imagename;                                    
                                        $profileurlforvideo[$videoid]=$listartists_exp[$j];                                    break;
                                    
                                    case 'images_post':
                                    
                                        $gallery_exp = explode('~',$media);
                                        $galleryid=$gallery_exp[0];
			                            $temp_tagged_gallery_info=$new_profile_class_obj->get_gallery($galleryid);                   
                                        $exp_tagged_gallery_info[$galleryid] = $temp_tagged_gallery_info;
                                        $creator_exp=explode('|',$temp_tagged_gallery_info['creator_info']);
                                        $artist_check_gallery = $display->general_acusers('artist',$creator_exp[1]);

                                        $artistprofileurl[$galleryid]=$artist_check_gallery['profile_url'];
                                        $GalleryImageNames="";
                                        $GalleryImagetitles="";
                                        //echo "<br> gallery id = ".$galleryid." <br>";                                    
                                        //print_r($exp_tagged_gallery_info[$galleryid]);
 
                                        //Get count of images in general_artist_gallery
				                        $Get_count_img_at_reg=$new_profile_class_obj->get_count_Gallery_at_register($get_common_id,$galleryid);
				                        if($Get_count_img_at_reg['count(*)']>0)
				                        {
                                            //echo '<br>Found '.$Get_count_img_at_reg['count(*)']. ' gallery images<br>'; 
					                        $Get_img_at_reg=$new_profile_class_obj->get_Gallery_at_register($get_common_id,$galleryid);
					                        $Get_medi_cover_pic="";
                                                
					                        $play_array = array();
						                    if($Get_img_at_reg!="" && $Get_img_at_reg!=Null)
						                    {
							                    if(mysql_num_rows($Get_img_at_reg)>0)
							                    {
								                    while($row = mysql_fetch_assoc($Get_img_at_reg))
								                    {
									                    $play_array[] = $row;
								                    }
                                                    $GalleryOrgPath[$galleryid]="https://reggallery.s3.amazonaws.com/";
                                                    $GalleryThumbPath[$galleryid]="https://reggalthumb.s3.amazonaws.com/";							
								                    //${'orgPath'.$galleryid}="https://reggallery.s3.amazonaws.com/";
								                    //${'thumbPath'.$galleryid}="https://reggalthumb.s3.amazonaws.com/";
								                    $register_cover_pic=$play_array[0]['cover_pic'];
								                    $register_No_pic=$play_array[0]['image_name'];
                                                    $GalleryImageName[$galleryid]=setGalleryImage("","","",$register_cover_pic,$register_No_pic);
								                    //$exp_tagged_gallerieslery=$new_profile_class_obj->get_Gallery_title_at_register($play_array[0]['gallery_id']);
								                    //$galleryImagesDataIndex="";
								                    for($l=0;$l<count($play_array);$l++)
								                    {
                                                        $loopmediaid=$play_array[$l]['id'];                                                        
									                    $mediaSrc = $play_array[0]['image_name'];
									                    $filePath = "https://reggallery.s3.amazonaws.com/".$mediaSrc;
									                    //${'galleryImagesData'.$galleryid}[$galleryImagesDataIndex]="";
									                    $galleryImagesImageName[$loopmediaid]=$play_array[$l]['image_name'];
                                                        $galleryImagesImageTitle[$loopmediaid]=$play_array[$l]['image_title']; 
                                                        $GalleryImageNames=$GalleryImageNames.",".$galleryImagesImageName[$loopmediaid]=$play_array[$l]['image_name'];
                                                        $GalleryImagetitles=$GalleryImagetitles.",".$galleryImagesImageName[$loopmediaid]=$play_array[$l]['image_title'];                                                            
									                    //${'galleryImagesData'.$galleryid}[$galleryImagesDataIndex] = $play_array[$l]['image_name'];
									                    //${'galleryImagestitle'.$galleryid}[$galleryImagesDataIndex] = $play_array[$l]['image_title'];
									                    $galleryImagesDataIndex++;
                                                        //echo '<br> $galleryImagesDataIndex = '.$galleryImagesDataIndex.'<br>';
								                    }
                                                    $GalleryImageNamesList[$galleryid]=$GalleryImageNames;
                                                    $GalleryImageTitlesList[$galleryid]=$GalleryImagetitles;                                                        
							                    }
						                    }
				                        } // no images found in general_artist_gallery
				                        else
				                        { // get images from media_images for gallery id not = 0
					                        //echo '<br>Found '.$Get_count_img_at_reg['count(*)']. ' gallery images<br>'; 
                                            //echo '<br>now try to get images from media_images for gallery id not = 0 for real gallery id = '.$galleryid.' <br>';
                                            //$exp_tagged_gallerieslery=$new_profile_class_obj->get_gallery($galleryid);
					                        $Get_medi_cover_pic=$new_profile_class_obj->get_media_cover_pic($galleryid);
					                        $sql_play1 = mysql_query("SELECT * FROM media_images WHERE media_id='".$galleryid."' AND gallery_id!=0");
						
						                    $play_array = array();
						                    if($sql_play1!="" && $sql_play1!=Null)
						                    {
							                    //echo '<br>Number of Images Found = '.mysql_num_rows($sql_play1). ' in gallery id '.$galleryid.'<br>';
                                                if(mysql_num_rows($sql_play1)>0)
							                    {
								                    while($row = mysql_fetch_assoc($sql_play1))
								                    {
									                    $play_array[] = $row;
								                    }
                                                    $GalleryOrgPath[$galleryid]="https://medgallery.s3.amazonaws.com/";
                                                    $GalleryThumbPath[$galleryid]="https://medgalthumb.s3.amazonaws.com/";
                                                    $GalleryImageName[$galleryid]=setGalleryImage($Get_medi_cover_pic['profile_image'],'',$exp_tagged_gallery_info[$galleryid]['title'],"","");
								                    //${'orgPath'.$galleryid}="https://medgallery.s3.amazonaws.com/";
								                    //${'thumbPath'.$galleryid}="https://medgalthumb.s3.amazonaws.com/";
								                    for($l=0;$l<count($play_array);$l++)
								                    {
                                                        $loopmediaid=$play_array[$l]['id'];
                                                        //echo '<br>loop media id = '.$loopmediaid.' <br>';
									                    $mediaSrc = $play_array[0]['image_name'];
									                    $filePath = "https://medgallery.s3.amazonaws.com/".$mediaSrc;
									            
                                                        //echo '<br>file path= '.$filePath.' <br>';
									                    $galleryImagesImageName[$loopmediaid]=$play_array[$l]['image_name'];
                                                        $galleryImagesImageTitle[$loopmediaid]=$play_array[$l]['image_title']; 
                                                        $GalleryImageNames=$GalleryImageNames.",".$galleryImagesImageName[$loopmediaid]=$play_array[$l]['image_name'];
                                                        $GalleryImagetitles=$GalleryImagetitles.",".$galleryImagesImageName[$loopmediaid]=$play_array[$l]['image_title']; 
									                    //${'galleryImagesData'.$galleryid}[$galleryImagesDataIndex] = $play_array[$l]['image_name'];
									                    //${'galleryImagestitle'.$galleryid}[$galleryImagesDataIndex] = $play_array[$l]['image_title'];
									                    //echo '<br> galery image name '.$galleryImagesImageName[$loopmediaid];
                                                        //echo '<br> galery image title '.$galleryImagesImageTitle[$loopmediaid];
									                    $galleryImagesDataIndex++;
                                                        //echo '<br> $galleryImagesDataIndex = '.$galleryImagesDataIndex.'<br>';
								                    } // end of loop processing images
                                                    $GalleryImageNamesList[$galleryid]=$GalleryImageNames;
                                                    $GalleryImageTitlesList[$galleryid]=$GalleryImagetitles;                                                        
							                    } // no images fond
							                    else
							                    { // get images from media_images with gallery id = 0
					                                //echo '<br>get images from media_images with gallery id = 0 for real gallery id = '.$galleryid.' <br>';
                                                    $GalleryOrgPath[$galleryid]="https://medsingleimage.s3.amazonaws.com/";
                                                    $GalleryThumbPath[$galleryid]="https://medgalthumb.s3.amazonaws.com/"; 
                                                    $GalleryImageName[$galleryid]=setGalleryImage($Get_medi_cover_pic['profile_image'],'',$exp_tagged_gallery_info[$galleryid]['title'],"","");
                                                    //${'orgPath'.$galleryid}="https://medsingleimage.s3.amazonaws.com/";
								                    //${'thumbPath'.$galleryid}="https://medgalthumb.s3.amazonaws.com/";
								
								                    $sql_play1 = mysql_query("SELECT * FROM media_images WHERE media_id='".$galleryid."' AND gallery_id=0");
					
                                                    $play_array = array();
								                    if($sql_play1!="" && $sql_play1!=Null)
								                    {
									                    if(mysql_num_rows($sql_play1)>0)
									                    {
										                    while($row = mysql_fetch_assoc($sql_play1))
										                    {
											                    $play_array[] = $row;
										                    }
										
										                    for($l=0;$l<count($play_array);$l++)
										                    {
											                    $loopmediaid=$play_array[$l]['id'];    
                                                                $mediaSrc = $play_array[0]['image_name'];
											                    if($play_array[0]['for_sale']==1)
											                    {
                                                                    $GalleryOrgPath[$galleryid]="https://medgalhighres.s3.amazonaws.com/";
                                                                    $GalleryThumbPath[$galleryid]="https://medgalhighres.s3.amazonaws.com/";
                                                                    $GalleryImageName[$galleryid]=setGalleryImage($Get_medi_cover_pic['profile_image'],'',$exp_tagged_gallery_info[$galleryid]['title'],"","");                                                                        
												                    $filePath = "https://medgalhighres.s3.amazonaws.com/".$mediaSrc;
												                    //${'orgPath'.$galleryid}="https://medgalhighres.s3.amazonaws.com/";
												                    //${'thumbPath'.$galleryid}="https://medgalhighres.s3.amazonaws.com/";
											                    }
											                    else
											                    {
												                    $filePath = "https://medsingleimage.s3.amazonaws.com/".$mediaSrc;
											                    }
									                            $galleryImagesImageName[$loopmediaid]=$play_array[$l]['image_name'];
                                                                $galleryImagesImageTitle[$loopmediaid]=$play_array[$l]['image_title']; 
                                                                $GalleryImageNames=$GalleryImageNames.",".$galleryImagesImageName[$loopmediaid]=$play_array[$l]['image_name'];
                                                                $GalleryImagetitles=$GalleryImagetitles.",".$galleryImagesImageName[$loopmediaid]=$play_array[$l]['image_title']; 											
											                    //${'galleryImagesData'.$galleryid}[$galleryImagesDataIndex] = $play_array[$l]['image_name'];
											                    //${'galleryImagestitle'.$galleryid}[$galleryImagesDataIndex] = $play_array[$l]['image_title'];
											                    //echo $play_array[$l]['image_title'];
											                    //die;
											                    //echo ${'galleryImagesData'.$galleryid}[$galleryImagesDataIndex];
											                    $galleryImagesDataIndex++;
											                    //echo '<br> $galleryImagesDataIndex = '.$galleryImagesDataIndex.'<br>';
										                    } // end of loop getting image names and titles
                                                            $GalleryImageNamesList[$galleryid]=$GalleryImageNames;
                                                            $GalleryImageTitlesList[$galleryid]=$GalleryImagetitles;                                                                
									                    } // no images found
								                    } // no images
							                    } // end of get images from media_images with gallery id = 0
						                    } // no images found
						                    else
						                    { // get images from media_images with gallery id = 0
                                                //echo '<br>get images from media_images with gallery id = 0 for real gallery id = '.$galleryid.' <br>';
                                                $GalleryOrgPath[$galleryid]="https://medsingleimage.s3.amazonaws.com/";
                                                $GalleryThumbPath[$galleryid]="https://medgalthumb.s3.amazonaws.com/";
                                                $GalleryImageName[$galleryid]=setGalleryImage($Get_medi_cover_pic['profile_image'],'',$exp_tagged_gallery_info[$galleryid]['title'],"","");                                                    
							                    //${'orgPath'.$galleryid}="https://medsingleimage.s3.amazonaws.com/";
							                    //${'thumbPath'.$galleryid}="https://medgalthumb.s3.amazonaws.com/";
							
							                    $sql_play1 = mysql_query("SELECT * FROM media_images WHERE media_id='".$galleryid."' AND gallery_id=0");
							                    $play_array = array();
							                    if($sql_play1!="" && $sql_play1!=Null)
							                    {
								                    if(mysql_num_rows($sql_play1)>0)
								                    {
									                    while($row = mysql_fetch_assoc($sql_play1))
									                    {
										                    $play_array[] = $row;
									                    }
									
									                    for($l=0;$l<count($play_array);$l++)
									                    {
                                                            $loopmediaid=$play_array[$l]['id'];
										                    $mediaSrc = $play_array[0]['image_name'];
										                    if($play_array[0]['for_sale']==1)
										                    {
											                    $filePath = "https://medgalhighres.s3.amazonaws.com/".$mediaSrc;
                                                                $GalleryOrgPath[$galleryid]="https://medgalhighres.s3.amazonaws.com/";
                                                                $GalleryThumbPath[$galleryid]="https://medgalhighres.s3.amazonaws.com/";
                                                                $GalleryImageName[$galleryid]=setGalleryImage($Get_medi_cover_pic['profile_image'],'',$exp_tagged_gallery_info[$galleryid]['title'],"","");                                                                    
											                    //${'orgPath'.$galleryid}="https://medgalhighres.s3.amazonaws.com/";
											                    //${'thumbPath'.$galleryid}="https://medgalhighres.s3.amazonaws.com/";
										                    }
										                    else
										                    {
											                    $filePath = "https://medsingleimage.s3.amazonaws.com/".$mediaSrc;
										                    }
									                        $galleryImagesImageName[$loopmediaid]=$play_array[$l]['image_name'];
                                                            $galleryImagesImageTitle[$loopmediaid]=$play_array[$l]['image_title'];	 
                                                            $GalleryImageNames=$GalleryImageNames.",".$galleryImagesImageName[$loopmediaid]=$play_array[$l]['image_name'];
                                                            $GalleryImagetitles=$GalleryImagetitles.",".$galleryImagesImageName[$loopmediaid]=$play_array[$l]['image_title']; 									
										                    //${'galleryImagesData'.$galleryid}[$galleryImagesDataIndex] = $play_array[$l]['image_name'];
										                    //${'galleryImagestitle'.$galleryid}[$galleryImagesDataIndex] = $play_array[$l]['image_title'];
										                    //echo $play_array[$l]['image_title'];
										                    //die;
										                    //echo ${'galleryImagesData'.$galleryid}[$galleryImagesDataIndex];
										                    $galleryImagesDataIndex++;
										                    //echo '<br> $galleryImagesDataIndex = '.$galleryImagesDataIndex.'<br>';
									                    } // end of loop processing images
                                                        $GalleryImageNamesList[$galleryid]=$GalleryImageNames;
                                                        $GalleryImageTitlesList[$galleryid]=$GalleryImagetitles;                                                            
								                    } // no images found
							                    } // no images found
						                    } // end of if 						                        
					                    } // end of if
                                    
                                    break;
                                    
                                    case 'songs_post':
                                    
                                        $media_exp = explode('~',$media);
                                        $mediaid=$media_exp[0];
                                        $get_usermediasong_info[$mediaid] = $new_project_class_obj->get_User_Media_Song($mediaid);
                                        $imagename= $new_project_class_obj->get_audio_img($mediaid);
                                        $songimagefile[$mediaid]=$imagename;                                        
			                            $songbyprofileurl[$mediaid] = $new_project_class_obj->get_ProfileURLfromCreatorInfo($get_usermediasong_info[$mediaid]['creator_info']);                                        
                                        $songfromprofileurl[$mediaid] = $new_project_class_obj->get_ProfileURLfromFromInfo($get_usermediasong_info[$mediaid]['from_info']);
                                    break; 
                                }

                                $k++;
	                        }
                        }
                    }
                }
            }
        }
    }
    
    // Sort POSTS descending by ID
    array_sort_by_column($get_posts_for_all_artists,'id',SORT_DESC);    
    
    $result[] = array('postsforallartists'=>$get_posts_for_all_artists);
    $result[] = array('artistproject'=>$get_project);
    $result[] = array('artistevents'=>$get_artist_event);
    $result[] = array('communityevents'=>$get_community_event);
    $result[] = array('mediavideo'=>$exp_tagged_videos_info); 
    $result[] = array('videoimagefile'=>$videoimagefile);
    $result[] = array('profileurlforvideo'=>$profileurlforvideo);
    $result[] = array('galleries'=>$exp_tagged_gallery_info);
    $result[] = array('GalleryImageName'=>$GalleryImageName);
    $result[] = array('artistprofileurl'=>$artistprofileurl);
    $result[] = array('GalleryOrgPath'=>$GalleryOrgPath);
    $result[] = array('GalleryThumbPath'=>$GalleryThumbPath);
    $result[] = array('GalleryImageNamesList'=>$GalleryImageNamesList);
    $result[] = array('GalleryImageTitlesList'=>$GalleryImageTitlesList);
    $result[] = array('usermediasong'=>$get_usermediasong_info); 
    $result[] = array('songimagefile'=>$songimagefile);
    $result[] = array('songbyprofileurl'=>$songbyprofileurl);
    $result[] = array('songfromprofileurl'=>$songfromprofileurl);
    $result[] = array('generalartist'=>$generalartist);
    $result[] = array('featuredMediaAlbum'=>$featuredMediaAlbum);

} //end of if error == no

/* output in necessary format */
if($format == 'json') {
    header('Content-type: application/json');
    echo json_encode($result);
    //echo json_encode(array('artistprofile'=>$result));
    //echo array_to_json($result);
}
else {
    if ($format == 'xml'){
        header('Content-type: text/xml');
        echo '<result>';
        foreach($result as $index => $resultentry) {
            if(is_array($resultentry)) {
                foreach($resultentry as $key => $value) {
                    echo '<',$key,'>';
                    if(is_array($value)) {
                        foreach($value as $tag => $val) {
                            echo '<',$tag,'>',htmlentities($val),'</',$tag,'>';
                        }
                    }
                    echo '</',$key,'>';
                }
            }
        }
        echo '</result>';
    }
}

function array_to_json( $array ){

    if( !is_array( $array ) ){
        return false;
    }

    $associative = count( array_diff( array_keys($array), array_keys( array_keys( $array )) ));
    if( $associative ){

        $construct = array();
        foreach( $array as $key => $value ){

            // We first copy each key/value pair into a staging array,
            // formatting each key and value properly as we go.

            // Format the key:
            if( is_numeric($key) ){
                $key = "key_$key";
            }
            $key = "\"".addslashes($key)."\"";

            // Format the value:
            if( is_array( $value )){
                $value = array_to_json( $value );
            } else if( !is_numeric( $value ) || is_string( $value ) ){
                $value = "\"".addslashes($value)."\"";
            }

            // Add to staging array:
            $construct[] = "$key: $value";
        }

        // Then we collapse the staging array into the JSON form:
        $result = "{ " . implode( ", ", $construct ) . " }";

    } else { // If the array is a vector (not associative):

        $construct = array();
        foreach( $array as $value ){

            // Format the value:
            if( is_array( $value )){
                $value = array_to_json( $value );
            } else if( !is_numeric( $value ) || is_string( $value ) ){
                $value = "'".addslashes($value)."'";
            }

            // Add to staging array:
            $construct[] = $value;
        }

        // Then we collapse the staging array into the JSON form:
        $result = "[ " . implode( ", ", $construct ) . " ]";
    }
	
    return $result;
}
function print_r_tree($data)
{
    // capture the output of print_r
    $out = print_r($data, true);

    // replace something like '[element] => <newline> (' with <a href="javascript:toggleDisplay('...');">...</a><div id="..." style="display: none;">
    $out = preg_replace('/([ \t]*)(\[[^\]]+\][ \t]*\=\>[ \t]*[a-z0-9 \t_]+)\n[ \t]*\(/iUe',"'\\1<a href=\"javascript:toggleDisplay(\''.(\$id = substr(md5(rand().'\\0'), 0, 7)).'\');\">\\2</a><div id=\"'.\$id.'\" style=\"display: none;\">'", $out);

    // replace ')' on its own on a new line (surrounded by whitespace is ok) with '</div>
    $out = preg_replace('/^\s*\)\s*$/m', '</div>', $out);

    // print the javascript function toggleDisplay() and then the transformed output
    echo '<script language="Javascript">function toggleDisplay(id) { document.getElementById(id).style.display = (document.getElementById(id).style.display == "block") ? "none" : "block"; }</script>'."\n$out";
}
function cleanstring($stringin)
{
   $sanitized = strtolower(preg_replace('/[^a-zA-Z0-9-_\.]/','', $stringin)); 
    return ($sanitized);
}
function setGalleryImage($galleryimage,$gallerygallerytitle,$gallerytitle,$register_cover_pic,$register_No_pic)
{
    $gallery_image_name_cart="";    
    if($gallerygallerytitle !="" || $gallerytitle!="")
    {
        if($galleryimage !="" && $galleryimage !=null )
		{
			$gallery_image_name_cart = $galleryimage;
		}
		else{
			$gallery_image_name_cart = "https://medgalthumb.s3.amazonaws.com/Noimage.png";
		}
	}
	else
	{
		if($register_cover_pic=="")
		{
			$gallery_image_name_cart = "https://reggalthumb.s3.amazonaws.com/".$register_No_pic;
		}
		else
		{
			$gallery_image_name_cart ="https://reggalthumb.s3.amazonaws.com/".$register_cover_pic;
		}
	}
    return($gallery_image_name_cart);
}
function array_sort_by_column(&$arr, $col, $dir = SORT_ASC) {
    $sort_col = array();
    foreach ($arr as $key=> $row) {
        $sort_col[$key] = $row[$col];
    }

    array_multisort($sort_col, $dir, $arr);
}

?>