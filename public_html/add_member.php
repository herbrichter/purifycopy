<?php
session_start();
include("classes/GetFanClubMember.php");
include_once('sendgrid/SendGrid_loader.php');
$sendgrid = new SendGrid('','');
?>
<html>
<head>
<?php
$get_fan_club= new GetFanClubMember();
$get_gen_info = $get_fan_club->Get_loggedin_Info();
$get_art_info = $get_fan_club->Get_Artist_Info($get_gen_info['artist_id']);
$get_comm_info = $get_fan_club->Get_community_Info($get_gen_info['community_id']);
?>
<!--<script src='http://ajax.googleapis.com/ajax/libs/jquery/1.6.1/jquery.min.js'></script>-->
	<script>
		
		function validate_fields()
		{
			var email = document.getElementById("add_member_email_address").value;
			//var name = document.getElementById("name").value;
			var atpos = email.indexOf("@");
			var dotpos = email.lastIndexOf(".");
			if (atpos<1 || dotpos<atpos+2 || dotpos+2>=email.lenght || email=="" || email==null)
			{
				alert("Please enter a valid email address.");
				return false;
			}
		/*	if(name=="" || name==null)
			{
				alert("Please enter a your name.");
				return false;
			}*/
			 
			
			// if(check_vid=="")
			// {
				// alert("Please enter your Email Id.");
				// return false;
			// }
			var check_vid = $("#member_email").html();
			if(check_vid == "You are already fan of this user.")
			{
				alert("You are already fan of this user.");
				return false;
			}
			//return false;
		}
		

		$("document").ready(function(){
			$("#add_member_email_address").focusout(function(){
				var text_value = document.getElementById("add_member_email_address").value;
			//alert(text_value);
				var match_db_id = document.getElementById("rel_id").value;
				var match_db_text = document.getElementById("rel_type").value;
				//alert(match_db_text);
				var text_data = 'reg='+ text_value+'&'+'match_id='+ match_db_id+'&'+'match_text='+ match_db_text;
				 
				$.ajax({	 
					type: 'POST',
					url : 'MembershipAjaxValidations.php',
					data: text_data,
					success: function(data) 
					{
						$("#member_email").html(data);
					}
				});
			});
		});
		
	$("document").ready(function(){
			$("#select_type").change(function(){
				var text_value = document.getElementById("select_type").value;
				var text_value1 = $('#select_type option:selected').attr('name');
				document.getElementById("rel_type").value = text_value;
				
				var text_data = 'reg='+ text_value + '&reg_id='+ text_value1;
				$.ajax({	 
					type: 'POST',
					url : 'member_info.php',
					data: text_data,
					success: function(data) 
					{
						if(data != "")
						{
							document.getElementById("create_member").style.display="block";
							document.getElementById("err_message").style.display="none";
							document.getElementById("rel_id").value = data;
						}
						else
						{
							document.getElementById("err_message").style.display="block";
							document.getElementById("create_member").style.display="none";
						}
					}
				});
			});
		});		
	</script>
	<script type="text/javascript">
		$("#add_member_form").bind("submit", function() {
			$.ajax({
				type        : "POST",
				cache       : false,
				url         : "fan_club.php",
				data        : $(this).serializeArray(),
				success     :function(data){
								//$.fancybox(data);
								$("#add_member_outer").html(data);
							}
			});
			return false;
		});
</script>

<link rel="stylesheet" type="text/css" media="screen" href="includes/purify.css" />
<!--Files For Displaying Autocompleter
<script type="text/javascript" src="javascripts/jquery.js"></script>
<script type="text/javascript" src="javascripts/jquery.autocomplete.js"></script>
<link rel="stylesheet" href="css/jquery.autocomplete.css" type="text/css" />
<script src="ui/jquery-1.7.2.js"></script>
<script src="ui/jquery.ui.core.js"></script>
<script src="ui/jquery.ui.widget.js"></script>
<link  rel="stylesheet" type="text/css" media="screen" href="css/jquery.ui.autocomplete.css"></link>
<link rel="stylesheet" href="includes/jquery.ui.all.css">
<script type="text/javascript" src="javascripts/jquery.ui.autocomplete.js"></script>
<script type="text/javascript" src="javascripts/jquery.ui.position.js"></script>
<!--Files For Displaying Autocompleter Ends Here-->
</head>
<body>
<div >
	<div class="fancy_header">Add Member</div>
	<div id="add_member_outer" class="fancy_box_content_addmember">
	<div id="actualContent" style="width:323px;">
	<?php
	if($_POST == null)
	{
	?>
		<div class="fieldCont" style="width: 323px;">
			<div class="fieldTitle">Select Fan Club</div>
			<select class="dropdown" id="select_type">
				<option value="0">Select</option>
				<?php
				if($get_art_info!= null && $get_art_info != "no")
				{
				?>
					<option value="artist"><?php echo $get_art_info['name']." Fan"; ?></option>
				<?php
				}
				if($get_comm_info!= null && $get_comm_info != "no")
				{
				?>
					<option value="community"> <?php echo $get_comm_info['name']." Fan";?> </option>
				<?php
				}
				$get_art_pro = $get_fan_club->get_all_artist_project($get_gen_info['artist_id']);
				if(!empty($get_art_pro)){
					for($count_pro=0;$count_pro<=count($get_art_pro);$count_pro++){
						if($get_art_pro[$count_pro]!=""){
				?>
							<option value="artist_project" name ="<?php echo $get_art_pro[$count_pro]['id'];?>"> <?php echo $get_art_pro[$count_pro]['title']." Fan";?> </option>
				<?php
						}
					}
				}
				$get_comm_pro = $get_fan_club->get_all_community_project($get_gen_info['community_id']);
				if(!empty($get_comm_pro)){
					for($count_pro=0;$count_pro<=count($get_comm_pro);$count_pro++){
						if($get_comm_pro[$count_pro]!=""){
				?>
							<option value="community_project" name ="<?php echo $get_comm_pro[$count_pro]['id'];?>"> <?php echo $get_comm_pro[$count_pro]['title']." Fan";?> </option>
				<?php
						}
					}
				}
				
				?>	
			</select>
		</div>
		<div id="create_member" style="display:none;">
			<form id="add_member_form" action="" method="POST" onSubmit="return validate_fields()" >
					<!--<div class="fieldCont" style="width: 323px;">
						<div class="fieldTitle">Name</div>
						<input type="text" name="name" id="name" class="fieldText" />
						<div class="hint" id="user_error" style="display:none;">Please Enter Only Registered Users.</div>
					</div>-->
					<div class="hintR" id="member_email"></div>
					<div class="fieldCont" style="width: 323px;">
						<div class="fieldTitle">Email</div>
						<input type="text" name="email" id="add_member_email_address" class="fieldText" />
					</div>
					
					
						<input type="hidden" name="rel_id" id="rel_id" class="fieldText" />
						<input type="hidden" name="rel_type" id="rel_type" class="fieldText" />
					<div class="fieldCont" style="width: 323px;margin:0;">
						<div class="fieldTitle"></div>
						<input type="submit" value="submit" id="member_button" class="round_black_button"/>
					</div>
			</form>
		</div>
		<div id="err_message" style="display:none;">
			<p style="border-top:none;">This Facility Is Not Present For This User.</p>
		</div>
	</div>	
	</div>	
</div>

<?php
}



if(isset($_POST) && $_POST != null)
{
	$row = $get_fan_club->get_related_id($_POST['rel_type']);
	//var_dump($row);
	//die;
	$purchase_date = date('Y-m-d');
	$expiry_date = strtotime(date("Y-m-d", strtotime($purchase_date)) . " +1 year");
	$expiry_date = date('Y-m-d', $expiry_date);
	//echo $expiry_date;
	
	$sql_check_same_fan = $get_fan_club->same_user_check($_POST['email']);///////////// 0:-Same user and 1:-diff user
	if($sql_check_same_fan==0)
	{
		echo "You are this user only.";
	}
	else
	{
		$add_fan = $get_fan_club->become_a_fan($_POST['email'],$_POST['rel_id'],$_POST['rel_type'],$purchase_date,$expiry_date);
		
		$sql_subscrb = $get_fan_club->become_a_subscrb($_POST['email'],$_POST['rel_id'],$_POST['rel_type'],$purchase_date,$expiry_date);

		//$sql = $new_radio_obj->address_book_chng($setting,$_POST['from'],$_POST['to'],$_POST['type']);
		
		echo "You have successfully added ".$_POST['email']." as a member. They have been sent a mail to verify their membership.";
		
		$to = $_POST['email'];
		$subject = "Fan Club Membership Successfull.";
		
		$chk_registered = $get_fan_club->chk_registered($_POST['email']);
		if($chk_registered==1)			//////// 0:-Not Registered and 1:-Registered User
		{	
			$sql_type_mail = $get_fan_club->get_general_user_id($_POST['rel_id'],$_POST['rel_type']);
			if($_POST['rel_type']=='artist')
			{
				//$message = "Hello ".$_POST['name'].","."\n \n"."\t\tYou successfully became Fan of Artist Profile of '".$sql_type_mail['fname']."'.\n\n\t\tTo view your upgraded Profile please Login To www.purifyart.net.";
				$message = "Hello ,"."\n \n"."\t\tYou successfully became Fan of Artist Profile of '".$sql_type_mail['fname']."'.\n\n\t\tTo view your upgraded Profile please Login To www.purifyart.com.";
			}
			if($_POST['rel_type']=='community')
			{
				//$message = "Hello ".$_POST['name'].","."\n \n"."\t\tYou successfully became Fan of Community Profile of '".$sql_type_mail['fname']."'.\n\n\t\tTo view your upgraded Profile please Login To www.purifyart.net.";
				$message = "Hello ,"."\n \n"."\t\tYou successfully became Fan of Community Profile of '".$sql_type_mail['fname']."'.\n\n\t\tTo view your upgraded Profile please Login To www.purifyart.com.";
			}
		}
		else
		{
			$sql_type_mail = $get_fan_club->get_general_user_id($_POST['rel_id'],$_POST['rel_type']);
			if($_POST['rel_type']=='artist')
			{
				//$message = "Hello ".$_POST['name'].","."\n \n"."\t\tYou successfully became Fan of Artist Profile of '".$sql_type_mail['fname']."'.\n\n\t\tTo view your upgraded Profile please Sign Up and Login To www.purifyart.net.";
				//$message = "Hello ,"."\n \n"."\t\tYou successfully became Fan of Artist Profile of '".$sql_type_mail['fname']."'.\n\n\t\tTo view your upgraded Profile please Sign Up and Login To www.purifyart.net.";
				$get_profile_name = $get_fan_club->get_profile_title($_POST['rel_type'],$_POST['rel_id']);
				if($_POST['rel_type'] == "artist" || $name == "community"){ $pro_name = $get_profile_name['name'];}
				if($_POST['rel_type'] == "artist_project" || $name == "community_project"){ $pro_name = $get_profile_name['title'];}
				$message = "Thank you for purchasing a fan club membership to ".$pro_name.". In order to view and download the media included in this fan club membership you need to register for Purify Art. Once you register your new media will be in your media library.";
			}
			if($_POST['rel_type']=='community')
			{
				//$message = "Hello ".$_POST['name'].","."\n \n"."\t\tYou successfully became Fan of Community Profile of '".$sql_type_mail['fname']."'.\n\n\t\tTo view your upgraded Profile please Sign Up and Login To www.purifyart.net.";
				//$message = "Hello ,"."\n \n"."\t\tYou successfully became Fan of Community Profile of '".$sql_type_mail['fname']."'.\n\n\t\tTo view your upgraded Profile please Sign Up and Login To www.purifyart.net.";
				$get_profile_name = $get_fan_club->get_profile_title($_POST['rel_type'],$_POST['rel_id']);
				if($_POST['rel_type'] == "artist" || $name == "community"){ $pro_name = $get_profile_name['name'];}
				if($_POST['rel_type'] == "artist_project" || $name == "community_project"){ $pro_name = $get_profile_name['title'];}
				$message = "Thank you for purchasing a fan club membership to ".$pro_name.". In order to view and download the media included in this fan club membership you need to register for Purify Art. Once you register your new media will be in your media library.";
			}
		}
		
		$from = "Purify Art < no-reply@purifyart.com>";
		$headers = "From:" . $from;
		
		$url = "admin/newsletter/"; 
		$myFile = $url . "other_templates.txt";
		$fh = fopen($myFile, 'r');
		$theData = fread($fh, filesize($myFile));
		fclose($fh);
		
		$data_email = explode('|~|', $theData);
		
		$ns_user_n = str_replace('{::message::}', $message, $data_email['1']);
		
		$body_email = $data_email['0'] . $ns_user_n . $data_email['2'];
		
		$mail_grid = new SendGrid\Mail();
		
		$mail_grid->addTo($to)->
					setFromName('Purify Art')->
					setFrom('no-reply@purifyart.com')->
					setSubject($subject)->
					setHtml($body_email);
					
			//$sendgrid->web->send($mail_grid);
			$sendgrid -> smtp -> send($mail_grid);
		//mail($to,$subject,$message,$headers);
	
		
		/*$fan_to_user = $sql_type_mail['email'];
		$fan_subject = "User became your fan.";
		$fan_message = "Hello ".$sql_type_mail['fname'].","."\n\n\t\tUser '".$_POST['name']."' became Fan of your's ".$_POST['rel_type']." profile successfully.";
		
		mail($fan_to_user,$fan_subject,$fan_message,$headers);*/
	}
}
?>
<!--<style>
	.ui-autocomplete-loading { background: white url('css/ui-anim_basic_16x16.gif') right center no-repeat; }
</style>
<script>
$(function() {
	$( "#name" ).autocomplete({
		source: function(request, response) {
		$.ajax({
		  url: "general_user_suggession.php",
		  dataType: "json",
		  data: request,                    
		  success: function (data) {
			  if (data.length == 0) {
					$("#user_error").css({"display" : "block","color" : "red","width":"200px","position":"relative","left":"107px"});
					document.getElementById("name").value="";
					$(".ui-autocomplete-loading").css({"background" : "none"});
				}
				else {
					$("#user_error").css({"display" : "none"});
				$(".ui-autocomplete-loading").css({"background" : "none"});
				  response(data);
				}
		  }});
		},
		minLength: 1,
		select: function( event, ui ) {
		document.getElementById("email").value=ui.item.value1;
		}
	});
});
</script>-->
</body>
</html>
