<?php
session_start(); 
include_once("commons/db.php");
include('classes/viewCommunityProject.php');

$new_obj = new viewCommunityProject();
$get_pro = $new_obj->get_CommunityProject_new($_GET['id']);
//var_dump($get_pro);
?>
<link rel="stylesheet" type="text/css" media="screen" href="includes/purify.css" />
<?php
if($get_pro['type'] =="fan_club")
{
	$count_song=$count_video=$count_gal=$count_chan=0;
	$strem_song=$down_song=$strem_Channel=$down_Channel=$strem_video=$down_video=$strem_gal=$down_gal=0;
	$exp_fan_song = explode(',',$get_pro['fan_song']);
	$exp_fan_gallery = explode(',',$get_pro['fan_gallery']);
	$exp_fan_video = explode(',',$get_pro['fan_video']);
	$exp_fan_channel = explode(',',$get_pro['fan_channel']);
	$strem_song = 0;
	$down_song = 0;
	for($count_s = 0;$count_s < count($exp_fan_song);$count_s++)
	{
		if($exp_fan_song[$count_s]=="" || $exp_fan_song[$count_s]==0){
			continue;}
		else{
		$res = $new_obj->get_songtypes($exp_fan_song[$count_s]);
		if($res['sharing_preference']==3){
			$strem_song = $strem_song + 1;}
		if($res['sharing_preference']==4){
			$down_song = $down_song + 1;}
			$count_song +=1;
		}
	}
	for($count_g = 0;$count_g < count($exp_fan_gallery);$count_g++)
	{
		if($exp_fan_gallery[$count_g]=="" || $exp_fan_gallery[$count_g]==0){
			continue;}
		else{
		$res = $new_obj->get_songtypes($exp_fan_gallery[$count_g]);
		if($res['sharing_preference']==3){
			$strem_gal = $strem_gal + 1;}
		if($res['sharing_preference']==4){
			$down_gal = $down_gal + 1;}
			$count_gal +=1;
		}
	}
	for($count_v = 0;$count_v < count($exp_fan_video);$count_v++)
	{
		if($exp_fan_video[$count_v]=="" || $exp_fan_video[$count_v]==0){
			continue;}
		else{
		$res = $new_obj->get_songtypes($exp_fan_video[$count_v]);
		if($res['sharing_preference']==3){
			$strem_video = $strem_video + 1;}
		if($res['sharing_preference']==4){
			$down_video = $down_video + 1;}
			$count_video +=1;
		}
	}
	for($count_c = 0;$count_c < count($exp_fan_channel);$count_c++)
	{
		if($exp_fan_channel[$count_c]=="" || $exp_fan_channel[$count_c]==0){
			continue;}
		else{
		$res = $new_obj->get_songtypes($exp_fan_channel[$count_c]);
		if($res['sharing_preference']==3){
			$strem_channel = $strem_channel + 1;}
		if($res['sharing_preference']==4){
			$down_channel = $down_channel + 1;}
			$count_chan +=1;
		}
	}
?>

<div id="mediaContent">
	<div class="titleCont" style="position:relative;left:125px;width:545px">
		<div style="width:65px" class="blkG">S Song</div>
		<div style="width:65px" class="blkB">D Song</div>
		<div style="width:65px" class="blkG">S Video</div>
		<div style="width:65px" class="blkB">D Video</div>
		<div style="width:65px" class="blkG">S Gallery</div>
		<div style="width:65px" class="blkB">D Gallery</div>
		<div style="width:65px" class="blkG">S Channel</div>
		<div style="width:65px" class="blkB">D Channel</div>
	</div>
	<div style="border-bottom:none;" class="tableCont">
		<div class="tableCont" id="AccordionContainer" style="position:relative;left:120px;width:540px;">
			<div class="blkA" style="width:65px;"><?php echo $strem_song;?></div>
			<div class="blkB" style="width:65px;"><?php echo $down_song;?></div>
			<div class="blkC" style="width:65px;"><?php echo $strem_video;?></div>
			<div class="blkD" style="width:65px;"><?php echo $down_video;?></div>
			<div class="blkE" style="width:65px;"><?php echo $strem_gal;?></div>
			<div class="blkF" style="width:65px;"><?php echo $down_gal;?></div>
			<div class="blkG" style="width:65px;"><?php echo $strem_Channel;?></div>
			<div class="blkH" style="width:65px;"><?php echo $down_Channel;?></div>
		</div>
	</div>
</div>
<?php
}
if($get_pro['type'] =="for_sale")
{
	$count_song=$count_gal=0;
	$down_song = $down_gal=0;
	$exp_sale_song = explode(',',$get_pro['sale_song']);
	$exp_sale_gallery = explode(',',$get_pro['sale_gallery']);
	$down_song = 0;
	for($count_s = 0;$count_s < count($exp_sale_song);$count_s++)
	{
		if($exp_sale_song[$count_s]=="" || $exp_sale_song[$count_s]==0){
			continue;}
		else{
		$res = $new_obj->get_songtypes($exp_sale_song[$count_s]);
		if($res['sharing_preference']==5){
			$down_song = $down_song + 1;}
			$count_song += 1;
		}
	}
	for($count_g = 0;$count_g < count($exp_sale_gallery);$count_g++)
	{
		if($exp_sale_gallery[$count_g]=="" || $exp_sale_gallery[$count_g]==0){
			continue;}
		else{
		$res = $new_obj->get_songtypes($exp_sale_gallery[$count_g]);
		if($res['sharing_preference']==5){
			$down_gal = $down_gal + 1;}
			$count_gal += 1;
		}
	}
?>
<div id="mediaContent">
	<div class="titleCont" style="position:relative;left:125px;width:545px">
		<div style="width:200px" class="blkG">Downloading Song</div>
		<div style="width:200px" class="blkG">Downloading Gallery</div>
	</div>
	<div style="border-bottom:none;" class="tableCont">
		<div class="tableCont" id="AccordionContainer" style="position:relative;left:120px;width:540px;">
			<div class="blkB" style="width:200px;"><?php echo $down_song;?></div>
			<div class="blkF" style="width:200px;"><?php echo $down_gal;?></div>
		</div>
	</div>
</div>
<?php
}
?>