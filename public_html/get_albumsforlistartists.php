<?php
$result = array();
$errorflag='no';

$errormsg=array();
$inputparams=array();
$username='nouser';
$userpass='nopass';
$listartists='leerick,kaz,hadley';
$format='json';

$data = file_get_contents("php://input");
$dataary=json_decode($data,true);

if (isset($dataary['format'])){    
    $format = strtolower($dataary['format']);
}
if(isset($dataary['listartists'])) {
    $listartists=$dataary['listartists'];
}else {
    $errormessage='missing profile';
    $errormsg[]=array('errormsg'=>$errormessage);
    $result[] = array('error'=>$errormsg);
    $inputparams[] = array('listartists'=>$listartists);
    $inputparams[] = array('format'=>$format);
    $inputparams[] = array('post'=>$dataary);
    $result[] = array('inputparams'=>$inputparams);
    $errorflag='yes';
}

if ($errorflag='no'){
    include_once("commons/db.php");
    include_once('classes/Artist.php');
    include_once('classes/Media.php');
    include_once('classes/CountryState.php');
    include_once('classes/Type.php');
    include_once('classes/SubType.php');
    include_once('classes/MetaType.php');
    include_once("classes/viewArtistProjectURL.php");    
    include_once('classes/ProfileDisplay.php');    
    include_once("classes/ViewArtistProfileURL.php");    
    
    // add user name to bucketname in s3 url
    $root = realpath($_SERVER["DOCUMENT_ROOT"]);
    if (!class_exists('S3')) require_once ($root.'/S3.php');    
    $s3 = new S3("", "");
  
    $new_project_class_obj = new viewArtistProjectURL();
    //$new_profile_class_obj = new ViewArtistProfileURL();
    $date = date('Y-m-d H:i:s');
    $display = new ProfileDisplay();    
    $foundartists=",";
    $foundalbums=",";
    
    $listartists_exp = explode(',',$listartists);
    //echo '<br> exp listartists_exp <br>';
    //print_r($listartists_exp);    
    $outputalbumindex=0;
    if(isset($listartists_exp) && count($listartists_exp)>0 && !empty($listartists_exp))
    {
        $tagged_profileurls="";
        for($artistindex=0;$artistindex<count($listartists_exp);$artistindex++)
        {               
            //echo '<br>getting profileurl='.$listartists_exp[$artistindex].' <br>';     
            $pos=strpos($foundartists,$listartists_exp[$artistindex]);
            if ($pos==false)
            {                            
                $foundartists=$foundartists.','.$listartists_exp[$artistindex];                           
                if($listartists_exp[$artistindex]!="")
                { 
                    $artistprofileurl=$listartists_exp[$artistindex];
                    
                    If($artistindex==0)
                    {
                        $objArtist=new Artist();
                        $objMedia=new Media();
                        $display = new ProfileDisplay();
                        $artist_check = $display->artistCheck($artistprofileurl);
    
                        $new_profile_class_obj = new ViewArtistProfileURL();
                        $getgeneral =  $new_profile_class_obj->get_user_info($artist_check['artist_id']);
	                    $result[] = array('generaluser'=>$getgeneral);
    
                        //$get_user_delete_or_not = $new_profile_class_obj->chk_user_delete($artist_check['artist_id']);
                        //$get_common_id=$artist_check['artist_id'];
    
    
                        ////var_dump($getgeneral);
                        $getuser = $new_profile_class_obj->get_user_artist_profile($artist_check['artist_id']);
                        $result[] = array('generalartist'=>$getuser);
                        $getcountry = $new_profile_class_obj->get_user_country($artist_check['artist_id']);
                        $result[] = array('country'=>$getcountry);
                        $getstate = $new_profile_class_obj->get_user_state($artist_check['artist_id']);
                        $result[] = array('state'=>$getstate);
                        $gettype = $new_profile_class_obj->get_artist_user_type($artist_check['artist_id']);
                        $result[] = array('type'=>$gettype);
                        ////$get_type_dis = $new_profile_class_obj->get_artist_user($artist_check['artist_id']);
                        ////$result[] = array('get_type_dis'=>$get_type_dis);
	
                        $get_subtype = $new_profile_class_obj->get_artist_user_subtype($artist_check['artist_id']);
                        $result[] = array('subtype'=>$get_subtype);
                        ////$get_subtype_dis = $new_profile_class_obj->get_artist_user_subs($artist_check['artist_id']);
                        ////$result[] = array('get_subtype_dis'=>$get_subtype_dis);
	
                        ////var_dump($get_subtype);
                        $get_metatype = $new_profile_class_obj->get_artist_user_metatype($artist_check['artist_id']);
                        $result[] = array('metatype'=>$get_metatype);
                        //$src = "http://artjcropprofile.s3.amazonaws.com/";
                        //$src_list = "http://artjcropthumb.s3.amazonaws.com/";
                        //$src1 = "../";
    
                        //echo '<br> about to get all projects <br>';
                        
                    
                    
                    
                    
                    
                    
                    //echo '<br>getting user artist for artisturl='.$listartists_exp[$artistindex].' <br>'; 
                    }
                    
                    
                    $userartist =$new_project_class_obj->get_user_artist_profileurl($artistprofileurl);
                    //echo '<br> userartist <br>';
                    //print_r($userartist);                   
                    
                    $exp_tagged_albums=explode(',',$userartist['taggedprojects']);
                    
                    //echo '<br> exp tagged albums <br>';
                    //print_r($exp_tagged_albums);
                    
	                $artist_project_check = $display->artistProjectCheck($artistprofileurl);
                    $artistid=$userartist['artist_id'];           

                    if(isset($exp_tagged_albums) && count($exp_tagged_albums)>0)
                    {
                        for($tagalbumindex=0;$tagalbumindex<count($exp_tagged_albums);$tagalbumindex++)
                        {
                            //echo '<br>tagalbumindex='.$tagalbumindex.' Checking for duplicate album. foundalbums='.$foundalbums.' This album='.$exp_tagged_albums[$tagalbumindex].'<br>';
                            $pos=strpos($foundalbums,','.$exp_tagged_albums[$tagalbumindex]);
                            if ($pos===false)
                            {                            
                                $foundalbums=$foundalbums.','.$exp_tagged_albums[$tagalbumindex];                      
                        
                                //echo '<br> tagalbumindex '.$tagalbumindex.' tagged album ' .$exp_tagged_albums[$tagalbumindex].'<br>';
                                if($exp_tagged_albums[$tagalbumindex]==""||$exp_tagged_albums[$tagalbumindex]==" "||$exp_tagged_albums[$tagalbumindex]==Null){continue;}
                                else
                                {  
                                    $project_exp_n = explode('~',$exp_tagged_albums[$tagalbumindex]);
                                    //echo '<br> $project_exp_n <br>';
                                    //print_r($project_exp_n);
                                    if($project_exp_n[1]!='art')
                                    { // not a project so skip it
                                        continue;
                                    }
                                    $artistprojectid=$project_exp_n[0];    
                                    $artistproject_info = mysql_fetch_assoc($new_project_class_obj->get_aproject_count($artistprojectid));                                    

                                    $gettype_info = $new_project_class_obj->getType($artistprojectid);
	                                $getsubtype_info = $new_project_class_obj->getSubType($artistprojectid);
                                    //echo 'CHECKING artistprojectid='.$artistprojectid.'checking type='.$gettype_info['name'].' subtype='.$getsubtype_info['name'].' album title='.$artistproject_info['title'];
                                    if($gettype_info['name']=="Music" && $getsubtype_info['name']=="Album")
                                    {
                                        //$artistprofileurls[$artistprojectid]=$artistprofileurl;
                                        $ProjectCreatorsInfo=$artistproject_info['creators_info'];
                                        $ProjectCreatorsProfileURL[$artistprojectid]=$new_profile_class_obj->get_creators_profileURL($ProjectCreatorsInfo);
	                                    //$getcountry[$artistprojectid] = $new_project_class_obj->getCountryforCountryid($artistproject_info['country_id']);
                                        //$getstate[$artistprojectid] = $new_project_class_obj->getStateforStateid($artistproject_info['state_id']);
	                                    //$get_type_dis[$artistprojectid] = $new_project_class_obj->getMinType($id);
	                                    //$get_subtype_dis[$artistprojectid] = $new_project_class_obj->getMinType_subs($id);
	
	                                    //$get_metatype[$artistprojectid] = $new_project_class_obj->getMetaType($artistprojectid);                                                                         
                                    
                                        $get_artistproject[$outputalbumindex]=$artistproject_info; 
                                        
                                        $AlbumProfileURL=$artistproject_info['profile_url'];
                                        $featuredMediaAlbum[$AlbumProfileURL]['featured_media']=$artistproject_info['featured_media'];
                                        $featuredMediaAlbum[$AlbumProfileURL]['media_id']=$artistproject_info['media_id'];
    
                                        if($featuredMediaAlbum[$AlbumProfileURL]['featured_media']=="Song"){
                                            $mediaid=$featuredMediaAlbum[$AlbumProfileURL]['media_id'];
                                            $get_media_song=$new_profile_class_obj->get_Media_Song($mediaid);
                                            $generaluserid= $get_media_song["general_user_id"];
                                            //echo '<br>generaluserid='.$generaluserid;
                                            $s3->resetAuth("", "",false,$generaluserid);
                                            $medaudioURI=$s3->getNewURI("medaudio"); 
                                            $songname=$get_media_song["song_name"];
                                            $songimageurl=$new_profile_class_obj->get_audio_img($mediaid);
                                            $featuredMediaAlbum[$AlbumProfileURL]["song_title"]=$get_media_song["songtitle"];
                                            $featuredMediaAlbum[$AlbumProfileURL]["song_url"]=$medaudioURI.$songname;
                                            $featuredMediaAlbum[$AlbumProfileURL]["creator_name"]=$get_media_song["creator"];
                                            $featuredMediaAlbum[$AlbumProfileURL]["from_name"]=$get_media_song["from"];
                                            $featuredMediaAlbum[$AlbumProfileURL]["song_image_url"]=$s3->replaceURI($songimageurl);
                                            $featuredMediaAlbum[$AlbumProfileURL]["for_sale"]=$get_media_song["for_sale"];
                                            $featuredMediaAlbum[$AlbumProfileURL]["price"]=$get_media_song["price"];
                                            $featuredMediaAlbum[$AlbumProfileURL]["generaluserid"]=$generaluserid;
                                        } else {
                                            if($featuredMediaAlbum[$AlbumProfileURL]['featured_media']=="Video"){
                                                $mediaid=$featuredMediaAlbum[$AlbumProfileURL]['media_id'];
                                                $get_media_video=$new_profile_class_obj->get_Media_Video($mediaid); 
                                                $generaluserid= $get_media_video["general_user_id"];
                                                //echo '<br>generaluserid='.$generaluserid;
                                                $s3->resetAuth("", "",false,$generaluserid);
                                                $medaudioURI=$s3->getNewURI("medaudio"); 
                                                $featuredMediaAlbum[$AlbumProfileURL]["videotitle"]=$get_media_video["videotitle"];
                                                $featuredMediaAlbum[$AlbumProfileURL]["videolink"]=$get_media_video["videolink"];
                                                $featuredMediaAlbum[$AlbumProfileURL]["generaluserid"]=$generaluserid;
                                            }
                                        }   
                                        
                                        $outputalbumindex++;                                     
                                    }
                                    else
                                    { // not a music album  
                                        //echo 'SKIPPING artistprojectid='.$artistprojectid.'checking type='.$gettype_info['name'].' subtype='.$getsubtype_info['name'].' album title='.$artistproject_info['title'];
                                        continue;
                                    } 
                                }
                            } // skip album
                        } // end of album loop
                    } // no albums
                    
                    //print_r($get_all_user_artists_for_profileurl); 
                    //echo '<br>emails in project index ='.$j.' <br>';
                } // no artists - should not have this error
            } // skipping this profileurl already processed
        } // end of get videos for profileurl
    }    

    ////print_r($songimagefile);
    ////die;
        
    // Obtain a list of columns
    foreach ($get_artistproject as $key => $row) {
        $from[$key]  = $row['creator'];
        $track[$key] = $row['title'];
    }

    // Sort the data with volume descending, edition ascending
    // Add $data as the last parameter, to sort by the common key
    array_multisort($from, SORT_ASC, $track, SORT_ASC, $get_artistproject);
                        
    
    $result[] = array('artistproject'=>$get_artistproject); 
    $result[] = array('ProjectCreatorsProfileURL'=>$ProjectCreatorsProfileURL);
    
    $songinfo=""; 
    
    if($getuser["featured_media"]=="Song"){
        $mediaid=$getuser["media_id"];
        $get_featured_media_for_artist[$mediaid]=$new_profile_class_obj->get_Media_Song($mediaid);
        $generaluserid= $get_featured_media_for_artist[$mediaid]["general_user_id"];
        //echo '<br>generaluserid='.$generaluserid;
        $s3->resetAuth("", "",false,$generaluserid);
        $medaudioURI=$s3->getNewURI("medaudio"); 
        $songname=$get_featured_media_for_artist[$mediaid]["song_name"];
        $songimageurl=$new_profile_class_obj->get_audio_img($mediaid);
        $songinfo[$getuser["media_id"]]["featured_media"]=$getuser["featured_media"];
        $songinfo[$getuser["media_id"]]["media_id"]=$getuser["media_id"];
        $songinfo[$getuser["media_id"]]["song_title"]=$get_featured_media_for_artist[$mediaid]["songtitle"];
        $songinfo[$getuser["media_id"]]["song_url"]=$medaudioURI.$songname;
        $songinfo[$getuser["media_id"]]["creator_name"]=$get_featured_media_for_artist[$mediaid]["creator"];
        $songinfo[$getuser["media_id"]]["from_name"]=$get_featured_media_for_artist[$mediaid]["from"];
        $songinfo[$getuser["media_id"]]["song_image_url"]=$s3->replaceURI($songimageurl);
        $songinfo[$getuser["media_id"]]["for_sale"]=$get_featured_media_for_artist[$mediaid]["for_sale"];
        $songinfo[$getuser["media_id"]]["price"]=$get_featured_media_for_artist[$mediaid]["price"];  
        $songinfo[$getuser["media_id"]]["generaluserid"]=$generaluserid;
    }
    
    $result[] = array('featuredsonginfo'=>$songinfo);
    
    $result[] = array('featuredMediaAlbum'=>$featuredMediaAlbum);     
    

    
    
//print ( '<pre>' );
//print_r($exp_tagged_albums_info);
//print_r($videoimagefile);
//print_r($profileurlforvideo);
//print ( '</pre>' );    
//die;

  
    // add alluserartistsforproject to result array
  
    

} //end of if error == no

/* output in necessary format */
if($format == 'json') {
    header('Content-type: application/json');
    echo json_encode($result);
    //echo json_encode(array('artistprofile'=>$result));
    //echo array_to_json($result);
}
else {
    if ($format == 'xml'){
        header('Content-type: text/xml');
        echo '<result>';
        foreach($result as $index => $resultentry) {
            if(is_array($resultentry)) {
                foreach($resultentry as $key => $value) {
                    echo '<',$key,'>';
                    if(is_array($value)) {
                        foreach($value as $tag => $val) {
                            echo '<',$tag,'>',htmlentities($val),'</',$tag,'>';
                        }
                    }
                    echo '</',$key,'>';
                }
            }
        }
        echo '</result>';
    }
}

function array_to_json( $array ){

    if( !is_array( $array ) ){
        return false;
    }

    $associative = count( array_diff( array_keys($array), array_keys( array_keys( $array )) ));
    if( $associative ){

        $construct = array();
        foreach( $array as $key => $value ){

            // We first copy each key/value pair into a staging array,
            // formatting each key and value properly as we go.

            // Format the key:
            if( is_numeric($key) ){
                $key = "key_$key";
            }
            $key = "\"".addslashes($key)."\"";

            // Format the value:
            if( is_array( $value )){
                $value = array_to_json( $value );
            } else if( !is_numeric( $value ) || is_string( $value ) ){
                $value = "\"".addslashes($value)."\"";
            }

            // Add to staging array:
            $construct[] = "$key: $value";
        }

        // Then we collapse the staging array into the JSON form:
        $result = "{ " . implode( ", ", $construct ) . " }";

    } else { // If the array is a vector (not associative):

        $construct = array();
        foreach( $array as $value ){

            // Format the value:
            if( is_array( $value )){
                $value = array_to_json( $value );
            } else if( !is_numeric( $value ) || is_string( $value ) ){
                $value = "'".addslashes($value)."'";
            }

            // Add to staging array:
            $construct[] = $value;
        }

        // Then we collapse the staging array into the JSON form:
        $result = "[ " . implode( ", ", $construct ) . " ]";
    }
	
    return $result;
}

?>