<?php
	include_once('classes/User.php');

	$username=$_POST['name'];
	$obj=new User();
	
	if($obj->checkAvailability($username))
	{
		$data="<div align='right' class='hintRed'>Username you selected already exists, please select a new one.</div>";
	}
	else
	{
		 $data="<div align='right' class='hintGreen'>Username available.</div>";
	}
	echo $data;
?>