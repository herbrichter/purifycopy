<?php
    ob_start();
	include_once('login_includes.php');
	include_once('classes/UserSubscription.php');

	$user_id=$_GET['uid'];
	$obj=new UserSubscription;
	$rs=$obj->getUserSubscription($user_id);
	$subscription=mysql_fetch_assoc($rs);
	$subscription_id=$subscription['subscription_id'];
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<title>Purify Entertainment: Six Feature</title>
<link rel="stylesheet" type="text/css" media="screen" href="includes/purify.css" />
<?php include_once('login_js.php'); ?>
</head>
<body>

<div id="outerContainer">


  <div id="purifyMasthead">
    <div id="purifyLogo"><a href="../Demo/index.html"><img src="images/4.gif" alt="Purify Entertainment" width="302" height="53" border="0" /></a></div>
	<?php include("login_area.php"); ?>
  </div>

  
	<div id="toplevelNav">
    <ul>
      <li><a href="../Demo/index.html">Home</a></li>
      <li><a href="../Demo/about.html">About</a></li>
      <li><a href="../Demo/artists.html">Artists</a></li>
      <li><a href="../Demo/projects.html">Projects</a></li>
      <li><a href="../Demo/events.html">Events</a></li>
      <li><a href="../Demo/community.html">Community</a></li>
      <li><a href="../Demo/services.html">Services</a></li>
      <li><a href="../Demo/search.html">Search</a></li>
    </ul>
  </div>

	
  <div id="contentContainer">
    <h1>Complete your registration</h1>
    <p>Dear user, you have been directed to this page either because you have not yet completed the payment for your subscription or your request is still pending approval.</p>
    <p>Please <a href='registration_payment.php?uid=<?php echo $user_id; ?>&&sid=<?php echo $subscription_id ?>' title="Pay using Google Checkout">click here</a> to complete your payment or ignore this message if you have already paid. Your account will be activated once approved.</p>
  </div>
  

<div id="footer"><strong>2008 Purify Entertainment</strong> This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/3.0/">Creative Commons Attribution-Share Alike 3.0 Unported License</a>.<br />
  <a href="/Demo/privacypolicy.html">Privacy Policy</a> / <a href="/Demo/useragreement.html">User Agreement</a> / <a href="/Demo/originalityagreement.html">Originality Agreement</a> / <a href="/Demo/busplan.html">Business Plan</a> </div>

<!-- end of main container -->
</div>

</body>
</html>