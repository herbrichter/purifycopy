<?php
include_once('commons/db.php');
include('classes/AddContactSuggession.php');

$newclassobj = new AddContactSuggession();
$q = strtolower($_GET["term"]);
if (!$q) return;

	$cou_gt=$newclassobj->CountNameSuggession($q);
	$rs=$newclassobj->GetNameSuggession($q);
	if($cou_gt[0]>0)
	{
		$result = array();
		while ($row = mysql_fetch_array($rs)) {
			array_push($result, array("id"=>$row["fname"].$row["lname"], "label"=>$row["fname"].$row["lname"], "value" => $row["fname"].$row["lname"]));
			//echo $row["fname"].$row["lname"]."\n";
		}
		echo array_to_json($result);
	}
	
function array_to_json( $array ){

    if( !is_array( $array ) ){
        return false;
    }

    $associative = count( array_diff( array_keys($array), array_keys( array_keys( $array )) ));
    if( $associative ){

        $construct = array();
        foreach( $array as $key => $value ){

            // We first copy each key/value pair into a staging array,
            // formatting each key and value properly as we go.

            // Format the key:
            if( is_numeric($key) ){
                $key = "key_$key";
            }
            $key = "\"".addslashes($key)."\"";

            // Format the value:
            if( is_array( $value )){
                $value = array_to_json( $value );
            } else if( !is_numeric( $value ) || is_string( $value ) ){
                $value = "\"".addslashes($value)."\"";
            }

            // Add to staging array:
            $construct[] = "$key: $value";
        }

        // Then we collapse the staging array into the JSON form:
        $result = "{ " . implode( ", ", $construct ) . " }";

    } else { // If the array is a vector (not associative):

        $construct = array();
        foreach( $array as $value ){

            // Format the value:
            if( is_array( $value )){
                $value = array_to_json( $value );
            } else if( !is_numeric( $value ) || is_string( $value ) ){
                $value = "'".addslashes($value)."'";
            }

            // Add to staging array:
            $construct[] = $value;
        }

        // Then we collapse the staging array into the JSON form:
        $result = "[ " . implode( ", ", $construct ) . " ]";
    }

    return $result;
}	
?>
