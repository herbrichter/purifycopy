<?php
if(session_id() == '') {
    @session_start();
}
error_reporting(0);
ob_start();

$domainname=$_SERVER['SERVER_NAME'];

if(isset($_SESSION['login_email']))
{
	include_once('classes/Commontabs.php');
	$newtab=new Commontabs();
	
	include_once('header.php');
}
else
{
?>
<!--<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "//www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="//www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Purify Art</title>-->
<link rel="stylesheet" href="css/nivo/themes/default/default.css" type="text/css" media="screen" />
<link rel="stylesheet" href="css/nivo/themes/light/light.css" type="text/css" media="screen" />
<link rel="stylesheet" href="css/nivo/themes/dark/dark.css" type="text/css" media="screen" />
<link rel="stylesheet" href="css/nivo/themes/bar/bar.css" type="text/css" media="screen" />
<link rel="stylesheet" href="css/nivo/nivo-slider.css" type="text/css" media="screen" />
<link rel="stylesheet" href="css/nivo/slide_nivo_style.css" type="text/css" media="screen" />
<link rel="stylesheet" type="text/css" media="screen" href="includes/purify.css" />
<link rel="stylesheet" type="text/css" media="screen" href="css/style.css" />
<link rel="stylesheet" type="text/css" href="style.css" />
<link rel="stylesheet" href="includes/light.css" />
<!--<link rel="stylesheet" href="engine/css/vlightbox.css" type="text/css" />
<link rel="stylesheet" href="engine/css/visuallightbox.css" type="text/css" media="screen" />-->
<link rel="stylesheet" href="css/jquery.autocomplete.css" type="text/css" />
<link rel="stylesheet" href="includes/jquery.ui.all.css">
<link  rel="stylesheet" type="text/css" media="screen" href="css/jquery.ui.autocomplete.css"></link>

<!--<script type="text/javascript" src="../js/jquery-latest.pack.js"></script>
<script type="text/javascript" src="javascripts/country_state.js"></script>-->
<script language="javascript" type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
<!--<script type="text/javascript" src="includes/jquery.min.js"></script>-->
<script type="text/javascript" src="includes/light.js"></script>
<!--<script src="scroll_javascripts/jquery.js"> </script>-->


<!--<script src="includes/jquery.js"></script>
<script src="javascripts/jquery-1.2.6.js"></script>-->
<!--<script type="text/javascript" src="javascripts/flowplayer-3.1.4.min.js"></script>
<script type="text/javascript" src="javascripts/audio-player.js"></script>-->
<style type="text/css">#vlightbox a#vlb{display:none}</style>
<script src="engine/js/jquery.min.js" type="text/javascript"></script>
<script src="javascripts/jquery-1.2.6.js"></script>
<script src="ui/jquery-1.7.2.js"></script>
<script src="includes/jquery.ui.core.js"></script>
<script src="includes/jquery.ui.widget.js"></script>
<script src="includes/jquery.ui.datepicker.js"></script>
<script type="text/javascript" src="javascripts/jquery.ui.autocomplete.js"></script>
<script type="text/javascript" src="javascripts/jquery.ui.position.js"></script>
<script type="text/javascript" src="javascripts/checkForm.js"></script>
<style>
	.ui-autocomplete-loading { background: white url('css/ui-anim_basic_16x16.gif') right center no-repeat; }
	.ui-menu { z-index: 100 !important; }
</style>

<?php //if(isset($login_flag) && !$login_flag) include_once('login_js.php'); ?>

<script src="javascript.js"> </script>
<script src="js/loginvalidate.js"></script>
</head>
<body>
<?php

    $scriptfilename=$_Server['DOCUMENT_ROOT'].'/profile.php';
    
	if($_SERVER['SCRIPT_FILENAME']!=$scriptfilename)
	{
?>
		<script type="text/javascript" src="//assets.zendesk.com/external/zenbox/v2.6/zenbox.js"></script>
		<style type="text/css" media="screen, projection">
		  @import url(//assets.zendesk.com/external/zenbox/v2.6/zenbox.css);
		</style>
		<script type="text/javascript">
		  if (typeof(Zenbox) !== "undefined") {
			Zenbox.init({
			  dropboxID:   "20267868",
			  url:         "https://purifyart.zendesk.com",
			  tabTooltip:  "Feedback",
			  tabImageURL: "https://assets.zendesk.com/external/zenbox/images/tab_feedback_right.png",
			  tabColor:    "black",
			  tabPosition: "Right"
			});
		  }
		</script>
<?php
	}
if ($domainname=='purifyart.com'||$domainname=='www.purifyart.com') {
    $staging='';
} elseif ($domainname=='purifystaging.com'||$domainname=='www.purifystaging.com'){
    $staging='-staging';
} elseif ($domainname=='purifycopy.com'||$domainname=='www.purifycopy.com'){
    $staging='-pcopy';
} elseif ($domainname=='localhost'){
    $staging='-localhost';
} else {echo "server name is ".$domainname; die();}
?>

    <div class="wrapper">
		<div class="header" style="font-size: 12px;">
		<div class="topBorder"></div>
    	<div class="logo"><a href="https://<?php echo $domainname;?>/index.php"><img src="images/purelogo-19046<?php echo $staging; ?>.png" title="Purify Art Logo" alt="Logo of Purify Art" /></a></div>
        <div class="search">
        	<input type="text" class="searchfield" name="general_search" id="general_search" value="Search for Artists, Media, Events etc" onfocus="if(this.value == 'Search for Artists, Media, Events etc'){this.value = '';}" onblur="if(this.value == ''){this.value='Search for Artists, Media, Events etc';}" />
			<input name="table_name" id="table_name" type="hidden" class="searchField" value=""  />
			<input name="table_id" id="table_id" type="hidden" class="searchField" value=""  />
            <a onClick="sendString()" id="searchlink"><input type="button" class="button" /></a>
        </div>
        <div class="cart_new">
			<a class="fancybox fancybox.ajax" href ="addtocart.php?buyer_id=<?php echo $newres1['general_user_id'];?>&display_cart=1" id="showcart">
				<img src="images/cart.png" title="Purify Art Shopping" alt="Shopping Cart of Purify Art" />
			</a>
		</div>
        <div class="login"><a href="https://<?php echo $domainname;?>/login_email.php" >Log In</a> |</div>
        <div class="sign"><a href="https://<?php echo $domainname;?>/registration_up.php">Sign Up</a></div>
    </div>
</div>
<div id="boxes">
    <div id="dialog1" class="window">
		<div style="float: right; padding: 3px 2px 0px; cursor: pointer;">
			<img id="close" src="images/close_black.png">
		</div>
    	<div class="wrap">
        	<div class="popLogo"><img src="../images/popup-logo.jpg" /></div>
            <!--<form>-->
			<form name="form1" method="post" action="https://<?php echo $domainname;?>/login_check.php" onsubmit="return login();" >
				<input type="text" class="loginField" id="email_values_chkout" name="email_values_chkout" />
            	<div class="fieldCont">
                	<div class="fieldTitle">Email</div>
                    <input type="text" name="username" id="username" class="textfield" />
                </div>
                <div class="fieldCont">
                	<div class="fieldTitle">Password</div>
                    <input name="password" id="password" type="password" class="textfield" />
                </div>
                <div class="chkfieldCont">
                    <div class="chekCont">
                        <input type="checkbox" class="chk" id="remember_me_behind" name="remember_me_behind" />
                        <div class="chkTitle">Remember Me</div>
                    </div>
                    <input type="submit" class="submit" id="loginButton_th" value="Log In" />
                    <div class="forgot_new"><a href="">Forgot Password?</a></div>
                </div>
				<div>
					<input id="redirect_url" name="redirect_url" type="hidden" value="<?php echo $urils; ?>" />
				</div>
            </form>
            <div class="popsign">
            	<div class="signTitle">Not a Purify Member?</div>
                <div class="signButton">
                	<a href="registration_up.php">Create a Free Account Now!</a>
                </div>
            </div>
        </div>
    </div>
    <div id="mask"></div>
    <div id="qTip"></div>
</div>

<?php
//	include("login_area.php"); 
		?>
	<!--
</div>-->
<script>
	function sendString()
	{
		var search_text = document.getElementById("general_search").value;
		var search_table = document.getElementById("table_name").value;
		var search_id = document.getElementById("table_id").value;
		//var seacrh_form = document.getElementById("search_form");
		
		if(document.getElementById("search_form")!=null)
		{
			//alert("if");
			var search_form = document.getElementById("search_form");
			//location.href = "search.php?q_match="+search_text+"&q_table="+search_table+"&q_id="+search_id;
			search_form.action = "search.php?q_match="+search_text+"&q_table="+search_table+"&q_id="+search_id;
			document.getElementById("search_form").submit();
		}
		else
		{
			//alert("else");
			location.href = "//"+window.location.hostname+"/search.php?q_match="+search_text+"&q_table="+search_table+"&q_id="+search_id;
		}
	}
	/*function sendString_button()
	{
		var search_text = document.getElementById("general_search").value;
		var search_table = document.getElementById("table_name").value;
		var search_id = document.getElementById("table_id").value;
		var search_form = document.getElementById("search_form");
		//location.href = "search.php?q_match="+search_text+"&q_table="+search_table+"&q_id="+search_id;
		search_form.action = "search.php?q_match="+search_text+"&q_table="+search_table+"&q_id="+search_id;
		document.getElementById("search_form").submit();
	}*/
</script>

<script>

$(document).ready(function() {

	//select all the a tag with name equal to modal
	$('a[name=modal]').click(function(e) {
		//Cancel the link behavior
		e.preventDefault();

		//Get the A tag
		var id = $(this).attr('href');

		//Get the screen height and width
		var maskHeight = $(document).height();
		var maskWidth = $(window).width();

		//Set heigth and width to mask to fill up the whole screen
		$('#mask').css({'width':maskWidth,'height':maskHeight});

		//transition effect
		//$('#mask').fadeIn(500);
		$('#mask').fadeTo("slow",0.8);

		//Get the window height and width
		var winH = $(window).height();
		var winW = $(window).width();

		//Set the popup window to center
		//$(id).css('top',  winH/2-$(id).height()/1.1);
		$(id).css('left', winW/2-$(id).width()/2);

		//transition effect
		$(id).fadeIn(500);

	});

	//if close button is clicked
	$('.window #close').click(function (e) {
		//Cancel the link behavior
		e.preventDefault();

		$('#mask').hide();
		$('.window').hide();
	});

	//if mask is clicked
	$('#mask').click(function () {
		$(this).hide();
		$('.window').hide();
	});

	$(window).resize(function () {

 		var box = $('#boxes .window');

        //Get the screen height and width
        var maskHeight = $(document).height();
        var maskWidth = $(window).width();

        //Set height and width to mask to fill up the whole screen
        $('#mask').css({'width':maskWidth,'height':maskHeight});

        //Get the window height and width
        var winH = $(window).height();
        var winW = $(window).width();

        //Set the popup window to center
        box.css('top',  winH/2 - box.height()/2);
        box.css('left', winW/2 - box.width()/2);

	});

});

</script>
<script type="text/javascript">
$("#general_search").click(function(){
window.onkeypress = function(evt) {
		evt = evt || window.event;
		var charCode = evt.keyCode || evt.which;
		if(charCode==13){
			$("#searchlink").click();
		}
	};
});
</script>
<?php
if($_SERVER['HTTPS']=='on')
{
?>
		<script>
		 $(function() {
				$( "#general_search" ).autocomplete({
					source: "suggest_search.php",
					minLength: 1,
					focus: function (event, ui) {
					document.getElementById("table_name").value = ui.item.value1;
					document.getElementById("table_id").value = ui.item.value2;
					},
					select: function( event, ui ) {
					document.getElementById("table_name").value = ui.item.value1;
					document.getElementById("table_id").value = ui.item.value2;
					},
					open:function(event,ui){ 
						var maxListLength=10;
						var ul = jQuery( "#general_search" ).autocomplete("widget")[0];
						//alert(ul.offsetWidth);
						var len_org = ul.childNodes.length;
						while(ul.childNodes.length > maxListLength)
						{
							  ul.removeChild(ul.childNodes[ul.childNodes.length-1]);
						}
							if(ul.childNodes.length > maxListLength-1)
							{
								$('<li id="ac-add-venue" style="text-align: center;"><a href="search.php?alpha='+$("#general_search").val()+'" >See More...</a></li>').appendTo('ul.ui-autocomplete');
								$('<li id="ac-add-venue" style="text-align: center;">Displaying Top 10 of '+ len_org +' Results.</li>').appendTo('ul.ui-autocomplete');
							}
						}
				});
				
				$('.ui-autocomplete').on('click', '.ui-menu-item', function(){
					/* document.getElementById("table_name").value = ui.item.value1;
					document.getElementById("table_id").value = ui.item.value2; */
					$('#searchlink').trigger('click');
				});
			});
		</script>
	<?php
}
else
{
?>
		<script>
		 $(function() {
				$( "#general_search" ).autocomplete({
					source: "same_ht.php",
					minLength: 1,
					focus: function (event, ui) {
					document.getElementById("table_name").value = ui.item.value1;
					document.getElementById("table_id").value = ui.item.value2;
					},
					select: function( event, ui ) {
					document.getElementById("table_name").value = ui.item.value1;
					document.getElementById("table_id").value = ui.item.value2;
					},
					open:function(event,ui){ 
						var maxListLength=10;
						var ul = jQuery( "#general_search" ).autocomplete("widget")[0];
						//alert(ul.offsetWidth);
						var len_org = ul.childNodes.length;
						while(ul.childNodes.length > maxListLength)
						{
							  ul.removeChild(ul.childNodes[ul.childNodes.length-1]);
						}
							if(ul.childNodes.length > maxListLength-1)
							{
								$('<li id="ac-add-venue" style="text-align: center;"><a href="search.php?alpha='+$("#general_search").val()+'" >See More...</a></li>').appendTo('ul.ui-autocomplete');
								$('<li id="ac-add-venue" style="text-align: center;">Displaying Top 10 of '+ len_org +' Results.</li>').appendTo('ul.ui-autocomplete');
							}
						}
				});
				
				$('.ui-autocomplete').on('click', '.ui-menu-item', function(){
					/* document.getElementById("table_name").value = ui.item.value1;
					document.getElementById("table_id").value = ui.item.value2; */
					$('#searchlink').trigger('click');
				});
			});
		</script>
	<?php
}
}
?>
<!------------------------------- OLD FANCY BOX ---------------->
		<!--<script>
			!window.jQuery && document.write('<script src="fancybox/jquery-1.4.3.min.js"><\/script>');
		</script>
		<script type="text/javascript" src="fancybox/jquery.fancybox-1.3.4.pack.js"></script>
		<link rel="stylesheet" type="text/css" href="fancybox/jquery.fancybox-1.3.4.css" media="screen" />-->
		
		<!--<script type="text/javascript" src="../new_fancy_box/jquery-1.9.0.min.js"></script>-->
		
		<!------------------------------- NEW FANCY BOX ---------------->
		<script type="text/javascript" src="../new_fancy_box/jquery.fancybox.js?v=2.1.4"></script>
		<link rel="stylesheet" type="text/css" href="../new_fancy_box/jquery.fancybox.css?v=2.1.4" media="screen" />
		<script type="text/javascript" src="../new_fancy_box/helpers/jquery.fancybox-media.js?v=1.0.5"></script>
	

		
		<script type="text/javascript">
			$(document).ready(function() {
				$("#showcart").fancybox({
					helpers : {
						media : {},
						buttons : {},
						title : null
					}
				});
			});
			</script>
			<script type="text/javascript" src="../javascripts/jquery.nivo.slider.js"></script>
			<script type="text/javascript">
				$(window).load(function() {
					$('#slider').nivoSlider();
				});
			</script>