function startTab() {
	$("div[class^=hiddenBlock]").hide();
	$("div[class^=hiddenBlock]:first").fadeIn(500);
	$("a[id^=tab]").css('background-image',"url('images/profile/tab_off.gif')");
	$("a[id^=tab]:first").css('background-image',"url('images/profile/tab_over.png')");

}

function showTab(type){
	$("div[class^=hiddenBlock]").fadeOut();
	$(".hiddenBlock"+type).fadeIn(500);
	$("a[class^=profileTab]").css('background-image',"url('images/profile/tab_off.gif')");
	$("#tab"+type+":first").css('background-image',"url('images/profile/tab_over.png')");
}

function checkLogin(){
	if($("#username").val()==""){
		$("#errMsg").html("Please enter Username.");
		$("#username").focus();
		return;
	}else if($("#userpass").val()==""){
		$("#errMsg").html("Please enter Password.");
		$("#userpass").focus();
		return;
	}
	$.ajax({
		type: "POST",
		url: 'player/getMediaInfo.php',
		data: { "username":$("#username").val(), "userpass":$("#userpass").val()},
		success: function(data){
			if(data==0){
				$("#errMsg").html("Invalid Username/Password.");
				$("#username").focus();
				return;
			}else{
				logedinFlag=true;
				/* $("#validateFlag").val(1);
				$("#validateFrm").submit(); */
			}
		}
	});
}

function checkLogin_all(){

	if($("#username_all").val()==""){
		
		$("#errMsg").html("Please enter Username.");
		$("#username_all").focus();
		return;
	}else if($("#userpass").val()==""){
		
		$("#errMsg").html("Please enter Password.");
		$("#userpass").focus();
		return;
	}
	$.ajax({
		type: "POST",
		url: 'player/getMediaInfo.php',
		data: { "username":$("#username_all").val(), "userpass":$("#userpass").val()},
		success: function(data){
			if(data==0){
				$("#errMsg").html("Invalid Username/Password.");
				$("#username_all").focus();
				return;
			}else{
				logedinFlag=true;
				$("#validateFlag").val(1);
				$("#validateFrm").submit();
			}
		}
	});
}