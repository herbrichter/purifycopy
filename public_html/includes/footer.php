<?php
	include_once("analyticstracking.php");
?>
<div id="footer">
  <div id="footerLinksWrapper">
    <ul class="footerList" id="footerAboutUs">
      <li class="listHeader">About Us</li>
      <li><a href="about.php">Mission</a></li>
      <li><a href="about_contacts.php">Contact</a></li>
      <li><a href="about_news.php">News History</a></li>
      <li><a href="about_services.php">Services</a></li>
      <li><a href="about_team.php">Team</a></li>
    </ul>
    <ul class="footerList" id="footerPartners">
      <li class="listHeader">Powered By</li>
      <li><a href="//aws.amazon.com/s3/" target="_blank">Amazon S3</a></li>
      <li><a href="//www.paypal.com/" target="_blank">Paypal</a></li>
      <li><a href="//soundcloud.com/" target="_blank">Soundcloud</a></li>
      <li><a href="//www.youtube.com/" target="_blank">Youtube</a></li>
    </ul>
    <ul class="footerList" id="footerLegal">
      <li class="listHeader" >Legal</li>
      <li><a href="about_agreements_copyright.php" target="_blank">Intellectual Property Policy</a></li>
      <li><a href="about_agreements_privacy.php" target="_blank">Privacy Policy</a></li>
      <li><a href="about_agreements_user.php" target="_blank">User Agreement</a></li>
    </ul>
    <ul class="footerList" id="footerSocial">
      <li class="listHeader">Social</li>
      <li><a href="//www.facebook.com/pages/Purify-Art/289068801146751" target="_blank">Facebook</a></li>
      <li><a href="//plus.google.com/u/0/b/103421305625407216442/103421305625407216442/about/edit/d" target="_blank">Google+</a></li>
      <li><a href="//twitter.com/PurifyArt" target="_blank">Twitter</a></li>
    </ul>
    <ul class="footerList" id="footerHelp">
      <li class="listHeader">Help</li>
      <li><a href="help_donate.php">Donate</a></li>
      <li><a href="help_faq.php">FAQ</a></li>
      <li><a href="help_pricing.php">Pricing</a></li>
    </ul>    
  </div>
  <?php if(isset($_SESSION['login_email']))
	{
		echo "</div>";
	}
  ?>
  <p id="copyright">&copy; Copyright 2013 Purify Art</p>
</div>
<!-- end of main container -->
</div>
</body>
</html>