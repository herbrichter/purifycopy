function addBands()
{
	// Retrieve the elements from the document body
	var textbox = document.getElementById('bands');
	if(textbox.value=="")
	{
		alert("Please Enter Some Text");
		return false;
	}
	var listbox = document.getElementById('bands_tagged[]');
	var rmv_button = document.getElementById("remove");
	var options = document.getElementById('bands_tagged[]').options;
	var optiontext,text;
	for (i=0; i<options.length; i++)
	{
	//	optiontext=options[i].value.toLowerCase();
		if(options[i].value.toLowerCase()==textbox.value.toLowerCase())
		{
			alert(options[i].value + "  Already Exist.");
			return false;
		}
	}
	// Now we need to create a new 'option' tag to add to MyListbox
	var newOption = document.createElement('option');
	newOption.value = textbox.value; // The value that this option will have
	newOption.innerHTML = textbox.value; // The displayed text inside of the <option> tags
	newOption.selected=true;
	// Finally, add the new option to the listbox
	listbox.appendChild(newOption);
	textbox.value="";
	rmv_button.style.display="block";
}
function addartist()
{
	// Retrieve the elements from the document body
	var textbox = document.getElementById('artistname');
	if(textbox.value=="")
	{
		alert("Please Enter Some Text");
		return false;
	}
	var listbox = document.getElementById('artist_tagged[]');
	var options = document.getElementById('artist_tagged[]').options;
	for(i=0;i<options.length;i++)
	{
		if(options[i].value.toLowerCase()==textbox.value.toLowerCase())
		{
			alert(options[i].value + "  Alreay Exist.");
			return false;
		}
	}
	// Now we need to create a new 'option' tag to add to MyListbox
	var newOption = document.createElement('option');
	newOption.value = textbox.value; // The value that this option will have
	newOption.innerHTML = textbox.value; // The displayed text inside of the <option> tags
	newOption.selected=true;
	// Finally, add the new option to the listbox
	listbox.appendChild(newOption);
	textbox.value="";
}
function addguestname()
{
	// Retrieve the elements from the document body
	var textbox = document.getElementById('addguest');
	if(textbox.value=="")
	{
		alert("Please Enter Some Text");
		return false;
	}
	var listbox = document.getElementById('guest_list[]');
	var options = document.getElementById('guest_list[]').options;
	for(i=0;i<options.length;i++)
	{
		if(options[i].value.toLowerCase()==textbox.value.toLowerCase())
		{
			alert(options[i].value + "  Alreay Exist.");
			return false;
		}
	}
	// Now we need to create a new 'option' tag to add to MyListbox
	var newOption = document.createElement('option');
	newOption.value = textbox.value; // The value that this option will have
	newOption.innerHTML = textbox.value; // The displayed text inside of the <option> tags
	newOption.selected=true;
	// Finally, add the new option to the listbox
	listbox.appendChild(newOption);
	textbox.value="";
}

/***********************************/
/*******Function for Add_artist_event.php ********/
/***********************************/
function remove_artist_event_user()
{
	var listbox = document.getElementById('bands_tagged[]');
    //iterate through each option of the listbox
    for(var count= listbox.options.length-1; count >= 0; count--)
	{
         //if the option is selected, delete the option
        if(listbox.options[count].selected == true)
		{
            listbox.remove(count);
        }
    }
	if(listbox.options.length==0 || listbox.options.length== "")
	{
		var rmv_button = document.getElementById("remove");
		rmv_button.style.display="none";
	}
}
/***********************************/
/*******Function for Add_artist_event.php Ends Here ********/
/***********************************/
