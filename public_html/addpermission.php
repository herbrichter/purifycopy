<?php
include("classes/AddPermission.php");
$add_perm_obj = new AddPermission();

$get_gen_id = $add_perm_obj->get_general_user();

if(isset($_GET['edit']) && $_GET['edit']!="")
{
	$add_perm = $add_perm_obj->update_permission($get_gen_id['general_user_id'],mysql_real_escape_string($_POST["generalevent"]),mysql_real_escape_string($_POST["generalfriend"]),mysql_real_escape_string($_POST["generalinfo"]),mysql_real_escape_string($_POST["generallocation"]),mysql_real_escape_string($_POST["generalpoint"]),mysql_real_escape_string($_POST["generalservice"]),mysql_real_escape_string($_POST["generalstatistics"]),mysql_real_escape_string($_POST["generalsubscription"]),mysql_real_escape_string($_POST["artistbiography"]),mysql_real_escape_string($_POST["artisthomepage"]),mysql_real_escape_string($_POST["artistproject"]),mysql_real_escape_string($_POST["artistevent"]),mysql_real_escape_string($_POST["communitybiography"]),mysql_real_escape_string($_POST["communityhomepage"]),mysql_real_escape_string($_POST["communityproject"]),mysql_real_escape_string($_POST["communityevent"]));
}
else
{
	$add_perm = $add_perm_obj->add_permission($get_gen_id['general_user_id'],mysql_real_escape_string($_POST["generalevent"]),mysql_real_escape_string($_POST["generalfriend"]),mysql_real_escape_string($_POST["generalinfo"]),mysql_real_escape_string($_POST["generallocation"]),mysql_real_escape_string($_POST["generalpoint"]),mysql_real_escape_string($_POST["generalservice"]),mysql_real_escape_string($_POST["generalstatistics"]),mysql_real_escape_string($_POST["generalsubscription"]),mysql_real_escape_string($_POST["artistbiography"]),mysql_real_escape_string($_POST["artisthomepage"]),mysql_real_escape_string($_POST["artistproject"]),mysql_real_escape_string($_POST["artistevent"]),mysql_real_escape_string($_POST["communitybiography"]),mysql_real_escape_string($_POST["communityhomepage"]),mysql_real_escape_string($_POST["communityproject"]),mysql_real_escape_string($_POST["communityevent"]));
}
header("Location: profileedit.php#permissions");
?>