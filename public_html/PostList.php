<?php
	session_start();
	
	include_once("commons/db.php");
	include_once("classes/Addfriend.php");
	include_once("classes/Listpost.php");

	//echo $_POST['type_val'];
	
	$oldob_suugest = new Addfriend();
	$listob_suugest = new Listpost();
	
	$gen_log_id = $oldob_suugest->general_user_email_gen($_SESSION['login_email']);
	
	if($gen_log_id!="")
	{
		if(isset($_POST['type_val']) && $_POST['type_val']!="" && $_POST['type_val']=='events_post')
		{
			$get_ac_events = $listob_suugest->get_all_events($gen_log_id['general_user_id'],$gen_log_id['artist_id'],$gen_log_id['community_id']);
			
			if(count($get_ac_events)>0)
			{
				$sort = array();
				foreach($get_ac_events as $k=>$v)
				{										
					$sort['date'][$k] = $v['date'];
				}
				//var_dump($sort);
				array_multisort($sort['date'], SORT_DESC,$get_ac_events);
?>
				<select name="list_epm[]" size="5" id="list_epm[]" class="list" style="left:115px; position:relative;">
<?php
					for($ac_eve=0;$ac_eve<count($get_ac_events);$ac_eve++)
					{
?>
						<option value="<?php
							if(isset($get_ac_events[$ac_eve]['table_name']) && $get_ac_events[$ac_eve]['table_name']=='general_artist') 
							{
								echo $get_ac_events[$ac_eve]['id'].'~artist_event';
							}
							elseif(isset($get_ac_events[$ac_eve]['table_name']) && $get_ac_events[$ac_eve]['table_name']=='general_community')
							{
								echo $get_ac_events[$ac_eve]['id'].'~community_event';
							}?>"><?php echo date("d.m.y", strtotime($get_ac_events[$ac_eve]['title']['date'])).' '.$get_ac_events[$ac_eve]['title']; ?></option>
<?php
					}
?>
				</select>
<?php
			}
			else
			{
?>
				<input class="fieldText" type="text" disabled="true" value="No Events Found." style=" left: 115px; position: relative;"/>
<?php
			}
		}
		elseif(isset($_POST['type_val']) && $_POST['type_val']!="" && $_POST['type_val']=='projects_post')
		{
			$get_ac_projects = $listob_suugest->get_all_projects($gen_log_id['general_user_id'],$gen_log_id['artist_id'],$gen_log_id['community_id']);
			
			if(count($get_ac_projects)>0)
			{
				$sort = array();
				foreach($get_ac_projects as $k=>$v)
				{
					$create_c = strpos($v['creator'],'(');
					$end_c1 = substr($v['creator'],0,$create_c);
					$end_c = trim($end_c1);
					
					$sort['creator'][$k] = strtolower($end_c);
					//$sort['from'][$k] = $end_f;
					//$sort['track'][$k] = $v['track'];
					$sort['title'][$k] = strtolower($v['title']);
				}
				//var_dump($sort);
				array_multisort($sort['creator'], SORT_ASC, $sort['title'], SORT_ASC,$get_ac_projects);
?>
				<select name="list_epm[]" size="5" id="list_epm[]" class="list" style="left:115px; position:relative;">
<?php
					for($ac_pro=0;$ac_pro<count($get_ac_projects);$ac_pro++)
					{
						$create_c = strpos($get_ac_projects[$ac_pro]['creator'],'(');
						$end_c = substr($get_ac_projects[$ac_pro]['creator'],0,$create_c);
						
						$ecei = "";
						if($end_c!="")
						{
							$ecei = $end_c;
							$ecei .= ' : '.$get_ac_projects[$ac_pro]['title'];
						}
						else
						{
							$ecei = $get_ac_projects[$ac_pro]['title'];
						}
?>
						<option value="<?php
							if(isset($get_ac_projects[$ac_pro]['table_name']) && $get_ac_projects[$ac_pro]['table_name']=='general_artist') 
							{
								echo $get_ac_projects[$ac_pro]['id'].'~artist_project';
							}
							elseif(isset($get_ac_projects[$ac_pro]['table_name']) && $get_ac_projects[$ac_pro]['table_name']=='general_community')
							{
								echo $get_ac_projects[$ac_pro]['id'].'~community_project';
							}?>"><?php echo $ecei; ?></option>
<?php
					}
?>
				</select>
<?php
			}
			else
			{
?>
				<input class="fieldText" type="text" disabled="true" value="No Projects Found." style=" left: 115px; position: relative;"/>
<?php
			}
		}
		elseif(isset($_POST['type_val']) && $_POST['type_val']!="" && $_POST['type_val']=='images_post')
		{
			$get_ac_galleries = $listob_suugest->get_all_galleries($gen_log_id['general_user_id'],$gen_log_id['artist_id'],$gen_log_id['community_id']);
			if(count($get_ac_galleries)>0)
			{
				$sort = array();
				foreach($get_ac_galleries as $k=>$v)
				{										
					$create_c = strpos($v['creator'],'(');
					$end_c1 = substr($v['creator'],0,$create_c);
					$end_c = trim($end_c1);
					
					$create_f = strpos($v['from'],'(');
					$end_f1 = substr($v['from'],0,$create_f);
					$end_f = trim($end_f1);
					
					$sort['creator'][$k] = strtolower($end_c);
					$sort['from'][$k] = strtolower($end_f);
					//$sort['track'][$k] = $v['track'];
					$sort['title'][$k] = strtolower($v['title']);
				}
				//var_dump($sort);
				array_multisort($sort['creator'], SORT_ASC, $sort['from'], SORT_ASC, $sort['title'], SORT_ASC,$get_ac_galleries);
?>
				<select name="list_epm[]" size="5" id="list_epm[]" class="list" style="left:115px; position:relative;">
<?php
					for($ac_gal=0;$ac_gal<count($get_ac_galleries);$ac_gal++)
					{
						$create_c = strpos($get_ac_galleries[$ac_gal]['creator'],'(');
						$end_c = substr($get_ac_galleries[$ac_gal]['creator'],0,$create_c);
						
						$create_f = strpos($get_ac_galleries[$ac_gal]['from'],'(');
						$end_f = substr($get_ac_galleries[$ac_gal]['from'],0,$create_f);
						
						$eceksg = "";
						if($end_c!="")
						{
							$eceksg = $end_c;
						}
						if($end_f!="")
						{
							if($end_c!="")
							{
								$eceksg .= " : ".$end_f;
							}
							else
							{
								$eceksg .= $end_f;
							}
						}
						if($get_ac_galleries[$ac_gal]['title']!="")
						{
							if($end_c!="" || $end_f!="")
							{
								$eceksg .= " : ".$get_ac_galleries[$ac_gal]['title'];
							}
							else
							{
								$eceksg .= $get_ac_galleries[$ac_gal]['title'];
							}
						}
?>
						<option value="<?php
							if(isset($get_ac_galleries[$ac_gal]['table_name']) && $get_ac_galleries[$ac_gal]['table_name']=='general_artist') 
							{
								echo $get_ac_galleries[$ac_gal]['gallery_id'].'~reg_art';
							}
							elseif(isset($get_ac_galleries[$ac_gal]['table_name']) && $get_ac_galleries[$ac_gal]['table_name']=='general_community')
							{
								echo $get_ac_galleries[$ac_gal]['gallery_id'].'~reg_com';
							}
							elseif(isset($get_ac_galleries[$ac_gal]['table_name']) && $get_ac_galleries[$ac_gal]['table_name']=='general_media')
							{
								echo $get_ac_galleries[$ac_gal]['id'].'~reg_med';
							}?>"><?php
							if(isset($get_ac_galleries[$ac_gal]['gallery_title']) && $get_ac_galleries[$ac_gal]['gallery_title']!="")
							{
								echo $get_ac_galleries[$ac_gal]['gallery_title']; 
							}
							elseif(isset($get_ac_galleries[$ac_gal]['title']) && $get_ac_galleries[$ac_gal]['title']!="")
							{
								echo $eceksg; 
							}?></option>
<?php
					}
?>
				</select>
<?php
			}
			else
			{
?>
				<input class="fieldText" type="text" disabled="true" value="No Galleries Found." style=" left: 115px; position: relative;"/>
<?php
			}
		}
		elseif(isset($_POST['type_val']) && $_POST['type_val']!="" && $_POST['type_val']=='songs_post')
		{
			$get_ac_songs = $listob_suugest->get_all_songs($gen_log_id['general_user_id'],$gen_log_id['artist_id'],$gen_log_id['community_id']);
			if(count($get_ac_songs)>0)
			{
				$sort = array();
				foreach($get_ac_songs as $k=>$v)
				{										
					$create_c = strpos($v['creator'],'(');
					$end_c1 = substr($v['creator'],0,$create_c);
					$end_c = trim($end_c1);
					
					$create_f = strpos($v['from'],'(');
					$end_f1 = substr($v['from'],0,$create_f);
					$end_f = trim($end_f1);
					
					$sort['creator'][$k] = strtolower($end_c);
					$sort['from'][$k] = strtolower($end_f);
					$sort['track'][$k] = $v['track'];
					$sort['title'][$k] = strtolower($v['title']);
				}
				//var_dump($sort);
				array_multisort($sort['creator'], SORT_ASC, $sort['from'], SORT_ASC, $sort['track'], SORT_ASC,$sort['title'], SORT_ASC,$get_ac_songs);
?>
				<select name="list_epm[]" size="5" id="list_epm[]" class="list" style="left:115px; position:relative;">
<?php
					for($ac_song=0;$ac_song<count($get_ac_songs);$ac_song++)
					{
						$create_c = strpos($get_ac_songs[$ac_song]['creator'],'(');
						$end_c = substr($get_ac_songs[$ac_song]['creator'],0,$create_c);
						
						$create_f = strpos($get_ac_songs[$ac_song]['from'],'(');
						$end_f = substr($get_ac_songs[$ac_song]['from'],0,$create_f);
						
						$eceks = "";
						if($end_c!="")
						{
							$eceks = $end_c;
						}
						if($end_f!="")
						{
							if($end_c!="")
							{
								$eceks .= " : ".$end_f;
							}
							else
							{
								$eceks .= $end_f;
							}
						}
						if($get_ac_songs[$ac_song]['track']!="")
						{
							if($end_c!="" || $end_f!="")
							{
								$eceks .= " : ".$get_ac_songs[$ac_song]['track'];
							}
							else
							{
								$eceks .= $get_ac_songs[$ac_song]['track'];
							}
						}
						if($get_ac_songs[$ac_song]['title']!="")
						{
							if($end_c!="" || $end_f!="" || $get_ac_songs[$ac_song]['track']!="")
							{
								$eceks .= " : ".$get_ac_songs[$ac_song]['title'];
							}
							else
							{
								$eceks .= $get_ac_songs[$ac_song]['title'];
							}
						}
?>
							<option value="<?php
								if(isset($get_ac_songs[$ac_song]['table_name']) && $get_ac_songs[$ac_song]['table_name']=='general_artist') 
								{
									echo $get_ac_songs[$ac_song]['audio_id'].'~reg_art';
								}
								elseif(isset($get_ac_songs[$ac_song]['table_name']) && $get_ac_songs[$ac_song]['table_name']=='general_community')
								{
									echo $get_ac_songs[$ac_song]['audio_id'].'~reg_com';
								}
								elseif(isset($get_ac_songs[$ac_song]['table_name']) && $get_ac_songs[$ac_song]['table_name']=='general_media')
								{
									echo $get_ac_songs[$ac_song]['id'].'~reg_med';
								}?>"><?php
								if(isset($get_ac_songs[$ac_song]['audio_name']) && $get_ac_songs[$ac_song]['audio_name']!="")
								{
									echo $get_ac_songs[$ac_song]['audio_name']; 
								}
								elseif(isset($get_ac_songs[$ac_song]['title']) && $get_ac_songs[$ac_song]['title']!="")
								{
									echo $eceks; 
								}?></option>
<?php
						}					
?>
				</select>
<?php
			}
			else
			{
?>
				<input class="fieldText" type="text" disabled="true" value="No Songs Found." style=" left: 115px; position: relative;"/>
<?php
			}
		}
		elseif(isset($_POST['type_val']) && $_POST['type_val']!="" && $_POST['type_val']=='videos_post')
		{
			$get_ac_videos = $listob_suugest->get_all_videos($gen_log_id['general_user_id'],$gen_log_id['artist_id'],$gen_log_id['community_id']);
			if(count($get_ac_videos)>0)
			{
				$sort = array();
				foreach($get_ac_videos as $k=>$v)
				{
				
					$create_c = strpos($v['creator'],'(');
					$end_c1 = substr($v['creator'],0,$create_c);
					$end_c = trim($end_c1);
					
					$create_f = strpos($v['from'],'(');
					$end_f1 = substr($v['from'],0,$create_f);
					$end_f = trim($end_f1);
					
					$sort['creator'][$k] = strtolower($end_c);
					$sort['from'][$k] = strtolower($end_f);
					//$sort['track'][$k] = $v['track'];
					$sort['title'][$k] = strtolower($v['title']);
				}
				//var_dump($sort);
				array_multisort($sort['creator'], SORT_ASC, $sort['from'], SORT_ASC, $sort['title'], SORT_ASC,$get_ac_videos);
?>
				<select name="list_epm[]" size="5" id="list_epm[]" class="list" style="left:115px; position:relative;">
<?php
					for($ac_vid=0;$ac_vid<count($get_ac_videos);$ac_vid++)
					{
						$create_c = strpos($get_ac_videos[$ac_vid]['creator'],'(');
						$end_c = substr($get_ac_videos[$ac_vid]['creator'],0,$create_c);
						
						$create_f = strpos($get_ac_videos[$ac_vid]['from'],'(');
						$end_f = substr($get_ac_videos[$ac_vid]['from'],0,$create_f);
						
						$eceksv = "";
						if($end_c!="")
						{
							$eceksv = $end_c;
						}
						if($end_f!="")
						{
							if($end_c!="")
							{
								$eceksv .= " : ".$end_f;
							}
							else
							{
								$eceksv .= $end_f;
							}
						}
						if($get_ac_videos[$ac_vid]['title']!="")
						{
							if($end_c!="" || $end_f!="")
							{
								$eceksv .= " : ".$get_ac_videos[$ac_vid]['title'];
							}
							else
							{
								$eceksv .= $get_ac_videos[$ac_vid]['title'];
							}
						}
?>
						<option value="<?php
							if(isset($get_ac_videos[$ac_vid]['table_name']) && $get_ac_videos[$ac_vid]['table_name']=='general_artist') 
							{
								echo $get_ac_videos[$ac_vid]['video_id'].'~reg_art';
							}
							elseif(isset($get_ac_videos[$ac_vid]['table_name']) && $get_ac_videos[$ac_vid]['table_name']=='general_community')
							{
								echo $get_ac_videos[$ac_vid]['video_id'].'~reg_com';
							}
							elseif(isset($get_ac_videos[$ac_vid]['table_name']) && $get_ac_videos[$ac_vid]['table_name']=='general_media')
							{
								echo $get_ac_videos[$ac_vid]['id'].'~reg_med';
							}
							?>"><?php
							if(isset($get_ac_videos[$ac_vid]['video_name']) && $get_ac_videos[$ac_vid]['video_name']!="")
							{
								echo $get_ac_videos[$ac_vid]['video_name'];
							}
							elseif(isset($get_ac_videos[$ac_vid]['title']) && $get_ac_videos[$ac_vid]['title']!="")
							{
								echo $eceksv;
							}
							?></option>
<?php
					}
?>
				</select>
<?php
			}
			else
			{
?>
				<input class="fieldText" type="text" disabled="true" value="No Videos Found." style=" left: 115px; position: relative;"/>
<?php
			}
		}
	}
?>