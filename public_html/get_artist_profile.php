<?php
$result = array();
$errorflag='no';

$errormsg=array();
$inputparams=array();
$username='nouser';
$userpass='nopass';
$profileurl='leerick';
$galleryurl='leerick';
$format='json';

$data = file_get_contents("php://input");
$dataary=json_decode($data,true);

if (isset($dataary['format'])){    
    $format = strtolower($dataary['format']);
}
if (isset($dataary['galleryurl'])){    
    $galleryurl = strtolower($dataary['galleryurl']);
}
if(isset($dataary['profile'])) {
    $profileurl=$dataary['profile'];
}else {
    $errormessage='missing profile';
    $errormsg[]=array('errormsg'=>$errormessage);
    $result[] = array('error'=>$errormsg);
    $inputparams[] = array('profileurl'=>$profileurl);
    $inputparams[] = array('format'=>$format);
    $inputparams[] = array('post'=>$dataary);
    $result[] = array('inputparams'=>$inputparams);
    $errorflag='yes';
}

if ($errorflag='no'){
    include_once("commons/db.php");
    include_once('classes/Artist.php');
    include_once('classes/Media.php');
    include_once('classes/CountryState.php');
    include_once('classes/Type.php');
    include_once('classes/SubType.php');
    include_once('classes/MetaType.php');
    include_once('classes/ProfileDisplay.php');
    include_once('classes/Commontabs.php');
    include_once('classes/Addfriend.php');
    include_once("classes/ViewArtistProfileURL.php");
    include_once("classes/ViewCommunityProfileURL.php");
    include_once("classes/viewCommunityEventURL.php");
    include_once("classes/viewArtistEventURL.php");
    include_once("classes/viewArtistProjectURL.php");
    include_once("classes/viewCommunityProjectURL.php");
    include_once("classes/DisplayStatistics.php");
    include_once("classes/PopularityMeter.php");
    require_once('vimeo-vimeo-php-lib/vimeo.php');
    include_once("classes/viewArtistProjectURL.php");  

    // add user name to bucketname in s3 url
    $root = realpath($_SERVER["DOCUMENT_ROOT"]);
    if (!class_exists('S3')) require_once ($root.'/S3.php');    
    $s3 = new S3("", "");

    //require_once('vimeo-vimeo-php-lib/vimeo.php');
    
    //$all_stats = new DisplayStatistics();
    //$friend_obj_new = new Addfriend();
    //$button_test = new AjaxButtonsTest();
    //$button_test_com = new AjaxButtonsTestCom();
    //$popular_obj = new PopularityMeter();

    $key = '6b55800e7503e2cb8d69f93d2f7bda0df9fac434';
    $secret = 'f924407f6edf2e3398092516363de7b562a9d4a5';
    $vimeo = new phpVimeo($key, $secret);  
    
    $objArtist=new Artist();
    $objMedia=new Media();
    $display = new ProfileDisplay();
    $artist_check = $display->artistCheck($profileurl);
    
    $new_profile_class_obj = new ViewArtistProfileURL();
    $getgeneral =  $new_profile_class_obj->get_user_info($artist_check['artist_id']);
	$result[] = array('generaluser'=>$getgeneral);
    
    //$get_user_delete_or_not = $new_profile_class_obj->chk_user_delete($artist_check['artist_id']);
    //$get_common_id=$artist_check['artist_id'];
    
    
    ////var_dump($getgeneral);
    $getuser = $new_profile_class_obj->get_user_artist_profile($artist_check['artist_id']);
    $result[] = array('generalartist'=>$getuser);
    $getcountry = $new_profile_class_obj->get_user_country($artist_check['artist_id']);
    $result[] = array('country'=>$getcountry);
    $getstate = $new_profile_class_obj->get_user_state($artist_check['artist_id']);
    $result[] = array('state'=>$getstate);
    $gettype = $new_profile_class_obj->get_artist_user_type($artist_check['artist_id']);
    $result[] = array('type'=>$gettype);
    ////$get_type_dis = $new_profile_class_obj->get_artist_user($artist_check['artist_id']);
    ////$result[] = array('get_type_dis'=>$get_type_dis);
	
    $get_subtype = $new_profile_class_obj->get_artist_user_subtype($artist_check['artist_id']);
    $result[] = array('subtype'=>$get_subtype);
    ////$get_subtype_dis = $new_profile_class_obj->get_artist_user_subs($artist_check['artist_id']);
    ////$result[] = array('get_subtype_dis'=>$get_subtype_dis);
	
    ////var_dump($get_subtype);
    $get_metatype = $new_profile_class_obj->get_artist_user_metatype($artist_check['artist_id']);
    $result[] = array('metatype'=>$get_metatype);
    //$src = "http://artjcropprofile.s3.amazonaws.com/";
    //$src_list = "http://artjcropthumb.s3.amazonaws.com/";
    //$src1 = "../";
    
    //echo '<br> about to get all projects <br>';
    
    $new_project_class_obj = new viewArtistProjectURL();
    
    $foundemails=",";
    
    $project_exp = explode(',',$getuser['taggedprojects']);
    //$result[] = array('project_exp'=>$project_exp);
    $k=0;
    $p1=-1;
    if(isset($project_exp) && count($project_exp)>0 && !empty($project_exp))
    {
        $tagged_user_emails="";
        for($p=0;$p<count($project_exp);$p++)
        {
            $project_exp_n = explode('~',$project_exp[$p]);
            
            if($project_exp_n[1]=='art')
            {                
                $artistprojectid=$project_exp_n[0];
                $gettype_info = $new_project_class_obj->getType($artistprojectid);
	            $getsubtype_info = $new_project_class_obj->getSubType($artistprojectid);
                //echo '<br>CHECKING artistprojectid='.$artistprojectid.'checking type='.$gettype_info['name'].' subtype='.$getsubtype_info['name'].' album title='.$artistproject_info['title'].'<br>';
                if($gettype_info['name']=="Music" && $getsubtype_info['name']=="Album")
                {
                    $p1++;
                    $get_project[$p1] = mysql_fetch_assoc($new_profile_class_obj->get_artist_project($artistprojectid));

                    $AlbumProfileURL=$get_project[$p1]['profile_url'];
                    $featuredMediaAlbum[$AlbumProfileURL]['featured_media']=$get_project[$p1]['featured_media'];
                    $featuredMediaAlbum[$AlbumProfileURL]['media_id']=$get_project[$p1]['media_id'];
    
                    if($featuredMediaAlbum[$AlbumProfileURL]['featured_media']=="Song"){
                        $mediaid=$featuredMediaAlbum[$AlbumProfileURL]['media_id'];
                        $get_media_song=$new_profile_class_obj->get_Media_Song($mediaid);
                        $generaluserid= $get_media_song["general_user_id"];
                        //echo '<br>generaluserid='.$generaluserid;
                        $s3->resetAuth("", "",false,$generaluserid);
                        $medaudioURI=$s3->getNewURI("medaudio"); 
                        $songname=$get_media_song["song_name"];
                        $songimageurl=$new_profile_class_obj->get_audio_img($mediaid);
                        $featuredMediaAlbum[$AlbumProfileURL]["song_title"]=$get_media_song["songtitle"];
                        $featuredMediaAlbum[$AlbumProfileURL]["song_url"]=$medaudioURI.$songname;
                        $featuredMediaAlbum[$AlbumProfileURL]["creator_name"]=$get_media_song["creator"];
                        $featuredMediaAlbum[$AlbumProfileURL]["from_name"]=$get_media_song["from"];
                        $featuredMediaAlbum[$AlbumProfileURL]["song_image_url"]=$s3->replaceURI($songimageurl);
                        $featuredMediaAlbum[$AlbumProfileURL]["for_sale"]=$get_media_song["for_sale"];
                        $featuredMediaAlbum[$AlbumProfileURL]["price"]=$get_media_song["price"];
                        $featuredMediaAlbum[$AlbumProfileURL]["generaluserid"]=$generaluserid;
                    } else {
                        if($featuredMediaAlbum[$AlbumProfileURL]['featured_media']=="Video"){
                            $mediaid=$featuredMediaAlbum[$AlbumProfileURL]['media_id'];
                            $get_media_video=$new_profile_class_obj->get_Media_Video($mediaid); 
                            $generaluserid= $get_media_video["general_user_id"];
                            //echo '<br>generaluserid='.$generaluserid;
                            $s3->resetAuth("", "",false,$generaluserid);
                            $medaudioURI=$s3->getNewURI("medaudio"); 
                            $featuredMediaAlbum[$AlbumProfileURL]["videotitle"]=$get_media_video["videotitle"];
                            $featuredMediaAlbum[$AlbumProfileURL]["videolink"]=$get_media_video["videolink"];
                            $featuredMediaAlbum[$AlbumProfileURL]["generaluserid"]=$generaluserid;
                        }
                    }
                                  
                    
                    $ProjectCreatorsInfo=$get_project[$p1]['creators_info'];
                    //echo '<br>expcreatorinfo= ';
                    //print_r($ProjectCreatorsInfo);
                    //echo '<br>';
                    $ProjectCreatorsProfileURL[$get_project[$p1]['id']]=$new_profile_class_obj->get_creators_profileURL($ProjectCreatorsInfo);
                    
                     //echo '<br> about to get all artists for a project= '.$get_project[$p]['id'].' <br>';
                
                    // get all artists for this project - check if already searched for this email             
              
                    $tagged_user_emails=$getgeneral['email'].$get_project[$p]['tagged_user_email'];
                
                    $exp_tagged_user_emails = explode(',',$tagged_user_emails);
	
                    if(isset($exp_tagged_user_emails) && count($exp_tagged_user_emails)>0)
                    {
                        for($j=0;$j<count($exp_tagged_user_emails);$j++)
                        {
                            //echo '<br>email='.$exp_tagged_user_emails[$j].' <br>';     
                            $pos=strpos($foundemails,$exp_tagged_user_emails[$j]);
                            if ($pos===false)
                            {                            
                                $foundemails=$foundemails.','.$exp_tagged_user_emails[$j];
                           
                                if($exp_tagged_user_emails[$j]!="")
                                { 
                                    //echo '<br>getting artists for email='.$exp_tagged_user_emails[$j].' <br>'; 
                                    $get_all_user_artists_for_project[$k] =$new_profile_class_obj->get_user_artist($exp_tagged_user_emails[$j]);
                                    

                                    //print_r($get_all_user_artists_for_project); 
                                    $k++;
                                    //echo '<br>emails in project index ='.$j.' <br>';
                                    //echo '<br>emails total index ='.$k.' <br>';
                                }
                                //echo '<br>project index ='.$p.' <br>';
                            } // skipping this email already processed
                        } // end of get artists for project loop
                    } //  no tagged emails
                } // not Music Album
                else
                {
                    continue;
                }
            }
            elseif($project_exp_n[1]=='com')
            {
                $get_project[$p] = mysql_fetch_assoc($new_profile_class_obj->get_community_project($project_exp_n[0]));
            }
        } // end of projects loop
        
        if (count($get_project)>6)
        {
            $allbuttons["projects"]=true;
        }
        else
        {
            $allbuttons["projects"]=false;        
        }
        
        array_sort_by_column($get_project,'id',SORT_DESC);
        $result[] = array('projects'=>array_slice($get_project, 0, 6));        
    }    

//    print ( '<pre>' )  ;
//print_r($get_all_user_artists_for_project);
//print ( '</pre>' )  ;
    
//    die;

    //$get_media_Info = $new_profile_class_obj->get_artist_Info($artist_check['artist_id']);
    //$result[] = array('get_media_Info'=>$get_media_Info);
    //$date=date('y-m-d');

    $event_exp_up = explode(',',$getuser['taggedupcomingevents']);

    if(isset($event_exp_up) && count($event_exp_up)>0)
    {
        for($i=0;$i<count($event_exp_up);$i++)
        {
            if($event_exp_up[$i]==""){continue;}
            else
            {
                $act_id = explode('~',$event_exp_up[$i]);
				
                if($act_id[1]=='art')
                {
                    $get_upcoming_event[$i] = mysql_fetch_assoc($new_profile_class_obj->upcomingArtistEvents($act_id[0],$date));
                }
                elseif($act_id[1]=='com')
                {
                    $get_upcoming_event[$i] = mysql_fetch_assoc($new_profile_class_obj->upcomingComEvents($act_id[0],$date));
                }
            }
        }
        
        if (count($get_upcoming_event)>1)
        {
            $allbuttons["upcomingevents"]=true;
        }
        else
        {
            $allbuttons["upcomingevents"]=false;        
        }        
        
        
        array_sort_by_column($get_upcoming_event,'id',SORT_DESC);
        $result[] = array('upcomingevents'=>array_slice($get_upcoming_event, 0, 1)); 
        //$result[] = array('upcomingevents'=>$get_upcoming_event);
    }
    else{
        $result[] = array('upcomingevents'=>array());
        $noevent_up =1;
    }
    
    $event_exp_rec = explode(',',$getuser['taggedrecordedevents']);
	
    if(isset($event_exp_rec) && count($event_exp_rec)>0)
    {
        for($i=0;$i<count($event_exp_rec);$i++)
        {
            if($event_exp_rec[$i]==""){continue;}
            else
            {
                $act_id = explode('~',$event_exp_rec[$i]);
				
                if($act_id[1]=='art')
                {
                    $get_recorded_event[$i] = mysql_fetch_assoc($new_profile_class_obj->recordedArtistEvents($act_id[0],$date));
                }
                elseif($act_id[1]=='com')
                {
                    $get_recorded_event[$i] = mysql_fetch_assoc($new_profile_class_obj->recordedComEvents($act_id[0],$date));
                }
            }            
        }
        
        
        if (count($get_recorded_event)>1)
        {
            $allbuttons["recordedevents"]=true;
        }
        else
        {
            $allbuttons["recordedevents"]=false;        
        }         
        
        array_sort_by_column($get_recorded_event,'id',SORT_DESC);
        $result[] = array('recordedevents'=>array_slice($get_recorded_event, 0, 1)); 
        //$result[] = array('recordedevents'=>$get_recorded_event);
    }
    else{
        $result[] = array('recordedevents'=>array());
        $noevent_rec =1;
    }


    $get_media=explode(',',$getuser['taggedsongs']);

    //echo '<br> tagged songs <br>';
    //print_r($get_media);

    if(isset($get_media) && count($get_media)>0)
    {
        for($i=0;$i<count($get_media);$i++)
        {
            //echo '<br>'.$i.' ' .$get_media[$i].'<br>';
            if($get_media[$i]==""||$get_media[$i]==" "||$get_media[$i]==Null){continue;}
            else
            {                
                $get_mediasong_info[$i] = $new_profile_class_obj->get_Media_Song($get_media[$i]);
                $imagename= $new_profile_class_obj->get_audio_img($get_media[$i]);
                $songimagefile[$get_media[$i]]=$imagename;
                //$songimagefile[]=array($get_media[$i]=>$imagename);
                //$songimagefile1 = array_shift($songimagefile);
                
                //echo '<br> for key '.$get_media[$i].' value is '.$songimagefile1[$get_media[$i]].' <br>';
                
                //print_r($get_mediasong_info[$i]);
                //echo '<br> track = '.$get_mediasong_info[$i]['track'].'<br>';
                //echo '<br> song file= '.$get_mediasong_info[$i]['song_name'].'<br>';
            }
        }
        
        if (count($get_mediasong_info)>6)
        {
            $allbuttons["mediasong"]=true;
        }
        else
        {
            $allbuttons["mediasong"]=false;        
        }          
        
        array_sort_by_column($get_mediasong_info,'id',SORT_DESC);
        
        $get_mediasong_info2 = array_slice($get_mediasong_info, 0, 6);  
        
        
        //print_r($songimagefile);
        //die;
        
        // Obtain a list of columns
        foreach ($get_mediasong_info2 as $key => $row) {
            $from[$key]  = $row['from'];
            $track[$key] = $row['track'];
        }

        // Sort the data with volume descending, edition ascending
        // Add $data as the last parameter, to sort by the common key
        array_multisort($from, SORT_ASC, $track, SORT_ASC, $get_mediasong_info2);
        $result[] = array('mediasong'=>$get_mediasong_info2); 
        $result[] = array('songimagefile'=>$songimagefile);
        
        // Get first 6 unique from songs + songs by date desc
        //array_sort_by_column($get_mediasong_info,'from',SORT_ASC);
        
        foreach ($get_mediasong_info as $key => $row) {
            $from2[$key]  = $row['from'];
            $lastmodifieddate2[$key] = $row['lastmodifieddate'];
        }  
     
        array_multisort($lastmodifieddate2, SORT_DESC, $from2, SORT_ASC, $get_mediasong_info);
        
        $savefrom='NULL';
        $count=0;
        foreach($get_mediasong_info as $key1 => $row1) 
        {    
            $pos=strpos($savefrom,$row1['from']);
            if ($pos==false) 
            {
                //echo 'from= '.$row1['from'].' title = '.$row1['title'];
                $get_mediasong_info_select[] = $row1;    
                $savefrom=$savefrom.','.$row1['from'];
                $count++;
            } else
            {
                $get_mediasong_info_select2[] = $row1;
            }
            If ($count == 6)
            {
                break;
            }
        }
        
        if (count($get_mediasong_info_select)<6) 
        {
            //array_sort_by_column($get_mediasong_info_select2,'id',SORT_DESC);
            $k=0;
            for($j=count($get_mediasong_info_select);$j<6;$j++)
            {
                $get_mediasong_info_select[] = $get_mediasong_info_select2[$k];
                $k++;
            }
        }
        //array_sort_by_column($get_mediasong_info_select,'lastmodifieddate',SORT_DESC);
        
        if (count($get_mediasong_info_select)>6)
        {
            $allbuttons["mediasongselect"]=true;
        }
        else
        {
            $allbuttons["mediasongselect"]=false;        
        }        
    }
    else{
    $nosongs =1;
    }
   
    // add alluserartistsforproject to result array
    $result[] = array('alluserartistsforproject'=>$get_all_user_artists_for_project);    
    
    $get_permission = $new_profile_class_obj->get_all_permission($getgeneral['general_user_id']);
    $result[] = array('get_permission'=>$get_permission);
    ////$get_user_id = $new_profile_class_obj->get_loggedin_user_id($_SESSION['login_email']);
    ////$find_member = $new_profile_class_obj->get_member_or_not($getgeneral['general_user_id']);
    ////$result[] = array('find_member'=>$find_member);

    $foundurls=",";
    $foundvideos=",";
    
    $outputvideoindex=0;

    $exp_tagged_videos=explode(',',$getuser['taggedvideos']);                    

    //echo '<br> tagged videos <br>';
    //print_r($exp_tagged_videos);
    //print_r('<br> Count='.count($exp_tagged_videos));

    if(isset($exp_tagged_videos) && count($exp_tagged_videos)>0)
    {
        for($tagvideoindex=0;$tagvideoindex<count($exp_tagged_videos);$tagvideoindex++)
        {
            if($exp_tagged_videos[$tagvideoindex]!="")
            {
                $pos=strpos($foundvideos,$exp_tagged_videos[$tagvideoindex]);
                //print_r('foundvideo='.$foundvideos.' taggedvideo='.$exp_tagged_videos[$tagvideoindex].' pos='.$pos);
                if ($pos==false)
                {                            
                    $foundvideos=$foundvideos.','.$exp_tagged_videos[$tagvideoindex];                      
                        
                    // echo '<br> tagvideoindex '.$tagvideoindex.' tagged video ' .$exp_tagged_videos[$tagvideoindex].'<br>';
                    if($exp_tagged_videos[$tagvideoindex]==""||$exp_tagged_videos[$tagvideoindex]==" "||$exp_tagged_videos[$tagvideoindex]==Null){continue;}
                    else
                    {                
                        $exp_tagged_videos_info[$outputvideoindex] = $new_profile_class_obj->get_Media_video($exp_tagged_videos[$tagvideoindex]);
                        //$imagename= $new_profile_class_obj->get_audio_img($exp_tagged_videos[$tagvideoindex]);
                                    
			            $get_video_image = $new_profile_class_obj->getvideoimage($exp_tagged_videos[$tagvideoindex]);
			                        
                        //echo '<br>Called getvideoimage <br>';
                        //print_r($get_video_image);
                                    
                                    
			            if(!empty($get_video_image))
			            {
				            if($get_video_image[1]=='youtube')
				            {
                                $imagename="https://img.youtube.com/vi/".$get_video_image[0]."/default.jpg";
                            }                                     
				            elseif($get_video_image[1]=='vimeo' && $get_video_image[0]!=NULL)
				            {					
					            $video = $vimeo->call('vimeo.videos.getInfo', array('video_id' => $get_video_image[0]));
					            $imagename = $video->video[0]->thumbnails->thumbnail[1]->_content; 
                            }
                        }else
                        {
                            $imagename ="https://comjcropprofile.s3.amazonaws.com/Noimage.png";
                        }
                        //echo '<br>imagename = '.$imagename.' <br>';                                    
                                    
                                    
                        $videoimagefile[$exp_tagged_videos[$tagvideoindex]]=$imagename;
                                    
                        $profileurlforvideo[$exp_tagged_videos[$tagvideoindex]]=$listartists_exp[$artistindex];                                     
                        //echo '<br> output videoindex '.$outputvideoindex.'<br>';
                        //echo '<br> for key '.$exp_tagged_videos[$tagvideoindex].' value is '.$videoimagefile[$exp_tagged_videos[$tagvideoindex]].' <br>';                
                        //print_r($exp_tagged_videos_info[$outputvideoindex]);
                        $outputvideoindex++;
                    }
                } // skip video - duplicate
            } // no videos
        } // end of video loop
    } // no videos
        
    if (count($exp_tagged_videos_info)>1)
    {
        $allbuttons["mediavideo"]=true;
    }
    else
    {
        $allbuttons["mediavideo"]=false;        
    }
        
    array_sort_by_column($exp_tagged_videos_info,'id',SORT_DESC);
    $result[] = array('mediavideo'=>array_slice($exp_tagged_videos_info, 0, 1));  

    //print_r($songimagefile);
    //die;
        
    // Obtain a list of columns
    //foreach ($exp_tagged_videos_info as $key => $row) {
    //    $from[$key]  = $row['creator'];
    //    $track[$key] = $row['title'];
    //}

    // Sort the data with volume descending, edition ascending
    // Add $data as the last parameter, to sort by the common key
    //array_multisort($from, SORT_ASC, $track, SORT_ASC, $exp_tagged_videos_info);
                        
    //$result[] = array('mediavideo'=>$exp_tagged_videos_info); 
    $result[] = array('videoimagefile'=>$videoimagefile);
    $result[] = array('profileurlforvideo'=>$profileurlforvideo);
    
    $foundurls=",";
    $foundgalleries=",";
    $dummy_gp = array();
	$galleryImagesDataIndex = 0;
	$galleryImagesData = ""; 
    $foundgalleryindex=-1;
    $outputvideoindex=0;   
    
    $userartist =$new_profile_class_obj->get_user_artist_profileurl($profileurl);
    $exp_tagged_galleries=explode(',',$userartist['taggedgalleries']);                    

    //echo '<br> tagged galleries <br>';
    //print_r($exp_tagged_galleries);
                    
    if(isset($exp_tagged_galleries) && count($exp_tagged_galleries)>0)
    {
        for($taggalleryindex=0;$taggalleryindex<count($exp_tagged_galleries);$taggalleryindex++)
        {
            $pos=strpos($foundgalleries,$exp_tagged_galleries[$taggalleryindex]);
            if ($pos===false)
            {                            
                $foundgalleries=$foundgalleries.','.$exp_tagged_galleries[$taggalleryindex];                      
                        
                if($exp_tagged_galleries[$taggalleryindex]==""||$exp_tagged_galleries[$taggalleryindex]==" "||$exp_tagged_galleries[$taggalleryindex]==Null){continue;}
                else
                {  
                    //echo '<br>111111 taggalleryindex '.$taggalleryindex.' tagged gallery ' .$exp_tagged_galleries[$taggalleryindex].'<br>';
                
                    // get gallery from general media table
                    //$exp_tagged_gallery_info = array();
                    //for($i=0;$i<count($exp_tagged_galleries);$i++)
                    //{
                    $foundgalleryindex++;
                    $temp_tagged_gallery_info=$new_profile_class_obj->get_gallery($exp_tagged_galleries[$taggalleryindex]);
                    //printr($temp_tagged_gallery_info,'temp_tagged_gallery_info');
                    
                    ////}
                    //echo '<br> tagged gallery info <br>';
                    //print_r($exp_tagged_gallery_info);
                                    
                    //// sort images by play count
                    //$sorttaggedgallery = array();
                    //foreach($exp_tagged_gallery_info as $k=>$v)
                    //{		
                    //    $sorttaggedgallery['play_count'][$k] = $v['play_count'];
                    //}
                    //if(!empty($sorttaggedgallery))
                    //{
                    //    array_multisort($sorttaggedgallery['play_count'], SORT_DESC,$exp_tagged_gallery_info);
                    //}

                    //echo '<br> sorted general media in tagged galleries <br>';
                    //print_r($exp_tagged_gallery_info);
                                    
                    //$dummy_gp = array();
                    //$galleryImagesDataIndex = 0;
                    //$galleryImagesData = "";
                    
                    
                    //// Temporary 
                    //if (cleanstring($temp_tagged_gallery_info['title'])!=$galleryurl){
                    //    //echo '<br>gallery '.$galleryurl.' did not match '.cleanstring($exp_tagged_gallery_info[$foundgalleryindex]['title']).'  for project ' .$profileurl. ' <br>';    
                    //    continue;
                    //}
                    
                    //echo '<br>found gallery '.$galleryurl.'  for artist ' .$profileurl. ' <br>';
                    $exp_tagged_gallery_info[$foundgalleryindex] = $temp_tagged_gallery_info;
                    $loopgalleryid=$exp_tagged_gallery_info[$foundgalleryindex]['id'];
                    $artistprofileurl[$loopgalleryid]=$profileurl;
                    $GalleryImageNames="";
                    $GalleryTitleNames="";
                    //echo "<br>222222 gallery id = ".$loopgalleryid." <br>";                                    
                    //printr($exp_tagged_gallery_info[$foundgalleryindex],'exptaggedgalleryinfo');
                    if($loopgalleryid=="")
                    {
                        //echo '<br>tagged gallery NOT FOUND = '.$loopgalleryid.' <br>';
                        continue;
                    }
                    else
                    {
                        //echo "<br> processing gallery id = ".$loopgalleryid." <br>";
                        //print_r($exp_tagged_gallery_info);
                        if(in_array($loopgalleryid,$dummy_gp))
                        {
                            //echo "<br> duplicate gallery id = ".$loopgalleryid;
                        }
                        else
                        {
                            //echo '<br> Get images from general_artist_gallery <br>';
                            $dummy_gp[] = $loopgalleryid;
                            //echo "<br> Common id= ".$get_common_id."<br>";
                            //print_r($dummy_gp); 
                            //Get count of images in general_artist_gallery
                            $Get_count_img_at_reg=$new_profile_class_obj->get_count_Gallery_at_register($get_common_id,$loopgalleryid);
                            if($Get_count_img_at_reg['count(*)']>0)
                            {
                                //echo '<br>Found '.$Get_count_img_at_reg['count(*)']. ' gallery images<br>'; 
                                $Get_img_at_reg=$new_profile_class_obj->get_Gallery_at_register($get_common_id,$loopgalleryid);
                                $Get_medi_cover_pic="";
                                                
                                $play_array = array();
                                if($Get_img_at_reg!="" && $Get_img_at_reg!=Null)
                                {
                                    if(mysql_num_rows($Get_img_at_reg)>0)
                                    {
                                        while($row = mysql_fetch_assoc($Get_img_at_reg))
                                        {
                                            $play_array[] = $row;
                                        }
                                        $GalleryOrgPath[$loopgalleryid]="https://reggallery.s3.amazonaws.com/";
                                        $GalleryThumbPath[$loopgalleryid]="https://reggalthumb.s3.amazonaws.com/";							
                                        //${'orgPath'.$taggalleryindex}="https://reggallery.s3.amazonaws.com/";
                                        //${'thumbPath'.$taggalleryindex}="https://reggalthumb.s3.amazonaws.com/";
                                        $register_cover_pic=$play_array[0]['cover_pic'];
                                        $register_No_pic=$play_array[0]['image_name'];
                                        $GalleryImageName[$loopgalleryid]=setGalleryImage("","","",$register_cover_pic,$register_No_pic);
                                        //$exp_tagged_gallerieslery=$new_profile_class_obj->get_Gallery_title_at_register($play_array[0]['gallery_id']);
                                        //$galleryImagesDataIndex="";
                                        for($l=0;$l<count($play_array);$l++)
                                        {
                                            $loopmediaid=$play_array[$l]['id'];                                                        
                                            $mediaSrc = $play_array[0]['image_name'];
                                            $filePath = "https://reggallery.s3.amazonaws.com/".$mediaSrc;
                                            //${'galleryImagesData'.$taggalleryindex}[$galleryImagesDataIndex]="";
                                            $galleryImagesImageName[$loopmediaid]=$play_array[$l]['image_name'];
                                            $galleryImagesImageTitle[$loopmediaid]=$play_array[$l]['image_title']; 
                                            $GalleryImageNames=$GalleryImageNames.",".$galleryImagesImageName[$loopmediaid]=$play_array[$l]['image_name'];
                                            $GalleryImagetitles=$GalleryImagetitles.",".$galleryImagesImageName[$loopmediaid]=$play_array[$l]['image_title'];                                                            
                                            //${'galleryImagesData'.$taggalleryindex}[$galleryImagesDataIndex] = $play_array[$l]['image_name'];
                                            //${'galleryImagestitle'.$taggalleryindex}[$galleryImagesDataIndex] = $play_array[$l]['image_title'];
                                            $galleryImagesDataIndex++;
                                            //echo '<br> $galleryImagesDataIndex = '.$galleryImagesDataIndex.'<br>';
                                        }
                                        $GalleryImageNamesList[$loopgalleryid]=$GalleryImageNames;
                                        $GalleryImageTitlesList[$loopgalleryid]=$GalleryImagetitles;                                                        
                                    }
                                }
                            } // no images found in general_artist_gallery
                            else
                            { // get images from media_images for gallery id not = 0
                                //echo '<br>Found '.$Get_count_img_at_reg['count(*)']. ' gallery images<br>'; 
                                //echo '<br>now try to get images from media_images for gallery id not = 0 for real gallery id = '.$loopgalleryid.' <br>';
                                //$exp_tagged_galleries=$new_profile_class_obj->get_gallery($loopgalleryid);
                                $Get_medi_cover_pic=$new_profile_class_obj->get_media_cover_pic($loopgalleryid);
                                //printr($Get_medi_cover_pic,'mediacoverpic');
                                $sql_play1 = mysql_query("SELECT * FROM media_images WHERE media_id='".$loopgalleryid."' AND gallery_id!=0");
						
                                $play_array = array();
                                if($sql_play1!="" && $sql_play1!=Null)
                                {
                                    //echo '<br>Number of Images Found = '.mysql_num_rows($sql_play1). ' in gallery id '.$loopgalleryid.'<br>';
                                    if(mysql_num_rows($sql_play1)>0)
                                    {
                                        while($row = mysql_fetch_assoc($sql_play1))
                                        {
                                            $play_array[] = $row;
                                        }
                                        $GalleryOrgPath[$loopgalleryid]="https://medgallery.s3.amazonaws.com/";
                                        $GalleryThumbPath[$loopgalleryid]="https://medgalthumb.s3.amazonaws.com/";
                                        $GalleryImageName[$loopgalleryid]=setGalleryImage($Get_medi_cover_pic['profile_image'],$exp_tagged_gallery_info[$foundgalleryindex]['gallery_title'],$exp_tagged_gallery_info[$foundgalleryindex]['title'],"","");
                                        //${'orgPath'.$taggalleryindex}="https://medgallery.s3.amazonaws.com/";
                                        //${'thumbPath'.$taggalleryindex}="https://medgalthumb.s3.amazonaws.com/";
                                        for($l=0;$l<count($play_array);$l++)
                                        {
                                            $loopmediaid=$play_array[$l]['id'];
                                            //echo '<br>loop media id = '.$loopmediaid.' <br>';
                                            $mediaSrc = $play_array[0]['image_name'];
                                            $filePath = "https://medgallery.s3.amazonaws.com/".$mediaSrc;
									            
                                            //echo '<br>file path= '.$filePath.' <br>';
                                            $galleryImagesImageName[$loopmediaid]=$play_array[$l]['image_name'];
                                            $galleryImagesImageTitle[$loopmediaid]=$play_array[$l]['image_title']; 
                                            $GalleryImageNames=$GalleryImageNames.",".$galleryImagesImageName[$loopmediaid]=$play_array[$l]['image_name'];
                                            $GalleryImagetitles=$GalleryImagetitles.",".$galleryImagesImageName[$loopmediaid]=$play_array[$l]['image_title']; 
                                            //${'galleryImagesData'.$taggalleryindex}[$galleryImagesDataIndex] = $play_array[$l]['image_name'];
                                            //${'galleryImagestitle'.$taggalleryindex}[$galleryImagesDataIndex] = $play_array[$l]['image_title'];
                                            //echo '<br> galery image name '.$galleryImagesImageName[$loopmediaid];
                                            //echo '<br> galery image title '.$galleryImagesImageTitle[$loopmediaid];
                                            $galleryImagesDataIndex++;
                                            //echo '<br> $galleryImagesDataIndex = '.$galleryImagesDataIndex.'<br>';
                                        } // end of loop processing images
                                        $GalleryImageNamesList[$loopgalleryid]=$GalleryImageNames;
                                        $GalleryImageTitlesList[$loopgalleryid]=$GalleryImagetitles;                                                        
                                    } // no images fond
                                    else
                                    { // get images from media_images with gallery id = 0
                                        //echo '<br>get images from media_images with gallery id = 0 for real gallery id = '.$loopgalleryid.' <br>';
                                        $GalleryOrgPath[$loopgalleryid]="https://medsingleimage.s3.amazonaws.com/";
                                        $GalleryThumbPath[$loopgalleryid]="https://medgalthumb.s3.amazonaws.com/"; 
                                        $GalleryImageName[$loopgalleryid]=setGalleryImage($Get_medi_cover_pic['profile_image'],$exp_tagged_gallery_info[$foundgalleryindex]['gallery_title'],$exp_tagged_gallery_info[$foundgalleryindex]['title'],"","");
                                        //${'orgPath'.$taggalleryindex}="https://medsingleimage.s3.amazonaws.com/";
                                        //${'thumbPath'.$taggalleryindex}="https://medgalthumb.s3.amazonaws.com/";
								
                                        $sql_play1 = mysql_query("SELECT * FROM media_images WHERE media_id='".$loopgalleryid."' AND gallery_id=0");
					
                                        $play_array = array();
                                        if($sql_play1!="" && $sql_play1!=Null)
                                        {
                                            if(mysql_num_rows($sql_play1)>0)
                                            {
                                                while($row = mysql_fetch_assoc($sql_play1))
                                                {
                                                    $play_array[] = $row;
                                                }
										
                                                for($l=0;$l<count($play_array);$l++)
                                                {
                                                    $loopmediaid=$play_array[$l]['id'];    
                                                    $mediaSrc = $play_array[0]['image_name'];
                                                    if($play_array[0]['for_sale']==1)
                                                    {
                                                        $GalleryOrgPath[$loopgalleryid]="https://medgalhighres.s3.amazonaws.com/";
                                                        $GalleryThumbPath[$loopgalleryid]="https://medgalhighres.s3.amazonaws.com/";
                                                        $GalleryImageName[$loopgalleryid]=setGalleryImage($Get_medi_cover_pic['profile_image'],$exp_tagged_gallery_info[$foundgalleryindex]['gallery_title'],$exp_tagged_gallery_info[$foundgalleryindex]['title'],"","");                                                                        
                                                        $filePath = "https://medgalhighres.s3.amazonaws.com/".$mediaSrc;
                                                        //${'orgPath'.$taggalleryindex}="https://medgalhighres.s3.amazonaws.com/";
                                                        //${'thumbPath'.$taggalleryindex}="https://medgalhighres.s3.amazonaws.com/";
                                                    }
                                                    else
                                                    {
                                                        $filePath = "https://medsingleimage.s3.amazonaws.com/".$mediaSrc;
                                                    }
                                                    $galleryImagesImageName[$loopmediaid]=$play_array[$l]['image_name'];
                                                    $galleryImagesImageTitle[$loopmediaid]=$play_array[$l]['image_title']; 
                                                    $GalleryImageNames=$GalleryImageNames.",".$galleryImagesImageName[$loopmediaid]=$play_array[$l]['image_name'];
                                                    $GalleryImagetitles=$GalleryImagetitles.",".$galleryImagesImageName[$loopmediaid]=$play_array[$l]['image_title']; 											
                                                    //${'galleryImagesData'.$taggalleryindex}[$galleryImagesDataIndex] = $play_array[$l]['image_name'];
                                                    //${'galleryImagestitle'.$taggalleryindex}[$galleryImagesDataIndex] = $play_array[$l]['image_title'];
                                                    //echo $play_array[$l]['image_title'];
                                                    //die;
                                                    //echo ${'galleryImagesData'.$taggalleryindex}[$galleryImagesDataIndex];
                                                    $galleryImagesDataIndex++;
                                                    //echo '<br> $galleryImagesDataIndex = '.$galleryImagesDataIndex.'<br>';
                                                } // end of loop getting image names and titles
                                                $GalleryImageNamesList[$loopgalleryid]=$GalleryImageNames;
                                                $GalleryImageTitlesList[$loopgalleryid]=$GalleryImagetitles;                                                                
                                            } // no images found
                                        } // no images
                                    } // end of get images from media_images with gallery id = 0
                                } // no images found
                                else
                                { // get images from media_images with gallery id = 0
                                    //echo '<br>get images from media_images with gallery id = 0 for real gallery id = '.$loopgalleryid.' <br>';
                                    $GalleryOrgPath[$loopgalleryid]="https://medsingleimage.s3.amazonaws.com/";
                                    $GalleryThumbPath[$loopgalleryid]="https://medgalthumb.s3.amazonaws.com/";
                                    $GalleryImageName[$loopgalleryid]=setGalleryImage($Get_medi_cover_pic['profile_image'],$exp_tagged_gallery_info[$foundgalleryindex]['gallery_title'],$exp_tagged_gallery_info[$foundgalleryindex]['title'],"","");                                                    
                                    //${'orgPath'.$taggalleryindex}="https://medsingleimage.s3.amazonaws.com/";
                                    //${'thumbPath'.$taggalleryindex}="https://medgalthumb.s3.amazonaws.com/";
							
                                    $sql_play1 = mysql_query("SELECT * FROM media_images WHERE media_id='".$loopgalleryid."' AND gallery_id=0");
                                    $play_array = array();
                                    if($sql_play1!="" && $sql_play1!=Null)
                                    {
                                        if(mysql_num_rows($sql_play1)>0)
                                        {
                                            while($row = mysql_fetch_assoc($sql_play1))
                                            {
                                                $play_array[] = $row;
                                            }
									
                                            for($l=0;$l<count($play_array);$l++)
                                            {
                                                $loopmediaid=$play_array[$l]['id'];
                                                $mediaSrc = $play_array[0]['image_name'];
                                                if($play_array[0]['for_sale']==1)
                                                {
                                                    $filePath = "https://medgalhighres.s3.amazonaws.com/".$mediaSrc;
                                                    $GalleryOrgPath[$loopgalleryid]="https://medgalhighres.s3.amazonaws.com/";
                                                    $GalleryThumbPath[$loopgalleryid]="https://medgalhighres.s3.amazonaws.com/";
                                                    $GalleryImageName[$loopgalleryid]=setGalleryImage($Get_medi_cover_pic['profile_image'],$exp_tagged_gallery_info[$foundgalleryindex]['gallery_title'],$exp_tagged_gallery_info[$foundgalleryindex]['title'],"","");                                                                    
                                                    //${'orgPath'.$taggalleryindex}="https://medgalhighres.s3.amazonaws.com/";
                                                    //${'thumbPath'.$taggalleryindex}="https://medgalhighres.s3.amazonaws.com/";
                                                }
                                                else
                                                {
                                                    $filePath = "https://medsingleimage.s3.amazonaws.com/".$mediaSrc;
                                                }
                                                $galleryImagesImageName[$loopmediaid]=$play_array[$l]['image_name'];
                                                $galleryImagesImageTitle[$loopmediaid]=$play_array[$l]['image_title'];	 
                                                $GalleryImageNames=$GalleryImageNames.",".$galleryImagesImageName[$loopmediaid]=$play_array[$l]['image_name'];
                                                $GalleryImagetitles=$GalleryImagetitles.",".$galleryImagesImageName[$loopmediaid]=$play_array[$l]['image_title']; 									
                                                //${'galleryImagesData'.$taggalleryindex}[$galleryImagesDataIndex] = $play_array[$l]['image_name'];
                                                //${'galleryImagestitle'.$taggalleryindex}[$galleryImagesDataIndex] = $play_array[$l]['image_title'];
                                                //echo $play_array[$l]['image_title'];
                                                //die;
                                                //echo ${'galleryImagesData'.$taggalleryindex}[$galleryImagesDataIndex];
                                                $galleryImagesDataIndex++;
                                                //echo '<br> $galleryImagesDataIndex = '.$galleryImagesDataIndex.'<br>';
                                            } // end of loop processing images
                                            $GalleryImageNamesList[$loopgalleryid]=$GalleryImageNames;
                                            $GalleryImageTitlesList[$loopgalleryid]=$GalleryImagetitles;                                                            
                                        } // no images found
                                    } // no images found
                                } // end of if 						                        
                            } // end of if 
                        } // end of processing galleries                                         
                    } // end  of gallery id found
                } // tagged galleries loop
            } // skip gallery
        } // end of for gallery loop
    } // no galleries
        
    if (count($exp_tagged_gallery_info)>1)
    {
        $allbuttons["galleries"]=true;
    }
    else
    {
        $allbuttons["galleries"]=false;        
    }

    array_sort_by_column($exp_tagged_gallery_info,'id',SORT_DESC);
    $result[] = array('galleries'=>array_slice($exp_tagged_gallery_info, 0, 1));  

    //print_r($songimagefile);
    //die;
        
    // Obtain a list of columns
    //foreach ($exp_tagged_gallery_info as $key => $row) {
    //    $from[$key]  = $row['creator'];
    //    $track[$key] = $row['title'];
    //}

    // Sort the data with volume descending, edition ascending
    // Add $data as the last parameter, to sort by the common key
    //array_multisort($from, SORT_ASC, $track, SORT_ASC, $exp_tagged_gallery_info);

  
    //$result[] = array('galleries'=>$exp_tagged_gallery_info);
    $result[] = array('GalleryImageName'=>$GalleryImageName);
    $result[] = array('artistprofileurl'=>$artistprofileurl);
    $result[] = array('GalleryOrgPath'=>$GalleryOrgPath);
    $result[] = array('GalleryThumbPath'=>$GalleryThumbPath);
    $result[] = array('GalleryImageNamesList'=>$GalleryImageNamesList);
    $result[] = array('GalleryImageTitlesList'=>$GalleryImageTitlesList); 
    
    //    if($get_gallery['gallery_title'] !="" || $get_gallery['title']!="")
    //{
    //    if(isset(${'galleryImagesData'.$g}))
    //    {
    //        ${'galleryImagesData'.$g} = implode(",", ${'galleryImagesData'.$g});
    //        ${'galleryImagestitle'.$g} = implode(",", ${'galleryImagestitle'.$g});
    //    }
    
//echo '<br> Printing data before returning json<br>';    
//print ( '<pre>' );
//print_r($exp_tagged_gallery_info);
//print_r($galleryImagesImageName);
//print_r($galleryImagesImageTitle);
//print_r($GalleryOrgPath);
//print_r($GalleryThumbPath);
//print ( '</pre>' );    
//die;

  
    // add alluserartistsforproject to result array
  
    $result[] = array('ProjectCreatorsProfileURL'=>$ProjectCreatorsProfileURL);   
    
    $songinfo="";
    
    if($getuser["featured_media"]=="Song"){
        $mediaid=$getuser["media_id"];
        $get_featured_media_for_artist[$mediaid]=$new_profile_class_obj->get_Media_Song($mediaid);
        $generaluserid= $get_featured_media_for_artist[$mediaid]["general_user_id"];
        //echo '<br>generaluserid='.$generaluserid;
        $s3->resetAuth("", "",false,$generaluserid);
        $medaudioURI=$s3->getNewURI("medaudio"); 
        $songname=$get_featured_media_for_artist[$mediaid]["song_name"];
        $songimageurl=$new_profile_class_obj->get_audio_img($mediaid);
        $songinfo[$mediaid]["featured_media"]=$getuser["featured_media"];
        $songinfo[$mediaid]["media_id"]=$getuser["media_id"];
        $songinfo[$mediaid]["song_title"]=$get_featured_media_for_artist[$mediaid]["songtitle"];
        $songinfo[$mediaid]["song_url"]=$medaudioURI.$songname;
        $songinfo[$mediaid]["creator_name"]=$get_featured_media_for_artist[$mediaid]["creator"];
        $songinfo[$mediaid]["from_name"]=$get_featured_media_for_artist[$mediaid]["from"];
        $songinfo[$mediaid]["song_image_url"]=$s3->replaceURI($songimageurl);
        $songinfo[$mediaid]["for_sale"]=$get_featured_media_for_artist[$mediaid]["for_sale"];
        $songinfo[$mediaid]["price"]=$get_featured_media_for_artist[$mediaid]["price"];
        $songinfo[$mediaid]["generaluserid"]=$generaluserid;
    }
    
    $result[] = array('featuredsonginfo'=>$songinfo);
    
    $result[] = array('featuredmediaalbum'=>$featuredMediaAlbum);   
    
    $result[] = array('mediasong_select'=>$get_mediasong_info_select);
    
    $result[] = array('allbuttons'=>$allbuttons);
    
    } //end of if error == no

/* output in necessary format */
if($format == 'json') {
    header('Content-type: application/json');
    echo json_encode($result);
    //echo json_encode(array('artistprofile'=>$result));
    //echo array_to_json($result);
}
else {
    if ($format == 'xml'){
        header('Content-type: text/xml');
        echo '<result>';
        foreach($result as $index => $resultentry) {
            if(is_array($resultentry)) {
                foreach($resultentry as $key => $value) {
                    echo '<',$key,'>';
                    if(is_array($value)) {
                        foreach($value as $tag => $val) {
                            echo '<',$tag,'>',htmlentities($val),'</',$tag,'>';
                        }
                    }
                    echo '</',$key,'>';
                }
            }
        }
        echo '</result>';
    }
}

function array_to_json( $array ){

    if( !is_array( $array ) ){
        return false;
    }

    $associative = count( array_diff( array_keys($array), array_keys( array_keys( $array )) ));
    if( $associative ){

        $construct = array();
        foreach( $array as $key => $value ){

            // We first copy each key/value pair into a staging array,
            // formatting each key and value properly as we go.

            // Format the key:
            if( is_numeric($key) ){
                $key = "key_$key";
            }
            $key = "\"".addslashes($key)."\"";

            // Format the value:
            if( is_array( $value )){
                $value = array_to_json( $value );
            } else if( !is_numeric( $value ) || is_string( $value ) ){
                $value = "\"".addslashes($value)."\"";
            }

            // Add to staging array:
            $construct[] = "$key: $value";
        }

        // Then we collapse the staging array into the JSON form:
        $result = "{ " . implode( ", ", $construct ) . " }";

    } else { // If the array is a vector (not associative):

        $construct = array();
        foreach( $array as $value ){

            // Format the value:
            if( is_array( $value )){
                $value = array_to_json( $value );
            } else if( !is_numeric( $value ) || is_string( $value ) ){
                $value = "'".addslashes($value)."'";
            }

            // Add to staging array:
            $construct[] = $value;
        }

        // Then we collapse the staging array into the JSON form:
        $result = "[ " . implode( ", ", $construct ) . " ]";
    }
	
    return $result;
}
function cleanstring($stringin)
{
   $sanitized = strtolower(preg_replace('/[^a-zA-Z0-9-_\.]/','', $stringin)); 
    return ($sanitized);
}
function setGalleryImage($galleryimage,$gallerygallerytitle,$gallerytitle,$register_cover_pic,$register_No_pic)
{
    $gallery_image_name_cart="";    
    if($gallerygallerytitle !="" || $gallerytitle!="")
    {
        if($galleryimage !="" && $galleryimage !=null )
		{
			$gallery_image_name_cart = $galleryimage;
		}
		else{
			$gallery_image_name_cart = "https://medgalthumb.s3.amazonaws.com/Noimage.png";
		}
	}
	else
	{
		if($register_cover_pic=="")
		{
			$gallery_image_name_cart = "https://reggalthumb.s3.amazonaws.com/".$register_No_pic;
		}
		else
		{
			$gallery_image_name_cart ="https://reggalthumb.s3.amazonaws.com/".$register_cover_pic;
		}
	}
    return($gallery_image_name_cart);
}
function printr ( $object , $name = '' ) {

    print ( '\'' . $name . '\' : ' ) ;

    if ( is_array ( $object ) ) {
        print ( '<pre>' )  ;
        print_r ( $object ) ; 
        print ( '</pre>' ) ;
    } else {
        var_dump ( $object ) ;
    }

}
function make_comparer() {
    // Normalize criteria up front so that the comparer finds everything tidy
    $criteria = func_get_args();
    foreach ($criteria as $index => $criterion) {
        $criteria[$index] = is_array($criterion)
            ? array_pad($criterion, 3, null)
            : array($criterion, SORT_ASC, null);
    }

    return function($first, $second) use (&$criteria) {
        foreach ($criteria as $criterion) {
            // How will we compare this round?
            list($column, $sortOrder, $projection) = $criterion;
            $sortOrder = $sortOrder === SORT_DESC ? -1 : 1;

            // If a projection was defined project the values now
            if ($projection) {
                $lhs = call_user_func($projection, $first[$column]);
                $rhs = call_user_func($projection, $second[$column]);
            }
            else {
                $lhs = $first[$column];
                $rhs = $second[$column];
            }

            // Do the actual comparison; do not return if equal
            if ($lhs < $rhs) {
                return -1 * $sortOrder;
            }
            else if ($lhs > $rhs) {
                return 1 * $sortOrder;
            }
        }

        return 0; // tiebreakers exhausted, so $first == $second
    };
}
function array_sort_by_column(&$arr, $col, $dir = SORT_ASC) {
    $sort_col = array();
    foreach ($arr as $key=> $row) {
        $sort_col[$key] = $row[$col];
    }

    array_multisort($sort_col, $dir, $arr);
}
?>
