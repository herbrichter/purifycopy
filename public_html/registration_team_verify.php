<?php
    ob_start();
	include_once('login_includes.php');
	include_once('classes/Team.php');
	
	$verification_no=$_GET['id'];
	$category_id=$_GET['c'];
	$reg_status="Complete";
	
	$obj=new Team();
	if($obj->checkVerificationNo($verification_no))
	{
		$obj->updateVerification($verification_no);
		
		//The line below is added just temporarily to divert user to demo site. This should be removed when actual site is ready.
		header('location:http://www.purifyentertainment.net/Design/index.php');
		//-----------------------------------------------------------------------------------------------------------------------
		
		switch($category_id)
		{
			case 1:	$msg="Thank you for registering as a Director here at Purify Entertainment. We appreciate your interest and your acceptance to the board will be voted on at the next board meeting. We will be contacting you with more information on the meeting and how our Board works soon.";
					break;
			case 2: $msg="Thank you for registering as a Team Member. We appreciate your help and will be contacting you soon with information on how to get started.";
					break;
			case 3: $msg="Thank you for registering as a Team Member. We appreciate your help and will be contacting you soon with information on how to get started.";
					break;
			default:$msg="Error in registration. Either your registration request is already in progress or the link is bad.";
					$reg_status="Failed";
					break;
		}
	}
	else
	{
		$msg="Error in registration. Either your registration request is already in progress or the link is bad.";
		$reg_status="Failed";
	}
?>

<?php include_once('includes/header.php'); ?>
<title>Purify Entertainment: Six Feature</title>

  <div id="contentContainer">
    <h1>Registration <?php echo $reg_status; ?></h1>
    <p><?php echo $msg; ?></p>
  </div>
  
<?php include_once('includes/footer.php'); ?>