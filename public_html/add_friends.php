<?php
	include("classes/Addfriend.php");
	include("newsmtp/PHPMailer/index.php");
	include_once('sendgrid/SendGrid_loader.php');
	
	$friend_obj = new Addfriend();
	//var_dump($_GET);
	$sendgrid = new SendGrid('','');
	
	if(isset($_GET['friend']) && isset($_GET['log_id']))
	{
		$find_friend = $friend_obj->find_friend($_GET['friend']);
		
		if(isset($find_friend) && $find_friend=='a')
		{
			$art_id = $friend_obj->found_artist($_GET['friend']);
			$gen_id = $friend_obj->found_ageneral($art_id['artist_id']);
		}
		
		if(isset($find_friend) && $find_friend=='c')
		{
			$comm_id = $friend_obj->found_community($_GET['friend']);
			$gen_id = $friend_obj->found_cgeneral($comm_id['community_id']);
		}
		
		
		
		//if(isset($art_id) && $art_id!="")
		//{
			//echo "hii";
			$community = 0;
			$status = 1;							////////////////////////1:-Pending and 0:-Accepted and 2:-Declined
			$insert_friend = $friend_obj->addfriend_gen($_GET['log_id'],$gen_id['general_user_id'],$status);
			
			$find_to = $friend_obj->general_user_sec($gen_id['general_user_id']);
			$find_from = $friend_obj->general_user_sec($_GET['log_id']);
			$headers = "";
			$headers .= "MIME-Version: 1.0\r\n";
			$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
			
			//$to = $find_to['email'];
			/* $mail->AddAddress($find_to['email']);
			//$subject = 'Purify Art | Friend Request';
			$mail->Subject = 'Purify Art | Friend Request';
			$mail_body = "<html><body>".$find_from['fname'].' '.$find_from['lname']." has friend requested you."."\r\n"."\r\n"."To accept or deny the request go to your <a href='purifyart.net/profileedit.php#friends'>Friends and Fans</a> page in your personal profile.</body></html>";
			$headers .= "from: Purify Art < no-reply@purifyart.net>"."\r\n";
			//$sentmail = mail($to,$subject,$mail_body,$headers);
			$mail->Body    = $mail_body; */
			
			$mail_body = "<html><body>".$find_from['fname'].' '.$find_from['lname']." has friend requested you."."\r\n"."\r\n"." To accept, click on the button below. Once you accept the friend request you will be subscribed to ".$find_from['fname'].' '.$find_from['lname']."'s profiles. You may edit your subscription at any time in your subscriptions page.<div style='background-color: #D3D3D3; color: #FFFFFF; font-size: 18px; line-height: 35px; margin: 20px auto; width: 95px;'><a href='purifyart.com/profileedit.php?frd_gen_acc_id=".$_GET['log_id']."#friends'  style='text-decoration:none;color:#000000'>Accept</a></div></body></html>";
			
			$url = "admin/newsletter/"; 
			$myFile = $url . "other_templates.txt";
			$fh = fopen($myFile, 'r');
			$theData = fread($fh, filesize($myFile));
			fclose($fh);
			
			$data_email = explode('|~|', $theData);
			
			$ns_user_n = str_replace('{::message::}', $mail_body, $data_email['1']);
			
			$body_email = $data_email['0'] . $ns_user_n . $data_email['2'];
			
			$mail_grid = new SendGrid\Mail();

			$mail_grid->addTo($find_to['email'])->
				   setFromName('Purify Art')->
					setFrom('no-reply@purifyart.com')->
				   setSubject('Purify Art | Friend Request')->
				   setHtml($body_email);
			//$sendgrid->web->send($mail_grid);
			
			$sendgrid -> smtp -> send($mail_grid); 
		
			/* if($mail->Send()){
				$sentmail = true;
			} */
			//die;
			//echo "Mail Sent.";
			//$find_artist_2 = $friend_obj->general_user_sec($_GET['log_id']);
			//$insert_friend_2 = $friend_obj->addfriend_gen($gen_id['general_user_id'],$_GET['log_id'],$find_artist_2['artist_id'],$community,$status);
			//var_dump($insert_friend);
		/*}
		elseif(isset($comm_id) && $comm_id!="")
		{
			//echo "hii23";
			$artist = 0;
			$status = 1;							////////////////////////1:-Pending and 0:-Accepted and 2:-Declined
			$insert_friend = $friend_obj->addfriend_gen($_GET['log_id'],$gen_id['general_user_id'],$artist,$comm_id['community_id'],$status);
			//$find_community_2 = $friend_obj->general_user_sec($_GET['log_id']);
			//$insert_friend_2 = $friend_obj->addfriend_gen($gen_id['general_user_id'],$_GET['log_id'],$artist,$find_community_2['community_id'],$status);
		}*/
		
		header("Location: /".$_GET['friend']);
	}
	
	if(isset($_GET['confirm_friend_id']) && isset($_GET['confirm_gen']))
	{
		if($_GET['type']=='artist')
		{
			$confirm_friend = $friend_obj->confirm_friend_a($_GET['confirm_friend_id'],$_GET['confirm_gen']);
			
			$find_to = $friend_obj->general_user_sec($_GET['confirm_gen']);
			$find_from = $friend_obj->general_user_sec($_GET['confirm_friend_id']);
			$headers = "";
			$headers .= "MIME-Version: 1.0\r\n";
			$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
			/* //$to = $find_to['email'];
			//$mail->AddAddress($find_to['email']);
			//$subject = 'Friend Request Confirmed';
			//$mail->Subject = 'Friend Request Confirmed';
			//$mail_body = $find_from['fname'].' '.$find_from['lname']." has accepted your friend request."."\r\n"."\r\n"."They will now receive updates you send. To send updates use your <html><body><a href='purifyart.net/profileedit_artist.php#promotions'>promotions page</a> or add a <a href='purifyart.net/profileedit.php'>news post</a></body></html>.";
			$headers .= "from: Purify Art < no-reply@purifyart.net>"."\r\n";
			//$sentmail = mail($to,$subject,$mail_body,$headers);
			//$mail->Body    = $mail_body; */
			
			$mail_body = "<html><body>".$find_from['fname'].' '.$find_from['lname']." has accepted your friend request."."\r\n"."\r\n"."They will now receive updates you send. To send updates use your <a href='purifyart.com/profileedit_artist.php#promotions'>promotions page</a> or add a <a href='purifyart.com/profileedit.php'>news post</a>.</body></html>";
			
			$mail_grid = new SendGrid\Mail();
			
			$url = "admin/newsletter/"; 
			$myFile = $url . "other_templates.txt";
			$fh = fopen($myFile, 'r');
			$theData = fread($fh, filesize($myFile));
			fclose($fh);
			
			$data_email = explode('|~|', $theData);
			
			$ns_user_n = str_replace('{::message::}', $mail_body, $data_email['1']);
			
			$body_email = $data_email['0'] . $ns_user_n . $data_email['2'];

			$mail_grid->addTo($find_to['email'])->
				   setFromName('Purify Art')->
					setFrom('no-reply@purifyart.com')->
				   setSubject('Friend Request Confirmed')->
				   setHtml($body_email);
			//$sendgrid->web->send($mail_grid);
			$sendgrid -> smtp -> send($mail_grid); 
			
			/* if($mail->Send()){
				$sentmail = true;
			} */
		}
		if($_GET['type']=='community')
		{
			$confirm_friend = $friend_obj->confirm_friend_c($_GET['confirm_friend_id'],$_GET['confirm_gen']);
			
			$find_to = $friend_obj->general_user_sec($_GET['confirm_gen']);
			$find_from = $friend_obj->general_user_sec($_GET['confirm_friend_id']);
			$headers = "";
			/* $headers .= "MIME-Version: 1.0\r\n";
			$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
			//$to = $find_to['email'];
			$mail->AddAddress($find_to['email']);
			//$subject = 'Friend Request Confirmed';
			//$mail->Subject = 'Friend Request Confirmed';
			//$mail_body = $find_from['fname'].' '.$find_from['lname']." has accepted your friend request."."\r\n"."\r\n"."They will now receive updates you send. To send updates use your <html><body><a href='purifyart.net/profileedit_artist.php#promotions'>promotions page</a> or add a <a href='purifyart.net/profileedit.php'>news post</a></body></html>";
			$headers .= "from: Purify Art < no-reply@purifyart.net>"."\r\n";
			//$sentmail = mail($to,$subject,$mail_body,$headers);
			//$mail->Body    = $mail_body; */
			
			$mail_body = "<html><body>".$find_from['fname'].' '.$find_from['lname']." has accepted your friend request."."\r\n"."\r\n"."They will now receive updates you send. To send updates use your <a href='purifyart.com/profileedit_artist.php#promotions'>promotions page</a> or add a <a href='purifyart.com/profileedit.php'>news post</a>.</body></html>";
			
			$mail_grid = new SendGrid\Mail();
			
			$url = "admin/newsletter/"; 
			$myFile = $url . "other_templates.txt";
			$fh = fopen($myFile, 'r');
			$theData = fread($fh, filesize($myFile));
			fclose($fh);
			
			$data_email = explode('|~|', $theData);
			
			$ns_user_n = str_replace('{::message::}', $mail_body, $data_email['1']);
			
			$body_email = $data_email['0'] . $ns_user_n . $data_email['2'];

			$mail_grid->addTo($find_to['email'])->
				   setFromName('Purify Art')->
					setFrom('no-reply@purifyart.com')->
				   setSubject('Friend Request Confirmed')->
				   setHtml($body_email);
			//$sendgrid->web->send($mail_grid);
			
			$sendgrid -> smtp -> send($mail_grid); 
			
			/* if($mail->Send()){
				$sentmail = true;
			} */
		}
		
		header("Location: /profileedit.php#friends");
	}
	
	if(isset($_GET['ignore_friend_id']) && isset($_GET['ignore_gen']))
	{
		if($_GET['type']=='artist')
		{
			$confirm_friend = $friend_obj->ignore_friend_a($_GET['ignore_friend_id'],$_GET['ignore_gen']);
		}
		if($_GET['type']=='community')
		{
			$confirm_friend = $friend_obj->ignore_friend_c($_GET['ignore_friend_id'],$_GET['ignore_gen']);
		}
		
		header("Location: /profileedit.php");
	}
	//echo $_SERVER['HTTP_HOST']; 
	
?>
