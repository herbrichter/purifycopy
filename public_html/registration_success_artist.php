<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<div id="outerContainer">
<?php include_once("includes/header.php"); ?>
<script src="js/loginvalidate.js"></script>
<title>Purify Art: About</title>
<body>
<?php
$logerr=isset($_GET['logerr']);
	if (isset($logerr) && $logerr==1)
	{
		$invalid_user="Invalid User Name or Password";
	}
	else
	{
		$invalid_user="";
	}
?>


<div id="toplevelNav"></div>
  <div id="contentContainer" style="padding:60px 0px 60px 0px; text-align:center;">
    	<p>Thank you for registering for an artist profile.
		We will review your request and either approve</p> <p>or deny it according to the artistic quality of the samples you uploaded and links you have provided.</p><p> You will receive notice either way.</p>
		<p>Once you are approved you may build and use your artist profile across the network. Thank You.</p></div>
  

<?php include_once("footer.php");?>
<!-- end of main container -->
</div>
</body>