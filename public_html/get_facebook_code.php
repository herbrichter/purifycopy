<?php
session_start();
include_once('commons/db.php');
include_once('classes/Social_details.php');
require_once 'facebook/facebook.php';
$save_obj = new SocialDetails();

if(isset($_GET['activate_me']) && !empty($_GET['activate_me'])){
	$activate_posts = $save_obj ->activateposts(mysql_real_escape_string($_GET['activate_me']),mysql_real_escape_string($_GET['status']));
	header("Location: profileedit.php#social_connection");
}else{
	if (!class_exists('\user_keys'))
	{
		require_once($_SERVER["DOCUMENT_ROOT"].'/classes/user_keys.php');
	}
	$config = array();
	$sgk = $uk->get_UserKeys("Facebook"); 
	$config['appId'] = $sgk['clientid'];
	$config['secret'] = $sgk['clientsecret'];
	$facebook = new Facebook(array(
		 'appId' => $config['appId'],
		 'secret' => $config['secret'],
		 'cookie' => true,
		));
	
	/*$get_fb_details = $save_obj->get_facebook_appid_seckey();
	$facebook = new Facebook(array(
		 'appId' => $get_fb_details['clientid'],
		 'secret' => $get_fb_details['clientsecret'],
		 'cookie' => true,
		));*/
	$user = $facebook->getUser();
	if ($user) {
		$token = $facebook->getAccessToken();
		$user_profile = $facebook->api('/me');
		if(!empty($user_profile)){
			$save_fb_details = $save_obj -> save_facebook_token($_SESSION['login_id'],$token,$user_profile['name']);
		}
		$pages = $facebook->api('/me/accounts');
		foreach($pages['data'] as $account)
		{
			$save_fb_details = $save_obj -> save_facebook_token($_SESSION['login_id'],$account['access_token'],$account['name'],$account['id']);
		}
	}
	header("Location: http://purifycopy.com/profileedit.php#social_connection");
}

?>
