<?php
include_once('commons/db.php');
session_start();
include_once('classes/AddCommunityEvent.php');
include_once('classes/EditArtist.php');
include_once('classes/Commontabs.php');
include_once('classes/ProfileeditCommunity.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "https://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="https://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<meta https-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Purify Art: Event Profile</title>
<!--<script type="text/javascript" src="includes/jquery.js"></script>-->

<?php
$Addcomevent = new AddCommunityEvent();
$editprofile=new EditArtist();
$newgeneral= new ProfileeditCommunity();

$id = $Addcomevent->selgeneral();

$acc_media=explode(',',$id['accepted_media_id']);

$media_avail = 0;

for($k=0;$k<count($acc_media);$k++)
{
	if($acc_media[$k]!="")
	{
		$media_avail = $media_avail + 1;
	}
}

	$new_id=$id['community_id'];
	
	$newrow2=$Addcomevent->selgeneral_community_profile();

	$newrow3=$Addcomevent->selType();
	
	$newrow4=$Addcomevent->selsubtype();

	$sql5=$Addcomevent->seltypeby_profile_id();
	$selType=$Addcomevent->sel_types_for_event();
	
	//$sql6=$Addcomevent->selsubtypeby_type();

	$sql7=$Addcomevent->selmatatype();
	
	$res=$Addcomevent->selcommunity_event_tagged();
	
	
	$bands=$res['users_tagged'];
	//$artist=$res['artist_tagged'];
	$guest=$res['guest_list'];
	$showband = explode(',',$bands);
	//$showartist = explode(',',$artist);
	
	$get_event = $Addcomevent->getEventUpcoming();
	$get_eventrec = $Addcomevent->getEventRecorded();
	
	$get_aeevent = $Addcomevent->getArtEventUpcoming();
	$get_aeeventrec = $Addcomevent->getArtEventRecorded();
	
	$acc_event=explode(',',$id['taggedcommunityevents']);
	$event_avail = 0;
	for($k=0;$k<count($acc_event);$k++)
	{
		if($acc_event[$k]!="")
		{
			$event_avail = $event_avail + 1;
		}
	}
	
	$acc_aevent=explode(',',$id['taggedartistevents']);
	$event_aavail = 0;
	for($k=0;$k<count($acc_aevent);$k++)
	{
		if($acc_aevent[$k]!="")
		{
			$event_aavail = $event_aavail + 1;
		}
	}

	$acc_project=explode(',',$id['taggedcommunityprojects']);
	$project_avail = 0;
	for($k=0;$k<count($acc_project);$k++)
	{
		if($acc_project[$k]!="")
		{
			$project_avail = $project_avail + 1;
		}
	}
	
	$acc_aproject = explode(',',$id['taggedartistprojects']);
	$project_aavail = 0;
	for($k=0;$k<count($acc_aproject);$k++)
	{
		if($acc_aproject[$k]!="")
		{
			$project_aavail = $project_aavail + 1;
		}
	}

$newrow=$Addcomevent->Get_selected_tagg($_GET['id']);
//var_dump($newrow);
/**********Code For Displaying selected options in list box for tagged************/

$get_select_songs = explode(',',$newrow['tagged_songs']);
//var_dump($get_select_songs);

$get_select_videos = explode(',',$newrow['tagged_videos']);
//var_dump($get_select_songs);

//$get_select_channels = explode(',',$newrow['taggedchannels']);
//var_dump($get_select_songs);

$get_select_galleries = explode(',',$newrow['tagged_galleries']);
//var_dump($get_select_songs);

$get_select_projects= explode(',',$newrow['tagged_projects']);

$get_select_events = explode(',',$newrow['taggedupcomingevents']);

$get_select_events_rec = explode(',',$newrow['taggedrecordedevents']);
	
	
/********Functions For Tagged channel video song *******/

$get_community_channel_id=$Addcomevent->get_community_channel_id();

$get_community_video_id=$Addcomevent->get_community_video_id();
$get_community_song_id=$Addcomevent->get_community_song_id();
$get_community_gallery_id=$Addcomevent->get_community_gallery_id();

$get_community_song_register=$Addcomevent->get_community_song_at_register();
$get_community_video_register=$Addcomevent->get_community_video_at_register();
$get_community_gallery_register=$Addcomevent->get_community_gallery_name_at_register();
$get_all_countries=$Addcomevent->get_all_countries();

if($get_community_song_register!=null)
{
	$res_song = mysql_fetch_assoc($get_community_song_register);
}
if($get_community_video_register!=null)
{
	$res_video = mysql_fetch_assoc($get_community_video_register);
}
if($get_community_gallery_register!=null)
{
	$res_gal = mysql_fetch_assoc($get_community_gallery_register);
}

/********Ends here ******/	

/**********Code For Displaying selected options in list box for tagged************/

/*$get_select_songs = explode(',',$res1['taggedsongs']);
//var_dump($get_select_songs);

$get_select_videos = explode(',',$res1['taggedvideos']);
//var_dump($get_select_songs);

$get_select_channels = explode(',',$res1['taggedchannels']);
//var_dump($get_select_channels);

$get_select_galleries = explode(',',$res1['taggedgalleries']);
//var_dump($get_select_songs);
*/
/**********Code For Displaying selected options in list box for tagged ends here************/
	
if(isset($_GET['id']))
{
	
	$id1=$_GET['id'];
	$getdetail=$Addcomevent->selcommunity_event($id1);
	
	/****************Code For displaying Selected************/
	//var_dump($getdetail);
	if(isset($getdetail['tagged_galleries']))
	{
		$sel_gal=explode(',',$getdetail['tagged_galleries']);
	}
	//echo count($sel_gal);
	if(isset($getdetail['tagged_songs']))
	{
		$sel_song=explode(',',$getdetail['tagged_songs']);
	}
	//echo count($sel_song);
	if(isset($getdetail['tagged_videos']))
	{
		$sel_video=explode(',',$getdetail['tagged_videos']);
	}
	//echo count($sel_video);
	
	if(isset($getdetail['tagged_projects']))
	{
		//var_dump($getdetail['tagged_projects']);
		$sel_project=explode(',',$getdetail['tagged_projects']);
		//var_dump($sel_project);
		//echo count($sel_project);
	}
	if(isset($getdetail['community_view_selected']))
	{
		$sel_all_art = explode(',',$getdetail['community_view_selected']);
	}
	if(isset($getdetail['artist_view_selected']))
	{
		$sel_all_art_i = explode(',',$getdetail['artist_view_selected']);
	}
	/****************Code For displaying Selected Ends Here************/
	
	$get_select_events = explode(',',$getdetail['taggedupcomingevents']);
	$get_select_events_rec = explode(',',$getdetail['taggedrecordedevents']);
	
	
	//$exp_usr=explode(',',$getdetail['users']);
	$exp_usr=explode(',',$getdetail['tagged_user_email']);
	$showguest = explode(',',$getdetail['add_guest_name']);
	
	$sql6=$Addcomevent->selsubtypeby_type($id1);
	$newsql=$Addcomevent->sel_edit_subtype($id1);
	
	//$getsubtype =$Addcomevent->sel_subtype_comm($id);
	
	$newsql1=$Addcomevent->selmeta_type($id1);
	$getmeta=$Addcomevent->selcommunity_event_profile($id1);
	
	$arr=Array();
	while($gmeta=mysql_fetch_assoc($getmeta))	
	{
		//var_dump($gmeta);
		$arr[]=$gmeta;
	}
	$get_all_states=$Addcomevent->get_all_states($getdetail['country_id']);
}

$newtab=new Commontabs();
include("header.php");
?>

<div id="outerContainer">

<!--        <p><a href="profile_community_event.php?id=<?php if(isset($_GET['id'])) { echo $_GET['id']; } ?>">View <?php echo $id['fname']; ?> Profile</a><br />
        <a href="../logout.php">Logout</a></p>
      </div>
    </div>
  </div>
 </div>-->
 
 
<!--<link rel="stylesheet" type="text/css" media="screen" href="includes/purify.css" />
<link  rel="stylesheet" type="text/css" media="screen" href="css/jquery.ui.autocomplete.css"></link>
<script src='https://ajax.googleapis.com/ajax/libs/jquery/1.6.1/jquery.min.js'></script>
<script src="ui/jquery-1.7.2.js"></script>
	<script src="ui/jquery.ui.core.js"></script>
	<script src="ui/jquery.ui.widget.js"></script>
	<script src="ui/jquery.ui.mouse.js"></script>
	<script src="ui/jquery.ui.resizable.js"></script>-->

<!--css file for displaying types
<link rel="stylesheet" type="text/css" media="screen" href="includes/multiple_select.css" />-->
<!--css file for displaying types Ends here-->
<!--<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1/jquery.js"></script>
	<script type="text/javascript" src="ckeditor.js"></script>
	<script type="text/javascript" src="jquery.js"></script>
	<script src="sample.js" type="text/javascript"></script>
	<script src="chk_community_list_event.js" type="text/javascript"></script>
	<link href="sample.css" rel="stylesheet" type="text/css" />
	<script src="includes/organictabs-jquery.js"></script>-->
	<script>
	$(function() 
	{
		$("#communityTab").organicTabs();
	});
	
	function changeForm()
	{
	 document.membership_type.submit();
	 
  	}
</script>
<!--<link rel="stylesheet" href="includes/jquery.ui.all.css">
<script src="includes/jquery.ui.core.js"></script>
<script src="includes/jquery.ui.widget.js"></script>
<script src="includes/jquery.ui.datepicker.js"></script>-->
<script>
	$(function() {
		$( "#datepicker").datepicker({
		dateFormat: 'yy-mm-dd',
			changeMonth: true,
			changeYear: true
	});

		
	});
</script>
<script type="text/javascript">



function validate()
{
var name = document.getElementById("title");
	if(name.value=="" || name.value==null)
	{
		alert("Please enter Title.");
		return false;
	}
	var a = document.getElementById("selected-types");
	if(a.innerHTML =="" || a.innerHTML ==null)
	{
		alert("Please select at least one type and click the add button.");
		return false;
	}
/**********Ck Editor Validation **************/
/*
	var bio=$("#description").val();
	var index;
	var res_bio=bio.split("<p>");

	/*if(bio=="")
	{
		alert("You Can Not Leave Description Blank");
		return false;
	}
	//alert(bio.length-10);
	if(bio.length-10 > 300)
	{
		alert("Please Enter Bio Less Than 300 Characters.");
		return false;
	}
	if(res_bio.length-1 > 4)
	{
		alert("Please Enter only Four lines.");
		return false;
	}*/
	/*
	if(res_bio.length-1 == 3)
	{
		for(index=1;index <= res_bio.length-1 ; index++)
		{
			if(res_bio[index].length-7 > 75)
			{
				alert("Please Enter only 75 characters in one line");
				return false;
			}
		}
	}
	*/
/**********Ck Editor Validation Ends Here**************/
	
	
var profile_url = $("#fieldCont_domain").html();
var profile_field = document.getElementById("subdomain").value;
if(profile_field=='' || profile_field==null)
	{
		alert("Please enter Profile URL.");
		return false;
	}
	if(profile_url == "Please Fill Out Domain Name." || profile_url == "Domain Is Not Available.")
	{
		alert("Please Fill Profile URL Correctly.");
		return false;
	}
	var profile_field_commeve = profile_field.trim();
    if (/\s/g.test(profile_field_commeve))
	{
        alert("Please Check Your Profile URL For Spaces.");
        return false;
    }
	
var profile_image = document.getElementById("profile_image");
	if(profile_image.value=="" || profile_image.value==null)
	{
		alert("Please select your Profile Image.");
		return false;
	}
	
var listing_image = document.getElementById("listing_image");
	if(listing_image.value=="" || listing_image.value==null)
	{
		alert("Please select your Listing Image.");
		return false;
	}
}
</script>

<script>
$(function() {
		$( "#popupContact" ).resizable();
	});
</script>

<script>
var myUploader = null;
var myAudioUploader = null;
var iMaxUploadSize = 10485760; //10MB
	window.onload = function(){ 
		$.post("State.php", { country_id:$("#countrySelect").val() },
			function(data)
			{
					//alert("Data Loaded: " + data);											
					$("#stateSelect").html(data);
			}
		);
	}

	$(document).ready(
					function()
					{
						/*$("#addType").click(
							function () 
							{	
								if ($("#type-select").val()!=0 )
								{
									//alert("type selected");
									if(!$("div").hasClass($("#type-select").val()))
									{
									//alert("no div for type");
										$("#selected-types").append('<div class="'+$("#type-select").val()+' box1" id="'+$("#type-select").val()+'">'+'<input type="checkbox" name="type-checked" value="'+$("#type-select").val()+'" checked onclick="$(this).parent().remove()" />'+$("#type-select option:selected").attr("chkval")+'</div>');
									}
								
								}
								else
								{
										alert("Select PageType");
								}	
																
								//alert("hi");
								if($("#subtype-select").val()!=0)
								{
									//alert("subtype selected");
									if(!$("div").hasClass($("#type-select").val()+'_'+$("#subtype-select").val()))
									{
										//alert("no div for type subtype");
										//if($("div").hasClass($("#type-select").val())
										$("."+$("#type-select").val()).append('<div id="'+$("#subtype-select").val()+'" class="'+$("#type-select").val()+'_'+$("#subtype-select").val() + ' box1A"><input type="checkbox" name="subtype-checked" value="'+$("#type-select").val()+'_'+$("#subtype-select").val() + '" checked  onclick="$(this).parent().remove()"/>'+$("#subtype-select option:selected").attr("chkval")+'<br>'+'</div>');
									}
								}
								
								if($("#metatype-select").val()!=0)
								{
									//alert("metatype selected");
									if(!$("div").hasClass($("#type-select").val()+'_'+$("#subtype-select").val()+'_'+$("#metatype-select").val()))
									{
										//alert("no div for type subtype metatype");
										//if($("div").hasClass($("#type-select").val())
										$("."+$("#type-select").val()+'_'+$("#subtype-select").val()).append('<div class="'+$("#type-select").val()+'_'+$("#subtype-select").val()+'_'+$("#metatype-select").val() + ' box1B"><input type="checkbox" name="metatype_checked[]" value="'+$("#type-select").val()+'_'+$("#subtype-select").val()+'_'+$("#metatype-select").val() +'" checked  onclick="$(this).parent().remove()"/>'+$("#metatype-select option:selected").attr("chkval")+'</div>');
									}
								}
							}
						);*/
						
	//Meta type displaying starts here
	
       //meta type displaying ends here//
       	   
	   
	   $("#addType").click(function () 
       {
		 //alert("hiType");
         //alert($("#type-select option:selected").length);
         //alert($("#subtype-select option:selected").length);
         //alert($("#metatype-select option:selected").length);
         //alert($("#type-select option:selected").attr("key"));
         if ($("#type-select").val()!=0 )
         {
          //alert("type selected");
			  if(!$("div").hasClass($("#type-select").val()))
			  {
				  //alert("no div for type");
				   $("#selected-types").append('<div class="'+$("#type-select").val()+' box1" id="'+$("#type-select").val()+'">'+'<input type="checkbox" name="type_array['+$("#type-select").val() +']" value="'+$("#type-select").val()+'" checked onclick="$(this).parent().remove()" />'+$("#type-select option:selected").attr("chkval")+'<input type="hidden" name="typeVals[]" id="typeVals[]" value="'+$("#type-select").val()+'_0_0"/></div>');
				  /* $("#selected-types-hide").append('<div style="display:none;" class="'+$("#type-select").val()+' box1 type-select-hide" id="'+$("#type-select").val()+'">'+'<input type="checkbox" name="type_array['+$("#type-select").val() +']" value="'+$("#type-select").val()+'" checked onclick="$(this).parent().remove()" />'+$("#type-select option:selected").attr("chkval")+'<input type="hidden" name="typeVals[]" value="'+$("#type-select").val()+'_0_0"/></div>');*/
				   
			  }
         
         }
         else
         {
			   alert("Select PageType");
         } 
                 
         //alert("hi");
         if($("#subtype-select").val()!=0)
         {
         // alert("subtype selected");
			  if(!$("div").hasClass($("#type-select").val()+'_'+$("#subtype-select").val()))
			  {
			   
			   $("."+$("#type-select").val()).append('<div id="'+$("#subtype-select").val()+'" class="'+$("#type-select").val()+'_'+$("#subtype-select").val() + ' box1A"><input type="checkbox" name="type_array['+$("#type-select").val() +']['+$("#subtype-select").val()+']" value="'+$("#type-select").val()+'_'+$("#subtype-select").val() + '" checked  onclick="$(this).parent().remove()"/>'+$("#subtype-select option:selected").attr("chkval")+'<br>'+'<input type="hidden" name="subTypeVals[]" value="'+$("#type-select").val()+'_'+$("#subtype-select").val()+'_0"/></div>');
			  /* $("#type-select-hide").append('<div style="display:none;" id="'+$("#subtype-select").val()+'" class="'+$("#type-select").val()+'_'+$("#subtype-select").val() + ' box1A subtype-select-hide"><input type="checkbox" name="type_array['+$("#type-select").val() +']['+$("#subtype-select").val()+']" value="'+$("#type-select").val()+'_'+$("#subtype-select").val() + '" checked  onclick="$(this).parent().remove()"/>'+$("#subtype-select option:selected").attr("chkval")+'<br>'+'<input type="hidden" name="subTypeVals[]" value="'+$("#type-select").val()+'_'+$("#subtype-select").val()+'_0"/></div>');*/
			  
			  }
         
         }
         
         if($("#metatype-select").val()!=0)
         {
         //alert("metatype selected");
			  if(!$("div").hasClass($("#type-select").val()+'_'+$("#subtype-select").val()+'_'+$("#metatype-select").val()))
			  {
				if($("#metatype-select").val()== null)
			   {
				return false;
			   }
			   $("."+$("#type-select").val()+'_'+$("#subtype-select").val()).append('<div class="'+$("#type-select").val()+'_'+$("#subtype-select").val()+'_'+$("#metatype-select").val() + ' box1B"><input type="checkbox" name="metatype_checked[]" value="'+$("#type-select").val()+'_'+$("#subtype-select").val()+'_'+$("#metatype-select").val() +'" checked  onclick="$(this).parent().remove()"/>'+$("#metatype-select option:selected").attr("chkval")+'<input type="hidden" name="metaTypeVals[]" id="metaTypeVals[]" value="'+$("#type-select").val()+'_'+$("#subtype-select").val()+'_'+$("#metatype-select").val() +'"/></div>');
			   //alert($("#type-select").val()+'_'+$("#subtype-select").val()+'_'+$("#metatype-select").val());
			  /* $("#subtype-select-hide").append('<div style="display:none;" class="'+$("#type-select").val()+'_'+$("#subtype-select").val()+'_'+$("#metatype-select").val() + ' box1B"><input type="checkbox" name="metatype_checked[]" value="'+$("#type-select").val()+'_'+$("#subtype-select").val()+'_'+$("#metatype-select").val() +'" checked  onclick="$(this).parent().remove()"/>'+$("#metatype-select option:selected").attr("chkval")+'<input type="hidden" name="metaTypeVals[]" value="'+$("#type-select").val()+'_'+$("#subtype-select").val()+'_'+$("#metatype-select").val() +'"/></div>');*/
			  
			  }
         
         }
                 
        });
		  
	   					
						$("#type-select").change(
							function ()
							{
								//alert($("#type-select").val());
								$.post("registration_block_subtype.php", { type_id:$("#type-select").val() },
									function(data)
									{
									//alert("Data Loaded: " + data);
										//$("#metatype-select").html("<option value='0' selected='selected'>Select Metatype</option>");
										//$("#subtype-select").html(data);
										if(data == " " || data == null || data == "")
										{
											document.getElementById("subtype-select").disabled=true;
										}else{
											document.getElementById("subtype-select").disabled=false;
											$("#subtype-select").html(data);
										}
									}
								);
							}
						);
										
						$("#subtype-select").change(
							function ()
							{
								//alert($("#subtype-select").val());
								$.post("registration_block_metatype.php", { subtype_id:$("#subtype-select").val() },
									function(data)
									{
										//alert("Data Loaded: " + data);
										//$("#metatype-select").html(data);
										if(data == " " || data == null || data == "")
										{
											document.getElementById("metatype-select").disabled=true;
										}else{
											document.getElementById("metatype-select").disabled=false;
											$("#metatype-select").html(data);
										}
									}
								);
							}	
						);
							//Select State o change in values of Country
							
						$("#countrySelect").change(
							function ()
							{
								//alert($("#type-select").val());
								$.post("State.php", { country_id:$("#countrySelect").val() },
									function(data)
									{
									//alert("Data Loaded: " + data);											
										$("#stateSelect").html(data);
									}
								);
							}
						);
							
						$('#selectuploadtype').change(
							function() 
							{
								$("#imgUploadDiv").fadeOut();
								$("#audioUploadDiv").fadeOut();
								$("#videoUploadDiv").fadeOut();
								$("#audiofileform").fadeOut();
								$("#audiolinkform").fadeOut();
								  var val = $('#selectuploadtype').val();
								switch(val)
								{
									
									case 'Image':
											$("#imgUploadDiv").fadeIn();
											$("#uploadedImages").val('');
											clearFields();
										if(!myUploader){
											myUploader = {
													uploadify : function(){
														$('#file_upload').uploadify({
															'uploader'  : './uploadify/uploadify.swf',
															'script'    : './uploadify/uploadFiles.php',
															'cancelImg' : './uploadify/cancel.png',
															'folder'    : './uploads/',
															'auto'      : true,
															'multi'		: true,
															'removeCompleted' : true,
															'wmode'		: 'transparent',
															'buttonText': 'Upload Gallery',
															'fileExt'     : '*.jpg;*.gif;*.png',
															'fileDesc'    : 'Image Files',
															'simUploadLimit' : 4,
															'sizeLimit'	: iMaxUploadSize, //10 MB size
															'onComplete': function(event, ID, fileObj, response, data) {
																// On File Upload Completion			
																//location = 'uploadtos3.php?uploads=complete';
																$("#uploadedImages").val($("#uploadedImages").val() + response + "|");
															},
															'onSelect' : function (event, ID, fileObj){
															},
															'onSelectOnce' : function(event, data)
															{
																// This function fires after onSelect
																// Checks total size of queue and warns user if too big
																iTotFileSize = data.allBytesTotal;
																//alert("iTotFileSize = " + iTotFileSize);
																if(iTotFileSize >= iMaxUploadSize){
																	var iOverSize = (iTotFileSize - iMaxUploadSize) / 1024 / 1024; // Size, in MB
																	//alert("Total file size in queue exceeds maximum 10MB allowed.  Please remove " + Math.ceil(iOverSize) + " MB worth.");
																	$("#divGalleryFileSize").show().html("Maximum 10MB allowed. Total queue size approximately: <font color='red'>" + Math.ceil(iTotFileSize/1024/1024) + " (MB)</font>");
																	$('#file_upload').uploadifyClearQueue();
																}
																else
																{
																   $("#divGalleryFileSize").hide() 
																}
															},
															'onOpen'	: function() {
																//hide overly
															}
															/*,
															'onError'     : function (event,ID,fileObj,errorObj) {
															  alert(errorObj.type + ' Error: ' + errorObj.info);
															},
															'onProgress'  : function(event,ID,fileObj,data) {
															  var bytes = Math.round(data.bytesLoaded / 1024);
															  $('#containerResult').html(' - ' + bytes + 'KB Uploaded');
															  return false;
															}*/

															});
														}
													};
													myUploader.uploadify();
												}
											break;
									case 'Audio':
											clearFields();
											$("#audioUploadDiv").fadeIn();
											break;
									case 'Video':
											clearFields();
											$("#videoUploadDiv").fadeIn();
											break;
									default :
											alert("Default");
											break;
											
								}
							}
						);

						
						$("input[name^=audiouploadoption]").click(
							function()
							{
								$("#audiofileform").fadeOut();
								$("#audiolinkform").fadeOut();
								$("#uploadedAudio").val('');
								if($(this).val()=="file"){
									$("#audiofileform").fadeIn();
									if(!myAudioUploader){
									myAudioUploader = {
										uploadify : function(){
											$('#file_upload_audio').uploadify({
												'uploader'  : './uploadify/uploadify.swf',
												'script'    : './uploadify/uploadFilesAudio.php',
												'cancelImg' : './uploadify/cancel.png',
												'folder'    : './uploads/',
												'auto'      : true,
												'multi'		: false,
												'removeCompleted' : true,
												'wmode'		: 'transparent',
												'buttonText': 'Upload Media',
												'fileExt'     : '*.mp3',
												'fileDesc'    : 'Audio Files',
												'simUploadLimit' : 4,
												'sizeLimit'	: iMaxUploadSize, //10 MB size
												'onComplete': function(event, ID, fileObj, response, data) {
													// On File Upload Completion			
													//location = 'uploadtos3.php?uploads=complete';
													alert(response);
													$("#uploadedAudio").val($("#uploadedAudio").val() + response + "|");
												},
												'onSelectOnce' : function(event, data)
												{
													// This function fires after onSelect
													// Checks total size of queue and warns user if too big
													iTotFileSize = data.allBytesTotal;
													//alert("iTotFileSize = " + iTotFileSize);
													if(iTotFileSize >= iMaxUploadSize){
														var iOverSize = (iTotFileSize - iMaxUploadSize) / 1024 / 1024; // Size, in MB
														//alert("Total file size in queue exceeds maximum 10MB allowed.  Please remove " + Math.ceil(iOverSize) + " MB worth.");
														$("#divAudioFileSize").show().html("Maximum 10MB allowed. Total queue size approximately: <font color='red'>" + Math.ceil(iTotFileSize/1024/1024) + " (MB)</font>");
														$('#file_upload_audio').uploadifyClearQueue();
													}
													else
													{
													   $("#divGalleryFileSize").hide() 
													}
												},
												'onOpen'	: function() {
													//hide overly
												}
												/*,
												'onError'     : function (event,ID,fileObj,errorObj) {
												  alert(errorObj.type + ' Error: ' + errorObj.info);
												},
												'onProgress'  : function(event,ID,fileObj,data) {
												  var bytes = Math.round(data.bytesLoaded / 1024);
												  $('#containerResult').html(' - ' + bytes + 'KB Uploaded');
												  return false;
												}*/

												});
											}
										};
										myAudioUploader.uploadify();
									}
								}else if($(this).val()=="link"){
									$("#audiolinkform").fadeIn();							
								}
							}
						);

						$("#chk_avail_link").click(
							function()
							{
								$.post("registration_user_availablity.php", { name:$("#username").val() },
								function(data)
								{
									$("#user_name_msg").html(data);
									$("#register").focus();	
									$("#username").focus();																	
								}
								);
							}
						);	
						$("#chk_avail_link1").click(
							function()
							{
								$.post("registration_artist_email.php", { email:$("#email").val() },
								function(data)
								{
									$("#user_email_msg").html(data);
									$("#register").focus();	
									$("#email").focus();																	
								}
								);
							}
						);			
		
						$("#register_btn").click(
							function()
							{
								var v = validate();
								if(v){
									$('#selected-types').clone().appendTo('#selected-types-hide');
									document.registration_form.register.value = "1";
									document.registration_form.submit();
								}
							}
						);

						
						$("#audioLinkUrl").blur(
							function()
							{
								clearFields();
								$("#uploadedAudioLink").val($("#audioLinkUrl").val());
								$("#uploadedAudioLinkTitle").val($("#audioLinkName").val());
							}
						);

						$("#videoLinkUrl").blur(
							function()
							{
								clearFields();
								$("#uploadedVideoLink").val($("#videoLinkUrl").val());
								$("#uploadedVideoLinkTitle").val($("#videoLinkName").val());
							}
						);

						$("#audioLinkName").blur(
							function()
							{
								clearFields();
								$("#uploadedAudioLink").val($("#audioLinkUrl").val());
								$("#uploadedAudioLinkTitle").val($("#audioLinkName").val());
							}
						);

						$("#videoLinkName").blur(
							function()
							{
								clearFields();
								$("#uploadedVideoLink").val($("#videoLinkUrl").val());
								$("#uploadedVideoLinkTitle").val($("#videoLinkName").val());
							}
						);

						$("#audioFileName").blur(
							function()
							{
								var v = $("#uploadedAudio").val();
								clearFields();
								$("#uploadedAudio").val(v);
								$("#audioFileNameTitle").val($("#audioFileName").val());
							}
						);
						$("#galleryFileName").blur(
							function()
							{
								var v = $("#uploadedImages").val();
								clearFields();
								$("#uploadedImages").val(v);
								$("#galleryFileNameTitle").val($("#galleryFileName").val());
							}
						);

						/*$("#mediatype").change(
							function ()
							{
								alert($("#mediatype option:checked").val);
								switch($("#mediatype option:checked").val)
								{
									
									case 'Image':
											alert("Image");
											break;
									case 'Audio':
											alert("Audio");
											break;
									case 'Video':
											alert("Audio");
											break;
									default :
											alert("Default");
											break;
											
								}
								
							}	
						);*/	

					}
				);

function clearFields(){
	$("#uploadedImages").val('');
	$("#uploadedAudio").val('');
	$("#uploadedAudioLink").val('');
	$("#uploadedVideoLink").val('');
	$("#uploadedAudioLinkTitle").val('');
	$("#uploadedVideoLinkTitle").val('');
	$("#audioFileNameTitle").val('');
	$("#galleryFileNameTitle").val('');
}

$("document").ready(function(){
$("#selectCountry").change(function ()
{
	$.post("State.php", { country_id:$("#selectCountry").val() },
		function(data)
		{
			$("#selectState").html(data);
		}
	);
});
});
/*$("document").ready(function(){
$("#selectCountry").val(239);
	$.post("State.php", { country_id:$("#selectCountry").val() },
		function(data)
		{
			$("#selectState").html(data);
		}
	);
});*/
function check_changed(s)
{
	if(s=="")
	{
		alert("To View The Profile Display Page You Have To Enter Profile URL.");
		return false;
	}
}
</script>	
<script type="text/javascript">
$("document").ready(function(){

	 $("#featured_media").change(function() 
    {
	<?php
			if(isset($_GET['id']))
			{
		?>
				var text_data1 = <?php echo $_GET['id'] ?>;
		<?php
			}
			else
			{
		?>
				var text_data1 = "";
		<?php
			}
		?>
	//alert("sss");
	var selected = $("#featured_media").val();
	var dataString = 'reg='+ selected + '&id=' + text_data1;
	
	$("#loader").show();
	$("#fieldCont_media").hide();
	 $.ajax({	 
		
			type: 'POST',
            url : 'CommunityEventAjaxActions.php',
            data: dataString,
            success: function(data) 
			{
                $("#fieldCont_media").html(data);
				$("#loader").hide();
				$("#fieldCont_media").show();
			}
		
		
	});
}); 
}); 

function selected_feature(sel)
{
	<?php
			if(isset($_GET['id']))
			{
		?>
				var text_data1 = <?php echo $_GET['id'] ?>;
		<?php
			}
			else
			{
		?>
				var text_data1 = "";
		<?php
			}
		?>
	var selected = sel;
	var dataString = 'reg='+ selected + '&id=' + text_data1;
	$("#loader").show();
	$("#fieldCont_media").hide();
	 $.ajax({	 
		
			type: 'POST',
            url : 'CommunityEventAjaxActions.php',
            data: dataString,
            success: function(data) 
			{
                $("#fieldCont_media").html(data);
				$("#loader").hide();
				$("#fieldCont_media").show();
			}
		
		
	});

}
</script>


	<script type="text/javascript">
	//<![CDATA[
/*
$(function()
{
	var config = {
		toolbar:
		[
			['Bold', 'Italic', '-', 'NumberedList', 'BulletedList', '-', 'Link', 'Unlink'],
			['UIColor']
		]
	};

	// Initialize the editor.
	// Callback function can be passed and executed after full instance creation.
	$('.jquery_ckeditor').ckeditor(config);
	config.forcePasteAsPlainText = true;
});

	//]]>*/
	</script>	
<!--Ends here-->

<script type="text/javascript">

$("document").ready(function(){
	$("#subdomain").blur(function(){
		var text_value = document.getElementById("subdomain").value;
		//alert(text_value);
		
		<?php
			if(isset($_GET['id']))
			{
		?>
				var text_data1 = <?php echo $_GET['id'] ?>;
		<?php
			}
			else
			{
		?>
				var text_data1 = "";
		<?php
			}
		?>
		
		var text_data = 'reg='+ text_value + '&id=' + text_data1;
		
	 $.ajax({	 
		
			type: 'POST',
            url : 'CommunityEventDomain.php',
            data: text_data,
            success: function(data) 
			{
                $("#fieldCont_domain").html(data);
			}
		
		
	});

	});
	
});

function confirm_saving()
{
	/*var r=confirm("Do you want to save your changes.");
	if (r==true)
	{*/
		submit_form();
	//}
}
function submit_form()
{
	var a=document.getElementById("clickbutton");
	a.click();
	//document.forms["registration_form"].submit();
}


$("document").ready(function(){
/*
bio1 = $('#description').val();
if(bio1!="")
{
	 len = bio1.length - 9;
	 $("#count_for_bio").text(len + "");
}
var bio1,len,len1=0,len2=0,len3=0,len4=0,oldlen0=0,oldlen=0,oldlen1=0,oldlen2=0,oldlen3=0;
	var editor = $('#description').ckeditorGet();
	editor.on( 'key', function(ev){
		 bio1 = $('#description').val();

		len = bio1.length - 9;
		$("#count_for_bio").text(len + "");
		
		var res_bio1=bio1.split("<p>");
		var editor1 = $('#description').ckeditorGet();
		editor1.document.on( 'keydown', function (evt)
		{
			var key_code = evt.data.getKey();
			
			if(key_code == 13)
			{
				res_bio1=bio1.split("<p>");
				if(res_bio1[1] != null)
				{
					len1 = (res_bio1[1].length - 7);
					oldlen0 = (75 - len1);
				}
				if(res_bio1[2] != null)
				{
					len2 = (res_bio1[2].length - 7);
					oldlen = (75 - len2);
				}
				if(res_bio1[3] != null)
				{
					len3 = (res_bio1[3].length - 7);
					oldlen2 = (75 - len3);
				}
				if(res_bio1[4] != null)
				{
					len4 = (res_bio1[4].length - 6);
					oldlen3 = (75 - len4);
				}
				oldlen1 = (len2 + oldlen + oldlen0 + len1 + oldlen2 + len3 + oldlen3 + len4);
				$("#count_for_bio").text(oldlen1 + "");
			}
		});
		
		if(res_bio1.length > 2)
		{
			oldlen = (75 - (res_bio1[1].length - 7));
			oldlen1 = (res_bio1[1].length - 7) + oldlen;
			$("#count_for_bio").text((oldlen1 + (res_bio1[2].length - 6)) + "");
		}
		if(res_bio1.length > 3)
		{
			oldlen0 = (75 - (res_bio1[1].length - 7));
			oldlen = (75 - (res_bio1[2].length - 7));
			oldlen1 = (res_bio1[2].length - 7) + oldlen + oldlen0 + (res_bio1[1].length - 7);
			$("#count_for_bio").text((oldlen1 + (res_bio1[3].length - 6)) + "");
		}
		if(res_bio1.length > 4)
		{
			oldlen0 = (75 - (res_bio1[1].length - 7));
			oldlen = (75 - (res_bio1[2].length - 7));
			oldlen2 = (75 - (res_bio1[3].length - 7));
			oldlen1 = (res_bio1[2].length - 7) + oldlen + oldlen0 + (res_bio1[1].length - 7) + oldlen2 + (res_bio1[3].length - 7);
			$("#count_for_bio").text((oldlen1 + (res_bio1[4].length - 6)) + "");
		}
		if(res_bio1.length > 5)
		{
			$("#count_for_bio").text(300 + "");
		}
		
		
	});
	*/
});
</script>
<?php
if(isset($_GET['id']))
{
?>
<script type="text/javascript">
$("document").ready(function(){
<?php 
for($i=0;$i<count($arr);$i++)
{
?>
if(!$("div").hasClass(<?php echo $arr[$i]['type_id'];?>))
{
	<?php $qu = mysql_query("select name from type where type_id='".$arr[$i]['type_id']."'");
	$name=mysql_fetch_array($qu);
	//$name=$newgeneral->seltype_name($arr);
	if($name[0]!="")
	{
	?>
		$("#selected-types").append('<div class="'
		+<?php echo $arr[$i]['type_id'];?>+' box1" id="'
		+<?php echo $arr[$i]['type_id'];?>+'">'
		+'<input type="checkbox" name="type_array['
		+<?php echo $arr[$i]['type_id'];?>+']" value="'
		+<?php echo $arr[$i]['type_id'];?>
		+'" checked onclick="$(this).parent().remove()" />'
		+"<?php echo $name[0];
		//echo $arr[$i]['type_id'];
		?>"
		+'<input type="hidden" name="typeVals[]" value="'
		+<?php echo $arr[$i]['type_id'];?>+'_0_0"/></div>');
	<?php
	}
	?>
}

if(!$("div").hasClass(<?php echo $arr[$i]['type_id'];?>+'_'+<?php echo $arr[$i]['subtype_id'];?>))
{
	<?php $qu = mysql_query("select name from subtype where subtype_id='".$arr[$i]['subtype_id']."'");
	$name=mysql_fetch_array($qu);
	if($name[0]!="")
	{
	?>
		 $("."+<?php echo $arr[$i]['type_id'];?>).append('<div id="'
		 +<?php echo $arr[$i]['subtype_id'];?>+'" class="'
		 +<?php echo $arr[$i]['type_id'];?>+'_'+<?php echo $arr[$i]['subtype_id'];?>
		 + ' box1A"><input type="checkbox" name="type_array['+<?php echo $arr[$i]['type_id'];?>
		 +']['+<?php echo $arr[$i]['subtype_id'];?>
		 +']" value="'+$("#showtype-select").val()
		 +'_'+<?php echo $arr[$i]['subtype_id'];?>
		 + '" checked  onclick="$(this).parent().remove()"/>'
		 +"<?php echo $name[0];
			//echo $arr[$i]['subtype_id'];
			?>"
		 +'<br>'+'<input type="hidden" name="subTypeVals[]" value="'
		 +<?php echo $arr[$i]['type_id'];?>+'_'+<?php echo $arr[$i]['subtype_id'];?>+'_0"/></div>');
	<?php
	}
	?>
}

if(!$("div").hasClass(<?php echo $arr[$i]['type_id'];?>+'_'+<?php echo $arr[$i]['subtype_id'];?>+'_'+<?php echo $arr[$i]['metatype_id'];?>))
{
<?php $qu = mysql_query("select name from meta_type where meta_id='".$arr[$i]['metatype_id']."'");
	$name=mysql_fetch_array($qu);
	if($name[0]!=null)
	{
	?>
		$("."+<?php echo $arr[$i]['type_id'];?>+'_'
		+<?php echo $arr[$i]['subtype_id'];?>).append('<div class="'
		+<?php echo $arr[$i]['type_id'];?>+'_'+<?php echo $arr[$i]['subtype_id'];?>
		+'_'+<?php echo $arr[$i]['metatype_id'];?>
		+ ' box1B"><input type="checkbox" name="metatype_checked[]" value="'
		+<?php echo $arr[$i]['type_id'];?>+'_'+<?php echo $arr[$i]['subtype_id'];?>
		+'_'+<?php echo $arr[$i]['metatype_id'];?>
		+'" checked  onclick="$(this).parent().remove()"/>'
		+"<?php echo $name[0];
			//echo $arr[$i]['metatype_id'];
			?>"
		+'<input type="hidden" name="metaTypeVals[]" value="'
		+<?php echo $arr[$i]['type_id'];?>+'_'+<?php echo $arr[$i]['subtype_id'];?>
		+'_'+<?php echo $arr[$i]['metatype_id'];?>+'"/></div>');
	<?php
	}
	?>
}
<?php
}
?>
});
</script>
<?php
}
?>
  <div id="toplevelNav"></div>
<!-- Files adde for fancy box2-->
<script type="text/javascript" src="new_fancy_box/jquery.fancybox.js?v=2.1.4"></script>
<link rel="stylesheet" type="text/css" href="new_fancy_box/jquery.fancybox.css?v=2.1.4" media="screen" />
<script type="text/javascript" src="new_fancy_box/helpers/jquery.fancybox-media.js?v=1.0.5"></script>
<!-- Files adde for fancy box2 ends here-->
      <div id="profileTabs">
          <ul>
                <?php
		   $new_artist=$newtab->artist_status();
		   $new_community=$newtab->community_status();
		   $new_team=$newtab->team_status();
		   
		  if(($newres1['artist_id']==0 && $new_artist['status']==1) && $newres1['member_id']==0 && ($newres1['community_id']==0 && $new_community['status']==1) && ($newres1['team_id']==0 && $new_team['status']==1))
		  {		  
		  ?>
            <li><a href="profileedit.php">HOME</a></li>		
            <!--<li><a href="profileedit_support.php">SUPPORT</a></li>-->
		   <?php
		   }
		   elseif(($newres1['artist_id']!=0 && $new_artist['status']==0 && $new_artist['active']==0) && ($newres1['community_id']!=0 && $new_community['status']==0 && $new_community['active']==0) && ($newres1['team_id']!=0 && $new_team['status']==0))
		   {
		   ?>
		   <li><a href="profileedit.php">HOME</a></li>
		   <li><a href="profileedit_media.php">MEDIA</a></li>
		   <li><a href="profileedit_artist.php">ARTIST</a></li>
		   <!--<li><a href="profileedit_community.php">COMPANY</a></li>-->
		   <li class="active">COMPANY</li>
		   <li><a href="profileedit_team.php">TEAM</a></li>
           <!--<li><a href="profileedit_support.php">SUPPORT</a></li>-->
		   <?php
		   }
		   elseif(($newres1['artist_id']!=0 && $new_artist['status']==0 && $new_artist['active']==0) && ($newres1['community_id']!=0 && $new_community['status']==0 && $new_community['active']==0))
		   {
		   ?>
		   <li><a href="profileedit.php">HOME</a></li>
<li><a href="profileedit_media.php">MEDIA</a></li>		   
		   <li><a href="profileedit_artist.php">ARTIST</a></li>
		   <!--<li><a href="profileedit_community.php">COMPANY</a></li>-->
		   <li class="active">COMPANY</li>
           <!--<li><a href="profileedit_support.php">SUPPORT</a></li>-->
		   <?php
		   }
		   elseif(($newres1['artist_id']!=0 && $new_artist['status']==0 && $new_artist['active']==0) && ($newres1['team_id']!=0 && $new_team['status']==0))
		   {
		   ?>
			<li><a href="profileedit.php">HOME</a></li>	
			<li><a href="profileedit_media.php">MEDIA</a></li>			
		    <li><a href="profileedit_artist.php">ARTIST</a></li>
			<li><a href="profileedit_team.php">TEAM</a></li>
            <!--<li><a href="profileedit_support.php">SUPPORT</a></li>-->
		   <?php
		   }
		   elseif(($newres1['community_id']!=0 && $new_community['status']==0 && $new_community['active']==0) && ($newres1['team_id']!=0 && $new_team['status']==0))
		   {
		   ?>
		   <li><a href="profileedit.php">HOME</a></li>
		   <li><a href="profileedit_media.php">MEDIA</a></li>
		   <!--<li><a href="profileedit_community.php">COMPANY</a></li>-->
		   <li class="active">COMPANY</li>
		   <li><a href="profileedit_team.php">TEAM</a></li>
           <!--<li><a href="profileedit_support.php">SUPPORT</a></li>-->
		   <?php
		   }
		    elseif($newres1['artist_id']!=0 && $new_artist['status']==0 && $new_artist['active']==0)
		   {?>
		    <li><a href="profileedit.php">HOME</a></li>	
<li><a href="profileedit_media.php">MEDIA</a></li>			
			<li><a href="profileedit_artist.php">ARTIST</a></li>
            <!--<li><a href="profileedit_support.php">SUPPORT</a></li>-->
		   <?php
		   }
		   elseif($newres1['member_id']!=0)
		   {
		   ?>
		   <?php
		   }
		    elseif($newres1['community_id']!=0 && $new_community['status']==0 && $new_community['active']==0)
		   {
		   ?>
		    <li><a href="profileedit.php">HOME</a></li>	
<li><a href="profileedit_media.php">MEDIA</a></li>			
			<!--<li><a href="profileedit_community.php">COMPANY</a></li>-->
			<li class="active">COMPANY</li>
           <!--<li><a href="profileedit_support.php">SUPPORT</a></li>-->
		   <?php
		   }
		elseif($newres1['team_id']!=0 && $new_team['status']==0)
		   {
		   ?>
		    <li><a href="profileedit.php">HOME</a></li>	
<li><a href="profileedit_media.php">MEDIA</a></li>			
			<li><a href="profileedit_team.php">TEAM</a></li>
           <!--<li><a href="profileedit_support.php">SUPPORT</a></li>-->
`		   <?php
		   }
		   else
		   {
		   ?>
		   <li class="active">HOME</li>		
            <!--<li><a href="profileedit_support.php">SUPPORT</a></li>-->
		   <?php
		   }
			?>
          </ul>
      </div>
          <div id="communityTab">
            <div id="subNavigation" class="tabs">
              <ul class="nav">
			<?php 
			
				//echo $_SERVER['REQUEST_URI'];
				$link=$_SERVER['REQUEST_URI'];
				$newlink=explode('/',$link);
				//echo $newlink[1];
				if($newlink[1]=="purifyart") 
				{
			?>
                <li><a href="/purifyart/profileedit_community.php#profile" class="current">Profile</a></li>
                <li><a href="/purifyart/profileedit_community.php#event" class="current">Events</a></li>
                <!--<li><a href="/purifyart/profileedit_community.php#services">Add Service</a></li>
                <li><a href="/purifyart/profileedit_community.php#memberships">Membership</a></li>-->
                <li><a href="/purifyart/profileedit_community.php#projects">Projects</a></li>
                <li><a href="/purifyart/profileedit_community.php#promotions">Promotions</a></li>
			<?php
				}
				else
				{
			?>
				<li><a href="profileedit_community.php" class="current">Profile</a></li>
                <li><a href="profileedit_community.php#event" class="current">Events</a></li>
                <!--<li><a href="profileedit_community.php#services">Add Service</a></li>
                <li><a href="profileedit_community.php#memberships">Membership</a></li>-->
                <li><a href="profileedit_community.php#projects">Projects</a></li>
                <li><a href="profileedit_community.php#promotions">Promotions</a></li>
			<?php
				}
			?>
              </ul>
            </div>
            <div class="list-wrap">
            	<div class="subTabs">

				<div class="subTabs" id="event">
				<?php
					if(isset($_GET['id']) && $_GET['id']!=""){
						echo "<h1>Edit  ".$getdetail['title']."</h1>";
					}else{
						echo "<h1>Add New Event</h1>";
					}
				?>
                	
					<div id="mediaContent">
                    	<div class="topLinks" style="border-bottom:1px dotted #999;">
                            <div class="links" style="float:left;">
								<ul>
									<li><a href="/<?php echo $getdetail['profile_url']; ?>" onclick ="return check_changed('<?php echo $getdetail['profile_url']; ?>');"><input type="button" value="View Profile" style="border-radius:5px; -webkit-border-radius:5px; -moz-border-radius:5px; -ms-border-radius:5px; -o-border-radius:5px; color: rgb(255, 255, 255); font-family: 'futura-pt',sans-serif; font-size: 0.825em; font-weight: bold; padding: 6px 10px; float: left; margin-right: 10px; border: 0px none; background-color: rgb(0, 0, 0);" /></a></li>
									<?php
									if(isset($_GET['id']) && $_GET['id']!="")
									{
									?>
										<li><a href="javascript:void(0);" onclick="confirm_saving()"><input type="button" value=" Save " style="border-radius:5px; -webkit-border-radius:5px; -moz-border-radius:5px; -ms-border-radius:5px; -o-border-radius:5px; color: rgb(255, 255, 255); font-family: 'futura-pt',sans-serif; font-size: 0.825em; font-weight: bold; padding: 6px 10px; float: left; margin-right: 10px; border: 0px none; background-color: rgb(0, 0, 0);" /></a></li>
									<?php	
									}
									else{
									?>
										<li><input type="button" value=" Save " onclick="submit_form()" style="border-radius:5px; -webkit-border-radius:5px; -moz-border-radius:5px; -ms-border-radius:5px; -o-border-radius:5px; color: rgb(255, 255, 255); font-family: 'futura-pt',sans-serif; font-size: 0.825em; font-weight: bold; padding: 6px 10px; float: left; margin-right: 10px; border: 0px none; background-color: rgb(0, 0, 0);" /></li>
									<?php
									}
									?>
									
								</ul>
							</div>
						</div>
					</div>
                    <div style="width:665px; float:left;">
                    	<div id="actualContent">

								<?php
								include('community_event_jcrop/community_event.php'); 
										if(isset($thumb_image_name))
										{
											//echo $thumb_image_name;
										}
								?>

							
							<!--<div class="fieldCont">
							  <div class="fieldTitle">*Listing Picture</div>
							 <img src="community_jcrop/croppedFiles/profile/<?php// echo $res1['image_name'];?>"/>
							 
							</div>-->
						
						<?php
						
						if(isset($_GET['id']))
						{?>
						<form action="insertcommunityevent.php?update=<?php  echo $_GET['id'];?>" method="POST"  onsubmit="return validate()">
						<?php
						}
						else
						{
						?>
							<form action="insertcommunityevent.php" method="POST"  onsubmit="return validate()">
						<?php
						}
						?>
						<input type="hidden" id="profile_image" name="profile_image" value="<?php if(isset($_GET['id'])) echo $getdetail['image_name'];?>"/>
						<input type="hidden" id="listing_image" name="listing_image" value="<?php if(isset($_GET['id'])) echo $getdetail['listing_image_name'];?>"/>
						<input type="submit" id="clickbutton"  value="save" style="display:none"/>
						 <div class="fieldCont">
                           	  	<div title="Select a unique title to display on your profile page and across the site when listed." class="fieldTitle">*Title</div>
                                <input title="Select a unique title to display on your profile page and across the site when listed." name="title" type="text" class="fieldText" id="title" value="<?php if(isset($_GET['id'])) echo $getdetail['title'];?>">
                    		</div>
							
							<div class="fieldCont">
                           	  	<div title="The registered user who created and/or is promoting the event. If the creator does not display when typing than have them register as an artist or community user." class="fieldTitle">Creator</div>
                                <input title="The registered user who created and/or is promoting the event. If the creator does not display when typing than have them register as an artist or community user." name="creator" type="text" class="fieldText" id="creator" value="<?php if(isset($_GET['id'])) {
								
									if($getdetail['creators_info']!="") 
										{
											$exps = explode('|',$getdetail['creators_info']);
											$naam = $exps[0];
											$nam_id = $exps[1];
											
											if($naam=='general_artist')
											{
												$sql = mysql_query("SELECT * FROM general_artist WHERE artist_id='".$nam_id."'");
											}
											if($naam=='general_community')
											{
												$sql = mysql_query("SELECT * FROM general_community WHERE community_id='".$nam_id."'");
											}
											if($naam=='artist_project' || $naam=='community_project')
											{
												$sql = mysql_query("SELECT * FROM $naam WHERE id='".$nam_id."'");
											}
											
											if(isset($sql) && mysql_num_rows($sql)>0)
											{
												$ans = mysql_fetch_assoc($sql);
												if(isset($ans['name']))
												{
													echo $ans['name'];
												}
												elseif(isset($ans['title']))
												{
													echo $ans['title'];
												}
											}
										}
								}
								?>">
								<div class="hint">
								Promoter
								</div>
								<div class="hint" style="display:none;" id="User_error_creator">No profiles found with that name.</div>
								<input name="creators_info" id="creators_info" type="hidden" style="width:300px;" class="fieldText" value="<?php if(isset($_GET['id'])) echo $getdetail['creators_info'];?>"  />
								<input name="previous_creators_info" id="previous_creators_info" type="hidden" style="width:300px;" class="fieldText" value="<?php if(isset($_GET['id'])) echo $getdetail['creators_info'];?>"  />
                    		</div>
                        	
							<div class="fieldCont">
                           	  <div title="Tag users who may use the event across the site but may not edit it." class="fieldTitle">Users</div>
                                <input title="Tag users who may use the event across the site but may not edit it." name="communitybands" type="text" class="fieldText" style="width:300px;" id="communitybands" value="<?php //if(isset($_GET['id'])) echo $getdetail['users'];?>">
								<input name="useremail_old" type="hidden" class="fieldText" style="width:300px;" id="useremail_old" value="">
								<input name="useremail" type="hidden" class="fieldText" style="width:300px;" id="useremail" value="<?php if(isset($_GET['id'])) echo $getdetail['tagged_user_email'];?>">
								<input name="useremail_removed" type="hidden" class="fieldText" id="useremail_removed" />
								<div class="hint" style="width:0px;">
								<input title="Press to add tagged user." name="ADD" type="button" id="add_user_enable" value=" Add " style="border-radius:5px; -webkit-border-radius:5px; -moz-border-radius:5px; -ms-border-radius:5px; -o-border-radius:5px;" onclick="addcommunityBands()" />
								</div>
								<div class="hint" style="width:0px;">
								<input style="border-radius:5px; -webkit-border-radius:5px; -moz-border-radius:5px; -ms-border-radius:5px; -o-border-radius:5px;" title="Press to add tagged user." name="ADD" type="button" id="add_user_disable" value=" Add " />
								</div>
								<div class="hint" style="display:none;" id="User_error">No profiles found with that name.</div>
                    		</div>
                            <div class="fieldCont">
                              <div title="Users who have been tagged in the event." class="fieldTitle">Users Tagged</div>
                                <select title="Users who have been tagged in the event." name="communitybands_tagged[]" size="5" multiple="multiple" id="communitybands_tagged[]" class="list">
                                   <?php
								   if(isset($_GET['id']))
								   {
										for($usr=0;$usr<count($exp_usr);$usr++)
										{
											$sql = $Addcomevent->get_users_taggeds($exp_usr[$usr]);
											if(empty($sql))
											{
												continue;
											}
											else
											{
										?>
											<option value="<?php echo $sql['fname'].' '.$sql['lname']; ?>" selected><?php echo $sql['fname'].' '.$sql['lname']; ?></option>
										<?php
											}
										}
								   }
									/*if($res['id']>0)
									{
											for($i=0;$i<count($showband);$i++)
											{
									?>
												<option value="<?php echo $showband[$i];?>"><?php echo $showband[$i];?></option>
									<?php	
											}
									}*/
									?>
                                </select>
                            </div>
							<div class="hint" style="width:0px;"><input name="remove" id="remove" type="button"  value=" Remove " onclick="remove_community_event_user()" style="border-radius:5px; -webkit-border-radius:5px; -moz-border-radius:5px; -ms-border-radius:5px; -o-border-radius:5px; display:none;left:-20px;position:relative;"/></div>
							
							<!--<div class="fieldCont">
                           	  <div class="fieldTitle">Bands</div>
                                <input name="communitybands" type="text" class="fieldText" id="communitybands" value="<?php //if(isset($_GET['id'])) echo $getdetail['bands'];?>">
								<div class="hint"><input name="ADD" type="button"  value=" Add " onclick="addcommunityBands()"></div>
                    		</div>
                            <div class="fieldCont">
                              <div class="fieldTitle">Bands Tagged</div>
                                <select name="communitybands_tagged[]" size="5" multiple="multiple" id="communitybands_tagged[]" class="list">
                                   <?php
									//if($res['id']>0)
								//	{
								//			for($i=0;$i<count($showband);$i++)
								//			{
									?>
												<option value="<?php //echo $showband[$i];?>"><?php //echo $showband[$i];?></option>
									<?php	
								//			}
								//	}
									?>
                                </select>
                            </div>
							<div class="fieldCont">
                           	  <div class="fieldTitle">Artist</div>
                                <input name="communityartistname" type="text" class="fieldText" id="communityartistname" value="<?php //if(isset($_GET['id'])) echo $getdetail['artist'];?>">
								<div class="hint"><input name="ADD" type="button" value=" Add " onclick="addcommunityartist()" /></div>
                    		</div>
                            
                            <div class="fieldCont">
                              <div class="fieldTitle">Artists Tagged</div>
                                <select name="communityartists_tagged[]" size="5" multiple="multiple" id="communityartists_tagged[]" class="list">
                                     <?php
									//if($res['id']>0)
									//{
											//for($i=0;$i<count($showartist);$i++)
											//{
									?>
												<option value="<?php //echo $showartist[$i];?>"><?php //echo $showartist[$i];?></option>
									<?php	
											//}
									//}
								?>
                                </select>
                            </div>-->
							<!--<div class="fieldCont">
                           	  <div class="fieldTitle">Other Artists</div>
                                <textarea name="otherartist" id="otherartist" class="textbox"><?php if(isset($_GET['id'])) echo $getdetail['other_artists'];?></textarea>
                    		</div>-->
							<div class="fieldCont">
                           	  <div class="fieldTitle">Date</div>
                                <input name="datepicker" type="text" class="fieldText" id="datepicker" value="<?php if(isset($_GET['id'])) echo $getdetail['date'];?>">
                    		</div>
							<div class="fieldCont">
                           	  <div class="fieldTitle">Start Time</div>
                                
								<?php
									if(isset($_GET['id']))
									{
										$exp_time = explode(':',$getdetail['start_time']);
										$hrs = $exp_time[0];
										$min = $exp_time[1];
									}
									else
									{
										$hrs = 1;
										$min = 0;
									}
								?>
								<select name="start_hrs" id="start_hrs" class="dropdown" style="width: 50px;">
								<?php
									$start_s = 1;
									$last_s = 12;
									for(;$start_s<=$last_s;$start_s++)
									{
										if($hrs==$start_s)
										{
								?>
										<option selected value="<?php echo $start_s; ?>"><?php echo $start_s; ?></option>
								<?php
										}
										else
										{
								?>
											<option value="<?php echo $start_s; ?>"><?php echo $start_s; ?></option>
								<?php
										}
									}
								?>
								</select>
								
								<select name="start_min" id="start_min" class="dropdown" style="margin-left: 23px; width: 50px;">
								<?php
									$start_s = 0;
									$last_s = 59;
									for(;$start_s<=$last_s;$start_s++)
									{
										if($min==$start_s)
										{
								?>
											<option selected value="<?php echo $start_s; ?>"><?php
											if($start_s=='0' || $start_s=='1' || $start_s=='2' || $start_s=='3' || $start_s=='4' || $start_s=='5' || $start_s=='6' || $start_s=='7' || $start_s=='8' || $start_s=='9')
											{
												echo '0'.$start_s; 
											}
											else
											{
												echo $start_s; 
											}
											?></option>
								<?php
										}
										else
										{
								?>
											<option value="<?php echo $start_s; ?>"><?php
											if($start_s=='0' || $start_s=='1' || $start_s=='2' || $start_s=='3' || $start_s=='4' || $start_s=='5' || $start_s=='6' || $start_s=='7' || $start_s=='8' || $start_s=='9')
											{
												echo '0'.$start_s; 
											}
											else
											{
												echo $start_s; 
											}
											?></option>
								<?php
										}
									}
								?>
								</select>
								
								<!--<input name="starttime" type="text" class="fieldText" id="starttime" value="<?php if(isset($_GET['id'])) echo $getdetail['start_time'];?>">-->
								
								<select name="startevmo" id="startevmo" class="dropdown" style="width:50px; margin-left:23px;">
								<?php
								if(isset($_GET['id']) && $getdetail['start_timeap']!="") 
								{ 
									if($getdetail['start_timeap']=='am')
									{
								?>
									<option selected value="am">am</option>
									<option value="pm">pm</option>
								<?php
									}
									elseif($getdetail['start_timeap']=='pm')
									{
								?>
									<option  value="am">am</option>
									<option selected value="pm">pm</option>
								<?php
									}
								}
								else
								{
								?>
									<option value="am">am</option>
									<option value="pm">pm</option>
								<?php
								}?>
								</select>
                    		</div>
							<div class="fieldCont">
                           	  <div class="fieldTitle">End Time</div>
                                
								<?php
									if(isset($_GET['id']))
									{
										$exp_time = explode(':',$getdetail['end_time']);
										$hrs = $exp_time[0];
										$min = $exp_time[1];
									}
									else
									{
										$hrs = 1;
										$min = 0;
									}
								?>
								<select name="end_hrs" id="end_hrs" class="dropdown" style="width: 50px;">
								<?php
									$start_s = 1;
									$last_s = 12;
									for(;$start_s<=$last_s;$start_s++)
									{
										if($hrs==$start_s)
										{
								?>
										<option selected value="<?php echo $start_s; ?>"><?php echo $start_s; ?></option>
								<?php
										}
										else
										{
								?>
											<option value="<?php echo $start_s; ?>"><?php echo $start_s; ?></option>
								<?php
										}
									}
								?>
								</select>
								
								<select name="end_min" id="end_min" class="dropdown" style="margin-left: 23px; width: 50px;">
								<?php
									$start_s = 0;
									$last_s = 59;
									for(;$start_s<=$last_s;$start_s++)
									{
										if($min==$start_s)
										{
										
								?>
											<option selected value="<?php echo $start_s; ?>"><?php
											if($start_s=='0' || $start_s=='1' || $start_s=='2' || $start_s=='3' || $start_s=='4' || $start_s=='5' || $start_s=='6' || $start_s=='7' || $start_s=='8' || $start_s=='9')
											{
												echo '0'.$start_s; 
											}
											else
											{
												echo $start_s; 
											}
											?></option>
								<?php
										}
										else
										{
								?>
											<option value="<?php echo $start_s; ?>"><?php
											if($start_s=='0' || $start_s=='1' || $start_s=='2' || $start_s=='3' || $start_s=='4' || $start_s=='5' || $start_s=='6' || $start_s=='7' || $start_s=='8' || $start_s=='9')
											{
												echo '0'.$start_s; 
											}
											else
											{
												echo $start_s;
											}											
											?></option>
								<?php
										}
									}
								?>
								</select>
								
								<!--<input name="endtime" type="text" class="fieldText" id="endtime" value="<?php if(isset($_GET['id'])) echo $getdetail['end_time'];?>">-->
								
								<select name="startevmoe" id="startevmoe" class="dropdown" style="width:50px; margin-left:23px;">
								<?php if(isset($_GET['id']) && $getdetail['end_timeap']!="") 
								{ 
									if($getdetail['end_timeap']=='am')
									{
								?>
									<option selected value="am">am</option>
									<option value="pm">pm</option>
								<?php
									}
									elseif($getdetail['end_timeap']=='pm')
									{
								?>
									<option  value="am">am</option>
									<option selected value="pm">pm</option>
								<?php
									}
								}
								else
								{
								?>
									<option value="am">am</option>
									<option value="pm">pm</option>
								<?php
								}
								?>
								</select>
                    		</div>
							
							<div class="fieldCont">
                                  <div title="Select a unique URL to be used for accessing the events profile page." class="fieldTitle">*Profile URL</div>
								  <div class="urlTitle">purifyart.com/</div>
                                  <input title="Select a unique URL to be used for accessing the events profile page." name="subdomain" type="text" class="urlField" id="subdomain" value="<?php if(isset($_GET['id'])) echo $getdetail['profile_url'];?>">
								  <div class="hintnew"><span class="hintR" id="fieldCont_domain"></span><br />(purifyart.com/leerick)</div>
                                </div>
								
							<!--<div class="fieldCont">
                                <div class="fieldTitle">Venue</div>
                                 <input name="venue" type="text" class="fieldText" id="venue" value="<?php //if(isset($_GET['id'])) echo $getdetail['venue'];?>">
                            </div>-->
							
							<div class="fieldCont">
                                <div class="fieldTitle">Venue Name</div>
                                <input name="venuename" type="text" class="fieldText" id="venuename" value="<?php if(isset($_GET['id'])) echo $getdetail['venue_name'];?>">
                            </div>
							
                            <div class="fieldCont">
                                <div class="fieldTitle">Venue Address</div>
                                <input name="venueaddress" type="text" class="fieldText" id="venueaddress" value="<?php if(isset($_GET['id'])) echo $getdetail['venue_address'];?>">
                            </div>
							
                            <div class="fieldCont">
                                <div class="fieldTitle">Venue Website</div>
								<?php 
								if(!isset($getdetail['venue_website']) && $getdetail['venue_website'] =="")
								{
								?>
									<div class="urlTitle">http://www.</div>
								<?php
								}
								?>
                                <input name="venuewebsite" type="text" class="<?php if(isset($_GET['id']) && $getdetail['venue_website']!=""){echo "fieldText";}else {echo "urlField";}?>" id="venuewebsite" value="<?php if(isset($_GET['id']) && $getdetail['venue_website']!=""){if(strpos($getdetail['venue_website'],':')>0){echo $getdetail['venue_website'];}else {echo "http://www.".$getdetail['venue_website'];}}?>">
                            </div>
							
							<div class="fieldCont">
                                <div class="fieldTitle">Country</div>
                                <select name="selectCountry" id="selectCountry" class="dropdown">
                                   <option value="0">Select</option>
                                    <?php
									while($get_country = mysql_fetch_assoc($get_all_countries))
									{
										if(isset($_GET['id']) && $getdetail['country_id'] == $get_country['country_id'])
										{	
										?>
											<option value="<?php echo $get_country['country_id']?>" Selected><?php echo $get_country['country_name'];?></option>
										<?php	
										}
									?>
										<option value="<?php echo $get_country['country_id']?>"><?php echo $get_country['country_name'];?></option>
									<?php
									}
									
									?>
                                </select>
                            </div>
                            <div class="fieldCont">
                                <div class="fieldTitle">State / Province</div>
                                <select name="selectState" id="selectState" class="dropdown">
                                   <option value="0">Select State</option>
                                    <?php
									if(isset($_GET['id']))
									{
										while($getstate = mysql_fetch_assoc($get_all_states))
										{
											if($getstate['state_id'] == $getdetail['state_id'])
											{
										?>
												<option value="<?php echo $getstate['state_id']; ?>" selected><?php echo $getstate['state_name']; ?></option>
										<?php
											}
											else
											{
											?>
												<option value="<?php echo $getstate['state_id']; ?>" ><?php echo $getstate['state_name']; ?></option>
											<?php
											}
										}
									}
									?>
                                </select>
                            </div>
                            <div class="fieldCont">
                                <div class="fieldTitle">City</div>
                                <input name="editCity" id="editCity" class="fieldText" value="<?php if(isset($_GET['id'])) echo $getdetail['city'];?>"/>
                                </input>
                            </div>
							
							<div class="fieldCont">
                                  <div title="Select a media file to feature on the events profile page and when the event is listed on other profiles and in search engine." class="fieldTitle">Featured Media</div>
                                 <select title="Select a media file to feature on the events profile page and when the event is listed on other profiles and in search engine." name="featured_media" class="dropdown" id="featured_media">
                                    <option value="nodisplay" >Select Featured Media</option>
                                    <option value="Gallery"  <?php if(isset($getdetail['featured_media']) && $getdetail['featured_media']=="Gallery") echo "selected";?> >Gallery</option>	
                                    <option value="Song" <?php if(isset($getdetail['featured_media']) && $getdetail['featured_media']=="Song") echo "selected";?> >Song</option>
                                    <option value="Video" <?php if(isset($getdetail['featured_media']) && $getdetail['featured_media']=="Video") echo "selected";?> >Video</option>		
                                    <!--<option value="Channel" <?php /*if(isset($getdetail['featured_media']) && $getdetail['featured_media']=="Channel") echo "selected";*/?> >Channel</option>-->
                                </select>
								<div id="loader" style="display:none;">
									<img src="images/ajax_loader_large.gif" style="height: 25px; position: relative; right: 120px; top: 34px; width: 25px;">
								</div>
								<div id="fieldCont_media" style="display:none;">
								</div>
                                </div>
							<?php 
								if(isset($getdetail['featured_media']))
								{
								?>
								<script type="text/javascript">
									selected_feature("<?php echo $getdetail['featured_media'];?>");
								</script>
								<?php
								}
								?>
							
							
                            <div class="fieldCont">
                                <div class="fieldTitle">Description</div>
								<!--<input type="text" name="description" class="fieldText" id="description" value="<?php //if(isset($_GET['id'])) echo $getdetail['description'];?>"/>-->
                             <!--  <textarea  class="jquery_ckeditor" cols="4" id="description" name="description" rows="10"><?php if(isset($_GET['id'])) echo $getdetail['description'];?></textarea>
							   <textarea  class="jquery_ckeditor" cols="4" id="description" name="description" rows="10"><?php //if(isset($_GET['id'])) echo $getdetail['description'];?></textarea> 
							   <div id="count_for_bio" class="counts"></div>-->
							   <textarea spellcheck="true" cols="65" id="description" name="description" rows="10"><?php if(isset($_GET['id'])) echo $getdetail['description'];?></textarea>
							   
                            </div>
							
                            <!--<div class="fieldCont">
                              <div class="fieldTitle">Featured Media</div>
                              <select name="featured_media"  id="featured_media" class="dropdown">
                                    <option value="nodisplay" >Select Featured Media</option>
                                    <option value="selectMedia" >Gallery</option>					
                                    <option value="selectMedia" >Song</option>					
                                    <option value="selectMedia" >Video</option>					
                                    <option value="selectMedia" >Channel</option>					
                                </select>
                            </div>-->
							
							
							
                            <div class="fieldCont"><h4>Type Association</h4>
							<!--<h4>Type Association</h4>-->
<div class="fieldCont" style="font-size:12px;"><!--Select type associations to represent your profile. Your first selections will display on your profile page, all others will be used for search engines. If you would like to change your display than remove all types and select your desired display type first.-->Suggest a new type or subtype by emailing info@purifyart.com</div></div>
							<div  style="float:right;" id="selected-types"></div>
                            <div class="fieldCont">
                            	<div class="fieldTitle">*Type</div>
                                <select name="type-select" class="dropdown" id="type-select">
								<option value="0" selected="selected">Select Type</option>
                                   	<?php
									while($newrow5=mysql_fetch_assoc($selType))
									{
										if(isset($_GET['id']))
										{
											/*if($getdetail['primary_type']==$newrow5['type_id'])
											{
									?>
											<option value="<?php echo $newrow5['type_id']; ?>" selected="selected"  chkval="<?php echo $newrow5['name']; ?>"><?php echo $newrow5['name']; ?></option>
									<?php
											}
											else
											{*/
											?>
												<option value="<?php echo $newrow5['type_id']; ?>"  chkval="<?php echo $newrow5['name']; ?>"><?php echo $newrow5['name']; ?></option>
											<?php
											//}
										}
										else
										{
										?>
											<option value="<?php echo $newrow5['type_id']; ?>"  chkval="<?php echo $newrow5['name']; ?>"><?php echo $newrow5['name']; ?></option>
										<?php
										}
									}
									?>					
                    			</select>
                    		</div>
                            <div class="fieldCont">
                                <div class="fieldTitle">Sub Type</div>
                                <select name="subtype-select" class="dropdown" id="subtype-select" disabled>
									<option selected="selected" value="0">Select SubType</option>
                                	<?php
											//while($getsubtype1=mysql_fetch_assoc($getsubtype))
											while($getsubtype1=mysql_fetch_assoc($sql6))
											{
												/*if($getsubtype1['subtype_id']==$getdetail['sub_type'])
												{
										?>
													<option value="<?php echo $getdetail['sub_type'];?>" selected="selected" chkval="<?php echo $getsubtype1['name']; ?>"><?php echo $getsubtype1['name']; ?></option>
										<?php
												}
												else
												{*/
										?>
													<option value="<?php echo $getsubtype1['subtype_id'];?>" chkval="<?php echo $getsubtype1['name']; ?>" ><?php echo $getsubtype1['name']; ?></option>
										<?php
												//}
											}				
										?>					
                    			</select>
                            </div>
							<div class="fieldCont">
                                <div class="fieldTitle">Meta Type</div>
                                <select name="metatype-select" class="dropdown" id="metatype-select" disabled>
									<option selected="selected" value="0">Select MetaType</option>
                                	<?php
											while($getmetatype=mysql_fetch_assoc($newsql1))
											{
												/*if($getmetatype['meta_id']==$getdetail['meta_type'])
												{
										?>
													<option value="<?php echo $getdetail['meta_type'];?>" selected="selected"><?php echo $getmetatype['name'];?></option>
										<?php
												}
												else
												{*/
										?>
													<option value="<?php echo $getmetatype['meta_id'];?>"><?php echo $getmetatype['name'];?></option>
										<?php
												//}
											}				
										?>
														
                    			</select>
								<div class="hint"><input name="ADD" type="button" id="addType" value=" Add "></div>
                            </div>
							
							<!--<div class="fieldCont">
                            	<div class="fieldTitle">Secondary Type</div>
                                <select name="select-category-artist-subtype" class="dropdown" id="subtype-select">
                                   	<option value="0" selected="selected">Select Type</option>					
                    			</select>
                    		</div>
                            <div class="fieldCont">
                                <div class="fieldTitle">Sub Type</div>
                                <select name="select-category-artist-metatype" class="dropdown" id="metatype-select">
                                	<option value="0" selected="selected">Select Sub Type</option>					
                    			</select>
                            </div>
							<div class="fieldCont">
                                <div class="fieldTitle">Meta Type</div>
                                <select name="select-category-artist-metatype" class="dropdown" id="metatype-select">
                                	<option value="0" selected="selected">Select Meta Type</option>					
                    			</select>
                                <div class="hint"><input name="ADD" type="button" id="addType" value=" Add "></div>
                            </div>-->
            <?php /* ?>                <div class="fieldCont"><h4>Tickets</h4>
							<div class="fieldCont">
                                <div class="fieldTitle">Cost</div>
                                <input name="cost" type="text" class="fieldText" id="cost" value="<?php if(isset($_GET['id'])) echo $getdetail['cost'];?>">
							</div>
							<div class="fieldCont">
                               <div class="fieldTitle">Sell Tickets in Store</div>
									<div class="chkCont">
										<?php 
											if(isset ($getdetail['sell_tickets']) && $getdetail['sell_tickets']=='1')
											{
										?>
												<input type="radio" class="chk" name="yes" id="yes" checked="checked" value="1"/>
												<div class="radioTitle" >Yes</div>
										<?php
											}
											else
											{
										?>
												<input type="radio" class="chk" name="yes" id="yes" value="1"/>
												<div class="radioTitle" >Yes</div>
										<?php
											}
										?>
									</div>
									<div class="chkCont">
										<?php 
											if(isset ($getdetail['sell_tickets']) && $getdetail['sell_tickets']=='0')
											{
										?>
												<input type="radio" class="chk" name="yes" id="no" checked="checked" value="0"/>
												<div class="radioTitle">No</div>
										<?php
											}
											else
											{
										?>
												<input type="radio" class="chk" name="yes" id="no" value="0"/>
												<div class="radioTitle">No</div>
										<?php
											}
										?>
									</div>
							</div>
                            <div class="fieldCont">
                                <div class="fieldTitle">Add Name to Guest List</div>
                                <input name="communityaddguest" type="text" class="fieldText" id="communityaddguest" value="<?php //if(isset($_GET['id'])) echo $getdetail['add_guest_name'];?>">
                                <div class="hint"><input name="ADD" type="button"  value=" Add " onclick="addcommunityguestname()"></div>
							</div>
							<div class="fieldCont">
                           	  <div class="fieldTitle">Guest List</div>
                                <select name="communityguest_list[]" size="5" multiple="multiple" id="communityguest_list[]" class="list">
                                   	<?php
									//if($res['id']>0)
									//{
											for($i=0;$i<count($showguest);$i++)
											{
												if($showguest[$i]=="")
												{
												}
												else
												{
												?>
													<option value="<?php echo $showguest[$i];?>" selected><?php echo $showguest[$i];?></option>
												<?php	
												}
									
											}
									//}
								?>
                    			</select>
								</div>
								<input style="position:relative; left:114px;" id="copyvalue" class="button" type="button" value="Copy Guest List" />
                    		</div><?php */ ?>
							
							<h4>Share Links</h4>
						
                                <div class="fieldCont">
									<div title="" class="fieldTitle">Facebook</div>
									<div class="urlTitle_share">facebook.com/</div>
									<input title="Select a unique URL to be used for accessing your profile page." name="fb_share" type="text" class="urlField" id="fb_share" value="<?php echo $getdetail['fb_share']; ?>" />
                                </div>
								
								<div class="fieldCont">
									<div title="" class="fieldTitle">Twitter</div>
									<div class="urlTitle_share" >twitter.com/</div>
									<input title="Select a unique URL to be used for accessing your profile page." name="twit_share" type="text" class="urlField" id="twit_share" value="<?php echo $getdetail['twit_share']; ?>" />
                                </div>
								
								<div class="fieldCont">
									<div title="" class="fieldTitle">Google+</div>
									<div class="urlTitle_share" >plus.google.com/</div>
									<input title="Select a unique URL to be used for accessing your profile page." name="gplus_share" type="text" class="urlField" id="gplus_share" value="<?php echo $getdetail['gplus_share']; ?>" />
                                </div>
								
								<div class="fieldCont">
									<div title="" class="fieldTitle">Tumblr</div>
									<input title="Select a unique URL to be used for accessing your profile page." name="tubm_share" type="text" class="urlField" id="tubm_share" value="<?php echo $getdetail['tubm_share']; ?>" />
									<div class="urlTitle_share" >.tumblr.com</div>
                                </div>
								
								<div class="fieldCont">
									<div title="" class="fieldTitle">StumbleUpon</div>
									<div class="urlTitle_share" >stumbleupon.com/stumbler/</div>
									<input title="Select a unique URL to be used for accessing your profile page." name="stbu_share" type="text" class="urlField" id="stbu_share" value="<?php echo $getdetail['stbu_share']; ?>" />
                                </div>
								
								<div class="fieldCont">
									<div title="" class="fieldTitle">Pinterest</div>
									<div class="urlTitle_share" >pinterest.com/</div>
									<input title="Select a unique URL to be used for accessing your profile page." name="pin_share" type="text" class="urlField" id="pin_share" value="<?php echo $getdetail['pin_share']; ?>" />
                                </div>
								
								<div class="fieldCont">
									<div title="" class="fieldTitle">Youtube</div>
									<div class="urlTitle_share" >youtube.com/</div>
									<input title="Select a unique URL to be used for accessing your profile page." name="you_share" type="text" class="urlField" id="you_share" value="<?php echo $getdetail['you_share']; ?>" />
                                </div>
								
								<div class="fieldCont">
									<div title="" class="fieldTitle">Vimeo</div>
									<div class="urlTitle_share" >vimeo.com/</div>
									<input title="Select a unique URL to be used for accessing your profile page." name="vimeo_share" type="text" class="urlField" id="vimeo_share" value="<?php echo $getdetail['vimeo_share']; ?>" />
                                </div>
								
								<div class="fieldCont">
									<div title="" class="fieldTitle">Soundcloud</div>
									<div class="urlTitle_share" >soundcloud.com/</div>
									<input title="Select a unique URL to be used for accessing your profile page." name="sdcl_share" type="text" class="urlField" id="sdcl_share" value="<?php echo $getdetail['sdcl_share']; ?>" />
                                </div>
								
								<div class="fieldCont">
									<div title="" class="fieldTitle">Instagram</div>
									<div class="urlTitle_share" >instagram.com/</div>
									<input title="Select a unique URL to be used for accessing your profile page." name="ints_share" type="text" class="urlField" id="ints_share" value="<?php echo $getdetail['ints_share']; ?>" />
                                </div>
							
							
                            <div class="fieldCont"><h4>Tag Profiles</h4>
							<div class="fieldCont" style="font-size:12px;">Select all projects, media and events that you would like to list on this profiles display page.</div>
							</div>
                            <div class="fieldCont">
                           	  <div title="Select media and profiles to display on your profile page." class="fieldTitle">Galleries</div>
                                   	<?php
											/* if(isset($res_gal) && $res_gal!=null)
											{
												//$get_community_gallery_name=$Addcomevent->get_community_gallery_name_at_register();
												for($gal_reg=0;$gal_reg<count($sel_gal);$gal_reg++)
												{
													if($res_gal['gallery_id']==$sel_gal[$gal_reg])
													{
												?>
													<option value="<?php echo $res_gal['gallery_id'];?>" selected ><?php echo substr($newres1['fname'].' '.$newres1['lname'],0,20) ." : ". $res_gal['gallery_title'];?></option>
												<?php	
														$gal_reg_con=1;
														continue;
													}
												}
												if($gal_reg_con==1)
												{
												}
												else
												{
												?>
													<option value="<?php echo $res_gal['gallery_id'];?>" ><?php echo substr($newres1['fname'].' '.$newres1['lname'],0,20) ." : ". $res_gal['gallery_title'];?></option>
												<?php	
												}
											} */
											
										$get_gallery_id = array();
										while($get_gallery_ids = mysql_fetch_assoc($get_community_gallery_id))
										{
											$get_gallery_id[] = $get_gallery_ids;
										}
											
										 $get_media_tag_art_pro = $Addcomevent->get_artist_project_tag($_SESSION['login_email']);
										if(mysql_num_rows($get_media_tag_art_pro)>0)
										{
											$is_g = 0;
											while($art_pro_tag = mysql_fetch_assoc($get_media_tag_art_pro))
											{
												$get_gallery = explode(",",$art_pro_tag['tagged_galleries']);
												for($count_art_t1_song=0;$count_art_t1_song<count($get_gallery);$count_art_t1_song++)
												{
													if($get_gallery[$count_art_t1_song]!="")
													{
														$get_gal_new[$is_g] = $get_gallery[$count_art_t1_song];
														$is_g = $is_g + 1;
													}
												}
											}
											if(!empty($get_gal_new))
											{
												$get_gal_n = array_unique($get_gal_new);
											}
										}
											
										$get_media_tag_com_pro = $Addcomevent->get_community_project_tag($_SESSION['login_email']);
										if(mysql_num_rows($get_media_tag_com_pro)>0)
										{
											$is_gc = 0;
											while($com_pro_tag = mysql_fetch_assoc($get_media_tag_com_pro))
											{
												$get_com_gallery = explode(",",$com_pro_tag['tagged_galleries']);
												for($cou1_com_tag_song=0;$cou1_com_tag_song<count($get_com_gallery);$cou1_com_tag_song++)
												{
													if($get_com_gallery[$cou1_com_tag_song]!="")
													{
														$get_gal_new_c[$is_gc] = $get_com_gallery[$cou1_com_tag_song];
														$is_gc = $is_gc + 1;
													}
												}
											}
											if(!empty($get_gal_new_c))
											{
												$get_gal_n_c = array_unique($get_gal_new_c);
											}
										}
										
										$get_media_tag_art_eve = $Addcomevent->get_artist_event_tag($_SESSION['login_email']);
										if(mysql_num_rows($get_media_tag_art_eve)>0)
										{
											$is_g = 0;
											while($art_pro_tag = mysql_fetch_assoc($get_media_tag_art_eve))
											{
												$get_gallery = explode(",",$art_pro_tag['tagged_galleries']);
												for($count_art_t1_song=0;$count_art_t1_song<count($get_gallery);$count_art_t1_song++)
												{
													if($get_gallery[$count_art_t1_song]!="")
													{
														$get_gdfal_new[$is_g] = $get_gallery[$count_art_t1_song];
														$is_g = $is_g + 1;
													}
												}
											}
											if(!empty($get_gdfal_new))
											{
												$get_gal_qen = array_unique($get_gdfal_new);
											}
										}
											
										$get_media_tag_com_eve = $Addcomevent->get_community_event_tag($_SESSION['login_email']);
										if(mysql_num_rows($get_media_tag_com_eve)>0)
										{
											$is_gc = 0;
											while($com_pro_tag = mysql_fetch_assoc($get_media_tag_com_eve))
											{
												$get_com_gallery = explode(",",$com_pro_tag['tagged_galleries']);
												for($cou1_com_tag_song=0;$cou1_com_tag_song<count($get_com_gallery);$cou1_com_tag_song++)
												{
													if($get_com_gallery[$cou1_com_tag_song]!="")
													{
														$get_gal_nuyew_c[$is_gc] = $get_com_gallery[$cou1_com_tag_song];
														$is_gc = $is_gc + 1;
													}
												}
											}
											if(!empty($get_gal_nuyew_c))
											{
												$get_gal_n_rtc = array_unique($get_gal_nuyew_c);
											}
										}
										
										$get_media_cre_art_pro = $Addcomevent->get_project_create();
										
										if(!empty($get_media_cre_art_pro))
										{
											$is_gp = 0;
											//while($art_pro_tag = mysql_fetch_assoc($get_media_cre_art_pro))
											for($cra_i=0;$cra_i<count($get_media_cre_art_pro);$cra_i++)
											{
												$get_cgal = explode(",",$get_media_cre_art_pro[$cra_i]['tagged_galleries']);
												
												for($count_art_cr_g=0;$count_art_cr_g<count($get_cgal);$count_art_cr_g++)
												{
													if($get_cgal[$count_art_cr_g]!="")
													{
														$get_gal_pn[$is_gp] = $get_cgal[$count_art_cr_g];
														$is_gp = $is_gp + 1;
													}
												}
											}
											if(!empty($get_gal_pn))
											{
												$get_gal_p_new = array_unique($get_gal_pn);
											}
										} 
										
										if(!isset($get_gal_n))
										{
											$get_gal_n = array();
										}
										
										
										if(!isset($get_gal_n_c))
										{
											$get_gal_n_c = array();
										}
										
										if(!isset($get_gal_qen))
										{
											$get_gal_qen = array();
										}
										
										if(!isset($get_gal_n_rtc))
										{
											$get_gal_n_rtc = array();
										}
										
										if(!isset($get_gal_p_new))
										{
											$get_gal_p_new = array();
										}
										
										/*$find_creator_heis = $Addcomevent -> get_creator_heis();
										$get_genral_user_info = $Addcomevent -> selgeneral();
										$where_creator = array();
										if(!empty($find_creator_heis)){
											for($new_cre_med=0;$new_cre_med<=count($find_creator_heis);$new_cre_med++)
											{
												if($find_creator_heis[$new_cre_med]['general_user_id']!= $get_genral_user_info['general_user_id']){
													if($find_creator_heis[$new_cre_med]['media_type']==113){
													$where_creator[] = $find_creator_heis[$new_cre_med];}
												}
											}
										}
										$find_tag_heis = $Addcomevent -> get_tag_where_heis($_SESSION['login_email']);;
										$get_genral_user_info = $Addcomevent -> selgeneral();
										$where_tagged = array();
										if(!empty($find_tag_heis)){
											for($new_cre_med=0;$new_cre_med<=count($find_tag_heis);$new_cre_med++)
											{
												if($find_tag_heis[$new_cre_med]['general_user_id']!= $get_genral_user_info['general_user_id']){
													if($find_tag_heis[$new_cre_med]['media_type']==113){
													$where_tagged[] = $find_tag_heis[$new_cre_med];}
												}
											}
										}*/
										
										/*This function will display the user in which profile he is creator and display media where that profile was creator*/
										$new_medi = $Addcomevent->get_creator_heis();
										$new_creator_creator_media_array = array();
										if(!empty($new_medi))
										{
											for($count_cre_m=0;$count_cre_m<count($new_medi);$count_cre_m++)
											{
												if($new_medi[$count_cre_m]['media_type']==113){
													$new_creator_creator_media_array[] = $new_medi[$count_cre_m];
												}
											}
										}
										//var_dump($new_creator_creator_media_array);
										/*This function will display the user in which profile he is creator and display media where that profile was creator Ends here*/
										
										/*This function will display the user in which profile he is from and display media where that profile was creator*/
										$new_medi_from = $Addcomevent->get_from_heis();
										$new_from_from_media_array = array();
										if(!empty($new_medi_from))
										{
											for($count_cre_m=0;$count_cre_m<count($new_medi_from);$count_cre_m++)
											{
												if($new_medi_from[$count_cre_m]['media_type']==113){
												$new_from_from_media_array[] = $new_medi_from[$count_cre_m];
												}
											}
										}
										//var_dump($new_creator_creator_media_array);
										/*This function will display the user in which profile he is from and display media where that profile was creator Ends here*/
										
										
										/*This function will display the user in which profile he is from or creator and tagged and display media where that profile was creator*/
										$new_medi_tagged = $Addcomevent->get_the_user_tagin_project($_SESSION['login_email']);
										$new_tagged_from_media_array = array();
										if(!empty($new_medi_tagged))
										{
											for($count_cre_m=0;$count_cre_m<count($new_medi_tagged);$count_cre_m++)
											{
												if($new_medi_tagged[$count_cre_m]['media_type']==113){
												$new_tagged_from_media_array[] = $new_medi_tagged[$count_cre_m];
												}
											}
										}
										//var_dump($new_creator_creator_media_array);
										/*This function will display the user in which profile he is from or creator and tagged and display media where that profile was creator Ends here*/
										
										
										/*This function will display in which media the curent user was creator*/
										$get_where_creator_media = $Addcomevent->get_media_where_creator_or_from();
										$new_creator_media_array = array();
										if(!empty($get_where_creator_media))
										{
											for($count_tag_m=0;$count_tag_m<count($get_where_creator_media);$count_tag_m++)
											{
												if($get_where_creator_media[$count_tag_m]['media_type']==113){
													$new_creator_media_array[] = $get_where_creator_media[$count_tag_m];
												}
											}
										}
										/*This function will display in which media the curent user was creator ends here*/
										
										$get_gals_pngew_w = array_merge($get_gal_n,$get_gal_n_c,$get_gal_p_new,$get_gal_qen,$get_gal_n_rtc);
										
										$get_gals_pnew_g = array_unique($get_gals_pngew_w);
										$get_gals_final_ct = array();
										
										for($count_art_cr_gs=0;$count_art_cr_gs<count($get_gals_pnew_g);$count_art_cr_gs++)
										{
											if($get_gals_pnew_g[$count_art_cr_gs]!="")
											{
												$get_media_tag_g_art = $newgeneral->get_tag_media_info($get_gals_pnew_g[$count_art_cr_gs]);
												$get_gals_final_ct[] = $get_media_tag_g_art;
											}
										}
										
										$acc_medsg = array();
										for($acc_vis=0;$acc_vis<count($acc_media);$acc_vis++)
										{
											if($acc_media[$acc_vis]!="")
											{
												$segl_tagged = $newgeneral->get_community_media_type_tagged_by_other_user($acc_media[$acc_vis]);
												if(!empty($segl_tagged))
												{
													$acc_medsg[] = $segl_tagged;
												}
											}
										}
										
										$get_gals_final_ct = array();
										$type = '113';
										$cre_frm_med = array();
										$cre_frm_med = $Addcomevent->get_media_create($type);
										
										$get_vgals_pnew_w = array_merge($acc_medsg,$get_gallery_id,$get_gals_final_ct,$cre_frm_med,$new_creator_creator_media_array,$new_from_from_media_array,$new_tagged_from_media_array,$new_creator_media_array);
										
										$sort = array();
										foreach($get_vgals_pnew_w as $k=>$v) {
										$end_c1 = $v['creator'];
											//$create_c = strpos($v['creator'],'(');
											//$end_c1 = substr($v['creator'],0,$create_c);
											$end_c = trim($end_c1);
										$end_f1 = $v['from'];	
											//$create_f = strpos($v['from'],'(');
											//$end_f1 = substr($v['from'],0,$create_f);
											$end_f = trim($end_f1);
											
											$sort['creator'][$k] = strtolower($end_c);
											$sort['from'][$k] = strtolower($end_f);
											//$sort['track'][$k] = $v['track'];
											$sort['title'][$k] = strtolower($v['title']);
										}
										//var_dump($sort);
										if(!empty($sort))
										{
											array_multisort($sort['creator'], SORT_ASC, $sort['from'], SORT_ASC, $sort['title'], SORT_ASC,$get_vgals_pnew_w);
										}
										
										if(!empty($get_vgals_pnew_w))
										{
										?>
												<select title="Select media and profiles to display on your profile page." name="tagged_gallery[]" id="tagged_gallery[]" size="5" multiple="multiple"  class="list">
										<?php
										
										$dummy_g = array();
										$get_all_del_id = $Addcomevent->get_all_del_media_id();
										$deleted_id = explode(",",$get_all_del_id);
										for($count_del=0;$count_del<=count($get_vgals_pnew_w);$count_del++)
										{
											if($get_vgals_pnew_w[$count_del]==""){continue;}
											else{
												for($count_deleted=0;$count_deleted<=count($deleted_id);$count_deleted++)
												{
													if($deleted_id[$count_deleted]==""){continue;}
													else{
														if($deleted_id[$count_deleted] == $get_vgals_pnew_w[$count_del]['id'])
														{
															$get_vgals_pnew_w[$count_del]="";
														}
													}
												}
											}
										}
										for($acc_glae=0;$acc_glae<count($get_vgals_pnew_w);$acc_glae++)
										{
											if($get_vgals_pnew_w[$acc_glae]['delete_status']==0){
												if(isset($get_vgals_pnew_w[$acc_glae]['id']))
												{
													if(in_array($get_vgals_pnew_w[$acc_glae]['id'],$dummy_g))
													{
														
													}
													else
													{
														$dummy_g[] = $get_vgals_pnew_w[$acc_glae]['id'];
														if($get_vgals_pnew_w[$acc_glae]['id']!="" && $get_vgals_pnew_w[$acc_glae]['media_type']==113)
														{
															//$create_c = strpos($get_vgals_pnew_w[$acc_glae]['creator'],'(');
															//$end_c = substr($get_vgals_pnew_w[$acc_glae]['creator'],0,$create_c);
															$end_c = $get_vgals_pnew_w[$acc_glae]['creator'];
															//$create_f = strpos($get_vgals_pnew_w[$acc_glae]['from'],'(');
															//$end_f = substr($get_vgals_pnew_w[$acc_glae]['from'],0,$create_f);
															$end_f = $get_vgals_pnew_w[$acc_glae]['from'];
															$eceksg = "";
															if($end_c!="")
															{
																$eceksg = $end_c;
															}
															if($end_f!="")
															{
																if($end_c!="")
																{
																	$eceksg .= " : ".$end_f;
																}
																else
																{
																	$eceksg .= $end_f;
																}
															}
															if($get_vgals_pnew_w[$acc_glae]['title']!="")
															{
																if($end_c!="" || $end_f!="")
																{
																	$eceksg .= " : ".$get_vgals_pnew_w[$acc_glae]['title'];
																}
																else
																{
																	$eceksg .= $get_vgals_pnew_w[$acc_glae]['title'];
																}
															}
															
															if($get_select_galleries!="")
															{
																$get_select_galleries = array_unique($get_select_galleries);
																$yes_com_tag_pro_song =0;
																for($count_sel=0;$count_sel<=count($get_select_galleries);$count_sel++)
																{
																	if($get_select_galleries[$count_sel]==""){continue;}
																	else
																	{
																		if($get_select_galleries[$count_sel] == $get_vgals_pnew_w[$acc_glae]['id'])
																		{
																			$yes_com_tag_pro_song = 1;
											?>
																			<option value="<?php echo $get_vgals_pnew_w[$acc_glae]['id'];?>" selected><?php echo stripslashes($eceksg); ?></option>
											<?php
																		}
																	}
																}
																if($yes_com_tag_pro_song == 0)
																{													
											?>
																	<option value="<?php echo $get_vgals_pnew_w[$acc_glae]['id'];?>"><?php echo stripslashes($eceksg); ?></option>
											<?php
																}
															}
															else
															{
				?>
																<option value="<?php echo $get_vgals_pnew_w[$acc_glae]['id'];?>" ><?php echo stripslashes($eceksg); ?></option>
				<?php
															}
														}
													}
												}
											}
										}
												
												/* for($count_art_cr_g=0;$count_art_cr_g<count($get_gals_pnew);$count_art_cr_g++)
												{
													if($get_gals_pnew[$count_art_cr_g]!=""){
													
													$art_coms_g = '';
													if($get_media_cre_art_pro[$cra_i]['artist_id']!=0 && $get_media_cre_art_pro[$cra_i]['artist_id']!="")
													{
														$art_coms_g = 'artist~'.$get_media_cre_art_pro[$cra_i]['artist_id'];
													}
													elseif($get_media_cre_art_pro[$cra_i]['community_id']!=0 && $get_media_cre_art_pro[$cra_i]['community_id']!="")
													{
														$art_coms_g = 'community~'.$get_media_cre_art_pro[$cra_i]['community_id'];
													}

													$reg_meds = $Addcomevent->get_media_reg_info($get_gals_pnew[$count_art_cr_g],$art_coms_g);
													if($reg_meds!=NULL && $reg_meds!='')
													{
														if($reg_meds=='general_artist_gallery')
														{
															$sql_a = mysql_query("SELECT * FROM general_artist_gallery_list WHERE gallery_id='".$get_gals_pnew[$count_art_cr_g]."'");
															if(mysql_num_rows($sql_a)>0)
															{
																$ans_a = mysql_fetch_assoc($sql_a);
																$sql_gens = $Addcomevent->sel_general_aid($get_media_cre_art_pro[$cra_i]['artist_id']);
																
																if($get_select_galleries!="")
																{
																	//$get_sel_song =$Addcomevent->get_sel_songs($_GET['edit']);
																	//$exp_song_id = explode(",",$get_sel_song['songs']);
																	$yes_com_tag_pro_song =0;
																	for($count_sela=0;$count_sela<=count($get_select_galleries);$count_sela++)
																	{
																		if($get_select_galleries[$count_sela]==""){continue;}
																		else{
																			if($get_select_galleries[$count_sela] == $ans_a['gallery_id'])
																			{
																				$yes_com_tag_pro_song = 1;
																			?>															
																				<option value="<?php echo $ans_a['gallery_id'];?>" selected><?php echo substr($sql_gens['fname'] ." ". $sql_gens['lname'],0,20) ." : ".  $ans_a['gallery_title'];?></option>
																			<?php
																			}
																		}
																	}
																	if($yes_com_tag_pro_song == 0)
																	{
																		
																	?>
																		<option value="<?php echo $ans_a['gallery_id'];?>"><?php echo substr($sql_gens['fname'] ." ". $sql_gens['lname'],0,20) ." : ".  $ans_a['gallery_title'];?></option>
																	<?php
																	}
																}
																else{
																?>
																	<option value="<?php echo $ans_a['gallery_id'];?>"><?php echo substr($sql_gens['fname'] ." ". $sql_gens['lname'],0,20) ." : ".  $ans_a['gallery_title'];?></option>
																<?php
																	}
															}
														}
														if($reg_meds=='general_community_gallery')
														{
															$sql_a = mysql_query("SELECT * FROM general_community_gallery_list WHERE gallery_id='".$get_gals_pnew[$count_art_cr_g]."'");
															if(mysql_num_rows($sql_a)>0)
															{
																$ans_a = mysql_fetch_assoc($sql_a);
																$sql_gens = $Addcomevent->sel_general_cid($get_media_cre_art_pro[$cra_i]['community_id']);
																
																if($get_select_galleries!="")
																{
																	//$get_sel_song =$Addcomevent->get_sel_songs($_GET['edit']);
																	//$exp_song_id = explode(",",$get_sel_song['songs']);
																	$yes_com_tag_pro_song =0;
																	for($count_selc=0;$count_selc<=count($get_select_galleries);$count_selc++)
																	{
																		if($get_select_galleries[$count_selc]==""){continue;}
																		else{
																			if($get_select_galleries[$count_selc] == $ans_a['gallery_id'])
																			{
																				$yes_com_tag_pro_song = 1;
																			?>															
																				<option value="<?php echo $ans_a['gallery_id'];?>" selected><?php echo substr($sql_gens['fname'] ." ". $sql_gens['lname'],0,20) ." : ".  $ans_a['gallery_title'];?></option>
																			<?php
																			}
																		}
																	}
																	if($yes_com_tag_pro_song == 0)
																	{
																		
																	?>
																		<option value="<?php echo $ans_a['gallery_id'];?>"><?php echo substr($sql_gens['fname'] ." ". $sql_gens['lname'],0,20) ." : ".  $ans_a['gallery_title'];?></option>
																	<?php
																	}
																}
																else{
																?>
																	<option value="<?php echo $ans_a['gallery_id'];?>"><?php echo substr($sql_gens['fname'] ." ". $sql_gens['lname'],0,20) ." : ".  $ans_a['gallery_title'];?></option>
																<?php
																	}
															}
														}
													}
													else
													{
														$get_media_tag_art_pro_song = $Addcomevent->get_tag_media_info($get_gals_pnew[$count_art_cr_g]);
														
														if($get_select_galleries!="")
														{
															//$get_sel_song =$Addcomevent->get_sel_songs($_GET['edit']);
															//$exp_song_id = explode(",",$get_sel_song['songs']);
															$yes_com_tag_pro_song =0;
															for($count_sel=0;$count_sel<=count($get_select_galleries);$count_sel++)
															{
																if($get_select_galleries[$count_sel]==""){continue;}
																else{
																	if($get_select_galleries[$count_sel] == $get_media_tag_art_pro_song['id'])
																	{
																		$yes_com_tag_pro_song = 1;
																	?>
																		<option value="<?php echo $get_media_tag_art_pro_song['id'];?>" selected><?php echo substr($get_media_tag_art_pro_song['creator'],0,20) ." : ". substr($get_media_tag_art_pro_song['from'],0,20) ." : ".  $get_media_tag_art_pro_song['title'];?></option>
																	<?php
																	}
																}
															}
															if($yes_com_tag_pro_song == 0)
															{
																
															?>
																<option value="<?php echo $get_media_tag_art_pro_song['id'];?>"><?php echo substr($get_media_tag_art_pro_song['creator'],0,20) ." : ". substr($get_media_tag_art_pro_song['from'],0,20) ." : ".  $get_media_tag_art_pro_song['title'];?></option>
															<?php
															}
														}
														else{
														?>
															<option value="<?php echo $get_media_tag_art_pro_song['id'];?>"><?php echo substr($get_media_tag_art_pro_song['creator'],0,20) ." : ". substr($get_media_tag_art_pro_song['from'],0,20) ." : ".  $get_media_tag_art_pro_song['title'];?></option>
														<?php
															}
														}
													}
												} */
										
										//if(mysql_num_rows($get_community_gallery_id)<=0 && $res_gal==null && $media_gal=="" && count($get_media_cre_art_pro)<=0 && mysql_num_rows($get_media_tag_com_pro)<=0 && mysql_num_rows($get_media_tag_art_pro)<=0)
									?>
											</select>
									<?php
										}
										if(empty($get_vgals_pnew_w))
										{
										?>
											<a style="border-radius:5px; -webkit-border-radius:5px; -moz-border-radius:5px; -ms-border-radius:5px; -o-border-radius:5px; background-color: #000000; border: 0 none; color: #FFFFFF; font-size: 13px; margin-left: 5px; padding: 6px 8px; text-decoration: none;" href="add_media.php?adds=113">Add Gallery</a>
										<?php	
										}
										?>
                    			
                    		</div>
                            <div class="fieldCont">
                           	  <div title="Select media and profiles to display on your profile page." class="fieldTitle">Songs</div>
                                    <?php
											/* if(isset($res_song) && $res_song!=null)
											{
												for($song_reg=0;$song_reg<count($sel_song);$song_reg++)
												{
													if($res_song['audio_id']==$sel_song[$song_reg])
													{
													?>
														<option value="<?php echo $res_song['audio_id'];?>" selected ><?php echo substr($newres1['fname'].' '.$newres1['lname'],0,20) ." : ". $res_song['audio_name'];?></option>
													<?php	
														$song_reg_con=1;
														continue;
													}
												}
												if($song_reg_con==1)
												{
												}
												else
												{
												?>
													<option value="<?php echo $res_song['audio_id'];?>" ><?php echo substr($newres1['fname'].' '.$newres1['lname'],0,20) ." : ". $res_song['audio_name'];?></option>
												<?php 
												}
											} */
										$get_song_id = array();
										while($get_song_ids = mysql_fetch_assoc($get_community_song_id))
										{
											$get_song_id[] = $get_song_ids;
										}
											
										$get_media_tag_art_pro = $Addcomevent->get_artist_project_tag($_SESSION['login_email']);
										if(mysql_num_rows($get_media_tag_art_pro)>0)
										{
											$is = 0;
											while($art_pro_tag = mysql_fetch_assoc($get_media_tag_art_pro))
											{
												$get_songs = explode(",",$art_pro_tag['tagged_songs']);
												for($count_art_tag_song=0;$count_art_tag_song<count($get_songs);$count_art_tag_song++)
												{
													if($get_songs[$count_art_tag_song]!="")
													{
														$get_songs_new[$is] = $get_songs[$count_art_tag_song];
														$is = $is + 1;
													}
												}
											}
											if(!empty($get_songs_new))
											{
												$get_songs_n = array_unique($get_songs_new);
											}
										}
										
										$get_media_tag_com_pro = $Addcomevent->get_community_project_tag($_SESSION['login_email']);
										if(mysql_num_rows($get_media_tag_com_pro)>0)
										{
											$is_c = 0;
											while($com_pro_tag = mysql_fetch_assoc($get_media_tag_com_pro))
											{
												$get_com_songs = explode(",",$com_pro_tag['tagged_songs']);
												for($count_com_tag_song=0;$count_com_tag_song<count($get_com_songs);$count_com_tag_song++)
												{
													if($get_com_songs[$count_com_tag_song]!="")
													{
														$get_songs_new_c[$is_c] = $get_com_songs[$count_com_tag_song];
														$is_c = $is_c + 1;
													}
												}
											}
											if(!empty($get_songs_new_c))
											{
												$get_songs_n_c = array_unique($get_songs_new_c);
											}
										}
										
										$get_media_tag_art_eve = $Addcomevent->get_artist_event_tag($_SESSION['login_email']);
										if(mysql_num_rows($get_media_tag_art_eve)>0)
										{
											$is = 0;
											while($art_pro_tag = mysql_fetch_assoc($get_media_tag_art_eve))
											{
												$get_songs = explode(",",$art_pro_tag['tagged_songs']);
												for($count_art_tag_song=0;$count_art_tag_song<count($get_songs);$count_art_tag_song++)
												{
													if($get_songs[$count_art_tag_song]!="")
													{
														$get_songs_netww[$is] = $get_songs[$count_art_tag_song];
														$is = $is + 1;
													}
												}
											}
											if(!empty($get_songs_netww))
											{
												$get_songs_nuq = array_unique($get_songs_netww);
											}
										}
										
										$get_media_tag_com_eve = $Addcomevent->get_community_event_tag($_SESSION['login_email']);
										if(mysql_num_rows($get_media_tag_com_eve)>0)
										{
											$is_c = 0;
											while($com_pro_tag = mysql_fetch_assoc($get_media_tag_com_eve))
											{
												$get_com_songs = explode(",",$com_pro_tag['tagged_songs']);
												for($count_com_tag_song=0;$count_com_tag_song<count($get_com_songs);$count_com_tag_song++)
												{
													if($get_com_songs[$count_com_tag_song]!="")
													{
														$get_songs_ionew_c[$is_c] = $get_com_songs[$count_com_tag_song];
														$is_c = $is_c + 1;
													}
												}
											}
											if(!empty($get_songs_ionew_c))
											{
												$get_songs_n_caz = array_unique($get_songs_ionew_c);
											}
										}
											
										$get_media_cre_art_pro = $Addcomevent->get_project_create();
										if(!empty($get_media_cre_art_pro))
										{
											$is_p = 0;
											//while($art_pro_tag = mysql_fetch_assoc($get_media_cre_art_pro))
											for($cr_i=0;$cr_i<count($get_media_cre_art_pro);$cr_i++)
											{
												$get_songs = explode(",",$get_media_cre_art_pro[$cr_i]['tagged_songs']);
												for($count_art_cr_song=0;$count_art_cr_song<count($get_songs);$count_art_cr_song++)
												{
													if($get_songs[$count_art_cr_song]!="")
													{
														$get_songs_pn[$is_p] = $get_songs[$count_art_cr_song];
														$is_p = $is_p + 1;
													}
												}
											}
											if(!empty($get_songs_pn))
											{
												$get_songs_p_new = array_unique($get_songs_pn);
											}
										}
										
										
										if(!isset($get_songs_n))
										{
											$get_songs_n = array();
										}
										
										if(!isset($get_songs_nuq))
										{
											$get_songs_nuq = array();
										}
										
										if(!isset($get_songs_n_caz))
										{
											$get_songs_n_caz = array();
										}
										
										if(!isset($get_songs_n_c))
										{
											$get_songs_n_c = array();
										}
										
										if(!isset($get_songs_p_new))
										{
											$get_songs_p_new = array();
										}
										
										/*$find_creator_heis = $Addcomevent -> get_creator_heis();
										$get_genral_user_info = $Addcomevent -> selgeneral();
										$where_creator = array();
										if(!empty($find_creator_heis)){
											for($new_cre_med=0;$new_cre_med<=count($find_creator_heis);$new_cre_med++)
											{
												if($find_creator_heis[$new_cre_med]['general_user_id']!= $get_genral_user_info['general_user_id']){
													if($find_creator_heis[$new_cre_med]['media_type']==114){
													$where_creator[] = $find_creator_heis[$new_cre_med];}
												}
											}
										}
										
										$find_tag_heis = $Addcomevent -> get_tag_where_heis($_SESSION['login_email']);;
										$get_genral_user_info = $Addcomevent -> selgeneral();
										$where_tagged = array();
										if(!empty($find_tag_heis)){
											for($new_cre_med=0;$new_cre_med<=count($find_tag_heis);$new_cre_med++)
											{
												if($find_tag_heis[$new_cre_med]['general_user_id']!= $get_genral_user_info['general_user_id']){
													if($find_tag_heis[$new_cre_med]['media_type']==114){
													$where_tagged[] = $find_tag_heis[$new_cre_med];}
												}
											}
										}*/
										
										/*This function will display the user in which profile he is creator and display media where that profile was creator*/
										$new_medi = $Addcomevent->get_creator_heis();
										$new_creator_creator_media_array = array();
										if(!empty($new_medi))
										{
											for($count_cre_m=0;$count_cre_m<count($new_medi);$count_cre_m++)
											{
												if($new_medi[$count_cre_m]['media_type']==114){
													$new_creator_creator_media_array[] = $new_medi[$count_cre_m];
												}
											}
										}
										//var_dump($new_creator_creator_media_array);
										/*This function will display the user in which profile he is creator and display media where that profile was creator Ends here*/
										
										/*This function will display the user in which profile he is from and display media where that profile was creator*/
										$new_medi_from = $Addcomevent->get_from_heis();
										$new_from_from_media_array = array();
										if(!empty($new_medi_from))
										{
											for($count_cre_m=0;$count_cre_m<count($new_medi_from);$count_cre_m++)
											{
												if($new_medi_from[$count_cre_m]['media_type']==114){
												$new_from_from_media_array[] = $new_medi_from[$count_cre_m];
												}
											}
										}
										//var_dump($new_creator_creator_media_array);
										/*This function will display the user in which profile he is from and display media where that profile was creator Ends here*/
										
										
										/*This function will display the user in which profile he is from or creator and tagged and display media where that profile was creator*/
										$new_medi_tagged = $Addcomevent->get_the_user_tagin_project($_SESSION['login_email']);
										$new_tagged_from_media_array = array();
										if(!empty($new_medi_tagged))
										{
											for($count_cre_m=0;$count_cre_m<count($new_medi_tagged);$count_cre_m++)
											{
												if($new_medi_tagged[$count_cre_m]['media_type']==114){
												$new_tagged_from_media_array[] = $new_medi_tagged[$count_cre_m];
												}
											}
										}
										//var_dump($new_creator_creator_media_array);
										/*This function will display the user in which profile he is from or creator and tagged and display media where that profile was creator Ends here*/
										
										
										/*This function will display in which media the curent user was creator*/
										$get_where_creator_media = $Addcomevent->get_media_where_creator_or_from();
										$new_creator_media_array = array();
										if(!empty($get_where_creator_media))
										{
											for($count_tag_m=0;$count_tag_m<count($get_where_creator_media);$count_tag_m++)
											{
												if($get_where_creator_media[$count_tag_m]['media_type']==114){
													$new_creator_media_array[] = $get_where_creator_media[$count_tag_m];
												}
											}
										}
										/*This function will display in which media the curent user was creator ends here*/
										
										$get_songs_pnsew_w = array_merge($get_songs_n,$get_songs_n_c,$get_songs_p_new,$get_songs_nuq,$get_songs_n_caz);
										
										$get_songs_pnew_ct = array_unique($get_songs_pnsew_w);
										$get_songs_final_ct = array();
										
										for($count_art_cr_song=0;$count_art_cr_song<count($get_songs_pnew_ct);$count_art_cr_song++)
										{
											if($get_songs_pnew_ct[$count_art_cr_song]!="")
											{
												$get_media_tag_art_pro_song = $newgeneral->get_tag_media_info($get_songs_pnew_ct[$count_art_cr_song]);
												$get_songs_final_ct[] = $get_media_tag_art_pro_song;
											}
										}
										
										$acc_medss = array();
										for($acc_vis=0;$acc_vis<count($acc_media);$acc_vis++)
										{
											if($acc_media[$acc_vis]!="")
											{
												$sels_tagged = $newgeneral->get_community_media_type_tagged_by_other_user($acc_media[$acc_vis]);
												if(!empty($sels_tagged))
												{
													
													$acc_medss[] = $sels_tagged;
												}
											}
										}
										
										$get_songs_final_ct = array();
										$type = '114';
										$cre_frm_med = array();
										$cre_frm_med = $Addcomevent->get_media_create($type);
										
										$get_songs_pnew_w = array_merge($acc_medss,$get_song_id,$get_songs_final_ct,$cre_frm_med,$new_creator_creator_media_array,$new_from_from_media_array,$new_tagged_from_media_array,$new_creator_media_array);
										
										for($acc_song1=0;$acc_song1<count($get_songs_pnew_w);$acc_song1++)
										{
											if($get_songs_pnew_w[$acc_song1]['id']!="" && $get_songs_pnew_w[$acc_song1]['id']!=0)
											{
												$get_artist_song1 = $Addcomevent->get_community_song($get_songs_pnew_w[$acc_song1]['id']);
												$getsong1 = mysql_fetch_assoc($get_artist_song1);
												$ids = $get_songs_pnew_w[$acc_song1]['id'];
												$get_songs_pnew_w[$acc_song1]['track'] = $getsong1['track'];
											}
										}
										
										$sort = array();
										foreach($get_songs_pnew_w as $k=>$v) {
										$end_c1 = $v['creator'];
											//$create_c = strpos($v['creator'],'(');
											//$end_c1 = substr($v['creator'],0,$create_c);
											$end_c = trim($end_c1);
											$end_f1 = $v['from'];
											//$create_f = strpos($v['from'],'(');
											//$end_f1 = substr($v['from'],0,$create_f);
											$end_f = trim($end_f1);
											
											$sort['creator'][$k] = strtolower($end_c);
											$sort['from'][$k] = strtolower($end_f);
											$sort['track'][$k] = $v['track'];
											$sort['title'][$k] = strtolower($v['title']);
										}
										//var_dump($sort);
										if(!empty($sort))
										{
											array_multisort($sort['creator'], SORT_ASC, $sort['from'], SORT_ASC, $sort['track'], SORT_ASC,$sort['title'], SORT_ASC,$get_songs_pnew_w);
										}
										//$get_songs_pnew = array_unique($get_songs_pnew_w);
										//var_dump($get_songs_pnew);
										
										if(!empty($get_songs_pnew_w))
										{
										?>
												<select title="Select media and profiles to display on your profile page." name="tagged_songs[]" id="tagged_song[]" size="5" multiple="multiple"  class="list">
										<?php
										
										$dummy_s = array();
										$get_all_del_id = $Addcomevent->get_all_del_media_id();
										$deleted_id = explode(",",$get_all_del_id);
										for($count_del=0;$count_del<=count($get_songs_pnew_w);$count_del++)
										{
											if($get_songs_pnew_w[$count_del]==""){continue;}
											else{
												for($count_deleted=0;$count_deleted<=count($deleted_id);$count_deleted++)
												{
													if($deleted_id[$count_deleted]==""){continue;}
													else{
														if($deleted_id[$count_deleted] == $get_songs_pnew_w[$count_del]['id'])
														{
															$get_songs_pnew_w[$count_del]="";
														}
													}
												}
											}
										}
										for($acc_song=0;$acc_song<count($get_songs_pnew_w);$acc_song++)
										{
											if($get_songs_pnew_w[$acc_song]['delete_status']==0)
											{
												if(isset($get_songs_pnew_w[$acc_song]['id']))
												{
													if(in_array($get_songs_pnew_w[$acc_song]['id'],$dummy_s))
													{
														
													}
													else
													{
														$dummy_s[] = $get_songs_pnew_w[$acc_song]['id'];
														if($get_songs_pnew_w[$acc_song]['id']!="" && $get_songs_pnew_w[$acc_song]['media_type']==114)
														{
															//$create_c = strpos($get_songs_pnew_w[$acc_song]['creator'],'(');
															//$end_c = substr($get_songs_pnew_w[$acc_song]['creator'],0,$create_c);
															$end_c = $get_songs_pnew_w[$acc_song]['creator'];
															$end_f = $get_songs_pnew_w[$acc_song]['from'];
															//$create_f = strpos($get_songs_pnew_w[$acc_song]['from'],'(');
															//$end_f = substr($get_songs_pnew_w[$acc_song]['from'],0,$create_f);
															
															$eceks = "";
															if($end_c!="")
															{
																$eceks = $end_c;
															}
															if($end_f!="")
															{
																if($end_c!="")
																{
																	$eceks .= " : ".$end_f;
																}
																else
																{
																	$eceks .= $end_f;
																}
															}
															if($get_songs_pnew_w[$acc_song]['track']!="")
															{
																if($end_c!="" || $end_f!="")
																{
																	$eceks .= " : ".$get_songs_pnew_w[$acc_song]['track'];
																}
																else
																{
																	$eceks .= $get_songs_pnew_w[$acc_song]['track'];
																}
															}
															if($get_songs_pnew_w[$acc_song]['title']!="")
															{
																if($end_c!="" || $end_f!="" || $get_songs_pnew_w[$acc_song]['track']!="")
																{
																	$eceks .= " : ".$get_songs_pnew_w[$acc_song]['title'];
																}
																else
																{
																	$eceks .= $get_songs_pnew_w[$acc_song]['title'];
																}
															}
															
															if($get_select_songs!="")
															{
																$get_select_songs = array_unique($get_select_songs);
																$yes_com_tag_pro_song =0;
																for($count_sel=0;$count_sel<=count($get_select_songs);$count_sel++)
																{
																	if($get_select_songs[$count_sel]==""){continue;}
																	else
																	{
																		if($get_select_songs[$count_sel] == $get_songs_pnew_w[$acc_song]['id'])
																		{
																			$yes_com_tag_pro_song = 1;
											?>
																			<option value="<?php echo $get_songs_pnew_w[$acc_song]['id'];?>" selected><?php echo stripslashes($eceks); ?></option>
											<?php
																		}
																	}
																}
																if($yes_com_tag_pro_song == 0)
																{													
											?>
																	<option value="<?php echo $get_songs_pnew_w[$acc_song]['id'];?>"><?php echo stripslashes($eceks); ?></option>
											<?php
																}
															}
															else
															{
				?>
																<option value="<?php echo $get_songs_pnew_w[$acc_song]['id'];?>" ><?php echo stripslashes($eceks); ?></option>
				<?php
															}
														}
													}
												}
											}
										}
												/* for($count_art_cr_song=0;$count_art_cr_song<count($get_songs_pnew);$count_art_cr_song++)
												{
													if($get_songs_pnew[$count_art_cr_song]!=""){
													
													$art_coms_a = '';
														if($get_media_cre_art_pro[$cr_i]['artist_id']!=0 && $get_media_cre_art_pro[$cr_i]['artist_id']!="")
														{
															$art_coms_a = 'artist~'.$get_media_cre_art_pro[$cr_i]['artist_id'];
														}
														elseif($get_media_cre_art_pro[$cr_i]['community_id']!=0 && $get_media_cre_art_pro[$cr_i]['community_id']!="")
														{
															$art_coms_a = 'community~'.$get_media_cre_art_pro[$cr_i]['community_id'];
														}
														
														$reg_meds = $Addcomevent->get_media_reg_info($get_songs_pnew[$count_art_cr_song],$art_coms_a);
														if($reg_meds!=NULL && $reg_meds!='')
														{
															if($reg_meds=='general_artist_audio')
															{
																$sql_a = mysql_query("SELECT * FROM general_artist_audio WHERE audio_id='".$get_songs_pnew[$count_art_cr_song]."'");
																if(mysql_num_rows($sql_a)>0)
																{
																	$ans_a = mysql_fetch_assoc($sql_a);
																	$sql_gens = $Addcomevent->sel_general_aid($get_media_cre_art_pro[$cr_i]['artist_id']);
																	
																	if($get_select_songs!="")
																	{
																		//$get_sel_song =$Addcomevent->get_sel_songs($_GET['edit']);
																		//$exp_song_id = explode(",",$get_sel_song['songs']);
																		$yes_com_tag_pro_song =0;
																		for($count_sela=0;$count_sela<=count($get_select_songs);$count_sela++)
																		{
																			if($get_select_songs[$count_sela]==""){continue;}
																			else{
																				if($get_select_songs[$count_sela] == $ans_a['audio_id'])
																				{
																					$yes_com_tag_pro_song = 1;
																				?>															
																					<option value="<?php echo $ans_a['audio_id'];?>" selected><?php echo substr($sql_gens['fname'] ." ". $sql_gens['lname'],0,20) ." : ".  $ans_a['audio_name'];?></option>
																				<?php
																				}
																			}
																		}
																		if($yes_com_tag_pro_song == 0)
																		{
																			
																		?>
																			<option value="<?php echo $ans_a['audio_id'];?>"><?php echo substr($sql_gens['fname'] ." ". $sql_gens['lname'],0,20) ." : ".  $ans_a['audio_name'];?></option>
																		<?php
																		}
																	}
																	else{
																	?>
																		<option value="<?php echo $ans_a['audio_id'];?>"><?php echo substr($sql_gens['fname'] ." ". $sql_gens['lname'],0,20) ." : ".  $ans_a['audio_name'];?></option>
																	<?php
																		}
																}
															}
															if($reg_meds=='general_community_audio')
															{
																$sql_a = mysql_query("SELECT * FROM general_community_audio WHERE audio_id='".$get_songs_pnew[$count_art_cr_song]."'");
																if(mysql_num_rows($sql_a)>0)
																{
																	$ans_a = mysql_fetch_assoc($sql_a);
																	$sql_gens = $Addcomevent->sel_general_cid($get_media_cre_art_pro[$cr_i]['community_id']);
																	
																	if($get_select_songs!="")
																	{
																		//$get_sel_song =$Addcomevent->get_sel_songs($_GET['edit']);
																		//$exp_song_id = explode(",",$get_sel_song['songs']);
																		$yes_com_tag_pro_song =0;
																		for($count_selc=0;$count_selc<=count($get_select_songs);$count_selc++)
																		{
																			if($get_select_songs[$count_selc]==""){continue;}
																			else{
																				if($get_select_songs[$count_selc] == $ans_a['audio_id'])
																				{
																					$yes_com_tag_pro_song = 1;
																				?>															
																					<option value="<?php echo $ans_a['audio_id'];?>" selected><?php echo substr($sql_gens['fname'] ." ". $sql_gens['lname'],0,20) ." : ".  $ans_a['audio_name'];?></option>
																				<?php
																				}
																			}
																		}
																		if($yes_com_tag_pro_song == 0)
																		{
																			
																		?>
																			<option value="<?php echo $ans_a['audio_id'];?>"><?php echo substr($sql_gens['fname'] ." ". $sql_gens['lname'],0,20) ." : ".  $ans_a['audio_name'];?></option>
																		<?php
																		}
																	}
																	else{
																	?>
																		<option value="<?php echo $ans_a['audio_id'];?>"><?php echo substr($sql_gens['fname'] ." ". $sql_gens['lname'],0,20) ." : ".  $ans_a['audio_name'];?></option>
																	<?php
																		}
																}
															}
														}
													else
													{
														$get_media_tag_art_pro_song = $Addcomevent->get_tag_media_info($get_songs_pnew[$count_art_cr_song]);
														if($sel_song!="")
														{
															//$get_sel_song =$Addcomevent->get_sel_songs($_GET['edit']);
															//$exp_song_id = explode(",",$get_sel_song['songs']);
															$yes_com_tag_pro_song =0;
															for($count_sel=0;$count_sel<=count($sel_song);$count_sel++)
															{
																if($sel_song[$count_sel]==""){continue;}
																else{
																	if($sel_song[$count_sel] == $get_media_tag_art_pro_song['id'])
																	{
																		$yes_com_tag_pro_song = 1;
																	?>
																		<option value="<?php echo $get_media_tag_art_pro_song['id'];?>" selected><?php echo substr($get_media_tag_art_pro_song['creator'],0,20) ." : ". substr($get_media_tag_art_pro_song['from'],0,20) ." : ".  $get_media_tag_art_pro_song['title'];?></option>
																	<?php
																	}
																}
															}
															if($yes_com_tag_pro_song == 0)
															{
																
															?>
																<option value="<?php echo $get_media_tag_art_pro_song['id'];?>"><?php echo substr($get_media_tag_art_pro_song['creator'],0,20) ." : ". substr($get_media_tag_art_pro_song['from'],0,20) ." : ".  $get_media_tag_art_pro_song['title'];?></option>
															<?php
															}
														}
														else{
														?>
															<option value="<?php echo $get_media_tag_art_pro_song['id'];?>"><?php echo substr($get_media_tag_art_pro_song['creator'],0,20) ." : ". substr($get_media_tag_art_pro_song['from'],0,20) ." : ".  $get_media_tag_art_pro_song['title'];?></option>
														<?php
															}
														}
													}
												} */
										
										//if(mysql_num_rows($get_community_song_id)<=0 && $res_song==null && $media_song=="" && count($get_media_cre_art_pro)<=0 && mysql_num_rows($get_media_tag_com_pro)<=0 && mysql_num_rows($get_media_tag_art_pro)<=0)
									?>
											</select>
									<?php
										}
										
										if(empty($get_songs_pnew_w))
										{
										?>
											<a style="border-radius:5px; -webkit-border-radius:5px; -moz-border-radius:5px; -ms-border-radius:5px; -o-border-radius:5px; background-color: #000000; border: 0 none; color: #FFFFFF; font-size: 13px; margin-left: 5px; padding: 6px 8px; text-decoration: none;" href="add_media.php?adds=114">Add Song</a>
										<?php	
										}
										?>
                    			
                    		</div>
                            <div class="fieldCont">
                           	  <div title="Select media and profiles to display on your profile page." class="fieldTitle">Videos</div>
                                   	<?php
											/* if(isset($res_video) && $res_video!=null)
											{
												for($video_reg=0;$video_reg<=count($sel_video);$video_reg++)
												{
													if($res_video['video_id']==$sel_video[$video_reg])
													{
													?>
														<option value="<?php echo $res_video['video_id'];?>" selected ><?php echo substr($newres1['fname'].' '.$newres1['lname'],0,20) ." : ". $res_video['video_name'];?></option>
													<?php	
														$video_reg_con=1;
														continue;
													}
												}
												if($video_reg_con==1)
												{
												}
												else
												{
												?>
													<option value="<?php echo $res_video['video_id'];?>" ><?php echo substr($newres1['fname'].' '.$newres1['lname'],0,20) ." : ". $res_video['video_name'];?></option>
												<?php		
												}
											} */
										
										$get_video_id = array();
										while($get_video_ids = mysql_fetch_assoc($get_community_video_id))
										{
											$get_video_id[] = $get_video_ids;
										}
										
										$get_media_tag_art_pro = $Addcomevent->get_artist_project_tag($_SESSION['login_email']);
										if(mysql_num_rows($get_media_tag_art_pro)>0)
										{
											$is_v = 0;
											while($art_pro_tag = mysql_fetch_assoc($get_media_tag_art_pro))
											{
												$get_videos = explode(",",$art_pro_tag['tagged_videos']);
												for($count_art_tag_video=0;$count_art_tag_video<count($get_videos);$count_art_tag_video++)
												{
													if($get_videos[$count_art_tag_video]!="")
													{
														$get_videos_new[$is_v] = $get_videos[$count_art_tag_video];
														$is_v = $is_v + 1;
													}
												}
											}
											if(!empty($get_videos_new))
											{
												$get_videos_n = array_unique($get_videos_new);
											}
										}
											
										$get_media_tag_com_pro = $Addcomevent->get_community_project_tag($_SESSION['login_email']);
										if(mysql_num_rows($get_media_tag_com_pro)>0)
										{
											$is_vc = 0;
											while($com_pro_tag = mysql_fetch_assoc($get_media_tag_com_pro))
											{
												$get_com_videos = explode(",",$com_pro_tag['tagged_videos']);
												for($count_com_tag_video=0;$count_com_tag_video<count($get_com_videos);$count_com_tag_video++)
												{
													if($get_com_videos[$count_com_tag_video]!="")
													{
														$get_videos_c_new[$is_vc] = $get_com_videos[$count_com_tag_video];
														$is_vc = $is_vc + 1;
													}
												}
											}
											if(!empty($get_videos_c_new))
											{
												$get_videos_c_n = array_unique($get_videos_c_new);
											}
										}
										
										$get_media_tag_art_eve = $Addcomevent->get_artist_event_tag($_SESSION['login_email']);
										if(mysql_num_rows($get_media_tag_art_eve)>0)
										{
											$is_v = 0;
											while($art_pro_tag = mysql_fetch_assoc($get_media_tag_art_eve))
											{
												$get_videos = explode(",",$art_pro_tag['tagged_videos']);
												for($count_art_tag_video=0;$count_art_tag_video<count($get_videos);$count_art_tag_video++)
												{
													if($get_videos[$count_art_tag_video]!="")
													{
														$get_videos_nmnew[$is_v] = $get_videos[$count_art_tag_video];
														$is_v = $is_v + 1;
													}
												}
											}
											if(!empty($get_videos_nmnew))
											{
												$get_videos_rxn = array_unique($get_videos_nmnew);
											}
										}
											
										$get_media_tag_com_eve = $Addcomevent->get_community_event_tag($_SESSION['login_email']);
										if(mysql_num_rows($get_media_tag_com_eve)>0)
										{
											$is_vc = 0;
											while($com_pro_tag = mysql_fetch_assoc($get_media_tag_com_eve))
											{
												$get_com_videos = explode(",",$com_pro_tag['tagged_videos']);
												for($count_com_tag_video=0;$count_com_tag_video<count($get_com_videos);$count_com_tag_video++)
												{
													if($get_com_videos[$count_com_tag_video]!="")
													{
														$get_videknos_c_new[$is_vc] = $get_com_videos[$count_com_tag_video];
														$is_vc = $is_vc + 1;
													}
												}
											}
											if(!empty($get_videknos_c_new))
											{
												$get_videovbs_c_n = array_unique($get_videknos_c_new);
											}
										}
										
										$get_media_cre_art_pro = $Addcomevent->get_project_create();
										if(!empty($get_media_cre_art_pro))
										{
											$is_v_c = 0;
											//while($art_pro_tag = mysql_fetch_assoc($get_media_cre_art_pro))
											for($cr_ei=0;$cr_ei<count($get_media_cre_art_pro);$cr_ei++)
											{
												$get_videos = explode(",",$get_media_cre_art_pro[$cr_ei]['tagged_videos']);

												for($count_art_cr_video=0;$count_art_cr_video<count($get_videos);$count_art_cr_video++)
												{
													if($get_videos[$count_art_cr_video]!="")
													{
														$get_videos__new[$is_v_c] = $get_videos[$count_art_cr_video];
														$is_v_c = $is_v_c + 1;
													}
												}
											}
											if(!empty($get_gal_new))
											{
												$get_videos__n = array_unique($get_videos__new);
											}
										}
										
										
										if(!isset($get_videos_n))
										{
											$get_videos_n = array();
										}
										
										if(!isset($get_videos_rxn))
										{
											$get_videos_rxn = array();
										}
										
										if(!isset($get_videovbs_c_n))
										{
											$get_videovbs_c_n = array();
										}
										
										if(!isset($get_videos_c_n))
										{
											$get_videos_c_n = array();
										}
										
										if(!isset($get_videos__n))
										{
											$get_videos__n = array();
										}
										
										/*$find_creator_heis = $Addcomevent -> get_creator_heis();
										$get_genral_user_info = $Addcomevent -> selgeneral();
										$where_creator = array();
										if(!empty($find_creator_heis)){
											for($new_cre_med=0;$new_cre_med<=count($find_creator_heis);$new_cre_med++)
											{
												if($find_creator_heis[$new_cre_med]['general_user_id']!= $get_genral_user_info['general_user_id']){
													if($find_creator_heis[$new_cre_med]['media_type']==115){
													$where_creator[] = $find_creator_heis[$new_cre_med];}
												}
											}
										}
										
										$find_tag_heis = $Addcomevent -> get_tag_where_heis($_SESSION['login_email']);;
										$get_genral_user_info = $Addcomevent -> selgeneral();
										$where_tagged = array();
										if(!empty($find_tag_heis)){
											for($new_cre_med=0;$new_cre_med<=count($find_tag_heis);$new_cre_med++)
											{
												if($find_tag_heis[$new_cre_med]['general_user_id']!= $get_genral_user_info['general_user_id']){
													if($find_tag_heis[$new_cre_med]['media_type']==115){
													$where_tagged[] = $find_tag_heis[$new_cre_med];}
												}
											}
										}*/
										
										/*This function will display the user in which profile he is creator and display media where that profile was creator*/
										$new_medi = $Addcomevent->get_creator_heis();
										$new_creator_creator_media_array = array();
										if(!empty($new_medi))
										{
											for($count_cre_m=0;$count_cre_m<count($new_medi);$count_cre_m++)
											{
												if($new_medi[$count_cre_m]['media_type']==115){
													$new_creator_creator_media_array[] = $new_medi[$count_cre_m];
												}
											}
										}
										//var_dump($new_creator_creator_media_array);
										/*This function will display the user in which profile he is creator and display media where that profile was creator Ends here*/
										
										/*This function will display the user in which profile he is from and display media where that profile was creator*/
										$new_medi_from = $Addcomevent->get_from_heis();
										$new_from_from_media_array = array();
										if(!empty($new_medi_from))
										{
											for($count_cre_m=0;$count_cre_m<count($new_medi_from);$count_cre_m++)
											{
												if($new_medi_from[$count_cre_m]['media_type']==115){
												$new_from_from_media_array[] = $new_medi_from[$count_cre_m];
												}
											}
										}
										//var_dump($new_creator_creator_media_array);
										/*This function will display the user in which profile he is from and display media where that profile was creator Ends here*/
										
										
										/*This function will display the user in which profile he is from or creator and tagged and display media where that profile was creator*/
										$new_medi_tagged = $Addcomevent->get_the_user_tagin_project($_SESSION['login_email']);
										$new_tagged_from_media_array = array();
										if(!empty($new_medi_tagged))
										{
											for($count_cre_m=0;$count_cre_m<count($new_medi_tagged);$count_cre_m++)
											{
												if($new_medi_tagged[$count_cre_m]['media_type']==115){
												$new_tagged_from_media_array[] = $new_medi_tagged[$count_cre_m];
												}
											}
										}
										//var_dump($new_creator_creator_media_array);
										/*This function will display the user in which profile he is from or creator and tagged and display media where that profile was creator Ends here*/
										
										
										/*This function will display in which media the curent user was creator*/
										$get_where_creator_media = $Addcomevent->get_media_where_creator_or_from();
										$new_creator_media_array = array();
										if(!empty($get_where_creator_media))
										{
											for($count_tag_m=0;$count_tag_m<count($get_where_creator_media);$count_tag_m++)
											{
												if($get_where_creator_media[$count_tag_m]['media_type']==115){
													$new_creator_media_array[] = $get_where_creator_media[$count_tag_m];
												}
											}
										}
										/*This function will display in which media the curent user was creator ends here*/
										
										$get_videos_n_n = array_merge($get_videos_n,$get_videos_c_n,$get_videos__n,$get_videos_rxn,$get_videovbs_c_n);
										
										$get_vids_pnew_ct = array_unique($get_videos_n_n);
										$get_vids_final_ct = array();
										
										for($count_art_cr_video=0;$count_art_cr_video<count($get_vids_pnew_ct);$count_art_cr_video++)
										{
											if($get_vids_pnew_ct[$count_art_cr_video]!="")
											{
												$get_media_tag_art_pro_vi = $newgeneral->get_tag_media_info($get_vids_pnew_ct[$count_art_cr_video]);
												$get_vids_final_ct[] = $get_media_tag_art_pro_vi;
											}
										}
										
										$acc_medsv = array();
										for($acc_vis=0;$acc_vis<count($acc_media);$acc_vis++)
										{
											if($acc_media[$acc_vis]!="")
											{
												$sel_tagged = $newgeneral->get_community_media_type_tagged_by_other_user($acc_media[$acc_vis]);
												if(!empty($sel_tagged))
												{
													$acc_medsv[] = $sel_tagged;
												}
											}
										}
										$get_vids_final_ct = array();
										$type = '115';
										$cre_frm_med = array();
										$cre_frm_med = $Addcomevent->get_media_create($type);
										
										$get_vids_pnew_w = array_merge($acc_medsv,$get_video_id,$get_vids_final_ct,$cre_frm_med,$new_creator_creator_media_array,$new_from_from_media_array,$new_tagged_from_media_array,$new_creator_media_array);
										
										$sort = array();
										foreach($get_vids_pnew_w as $k=>$v) {
										$end_c1 = $v['creator'];
											//$create_c = strpos($v['creator'],'(');
											//$end_c1 = substr($v['creator'],0,$create_c);
											$end_c = trim($end_c1);
											$end_f1 = $v['from'];
											//$create_f = strpos($v['from'],'(');
											//$end_f1 = substr($v['from'],0,$create_f);
											$end_f = trim($end_f1);
											
											$sort['creator'][$k] = strtolower($end_c);
											$sort['from'][$k] = strtolower($end_f);
											//$sort['track'][$k] = $v['track'];
											$sort['title'][$k] = strtolower($v['title']);
										}
										//var_dump($sort);
										if(!empty($sort))
										{
											array_multisort($sort['creator'], SORT_ASC, $sort['from'], SORT_ASC, $sort['title'], SORT_ASC,$get_vids_pnew_w);
										}
										
										if(!empty($get_vids_pnew_w))
										{
										?>
											<select title="Select media and profiles to display on your profile page." name="tagged_videos[]" size="5" multiple="multiple" id="tagged_videos[]"  class="list">
										<?php
										
										$dummy_v = array();
										$get_all_del_id = $Addcomevent->get_all_del_media_id();
										$deleted_id = explode(",",$get_all_del_id);
										for($count_del=0;$count_del<=count($get_vids_pnew_w);$count_del++)
										{
											if($get_vids_pnew_w[$count_del]==""){continue;}
											else{
												for($count_deleted=0;$count_deleted<=count($deleted_id);$count_deleted++)
												{
													if($deleted_id[$count_deleted]==""){continue;}
													else{
														if($deleted_id[$count_deleted] == $get_vids_pnew_w[$count_del]['id'])
														{
															$get_vids_pnew_w[$count_del]="";
														}
													}
												}
											}
										}
										for($acc_vide=0;$acc_vide<count($get_vids_pnew_w);$acc_vide++)
										{
											if($get_vids_pnew_w[$acc_vide]['delete_status']==0)
											{
												if(isset($get_vids_pnew_w[$acc_vide]['id']))
												{
													if(in_array($get_vids_pnew_w[$acc_vide]['id'],$dummy_v))
													{
														
													}
													else
													{
														$dummy_v[] = $get_vids_pnew_w[$acc_vide]['id'];
														if($get_vids_pnew_w[$acc_vide]['id']!="" && $get_vids_pnew_w[$acc_vide]['media_type']==115)
														{
															//$create_c = strpos($get_vids_pnew_w[$acc_vide]['creator'],'(');
															//$end_c = substr($get_vids_pnew_w[$acc_vide]['creator'],0,$create_c);
															$end_c = $get_vids_pnew_w[$acc_vide]['creator'];
															//$create_f = strpos($get_vids_pnew_w[$acc_vide]['from'],'(');
															//$end_f = substr($get_vids_pnew_w[$acc_vide]['from'],0,$create_f);
															$end_f = $get_vids_pnew_w[$acc_vide]['from'];
															$eceksv = "";
															if($end_c!="")
															{
																$eceksv = $end_c;
															}
															if($end_f!="")
															{
																if($end_c!="")
																{
																	$eceksv .= " : ".$end_f;
																}
																else
																{
																	$eceksv .= $end_f;
																}
															}
															if($get_vids_pnew_w[$acc_vide]['title']!="")
															{
																if($end_c!="" || $end_f!="")
																{
																	$eceksv .= " : ".$get_vids_pnew_w[$acc_vide]['title'];
																}
																else
																{
																	$eceksv .= $get_vids_pnew_w[$acc_vide]['title'];
																}
															}
															
															if($get_select_videos!="")
															{
																$get_select_videos = array_unique($get_select_videos);
																$yes_com_tag_pro_song =0;
																for($count_sel=0;$count_sel<=count($get_select_videos);$count_sel++)
																{
																	if($get_select_videos[$count_sel]==""){continue;}
																	else
																	{
																		if($get_select_videos[$count_sel] == $get_vids_pnew_w[$acc_vide]['id'])
																		{
																			$yes_com_tag_pro_song = 1;
											?>
																			<option value="<?php echo $get_vids_pnew_w[$acc_vide]['id'];?>" selected><?php echo stripslashes($eceksv); ?></option>
											<?php
																		}
																	}
																}
																if($yes_com_tag_pro_song == 0)
																{													
											?>
																	<option value="<?php echo $get_vids_pnew_w[$acc_vide]['id'];?>"><?php echo stripslashes($eceksv); ?></option>
											<?php
																}
															}
															else
															{
				?>
																<option value="<?php echo $get_vids_pnew_w[$acc_vide]['id'];?>" ><?php echo stripslashes($eceksv); ?></option>
				<?php
															}
														}
													}
												}
											}
										}

												/* for($count_art_cr_video=0;$count_art_cr_video<count($get_videos_n_new);$count_art_cr_video++)
												{
													if($get_videos_n_new[$count_art_cr_video]!=""){
													
													$art_coms_v = '';
													if($get_media_cre_art_pro[$cr_ei]['artist_id']!=0 && $get_media_cre_art_pro[$cr_ei]['artist_id']!="")
													{
														$art_coms_v = 'artist~'.$get_media_cre_art_pro[$cr_ei]['artist_id'];
													}
													elseif($get_media_cre_art_pro[$cr_ei]['community_id']!=0 && $get_media_cre_art_pro[$cr_ei]['community_id']!="")
													{
														$art_coms_v = 'community~'.$get_media_cre_art_pro[$cr_ei]['community_id'];
													}
													$reg_meds = $Addcomevent->get_media_reg_info($get_videos_n_new[$count_art_cr_video],$art_coms_v);
													
													if($reg_meds!=NULL && $reg_meds!='')
													{
														if($reg_meds=='general_artist_video')
														{
															$sql_a = mysql_query("SELECT * FROM general_artist_video WHERE video_id='".$get_videos_n_new[$count_art_cr_video]."'");
															if(mysql_num_rows($sql_a)>0)
															{
																$ans_a = mysql_fetch_assoc($sql_a);
																$sql_gens = $Addcomevent->sel_general_aid($get_media_cre_art_pro[$cr_ei]['artist_id']);
																
																if($get_select_videos!="")
																{
																	//$get_sel_song =$Addcomevent->get_sel_songs($_GET['edit']);
																	//$exp_song_id = explode(",",$get_sel_song['songs']);
																	$yes_com_tag_pro_song =0;
																	for($count_selv=0;$count_selv<=count($get_select_videos);$count_selv++)
																	{
																		if($get_select_videos[$count_selv]==""){continue;}
																		else{
																			if($get_select_videos[$count_selv] == $ans_a['video_id'])
																			{
																				$yes_com_tag_pro_song = 1;
																			?>															
																				<option value="<?php echo $ans_a['video_id'];?>" selected><?php echo substr($sql_gens['fname'] ." ". $sql_gens['lname'],0,20) ." : ".  $ans_a['video_name'];?></option>
																			<?php
																			}
																		}
																	}
																	if($yes_com_tag_pro_song == 0)
																	{
																		
																	?>
																		<option value="<?php echo $ans_a['video_id'];?>"><?php echo substr($sql_gens['fname'] ." ". $sql_gens['lname'],0,20) ." : ".  $ans_a['video_name'];?></option>
																	<?php
																	}
																}
																else{
																?>
																	<option value="<?php echo $ans_a['video_id'];?>"><?php echo substr($sql_gens['fname'] ." ". $sql_gens['lname'],0,20) ." : ".  $ans_a['video_name'];?></option>
																<?php
																	}
															}
														}
														if($reg_meds=='general_community_video')
														{
															$sql_a = mysql_query("SELECT * FROM general_community_video WHERE video_id='".$get_videos_n_new[$count_art_cr_video]."'");
															if(mysql_num_rows($sql_a)>0)
															{
																$ans_a = mysql_fetch_assoc($sql_a);
																$sql_gens = $Addcomevent->sel_general_cid($get_media_cre_art_pro[$cr_ei]['community_id']);
																
																if($get_select_videos!="")
																{
																	//$get_sel_song =$Addcomevent->get_sel_songs($_GET['edit']);
																	//$exp_song_id = explode(",",$get_sel_song['songs']);
																	$yes_com_tag_pro_song =0;
																	for($count_selvc=0;$count_selvc<=count($get_select_videos);$count_selvc++)
																	{
																		if($get_select_videos[$count_selvc]==""){continue;}
																		else{
																			if($get_select_videos[$count_selvc] == $ans_a['video_id'])
																			{
																				$yes_com_tag_pro_song = 1;
																			?>															
																				<option value="<?php echo $ans_a['video_id'];?>" selected><?php echo substr($sql_gens['fname'] ." ". $sql_gens['lname'],0,20) ." : ".  $ans_a['video_name'];?></option>
																			<?php
																			}
																		}
																	}
																	if($yes_com_tag_pro_song == 0)
																	{
																		
																	?>
																		<option value="<?php echo $ans_a['video_id'];?>"><?php echo substr($sql_gens['fname'] ." ". $sql_gens['lname'],0,20) ." : ".  $ans_a['video_name'];?></option>
																	<?php
																	}
																}
																else{
																?>
																	<option value="<?php echo $ans_a['video_id'];?>"><?php echo substr($sql_gens['fname'] ." ". $sql_gens['lname'],0,20) ." : ".  $ans_a['video_name'];?></option>
																<?php
																	}
															}
														}
													}
													else
													{
														$get_media_tag_art_pro_video = $Addcomevent->get_tag_media_info($get_videos_n_new[$count_art_cr_video]);
														if($sel_video!="")
														{
															//$get_sel_song =$Addcomevent->get_sel_songs($_GET['edit']);
															//$exp_video_id = explode(",",$get_sel_video['taggedvideos']);
															$yes_tag_pro_video =0;
															for($count_sel=0;$count_sel<=count($sel_video);$count_sel++)
															{
																if($sel_video[$count_sel]==""){continue;}
																else{
																	if($sel_video[$count_sel] == $get_media_tag_art_pro_video['id'])
																	{
																		$yes_tag_pro_video = 1;
																	?>
																		<option value="<?php echo $get_media_tag_art_pro_video['id'];?>" selected><?php echo substr($get_media_tag_art_pro_video['creator'],0,20) ." : ". substr($get_media_tag_art_pro_video['from'],0,20) ." : ".  $get_media_tag_art_pro_video['title'];?></option>
																	<?php
																	}
																}
															}
															if($yes_tag_pro_video == 0)
															{
																
															?>
																<option value="<?php echo $get_media_tag_art_pro_video['id'];?>"><?php echo substr($get_media_tag_art_pro_video['creator'],0,20) ." : ". substr($get_media_tag_art_pro_video['from'],0,20) ." : ".  $get_media_tag_art_pro_video['title'];?></option>
															<?php
															}
														}
														else{
														?>
															<option value="<?php echo $get_media_tag_art_pro_video['id'];?>"><?php echo substr($get_media_tag_art_pro_video['creator'],0,20) ." : ". substr($get_media_tag_art_pro_video['from'],0,20) ." : ".  $get_media_tag_art_pro_video['title'];?></option>
														<?php
															}
														}
													}
												} */
												
										//if(mysql_num_rows($get_community_video_id)<=0 && $res_video==null && $media_video=="" && count($get_media_cre_art_pro)<=0 && mysql_num_rows($get_media_tag_com_pro)<=0 && mysql_num_rows($get_media_tag_art_pro)<=0)
									?>
											</select>
									<?php									
									}
										
										if(empty($get_vids_pnew_w))
										{
										?>
											<a style="border-radius:5px; -webkit-border-radius:5px; -moz-border-radius:5px; -ms-border-radius:5px; -o-border-radius:5px; background-color: #000000; border: 0 none; color: #FFFFFF; font-size: 13px; margin-left: 5px; padding: 6px 8px; text-decoration: none;" href="add_media.php?adds=115">Add Video</a>
										<?php	
										}
										?>
                    			
                    		</div>
                            
							<div class="fieldCont">
                                  <div title="Select media and profiles to display on your profile page." class="fieldTitle">Upcoming Events</div>
                                        <?php
										$events_comm_upc = 0;
										//var_dump($acc_event);
										//	die;
										$get_evec_id = array();
										while($get_evec_ids = mysql_fetch_assoc($get_event))
										{
											$get_evec_ids['id'] = $get_evec_ids['id'].'~com';
											$get_evec_id[] = $get_evec_ids;
										}
										
										$get_aevec_id = array();
										while($get_aevec_ids = mysql_fetch_assoc($get_aeevent))
										{
											$get_aevec_ids['id'] = $get_aevec_ids['id'].'~art';
											$get_aevec_id[] = $get_aevec_ids;
										}
										
										$get_onlycre_upv = array();				
										$get_onlycre_upv = $Addcomevent->only_creator_upevent();
										
										$get_only2cre_ev = array();
										$get_only2cre_ev = $Addcomevent->only_creator2ev_upevent();
										
										$selcoms_tagged = array();
										if($event_avail!=0)
										{
											$acc_event = array_unique($acc_event);
											for($h=0;$h<count($acc_event);$h++)
											{
												if($acc_event[$h]!="" && $acc_event[$h]!=0)
												{
													$dumcomu = $Addcomevent->get_event_tagged_by_other_user($acc_event[$h]);
													if(!empty($dumcomu) && $dumcomu['id']!="" && $dumcomu['id']!="")
													{
														$dumcomu['id'] = $dumcomu['id'].'~'.'com';
														$selcoms_tagged[] = $dumcomu;
													}	
												}
											}
										}
										
										$selarts_tagged = array();
										if($event_aavail!=0)
										{
											$acc_aevent = array_unique($acc_aevent);
											for($h=0;$h<count($acc_aevent);$h++)
											{
												if($acc_aevent[$h]!="" && $acc_aevent[$h]!=0)
												{
													$dumartu = $Addcomevent->get_aevent_tagged_by_other_user($acc_aevent[$h]);
													if(!empty($dumartu) && $dumartu['id']!="" && $dumartu['id']!="")
													{
														$dumartu['id'] = $dumartu['id'].'~'.'art';
														$selarts_tagged[] = $dumartu;
													}	
												}
											}
										}
										
										$selarts2_tagged = array();
										if($project_aavail!=0)
										{
											$acc_aproject = array_unique($acc_aproject);
											for($acc_pro=0;$acc_pro<count($acc_aproject);$acc_pro++)
											{												
												if($acc_aproject[$acc_pro]!="" && $acc_aproject[$acc_pro]!=0)
												{													
													$sql_1_cre = $newgeneral->get_event_tagged_by_other2_user($acc_aproject[$acc_pro]);
													if($sql_1_cre!="")
													{
														if(mysql_num_rows($sql_1_cre)>0)
														{
															while($run = mysql_fetch_assoc($sql_1_cre))
															{
																$run['id'] = $run['id'].'~'.'art';
																//$ans_a_als[] = $run;
																$selarts2_tagged[] = $run;
															}
														}
													}
													
													$sql_2_cre = $newgeneral->get_event_atagged_by_other2_user($acc_aproject[$acc_pro]);
													if($sql_2_cre!="")
													{
														if(mysql_num_rows($sql_2_cre)>0)
														{
															while($run_c = mysql_fetch_assoc($sql_2_cre))
															{
																$run_c['id'] = $run_c['id'].'~'.'com';
																//$ans_c_als[]
																$selarts2_tagged[] = $run_c;
															}
														}
													}
												}
											}
										}
										
										$selcoms2_tagged = array();
										if($project_avail!=0)
										{
											$acc_project = array_unique($acc_project);
											for($acc_pro=0;$acc_pro<count($acc_project);$acc_pro++)
											{												
												if($acc_project[$acc_pro]!="" && $acc_project[$acc_pro]!=0)
												{													
													$sql_1_cre = $newgeneral->get_aevent_tagged_by_other2_user($acc_project[$acc_pro]);
													if($sql_1_cre!="")
													{
														if(mysql_num_rows($sql_1_cre)>0)
														{
															while($run = mysql_fetch_assoc($sql_1_cre))
															{
																$run['id'] = $run['id'].'~'.'art';
																//$ans_a_als[] = $run;
																$selcoms2_tagged[] = $run;
															}
														}
													}
													
													$sql_2_cre = $newgeneral->get_aevent_atagged_by_other2_user($acc_project[$acc_pro]);
													if($sql_2_cre !="" && $sql_2_cre!= Null)
													{
														if(mysql_num_rows($sql_2_cre)>0)
														{
															while($run_c = mysql_fetch_assoc($sql_2_cre))
															{
																$run_c['id'] = $run_c['id'].'~'.'com';
																//$ans_c_als[]
																$selcoms2_tagged[] = $run_c;
															}
														}
													}
												}
											}
										}
										
										$final_evesup = array_merge($get_evec_id,$selarts_tagged,$selcoms_tagged,$get_onlycre_upv,$get_aevec_id,$get_only2cre_ev,$selcoms2_tagged,$selarts2_tagged);
										
										$sort = array();
										foreach($final_evesup as $k=>$v) {
										
											//$create_c = strpos($v['creator'],'(');
											//$end_c = substr($v['creator'],0,$create_c);
											
											//$sort['creator'][$k] = $end_c;
											//$sort['from'][$k] = $end_f;
											//$sort['track'][$k] = $v['track'];
											$sort['date'][$k] = $v['date'];
										}
										//var_dump($sort);
										if(!empty($sort))
										{
											array_multisort($sort['date'], SORT_DESC,$final_evesup);
										}
										
										$dummy_uev = array();
										$get_all_del_id = $newgeneral->get_deleted_project_id();
										$exp_del_id = explode(",",$get_all_del_id['deleted_event_id']);
										for($count_all=0;$count_all<count($final_evesup);$count_all++){
											for($count_del=0;$count_del<count($exp_del_id);$count_del++){
												if($exp_del_id[$count_del]!=""){
													if($final_evesup[$count_all]['id'] == $exp_del_id[$count_del]){
														$final_evesup[$count_all] ="";
													}
												}
											}
										}
										
										if(!empty($final_evesup))
										{
			?>
											<select title="Select media and profiles to display on your profile page." name="tagged_upcoming_event[]" size="5" multiple="multiple" id="tagged_upcoming_event[]" class="list">
			<?php
										for($ev_f=0;$ev_f<count($final_evesup);$ev_f++)
										{
											if($final_evesup[$ev_f]!="")
											{
												$not_editabrle = 0;
												if(isset($_GET['id']))
												{
													$exps = explode('~',$final_evesup[$ev_f]['id']);
													if($_GET['id']==$exps[0])
													{
														$not_editabrle = 1;
													}
												}
												if($not_editabrle!=1)
												{
													if(in_array($final_evesup[$ev_f]['id'],$dummy_uev))
													{}
													else
													{
														$dummy_uev[] = $final_evesup[$ev_f]['id'];
														if($get_select_events[0]!="")
														{
															$get_select_events = array_unique($get_select_events);
															for($f=0;$f<count($get_select_events);$f++)
															{
																if($get_select_events[$f]==$final_evesup[$ev_f]['id'])
																{
																	$j_comm_acc=1;
				?>
																	<option value="<?php echo $final_evesup[$ev_f]['id'];?>" selected ><?php echo date("m.d.y", strtotime($final_evesup[$ev_f]['date'])).' '.stripslashes($final_evesup[$ev_f]['title']);?></option>
				<?php	
																	continue;
																}
															}	
															
															if($j_comm_acc==1)
															{
																$j_comm_acc=0;
																continue;
															}
														}
				?>
														<option value="<?php echo $final_evesup[$ev_f]['id'];?>" ><?php echo date("m.d.y", strtotime($final_evesup[$ev_f]['date'])).' '.stripslashes($final_evesup[$ev_f]['title']);?></option>
				<?php
													}
												}
											}
										}
										
										//if(mysql_num_rows($get_event)<=0 && empty($dummy_uev) && mysql_num_rows($get_aeevent)<=0)
				?>
											</select>
				<?php
										}
										if(empty($final_evesup))
										{
										?>
											<a style="border-radius:5px; -webkit-border-radius:5px; -moz-border-radius:5px; -ms-border-radius:5px; -o-border-radius:5px; background-color: #000000; border: 0 none; color: #FFFFFF; font-size: 13px; margin-left: 5px; padding: 6px 8px; text-decoration: none;" href="add_community_event.php">Add Event</a>
										<?php	
										}
										?>
                                </div>
								
								<div class="fieldCont">
                                  <div title="Select media and profiles to display on your profile page." class="fieldTitle">Recorded Events</div>
                                        <?php
										$comm_rec_events = 0;
										//var_dump($acc_event);
										//	die;
										$get_rec_id = array();
										while($get_rec_ids = mysql_fetch_assoc($get_eventrec))
										{
											$get_rec_ids['id'] = $get_rec_ids['id'].'~com';
											$get_rec_id[] = $get_rec_ids;
										}
										
										$get_arec_id = array();
										while($get_arec_ids = mysql_fetch_assoc($get_aeeventrec))
										{
											$get_arec_ids['id'] = $get_arec_ids['id'].'~art';
											$get_arec_id[] = $get_arec_ids;
										}
										
										$get_onlycre_rev = array();
										$get_onlycre_rev = $Addcomevent->only_creator_recevent();
										
										$get_only2cre_rev = array();
										$get_only2cre_rev = $Addcomevent->only_creator2_recevent();
										
										$selcom_stagged = array();
										if($event_avail!=0)
										{
											$acc_event = array_unique($acc_event);
											for($k=0;$k<count($acc_event);$k++)
											{
												if($acc_event[$k]!="" && $acc_event[$k]!=0)
												{
													$dumec = $Addcomevent->get_event_tagged_by_other_userrec($acc_event[$k]);
													if(!empty($dumec) && $dumec['id']!="" && $dumec['id']!="")
													{
														$dumec['id'] = $dumec['id'].'~'.'com';
														$selcom_stagged[] = $dumec;
													}												
												}
											}
										}
										
										$selart_stagged = array();
										if($event_aavail!=0)
										{
											$acc_aevent = array_unique($acc_aevent);
											for($ki=0;$ki<count($acc_aevent);$ki++)
											{
												if($acc_aevent[$ki]!="" && $acc_aevent[$ki]!=0)
												{
													$dumc = $Addcomevent->get_aevent_tagged_by_other_userrec($acc_aevent[$ki]);
													if(!empty($dumc) && $dumc['id']!="" && $dumc['id']!="")
													{
														$dumc['id'] = $dumc['id'].'~'.'art';
														$selart_stagged[] = $dumc;
													}												
												}
											}
										}
										
										$selarts2_tagged = array();
										if($project_aavail!=0)
										{
											$acc_aproject = array_unique($acc_aproject);
											for($acc_pro=0;$acc_pro<count($acc_aproject);$acc_pro++)
											{												
												if($acc_aproject[$acc_pro]!="" && $acc_aproject[$acc_pro]!=0)
												{													
													$sql_1_cre = $newgeneral->get_event_tagged_by_other2_userrec($acc_aproject[$acc_pro]);
													if($sql_1_cre!="")
													{
														if(mysql_num_rows($sql_1_cre)>0)
														{
															while($run = mysql_fetch_assoc($sql_1_cre))
															{
																$run['id'] = $run['id'].'~'.'art';
																//$ans_a_als[] = $run;
																$selarts2_tagged[] = $run;
															}
														}
													}
													
													$sql_2_cre = $newgeneral->get_event_atagged_by_other2_userrec($acc_aproject[$acc_pro]);
													if($sql_2_cre!="")
													{
														if(mysql_num_rows($sql_2_cre)>0)
														{
															while($run_c = mysql_fetch_assoc($sql_2_cre))
															{
																$run_c['id'] = $run_c['id'].'~'.'com';
																//$ans_c_als[]
																$selarts2_tagged[] = $run_c;
															}
														}
													}
												}
											}
										}
										
										$selcoms2_tagged = array();
										if($project_avail!=0)
										{
											$acc_project = array_unique($acc_project);
											for($acc_pro=0;$acc_pro<count($acc_project);$acc_pro++)
											{												
												if($acc_project[$acc_pro]!="" && $acc_project[$acc_pro]!=0)
												{													
													$sql_1_cre = $newgeneral->get_aevent_tagged_by_other2_userrec($acc_project[$acc_pro]);
													if($sql_1_cre!="")
													{
														if(mysql_num_rows($sql_1_cre)>0)
														{
															while($run = mysql_fetch_assoc($sql_1_cre))
															{
																$run['id'] = $run['id'].'~'.'art';
																//$ans_a_als[] = $run;
																$selcoms2_tagged[] = $run;
															}
														}
													}
													
													$sql_2_cre = $newgeneral->get_aevent_atagged_by_other2_userrec($acc_project[$acc_pro]);
													if($sql_2_cre !="" && $sql_2_cre!= Null)
													{	
														if(mysql_num_rows($sql_2_cre)>0)
														{
															while($run_c = mysql_fetch_assoc($sql_2_cre))
															{
																$run_c['id'] = $run_c['id'].'~'.'com';
																//$ans_c_als[]
																$selcoms2_tagged[] = $run_c;
															}
														}
													}
												}
											}
										}
										
										$final_eves = array_merge($get_rec_id,$selart_stagged,$selcom_stagged,$get_onlycre_rev,$get_arec_id,$get_only2cre_rev,$selcoms2_tagged,$selarts2_tagged);
										
										$sort = array();
										foreach($final_eves as $k=>$v) {
										
											//$create_c = strpos($v['creator'],'(');
											//$end_c = substr($v['creator'],0,$create_c);
											
											//$sort['creator'][$k] = $end_c;
											//$sort['from'][$k] = $end_f;
											//$sort['track'][$k] = $v['track'];
											$sort['date'][$k] = $v['date'];
										}
										//var_dump($sort);
										if(!empty($sort))
										{
											array_multisort($sort['date'], SORT_DESC,$final_eves);
										}
										
										$dummy_ev = array();
										$get_all_del_id = $newgeneral->get_deleted_project_id();
										$exp_del_id = explode(",",$get_all_del_id['deleted_event_id']);
										for($count_all=0;$count_all<count($final_eves);$count_all++){
											for($count_del=0;$count_del<count($exp_del_id);$count_del++){
												if($exp_del_id[$count_del]!=""){
													if($final_eves[$count_all]['id'] == $exp_del_id[$count_del]){
														$final_eves[$count_all] ="";
													}
												}
											}
										}
										
										if(!empty($final_eves))
										{
			?>
											<select title="Select media and profiles to display on your profile page." name="tagged_recorded_event[]" size="5" multiple="multiple" id="tagged_recorded_event[]" class="list">
			<?php
										for($ev_f=0;$ev_f<count($final_eves);$ev_f++)
										{
											if($final_eves[$ev_f]!="")
											{
												$not_editable = 0;
												if(isset($_GET['id']))
												{
													$exps = explode('~',$final_eves[$ev_f]['id']);
													if($_GET['id']==$exps[0])
													{
														$not_editable = 1;
													}
												}
												if($not_editable!=1)
												{
													if(in_array($final_eves[$ev_f]['id'],$dummy_ev))
													{}
													else
													{
														$dummy_ev[] = $final_eves[$ev_f]['id'];
														if($get_select_events_rec[0]!="")
														{
															$get_select_events_rec = array_unique($get_select_events_rec);
															for($f=0;$f<count($get_select_events_rec);$f++)
															{
																if($get_select_events_rec[$f]==$final_eves[$ev_f]['id'])
																{
																	$j_comm_acc=1;
				?>
																	<option value="<?php echo $final_eves[$ev_f]['id'];?>" selected ><?php echo date("m.d.y", strtotime($final_eves[$ev_f]['date'])).' '.stripslashes($final_eves[$ev_f]['title']);?></option>
				<?php	
																	continue;
																}
															}	
															
															if($j_comm_acc==1)
															{
																$j_comm_acc=0;
																continue;
															}
														}
				?>
														<option value="<?php echo $final_eves[$ev_f]['id'];?>" ><?php echo date("m.d.y", strtotime($final_eves[$ev_f]['date'])).' '.stripslashes($final_eves[$ev_f]['title']);?></option>
				<?php
													}
												}
											}
										}
										
										//if(mysql_num_rows($get_eventrec)<=0 && empty($dummy_ev) && mysql_num_rows($get_aeeventrec)<=0)
			?>
											</select>
			<?php
										}
										
										if(empty($final_eves))
										{
										?>
											<a style="border-radius:5px; -webkit-border-radius:5px; -moz-border-radius:5px; -ms-border-radius:5px; -o-border-radius:5px; background-color: #000000; border: 0 none; color: #FFFFFF; font-size: 13px; margin-left: 5px; padding: 6px 8px; text-decoration: none;" href="add_community_event.php">Add Event</a>
										<?php	
										}
										?>
                                    
                                </div>
							
                            <div class="fieldCont">
                           	  <div title="Select media and profiles to display on your profile page." class="fieldTitle">Projects</div>
								<?php
								$get_proc_id = array();
								$get_pro = $Addcomevent->get_all_projects();
								while($row = mysql_fetch_assoc($get_pro))
								{
									$row['id'] = $row['id'].'~'.'com';
									$get_proc_id[] = $row;
								}
								
								$all_community_friend_project = array();
								$get_all_friends = $newgeneral->Get_all_friends();
								if($get_all_friends!="")
								{
									while($res_all_friends = mysql_fetch_assoc($get_all_friends))
									{
										$get_friend_art_info = $newgeneral->get_friend_art_info($res_all_friends['fgeneral_user_id']);
										$get_all_tag_pro = $newgeneral->get_all_tagged_pro($get_friend_art_info);
										if(mysql_num_rows($get_all_tag_pro)>0)
										{
											while($res_projects = mysql_fetch_assoc($get_all_tag_pro))
											{
												//$get_project_type = $newgeneral->Get_Project_type($res_projects['id']);
												$exp_creator = explode('(',$res_projects['creator']);
												$exp_tagged_email = explode(",",$res_projects['tagged_user_email']);
												for($exp_count = 0;$exp_count<count($exp_tagged_email);$exp_count++)
												{
													if($exp_tagged_email[$exp_count]==""){continue;}
													else{
														if($exp_tagged_email[$exp_count]==$_SESSION['login_email'])
														{
															//$res_projects['project_type_name'] = $get_project_type['name'];
															$res_projects['whos_project'] = "tag_pro_com";
															$res_projects['id'] = $res_projects['id']."~com";
															$all_community_friend_project[] = $res_projects;
															?>
															<!--<div id="AccordionContainer" class="tableCont">
																<div class="blkG" style="width:200px;"><?php /*if($res_projects['title']!=""){echo $res_projects['title'];}else{?>&nbsp;<?php }?></div>
																<div class="blkB" style="width:200px;"><a href=""><?php if($exp_creator[0]!=""){echo $exp_creator[0];}else{?>&nbsp;<?php }?></a></div>
																<div class="blkB" style="width:100px;"><?php if($get_project_type['name']!=""){echo $get_project_type['name'];}else{?>&nbsp;<?php }?></div>
																<div class="blkD">&nbsp;</div>
																<div class="icon">&nbsp;</div>
																<div class="icon"><a onclick="return confirmdelete('<?php echo $res_projects['title'];?>')" href="insertcommunityproject.php?delete=<?php echo $res_projects['id'];*/?>"><img src="images/profile/delete.png" /></a></div>
															</div>-->
															<?php
														}
													}
													
												}
											}
										}
									}
								}
								
								$all_artist_friend_project = array();
								$get_all_friends = $editprofile->Get_all_friends();
								if($get_all_friends !="")
								{
									while($res_all_friends = mysql_fetch_assoc($get_all_friends))
									{
										$get_friend_art_info = $editprofile->get_friend_art_info($res_all_friends['fgeneral_user_id']);
										$get_all_tag_pro = $editprofile->get_all_tagged_pro($get_friend_art_info);
										if(mysql_num_rows($get_all_tag_pro)>0)
										{
											while($res_projects = mysql_fetch_assoc($get_all_tag_pro))
											{
												//$get_project_type = $editprofile->Get_Project_type($res_projects['id']);
												$exp_creator = explode('(',$res_projects['creator']);
												$exp_tagged_email = explode(",",$res_projects['tagged_user_email']);
												for($exp_count = 0;$exp_count<count($exp_tagged_email);$exp_count++)
												{
													if($exp_tagged_email[$exp_count]==""){continue;}
													else{
														if($exp_tagged_email[$exp_count]==$_SESSION['login_email'])
														{
															//$res_projects['project_type_name'] = $get_project_type['name'];
															$res_projects['whos_project'] = "tag_pro_art";
															$res_projects['id'] = $res_projects['id']."~art";
															$all_artist_friend_project[] = $res_projects;
															?>
															<!--<div id="AccordionContainer" class="tableCont">
																<div class="blkG" style="width:200px;"><?php /*if($res_projects['title']!=""){echo $res_projects['title'];}else{?>&nbsp;<?php }?></div>
																<div class="blkB" style="width:200px;"><a href=""><?php if($exp_creator[0]!=""){echo $exp_creator[0];}else{?>&nbsp;<?php }?></a></div>
																<div class="blkB" style="width:100px;"><?php if($get_project_type['name']!=""){echo $get_project_type['name'];}else{?>&nbsp;<?php }?></div>
																<div class="blkD">&nbsp;</div>
																<div class="icon">&nbsp;</div>
																<div class="icon"><a onclick="return confirmdelete('<?php echo $res_projects['title'];?>')" href="insertartistproject.php?delete=<?php echo $res_projects['id'];*/?>"><img src="images/profile/delete.png" /></a></div>
															</div>-->
															<?php
														}
													}
													
												}
											}
										}
									}
								}
								
								$coms_pros = array();
								$coms_pros = $Addcomevent->get_projects_creators();
								
								$get_only2cre = array();
								$get_only2cre = $Addcomevent->only_creator2pr_this();
								
								$get_proa_id = array();
								$get_apro = $Addcomevent->get_all_aprojects();
								while($rowa = mysql_fetch_assoc($get_apro))
								{
									$rowa['id'] = $rowa['id'].'~'.'art';
									$get_proa_id[] = $rowa;
								}
								
								$sel_comtagged = array();
								if($project_avail!=0)
								{
									$acc_project = array_unique($acc_project);
									for($acc_pro=0;$acc_pro<count($acc_project);$acc_pro++)
									{
										if($acc_project[$acc_pro]!="" && $acc_project[$acc_pro]!=0)
										{
											$dum_com = $newgeneral->get_project_tagged_by_other_user($acc_project[$acc_pro]);
											if(!empty($dum_com) && $dum_com['id']!="" && $dum_com['id']!="")
											{
												$dum_com['id'] = $dum_com['id'].'~'.'com';
												$sel_comtagged[] = $dum_com;
											}
										}
									}
								}
								
								$sel_arttagged = array();
								if($project_aavail!=0)
								{
									$acc_aproject = array_unique($acc_aproject);
									for($acc_pro=0;$acc_pro<count($acc_aproject);$acc_pro++)
									{
										if($acc_aproject[$acc_pro]!="" && $acc_aproject[$acc_pro]!=0)
										{
											$dum_art = $newgeneral->get_aproject_tagged_by_other_user($acc_aproject[$acc_pro]);
											if(!empty($dum_art) && $dum_art['id']!="" && $dum_art['id']!="")
											{
												$dum_art['id'] = $dum_art['id'].'~'.'art';
												$sel_arttagged[] = $dum_art;
											}
										}
									}
								}
								
								$sel_art2tagged = array();
								if($project_aavail!=0)
								{
									$acc_aproject = array_unique($acc_aproject);
									for($acc_pro=0;$acc_pro<count($acc_aproject);$acc_pro++)
									{
										if($acc_aproject[$acc_pro]!="" && $acc_aproject[$acc_pro]!=0)
										{
											$sql_1_cre = $newgeneral->get_aproject_tagged_by_other2_user($acc_aproject[$acc_pro]);
											if($sql_1_cre!="")
											{
												if(mysql_num_rows($sql_1_cre)>0)
												{
													while($run = mysql_fetch_assoc($sql_1_cre))
													{
														$run['id'] = $run['id'].'~'.'art';
														$sel_art2tagged[] = $run;
													}
												}
											}
											
											$sql_2_cre = $newgeneral->get_aproject_atagged_by_other2_user($acc_aproject[$acc_pro]);
											if($sql_2_cre!="")
											{
												if(mysql_num_rows($sql_2_cre)>0)
												{
													while($run_c = mysql_fetch_assoc($sql_2_cre))
													{
														$run_c['id'] = $run_c['id'].'~'.'com';
														$sel_art2tagged[] = $run_c;
													}
												}
											}
										}
									}
								}
								
								$sel_com2tagged = array();
								if($project_avail!=0)
								{
									$acc_project = array_unique($acc_project);
									for($acc_pro=0;$acc_pro<count($acc_project);$acc_pro++)
									{
										if($acc_project[$acc_pro]!="" && $acc_project[$acc_pro]!=0)
										{
											$sql_1_cre = $newgeneral->get_project_tagged_by_other2_user($acc_project[$acc_pro]);
											if($sql_1_cre!="")
											{
												if(mysql_num_rows($sql_1_cre)>0)
												{
													while($run = mysql_fetch_assoc($sql_1_cre))
													{
														$run['id'] = $run['id'].'~'.'art';
														$sel_com2tagged[] = $run;
													}
												}											
											}
											
											$sql_2_cre = $newgeneral->get_project_atagged_by_other2_user($acc_project[$acc_pro]);
											if($sql_2_cre!="")
											{
												if(mysql_num_rows($sql_2_cre)>0)
												{
													while($run_c = mysql_fetch_assoc($sql_2_cre))
													{
														$run_c['id'] = $run_c['id'].'~'.'com';
														$sel_com2tagged[] = $run_c;
													}
												}										
											}
										}
									}
								}
						
								$final_arrs = array_merge($get_proa_id,$get_proc_id,$sel_arttagged,$sel_comtagged,$coms_pros,$get_only2cre,$sel_art2tagged,$sel_com2tagged,$all_artist_friend_project,$all_community_friend_project);
										
								$sort = array();
								foreach($final_arrs as $k=>$v)
								{
									$end_c1 = $v['creator'];
									//$create_c = strpos($v['creator'],'(');
									//$end_c1 = substr($v['creator'],0,$create_c);
									$end_c = trim($end_c1);
									
									$sort['creator'][$k] = strtolower($end_c);
									//$sort['from'][$k] = $end_f;
									//$sort['track'][$k] = $v['track'];
									$sort['title'][$k] = strtolower($v['title']);
								}
								//var_dump($sort);
								if(!empty($sort))
								{
									array_multisort($sort['creator'], SORT_ASC, $sort['title'], SORT_ASC,$final_arrs);
								}
								
								$dummy_pr = array();
								$get_all_del_id = $newgeneral->get_deleted_project_id();
								$exp_del_id = explode(",",$get_all_del_id['deleted_project_id']);
								for($count_all=0;$count_all<count($final_arrs);$count_all++){
									for($count_del=0;$count_del<count($exp_del_id);$count_del++){
										if($exp_del_id[$count_del]!=""){
											if($final_arrs[$count_all]['id'] == $exp_del_id[$count_del]){
												$final_arrs[$count_all] ="";
											}
										}
									}
								}
								
								if(!empty($final_arrs))
								{
			?>
									<select title="Select media and profiles to display on your profile page." name="tagged_projects[]" id="tagged_projects[]" size="5" multiple="multiple"  class="list">
			<?php
								for($f_i=0;$f_i<count($final_arrs);$f_i++)
								{
									if($final_arrs[$f_i]!="")
									{
										if(in_array($final_arrs[$f_i]['id'],$dummy_pr))
										{
											
										}
										else
										{
											$dummy_pr[] = $final_arrs[$f_i]['id'];
											
											//$create_c = strpos($final_arrs[$f_i]['creator'],'(');
											//$end_c = substr($final_arrs[$f_i]['creator'],0,$create_c);
											$end_c = $final_arrs[$f_i]['creator'];
											
											$ecei = "";
											if($end_c!="")
											{
												$ecei = $end_c;
												$ecei .= ' : '.$final_arrs[$f_i]['title'];
											}
											else
											{
												$ecei = $final_arrs[$f_i]['title'];
											}
											
											if(!empty($get_select_projects))
											{
												$get_select_projects = array_unique($get_select_projects);
												for($i=0;$i<count($get_select_projects);$i++)
												{
													if($get_select_projects[$i]!="")
													{
														if($get_select_projects[$i]==$final_arrs[$f_i]['id'])
														{
															$j_chan=1;
				?>
															<option value="<?php echo $final_arrs[$f_i]['id'];?>" selected ><?php echo stripslashes($ecei);?></option>
				<?php
															continue;
														}
													}
												}
												if($j_chan==1)
												{
													$j_chan=0;
													continue;
												}
											}
											?>
											<option value="<?php echo $final_arrs[$f_i]['id'];?>" ><?php echo stripslashes($ecei);?></option><?php
										}
									}
								}
								
								/* if($project_avail!=0)
								{
									for($acc_pro=1;$acc_pro<count($acc_project);$acc_pro++)
									{
											//$sel_tagged="";
										$sel_tagged=$newgeneral->get_project_tagged_by_other_user($acc_project[$acc_pro]);
										
										if(isset($sel_tagged) && ($sel_tagged['title']!="" || $sel_tagged['title']!=null))
										{
											if($get_select_projects[0]!="")
											{
												for($i=0;$i<count($get_select_projects);$i++)
												{
													if($get_select_projects[$i]==$sel_tagged['id'])
													{
														$j_chan=1;
											?>
												<option value="<?php echo $sel_tagged['id'];?>" selected ><?php echo $sel_tagged['title'];?></option>
											<?php	
														continue;
													}
												}
											
												if($j_chan==1)
												{
													$j_chan=0;
													continue;
												}
											}
										?>
											<option value="<?php echo $sel_tagged['id'];?>" ><?php echo $sel_tagged['title'];?></option>
										<?php
										}
									}
								} */
								
								
								//if(mysql_num_rows($get_pro)<=0 && $project_avail==0 && mysql_num_rows($get_apro)<=0 && $project_aavail==0)
		?>
										</select>
		<?php
								}
								if(empty($final_arrs))
								{
		?>
									<a style="border-radius:5px; -webkit-border-radius:5px; -moz-border-radius:5px; -ms-border-radius:5px; -o-border-radius:5px; background-color: #000000; border: 0 none; color: #FFFFFF; font-size: 13px; margin-left: 5px; padding: 6px 8px; text-decoration: none;" href="add_community_project.php">Add Project</a>
		<?php
								}
		?>
                    		</div>
							
							
							<!--<div class="fieldCont">
                           	  <div class="fieldTitle">Artists</div>
                                <select name="tagged_all_artist[]" id="tagged_all_artist[]" size="5" multiple="multiple" class="list">
                                   	<?php/*
									$get_all_art = $Addcomevent->get_all_Artists();
									//$get_select_art = $newclassobj->Get_selected_tagg($_GET['id']);
									if(mysql_num_rows($get_all_art)>0)
									{
										while($ans_all_artist = mysql_fetch_assoc($get_all_art))
										{	
											for($art_all_i=0;$art_all_i<=count($sel_all_art_i);$art_all_i++)
											{
												if($sel_all_art_i[$art_all_i]==$ans_all_artist['artist_id'])
												{
													$chain_cont=1;
													?>
														<option value="<?php echo $ans_all_artist['artist_id']; ?>" selected><?php echo $ans_all_artist['name']; ?></option>
													<?php
													continue;
												}
											}
											if($chain_cont==1)
											{
												$chain_cont=0;
												continue;
											}
											?>
												<option value="<?php echo $ans_all_artist['artist_id']; ?>"><?php echo $ans_all_artist['name']; ?></option>
											<?php	
										}
									}
									if(mysql_num_rows($get_all_art)==0)
									{
										?>
											<option value="0">No Listings</option>
										<?php
									}
									?>
                    			</select>
                    		</div>
							
							
							<div class="fieldCont">
                           	  <div class="fieldTitle">Community</div>
                                <select name="tagged_all_community[]" id="tagged_all_community[]" size="5" multiple="multiple" class="list">
                                   	<?php
									$get_all_art = $Addcomevent->get_all_Community();
									//$get_select_art = $newclassobj->Get_selected_tagg($_GET['id']);
									if(mysql_num_rows($get_all_art)>0)
									{
										while($ans_all_artist = mysql_fetch_assoc($get_all_art))
										{	
											for($art_alli=0;$art_alli<=count($sel_all_art);$art_alli++)
											{
												if($sel_all_art[$art_alli]==$ans_all_artist['community_id'])
												{
													$chain_cont=1;
													?>
														<option value="<?php echo $ans_all_artist['community_id']; ?>" selected><?php echo $ans_all_artist['name']; ?></option>
													<?php
													continue;
												}
											}
											if($chain_cont==1)
											{
												$chain_cont=0;
												continue;
											}
											?>
												<option value="<?php echo $ans_all_artist['community_id']; ?>"><?php echo $ans_all_artist['name']; ?></option>
											<?php	
										}
									}
									if(mysql_num_rows($get_all_art)==0)
									{
										?>
											<option value="0">No Listings</option>
										<?php
									}*/
									?>
                    			</select>
                    		</div>-->
							<input type="hidden" value="<?php echo $getdetail['tagged_galleries'];?>" class="fieldText" id="old_gallery" name="old_gallery">
							<input type="hidden" value="<?php echo $getdetail['tagged_songs'];?>" class="fieldText" id="old_song" name="old_song">
							<input type="hidden" value="<?php echo $getdetail['tagged_videos'];?>" class="fieldText" id="old_video" name="old_video">
							<input type="hidden" value="<?php echo $getdetail['tagged_user_email'];?>" class="fieldText" id="tagged_email_old" name="tagged_email_old">
							<input type="hidden" value="<?php echo $getdetail['creators_info'];?>" class="fieldText" id="previous_creator" name="previous_creator">
                            <div class="fieldCont">
                                <div class="fieldTitle"></div>
								<?php
								if(isset($_GET['id']) && $_GET['id']!="")
								{
								?>
									<a href="javascript:void(0);" onclick="confirm_saving()"><input type="button" value=" Save " style="border-radius:5px; -webkit-border-radius:5px; -moz-border-radius:5px; -ms-border-radius:5px; -o-border-radius:5px; color: rgb(255, 255, 255); font-family: 'futura-pt',sans-serif; font-size: 0.825em; font-weight: bold; padding: 6px 10px; float: left; margin-right: 10px; border: 0px none; background-color: rgb(0, 0, 0);" /></a>
								<?php	
								}else{
								?>
                                <input type="submit" class="register" style="border-radius:5px; -webkit-border-radius:5px; -moz-border-radius:5px; -ms-border-radius:5px; -o-border-radius:5px;" value="Save" />
								<?php
								}
								?>
                            	<!--<input type="button" class="register" value="Publish" />-->
                            </div>
                            <!--<div class="fieldCont">
                                <div class="fieldTitle"></div>
                                
                            </div>
                            <div class="fieldCont">
                                <div class="fieldTitle"></div>
                                
                            </div>
                            <div class="fieldCont">
                                <div class="fieldTitle"></div>
                                
                            </div>
                            <div class="fieldCont">
                                <div class="fieldTitle"></div>
                                
                            </div>
                            <div class="fieldCont">
                                <div class="fieldTitle"></div>
                                
                            </div>-->
                            </form>
                        </div>
                        <div style="float:right;" id="selected-types"></div>
                    </div>
                </div>
				</div>
				</div>
				</div>
				</div>
				<?php include_once("displayfooter.php"); ?>
		

<!--files for search technique
<script type="text/javascript" src="javascripts/jquery.js"></script>
<script type="text/javascript" src="javascripts/jquery.autocomplete.js"></script>
<link rel="stylesheet" href="css/jquery.autocomplete.css" type="text/css" />-->
<!--files for search technique ends here -->
<script type="text/javascript">
   $(function() {
	$( "#communitybands" ).autocomplete({
		source: function(request, response) {
		$.ajax({
		  url: "usersuggestion.php",
		  dataType: "json",
		  data: request,                    
		  success: function (data) {
			// No matching result
			if(data != null)
			{
				if (data.length == 0) {
				  document.getElementById("add_user_enable").style.display = "none" ;
					document.getElementById("add_user_disable").style.display = "block" ;
					$(".ui-autocomplete-loading").css({"background" : "none"});
					$("#User_error").css({"display" : "block","color" : "red","width":"200px","position":"relative","left":"107px"});
					document.getElementById("communitybands").value="";
				}
				else {
					$("#User_error").css({"display" : "none"});
				  response(data);
				}
			}
			else
			{
				$(".ui-autocomplete-loading").css({"background" : "none"});
			}
		  }});
		},

		minLength: 1,
		open:function(e){
			document.getElementById("add_user_enable").style.display = "block" ;
			document.getElementById("add_user_disable").style.display = "none" ;
		},
		select: function( event, ui ) {
			document.getElementById("useremail_old").value = ui.item.value1;
			//document.getElementById("venuewebsite").value=ui.item.value2;
		},
		change: function(event, ui) {
			console.log(this.value);
			if (ui.item == null) {
				document.getElementById("communitybands").value="";
				$("#User_error").css({"display" : "block","color" : "red","width":"200px","position":"relative","left":"107px"});
			} 
		}
	});
});
</script>	

<script type="text/javascript">
$(function() {
	$( "#creator" ).autocomplete({
		source: function(request, response) {
		$.ajax({
		  url: "usersuggestion_Creator.php",
		  dataType: "json",
		  data: request,                    
		  success: function (data) {
			// No matching result
			if (data.length == 0) {
			  $("#User_error_creator").css({"display" : "block","color" : "red","width":"200px","position":"relative","left":"107px"});
				$(".ui-autocomplete-loading").css({"background" : "none"});
				document.getElementById("creator").value="";
			}
			else {
				$("#User_error_creator").css({"display" : "none"});
			  response(data);
			}
		  }});
		},

		minLength: 1,
		select: function( event, ui ) {
			document.getElementById("creators_info").value = ui.item.value2;
		},
		change: function(event, ui) {
			console.log(this.value);
			if (ui.item == null) {
				document.getElementById("creator").value="";
				$("#User_error_creator").css({"display" : "block","color" : "red","width":"200px","position":"relative","left":"107px"});
			} 
		}
	});
});
</script>	
	
<!--<script type="text/javascript" src="ZeroClipboard.js" ></script>-->
<script type="text/javascript">
ZeroClipboard.setMoviePath('ZeroClipboard.swf');			
$("document").ready(function(){
	$("#copyvalue").click(function(){
		var clip = new ZeroClipboard.Client();
		clip.glue("copyvalue");
		var theList = document.getElementById("communityguest_list[]");
		var sContent = "";
		for(var i=0;i<theList.options.length;i++) 
		{	
			sContent += theList.options[i].text + "\n";
		}
			//alert(sContent);
			clip.setText(sContent);
		  //window.clipboardData.setData("text",sContent);
		  
		  clip.addEventListener( 'complete', function(client, sContent) {
                                alert("Copied text to clipboard: " + sContent );
                        } );
	//} 
	});  
});

function copyexam()
{
		var clip = new ZeroClipboard.Client();
		clip.glue("copyvalue");
		var theList = document.getElementById("communityguest_list[]");
		var sContent = "";
		for(var i=0;i<theList.options.length;i++) 
		{	
			sContent += theList.options[i].text + "\n";
		}
		//	alert(sContent);
			clip.setText(sContent);
			
}
</script>	
<!--<script type="text/javascript" src="javascripts/jquery.ui.autocomplete.js"></script>
		<script type="text/javascript" src="javascripts/jquery.ui.position.js"></script>-->
		
<!--<style>
	.ui-autocomplete-loading { background: white url('css/ui-anim_basic_16x16.gif') right center no-repeat; }
	</style>-->
<script>
	$(function()
	{
		$( "#venuename" ).autocomplete({
			source: "suggest_community_venue.php",
			minLength: 1,
			select: function( event, ui ) {
			document.getElementById("venueaddress").value=ui.item.value1;
			document.getElementById("venuewebsite").value=ui.item.value2;
			}
		});
	});
	
	$(window).load(function()
	{
		copyexam();
	});
	</script>

	</body>
	</html>