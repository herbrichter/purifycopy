<?php
include_once('commons/db.php');
include('classes/AddArtistEvent.php');
include('classes/NewSuggestion.php');
session_start();

$newclassobj = new AddArtistEvent();
$newclassobj_sug = new NewSuggestion();
$q = strtolower($_GET["term"]);
$art_comm = $newclassobj->selgeneral();

if (!$q) return;

	$result = array();
	
	$rs_a_f = $newclassobj_sug->friend_art($_SESSION['login_id']);
	if(mysql_num_rows($rs_a_f)>0)
	{
		$art_pro_f = array();
		while ($row_fa = mysql_fetch_array($rs_a_f)) 
		{
			$rs_a_fi_g = $newclassobj_sug->general_user_sec($row_fa['fgeneral_user_id']);
			if($rs_a_fi_g['artist_id']!=0)
			{
				$art_pro_f[] = $rs_a_fi_g;
			}
		}
	}
	
	$rs_c_f = $newclassobj_sug->friend_comm($_SESSION['login_id']);
	if(mysql_num_rows($rs_c_f)>0)
	{
		$com_pro_f = array();
		while ($row_fc = mysql_fetch_array($rs_c_f)) 
		{	
			$rs_c_fi_g = $newclassobj_sug->general_user_sec($row_fc['fgeneral_user_id']);
			if($rs_c_fi_g['community_id']!=0)
			{
				$com_pro_f[] = $rs_c_fi_g;
			}
		}
	}
	
	//////////////////////////////////////Logged In Artist Project/////////////////////////////
	if(isset($art_comm['artist_id']) && $art_comm['artist_id']!=0)
	{
		$rs_apr = $newclassobj_sug->user_suggestionartpr($q,$art_comm['artist_id']);
		if(mysql_num_rows($rs_apr)>0)
		{
			while ($row = mysql_fetch_array($rs_apr)) 
			{
				$rs_t_apr = $newclassobj_sug->user_arttype_new($row['id']);
				
				//array_push($result, array("id"=>str_replace("'","`",$row["title"])."(".str_replace("'","`",$rs_t_apr["name"]).")", "label"=>str_replace("'","`",$row["title"])."(".str_replace("'","`",$rs_t_apr["name"]).")", "value" => str_replace("'","`",$row["title"])."(".str_replace("'","`",$rs_t_apr["name"]).")", "value1" =>$row["id"], "value2" => "artist_project|".$row["id"].""));
				array_push($result, array("id"=>str_replace("'","`",$row["title"])."(".str_replace("'","`",$rs_t_apr["name"]).")", "label"=>str_replace("'","`",$row["title"])."(".str_replace("'","`",$rs_t_apr["name"]).")", "value" => str_replace("'","`",$row["title"]), "value1" =>$row["id"], "value2" => "artist_project|".$row["id"].""));
			}
		}
	}
	
	//////////////////////////////////////Logged In Friend's Artist Project/////////////////////////////
	if(isset($art_pro_f) && count($art_pro_f)>0)
	{
		for($ap_f=0;$ap_f<count($art_pro_f);$ap_f++)
		{
			$rs_apr_f = $newclassobj_sug->user_suggestionartpr($q,$art_pro_f[$ap_f]['artist_id']);
			if(mysql_num_rows($rs_apr_f)>0)
			{
				while ($row_ap_f = mysql_fetch_array($rs_apr_f)) 
				{
					$rs_t_apr_f = $newclassobj_sug->user_arttype_new($row_ap_f['id']);
					
					//array_push($result, array("id"=>str_replace("'","`",$row_ap_f["title"])."(".str_replace("'","`",$rs_t_apr_f["name"]).")", "label"=>str_replace("'","`",$row_ap_f["title"])."(".str_replace("'","`",$rs_t_apr_f["name"]).")", "value" => str_replace("'","`",$row_ap_f["title"])."(".str_replace("'","`",$rs_t_apr_f["name"]).")", "value1" => $row_ap_f["id"], "value2" => "artist_project|".$row_ap_f["id"].""));
					array_push($result, array("id"=>str_replace("'","`",$row_ap_f["title"])."(".str_replace("'","`",$rs_t_apr_f["name"]).")", "label"=>str_replace("'","`",$row_ap_f["title"])."(".str_replace("'","`",$rs_t_apr_f["name"]).")", "value" => str_replace("'","`",$row_ap_f["title"]), "value1" => $row_ap_f["id"], "value2" => "artist_project|".$row_ap_f["id"].""));
				}
			}
		}
	}
	
	//////////////////////////////////////Logged In Community Project/////////////////////////////
	if(isset($art_comm['community_id']) && $art_comm['community_id']!=0)
	{
		$rs_cpr = $newclassobj_sug->user_suggestioncompr($q,$art_comm['community_id']);
		
		if(mysql_num_rows($rs_cpr)>0)
		{
			while ($row = mysql_fetch_array($rs_cpr)) 
			{
				$rs_t_cpr = $newclassobj_sug->user_commtype_new($row['id']);
				
				//array_push($result, array("id"=>str_replace("'","`",$row["title"])."(".str_replace("'","`",$rs_t_cpr["name"]).")", "label"=>str_replace("'","`",$row["title"])."(".str_replace("'","`",$rs_t_cpr["name"]).")", "value" =>str_replace("'","`",$row["title"])."(".str_replace("'","`",$rs_t_cpr["name"]).")", "value1" => $row["id"], "value2" => "community_project|".$row["id"].""));
				array_push($result, array("id"=>str_replace("'","`",$row["title"])."(".str_replace("'","`",$rs_t_cpr["name"]).")", "label"=>str_replace("'","`",$row["title"])."(".str_replace("'","`",$rs_t_cpr["name"]).")", "value" =>str_replace("'","`",$row["title"]), "value1" => $row["id"], "value2" => "community_project|".$row["id"].""));
			}
		}
	}
	
	//////////////////////////////////////Logged In Friend's Community Project/////////////////////////////
	if(isset($com_pro_f) && count($com_pro_f)>0)
	{
		for($cp_f=0;$cp_f<count($com_pro_f);$cp_f++)
		{
			$rs_cpr_f = $newclassobj_sug->user_suggestioncompr($q,$com_pro_f[$cp_f]['community_id']);
			if(mysql_num_rows($rs_cpr_f)>0)
			{
				while ($row_cp_f = mysql_fetch_array($rs_cpr_f)) 
				{
					$rs_t_cpr_f = $newclassobj_sug->user_commtype_new($row_cp_f['id']);
					
					//array_push($result, array("id"=>str_replace("'","`",$row_cp_f["title"])."(".str_replace("'","`",$rs_t_cpr_f["name"]).")", "label"=>str_replace("'","`",$row_cp_f["title"])."(".str_replace("'","`",$rs_t_cpr_f["name"]).")", "value" =>str_replace("'","`",$row_cp_f["title"])."(".str_replace("'","`",$rs_t_cpr_f["name"]).")", "value1" => $row_cp_f["id"], "value2" => "community_project|".$row_cp_f["id"].""));
					array_push($result, array("id"=>str_replace("'","`",$row_cp_f["title"])."(".str_replace("'","`",$rs_t_cpr_f["name"]).")", "label"=>str_replace("'","`",$row_cp_f["title"])."(".str_replace("'","`",$rs_t_cpr_f["name"]).")", "value" =>str_replace("'","`",$row_cp_f["title"]), "value1" => $row_cp_f["id"], "value2" => "community_project|".$row_cp_f["id"].""));
				}
			}
		}
	}
	
	//////////////////////////////////////Logged In Artist Event/////////////////////////////
	if(isset($art_comm['artist_id']) && $art_comm['artist_id']!=0)
	{
		$rs_aev = $newclassobj_sug->user_suggestionartevent($q,$art_comm['artist_id']);
		if(mysql_num_rows($rs_aev)>0)
		{
			while ($row_ev_a = mysql_fetch_array($rs_aev)) 
			{
				$rs_t_aev = $newclassobj_sug->user_arttype_eve_new($row_ev_a['id']);
				
				//array_push($result, array("id"=>str_replace("'","`",$row_ev_a["title"])."(".str_replace("'","`",$rs_t_aev["name"]).")", "label"=>str_replace("'","`",$row_ev_a["title"])."(".str_replace("'","`",$rs_t_aev["name"]).")", "value" =>str_replace("'","`",$row_ev_a["title"])."(".str_replace("'","`",$rs_t_aev["name"]).")", "value1" => $row_ev_a["id"], "value2" => "artist_event|".$row_ev_a["id"].""));
				$new_dt = date("m.d.y", strtotime($row_ev_a["date"]));
				array_push($result, array("id"=>str_replace("'","`",$row_ev_a["title"])."(".str_replace("'","`",$rs_t_aev["name"]).")", "label"=>$new_dt .' '. str_replace("'","`",$row_ev_a["title"])."(".str_replace("'","`",$rs_t_aev["name"]).")", "value" =>str_replace("'","`",$row_ev_a["title"]), "value1" => $row_ev_a["id"], "value2" => "artist_event|".$row_ev_a["id"].""));
			}
		}
	}
	
	//////////////////////////////////////Logged In Friend's Artist Event/////////////////////////////
	if(isset($art_pro_f) && count($art_pro_f)>0)
	{
		for($ap_f=0;$ap_f<count($art_pro_f);$ap_f++)
		{
			$rs_apr_f = $newclassobj_sug->user_suggestionartevent($q,$art_pro_f[$ap_f]['artist_id']);
			if(mysql_num_rows($rs_apr_f)>0)
			{
				while ($row_ap_f = mysql_fetch_array($rs_apr_f)) 
				{
					$rs_t_apr_f = $newclassobj_sug->user_arttype_eve_new($row_ap_f['id']);
					
					//array_push($result, array("id"=>str_replace("'","`",$row_ap_f["title"])."(".str_replace("'","`",$rs_t_apr_f["name"]).")", "label"=>str_replace("'","`",$row_ap_f["title"])."(".str_replace("'","`",$rs_t_apr_f["name"]).")", "value" => str_replace("'","`",$row_ap_f["title"])."(".str_replace("'","`",$rs_t_apr_f["name"]).")", "value1" => $row_ap_f["id"], "value2" => "artist_event|".$row_ap_f["id"].""));
					$new_dt = date("m.d.y", strtotime($row_ap_f["date"]));
					array_push($result, array("id"=>str_replace("'","`",$row_ap_f["title"])."(".str_replace("'","`",$rs_t_apr_f["name"]).")", "label"=>$new_dt .' '. str_replace("'","`",$row_ap_f["title"])."(".str_replace("'","`",$rs_t_apr_f["name"]).")", "value" => str_replace("'","`",$row_ap_f["title"]), "value1" => $row_ap_f["id"], "value2" => "artist_event|".$row_ap_f["id"].""));
				}
			}
		}
	}
	
	//////////////////////////////////////Logged In Community Event/////////////////////////////
	if(isset($art_comm['community_id']) && $art_comm['community_id']!=0)
	{
		$rs_cev = $newclassobj_sug->user_suggestioncomevent($q,$art_comm['community_id']);
		if(mysql_num_rows($rs_cev)>0)
		{
			while ($row_ev_c = mysql_fetch_array($rs_cev)) 
			{
				$rs_t_cev = $newclassobj_sug->user_comtype_eve_new($row_ev_c['id']);
				
				//array_push($result, array("id"=>str_replace("'","`",$row_ev_c["title"])."(".str_replace("'","`",$rs_t_cev["name"]).")", "label"=>str_replace("'","`",$row_ev_c["title"])."(".str_replace("'","`",$rs_t_cev["name"]).")", "value" => str_replace("'","`",$row_ev_c["title"])."(".str_replace("'","`",$rs_t_cev["name"]).")", "value1" => $row_ev_c["id"], "value2" => "community_event|".$row_ev_c["id"].""));
				$new_dt = date("m.d.y", strtotime($row_ev_c["date"]));
				array_push($result, array("id"=>str_replace("'","`",$row_ev_c["title"])."(".str_replace("'","`",$rs_t_cev["name"]).")", "label"=>$new_dt .' '. str_replace("'","`",$row_ev_c["title"])."(".str_replace("'","`",$rs_t_cev["name"]).")", "value" => str_replace("'","`",$row_ev_c["title"]), "value1" => $row_ev_c["id"], "value2" => "community_event|".$row_ev_c["id"].""));
			}
		}
	}
	
	//////////////////////////////////////Logged In Friend's Community Event/////////////////////////////
	if(isset($com_pro_f) && count($com_pro_f)>0)
	{
		for($cp_f=0;$cp_f<count($com_pro_f);$cp_f++)
		{
			$rs_cpr_f = $newclassobj_sug->user_suggestioncomevent($q,$com_pro_f[$cp_f]['community_id']);
			if(mysql_num_rows($rs_cpr_f)>0)
			{
				while ($row_cp_f = mysql_fetch_array($rs_cpr_f)) 
				{
					$rs_t_cpr_f = $newclassobj_sug->user_comtype_eve_new($row_cp_f['id']);
					
					//array_push($result, array("id"=>str_replace("'","`",$row_cp_f["title"])."(".str_replace("'","`",$rs_t_cpr_f["name"]).")", "label"=>str_replace("'","`",$row_cp_f["title"])."(".str_replace("'","`",$rs_t_cpr_f["name"]).")", "value" => str_replace("'","`",$row_cp_f["title"])."(".str_replace("'","`",$rs_t_cpr_f["name"]).")", "value1" => $row_cp_f["id"], "value2" => "community_event|".$row_cp_f["id"].""));
					$new_dt = date("m.d.y", strtotime($row_cp_f["date"]));
					array_push($result, array("id"=>str_replace("'","`",$row_cp_f["title"])."(".str_replace("'","`",$rs_t_cpr_f["name"]).")", "label"=>$new_dt .' '. str_replace("'","`",$row_cp_f["title"])."(".str_replace("'","`",$rs_t_cpr_f["name"]).")", "value" => str_replace("'","`",$row_cp_f["title"]), "value1" => $row_cp_f["id"], "value2" => "community_event|".$row_cp_f["id"].""));
				}
			}
		}
	}
	
	echo array_to_json($result);
		
function array_to_json( $array )
{
    if( !is_array( $array ) )
	{
        return false;
    }

    $associative = count( array_diff( array_keys($array), array_keys( array_keys( $array )) ));
    if( $associative ){

        $construct = array();
        foreach( $array as $key => $value ){

            // We first copy each key/value pair into a staging array,
            // formatting each key and value properly as we go.

            // Format the key:
            if( is_numeric($key) ){
                $key = "key_$key";
            }
            $key = "\"".addslashes($key)."\"";

            // Format the value:
            if( is_array( $value )){
                $value = array_to_json( $value );
            } else if( !is_numeric( $value ) || is_string( $value ) ){
                $value = "\"".addslashes($value)."\"";
            }

            // Add to staging array:
            $construct[] = "$key: $value";
        }

        // Then we collapse the staging array into the JSON form:
        $result = "{ " . implode( ", ", $construct ) . " }";

    } else { // If the array is a vector (not associative):

        $construct = array();
        foreach( $array as $value ){

            // Format the value:
            if( is_array( $value )){
                $value = array_to_json( $value );
            } else if( !is_numeric( $value ) || is_string( $value ) ){
                $value = "'".addslashes($value)."'";
            }

            // Add to staging array:
            $construct[] = $value;
        }

        // Then we collapse the staging array into the JSON form:
        $result = "[ " . implode( ", ", $construct ) . " ]";
    }

    return $result;
}


?>