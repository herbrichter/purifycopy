
    <!--<script src="jcrop/js/jquery.min.js" type="text/javascript"></script>-->
    <script src="jcrop/js/jquery.Jcrop.js" type="text/javascript"></script>
    <link rel="stylesheet" href="jcrop/css/jquery.Jcrop.css" type="text/css" />
    <link rel="stylesheet" href="jcrop/demo_files/demos.css" type="text/css" />

	<!--<script src="jcrop/js/popup.js" type="text/javascript"></script>
    <link rel="stylesheet" href="jcrop/css/popup.css" type="text/css" />-->

	<script type="text/javascript" src="uploadify/swfobject.js"></script>
	<script type="text/javascript" src="uploadify/jquery.uploadify.v2.1.4.min.js"></script>
	<link href="uploadify/uploadify.css" rel="stylesheet"/>

	<?php
		if(isset($_SESSION['login_email']))
		{
			$sql = mysql_query("select * from  general_user where email='".$_SESSION['login_email']."'");
			$res=mysql_fetch_assoc($sql);
			//var_dump($res);

			$sql1=mysql_query("select * from  general_artist where artist_id = '".$res['artist_id']."'");
			$res1=mysql_fetch_assoc($sql1);
		}
	?>
	<script type="text/javascript" >
	var myUploader = null;
	var iMaxUploadSize = 10485760; //10MB
	var IMG_UPLOAD_PATH = "http://artistjcrop.s3.amazonaws.com/";
	// Create variables (in this scope) to hold the API and image size
	var jcrop_api, boundx, boundy;
	var allowResizeCropper = false;
    jQuery(function($){
		$("#cropBtnPro_artist_profile").click(function(){
		
			if($('#x_artist_profile').val()!="" && $("#target_artist_profile").attr('src')!=""){
			var name_split = $("#target_artist_profile").attr('src').split('/');
			if(name_split[0]=='uploads')
			{
				var slash = '../' + $("#target_artist_profile").attr('src');
				
			}
			else
			{
				var slash = $("#target_artist_profile").attr('src');
			}
				$.ajax({
					type: "POST",
					url: 'jcrop/cropImage.php',
					data: { "action":'crop',"resize":allowResizeCropper,"x":$('#x_artist_profile').val(), "y":$('#y_artist_profile').val(), "w":$('#w_artist_profile').val(), "h":$('#h_artist_profile').val(), "srcImg":slash},
					success: function(data){
					//disablePopup();
						parent.jQuery.fancybox.close();
						//window.location.href="profileedit_artist.php";
						window.location.reload();
						if(allowResizeCropper)
							$("#resultImgThumb").attr('src',data);
						else
							$("#resultImgProfile").attr('src',data);
							var diff=data.split("/");
							//window.parent.document.getElementById("pro_pic").value = diff['4'];
					}
				});
			}
		});
    });


	function generateUploader(){
		if(!myUploader){
				myUploader = {
				uploadify : function(){
					$('#file_upload_artist_profile').uploadify({
						'uploader'  : '../uploadify/uploadify.swf',
						'script'    : 'jcrop/uploadFiles.php?id=<?php echo $res['artist_id']; ?>',
						'cancelImg' : '../uploadify/cancel.png',
						'folder'    : 'jcrop/croppingFiles/',
						'auto'      : true,
						'multi'		: true,
						'removeCompleted' : true,
						'wmode'		: 'transparent',
						'buttonText': 'Upload Image',
						'fileExt'     : '*.jpg;*.gif;*.png',
						'fileDesc'    : 'Image Files',
						'simUploadLimit' : 1,
						'sizeLimit'	: iMaxUploadSize, //10 MB size
						'onComplete': function(event, ID, fileObj, response, data) {
							// On File Upload Completion			
							//location = 'uploadtos3.php?uploads=complete';
							//$("#uploadedImages").val($("#uploadedImages").val() + response + "|");
							if(response=="invalid")
							{
								$("#errMsg").html("Invalid image size.");
							}
							else
							{
								$("#errMsg").html('');
								$("#cropperSpace").show();
								generateCropperObj(response);
							}
							setTimeout(function(){
								$.fancybox.update();
							},1000);
						},
						'onSelect' : function (event, ID, fileObj){
						},
						'onSelectOnce' : function(event, data)
						{
							/*iTotFileSize = data.allBytesTotal;
							if(iTotFileSize >= iMaxUploadSize){
								var iOverSize = (iTotFileSize - iMaxUploadSize) / 1024 / 1024; // Size, in MB
								$("#divGalleryFileSize").show().html("Maximum 10MB allowed. Total queue size approximately: <font color='red'>" + Math.ceil(iTotFileSize/1024/1024) + " (MB)</font>");
								$('#file_upload').uploadifyClearQueue();
							}
							else
							{
							   $("#divGalleryFileSize").hide() 
							}*/
						},
						'onOpen'	: function() {
							//hide overly
						}
						/*,
						'onError'     : function (event,ID,fileObj,errorObj) {
						},
						'onProgress'  : function(event,ID,fileObj,data) {
						  var bytes = Math.round(data.bytesLoaded / 1024);
						  $('#containerResult').html(' - ' + bytes + 'KB Uploaded');
						  return false;
						}*/

						});
					}
				};
				file_upload=document.getElementById("file_upload_artist_profile");
				myUploader.uploadify();
		}
	}

function generateCropperObj(img){
      if(jcrop_api){ 
		jcrop_api.destroy(); 
	  }
		$("#targetTD_artist_profile").html('<img id="target_artist_profile">');
		$("#target_artist_profile").attr('src',img);
		$("#preview_artist_profile").attr('src',img);

      $('#target_artist_profile').Jcrop({
        onChange: updatePreview,
        onSelect: updateCoords,
		allowResize: allowResizeCropper,
		allowSelect: allowResizeCropper,
        aspectRatio: 1
      },function(){
        // Use the API to get the real image size
        var bounds = this.getBounds();
        boundx = bounds[0];
        boundy = bounds[1];
        jcrop_api = this;
		if(!allowResizeCropper){
		$("#hinttext").text("Image size should be more than 450x450.");
			jcrop_api.animateTo([0,0,450,450]);
			jcrop_api.setOptions({ allowSelect: false });
			jcrop_api.setOptions({ allowResize: true });
			 jcrop_api.setOptions(true? {
            minSize: [ 450, 450 ],
            maxSize: [ 0, 0 ]
          }: {
            minSize: [ 0, 0 ],
            maxSize: [ 0, 0 ]
          });
		  jcrop_api.focus();
		}
		else
		{
			$("#hinttext").text("Image size should be more than 450x450.");
		}
        // Store the API in the jcrop_api variable
      });

	function updateCoords(c)
	{
		$('#x_artist_profile').val(c.x);
		$('#y_artist_profile').val(c.y);
		$('#w_artist_profile').val(c.w);
		$('#h_artist_profile').val(c.h);
		updatePreview(c);
	};

      function updatePreview(c)
      {
        if (parseInt(c.w) > 0)
        {
          var rx = 100 / c.w;
          var ry = 100 / c.h;
          $('#preview_artist_profile').css({
            width: Math.round(rx * boundx) + 'px',
            height: Math.round(ry * boundy) + 'px',
            marginLeft: '-' + Math.round(rx * c.x) + 'px',
            marginTop: '-' + Math.round(ry * c.y) + 'px'
          });
        }
      };
	
}

function showCropper(resizeFlag){
	allowResizeCropper = resizeFlag;
	/*this line is commented as per our requirement*/
		
		//$("#cropperSpace").hide();
		
	/*this line is commented as per our requirement*/
	//centerPopup();	loadPopup(); 
	generateUploader(); //generateCropperObj();
}

function create(showcropperflag)
{
	showCropper(showcropperflag);
	updateCoords_manually();
	var str ="<?php echo $newrow['image_name']; ?>";
	var image_name = str.substr(10);
	var response = image_name;
	if(response=="" || response==null)
	{
		return;
	}
	else
	{
		$("#cropperSpace").show();
		allowResizeCropper=false;
		//community_jcrop_api.destroy(); 
		if(jcrop_api)
		{ 
			jcrop_api.destroy(); 
		}
		<?php 
			$name_image = $newrow['image_name'];
			$new_image1 = strpos($name_image,'bnail');
			if($new_image1==0)
			{
				if($newrow['image_name']!="")
				{
		?>
					var response = "<?php echo $newrow['image_name']; ?>";
					var IMG_UPLOAD_PATH = "http://regprofilepic.s3.amazonaws.com/";
		<?php
					$source = "http://regprofilepic.s3.amazonaws.com/".$newrow['image_name'];
					$dest = "jcrop/croppingFiles/".$newrow['image_name'];
					if(copy($source,$dest))
					{
		?>			
		<?php
					}
					$path = $_SERVER['DOCUMENT_ROOT']."/jcrop/croppingFiles/".$newrow['image_name'];
                    $root = realpath($_SERVER["DOCUMENT_ROOT"]);
                    if (!class_exists('S3')) require_once ($root.'/S3.php');
		            if (!defined('awsAccessKey')) define('awsAccessKey', '');
		            if (!defined('awsSecretKey')) define('awsSecretKey', '');
					$s3 = new S3(awsAccessKey, awsSecretKey);
					$bucket_name = "artistjcrop";
					$s3->putObject(S3::inputFile($path),$bucket_name,$newrow['image_name'], S3::ACL_PUBLIC_READ);
					unlink($path);
				}
			}
			else
			{
		?>
				var check_split = response.split('_');
				if(check_split[1]=='thumbnail')
				{
					 var i;
					 var response = "";
					 for(i=2;i<check_split.length;i++)
					 {
						if(i!=2)
						{
							var response = response +'_'+ check_split[i] ;
						}
						else
						{
							var response = response + check_split[i] ;
						}
					}
					var IMG_UPLOAD_PATH = "http://regprofilepic.s3.amazonaws.com/";
				}
				else
				{
					var IMG_UPLOAD_PATH = "http://artistjcrop.s3.amazonaws.com/";
				}
		<?php
			}
		?>
		generateCropperObj(IMG_UPLOAD_PATH+response);
	}

}

function updateCoords_manually()
{
	$('#x_artist_profile').val(0);
	$('#y_artist_profile').val(0);
	$('#w_artist_profile').val(450);
	$('#h_artist_profile').val(450);
}

/*Script For Detecting Flash*/
$("document").ready(function(){
	var flashEnabled = !!(navigator.mimeTypes["application/x-shockwave-flash"] || window.ActiveXObject && new ActiveXObject('ShockwaveFlash.ShockwaveFlash'));
	if (!flashEnabled) { 
		$("#Flashnotfound_img_crop_pro").css({"display":"block","color":"red","float":"left","width":"300px","position":"relative","left":"40px"});
	}
});
/*Script For Detecting Flash Ends Here*/
  </script>


<!--<a href="javascript:showCropper(true);">Create Thumbnail</a><br/><br/>
<a title="Select an image to crop and display on your profile page." href="javascript:showCropper(false);" style="text-decoration:none;" class="hint" onClick="create()"><input type="button" value=" Change "/></a>-->
<a href="#popupContactartist_pro" title="Select an image to crop and display on your profile page." style="text-decoration:none;" class="fancybox" onClick="create(false)"><input class="blackhint" type="button" value=" Change "/></a>
<script type="text/javascript">
	$(".fancybox").fancybox({
		helpers : {
			media : {},
			buttons : {},
			title : null
		},
		afterShow: function(){
			var resize = setTimeout(function(){
			$.fancybox.update();
			},1000);
		}
	});
</script>
<!--<div style="margin:50px;">
	<img id="resultImgThumb" style="vertical-align:top;"/>
	<img id="resultImgProfile"/>
</div>-->

<!-- POPUP BOX START -->

<div id="popupContactartist_pro" style="display:none;">
	<div class="fancy_header">
		Profile Picture
	</div>
	<div class="pop_whole_image_content" >
		<div class="popup_image_preview_box">
			Preview
			<div class="popup_image_preview">
				<img src="" id="preview_artist_profile" alt="Preview" class="jcrop-preview" />
			</div>
		</div>
		<div id="Flashnotfound_img_crop_pro" style="display:none;">
			Either your flash player plugin is disabled or if you don't have the flash player <a href="http://get.adobe.com/flashplayer/" target="_blank">Click Here</a> to install.
		</div>
		<div class="pop_image_function_button">
			<form action="" methos="post" enctype="multipart/form-data" class="pop_image_form">
				<div class="pop_image_upload_button"><input name="theFile" type="file" id="file_upload_artist_profile" /></div>
				<div id="hinttext" class="pop_image_upload_hinttext">Image size should be more than 450x450.</div>
			</form>
			<div class="pop_image_crop_button">
				<input type="button" value="Crop Image" id="cropBtnPro_artist_profile" style=" background-color: #404040;border-radius: 5px;color: white;cursor: pointer;padding: 7px 27px;border:none;"/>
				<input type="hidden" id="x_artist_profile" name="x" />
				<input type="hidden" id="y_artist_profile" name="y" />
				<input type="hidden" id="w_artist_profile" name="w" />
				<input type="hidden" id="h_artist_profile" name="h" />
			</div>
		</div>
		<div style="clear:both"></div>
        <div id="errMsg" style="color:red;text-align:center;"></div>
			
			
		
		<div style="float:left; clear:both;">	
			
			<table id="cropperSpace" style="display:none;  bottom: 100px;" class="big_image">
			<tr>
			<td rowspan="2" id="targetTD_artist_profile">
				  <img src="" style="max-width:400px" id="target_artist_profile"/>				
				</td>
				
			  </tr>
			  <!--<tr>
			    <td>Result
					<div style="width:100px;height:100px;overflow:hidden;">
						<img  id="result" alt="Result" class="jcrop-result" style="width:100px;height:100px;" />
					</div>
				</td>
		      </tr>-->
			  
			</table>
        </div>
		<br>
	</div>
</div>
<div id="backgroundPopup"></div>
<!-- POPUP BOX END -->