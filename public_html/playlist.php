<?php
header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past
?>
<?php
if(isset($_GET['getfile']))
{
header('Content-type: text/xml');
// The PDF source is in original.pdf
    $filename = $_GET['getfile'];
    $xml = new DOMDocument();
    if (file_exists($filename))
        {
            $xml->load($filename);
            echo $xml->saveXML();
        }
}
?>
<?php
if(isset($_GET['uid']))
    $uid = $_GET['uid'];
elseif(isset($_GET['userid']))
    $uid = $_GET['userid'];


$filename = 'playlist'.$uid.'.xml';
if (isset($_GET['delete'])) {
 if($_GET['delete']=='all')
 {
    if (file_exists($filename)) {
        unlink($filename);
    }
    exit;
}}
if (isset($_GET['delete'])) {
    $flag = false;
    $xml = new DOMDocument();
    if (file_exists($filename)) {
        $xml->load($filename);
        $root = $xml->documentElement;
        $songss = $root->getElementsByTagName("song");
        $count_songs = 0;
        foreach ($songss as $songsss) {
            $urls = $songsss->getElementsByTagName("url");
            $url = $urls->item(0)->nodeValue;
            if ($_GET['url'] == $url) {
                $flag = true;
            }
            if ($flag) {
                $nodetoremove = $songsss;
                $root->removeChild($nodetoremove);
                $xml->save($filename);
                $flag = false;
            }
            $count_songs++;
        }

        $xml->save($filename);
    }
    $delurl = $_GET['url'];
    echo 'removed :'.$delurl;
    exit;
}
if(isset($_GET['getplaylist']))
{
    if(isset($_GET['url']))
    $uid = $_GET['url'];
        $xml = new DOMDocument();
        if (file_exists($filename)) {
            $xml->load($filename);
            $songtags = $xml->getElementsByTagName('song');
            foreach ($songtags as $songtag)
            {
               echo $songtag->getAttribute('id');
               echo "|";
            }
            exit;
            }
}
if (isset($_GET['id']) && isset($_GET['ajaxadd'])) {
    if (isset($_GET['path'])) {
        $path = $_GET['path'];
        $xml = new DOMDocument();
        if (file_exists($filename)) {
            $xml->load($filename);
            $xml->formatOutput = true;
            $songss = $xml->getElementsByTagName("song");
            $flag = false;
            $songsscount = 0;
            $clipindex = 0;
            foreach ($songss as $songsss) {

                $titles = $songsss->getElementsByTagName("title");
                $title = $titles->item(0)->nodeValue;
                $urls = $songsss->getElementsByTagname("url");
                $url = $urls->item(0)->nodeValue;
                if (strtolower($title) == strtolower($_GET['name'])) {
                    $clipindex = $songsscount;
                    $flag = true;
                }
                $songsscount++;
            }
            if (!$flag) {
                $clipindex = $songsscount;
                $r = $xml->getElementsByTagName("response")->item(0);

                    $b = $xml->createElement("song");
                    $b->setAttribute("id", $_GET['id']);
                    $title = $xml->createElement("title");
                    $title->appendChild($xml->createTextNode($_GET['name']));
                    $b->appendChild($title);
                    $url = $xml->createElement("url");
                    $url->appendChild($xml->createTextNode($path));
                    $b->appendChild($url);
                    $pro_img = $xml->createElement("profile_image");
                    $pro_img->appendChild($xml->createTextNode($_GET['pro_img']));

                    $home_page = $xml->createElement("home_page");
                    $home_page->appendChild($xml->createTextNode($_GET['profileurl']));
                    $b->appendChild($home_page);
                    
                    $user_name = $xml->createElement("username");
                    $user_name->appendChild($xml->createTextNode($_GET['username']));
                    $b->appendChild($user_name);

                    $b->appendChild($pro_img);
                    $r->appendChild($b);
                    $xml->save($filename);
                    header('Content-Type:application/xml;charset=utf-8', true);
                    echo $xml->saveXML();
            }
        } else {
            $xml = new DOMDocument('1.0', 'ISO-8859-1');
            $xml->formatOutput = true;
            $root = $xml->createElement("response");
            $b = $xml->createElement("song");
            $b->setAttribute("id", $_GET['id']);
            $title = $xml->createElement("title");
            $title->appendChild(
                    $xml->createTextNode($_GET['name'])
            );
            $b->appendChild($title);

            $url = $xml->createElement("url");
            $url->appendChild(
                    $xml->createTextNode($path)
            );
            $b->appendChild($url);
            $pro_img = $xml->createElement("profile_image");
            $pro_img->appendChild($xml->createTextNode($_GET['pro_img']));
            $b->appendChild($pro_img);
                    $home_page = $xml->createElement("home_page");
                    $home_page->appendChild($xml->createTextNode($_GET['profileurl']));
                    $b->appendChild($home_page);

                    $user_name = $xml->createElement("username");
                    $user_name->appendChild($xml->createTextNode($_GET['username']));
                    $b->appendChild($user_name);

            $root->appendChild($b);
            $xml->appendChild($root);
            $xml->save($filename);
            header('Content-Type:application/xml;charset=utf-8', true);
            echo $xml->saveXML();
        }
    }
    exit;
}
?>
<?php
include_once('commons/db.php');
include_once('classes/User.php');
$objUser = new User();
$rs;
$row;
$playlistout = false;
if(isset($_GET["allmp3"]))
{
    $sql = " SELECT distinct `user`.`user_id`, `name`, `homepage`, `category`, `more_abt_user`, `upload_type`, `upload_name`, `upload_path`, `create_date`, `update_date`, `featured`, `subscribed`, `status`, `user`.`del`, `img_path` FROM `user` inner join `user_img` on `user`.`user_id` = `user_img`.`user_id` WHERE `upload_type` = 1 ";
    $rs = mysql_query($sql) or die(mysql_error());
$playlistout = true;
}
if(isset($_GET["allvideo"]))
{
    $sql = "  SELECT distinct `user`.`user_id`, `name`, `homepage`, `category`, `more_abt_user`, `upload_type`, `upload_name`, `upload_path`, `create_date`, `update_date`, `featured`, `subscribed`, `status`, `user`.`del`, `img_path` FROM `user` inner join `user_img` on `user`.`user_id` = `user_img`.`user_id` WHERE `upload_type` = 2 ";
    $rs = mysql_query($sql) or die(mysql_error());
    $playlistout = true;
}
if($playlistout )
{
$xml = new DOMDocument('1.0', 'ISO-8859-1');
$xml->formatOutput = true;
$root = $xml->createElement("response");
while (    $row = mysql_fetch_assoc($rs))
{
        $b = $xml->createElement("song");
        $b->setAttribute("id", $row['user_id']);
        $title = $xml->createElement("title");
        $songtitle = str_ireplace(".mp3", "", $row['upload_name']);
        $songtitle = str_ireplace(".flv", "", $songtitle);
        $title->appendChild($xml->createTextNode($songtitle));
        $b->appendChild($title);

        $url = $xml->createElement("url");
        $url->appendChild(
                $xml->createTextNode($row['upload_path'])
        );
        $b->appendChild($url);
        $pro_img = $xml->createElement("profile_image");
        $pro_img->appendChild($xml->createTextNode($row['img_path']));

        $b->appendChild($pro_img);

        $home_page = $xml->createElement("home_page");
        $home_page->appendChild($xml->createTextNode($row['homepage']));
        $b->appendChild($home_page);

        $user_name = $xml->createElement("username");
        $user_name->appendChild($xml->createTextNode($row['name']));
        $b->appendChild($user_name);

        $root->appendChild($b);
        $xml->appendChild($root);
        $xml->save($filename);
}
header('Content-Type:application/xml;charset=utf-8', true);
echo $xml->saveXML();
}

?>