<?php
	include("classes/forgotPassword.php");
	include_once('sendgrid/SendGrid_loader.php');
	$sendgrid = new SendGrid('','');
	$pass_obj = new forgotPassword();
    $domainname=$_SERVER['SERVER_NAME'];
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "https://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="https://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<meta https-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Purify Art</title>
<link rel="stylesheet" type="text/css" media="screen" href="includes/purify.css" />
<script src="https://jqueryjs.googlecode.com/files/jquery-1.2.6.min.js" type="text/javascript"></script>
<script src="includes/popup.js" type="text/javascript"></script>
<?php 
	include_once('classes/Commontabs.php');
	$newtab = new Commontabs();
	include_once('includes/header.php');  
	
	require 'admin/openid.php';
		$data = array();
		/* if(isset($_POST))
		{
			if(isset($_POST['pass_email']))
			{ */
			try
			{
				# Change 'localhost' to your domain name.
				$openid = new LightOpenID($domainname);
				 
				//Not already logged in
				if(!$openid->mode)
				{
					//The google openid url
					//https://me.yahoo.com/[your_username]
					$openid->identity = 'https://www.google.com/accounts/o8/id';
					 
					//Get additional google account information about the user , name , email , country
					$openid->required = array('contact/email' , 'namePerson/first' , 'namePerson/last' , 'pref/language' , 'contact/country/home');
					 
					//start discovery
					header('Location: ' . $openid->authUrl());
				}
				 
				else if($openid->mode == 'cancel')
				{
					echo 'User has canceled authentication!';
					//redirect back to login page ??
				}
				 
				//Echo login information by default
				else
				{
					if($openid->validate())
					{
						//User logged in
						$d = $openid->getAttributes();
						 
						$first_name = $d['namePerson/first'];
						$last_name = $d['namePerson/last'];
						$email = $d['contact/email'];
						$language_code = $d['pref/language'];
						$country_code = $d['contact/country/home'];
						 
						$data = array(
							'first_name' => $first_name ,
							'last_name' => $last_name ,
							'email' => $email ,
						);
	 
						//now signup/login the user.
						//process_google_data($data);
					}
					else
					{
						//user is not logged in
					}
				}
				
				//var_dump($data);
				
				//var_dump($user_chk);
				//die;
			}

			catch(ErrorException $e)
			{
				echo $e->getMessage();
			}	
?>
	
	<div id="miscNav" style="padding: 30px; top: 30px;width:940px;margin: 0 auto 100px;float:none;height:150px;">
		<b style="float: left;font-size: 24px;text-align: center;width: inherit;">RESET PASSWORD</b>
		<?php
			$email_us_chk = $newtab->chk_user($data['email']);
			//var_dump($email_us_chk);
			//die;
			if($email_us_chk=="0" && !isset($_GET['openid.ns']))
			{
				header('Location: login_email.php');
			}
		?>
		<div id="loginBox" style="width:610px;">
			<form name="passForm" method="POST" id="passForm" autocomplete="off" action="" style="float:none;">
				<div class="logFieldCont">
					<input name="newpasses" id="newpasses" type="text" class="loginField" value="New Password" onfocus="if(this.value == 'New Password'){this.value = '';} chng_type('1_0');" onblur="if(this.value == ''){this.value='New Password';} chng_type('1_1');" style="width:250px;padding:10px;font-size:15px;"/>
				</div>	
				<div class="logFieldCont">	
					<input name="cnewpasses" id="cnewpasses" type="text" class="loginField" value="Confirm Password" onfocus="if(this.value == 'Confirm Password'){this.value = '';} chng_type('2_0');" onblur="if(this.value == ''){this.value='Confirm Password';} chng_type('2_1');" style="width:250px;padding:10px;font-size:15px;"/>
				</div>	
				<input type="hidden" name="pass_email" id="pass_email" value="<?php echo $data['email']; ?>"/>
				<input type="button" onclick="return validate_passform();" name="Submit_pass" id="Submit_pass" value="Save" class="loginButton" style="font-size:15px;padding:7px 15px;left:66px;position:relative;"/>
			</form>
		</div>
	</div>

<?php /* } } */  include_once('includes/footer.php'); ?>

<script>
	
	$("document").ready(function(){
		$("#Submit_pass").click(function()
		{
			var pw1 = $("#newpasses").val();
			var pw2 = $("#cnewpasses").val();
			var email = $("#pass_email").val();
			var invalid = " ";
			if(pw1!="" && pw2!="")
			{
				//var ajax_mess = 'newie='+ pw1 +'&newiec='+ pw2+'&email='+ email;
				$.ajax({
					type: 'POST',
					url : 'Change_pwd.php',
					//data: ajax_mess,
					data: {
						newie : pw1,
						newiec : pw2,
						email : email
					},
					success: function(data)  
					{
						if(data!="")
						{
							$("#error_pass").html(data);
						}
						else
						{
							$("#error_pass").html('');
							alert("Your Password is changed successfully.");
							window.location = "login_email.php";
						}
					}
				});
			}
		});
	});
	
	function chng_type(chng)
	{
		var pw1 = $("#newpasses").val();
		var pw2 = $("#cnewpasses").val();
		//alert(newpasses.value);
		if(chng=="1_1")
		{
			if(pw1=='New Password')
			{
				document.getElementById('newpasses').type = 'text';
			}
		}
		else if(chng=="1_0")
		{
			document.getElementById('newpasses').type = 'password';
		}
		else if(chng=="2_1")
		{
			if(pw2=="Confirm Password")
			{
				document.getElementById('cnewpasses').type = 'text';
			}
		}
		else if(chng=="2_0")
		{
			document.getElementById('cnewpasses').type = 'password';
		}
	}
	
	function validate_passform()
	{
		var invalid = " "; // Invalid character is a space
		var minLength = 6; // Minimum length
		var pw1 = $("#newpasses").val();
		var pw2 = $("#cnewpasses").val();
		
		
		if($("#newpasses").val() == 'New Password')
		{
			alert("Please enter your new password.");
			$("#newpasses").focus();
			return false;			
		}
		if($("#cnewpasses").val() == 'Confirm Password')
		{
			alert("Please re-enter your new password.");
			$("#cnewpasses").focus();
			return false;
		}
		// check for minimum length
		if ($("#newpasses").val().length < minLength)
		{
			alert('Your password must be at least ' + minLength + ' characters long. Try again.');
			$("#newpasses").focus();
			return false;
		}
		// check for a value in both fields.
		if (pw1 == '' || pw2 == '')
		{
			alert('Please Re-enter your password twice ');
			$("#cnewpasses").focus();
			return false;
		}
		// check for spaces
		if ($("#newpasses").val().indexOf(invalid) > -1)
		{
			alert("Sorry, spaces are not allowed.");
			$("#newpasses").focus();
			return false;
		}
		
		if (pw1 != pw2)
		{
			alert ("You did not enter the same new password twice. Please re-enter your password.");
			$("#cnewpasses").focus();
			return false;
		}
	}
	
</script>
