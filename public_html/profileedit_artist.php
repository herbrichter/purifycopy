<?php
session_start();
	//ob_start();
	include_once('loggedin_includes.php');
	include_once('classes/ProfileType.php');
	include_once('classes/AddContact.php');
	include_once('classes/EditArtist.php');
	include_once('classes/Commontabs.php');
	include_once('classes/Promotions.php');
	include_once('classes/ProfileeditCommunity.php');
	//include('recaptchalib.php');
	$publickey = "6LeEdQcAAAAAAKFe18Wkza5LXfFd5FWlRW2rx-HU";
	$privatekey = "6LeEdQcAAAAAAMnXxGuMT3gLx2rcEtPvQcgQ4iw9";
	//$parent=$_POST['member_type'];
    $domainname=$_SERVER['SERVER_NAME'];
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<title>Purify Art: Artist Profile</title>
<!--<script type="text/javascript" src="includes/jquery.js"></script>-->
<?php //header("Content-Type: text/javascript; charset=UTF-8"); ?>
<script type="text/javascript">var switchTo5x=true;</script>
<script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script>
<script type="text/javascript">stLight.options({popup:'false', publisher: "65a11f7c-e04e-4326-b62a-efc51e5dd2b3"});</script>
<?php //header("Content-Type: text/html; charset=UTF-8"); ?>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php
$newtab=new Commontabs();
include("header.php");

$date=date("Y-m-d");

$editprofile=new EditArtist();
$newgeneral = new ProfileeditCommunity();
$sel_general=$editprofile->sel_general();
//var_dump($sel_general);
$acc_media=explode(',',$sel_general['accepted_media_id']);

$media_avail = 0;

for($k=0;$k<count($acc_media);$k++)
{
	if($acc_media[$k]!="")
	{
		$media_avail = $media_avail + 1;
	}
}

$get_purify_member=$editprofile->Get_purify_member_Id();
//$get_comm_member=$editprofile->Get_comm_member_Id();


/* $update_event = $editprofile->selectUpcomingEvents();
$update_ans = mysql_fetch_assoc($update_event);
$ans_exp = explode(',',$update_ans['taggedupcomingevents']);
//var_dump($ans_exp);
//echo $date;
//echo "<pre>";
$new_array = array();
for($i=0;$i<count($ans_exp);$i++)
{
	$check_artist = $editprofile->selectArtistEvents($ans_exp[$i]);
	$check_ans = mysql_fetch_assoc($check_artist);
	//echo $check_ans['date'];
	//echo "<pre>";
	if($check_ans['date']>=$date)
	{
		//echo "in if";
		
		$new_array[$i] = $ans_exp[$i];
		//var_dump($new_array);
	}
	else
	{
		//echo "in else";
		$sql_update_up = $editprofile->selectRecordedEvents();
		$sql_update_ans = mysql_fetch_assoc($sql_update_up);
		$all = $sql_update_ans['taggedrecordedevents'];
		$final_value = $all.','.$ans_exp[$i];
		if($ans_exp[$i]!="")
		{
			$update_upc = $editprofile->updatedRecEvents($final_value);
		}
	}
//die;
}
//var_dump($new_array);
//die;
 if(isset($new_array) && count($new_array)>0 && $new_array!=NULL)
 {
	$new_array1 = implode(',',$new_array);
	$update_rec = $editprofile->updatedUpcEvents($new_array1);
 } */


$acc_event=explode(',',$sel_general['taggedartistevents']);
$event_avail = 0;
for($k=0;$k<count($acc_event);$k++)
{
	if($acc_event[$k]!="")
	{
		$event_avail = $event_avail + 1;
	}
}

$acc_cevent=explode(',',$sel_general['taggedcommunityevents']);
$event_cavail = 0;
for($k=0;$k<count($acc_cevent);$k++)
{
	if($acc_cevent[$k]!="")
	{
		$event_cavail = $event_cavail + 1;
	}
}

$acc_project=explode(',',$sel_general['taggedartistprojects']);
$project_avail = 0;
for($k=0;$k<count($acc_project);$k++)
{
	if($acc_project[$k]!="")
	{
		$project_avail = $project_avail + 1;
	}
}

$acc_cproject = explode(',',$sel_general['taggedcommunityprojects']);
$project_cavail = 0;
for($k=0;$k<count($acc_cproject);$k++)
{
	if($acc_cproject[$k]!="")
	{
		$project_cavail = $project_cavail + 1;
	}
}

//var_dump($acc_event[1]);
//var_dump($acc_event[2]);
//echo count($acc_event);
//var_dump($acc_event[4]);
//var_dump($acc_event);
//if($acc_event[1]=="" || $acc_event[1]==null)
//{
	//echo "in if";
//}
//die;

$newrow=$editprofile->Artists();

/**********Code For Displaying selected options in list box for tagged************/

$get_select_songs = explode(',',$newrow['taggedsongs']);
$get_select_songs_fanclub = explode(',',$newrow['fan_song']);
//var_dump($get_select_songs);

$get_select_videos = explode(',',$newrow['taggedvideos']);
$get_select_videos_fanclub = explode(',',$newrow['fan_video']);
//var_dump($get_select_songs);

$get_select_channels = explode(',',$newrow['taggedchannels']);
$get_select_channels_fanclub = explode(',',$newrow['fan_channel']);
//var_dump($get_select_songs);

$get_select_galleries = explode(',',$newrow['taggedgalleries']);
$get_select_galleries_fanclub = explode(',',$newrow['fan_gallery']);
//var_dump($get_select_songs);

$get_select_projects= explode(',',$newrow['taggedprojects']);

$get_select_events = explode(',',$newrow['taggedupcomingevents']);

$get_select_events_rec = explode(',',$newrow['taggedrecordedevents']);
//var_dump($newrow);
//die;

/**********Code For Displaying selected options in list box for tagged ends here************/

$newrow2=$editprofile->ArtistProfile();

$newrow3=$editprofile->Type();
$newrow4=$editprofile->SubType();

/********Functions For Tagged channel video song *******/

$get_artist_channel_id=$editprofile->get_artist_channel_id();




$get_artist_song_register=$editprofile->get_artist_song_at_register();
$get_artist_video_register=$editprofile->get_artist_video_at_register();
$get_artist_gallery_register=$editprofile->get_artist_gallery_name_at_register();

$get_project = $editprofile->getAllProjects();

$get_cproject = $editprofile->getAllC_Projects();

$get_sh_project = $editprofile->getAllProjects();

$get_sh_cproject = $editprofile->getAllC_Projects();
//while($row = mysql_fetch_assoc($get_project))
//{
	//var_dump($row);
	//echo "<pre>";
	//echo count($row);
//}
//echo count($row);
	//die;

$get_event = $editprofile->getEventUpcoming();
$get_eventrec = $editprofile->getEventRecorded();

$get_levent = $editprofile->getEventUpcoming();
$get_leventrec = $editprofile->getEventRecorded();

$get_ceevent = $editprofile->getComEventUpcoming();
$get_ceeventrec = $editprofile->getComEventRecorded();

$get_lceevent = $editprofile->getComEventUpcoming();
$get_lceeventrec = $editprofile->getComEventRecorded();

$get_sh_event = $editprofile->getEventUpcoming();
$get_sh_eventrec = $editprofile->getEventRecorded();

$get_sh_levent = $editprofile->getEventUpcoming();
$get_sh_leventrec = $editprofile->getEventRecorded();

$get_sh_ceevent = $editprofile->getComEventUpcoming();
$get_sh_ceeventrec = $editprofile->getComEventRecorded();

$get_sh_lceevent = $editprofile->getComEventUpcoming();
$get_sh_lceeventrec = $editprofile->getComEventRecorded();
$get_all_countries = $editprofile->get_all_countries();
$get_all_states = $editprofile->get_all_states($newrow['country_id']);

//while($row = mysql_fetch_assoc($get_event))
//{
//	var_dump($row);
//}


if($get_artist_song_register!=null)
{
	$res_song = mysql_fetch_assoc($get_artist_song_register);
	//var_dump($res_song);
}
if($get_artist_video_register!=null)
{
	$res_video = mysql_fetch_assoc($get_artist_video_register);
	//var_dump($res_video);
}
if($get_artist_gallery_register!=null)
{
	$res_gal = mysql_fetch_assoc($get_artist_gallery_register);
	//var_dump($res_gal);
}
//echo $res;

/********Ends here ******/	

$sql5=$editprofile->AccProfileType();
$get_primary_type=$editprofile->Get_artist_primarytype();

$sql6=$editprofile->AccProfileSubType();
$sql7=$editprofile->AccProfileMetaType();
$getmeta=$editprofile->MetaType();
$get_dis_meta=$editprofile->Get_disp_MetaType();
/*while($res=mysql_fetch_assoc($get_dis_meta))
{
	var_dump($get_dis_meta);
}*/
//var_dump($get_dis_meta);
$arr=Array();
	if($getmeta !="" && $getmeta !=Null)
	{
		while($gmeta=mysql_fetch_assoc($getmeta))
		{
			$arr[]=$gmeta;
		}
	}
	
	$getproject=$editprofile->GetProject();
	$getproject_community=$newgeneral->selcommunity_project();
	$getevent=$editprofile->GetEvent();
	
	//$getname=$editprofile->Artists();
	$nameres=$editprofile->Artists();
	//var_dump($nameres);
// echo count($acc_event);
// echo "<pre>";

?>


<!--<select class="dropdown" id="showtype-select">
<?php
/*for($i=0;$i<count($arr);$i++)
{
?>
<option value="<?php echo $arr[$i]['type_id'];?>" chkval="<?php $qu = mysql_query("select name from type where type_id='".$arr[$i]['type_id']."'");
$name=mysql_fetch_array($qu);
echo $name[0];
?>"><?php echo $name[0];
?></option>
<?php
}
?>
</select>
<select class="dropdown" id="showsubtype-select">
<?php
for($i=0;$i<count($arr);$i++)
{
?>
<option value="<?php echo $arr[$i]['subtype_id'];?>" chkval="<?php $qu = mysql_query("select name from subtype where subtype_id='".$arr[$i]['subtype_id']."'");
$name=mysql_fetch_array($qu);
echo $name[0];
?>"><?php echo $name[0];
?></option>
<?php
}
?>
</select>
<select class="dropdown" id="showmetatype-select">
<?php
for($i=0;$i<count($arr);$i++)
{
?>
<option value="<?php echo $arr[$i]['metatype_id'];?>" chkval="<?php $qu = mysql_query("select name from meta_type where meta_id='".$arr[$i]['metatype_id']."'");
$name=mysql_fetch_array($qu);
echo $name[0];
?>"><?php echo $name[0];
?></option>
<?php
}*/
?>
</select>-->
<div id="outerContainer">
<!--<p><a href="artist_profile.php">View <?php //echo $newrow['name']; ?> Profile</a></p>-->
                <!--<p>
                <a href="../logout.php">Logout</a></p>
            </div>
		</div>
    </div>
  
  </div>-->
  <!--<link rel="stylesheet" type="text/css" media="screen" href="includes/purify.css" />-->
<!--css file for displaying types
<link rel="stylesheet" type="text/css" media="screen" href="includes/multiple_select.css" />
<!--css file for displaying types Ends here-->
<!--<script src='http://ajax.googleapis.com/ajax/libs/jquery/1.6.1/jquery.min.js'></script>
	<script src="ui/jquery-1.7.2.js"></script>
	<script src="ui/jquery.ui.core.js"></script>
	<script src="ui/jquery.ui.widget.js"></script>
	<script src="ui/jquery.ui.mouse.js"></script>
	<script src="ui/jquery.ui.resizable.js"></script>
<script src="includes/organictabs-jquery.js"></script>-->

<script>
	$(function() {
		$("#artistTab").organicTabs();
	});
</script>
<!--<link rel="stylesheet" href="includes/jquery.ui.all.css">
<script src="includes/jquery.ui.core.js"></script>
<script src="includes/jquery.ui.widget.js"></script>
<script src="includes/jquery.ui.datepicker.js"></script>
<script type="text/javascript" src="ckeditor.js"></script>
<script type="text/javascript" src="jquery.js"></script>
<script src="sample.js" type="text/javascript"></script>
<link href="sample.css" rel="stylesheet" type="text/css" />-->


 <script type="text/javascript">
	//<![CDATA[

$(function()
{
	var config = {
		toolbar:
		[
			['Bold', 'Italic', '-', 'NumberedList', 'BulletedList', '-', 'Link', 'Unlink'],
			['UIColor']
		]
	};

	// Initialize the editor.
	// Callback function can be passed and executed after full instance creation.
	$('.jquery_ckeditor').ckeditor(config);
	config.forcePasteAsPlainText = true;
});

	//]]>
	</script> 	
<!--Ends here-->
<script>
	$(function() {
		//$( "#datepicker, #datepickerB" ).datepicker();
		
		
	});
</script>
<script>
$(function() {
		$( "#popupContact" ).resizable();
	});
</script>


<script type="text/javascript">

function handleSelection(choice) {
//document.getElementById('select').disabled=true;
// if(choice=='venue')
	// {
	  // document.getElementById(choice).style.display="";
	  // document.getElementById('nodisplay').style.display="none";
	// }
	// else
	// {
	  // document.getElementById(choice).style.display="";
	  // document.getElementById('venue').style.display="none";
	// }
	
	if(choice=='all_profiles_share')
	{
		document.getElementById(choice).style.display="block";
		document.getElementById('projects_profiles_share').style.display="none";
		document.getElementById('events_profiles_share').style.display="none";
	}
	
	if(choice=='projects_profiles_share')
	{
		document.getElementById(choice).style.display="block";
		document.getElementById('all_profiles_share').style.display="none";
		document.getElementById('events_profiles_share').style.display="none";
	}
	
	if(choice=='events_profiles_share')
	{
		document.getElementById(choice).style.display="block";
		document.getElementById('all_profiles_share').style.display="none";
		document.getElementById('projects_profiles_share').style.display="none";
	}		
}
</script>
<script>
	$(function ()
    {
        $('#bio').keyup(function (e){
            if(e.keyCode == 13){
                var curr = getCaret(this);
                var val = $(this).val();
                var end = val.length;

                $(this).val( val.substr(0, curr) + '<br>' + val.substr(curr, end));
            }

        })
    });
    
    function getCaret(el) { 
        if (el.selectionStart) { 
            return el.selectionStart; 
        }
        else if (document.selection) { 
            el.focus(); 

            var r = document.selection.createRange(); 
            if (r == null) { 
                return 0; 
            } 

            var re = el.createTextRange(), 
            rc = re.duplicate(); 
            re.moveToBookmark(r.getBookmark()); 
            rc.setEndPoint('EndToStart', re); 

            return rc.text.length; 
        }  
        return 0; 
    }
</script>

<script type="text/javascript">
function forsale_check(x)
{
	if(x==1){
		$("#fan_club").css({"display":"block"});
	}
	if(x==0){
		$("#fan_club").css({"display":"none"});
	}
	/*if(x=="no"){
		//$("#member_error").click();
		//alert("For using this facility you have to be member of purify.");
		document.getElementById("type2").checked = false;
		document.getElementById("type1").checked = false;
	}*/
}
/*$(document).ready(function() {
		$("#member_error").fancybox({
		'width'				: '35%',
		'height'			: '15%',
		'autoScale'			: true,
		'transitionIn'		: 'none',
		'transitionOut'		: 'none',
		'type'				: 'iframe'
	});
});
 $("document").ready(function(){
bio1 = $('#bio').val();
if(bio1!="")
{
	 len = bio1.length - 9;
	 $("#count_for_bio").text(len + "");
	 var res_bio2=bio1.split("<p>");
	res_bio2=bio1.split("<p>");
	oldlen4 =len5 = oldlen5 =len6 = 0;
	if(res_bio2[2] != null){
		if(res_bio2[1] != null)
		{
			len1 = (res_bio2[1].length - 7);
			oldlen0 = (75 - len1);
			oldlen1 = (len2 + len1 + len3 + len4 + len5 + len6);
		}
		if(res_bio2[2] != null)
		{
			len2 = (res_bio2[2].length - 7);
			oldlen = (75 - len2);
			oldlen1 = (len2 + oldlen0 + len1 + len3 + len4 + len5 + len6);
		}
		if(res_bio2[3] != null)
		{
			len3 = (res_bio2[3].length - 7);
			oldlen2 = (75 - len3);
			oldlen1 = (len2 + oldlen + oldlen0 + len1 + len3 + len4 + len5 + len6);
		}
		if(res_bio2[4] != null)
		{
			len4 = (res_bio2[4].length - 6);
			oldlen3 = (75 - len4);
			oldlen1 = (len2 + oldlen + oldlen0 + len1 + oldlen2 + len3 + len4 + len5 + len6);
		}
		if(res_bio2[5] != null)
		{
			len5 = (res_bio2[5].length - 6);
			oldlen4 = (75 - len5);
			oldlen1 = (len2 + oldlen + oldlen0 + len1 + oldlen2 + len3 + oldlen3 + len4 + len5 + len6);
		}
		if(res_bio2[6] != null)
		{
			len6 = (res_bio2[6].length - 6);
			oldlen1 = (len2 + oldlen + oldlen0 + len1 + oldlen2 + len3 + oldlen3 + len4 + oldlen4 + len5 + len6);
		}
		$("#count_for_bio").text(oldlen1 + "");		
	}
}

var bio1,len,len1=0,len2=0,len3=0,len4=0,oldlen0=0,oldlen=0,oldlen1=0,oldlen2=0,oldlen3=0;
	var editor = $('#bio').ckeditorGet();
	editor.on( 'key', function(ev){
		 bio1 = $('#bio').val();

		len = bio1.length - 9;
		$("#count_for_bio").text(len + "");
		
		var res_bio1=bio1.split("<p>");
		var editor1 = $('#bio').ckeditorGet();
		editor1.document.on( 'keydown', function (evt)
		{
			var key_code = evt.data.getKey();
			
			if(key_code == 13)
			{
				res_bio1=bio1.split("<p>");
				if(res_bio1[1] != null)
				{
					len1 = (res_bio1[1].length - 7);
					oldlen0 = (75 - len1);
				}
				if(res_bio1[2] != null)
				{
					len2 = (res_bio1[2].length - 7);
					oldlen = (75 - len2);
				}
				if(res_bio1[3] != null)
				{
					len3 = (res_bio1[3].length - 7);
					oldlen2 = (75 - len3);
				}
				if(res_bio1[4] != null)
				{
					len4 = (res_bio1[4].length - 6);
					oldlen3 = (75 - len4);
				}
				oldlen1 = (len2 + oldlen + oldlen0 + len1 + oldlen2 + len3 + oldlen3 + len4);
				$("#count_for_bio").text(oldlen1 + "");
			}
			if(key_code == 46)
			{
				setTimeout(function(){
					bio1 = $('#bio').val();
					len = bio1.length - 9;
					$("#count_for_bio").text(len + "");
				},200);
			}
		});
		
		if(res_bio1.length > 2)
		{
			oldlen = (75 - (res_bio1[1].length - 7));
			oldlen1 = (res_bio1[1].length - 7) + oldlen;
			$("#count_for_bio").text((oldlen1 + (res_bio1[2].length - 6)) + "");
		}
		if(res_bio1.length > 3)
		{
			oldlen0 = (75 - (res_bio1[1].length - 7));
			oldlen = (75 - (res_bio1[2].length - 7));
			oldlen1 = (res_bio1[2].length - 7) + oldlen + oldlen0 + (res_bio1[1].length - 7);
			$("#count_for_bio").text((oldlen1 + (res_bio1[3].length - 6)) + "");
		}
		if(res_bio1.length > 4)
		{
			oldlen0 = (75 - (res_bio1[1].length - 7));
			oldlen = (75 - (res_bio1[2].length - 7));
			oldlen2 = (75 - (res_bio1[3].length - 7));
			oldlen1 = (res_bio1[2].length - 7) + oldlen + oldlen0 + (res_bio1[1].length - 7) + oldlen2 + (res_bio1[3].length - 7);
			$("#count_for_bio").text((oldlen1 + (res_bio1[4].length - 6)) + "");
		}
		if(res_bio1.length > 5)
		{
			oldlen0 = (75 - (res_bio1[1].length - 7));
			oldlen = (75 - (res_bio1[2].length - 7));
			oldlen2 = (75 - (res_bio1[3].length - 7));
			oldlen3 = (75 - (res_bio1[4].length - 7));
			oldlen1 = (res_bio1[2].length - 7) + oldlen + oldlen0 + (res_bio1[1].length - 7) + oldlen2 + (res_bio1[3].length - 7) + oldlen3 + (res_bio1[4].length - 7);
			$("#count_for_bio").text((oldlen1 + (res_bio1[5].length - 6)) + "");
		}
		if(res_bio1.length > 6)
		{
			oldlen0 = (75 - (res_bio1[1].length - 7));
			oldlen = (75 - (res_bio1[2].length - 7));
			oldlen2 = (75 - (res_bio1[3].length - 7));
			oldlen3 = (75 - (res_bio1[4].length - 7));
			oldlen4 = (75 - (res_bio1[5].length - 7));
			oldlen1 = (res_bio1[2].length - 7) + oldlen + oldlen0 + (res_bio1[1].length - 7) + oldlen2 + (res_bio1[3].length - 7) + oldlen3 + (res_bio1[4].length - 7)+ oldlen4 + (res_bio1[5].length - 7);
			$("#count_for_bio").text((oldlen1 + (res_bio1[6].length - 6)) + "");
		}
		if(res_bio1.length > 7)
		{
			$("#count_for_bio").text(400 + "");
		}
	});
}); */
function validate()
{
	var name = document.getElementById("name");
	if(name.value=="" || name.value==null)
	{
		alert("Please enter name.");
		return false;
	}
	var a = document.getElementById("selected-types");
	if(a.innerHTML =="" || a.innerHTML ==null)
	{
		alert("Please select at least one type and click the add button.");
		return false;
	}
	var url = document.getElementById("website").value;
	
	/* var patt1 =  new RegExp("^(https|https|ftp)\://([a-zA-Z0-9\.\-]+(\:[a-zA-Z0-9\.&amp;%\$\-]+)*@)*((25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9])\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9]|0)\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9]|0)\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[0-9])|([a-zA-Z0-9\-]+\.)*[a-zA-Z0-9\-]+\.(com|edu|gov|int|mil|net|org|biz|arpa|info|name|pro|aero|coop|museum|[a-zA-Z]{2}))(\:[0-9]+)*(/($|[a-zA-Z0-9\.\,\?\'\\\+&amp;%\$#\=~_\-]+))*$");
	
	if(url!="")
	{
		if (!patt1.test(url)) 
		{
			alert("Please Enter Valid Website.");
			return false;
		}
	} 

    var pattern = new RegExp("[A-Za-z0-9_\\-]+\\.[A-Za-z0-9_\\-\\%\\&\\?\\.\\=\\+]+$");
	
	if(url!="")
	{
		if (!pattern.test(url)) 
		{
			alert("Please Enter Valid Website.");
			return false;
		}
	}*/
	/*var url=document.getElementById("website").value;
	if(! (/^(www)((\.[A-Z0-9][A-Z0-9_-]*)+.(com|org|net|dk|at|us|tv|info|uk|co.uk|biz|se)$)(:(\d+))?\/?/i.test(url)))
	{
		alert("Please enter valid URL for website.");
		return false;
	}*/
	
/**********Ck Editor Validation *************/
/*
	var bio=$("#bio").val();
	var index;
	var res_bio=bio.split("<p>");

	/*if(bio=="")
	{
		alert("You Can Not Leave Description Blank");
		return false;
	}*/
	//alert(bio.length-10);
	/* if(bio.length-10 > 400)
	{
		alert("Please Enter Bio Less Than 400 Characters.");
		return false;
	} 
	if(res_bio.length-1 > 6)
	{
		alert("Please Enter only Six lines.");
		return false;
	}*/
/*
	if(res_bio.length-1 == 3)
	{
		for(index=1;index <= res_bio.length-1 ; index++)
		{
			if(res_bio[index].length-7 > 75)
			{
				alert("Please Enter only 75 characters in one line");
				return false;
			}
		}
	}
	*/
/***********Ck Editor Validation Ends Here**************/	


	
	var profile_url = $("#fieldCont_domain").html();
	var profile_field = document.getElementById("subdomain").value;
	if(profile_url == "Domain Is Not Available.")
	{
		alert("Please Choose Another Profile URL.");
		return false;
	}
	if(profile_field == "" || profile_field == " ")
	{
		alert("Please Choose Any Profile URL.");
		return false;
	}
	var profile_field_new = profile_field.trim();
    if (/\s/g.test(profile_field_new))
	{
        alert("Please Check Your Profile URL For Spaces.");
        return false;
    }
	
	var profile_image_new = document.getElementById("profile_image_new").getAttribute('src');
	if(profile_image_new.indexOf('Noimage')>0)
	{
		alert("Please select your Profile Picture.");
		return false;
	}
	
	var listing_image_new = document.getElementById("listing_image_new").getAttribute('src');
	if(listing_image_new.indexOf('Noimage')>0)
	{
		alert("Please select your Listing Picture.");
		return false;
	}
	
	var type1_chk = document.getElementById("type1");
	if(type1_chk.checked==true)
	{
		var price_chk = document.getElementById("fan_price").value;
		if(price_chk =="" || price_chk ==null){
			alert("Please Enter Price.");
			return false;
		}
		else if(price_chk==0)
		{
			alert('You can not have a price of $0.');
			return false;
		}
	}
}
</script>
<script>
var myUploader = null;
var myAudioUploader = null;
var iMaxUploadSize = 10485760; //10MB
	window.onload = function(){ 
		$.post("State.php", { country_id:$("#countrySelect").val() },
			function(data)
			{
					//alert("Data Loaded: " + data);											
					$("#stateSelect").html(data);
			}
		);
	}
	
	$("document").ready(function(){
$("#selectCountry").change(function ()
{
	$.post("State.php", { country_id:$("#selectCountry").val() },
		function(data)
		{
			$("#selectState").html(data);
		}
	);
});
});
	
	$(document).ready(
					function()
					{
						/*$("#addType").click(
							function () 
							{	
								if ($("#type-select").val()!=0 )
								{
									//alert("type selected");
									if(!$("div").hasClass($("#type-select").val()))
									{
									//alert("no div for type");
										$("#selected-types").append('<div class="'+$("#type-select").val()+' box1" id="'+$("#type-select").val()+'">'+'<input type="checkbox" name="type-checked" value="'+$("#type-select").val()+'" checked onclick="$(this).parent().remove()" />'+$("#type-select option:selected").attr("chkval")+'</div>');
									}
								
								}
								else
								{
										alert("Select PageType");
								}	
																
								//alert("hi");
								if($("#subtype-select").val()!=0)
								{
									//alert("subtype selected");
									if(!$("div").hasClass($("#type-select").val()+'_'+$("#subtype-select").val()))
									{
										//alert("no div for type subtype");
										//if($("div").hasClass($("#type-select").val())
										$("."+$("#type-select").val()).append('<div id="'+$("#subtype-select").val()+'" class="'+$("#type-select").val()+'_'+$("#subtype-select").val() + ' box1A"><input type="checkbox" name="subtype-checked" value="'+$("#type-select").val()+'_'+$("#subtype-select").val() + '" checked  onclick="$(this).parent().remove()"/>'+$("#subtype-select option:selected").attr("chkval")+'<br>'+'</div>');
									}
								}
								
								if($("#metatype-select").val()!=0)
								{
									//alert("metatype selected");
									if(!$("div").hasClass($("#type-select").val()+'_'+$("#subtype-select").val()+'_'+$("#metatype-select").val()))
									{
										//alert("no div for type subtype metatype");
										//if($("div").hasClass($("#type-select").val())
										$("."+$("#type-select").val()+'_'+$("#subtype-select").val()).append('<div class="'+$("#type-select").val()+'_'+$("#subtype-select").val()+'_'+$("#metatype-select").val() + ' box1B"><input type="checkbox" name="metatype_checked[]" value="'+$("#type-select").val()+'_'+$("#subtype-select").val()+'_'+$("#metatype-select").val() +'" checked  onclick="$(this).parent().remove()"/>'+$("#metatype-select option:selected").attr("chkval")+'</div>');
									}
								}
							}
						);*/
						
	//Meta type displaying starts here
	
       //meta type displaying ends here//
       	   
	   
	   $("#addType").click(function () 
       {
         //alert($("#type-select option:selected").length);
         //alert($("#subtype-select option:selected").length);
         //alert($("#metatype-select option:selected").length);
         //alert($("#type-select option:selected").attr("key"));
         if ($("#type-select").val()!=0 )
         {
          //alert("type selected");
			  if(!$("div").hasClass($("#type-select").val()))
			  {
				  //alert("no div for type");
				  //onclick="$(this).parent().remove()" /**This one is old line**/
				   $("#selected-types").append('<div class="'+$("#type-select").val()+' box1" id="'+$("#type-select").val()+'">'+'<input type="checkbox" name="type_array['+$("#type-select").val() +']" value="'+$("#type-select").val()+'" checked onclick="$(this).parent().remove()" />'+$("#type-select option:selected").attr("chkval")+'<input type="hidden" name="typeVals[]" id="typeVals[]" value="'+$("#type-select").val()+'_0_0"/></div>');
				  /* $("#selected-types-hide").append('<div style="display:none;" class="'+$("#type-select").val()+' box1 type-select-hide" id="'+$("#type-select").val()+'">'+'<input type="checkbox" name="type_array['+$("#type-select").val() +']" value="'+$("#type-select").val()+'" checked onclick="$(this).parent().remove()" />'+$("#type-select option:selected").attr("chkval")+'<input type="hidden" name="typeVals[]" value="'+$("#type-select").val()+'_0_0"/></div>');*/
				   
			  }
         
         }
         else
         {
			   alert("Select PageType");
         } 
                 
         //alert("hi");
         if($("#subtype-select").val()!=0)
         {
          //alert("subtype selected");
			  if(!$("div").hasClass($("#type-select").val()+'_'+$("#subtype-select").val()))
			  {
			   
			   $("."+$("#type-select").val()).append('<div id="'+$("#subtype-select").val()+'" class="'+$("#type-select").val()+'_'+$("#subtype-select").val() + ' box1A"><input type="checkbox" name="type_array['+$("#type-select").val() +']['+$("#subtype-select").val()+']" value="'+$("#type-select").val()+'_'+$("#subtype-select").val() + '" checked  onclick="$(this).parent().remove()"/>'+$("#subtype-select option:selected").attr("chkval")+'<br>'+'<input type="hidden" name="subTypeVals[]" value="'+$("#type-select").val()+'_'+$("#subtype-select").val()+'_0"/></div>');
			  /* $("#type-select-hide").append('<div style="display:none;" id="'+$("#subtype-select").val()+'" class="'+$("#type-select").val()+'_'+$("#subtype-select").val() + ' box1A subtype-select-hide"><input type="checkbox" name="type_array['+$("#type-select").val() +']['+$("#subtype-select").val()+']" value="'+$("#type-select").val()+'_'+$("#subtype-select").val() + '" checked  onclick="$(this).parent().remove()"/>'+$("#subtype-select option:selected").attr("chkval")+'<br>'+'<input type="hidden" name="subTypeVals[]" value="'+$("#type-select").val()+'_'+$("#subtype-select").val()+'_0"/></div>');*/
			  
			  }
         
         }
         
         if($("#metatype-select").val()!=0)
         {
          //alert("metatype selected");
			  if(!$("div").hasClass($("#type-select").val()+'_'+$("#subtype-select").val()+'_'+$("#metatype-select").val()))
			  {
			   if($("#metatype-select").val()== null)
			   {
				return false;
			   }
			   $("."+$("#type-select").val()+'_'+$("#subtype-select").val()).append('<div class="'+$("#type-select").val()+'_'+$("#subtype-select").val()+'_'+$("#metatype-select").val() + ' box1B"><input type="checkbox" name="metatype_checked[]" value="'+$("#type-select").val()+'_'+$("#subtype-select").val()+'_'+$("#metatype-select").val() +'" checked  onclick="$(this).parent().remove()"/>'+$("#metatype-select option:selected").attr("chkval")+'<input type="hidden" name="metaTypeVals[]" id="metaTypeVals[]" value="'+$("#type-select").val()+'_'+$("#subtype-select").val()+'_'+$("#metatype-select").val() +'"/></div>');
			  /* $("#subtype-select-hide").append('<div style="display:none;" class="'+$("#type-select").val()+'_'+$("#subtype-select").val()+'_'+$("#metatype-select").val() + ' box1B"><input type="checkbox" name="metatype_checked[]" value="'+$("#type-select").val()+'_'+$("#subtype-select").val()+'_'+$("#metatype-select").val() +'" checked  onclick="$(this).parent().remove()"/>'+$("#metatype-select option:selected").attr("chkval")+'<input type="hidden" name="metaTypeVals[]" value="'+$("#type-select").val()+'_'+$("#subtype-select").val()+'_'+$("#metatype-select").val() +'"/></div>');*/
			  
			  }
         
         }
                 
        });
	   
	   
						
						$("#type-select").change(
							function ()
							{
								//alert($("#type-select").val());
								$.post("registration_block_subtype.php", { type_id:$("#type-select").val() },
									function(data)
									{
									//alert("Data Loaded: " + data);
										//$("#metatype-select").html("<option value='0' selected='selected'>Select Metatype</option>");
										//$("#subtype-select").html(data);
										if(data == " " || data == null || data == "")
										{
											document.getElementById("subtype-select").disabled=true;
										}else{
											document.getElementById("subtype-select").disabled=false;
											$("#subtype-select").html(data);
										}
									}
								);
							}
						);
										
						$("#subtype-select").change(
							function ()
							{
								//alert($("#subtype-select").val());
								$.post("registration_block_metatype.php", { subtype_id:$("#subtype-select").val() },
									function(data)
									{
										//alert("Data Loaded: " + data);
										//$("#metatype-select").html(data);
										if(data == " " || data == null || data == "")
										{
											document.getElementById("metatype-select").disabled=true;
										}else{
											document.getElementById("metatype-select").disabled=false;
											$("#metatype-select").html(data);
										}
									}
								);
							}	
						);
							//Select State o change in values of Country
							
						$("#countrySelect").change(
							function ()
							{
								//alert($("#type-select").val());
								$.post("State.php", { country_id:$("#countrySelect").val() },
									function(data)
									{
									//alert("Data Loaded: " + data);											
										$("#stateSelect").html(data);
									}
								);
							}
						);
							
						$('#selectuploadtype').change(
							function() 
							{
								$("#imgUploadDiv").fadeOut();
								$("#audioUploadDiv").fadeOut();
								$("#videoUploadDiv").fadeOut();
								$("#audiofileform").fadeOut();
								$("#audiolinkform").fadeOut();
								  var val = $('#selectuploadtype').val();
								switch(val)
								{
									
									case 'Image':
											$("#imgUploadDiv").fadeIn();
											$("#uploadedImages").val('');
											clearFields();
										if(!myUploader){
											myUploader = {
													uploadify : function(){
														$('#file_upload').uploadify({
															'uploader'  : './uploadify/uploadify.swf',
															'script'    : './uploadify/uploadFiles.php',
															'cancelImg' : './uploadify/cancel.png',
															'folder'    : './uploads/',
															'auto'      : true,
															'multi'		: true,
															'removeCompleted' : true,
															'wmode'		: 'transparent',
															'buttonText': 'Upload Gallery',
															'fileExt'     : '*.jpg;*.gif;*.png',
															'fileDesc'    : 'Image Files',
															'simUploadLimit' : 4,
															'sizeLimit'	: iMaxUploadSize, //10 MB size
															'onComplete': function(event, ID, fileObj, response, data) {
																// On File Upload Completion			
																//location = 'uploadtos3.php?uploads=complete';
																$("#uploadedImages").val($("#uploadedImages").val() + response + "|");
															},
															'onSelect' : function (event, ID, fileObj){
															},
															'onSelectOnce' : function(event, data)
															{
																// This function fires after onSelect
																// Checks total size of queue and warns user if too big
																iTotFileSize = data.allBytesTotal;
																//alert("iTotFileSize = " + iTotFileSize);
																if(iTotFileSize >= iMaxUploadSize){
																	var iOverSize = (iTotFileSize - iMaxUploadSize) / 1024 / 1024; // Size, in MB
																	//alert("Total file size in queue exceeds maximum 10MB allowed.  Please remove " + Math.ceil(iOverSize) + " MB worth.");
																	$("#divGalleryFileSize").show().html("Maximum 10MB allowed. Total queue size approximately: <font color='red'>" + Math.ceil(iTotFileSize/1024/1024) + " (MB)</font>");
																	$('#file_upload').uploadifyClearQueue();
																}
																else
																{
																   $("#divGalleryFileSize").hide() 
																}
															},
															'onOpen'	: function() {
																//hide overly
															}
															/*,
															'onError'     : function (event,ID,fileObj,errorObj) {
															  alert(errorObj.type + ' Error: ' + errorObj.info);
															},
															'onProgress'  : function(event,ID,fileObj,data) {
															  var bytes = Math.round(data.bytesLoaded / 1024);
															  $('#containerResult').html(' - ' + bytes + 'KB Uploaded');
															  return false;
															}*/

															});
														}
													};
													myUploader.uploadify();
												}
											break;
									case 'Audio':
											clearFields();
											$("#audioUploadDiv").fadeIn();
											break;
									case 'Video':
											clearFields();
											$("#videoUploadDiv").fadeIn();
											break;
									default :
											alert("Default");
											break;
											
								}
							}
						);

						
						$("input[name^=audiouploadoption]").click(
							function()
							{
								$("#audiofileform").fadeOut();
								$("#audiolinkform").fadeOut();
								$("#uploadedAudio").val('');
								if($(this).val()=="file"){
									$("#audiofileform").fadeIn();
									if(!myAudioUploader){
									myAudioUploader = {
										uploadify : function(){
											$('#file_upload_audio').uploadify({
												'uploader'  : './uploadify/uploadify.swf',
												'script'    : './uploadify/uploadFilesAudio.php',
												'cancelImg' : './uploadify/cancel.png',
												'folder'    : './uploads/',
												'auto'      : true,
												'multi'		: false,
												'removeCompleted' : true,
												'wmode'		: 'transparent',
												'buttonText': 'Upload Media',
												'fileExt'     : '*.mp3',
												'fileDesc'    : 'Audio Files',
												'simUploadLimit' : 4,
												'sizeLimit'	: iMaxUploadSize, //10 MB size
												'onComplete': function(event, ID, fileObj, response, data) {
													// On File Upload Completion			
													//location = 'uploadtos3.php?uploads=complete';
													alert(response);
													$("#uploadedAudio").val($("#uploadedAudio").val() + response + "|");
												},
												'onSelectOnce' : function(event, data)
												{
													// This function fires after onSelect
													// Checks total size of queue and warns user if too big
													iTotFileSize = data.allBytesTotal;
													//alert("iTotFileSize = " + iTotFileSize);
													if(iTotFileSize >= iMaxUploadSize){
														var iOverSize = (iTotFileSize - iMaxUploadSize) / 1024 / 1024; // Size, in MB
														//alert("Total file size in queue exceeds maximum 10MB allowed.  Please remove " + Math.ceil(iOverSize) + " MB worth.");
														$("#divAudioFileSize").show().html("Maximum 10MB allowed. Total queue size approximately: <font color='red'>" + Math.ceil(iTotFileSize/1024/1024) + " (MB)</font>");
														$('#file_upload_audio').uploadifyClearQueue();
													}
													else
													{
													   $("#divGalleryFileSize").hide() 
													}
												},
												'onOpen'	: function() {
													//hide overly
												}
												/*,
												'onError'     : function (event,ID,fileObj,errorObj) {
												  alert(errorObj.type + ' Error: ' + errorObj.info);
												},
												'onProgress'  : function(event,ID,fileObj,data) {
												  var bytes = Math.round(data.bytesLoaded / 1024);
												  $('#containerResult').html(' - ' + bytes + 'KB Uploaded');
												  return false;
												}*/

												});
											}
										};
										myAudioUploader.uploadify();
									}
								}else if($(this).val()=="link"){
									$("#audiolinkform").fadeIn();							
								}
							}
						);

						$("#chk_avail_link").click(
							function()
							{
								$.post("registration_user_availablity.php", { name:$("#username").val() },
								function(data)
								{
									$("#user_name_msg").html(data);
									$("#register").focus();	
									$("#username").focus();																	
								}
								);
							}
						);	
						$("#chk_avail_link1").click(
							function()
							{
								$.post("registration_artist_email.php", { email:$("#email").val() },
								function(data)
								{
									$("#user_email_msg").html(data);
									$("#register").focus();	
									$("#email").focus();																	
								}
								);
							}
						);			
		
						$("#register_btn").click(
							function()
							{
								var v = validate();
								if(v){
									$('#selected-types').clone().appendTo('#selected-types-hide');
									document.registration_form.register.value = "1";
									document.registration_form.submit();
								}
							}
						);

						
						$("#audioLinkUrl").blur(
							function()
							{
								clearFields();
								$("#uploadedAudioLink").val($("#audioLinkUrl").val());
								$("#uploadedAudioLinkTitle").val($("#audioLinkName").val());
							}
						);

						$("#videoLinkUrl").blur(
							function()
							{
								clearFields();
								$("#uploadedVideoLink").val($("#videoLinkUrl").val());
								$("#uploadedVideoLinkTitle").val($("#videoLinkName").val());
							}
						);

						$("#audioLinkName").blur(
							function()
							{
								clearFields();
								$("#uploadedAudioLink").val($("#audioLinkUrl").val());
								$("#uploadedAudioLinkTitle").val($("#audioLinkName").val());
							}
						);

						$("#videoLinkName").blur(
							function()
							{
								clearFields();
								$("#uploadedVideoLink").val($("#videoLinkUrl").val());
								$("#uploadedVideoLinkTitle").val($("#videoLinkName").val());
							}
						);

						$("#audioFileName").blur(
							function()
							{
								var v = $("#uploadedAudio").val();
								clearFields();
								$("#uploadedAudio").val(v);
								$("#audioFileNameTitle").val($("#audioFileName").val());
							}
						);
						$("#galleryFileName").blur(
							function()
							{
								var v = $("#uploadedImages").val();
								clearFields();
								$("#uploadedImages").val(v);
								$("#galleryFileNameTitle").val($("#galleryFileName").val());
							}
						);

						/*$("#mediatype").change(
							function ()
							{
								alert($("#mediatype option:checked").val);
								switch($("#mediatype option:checked").val)
								{
									
									case 'Image':
											alert("Image");
											break;
									case 'Audio':
											alert("Audio");
											break;
									case 'Video':
											alert("Audio");
											break;
									default :
											alert("Default");
											break;
											
								}
								
							}	
						);*/	

					}
				);

function clearFields(){
	$("#uploadedImages").val('');
	$("#uploadedAudio").val('');
	$("#uploadedAudioLink").val('');
	$("#uploadedVideoLink").val('');
	$("#uploadedAudioLinkTitle").val('');
	$("#uploadedVideoLinkTitle").val('');
	$("#audioFileNameTitle").val('');
	$("#galleryFileNameTitle").val('');
}
</script>


<script type="text/javascript">
 /*function selectmedia()
{
	var select2=document.getElementById("featured_media").value;
	window.location = "profileedit_artist.php?sel_val="+document.getElementById("featured_media").value;
} */
$("document").ready(function(){

	 $("#featured_media").change(function() 
    {
	//alert("sss");
	var selected = $("#featured_media").val();
	var dataString = 'reg='+ selected;
	$("#loader").show();
	$("#fieldCont_media").hide();
	 $.ajax({	 
		
			type: 'POST',
            url : 'ArtistAjaxactions.php',
            data: dataString,
            success: function(data) 
			{
                $("#fieldCont_media").html(data);
				$("#loader").hide();
				$("#fieldCont_media").show();
			}
	});
});

$("#subdomain").blur(function(){ 
		var text_value = document.getElementById("subdomain").value;
		//alert(text_value);
		
		var text_data = 'reg='+ text_value;
		
	 $.ajax({	 
		
			type: 'POST',
            url : 'ArtistCheckDomain.php',
            data: text_data,
            success: function(data) 
			{
                $("#fieldCont_domain").html(data);
			}
		
		
	});

	});
	
});

function selected_feature(sel)
{
	var selected = sel;
	var dataString = 'reg='+ selected;
	$("#loader").show();
	$("#fieldCont_media").hide();
	 $.ajax({	 
		
			type: 'POST',
            url : 'ArtistAjaxactions.php',
            data: dataString,
            success: function(data) 
			{
                $("#fieldCont_media").html(data);
				$("#loader").hide();
				$("#fieldCont_media").show();
			}
		
		
	});

}
function confirmdelete(x)
{
	var r =confirm("Are You Sure You Want To Delete "+x+" ?");
	if(r==false)
	{
		return false;
	}
}
function chk_profile_url(s)
{
	if(s=="")
	{
		aletr("To View The Profile You Have To Enter Profile URL.");
		return false;
	}
}
</script>
<script type="text/javascript">
$("document").ready(function(){
<?php 
for($i=0;$i<count($arr);$i++)
{
?>
if(!$("div").hasClass(<?php echo $arr[$i]['type_id'];?>))
{
	<?php
	$qu = $editprofile->DisplayType($arr[$i]['type_id']);
	$name=mysql_fetch_array($qu);
	if($name[0]!="")
	{
	?>
		$("#selected-types").append('<div class="'
		+<?php echo $arr[$i]['type_id'];?>+' box1" id="'
		+<?php echo $arr[$i]['type_id'];?>+'">'
		+'<input type="checkbox" name="type_array['
		+<?php echo $arr[$i]['type_id'];?>+']" value="'
		+<?php echo $arr[$i]['type_id'];?>
		+'" checked onclick="$(this).parent().remove()" />'
		+"<?php echo $name[0];
		//echo $arr[$i]['type_id'];
		//$(this).parent().remove() /*This one is old line*/
		?>"
		+'<input type="hidden" name="typeVals[]" value="'
		+<?php echo $arr[$i]['type_id'];?>+'_0_0"/></div>');
	<?php
	}
	?>
}

if(!$("div").hasClass(<?php echo $arr[$i]['type_id'];?>+'_'+<?php echo $arr[$i]['subtype_id'];?>))
{
	<?php 
	$qu = $editprofile->DisplaySubType($arr[$i]['subtype_id']);
	$name=mysql_fetch_array($qu);
	if($name[0]!="")
	{
	?>
		 $("."+<?php echo $arr[$i]['type_id'];?>).append('<div id="'
		 +<?php echo $arr[$i]['subtype_id'];?>+'" class="'
		 +<?php echo $arr[$i]['type_id'];?>+'_'+<?php echo $arr[$i]['subtype_id'];?>
		 + ' box1A"><input type="checkbox" name="type_array['+<?php echo $arr[$i]['type_id'];?>
		 +']['+<?php echo $arr[$i]['subtype_id'];?>
		 +']" value="'+$("#showtype-select").val()
		 +'_'+<?php echo $arr[$i]['subtype_id'];?>
		 + '" checked  onclick="$(this).parent().remove()"/>'
		 +"<?php echo $name[0];
			//echo $arr[$i]['subtype_id'];
			?>"
		 +'<br>'+'<input type="hidden" name="subTypeVals[]" value="'
		 +<?php echo $arr[$i]['type_id'];?>+'_'+<?php echo $arr[$i]['subtype_id'];?>+'_0"/></div>');
	<?php
	}
	?>
}
if(!$("div").hasClass(<?php echo $arr[$i]['type_id'];?>+'_'+<?php echo $arr[$i]['subtype_id'];?>+'_'+<?php echo $arr[$i]['metatype_id'];?>))
{
<?php 
	$qu = $editprofile->DisplayMetaType($arr[$i]['metatype_id']);
	$name=mysql_fetch_array($qu);
	if($name[0]!=null)
	{
	?>
		$("."+<?php echo $arr[$i]['type_id'];?>+'_'
		+<?php echo $arr[$i]['subtype_id'];?>).append('<div class="'
		+<?php echo $arr[$i]['type_id'];?>+'_'+<?php echo $arr[$i]['subtype_id'];?>
		+'_'+<?php echo $arr[$i]['metatype_id'];?>
		+ ' box1B"><input type="checkbox" name="metatype_checked[]" value="'
		+<?php echo $arr[$i]['type_id'];?>+'_'+<?php echo $arr[$i]['subtype_id'];?>
		+'_'+<?php echo $arr[$i]['metatype_id'];?>
		+'" checked  onclick="$(this).parent().remove()"/>'
		+"<?php echo $name[0];
			//echo $arr[$i]['metatype_id'];
			?>"
		+'<input type="hidden" name="metaTypeVals[]" value="'
		+<?php echo $arr[$i]['type_id'];?>+'_'+<?php echo $arr[$i]['subtype_id'];?>
		+'_'+<?php echo $arr[$i]['metatype_id'];?>+'"/></div>');
	<?php
	}
	?>
}
<?php
}
?>
});
</script>
<script src="chk_form_change.js"></script>
  
  
  
  
  
  
  
  <div id="toplevelNav"></div>
  <div>
      <div id="profileTabs">
          <ul>
            <?php
		  
		   $new_artist=$newtab->artist_status();
		   $new_community=$newtab->community_status();
		   $new_team=$newtab->team_status();
		   
		 if(($newres1['artist_id']==0 && $new_artist['status']==1) && $newres1['member_id']==0 && ($newres1['community_id']==0 && $new_community['status']==1) && ($newres1['team_id']==0 && $new_team['status']==1))
		  {		  
		  ?>
            <li><a href="profileedit.php">HOME</a></li>		
            <!--<li><a href="profileedit_support.php">SUPPORT</a></li>-->
		   <?php
		   }
		   elseif(($newres1['artist_id']!=0 && $new_artist['status']==0 && $new_artist['active']==0) && ($newres1['community_id']!=0 && $new_community['status']==0 && $new_community['active']==0) && ($newres1['team_id']!=0 && $new_team['status']==0))
		   {
		   ?>
		   <li><a href="profileedit.php">HOME</a></li>
		   <li><a href="profileedit_media.php">MEDIA</a></li>
		   <li class="active">ARTIST</li>
		   <!--<li><a href="profileedit_community.php">COMPANY</a></li>-->
		   <li><a href="profileedit_community.php">COMPANY</a></li>
		   <li><a href="profileedit_team.php">TEAM</a></li>
           <!--<li><a href="profileedit_support.php">SUPPORT</a></li>-->
		   <?php
		   }
		  elseif(($newres1['artist_id']!=0 && $new_artist['status']==0 && $new_artist['active']==0) && ($newres1['community_id']!=0 && $new_community['status']==0 && $new_community['active']==0))
		   {
		   ?>
		   <li><a href="profileedit.php">HOME</a></li>	
<li><a href="profileedit_media.php">MEDIA</a></li>		   
		    <li class="active">ARTIST</li>
		   <!--<li><a href="profileedit_community.php">COMPANY</a></li>-->
		   <li><a href="profileedit_community.php">COMPANY</a></li>
           <!--<li><a href="profileedit_support.php">SUPPORT</a></li>-->
		   <?php
		   }
		    elseif(($newres1['artist_id']!=0 && $new_artist['status']==0 && $new_artist['active']==0) && ($newres1['team_id']!=0 && $new_team['status']==0))
		   {
		   ?>
			<li><a href="profileedit.php">HOME</a></li>		
			<li><a href="profileedit_media.php">MEDIA</a></li>
		    <li class="active">ARTIST</li>
			<li><a href="profileedit_team.php">TEAM</a></li>
           <!--<li><a href="profileedit_support.php">SUPPORT</a></li>-->
		   <?php
		   }
		  elseif(($newres1['community_id']!=0 && $new_community['status']==0 && $new_community['active']==0) && ($newres1['team_id']!=0 && $new_team['status']==0))
		   {
		   ?>
		   <li><a href="profileedit.php">HOME</a></li>
		   <li><a href="profileedit_media.php">MEDIA</a></li>
		   <!--<li><a href="profileedit_community.php">COMPANY</a></li>-->
			<li><a href="profileedit_community.php">COMPANY</a></li>
		   <li><a href="profileedit_team.php">TEAM</a></li>
           <!--<li><a href="profileedit_support.php">SUPPORT</a></li>-->
		   <?php
		   }
		    elseif($newres1['artist_id']!=0 && $new_artist['status']==0 && $new_artist['active']==0)
		   {?>
		    <li><a href="profileedit.php">HOME</a></li>		
			<li><a href="profileedit_media.php">MEDIA</a></li>
			<li class="active">ARTIST</li>
            <!--<li><a href="profileedit_support.php">SUPPORT</a></li>-->
		   <?php
		   }
		   elseif($newres1['member_id']!=0)
		   {
		   ?>
		   <?php
		   }
		    elseif($newres1['community_id']!=0 && $new_community['status']==0 && $new_community['active']==0)
		   {
		   ?>
		    <li><a href="profileedit.php">HOME</a></li>		
			<li><a href="profileedit_media.php">MEDIA</a></li>
			<!--<li><a href="profileedit_community.php">COMPANY</a></li>-->
			<li><a href="profileedit_community.php">COMPANY</a></li>
            <!--<li><a href="profileedit_support.php">SUPPORT</a></li>-->
		   <?php
		   }
		    elseif($newres1['team_id']!=0 && $new_team['status']==0)
		   {
		   ?>
		    <li><a href="profileedit.php">HOME</a></li>	
<li><a href="profileedit_media.php">MEDIA</a></li>			
			<li><a href="profileedit_team.php">TEAM</a></li>
            <!--<li><a href="profileedit_support.php">SUPPORT</a></li>-->
`		   <?php
		   }
		   else
		   {
		   ?>
			<li class="active">HOME</li>		
            <!--<li><a href="profileedit_support.php">SUPPORT</a></li>-->
		   <?php
		   }
		   
			?>
          </ul>
      </div>
<script type="text/javascript">
function confirm_saving()
{
	var r=confirm("Do you want to save your changes.");
	if (r==true)
	{
		submit_form();
	}
}
function submit_form()
{
	var a=document.getElementById("clickbutton");
	a.click();
	//document.forms["registration_form"].submit();
}

</script>
      <div id="artistTab">
      	<div id="subNavigation" class="tabs">
              <ul class="nav">
                <li><a href="#profile" class="current">Profile</a></li>
                <li><a href="#event" class="current">Events</a></li>
               <!-- <li><a href="#services">Add Service</a></li>-->
                <!--<li><a href="#memberships">Fan Club</a></li>-->
                <li><a href="#projects">Projects</a></li>
                <li><a href="#promotions">Promotions</a></li>
              </ul>
            </div>
      	<div class="list-wrap">
            	<div class="subTabs" id="profile" style="display:none;">
                	<h1> <?php echo $newrow['name']; ?> Profile</h1>
					<!-- Files adde for fancy box2-->
					<script type="text/javascript" src="new_fancy_box/jquery.fancybox.js?v=2.1.4"></script>
					<link rel="stylesheet" type="text/css" href="new_fancy_box/jquery.fancybox.css?v=2.1.4" media="screen" />
					<script type="text/javascript" src="new_fancy_box/helpers/jquery.fancybox-media.js?v=1.0.5"></script>
					<!-- Files adde for fancy box2 ends here-->
					<div id="mediaContent">
                    	<div class="topLinks" style="border-bottom:1px dotted #999;">
                            <div class="links" style="float:left;">
								<ul>
									<li><a href="/<?php echo $newrow['profile_url']; ?>" onclick ="return check_changed('<?php echo $newrow['profile_url']; ?>');"><input type="button" value="View Profile" style="border-radius:5px; -webkit-border-radius:5px; -moz-border-radius:5px; -ms-border-radius:5px; -o-border-radius:5px; background-color: #000; border: 0 none; color: #FFFFFF; font-family: 'futura-pt',sans-serif; font-size: 0.825em; font-weight: bold; padding: 6px 10px; float:left; margin-right:10px;" /></a></li>
									<?php
									if(isset($_GET['id']) && $_GET['id']!="")
									{
									?>
									<li><input type="button" value=" Save " style="border-radius:5px; -webkit-border-radius:5px; -moz-border-radius:5px; -ms-border-radius:5px; -o-border-radius:5px; background-color: #000; border: 0 none; color: #FFFFFF; font-family: 'futura-pt',sans-serif; font-size: 0.825em; font-weight: bold; padding: 6px 10px; float:left;"/></li>
									<?php
									}
									else
									{
									?>
									<li><input type="button" value=" Save " onclick="submit_form()" style="border-radius:5px; -webkit-border-radius:5px; -moz-border-radius:5px; -ms-border-radius:5px; -o-border-radius:5px; background-color: #000; border: 0 none; color: #FFFFFF; font-family: 'futura-pt',sans-serif; font-size: 0.825em; font-weight: bold; padding: 6px 10px; float:left;"/></li>
									<?php
									}
									?>
									
								</ul>
							</div>
						</div>
					</div>
                	<div style="width:665px; float:left;">
						<!--<div id="actualContent">-->
						<div id="actualContent">
						 <div class="fieldCont">
                            <div title="Select an image to crop and display on your profile page." class="fieldTitle">*Profile Picture</div>
							<?php
							 //echo $res1['image_name'];
							 
								if(isset($newrow['image_name']))
								{
									$name_image = $newrow['image_name'];
									//$new_image = strpos($name_image,'bnail');
								}
								else
								{
									$new_image = "";
								}
							 ?>
							
							
							<img id="profile_image_new" src="http://artjcropprofile.s3.amazonaws.com/<?php 
							if($newrow['image_name']=="")
							{
								echo "Noimage.png";
								//var_dump($newrow['image_name']);
								//var_dump($new_image);
							}
							else
							{
								echo $newrow['image_name'];
							}	
							?>" width="100"/>
							<?php include('jcrop/artist.php'); ?>
						 </div>
						<div class="fieldCont">
                             <div title="Select an image to crop and display on when your profile is listed on other profile pages and in the search engine." class="fieldTitle">*Listing Picture</div>
							 <img width="100" id="listing_image_new" src="http://artjcropthumb.s3.amazonaws.com/<?php if($newrow['listing_image_name']==""){echo "Noimage.png";}else { echo $newrow['listing_image_name'];}?>" />
   							 <?php include('jcrop/artistThumb.php'); ?>					
						</div>
                          <form id="registration_form" name="registration_form" method="post" action="add_general_artist.php" enctype="multipart/form-data" onsubmit="return validate()">
                    <input type="submit" id="clickbutton" value="save" style="display:none"/>
                                <div class="fieldCont">
                                    <div id="user_email_msg" class="hint" style="color:#FF0000">
                                                  
                                    </div>           
                                </div>
								<?php
								if(isset($thumb_image_name))
								{
								?>
								<input type="hidden" name="pro_pic" id="pro_pic" value="<?php echo $thumb_image_name; ?>">
                                <?php
								}
								
								?>
								<div class="fieldCont">
                                  <div title="Select a unique name to display on your profile page and across the site when listed." class="fieldTitle">*Name</div>
                                  <input title="Select a unique name to display on your profile page and across the site when listed." name="name" type="text" class="fieldText" id="name" value="<?php echo $newrow['name']; ?>">
                                  <div class="hint">Full Artist Name</div>
                                </div>
                                <div class="fieldCont">
                                                </div>
								
								<!--<div class="fieldCont">
                                  <div class="fieldTitle">Email</div>
                                  <input name="city" type="text" class="fieldText" disabled="disabled" id="city" value="<?php //echo $_SESSION['login_email']; ?>">
                                </div>-->
								
								<div class="fieldCont">
                                  <div title="Select a unique URL to be used for accessing your profile page." class="fieldTitle">*Profile URL</div>
								  <div class="urlTitle"><?php echo $domainname; ?>/</div>
                                  <input title="Select a unique URL to be used for accessing your profile page." name="subdomain" type="text" class="urlField" id="subdomain" value="<?php echo $newrow['profile_url']; ?>" />
								  <div class="hintnew"><span class="hintR" id="fieldCont_domain"></span>(<?php echo $domainname; ?>/leerick)</div>
                                </div>
								
								<div class="fieldCont">
                                  <div title="Add your own website to link to from your profile page." class="fieldTitle">Website</div>
								  <?php
								  if($newrow['homepage']=="")
								  {
								  ?>
									<div class="urlTitle">http://www.</div>
								  <?php
								  }
								  ?>
								  
                                  <input title="Add your own website to link to from your profile page." name="website" type="text" class="<?php if(isset($newrow['homepage']) && $newrow['homepage']!=""){echo "fieldText";}else{echo "urlField";}?>" id="website" value="<?php if(isset($newrow['homepage']) && $newrow['homepage']!=""){ if(strpos($newrow['homepage'],':')>0){ echo $newrow['homepage'];}else {echo "http://www.".$newrow['homepage'];}}?>">
								  <div class="hintnew">Example : <?php echo $domainname; ?></div>
                                </div>
								
								<div class="fieldCont">
                                <div class="fieldTitle">Country</div>
                                <select name="selectCountry" id="selectCountry" class="dropdown">
									<option value="0">Select</option>
                                    <?php
									while($get_country = mysql_fetch_assoc($get_all_countries))
									{
										if($newrow['country_id'] == $get_country['country_id'])
										{	
										?>
											<option value="<?php echo $get_country['country_id']?>" Selected><?php echo $get_country['country_name'];?></option>
										<?php	
										}
									?>
										<option value="<?php echo $get_country['country_id']?>"><?php echo $get_country['country_name'];?></option>
									<?php
									}
									
									?>
                                </select>
                            </div>
                            <div class="fieldCont">
                                <div class="fieldTitle">State / Province</div>
                                <select name="selectState" id="selectState" class="dropdown">
									<option value="0">Select State</option>
                                    <?php
									//if(isset($_GET['id']))
									//{
										while($getstate = mysql_fetch_assoc($get_all_states))
										{
											if($getstate['state_id'] == $newrow['state_id'])
											{
										?>
												<option value="<?php echo $getstate['state_id']; ?>" selected><?php echo $getstate['state_name']; ?></option>
										<?php
											}
											else
											{
											?>
												<option value="<?php echo $getstate['state_id']; ?>" ><?php echo $getstate['state_name']; ?></option>
											<?php
											}
										}
									//}
									?>
                                </select>
                            </div>
                            <div class="fieldCont">
                                <div class="fieldTitle">City</div>
                                <input name="editCity" id="editCity" class="fieldText" value="<?php echo $newrow['city']; ?>"/>
                                </input>
                            </div>
							
								<div class="fieldCont">
                                  <div title="Select a media file to feature on your profile page and when your profile is listed on other profiles and in search engine." class="fieldTitle">Featured Media</div>
                                 <select title="Select a media file to feature on your profile page and when your profile is listed on other profiles and in search engine." name="featured_media" class="dropdown" id="featured_media">
                                    <option value="nodisplay" >Select Featured Media</option>
                                    <option value="Gallery" <?php if(isset($newrow['featured_media']) && $newrow['featured_media']=="Gallery") echo "selected";?> >Gallery</option>	
                                    <option value="Song" <?php if(isset($newrow['featured_media']) && $newrow['featured_media']=="Song") echo "selected";?>  >Song</option>
                                    <option value="Video" <?php if(isset($newrow['featured_media']) && $newrow['featured_media']=="Video") echo "selected";?>  >Video</option>		
                                    <!--<option value="Channel" <?php /*if(isset($newrow['featured_media']) && $newrow['featured_media']=="Channel") echo "selected";*/?>  >Channel</option>-->
                                </select>
								<div id="loader" style="display:none;">
									<img src="images/ajax_loader_large.gif" style="height: 25px; position: relative; right: 120px; top: 34px; width: 25px;">
								</div>
								<div id="fieldCont_media" style="display:none;">
								</div>
                                </div>
								<?php 
								if(isset($newrow['featured_media']))
								{
								?>
								<script type="text/javascript">
									selected_feature("<?php echo $newrow['featured_media'];?>");
								</script>
								<?php
								}
								
								?>
								
								<div class="fieldCont">
                                  <div class="fieldTitle">Bio</div>
                                  <!--<input name="bio" type="text" class="fieldText" id="bio" value="<?php //echo $res1['bio'];?>">-->
								  <textarea spellcheck="true" class="" cols="65" id="bio" name="bio" rows="10"><?php echo $newrow['bio'];?></textarea>
								  <!--<div id="count_for_bio" class="counts"></div>-->
                                </div>
								  
								
								
                                       <div class="fieldCont">
									 
								 <h4>Type Association</h4>
								 
							 
<div class="fieldCont" style="font-size:12px;">Select type associations to represent your profile. Your first selections will display on your profile page, all others will be used for search engines. If you would like to change your display than remove all types and select your desired display type first.</br></br>Suggest a new type or subtype by emailing info@purifyart.com</div>

<!--<span class="hintp">&nbsp;&nbsp;&nbsp;&nbsp;Suggest a new type or subtype by emailing info@purifyart.com</span>									-->
                              <div class="fieldTitle">*Type</div>
                              <select name="type-select" class="dropdown" id="type-select">
							  <option value="0">Select Primary Type</option>
                                        <!--<option value="0" selected="selected">Select Profiletype</option>-->
                     <!--<option value="98" chkval="Material Artist">Material Artist</option>  <option value="154" chkval="Musician">Musician</option>  <option value="158" chkval="Performing Artist">Performing Artist</option>  <option value="157" chkval="Video Artist">Video Artist</option>  <option value="155" chkval="Visual Artist">Visual Artist</option>-->
					 <!--<option value="<?php// echo $type; ?>" chkval="<?php //echo $newrow3['name']; ?>"><?php //echo $newrow3['name']; ?></option>-->
							 <?php
								if($sql5 !="" && $sql5 !=Null)
								{
									while($newrow5=mysql_fetch_assoc($sql5))
									{
										/*if($get_primary_type['type_id']==$newrow5['type_id'])
										{
									?>
											<option value="<?php echo $newrow5['type_id']; ?>" selected="selected" chkval="<?php echo $newrow5['name']; ?>"><?php echo $newrow5['name']; ?></option>
									<?php
										}
										else
										{*/
										?>
											<option value="<?php echo $newrow5['type_id']; ?>"  chkval="<?php echo $newrow5['name']; ?>"><?php echo $newrow5['name']; ?></option>
									<?php
										//}
									}
								}
							 ?>
							 </select>
                              
                              
                            </div>
							<div  id="selected-types" style="float:right;"></div>
                                <div class="fieldCont">
                                  <div class="fieldTitle">Sub Type</div>
								  <select name="select-category-artist-subtype" class="dropdown" id="subtype-select" disabled>
								  <option value="0">Select Sub Type</option>
                                           <!-- <option value="0" selected="selected">Select Subtype</option>	-->
										   <!--<option value="<?php //echo $newrow4['subtype_id']; ?>" selected="selected"><?php //echo $newrow4['name']; ?></option>-->
											<?php
												if($newrow4 !="" && $newrow4 !=Null)
												{
													while($newrow4res=mysql_fetch_assoc($newrow4))
													{
														//var_dump($newrow4res);
														/*if($newrow4res['subtype_id']==$get_primary_type['subtype_id'])
														{
													?>
															<option value="<?php echo $newrow4res['subtype_id']; ?>" selected="selected" chkval="<?php echo $newrow4res['name']; ?>"><?php echo $newrow4res['name']; ?></option>
													<?php
														}
														else
														{*/
														?>
															<option value="<?php echo $newrow4res['subtype_id']; ?>"  chkval="<?php echo $newrow4res['name']; ?>"><?php echo $newrow4res['name']; ?></option>
													<?php
														//}
													}
												}
											 ?>
                                        </select>
                    
                                  <!--<div class="hint"><input name="ADD" type="button" id="addType" value=" Add " /></div>-->
							   </div>
                                <div class="fieldCont">
                                <div class="fieldTitle">Meta Type</div>
                                    <select name="select-category-artist-metatype" class="dropdown" id="metatype-select" disabled>
                                       <option value="0">Select Meta Type</option>
									   <?php
											if($get_dis_meta !="" && $get_dis_meta !=Null)
											{
												while($getmeta_type=mysql_fetch_assoc($get_dis_meta))
												{
													/*if($getmeta_type['meta_id']==$get_primary_type['metatype_id'])
													{
												?>
														<option value="<?php echo $getmeta_type['meta_id']; ?>" selected="selected" chkval="<?php echo $getmeta_type['name']; ?>"><?php echo $getmeta_type['name']; ?></option>
												<?php
													}
													else
													{*/
													?>
														<option value="<?php echo $getmeta_type['meta_id']; ?>"  chkval="<?php echo $getmeta_type['name']; ?>"><?php echo $getmeta_type['name']; ?></option>
												<?php
													//}
												}
											}
										?>
                    
                                    </select>
                                  
                                  <div class="hint"><input name="ADD" type="button" id="addType" value=" Add "></div>
                                </div>
<!--Fan Club Code Starts Here-->

								<h4>Fan Club </h4>
								<?php
								if(!isset($get_purify_member['id']) && $get_purify_member['id']=="")
								{
								?>
								<script type="text/javascript">
								$(document).ready(function(){
								$("#fanclub_fields_none").css({"display":"block"});
								$("#fanclub_fields").css({"display":"none"});
								});
								</script>
								<?php
								}else{
								?>
								<script type="text/javascript">
								$(document).ready(function(){
								$("#fanclub_fields_none").css({"display":"none"});
								$("#fanclub_fields").css({"display":"block"});
								});
								</script>
								<?php
								}
								?>
									<div class="fieldCont" id="fanclub_fields_none" style="display:none">
										Only Purify Art members can sell media or fan club memberships. Purchase or renew your membership <a href="profileedit.php#Become_a_member">here</a>.
									</div>
									<div class="fieldCont" id="fanclub_fields" style="display:none">
										<div title="Sell fan club memberships to users who will be able to access exclusive media you select below." class="fieldTitle">Fan Club</div>
										<div class="chkCont">
											<input title="Sell fan club memberships to users who will be able to access exclusive media you select below." type="radio" name="type" class="chk" value="fan_club_yes" id="type1" onclick="forsale_check(1);" <?php if($nameres['type'] =="fan_club" && $nameres['type'] !=""){echo "checked";}?>>
											<div title="Sell fan club memberships to users who will be able to access exclusive media you select below." class="radioTitle">Yes</div>
										</div>
										<div class="chkCont">
											<input title="Sell fan club memberships to users who will be able to access exclusive media you select below." type="radio" name="type" class="chk" value="fan_club_no" id="type2" onclick="forsale_check(0);" <?php if($nameres['type'] =="for_sale" && $nameres['type'] !=""){echo "checked";}?>>
											<div title="Sell fan club memberships to users who will be able to access exclusive media you select below." class="radioTitle">No </div>
										</div>
										<a href="notmembererror.php" style="display:none;" id="member_error"></a>
									</div>
							<?php
							//if(isset($_GET['id']) && $_GET['id']!="")
							//{
								if($nameres['type'] =="fan_club_yes")
								{
								?>
								<script>
								$(document).ready(function(){
									$("#type1").click();
								});
								</script>
								<?php	
								}
								if($nameres['type'] =="fan_club_no")
								{
								?>
								<script>
									$(document).ready(function(){
										$("#type2").click();
									});
								</script>
								<?php	
								}
								$get_fan_det = $editprofile -> Get_artist_member_Id();
							//}
							if(isset($get_purify_member['id']) || $get_purify_member['id']!=null )
							{
							?>
							<div id="fan_club" style="display:none">
								<div class="fieldCont" style="display:none">
									<div title="Choose to sell annual or lifetime fan club memberships to users who will be able to access exclusive media you select below." class="fieldTitle">Frequency</div>
									<select title="Choose to sell annual or lifetime fan club memberships to users who will be able to access exclusive media you select below." name="frequency" class="dropdown" id="frequency">
										<option value="0">Select</option>
										<option value="annual" <?php if($get_fan_det['frequency']=="annual"){echo "selected"; }?>>Annual</option>
										<option value="lifetime" <?php if($get_fan_det['frequency']=="lifetime"){echo "selected"; }?>>LifeTime</option>
									</select>
								</div>
								<div class="fieldCont">
									<div title="Sell fan club memberships to users who will be able to access exclusive media you select below." class="fieldTitle">Price</div>
									<input title="Sell fan club memberships to users who will be able to access exclusive media you select below." type="text" class="fieldText" name="fan_price" id="fan_price" value="<?php echo $nameres['price'];?>"><div class="hintnew">/ Year</div>
								</div>
								<div class="fieldCont">
									<div title="Sell fan club memberships to users who will be able to access exclusive media you select below." class="fieldTitle">Description</div>
									<textarea title="Sell fan club memberships to users who will be able to access exclusive media you select below." class="jquery_ckeditor" cols="4" id="description" name="description" rows="10"><?php echo $get_fan_det['description'];?></textarea>
								</div>
								<div class="fieldCont">
								  <div class="fieldTitle">Songs</div>
									<select class="list" id="fan_club_song[]" multiple="multiple" size="5" name="fan_club_song[]">
									<?php
										$get_song_id_fan = array();
										$get_artist_song_id=$editprofile->get_artist_song_id();
										if($get_artist_song_id!="" && $get_artist_song_id !=Null)
										{
											while($get_song_ids_fan = mysql_fetch_assoc($get_artist_song_id))
											{
												$get_song_id_fan[] = $get_song_ids_fan;
											}
										}

										/*This function will display the user in which profile he is creator and display media where that profile was creator*/
										$new_medi_fan = $editprofile->get_creator_heis();
										$new_creator_creator_media_array_fan = array();
										if(!empty($new_medi_fan))
										{
											for($count_cre_m=0;$count_cre_m<count($new_medi_fan);$count_cre_m++)
											{
												if($new_medi_fan[$count_cre_m]['media_type']==114){
													$new_creator_creator_media_array_fan[] = $new_medi_fan[$count_cre_m];
												}
											}
										}
										//var_dump($new_creator_creator_media_array);
										/*This function will display the user in which profile he is creator and display media where that profile was creator Ends here*/
										
										/*This function will display the user in which profile he is from and display media where that profile was creator*/
										$new_medi_from_fan = $editprofile->get_from_heis();
										$new_from_from_media_array_fan = array();
										if(!empty($new_medi_from_fan))
										{
											for($count_cre_m=0;$count_cre_m<count($new_medi_from_fan);$count_cre_m++)
											{
												if($new_medi_from_fan[$count_cre_m]['media_type']==114){
												$new_from_from_media_array_fan[] = $new_medi_from_fan[$count_cre_m];
												}
											}
										}
										//var_dump($new_from_from_media_array_fan);
										/*This function will display the user in which profile he is from and display media where that profile was creator Ends here*/
										
										
										/*This function will display the user in which profile he is from or creator and tagged and display media where that profile was creator*/
										$new_medi_tagged_fan = $editprofile->get_the_user_tagin_project($_SESSION['login_email']);
										$new_tagged_from_media_array_fan = array();
										if(!empty($new_medi_tagged_fan))
										{
											for($count_cre_m=0;$count_cre_m<count($new_medi_tagged_fan);$count_cre_m++)
											{
												if($new_medi_tagged_fan[$count_cre_m]['media_type']==114){
												$new_tagged_from_media_array_fan[] = $new_medi_tagged_fan[$count_cre_m];
												}
											}
										}
										//var_dump($new_tagged_from_media_array);
										/*This function will display the user in which profile he is from or creator and tagged and display media where that profile was creator Ends here*/
										
										
										/*This function will display in which media the curent user was creator*/
										$get_where_creator_media_fan = $editprofile->get_media_where_creator_or_from();
										$new_creator_media_array_fan = array();
										if(!empty($get_where_creator_media_fan))
										{
											for($count_tag_m=0;$count_tag_m<count($get_where_creator_media_fan);$count_tag_m++)
											{
												if($get_where_creator_media_fan[$count_tag_m]['media_type']==114){
													$new_creator_media_array_fan[] = $get_where_creator_media_fan[$count_tag_m];
												}
											}
										}
										//var_dump($new_creator_media_array);
										/*This function will display in which media the curent user was creator ends here*/
										
										$acc_medss_fan = array();
										for($acc_vis=0;$acc_vis<count($acc_media);$acc_vis++)
										{
											if($acc_media[$acc_vis]!="")
											{
												$sels_tagged_fan = $editprofile->get_artist_media_type_tagged_by_other_user($acc_media[$acc_vis]);
												if(!empty($sels_tagged_fan))
												{
													
													$acc_medss_fan[] = $sels_tagged_fan;
												}
											}
										}
										//var_dump($acc_medss);
										$type = '114';
										$cre_frm_med_fan = array();
										$getsong1 = array();
										$cre_frm_med_fan = $editprofile->get_media_create($type);
										
										$get_songs_final_ct = array();
										$get_songs_pnew_w = array_merge($acc_medss_fan,$get_song_id_fan,$get_songs_final_ct,$cre_frm_med_fan,$new_creator_creator_media_array_fan,$new_from_from_media_array_fan,$new_tagged_from_media_array_fan,$new_creator_media_array_fan);
										
										for($acc_song1=0;$acc_song1<count($get_songs_pnew_w);$acc_song1++)
										{
											if($get_songs_pnew_w[$acc_song1]['id']!="" && $get_songs_pnew_w[$acc_song1]['id']!=0)
											{
												$get_artist_song1 = $editprofile->get_artist_song($get_songs_pnew_w[$acc_song1]['id']);
												if($get_artist_song1 !="" && $get_artist_song1 !=Null)
												{
													$getsong1 = mysql_fetch_assoc($get_artist_song1);
												}
												$ids = $get_songs_pnew_w[$acc_song1]['id'];
												$get_songs_pnew_w[$acc_song1]['track'] = $getsong1['track'];
											}
										}
										
										$sort = array();
										//var_dump($get_songs_pnew_w);
										foreach($get_songs_pnew_w as $k=>$v) {
										
											$end_c1 = $v['creator'];
											//$create_c = strpos($v['creator'],'(');
											//$end_c1 = substr($v['creator'],0,$create_c);
											$end_c = trim($end_c1);
											
											$end_f1 = $v['from'];
											//$create_f = strpos($v['from'],'(');
											//$end_f1 = substr($v['from'],0,$create_f);
											$end_f = trim($end_f1);
											
											$sort['creator'][$k] = strtolower($end_c);
											$sort['from'][$k] = strtolower($end_f);
											$sort['track'][$k] = $v['track'];
											$sort['title'][$k] = strtolower($v['title']);
										}
										//var_dump($sort);
										if(!empty($sort))
										{
											array_multisort($sort['creator'], SORT_ASC, $sort['from'], SORT_ASC, $sort['track'], SORT_ASC, $sort['title'], SORT_ASC,$get_songs_pnew_w);
										}
										
										$dummy_s = array();
										$get_all_del_id = $editprofile->get_all_del_media_id();
										$deleted_id = explode(",",$get_all_del_id);
										for($count_del=0;$count_del<=count($get_songs_pnew_w);$count_del++)
										{
											if($get_songs_pnew_w[$count_del]==""){continue;}
											else{
												for($count_deleted=0;$count_deleted<=count($deleted_id);$count_deleted++)
												{
													if($deleted_id[$count_deleted]==""){continue;}
													else{
														if($deleted_id[$count_deleted] == $get_songs_pnew_w[$count_del]['id'])
														{
															$get_songs_pnew_w[$count_del]="";
														}
													}
												}
											}
										}
										for($acc_song=0;$acc_song<count($get_songs_pnew_w);$acc_song++)
										{
											if($get_songs_pnew_w[$acc_song]['delete_status']==0){
												if(isset($get_songs_pnew_w[$acc_song]['id']))
												{
													if(in_array($get_songs_pnew_w[$acc_song]['id'],$dummy_s))
													{
														
													}
													else
													{
														$dummy_s[] = $get_songs_pnew_w[$acc_song]['id'];
														if($get_songs_pnew_w[$acc_song]['id']!="" && $get_songs_pnew_w[$acc_song]['media_type']==114)
														{
															//$create_c = strpos($get_songs_pnew_w[$acc_song]['creator'],'(');
															//$end_c = substr($get_songs_pnew_w[$acc_song]['creator'],0,$create_c);
															$end_c = $get_songs_pnew_w[$acc_song]['creator'];
															
															//$create_f = strpos($get_songs_pnew_w[$acc_song]['from'],'(');
															//$end_f = substr($get_songs_pnew_w[$acc_song]['from'],0,$create_f);
															$end_f = $get_songs_pnew_w[$acc_song]['from'];
															
															$eceks = "";
															if($end_c!="")
															{
																$eceks = $end_c;
															}
															if($end_f!="")
															{
																if($end_c!="")
																{
																	$eceks .= " : ".$end_f;
																}
																else
																{
																	$eceks .= $end_f;
																}
															}
															if($get_songs_pnew_w[$acc_song]['track']!="")
															{
																if($end_c!="" || $end_f!="")
																{
																	$eceks .= " : ".$get_songs_pnew_w[$acc_song]['track'];
																}
																else
																{
																	$eceks .= $get_songs_pnew_w[$acc_song]['track'];
																}
															}
															if($get_songs_pnew_w[$acc_song]['title']!="")
															{
																if($end_c!="" || $end_f!="" || $get_songs_pnew_w[$acc_song]['track']!="")
																{
																	$eceks .= " : ".$get_songs_pnew_w[$acc_song]['title'];
																}
																else
																{
																	$eceks .= $get_songs_pnew_w[$acc_song]['title'];
																}
															}
															
															if($get_select_songs_fanclub!="" && $get_select_songs_fanclub!=0)
															{
																$get_select_songs_fanclub = array_unique($get_select_songs_fanclub);
																$yes_com_tag_pro_song =0;
																for($count_sel=0;$count_sel<=count($get_select_songs_fanclub);$count_sel++)
																{
																	if($get_select_songs_fanclub[$count_sel]==""){continue;}
																	else
																	{
																		if($get_select_songs_fanclub[$count_sel] == $get_songs_pnew_w[$acc_song]['id'])
																		{
																			$yes_com_tag_pro_song = 1;
											?>
																			<option value="<?php echo $get_songs_pnew_w[$acc_song]['id'];?>" selected><?php echo stripslashes($eceks); ?></option>
											<?php
																		}
																	}
																}
																if($yes_com_tag_pro_song == 0)
																{													
											?>
																	<option value="<?php echo $get_songs_pnew_w[$acc_song]['id'];?>"><?php echo stripslashes($eceks); ?></option>
											<?php
																}
															}
															else
															{
				?>
																<option value="<?php echo $get_songs_pnew_w[$acc_song]['id'];?>" ><?php echo stripslashes($eceks); ?></option>
				<?php
															}
														}
													}
												}
											}
										}
										
										//if(mysql_num_rows($get_artist_song_id)<=0 && $media_song=="" && count($get_media_cre_art_pro)<=0 && mysql_num_rows($get_media_tag_com_pro)<=0 && mysql_num_rows($get_media_tag_art_pro)<=0)
										if(empty($get_songs_pnew_w))
										{
									?>
											<option value="" >No Listings</option>
									<?php	
										}
									?>
									</select>
								</div>
								<div class="fieldCont">
								  <div class="fieldTitle">Videos</div>
									<select class="list" id="fan_club_video[]" multiple="multiple" size="5" name="fan_club_video[]">
										<?php
										$get_video_id = array();
										$get_artist_video_id=$editprofile->get_artist_video_id();
										if($get_artist_video_id!="" && $get_artist_video_id!=Null)
										{
											while($get_video_ids = mysql_fetch_assoc($get_artist_video_id))
											{
												$get_video_id[] = $get_video_ids;
											}
										}
										
										$get_media_tag_art_pro = $editprofile->get_artist_project_tag($_SESSION['login_email']);
										if($get_media_tag_art_pro!="" && $get_media_tag_art_pro !=Null)
										{
											if(mysql_num_rows($get_media_tag_art_pro)>0)
											{
												$is_v = 0;
												while($art_pro_tag = mysql_fetch_assoc($get_media_tag_art_pro))
												{
													$get_videos = explode(",",$art_pro_tag['tagged_videos']);
													for($count_art_tag_video=0;$count_art_tag_video<count($get_videos);$count_art_tag_video++)
													{
														if($get_videos[$count_art_tag_video]!="")
														{
															$get_videos_new[$is_v] = $get_videos[$count_art_tag_video];
															$is_v = $is_v + 1;
														}
													}
												}
												if($get_videos_new!="")
												{
													$get_videos_n = array_unique($get_videos_new);
												}
											}
										}
										//var_dump($get_videos_n);
										$get_media_tag_art_eve = $editprofile->get_artist_event_tag($_SESSION['login_email']);
										if($get_media_tag_art_eve!="" && $get_media_tag_art_eve!=Null)
										{
											if(mysql_num_rows($get_media_tag_art_eve)>0)
											{
												$is_v = 0;
												while($art_pro_tag = mysql_fetch_assoc($get_media_tag_art_eve))
												{
													$get_videos = explode(",",$art_pro_tag['tagged_videos']);
													for($count_art_tag_video=0;$count_art_tag_video<count($get_videos);$count_art_tag_video++)
													{
														if($get_videos[$count_art_tag_video]!="")
														{
															$get_videos_enewe[$is_v] = $get_videos[$count_art_tag_video];
															$is_v = $is_v + 1;
														}
													}
												}
												if($get_videos_enewe!="")
												{
													$get_videos_ne = array_unique($get_videos_enewe);
												}
											}
										}
										
										$get_media_tag_com_pro = $editprofile->get_community_project_tag($_SESSION['login_email']);
										if($get_media_tag_com_pro!="" && $get_media_tag_com_pro!=Null)
										{
											if(mysql_num_rows($get_media_tag_com_pro)>0)
											{
												$is_vc = 0;
												while($com_pro_tag = mysql_fetch_assoc($get_media_tag_com_pro))
												{
													$get_com_videos = explode(",",$com_pro_tag['tagged_videos']);
													for($count_com_tag_video=0;$count_com_tag_video<count($get_com_videos);$count_com_tag_video++)
													{
														if($get_com_videos[$count_com_tag_video]!="")
														{
															$get_videos_c_new[$is_vc] = $get_com_videos[$count_com_tag_video];
															$is_vc = $is_vc + 1;
														}
													}
												}
												if($get_videos_c_new!="")
												{
													$get_videos_c_n = array_unique($get_videos_c_new);
												}
											}
										}
										
										$get_media_tag_com_eve = $editprofile->get_community_event_tag($_SESSION['login_email']);
										if($get_media_tag_com_eve!="" && $get_media_tag_com_eve!=Null)
										{
											if(mysql_num_rows($get_media_tag_com_eve)>0)
											{
												$is_vc = 0;
												while($com_pro_tag = mysql_fetch_assoc($get_media_tag_com_eve))
												{
													$get_com_videos = explode(",",$com_pro_tag['tagged_videos']);
													for($count_com_tag_video=0;$count_com_tag_video<count($get_com_videos);$count_com_tag_video++)
													{
														if($get_com_videos[$count_com_tag_video]!="")
														{
															$get_videos_c_enewe[$is_vc] = $get_com_videos[$count_com_tag_video];
															$is_vc = $is_vc + 1;
														}
													}
												}
												if($get_videos_c_enewe!="")
												{
													$get_videose_c_rn = array_unique($get_videos_c_enewe);
												}
											}
										}
										
										$get_media_cre_art_pro = $editprofile->get_project_create();
										if(!empty($get_media_cre_art_pro))
										{
											$is_v_c = 0;
											//while($art_pro_tag = mysql_fetch_assoc($get_media_cre_art_pro))
											//var_dump($get_media_cre_art_pro);
											for($cr_i=0;$cr_i<count($get_media_cre_art_pro);$cr_i++)
											{
												$get_cvideos = explode(",",$get_media_cre_art_pro[$cr_i]['tagged_videos']);

												for($count_art_cr_video=0;$count_art_cr_video<count($get_cvideos);$count_art_cr_video++)
												{
													if($get_cvideos[$count_art_cr_video]!="")
													{
														$get_videos__new[$is_v_c] = $get_cvideos[$count_art_cr_video];
														$is_v_c = $is_v_c + 1;
													}
												}
											}
											if($get_videos__new!="")
											{
												$get_videos__n = array_unique($get_videos__new);
											}
										}
										
										if(!isset($get_videos_n))
										{
											$get_videos_n = array();
										}
										
										if(!isset($get_videose_c_rn))
										{
											$get_videose_c_rn = array();
										}
										
										if(!isset($get_videos_ne))
										{
											$get_videos_ne = array();
										}
										
										if(!isset($get_videos_c_n))
										{
											$get_videos_c_n = array();
										}
										
										if(!isset($get_videos__n))
										{
											$get_videos__n = array();
										}
										
										/*This function will display the user in which profile he is creator and display media where that profile was creator*/
										$new_medi = $editprofile->get_creator_heis();
										$new_creator_creator_media_array = array();
										if(!empty($new_medi))
										{
											for($count_cre_m=0;$count_cre_m<count($new_medi);$count_cre_m++)
											{
												if($new_medi[$count_cre_m]['media_type']==115){
													$new_creator_creator_media_array[] = $new_medi[$count_cre_m];
												}
											}
										}
										//var_dump($new_creator_creator_media_array);
										/*This function will display the user in which profile he is creator and display media where that profile was creator Ends here*/
										
										/*This function will display the user in which profile he is from and display media where that profile was creator*/
										$new_medi_from = $editprofile->get_from_heis();
										$new_from_from_media_array = array();
										if(!empty($new_medi_from))
										{
											for($count_cre_m=0;$count_cre_m<count($new_medi_from);$count_cre_m++)
											{
												if($new_medi_from[$count_cre_m]['media_type']==115){
												$new_from_from_media_array[] = $new_medi_from[$count_cre_m];
												}
											}
										}
										//var_dump($new_creator_creator_media_array);
										/*This function will display the user in which profile he is from and display media where that profile was creator Ends here*/
										
										
										/*This function will display the user in which profile he is from or creator and tagged and display media where that profile was creator*/
										$new_medi_tagged = $editprofile->get_the_user_tagin_project($_SESSION['login_email']);
										$new_tagged_from_media_array = array();
										if(!empty($new_medi_tagged))
										{
											for($count_cre_m=0;$count_cre_m<count($new_medi_tagged);$count_cre_m++)
											{
												if($new_medi_tagged[$count_cre_m]['media_type']==115){
												$new_tagged_from_media_array[] = $new_medi_tagged[$count_cre_m];
												}
											}
										}
										//var_dump($new_tagged_from_media_array);
										/*This function will display the user in which profile he is from or creator and tagged and display media where that profile was creator Ends here*/
										
										
										/*This function will display in which media the curent user was creator*/
										$get_where_creator_media = $editprofile->get_media_where_creator_or_from();
										$new_creator_media_array = array();
										if(!empty($get_where_creator_media))
										{
											for($count_tag_m=0;$count_tag_m<count($get_where_creator_media);$count_tag_m++)
											{
												if($get_where_creator_media[$count_tag_m]['media_type']==115){
													$new_creator_media_array[] = $get_where_creator_media[$count_tag_m];
												}
											}
										}
										
										$get_vids_pnew_w_c = array_merge($get_videos_n,$get_videos_c_n,$get_videos__n,$get_videos_ne,$get_videose_c_rn);
										
										$get_vids_pnew_ct = array_unique($get_vids_pnew_w_c);
										$get_vids_final_ct = array();
										
										for($count_art_cr_video=0;$count_art_cr_video<count($get_vids_pnew_ct);$count_art_cr_video++)
										{
											if($get_vids_pnew_ct[$count_art_cr_video]!="")
											{
												$get_media_tag_art_pro_vi = $editprofile->get_tag_media_info($get_vids_pnew_ct[$count_art_cr_video]);
												$get_vids_final_ct[] = $get_media_tag_art_pro_vi;
											}
										}
										
										$acc_medsv = array();
										for($acc_vis=0;$acc_vis<count($acc_media);$acc_vis++)
										{
											if($acc_media[$acc_vis]!="")
											{
												$sel_tagged = $editprofile->get_artist_media_type_tagged_by_other_user($acc_media[$acc_vis]);
												if(!empty($sel_tagged))
												{
													$acc_medsv[] = $sel_tagged;
												}
											}
										}
										$get_vids_final_ct = array();
										$type = '115';
										$cre_frm_med = array();
										$cre_frm_med = $editprofile->get_media_create($type);
										
										$get_vids_pnew_w = array_merge($acc_medsv,$get_video_id,$get_vids_final_ct,$cre_frm_med,$new_creator_creator_media_array,$new_from_from_media_array,$new_tagged_from_media_array,$new_creator_media_array);
										//var_dump($get_vids_pnew_w);
										$sort = array();
										foreach($get_vids_pnew_w as $k=>$v) {
											
											$end_c1 = $v['creator'];
											//$create_c = strpos($v['creator'],'(');
											//$end_c1 = substr($v['creator'],0,$create_c);
											$end_c = trim($end_c1);
											
											$end_f1 = $v['from'];
											//$create_f = strpos($v['from'],'(');
											//$end_f1 = substr($v['from'],0,$create_f);
											$end_f = trim($end_f1);
											
											$sort['creator'][$k] = strtolower($end_c);
											$sort['from'][$k] = strtolower($end_f);
											//$sort['track'][$k] = $v['track'];
											$sort['title'][$k] = strtolower($v['title']);
										}
										//var_dump($sort);
										if(!empty($sort))
										{
											array_multisort($sort['creator'], SORT_ASC, $sort['from'], SORT_ASC, $sort['title'], SORT_ASC,$get_vids_pnew_w);
										}
										
										$dummy_v = array();
										$get_all_del_id = $editprofile->get_all_del_media_id();
										$deleted_id = explode(",",$get_all_del_id);
										for($count_del=0;$count_del<=count($get_vids_pnew_w);$count_del++)
										{
											if($get_vids_pnew_w[$count_del]==""){continue;}
											else{
												for($count_deleted=0;$count_deleted<=count($deleted_id);$count_deleted++)
												{
													if($deleted_id[$count_deleted]==""){continue;}
													else{
														if($deleted_id[$count_deleted] == $get_vids_pnew_w[$count_del]['id'])
														{
															$get_vids_pnew_w[$count_del]="";
														}
													}
												}
											}
										}
										for($acc_vide=0;$acc_vide<count($get_vids_pnew_w);$acc_vide++)
										{
											if($get_vids_pnew_w[$acc_vide]['delete_status']==0){
												if(isset($get_vids_pnew_w[$acc_vide]['id']))
												{
													if(in_array($get_vids_pnew_w[$acc_vide]['id'],$dummy_v))
													{

													}
													else
													{
														$dummy_v[] = $get_vids_pnew_w[$acc_vide]['id'];
														if($get_vids_pnew_w[$acc_vide]['id']!="" && $get_vids_pnew_w[$acc_vide]['media_type']==115)
														{
															//$create_c = strpos($get_vids_pnew_w[$acc_vide]['creator'],'(');
															//$end_c = substr($get_vids_pnew_w[$acc_vide]['creator'],0,$create_c);
															$end_c = $get_vids_pnew_w[$acc_vide]['creator'];
															
															//$create_f = strpos($get_vids_pnew_w[$acc_vide]['from'],'(');
															//$end_f = substr($get_vids_pnew_w[$acc_vide]['from'],0,$create_f);
															$end_f = $get_vids_pnew_w[$acc_vide]['from'];
															
															$eceksv = "";
															if($end_c!="")
															{
																$eceksv = $end_c;
															}
															if($end_f!="")
															{
																if($end_c!="")
																{
																	$eceksv .= " : ".$end_f;
																}
																else
																{
																	$eceksv .= $end_f;
																}
															}
															if($get_vids_pnew_w[$acc_vide]['title']!="")
															{
																if($end_c!="" || $end_f!="")
																{
																	$eceksv .= " : ".$get_vids_pnew_w[$acc_vide]['title'];
																}
																else
																{
																	$eceksv .= $get_vids_pnew_w[$acc_vide]['title'];
																}
															}
															
															if($get_select_videos_fanclub!="" && $get_select_videos_fanclub!=0)
															{
																$get_select_videos_fanclub = array_unique($get_select_videos_fanclub);
																$yes_com_tag_pro_song =0;
																for($count_sel=0;$count_sel<=count($get_select_videos_fanclub);$count_sel++)
																{
																	if($get_select_videos_fanclub[$count_sel]==""){continue;}
																	else
																	{
																		if($get_select_videos_fanclub[$count_sel] == $get_vids_pnew_w[$acc_vide]['id'])
																		{
																			$yes_com_tag_pro_song = 1;
											?>
																			<option value="<?php echo $get_vids_pnew_w[$acc_vide]['id'];?>" selected><?php echo stripslashes($eceksv); ?></option>
											<?php
																		}
																	}
																}
																if($yes_com_tag_pro_song == 0)
																{													
											?>
																	<option value="<?php echo $get_vids_pnew_w[$acc_vide]['id'];?>"><?php echo stripslashes($eceksv); ?></option>
											<?php
																}
															}
															else
															{
				?>
																<option value="<?php echo $get_vids_pnew_w[$acc_vide]['id'];?>" ><?php echo stripslashes($eceksv); ?></option>
				<?php
															}
														}
													}
												}
											}
										}
										
										//if(mysql_num_rows($get_artist_video_id)<=0 && $media_video=="" && count($get_media_cre_art_pro)<=0 && mysql_num_rows($get_media_tag_com_pro)<=0 && mysql_num_rows($get_media_tag_art_pro)<=0)
										if(empty($get_vids_pnew_w))
										{
										?>
											<option value="" >No Listings</option>
										<?php	
										}
										?>
									</select>
								</div>
								<div class="fieldCont">
								  <div class="fieldTitle">Galleries</div>
									<select class="list" id="fan_club_gallery[]" multiple="multiple" size="5" name="fan_club_gallery[]">
										<?php
										$j="";
										$get_gallery_id = array();
										$get_artist_gallery_id=$editprofile->get_artist_gallery_id();
										if($get_artist_gallery_id!="" && $get_artist_gallery_id !=Null)
										{
											while($get_gallery_ids = mysql_fetch_assoc($get_artist_gallery_id))
											{
												$get_gallery_id[] = $get_gallery_ids; 
											}
										}
										
										 $get_media_tag_art_pro = $editprofile->get_artist_project_tag($_SESSION['login_email']);
										if($get_media_tag_art_pro!="" && $get_media_tag_art_pro!=Null)
										{
											if(mysql_num_rows($get_media_tag_art_pro)>0)
											{
												$is_g = 0;
												while($art_pro_tag = mysql_fetch_assoc($get_media_tag_art_pro))
												{
													$get_gallery = explode(",",$art_pro_tag['tagged_galleries']);
													for($count_art_t1_song=0;$count_art_t1_song<count($get_gallery);$count_art_t1_song++)
													{
														if($get_gallery[$count_art_t1_song]!="")
														{
															$get_gal_new[$is_g] = $get_gallery[$count_art_t1_song];
															$is_g = $is_g + 1;
														}
													}
												}
												if($get_gal_new!="")
												{
													$get_gal_n = array_unique($get_gal_new);
												}
											}
										}
										
										$get_media_tag_art_eve = $editprofile->get_artist_event_tag($_SESSION['login_email']);
										if($get_media_tag_art_eve!="" && $get_media_tag_art_eve!=Null)
										{
											if(mysql_num_rows($get_media_tag_art_eve)>0)
											{
												$is_g = 0;
												while($art_pro_tag = mysql_fetch_assoc($get_media_tag_art_eve))
												{
													$get_gallery = explode(",",$art_pro_tag['tagged_galleries']);
													for($count_art_t1_song=0;$count_art_t1_song<count($get_gallery);$count_art_t1_song++)
													{
														if($get_gallery[$count_art_t1_song]!="")
														{
															$get_gal_newe[$is_g] = $get_gallery[$count_art_t1_song];
															$is_g = $is_g + 1;
														}
													}
												}
												if($get_gal_newe!="")
												{
													$get_gal_ne = array_unique($get_gal_newe);
												}
											}
										}
										
										$get_media_tag_com_pro = $editprofile->get_community_project_tag($_SESSION['login_email']);
										if($get_media_tag_com_pro!="" && $get_media_tag_com_pro!=Null)
										{
											if(mysql_num_rows($get_media_tag_com_pro)>0)
											{
												$is_gc = 0;
												while($com_pro_tag = mysql_fetch_assoc($get_media_tag_com_pro))
												{
													$get_com_gallery = explode(",",$com_pro_tag['tagged_galleries']);
													for($cou1_com_tag_song=0;$cou1_com_tag_song<count($get_com_gallery);$cou1_com_tag_song++)
													{
														if($get_com_gallery[$cou1_com_tag_song]!="")
														{
															$get_gal_new_c[$is_gc] = $get_com_gallery[$cou1_com_tag_song];
															$is_gc = $is_gc + 1;
														}
													}
												}
												if($get_gal_new_c!="")
												{
													$get_gal_n_c = array_unique($get_gal_new_c);
												}
											}
										}
										
										$get_media_tag_com_eve = $editprofile->get_community_event_tag($_SESSION['login_email']);
										if($get_media_tag_com_eve!="" && $get_media_tag_com_eve!=Null)
										{
											if(mysql_num_rows($get_media_tag_com_eve)>0)
											{
												$is_gc = 0;
												while($com_pro_tag = mysql_fetch_assoc($get_media_tag_com_eve))
												{
													$get_com_gallery = explode(",",$com_pro_tag['tagged_galleries']);
													for($cou1_com_tag_song=0;$cou1_com_tag_song<count($get_com_gallery);$cou1_com_tag_song++)
													{
														if($get_com_gallery[$cou1_com_tag_song]!="")
														{
															$get_gal_new_ce[$is_gc] = $get_com_gallery[$cou1_com_tag_song];
															$is_gc = $is_gc + 1;
														}
													}
												}
												if($get_gal_new_ce!="")
												{
													$get_gal_e_ce = array_unique($get_gal_new_ce);
												}
											}
										}
										
										$get_media_cre_art_pro = $editprofile->get_project_create();
										if(!empty($get_media_cre_art_pro))
										{
											$is_gp = 0;
											//while($art_pro_tag = mysql_fetch_assoc($get_media_cre_art_pro))
											for($cra_i=0;$cra_i<count($get_media_cre_art_pro);$cra_i++)
											{
												$get_cgal = explode(",",$get_media_cre_art_pro[$cra_i]['tagged_galleries']);
												
												for($count_art_cr_g=0;$count_art_cr_g<count($get_cgal);$count_art_cr_g++)
												{
													if($get_cgal[$count_art_cr_g]!="")
													{
														$get_gal_pn[$is_gp] = $get_cgal[$count_art_cr_g];
														$is_gp = $is_gp + 1;
													}
												}
											}
											if($get_gal_pn!="")
											{
												$get_gal_p_new = array_unique($get_gal_pn);
											}
										}
										
										/*This function will display the user in which profile he is creator and display media where that profile was creator*/
										$new_medi = $editprofile->get_creator_heis();
										$new_creator_creator_media_array = array();
										if(!empty($new_medi))
										{
											for($count_cre_m=0;$count_cre_m<count($new_medi);$count_cre_m++)
											{
												if($new_medi[$count_cre_m]['media_type']==113){
													$new_creator_creator_media_array[] = $new_medi[$count_cre_m];
												}
											}
										}
										
										/*This function will display the user in which profile he is creator and display media where that profile was creator Ends here*/
										
										/*This function will display the user in which profile he is from and display media where that profile was creator*/
										$new_medi_from = $editprofile->get_from_heis();
										$new_from_from_media_array = array();
										if(!empty($new_medi_from))
										{
											for($count_cre_m=0;$count_cre_m<count($new_medi_from);$count_cre_m++)
											{
												if($new_medi_from[$count_cre_m]['media_type']==113){
												$new_from_from_media_array[] = $new_medi_from[$count_cre_m];
												}
											}
										}
										/*This function will display the user in which profile he is from and display media where that profile was creator Ends here*/
										
										
										/*This function will display the user in which profile he is from or creator and tagged and display media where that profile was creator*/
										$new_medi_tagged = $editprofile->get_the_user_tagin_project($_SESSION['login_email']);
										$new_tagged_from_media_array = array();
										if(!empty($new_medi_tagged))
										{
											for($count_cre_m=0;$count_cre_m<count($new_medi_tagged);$count_cre_m++)
											{
												if($new_medi_tagged[$count_cre_m]['media_type']==113){
												$new_tagged_from_media_array[] = $new_medi_tagged[$count_cre_m];
												}
											}
										}
										
										/*This function will display the user in which profile he is from or creator and tagged and display media where that profile was creator Ends here*/
										
										
										/*This function will display in which media the curent user was creator*/
										$get_where_creator_media = $editprofile->get_media_where_creator_or_from();
										$new_creator_media_array = array();
										if(!empty($get_where_creator_media))
										{
											for($count_tag_m=0;$count_tag_m<count($get_where_creator_media);$count_tag_m++)
											{
												if($get_where_creator_media[$count_tag_m]['media_type']==113){
													$new_creator_media_array[] = $get_where_creator_media[$count_tag_m];
												}
											}
										}
										//var_dump($new_creator_media_array);
										/*This function will display in which media the curent user was creator ends here*/
										
										if(!isset($get_gal_n))
										{
											$get_gal_n = array();
										}
										
										if(!isset($get_gal_ne))
										{
											$get_gal_ne = array();
										}
										
										if(!isset($get_gal_n_c))
										{
											$get_gal_n_c = array();
										}
										
										if(!isset($get_gal_p_new))
										{
											$get_gal_p_new = array();
										}
										
										if(!isset($get_gal_e_ce))
										{
											$get_gal_e_ce = array();
										}
										
										$get_gals_pnew_w_c = array_merge($get_gal_n,$get_gal_n_c,$get_gal_p_new,$get_gal_ne,$get_gal_e_ce);
										//var_dump($get_gals_pnew_w_c);
										$get_gals_pnew_g = array_unique($get_gals_pnew_w_c);
										$get_gals_final_ct = array();
										
										for($count_art_cr_gs=0;$count_art_cr_gs<count($get_gals_pnew_g);$count_art_cr_gs++)
										{
											if($get_gals_pnew_g[$count_art_cr_gs]!="")
											{
												$get_media_tag_g_art = $editprofile->get_tag_media_info($get_gals_pnew_g[$count_art_cr_gs]);
												$get_gals_final_ct[] = $get_media_tag_g_art;
											}
										}
										//var_dump($get_gals_final_ct);
										$chk_fan = $editprofile->chk_fan();
										$new_fan_media_array = array();
										if($chk_fan!="" && $chk_fan!=Null)
										{
											while($row_fan = mysql_fetch_assoc($chk_fan))
											{
												$new_fan_arr[] = $row_fan;
												$get_fan_media = $editprofile->get_fan_media($row_fan['buyer_id'],$row_fan['seller_id'],$row_fan['table_name'],$row_fan['type_id']);
												if($get_fan_media!="" && $get_fan_media!=Null)
												{
													if(mysql_num_rows($get_fan_media)>0)
													{
														while($row_fan_media = mysql_fetch_assoc($get_fan_media))
														{
															$row_fan_media['media_from'] = "media_coming_fan";
															$new_fan_media_array = $row_fan_media;
														}
													}
												}
											}
										}
										$new_fan_media_array = array();
										$acc_medsg = array();
										for($acc_vis=0;$acc_vis<count($acc_media);$acc_vis++)
										{
											if($acc_media[$acc_vis]!="")
											{
												$segl_tagged = $editprofile->get_artist_media_type_tagged_by_other_user($acc_media[$acc_vis]);
												if(!empty($segl_tagged))
												{
													$acc_medsg[] = $segl_tagged;
												}
											}
										}
										//var_dump($acc_medsg);
										$get_gals_final_ct = array();
										$type = '113';
										$cre_frm_med = array();
										$cre_frm_med = $editprofile->get_media_create($type);
										//var_dump($cre_frm_med);
										$get_vgals_pnew_w = array_merge($acc_medsg,$get_gallery_id,$get_gals_final_ct,$cre_frm_med,$new_creator_creator_media_array,$new_from_from_media_array,$new_tagged_from_media_array,$new_creator_media_array,$new_fan_media_array);
										
										$sort = array();
										foreach($get_vgals_pnew_w as $k=>$v)
										{	
											$end_c1 = $v['creator'];
											//$create_c = strpos($v['creator'],'(');
											//$end_c1 = substr($v['creator'],0,$create_c);
											$end_c = trim($end_c1);
											
											$end_f1 = $v['from'];
											//$create_f = strpos($v['from'],'(');
											//$end_f1 = substr($v['from'],0,$create_f);
											$end_f = trim($end_f1);
											
											$sort['creator'][$k] = strtolower($end_c);
											$sort['from'][$k] = strtolower($end_f);
											//$sort['track'][$k] = $v['track'];
											$sort['title'][$k] = strtolower($v['title']);
										}
										//var_dump($sort);
										if(!empty($sort))
										{
											array_multisort($sort['creator'], SORT_ASC, $sort['from'], SORT_ASC, $sort['title'], SORT_ASC,$get_vgals_pnew_w);
										}
										
										$dummy_g = array();
										//var_dump($get_vgals_pnew_w);
										$get_all_del_id = $editprofile->get_all_del_media_id();
										$deleted_id = explode(",",$get_all_del_id);
										for($count_del=0;$count_del<=count($get_vgals_pnew_w);$count_del++)
										{
											if($get_vgals_pnew_w[$count_del]==""){continue;}
											else{
												for($count_deleted=0;$count_deleted<=count($deleted_id);$count_deleted++)
												{
													if($deleted_id[$count_deleted]==""){continue;}
													else{
														if($deleted_id[$count_deleted] == $get_vgals_pnew_w[$count_del]['id'])
														{
															$get_vgals_pnew_w[$count_del]="";
														}
													}
												}
											}
										}
										for($acc_glae=0;$acc_glae<count($get_vgals_pnew_w);$acc_glae++)
										{
											if($get_vgals_pnew_w[$acc_glae]['delete_status']==0){
												if(isset($get_vgals_pnew_w[$acc_glae]['id']))
												{
													if(in_array($get_vgals_pnew_w[$acc_glae]['id'],$dummy_g))
													{
														
													}
													else
													{
														$dummy_g[] = $get_vgals_pnew_w[$acc_glae]['id'];
														if($get_vgals_pnew_w[$acc_glae]['id']!="" && $get_vgals_pnew_w[$acc_glae]['media_type']==113)
														{
															//$create_c = strpos($get_vgals_pnew_w[$acc_glae]['creator'],'(');
															//$end_c = substr($get_vgals_pnew_w[$acc_glae]['creator'],0,$create_c);
															$end_c = $get_vgals_pnew_w[$acc_glae]['creator'];
															
															//$create_f = strpos($get_vgals_pnew_w[$acc_glae]['from'],'(');
															//$end_f = substr($get_vgals_pnew_w[$acc_glae]['from'],0,$create_f);
															$end_f = $get_vgals_pnew_w[$acc_glae]['from'];
															
															$eceksg = "";
															if($end_c!="")
															{
																$eceksg = $end_c;
															}
															if($end_f!="")
															{
																if($end_c!="")
																{
																	$eceksg .= " : ".$end_f;
																}
																else
																{
																	$eceksg .= $end_f;
																}
															}
															if($get_vgals_pnew_w[$acc_glae]['title']!="")
															{
																if($end_c!="" || $end_f!="")
																{
																	$eceksg .= " : ".$get_vgals_pnew_w[$acc_glae]['title'];
																}
																else
																{
																	$eceksg .= $get_vgals_pnew_w[$acc_glae]['title'];
																}
															}
															
															if($get_select_galleries_fanclub!="" && $get_select_galleries_fanclub!=0)
															{
																$get_select_galleries_fanclub = array_unique($get_select_galleries_fanclub);
																$yes_com_tag_pro_song =0;
																for($count_sel=0;$count_sel<=count($get_select_galleries_fanclub);$count_sel++)
																{
																	if($get_select_galleries_fanclub[$count_sel]==""){continue;}
																	else
																	{
																		if($get_select_galleries_fanclub[$count_sel] == $get_vgals_pnew_w[$acc_glae]['id'])
																		{
																			$yes_com_tag_pro_song = 1;
											?>
																			<option value="<?php echo $get_vgals_pnew_w[$acc_glae]['id'];?>" selected><?php echo stripslashes($eceksg); ?></option>
											<?php
																		}
																	}
																}
																if($yes_com_tag_pro_song == 0)
																{													
											?>
																	<option value="<?php echo $get_vgals_pnew_w[$acc_glae]['id'];?>"><?php echo stripslashes($eceksg); ?></option>
											<?php
																}
															}
															else
															{
				?>
																<option value="<?php echo $get_vgals_pnew_w[$acc_glae]['id'];?>" ><?php echo stripslashes($eceksg); ?></option>
				<?php
															}
														}
													}
												}
											}
										}

										//if(mysql_num_rows($get_artist_gallery_id)<=0 && $media_avail==0 && count($get_media_cre_art_pro)<=0 && mysql_num_rows($get_media_tag_com_pro)<=0 && mysql_num_rows($get_media_tag_art_pro)<=0)
										if(empty($get_vgals_pnew_w))
										{
										?>
											<option value="" >No Listings</option>
										<?php	
										}
										
										?>
									</select>
								</div> <?php  ?>
								<div class="fieldCont">
								  <div class="fieldTitle">Playlist</div>
									<select class="list" id="fan_club_channel[]" multiple="multiple" size="5" name="fan_club_channel[]">
										<?php
										$j="";
										$get_channel_id = array();
										if($get_artist_channel_id!="" && $get_artist_channel_id !=Null)
										{
											while($get_channel_ids = mysql_fetch_assoc($get_artist_channel_id))
											{
												$get_channel_id[] = $get_channel_ids; 
											}
										}
										
										/*This function will display the user in which profile he is creator and display media where that profile was creator*/
										$new_medi = $editprofile->get_creator_heis();
										$new_creator_creator_media_array = array();
										if(!empty($new_medi))
										{
											for($count_cre_m=0;$count_cre_m<count($new_medi);$count_cre_m++)
											{
												if($new_medi[$count_cre_m]['media_type']==116){
													$new_creator_creator_media_array[] = $new_medi[$count_cre_m];
												}
											}
										}
										
										/*This function will display the user in which profile he is creator and display media where that profile was creator Ends here*/
										
										/*This function will display the user in which profile he is from and display media where that profile was creator*/
										$new_medi_from = $editprofile->get_from_heis();
										$new_from_from_media_array = array();
										if(!empty($new_medi_from))
										{
											for($count_cre_m=0;$count_cre_m<count($new_medi_from);$count_cre_m++)
											{
												if($new_medi_from[$count_cre_m]['media_type']==116){
												$new_from_from_media_array[] = $new_medi_from[$count_cre_m];
												}
											}
										}
										/*This function will display the user in which profile he is from and display media where that profile was creator Ends here*/
										
										
										/*This function will display the user in which profile he is from or creator and tagged and display media where that profile was creator*/
										$new_medi_tagged = $editprofile->get_the_user_tagin_project($_SESSION['login_email']);
										$new_tagged_from_media_array = array();
										if(!empty($new_medi_tagged))
										{
											for($count_cre_m=0;$count_cre_m<count($new_medi_tagged);$count_cre_m++)
											{
												if($new_medi_tagged[$count_cre_m]['media_type']==116){
												$new_tagged_from_media_array[] = $new_medi_tagged[$count_cre_m];
												}
											}
										}
										
										/*This function will display the user in which profile he is from or creator and tagged and display media where that profile was creator Ends here*/
										
										
										/*This function will display in which media the curent user was creator*/
										$get_where_creator_media = $editprofile->get_media_where_creator_or_from();
										$new_creator_media_array = array();
										if(!empty($get_where_creator_media))
										{
											for($count_tag_m=0;$count_tag_m<count($get_where_creator_media);$count_tag_m++)
											{
												if($get_where_creator_media[$count_tag_m]['media_type']==116){
													$new_creator_media_array[] = $get_where_creator_media[$count_tag_m];
												}
											}
										}
										$chk_fan = $editprofile->chk_fan();
										$new_fan_media_array = array();
										if($chk_fan!="" && $chk_fan!=Null)
										{
											while($row_fan = mysql_fetch_assoc($chk_fan))
											{
												$new_fan_arr[] = $row_fan;
												$get_fan_media = $editprofile->get_fan_media($row_fan['buyer_id'],$row_fan['seller_id'],$row_fan['table_name'],$row_fan['type_id']);
												if($get_fan_media!="" && $get_fan_media!=Null)
												{
													if(mysql_num_rows($get_fan_media)>0)
													{
														while($row_fan_media = mysql_fetch_assoc($get_fan_media))
														{
															$row_fan_media['media_from'] = "media_coming_fan";
															$new_fan_media_array = $row_fan_media;
														}
													}
												}
											}
										}
										$new_fan_media_array = array();
										$acc_medsg = array();
										for($acc_vis=0;$acc_vis<count($acc_media);$acc_vis++)
										{
											if($acc_media[$acc_vis]!="")
											{
												$segl_tagged = $editprofile->get_artist_media_type_tagged_by_other_user($acc_media[$acc_vis]);
												if(!empty($segl_tagged))
												{
													$acc_medsg[] = $segl_tagged;
												}
											}
										}
										$get_gals_final_ct = array();
										$type = '116';
										$cre_frm_med = array();
										$cre_frm_med = $editprofile->get_media_create($type);
										//var_dump($cre_frm_med);
										$get_vgals_pnew_w = array_merge($acc_medsg,$get_channel_id,$get_gals_final_ct,$cre_frm_med,$new_creator_creator_media_array,$new_from_from_media_array,$new_tagged_from_media_array,$new_creator_media_array,$new_fan_media_array);
										
										$sort = array();
										foreach($get_vgals_pnew_w as $k=>$v)
										{		
											$end_c1 = $v['creator'];
											//$create_c = strpos($v['creator'],'(');
											//$end_c1 = substr($v['creator'],0,$create_c);
											$end_c = trim($end_c1);
											
											$end_f1 = $v['from'];
											//$create_f = strpos($v['from'],'(');
											//$end_f1 = substr($v['from'],0,$create_f);
											$end_f = trim($end_f1);
											
											$sort['creator'][$k] = strtolower($end_c);
											$sort['from'][$k] = strtolower($end_f);
											//$sort['track'][$k] = $v['track'];
											$sort['title'][$k] = strtolower($v['title']);
										}
										//var_dump($sort);
										if(!empty($sort))
										{
											array_multisort($sort['creator'], SORT_ASC, $sort['from'], SORT_ASC, $sort['title'], SORT_ASC,$get_vgals_pnew_w);
										}
										
										$dummy_g = array();
										//var_dump($get_vgals_pnew_w);
										$get_all_del_id = $editprofile->get_all_del_media_id();
										$deleted_id = explode(",",$get_all_del_id);
										for($count_del=0;$count_del<=count($get_vgals_pnew_w);$count_del++)
										{
											if($get_vgals_pnew_w[$count_del]==""){continue;}
											else{
												for($count_deleted=0;$count_deleted<=count($deleted_id);$count_deleted++)
												{
													if($deleted_id[$count_deleted]==""){continue;}
													else{
														if($deleted_id[$count_deleted] == $get_vgals_pnew_w[$count_del]['id'])
														{
															$get_vgals_pnew_w[$count_del]="";
														}
													}
												}
											}
										}
										for($acc_glae=0;$acc_glae<count($get_vgals_pnew_w);$acc_glae++)
										{
											if($get_vgals_pnew_w[$acc_glae]['delete_status']==0){
												if(isset($get_vgals_pnew_w[$acc_glae]['id']))
												{
													if(in_array($get_vgals_pnew_w[$acc_glae]['id'],$dummy_g))
													{
														
													}
													else
													{
														$dummy_g[] = $get_vgals_pnew_w[$acc_glae]['id'];
														if($get_vgals_pnew_w[$acc_glae]['id']!="" && $get_vgals_pnew_w[$acc_glae]['media_type']==116)
														{
															//$create_c = strpos($get_vgals_pnew_w[$acc_glae]['creator'],'(');
															//$end_c = substr($get_vgals_pnew_w[$acc_glae]['creator'],0,$create_c);
															$end_c = $get_vgals_pnew_w[$acc_glae]['creator'];
															$end_f = $get_vgals_pnew_w[$acc_glae]['from'];
															//$create_f = strpos($get_vgals_pnew_w[$acc_glae]['from'],'(');
															//$end_f = substr($get_vgals_pnew_w[$acc_glae]['from'],0,$create_f);
															
															$eceksg = "";
															if($end_c!="")
															{
																$eceksg = $end_c;
															}
															if($end_f!="")
															{
																if($end_c!="")
																{
																	$eceksg .= " : ".$end_f;
																}
																else
																{
																	$eceksg .= $end_f;
																}
															}
															if($get_vgals_pnew_w[$acc_glae]['title']!="")
															{
																if($end_c!="" || $end_f!="")
																{
																	$eceksg .= " : ".$get_vgals_pnew_w[$acc_glae]['title'];
																}
																else
																{
																	$eceksg .= $get_vgals_pnew_w[$acc_glae]['title'];
																}
															}
															
															if($get_select_channels_fanclub!="" && $get_select_channels_fanclub!=0)
															{
																$get_select_channels_fanclub = array_unique($get_select_channels_fanclub);
																$yes_com_tag_pro_song =0;
																for($count_sel=0;$count_sel<=count($get_select_channels_fanclub);$count_sel++)
																{
																	if($get_select_channels_fanclub[$count_sel]==""){continue;}
																	else
																	{
																		if($get_select_channels_fanclub[$count_sel] == $get_vgals_pnew_w[$acc_glae]['id'])
																		{
																			$yes_com_tag_pro_song = 1;
											?>
																			<option value="<?php echo $get_vgals_pnew_w[$acc_glae]['id'];?>" selected><?php echo stripslashes($eceksg); ?></option>
											<?php
																		}
																	}
																}
																if($yes_com_tag_pro_song == 0)
																{													
											?>
																	<option value="<?php echo $get_vgals_pnew_w[$acc_glae]['id'];?>"><?php echo stripslashes($eceksg); ?></option>
											<?php
																}
															}
															else
															{
				?>
																<option value="<?php echo $get_vgals_pnew_w[$acc_glae]['id'];?>" ><?php echo stripslashes($eceksg); ?></option>
				<?php
															}
														}
													}
												}
											}
										}
										
										//if(mysql_num_rows($get_artist_gallery_id)<=0 && $media_avail==0 && count($get_media_cre_art_pro)<=0 && mysql_num_rows($get_media_tag_com_pro)<=0 && mysql_num_rows($get_media_tag_art_pro)<=0)
										if(empty($get_vgals_pnew_w))
										{
										?>
											<option value="" >No Listings</option>
										<?php	
										}
										
										?>					
									</select>
								</div>
							</div>	
									<!--<div id="for_sale_yes" style="display:none">
										<div class="fieldCont">
											<div class="fieldTitle">Price</div>
											<input type="text" class="fieldText" name="sale_price" id="sale_price" value="<?php echo $nameres['price'];?>">
										</div>
										<div class="fieldCont">
										  <div class="fieldTitle">Songs</div>
											<select class="list" id="sale_song[]" multiple="multiple" size="5" name="sale_song[]">
												<?php
											/*	$get_sale_club = $editprofile->get_forsale_media();
												while($row_sale = mysql_fetch_assoc($get_sale_club))
												{
													if($row_sale['media_type']==114)
													{
														$sale_song=1;
															$sale_song_sel =0;
															$exp_sale_song = explode(",",$nameres['sale_song']);
															for($count_ss_exp=0;$count_ss_exp<count($exp_sale_song);$count_ss_exp++)
															{
																//
																if($exp_sale_song[$count_ss_exp]=="")
																{
																	continue;
																}
																else if($row_sale['id'] == $exp_sale_song[$count_ss_exp])
																{
																	$sale_song_sel=1;
																	?>
																		<option value="<?php echo $row_sale['id'];?>" selected><?php echo $row_sale['title'];?></option>
																	<?php
																}
															}
															if($sale_song_sel !=1){
														?>
															<option value="<?php echo $row_sale['id'];?>"><?php echo $row_sale['title'];?></option>
														<?php
															}
													}
												}
												if($sale_song==0)
												{
													?>
													<option selected value="0">Select Song</option>
													<?php
												}
												?>	
											</select>
										</div>
										
										<div class="fieldCont">
										  <div class="fieldTitle">Galleries</div>
											<select class="list" id="sale_gallery[]" multiple="multiple" size="5" name="sale_gallery[]">
												<?php
												$get_sale_club = $editprofile->get_forsale_media();
												while($row_sale = mysql_fetch_assoc($get_sale_club))
												{
													if($row_sale['media_type']==113)
													{
															$sale_gallery=1;
															$sale_gallery_sel =0;
															$exp_sale_gallery = explode(",",$nameres['sale_gallery']);
															for($count_sg_exp=0;$count_sg_exp<count($exp_sale_gallery);$count_sg_exp++)
															{
																if($exp_sale_gallery[$count_sg_exp]=="")
																{
																	continue;
																}
																else if($row_sale['id'] == $exp_sale_gallery[$count_sg_exp])
																{
																	$sale_gallery_sel=1;
																	?>
																		<option value="<?php echo $row_sale['id'];?>" selected><?php echo $row_sale['title'];?></option>
																	<?php
																}
															}
															if($sale_gallery_sel !=1){
														?>
															<option value="<?php echo $row_sale['id'];?>"><?php echo $row_sale['title'];?></option>
														<?php
															}
													}
												}
												if($sale_gallery==0)
												{
													?>
													<option selected value="0">Select Gallery</option>
													<?php
												}*/
												?>						
											</select>
										</div>
									</div>-->
								<?php
								}
								?>
<!--Fan Club Code Ends Here-->
								<h4>Share Links</h4>
						
                                <div class="fieldCont">
									<div title="" class="fieldTitle">Facebook</div>
									<div class="urlTitle_share">facebook.com/</div>
									<input title="Select a unique URL to be used for accessing your profile page." name="fb_share" type="text" class="urlField" id="fb_share" value="<?php echo $newrow['fb_share']; ?>" />
                                </div>
								
								<div class="fieldCont">
									<div title="" class="fieldTitle">Twitter</div>
									<div class="urlTitle_share" >twitter.com/</div>
									<input title="Select a unique URL to be used for accessing your profile page." name="twit_share" type="text" class="urlField" id="twit_share" value="<?php echo $newrow['twit_share']; ?>" />
                                </div>
								
								<div class="fieldCont">
									<div title="" class="fieldTitle">Google+</div>
									<div class="urlTitle_share" >plus.google.com/</div>
									<input title="Select a unique URL to be used for accessing your profile page." name="gplus_share" type="text" class="urlField" id="gplus_share" value="<?php echo $newrow['gplus_share']; ?>" />
                                </div>
								
								<div class="fieldCont">
									<div title="" class="fieldTitle">Tumblr</div>
									<input title="Select a unique URL to be used for accessing your profile page." name="tubm_share" type="text" class="urlField" id="tubm_share" value="<?php echo $newrow['tubm_share']; ?>" />
									<div class="urlTitle_share" >.tumblr.com</div>
                                </div>
								
								<div class="fieldCont">
									<div title="" class="fieldTitle">StumbleUpon</div>
									<div class="urlTitle_share" >stumbleupon.com/stumbler/</div>
									<input title="Select a unique URL to be used for accessing your profile page." name="stbu_share" type="text" class="urlField" id="stbu_share" value="<?php echo $newrow['stbu_share']; ?>" />
                                </div>
								
								<div class="fieldCont">
									<div title="" class="fieldTitle">Pinterest</div>
									<div class="urlTitle_share" >pinterest.com/</div>
									<input title="Select a unique URL to be used for accessing your profile page." name="pin_share" type="text" class="urlField" id="pin_share" value="<?php echo $newrow['pin_share']; ?>" />
                                </div>
								
								<div class="fieldCont">
									<div title="" class="fieldTitle">Youtube</div>
									<div class="urlTitle_share" >youtube.com/</div>
									<input title="Select a unique URL to be used for accessing your profile page." name="you_share" type="text" class="urlField" id="you_share" value="<?php echo $newrow['you_share']; ?>" />
                                </div>
								
								<div class="fieldCont">
									<div title="" class="fieldTitle">Vimeo</div>
									<div class="urlTitle_share" >vimeo.com/</div>
									<input title="Select a unique URL to be used for accessing your profile page." name="vimeo_share" type="text" class="urlField" id="vimeo_share" value="<?php echo $newrow['vimeo_share']; ?>" />
                                </div>
								
								<div class="fieldCont">
									<div title="" class="fieldTitle">Soundcloud</div>
									<div class="urlTitle_share" >soundcloud.com/</div>
									<input title="Select a unique URL to be used for accessing your profile page." name="sdcl_share" type="text" class="urlField" id="sdcl_share" value="<?php echo $newrow['sdcl_share']; ?>" />
                                </div>
								
								<div class="fieldCont">
									<div title="" class="fieldTitle">Instagram</div>
									<div class="urlTitle_share" >instagram.com/</div>
									<input title="Select a unique URL to be used for accessing your profile page." name="ints_share" type="text" class="urlField" id="ints_share" value="<?php echo $newrow['ints_share']; ?>" />
                                </div>
								
                                  
                                <h4>Tag Profiles</h4>
								
								<div class="fieldCont" style="font-size:12px;">Select all projects, media and events that you would like to list on this profiles display page.</div>
                                <div class="fieldCont">
                                  <div title="Select media and profiles to display on your profile page." class="fieldTitle">Projects</div>
                                        <?php
										//var_dump($acc_event);
										//	die;
										/* while($get_channel_id=mysql_fetch_assoc($get_project))
										{	//$get_artist_channel=$editprofile->get_artist_channel($get_channel_id['id']);
											//$getchannel=mysql_fetch_assoc($get_artist_channel);
											if($get_select_projects[0]!="")
											{
												for($i=0;$i<count($get_select_projects);$i++)
												{
													if($get_select_projects[$i]==$get_channel_id['id'])
													{
														$j_chan=1;
											?>
												<option value="<?php echo $get_channel_id['id'];?>" selected ><?php echo substr($get_channel_id['creator'],0,20) ." : ". $get_channel_id['title'];?></option>
											<?php
														continue;
													}
												}
												if($j_chan==1)
												{
													$j_chan=0;
													continue;
												}
											}										
										?>
											<option value="<?php echo $get_channel_id['id'];?>" ><?php echo substr($get_channel_id['creator'],0,20) ." : ". $get_channel_id['title'];?></option>
										<?php	
										
										} */
										
										$all_friends_project = array();
										$get_all_friends = $editprofile->Get_all_friends();
										if($get_all_friends !="" && $get_all_friends !=Null)
										{
											while($res_all_friends = mysql_fetch_assoc($get_all_friends))
											{
												$get_friend_art_info = $editprofile->get_friend_art_info($res_all_friends['fgeneral_user_id']);
												$get_all_tag_pro = $editprofile->get_all_tagged_pro($get_friend_art_info);
												if($get_all_tag_pro!="" && $get_all_tag_pro!=Null)
												{
													if(mysql_num_rows($get_all_tag_pro)>0)
													{
														while($res_projects = mysql_fetch_assoc($get_all_tag_pro))
														{
															//$get_project_type = $editprofile->Get_Project_subtype($res_projects['id']);
															$exp_creator = explode('(',$res_projects['creator']);
															$exp_tagged_email = explode(",",$res_projects['tagged_user_email']);
															for($exp_count = 0;$exp_count<count($exp_tagged_email);$exp_count++)
															{
																if($exp_tagged_email[$exp_count]==""){continue;}
																else{
																	if($exp_tagged_email[$exp_count]==$_SESSION['login_email'])
																	{
																		//$res_projects['project_type_name'] = $get_project_type['name'];
																		$res_projects['whos_project'] = "tag_pro_art";
																		$res_projects['id'] = $res_projects['id'] ."~art";
																		$all_friends_project[] = $res_projects;
																	
																		?>
																		<!--<div id="AccordionContainer" class="tableCont">
																			<div class="blkG" style="width:200px;"><?php/* if($res_projects['title']!=""){echo $res_projects['title'];}else{?>&nbsp;<?php }?></div>
																			<div class="blkB" style="width:200px;"><a href=""><?php if($exp_creator[0]!=""){echo $exp_creator[0];}else{?>&nbsp;<?php }?></a></div>
																			<div class="blkB" style="width:100px;"><?php if($get_project_type['name']!=""){echo $get_project_type['name'];}else{?>&nbsp;<?php }?></div>
																			<div class="blkD">&nbsp;</div>
																			<div class="icon">&nbsp;</div>
																			<div class="icon"><a onclick="return confirmdelete('<?php echo $res_projects['title'];?>')" href="insertartistproject.php?delete=<?php echo $res_projects['id'];*/?>"><img src="images/profile/delete.png" /></a></div>
																		</div>-->
																		<?php
																	}
																}
																
															}
														}
													}
												}
											}
										}
										
										$all_community_friends_project = array();
										$get_all_friends = $newgeneral->Get_all_friends();
										if($get_all_friends !="" && $get_all_friends !=Null)
										{
											while($res_all_friends = mysql_fetch_assoc($get_all_friends))
											{
												$get_friend_art_info = $newgeneral->get_friend_art_info($res_all_friends['fgeneral_user_id']);
												$get_all_tag_pro = $newgeneral->get_all_tagged_pro($get_friend_art_info);
												if($get_all_tag_pro!="" && $get_all_tag_pro!=Null)
												{
													if(mysql_num_rows($get_all_tag_pro)>0)
													{
														while($res_projects = mysql_fetch_assoc($get_all_tag_pro))
														{
															//$get_project_type = $newgeneral->Get_Project_subtype($res_projects['id']);
															$exp_creator = explode('(',$res_projects['creator']);
															$exp_tagged_email = explode(",",$res_projects['tagged_user_email']);
															for($exp_count = 0;$exp_count<count($exp_tagged_email);$exp_count++)
															{
																if($exp_tagged_email[$exp_count]==""){continue;}
																else{
																	if($exp_tagged_email[$exp_count]==$_SESSION['login_email'])
																	{
																		//$res_projects['project_type_name'] = $get_project_type['name'];
																		$res_projects['whos_project'] = "tag_pro_com";
																		$res_projects['id'] = $res_projects['id'] ."~com";
																		$all_community_friends_project[] = $res_projects;
																		?>
																		<!--<div id="AccordionContainer" class="tableCont">
																			<div class="blkG" style="width:200px;"><?php /*if($res_projects['title']!=""){echo $res_projects['title'];}else{?>&nbsp;<?php }?></div>
																			<div class="blkB" style="width:200px;"><a href=""><?php if($exp_creator[0]!=""){echo $exp_creator[0];}else{?>&nbsp;<?php }?></a></div>
																			<div class="blkB" style="width:100px;"><?php if($get_project_type['name']!=""){echo $get_project_type['name'];}else{?>&nbsp;<?php }?></div>
																			<div class="blkD">&nbsp;</div>
																			<div class="icon">&nbsp;</div>
																			<div class="icon"><a onclick="return confirmdelete('<?php echo $res_projects['title'];?>')" href="insertcommunityproject.php?delete=<?php echo $res_projects['id'];*/?>"><img src="images/profile/delete.png" /></a></div>
																		</div>-->
																		<?php
																	}
																}
																
															}
														}
													}
												}
											}
										}
										$get_onlycre_id = array();
										$get_onlycre_id = $editprofile->only_creator_this();
										
										$get_only2cre = array();
										$get_only2cre = $editprofile->only_creator2pr_this();
										
										//var_dump($get_onlycre_id);
										$get_proa_id = array();
										if($get_project !="" && $get_project !=Null)
										{
											while($get_proa_ids = mysql_fetch_assoc($get_project))
											{
												$get_proa_ids['id'] = $get_proa_ids['id'].'~'.'art';
												$get_proa_id[] = $get_proa_ids;
											}
										}
										
										$get_proc_id = array();
										if($get_cproject !="" && $get_cproject !=Null)
										{
											while($get_proc_ids = mysql_fetch_assoc($get_cproject))
											{
												$get_proc_ids['id'] = $get_proc_ids['id'].'~'.'com';
												$get_proc_id[] = $get_proc_ids;
											}
										}
										
										$sel_arttagged = array();
										if($project_avail!=0)
										{
											$acc_project = array_unique($acc_project);
											for($acc_pro=0;$acc_pro<count($acc_project);$acc_pro++)
											{
												if($acc_project[$acc_pro]!="" && $acc_project[$acc_pro]!=0)
												{
													$dum_art = $editprofile->get_project_tagged_by_other_user($acc_project[$acc_pro]);
													if(!empty($dum_art) && $dum_art['id']!="" && $dum_art['id']!="")
													{
														$dum_art['id'] = $dum_art['id'].'~'.'art';
														$sel_arttagged[] = $dum_art;
													}
												}
											}
										}
										
										$sel_art2tagged = array();
										if($project_avail!=0)
										{
											$acc_project = array_unique($acc_project);
											for($acc_pro=0;$acc_pro<count($acc_project);$acc_pro++)
											{												
												if($acc_project[$acc_pro]!="" && $acc_project[$acc_pro]!=0)
												{													
													$sql_1_cre = $editprofile->get_project_tagged_by_other2_user($acc_project[$acc_pro]);
													if($sql_1_cre!="")
													{
														if($sql_1_cre!="" && $sql_1_cre!=Null)
														{
															if(mysql_num_rows($sql_1_cre)>0)
															{
																while($run = mysql_fetch_assoc($sql_1_cre))
																{
																	$run['id'] = $run['id'].'~'.'art';
																	$sel_art2tagged[] = $run;
																}
															}
														}
													}
													
													$sql_2_cre = $editprofile->get_project_ctagged_by_other2_user($acc_project[$acc_pro]);
													if($sql_2_cre!="")
													{
														if($sql_2_cre!="" && $sql_2_cre!=Null)
														{
															if(mysql_num_rows($sql_2_cre)>0)
															{
																while($run_c = mysql_fetch_assoc($sql_2_cre))
																{
																	$run_c['id'] = $run_c['id'].'~'.'com';
																	$sel_art2tagged[] = $run_c;
																}
															}
														}
													}
												}
											}
										}
										
										$sel_comtagged = array();
										if($project_cavail!=0)
										{
											$acc_cproject = array_unique($acc_cproject);
											for($acc_pro=0;$acc_pro<count($acc_cproject);$acc_pro++)
											{
												if($acc_cproject[$acc_pro]!="" && $acc_cproject[$acc_pro]!=0)
												{
													$dum_com = $editprofile->get_cproject_tagged_by_other_user($acc_cproject[$acc_pro]);
													if(!empty($dum_com) && $dum_com['id']!="" && $dum_com['id']!="")
													{
														$dum_com['id'] = $dum_com['id'].'~'.'com';
														$sel_comtagged[] = $dum_com;
													}
												}
											}
										}
										
										$sel_com2tagged = array();
										if($project_cavail!=0)
										{
											$acc_cproject = array_unique($acc_cproject);
											for($acc_pro=0;$acc_pro<count($acc_cproject);$acc_pro++)
											{
												if($acc_cproject[$acc_pro]!="" && $acc_cproject[$acc_pro]!=0)
												{
													$sql_1_cre = $editprofile->get_cproject_tagged_by_other2_user($acc_cproject[$acc_pro]);
													if($sql_1_cre!="")
													{
														if($sql_1_cre!="" && $sql_1_cre!=Null)
														{
															if(mysql_num_rows($sql_1_cre)>0)
															{
																while($run = mysql_fetch_assoc($sql_1_cre))
																{
																	$run['id'] = $run['id'].'~'.'art';
																	$sel_com2tagged[] = $run;
																}
															}
														}
													}
													
													$sql_2_cre = $editprofile->get_cproject_ctagged_by_other2_user($acc_cproject[$acc_pro]);
													if($sql_2_cre!="")
													{
														if($sql_2_cre!="" && $sql_2_cre!=Null)
														{
															if(mysql_num_rows($sql_2_cre)>0)
															{
																while($run_c = mysql_fetch_assoc($sql_2_cre))
																{
																	$run_c['id'] = $run_c['id'].'~'.'com';
																	$sel_com2tagged[] = $run_c;
																}
															}
														}
													}
												}
											}
										}
										//var_dump($get_proa_id);
										//var_dump($get_proc_id);
										//var_dump($sel_arttagged);
										//var_dump($sel_comtagged);
										//var_dump($get_onlycre_id);
										$final_arrs = array_merge($get_proa_id,$get_proc_id,$sel_arttagged,$sel_comtagged,$get_onlycre_id,$get_only2cre,$sel_art2tagged,$sel_com2tagged,$all_friends_project,$all_community_friends_project);
										
										$sort = array();
										foreach($final_arrs as $k=>$v) {
											
											$end_c1 = $v['creator'];
											//$create_c = strpos($v['creator'],'(');
											//$end_c1 = substr($v['creator'],0,$create_c);
											$end_c = trim($end_c1);
											
											$sort['creator'][$k] = strtolower($end_c);
											//$sort['from'][$k] = $end_f;
											//$sort['track'][$k] = $v['track'];
											$sort['title'][$k] = strtolower($v['title']);
										}
										//var_dump($sort);
										if(!empty($sort))
										{
											array_multisort($sort['creator'], SORT_ASC, $sort['title'], SORT_ASC,$final_arrs);
										}
										
										$dummy_pr = array();
										
										$get_all_del_id = $editprofile->get_deleted_project_id();
										$exp_del_id = explode(",",$get_all_del_id['deleted_project_id']);
										for($count_all=0;$count_all<count($final_arrs);$count_all++){
											for($count_del=0;$count_del<count($exp_del_id);$count_del++){
												if($exp_del_id[$count_del]!=""){
													if($final_arrs[$count_all]['id'] == $exp_del_id[$count_del]){
														$final_arrs[$count_all] ="";
													}
												}
											}
										}
										
										if(!empty($final_arrs))
										{
				?>
											<select title="Select media and profiles to display on your profile page." name="tagged_projects[]" size="5" multiple="multiple" class="list" id="tagged_projects[]">
				<?php
										for($f_i=0;$f_i<count($final_arrs);$f_i++)
										{
											//if($final_arrs[$f_i]['delete_status']==0){
												if($final_arrs[$f_i]!="")
												{
													if(in_array($final_arrs[$f_i]['id'],$dummy_pr))
													{

													}
													else
													{
														$dummy_pr[] = $final_arrs[$f_i]['id'];
														
														//$create_c = strpos($final_arrs[$f_i]['creator'],'(');
														//$end_c = substr($final_arrs[$f_i]['creator'],0,$create_c);
														$end_c = $final_arrs[$f_i]['creator'];
														
														$ecei = "";
														if($end_c!="")
														{
															$ecei = $end_c;
															$ecei .= ' : '.$final_arrs[$f_i]['title'];
														}
														else
														{
															$ecei = $final_arrs[$f_i]['title'];
														}
														
														if(!empty($get_select_projects))
														{
															$get_select_projects = array_unique($get_select_projects);
															for($i=0;$i<count($get_select_projects);$i++)
															{
																if($get_select_projects[$i]!="")
																{
																	if($get_select_projects[$i]==$final_arrs[$f_i]['id'])
																	{
																		$j_chan=1;
															?>
																		<option value="<?php echo $final_arrs[$f_i]['id'];?>" selected ><?php echo stripslashes($ecei);?></option>
															<?php
																		continue;
																	}
																}
															}
															if($j_chan==1)
															{
																$j_chan=0;
																continue;
															}
														}
													?>
														<option value="<?php echo $final_arrs[$f_i]['id'];?>" ><?php echo stripslashes($ecei);?></option>
		<?php											
													}
												}
											//}
										}
										
										//$project_avail = 0;
										/* if($project_avail!=0)
										{
											for($acc_pro=1;$acc_pro<count($acc_project);$acc_pro++)
											{
													//$sel_tagged="";
												$sel_tagged=$editprofile->get_project_tagged_by_other_user($acc_project[$acc_pro]);
												
												if(isset($sel_tagged) && ($sel_tagged['title']!="" || $sel_tagged['title']!=null))
												{
													if($get_select_projects[0]!="")
													{
														for($i=0;$i<count($get_select_projects);$i++)
														{
															if($get_select_projects[$i]==$sel_tagged['id'])
															{
																$j_chan=1;
													?>
														<option value="<?php echo $sel_tagged['id'];?>" selected ><?php echo substr($sel_tagged['creator'],0,20) ." : ". $sel_tagged['title'];?></option>
													<?php	
																continue;
															}
														}
													
														if($j_chan==1)
														{
															$j_chan=0;
															continue;
														}
													}
												?>
													<option value="<?php echo $sel_tagged['id'];?>" ><?php echo substr($sel_tagged['creator'],0,20) ." : ". $sel_tagged['title'];?></option>
												<?php
												}
											}
										} */
										
										//if(mysql_num_rows($get_project)<=0 && $project_avail==0 && $project_cavail==0 && mysql_num_rows($get_cproject)<=0)
				?>
										</select>
				<?php				
									}
										if(empty($final_arrs))
										{
										?>
											<a style="border-radius:5px; -webkit-border-radius:5px; -moz-border-radius:5px; -ms-border-radius:5px; -o-border-radius:5px; background-color: #000000; border: 0 none; color: #FFFFFF; font-size: 13px; margin-left: 5px; padding: 6px 8px; text-decoration: none;" href="add_artist_project.php">Add Project</a>
										<?php
										}
										?>
                                    			
                                </div>
								
                                <div class="fieldCont">
                                  <div title="Select media and profiles to display on your profile page." class="fieldTitle">Galleries</div>
                                        <?php
										
										$j="";
										/* if(isset($res_gal) && $res_gal!=null)
										{
											//$get_artist_gallery_name1=$editprofile->get_artist_gallery_name_at_register();
											//$get_artist_gallery_name = mysql_fetch_assoc($get_artist_gallery_name1);
											//for($i=0;$i<count($get_select_galleries);$i++)
											//{
												if($get_select_galleries[$i]==$res_gal['gallery_id'])
												{
													$reg_gal=1;
										?>
												<option value="<?php echo $res_gal['gallery_id'];?>" selected ><?php echo substr($newres1['fname'].' '.$newres1['lname'],0,20) ." : ". $res_gal['gallery_title'];?></option>
										<?php
													continue;
												}
											//}
											if($reg_gal==1)	
											{
												$reg_gal=0;
											}
											else
											{
											?>
												<option value="<?php echo $res_gal['gallery_id'];?>"><?php echo substr($newres1['fname'].' '.$newres1['lname'],0,20) ." : ". $res_gal['gallery_title'];?></option>
											<?php
											}
										} */
										$get_gallery_id = array();
										$get_artist_gallery_id=$editprofile->get_artist_gallery_id();
										if($get_artist_gallery_id !="" && $get_artist_gallery_id !=Null)
										{
											while($get_gallery_ids = mysql_fetch_assoc($get_artist_gallery_id))
											{
												$get_gallery_id[] = $get_gallery_ids; 
											}
										}
										
										$get_media_tag_art_pro = $editprofile->get_artist_project_tag($_SESSION['login_email']);
										if($get_media_tag_art_pro!="" && $get_media_tag_art_pro!=Null)
										{
											if(mysql_num_rows($get_media_tag_art_pro)>0)
											{
												$is_g = 0;
												while($art_pro_tag = mysql_fetch_assoc($get_media_tag_art_pro))
												{
													$get_gallery = explode(",",$art_pro_tag['tagged_galleries']);
													for($count_art_t1_song=0;$count_art_t1_song<count($get_gallery);$count_art_t1_song++)
													{
														if($get_gallery[$count_art_t1_song]!="")
														{
															$get_gal_new[$is_g] = $get_gallery[$count_art_t1_song];
															$is_g = $is_g + 1;
														}
													}
												}
												if($get_gal_new!="")
												{
													$get_gal_n = array_unique($get_gal_new);
												}
											}
										}
										
										$get_media_tag_art_eve = $editprofile->get_artist_event_tag($_SESSION['login_email']);
										if($get_media_tag_art_eve!="" && $get_media_tag_art_eve!=Null)
										{
											if(mysql_num_rows($get_media_tag_art_eve)>0)
											{
												$is_g = 0;
												while($art_pro_tag = mysql_fetch_assoc($get_media_tag_art_eve))
												{
													$get_gallery = explode(",",$art_pro_tag['tagged_galleries']);
													for($count_art_t1_song=0;$count_art_t1_song<count($get_gallery);$count_art_t1_song++)
													{
														if($get_gallery[$count_art_t1_song]!="")
														{
															$get_gal_newe[$is_g] = $get_gallery[$count_art_t1_song];
															$is_g = $is_g + 1;
														}
													}
												}
												if($get_gal_newe!="")
												{
													$get_gal_ne = array_unique($get_gal_newe);
												}
											}
										}
										
										$get_media_tag_com_pro = $editprofile->get_community_project_tag($_SESSION['login_email']);
										if($get_media_tag_com_pro !="" && $get_media_tag_com_pro!= Null)
										{
											if(mysql_num_rows($get_media_tag_com_pro)>0)
											{
												$is_gc = 0;
												while($com_pro_tag = mysql_fetch_assoc($get_media_tag_com_pro))
												{
													$get_com_gallery = explode(",",$com_pro_tag['tagged_galleries']);
													for($cou1_com_tag_song=0;$cou1_com_tag_song<count($get_com_gallery);$cou1_com_tag_song++)
													{
														if($get_com_gallery[$cou1_com_tag_song]!="")
														{
															$get_gal_new_c[$is_gc] = $get_com_gallery[$cou1_com_tag_song];
															$is_gc = $is_gc + 1;
														}
													}
												}
												if($get_gal_new_c!="")
												{
													$get_gal_n_c = array_unique($get_gal_new_c);
												}
											}
										}
										
										$get_media_tag_com_eve = $editprofile->get_community_event_tag($_SESSION['login_email']);
										if($get_media_tag_com_eve !="" && $get_media_tag_com_eve!= Null)
										{
											if(mysql_num_rows($get_media_tag_com_eve)>0)
											{
												$is_gc = 0;
												while($com_pro_tag = mysql_fetch_assoc($get_media_tag_com_eve))
												{
													$get_com_gallery = explode(",",$com_pro_tag['tagged_galleries']);
													for($cou1_com_tag_song=0;$cou1_com_tag_song<count($get_com_gallery);$cou1_com_tag_song++)
													{
														if($get_com_gallery[$cou1_com_tag_song]!="")
														{
															$get_gal_new_ce[$is_gc] = $get_com_gallery[$cou1_com_tag_song];
															$is_gc = $is_gc + 1;
														}
													}
												}
												if($get_gal_new_ce!="")
												{
													$get_gal_e_ce = array_unique($get_gal_new_ce);
												}
											}
										}
										
										$get_media_cre_art_pro = $editprofile->get_project_create();
										if(!empty($get_media_cre_art_pro))
										{
											$is_gp = 0;
											//while($art_pro_tag = mysql_fetch_assoc($get_media_cre_art_pro))
											for($cra_i=0;$cra_i<count($get_media_cre_art_pro);$cra_i++)
											{
												$get_cgal = explode(",",$get_media_cre_art_pro[$cra_i]['tagged_galleries']);
												
												for($count_art_cr_g=0;$count_art_cr_g<count($get_cgal);$count_art_cr_g++)
												{
													if($get_cgal[$count_art_cr_g]!="")
													{
														$get_gal_pn[$is_gp] = $get_cgal[$count_art_cr_g];
														$is_gp = $is_gp + 1;
													}
												}
											}
											if($get_gal_pn!="")
											{
												$get_gal_p_new = array_unique($get_gal_pn);
											}
										}
										
										/*$find_creator_heis = $editprofile -> get_creator_heis();
										$get_genral_user_info = $editprofile -> sel_general();
										$where_creator = array();
										if(!empty($find_creator_heis)){
											for($new_cre_med=0;$new_cre_med<=count($find_creator_heis);$new_cre_med++)
											{
												if($find_creator_heis[$new_cre_med]['general_user_id']!= $get_genral_user_info['general_user_id']){
													if($find_creator_heis['media_type']==113){
													$where_creator[] = $find_creator_heis;}
												}
											}
										}
										
										$find_tag_heis = $editprofile -> get_tag_where_heis($_SESSION['login_email']);
										$get_genral_user_info = $editprofile -> sel_general();
										$where_tagged = array();
										if(!empty($find_tag_heis)){
											for($new_cre_med=0;$new_cre_med<=count($find_tag_heis);$new_cre_med++)
											{
												if($find_tag_heis[$new_cre_med]['general_user_id']!= $get_genral_user_info['general_user_id']){
													if($find_tag_heis['media_type']==113){
													$where_tagged[] = $find_tag_heis;}
												}
											}
										}*/
										
										/*This function will display the user in which profile he is creator and display media where that profile was creator*/
										$new_medi = $editprofile->get_creator_heis();
										$new_creator_creator_media_array = array();
										if(!empty($new_medi))
										{
											for($count_cre_m=0;$count_cre_m<count($new_medi);$count_cre_m++)
											{
												if($new_medi[$count_cre_m]['media_type']==113){
													$new_creator_creator_media_array[] = $new_medi[$count_cre_m];
												}
											}
										}
										
										/*This function will display the user in which profile he is creator and display media where that profile was creator Ends here*/
										
										/*This function will display the user in which profile he is from and display media where that profile was creator*/
										$new_medi_from = $editprofile->get_from_heis();
										$new_from_from_media_array = array();
										if(!empty($new_medi_from))
										{
											for($count_cre_m=0;$count_cre_m<count($new_medi_from);$count_cre_m++)
											{
												if($new_medi_from[$count_cre_m]['media_type']==113){
												$new_from_from_media_array[] = $new_medi_from[$count_cre_m];
												}
											}
										}
										/*This function will display the user in which profile he is from and display media where that profile was creator Ends here*/
										
										
										/*This function will display the user in which profile he is from or creator and tagged and display media where that profile was creator*/
										$new_medi_tagged = $editprofile->get_the_user_tagin_project($_SESSION['login_email']);
										$new_tagged_from_media_array = array();
										if(!empty($new_medi_tagged))
										{
											for($count_cre_m=0;$count_cre_m<count($new_medi_tagged);$count_cre_m++)
											{
												if($new_medi_tagged[$count_cre_m]['media_type']==113){
												$new_tagged_from_media_array[] = $new_medi_tagged[$count_cre_m];
												}
											}
										}
										
										/*This function will display the user in which profile he is from or creator and tagged and display media where that profile was creator Ends here*/
										
										
										/*This function will display in which media the curent user was creator*/
										$get_where_creator_media = $editprofile->get_media_where_creator_or_from();
										$new_creator_media_array = array();
										if(!empty($get_where_creator_media))
										{
											for($count_tag_m=0;$count_tag_m<count($get_where_creator_media);$count_tag_m++)
											{
												if($get_where_creator_media[$count_tag_m]['media_type']==113){
													$new_creator_media_array[] = $get_where_creator_media[$count_tag_m];
												}
											}
										}
										//var_dump($new_creator_media_array);
										/*This function will display in which media the curent user was creator ends here*/
										
										if(!isset($get_gal_n))
										{
											$get_gal_n = array();
										}
										
										if(!isset($get_gal_ne))
										{
											$get_gal_ne = array();
										}
										
										if(!isset($get_gal_n_c))
										{
											$get_gal_n_c = array();
										}
										
										if(!isset($get_gal_p_new))
										{
											$get_gal_p_new = array();
										}
										
										if(!isset($get_gal_e_ce))
										{
											$get_gal_e_ce = array();
										}
										
										$get_gals_pnew_w_c = array_merge($get_gal_n,$get_gal_n_c,$get_gal_p_new,$get_gal_ne,$get_gal_e_ce);
										//var_dump($get_gals_pnew_w_c);
										$get_gals_pnew_g = array_unique($get_gals_pnew_w_c);
										$get_gals_final_ct = array();
										
										for($count_art_cr_gs=0;$count_art_cr_gs<count($get_gals_pnew_g);$count_art_cr_gs++)
										{
											if($get_gals_pnew_g[$count_art_cr_gs]!="")
											{
												$get_media_tag_g_art = $editprofile->get_tag_media_info($get_gals_pnew_g[$count_art_cr_gs]);
												$get_gals_final_ct[] = $get_media_tag_g_art;
											}
										}
										//var_dump($get_gals_final_ct);
										$chk_fan = $editprofile->chk_fan();
										$new_fan_media_array = array();
										if($chk_fan !="" && $chk_fan !=Null)
										{
											while($row_fan = mysql_fetch_assoc($chk_fan))
											{
												$new_fan_arr[] = $row_fan;
												$get_fan_media = $editprofile->get_fan_media($row_fan['buyer_id'],$row_fan['seller_id'],$row_fan['table_name'],$row_fan['type_id']);
												if($get_fan_media !="" && $get_fan_media!= Null)
												{
													if(mysql_num_rows($get_fan_media)>0)
													{
														while($row_fan_media = mysql_fetch_assoc($get_fan_media))
														{
															$row_fan_media['media_from'] = "media_coming_fan";
															$new_fan_media_array = $row_fan_media;
														}
													}
												}
											}
										}
										$new_fan_media_array = array();
										$acc_medsg = array();
										for($acc_vis=0;$acc_vis<count($acc_media);$acc_vis++)
										{
											if($acc_media[$acc_vis]!="")
											{
												$segl_tagged = $editprofile->get_artist_media_type_tagged_by_other_user($acc_media[$acc_vis]);
												if(!empty($segl_tagged))
												{
													$acc_medsg[] = $segl_tagged;
												}
											}
										}
										//var_dump($acc_medsg);
										$get_gals_final_ct = array();
										$type = '113';
										$cre_frm_med = array();
										$cre_frm_med = $editprofile->get_media_create($type);
										//var_dump($cre_frm_med);
										
										/*var_dump($acc_medsg);
										var_dump($get_gallery_id);
										var_dump($get_gals_final_ct);
										var_dump($cre_frm_med);
										var_dump($new_creator_creator_media_array);
										var_dump($new_from_from_media_array);
										var_dump($new_tagged_from_media_array);
										var_dump($new_creator_media_array);
										var_dump($new_fan_media_array);*/
										$get_vgals_pnew_w = array_merge($acc_medsg,$get_gallery_id,$get_gals_final_ct,$cre_frm_med,$new_creator_creator_media_array,$new_from_from_media_array,$new_tagged_from_media_array,$new_creator_media_array,$new_fan_media_array);
										
										$sort = array();
										foreach($get_vgals_pnew_w as $k=>$v)
										{			
											$end_c1 = $v['creator'];
											//$create_c = strpos($v['creator'],'(');
											//$end_c1 = substr($v['creator'],0,$create_c);
											$end_c = trim($end_c1);
											
											$end_f1 = $v['from'];
											//$create_f = strpos($v['from'],'(');
											//$end_f1 = substr($v['from'],0,$create_f);
											$end_f = trim($end_f1);
											
											$sort['creator'][$k] = strtolower($end_c);
											$sort['from'][$k] = strtolower($end_f);
											//$sort['track'][$k] = $v['track'];
											$sort['title'][$k] = strtolower($v['title']);
										}
										//var_dump($sort);
										if(!empty($sort))
										{
											array_multisort($sort['creator'], SORT_ASC, $sort['from'], SORT_ASC, $sort['title'], SORT_ASC,$get_vgals_pnew_w);
										}
										
										if(!empty($get_vgals_pnew_w))
										{
									?>
											<select title="Select media and profiles to display on your profile page." name="tagged_gallery[]" size="5" multiple="multiple" id="tagged_gallery[]" class="list">
									<?php
										$dummy_g = array();
										//var_dump($get_vgals_pnew_w);
										$get_all_del_id = $editprofile->get_all_del_media_id();
										$deleted_id = explode(",",$get_all_del_id);
										for($count_del=0;$count_del<=count($get_vgals_pnew_w);$count_del++)
										{
											if($get_vgals_pnew_w[$count_del]==""){continue;}
											else{
												for($count_deleted=0;$count_deleted<=count($deleted_id);$count_deleted++)
												{
													if($deleted_id[$count_deleted]==""){continue;}
													else{
														if($deleted_id[$count_deleted] == $get_vgals_pnew_w[$count_del]['id'])
														{
															$get_vgals_pnew_w[$count_del]="";
														}
													}
												}
											}
										}
										for($acc_glae=0;$acc_glae<count($get_vgals_pnew_w);$acc_glae++)
										{
											if($get_vgals_pnew_w[$acc_glae]['delete_status']==0){
											if(isset($get_vgals_pnew_w[$acc_glae]['id']))
											{
												if(in_array($get_vgals_pnew_w[$acc_glae]['id'],$dummy_g))
												{
													
												}
												else
												{
													$dummy_g[] = $get_vgals_pnew_w[$acc_glae]['id'];
													if($get_vgals_pnew_w[$acc_glae]['id']!="" && $get_vgals_pnew_w[$acc_glae]['media_type']==113)
													{
														//$create_c = strpos($get_vgals_pnew_w[$acc_glae]['creator'],'(');
														//$end_c = substr($get_vgals_pnew_w[$acc_glae]['creator'],0,$create_c);
														$end_c = $get_vgals_pnew_w[$acc_glae]['creator'];
														$end_f = $get_vgals_pnew_w[$acc_glae]['from'];
														//$create_f = strpos($get_vgals_pnew_w[$acc_glae]['from'],'(');
														//$end_f = substr($get_vgals_pnew_w[$acc_glae]['from'],0,$create_f);
														
														$eceksg = "";
														if($end_c!="")
														{
															$eceksg = $end_c;
														}
														if($end_f!="")
														{
															if($end_c!="")
															{
																$eceksg .= " : ".$end_f;
															}
															else
															{
																$eceksg .= $end_f;
															}
														}
														if($get_vgals_pnew_w[$acc_glae]['title']!="")
														{
															if($end_c!="" || $end_f!="")
															{
																$eceksg .= " : ".$get_vgals_pnew_w[$acc_glae]['title'];
															}
															else
															{
																$eceksg .= $get_vgals_pnew_w[$acc_glae]['title'];
															}
														}
														
														if($get_select_galleries!="")
														{
															$get_select_galleries = array_unique($get_select_galleries);
															$yes_com_tag_pro_song =0;
															for($count_sel=0;$count_sel<=count($get_select_galleries);$count_sel++)
															{
																if($get_select_galleries[$count_sel]==""){continue;}
																else
																{
																	if($get_select_galleries[$count_sel] == $get_vgals_pnew_w[$acc_glae]['id'])
																	{
																		$yes_com_tag_pro_song = 1;
										?>
																		<option value="<?php echo $get_vgals_pnew_w[$acc_glae]['id'];?>" selected><?php echo stripslashes($eceksg); ?></option>
										<?php
																	}
																}
															}
															if($yes_com_tag_pro_song == 0)
															{													
										?>
																<option value="<?php echo $get_vgals_pnew_w[$acc_glae]['id'];?>"><?php echo stripslashes($eceksg); ?></option>
										<?php
															}
														}
														else
														{
			?>
															<option value="<?php echo $get_vgals_pnew_w[$acc_glae]['id'];?>" ><?php echo stripslashes($eceksg); ?></option>
			<?php
														}
													}
												}
											}
											}
										}
										
										/* for($count_art_cr_g=0;$count_art_cr_g<count($get_gals_pnew);$count_art_cr_g++)
										{
											if($get_gals_pnew[$count_art_cr_g]!=""){
											
											$art_coms_g = '';
											if($get_media_cre_art_pro[$cra_i]['artist_id']!=0 && $get_media_cre_art_pro[$cra_i]['artist_id']!="")
											{
												$art_coms_g = 'artist~'.$get_media_cre_art_pro[$cra_i]['artist_id'];
											}
											elseif($get_media_cre_art_pro[$cra_i]['community_id']!=0 && $get_media_cre_art_pro[$cra_i]['community_id']!="")
											{
												$art_coms_g = 'community~'.$get_media_cre_art_pro[$cra_i]['community_id'];
											}

											$reg_meds = $editprofile->get_media_reg_info($get_gals_pnew[$count_art_cr_g],$art_coms_g);
											if($reg_meds!=NULL && $reg_meds!='')
											{
												if($reg_meds=='general_artist_gallery')
												{
													$sql_a = mysql_query("SELECT * FROM general_artist_gallery_list WHERE gallery_id='".$get_gals_pnew[$count_art_cr_g]."'");
													if(mysql_num_rows($sql_a)>0)
													{
														$ans_a = mysql_fetch_assoc($sql_a);
														$sql_gens = $editprofile->sel_general_aid($get_media_cre_art_pro[$cra_i]['artist_id']);
																
														if($get_select_galleries!="")
														{
																//$get_sel_song =$editprofile->get_sel_songs($_GET['edit']);
																//$exp_song_id = explode(",",$get_sel_song['songs']);
																$yes_com_tag_pro_song =0;
																for($count_sela=0;$count_sela<=count($get_select_galleries);$count_sela++)
																{
																	if($get_select_galleries[$count_sela]=="")
																	{continue;}
																	else
																	{
																		if($get_select_galleries[$count_sela] == $ans_a['gallery_id'])
																		{
																			$yes_com_tag_pro_song = 1;
					?>															
																			<option value="<?php echo $ans_a['gallery_id'];?>" selected><?php echo substr($sql_gens['fname'] ." ". $sql_gens['lname'],0,20) ." : ".  $ans_a['gallery_title'];?></option>
					<?php
																		}
																	}
																}
																if($yes_com_tag_pro_song == 0)
																{
					?>
																	<option value="<?php echo $ans_a['gallery_id'];?>"><?php echo substr($sql_gens['fname'] ." ". $sql_gens['lname'],0,20) ." : ".  $ans_a['gallery_title'];?></option>
					<?php
																}
															}
															else
															{
					?>
																<option value="<?php echo $ans_a['gallery_id'];?>"><?php echo substr($sql_gens['fname'] ." ". $sql_gens['lname'],0,20) ." : ".  $ans_a['gallery_title'];?></option>
					<?php
															}
														}
													}
													if($reg_meds=='general_community_gallery')
													{
														$sql_a = mysql_query("SELECT * FROM general_community_gallery_list WHERE gallery_id='".$get_gals_pnew[$count_art_cr_g]."'");
														if(mysql_num_rows($sql_a)>0)
														{
															$ans_a = mysql_fetch_assoc($sql_a);
															$sql_gens = $editprofile->sel_general_cid($get_media_cre_art_pro[$cra_i]['community_id']);
															
															if($get_select_galleries!="")
															{
																//$get_sel_song =$editprofile->get_sel_songs($_GET['edit']);
																//$exp_song_id = explode(",",$get_sel_song['songs']);
																$yes_com_tag_pro_song =0;
																for($count_selc=0;$count_selc<=count($get_select_galleries);$count_selc++)
																{
																	if($get_select_galleries[$count_selc]==""){continue;}
																	else
																	{
																		if($get_select_galleries[$count_selc] == $ans_a['gallery_id'])
																		{
																			$yes_com_tag_pro_song = 1;
					?>															
																			<option value="<?php echo $ans_a['gallery_id'];?>" selected><?php echo substr($sql_gens['fname'] ." ". $sql_gens['lname'],0,20) ." : ".  $ans_a['gallery_title'];?></option>
					<?php
																		}
																	}
																}
																if($yes_com_tag_pro_song == 0)
																{												
					?>
																	<option value="<?php echo $ans_a['gallery_id'];?>"><?php echo substr($sql_gens['fname'] ." ". $sql_gens['lname'],0,20) ." : ".  $ans_a['gallery_title'];?></option>
					<?php
																}
															}
															else
															{
					?>
																<option value="<?php echo $ans_a['gallery_id'];?>"><?php echo substr($sql_gens['fname'] ." ". $sql_gens['lname'],0,20) ." : ".  $ans_a['gallery_title'];?></option>
					<?php
															}
														}
													}
												}
												else
												{
													$get_media_tag_art_pro_song = $editprofile->get_tag_media_info($get_gals_pnew[$count_art_cr_g]);
													
													if($get_select_galleries!="")
													{
														//$get_sel_song =$editprofile->get_sel_songs($_GET['edit']);
														//$exp_song_id = explode(",",$get_sel_song['songs']);
														$yes_com_tag_pro_song =0;
														for($count_sel=0;$count_sel<=count($get_select_galleries);$count_sel++)
														{
															if($get_select_galleries[$count_sel]==""){continue;}
															else
															{
																if($get_select_galleries[$count_sel] == $get_media_tag_art_pro_song['id'])
																{
																	$yes_com_tag_pro_song = 1;
					?>
																	<option value="<?php echo $get_media_tag_art_pro_song['id'];?>" selected><?php echo substr($get_media_tag_art_pro_song['creator'],0,20) ." : ". substr($get_media_tag_art_pro_song['from'],0,20) ." : ".  $get_media_tag_art_pro_song['title'];?></option>
					<?php
																}
															}
														}
														if($yes_com_tag_pro_song == 0)
														{											
					?>
															<option value="<?php echo $get_media_tag_art_pro_song['id'];?>"><?php echo substr($get_media_tag_art_pro_song['creator'],0,20) ." : ". substr($get_media_tag_art_pro_song['from'],0,20) ." : ".  $get_media_tag_art_pro_song['title'];?></option>
					<?php
														}
													}
													else
													{
					?>
														<option value="<?php echo $get_media_tag_art_pro_song['id'];?>"><?php echo substr($get_media_tag_art_pro_song['creator'],0,20) ." : ". substr($get_media_tag_art_pro_song['from'],0,20) ." : ".  $get_media_tag_art_pro_song['title'];?></option>
					<?php
													}
												}
											}
										} */
										
										//if(mysql_num_rows($get_artist_gallery_id)<=0 && $media_avail==0 && count($get_media_cre_art_pro)<=0 && mysql_num_rows($get_media_tag_com_pro)<=0 && mysql_num_rows($get_media_tag_art_pro)<=0)
										//var_dump($get_vgals_pnew_w);
									?>
											</select>
									<?php
									}
										if(empty($get_vgals_pnew_w))
										{
										?>
											<a style="border-radius:5px; -webkit-border-radius:5px; -moz-border-radius:5px; -ms-border-radius:5px; -o-border-radius:5px; background-color: #000000; border: 0 none; color: #FFFFFF; font-size: 13px; margin-left: 5px; padding: 6px 8px; text-decoration: none;" href="add_media.php?adds=113">Add Gallery</a>
										<?php	
										}
										
										?>
                                    
                                </div>
								
                                <div class="fieldCont">
                                  <div title="Select media and profiles to display on your profile page." class="fieldTitle">Songs</div>
                                        <?php
										$get_song_id = array();
										/* if(isset($res_song) && $res_song!=null)
										{
											if($get_select_songs!="")
											{
												echo count($get_select_songs);
												//for($i=0;$i<count($get_select_songs);$i++)
												//{
													if($get_select_songs[$i]==$res_song['audio_id'])
													{
											?>
													<option value="<?php echo $res_song['audio_id'];?>" selected ><?php echo substr($newres1['fname'].' '.$newres1['lname'],0,20) ." : ". $res_song['audio_name'];?></option>
											<?php
														continue;
													}
													else
													{
														?>
												<option value="<?php echo $res_song['audio_id'];?>"><?php echo substr($newres1['fname'].' '.$newres1['lname'],0,20) ." : ". $res_song['audio_name'];?></option>
												<?php
													}
												//}
											}
											else
											{
												?>
													<option value="<?php echo $res_song['audio_id'];?>"><?php echo $res_song['audio_name'];?></option>
												<?php
											}
										} */
										$get_song_id = array();
										$get_artist_song_id=$editprofile->get_artist_song_id();
										if($get_artist_song_id !="" && $get_artist_song_id !=Null)
										{
											while($get_song_ids = mysql_fetch_assoc($get_artist_song_id))
											{
												$get_song_id[] = $get_song_ids;
											}
										}
										$get_media_tag_art_pro = $editprofile->get_artist_project_tag($_SESSION['login_email']);
										if($get_media_tag_art_pro !="" && $get_media_tag_art_pro!= Null)
										{
											if(mysql_num_rows($get_media_tag_art_pro)>0)
											{
												$is = 0;
												while($art_pro_tag = mysql_fetch_assoc($get_media_tag_art_pro))
												{
													$get_songs = explode(",",$art_pro_tag['tagged_songs']);
													for($count_art_tag_song=0;$count_art_tag_song<count($get_songs);$count_art_tag_song++)
													{
														if($get_songs[$count_art_tag_song]!="")
														{
															$get_songs_new[$is] = $get_songs[$count_art_tag_song];
															$is = $is + 1;
														}
													}
												}
												if($get_songs_new!="")
												{
													$get_songs_n = array_unique($get_songs_new);
												}
											}
										}
										
										$get_media_tag_art_eve = $editprofile->get_artist_event_tag($_SESSION['login_email']);
										if($get_media_tag_art_eve !="" && $get_media_tag_art_eve!= Null)
										{
											if(mysql_num_rows($get_media_tag_art_eve)>0)
											{
												$is = 0;
												while($art_pro_tag = mysql_fetch_assoc($get_media_tag_art_eve))
												{
													$get_songs = explode(",",$art_pro_tag['tagged_songs']);
													for($count_art_tag_song=0;$count_art_tag_song<count($get_songs);$count_art_tag_song++)
													{
														if($get_songs[$count_art_tag_song]!="")
														{
															$get_songs_newe[$is] = $get_songs[$count_art_tag_song];
															$is = $is + 1;
														}
													}
												}
												if($get_songs_newe!="")
												{
													$get_songs_ne = array_unique($get_songs_newe);
												}
											}
										}
										
										$get_media_tag_com_pro = $editprofile->get_community_project_tag($_SESSION['login_email']);
										if($get_media_tag_com_pro !="" && $get_media_tag_com_pro!= Null)
										{
											if(mysql_num_rows($get_media_tag_com_pro)>0)
											{
												$is_c = 0;
												while($com_pro_tag = mysql_fetch_assoc($get_media_tag_com_pro))
												{
													$get_com_songs = explode(",",$com_pro_tag['tagged_songs']);
													for($count_com_tag_song=0;$count_com_tag_song<count($get_com_songs);$count_com_tag_song++)
													{
														if($get_com_songs[$count_com_tag_song]!="")
														{
															$get_songs_new_c[$is_c] = $get_com_songs[$count_com_tag_song];
															$is_c = $is_c + 1;
														}
													}
												}
												if($get_songs_new_c!="")
												{
													$get_songs_n_c = array_unique($get_songs_new_c);
												}
											}
										}
										
										$get_media_tag_com_eve = $editprofile->get_community_event_tag($_SESSION['login_email']);
										if($get_media_tag_com_eve !="" && $get_media_tag_com_eve!= Null)
										{
											if(mysql_num_rows($get_media_tag_com_eve)>0)
											{
												$is_c = 0;
												while($com_pro_tag = mysql_fetch_assoc($get_media_tag_com_eve))
												{
													$get_com_songs = explode(",",$com_pro_tag['tagged_songs']);
													for($count_com_tag_song=0;$count_com_tag_song<count($get_com_songs);$count_com_tag_song++)
													{
														if($get_com_songs[$count_com_tag_song]!="")
														{
															$get_songs_new_ce[$is_c] = $get_com_songs[$count_com_tag_song];
															$is_c = $is_c + 1;
														}
													}
												}
												if($get_songs_new_ce!="")
												{
													$get_songs_n_ec = array_unique($get_songs_new_ce);
												}
											}
										}
										
										$get_media_cre_art_pro = $editprofile->get_project_create();
										if(!empty($get_media_cre_art_pro))
										{
											$is_p = 0;
											//while($art_pro_tag = mysql_fetch_assoc($get_media_cre_art_pro))
											for($cr_i=0;$cr_i<count($get_media_cre_art_pro);$cr_i++)
											{
												$get_csongs = explode(",",$get_media_cre_art_pro[$cr_i]['tagged_songs']);
												for($count_art_cr_song=0;$count_art_cr_song<count($get_csongs);$count_art_cr_song++)
												{
													if($get_csongs[$count_art_cr_song]!="")
													{
														$get_songs_pn[$is_p] = $get_csongs[$count_art_cr_song];
														$is_p = $is_p + 1;
													}
												}
											}
											if($get_songs_pn!="")
											{
												$get_songs_p_new = array_unique($get_songs_pn);
											}
										}
										
										/*This function will display the user in which profile he is creator and display media where that profile was creator*/
										$new_medi = $editprofile->get_creator_heis();
										$new_creator_creator_media_array = array();
										if(!empty($new_medi))
										{
											for($count_cre_m=0;$count_cre_m<count($new_medi);$count_cre_m++)
											{
												if($new_medi[$count_cre_m]['media_type']==114){
													$new_creator_creator_media_array[] = $new_medi[$count_cre_m];
												}
											}
										}
										//var_dump($new_creator_creator_media_array);
										/*This function will display the user in which profile he is creator and display media where that profile was creator Ends here*/
										
										/*This function will display the user in which profile he is from and display media where that profile was creator*/
										$new_medi_from = $editprofile->get_from_heis();
										$new_from_from_media_array = array();
										if(!empty($new_medi_from))
										{
											for($count_cre_m=0;$count_cre_m<count($new_medi_from);$count_cre_m++)
											{
												if($new_medi_from[$count_cre_m]['media_type']==114){
												$new_from_from_media_array[] = $new_medi_from[$count_cre_m];
												}
											}
										}
										//var_dump($new_from_from_media_array);
										/*This function will display the user in which profile he is from and display media where that profile was creator Ends here*/
										
										
										/*This function will display the user in which profile he is from or creator and tagged and display media where that profile was creator*/
										$new_medi_tagged = $editprofile->get_the_user_tagin_project($_SESSION['login_email']);
										$new_tagged_from_media_array = array();
										if(!empty($new_medi_tagged))
										{
											for($count_cre_m=0;$count_cre_m<count($new_medi_tagged);$count_cre_m++)
											{
												if($new_medi_tagged[$count_cre_m]['media_type']==114){
												$new_tagged_from_media_array[] = $new_medi_tagged[$count_cre_m];
												}
											}
										}
										//var_dump($new_tagged_from_media_array);
										/*This function will display the user in which profile he is from or creator and tagged and display media where that profile was creator Ends here*/
										
										
										/*This function will display in which media the curent user was creator*/
										$get_where_creator_media = $editprofile->get_media_where_creator_or_from();
										$new_creator_media_array = array();
										if(!empty($get_where_creator_media))
										{
											for($count_tag_m=0;$count_tag_m<count($get_where_creator_media);$count_tag_m++)
											{
												if($get_where_creator_media[$count_tag_m]['media_type']==114){
													$new_creator_media_array[] = $get_where_creator_media[$count_tag_m];
												}
											}
										}
										//var_dump($new_creator_media_array);
										/*This function will display in which media the curent user was creator ends here*/
										
										if(!isset($get_songs_n))
										{
											$get_songs_n = array();
										}
										
										if(!isset($get_songs_n_ec))
										{
											$get_songs_n_ec = array();
										}
										
										if(!isset($get_songs_ne))
										{
											$get_songs_ne = array();
										}
										
										if(!isset($get_songs_n_c))
										{
											$get_songs_n_c = array();
										}
										
										if(!isset($get_songs_p_new))
										{
											$get_songs_p_new = array();
										}
										
										
										$get_songs_pnew_w_c = array_merge($get_songs_n,$get_songs_n_c,$get_songs_p_new,$get_songs_ne,$get_songs_n_ec);
										$get_songs_pnew_ct = array_unique($get_songs_pnew_w_c);
										$get_songs_final_ct = array();
										
										for($count_art_cr_song=0;$count_art_cr_song<count($get_songs_pnew_ct);$count_art_cr_song++)
										{
											if($get_songs_pnew_ct[$count_art_cr_song]!="")
											{
												$get_media_tag_art_pro_song = $editprofile->get_tag_media_info($get_songs_pnew_ct[$count_art_cr_song]);
												$get_songs_final_ct[] = $get_media_tag_art_pro_song;
											}
										}
										
										//var_dump($get_songs_final_ct);
										
										$acc_medss = array();
										for($acc_vis=0;$acc_vis<count($acc_media);$acc_vis++)
										{
											if($acc_media[$acc_vis]!="")
											{
												$sels_tagged = $editprofile->get_artist_media_type_tagged_by_other_user($acc_media[$acc_vis]);
												if(!empty($sels_tagged))
												{
													
													$acc_medss[] = $sels_tagged;
												}
											}
										}
										
										//var_dump($acc_medss);
										//var_dump($acc_medss);
										$type = '114';
										$cre_frm_med = array();
										$cre_frm_med = $editprofile->get_media_create($type);
										
										//var_dump($acc_medss);
										//var_dump($get_song_id);
										//var_dump($get_songs_final_ct);
										//var_dump($cre_frm_med);
										//var_dump($new_creator_creator_media_array);
										//var_dump($new_from_from_media_array);
										//var_dump($new_tagged_from_media_array);
										//var_dump($new_creator_media_array);
										
										
										$get_songs_final_ct = array();
										$get_songs_pnew_w = array_merge($acc_medss,$get_song_id,$get_songs_final_ct,$cre_frm_med,$new_creator_creator_media_array,$new_from_from_media_array,$new_tagged_from_media_array,$new_creator_media_array);
										$getsong1 = array();
										for($acc_song1=0;$acc_song1<count($get_songs_pnew_w);$acc_song1++)
										{
											if($get_songs_pnew_w[$acc_song1]['id']!="" && $get_songs_pnew_w[$acc_song1]['id']!=0)
											{
												$get_artist_song1 = $editprofile->get_artist_song($get_songs_pnew_w[$acc_song1]['id']);
												if($get_artist_song1!="" && $get_artist_song1!=Null)
												{
													$getsong1 = mysql_fetch_assoc($get_artist_song1);
												}
												$ids = $get_songs_pnew_w[$acc_song1]['id'];
												$get_songs_pnew_w[$acc_song1]['track'] = $getsong1['track'];
											}
										}
										
										$sort = array();
										//var_dump($get_songs_pnew_w);
										foreach($get_songs_pnew_w as $k=>$v) {
										
											$end_c1 = $v['creator'];
											//$create_c = strpos($v['creator'],'(');
											//$end_c1 = substr($v['creator'],0,$create_c);
											$end_c = trim($end_c1);
											
											$end_f1 = $v['from'];
											//$create_f = strpos($v['from'],'(');
											//$end_f1 = substr($v['from'],0,$create_f);
											$end_f = trim($end_f1);
											
											$sort['creator'][$k] = strtolower($end_c);
											$sort['from'][$k] = strtolower($end_f);
											$sort['track'][$k] = $v['track'];
											$sort['title'][$k] = strtolower($v['title']);
										}
										//var_dump($sort);
										if(!empty($sort))
										{
											array_multisort($sort['creator'], SORT_ASC, $sort['from'], SORT_ASC, $sort['track'], SORT_ASC, $sort['title'], SORT_ASC,$get_songs_pnew_w);
										}
										
										$dummy_s = array();
										$get_all_del_id = $editprofile->get_all_del_media_id();
										$deleted_id = explode(",",$get_all_del_id);
										for($count_del=0;$count_del<=count($get_songs_pnew_w);$count_del++)
										{
											if($get_songs_pnew_w[$count_del]==""){continue;}
											else{
												for($count_deleted=0;$count_deleted<=count($deleted_id);$count_deleted++)
												{
													if($deleted_id[$count_deleted]==""){continue;}
													else{
														if($deleted_id[$count_deleted] == $get_songs_pnew_w[$count_del]['id'])
														{
															$get_songs_pnew_w[$count_del]="";
														}
													}
												}
											}
										}
										
										if(!empty($get_songs_pnew_w))
										{
						?>
									<select title="Select media and profiles to display on your profile page." name="tagged_songs[]" size="5" multiple="multiple" id="tagged_songs[]" class="list">
						<?php
										for($acc_song=0;$acc_song<count($get_songs_pnew_w);$acc_song++)
										{
											if($get_songs_pnew_w[$acc_song]['delete_status']==0){
												if(isset($get_songs_pnew_w[$acc_song]['id']))
												{
													if(in_array($get_songs_pnew_w[$acc_song]['id'],$dummy_s))
													{
														
													}
													else
													{
														$dummy_s[] = $get_songs_pnew_w[$acc_song]['id'];
														if($get_songs_pnew_w[$acc_song]['id']!="" && $get_songs_pnew_w[$acc_song]['media_type']==114)
														{
															//$create_c = strpos($get_songs_pnew_w[$acc_song]['creator'],'(');
															//$end_c = substr($get_songs_pnew_w[$acc_song]['creator'],0,$create_c);
															$end_c = $get_songs_pnew_w[$acc_song]['creator'];
															$end_f = $get_songs_pnew_w[$acc_song]['from'];
															//$create_f = strpos($get_songs_pnew_w[$acc_song]['from'],'(');
															//$end_f = substr($get_songs_pnew_w[$acc_song]['from'],0,$create_f);
															
															$eceks = "";
															if($end_c!="")
															{
																$eceks = $end_c;
															}
															if($end_f!="")
															{
																if($end_c!="")
																{
																	$eceks .= " : ".$end_f;
																}
																else
																{
																	$eceks .= $end_f;
																}
															}
															if($get_songs_pnew_w[$acc_song]['track']!="")
															{
																if($end_c!="" || $end_f!="")
																{
																	$eceks .= " : ".$get_songs_pnew_w[$acc_song]['track'];
																}
																else
																{
																	$eceks .= $get_songs_pnew_w[$acc_song]['track'];
																}
															}
															if($get_songs_pnew_w[$acc_song]['title']!="")
															{
																if($end_c!="" || $end_f!="" || $get_songs_pnew_w[$acc_song]['track']!="")
																{
																	$eceks .= " : ".$get_songs_pnew_w[$acc_song]['title'];
																}
																else
																{
																	$eceks .= $get_songs_pnew_w[$acc_song]['title'];
																}
															}
															
															if($get_select_songs!="")
															{
																$get_select_songs = array_unique($get_select_songs);
																$yes_com_tag_pro_song =0;
																for($count_sel=0;$count_sel<=count($get_select_songs);$count_sel++)
																{
																	if($get_select_songs[$count_sel]==""){continue;}
																	else
																	{
																		if($get_select_songs[$count_sel] == $get_songs_pnew_w[$acc_song]['id'])
																		{
																			$yes_com_tag_pro_song = 1;
											?>
																			<option value="<?php echo $get_songs_pnew_w[$acc_song]['id'];?>" selected><?php echo stripslashes($eceks); ?></option>
											<?php
																		}
																	}
																}
																if($yes_com_tag_pro_song == 0)
																{													
											?>
																	<option value="<?php echo $get_songs_pnew_w[$acc_song]['id'];?>"><?php echo stripslashes($eceks); ?></option>
											<?php
																}
															}
															else
															{
				?>
																<option value="<?php echo $get_songs_pnew_w[$acc_song]['id'];?>" ><?php echo stripslashes($eceks); ?></option>
				<?php
															}
														}
													}
												}
											}
										}
										
								/* for($count_art_cr_song=0;$count_art_cr_song<count($get_songs_pnew);$count_art_cr_song++)
								{
									if($get_songs_pnew[$count_art_cr_song]!=""){
									$get_media_tag_art_pro_song = $editprofile->get_tag_media_info($get_songs_pnew[$count_art_cr_song]);
									
									if($get_select_songs!="")
									{
										//$get_sel_song =$editprofile->get_sel_songs($_GET['edit']);
										//$exp_song_id = explode(",",$get_sel_song['songs']);
										$yes_com_tag_pro_song =0;
										for($count_sel=0;$count_sel<=count($get_select_songs);$count_sel++)
										{
											if($get_select_songs[$count_sel]==""){continue;}
											else
											{
												if($get_select_songs[$count_sel] == $get_media_tag_art_pro_song['id'])
												{
													$yes_com_tag_pro_song = 1;
					?>
													<option value="<?php echo $get_media_tag_art_pro_song['id'];?>" selected><?php echo substr($get_media_tag_art_pro_song['creator'],0,20) ." : ". substr($get_media_tag_art_pro_song['from'],0,20) ." : ".  $get_media_tag_art_pro_song['title'];?></option>
					<?php
												}
											}
										}
										if($yes_com_tag_pro_song == 0)
										{													
					?>
											<option value="<?php echo $get_media_tag_art_pro_song['id'];?>"><?php echo substr($get_media_tag_art_pro_song['creator'],0,20) ." : ". substr($get_media_tag_art_pro_song['from'],0,20) ." : ".  $get_media_tag_art_pro_song['title'];?></option>
					<?php
										}
									}
									else
									{
					?>
										<option value="<?php echo $get_media_tag_art_pro_song['id'];?>"><?php echo substr($get_media_tag_art_pro_song['creator'],0,20) ." : ". substr($get_media_tag_art_pro_song['from'],0,20) ." : ".  $get_media_tag_art_pro_song['title'];?></option>
					<?php
									}
								//}
							}
						} */
						
						//if(mysql_num_rows($get_artist_song_id)<=0 && $media_song=="" && count($get_media_cre_art_pro)<=0 && mysql_num_rows($get_media_tag_com_pro)<=0 && mysql_num_rows($get_media_tag_art_pro)<=0)
				?>
						</select>
				<?php
						}
						
						if(empty($get_songs_pnew_w))
						{
				?>
							<a style="border-radius:5px; -webkit-border-radius:5px; -moz-border-radius:5px; -ms-border-radius:5px; -o-border-radius:5px; background-color: #000000; border: 0 none; color: #FFFFFF; font-size: 13px; margin-left: 5px; padding: 6px 8px; text-decoration: none;" href="add_media.php?adds=114">Add Song</a>
				<?php	
						}	
				?>
						</div>
								
                                <div class="fieldCont">
                                  <div title="Select media and profiles to display on your profile page." class="fieldTitle">Videos</div>
                                    
										<?php
										/* if(isset($res_video) && $res_video!=null)
										{
											if($get_select_videos!="")
											{
												//for($i=0;$i<count($get_select_videos);$i++)
												//{
													if($get_select_videos[$i]==$res_video['video_id'])
													{
											?>
														<option value="<?php echo $res_video['video_id'];?>" selected ><?php echo substr($newres1['fname'].' '.$newres1['lname'],0,20) ." : ". $res_video['video_name'];?></option>
											<?php
														continue;
													}
													else
													{
														?>
														<option value="<?php echo $res_video['video_id'];?>" ><?php echo substr($newres1['fname'].' '.$newres1['lname'],0,20) ." : ". $res_video['video_name'];?></option>
											<?php
													}
												//}
											}
											else
											{
												?>
													<option value="<?php echo $res_video['video_id'];?>" ><?php echo substr($newres1['fname'].' '.$newres1['lname'],0,20) ." : ". $res_video['video_name'];?></option>
												<?php
											}
										} */
										$get_video_id = array();
										$get_artist_video_id=$editprofile->get_artist_video_id();
										if($get_artist_video_id!="" && $get_artist_video_id!=Null)
										{
											while($get_video_ids = mysql_fetch_assoc($get_artist_video_id))
											{
												$get_video_id[] = $get_video_ids;
											}
										}
										
										$get_media_tag_art_pro = $editprofile->get_artist_project_tag($_SESSION['login_email']);
										if($get_media_tag_art_pro !="" && $get_media_tag_art_pro!= Null)
										{
											if(mysql_num_rows($get_media_tag_art_pro)>0)
											{
												$is_v = 0;
												while($art_pro_tag = mysql_fetch_assoc($get_media_tag_art_pro))
												{
													$get_videos = explode(",",$art_pro_tag['tagged_videos']);
													for($count_art_tag_video=0;$count_art_tag_video<count($get_videos);$count_art_tag_video++)
													{
														if($get_videos[$count_art_tag_video]!="")
														{
															$get_videos_new[$is_v] = $get_videos[$count_art_tag_video];
															$is_v = $is_v + 1;
														}
													}
												}
												if($get_videos_new!="")
												{
													$get_videos_n = array_unique($get_videos_new);
												}
											}
										}
										//var_dump($get_videos_n);
										$get_media_tag_art_eve = $editprofile->get_artist_event_tag($_SESSION['login_email']);
										if($get_media_tag_art_eve !="" && $get_media_tag_art_eve!= Null)
										{
											if(mysql_num_rows($get_media_tag_art_eve)>0)
											{
												$is_v = 0;
												while($art_pro_tag = mysql_fetch_assoc($get_media_tag_art_eve))
												{
													$get_videos = explode(",",$art_pro_tag['tagged_videos']);
													for($count_art_tag_video=0;$count_art_tag_video<count($get_videos);$count_art_tag_video++)
													{
														if($get_videos[$count_art_tag_video]!="")
														{
															$get_videos_enewe[$is_v] = $get_videos[$count_art_tag_video];
															$is_v = $is_v + 1;
														}
													}
												}
												if($get_videos_enewe!="")
												{
													$get_videos_ne = array_unique($get_videos_enewe);
												}
											}
										}
										
										$get_media_tag_com_pro = $editprofile->get_community_project_tag($_SESSION['login_email']);
										if($get_media_tag_com_pro !="" && $get_media_tag_com_pro!= Null)
										{
											if(mysql_num_rows($get_media_tag_com_pro)>0)
											{
												$is_vc = 0;
												while($com_pro_tag = mysql_fetch_assoc($get_media_tag_com_pro))
												{
													$get_com_videos = explode(",",$com_pro_tag['tagged_videos']);
													for($count_com_tag_video=0;$count_com_tag_video<count($get_com_videos);$count_com_tag_video++)
													{
														if($get_com_videos[$count_com_tag_video]!="")
														{
															$get_videos_c_new[$is_vc] = $get_com_videos[$count_com_tag_video];
															$is_vc = $is_vc + 1;
														}
													}
												}
												if($get_videos_c_new!="")
												{
													$get_videos_c_n = array_unique($get_videos_c_new);
												}
											}
										}
										
										$get_media_tag_com_eve = $editprofile->get_community_event_tag($_SESSION['login_email']);
										if($get_media_tag_com_eve !="" && $get_media_tag_com_eve!= Null)
										{
											if(mysql_num_rows($get_media_tag_com_eve)>0)
											{
												$is_vc = 0;
												while($com_pro_tag = mysql_fetch_assoc($get_media_tag_com_eve))
												{
													$get_com_videos = explode(",",$com_pro_tag['tagged_videos']);
													for($count_com_tag_video=0;$count_com_tag_video<count($get_com_videos);$count_com_tag_video++)
													{
														if($get_com_videos[$count_com_tag_video]!="")
														{
															$get_videos_c_enewe[$is_vc] = $get_com_videos[$count_com_tag_video];
															$is_vc = $is_vc + 1;
														}
													}
												}
												if($get_videos_c_enewe!="")
												{
													$get_videose_c_rn = array_unique($get_videos_c_enewe);
												}
											}
										}
										
										$get_media_cre_art_pro = $editprofile->get_project_create();
										if(!empty($get_media_cre_art_pro))
										{
											$is_v_c = 0;
											//while($art_pro_tag = mysql_fetch_assoc($get_media_cre_art_pro))
											//var_dump($get_media_cre_art_pro);
											for($cr_i=0;$cr_i<count($get_media_cre_art_pro);$cr_i++)
											{
												$get_cvideos = explode(",",$get_media_cre_art_pro[$cr_i]['tagged_videos']);

												for($count_art_cr_video=0;$count_art_cr_video<count($get_cvideos);$count_art_cr_video++)
												{
													if($get_cvideos[$count_art_cr_video]!="")
													{
														$get_videos__new[$is_v_c] = $get_cvideos[$count_art_cr_video];
														$is_v_c = $is_v_c + 1;
													}
												}
											}
											if($get_videos__new!="")
											{
												$get_videos__n = array_unique($get_videos__new);
											}
										}
										
										if(!isset($get_videos_n))
										{
											$get_videos_n = array();
										}
										
										if(!isset($get_videose_c_rn))
										{
											$get_videose_c_rn = array();
										}
										
										if(!isset($get_videos_ne))
										{
											$get_videos_ne = array();
										}
										
										if(!isset($get_videos_c_n))
										{
											$get_videos_c_n = array();
										}
										
										if(!isset($get_videos__n))
										{
											$get_videos__n = array();
										}
										
										// $find_creator_heis = $editprofile -> get_creator_heis();
										// $get_genral_user_info = $editprofile -> sel_general();
										// $where_creator = array();
										//var_dump($find_creator_heis);
										// if(!empty($find_creator_heis)){
											// for($new_cre_med=0;$new_cre_med<=count($find_creator_heis);$new_cre_med++)
											// {
												// if($find_creator_heis[$new_cre_med]['general_user_id']!= $get_genral_user_info['general_user_id']){
													// if($find_creator_heis['media_type']==115){
													// $where_creator[] = $find_creator_heis;
													// }
												// }
											// }
										// }
										//var_dump($where_creator);
										
										// $find_tag_heis = $editprofile -> get_tag_where_heis($_SESSION['login_email']);
										// $get_genral_user_info = $editprofile -> sel_general();
										// $where_tagged = array();
										// if(!empty($find_tag_heis)){
											// for($new_cre_med=0;$new_cre_med<=count($find_tag_heis);$new_cre_med++)
											// {
												// if($find_tag_heis[$new_cre_med]['general_user_id']!= $get_genral_user_info['general_user_id']){
													// if($find_tag_heis['media_type']==115){
													// $where_tagged[] = $find_tag_heis;
													// }
												// }
											// }
										// }
										
										/*This function will display the user in which profile he is creator and display media where that profile was creator*/
										$new_medi = $editprofile->get_creator_heis();
										$new_creator_creator_media_array = array();
										if(!empty($new_medi))
										{
											for($count_cre_m=0;$count_cre_m<count($new_medi);$count_cre_m++)
											{
												if($new_medi[$count_cre_m]['media_type']==115){
													$new_creator_creator_media_array[] = $new_medi[$count_cre_m];
												}
											}
										}
										//var_dump($new_creator_creator_media_array);
										/*This function will display the user in which profile he is creator and display media where that profile was creator Ends here*/
										
										/*This function will display the user in which profile he is from and display media where that profile was creator*/
										$new_medi_from = $editprofile->get_from_heis();
										$new_from_from_media_array = array();
										if(!empty($new_medi_from))
										{
											for($count_cre_m=0;$count_cre_m<count($new_medi_from);$count_cre_m++)
											{
												if($new_medi_from[$count_cre_m]['media_type']==115){
												$new_from_from_media_array[] = $new_medi_from[$count_cre_m];
												}
											}
										}
										//var_dump($new_creator_creator_media_array);
										/*This function will display the user in which profile he is from and display media where that profile was creator Ends here*/
										
										
										/*This function will display the user in which profile he is from or creator and tagged and display media where that profile was creator*/
										$new_medi_tagged = $editprofile->get_the_user_tagin_project($_SESSION['login_email']);
										$new_tagged_from_media_array = array();
										if(!empty($new_medi_tagged))
										{
											for($count_cre_m=0;$count_cre_m<count($new_medi_tagged);$count_cre_m++)
											{
												if($new_medi_tagged[$count_cre_m]['media_type']==115){
												$new_tagged_from_media_array[] = $new_medi_tagged[$count_cre_m];
												}
											}
										}
										//var_dump($new_tagged_from_media_array);
										/*This function will display the user in which profile he is from or creator and tagged and display media where that profile was creator Ends here*/
										
										
										/*This function will display in which media the curent user was creator*/
										$get_where_creator_media = $editprofile->get_media_where_creator_or_from();
										$new_creator_media_array = array();
										if(!empty($get_where_creator_media))
										{
											for($count_tag_m=0;$count_tag_m<count($get_where_creator_media);$count_tag_m++)
											{
												if($get_where_creator_media[$count_tag_m]['media_type']==115){
													$new_creator_media_array[] = $get_where_creator_media[$count_tag_m];
												}
											}
										}
										/*This function will display in which media the curent user was creator ends here*/
										
										//var_dump($get_videos_n);
										//var_dump($get_videos_c_n);
										//var_dump($get_videos__n);
										//var_dump($get_videos_ne);
										//var_dump($get_videose_c_rn);
										$get_vids_pnew_w_c = array_merge($get_videos_n,$get_videos_c_n,$get_videos__n,$get_videos_ne,$get_videose_c_rn);
										
										$get_vids_pnew_ct = array_unique($get_vids_pnew_w_c);
										$get_vids_final_ct = array();
										
										for($count_art_cr_video=0;$count_art_cr_video<count($get_vids_pnew_ct);$count_art_cr_video++)
										{
											if($get_vids_pnew_ct[$count_art_cr_video]!="")
											{
												$get_media_tag_art_pro_vi = $editprofile->get_tag_media_info($get_vids_pnew_ct[$count_art_cr_video]);
												$get_vids_final_ct[] = $get_media_tag_art_pro_vi;
											}
										}
										
										$acc_medsv = array();
										for($acc_vis=0;$acc_vis<count($acc_media);$acc_vis++)
										{
											if($acc_media[$acc_vis]!="")
											{
												$sel_tagged = $editprofile->get_artist_media_type_tagged_by_other_user($acc_media[$acc_vis]);
												if(!empty($sel_tagged))
												{
													$acc_medsv[] = $sel_tagged;
												}
											}
										}
										$get_vids_final_ct = array();
										$type = '115';
										$cre_frm_med = array();
										$cre_frm_med = $editprofile->get_media_create($type);
										
										$get_vids_pnew_w = array_merge($acc_medsv,$get_video_id,$get_vids_final_ct,$cre_frm_med,$new_creator_creator_media_array,$new_from_from_media_array,$new_tagged_from_media_array,$new_creator_media_array);
										//var_dump($get_vids_pnew_w);
										$sort = array();
										foreach($get_vids_pnew_w as $k=>$v) {
										
											$end_c1 = $v['creator'];
											//$create_c = strpos($v['creator'],'(');
											//$end_c1 = substr($v['creator'],0,$create_c);
											$end_c = trim($end_c1);
											
											$end_f1 = $v['from'];
											//$create_f = strpos($v['from'],'(');
											//$end_f1 = substr($v['from'],0,$create_f);
											$end_f = trim($end_f1);
											
											$sort['creator'][$k] = strtolower($end_c);
											$sort['from'][$k] = strtolower($end_f);
											//$sort['track'][$k] = $v['track'];
											$sort['title'][$k] = strtolower($v['title']);
										}
										//var_dump($sort);
										if(!empty($sort))
										{
											array_multisort($sort['creator'], SORT_ASC, $sort['from'], SORT_ASC, $sort['title'], SORT_ASC,$get_vids_pnew_w);
										}
										
										if(!empty($get_vids_pnew_w))
										{
									?>
										<select title="Select media and profiles to display on your profile page." name="tagged_videos[]" size="5" multiple="multiple" id="tagged_videos[]" class="list">
									<?php
									
										$dummy_v = array();
										$get_all_del_id = $editprofile->get_all_del_media_id();
										$deleted_id = explode(",",$get_all_del_id);
										for($count_del=0;$count_del<=count($get_vids_pnew_w);$count_del++)
										{
											if($get_vids_pnew_w[$count_del]==""){continue;}
											else{
												for($count_deleted=0;$count_deleted<=count($deleted_id);$count_deleted++)
												{
													if($deleted_id[$count_deleted]==""){continue;}
													else{
														if($deleted_id[$count_deleted] == $get_vids_pnew_w[$count_del]['id'])
														{
															$get_vids_pnew_w[$count_del]="";
														}
													}
												}
											}
										}
										for($acc_vide=0;$acc_vide<count($get_vids_pnew_w);$acc_vide++)
										{
											if($get_vids_pnew_w[$acc_vide]['delete_status']==0){
												if(isset($get_vids_pnew_w[$acc_vide]['id']))
												{
													if(in_array($get_vids_pnew_w[$acc_vide]['id'],$dummy_v))
													{

													}
													else
													{
														$dummy_v[] = $get_vids_pnew_w[$acc_vide]['id'];
														if($get_vids_pnew_w[$acc_vide]['id']!="" && $get_vids_pnew_w[$acc_vide]['media_type']==115)
														{
															//$create_c = strpos($get_vids_pnew_w[$acc_vide]['creator'],'(');
															//$end_c = substr($get_vids_pnew_w[$acc_vide]['creator'],0,$create_c);
															$end_c = $get_vids_pnew_w[$acc_vide]['creator'];
															//$create_f = strpos($get_vids_pnew_w[$acc_vide]['from'],'(');
															//$end_f = substr($get_vids_pnew_w[$acc_vide]['from'],0,$create_f);
															$end_f = $get_vids_pnew_w[$acc_vide]['from'];
															$eceksv = "";
															if($end_c!="")
															{
																$eceksv = $end_c;
															}
															if($end_f!="")
															{
																if($end_c!="")
																{
																	$eceksv .= " : ".$end_f;
																}
																else
																{
																	$eceksv .= $end_f;
																}
															}
															if($get_vids_pnew_w[$acc_vide]['title']!="")
															{
																if($end_c!="" || $end_f!="")
																{
																	$eceksv .= " : ".$get_vids_pnew_w[$acc_vide]['title'];
																}
																else
																{
																	$eceksv .= $get_vids_pnew_w[$acc_vide]['title'];
																}
															}
															
															if($get_select_videos!="")
															{
																$get_select_videos = array_unique($get_select_videos);
																$yes_com_tag_pro_song =0;
																for($count_sel=0;$count_sel<=count($get_select_videos);$count_sel++)
																{
																	if($get_select_videos[$count_sel]==""){continue;}
																	else
																	{
																		if($get_select_videos[$count_sel] == $get_vids_pnew_w[$acc_vide]['id'])
																		{
																			$yes_com_tag_pro_song = 1;
											?>
																			<option value="<?php echo $get_vids_pnew_w[$acc_vide]['id'];?>" selected><?php echo stripslashes($eceksv); ?></option>
											<?php
																		}
																	}
																}
																if($yes_com_tag_pro_song == 0)
																{													
											?>
																	<option value="<?php echo $get_vids_pnew_w[$acc_vide]['id'];?>"><?php echo stripslashes($eceksv); ?></option>
											<?php
																}
															}
															else
															{
				?>
																<option value="<?php echo $get_vids_pnew_w[$acc_vide]['id'];?>" ><?php echo stripslashes($eceksv); ?></option>
				<?php
															}
														}
													}
												}
											}
										}
										
										
											/* for($count_art_cr_video=0;$count_art_cr_video<count($get_videos_n_new);$count_art_cr_video++)
												{
													if($get_videos_n_new[$count_art_cr_video]!=""){
													
													$art_coms_v = '';
													if($get_media_cre_art_pro[$cr_i]['artist_id']!=0 && $get_media_cre_art_pro[$cr_i]['artist_id']!="")
													{
														$art_coms_v = 'artist~'.$get_media_cre_art_pro[$cr_i]['artist_id'];
													}
													elseif($get_media_cre_art_pro[$cr_i]['community_id']!=0 && $get_media_cre_art_pro[$cr_i]['community_id']!="")
													{
														$art_coms_v = 'community~'.$get_media_cre_art_pro[$cr_i]['community_id'];
													}
													
													$reg_meds = $editprofile->get_media_reg_info($get_videos_n_new[$count_art_cr_video],$art_coms_v);
													if($reg_meds!=NULL && $reg_meds!='')
													{
														if($reg_meds=='general_artist_video')
														{
															$sql_a = mysql_query("SELECT * FROM general_artist_video WHERE video_id='".$get_videos_n_new[$count_art_cr_video]."'");
															if(mysql_num_rows($sql_a)>0)
															{
																$ans_a = mysql_fetch_assoc($sql_a);
																$sql_gens = $editprofile->sel_general_aid($get_media_cre_art_pro[$cr_i]['artist_id']);
																
																if($get_select_videos!="")
																{
																	//$get_sel_song =$editprofile->get_sel_songs($_GET['edit']);
																	//$exp_song_id = explode(",",$get_sel_song['songs']);
																	$yes_com_tag_pro_song =0;
																	for($count_selv=0;$count_selv<=count($get_select_videos);$count_selv++)
																	{
																		if($get_select_videos[$count_selv]==""){continue;}
																		else{
																			if($get_select_videos[$count_selv] == $ans_a['video_id'])
																			{
																				$yes_com_tag_pro_song = 1;
																			?>															
																				<option value="<?php echo $ans_a['video_id'];?>" selected><?php echo substr($sql_gens['fname'] ." ". $sql_gens['lname'],0,20) ." : ".  $ans_a['video_name'];?></option>
																			<?php
																			}
																		}
																	}
																	if($yes_com_tag_pro_song == 0)
																	{
																		
																	?>
																		<option value="<?php echo $ans_a['video_id'];?>"><?php echo substr($sql_gens['fname'] ." ". $sql_gens['lname'],0,20) ." : ".  $ans_a['video_name'];?></option>
																	<?php
																	}
																}
																else{
																?>
																	<option value="<?php echo $ans_a['video_id'];?>"><?php echo substr($sql_gens['fname'] ." ". $sql_gens['lname'],0,20) ." : ".  $ans_a['video_name'];?></option>
																<?php
																	}
															}
														}
														if($reg_meds=='general_community_video')
														{
															$sql_a = mysql_query("SELECT * FROM general_community_video WHERE video_id='".$get_videos_n_new[$count_art_cr_video]."'");
															if(mysql_num_rows($sql_a)>0)
															{
																$ans_a = mysql_fetch_assoc($sql_a);
																$sql_gens = $editprofile->sel_general_cid($get_media_cre_art_pro[$cr_i]['community_id']);
																
																if($get_select_videos!="")
																{
																	//$get_sel_song =$editprofile->get_sel_songs($_GET['edit']);
																	//$exp_song_id = explode(",",$get_sel_song['songs']);
																	$yes_com_tag_pro_song =0;
																	for($count_selvc=0;$count_selvc<=count($get_select_videos);$count_selvc++)
																	{
																		if($get_select_videos[$count_selvc]==""){continue;}
																		else{
																			if($get_select_videos[$count_selvc] == $ans_a['video_id'])
																			{
																				$yes_com_tag_pro_song = 1;
																			?>															
																				<option value="<?php echo $ans_a['video_id'];?>" selected><?php echo substr($sql_gens['fname'] ." ". $sql_gens['lname'],0,20) ." : ".  $ans_a['video_name'];?></option>
																			<?php
																			}
																		}
																	}
																	if($yes_com_tag_pro_song == 0)
																	{
																		
																	?>
																		<option value="<?php echo $ans_a['video_id'];?>"><?php echo substr($sql_gens['fname'] ." ". $sql_gens['lname'],0,20) ." : ".  $ans_a['video_name'];?></option>
																	<?php
																	}
																}
																else{
																?>
																	<option value="<?php echo $ans_a['video_id'];?>"><?php echo substr($sql_gens['fname'] ." ". $sql_gens['lname'],0,20) ." : ".  $ans_a['video_name'];?></option>
																<?php
																	}
															}
														}
													}
													else
													{
														$get_media_tag_art_pro_video = $editprofile->get_tag_media_info($get_videos_n_new[$count_art_cr_video]);
														if($get_select_videos!="")
														{
															//$get_sel_song =$editprofile->get_sel_songs($_GET['edit']);
															//$exp_video_id = explode(",",$get_sel_video['taggedvideos']);
															$yes_tag_pro_video =0;
															for($count_sel=0;$count_sel<=count($get_select_videos);$count_sel++)
															{
																if($get_select_videos[$count_sel]==""){continue;}
																else{
																	if($get_select_videos[$count_sel] == $get_media_tag_art_pro_video['id'])
																	{
																		$yes_tag_pro_video = 1;
																	?>
																		<option value="<?php echo $get_media_tag_art_pro_video['id'];?>" selected><?php echo substr($get_media_tag_art_pro_video['creator'],0,20) ." : ". substr($get_media_tag_art_pro_video['from'],0,20) ." : ".  $get_media_tag_art_pro_video['title'];?></option>
																	<?php
																	}
																}
															}
															if($yes_tag_pro_video == 0)
															{
																
															?>
																<option value="<?php echo $get_media_tag_art_pro_video['id'];?>"><?php echo substr($get_media_tag_art_pro_video['creator'],0,20) ." : ". substr($get_media_tag_art_pro_video['from'],0,20) ." : ".  $get_media_tag_art_pro_video['title'];?></option>
															<?php
															}
														}
														else{
														?>
															<option value="<?php echo $get_media_tag_art_pro_video['id'];?>"><?php echo substr($get_media_tag_art_pro_video['creator'],0,20) ." : ". substr($get_media_tag_art_pro_video['from'],0,20) ." : ".  $get_media_tag_art_pro_video['title'];?></option>
														<?php
															}
														}
													}
												} */
											
										//if(mysql_num_rows($get_artist_video_id)<=0 && $media_video=="" && count($get_media_cre_art_pro)<=0 && mysql_num_rows($get_media_tag_com_pro)<=0 && mysql_num_rows($get_media_tag_art_pro)<=0)
								?>
											</select>
								<?php
										}
										if(empty($get_vids_pnew_w))
										{
										?>
											<a style="border-radius:5px; -webkit-border-radius:5px; -moz-border-radius:5px; -ms-border-radius:5px; -o-border-radius:5px; background-color: #000000; border: 0 none; color: #FFFFFF; font-size: 13px; margin-left: 5px; padding: 6px 8px; text-decoration: none;" href="add_media.php?adds=115">Add Video</a>
										<?php	
										}
										?>
									
                                </div>
								<?php
								 // echo "sdfsdf";
								  //die;
								  ?>
                               <!-- <div class="fieldCont">
                                  <div class="fieldTitle">Channels</div>
                                    <select name="tagged_channels[]" size="5" multiple="multiple" id="tagged_channels[]" class="list">
                                        <?php
										/*while($get_channel_id=mysql_fetch_assoc($get_artist_channel_id))
										{
											$get_artist_channel=$editprofile->get_artist_channel($get_channel_id['id']);
											$getchannel=mysql_fetch_assoc($get_artist_channel);
											if($get_select_channels!="")
											{
												for($i=0;$i<count($get_select_channels);$i++)
												{
													if($get_select_channels[$i]==$get_channel_id['id'])
													{
														$j_chan=1;
											?>
												<option value="<?php echo $get_channel_id['id'];?>" selected ><?php echo substr($get_channel_id['creator'],0,20) ." : ". substr($get_channel_id['from'],0,20) ." : ". $get_channel_id['title'];?></option>
											<?php
														continue;
													}
												}
												if($j_chan==1)
												{
													$j_chan=0;
													continue;
												}
											}
										?>
											<option value="<?php echo $get_channel_id['id'];?>" ><?php echo substr($get_channel_id['creator'],0,20) ." : ". substr($get_channel_id['from'],0,20) ." : ". $get_channel_id['title'];?></option>
										<?php	
										}
										for($acc_chan=0;$acc_chan<count($acc_media);$acc_chan++)
										{
											$sel_tagged=$editprofile->get_artist_media_type_tagged_by_other_user($acc_media[$acc_chan]);
											if($sel_tagged['media_type']==116)
											{
												if($get_select_channels!="")
												{
													$media_chan="true";
													for($i=0;$i<count($get_select_channels);$i++)
													{
														if($get_select_channels[$i]==$sel_tagged['id'])
														{
															$j_chan=1;
												?>
													<option value="<?php echo $sel_tagged['id'];?>" selected ><?php echo substr($sel_tagged['creator'],0,20) ." : ". substr($sel_tagged['from'],0,20) ." : ". $sel_tagged['title'];?></option>
												<?php	
															continue;
														}
													}
													if($j_chan==1)
													{
														$j_chan=0;
														continue;
													}
												}
											?>
												<option value="<?php echo $sel_tagged['id'];?>" ><?php echo substr($sel_tagged['creator'],0,10) ." : ". substr($sel_tagged['from'],0,10) ." : ". $sel_tagged['title'];?></option>
											<?php
											}
										}
										if(mysql_num_rows($get_artist_channel_id)==0 && $media_chan=="")
										{
										?>
											<option value="" >No Listings</option>
										<?php	
										}*/
										?>
                                    </select>
                                </div>-->
								
								<div class="fieldCont">
                                  <div title="Select media and profiles to display on your profile page." class="fieldTitle">Upcoming Events</div>
                                        <?php
										$event_avail_check_up = 0;
										//var_dump($acc_event);
										//	die;
										$get_eveup_id = array();
										if($get_event!="" && $get_event!=Null)
										{
											while($get_eveup_ids = mysql_fetch_assoc($get_event))
											{
												$get_eveup_ids['id'] = $get_eveup_ids['id'].'~art';
												$get_eveup_id[] = $get_eveup_ids;
											}
										}
										
										$get_usqp_id = array();
										if($get_ceevent!="" && $get_ceevent!=Null)
										{
											while($get_xeveup_ids = mysql_fetch_assoc($get_ceevent))
											{
												$get_xeveup_ids['id'] = $get_xeveup_ids['id'].'~com';
												$get_usqp_id[] = $get_xeveup_ids;
											}
										}
										
										$get_onlycre_ev = array();
										$get_onlycre_ev = $editprofile->only_creator_upevent();
										
										$get_only2cre_ev = array();
										$get_only2cre_ev = $editprofile->only_creator2ev_upevent();
										
										$selarts_tagged = array();
										if($event_avail!=0)
										{
											$acc_event = array_unique($acc_event);
											for($i=0;$i<count($acc_event);$i++)
											{
													//$sel_tagged="";
												if($acc_event[$i]!="" && $acc_event[$i]!=0)
												{
													$dumartr = $editprofile->get_event_tagged_by_other_user($acc_event[$i]);
													if(!empty($dumartr) && $dumartr['id']!="" && $dumartr['id']!="")
													{
														$dumartr['id'] = $dumartr['id'].'~'.'art';
														$selarts_tagged[] = $dumartr;
													}												
												}
											}
										} 
										
										$selcoms_tagged = array();
										 if($event_cavail!=0)
										{
											$acc_cevent = array_unique($acc_cevent);
											for($t_i=0;$t_i<count($acc_cevent);$t_i++)
											{
													//$sel_tagged="";
												if($acc_cevent[$t_i]!="" && $acc_cevent[$t_i]!=0)
												{
													$dumacomr = $editprofile->get_cevent_tagged_by_other_user($acc_cevent[$t_i]);
													if(!empty($dumacomr) && $dumacomr['id']!="" && $dumacomr['id']!="")
													{
														$dumacomr['id'] = $dumacomr['id'].'~'.'com';
														$selcoms_tagged[] = $dumacomr;
													}												
												}
											}
										} 
										
										$selarts2_tagged = array();
										if($project_avail!=0)
										{
											$acc_project = array_unique($acc_project);
											for($acc_pro=0;$acc_pro<count($acc_project);$acc_pro++)
											{												
												if($acc_project[$acc_pro]!="" && $acc_project[$acc_pro]!=0)
												{													
													$sql_1_cre = $editprofile->get_event_tagged_by_other2_user($acc_project[$acc_pro]);
													if($sql_1_cre!="")
													{
														if($sql_1_cre !="" && $sql_1_cre!= Null)
														{
															if(mysql_num_rows($sql_1_cre)>0)
															{
																while($run = mysql_fetch_assoc($sql_1_cre))
																{
																	$run['id'] = $run['id'].'~'.'art';
																	//$ans_a_als[] = $run;
																	$selarts2_tagged[] = $run;
																}
															}
														}
													}
													
													$sql_2_cre = $editprofile->get_event_ctagged_by_other2_user($acc_project[$acc_pro]);
													if($sql_2_cre !="" && $sql_2_cre!= Null)
													{
														if(mysql_num_rows($sql_2_cre)>0)
														{
															while($run_c = mysql_fetch_assoc($sql_2_cre))
															{
																$run_c['id'] = $run_c['id'].'~'.'com';
																//$ans_c_als[]
																$selarts2_tagged[] = $run_c;
															}
														}
													}
												}
											}
										}
										
										$selcoms2_tagged = array();
										if($project_cavail!=0)
										{
											$acc_cproject = array_unique($acc_cproject);
											for($acc_pro=0;$acc_pro<count($acc_cproject);$acc_pro++)
											{												
												if($acc_cproject[$acc_pro]!="" && $acc_cproject[$acc_pro]!=0)
												{													
													$sql_1_cre = $editprofile->get_cevent_tagged_by_other2_user($acc_cproject[$acc_pro]);
													if($sql_1_cre!="")
													{
														if($sql_1_cre !="" && $sql_1_cre!= Null)
														{
															if(mysql_num_rows($sql_1_cre)>0)
															{
																while($run = mysql_fetch_assoc($sql_1_cre))
																{
																	$run['id'] = $run['id'].'~'.'art';
																	//$ans_a_als[] = $run;
																	$selcoms2_tagged[] = $run;
																}
															}
														}
													}
													
													$sql_2_cre = $editprofile->get_cevent_ctagged_by_other2_user($acc_cproject[$acc_pro]);
													if($sql_2_cre !="" && $sql_2_cre!= Null)
													{
														if(mysql_num_rows($sql_2_cre)>0)
														{
															while($run_c = mysql_fetch_assoc($sql_2_cre))
															{
																$run_c['id'] = $run_c['id'].'~'.'com';
																//$ans_c_als[]
																$selcoms2_tagged[] = $run_c;
															}
														}
													}
												}
											}
										}
										
										$final_evesup = array_merge($get_eveup_id,$selarts_tagged,$selcoms_tagged,$get_onlycre_ev,$get_usqp_id,$get_only2cre_ev,$selarts2_tagged,$selcoms2_tagged);
										//$final_evesup = $get_usqp_id;
										//var_dump($final_evesup);
										$sort = array();
										foreach($final_evesup as $k=>$v) {
										
											//$create_c = strpos($v['creator'],'(');
											//$end_c = substr($v['creator'],0,$create_c);
											
											//$sort['creator'][$k] = $end_c;
											//$sort['from'][$k] = $end_f;
											//$sort['track'][$k] = $v['track'];
											$sort['date'][$k] = $v['date'];
										}
										//var_dump($sort);
										if(!empty($sort))
										{
											array_multisort($sort['date'], SORT_DESC,$final_evesup);
										}
										
										$dummy_ueov = array();
										$get_all_del_id = $editprofile->get_deleted_project_id();
										$exp_del_id = explode(",",$get_all_del_id['deleted_event_id']);
										for($count_all=0;$count_all<count($final_evesup);$count_all++){
											for($count_del=0;$count_del<count($exp_del_id);$count_del++){
												if($exp_del_id[$count_del]!=""){
													if($final_evesup[$count_all]['id'] == $exp_del_id[$count_del]){
														$final_evesup[$count_all] ="";
													}
												}
											}
										}
										
										if(!empty($final_evesup))
										{
			?>
											<select title="Select media and profiles to display on your profile page." name="tagged_upcoming_events[]" size="5" multiple="multiple" id="tagged_upcoming_events[]" class="list">
			<?php
										for($eqv=0;$eqv<count($final_evesup);$eqv++)
										{
											if($final_evesup[$eqv]!="")
											{
												if(in_array($final_evesup[$eqv]['id'],$dummy_ueov))
												{}
												else
												{
													$dummy_ueov[] = $final_evesup[$eqv]['id'];
													if(!empty($get_select_events))
													{
														$get_select_events = array_unique($get_select_events);
														for($f=0;$f<count($get_select_events);$f++)
														{
															if($get_select_events[$f]==$final_evesup[$eqv]['id'])
															{
																$j_comm_acc=1;
			?>
																<option value="<?php echo $final_evesup[$eqv]['id'];?>" selected ><?php echo date("m.d.y", strtotime($final_evesup[$eqv]['date'])).' '.stripslashes($final_evesup[$eqv]['title']);?></option>
			<?php	
																continue;
															}
														}	
														
														if($j_comm_acc==1)
														{
															$j_comm_acc=0;
															continue;
														}
													}
			?>
													<option value="<?php echo $final_evesup[$eqv]['id'];?>" ><?php echo date("m.d.y", strtotime($final_evesup[$eqv]['date'])).' '.stripslashes($final_evesup[$eqv]['title']);?></option>
			<?php
												}
											}
										}
										
										//if(mysql_num_rows($get_event)<=0 && empty($dummy_ueov) && mysql_num_rows($get_ceevent)<=0)
			?>
											</select>
			<?php
										}
										
										if(empty($final_evesup))
										{
										?>
											<a style="border-radius:5px; -webkit-border-radius:5px; -moz-border-radius:5px; -ms-border-radius:5px; -o-border-radius:5px; background-color: #000000; border: 0 none; color: #FFFFFF; font-size: 13px; margin-left: 5px; padding: 6px 8px; text-decoration: none;" href="add_artist_event.php">Add Event</a>
										<?php	
										}
										?>
                                    
                                </div>
							
								<div class="fieldCont">
                                  <div title="Select media and profiles to display on your profile page." class="fieldTitle">Recorded Events</div>
                                        <?php
										$event_avail_check_rec = 0;
										//var_dump($acc_event);
										//	die;
										/* while($get_channel_id = mysql_fetch_assoc($get_eventrec))
										{
											
											//$get_artist_channel=$editprofile->get_artist_channel($get_channel_id['id']);
											//$getchannel=mysql_fetch_assoc($get_artist_channel);
											if($get_select_events_rec[0]!="")
											{
												for($q=0;$q<count($get_select_events_rec);$q++)
												{
													if($get_select_events_rec[$q]==$get_channel_id['id'])
													{
														$j_eve_tag=1;
											?>
												<option value="<?php echo $get_channel_id['id'];?>" selected ><?php echo $get_channel_id['title'];?></option>
											<?php
														continue;
													}
												}
												if($j_eve_tag==1)
												{
													$j_eve_tag=0;
													continue;
												}
											}
											
										?>
											<option value="<?php echo $get_channel_id['id'];?>" ><?php echo $get_channel_id['title'];?></option>
										<?php	
										} */
										
										$get_channel_id = array();
										if($get_eventrec!="" && $get_eventrec!=Null)
										{
											while($get_channel_ids = mysql_fetch_assoc($get_eventrec))
											{
												$get_channel_ids['id'] = $get_channel_ids['id'].'~art';
												$get_channel_id[] = $get_channel_ids; 
											}
										}
										
										$get_cchannel_id = array();
										if($get_ceeventrec!="" && $get_ceeventrec!=Null)
										{
											while($get_rchannel_ids = mysql_fetch_assoc($get_ceeventrec))
											{
												$get_rchannel_ids['id'] = $get_rchannel_ids['id'].'~com';
												$get_cchannel_id[] = $get_rchannel_ids; 
											}
										}
										
										$get_onlycre_rev = array();
										$get_onlycre_rev = $editprofile->only_creator_recevent();
										
										$get_only2cre_rev = array();
										$get_only2cre_rev = $editprofile->only_creator2_recevent();
										
										$sel_artstagged = array();
										if($event_avail!=0)
										{
											$acc_event = array_unique($acc_event);
											for($t=0;$t<count($acc_event);$t++)
											{
													//$sel_tagged="";
												if($acc_event[$t]!="" && $acc_event[$t]!=0)
												{
													$dum_arte = $editprofile->get_event_tagged_by_other_userrec($acc_event[$t]);
													if(!empty($dum_arte) && $dum_arte['id']!="" && $dum_arte['id']!="")
													{
														$dum_arte['id'] = $dum_arte['id'].'~'.'art';
														$sel_artstagged[] = $dum_arte;
													}
												}
											}
										}
										
										$sel_comstagged = array();
										if($event_cavail!=0)
										{
											$acc_cevent = array_unique($acc_cevent);
											for($t=0;$t<count($acc_cevent);$t++)
											{
													//$sel_tagged="";
												if($acc_cevent[$t]!="" && $acc_cevent[$t]!=0)
												{
													$dum_artec = $editprofile->get_cevent_tagged_by_other_userrec($acc_cevent[$t]);
													if(!empty($dum_artec) && $dum_artec['id']!="" && $dum_artec['id']!="")
													{
														$dum_artec['id'] = $dum_artec['id'].'~'.'com';
														$sel_comstagged[] = $dum_artec;
													}
												}
											}
										}
										
										$selarts2_tagged = array();
										if($project_avail!=0)
										{
											$acc_project = array_unique($acc_project);
											for($acc_pro=0;$acc_pro<count($acc_project);$acc_pro++)
											{												
												if($acc_project[$acc_pro]!="" && $acc_project[$acc_pro]!=0)
												{													
													$sql_1_cre = $editprofile->get_event_tagged_by_other2_userrec($acc_project[$acc_pro]);
													if($sql_1_cre!="")
													{
														if($sql_1_cre !="" && $sql_1_cre!= Null)
														{
															if(mysql_num_rows($sql_1_cre)>0)
															{
																while($run = mysql_fetch_assoc($sql_1_cre))
																{
																	$run['id'] = $run['id'].'~'.'art';
																	//$ans_a_als[] = $run;
																	$selarts2_tagged[] = $run;
																}
															}
														}
													}
													
													$sql_2_cre = $editprofile->get_event_ctagged_by_other2_userrec($acc_project[$acc_pro]);
													if($sql_2_cre !="" && $sql_2_cre!= Null)
													{
														if(mysql_num_rows($sql_2_cre)>0)
														{
															while($run_c = mysql_fetch_assoc($sql_2_cre))
															{
																$run_c['id'] = $run_c['id'].'~'.'com';
																//$ans_c_als[]
																$selarts2_tagged[] = $run_c;
															}
														}
													}
												}
											}
										}
										
										$selcoms2_tagged = array();
										if($project_cavail!=0)
										{
											$acc_cproject = array_unique($acc_cproject);
											for($acc_pro=0;$acc_pro<count($acc_cproject);$acc_pro++)
											{												
												if($acc_cproject[$acc_pro]!="" && $acc_cproject[$acc_pro]!=0)
												{													
													$sql_1_cre = $editprofile->get_cevent_tagged_by_other2_userrec($acc_cproject[$acc_pro]);
													if($sql_1_cre!="")
													{
														if($sql_1_cre !="" && $sql_1_cre!= Null)
														{
															if(mysql_num_rows($sql_1_cre)>0)
															{
																while($run = mysql_fetch_assoc($sql_1_cre))
																{
																	$run['id'] = $run['id'].'~'.'art';
																	//$ans_a_als[] = $run;
																	$selcoms2_tagged[] = $run;
																}
															}
														}
													}
													
													$sql_2_cre = $editprofile->get_cevent_ctagged_by_other2_userrec($acc_cproject[$acc_pro]);
													if($sql_2_cre !="" && $sql_2_cre!= Null)
													{
														if(mysql_num_rows($sql_2_cre)>0)
														{
															while($run_c = mysql_fetch_assoc($sql_2_cre))
															{
																$run_c['id'] = $run_c['id'].'~'.'com';
																//$ans_c_als[]
																$selcoms2_tagged[] = $run_c;
															}
														}
													}
												}
											}
										}
										
										//$final_eves = array();
										$final_eves = array_merge($get_channel_id,$sel_artstagged,$sel_comstagged,$get_onlycre_rev,$get_cchannel_id,$get_only2cre_rev,$selarts2_tagged,$selcoms2_tagged);
										
										$sort = array();
										foreach($final_eves as $k=>$v) {
										
											//$create_c = strpos($v['creator'],'(');
											//$end_c = substr($v['creator'],0,$create_c);
											
											//$sort['creator'][$k] = $end_c;
											//$sort['from'][$k] = $end_f;
											//$sort['track'][$k] = $v['track'];
											$sort['date'][$k] = $v['date'];
										}
										//var_dump($sort);
										if(!empty($sort))
										{
											array_multisort($sort['date'], SORT_DESC,$final_eves);
										}
										
										$dummy_ev = array();
										$get_all_del_id = $editprofile->get_deleted_project_id();
										$exp_del_id = explode(",",$get_all_del_id['deleted_event_id']);
										for($count_all=0;$count_all<count($final_eves);$count_all++){
											for($count_del=0;$count_del<count($exp_del_id);$count_del++){
												if($exp_del_id[$count_del]!=""){
													if($final_eves[$count_all]['id'] == $exp_del_id[$count_del]){
														$final_eves[$count_all] ="";
													}
												}
											}
										}
										
										if(!empty($final_eves))
										{
			?>
											<select title="Select media and profiles to display on your profile page." name="tagged_recorded_events[]" size="5" multiple="multiple" id="tagged_recorded_events[]" class="list">
			<?php
										for($ev_f=0;$ev_f<count($final_eves);$ev_f++)
										{
											if($final_eves[$ev_f]!="")
											{
												if(in_array($final_eves[$ev_f]['id'],$dummy_ev))
												{}
												else
												{
													$dummy_ev[] = $final_eves[$ev_f]['id'];
													if($get_select_events_rec[0]!="")
													{
														$get_select_events_rec = array_unique($get_select_events_rec);
														for($f=0;$f<count($get_select_events_rec);$f++)
														{
															if($get_select_events_rec[$f]==$final_eves[$ev_f]['id'])
															{
																$j_comm_acc=1;
			?>
																<option value="<?php echo $final_eves[$ev_f]['id'];?>" selected ><?php echo date("m.d.y", strtotime($final_eves[$ev_f]['date'])).' '.stripslashes($final_eves[$ev_f]['title']);?></option>
			<?php	
																continue;
															}
														}	
														
														if($j_comm_acc==1)
														{
															$j_comm_acc=0;
															continue;
														}
													}
			?>
													<option value="<?php echo $final_eves[$ev_f]['id'];?>" ><?php echo date("m.d.y", strtotime($final_eves[$ev_f]['date'])).' '.stripslashes($final_eves[$ev_f]['title']);?></option>
			<?php
												}
											}
										}
										
										//if(mysql_num_rows($get_eventrec)<=0 && empty($dummy_ev) && mysql_num_rows($get_ceeventrec)<=0)
			?>
											</select>
			<?php
										}
										
										if(empty($final_eves))
										{
										?>
											<a style="border-radius:5px; -webkit-border-radius:5px; -moz-border-radius:5px; -ms-border-radius:5px; -o-border-radius:5px; background-color: #000000; border: 0 none; color: #FFFFFF; font-size: 13px; margin-left: 5px; padding: 6px 8px; text-decoration: none;" href="add_artist_event.php">Add Event</a>
										<?php	
										}
										?>
                                    
                                </div>
								
                                <div class="fieldCont">
                                    <div class="fieldTitle"></div>
                                    <input type="submit" class="register" style="border-radius:5px; -webkit-border-radius:5px; -moz-border-radius:5px; -ms-border-radius:5px; -o-border-radius:5px;" value="Save" />
                                    <!--<input type="button" class="register" value="Publish" />-->
                                </div>
								<!--<div  id="selected-types" style="float:right;"></div>  -->
                             </form>
                                 
                             </div>
                        
                    </div>
                
                </div>
                
                <div class="subTabs" id="event" style="display:none;">
					<h1><?php echo $nameres['name'];?> Events</h1>
					<div id="mediaContent" style="width:685px;">
                    	<div class="topLinks" style="border-bottom:1px dotted #999;">
                            <div class="links" style="float:left;">
								<ul>
									<li><a href="add_artist_event.php"><input type="button" value="Add New Event" style="border-radius:5px; -webkit-border-radius:5px; -moz-border-radius:5px; -ms-border-radius:5px; -o-border-radius:5px; color: rgb(255, 255, 255); font-family: 'futura-pt',sans-serif; font-size: 0.825em; font-weight: bold; padding: 6px 10px; float: left; margin-right: 10px; border: 0px none; background-color: rgb(0, 0, 0);" /></a></li>
								</ul>
							</div>
						</div>
					</div>
					
                    <!--<div style="width:665px; float:left;">-->
						<div id="mediaContent" style="width:685px;">
                        	<div class="titleCont">
								<div class="blkB" style="width:60px; padding-left:6px;">Date</div>
								<div class="blkB" style="width:160px;">Title</div>
								<div class="blkB" style="width:110px;">Time</div>
								<div class="blkB" style="width:140px;">Venue</div>
								<!--<div class="blkB" style="width:200px;">Creator</div>-->
								<div class="blkI" style="width:110px;">Type</div>
                            </div>
						</div>
						<div id="mediaContent" style="width:685px; height:400px;overflow-x:hidden; overflow-y:auto;">
							<div class="tableCont" style="border-bottom:none;">
							<?php
								/* while($showevent=mysql_fetch_assoc($getevent))
								{
									$get_event_type=$editprofile->Get_Event_type($showevent['id']);
									//echo $showevent['id'];
								?>
								<div id="AccordionContainer" class="tableCont">
									<div class="blkB" style=""><?php if($showevent['date']!=""){ echo date("d.m.y", strtotime($showevent['date'])); } else{?>&nbsp;<?php } ?></div>
									<div class="blkB" style="width:200px;"><a href="/<?php echo $showevent['profile_url'];?>" onclick="return chk_profile_url('<?php echo $showevent['profile_url'];?>')"><?php if($showevent['title']!=""){echo $showevent['title'];}else{?>&nbsp;<?php }?></a></div>
									<!--<div class="blkB" style="width:200px;"><a href=""><?php if($nameres['name']!=""){echo $nameres['name'];}else{?>&nbsp;<?php }?></a></div>-->
									<div class="blkB" style="width:100px;"><?php if($get_event_type['name']!="") { echo $get_event_type['name'];}else{?>&nbsp;<?php }?></div>
									<div class="blkD"><a href="/<?php echo $showevent['profile_url'];?>" onclick="return chk_profile_url('<?php echo $showevent['profile_url'];?>')">Display</a></div>
									<div class="icon"><a href="add_artist_event.php?id=<?php echo $showevent['id'];?>"><img src="images/profile/edit.png" /></a></div>
									<div class="icon"><a onclick="return confirmdelete('<?php echo $showevent['title'];?>')" href="insertartistevent.php?delete=<?php echo $showevent['id'];?>"><img src="images/profile/delete.png" /></a></div>
								</div>
							<?php
								}
								$get_all_friends_event = $editprofile->Get_all_friends();
								if($get_all_friends_event !="")
								{
									while($res_all_friends_event = mysql_fetch_assoc($get_all_friends_event))
									{
										$get_friend_art_info_event = $editprofile->get_friend_art_info($res_all_friends_event['fgeneral_user_id']);
										$get_all_tag_event = $editprofile->get_all_tagged_event($get_friend_art_info_event);
										if(mysql_num_rows($get_all_tag_event)>0)
										{
											while($res_events = mysql_fetch_assoc($get_all_tag_event))
											{
												$get_event_type = $editprofile->Get_Event_type($res_events['id']);
												$exp_creator = explode('(',$res_events['creator']);
												$exp_tagged_email = explode(",",$res_events['tagged_user_email']);
												for($exp_count = 0;$exp_count<count($exp_tagged_email);$exp_count++)
												{
													if($exp_tagged_email[$exp_count]==""){continue;}
													else{
														if($exp_tagged_email[$exp_count]==$_SESSION['login_email'])
														{
															?>
															<div id="AccordionContainer" class="tableCont">
																<div class="blkB" style=""><?php if($res_events['date']!=""){ echo date("d.m.y", strtotime($res_events['date'])); } else{?>&nbsp;<?php } ?></div>
																<div class="blkB" style="width:200px;"><?php if($res_events['title']!=""){echo $res_events['title'];}else{?>&nbsp;<?php }?></div>
																<!--<div class="blkB" style="width:200px;"><a href=""><?php if($exp_creator[0]!=""){echo $exp_creator[0];}else{?>&nbsp;<?php }?></a></div>-->
																<div class="blkB" style="width:100px;"><?php if($get_event_type['name']!=""){echo $get_event_type['name'];}else{?>&nbsp;<?php }?></div>
																<div class="blkD">&nbsp;</div>
																<div class="icon">&nbsp;</div>
																<div class="icon"><a onclick="return confirmdelete('<?php echo $res_events['title'];?>')" href="insertartistevent.php?delete=<?php echo $res_events['id'];?>"><img src="images/profile/delete.png" /></a></div>
															</div>
															<?php
														}
													}
													
												}
											}
										}
									}
								} */
								
								$getuer_id = array();
								if($get_levent!="" && $get_levent!=Null)
								{
									while($get_eveup_ids = mysql_fetch_assoc($get_levent))
									{
										//$get_eveup_ids['id'] = $get_eveup_ids['id'].'~art';
										$get_eveup_ids['whos_event'] = "own_art_event";
										$get_eveup_ids['id'] = $get_eveup_ids['id'] ."~art";
										$getuer_id[] = $get_eveup_ids;
									}
								}
								
								$getuerc_id = array();
								if($get_lceevent!="" && $get_lceevent!=Null)
								{
									while($get_xeveup_ids = mysql_fetch_assoc($get_lceevent))
									{
										//$get_xeveup_ids['id'] = $get_xeveup_ids['id'].'~com';
										$get_xeveup_ids['edit_com'] = 'yes';
										$get_xeveup_ids['whos_event'] = "own_com_event";
										$get_xeveup_ids['id'] = $get_xeveup_ids['id'] ."~com";
										$getuerc_id[] = $get_xeveup_ids;
									}
								}
								
								$getua_ev = array();
								$getua_ev = $editprofile->only_creator_oupevent();
								$selartsup = array();
								 if($event_avail!=0)
								{
									$acc_event = array_unique($acc_event);
									for($i=0;$i<count($acc_event);$i++)
									{
											//$sel_tagged="";
										if($acc_event[$i]!="" && $acc_event[$i]!=0)
										{
											$dumartr = $editprofile->get_event_tagged_by_other_user($acc_event[$i]);
											if(!empty($dumartr) && $dumartr['id']!="" && $dumartr['id']!="")
											{
												$dumartr['id'] = $dumartr['id'].'~'.'art';
												$dumartr['edit'] = 'yes';
												$dumartr['whos_event'] = 'art_tag_eve';
												$selartsup[] = $dumartr;
											}												
										}
									}
								} 
								
								$selcomsup = array();
								 if($event_cavail!=0)
								{
									$acc_cevent = array_unique($acc_cevent);
									for($t_i=0;$t_i<count($acc_cevent);$t_i++)
									{
											//$sel_tagged="";
										if($acc_cevent[$t_i]!="" && $acc_cevent[$t_i]!=0)
										{
											$dumacomr = $editprofile->get_cevent_tagged_by_other_user($acc_cevent[$t_i]);
											if(!empty($dumacomr) && $dumacomr['id']!="" && $dumacomr['id']!="")
											{
												$dumacomr['id'] = $dumacomr['id'].'~'.'com';
												$dumacomr['edit'] = 'yes';
												$dumacomr['edit_com'] = 'yes';
												$dumacomr['whos_event'] = 'com_tag_eve';
												$selcomsup[] = $dumacomr;
											}												
										}
									}
								} 
								
								$getiu_id = array();
								if($get_leventrec!="" && $get_leventrec!=Null)
								{
									while($get_channel_ids = mysql_fetch_assoc($get_leventrec))
									{
										//$get_channel_ids['id'] = $get_channel_ids['id'].'~art';
										$get_channel_ids['whos_event'] = "own_art_event";
										$get_channel_ids['id'] = $get_channel_ids['id'] ."~art";
										$getiu_id[] = $get_channel_ids;
									}
								}
								
								$getic_id = array();
								if($get_lceeventrec!="" && $get_lceeventrec!=Null)
								{
									while($get_rchannel_ids = mysql_fetch_assoc($get_lceeventrec))
									{
										$get_rchannel_ids['edit_com'] = 'yes';	
										$get_rchannel_ids['whos_event'] = "own_com_event";
										$get_rchannel_ids['id'] = $get_rchannel_ids['id'] ."~com";
										$getic_id[] = $get_rchannel_ids; 
									}
								}
								
								$getonl_rev = array();
								$getonl_rev = $editprofile->only_creator_orecevent();
								
								$get_only2cre_rev = array();
								//$get_only2cre_rev = $editprofile->only_creator2_recevent();
								$get_only2cre_rev = $editprofile->only_creator2_orecevent();
								
								$get_only2cre_ev = array();
								//$get_only2cre_ev = $editprofile->only_creator2ev_upevent();
								$get_only2cre_ev = $editprofile->only_creator2ev_oupevent();
								
								$selarts2u_tagged = array();
								if($project_avail!=0)
								{
									$acc_project = array_unique($acc_project);
									for($acc_pro=0;$acc_pro<count($acc_project);$acc_pro++)
									{												
										if($acc_project[$acc_pro]!="" && $acc_project[$acc_pro]!=0)
										{													
											$sql_1_cre = $editprofile->get_event_tagged_by_other2_user($acc_project[$acc_pro]);
											if($sql_1_cre!="")
											{
												if($sql_1_cre !="" && $sql_1_cre!= Null)
												{
													if(mysql_num_rows($sql_1_cre)>0)
													{
														while($run = mysql_fetch_assoc($sql_1_cre))
														{
															if($newres1['artist_id']!=$run['artist_id'])
															{
																$run['whos_event'] = "art_creator_tag_eve";
																$run['id'] = $run['id'].'~'.'art';
																$selarts2u_tagged[] = $run;
															}
														}
													}
												}
											}
											
											$sql_2_cre = $editprofile->get_event_ctagged_by_other2_user($acc_project[$acc_pro]);
											if($sql_2_cre !="" && $sql_2_cre!= Null)
											{
												if(mysql_num_rows($sql_2_cre)>0)
												{
													while($run_c = mysql_fetch_assoc($sql_2_cre))
													{
														if($newres1['community_id']!=$run_c['community_id'])
														{
															$run_c['whos_event'] = "com_creator_tag_eve";
															$run_c['id'] = $run_c['id'].'~'.'com';
															$selarts2u_tagged[] = $run_c;
														}
													}
												}
											}
										}
									}
								}
								
								$selcoms2u_tagged = array();
								if($project_cavail!=0)
								{
									$acc_cproject = array_unique($acc_cproject);
									for($acc_pro=0;$acc_pro<count($acc_cproject);$acc_pro++)
									{												
										if($acc_cproject[$acc_pro]!="" && $acc_cproject[$acc_pro]!=0)
										{													
											$sql_1_cre = $editprofile->get_cevent_tagged_by_other2_user($acc_cproject[$acc_pro]);
											if($sql_1_cre!="")
											{
												if($sql_1_cre !="" && $sql_1_cre!= Null)
												{
													if(mysql_num_rows($sql_1_cre)>0)
													{
														while($run = mysql_fetch_assoc($sql_1_cre))
														{
															if($newres1['artist_id']!=$run['artist_id'])
															{
																$run['whos_event'] = "art_creator_tag_eve";
																$run['id'] = $run['id'].'~art';
																$selcoms2u_tagged[] = $run;
															}
														}
													}
												}
											}
											
											$sql_2_cre = $editprofile->get_cevent_ctagged_by_other2_user($acc_cproject[$acc_pro]);
											if($sql_2_cre !="" && $sql_2_cre!= Null)
											{
												if(mysql_num_rows($sql_2_cre)>0)
												{
													while($run_c = mysql_fetch_assoc($sql_2_cre))
													{
														if($newres1['community_id']!=$run_c['community_id'])
														{
															$run_c['whos_event'] = "com_creator_tag_eve";
															$run_c['id'] = $run_c['id'].'~com';
															$selcoms2u_tagged[] = $run_c;
														}
													}
												}
											}
										}
									}
								}
								
								$sel_arts = array();
								if($event_avail!=0)
								{
									$acc_event = array_unique($acc_event);
									for($t=0;$t<count($acc_event);$t++)
									{
										//$sel_tagged="";
										if($acc_event[$t]!="" && $acc_event[$t]!=0)
										{
											$dum_arte = $editprofile->get_event_tagged_by_other_userrec($acc_event[$t]);
											if(!empty($dum_arte) && $dum_arte['id']!="" && $dum_arte['id']!="")
											{
												if($newres1['artist_id']!=$dum_arte['artist_id'])
												{
													$dum_arte['edit'] = 'yes';
													$dum_arte['whos_event'] = "art_tag_eve";
													$dum_arte['id'] = $dum_arte['id'] ."~art";
													$sel_arts[] = $dum_arte;
												}
											}
										}
									}
								}
								
								$sel_coms = array();
								if($event_cavail!=0)
								{
									$acc_cevent = array_unique($acc_cevent);
									for($t=0;$t<count($acc_cevent);$t++)
									{
										//$sel_tagged="";
										if($acc_cevent[$t]!="" && $acc_cevent[$t]!=0)
										{
											$dum_artec = $editprofile->get_cevent_tagged_by_other_userrec($acc_cevent[$t]);
											if(!empty($dum_artec) && $dum_artec['id']!="" && $dum_artec['id']!="")
											{
												if($newres1['community_id']!=$dum_artec['community_id'])
												{
													$dum_artec['edit'] = 'yes';
													$dum_artec['edit_com'] = 'yes';
													$dum_artec['whos_event'] = "com_tag_eve";
													$dum_artec['id'] = $dum_artec['id'] ."~com";
													$sel_coms[] = $dum_artec;
												}
											}
										}
									}
								}
								
								$selarts2r_tagged = array();
								if($project_avail!=0)
								{
									$acc_project = array_unique($acc_project);
									for($acc_pro=0;$acc_pro<count($acc_project);$acc_pro++)
									{												
										if($acc_project[$acc_pro]!="" && $acc_project[$acc_pro]!=0)
										{													
											$sql_1_cre = $editprofile->get_event_tagged_by_other2_userrec($acc_project[$acc_pro]);
											if($sql_1_cre!="")
											{
												if($sql_1_cre !="" && $sql_1_cre!= Null)
												{
													if(mysql_num_rows($sql_1_cre)>0)
													{
														while($run = mysql_fetch_assoc($sql_1_cre))
														{
															if($newres1['artist_id']!=$run['artist_id'])
															{
																$run['id'] = $run['id'].'~art';
																$run['whos_event'] = "art_creator_tag_eve";
																$selarts2r_tagged[] = $run;
															}
														}
													}
												}
											}
											
											$sql_2_cre = $editprofile->get_event_ctagged_by_other2_userrec($acc_project[$acc_pro]);
											if($sql_2_cre !="" && $sql_2_cre!= Null)
											{
												if(mysql_num_rows($sql_2_cre)>0)
												{
													while($run_c = mysql_fetch_assoc($sql_2_cre))
													{
														if($newres1['community_id']!=$run_c['community_id'])
														{
															$run_c['id'] = $run_c['id'].'~com';
															$run_c['whos_event'] = "com_creator_tag_eve";
															$selarts2r_tagged[] = $run_c;
														}
													}
												}
											}
										}
									}
								}
								
								$selcoms2r_tagged = array();
								if($project_cavail!=0)
								{
									$acc_cproject = array_unique($acc_cproject);
									for($acc_pro=0;$acc_pro<count($acc_cproject);$acc_pro++)
									{												
										if($acc_cproject[$acc_pro]!="" && $acc_cproject[$acc_pro]!=0)
										{													
											$sql_1_cre = $editprofile->get_cevent_tagged_by_other2_userrec($acc_cproject[$acc_pro]);
											if($sql_1_cre!="")
											{
												if($sql_1_cre !="" && $sql_1_cre!= Null)
												{
													if(mysql_num_rows($sql_1_cre)>0)
													{
														while($run = mysql_fetch_assoc($sql_1_cre))
														{
															if($newres1['artist_id']!=$run['artist_id'])
															{
																$run['id'] = $run['id'].'~art';
																$run['whos_event'] = "art_creator_tag_eve";
																$selcoms2r_tagged[] = $run;
															}
														}
													}
												}
											}
											
											$sql_2_cre = $editprofile->get_cevent_ctagged_by_other2_userrec($acc_cproject[$acc_pro]);
											if($sql_2_cre !="" && $sql_2_cre!= Null)
											{
												if(mysql_num_rows($sql_2_cre)>0)
												{
													while($run_c = mysql_fetch_assoc($sql_2_cre))
													{
														if($newres1['community_id']!=$run_c['community_id'])
														{
															$run_c['id'] = $run_c['id'].'~com';
															$run_c['whos_event'] = "com_creator_tag_eve";
															$selcoms2r_tagged[] = $run_c;
														}
													}
												}
											}
										}
									}
								}
								
								//$final_eves = array();
								$final_ed = array_merge($getiu_id,$sel_arts,$sel_coms,$getonl_rev,$getic_id,$selcomsup,$selartsup,$getuerc_id,$getuer_id,$getua_ev,$get_only2cre_rev,$get_only2cre_ev,$selcoms2u_tagged,$selarts2u_tagged,$selarts2r_tagged,$selcoms2r_tagged);
								$sort = array();
								foreach($final_ed as $k=>$v) 
								{								
									//$create_c = strpos($v['creator'],'(');
									//$end_c = substr($v['creator'],0,$create_c);
									
									//$sort['creator'][$k] = $end_c;
									//$sort['from'][$k] = $end_f;
									//$sort['track'][$k] = $v['track'];
									$sort['date'][$k] = $v['date'];
								}
								//var_dump($sort);
								if(!empty($sort))
								{
									array_multisort($sort['date'], SORT_DESC,$final_ed);
								}
								
								$dummy_ape = array();
								$dummy_cpe = array();
								
								$get_all_del_id = $editprofile->get_deleted_project_id();
								$exp_del_id = explode(",",$get_all_del_id['deleted_event_id']);
								for($count_all=0;$count_all<count($final_ed);$count_all++){
									for($count_del=0;$count_del<count($exp_del_id);$count_del++){
										if($exp_del_id[$count_del]!=""){
											if($final_ed[$count_all]['id'] == $exp_del_id[$count_del]){
												$final_ed[$count_all] ="";
											}
										}
									}
								}
								for($evp_f=0;$evp_f<count($final_ed);$evp_f++)
								{
									if($final_ed[$evp_f]!="")
									{
										$chk_ac_pe = 0;
										if(isset($final_ed[$evp_f]['artist_id']))
										{
											$get_event_type = $editprofile->Get_Event_subtype($final_ed[$evp_f]['id']);
											if(in_array($final_ed[$evp_f]['id'],$dummy_ape))
											{
												$chk_ac_pe = 1;
											}
											else
											{
												$dummy_ape[] = $final_ed[$evp_f]['id'];
											}
										}
										elseif(isset($final_ed[$evp_f]['community_id']))
										{
											$get_event_type = $editprofile->Get_ACEvent_subtype($final_ed[$evp_f]['id']);
											if(in_array($final_ed[$evp_f]['id'],$dummy_cpe))
											{
												$chk_ac_pe = 1;
											}
											else
											{
												$dummy_cpe[] = $final_ed[$evp_f]['id'];
											}
										}
										if($chk_ac_pe==1)
										{}
										else
										{
			?>
											<div id="AccordionContainer" class="tableCont">
												<div class="blkB" style="width:60px;"><?php if($final_ed[$evp_f]['date']!=""){ echo date("m.d.y", strtotime($final_ed[$evp_f]['date'])); } else{?>&nbsp;<?php } ?></div>
												<div class="blkB" style="width:160px;"><?php 
												if($final_ed[$evp_f]['profile_url']!="")
												{ 
												?>
													<a href="/<?php echo $final_ed[$evp_f]['profile_url'];?>" onclick="return chk_profile_url('<?php echo $final_ed[$evp_f]['profile_url'];?>')"><?php if($final_ed[$evp_f]['title']!=""){echo $final_ed[$evp_f]['title'];}else{?>&nbsp;<?php }?></a>
												<?php 
												}
												else{ if($final_ed[$evp_f]['title']!=""){echo $final_ed[$evp_f]['title'];}else{?>&nbsp;<?php } }?></div>
												<div class="blkB" style="width:110px;"><?php if($final_ed[$evp_f]['start_time']!="" && $final_ed[$evp_f]['end_time']!="") { echo $final_ed[$evp_f]['start_time'].$final_ed[$evp_f]['start_timeap'].' - '.$final_ed[$evp_f]['end_time'].$final_ed[$evp_f]['end_timeap']; } else{?>&nbsp;<?php } ?></div>
												<div class="blkB" style="width:140px;"><?php
												if($final_ed[$evp_f]['venue_name']!="")
												{
													if($final_ed[$evp_f]['venue_website']!="" && $final_ed[$evp_f]['venue_website']!="http://" && $final_ed[$evp_f]['venue_website']!="https://")
													{
														echo '<a target="_blank" href='.$final_ed[$evp_f]['venue_website'].'>'.$final_ed[$evp_f]['venue_name'].'</a>';
													}
													else
													{
														echo $final_ed[$evp_f]['venue_name']; 
													}
												}
												else{?>&nbsp;<?php } ?></div>
												<!--<div class="blkB" style="width:200px;"><a href=""><?php if($nameres['name']!=""){echo $nameres['name'];}else{?>&nbsp;<?php }?></a></div>-->
												<div class="blkB" style="width:110px;"><?php if($get_event_type['name']!="") { echo $get_event_type['name'];}else{?>&nbsp;<?php }?></div>
												<!--<div class="blkD"><a href="/<?php echo $final_ed[$evp_f]['profile_url'];?>" onclick="return chk_profile_url('<?php echo $final_ed[$evp_f]['profile_url'];?>')">Display</a></div>-->
												<?php
												if($final_ed[$evp_f]['whos_event']=="own_art_event")
												{
													$exp_art_id = explode("~",$final_ed[$evp_f]['id']);
												?>
												<div class="icon"><a href="add_artist_event.php?id=<?php echo $exp_art_id[0];?>"><img src="images/profile/edit.png" /></a></div>
												<div class="icon"><a onclick="return confirmdelete('<?php echo $final_ed[$evp_f]['title'];?>')" href="insertartistevent.php?delete=<?php echo $exp_art_id[0];?>&whos_event=<?php echo $final_ed[$evp_f]['whos_event'];?>"><img src="images/profile/delete.png" /></a></div>
												<?php
												}else if($final_ed[$evp_f]['whos_event']=="own_com_event")
												{
													$exp_com_id = explode("~",$final_ed[$evp_f]['id']);
												?>
												<div class="icon"><a href="add_community_event.php?id=<?php echo $exp_com_id[0];?>"><img src="images/profile/edit.png" /></a></div>
												<div class="icon"><a onclick="return confirmdelete('<?php echo $final_ed[$evp_f]['title'];?>')" href="insertcommunityevent.php?delete=<?php echo $exp_com_id[0];?>&whos_event=<?php echo $final_ed[$evp_f]['whos_event'];?>"><img src="images/profile/delete.png" /></a></div>
												<?php
												}else if($final_ed[$evp_f]['whos_event']=="com_creator_tag_eve" || $final_ed[$evp_f]['whos_event']=="art_creator_tag_eve" || $final_ed[$evp_f]['whos_event']=="com_tag_eve" || $final_ed[$evp_f]['whos_event']=="art_tag_eve" || $final_ed[$evp_f]['whos_event']=="only_creator" || $final_ed[$evp_f]['whos_event']=="art_creator_creator_eve" || $final_ed[$evp_f]['whos_event']=="com_creator_creator_eve")
												{
												?>
												<div class="icon">&nbsp;</div>
												<div class="icon"><a onclick="return confirmdelete('<?php echo $final_ed[$evp_f]['title'];?>')" href="insertartistevent.php?delete=<?php echo $final_ed[$evp_f]['id'];?>&whos_event=<?php echo $final_ed[$evp_f]['whos_event'];?>"><img src="images/profile/delete.png" /></a></div>
												<?php
												} 
												if($final_ed[$evp_f]['profile_url']!="")
												{ 
												?>
													<div class="icon"><a href="/<?php echo $final_ed[$evp_f]['profile_url'];?>" onclick="return chk_profile_url('<?php echo $final_ed[$evp_f]['profile_url'];?>')"><img width="20" height="20" src="images/magnifying_glass_icon.gif" style=""/></a></div>
												<?php 
												}
												else
												{
												?>
													<div class="icon"><img width="20" height="20" src="images/magnifying_glass_icon.gif" style="position:relative;top:5px;"/></div>
												<?php
												}
												?>
											</div>
			<?php
										}
									}
								}										
		?>                            
							</div>
                        </div>
                    <!--</div>		-->			
				</div>
              
                
                
                <div class="subTabs" id="services" style="display:none;">
                	<h1>Add/Edit "Artist Name" Service</h1>
                    <div style="width:665px; float:left;">
                    	<div id="actualContent">
                        	<form>
                        	<div class="fieldCont">
                           		<div class="fieldTitle">Service offered</div>
                                <select name="select-category-artist-subtype" size="5" multiple="multiple" id="subtype-select" class="list">
                                   	<option value="0" selected="selected">Select Type</option>					
                    			</select>
                    		</div>
                            <div class="fieldCont">
                           	  	<div class="fieldTitle">Title</div>
                                <input name="homepage" type="text" class="fieldText" id="datepickerB">
                    		</div>
                            <div class="fieldCont">
                           	  	<div class="fieldTitle">Image</div>
                                <input name="homepage" type="file" class="fieldText" id="datepickerB">
                    		</div>
                            <div class="fieldCont">
                           	  	<div class="fieldTitle">Email</div>
                                <input name="homepage" type="text" class="fieldText" id="datepickerB">
                    		</div>
                            <div class="fieldCont">
                           	  	<div class="fieldTitle">Homepage</div>
                                <input name="homepage" type="text" class="fieldText" id="datepickerB">
                    		</div>
                            <div class="fieldCont">
                           	  	<div class="fieldTitle">Phone</div>
                                <input name="homepage" type="text" class="fieldText" id="datepickerB">
                    		</div>
                            <div class="fieldCont">
                                <div class="fieldTitle">Address</div>
                                <textarea class="textbox"></textarea>
                            </div>
                            <div class="fieldCont">
                            	<div class="fieldTitle">Type</div>
                                <select name="select-category-artist-subtype" class="dropdown" id="subtype-select">
                                   	<option value="0" selected="selected">Select Type</option>					
                    			</select>
                    		</div>
                            <div class="fieldCont">
                                <div class="fieldTitle">Sub Type</div>
                                <select name="select-category-artist-metatype" class="dropdown" id="metatype-select">
                                	<option value="0" selected="selected">Select Sybtype</option>					
                    			</select>
                            </div>
                            <div class="fieldCont">
                                <div class="fieldTitle">Description</div>
                                <textarea class="textbox"></textarea>
                            </div>
                            <div class="fieldCont">
                           		<div class="fieldTitle">Add Media</div>
                                <select name="select-category-artist-subtype" size="5" multiple="multiple" id="subtype-select" class="list">
                                   	<option value="0" selected="selected">Select Type</option>					
                    			</select>
                    		</div>
                            <div class="fieldCont">
                           		<div class="fieldTitle">Media</div>
                                <select name="select-category-artist-subtype" size="5" multiple="multiple" id="subtype-select" class="list">
                                   	<option value="0" selected="selected">Select Type</option>					
                    			</select>
                    		</div>
                            <div class="fieldCont">
                           		<div class="fieldTitle">Add Artist &amp; Community Members Worked With</div>
                                <select name="select-category-artist-subtype" size="5" multiple="multiple" id="subtype-select" class="list">
                                   	<option value="0" selected="selected">Select Type</option>					
                    			</select>
                    		</div>
                            <div class="fieldCont">
                           		<div class="fieldTitle">Artist &amp; Community Members Worked With</div>
                                <select name="select-category-artist-subtype" size="5" multiple="multiple" id="subtype-select" class="list">
                                   	<option value="0" selected="selected">Select Type</option>					
                    			</select>
                    		</div>
                            <div class="fieldCont">
                           		<div class="fieldTitle">Related Events</div>
                                <select name="select-category-artist-subtype" size="5" multiple="multiple" id="subtype-select" class="list">
                                   	<option value="0" selected="selected">Select Type</option>					
                    			</select>
                    		</div>
                            <div class="fieldCont">
                           		<div class="fieldTitle">Add Related Events</div>
                                <select name="select-category-artist-subtype" size="5" multiple="multiple" id="subtype-select" class="list">
                                   	<option value="0" selected="selected">Select Type</option>					
                    			</select>
                    		</div>
                            <div class="fieldCont">
                           		<div class="fieldTitle">Add Projects Worked On</div>
                                <select name="select-category-artist-subtype" size="5" multiple="multiple" id="subtype-select" class="list">
                                   	<option value="0" selected="selected">Select Type</option>					
                    			</select>
                    		</div>
                            <div class="fieldCont">
                           		<div class="fieldTitle">Add Related Services</div>
                                <select name="select-category-artist-subtype" size="5" multiple="multiple" id="subtype-select" class="list">
                                   	<option value="0" selected="selected">Select Type</option>					
                    			</select>
                    		</div>
                            <div class="fieldCont">
                           		<div class="fieldTitle">Related Services</div>
                                <select name="select-category-artist-subtype" size="5" multiple="multiple" id="subtype-select" class="list">
                                   	<option value="0" selected="selected">Select Type</option>					
                    			</select>
                    		</div>
                            <div class="fieldCont">
                           		<div class="fieldTitle"><b>Pricing</b></div>
                            </div>
                            <div class="fieldCont">
                           	  	<div class="fieldTitle">Date</div>
                                <input name="homepage" type="text" class="fieldText" id="datepickerB">
                    		</div>
                            <div class="fieldCont">
                           	  	<div class="fieldTitle">Interval</div>
                                <input name="homepage" type="text" class="fieldText" id="datepickerB">
                    		</div>
                            <div class="fieldCont">
                           	  	<div class="fieldTitle">Interval Length</div>
                                <input name="homepage" type="text" class="fieldText" id="datepickerB">
                    		</div>
                            <div class="fieldCont">
                           	  	<div class="fieldTitle">Cost Per Interval</div>
                                <input name="homepage" type="text" class="fieldText" id="datepickerB">
                    		</div>
                            <div class="fieldCont">
                           	  	<div class="fieldTitle">Total Intervals</div>
                                <input name="homepage" type="text" class="fieldText" id="datepickerB">
                    		</div>
                            <div class="fieldCont">
                           	  	<div class="fieldTitle">Total Cost</div>
                                <input name="homepage" type="text" class="fieldText" id="datepickerB">
                    		</div>
                            <div class="fieldCont">
                           		<div class="fieldTitle">Point Sharing</div>
                                <select name="select-category-artist-subtype" size="5" multiple="multiple" id="subtype-select" class="list">
                                   	<option value="0" selected="selected">Select Type</option>					
                    			</select>
                    		</div>
                            <div class="fieldCont">
                                <div class="fieldTitle">Description</div>
                                <textarea class="textbox"></textarea>
                            </div>
                            <div class="fieldCont">
                                <div class="fieldTitle"></div>
                                <input type="button" class="register" style="border-radius:5px; -webkit-border-radius:5px; -moz-border-radius:5px; -ms-border-radius:5px; -o-border-radius:5px;" value="Save" />
                            	<input type="button" style="border-radius:5px; -webkit-border-radius:5px; -moz-border-radius:5px; -ms-border-radius:5px; -o-border-radius:5px;" class="register" value="Publish" />
                            </div>
                            </form>
                        </div>
                       <!-- <div style="float:right;" id="selected-types"></div>-->
                    </div>
                </div>
                
                
				
				
				
				<div class="subTabs" id="memberships" style="display:none;">
					<h1><?php echo $newrow['name']; ?> Fan Club Memberships</h1>
					<div style="width:665px; float:left;">
						<div id="actualContent">
							<form action="create_artist_member.php" method="POST">
								<div class="fieldCont">
									<div class="fieldTitle">Frequency</div>
									<select name="frequency" class="dropdown" id="frequency">
										<option value="annual">Annual</option>
										<option value="lifetime">LifeTime</option>
									</select>
								</div>
								<div class="fieldCont">
									<div class="fieldTitle">Cost</div>
									<input name="cost" id="cost" type="text" class="fieldText" />
								</div>
								<div class="fieldCont">
									<div class="fieldTitle">Description</div>
									<!--<input name="description" type="text" class="fieldText" />
									<textarea  class="jquery_ckeditor" cols="4" id="description" name="description" rows="10"></textarea>-->
								</div>
								<?php $get_artist_fan = $editprofile->Get_artist_fan($newrow['artist_id']);
								$get_artist_total_fan = $editprofile->Get_artist_total_fan($newrow['artist_id']);
								?>
								<div class="fieldCont">Current Fan Club Memberships: <?php echo $get_artist_fan;?> ($864/year)</div>
                                <div class="fieldCont">Total Fan Club Memberships: <?php echo $get_artist_total_fan;?> ($4068 to Date)</div>
								<div class="fieldCont">
									<div class="fieldTitle"></div>
									<input type="submit" class="register" style="border-radius:5px; -webkit-border-radius:5px; -moz-border-radius:5px; -ms-border-radius:5px; -o-border-radius:5px;" value="Save" />
									<input type="button" style="border-radius:5px; -webkit-border-radius:5px; -moz-border-radius:5px; -ms-border-radius:5px; -o-border-radius:5px;" class="register" value="Publish" />
								</div>
								
							</form>
						</div>
					</div>
				</div>
                
				
				
				
                
               	<!--<div class="subTabs" id="memberships" style="display:none;">
					<h1>"Artist Name" Memberships</h1>
					<div style="width:665px; float:left;">
						<div id="actualContent">
							<form>
								<div class="fieldCont">
									<div class="fieldTitle">Frequency</div>
									<select name="select-category-artist-metatype" class="dropdown" id="metatype-select">
										<option value="0">Interval</option>
										<option value="0">LifeTime</option>
									</select>
								</div>
								<div class="fieldCont">
									<div class="fieldTitle">Interval Length</div>
									<input name="homepage" type="text" class="fieldText" />
								</div>
								<div class="fieldCont">
									<div class="fieldTitle">Cost</div>
									<input name="homepage" type="text" class="fieldText" />
								</div>
								<div class="fieldCont">
									<div class="fieldTitle">Description</div>
									<input name="homepage" type="text" class="fieldText" />
								</div>
								<div class="fieldCont">
									<div class="fieldTitle">Point Sharing</div>
									<select name="select-category-artist-subtype" size="5" multiple="multiple" id="subtype-select" class="list">
										<option value="0" selected="selected">Select Type</option>					
									</select>
								</div>
								<div class="fieldCont">
									<div class="fieldTitle"></div>
									<input type="button" class="register" value="Save" />
									<input type="button" class="register" value="Publish" />
								</div>
								<div class="fieldCont">
									<b>Add/Edit Other Membership</b>
								</div>
								<div class="fieldCont">
									<div class="fieldTitle">Frequency</div>
									<select name="select-category-artist-metatype" class="dropdown" id="metatype-select">
										<option value="0">Interval</option>
										<option value="0">LifeTime</option>
									</select>
								</div>
								<div class="fieldCont">
									<div class="fieldTitle">Interval Length</div>
									<input name="homepage" type="text" class="fieldText" />
								</div>
								<div class="fieldCont">
									<div class="fieldTitle">Cost</div>
									<input name="homepage" type="text" class="fieldText" />
								</div>
								<div class="fieldCont">
									<div class="fieldTitle">Description</div>
									<input name="homepage" type="text" class="fieldText" />
								</div>
								<div class="fieldCont">
									<div class="fieldTitle">Point Sharing</div>
									<select name="select-category-artist-subtype" size="5" multiple="multiple" id="subtype-select" class="list">
										<option value="0" selected="selected">Select Type</option>					
									</select>
								</div>
								<div class="fieldCont">
									<div class="fieldTitle"></div>
									<input type="button" class="register" value="Save" />
									<input type="button" class="register" value="Publish" />
								</div>
							</form>
						</div>
					</div>
					
					<p><strong>Edit Lee Rick Membership </strong></p>
					  <p><strong>Frequency:</strong> Interval or LifeTime <br />
						<strong>Interval Length</strong>: 1Hr (if Interval is chosen) <br />
						<strong>Cost: </strong>300 Purify Points
						<br />
					  <strong>Description:</strong> Text<br />
					  <strong>Share Points: </strong>Select a user and percentage to add <strong><br />
					  Point Sharing<br />
					  </strong>55% to Me<br />
					  25% to Kaz (Edit/Remove)<br />
					  25% to Granny (Edit/Remove)<br />
					  5% to Purify (Edit, Can make 0% but cannot remove)</p>
					  <p>Publish/Save</p>
					  <p><strong>Total Points Received from Lee Rick Membership: 5000 Points </strong></p>
					  <p><strong>Add/Edit Other Membership  </strong></p>
					  <p><strong>Title:<br />
						Frequency:</strong> Interval or LifeTime <br />
						<strong>Interval Length</strong>: 1Hr (if Interval is chosen) <br />
						<strong>Cost: </strong>300 Purify Points <br />
						<strong>Description:</strong> Text<br />
						<strong>Share Points: </strong>Select a user and percentage to add <strong><br />
					Point Sharing<br />
						</strong>55% to Me<br />
					25% to Kaz (Edit/Remove)<br />
					25% to Granny (Edit/Remove)<br />
					5% to Purify (Edit, Can make 0% but cannot remove)</p>
					  <p>Publish/Save</p>
					  <p><strong>Other Membership </strong></p>
					  <p><a href="#">Lee Rick Un-Titled</a> (Edit) <br />
						LifeTime, 100 Points, 30 Subscribers, 3000 Total Points Received (2000 to Lee Rick, 500 to Kaz, 500 to Purify)<br />
						<br />
						<a href="#">Lee Rick Self-Titled</a> (Edit)<a href="#"><br />
						</a>LifeTime, 100 Points, 20 Subscribers, 2000 Total Points Received (1000 to Lee Rick, 750 to Kaz, 250 to Purify)<br />
					<br />
					  <a href="#">03.12.09 Lee Rick Performance</a> (Edit)<br />
					  Interval,<a href="#"> El Rincon Club</a>, 500 Points, <a href="#">Confirm</a>, Paymet Not Received</p>
					  <p><strong>Total Points Received from Other Membership: 5000 Points
						</strong><em><a href="#"><br />
						</a></em></p>
				</div>-->
                
                
                
                <div class="subTabs" id="projects" style="display:none;">
					<h1><?php echo $nameres['name'];?> Projects</h1>
					<div style="float:left;">
					<div id="mediaContent" style="width:685px;">
                    	<div class="topLinks" style="border-bottom:1px dotted #999;">
                            <div class="links" style="float:left;">
								<ul>
									<li><a href="add_artist_project.php"><input type="button" value="Add Project" style="border-radius:5px; -webkit-border-radius:5px; -moz-border-radius:5px; -ms-border-radius:5px; -o-border-radius:5px; color: rgb(255, 255, 255); font-family: 'futura-pt',sans-serif; font-size: 0.825em; font-weight: bold; padding: 6px 10px; float: left; margin-right: 10px; border: 0px none; background-color: rgb(0, 0, 0);" /></a></li>
									<!--<li><a href="profile_artist.html"><input type="button" value="View Profile" /></a></li>-->
								</ul>
							</div>
						</div>
					</div>
                  <!--  <div style="width:665px; float:left;">-->
						<div id="mediaContent" style="width:685px;">
                        	<div class="titleCont">
								<div class="blkG" style="width:272px;">Title</div>
								<div class="blkE" >Creator</div>
								<div class="blkI" style="width:110px;">Type</div>
                            </div>
						</div>
						<div id="mediaContent" style="width:685px; height:400px;overflow-x:hidden; overflow-y:auto;">
							
							<?php
								$all_artist_project = array();
								if($getproject !="" && $getproject!=Null)
								{
									while($showproject=mysql_fetch_assoc($getproject))
									{
										//$exp_creator = explode('(',$showproject['creator']);
										//$get_project_type = $editprofile->Get_Project_subtype($showproject['id']);
										//$showproject['project_type_name'] = $get_project_type['name'];
										$showproject['whos_project'] = "own_artist";
										$showproject['id'] = $showproject['id'] ."~art";
										$all_artist_project[] =  $showproject;
									?>
									<!--<div id="AccordionContainer" class="tableCont">
										<div class="blkG" style="width:200px;"><?php /*if($showproject['title']!=""){ echo $showproject['title'];}else{?>&nbsp;<?php }?></div>
										<div class="blkB" style="width:200px;"><a href=""><?php if($exp_creator[0]!=""){ echo $exp_creator[0];}else{?>&nbsp;<?php }?></a></div>
										<div class="blkB" style="width:100px;"><?php if($get_project_type['name']!=""){echo $get_project_type['name'];}else{?>&nbsp;<?php }?></div>
										<div class="blkD"><a href="/<?php echo $showproject['profile_url'];?>" onclick="return chk_profile_url('<?php echo $showproject['profile_url'];?>')" >Display</a></div>
										<div class="icon"><a href="add_artist_project.php?id=<?php echo $showproject['id'];?>"><img src="images/profile/edit.png" /></a></div>
										<div class="icon"><a onclick="return confirmdelete('<?php echo $showproject['title'];?>')" href="insertartistproject.php?delete=<?php echo $showproject['id'];*/?>"><img src="images/profile/delete.png" /></a></div>
									</div>	-->
									<?php
									}
								}
								$all_friends_project = array();
								$get_all_friends = $editprofile->Get_all_friends();
								if($get_all_friends !="" && $get_all_friends !=Null)
								{
									while($res_all_friends = mysql_fetch_assoc($get_all_friends))
									{
										$get_friend_art_info = $editprofile->get_friend_art_info($res_all_friends['fgeneral_user_id']);
										$get_all_tag_pro = $editprofile->get_all_tagged_pro($get_friend_art_info);
										if($get_all_tag_pro !="" && $get_all_tag_pro!= Null)
										{
											if(mysql_num_rows($get_all_tag_pro)>0)
											{
												while($res_projects = mysql_fetch_assoc($get_all_tag_pro))
												{
													//$get_project_type = $editprofile->Get_Project_subtype($res_projects['id']);
													$exp_creator = explode('(',$res_projects['creator']);
													$exp_tagged_email = explode(",",$res_projects['tagged_user_email']);
													for($exp_count = 0;$exp_count<count($exp_tagged_email);$exp_count++)
													{
														if($exp_tagged_email[$exp_count]==""){continue;}
														else{
															if($exp_tagged_email[$exp_count]==$_SESSION['login_email'])
															{
																//$res_projects['project_type_name'] = $get_project_type['name'];
																$res_projects['whos_project'] = "tag_pro_art";
																$res_projects['id'] = $res_projects['id'] ."~art";
																$all_friends_project[] = $res_projects;
															
																?>
																<!--<div id="AccordionContainer" class="tableCont">
																	<div class="blkG" style="width:200px;"><?php/* if($res_projects['title']!=""){echo $res_projects['title'];}else{?>&nbsp;<?php }?></div>
																	<div class="blkB" style="width:200px;"><a href=""><?php if($exp_creator[0]!=""){echo $exp_creator[0];}else{?>&nbsp;<?php }?></a></div>
																	<div class="blkB" style="width:100px;"><?php if($get_project_type['name']!=""){echo $get_project_type['name'];}else{?>&nbsp;<?php }?></div>
																	<div class="blkD">&nbsp;</div>
																	<div class="icon">&nbsp;</div>
																	<div class="icon"><a onclick="return confirmdelete('<?php echo $res_projects['title'];?>')" href="insertartistproject.php?delete=<?php echo $res_projects['id'];*/?>"><img src="images/profile/delete.png" /></a></div>
																</div>-->
																<?php
															}
														}
														
													}
												}
											}
										}
									}
								}
							
							/***Projects From Community Starts Here ***/
							$all_community_project = array();
							if($getproject_community !="" && $getproject_community!= Null)
							{
								if(mysql_num_rows($getproject_community)>0)
								{
									while($showproject=mysql_fetch_assoc($getproject_community))
									{
										$exp_creator= explode('(',$showproject['creator']);
										//$get_project_type=$newgeneral->Get_Project_subtype($showproject['id']);
										
										//$showproject['project_type_name'] = $get_project_type['name'];
										$showproject['whos_project'] = "own_community";
										$showproject['id'] = $showproject['id'] ."~com";
										$all_community_project[] = $showproject;
									?>
										<!--<div id="AccordionContainer" class="tableCont">
											<div class="blkG" style="width:200px;"><?php /*if($showproject['title']!=""){echo $showproject['title'];}else{?> &nbsp;<?php }?></div>
											<div class="blkB" style="width:200px;"><a href=""><?php if($exp_creator[0]!=""){echo $exp_creator[0];} else{?> &nbsp;<?php }?></a></div>
											<div class="blkB" style="width:100px"><?php if($get_project_type['name']!=""){echo $get_project_type['name'];}else{?> &nbsp;<?php }?></div>
											<div class="blkD"><a href="/<?php echo $showproject['profile_url'];?>" onclick="return chk_profile_url('<?php echo $showproject['profile_url'];?>')">Display</a></div>
											<div class="icon"><a href="add_community_project.php?id=<?php echo $showproject['id'];?>"><img src="images/profile/edit.png" /></a></div>
											<div class="icon"><a onclick="return confirmdelete('<?php echo $showproject['title']; ?>')" href="insertcommunityproject.php?delete=<?php echo $showproject['id'];*/?>"><img src="images/profile/delete.png" /></a></div>
										</div>-->
									<?php
									}
								}
							}
							$all_community_friends_project = array();
							$get_all_friends = $newgeneral->Get_all_friends();
							if($get_all_friends!="" && $get_all_friends!=Null)
							{
								while($res_all_friends = mysql_fetch_assoc($get_all_friends))
								{
									$get_friend_art_info = $newgeneral->get_friend_art_info($res_all_friends['fgeneral_user_id']);
									$get_all_tag_pro = $newgeneral->get_all_tagged_pro($get_friend_art_info);
									if($get_all_tag_pro !="" && $get_all_tag_pro!= Null)
									{
										if(mysql_num_rows($get_all_tag_pro)>0)
										{
											while($res_projects = mysql_fetch_assoc($get_all_tag_pro))
											{
												//$get_project_type = $newgeneral->Get_Project_subtype($res_projects['id']);
												$exp_creator = explode('(',$res_projects['creator']);
												$exp_tagged_email = explode(",",$res_projects['tagged_user_email']);
												for($exp_count = 0;$exp_count<count($exp_tagged_email);$exp_count++)
												{
													if($exp_tagged_email[$exp_count]==""){continue;}
													else{
														if($exp_tagged_email[$exp_count]==$_SESSION['login_email'])
														{
															//$res_projects['project_type_name'] = $get_project_type['name'];
															$res_projects['whos_project'] = "tag_pro_com";
															$res_projects['id'] = $res_projects['id'] ."~com";
															$all_community_friends_project[] = $res_projects;
															?>
															<!--<div id="AccordionContainer" class="tableCont">
																<div class="blkG" style="width:200px;"><?php /*if($res_projects['title']!=""){echo $res_projects['title'];}else{?>&nbsp;<?php }?></div>
																<div class="blkB" style="width:200px;"><a href=""><?php if($exp_creator[0]!=""){echo $exp_creator[0];}else{?>&nbsp;<?php }?></a></div>
																<div class="blkB" style="width:100px;"><?php if($get_project_type['name']!=""){echo $get_project_type['name'];}else{?>&nbsp;<?php }?></div>
																<div class="blkD">&nbsp;</div>
																<div class="icon">&nbsp;</div>
																<div class="icon"><a onclick="return confirmdelete('<?php echo $res_projects['title'];?>')" href="insertcommunityproject.php?delete=<?php echo $res_projects['id'];*/?>"><img src="images/profile/delete.png" /></a></div>
															</div>-->
															<?php
														}
													}
													
												}
											}
										}
									}
								}
							}
							
							$get_onlycre_id = array();
							$get_onlycre_id = $editprofile->only_creator_this();
							
							$get_only2cre = array();
							//$get_only2cre = $editprofile->only_creator2pr_this();
							$get_only2cre = $editprofile->only_creator2pr_othis();
							$sel_art2tagged = array();
							if($project_avail!=0)
							{
								$acc_project = array_unique($acc_project);
								for($acc_pro=0;$acc_pro<count($acc_project);$acc_pro++)
								{												
									if($acc_project[$acc_pro]!="" && $acc_project[$acc_pro]!=0)
									{													
										$sql_1_cre = $editprofile->get_project_tagged_by_other2_user($acc_project[$acc_pro]);
										if($sql_1_cre!="")
										{
											if($sql_1_cre !="" && $sql_1_cre!= Null)
											{
												if(mysql_num_rows($sql_1_cre)>0)
												{
													while($run = mysql_fetch_assoc($sql_1_cre))
													{
														if($newres1['artist_id']!=$run['artist_id'])
														{
															$run['whos_project'] = "art_creator_tag_pro";
															$run['id'] = $run['id'].'~'.'art';
															$sel_art2tagged[] = $run;
														}
													}
												}
											}
										}
										
										$sql_2_cre = $editprofile->get_project_ctagged_by_other2_user($acc_project[$acc_pro]);
										if($sql_2_cre!="")
										{
											if($sql_2_cre !="" && $sql_2_cre!= Null)
											{
												if(mysql_num_rows($sql_2_cre)>0)
												{
													while($run_c = mysql_fetch_assoc($sql_2_cre))
													{
														if($newres1['community_id']!=$run_c['community_id'])
														{
															$run_c['whos_project'] = "com_creator_tag_pro";
															$run_c['id'] = $run_c['id'].'~'.'com';
															$sel_art2tagged[] = $run_c;
														}
													}
												}
											}
										}
									}
								}
							}
							
							$sel_com2tagged = array();
							if($project_cavail!=0)
							{
								$acc_cproject = array_unique($acc_cproject);
								for($acc_pro=0;$acc_pro<count($acc_cproject);$acc_pro++)
								{
									if($acc_cproject[$acc_pro]!="" && $acc_cproject[$acc_pro]!=0)
									{
										$sql_1_cre = $editprofile->get_cproject_tagged_by_other2_user($acc_cproject[$acc_pro]);
										if($sql_1_cre!="")
										{
											if($sql_1_cre !="" && $sql_1_cre!= Null)
											{
												if(mysql_num_rows($sql_1_cre)>0)
												{
													while($run = mysql_fetch_assoc($sql_1_cre))
													{
														if($newres1['artist_id']!=$run['artist_id'])
														{
															$run['whos_project'] = "art_creator_tag_pro";
															$run['id'] = $run['id'].'~art';
															$sel_com2tagged[] = $run;
														}
													}
												}
											}
										}
										
										$sql_2_cre = $editprofile->get_cproject_ctagged_by_other2_user($acc_cproject[$acc_pro]);
										if($sql_2_cre!="")
										{
											if($sql_2_cre !="" && $sql_2_cre!= Null)
											{
												if(mysql_num_rows($sql_2_cre)>0)
												{
													while($run_c = mysql_fetch_assoc($sql_2_cre))
													{
														if($newres1['community_id']!=$run_c['community_id'])
														{
															$run_c['whos_project'] = "com_creator_tag_pro";
															$run_c['id'] = $run_c['id'].'~com';
															$sel_com2tagged[] = $run_c;
														}
													}
												}
											}
										}
									}
								}
							}
							
							$sel_arttagged = array();
							if($project_avail!=0)
							{
								$acc_project = array_unique($acc_project);
								for($acc_pro=0;$acc_pro<count($acc_project);$acc_pro++)
								{
									if($acc_project[$acc_pro]!="" && $acc_project[$acc_pro]!=0)
									{
										$dum_art = $editprofile->get_project_tagged_by_other_user($acc_project[$acc_pro]);
										if(!empty($dum_art) && $dum_art['id']!="" && $dum_art['id']!="")
										{
											if($newres1['artist_id']!=$dum_art['artist_id'])
											{
												$dum_art['id'] = $dum_art['id'].'~art';
												$dum_art['whos_project'] = "tag_pro_art";
												$sel_arttagged[] = $dum_art;
											}
										}
									}
								}
							}
							
							$sel_comtagged = array();
							if($project_cavail!=0)
							{
								$acc_cproject = array_unique($acc_cproject);
								for($acc_pro=0;$acc_pro<count($acc_cproject);$acc_pro++)
								{
									if($acc_cproject[$acc_pro]!="" && $acc_cproject[$acc_pro]!=0)
									{
										$dum_com = $editprofile->get_cproject_tagged_by_other_user($acc_cproject[$acc_pro]);
										if(!empty($dum_com) && $dum_com['id']!="" && $dum_com['id']!="")
										{
											if($newres1['community_id']!=$dum_com['community_id'])
											{
												$dum_com['id'] = $dum_com['id'].'~com';
												$dum_com['whos_project'] = "tag_pro_com";
												$sel_comtagged[] = $dum_com;
											}
										}
									}
								}
							}
							
							//var_dump($all_artist_project);
							//var_dump($all_friends_project);
							//var_dump($all_community_project);
							//var_dump($all_community_friends_project);
							//var_dump($get_onlycre_id);
							//var_dump($sel_comtagged);
							//var_dump($sel_arttagged);
							//var_dump($get_only2cre);
							//var_dump($sel_com2tagged);
							//var_dump($sel_art2tagged);
							$all_combine_projects = array_merge($all_artist_project,$all_friends_project,$all_community_project,$all_community_friends_project,$get_onlycre_id,$sel_comtagged,$sel_arttagged,$get_only2cre,$sel_com2tagged,$sel_art2tagged);
							
							foreach($all_combine_projects as $k=>$v)
							{										
								$end_c1 = $v['creator'];
								//$create_c = strpos($v['creator'],'(');
								// = substr($v['creator'],0,$create_c);
								$end_c = trim($end_c1);
								
								$sort1['creator'][$k] = strtolower($end_c);
								//$sort['from'][$k] = $end_f;
								//$sort['track'][$k] = $v['track'];
								$sort1['title'][$k] = strtolower($v['title']);
							}
							
							if(!empty($sort1))
							{
								array_multisort($sort1['creator'], SORT_ASC, $sort1['title'], SORT_ASC,$all_combine_projects);
							}
							//var_dump($all_combine_projects);
							/*$sort = array();
							foreach($all_combine_projects[0] as $k=>$v) {
							
								$create_c = strpos($v['creator'],'(');
								$end_c = substr($v['creator'],0,$create_c);
								
								$create_f = strpos($v['from'],'(');
								$end_f = substr($v['from'],0,$create_f);
								
								$sort['creator'][$k] = strtolower($end_c);
								
								$sort['from'][$k] = strtolower($end_f);
								if($v['track']!=NULL){								
									$sort['track'][$k] = $v['track'];
								}else{									
									$sort['track'][$k] = " ";
								}
								$sort['title'][$k] = strtolower($v['title']);
							}
							array_multisort($sort['creator'], SORT_ASC, $sort['from'], SORT_ASC, $sort['track'], SORT_NUMERIC ,SORT_ASC,$sort['title'], SORT_ASC,$combine_arr[0]);
							$dummy_pr = array();
							for($newtest_count = 0;$newtest_count<=count($combine_arr[$test_count]);$newtest_count++){
								if(in_array($combine_arr[$test_count][$newtest_count]['id'],$dummy_pr))
							    {
								
							    }
							    else
							    {
									$dummy_pr[] = $combine_arr[$test_count][$newtest_count]['id'];
							
							*/
							$first_index = 0;
							$dumpsa_pros = array();
							$dumpsc_pros = array();
							
							$get_all_del_id = $editprofile->get_deleted_project_id();
							//var_dump($get_all_del_id['deleted_project_id']);
							$exp_del_id = explode(",",$get_all_del_id['deleted_project_id']);
							for($count_all=0;$count_all<count($all_combine_projects);$count_all++){
								for($count_del=0;$count_del<count($exp_del_id);$count_del++){
									if($exp_del_id[$count_del]!=""){
										if($all_combine_projects[$count_all]['id'] == $exp_del_id[$count_del]){
											$all_combine_projects[$count_all] ="";
										}
									}
								}
							}
							
							for($count_all=0;$count_all<count($all_combine_projects);$count_all++)
							{
								if($all_combine_projects[$count_all] !="")
								{
									$chk_ac_ep = 0;
									if(isset($all_combine_projects[$count_all]['artist_id']))
									{
										$get_project_type = $editprofile->Get_Project_subtype($all_combine_projects[$count_all]['id']);
										if(in_array($all_combine_projects[$count_all]['id'],$dumpsa_pros))
										{
											$chk_ac_ep = 1;
										}
										else
										{
											$dumpsa_pros[] = $all_combine_projects[$count_all]['id'];
										}
									}
									elseif(isset($all_combine_projects[$count_all]['community_id']))
									{
										$get_project_type = $editprofile->Get_ACProject_subtype($all_combine_projects[$count_all]['id']);
										if(in_array($all_combine_projects[$count_all]['id'],$dumpsc_pros))
										{
											$chk_ac_ep = 1;
										}
										else
										{
											$dumpsc_pros[] = $all_combine_projects[$count_all]['id'];
										}
									}
									if($chk_ac_ep==1)
									{}
									else
									{
									
									$exp_creator = explode('(',$all_combine_projects[$count_all]['creator']);
									if($all_combine_projects[$count_all]['creators_info']!="")
									{
										$link_du = explode("|",$all_combine_projects[$count_all]['creators_info']);
										if($link_du[0]=='general_artist')
										{
											$sql_li = mysql_query("SELECT * FROM general_artist WHERE artist_id='".$link_du[1]."'");
										}
										elseif($link_du[0]=='general_community')
										{
											$sql_li = mysql_query("SELECT * FROM general_community WHERE community_id='".$link_du[1]."'");
										}
										else
										{
											$sql_li = mysql_query("SELECT * FROM $link_du[0] WHERE id='".$link_du[1]."'");
										}
										
										if(isset($sql_li))
										{
											if($sql_li !="" && $sql_li!= Null)
											{
												if(mysql_num_rows($sql_li)>0)
												{
													$ans_li = mysql_fetch_assoc($sql_li);
												}
											}
										}
									}
								?>
									<div id="AccordionContainer" class="tableCont">
										<div class="blkG" style="width:272px;"><?php if($all_combine_projects[$count_all]['title']!=""){ echo $all_combine_projects[$count_all]['title'];}else{?>&nbsp;<?php }?></div>
										<div class="blkE" ><a href=""><?php
										if($exp_creator[0]!="")
										{
											if(isset($ans_li)) 
											{
												if($ans_li['profile_url']!="")
												{
													echo "<a href=".$ans_li['profile_url'].">".$exp_creator[0]."</a>";
												}
												else
												{
													echo $exp_creator[0];
												}
											}
											else
											{
												echo $exp_creator[0];
											}
										}
										else{?>&nbsp;<?php }?></a></div>
										<div class="blkB" style="width:110px;"><?php if($get_project_type['name']!=""){echo $get_project_type['name'];}else{?>&nbsp;<?php }?></div>
										<div class="blkD" style="width:78px;"><a href="/<?php echo $all_combine_projects[$count_all]['profile_url'];?>" onclick="return chk_profile_url('<?php echo $all_combine_projects[$count_all]['profile_url'];?>')" >Display</a></div>
										<?php
										if($all_combine_projects[$count_all]['whos_project'] == "own_artist")
										{
											$exp_art_id = explode("~",$all_combine_projects[$count_all]['id']);
										?>
											<div class="icon"><a href="add_artist_project.php?id=<?php echo $exp_art_id[0];?>"><img src="images/profile/edit.png" /></a></div>
											<div class="icon"><a onclick="return confirmdelete('<?php echo $all_combine_projects[$count_all]['title'];?>')" href="insertartistproject.php?delete=<?php echo $exp_art_id[0];?>&whos_project=own_artist"><img src="images/profile/delete.png" /></a></div>
										<?php
										}else if($all_combine_projects[$count_all]['whos_project'] == "own_community")
										{
											$exp_com_id = explode("~",$all_combine_projects[$count_all]['id']);
										?>
											<div class="icon"><a href="add_community_project.php?id=<?php echo $exp_com_id[0];?>"><img src="images/profile/edit.png" /></a></div>
											<div class="icon"><a onclick="return confirmdelete('<?php echo $all_combine_projects[$count_all]['title']; ?>')" href="insertcommunityproject.php?delete=<?php echo $exp_com_id[0];?>&whos_project=own_community"><img src="images/profile/delete.png" /></a></div>
										<?php
										}else if($all_combine_projects[$count_all]['whos_project'] == "community_friends")
										{
										?>
											<div class="icon">&nbsp;</div>
											<div class="icon"><a onclick="return confirmdelete('<?php echo $all_combine_projects[$count_all]['title'];?>')" href="insertcommunityproject.php?delete=<?php echo $all_combine_projects[$count_all]['id'];?>&whos_project=community_friends"><img src="images/profile/delete.png" /></a></div>
										<?php
										}else if($all_combine_projects[$count_all]['whos_project'] == "artist_friends")
										{
										?>
											<div class="icon">&nbsp;</div>
											<div class="icon"><a onclick="return confirmdelete('<?php echo $all_combine_projects[$count_all]['title'];?>')" href="insertartistproject.php?delete=<?php echo $all_combine_projects[$count_all]['id'];?>&whos_project=artist_friends"><img src="images/profile/delete.png" /></a></div>
										<?php
										}else if($all_combine_projects[$count_all]['whos_project'] == "only_creator")
										{
											$exp_id = explode("~",$all_combine_projects[$count_all]['id']);
											if($exp_id[1]=="art"){
											?>
												<div class="icon">&nbsp;</div>
												<div class="icon"><a onclick="return confirmdelete('<?php echo $all_combine_projects[$count_all]['title'];?>')" href="insertartistproject.php?delete=<?php echo $all_combine_projects[$count_all]['id'];?>&whos_project=only_creator"><img src="images/profile/delete.png" /></a></div>
											<?php
											}else if($exp_id[1]=="com"){
										?>
											<div class="icon">&nbsp;</div>
											<div class="icon"><a onclick="return confirmdelete('<?php echo $all_combine_projects[$count_all]['title'];?>')" href="insertcommunityproject.php?delete=<?php echo $all_combine_projects[$count_all]['id'];?>&whos_project=only_creator"><img src="images/profile/delete.png" /></a></div>
										<?php
											}
										}else if($all_combine_projects[$count_all]['whos_project'] == "tag_pro_art")
										{
										?>
											<div class="icon">&nbsp;</div>
											<div class="icon"><a onclick="return confirmdelete('<?php echo $all_combine_projects[$count_all]['title'];?>')" href="insertartistproject.php?delete=<?php echo $all_combine_projects[$count_all]['id'];?>&whos_project=tag_pro_art"><img src="images/profile/delete.png" /></a></div>
										<?php
										}else if($all_combine_projects[$count_all]['whos_project'] == "tag_pro_com")
										{
										?>
											<div class="icon">&nbsp;</div>
											<div class="icon"><a onclick="return confirmdelete('<?php echo $all_combine_projects[$count_all]['title'];?>')" href="insertcommunityproject.php?delete=<?php echo $all_combine_projects[$count_all]['id'];?>&whos_project=tag_pro_com"><img src="images/profile/delete.png" /></a></div>
										<?php
										}else if($all_combine_projects[$count_all]['whos_project'] == "art_creator_tag_pro" || $all_combine_projects[$count_all]['whos_project'] == "com_creator_tag_pro" || $all_combine_projects[$count_all]['whos_project'] == "art_creator_creator_pro" || $all_combine_projects[$count_all]['whos_project'] == "com_creator_creator_pro")
										{
										?>
											<div class="icon">&nbsp;</div>
											<div class="icon"><a onclick="return confirmdelete('<?php echo $all_combine_projects[$count_all]['title'];?>')" href="insertartistproject.php?delete=<?php echo $all_combine_projects[$count_all]['id'];?>&whos_project=<?php echo $all_combine_projects[$count_all]['whos_project']; ?>"><img src="images/profile/delete.png" /></a></div>
										<?php
										}
										?>
									</div>
								<?php
									}
								}
							}
							
							
							
							/***Projects From Community Ends Here ***/
							?>
								
                        </div>
                    <!--</div>-->
					
				</div>
            </div>
			
			<?php
			
				$share_promote = new Promotions();
				$chk_del_gen = $share_promote->check_avail_general($_SESSION['login_email']);
				if($chk_del_gen=='1')
				{
					$chk_art_del = $share_promote->check_avail_artist($newres1['artist_id']);
					if($chk_art_del=='1')
					{
			?>
						<div class="subTabs" id="promotions" style="display:none;">
						<input type="hidden" id="chk_vals_ups"/>
							<h1><?php echo $newrow['name']; ?> Promotions</h1>
							<div id="mediaContent" style="width:685px;">
								<div class="topLinks">
									<div class="links" style="width:665px;">
										<select class="eventdrop" id="share_facebook" onChange="handleSelection(value)"  style="float:right;">
											<option value="all_profiles_share">All Profiles</option>
											<option value="projects_profiles_share">Project Profiles</option>
											<option value="events_profiles_share">Event Profiles</option>
										</select>
									</div>
								</div>
								<div class="titleCont" style="width:680px;">
									<div class="blkA" style="width:150px;">Type</div>
									<div class="blkH" style="width:398px;">Profile Title</div>
									
								</div>
							</div>
							<div id="mediaContent" style="width:685px; height:400px;overflow-x:hidden; overflow-y:auto; margin-bottom: 20px;">
							<input type="hidden" id="post_face"/>
							
								
								<div id="all_profiles_share">
								<?php
									$all_profiles_art = array();
									$all_profiles_art = $share_promote->all_profiles_share_artist($newres1['artist_id']);
									
									$all_profiles_com = array();
									$all_profiles_com = $share_promote->all_profiles_share_community($newres1['community_id']);
									
									$get_eveup_id = array();
									if($get_sh_event!="" && $get_sh_event!=Null)
									{
										while($get_eveup_ids = mysql_fetch_assoc($get_sh_event))
										{
											$get_eveup_ids['table_name'] = 'artist_event';
											$get_eveup_ids['type'] = $share_promote->get_substypes($get_eveup_ids['id'],$get_eveup_ids['table_name']);
											$get_eveup_id[] = $get_eveup_ids;
										}
									}
									
									$get_usqp_id = array();
									if($get_sh_ceevent!="" && $get_sh_ceevent!=Null)
									{
										while($get_xeveup_ids = mysql_fetch_assoc($get_sh_ceevent))
										{
											$get_xeveup_ids['table_name'] = 'community_event';
											$get_xeveup_ids['type'] = $share_promote->get_substypes($get_xeveup_ids['id'],$get_xeveup_ids['table_name']);
											$get_usqp_id[] = $get_xeveup_ids;
										}
									}
									
									$get_onlycre_ev = array();
									$get_onlycre_ev = $editprofile->only_creator_sh_upevent();
									
									$get_only2cre_ev = array();
									$get_only2cre_ev = $editprofile->only_creator2ev_sh_upevent();
										
									$selarts_tagged = array();
									if($event_avail!=0)
									{
										$acc_event = array_unique($acc_event);
										for($i=0;$i<count($acc_event);$i++)
										{
												//$sel_tagged="";
											if($acc_event[$i]!="" && $acc_event[$i]!=0)
											{
												$dumartr = $editprofile->get_event_tagged_by_other_user($acc_event[$i]);
												if(!empty($dumartr) && $dumartr['id']!="" && $dumartr['id']!="")
												{
													$dumartr['table_name'] = 'artist_event';
													$dumartr['type'] = $share_promote->get_substypes($dumartr['id'],$dumartr['table_name']);
													$selarts_tagged[] = $dumartr;
												}												
											}
										}
									}	 
										
									$selcoms_tagged = array();
									if($event_cavail!=0)
									{
										$acc_cevent = array_unique($acc_cevent);
										for($t_i=0;$t_i<count($acc_cevent);$t_i++)
										{
												//$sel_tagged="";
											if($acc_cevent[$t_i]!="" && $acc_cevent[$t_i]!=0)
											{
												$dumacomr = $editprofile->get_cevent_tagged_by_other_user($acc_cevent[$t_i]);
												if(!empty($dumacomr) && $dumacomr['id']!="" && $dumacomr['id']!="")
												{
													$dumacomr['table_name'] = 'community_event';
													$dumacomr['type'] = $share_promote->get_substypes($dumacomr['id'],$dumacomr['table_name']);
													$selcoms_tagged[] = $dumacomr;
												}												
											}
										}
									} 
										
									$selarts2_tagged = array();
									if($project_avail!=0)
									{
										$acc_project = array_unique($acc_project);
										for($acc_pro=0;$acc_pro<count($acc_project);$acc_pro++)
										{												
											if($acc_project[$acc_pro]!="" && $acc_project[$acc_pro]!=0)
											{													
												$sql_1_cre = $editprofile->get_event_tagged_by_other2_user($acc_project[$acc_pro]);
												if($sql_1_cre!="")
												{
													if($sql_1_cre !="" && $sql_1_cre!= Null)
													{
														if(mysql_num_rows($sql_1_cre)>0)
														{
															while($run = mysql_fetch_assoc($sql_1_cre))
															{
																$run['table_name'] = 'artist_event';
																$run['type'] = $share_promote->get_substypes($run['id'],$run['table_name']);
																//$ans_a_als[] = $run;
																$selarts2_tagged[] = $run;
															}
														}
													}
												}
												
												$sql_2_cre = $editprofile->get_event_ctagged_by_other2_user($acc_project[$acc_pro]);
												if($sql_2_cre !="" && $sql_2_cre!= Null)
												{
													if(mysql_num_rows($sql_2_cre)>0)
													{
														while($run_c = mysql_fetch_assoc($sql_2_cre))
														{
															$run_c['table_name'] = 'community_event';
															$run_c['type'] = $share_promote->get_substypes($run_c['id'],$run_c['table_name']);
															//$ans_c_als[]
															$selarts2_tagged[] = $run_c;
														}
													}
												}
											}
										}
									}
									
									$selcoms2_tagged = array();
									if($project_cavail!=0)
									{
										$acc_cproject = array_unique($acc_cproject);
										for($acc_pro=0;$acc_pro<count($acc_cproject);$acc_pro++)
										{												
											if($acc_cproject[$acc_pro]!="" && $acc_cproject[$acc_pro]!=0)
											{													
												$sql_1_cre = $editprofile->get_cevent_tagged_by_other2_user($acc_cproject[$acc_pro]);
												if($sql_1_cre!="")
												{
													if($sql_1_cre !="" && $sql_1_cre!= Null)
													{
														if(mysql_num_rows($sql_1_cre)>0)
														{
															while($run = mysql_fetch_assoc($sql_1_cre))
															{
																$run['table_name'] = 'artist_event';
																$run['type'] = $share_promote->get_substypes($run['id'],$run['table_name']);
																//$ans_a_als[] = $run;
																$selcoms2_tagged[] = $run;
															}
														}
													}
												}
												
												$sql_2_cre = $editprofile->get_cevent_ctagged_by_other2_user($acc_cproject[$acc_pro]);
												if($sql_2_cre !="" && $sql_2_cre!= Null)
												{
													if(mysql_num_rows($sql_2_cre)>0)
													{
														while($run_c = mysql_fetch_assoc($sql_2_cre))
														{
															$run_c['table_name'] = 'community_event';
															$run_c['type'] = $share_promote->get_substypes($run_c['id'],$run_c['table_name']);
															//$ans_c_als[]
															$selcoms2_tagged[] = $run_c;
														}
													}
												}
											}
										}
									}
									
									$get_channel_id = array();
									if($get_sh_eventrec!="" && $get_sh_eventrec!=Null)
									{
										while($get_channel_ids = mysql_fetch_assoc($get_sh_eventrec))
										{
											$get_channel_ids['table_name'] = 'artist_event';
											$get_channel_ids['type'] = $share_promote->get_substypes($get_channel_ids['id'],$get_channel_ids['table_name']);
											$get_channel_id[] = $get_channel_ids; 
										}
									}
									
									$get_cchannel_id = array();
									if($get_sh_ceeventrec!="" && $get_sh_ceeventrec!=Null)
									{
										while($get_rchannel_ids = mysql_fetch_assoc($get_sh_ceeventrec))
										{
											$get_rchannel_ids['table_name'] = 'community_event';
											$get_rchannel_ids['type'] = $share_promote->get_substypes($get_rchannel_ids['id'],$get_rchannel_ids['table_name']);
											$get_cchannel_id[] = $get_rchannel_ids; 
										}
									}
										
									$get_onlycre_rev = array();
									$get_onlycre_rev = $editprofile->only_sh_creator_recevent();
									
									$get_only2cre_rev = array();
									$get_only2cre_rev = $editprofile->only_sh_creator2_recevent();
										
									$sel_artstagged = array();
									if($event_avail!=0)
									{
										$acc_event = array_unique($acc_event);
										for($t=0;$t<count($acc_event);$t++)
										{
												//$sel_tagged="";
											if($acc_event[$t]!="" && $acc_event[$t]!=0)
											{
												$dum_arte = $editprofile->get_event_tagged_by_other_userrec($acc_event[$t]);
												if(!empty($dum_arte) && $dum_arte['id']!="" && $dum_arte['id']!="")
												{
													$dum_arte['table_name'] = 'artist_event';
													$dum_arte['type'] = $share_promote->get_substypes($dum_arte['id'],$dum_arte['table_name']);
													$sel_artstagged[] = $dum_arte;
												}
											}
										}
									}
										
									$selr_comstagged = array();
									if($event_cavail!=0)
									{
										$acc_cevent = array_unique($acc_cevent);
										for($t=0;$t<count($acc_cevent);$t++)
										{
												//$sel_tagged="";
											if($acc_cevent[$t]!="" && $acc_cevent[$t]!=0)
											{
												$dum_artec = $editprofile->get_cevent_tagged_by_other_userrec($acc_cevent[$t]);
												if(!empty($dum_artec) && $dum_artec['id']!="" && $dum_artec['id']!="")
												{
													$dum_artec['table_name'] = 'community_event';
													$dum_artec['type'] = $share_promote->get_substypes($dum_artec['id'],$dum_artec['table_name']);
													$selr_comstagged[] = $dum_artec;
												}
											}
										}
									}
										
									$selarts2r_tagged = array();
									if($project_avail!=0)
									{
										$acc_project = array_unique($acc_project);
										for($acc_pro=0;$acc_pro<count($acc_project);$acc_pro++)
										{												
											if($acc_project[$acc_pro]!="" && $acc_project[$acc_pro]!=0)
											{													
												$sql_1_cre = $editprofile->get_event_tagged_by_other2_userrec($acc_project[$acc_pro]);
												if($sql_1_cre!="")
												{
													if($sql_1_cre !="" && $sql_1_cre!= Null)
													{
														if(mysql_num_rows($sql_1_cre)>0)
														{
															while($run = mysql_fetch_assoc($sql_1_cre))
															{
																$run['table_name'] = 'artist_event';
																$run['type'] = $share_promote->get_substypes($run['id'],$run['table_name']);
																//$ans_a_als[] = $run;
																$selarts2r_tagged[] = $run;
															}
														}
													}
												}
												
												$sql_2_cre = $editprofile->get_event_ctagged_by_other2_userrec($acc_project[$acc_pro]);
												if($sql_2_cre !="" && $sql_2_cre!= Null)
												{
													if(mysql_num_rows($sql_2_cre)>0)
													{
														while($run_c = mysql_fetch_assoc($sql_2_cre))
														{
															$run_c['table_name'] = 'community_event';
															$run_c['type'] = $share_promote->get_substypes($run_c['id'],$run_c['table_name']);
															//$ans_c_als[]
															$selarts2r_tagged[] = $run_c;
														}
													}
												}
											}
										}
									}
										
									$selcoms2r_tagged = array();
									if($project_cavail!=0)
									{
										$acc_cproject = array_unique($acc_cproject);
										for($acc_pro=0;$acc_pro<count($acc_cproject);$acc_pro++)
										{												
											if($acc_cproject[$acc_pro]!="" && $acc_cproject[$acc_pro]!=0)
											{													
												$sql_1_cre = $editprofile->get_cevent_tagged_by_other2_userrec($acc_cproject[$acc_pro]);
												if($sql_1_cre!="")
												{
													if($sql_1_cre !="" && $sql_1_cre!= Null)
													{
														if(mysql_num_rows($sql_1_cre)>0)
														{
															while($run = mysql_fetch_assoc($sql_1_cre))
															{
																$run['table_name'] = 'artist_event';
																$run['type'] = $share_promote->get_substypes($run['id'],$run['table_name']);
																//$ans_a_als[] = $run;
																$selcoms2r_tagged[] = $run;
															}
														}
													}
												}
												
												$sql_2_cre = $editprofile->get_cevent_ctagged_by_other2_userrec($acc_cproject[$acc_pro]);
												if($sql_2_cre !="" && $sql_2_cre!= Null)
												{
													if(mysql_num_rows($sql_2_cre)>0)
													{
														while($run_c = mysql_fetch_assoc($sql_2_cre))
														{
															$run_c['table_name'] = 'community_event';
															$run_c['type'] = $share_promote->get_substypes($run_c['id'],$run_c['table_name']);
															//$ans_c_als[]
															$selcoms2r_tagged[] = $run_c;
														}
													}
												}
											}
										}
									}
									
									$get_onlycre_id = array();
									$get_onlycre_id = $editprofile->only_sh_creator_this();
									
									$get_only2cre = array();
									$get_only2cre = $editprofile->only_sh_creator2pr_this();
									
									//var_dump($get_onlycre_id);
									$get_proa_id = array();
									if($get_sh_project!="" && $get_sh_project!=Null)
									{
										while($get_proa_ids = mysql_fetch_assoc($get_sh_project))
										{
											$get_proa_ids['table_name'] = 'artist_project';
											$get_proa_ids['type'] = $share_promote->get_substypes($get_proa_ids['id'],$get_proa_ids['table_name']);
											$get_proa_id[] = $get_proa_ids;
										}
									}
										
									$get_proc_id = array();
									if($get_sh_cproject!="" && $get_sh_cproject!=Null)
									{
										while($get_proc_ids = mysql_fetch_assoc($get_sh_cproject))
										{
											$get_proc_ids['table_name'] = 'community_project';
											$get_proc_ids['type'] = $share_promote->get_substypes($get_proc_ids['id'],$get_proc_ids['table_name']);
											$get_proc_id[] = $get_proc_ids;
										}
									}
										
									$sel_arttagged = array();
									if($project_avail!=0)
									{
										$acc_project = array_unique($acc_project);
										for($acc_pro=0;$acc_pro<count($acc_project);$acc_pro++)
										{
											if($acc_project[$acc_pro]!="" && $acc_project[$acc_pro]!=0)
											{
												$dum_art = $editprofile->get_project_tagged_by_other_user($acc_project[$acc_pro]);
												if(!empty($dum_art) && $dum_art['id']!="" && $dum_art['id']!="")
												{
													$dum_art['table_name'] = 'artist_project';
													$dum_art['type'] = $share_promote->get_substypes($dum_art['id'],$dum_art['table_name']);
													$sel_arttagged[] = $dum_art;
												}
											}
										}
									}
									
									$sel_art2tagged = array();
									if($project_avail!=0)
									{
										$acc_project = array_unique($acc_project);
										for($acc_pro=0;$acc_pro<count($acc_project);$acc_pro++)
										{												
											if($acc_project[$acc_pro]!="" && $acc_project[$acc_pro]!=0)
											{													
												$sql_1_cre = $editprofile->get_project_tagged_by_other2_user($acc_project[$acc_pro]);
												if($sql_1_cre!="")
												{
													if($sql_1_cre !="" && $sql_1_cre!= Null)
													{
														if(mysql_num_rows($sql_1_cre)>0)
														{
															while($run = mysql_fetch_assoc($sql_1_cre))
															{
																$run['table_name'] = 'artist_project';
																$run['type'] = $share_promote->get_substypes($run['id'],$run['table_name']);
																$sel_art2tagged[] = $run;
															}
														}
													}
												}
												
												$sql_2_cre = $editprofile->get_project_ctagged_by_other2_user($acc_project[$acc_pro]);
												if($sql_2_cre!="")
												{
													if($sql_2_cre !="" && $sql_2_cre!= Null)
													{
														if(mysql_num_rows($sql_2_cre)>0)
														{
															while($run_c = mysql_fetch_assoc($sql_2_cre))
															{
																$run_c['table_name'] = 'community_project';
																$run_c['type'] = $share_promote->get_substypes($run_c['id'],$run_c['table_name']);
																$sel_art2tagged[] = $run_c;
															}
														}
													}
												}
											}
										}
									}
										
									$sel_comtagged = array();
									if($project_cavail!=0)
									{
										$acc_cproject = array_unique($acc_cproject);
										for($acc_pro=0;$acc_pro<count($acc_cproject);$acc_pro++)
										{
											if($acc_cproject[$acc_pro]!="" && $acc_cproject[$acc_pro]!=0)
											{
												$dum_com = $editprofile->get_cproject_tagged_by_other_user($acc_cproject[$acc_pro]);
												if(!empty($dum_com) && $dum_com['id']!="" && $dum_com['id']!="")
												{
													$dum_com['table_name'] = 'community_project';
													$dum_com['type'] = $share_promote->get_substypes($dum_com['id'],$dum_com['table_name']);
													$sel_comtagged[] = $dum_com;
												}
											}
										}
									}
										
									$sel_com2tagged = array();
									if($project_cavail!=0)
									{
										$acc_cproject = array_unique($acc_cproject);
										for($acc_pro=0;$acc_pro<count($acc_cproject);$acc_pro++)
										{
											if($acc_cproject[$acc_pro]!="" && $acc_cproject[$acc_pro]!=0)
											{
												$sql_1_cre = $editprofile->get_cproject_tagged_by_other2_user($acc_cproject[$acc_pro]);
												if($sql_1_cre!="")
												{
													if($sql_1_cre !="" && $sql_1_cre!= Null)
													{
														if(mysql_num_rows($sql_1_cre)>0)
														{
															while($run = mysql_fetch_assoc($sql_1_cre))
															{
																$run['table_name'] = 'artist_project';
																$run['type'] = $share_promote->get_substypes($run['id'],$run['table_name']);
																$sel_com2tagged[] = $run;
															}
														}
													}
												}
												
												$sql_2_cre = $editprofile->get_cproject_ctagged_by_other2_user($acc_cproject[$acc_pro]);
												if($sql_2_cre!="")
												{
													if($sql_2_cre !="" && $sql_2_cre!= Null)
													{
														if(mysql_num_rows($sql_2_cre)>0)
														{
															while($run_c = mysql_fetch_assoc($sql_2_cre))
															{
																$run_c['table_name'] = 'community_project';
																$run_c['type'] = $share_promote->get_substypes($run_c['id'],$run_c['table_name']);
																$sel_com2tagged[] = $run_c;
															}
														}
													}
												}
											}
										}
									}
								
									//$all_profiles = $share_promote->all_profiles_share_artist($newres1['artist_id']);
									
									$all_profiles = array_merge($all_profiles_art,$all_profiles_com,$get_eveup_id,$get_usqp_id,$get_onlycre_ev,$get_only2cre_ev,$selarts_tagged,$selcoms_tagged,$selarts2_tagged,$selcoms2_tagged,$selcoms2r_tagged,$selarts2r_tagged,$selr_comstagged,$get_only2cre_rev,$sel_artstagged,$get_onlycre_rev,$get_cchannel_id,$get_channel_id,$get_onlycre_id,$get_only2cre,$get_proa_id,$get_proc_id,$sel_arttagged,$sel_art2tagged,$sel_comtagged,$sel_com2tagged);
									
									$sort = array();
									foreach($all_profiles as $k=>$v)
									{
										$sort['type'][$k] = strtolower($v['type']);
										//$sort['from'][$k] = $end_f;
										//$sort['track'][$k] = $v['track'];
										$sort['title'][$k] = strtolower($v['title']);
									}
									//var_dump($sort);
									if(!empty($sort))
									{
										array_multisort($sort['type'], SORT_ASC, $sort['title'], SORT_ASC,$all_profiles);
									}
									
									$get_all_del_id = $editprofile->get_deleted_project_id();
									$exp_del_id = explode(",",$get_all_del_id['deleted_event_id']);
									for($count_all=0;$count_all<count($all_profiles);$count_all++)
									{
										if($all_profiles[$count_all]['table_name']=='artist_event')
										{
											$all_profiles[$count_all]['id'] = $all_profiles[$count_all]['id'].'~art';
										}
										elseif($all_profiles[$count_all]['table_name']=='community_event')
										{
											$all_profiles[$count_all]['id'] = $all_profiles[$count_all]['id'].'~com';
										}
										for($count_del=0;$count_del<count($exp_del_id);$count_del++)
										{
											if($exp_del_id[$count_del]!="")
											{
												if($all_profiles[$count_all]['id'] == $exp_del_id[$count_del])
												{
													$all_profiles[$count_all] ="";
												}												
											}
										}
									}
									
									$get_all_del_id = $editprofile->get_deleted_project_id();
									$exp_del_id = explode(",",$get_all_del_id['deleted_project_id']);
									//var_dump($exp_del_id);
									for($count_all=0;$count_all<count($all_profiles);$count_all++)
									{
										if($all_profiles[$count_all]['table_name']=='artist_project')
										{
											$all_profiles[$count_all]['id'] = $all_profiles[$count_all]['id'].'~art';
										}
										elseif($all_profiles[$count_all]['table_name']=='community_project')
										{
											$all_profiles[$count_all]['id'] = $all_profiles[$count_all]['id'].'~com';
										}
										for($count_del=0;$count_del<count($exp_del_id);$count_del++)
										{
											if($exp_del_id[$count_del]!="")
											{	
												if($all_profiles[$count_all]['id'] == $exp_del_id[$count_del])
												{
													$all_profiles[$count_all] ="";
												}
											}
										}
									}
									
										
									$dums_sh_aev = array();
									$dums_sh_cev = array();
									$dums_sh_apr = array();
									$dums_sh_cpr = array();
									
									if($all_profiles!="" && count($all_profiles)>0)
									{
										$ups = 0;
										
										for($all_s_p=0;$all_s_p<count($all_profiles);$all_s_p++)
										{
											$chk_vals = 0;
											if($all_profiles[$all_s_p]['table_name']=='artist_event')
											{
												
												if(in_array($all_profiles[$all_s_p]['id'],$dums_sh_aev))
												{
													$chk_vals = 1;
												}
												else
												{
													$dums_sh_aev[] = $all_profiles[$all_s_p]['id'];
												}
											}
											
											if($all_profiles[$all_s_p]['table_name']=='community_event')
											{
												
												if(in_array($all_profiles[$all_s_p]['id'],$dums_sh_cev))
												{
													$chk_vals = 1;
												}
												else
												{
													$dums_sh_cev[] = $all_profiles[$all_s_p]['id'];
												}
											}
											
											if($all_profiles[$all_s_p]['table_name']=='artist_project')
											{
												if(in_array($all_profiles[$all_s_p]['id'],$dums_sh_apr))
												{
													$chk_vals = 1;
												}
												else
												{
													$dums_sh_apr[] = $all_profiles[$all_s_p]['id'];
												}
											}
											
											if($all_profiles[$all_s_p]['table_name']=='community_project')
											{
												
												if(in_array($all_profiles[$all_s_p]['id'],$dums_sh_cpr))
												{
													$chk_vals = 1;
												}
												else
												{
													$dums_sh_cpr[] = $all_profiles[$all_s_p]['id'];
												}
											}
											
										if($chk_vals==0 && ($all_profiles[$all_s_p]['name']!="" || $all_profiles[$all_s_p]['title']!=""))
										{
								?>
										<div class="tableCont" style="width:680px; overflow:auto;">
											<div class="blkA" style="width:150px;"><?php
											if($all_profiles[$all_s_p]['type']!="")
												{
													echo $all_profiles[$all_s_p]['type'];
												}
												else
												{
													?>&nbsp;<?php
												}
											if($all_profiles[$all_s_p]['table_name']=='general_artist')
											{
												$spcl = $all_profiles[$all_s_p]['table_name'].'|'.$all_profiles[$all_s_p]['artist_id'];
											}
											elseif($all_profiles[$all_s_p]['table_name']=='general_community')
											{
												$spcl = $all_profiles[$all_s_p]['table_name'].'|'.$all_profiles[$all_s_p]['community_id'];
											}
											elseif($all_profiles[$all_s_p]['table_name']=='artist_event')
											{
												$spcl = $all_profiles[$all_s_p]['table_name'].'|'.$all_profiles[$all_s_p]['id'];
											}
											elseif($all_profiles[$all_s_p]['table_name']=='community_event')
											{
												$spcl = $all_profiles[$all_s_p]['table_name'].'|'.$all_profiles[$all_s_p]['id'];
											}
											elseif($all_profiles[$all_s_p]['table_name']=='artist_project')
											{
												$spcl = $all_profiles[$all_s_p]['table_name'].'|'.$all_profiles[$all_s_p]['id'];
											}
											elseif($all_profiles[$all_s_p]['table_name']=='community_project')
											{
												$spcl = $all_profiles[$all_s_p]['table_name'].'|'.$all_profiles[$all_s_p]['id'];
											}
											?></div>
											<div class="blkH" style="width:398px;"><?php 
											if($all_profiles[$all_s_p]['name']!="") 
											{
												echo $all_profiles[$all_s_p]['name'];
											}
											elseif($all_profiles[$all_s_p]['title']!="")
											{
												if($all_profiles[$all_s_p]['table_name']=='artist_event' || $all_profiles[$all_s_p]['table_name']=='community_event')
												{
													echo date("m.d.y", strtotime($all_profiles[$all_s_p]['date'])).' '.$all_profiles[$all_s_p]['title'];
												}
												else
												{
													echo $all_profiles[$all_s_p]['title'];
												}
											}
											?></div>
											<!--<div class="blkGF" style="padding-top:5px;">-->
											<div>
												<span style="display:none;" id="faces_<?php echo $ups; ?>" st_url="<?php echo $all_profiles[$all_s_p]['profile_url']; ?>" class='st_facebook_hcount' displayText='Facebook'></span>
											<!--	<span class='st_email_hcount' displayText='Email'></span>-->
											</div>
											<div class="blkC" style="margin-top:1px;">
												<a title="Send updates on this profile using email newsletters, Purify Art news feeds, and/or Facebook wall posts." onClick="blank_val()" href="Update_facebook.php?spcl=<?php echo $spcl.'|'.$ups; ?>&url_mtch_ups=<?php echo $all_profiles[$all_s_p]['profile_url']; ?>&title_for_socials=<?php if($all_profiles[$all_s_p]['name']!="") 
											{
												echo urlencode($all_profiles[$all_s_p]['name']);
											}else
												{
													echo urlencode($all_profiles[$all_s_p]['title']);
												}?>" id="update_facebook_<?php echo $ups; ?>" class="fancybox fancybox.ajax"><input type="button" style="border-radius:5px; -webkit-border-radius:5px; -moz-border-radius:5px; -ms-border-radius:5px; -o-border-radius:5px; background-color: #000000; border: 0 none; color: #FFFFFF; float: left; font-size: 12px; font-weight: 700;     margin-right: 5px; padding: 8px 15px; text-decoration: none;" value="Send Update" /></a>
											</div>
											<script>
												$(document).ready(function() {
													$("#update_facebook_<?php echo $ups; ?>").fancybox({
													helpers : {
														media : {},
														buttons : {},
														title : null
													},
													afterClose: function() { 
															var face_po = document.getElementById("post_face").value; 
															if(face_po!="no" && face_po!="") 
															{ 
																var pot = face_po.split('|');
																
																$('#faces_'+pot[1]+' span.stButton').click(); 
															}
															var chk_vals_ups = document.getElementById("chk_vals_ups").value;
															
															if(chk_vals_ups=='1')
															{
																alert("Your updates have been sent.");
															}
															setTimeout(function(){
																location.reload();
															},2000);
															
														}
													});
													
													/*$("#update_facebook_<?php echo $ups; ?>").fancybox({
													'width'				: '75%',
													'height'			: '100%',
													'transitionIn'		: 'none',
													'transitionOut'		: 'none',
													'type'				: 'iframe',
													'onClosed'			: function() { 
															var face_po = document.getElementById("post_face").value; 
															if(face_po!="no" && face_po!="") 
															{ 
																var pot = face_po.split('|');
																
																$('#faces_'+pot[1]+' span.stButton').click(); 
															}
															var chk_vals_ups = document.getElementById("chk_vals_ups").value;
															
															if(chk_vals_ups=='1')
															{
																alert("Your updates have been sent.");
															}
														}
													});*/
												});
												
												function blank_val()
												{
													document.getElementById("post_face").value = ""; 
												}
											</script>
								<?php
											$ups = $ups + 1;
										?>
										</div>
										<?php
											}
										}
									}
								?>
								</div>
								
								<div id="projects_profiles_share" style="display:none;">
								<?php
									$pro_adumps = array();
									$pro_cdumps = array();
									
									if($all_profiles!="" && count($all_profiles)>0)
									{
										$up_sp = 0;
										for($all_s_p=0;$all_s_p<count($all_profiles);$all_s_p++)
										{
											$pors_chks = 0;
											
											if($all_profiles[$all_s_p]['table_name']=='artist_project')
											{
												if(in_array($all_profiles[$all_s_p]['id'],$pro_adumps))
												{
													$pors_chks = 1;
												}
												else
												{
													$pro_adumps[] = $all_profiles[$all_s_p]['id'];
												}
											}
											elseif($all_profiles[$all_s_p]['table_name']=='community_project')
											{
												if(in_array($all_profiles[$all_s_p]['id'],$pro_cdumps))
												{
													$pors_chks = 1;
												}
												else
												{
													$pro_cdumps[] = $all_profiles[$all_s_p]['id'];
												}
											}
											
											if($pors_chks==0)
											{
											if($all_profiles[$all_s_p]['table_name']=='community_project' || $all_profiles[$all_s_p]['table_name']=='artist_project')
											{
										?>
										<div class="tableCont">
										<?php
											if($all_profiles[$all_s_p]['table_name']=='community_project' || $all_profiles[$all_s_p]['table_name']=='artist_project')
											{	
											?>
												<div class="blkA" style="width:150px;">
											<?php
													if($all_profiles[$all_s_p]['type']!="")
													{
														echo $all_profiles[$all_s_p]['type'];
													}
													else
													{
														?>&nbsp;<?php
													}
											?>
												</div>
											<?php
											}
											
											if($all_profiles[$all_s_p]['table_name']=='community_project' || $all_profiles[$all_s_p]['table_name']=='artist_project')
											{
											?>
												<div class="blkH" style="width:398px;">
											<?php
													echo $all_profiles[$all_s_p]['title'];
											?>
												</div>
											<?php
											}
											
											$spcla = $all_profiles[$all_s_p]['table_name'].'|'.$all_profiles[$all_s_p]['id'];
											
											if($all_profiles[$all_s_p]['table_name']=='community_project' || $all_profiles[$all_s_p]['table_name']=='artist_project')
											{
											?>
												<!--<div class="blkGF" style="padding-top:5px;">
													<span class='st_facebook_hcount' displayText='Facebook'></span>-->
												<div>
													<span style="display:none;" id="facespro_<?php echo $up_sp; ?>" st_url="<?php echo $all_profiles[$all_s_p]['profile_url']; ?>" class='st_facebook_hcount' displayText='Facebook'></span>
													<!--<span class='st_email_hcount' displayText='Email'></span>-->
												</div>
											<?php
											}
											
											if($all_profiles[$all_s_p]['table_name']=='community_project' || $all_profiles[$all_s_p]['table_name']=='artist_project')
											{
											?>
												<div class="blkC" style="margin-top:1px;">
													<a title="Send updates on this profile using email newsletters, Purify Art news feeds, and/or Facebook wall posts." onClick="blank_val()" href="Update_facebook.php?spcl=<?php echo $spcla.'|'.$up_sp; ?>&url_mtch_ups=<?php echo $all_profiles[$all_s_p]['profile_url']; ?>&title_for_socials=<?php if($all_profiles[$all_s_p]['name']!="") 
											{
												echo urlencode($all_profiles[$all_s_p]['name']);
											}else
												{
													echo urlencode($all_profiles[$all_s_p]['title']);
												}?>" class="fancybox fancybox.ajax" id="updatep_facebook_<?php echo $up_sp; ?>"><input type="button" style="border-radius:5px; -webkit-border-radius:5px; -moz-border-radius:5px; -ms-border-radius:5px; -o-border-radius:5px; background-color: #000000; border: 0 none; color: #FFFFFF; float: left; font-size: 12px; font-weight: 700;     margin-right: 5px; padding: 8px 15px; text-decoration: none;" value="Send Update" /></a>
												</div>
												<script>
												$(document).ready(function() {
												
													$("#updatep_facebook_<?php echo $up_sp; ?>").fancybox({
													helpers : {
														media : {},
														buttons : {},
														title : null
													},
													afterClose: function() { 
															var face_po = document.getElementById("post_face").value; 
															if(face_po!="no" && face_po!="") 
															{ 
																var pot = face_po.split('|');
																
																$('#facespro_'+pot[1]+' span.stButton').click(); 
															}
															var chk_vals_ups = document.getElementById("chk_vals_ups").value;
															if(chk_vals_ups=='1')
															{
																alert("Your updates have been sent.");
															}
															setTimeout(function(){
																location.reload();
															},2000);
														}
													});
													/*$("#updatep_facebook_<?php echo $up_sp; ?>").fancybox({
													'width'				: '75%',
													'height'			: '100%',
													'transitionIn'		: 'none',
													'transitionOut'		: 'none',
													'type'				: 'iframe',
													'onClosed'			: function() { 
															var face_po = document.getElementById("post_face").value; 
															if(face_po!="no" && face_po!="") 
															{ 
																var pot = face_po.split('|');
																
																$('#facespro_'+pot[1]+' span.stButton').click(); 
															}
															var chk_vals_ups = document.getElementById("chk_vals_ups").value;
															if(chk_vals_ups=='1')
															{
																alert("Your updates have been sent.");
															}
															setTimeout(function(){
																location.reload();
															},2000);
														}
													});*/
												});
												
												function blank_val()
												{
													document.getElementById("post_face").value = ""; 
												}
											</script>
								<?php
											$up_sp = $up_sp + 1;
											}
											?>
											</div>
											<?php
											}
										}
									}
									}
								?>
								</div>
								
								<div id="events_profiles_share" style="display:none;">
								<?php
									$eve_adumps = array();
									$eve_cdumps = array();
									
									if($all_profiles!="" && count($all_profiles)>0)
									{
										$up_ep = 0;
										for($all_s_p=0;$all_s_p<count($all_profiles);$all_s_p++)
										{
											$eves_chks = 0;
											
											if($all_profiles[$all_s_p]['table_name']=='artist_event')
											{
												if(in_array($all_profiles[$all_s_p]['id'],$eve_adumps))
												{
													$eves_chks = 1;
												}	
												else
												{
													$eve_adumps[] = $all_profiles[$all_s_p]['id'];
												}
											}
											elseif($all_profiles[$all_s_p]['table_name']=='community_event')
											{
												if(in_array($all_profiles[$all_s_p]['id'],$eve_cdumps))
												{
													$eves_chks = 1;
												}	
												else
												{
													$eve_cdumps[] = $all_profiles[$all_s_p]['id'];
												}
											}
											
											if($eves_chks==0)
											{
											if($all_profiles[$all_s_p]['table_name']=='community_event' || $all_profiles[$all_s_p]['table_name']=='artist_event')
											{
											?>
											<div class="tableCont" >
											<?php
											if($all_profiles[$all_s_p]['table_name']=='community_event' || $all_profiles[$all_s_p]['table_name']=='artist_event')
											{	
											?>
												<div class="blkA" style="width:150px;">
											<?php
													if($all_profiles[$all_s_p]['type']!="")
													{
														echo $all_profiles[$all_s_p]['type'];
													}
													else
													{
														?>&nbsp;<?php
													}
											?>
												</div>
											<?php
											}
											
											if($all_profiles[$all_s_p]['table_name']=='community_event' || $all_profiles[$all_s_p]['table_name']=='artist_event')
											{
											?>
												<div class="blkH" style="width:398px;">
											<?php
													echo date("m.d.y", strtotime($all_profiles[$all_s_p]['date'])).' '.$all_profiles[$all_s_p]['title'];
											?>
												</div>
											<?php
											}
											
											$spcle = $all_profiles[$all_s_p]['table_name'].'|'.$all_profiles[$all_s_p]['id'];
											
											if($all_profiles[$all_s_p]['table_name']=='community_event' || $all_profiles[$all_s_p]['table_name']=='artist_event')
											{
											?>
												<!--<div class="blkGF" style="padding-top:5px;">
													<span class='st_facebook_hcount' displayText='Facebook'></span>-->
												<div>
													<span style="display:none;" id="faceseve_<?php echo $up_ep; ?>" st_url="<?php echo $all_profiles[$all_s_p]['profile_url']; ?>" class='st_facebook_hcount' displayText='Facebook'></span>
													<!--<span class='st_email_hcount' displayText='Email'></span>-->
												</div>
											<?php
											}
											
											if($all_profiles[$all_s_p]['table_name']=='community_event' || $all_profiles[$all_s_p]['table_name']=='artist_event')
											{
											?>
												<div class="blkC" style="margin-top:1px;">
													<a title="Send updates on this profile using email newsletters, Purify Art news feeds, and/or Facebook wall posts." onClick="blank_val()" href="Update_facebook.php?spcl=<?php echo $spcle.'|'.$up_ep; ?>&url_mtch_ups=<?php echo $all_profiles[$all_s_p]['profile_url']; ?>&title_for_socials=<?php if($all_profiles[$all_s_p]['name']!="") 
											{
												echo urlencode($all_profiles[$all_s_p]['name']);
											}else
												{
													echo urlencode($all_profiles[$all_s_p]['title']);
												}?>" class="fancybox fancybox.ajax" id="updatee_facebook_<?php echo $up_ep; ?>"><input type="button" style="border-radius:5px; -webkit-border-radius:5px; -moz-border-radius:5px; -ms-border-radius:5px; -o-border-radius:5px; background-color: #000000; border: 0 none; color: #FFFFFF; float: left; font-size: 12px; font-weight: 700;     margin-right: 5px; padding: 8px 15px; text-decoration: none;" value="Send Update" /></a>
												</div>
												<script>
												$(document).ready(function() {
												
													$("#updatee_facebook_<?php echo $up_ep; ?>").fancybox({
													helpers : {
														media : {},
														buttons : {},
														title : null
													},
													afterClose: function() { 
															var face_po = document.getElementById("post_face").value; 
															//alert(face_po);
															if(face_po!="no" && face_po!="") 
															{ 
																var pot = face_po.split('|');
																
																$('#faceseve_'+pot[1]+' span.stButton').click(); 
															}
															var chk_vals_ups = document.getElementById("chk_vals_ups").value;
															if(chk_vals_ups=='1')
															{
																alert("Your updates have been sent.");
															}
															setTimeout(function(){
																location.reload();
															},2000);
														}
													});
													/*$("#updatee_facebook_<?php echo $up_ep; ?>").fancybox({
													'width'				: '75%',
													'height'			: '100%',
													'transitionIn'		: 'none',
													'transitionOut'		: 'none',
													'type'				: 'iframe',
													'onClosed'			: function() { 
															var face_po = document.getElementById("post_face").value; 
															//alert(face_po);
															if(face_po!="no" && face_po!="") 
															{ 
																var pot = face_po.split('|');
																
																$('#faceseve_'+pot[1]+' span.stButton').click(); 
															}
															var chk_vals_ups = document.getElementById("chk_vals_ups").value;
															if(chk_vals_ups=='1')
															{
																alert("Your updates have been sent.");
															}
															setTimeout(function(){
																location.reload();
															},2000);
														}
													});*/
												});
												
												function blank_val()
												{
													document.getElementById("post_face").value = ""; 
												}
											</script>
								<?php
												$up_ep = $up_ep + 1;
											}
											?>
											</div>
											<?php
											}
										}
										}
									}
								?>
								</div>
							</div>
						</div>
			<?php
					}
				}
			?>
			
      </div>  
  </div>
</div>


  <!--<div id="footer"> <strong>&copy; 2010 Purify Entertainment</strong> This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/3.0/">Creative Commons Attribution-Share Alike 3.0 Unported License</a>.<br />
    <a href="privacypolicy.html">Privacy Policy</a> / <a href="useragreement.html">User Agreement</a> / <a href="originalityagreement.html">Originality Agreement</a> / <a href="busplan.html">Business Plan</a> </div>
-->
  <!-- end of main container -->
</div><?php include_once("displayfooter.php"); ?>
<script>
	$(document).ready(function(){
		var url_check = document.URL;
		
	 if(location.hash == "" || url_check.indexOf("#profile")>0)
 {

		$("#profile").show();
 }
	
	
	$("a").each(
    function()
    {    
        if($(this).attr("href") == location.hash)
        {
            $(this).click();
        }
    });
	
	$(window).hashchange(
		function()
		{
			$("a").each(
			function()
			{    
				if($(this).attr("href") == location.hash)
				{
					$(this).click();
				}
			});
		}
	)
	
	});
</script>
<?php if(isset($_REQUEST['sel_val']))
{
?>
<script type="text/javascript">



 /* $("document").ready(function(){
	var media_gallery_text=document.getElementById("media");
	var media_video_text=document.getElementById("media_video");
	var media_audio_text=document.getElementById("media_audio");
		
	if(media_gallery_text.value!=null)
	{
		alert(media_gallery_text.value);
	}
	
	if(media_video_text.value!=null)
	{
		alert(media_video_text);
	}
	if(media_audio_text.value!=null)
	{
		alert(media_audio_text);
	}
	
	
});  */
</script>

<?php
}
?>

</body>
</html>
<script type="text/javascript">
$(window).load(function() {
	$(".list-wrap").css({"height":""});
});
</script>