<?php
	include_once('commons/db.php');
	include_once('classes/Feeds_unsubcribe.php');
	session_start();
	
	$feeds_unsub = new Feeds_unsubcribe();
	
	if(isset($_GET['all']) && isset($_GET['user_ids']))
	{
		$sql_sub = $feeds_unsub->subscription($_SESSION['login_email'],$_GET['user_ids']);
		
		$sql_frd = $feeds_unsub->friendship($_SESSION['login_id'],$_GET['user_ids']);
		
		$sql_cnt = $feeds_unsub->remove_contact($_SESSION['login_id'],$_GET['user_ids']);
	}
	
	if(isset($_GET['sub']) && isset($_GET['user_ids']))
	{
		$sql_sub = $feeds_unsub->subscription($_SESSION['login_email'],$_GET['user_ids']);
		
		$sql_cnt = $feeds_unsub->remove_contact($_SESSION['login_id'],$_GET['user_ids']);
	}
	
	if(isset($_GET['frd']) && isset($_GET['user_ids']))
	{
		$sql_frd = $feeds_unsub->friendship($_SESSION['login_id'],$_GET['user_ids']);
		
		$sql_sub = $feeds_unsub->subscription($_SESSION['login_email'],$_GET['user_ids']);
		
		$sql_cnt = $feeds_unsub->remove_contact($_SESSION['login_id'],$_GET['user_ids']);
	}
	
	if(isset($_GET['del_posts']))
	{
		$sql = $feeds_unsub->delete_own($_GET['del_posts']);
	}
	
	header("Location: profileedit.php");
?>