<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "//www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="//www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<title>Purify Art: Services</title>

<?php include("includes/header.php");?>
<div id="outerContainer"></div>
  <div id="contentContainer" >
	<div id="actualContent" class="sidepages">
      <h1>Services </h1>
      <div class="serviceItem">
      <h4 class="service1">Network</h4>
      <p>The Purify Art network connects artists, fans, organizations and businesses in the arts industry and provides online tools that integrate audio, video, and visual art on one ad-free media platform. This network is driven by a media player that allows users to create custom playlists from their personal media libraries. Purify Art enables people to collaborate, promote, perform, share and make secure transactions with one another.</p>
      <p><strong>Ad-Free Profile Pages</strong>: Artists and Community users can share information and media through their profiles pages equipped with a media player and store. Users can build profile pages for events and projects such as bands, albums, collaborations, etc. </p>
      <p><strong>Email Support</strong>: Users will be provided with friendly and proffesional support in using the website and promoting their art.</p>
      <p><strong>Promotional Tools</strong>: Through PurifyArt.com email newsletters, Facebook integration, subscribed user updates and RSS feeds, Artist and Community users can promote from one easy-to-use web based platform. </p>
      <p><strong>Search Engine</strong>: Purify Art's ad free search functionality enables all users to find artists, organizations, media, and events specific to their interests and location.</p>
      </div>
      
      <div class="serviceItem">
      <h4 class="service2">Cloud Media Player</h4>
      <p>All profiles and search engines contain media buttons for users to build custom ad free channels on the media player while browsing the site. The media player integrates audio, video, and visual into one art media experience.</p>
      <p><strong>Uninterrupted Play</strong>: The media player allows the user to browse profiles and pages while building custom channels that play without interruption.</p>
      <p><strong>Audio/Visual Integration</strong>: When adding songs, musicians can tag visual art galleries so that the audio and visual are played simultaneously within the media player. This gives musicians and visual artists the ability to cross promote one another.</p>
      </div>
      
      <div class="serviceItem">
      <h4 class="service3">Memberships</h4>
      <p>Registering with Purify Art is free. Once registered, users can purchase an annual membership for $25 and receive our premium services listed below:</p>
      <p><strong>Media Library</strong>: Use your media library to organize purchased, shared or personal media and build custom channels. Add links to your Purify Art media library from YouTube or SoundCloud.</p>
      <p><strong>Free Downloads</strong>: Download free media from Artist and Community users to be used in your custom channels.</p>
      <p><strong>Amazon S3 Library</strong>: Add an Amazon S3 library to your account and upload unlimited media files through PurifyArt.com. ($50 setup fee paid to Purify Art, and a $.12/GB  hosting and streaming fee paid directly to Amazon)</p>
      <p><strong>Sales, Distribution and Publishing</strong>: Artist and Community members have the ability to make sales through the online digital store and have access to Purify Art's global distribution network and publishing services.</p>
      </div>
      
      <div class="serviceItem">
      <h4 class="service4">Digital Sales</h4>
      <p><strong>Low Fees</strong>: Artist and Community members have the ability to make sales through the online digital store and receive 95% of their sales. Purify Art takes 5% to provide the hosting and programming that enables artists to sell directly to their fans.</p>
      <p><strong>Fan Club Memberships</strong>: Artist and Community members have the ability to make sales through the online digital store and have access to Purify Art's global distribution network and publishing services.</p>
    	</div>
    
    </div>
    <div id="snipeSidebar">
    	<div class="snipe">
      	<h2>Questions?</h2>
        <p>If you have general questions about our services, check out our <a href="help_faq.php">FAQ ></a></p>
      </div>
     <div class="snipe">
      	<h2>Register Now!</h2>
        <p>Interested in joining Purify Art? Sign up for free. <a href="index.php">More ></a></p>
      </div>
    </div>
    
	<div class="clearMe"></div>
  </div>

</div><?php include_once("displayfooter.php"); ?>
</body>
</html>