function showGallery(){
	$("#light-gal").css('visibility','visible');
	
}
function CloseGallery(){
	$("#light-gal").css('visibility','hidden');
}
	//config
//set default images view mode
var rotate_time = 8000;
$defaultViewMode="full"; //full, normal, original
$tsMargin=30; //first and last thumbnail margin (for better cursor interaction) 
$scrollEasing=600; //scroll easing amount (0 for no easing) 
$scrollEasingType="easeOutCirc"; //scroll easing type 
$thumbnailsContainerOpacity=0.8; //thumbnails area default opacity
$thumbnailsContainerMouseOutOpacity=0; //thumbnails area opacity on mouse out
$thumbnailsOpacity=0.6; //thumbnails default opacity
$nextPrevBtnsInitState="show"; //next/previous image buttons initial state ("hide" or "show")
$keyboardNavigation="on"; //enable/disable keyboard navigation ("on" or "off")
$rotate_val=1;
//cache vars
$thumbnails_wrapper=$("#thumbnails_wrapper");
$outer_container=$("#outer_container");
$thumbScroller=$(".thumbScroller");
$thumbScroller_container=$(".thumbScroller .container");
$thumbScroller_content=$(".thumbScroller .content");
$thumbScroller_thumb=$(".thumbScroller .thumb");
$preloader=$("#preloader");
$toolbar=$("#toolbar");
$toolbar_a=$("#toolbar a");
$bgimg=$("#bgimg");
$img_title=$("#img_title");
$nextImageBtn=$(".nextImageBtn");
$prevImageBtn=$(".prevImageBtn");

function updateCacheVars(){

	//config
//set default images view mode
$defaultViewMode="normal"; //full, normal, original
$tsMargin=30; //first and last thumbnail margin (for better cursor interaction) 
$scrollEasing=600; //scroll easing amount (0 for no easing) 
$scrollEasingType="easeOutCirc"; //scroll easing type 
$thumbnailsContainerOpacity=0.8; //thumbnails area default opacity
$thumbnailsContainerMouseOutOpacity=0; //thumbnails area opacity on mouse out
$thumbnailsOpacity=0.6; //thumbnails default opacity
$nextPrevBtnsInitState="show"; //next/previous image buttons initial state ("hide" or "show")
$keyboardNavigation="on"; //enable/disable keyboard navigation ("on" or "off")

//cache vars
$thumbnails_wrapper=$("#thumbnails_wrapper");
$outer_container=$("#outer_container");
$thumbScroller=$(".thumbScroller");
$thumbScroller_container=$(".thumbScroller .container");
$thumbScroller_content=$(".thumbScroller .content");
$thumbScroller_thumb=$(".thumbScroller .thumb");
$preloader=$("#preloader");
$toolbar=$("#toolbar");
$toolbar_a=$("#toolbar a");
$bgimg=$("#bgimg");
$img_title=$("#img_title");
$nextImageBtn=$(".nextImageBtn");
$prevImageBtn=$(".prevImageBtn");
$rotate_val=1;
}

function loadGallery(){

$(".nextImageBtn").unbind("click");
$(".prevImageBtn").unbind("click");
	updateCacheVars();
	$rotate_val=0;
	$toolbar.data("imageViewMode",$defaultViewMode); //default view mode
	/*if($defaultViewMode=="full"){
		$toolbar_a.html("<img src='toolbar_n_icon.png' width='50' height='50'  />").attr("onClick", "ImageViewMode('normal');return false").attr("title", "Restore");
	} else {
		$toolbar_a.html("<img src='toolbar_fs_icon.png' width='50' height='50'  />").attr("onClick", "ImageViewMode('full');return false").attr("title", "Maximize");
	}*/
	ShowHideNextPrev($nextPrevBtnsInitState);
	//thumbnail scroller
	$thumbScroller_container.css("marginLeft",$tsMargin+"px"); //add margin
	sliderLeft=$thumbScroller_container.position().left;
	sliderWidth=$outer_container.width();
	$thumbScroller.css("width",sliderWidth);
	var totalContent=0;
	fadeSpeed=200;
	var $the_outer_container=document.getElementById("outer_container");
	var $placement=findPos($the_outer_container);
	
	$thumbScroller_content.each(function () {
		var $this=$(this);	
		//the old line is changed to display all thumnail in first time  /*totalContent+=$this.innerWidth();*/
		//totalContent += $this.innerWidth() + 50;
		totalContent = totalContent + 250;
		$thumbScroller_container.css("width",totalContent);
		$this.children().children().children(".thumb").fadeTo(fadeSpeed, $thumbnailsOpacity);
	});

	$thumbScroller.mousemove(function(e){
		if($thumbScroller_container.width()>sliderWidth){
	  		var mouseCoords=(e.pageX - $placement[1]);
			var mousePercentX=mouseCoords/sliderWidth;
	  		var destX=-((((totalContent+($tsMargin*2))-(sliderWidth))-sliderWidth)*(mousePercentX));
	  		var thePosA=mouseCoords-destX;
	  		var thePosB=destX-mouseCoords;
			if(mouseCoords>destX){
		  		$thumbScroller_container.stop().animate({left: -thePosA}, $scrollEasing,$scrollEasingType); //with easing
	  		} else if(mouseCoords<destX){
		  		$thumbScroller_container.stop().animate({left: thePosB}, $scrollEasing,$scrollEasingType); //with easing
			} else {
				$thumbScroller_container.stop();  
	  		}
		}
	});

	//$thumbnails_wrapper.fadeTo(fadeSpeed, $thumbnailsContainerOpacity);
	$thumbnails_wrapper.hover(
		function(){ //mouse over
			var $this=$(this);
			$this.stop().fadeTo("slow", 1);
		},
		function(){ //mouse out
			var $this=$(this);
			$this.stop().fadeTo("slow", $thumbnailsContainerMouseOutOpacity);
		}
	);

	$thumbScroller_thumb.hover(
		function(){ //mouse over
			var $this=$(this);
			$this.stop().fadeTo(fadeSpeed, 1);
		},
		function(){ //mouse out
			var $this=$(this);
			$this.stop().fadeTo(fadeSpeed, $thumbnailsOpacity);
		}
	);

	//on window resize scale image and reset thumbnail scroller
	$(window).resize(function() {
		FullScreenBackground("#bgimg",$bgimg.data("newImageW"),$bgimg.data("newImageH"));
		$thumbScroller_container.stop().animate({left: sliderLeft}, 400,"easeOutCirc"); 
		var newWidth=$outer_container.width();
		$thumbScroller.css("width",newWidth);
		sliderWidth=newWidth;
		$placement=findPos($the_outer_container);
	});
	
	//load 1st image
	var the1stImg = new Image();
	the1stImg.onload = CreateDelegate(the1stImg, theNewImg_onload);
	the1stImg.src = $bgimg.attr("src");
	$outer_container.data("nextImage",$(".content").first().next().find("a").attr("href"));
	$outer_container.data("firstImage",$(".content").first().find("a").attr("href"));
	var $this=$("#outer_container a[href='"+$outer_container.data("firstImage")+"']");
	GetImageTitle($this);
	$outer_container.data("prevImage",$(".content").last().find("a").attr("href"));


	//mouseover toolbar
	if($toolbar.css("display")!="none"){
		$toolbar.fadeTo("fast", 0.4);
	}
	$toolbar.hover(
		function(){ //mouse over
			var $this=$(this);
			$this.stop().fadeTo("fast", 1);
		},
		function(){ //mouse out
			var $this=$(this);
			$this.stop().fadeTo("fast", 0.4);
		}
	);

	//Clicking on thumbnail changes the background image
	$("#outer_container a").click(function(event){
		event.preventDefault();
		var $this=$(this);
		GetNextPrevImages($this);
		GetImageTitle($this);
		SwitchImage(this);
		ShowHideNextPrev("show");
		clearInterval(interval);
		startCycle();
	}); 

	
	//next/prev images buttons
	$nextImageBtn.click(function(event){
		event.preventDefault();
		SwitchImage($outer_container.data("nextImage"));
		var $this=$("#outer_container a[href='"+$outer_container.data("nextImage")+"']");
		GetNextPrevImages($this);
		GetImageTitle($this);
	});

	$prevImageBtn.click(function(event){
		event.preventDefault();
		SwitchImage($outer_container.data("prevImage"));
		var $this=$("#outer_container a[href='"+$outer_container.data("prevImage")+"']");
		GetNextPrevImages($this);
		GetImageTitle($this);
	});

	//next/prev images keyboard arrows
	if($keyboardNavigation=="on"){
	$(document).keydown(function(ev) {
		if(ev.keyCode == 39) { //right arrow
			SwitchImage($outer_container.data("nextImage"));
			var $this=$("#outer_container a[href='"+$outer_container.data("nextImage")+"']");
			GetNextPrevImages($this);
			GetImageTitle($this);
			return false; // don't execute the default action (scrolling or whatever)
		} else if(ev.keyCode == 37) { //left arrow
			SwitchImage($outer_container.data("prevImage"));
			var $this=$("#outer_container a[href='"+$outer_container.data("prevImage")+"']");
			GetNextPrevImages($this);
			GetImageTitle($this);
			return false; // don't execute the default action (scrolling or whatever)
		}
	});
	}

}
var interval;
function startCycle() {
	interval = setInterval( "rotate()"	, rotate_time );
}
$(document).ready(function(){
	$(function() {
	startCycle();
	});
});
function BackgroundLoad($this,imageWidth,imageHeight,imgSrc){
	$this.fadeOut("fast",function(){
		$this.attr("src", "").attr("src", imgSrc); //change image source
		FullScreenBackground($this,imageWidth,imageHeight); //scale background image
		$preloader.fadeOut("fast",function(){$this.fadeIn("slow");});
		var imageTitle=$img_title.data("imageTitle");
		if(imageTitle !=""){
			$this.attr("alt", imageTitle).attr("title", imageTitle);
			$img_title.fadeOut("fast",function(){
				$img_title.html(imageTitle).fadeIn();
			});
		} else {
			imageTitle ="";
			$this.attr("alt", imageTitle).attr("title", imageTitle);
			$img_title.fadeOut("fast",function(){
				$img_title.html(imageTitle).fadeIn();
			});
			/*$img_title.fadeOut("fast",function(){
				$img_title.html($this.attr("title")).fadeIn();
			});*/
		}
	});
}
function ShowHideNextPrev(state){
	if(state=="hide"){
		$nextImageBtn.fadeOut();
		$prevImageBtn.fadeOut();
	} else {
		$nextImageBtn.fadeIn();
		$prevImageBtn.fadeIn();
	}
}

//get image title
function GetImageTitle(elem){
	if(elem.children("img").attr("title")!="" && elem.children("img").attr("title")!="undefined"){
		var title_attr = elem.children("img").attr("title"); //get image title attribute
		$img_title.data("imageTitle", title_attr); //store image title
	}else{
		var title_attr = "";
		$img_title.data("imageTitle", title_attr); //store image title
	}
	
}

//get next/prev images
function GetNextPrevImages(curr){
	var nextImage=curr.parents(".content").next().find("a").attr("href");
	if(nextImage==null){ //if last image, next is first
		var nextImage=$(".content").first().find("a").attr("href");
	}
	//$outer_container.data("nextImage",$(".content").first().next().find("a").attr("href"));	
	$outer_container.data("nextImage",nextImage);
	var prevImage=curr.parents(".content").prev().find("a").attr("href");
	if(prevImage==null){ //if first image, previous is last
		var prevImage=$(".content").last().find("a").attr("href");
		
	}
	$outer_container.data("prevImage",prevImage);
}

//switch image
function SwitchImage(img) {

    //if (url.indexOf("undefined") != -1) {
        $preloader.fadeIn("fast"); //show preloader
        var theNewImg = new Image();
        theNewImg.onload = CreateDelegate(theNewImg, theNewImg_onload);
        theNewImg.src = img;
    //}
}
// verify that image exists
function ImageExist(url) {
   // if (url.indexOf("undefined") != -1) {
        var img = new Image();
        img.src = url;
        return img.height != 0;
   /* } else {
        return false;
    }*/

}
//get new image dimensions
function CreateDelegate(contextObject, delegateMethod){
	return function(){
		return delegateMethod.apply(contextObject, arguments);
	}
}

//new image on load
function theNewImg_onload(){
	$bgimg.data("newImageW",this.width).data("newImageH",this.height);
	BackgroundLoad($bgimg,this.width,this.height,this.src);
}

//Image scale function
function FullScreenBackground(theItem,imageWidth,imageHeight){
	var winWidth=$(window).width();
	var winHeight=$(window).height();
	if($toolbar.data("imageViewMode")!="original"){ //scale
		var picHeight = imageHeight / imageWidth;
		var picWidth = imageWidth / imageHeight;
		/*if($toolbar.data("imageViewMode")=="full"){ //fullscreen size image mode
			if ((winHeight / winWidth) < picHeight) {
				$(theItem).attr("width",winWidth);
				$(theItem).attr("height",picHeight*winWidth);
			} else {
				$(theItem).attr("height",winHeight);
				$(theItem).attr("width",picWidth*winHeight);
			};
		} else {*/ //normal size image mode
			if ((winHeight / winWidth) > picHeight) {
				$(theItem).attr("width",winWidth);
				$(theItem).attr("height",picHeight*winWidth);
			} else {
				$(theItem).attr("height",winHeight);
				$(theItem).attr("width",picWidth*winHeight);
			};
		//}
		$(theItem).css("margin-left",(winWidth-$(theItem).width())/2);
		$(theItem).css("margin-top",(winHeight-$(theItem).height())/2);
	} /*else { //no scale
		$(theItem).attr("width",imageWidth);
		$(theItem).attr("height",imageHeight);
		$(theItem).css("margin-left",(winWidth-imageWidth)/2);
		$(theItem).css("margin-top",(winHeight-imageHeight)/2);
	}*/
}

//Image view mode function - fullscreen or normal size
function ImageViewMode(theMode){
	$toolbar.data("imageViewMode", theMode);
	FullScreenBackground($bgimg,$bgimg.data("newImageW"),$bgimg.data("newImageH"));
	if(theMode=="full"){
		//$toolbar_a.html("<img src='toolbar_n_icon.png' width='50' height='50'  />").attr("onClick", "ImageViewMode('normal');return false").attr("title", "Restore");
		$("#maximizeImg").find("img").attr("src", "../galleryfiles/toolbar_n_icon.png");
		$("#maximizeImg").attr("onClick", "ImageViewMode('normal');return false").attr("title", "Restore");
		$("#maximizeImg").css({"display":"none"});
	} else {
		//$toolbar_a.html("<img src='toolbar_fs_icon.png' width='50' height='50'  />").attr("onClick", "ImageViewMode('full');return false").attr("title", "Maximize");
		$("#maximizeImg").find("img").attr("src", "../galleryfiles/toolbar_fs_icon.png");
		$("#maximizeImg").attr("onClick", "ImageViewMode('full');return false").attr("title", "Maximize");
	}

	if($("#maximizeImg").attr("title")=="Maximize"){
		if(typeof setFullScreen == 'function') {
			setFullScreen();
		}
		
	}else{
		if(typeof setRestoreScreen == 'function') {
			setRestoreScreen();
		}
	}
	
	if($("#maximizeImg").attr("title") == "Restore")
	{
		var el = document.documentElement
				, rfs = // for newer Webkit and Firefox
					   el.requestFullScreen
					|| el.webkitRequestFullScreen
					|| el.mozRequestFullScreen
					|| el.msRequestFullScreen
			;
			if(typeof rfs!="undefined" && rfs){
			  rfs.call(el);
			} else if(typeof window.ActiveXObject!="undefined"){
			  // for Internet Explorer
			  var wscript = new ActiveXObject("WScript.Shell");
			  if (wscript!=null) {
				 wscript.SendKeys("{F11}");
			  }
			}
	}
	if($("#maximizeImg").attr("title") == "Maximize")
	{
		
	}
}

//function to handle gallery play mode
function ImagePlayMode(playMode){
	if(playMode=="pause"){
		$("#pauseImg").find("img").attr("src", "../galleryfiles/play.png");
		$("#pauseImg").attr("onClick", "ImagePlayMode('play');return false").attr("title", "Play");
		setTimer=0;
		window.clearInterval(GalleryAutoPlayTimer);
	} else {
		$("#pauseImg").find("img").attr("src", "../galleryfiles/pause.png");
		$("#pauseImg").attr("onClick", "ImagePlayMode('pause');return false").attr("title", "Pause");
		setTimer=1;
		GalleryAutoPlayTimer=self.setInterval("autoRotate()",rotate_time);
	}
}

//Code For Auto Play Gallery
function rotate()
{
	if($rotate_val==0)
	{
		SwitchImage($outer_container.data("nextImage"));
		var $this=$("#outer_container a[href='"+$outer_container.data("nextImage")+"']");
		GetNextPrevImages($this);
		GetImageTitle($this);
	}
}
$("#closeGal").click(function(){
$rotate_val=1;
$("#miniFooter").css('display','block');
$("#profileHeader").css('display','block');
$("#contentContainer").css('display','block');
$("#outerContainer").css('display','block');
$(".header").css({"z-index":"99"});

$(".rowItem").css({"display":"block"});
$(".accountSettings").css('z-index','110');
if (!$.browser.msie) {
	$("#maximizeImg").find("img").attr("src", "../galleryfiles/toolbar_fs_icon.png");
	$("#maximizeImg").attr("onClick", "ImageViewMode('full');return false").attr("title", "Maximize");
	$("#maximizeImg").css({"display":"inline"});
}
if (document.exitFullscreen) {
    document.exitFullscreen();
}
else if (document.mozCancelFullScreen) {
    document.mozCancelFullScreen();
}
else if (document.webkitCancelFullScreen) {
    document.webkitCancelFullScreen();
}
// for Internet Explorer
 /* var wscript = new ActiveXObject("WScript.Shell");
  if (wscript!=null) {
	 wscript.SendKeys("{F11}");
  }*/
});

//function to find element Position
	function findPos(obj) {
		var curleft = curtop = 0;
		if (obj.offsetParent) {
			curleft = obj.offsetLeft
			curtop = obj.offsetTop
			while (obj = obj.offsetParent) {
				curleft += obj.offsetLeft
				curtop += obj.offsetTop
			}
		}
		return [curtop, curleft];
	}
	
$("img").bind("contextmenu",function(e){
	return false;
}); 