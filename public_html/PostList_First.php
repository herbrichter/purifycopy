<?php
	session_start();
	
	include_once("commons/db.php");
	include_once("classes/Addfriend.php");
	include_once("classes/Listpost.php");

	//echo $_POST['type_val'];
	
	$oldob_suugest = new Addfriend();
	$listob_suugest = new Listpost();
	
	if(isset($_SESSION['login_email']))
	{
		$gen_log_id = $oldob_suugest->general_user_email_gen($_SESSION['login_email']);
	}
	
	if($gen_log_id!="")
	{
		if(isset($_POST['type_val']) && $_POST['type_val']!="" && $_POST['type_val']=='events_post')
		{
			$get_ac_ol = $listob_suugest->get_all_events($gen_log_id['general_user_id'],$gen_log_id['artist_id'],$gen_log_id['community_id']);
			
			$get_ac_upol = $listob_suugest->only_creator_recevent();
			
			$get_ac_reol = $listob_suugest->only_creator_upevent();
			
			$get_ac_upol2 = $listob_suugest->only_creator2ev_upevent();
			
			$get_ac_reol2 = $listob_suugest->only_creator2_recevent();			
			
			$get_ac_events = array_merge($get_ac_ol,$get_ac_upol,$get_ac_reol,$get_ac_upol2,$get_ac_reol2);
			
			
			$get_all_del_id = $listob_suugest->get_deleted_project_id();
			$exp_del_id = explode(",",$get_all_del_id['deleted_event_id']);
			for($count_all=0;$count_all<count($get_ac_events);$count_all++)
			{
				if($get_ac_events[$count_all]['table_name']=='general_artist')
				{
					$get_ac_events[$count_all]['id'] = $get_ac_events[$count_all]['id'].'~art';
				}
				elseif($get_ac_events[$count_all]['table_name']=='general_community')
				{
					$get_ac_events[$count_all]['id'] = $get_ac_events[$count_all]['id'].'~com';
				}
				for($count_del=0;$count_del<count($exp_del_id);$count_del++)
				{
					if($exp_del_id[$count_del]!="")
					{
						if($get_ac_events[$count_all]['id'] == $exp_del_id[$count_del])
						{
							$get_ac_events[$count_all] ="";
						}
					}
				}
			}
			
			if(count($get_ac_events)>0 && !empty($get_ac_events))
			{
				$sort = array();
				foreach($get_ac_events as $k=>$v)
				{										
					$sort['date'][$k] = $v['date'];
				}
				//var_dump($sort);
				array_multisort($sort['date'], SORT_DESC,$get_ac_events);
				
				$dum_eve_a = array();
				$dum_eve_c = array();
?>
				<select name="list_epm_first[]" size="5" id="list_epm_first[]" class="list" style="left:115px; position:relative;">
<?php
					for($ac_eve=0;$ac_eve<count($get_ac_events);$ac_eve++)
					{
						$cont_ev = 0;
						if($get_ac_events[$ac_eve]['table_name']=='general_artist')
						{
							$org = explode('~',$get_ac_events[$ac_eve]['id']);
							$get_ac_events[$ac_eve]['id'] = $org[0];
							if(in_array($get_ac_events[$ac_eve]['id'],$dum_eve_a))
							{
								$cont_ev = 0;
							}
							else
							{
								$cont_ev = 1;
								$dum_eve_a[] = $get_ac_events[$ac_eve]['id'];
							}
						}
						elseif($get_ac_events[$ac_eve]['table_name']=='general_community')
						{
							$org = explode('~',$get_ac_events[$ac_eve]['id']);
							$get_ac_events[$ac_eve]['id'] = $org[0];
							if(in_array($get_ac_events[$ac_eve]['id'],$dum_eve_c))
							{
								$cont_ev = 0;
							}
							else
							{
								$cont_ev = 1;
								$dum_eve_c[] = $get_ac_events[$ac_eve]['id'];
							}
						}
						
						if($cont_ev==1)
						{
?>
							<option value="<?php
								if(isset($get_ac_events[$ac_eve]['table_name']) && $get_ac_events[$ac_eve]['table_name']=='general_artist') 
								{
									echo $get_ac_events[$ac_eve]['id'].'~artist_event';
								}
								elseif(isset($get_ac_events[$ac_eve]['table_name']) && $get_ac_events[$ac_eve]['table_name']=='general_community')
								{
									echo $get_ac_events[$ac_eve]['id'].'~community_event';
								}?>"><?php echo date("m.d.y", strtotime($get_ac_events[$ac_eve]['date'])).' '.$get_ac_events[$ac_eve]['title']; ?>
							</option>
<?php
						}
					}
?>
				</select>
<?php
			}
			else
			{
?>
				<input class="fieldText" type="text" disabled="true" value="No Events Found." style=" left: 115px; position: relative;"/>
<?php
			}
		}
		elseif(isset($_POST['type_val']) && $_POST['type_val']!="" && $_POST['type_val']=='projects_post')
		{
			$get_ac_pro_ol = $listob_suugest->get_all_projects($gen_log_id['general_user_id'],$gen_log_id['artist_id'],$gen_log_id['community_id']);
			
			$get_ac_pro_cr = $listob_suugest->only_creator_this();
			
			$get_ac_pro_cr2 = $listob_suugest->only_creator2pr_this();
			
			$get_ac_projects = array_merge($get_ac_pro_ol,$get_ac_pro_cr,$get_ac_pro_cr2);
			
			if(count($get_ac_projects)>0 && !empty($get_ac_projects))
			{
				$sort = array();
				foreach($get_ac_projects as $k=>$v)
				{
					$end_c1 = $v['creator'];
					//$create_c = strpos($v['creator'],'(');
					//$end_c1 = substr($v['creator'],0,$create_c);
					$end_c = trim($end_c1);
					
					$sort['creator'][$k] = strtolower($end_c);
					//$sort['from'][$k] = $end_f;
					//$sort['track'][$k] = $v['track'];
					$sort['title'][$k] = strtolower($v['title']);
				}
				//var_dump($sort);
				array_multisort($sort['creator'], SORT_ASC, $sort['title'], SORT_ASC,$get_ac_projects);
?>
				<select name="list_epm_first[]" size="5" id="list_epm_first[]" class="list" style="left:115px; position:relative;">
<?php
					$posts_pro_a = array();
					$posts_pro_c = array();
					for($ac_pro=0;$ac_pro<count($get_ac_projects);$ac_pro++)
					{
						$cont = 0;
						if($get_ac_projects[$ac_pro]['table_name']=='general_artist')
						{
							if(in_array($get_ac_projects[$ac_pro]['id'],$posts_pro_a))
							{
								$cont = 0;
							}
							else
							{
								$cont = 1;
								$posts_pro_a[] = $get_ac_projects[$ac_pro]['id'];
							}
						}
						elseif($get_ac_projects[$ac_pro]['table_name']=='general_community')
						{
							if(in_array($get_ac_projects[$ac_pro]['id'],$posts_pro_c))
							{
								$cont = 0;
							}
							else
							{
								$cont = 1;
								$posts_pro_c[] = $get_ac_projects[$ac_pro]['id'];
							}
						}
						
						if($cont==1)
						{
							//$create_c = strpos($get_ac_projects[$ac_pro]['creator'],'(');
							//$end_c = substr($get_ac_projects[$ac_pro]['creator'],0,$create_c);
							$end_c = $get_ac_projects[$ac_pro]['creator'];
							$ecei = "";
							if($end_c!="")
							{
								$ecei = $end_c;
								$ecei .= ' : '.$get_ac_projects[$ac_pro]['title'];
							}
							else
							{
								$ecei = $get_ac_projects[$ac_pro]['title'];
							}
?>
							<option value="<?php
								if(isset($get_ac_projects[$ac_pro]['table_name']) && $get_ac_projects[$ac_pro]['table_name']=='general_artist') 
								{
									echo $get_ac_projects[$ac_pro]['id'].'~artist_project';
								}
								elseif(isset($get_ac_projects[$ac_pro]['table_name']) && $get_ac_projects[$ac_pro]['table_name']=='general_community')
								{
									echo $get_ac_projects[$ac_pro]['id'].'~community_project';
								}?>"><?php echo $ecei; ?>
							</option>
<?php
						}
					}
?>
				</select>
<?php
			}
			else
			{
?>
				<input class="fieldText" type="text" disabled="true" value="No Projects Found." style=" left: 115px; position: relative;"/>
<?php
			}
		}
		elseif(isset($_POST['type_val']) && $_POST['type_val']!="" && $_POST['type_val']=='images_post')
		{
			$get_ac_galleries_1 = $listob_suugest->get_all_galleries($gen_log_id['general_user_id'],$gen_log_id['artist_id'],$gen_log_id['community_id']);
			
			$new_medi = $listob_suugest->get_creator_heis();
			$new_creator_creator_media_array = array();
			if(!empty($new_medi))
			{
				for($count_cre_m=0;$count_cre_m<count($new_medi);$count_cre_m++)
				{
					if($new_medi[$count_cre_m]['media_type'] == 113)
					{
						$new_creator_creator_media_array[] = $new_medi[$count_cre_m];
					}
					
				}
			}

			$new_medi_from = $listob_suugest->get_from_heis();
			$new_from_from_media_array = array();
			if(!empty($new_medi_from))
			{
				for($count_cre_m=0;$count_cre_m<count($new_medi_from);$count_cre_m++)
				{
					if($new_medi_from[$count_cre_m]['media_type'] == 113)
					{
						$new_from_from_media_array[] = $new_medi_from[$count_cre_m];
					}
				}
			}
			
			$new_medi_tagged = $listob_suugest->get_the_user_tagin_project($_SESSION['login_email']);
			$new_tagged_from_media_array = array();
			if(!empty($new_medi_tagged))
			{
				for($count_cre_m=0;$count_cre_m<count($new_medi_tagged);$count_cre_m++)
				{
					if($new_medi_tagged[$count_cre_m]['media_type'] == 113)
					{
						$new_tagged_from_media_array[] = $new_medi_tagged[$count_cre_m];
					}
				}
			}
			
			$get_where_creator_media = $listob_suugest->get_media_where_creator_or_from();
			$new_creator_media_array = array();
			if(!empty($get_where_creator_media))
			{
				for($count_tag_m=0;$count_tag_m<count($get_where_creator_media);$count_tag_m++)
				{
					if($get_where_creator_media[$count_tag_m]['media_type'] == 113)
					{
						$new_creator_media_array[] = $get_where_creator_media[$count_tag_m];
					}
				}
			}
			
			$cre_frm_med = array();
			$type = 113;
			$cre_frm_med = $listob_suugest->get_media_create($type);
			
			$get_ac_galleries = array_merge($get_ac_galleries_1,$new_creator_creator_media_array,$new_creator_media_array,$cre_frm_med,$new_tagged_from_media_array,$new_from_from_media_array);
			if(count($get_ac_galleries)>0)
			{
				$sort = array();
				foreach($get_ac_galleries as $k=>$v)
				{							
					$end_c1 = $v['creator'];			
					//$create_c = strpos($v['creator'],'(');
					//$end_c1 = substr($v['creator'],0,$create_c);
					$end_c = trim($end_c1);
					
					$end_f1 = $v['from'];
					//$create_f = strpos($v['from'],'(');
					//$end_f1 = substr($v['from'],0,$create_f);
					$end_f = trim($end_f1);
					
					$sort['creator'][$k] = strtolower($end_c);
					$sort['from'][$k] = strtolower($end_f);
					//$sort['track'][$k] = $v['track'];
					$sort['title'][$k] = strtolower($v['title']);
				}
				//var_dump($sort);
				array_multisort($sort['creator'], SORT_ASC, $sort['from'], SORT_ASC, $sort['title'], SORT_ASC,$get_ac_galleries);
				$get_all_del_id = $listob_suugest->get_all_del_media_id();
				$exp_all_del_id = explode(",",$get_all_del_id);
				if(!empty($exp_all_del_id))
				{
					for($count_all=0;$count_all<=count($get_ac_galleries);$count_all++)
					{
						for($count_all_del=0;$count_all_del<=count($exp_all_del_id);$count_all_del++)
						{
							if(isset($exp_all_del_id[$count_all_del]))
							{
								if($exp_all_del_id[$count_all_del]!="")
								{
									if($exp_all_del_id[$count_all_del]==""){continue;}
									else{
										if($exp_all_del_id[$count_all_del]==$get_ac_galleries[$count_all]['id'])
										{
											$get_ac_galleries[$count_all]="";
										}
									}
								}
							}
						}
					}
				}
				$dummy_pr = array();
				?>
				<select name="list_epm_first[]" size="5" id="list_epm_first[]" class="list" style="left:115px; position:relative;">
<?php
					for($ac_gal=0;$ac_gal<count($get_ac_galleries);$ac_gal++)
					{
						if(in_array($get_ac_galleries[$ac_gal]['id'],$dummy_pr))
						{
						
						}
						else
						{
							$dummy_pr[] = $get_ac_galleries[$ac_gal]['id'];
						//$create_c = strpos($get_ac_galleries[$ac_gal]['creator'],'(');
						//$end_c = substr($get_ac_galleries[$ac_gal]['creator'],0,$create_c);
						$end_c = $get_ac_galleries[$ac_gal]['creator'];
						//$create_f = strpos($get_ac_galleries[$ac_gal]['from'],'(');
						//$end_f = substr($get_ac_galleries[$ac_gal]['from'],0,$create_f);
						$end_f = $get_ac_galleries[$ac_gal]['from'];
						$eceksg = "";
						if($end_c!="")
						{
							$eceksg = $end_c;
						}
						if($end_f!="")
						{
							if($end_c!="")
							{
								$eceksg .= " : ".$end_f;
							}
							else
							{
								$eceksg .= $end_f;
							}
						}
						if($get_ac_galleries[$ac_gal]['title']!="")
						{
							if($end_c!="" || $end_f!="")
							{
								$eceksg .= " : ".$get_ac_galleries[$ac_gal]['title'];
							}
							else
							{
								$eceksg .= $get_ac_galleries[$ac_gal]['title'];
							}
						}
?>
						<option value="<?php
							if(isset($get_ac_galleries[$ac_gal]['table_name']) && $get_ac_galleries[$ac_gal]['table_name']=='general_artist') 
							{
								echo $get_ac_galleries[$ac_gal]['gallery_id'].'~reg_art';
							}
							elseif(isset($get_ac_galleries[$ac_gal]['table_name']) && $get_ac_galleries[$ac_gal]['table_name']=='general_community')
							{
								echo $get_ac_galleries[$ac_gal]['gallery_id'].'~reg_com';
							}
							elseif(isset($get_ac_galleries[$ac_gal]['table_name']) && $get_ac_galleries[$ac_gal]['table_name']=='general_media')
							{
								echo $get_ac_galleries[$ac_gal]['id'].'~reg_med';
							}?>"><?php
							if(isset($get_ac_galleries[$ac_gal]['gallery_title']) && $get_ac_galleries[$ac_gal]['gallery_title']!="")
							{
								echo $get_ac_galleries[$ac_gal]['gallery_title']; 
							}
							elseif(isset($get_ac_galleries[$ac_gal]['title']) && $get_ac_galleries[$ac_gal]['title']!="")
							{
								echo $eceksg; 
							}?></option>
<?php
						}
					}
?>
				</select>
<?php
			}
			else
			{
?>
				<input class="fieldText" type="text" disabled="true" value="No Galleries Found." style=" left: 115px; position: relative;"/>
<?php
			}
		}
		elseif(isset($_POST['type_val']) && $_POST['type_val']!="" && $_POST['type_val']=='songs_post')
		{
			$get_ac_songs_1 = $listob_suugest->get_all_songs($gen_log_id['general_user_id'],$gen_log_id['artist_id'],$gen_log_id['community_id']);
			
			$new_medi = $listob_suugest->get_creator_heis();
			$new_creator_creator_media_array = array();
			if(!empty($new_medi))
			{
				for($count_cre_m=0;$count_cre_m<count($new_medi);$count_cre_m++)
				{
					if($new_medi[$count_cre_m]['media_type'] == 114)
					{
						$get_media_track = $listob_suugest->get_media_track($new_medi[$count_cre_m]['id']); 
						$new_medi[$count_cre_m]["track"] = $get_media_track['track'];
						$new_creator_creator_media_array[] = $new_medi[$count_cre_m];
					}
				}
			}

			$new_medi_from = $listob_suugest->get_from_heis();
			$new_from_from_media_array = array();
			if(!empty($new_medi_from))
			{
				for($count_cre_m=0;$count_cre_m<count($new_medi_from);$count_cre_m++)
				{
					if($new_medi_from[$count_cre_m]['media_type'] == 114)
					{
						$get_media_track = $listob_suugest->get_media_track($new_medi_from[$count_cre_m]['id']); 
						$new_medi_from[$count_cre_m]["track"] = $get_media_track['track'];
						$new_from_from_media_array[] = $new_medi_from[$count_cre_m];
					}
				}
			}
			
			$new_medi_tagged = $listob_suugest->get_the_user_tagin_project($_SESSION['login_email']);
			$new_tagged_from_media_array = array();
			if(!empty($new_medi_tagged))
			{
				for($count_cre_m=0;$count_cre_m<count($new_medi_tagged);$count_cre_m++)
				{
					if($new_medi_tagged[$count_cre_m]['media_type'] == 114)
					{
						$get_media_track = $listob_suugest->get_media_track($new_medi_tagged[$count_cre_m]['id']); 
						$new_medi_tagged[$count_cre_m]["track"] = $get_media_track['track'];
						$new_tagged_from_media_array[] = $new_medi_tagged[$count_cre_m];
					}
				}
			}
			
			$get_where_creator_media = $listob_suugest->get_media_where_creator_or_from();
			$new_creator_media_array = array();
			if(!empty($get_where_creator_media))
			{
				for($count_tag_m=0;$count_tag_m<count($get_where_creator_media);$count_tag_m++)
				{
					if($get_where_creator_media[$count_tag_m]['media_type'] == 114)
					{
						$get_media_track = $listob_suugest->get_media_track($get_where_creator_media[$count_tag_m]['id']); 
						$get_where_creator_media[$count_tag_m]["track"] = $get_media_track['track'];
						$new_creator_media_array[] = $get_where_creator_media[$count_tag_m];
					}
					
				}
			}
			
			$cre_frm_med = array();
			$type = 114;
			$cre_frm_med = $listob_suugest->get_media_create($type);
			
			$get_ac_songs = array_merge($get_ac_songs_1,$new_creator_creator_media_array,$new_creator_media_array,$cre_frm_med,$new_tagged_from_media_array,$new_from_from_media_array);
			
			if(count($get_ac_songs)>0)
			{
				$sort = array();
				foreach($get_ac_songs as $k=>$v)
				{			
					$end_c1 = $v['creator'];
					//$create_c = strpos($v['creator'],'(');
					//$end_c1 = substr($v['creator'],0,$create_c);
					$end_c = trim($end_c1);
					$end_f1 = $v['from'];
					//$create_f = strpos($v['from'],'(');
					//$end_f1 = substr($v['from'],0,$create_f);
					$end_f = trim($end_f1);
					
					$sort['creator'][$k] = strtolower($end_c);
					$sort['from'][$k] = strtolower($end_f);
					$sort['track'][$k] = $v['track'];
					$sort['title'][$k] = strtolower($v['title']);
				}
				//var_dump($sort);
				array_multisort($sort['creator'], SORT_ASC, $sort['from'], SORT_ASC, $sort['track'], SORT_ASC,$sort['title'], SORT_ASC,$get_ac_songs);
				$get_all_del_id = $listob_suugest->get_all_del_media_id();
				$exp_all_del_id = explode(",",$get_all_del_id);
				for($count_all=0;$count_all<=count($get_ac_songs);$count_all++)
				{
					for($count_all_del=0;$count_all_del<=count($exp_all_del_id);$count_all_del++)
					{
						if($exp_all_del_id[$count_all_del]==""){continue;}
						else{
							if($exp_all_del_id[$count_all_del]==$get_ac_songs[$count_all]['id'])
							{
								$get_ac_songs[$count_all]="";
							}
						}
					}
				}
				$dummy_pr = array();
				
				?>
				<select name="list_epm_first[]" size="5" id="list_epm_first[]" class="list" style="left:115px; position:relative;">
<?php
					for($ac_song=0;$ac_song<count($get_ac_songs);$ac_song++)
					{	
						if(in_array($get_ac_songs[$ac_song]['id'],$dummy_pr))
						{
						
						}
						else
						{
							$dummy_pr[] = $get_ac_songs[$ac_song]['id'];
						//$create_c = strpos($get_ac_songs[$ac_song]['creator'],'(');
						//$end_c = substr($get_ac_songs[$ac_song]['creator'],0,$create_c);
						$end_c = $get_ac_songs[$ac_song]['creator'];
						$end_f = $get_ac_songs[$ac_song]['from'];
						//$create_f = strpos($get_ac_songs[$ac_song]['from'],'(');
						//$end_f = substr($get_ac_songs[$ac_song]['from'],0,$create_f);
						
						$eceks = "";
						if($end_c!="")
						{
							$eceks = $end_c;
						}
						if($end_f!="")
						{
							if($end_c!="")
							{
								$eceks .= " : ".$end_f;
							}
							else
							{
								$eceks .= $end_f;
							}
						}
						if($get_ac_songs[$ac_song]['track']!="")
						{
							if($end_c!="" || $end_f!="")
							{
								$eceks .= " : ".$get_ac_songs[$ac_song]['track'];
							}
							else
							{
								$eceks .= $get_ac_songs[$ac_song]['track'];
							}
						}
						if($get_ac_songs[$ac_song]['title']!="")
						{
							if($end_c!="" || $end_f!="" || $get_ac_songs[$ac_song]['track']!="")
							{
								$eceks .= " : ".$get_ac_songs[$ac_song]['title'];
							}
							else
							{
								$eceks .= $get_ac_songs[$ac_song]['title'];
							}
						}
?>
							<option value="<?php
								if(isset($get_ac_songs[$ac_song]['table_name']) && $get_ac_songs[$ac_song]['table_name']=='general_artist') 
								{
									echo $get_ac_songs[$ac_song]['audio_id'].'~reg_art';
								}
								elseif(isset($get_ac_songs[$ac_song]['table_name']) && $get_ac_songs[$ac_song]['table_name']=='general_community')
								{
									echo $get_ac_songs[$ac_song]['audio_id'].'~reg_com';
								}
								elseif(isset($get_ac_songs[$ac_song]['table_name']) && $get_ac_songs[$ac_song]['table_name']=='general_media')
								{
									echo $get_ac_songs[$ac_song]['id'].'~reg_med';
								}?>"><?php
								if(isset($get_ac_songs[$ac_song]['audio_name']) && $get_ac_songs[$ac_song]['audio_name']!="")
								{
									echo $get_ac_songs[$ac_song]['audio_name']; 
								}
								elseif(isset($get_ac_songs[$ac_song]['title']) && $get_ac_songs[$ac_song]['title']!="")
								{
									echo $eceks; 
								}?></option>
<?php
						}	
					}
?>
				</select>
<?php
			}
			else
			{
?>
				<input class="fieldText" type="text" disabled="true" value="No Songs Found." style=" left: 115px; position: relative;"/>
<?php
			}
		}
		elseif(isset($_POST['type_val']) && $_POST['type_val']!="" && $_POST['type_val']=='videos_post')
		{
			$get_ac_videos_1 = $listob_suugest->get_all_videos($gen_log_id['general_user_id'],$gen_log_id['artist_id'],$gen_log_id['community_id']);
			
			$new_medi = $listob_suugest->get_creator_heis();
			$new_creator_creator_media_array = array();
			if(!empty($new_medi))
			{
				for($count_cre_m=0;$count_cre_m<count($new_medi);$count_cre_m++)
				{
					if($new_medi[$count_cre_m]['media_type'] == 115)
					{
						$new_creator_creator_media_array[] = $new_medi[$count_cre_m];
					}
				}
			}

			$new_medi_from = $listob_suugest->get_from_heis();
			$new_from_from_media_array = array();
			if(!empty($new_medi_from))
			{
				for($count_cre_m=0;$count_cre_m<count($new_medi_from);$count_cre_m++)
				{
					if($new_medi_from[$count_cre_m]['media_type'] == 115)
					{
						$new_from_from_media_array[] = $new_medi_from[$count_cre_m];
					}
				}
			}
			
			$new_medi_tagged = $listob_suugest->get_the_user_tagin_project($_SESSION['login_email']);
			$new_tagged_from_media_array = array();
			if(!empty($new_medi_tagged))
			{
				for($count_cre_m=0;$count_cre_m<count($new_medi_tagged);$count_cre_m++)
				{
					if($new_medi_tagged[$count_cre_m]['media_type'] == 115)
					{
						$new_tagged_from_media_array[] = $new_medi_tagged[$count_cre_m];
					}
					
				}
			}
			
			$get_where_creator_media = $listob_suugest->get_media_where_creator_or_from();
			$new_creator_media_array = array();
			if(!empty($get_where_creator_media))
			{
				for($count_tag_m=0;$count_tag_m<count($get_where_creator_media);$count_tag_m++)
				{
					if($get_where_creator_media[$count_tag_m]['media_type'] == 115)
					{
						$new_creator_media_array[] = $get_where_creator_media[$count_tag_m];
					}
				}
			}
			
			$cre_frm_med = array();
			$type = 115;
			$cre_frm_med = $listob_suugest->get_media_create($type);
			
			$get_ac_videos = array_merge($get_ac_videos_1,$new_creator_creator_media_array,$new_creator_media_array,$cre_frm_med,$new_tagged_from_media_array,$new_from_from_media_array);

			if(count($get_ac_videos)>0)
			{
				$sort = array();
				foreach($get_ac_videos as $k=>$v)
				{
				$end_c1 = $v['creator'];
					//$create_c = strpos($v['creator'],'(');
					//$end_c1 = substr($v['creator'],0,$create_c);
					$end_c = trim($end_c1);
					$end_f1 = $v['from'];
					//$create_f = strpos($v['from'],'(');
					//$end_f1 = substr($v['from'],0,$create_f);
					$end_f = trim($end_f1);
					
					$sort['creator'][$k] = strtolower($end_c);
					$sort['from'][$k] = strtolower($end_f);
					//$sort['track'][$k] = $v['track'];
					$sort['title'][$k] = strtolower($v['title']);
				}
				//var_dump($sort);
				array_multisort($sort['creator'], SORT_ASC, $sort['from'], SORT_ASC, $sort['title'], SORT_ASC,$get_ac_videos);
				$get_all_del_id = $listob_suugest->get_all_del_media_id();
				$exp_all_del_id = explode(",",$get_all_del_id);
				for($count_all=0;$count_all<=count($total);$count_all++)
				{
					for($count_all_del=0;$count_all_del<=count($exp_all_del_id);$count_all_del++)
					{
						if($exp_all_del_id[$count_all_del]==""){continue;}
						else{
							if($exp_all_del_id[$count_all_del]==$total[$count_all]['id'])
							{
								$total[$count_all]="";
							}
						}
					}
				}
				
				$dummy_pr = array();
				
				?>
				<select name="list_epm_first[]" size="5" id="list_epm_first[]" class="list" style="left:115px; position:relative;">
<?php
					for($ac_vid=0;$ac_vid<count($get_ac_videos);$ac_vid++)
					{
						if(in_array($get_ac_videos[$ac_vid]['id'],$dummy_pr))
						{
						
						}
						else
						{
							$dummy_pr[] = $get_ac_videos[$ac_vid]['id'];
						//$create_c = strpos($get_ac_videos[$ac_vid]['creator'],'(');
						//$end_c = substr($get_ac_videos[$ac_vid]['creator'],0,$create_c);
						$end_c = $get_ac_videos[$ac_vid]['creator'];
						$end_f = $get_ac_videos[$ac_vid]['from'];
						//$create_f = strpos($get_ac_videos[$ac_vid]['from'],'(');
						//$end_f = substr($get_ac_videos[$ac_vid]['from'],0,$create_f);
						
						$eceksv = "";
						if($end_c!="")
						{
							$eceksv = $end_c;
						}
						if($end_f!="")
						{
							if($end_c!="")
							{
								$eceksv .= " : ".$end_f;
							}
							else
							{
								$eceksv .= $end_f;
							}
						}
						if($get_ac_videos[$ac_vid]['title']!="")
						{
							if($end_c!="" || $end_f!="")
							{
								$eceksv .= " : ".$get_ac_videos[$ac_vid]['title'];
							}
							else
							{
								$eceksv .= $get_ac_videos[$ac_vid]['title'];
							}
						}
?>
						<option value="<?php
							if(isset($get_ac_videos[$ac_vid]['table_name']) && $get_ac_videos[$ac_vid]['table_name']=='general_artist') 
							{
								echo $get_ac_videos[$ac_vid]['video_id'].'~reg_art';
							}
							elseif(isset($get_ac_videos[$ac_vid]['table_name']) && $get_ac_videos[$ac_vid]['table_name']=='general_community')
							{
								echo $get_ac_videos[$ac_vid]['video_id'].'~reg_com';
							}
							elseif(isset($get_ac_videos[$ac_vid]['table_name']) && $get_ac_videos[$ac_vid]['table_name']=='general_media')
							{
								echo $get_ac_videos[$ac_vid]['id'].'~reg_med';
							}
							?>"><?php
							if(isset($get_ac_videos[$ac_vid]['video_name']) && $get_ac_videos[$ac_vid]['video_name']!="")
							{
								echo $get_ac_videos[$ac_vid]['video_name'];
							}
							elseif(isset($get_ac_videos[$ac_vid]['title']) && $get_ac_videos[$ac_vid]['title']!="")
							{
								echo $eceksv;
							}
							?></option>
<?php
					}
				}
?>
				</select>
<?php
			}
			else
			{
?>
				<input class="fieldText" type="text" disabled="true" value="No Videos Found." style=" left: 115px; position: relative;"/>
<?php
			}
		}
	}
?>