<?php
	session_start();
	ob_start();
	include_once('commons/db.php');
	include_once('loggedin_includes.php');
	include_once('classes/ProfileType.php');
	include_once('classes/Commontabs.php');
	include_once('classes/AddContact.php');
	include_once('classes/GeneralInfo.php');
	include_once('classes/Mails.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Purify Art</title>
<link rel="stylesheet" type="text/css" media="screen" href="includes/purify.css" />
<script src="http://jqueryjs.googlecode.com/files/jquery-1.2.6.min.js" type="text/javascript"></script>
<script src="includes/popup.js" type="text/javascript"></script>
<script src='http://ajax.googleapis.com/ajax/libs/jquery/1.6.1/jquery.min.js'></script>
<script src="includes/organictabs-jquery.js"></script>
<script>
	$(function() {

		$("#personalTab").organicTabs();
	});
</script>

<script>
var timeout	= 500;
var closetimer	= 0;
var ddmenuitem	= 0;

// open hidden layer
function mopen(id)
{	
	// cancel close timer
	mcancelclosetime();

	// close old layer
	if(ddmenuitem) ddmenuitem.style.visibility = 'hidden';

	// get new layer and show it
	ddmenuitem = document.getElementById(id);
	ddmenuitem.style.visibility = 'visible';

}
// close showed layer
function mclose()
{
	if(ddmenuitem) ddmenuitem.style.visibility = 'hidden';
}

// go close timer
function mclosetime()
{
	closetimer = window.setTimeout(mclose, timeout);
}

// cancel close timer
function mcancelclosetime()
{
	if(closetimer)
	{
		window.clearTimeout(closetimer);
		closetimer = null;
	}
}

// close layer when click-out
document.onclick = mclose; 
</script>
<script>
var ContentHeight = 700;
var TimeToSlide = 250.0;

var openAccordion = '';

function runAccordion(index)
{
  var nID = "Accordion" + index + "Content";
  if(openAccordion == nID)
    nID = '';
    
  setTimeout("animate(" + new Date().getTime() + "," + TimeToSlide + ",'" + openAccordion + "','" + nID + "')", 33);
  
  openAccordion = nID;
}

function animate(lastTick, timeLeft, closingId, openingId)
{  
  var curTick = new Date().getTime();
  var elapsedTicks = curTick - lastTick;
  
  var opening = (openingId == '') ? null : document.getElementById(openingId);
  var closing = (closingId == '') ? null : document.getElementById(closingId);
 
  if(timeLeft <= elapsedTicks)
  {
    if(opening != null)
      opening.style.height = ContentHeight + 'px';
    
    if(closing != null)
    {
      closing.style.display = 'none';
      closing.style.height = '0px';
    }
    return;
  }
 
  timeLeft -= elapsedTicks;
  var newClosedHeight = Math.round((timeLeft/TimeToSlide) * ContentHeight);

  if(opening != null)
  {
    if(opening.style.display != 'block')
      opening.style.display = 'block';
    opening.style.height = (ContentHeight - newClosedHeight) + 'px';
  }
  
  if(closing != null)
    closing.style.height = newClosedHeight + 'px';

  setTimeout("animate(" + curTick + "," + timeLeft +",'" + closingId + "','" + openingId + "')", 33);
}</script>
</head>
<body>
<div id="outerContainer">
  <?php 
$newtab=new Commontabs();
include("header.php");
?>
        <p>
</div>
</div>
    </div>
  </div>
  <div id="toplevelNav"></div>
  <div>
      <div id="profileTabs">
          <ul>
            <?php
		   $check=new AddContact();
		   
		   $new_artist=$newtab->artist_status();
		   $new_community=$newtab->community_status();
		   $new_team=$newtab->team_status();
		   
		   
		   
		  // var_dump($newres1);
		  //echo $res1['artist_id'];
		  if(($newres1['artist_id']==0 && $new_artist['status']==1) && $newres1['member_id']==0 && ($newres1['community_id']==0 && $new_community['status']==1) && ($newres1['team_id']==0 && $new_team['status']==1))
		  {		  
		  ?>
            <li class="active">PERSONAL</li>		
            <li><a href="profileedit_promotions.php">SUPPORT</a></li>
		   <?php
		   }
		   elseif(($newres1['artist_id']!=0 && $new_artist['status']==0) && ($newres1['community_id']!=0 && $new_community['status']==0) && ($newres1['team_id']!=0 && $new_team['status']==0))
		   {
		   ?>
		   
		   <li class="active">PERSONAL</li>
		   <li><a href="profileedit_media.php">Media</a></li>
		   <li><a href="profileedit_artist.php">ARTIST</a></li>
		   <!--<li><a href="profileedit_community.php">COMMUNITY</a></li>-->
		   <li><a href="profileedit_community.php">COMMUNITY</a></li>
		   <li><a href="profileedit_team.php">TEAM</a></li>
           <li><a href="profileedit_promotions.php">SUPPORT</a></li>
		   
		   <?php
		   }
		   elseif(($newres1['artist_id']!=0 && $new_artist['status']==0) && ($newres1['community_id']!=0 && $new_community['status']==0))
		   {
		   ?>
		   <li class="active">PERSONAL</li>	
		   <li><a href="profileedit_media.php">Media</a></li>		   
		   <li><a href="profileedit_artist.php">ARTIST</a></li>
		   <!--<li><a href="profileedit_community.php">COMMUNITY</a></li>-->
		   <li><a href="profileedit_community.php">COMMUNITY</a></li>
           <li><a href="profileedit_promotions.php">SUPPORT</a></li>
		   <?php
		   }
		   elseif(($newres1['artist_id']!=0 && $new_artist['status']==0) && ($newres1['team_id']!=0 && $new_team['status']==0))
		   {
		   ?>
			<li class="active">PERSONAL</li>	
			<li><a href="profileedit_media.php">Media</a></li>
		    <li><a href="profileedit_artist.php">ARTIST</a></li>
			<li><a href="profileedit_team.php">TEAM</a></li>
            <li><a href="profileedit_promotions.php">SUPPORT</a></li>
		   <?php
		   }
		   elseif(($newres1['community_id']!=0 && $new_community['status']==0) && ($newres1['team_id']!=0 && $new_team['status']==0))
		   {
		   ?>
		   <li class="active">PERSONAL</li>
		   <li><a href="profileedit_media.php">Media</a></li>
		   <!--<li><a href="profileedit_community.php">COMMUNITY</a></li>-->
		   <li><a href="profileedit_community.php">COMMUNITY</a></li>
		   <li><a href="profileedit_team.php">TEAM</a></li>
           <li><a href="profileedit_promotions.php">SUPPORT</a></li>
		   <?php
		   }
		   elseif($newres1['artist_id']!=0 && $new_artist['status']==0)
		   {?>
		    <li class="active">PERSONAL</li>
			<li><a href="profileedit_media.php">Media</a></li>			
			<li><a href="profileedit_artist.php">ARTIST</a></li>
            <li><a href="profileedit_promotions.php">SUPPORT</a></li>
		   <?php
		   }
		   elseif($newres1['member_id']!=0)
		   {
		   ?>
		   <?php
		   }
		   elseif($newres1['community_id']!=0 && $new_community['status']==0)
		   {
		   ?>
		    <li class="active">PERSONAL</li>	
			<li><a href="profileedit_media.php">Media</a></li>
			<!--<li>COMMUNITY</a></li>-->
			<li><a href="profileedit_community.php">COMMUNITY</a></li>
            <li><a href="profileedit_promotions.php">SUPPORT</a></li>
		   <?php
		   }
		   elseif($newres1['team_id']!=0 && $new_team['status']==0)
		   {
		   ?>
		    <li class="active">PERSONAL</li>
			<li><a href="profileedit_media.php">Media</a></li>
			<li><a href="profileedit_team.php">TEAM</a></li>
            <li><a href="profileedit_promotions.php">SUPPORT</a></li>
`		   <?php
		   }
		   else
		   {
		   ?>
			 <li class="active">PERSONAL</li>		
            <li><a href="profileedit_promotions.php">SUPPORT</a></li>
		   <?php
		   }
			?>
          </ul>
      </div>
<script type="text/javascript">
/*Css for aranging body */
$("document").ready(function(){
	if($(".mailcont ol")==0 || $(".mailcont p")==0)
	{
		$(".mailcont ul").css({"margin":"0px" , "padding":"0px 0px 17px 37px"});
	}
	else
	{
		$(".mailcont ul").css({"margin":"0px" , "padding":"0px 0px 17px 12px"});
	}
});

$("document").ready(function(){
	if($(".mailcont ul")==0 || $(".mailcont p")==0)
	{
		$(".mailcont ol").css({"margin":"0px" , "padding":"0px 0px 17px 37px"});
	}
	else
	{
		$(".mailcont ol").css({"margin":"0px" , "padding":"0px 0px 17px 12px"});
	}
});
/*Css for aranging body ends here*/
// $("document").ready(function(){
// var imagename = "<?php echo $newres1['image_name']; ?>";
// if(imagename=="")
// {
	// imagename="Noimage.png";
	// $('#loggedin').css({
	// backgroundImage : 'url(/uploads/profile_pic/'+ imagename +')',
	// backgroundSize :'50px',
	// backgroundRepeat: 'no-repeat',
	// backgroundPosition: 'right top'
	// });
// }
// else
// {
	// $('#loggedin').css({
	// backgroundImage : 'url(general_jcrop/croppedFiles/thumb/'+ imagename +')',
//backgroundSize :'50px'
	// backgroundRepeat: 'no-repeat',
	// backgroundPosition: 'right top'
	// });
// }

// });
</script>    
          <div id="personalTab">
            <div id="subNavigation" class="tabs">
              <ul class="nav">
                
                <li><a href="profileedit.php#addressbook">Addressbook</a></li>
				<li><a href="profileedit.php#Become_a_member">Become A Member</a></li>
                <li><a href="profileedit.php#events">Events</a></li>
                <li><a href="profileedit.php#friends">Friends &amp; Fans</a></li>
                <li><a href="profileedit.php#general">General Info </a></li>
                <li><a href="profileedit.php#mail">Mail</a></li>        
                <li><a href="profileedit.php#mymemberships">My Memberships</a></li>
                <li><a href="profileedit.php">News Feeds</a></li>
                <li><a href="profileedit.php#permissions">Permissions</a></li>
                <li><a href="profileedit.php#points">Points</a></li>                
                <li><a href="profileedit.php#registration">Registration</a></li>
                <li><a href="profileedit.php#statistics">Statistics</a></li>
                <li><a href="profileedit.php#subscrriptions">Subscriptions</a></li>
                <li><a href="profileedit.php#services">Sales &amp; Distribution</a></li>
                <li><a href="profileedit.php#transactions">Transactions</a></li>
                
              </ul>
            </div>
            <div class="list-wrap">

                <div class="subTabs" id="mail">
                	<h1>Mail</h1>
                    <div id="mediaContent">
                    	<div class="topLinks" style="border-bottom:1px dotted #999;">
                            <div class="links" style="float:left;">
                                <ul>
                                    <li><a href="compose.php">Compose</a> |</li>
                                    <li><a href="profileedit.php#mail">Inbox</a> |</li>
                                    <li><a href="drafts.php">Drafts</a> |</li>
                                    <li><a href="sent-mails.php">Sent Mails</a></li>
                                </ul>
                            </div>
                        </div>
                        
                    </div>
					<?php
					if(isset($_GET['view_mail']))
					{
						$views = new Mails();
						$view_sql = $views->viewMail($_GET['view_mail']);
						$view_run = mysql_query($view_sql);
						if(mysql_num_rows($view_run)>0)
						{
							$view_ans = mysql_fetch_assoc($view_run);
							$from_mail = $views->from($view_ans['general_user_id']);
							$from_mail_run = mysql_query($from_mail);
							$from_mail_ans = mysql_fetch_assoc($from_mail_run);
							
					?>
							<div id="mailContent">
								<div class="fieldCont">
									<div class="mailTitle">From</div>
									<div class="mailcont"><?php echo $from_mail_ans['fname'].' '.$from_mail_ans['lname']; ?></div>
								</div>
								<div class="fieldCont">
									<div class="mailTitle">Email</div>
									<div class="mailcont"><?php echo $from_mail_ans['email']; ?></div>
								</div>
								<div class="fieldCont">
									<div class="mailTitle">Date</div>
									<div class="mailcont"><?php echo $view_ans['date']; ?></div>
								</div>
								<div class="fieldCont">
									<div class="mailTitle">Body</div>
									<div class="mailcont">
										<?php echo $view_ans['mail_body']; ?>
									</div>
								</div>
								<div class="fieldCont">
									<div class="mailTitle"></div>
									<input type="submit" class="register" value="Reply" onClick="mail_reply(<?php echo $_GET['view_mail']; ?>)"/>
									<input type="submit" class="register" value="Forward" onClick="mail_forward(<?php echo $_GET['view_mail']; ?>)"/>
								</div>
							</div>
					<?php
						}
					}
					

					if(isset($_GET['view_mail_draft']))
					{
						$views = new Mails();
						$view_sql = $views->viewMail($_GET['view_mail_draft']);
						$view_run = mysql_query($view_sql);
						if(mysql_num_rows($view_run)>0)
						{
							$view_ans = mysql_fetch_assoc($view_run);
							$from_mail = $views->from($view_ans['general_user_id']);
							$from_mail_run = mysql_query($from_mail);
							$from_mail_ans = mysql_fetch_assoc($from_mail_run);
							$exp_draft_mail = explode('_',$view_ans['related_mail_id']);
					?>
							<div id="mailContent">
								<div class="fieldCont">
									<div class="mailTitle">To</div>
									<div class="mailcont"><?php 
									if(isset($exp_draft_mail[1]) && $exp_draft_mail[1]!=0) 
									{
										$draft_counts_org = $exp_draft_mail[1] + 1;
										if($view_ans['users']=='custom_users')
										{
											echo "Custom";
										}
										elseif($view_ans['users']=='all_users')
										{
											echo "All";
										}
										elseif($view_ans['users']=='artist_users')
										{
											echo "Artist";
										}
										elseif($view_ans['users']=='community_users')
										{
											echo "Community";
										}
										elseif($view_ans['users']=='purifyart_fans')
										{
											echo "Fans";
										}
										echo ' ('.$draft_counts_org.')';
									}
									elseif(isset($exp_draft_mail[1]) && $exp_draft_mail[1]==0)
									{
										echo $view_ans['to'];
									} ?></div>
								</div>
								<div class="fieldCont">
									<div class="mailTitle">Subject</div>
									<div class="mailcont"><?php echo $view_ans['subject'];  ?></div>
								</div>
								<div class="fieldCont">
									<div class="mailTitle">Date</div>
									<div class="mailcont"><?php echo $view_ans['date']; ?></div>
								</div>
								<div class="fieldCont">
									<div class="mailTitle">Body</div>
									<div class="mailcont">
										<?php echo $view_ans['mail_body']; ?>
									</div>
								</div>
								<div class="fieldCont">
									<div class="mailTitle"></div>
											<input type="submit" class="register" value="Send" onClick="mail_send_draft(<?php echo $_GET['view_mail_draft']; ?>)"/>
								</div>
							</div>
					<?php
						}
					}
					
					if(isset($_GET['view_mail_sent']))
					{
						$views = new Mails();
						$view_sql = $views->viewMail($_GET['view_mail_sent']);
						$view_run = mysql_query($view_sql);
						if(mysql_num_rows($view_run)>0)
						{
							$view_ans = mysql_fetch_assoc($view_run);
							$from_mail = $views->from($view_ans['general_user_id']);
							$from_mail_run = mysql_query($from_mail);
							$from_mail_ans = mysql_fetch_assoc($from_mail_run);
							$exp_draft_mail = explode('_',$view_ans['related_mail_id']);
							
					?>
							<div id="mailContent">
								<div class="fieldCont">
									<div class="mailTitle">To</div>
									<div class="mailcont"><?php
									if(isset($exp_draft_mail[1]) && $exp_draft_mail[1]!=0) 
									{
										$draft_counts_org = $exp_draft_mail[1] + 1;
										if($view_ans['users']=='custom_users')
										{
											echo "Custom";
										}
										elseif($view_ans['users']=='all_users')
										{
											echo "All";
										}
										elseif($view_ans['users']=='artist_users')
										{
											echo "Artist";
										}
										elseif($view_ans['users']=='community_users')
										{
											echo "Community";
										}
										elseif($view_ans['users']=='purifyart_fans')
										{
											echo "Fans";
										}
										echo ' ('.$draft_counts_org.')';
									}
									elseif(isset($exp_draft_mail[1]) && $exp_draft_mail[1]==0)
									{
										echo $view_ans['to'];
									}
									?></div>
								</div>
								<div class="fieldCont">
									<div class="mailTitle">Subject</div>
									<div class="mailcont"><?php echo $view_ans['subject'];  ?></div>
								</div>
								<div class="fieldCont">
									<div class="mailTitle">Date</div>
									<div class="mailcont"><?php echo $view_ans['date']; ?></div>
								</div>
								<div class="fieldCont">
									<div class="mailTitle">Body</div>
									<div class="mailcont">
										<?php echo $view_ans['mail_body']; ?>
									</div>
								</div>
								<div class="fieldCont">
									<div class="mailTitle"></div>
									<?php
									if($view_ans['attachments']=="")
									{
									?>
										<input type="submit" class="register" value="Reply" onClick="mail_send_sent(<?php echo $_GET['view_mail_sent']; ?>,'1')" />
									<?php
									}
									else
									{
									?>
										<input type="submit" class="register" value="Reply" onClick="mail_send_sent(<?php echo $_GET['view_mail_sent']; ?>,'0')" />
									<?php
									}
									?>
								</div>
							</div>
					<?php
						}
					}
					?>
					
                    <!--<p><strong>Use the general Mail function from Cypress Groove with the following differences:</strong></p>
     				<p><strong>        1. 
        Send out all emails in regualar design format rather than the format used for Cypress Groove Emails. <br />
        2. &quot;Remove Members&quot; and &quot;Unregistered Contacts&quot; from the user categories<br />
        3. Add &quot;Out of Network&quot; to the user categories<br />
        4. Change &quot;Add Image&quot; field to &quot;Attachments&quot; field where user can select from original media that they have the right to share.<br />
        5. Add Compose, Drafts, Inbox, and Sent tabs on top of form<br />
      6. When a user mails a registered user the message will be sent to their Purify Inbox as well as the users email address.</strong></p>-->
      			</div>
                
                
                
               </div>
          </div>  
          
           
          
          
   </div>
</div>
<div style="float:left; width:100%; clear:both; margin-top:100px;">
 <div id="footer"> <strong>&copy; Copyright 2012 Purify Art</strong> <br />
    <a href="Demo/about.html">ABOUT US</a> / <a href="Demo/about_affiliates.html">AFFILIATE PROGRAM</a> / <a href="/Demo/about_agreements.html">TERMS &amp; PRIVACY</a> / <a href="Demo/about_services.html">SERVICES</a> / <a href="help_faq.html">FAQ</a> / <a href="help_pricing.html">PRICING</a></div>
</div>
  <!-- end of main container -->
</div>
<div id="popupContact">
		<a id="popupContactClose">x</a>
		<p id="contactArea">
			Only registered users can view media. 
		</p>
        <h1>Register <a href="index.php">Here</a>, It's Free!</h1><br />
        <p>Already Registered ? Login below.</p>
        <div class="formCont">
            <div class="formFieldCont">
        	<div class="fieldTitle">Username</div>
            <input type="text" class="textfield" />
        </div>
       		<div class="formFieldCont">
        	<div class="fieldTitle">Password</div>
            <input type="password" class="textfield" />
        </div>
        	<div class="formFieldCont">
        	<div class="fieldTitle"></div>
            <input type="image" class="login" src="images/profile/login.png" width="47" height="23" />
        </div>
        </div>
	</div>
<div id="backgroundPopup"></div>
</body>
</html>
<script>
	
	function mail_reply(reply)
	{
		<?php
			if(isset($_GET['check_attach_remove']))
			{
		?>
				document.location = "compose.php?reply="+reply+"&check_attach_remove";
		<?php
			}
			else
			{
		?>
				document.location = "compose.php?reply="+reply;
		<?php
			}
		?>
	}
	
	function mail_forward(forward)
	{
		<?php
			if(isset($_GET['check_attach_remove']))
			{
		?>
				document.location = "compose.php?forward="+forward+"&check_attach_remove";
		<?php
			}
			else
			{
		?>
				document.location = "compose.php?forward="+forward;
		<?php
			}
		?>
	
	}
	
	function mail_send_sent(send_sent,$dummy_check)
	{
		//alert($dummy_check);
		//return false;
		if($dummy_check=='1')
		{
			document.location = "compose.php?send_sent="+send_sent+"&check_attach_remove";
		}
		else
		{
			document.location = "compose.php?send_sent="+send_sent;
		}
	}
	
	function mail_send_draft(send_draft)
	{
		<?php
			if(isset($_GET['check_attach_remove']))
			{
		?>
				document.location = "compose.php?send_draft="+send_draft+"&check_attach_remove";
		<?php
			}
			else
			{
		?>	
				document.location = "compose.php?send_draft="+send_draft;
		<?php
			}
		?>
		
	}
	
</script>