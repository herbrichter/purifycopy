<?php
//session_start();
	//ob_start();
	include_once('commons/db.php');
	include_once('loggedin_includes.php');
	include_once('classes/Get_Transactions.php');
    include_once('classes/Commontabs.php');
    
    $trans = new Get_Transactions();
    
    $_SESSION['login_email']='ecarell@sugomusic.com';
    $_SESSION['login_id']='46';
    $domainname=$_SERVER['SERVER_NAME'];
    
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en" />

<head>
	<meta https-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="stylesheet" href="css/nivo/themes/default/default.css" type="text/css" media="screen" />
<link rel="stylesheet" href="css/nivo/themes/light/light.css" type="text/css" media="screen" />
<link rel="stylesheet" href="css/nivo/themes/dark/dark.css" type="text/css" media="screen" />
<link rel="stylesheet" href="css/nivo/themes/bar/bar.css" type="text/css" media="screen" />
<link rel="stylesheet" href="css/nivo/nivo-slider.css" type="text/css" media="screen" />
<link rel="stylesheet" href="css/nivo/slide_nivo_style.css" type="text/css" media="screen" />
<link rel="stylesheet" type="text/css" media="screen" href="includes/purify.css" />
<link rel="stylesheet" type="text/css" media="screen" href="css/style.css" />
<link rel="stylesheet" type="text/css" href="style.css" />
<link rel="stylesheet" href="includes/light.css">
<script type="text/javascript" src="javascripts/country_state.js"></script>
<link rel="stylesheet" href="engine/css/vlightbox.css" type="text/css" />
<style type="text/css">#vlightbox a#vlb{display:none}</style>
<link rel="stylesheet" href="engine/css/visuallightbox.css" type="text/css" media="screen" />
<script src="engine/js/jquery.min.js" type="text/javascript"></script>
<script type="text/javascript" src="includes/mini_header.js"></script>

<script language="javascript" type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
<!--<script type="text/javascript" src="includes/jquery.min.js"></script>-->
<script type="text/javascript" src="includes/light.js"></script>
    
	<title>Purify Art: Personal Profile</title>
	<script src="includes/jquery.js"></script>

<?php
	$newtab=new Commontabs();

//error_reporting(0);

//include_once("classes/GetCartData.php");
//$get_cart_obj = new GetCartData();
//include_once("avail_media_chks_tabs.php");
?>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" href="css/nivo/themes/default/default.css" type="text/css" media="screen" />
<link rel="stylesheet" href="css/nivo/themes/light/light.css" type="text/css" media="screen" />
<link rel="stylesheet" href="css/nivo/themes/dark/dark.css" type="text/css" media="screen" />
<link rel="stylesheet" href="css/nivo/themes/bar/bar.css" type="text/css" media="screen" />
<link rel="stylesheet" href="css/nivo/nivo-slider.css" type="text/css" media="screen" />
<link rel="stylesheet" href="css/nivo/slide_nivo_style.css" type="text/css" media="screen" />
<link rel="stylesheet" type="text/css" media="screen" href="includes/purify.css" />
<link rel="stylesheet" type="text/css" media="screen" href="css/style.css" />
<link rel="stylesheet" type="text/css" href="style.css" />
<link rel="stylesheet" href="includes/light.css">
<script type="text/javascript" src="javascripts/country_state.js"></script>
<link rel="stylesheet" href="engine/css/vlightbox.css" type="text/css" />
<style type="text/css">#vlightbox a#vlb{display:none}</style>
<link rel="stylesheet" href="engine/css/visuallightbox.css" type="text/css" media="screen" />
<script src="engine/js/jquery.min.js" type="text/javascript"></script>
<script type="text/javascript" src="includes/mini_header.js"></script>
<script language="javascript" type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
<script type="text/javascript" src="includes/light.js"></script>

</head>

<?php
$newres1 = $newtab->tabs();
if ($domainname=='purifyart.com'||$domainname=='www.purifyart.com') {
    $staging='';
} elseif ($domainname=='purifystaging.com'||$domainname=='www.purifystaging.com'){
    $staging='-staging';
} elseif ($domainname=='purifycopy.com'||$domainname=='www.purifycopy.com'){
    $staging='-pcopy';
} elseif ($domainname=='localhost'){
    $staging='-localhost';
} else {echo "server name is ".$domainname; die();}
?>

    <div class="wrapper">
		<div class="header">
		<div class="topBorder"></div>
    	<div class="logo"><a href="https://<?php echo $domainname; ?>/index.php"><img src="images/purelogo-19046<?php echo $staging; ?>.png" title="Purify Art Logo" alt="Logo of Purify Art" /></a></div>

        <div style="float:left; width:42px;">
		<a href="/profileedit.php"><img src="//generalproimage.s3.amazonaws.com/<?php if($newres1['image_name']==""){ echo "Noimage.png";} else {echo $newres1['image_name'];} ?>" width="38" height="38" id="profilePic" style="margin:4px 4px 0 4px;"/></a></div>
		<div style="float:left; left: 10px; position: relative; top: 13px; color: #000000; font-size: 14px; line-height: 15px; width:99px;">
		<p id="userName">
			<a href="/profileedit.php">
	<?php
				if($newres1['fname']=="" && $newres1['lname']=="")
				{
					//echo $newres1.''. $newres2;
					echo $newres1;
				}
				else
				{
					if(strlen($newres1['fname'])>=16)
					{
						echo substr($newres1['fname'],0,13)."...";
					}
					else
					{
						echo $newres1['fname'];
					}
					if(strlen($newres1['fname']<16))
					{
						$type_length = strlen($newres1['fname']) + 1;
						if($type_length + strlen($newres1['lname'])>=16)
						{
							echo " ".substr($newres1['lname'],0,13 - $type_length)."...";
						}
						else
						{
							echo " ".$newres1['lname'];
						}
					}
				}
				
				$all_urls = $newtab->get_all_urls();
				
				$fan_var = 0;
				$member_var = 0;
				$new_artist = $newtab->artist_status();
				$new_community = $newtab->community_status();
				$new_team = $newtab->team_status();
				
				$sql_fan_chk = mysql_query("SELECT * FROM fan_club_membership WHERE email='".$_SESSION['login_email']."' AND accept=1");
				if($sql_fan_chk!="" && $sql_fan_chk!=NULL)
				{
					if(mysql_num_rows($sql_fan_chk)>0)
					{
						while($row_fan_chk = mysql_fetch_assoc($sql_fan_chk))
						{
							$fan_var = $fan_var + 1;
						}
					}
				}
				
				$gen_det = mysql_query("SELECT * FROM general_user WHERE email='".$_SESSION['login_email']."'");
				if($gen_det!="" && $gen_det!=NULL)
				{
					if(mysql_num_rows($gen_det)>0){
						$res_gen_det = mysql_fetch_assoc($gen_det);
					}
				}
				
				$date = date("Y-m-d");
				$sql_member_chk = mysql_query("SELECT * FROM purify_membership WHERE general_user_id='".$res_gen_det['general_user_id']."' AND (expiry_date>'".$date."' OR lifetime=1)");
				if($sql_member_chk!="" && $sql_member_chk!=NULL)
				{
					if(mysql_num_rows($sql_member_chk)>0)
					{
						$member_var = $member_var + 1;
					}
				}
	?>
			</a>
		</p>
		</div>
        <div class="dropdownSettings" style="width:0px; float:right;"> <a class="accountSettings" ><img src="images/actions_.jpg" width="44" height="24" /></a>
			<div class="submenuSettings">
				<ul class="rootSettings">
					<li ><a href="profileedit.php">Home</a></li>
					<?php
						$scriptfilename=$_Server['DOCUMENT_ROOT'].'/profile.php';
						if($_SERVER['SCRIPT_FILENAME']==$scriptfilename)
						{
							$exp_uris = explode('/',$_SERVER['REQUEST_URI']);
							if(in_array($exp_uris[1],$all_urls))
							{
								if($artist_check!=0)
								{
									$edit_url = 'profileedit_artist.php';
								}
								elseif($community_check!=0)
								{
									$edit_url = 'profileedit_community.php';
								}
								elseif($community_event_check!=0)
								{
									$edit_url = 'add_community_event.php?id='.$community_event_check['id'];
								}
								elseif($artist_event_check!=0)
								{
									$edit_url = 'add_artist_event.php?id='.$artist_event_check['id'];
								}
								elseif($artist_project_check!=0)
								{
									$edit_url = 'add_artist_project.php?id='.$artist_project_check['id'];
								}
								elseif($community_project_check!=0)
								{
									$edit_url = 'add_community_project.php?id='.$community_project_check['id'];
								}
					?>
								<li ><a href="<?php echo $edit_url; ?>" id="edit_profile_page">Edit Page</a></li>
					<?php
							}
						}
						
						if(($newres1['artist_id']!=0 && $new_artist['status']==0 && $new_artist['active']==0) || ($newres1['community_id']!=0 && $new_community['status']==0 && $new_community['active']==0) || ($newres1['team_id']!=0 && $new_team['status']==0) || $member_var!=0 || $fan_var!=0)
						{
					?>
							<li ><a href="profileedit_media.php" id="edit_profile_page">Media Library</a></li>
					<?php
						}
						if(($newres1['artist_id']!=0 && $new_artist['status']==0 && $new_artist['active']==0) || ($newres1['community_id']!=0 && $new_community['status']==0 && $new_community['active']==0) || ($newres1['team_id']!=0 && $new_team['status']==0) || $member_var!=0)
						{
					?>	
							<li ><a href="add_media.php" id="edit_profile_page">Add Media</a></li>
					<?php
						}
						if(($newres1['artist_id']!=0 && $new_artist['status']==0 && $new_artist['active']==0) || ($newres1['community_id']!=0 && $new_community['status']==0 && $new_community['active']==0)){
							if(($newres1['community_id']!=0 && $new_community['status']==0 && $new_community['active']==0) && ($newres1['artist_id']==0 || $new_artist['status']==1 || $new_artist['active']==1))
							{
							?>	
									<li ><a href="profileedit_community.php#promotions" id="edit_profile_page">Promote</a></li>
							<?php
							}else{
							?>	
									<li ><a href="profileedit_artist.php#promotions" id="edit_profile_page">Promote</a></li>
							<?php
							}
						}
					?>
					<li ><a href="logout.php" >Logout</a></li>
				</ul>
			</div>
		</div>
    </div>
</div>


<!-- CSS Files -->
<link rel="stylesheet" href="includes/jquery.ui.all.css">
<link rel="stylesheet" href="css/jquery.autocomplete.css" type="text/css" />
<link  rel="stylesheet" type="text/css" media="screen" href="css/jquery.ui.autocomplete.css"></link>
<!-- Java Script Files-->
<script type="text/javascript" src="javascripts/checkForm.js"></script>

<!--

 Java Script Files Ends Here -->

<!--files for search technique ends here -->
<script src="ui/jquery-1.7.2.js"></script>
<script src="includes/jquery.ui.core.js"></script>
<script src="includes/jquery.ui.widget.js"></script>

<script type="text/javascript" src="javascripts/jquery.ui.position.js"></script>
<script type="text/javascript" src="javascripts/jquery.ui.autocomplete.js"></script>

<!----------------------------- NEW FANCY BOX ---------------------->
<script type="text/javascript" src="new_fancy_box/jquery.fancybox.js?v=2.1.4"></script>
<script type="text/javascript" src="new_fancy_box/helpers/jquery.fancybox-media.js?v=1.0.5"></script>

<script src="includes/jquery.ui.datepicker.js"></script>
<script src="includes/organictabs-jquery.js"></script>
<script src="javascript.js"> </script>
<script type="text/javascript"></script>

<link rel="stylesheet" type="text/css" media="screen" href="includes/multiple_select.css"/>
<script type="text/javascript" src="uploadify/swfobject.js"></script>
<script type="text/javascript" src="uploadify/jquery.uploadify.v2.1.4.min.js"></script>
<link href="uploadify/uploadify.css" rel="stylesheet"/>
<script type="text/javascript" src="ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="ckeditor.js"></script>
<script type="text/javascript" src="jquery.js"></script>
<script src="sample.js" type="text/javascript"></script>
<link href="sample.css" rel="stylesheet" type="text/css" />
<script src="ui/jquery.ui.mouse.js"></script>
<script src="ui/jquery.ui.resizable.js"></script>
<script src="chk_community_list_event.js" type="text/javascript"></script>
<script type="text/javascript" src="ZeroClipboard.js" ></script>
<script type="text/javascript" src="../javascripts/jquery.nivo.slider.js"></script>
<body>    
    
<link href="/galleryfiles/gallery.css" rel="stylesheet"/>
<script src="/galleryfiles/jquery.easing.1.3.js"></script>
<script src="/includes/functionsmedia.js" type="text/javascript"></script>
<script src="/includes/functionsURL.js" type="text/javascript"></script>
<script src="/includes/mediaelement-and-player.min.js"></script>
<link rel="stylesheet" href="/includes/mediaelementplayer.min.css" />

<link rel="stylesheet" href="../lightbox/prettyPhoto.css" type="text/css" media="screen" title="prettyPhoto main stylesheet" charset="utf-8" />
<script src="../lightbox/jquery.prettyPhoto.js" type="text/javascript" charset="utf-8"></script>
<script src="../javascripts/popup.js" type="text/javascript" charset="utf-8"></script>

<div id="outerContainer">



				<div class="subTabs" id="transactions" ><!---->
                	<h1>Ean Carell Transactions</h1>
                    <h2></h2>
                    <div style="width:665px; float:left;">
                        <div id="actualContent">
						<?php
							$all_stores = array();
							$meds_buy = array();
							$fans_buy = array();
							$donate_field = array();
							$meds_don = 0;
							
							$whole_profit = 0;
							$all_sales = $trans->get_all_trans();
                            //print_r($all_sales);
							if($all_sales!="" && $all_sales!=NULL)
							{
								if(mysql_num_rows($all_sales)>0)
								{
									$no = 1;
									
									while($rows = mysql_fetch_assoc($all_sales))
									{
										$total_profit = 0;
										$total_profit_do = 0;
										$exp_fee = explode(',',$rows['paypal_fee']);
										/* $total_trnc = ($rows['total'] * 0.05) + 0.05;
										$Paypal_Fee = round($total_trnc); */
										if($rows['total']!=0)
										{
											if($rows['price']!=0)
											{
												$purify_fee = round(($rows['price'] - $exp_fee[0]) * 0.05, 2);
												/* if($purify_fee<=0)
												{
													$purify_fee = 1;
												} */
												
												$total_profit = $rows['price'] - $exp_fee[0] - $purify_fee;
											}
											$buy = $trans->get_buyer($rows['buyer_id']);
											
											$get_media = $trans->get_media($rows['media_id']);
											
											if(mysql_num_rows($get_media)>0)
											{
												if($rows['price']!=0)
												{
													$meds_buy[$meds_don] = $rows;
												}
												if($rows['donate']!=0)
												{
													$purify_fee = round(($rows['donate'] - $exp_fee[1]) * 0.05, 2);
													$total_profit_do = $rows['donate'] - $exp_fee[1] - $purify_fee;
													
													$donate_field[$meds_don] = $rows;
													$donate_field[$meds_don]['donate_field'] = 'yes';
												}
												else
												{
													$total_profit_do = 0;
												}
												
												$whole_profit = $whole_profit + $total_profit + $total_profit_do;
												$meds_don = $meds_don + 1;
											}
										}
									}
								}
							}
							
							$all_fan_sales = $trans->get_all_fan_trans();
							if($all_fan_sales!="" && $all_fan_sales!=NULL)
							{
								if(mysql_num_rows($all_fan_sales)>0)
								{
									$no = 1;
									$fan_don = $meds_don;
									while($rows = mysql_fetch_assoc($all_fan_sales))
									{
										$total_profit_do = 0;
										/* $total_trnc = ($rows['total'] * 0.05) + 0.05;
										$Paypal_Fee = round($total_trnc); */
										$exp_fee = explode(',',$rows['paypal_fee']);
										
										$purify_fee = round(($rows['price'] - $exp_fee[0]) * 0.05, 2);
										$total_profit = $rows['price'] - $exp_fee[0] - $purify_fee;
										/* if($purify_fee<=0)
										{
											$purify_fee = 1;
										} */
										
										//$total_profit = $rows['total'] - $rows['paypal_fee'] - $purify_fee;
										
										//$buy = $trans->get_buyer($rows['buyer_id']);
										
										//$get_media = $trans->get_media($rows['media_id']);
										//if(mysql_num_rows($get_media)>0)
										//{
											
											//$whole_profit = $whole_profit + $total_profit;
											$fans_buy[$fan_don] = $rows;
											if($rows['donate']!=0)
											{
												$purify_fee = round(($rows['donate'] - $exp_fee[1]) * 0.05, 2);
												$total_profit_do = $rows['donate'] - $exp_fee[1] - $purify_fee;
													
												$donate_field[$fan_don] = $rows;
												$donate_field[$fan_don]['donate_field'] = 'yes';
											}
											else
											{
												$total_profit_do = 0;
											}
										//}
										$whole_profit = $whole_profit + $total_profit + $total_profit_do;
										$fan_don = $fan_don + 1;
									}
								}
							}
							
							if($whole_profit!=0)
							{
								$sql_got = $trans->get_bal();
								if($sql_got!="" && $sql_got!=NULL)
								{
									if(mysql_num_rows($sql_got)>0)
									{
										$amt_got = 0;
										while($rgot = mysql_fetch_assoc($sql_got))
										{
											$amt_got = $amt_got + $rgot['amount'];
										}
										
										//echo $amt_got;
										//echo $whole_profit;
										
										$org_pro = round($amt_got - $whole_profit, 2);
										//echo $org_pro;
										if($org_pro<0)
										{
											$org_pro = $org_pro * (-1);
										}
									}
									else
									{
										$org_pro = $whole_profit;
									}
								}
								else
								{
									$org_pro = $whole_profit;
								}

							}
							else
							{
								$org_pro = 0;
							}
							
							//echo $whole_profit;
							//echo $amt_got;
							//echo $org_pro;
							
							$total_rece = 0;
							$no_chks_fees = 0;
							$sql_re_bal = $trans->get_received_bal();
							if(mysql_num_rows($sql_re_bal)>0)
							{
								while($row_bal = mysql_fetch_assoc($sql_re_bal))
								{
									$total_rece = $total_rece + $row_bal['amount'] - 10;
								}
							}
							
							$sql_chk_bal = $trans->get_bal();
							if(mysql_num_rows($sql_chk_bal)>0)
							{
								while($row_bal = mysql_fetch_assoc($sql_chk_bal))
								{
									$no_chks_fees = $no_chks_fees + 1;
								}
							}
							$no_chks_fees = $no_chks_fees * 10;
							
							$all_stores = array_merge($meds_buy,$fans_buy,$donate_field);
							//print_r($all_stores);
							$sort = array();
							foreach($all_stores as $k=>$v)
							{
								//if(isset($v['name']) && $v['name']!="")
								//{
								if(isset($v['chk_date']) && $v['chk_date']!="")
								{
									$sort['chk_date'][$k] = strtotime($v['chk_date'].' '.$v['time_chk']);
								}
								elseif(isset($v['date']) && $v['date']!="")
								{
									$sort['chk_date'][$k] = strtotime($v['date'].' '.$v['time_chk']);
								}
								elseif(isset($v['purchase_date']) && $v['purchase_date']!="")
								{
									
									$sort['chk_date'][$k] = strtotime($v['purchase_date']);
								}
								//}									
							}
							//var_dump($sort);
							if(!empty($sort))
							{
								array_multisort($sort['chk_date'], SORT_DESC,$all_stores);
							}
							$hasing = count($all_stores);
						?>                            
							<input type="hidden" value="<?php echo $org_pro; ?>" id="whole" name="whole" />
                            <p style="font-size:18px; line-height:14px;">Balance: $<?php echo $org_pro; ?></p>
							<p style="line-height:14px;">
								Total Sales Profits - $<?php echo $whole_profit; ?> | Total Payments Received - $<?php echo $total_rece; ?> | Total Check Fees - $<?php echo $no_chks_fees; ?>
							</p>
                            <div class="fieldCont">
                                <a title="Request a payment to be sent to you from you current balance. As you make sales your balance goes up." onClick="return chk_amts();" id="req_pay" style="cursor: pointer; text-decoration:none; background-color: #000000; border: 0 none; color: #FFFFFF; float: left; font-family: 'futura-pt',sans-serif; font-size: 0.825em; font-weight: bold; margin-right: 10px; padding: 6px 10px; border-radius:5px; -webkit-border-radius:5px; -moz-border-radius:5px; -ms-border-radius:5px; -o-border-radius:5px;">Request Payment</a>
								<a id="req_pay_new" style="display:none; text-decoration:none; background-color: #000000; border-radius:5px; -webkit-border-radius:5px; -moz-border-radius:5px; -ms-border-radius:5px; -o-border-radius:5px; border: 0 none; color: #FFFFFF; float: left; font-family: 'futura-pt',sans-serif; font-size: 0.825em; font-weight: bold; margin-right: 10px; padding: 6px 10px;" href="request_payment.php?amt=<?php echo $org_pro; ?>" class="fancybox fancybox.ajax">Request Payment</a>
								<a title="Request a payment to be sent to you from you current balance. As you make sales your balance goes up." href="exports_scvs_ean.php" id="req_pay" style="border-radius:5px; -webkit-border-radius:5px; -moz-border-radius:5px; -ms-border-radius:5px; -o-border-radius:5px; cursor: pointer; text-decoration:none; background-color: #000000; border: 0 none; color: #FFFFFF; float: left; font-family: 'futura-pt',sans-serif; font-size: 0.825em; font-weight: bold; margin-right: 10px; padding: 6px 10px;">Download Sales Data</a>
                            </div>
                        </div>
                        <div id="mediaContent">
                            <div class="topLinks">
                                <div class="links" style="float:left;">
                                    
                                    <select class="dropdown" onChange="handleSelection(value)" style="margin-left:0;">
                                        <option value="salesLog">Sales Log</option>
                                        <option value="total">Totals</option>
										<option value="payments">Payments</option>
                                    </select>
                                    <ul>
                                    	<!--<li>|</li>-->
                                        <li></li>
                                        
                                    </ul>
                                </div>
                            </div>
                            <div id="salesLog">
                                
                                    <div class="titleCont">
                                        <div class="blkC" style="width:40px;">#</div>
                                        <div class="blkE">Seller</div>
                                        <div class="blkG">Profits</div>
                                        <div class="blkF">Type</div>
                                        <div class="blkD">Date</div>
                                        
                                        
                                    </div>
									<div style="position:relative;float:left;height:466px;overflow-x:hidden;overflow-y:auto;">
									<?php
										$sort = array();
                                        //echo '<BR> one <br>';
                                        //print_r($all_stores);
										foreach($all_stores as $k=>$v)
										{
											//if(isset($v['name']) && $v['name']!="")
											//{
												$sort['chk_date'][$k] = strtotime($v['chk_date'].' '.$v['time_chk']);
											//}									
										}
										
										if($sort!="" && !empty($sort))
										{
											array_multisort($sort['chk_date'], SORT_DESC,$all_stores);
										}
								
										$all_sales = $trans->get_all_trans();
                                        //echo '<BR> two <br>';
                                        //print_r($all_stores);
										//if(mysql_num_rows($all_sales)>0)
										$no = 1;
										$all_crets = "";
										$hasing = count($all_stores);
										
										for($se=0;$se<count($all_stores);$se++)
										{
											if(isset($all_stores[$se]['id']))
											{
											//while($rows = mysql_fetch_assoc($all_sales))
											//{
												//$total_trnc = ($rows['total'] * 0.05) + 0.05;
												//$Paypal_Fee = round($total_trnc);
												$exp_fee = explode(',',$all_stores[$se]['paypal_fee']);
												
												$floatNum = $exp_fee[0];
												$length = strlen($floatNum);

												$pos = strpos($floatNum, "."); // zero-based counting.

												$num_of_dec_places = ($length - $pos) - 1;
												/* echo $length;
												echo $pos;
												echo $num_of_dec_places; */
												if($num_of_dec_places==1)
												{
													$exp_fee[0] = $exp_fee[0].'0';
												}
												
												$purify_fee = round(($all_stores[$se]['price'] - $exp_fee[0]) * 0.05, 2);
												$purify_fee_2 = round(($all_stores[$se]['donate'] - $exp_fee[1]) * 0.05, 2);
												/* if($purify_fee<=0)
												{
													$purify_fee = 1;
												} */
												
												if(isset($all_stores[$se]['donate_field']))
												{
													$total_profit = $all_stores[$se]['donate'] - $purify_fee_2 - $exp_fee[1];
												}
												else
												{
													$total_profit = $all_stores[$se]['price'] - $exp_fee[0] - $purify_fee;
												}
												
												$float_to = $total_profit;
												$length = strlen($float_to);

												$pos = strpos($float_to, "."); // zero-based counting.

												$num_of_dec_places = ($length - $pos) - 1;
												/* echo $length;
												echo $pos;
												echo $num_of_dec_places; */
												if($num_of_dec_places==1)
												{
													$total_profit = $total_profit.'0';
												}
												elseif($pos==0)
												{
													$total_profit = $total_profit.'.00';
												}
												
												$buy = $trans->get_buyer($all_stores[$se]['buyer_id']);
												
												if(isset($all_stores[$se]['media_id']))
												{
													$get_media = $trans->get_media($all_stores[$se]['media_id']);
													if(mysql_num_rows($get_media)>0)
													{
														$gets_meds = mysql_fetch_assoc($get_media);
														$get_creator = $trans->get_creators($gets_meds['creator_info']);
													}
												}
												else
												{
													$gets_meds = "";
													if($all_stores[$se]['table_name']=='artist_project' || $all_stores[$se]['table_name']=='community_project')
													{
														
														$vars = $all_stores[$se]['table_name'].'|'.$all_stores[$se]['type_id'];
														
														$get_vars = $trans->get_creators($vars);
														$get_creator = $trans->get_creators($get_vars['creators_info']);
													}
													elseif($all_stores[$se]['table_name']=='general_artist' || $all_stores[$se]['table_name']=='general_community')
													{
														$vars = $all_stores[$se]['table_name'].'|'.$all_stores[$se]['type_id'];
														$get_creator = $trans->get_creators($vars);
													}
												}
									?>
									
										<div class="tableCont" id="AccordionContainer">
											<div class="blkC"><?php echo $hasing; ?></div>
											<div class="blkE"><?php 
											if($get_creator!="")
											{ 
												if(isset($get_creator['name']))
												{
													echo $get_creator['name'];
												}
												elseif(isset($get_creator['title']))
												{
													echo $get_creator['title'];
												}
											}else{ ?>&nbsp;<?php }
											?></div>
											<div class="blkG" style="width:80px;">$<?php echo $total_profit; ?></div>
											<div class="blkF" style="width:138px;"><?php
											//echo '<BR> three <br>';
                                            //print_r($gets_meds);
                                            if($gets_meds!=""){
											    if($gets_meds['media_type']==114)
											    {
												    if(isset($all_stores[$se]['donate_field']))
												    {
													    echo "Tip";
												    }
												    else
												    {
													    echo "Song";
												    }
											    }
											    elseif($gets_meds['media_type']==115)
											    {
												    if(isset($all_stores[$se]['donate_field']))
												    {
													    echo "Tip";
												    }
												    else
												    {
													    echo "Video";
												    }
											    }
											    elseif($gets_meds['media_type']==113)
											    {
												    if(isset($all_stores[$se]['donate_field']))
												    {
													    echo "Tip";
												    }
												    else
												    {
													    echo "Gallery";
												    }
											    }
                                            }else{
												if(isset($all_stores[$se]['donate_field']))
												{
													echo "Tip";
												}
												else
												{
													if($all_stores[$se]['fan_member_id']==0)
													{
														
														echo "Project";
													}
													else
													{
														echo "Membership";
													}
												}
											}
                                            

											?></div>
											<div class="blkB" style="width:145px;"><?php echo date("m.d.y", strtotime($all_stores[$se]['chk_date']));
											//$all_stores[$se]['chk_date']; ?></div>
											<div class="blkC" onclick="runAccordion('<?php echo $no; ?>_pay');">
												<div onselectstart="return false;"><img id="trcn_info_<?php echo $no; ?>" onclick="lists_views();" src="images/profile/view.jpg" /></div>
											</div>
											<div class="blkD"><a href="#"><img src="images/profile/delete.png" /></a></div>
											<div class="expandable" id="Accordion<?php echo $no; ?>_payContent" style="
											background: none repeat scroll 0 0 #F3F3F3; height: 215px !important; margin-left: 50px;">
												<div class="affiBlock">
													<div class="RowCont">
														<div class="sideL">Item Title :</div>
														<div class="sideR"><?php 
														if(isset($gets_meds) && $gets_meds['title']!="")
														{ 
															if($gets_meds['media_type']==114)
															{
																$get_trck = $trans->get_track($gets_meds['id']);
																if($get_trck!="" && $get_trck['track']!="")
																{
																	echo $get_trck['track'].' '.$gets_meds['title'];
																}
																else
																{
																	echo $gets_meds['title'];
																}
															}
															else
															{
																echo $gets_meds['title'];
															}
														}
														else
														{ 
															if($all_stores[$se]['table_name']=='artist_project' || $all_stores[$se]['table_name']=='community_project')
															{
																if($all_stores[$se]['fan_member_id']==0)
																{
																	echo $get_vars['title'];
																}
																else
																{
																	echo $get_vars['title'].' Fan Club';
																}
															}
															else
															{
																if(isset($get_creator['name']))
																{
																	echo $get_creator['name'].' Fan Club';
																}
																elseif(isset($get_creator['title']))
																{
																	echo $get_creator['title'];
																} 
															} 
														}?></div>
													</div>
													<?php
														if(isset($gets_meds) && $gets_meds['title']!="" && $gets_meds['from_info']!="")
														{ 
															$get_from = $trans->get_from($gets_meds['from_info']);
													?>
															<div class="RowCont">
																<div class="sideL">From :</div>
																<div class="sideR"><?php echo $get_from['title']; ?></div>
															</div>
													<?php
														}
													?>
													<div class="RowCont">
														<div class="sideL">Name :</div>
														<div class="sideR"><?php
														if(isset($all_stores[$se]['IP_address']))
														{
															if($all_stores[$se]['IP_address']!=NULL)
															{
																echo $all_stores[$se]['buy_name'];
															}
															else
															{
																if($buy['fname']=="" && $buy['lname']==""){ echo "N/A"; }else{ echo $buy['fname'].' '.$buy['lname'];}
															}
														}
														else
														{
															if($buy['fname']=="" && $buy['lname']==""){ echo "N/A"; }else{ echo $buy['fname'].' '.$buy['lname'];}
														} ?></div>
													</div>
													<div class="RowCont">
														<div class="sideL">Email :</div>
														<div class="sideR"><?php
														if(isset($all_stores[$se]['IP_address']))
														{
															if($all_stores[$se]['IP_address']!=NULL)
															{
																echo $all_stores[$se]['buy_email'];
															}
															else
															{
																if($buy['email']==""){ echo "N/A"; }else{ echo $buy['email']; }
															}
														}
														else
														{
															if($buy['email']==""){ echo "N/A"; }else{ echo $buy['email']; }
														} ?></div>
													</div>
													<div class="RowCont">
														<div class="sideL">Amount :</div>
														<div class="sideR">$<?php if(isset($all_stores[$se]['donate_field'])) { echo $all_stores[$se]['donate']; }else { echo $all_stores[$se]['price']; } ?></div>
													</div>
													<div class="RowCont">
														<div class="sideL">Paypal Fee :</div>
														<div class="sideR">$<?php if(isset($all_stores[$se]['donate_field'])) { echo $exp_fee[1]; }else{ echo $exp_fee[0]; } ?></div>
													</div>
													<div class="RowCont">
														<div class="sideL">Purify Fee :</div>
														<div class="sideR">$<?php if(isset($all_stores[$se]['donate_field'])) { echo $purify_fee_2; }else{ echo $purify_fee; } ?></div>
													</div>
													<div class="RowCont">
														<div class="sideL">Net Profit :</div>
														<div class="sideR">$<?php echo $total_profit; ?></div>
													</div>
													<!--<div class="RowCont">
														<div class="sideL">Payment :</div>
														<select class="fieldUpload">
															<option value="">Paypal</option>
															<option value="">Xoom</option>
															<option value="">Check</option>
														</select>
													</div>-->
												</div>
											</div>
										</div>
										<?php
											if(isset($all_stores[$se]['media_id']))
											{
												$cart = 'cart';
												$cretr = $gets_meds['creator_info'];
											}
											else
											{
												$cart = 'all';
												if($all_stores[$se]['table_name']=='artist_project' || $all_stores[$se]['table_name']=='community_project')
												{
													$cretr = $get_vars['creators_info'];
												}
												elseif($all_stores[$se]['table_name']=='general_artist' || $all_stores[$se]['table_name']=='general_community')
												{
													$cretr = $get_creator['creators_info'];
												}
											}
											if(!isset($all_stores[$se]['donate_field']))
											{
												$all_crets = $all_crets.','.$cretr.'~'.$all_stores[$se]['id'].'*'.$cart.'@'.$total_profit;
											}
											$no = $no + 1;
												//}
											//}
											}
											$hasing = $hasing - 1;
										}
										?>
									</div>
                            </div>
                            <div id="total" style="display:none;">
								<div class="titleCont">
									<div class="blkD">Year</div>
									<div class="blkB">Month</div>
									<div class="blkE">Total</div>
									<div class="blkE">Creator</div>
								</div>
								<div style="position:relative;float:left;height:466px;overflow-x:hidden;overflow-y:auto;">
<?php
									$same = array();
									if($all_crets!="")
									{
										$exp_crets = explode(',',$all_crets);
										$st = 1;
										for($c=0;$c<count($exp_crets);$c++)
										{
											$exp_ids[$c] = explode('~',$exp_crets[$c]);
											
											$exp_chk_t[$c] = explode('*',$exp_ids[$c][1]);
											$exps_at_rates[$c] = explode('@',$exp_chk_t[$c][1]);
											if($exps_at_rates[$c][0]=='cart')
											{
												$get_trans = $trans->get_trans($exp_chk_t[$c][0]);
											}
											elseif($exps_at_rates[$c][0]=='all')
											{
												$get_trans = $trans->get_trans_fans($exp_chk_t[$c][0]);
											}
											if($get_trans!=NULL)
											{
												if(mysql_num_rows($get_trans)>0)
												{
													$ans_trans[$c] = mysql_fetch_assoc($get_trans);
													if($exps_at_rates[$c][0]=='all')
													{
														if($exp_ids[$c][0]=='')
														{
															//echo "hii";
															$exp_ids[$c][0] = $ans_trans[$c]['table_name'].'|'.$ans_trans[$c]['type_id'];
															//echo $exp_ids[$c][0];
														}													
													}
													$mth[$c] = date("F",strtotime($ans_trans[$c]['chk_date']));
												
													$yr[$c] = date("Y",strtotime($ans_trans[$c]['chk_date']));
													
													
													$not_all = "";
													for($i=$c-1;$i>=0;$i--)
													{	
														if($exp_ids[$i][0]==$exp_ids[$c][0] && $mth[$c]==$mth[$i] && $yr[$i]==$yr[$c])
														{
															$not_all = $i;
															//echo $i;
														}	
													}
													if($not_all!="")
													{	
														//echo $not_all;
														//$same[$not_all]['amt'] = $same[$not_all]['amt'] + $ans_trans[$c]['total'];	
														
														$same[$not_all]['amt'] = $same[$not_all]['amt'] + $exps_at_rates[$c][1] + $do_total_profit_do[$c];
														$same[$c] = "";
													}
													else
													{		
														$same[$c]['creator'] = $exp_ids[$c][0];
														$same[$c]['mth'] = $mth[$c];
														$same[$c]['year'] = $yr[$c];
														//$same[$c]['amt'] = $ans_trans[$c]['total'];
														$same[$c]['amt'] = $exps_at_rates[$c][1] + $do_total_profit_do[$c];
													//	$st = $st + 1;
													}
												}
											}
										}
										
										
										if(!empty($same))
										{
											//$same_new = array_unique($same);
											$dummy = array();
											for($r=1;$r<=count($same);$r++)
											{
												if(!empty($same[$r]))
												{
												//if(in_array($same[$r]['creator'],$dummy))
												//{}
												//else
												//{
													//$dummy[] = $same[$r]['creator'];
?>													<div class="tableCont">
														<div class="blkD"><?php echo $same[$r]['year']; ?></div>
														<div class="blkB"><?php echo $same[$r]['mth']; ?></div>
														<div class="blkE">$<?php echo $same[$r]['amt']; ?></div>
														<?php
														$get_creator_name = $trans->get_creators($same[$r]['creator']);
														if($get_creator_name['name']!="" || $get_creator_name['title']!="")
														{
															if($get_creator_name['name']!="")
															{
?>
																<div class="blkE"><?php echo $get_creator_name['name']; ?></div>
<?php
															}
															else
															{
?>
																<div class="blkE"><?php echo $get_creator_name['title']; ?></div>
<?php
															}
														}
														else
														{
														?> &nbsp; <?php
														}
?>
													</div>
<?php
												}
											}
										}
									}
?>
								</div>
                            </div>
							
							<div id="payments" style="display:none;">
								<div class="titleCont">
									<div class="blkB">Date Requested</div>
									<div class="blkA">Amount</div>
									<div class="blkE">Date Sent</div>
								</div>
								<div style="position:relative;float:left;height:466px;overflow-x:hidden;overflow-y:auto;">
<?php
									$sql_got = $trans->get_bal();
									if(mysql_num_rows($sql_got)>0)
									{
										while($ans_got = mysql_fetch_assoc($sql_got))
										{
?>								
											<div class="tableCont">
												<div class="blkB"><?php echo date("m.d.y", strtotime($ans_got['date_sent']));  ?></div>
												<div class="blkA"><?php echo $ans_got['amount'] - 10; ?></div>
												<div class="blkE"><?php if($ans_got['date_received']!='0000-00-00') { echo date("m.d.y", strtotime($ans_got['date_received'])); }else{ echo "Processing"; }?></div>
											</div>
<?php
										}
									}
								?>
								
                            </div>
							</div>
                        </div>
                    </div>
                </div>
</div>

<?php include("footer.php");?>
<!-- end of main container -->
</div>
</body>
</html>
