<?php
session_start();
include ("classes/AddToCartFan.php");
include("classes/GetFanClubInfo.php");
include_once('sendgrid/SendGrid_loader.php');
$sendgrid = new SendGrid('','');

if(isset($_SESSION['guest_email']))
	{
		if($_SESSION['guest_email']!=NULL)
		{
			//session_destroy();
			unset($_SESSION['guest_email']);
			unset($_SESSION['guest_name']);
		}
	}
?>
<!--<script src='http://ajax.googleapis.com/ajax/libs/jquery/1.6.1/jquery.min.js'></script>
<script>
	!window.jQuery && document.write('<script src="./fancybox/jquery-1.4.3.min.js"><\/script>');
</script>
<script type="text/javascript" src="./fancybox/jquery.fancybox-1.3.4.pack.js"></script>
<link rel="stylesheet" type="text/css" href="./fancybox/jquery.fancybox-1.3.4.css" media="screen" />-->
<?php
if(isset($_SESSION['name']) && isset($_SESSION['rel_type']) && isset($_REQUEST['success']) && $_REQUEST['success']==1)
{
	echo "You have successfully added $_SESSION[name] as a member. They have been sent a mail to verify their membership.";
	//session_destroy();
}
else
{

$get_fan_club = new GetFanClubInfo();
$add_cart_fan = new AddToCartFan();
$get_seller = $add_cart_fan->getsellerdata($_GET['data']);
if(isset($_SESSION['login_email'])){ $login_email = $_SESSION['login_email']; } else { $login_email=""; }
$get_buyer = $add_cart_fan->get_buyer_id($login_email);
if(isset($_GET['renew'])){
$renew_fan = $add_cart_fan->renew_fan_club($_GET['email'],$_GET['rel_id'],$_GET['rel_type']);
}else{
	if(!isset($_GET['don_tips_al_pro']))
	{
		$media_song = explode(",",$get_seller['fan_song']);
		$media_video = explode(",",$get_seller['fan_video']);
		$media_gallery = explode(",",$get_seller['fan_gallery']);
		$media_channel = explode(",",$get_seller['fan_channel']);
		if($get_seller['tbl']=="general_community" || $get_seller['tbl']=="community_project")
		{
			$get_seller_id = $add_cart_fan->get_seller_id("community_id",$get_seller['community_id']);
			if($get_seller['tbl']=="general_community"){$type_id = $get_seller['community_id'];$id_name="community_id";}
			else{$type_id = $get_seller['id'];$id_name="id";}
		}
		if($get_seller['tbl']=="general_artist" || $get_seller['tbl']=="artist_project")
		{
			$get_seller_id = $add_cart_fan->get_seller_id("artist_id",$get_seller['artist_id']);
			if($get_seller['tbl']=="general_artist"){$type_id = $get_seller['artist_id'];$id_name="artist_id";}
			else{$type_id = $get_seller['id'];$id_name="id";}
			
		}
		for($media_song_count=0;$media_song_count<count($media_song);$media_song_count++)
		{
			if($media_song[$media_song_count]!="")
			{
				$get_selling_songs = $add_cart_fan->get_media_sell_song($media_song[$media_song_count]);
				if($get_selling_songs['id']=="")
				{
					//echo "Azhar Song no</br>";
					$get_stream_songs = $add_cart_fan->get_media_stream_song($media_song[$media_song_count]);
					$add_cart_all = $add_cart_fan->Add_to_Cart_all($get_seller_id['general_user_id'],$get_buyer['general_user_id'],$media_song[$media_song_count],$get_stream_songs['title'],$get_seller['tbl'],$type_id);
					continue;
				}
				else
				{
					$get_selling_songs_image = $add_cart_fan->get_media_sell_song_image($get_selling_songs['id']);
					if(isset($get_selling_songs_image['nogal_image_name']))
					{
						$img_name = $get_selling_songs_image['nogal_image_name'];
					}
					else
					{
						if($get_selling_songs_image['cover_pic']=="" || $get_selling_songs_image['image_name']=="")
						{
							$img_name ="http://medgalthumb.s3.amazonaws.com/Noimage.png";
						}
						else
						{
							$img_name = $get_selling_songs_image['path']."".$get_selling_songs_image['cover_pic'];
						}
					}
					//echo "Azhar Song yes</br>";
					$addto_cart_song = $add_cart_fan->Add_to_Cart($get_seller_id['general_user_id'],$get_buyer['general_user_id'],$get_selling_songs['id'],$img_name,$get_selling_songs['title'],$get_seller['price'],0,$get_seller['price'],$get_seller['tbl'],$type_id);
					$add_cart_all = $add_cart_fan->Add_to_Cart_all($get_seller_id['general_user_id'],$get_buyer['general_user_id'],$get_selling_songs['id'],$get_selling_songs['title'],$get_seller['tbl'],$type_id);
				}
			}
		}


		for($media_video_count=0;$media_video_count<count($media_video);$media_video_count++)
		{
			if($media_video[$media_video_count]!="")
			{
				$get_selling_video = $add_cart_fan->get_media_sell_video($media_video[$media_video_count]);
				if($get_selling_video['id']=="")
				{
					//echo "Azhar Video no</br>";
					$get_streaming_video = $add_cart_fan->get_media_stream_video($media_video[$media_video_count]);
					$add_cart_all = $add_cart_fan->Add_to_Cart_all($get_seller_id['general_user_id'],$get_buyer['general_user_id'],$media_video[$media_video_count],$get_streaming_video['title'],$get_seller['tbl'],$type_id);
					continue;
				}
				else
				{
					$get_selling_video_image = $add_cart_fan->get_media_sell_video_image($get_selling_video['id']);
					if(isset($get_selling_video_image['nogal_image_name']))
					{
						$img_name = $get_selling_video_image['nogal_image_name'];
					}
					else
					{
						if($get_selling_video_image['cover_pic']=="" || $get_selling_video_image['image_name']=="")
						{
							$img_name = $get_selling_video_image['path'].""."Noimage.png";
						}
						else
						{
							$img_name = $get_selling_video_image['path']."".$get_selling_video_image['cover_pic'];
						}
					}
					//echo "Azhar Video yes</br>";
					$addto_cart_song = $add_cart_fan->Add_to_Cart($get_seller_id['general_user_id'],$get_buyer['general_user_id'],$get_selling_video['id'],$img_name,$get_selling_video['title'],$get_seller['price'],0,$get_seller['price'],$get_seller['tbl'],$type_id);
					$add_cart_all = $add_cart_fan->Add_to_Cart_all($get_seller_id['general_user_id'],$get_buyer['general_user_id'],$get_selling_video['id'],$get_selling_video['title'],$get_seller['tbl'],$type_id);
				}
			}
		}

		for($media_gallery_count=0;$media_gallery_count<count($media_gallery);$media_gallery_count++)
		{
			if($media_gallery[$media_gallery_count]!="")
			{
				$get_selling_gallery = $add_cart_fan->get_media_sell_gallery($media_gallery[$media_gallery_count]);
				if($get_selling_gallery['id']=="")
				{
					//echo "Azhar Gallery no</br>";
					$get_streaming_gallery = $add_cart_fan->get_media_stream_gallery($media_gallery[$media_gallery_count]);
					$add_cart_all = $add_cart_fan->Add_to_Cart_all($get_seller_id['general_user_id'],$get_buyer['general_user_id'],$media_gallery[$media_gallery_count],$get_streaming_gallery['title'],$get_seller['tbl'],$type_id);
					continue;
				}
				else
				{
					$get_selling_gallery_image = $add_cart_fan->get_media_sell_gallery_image($get_selling_gallery['id']);
					if($get_selling_gallery_image['cover_pic']!="")
					{
						$img_name =  $get_selling_gallery_image['path']."".$get_selling_gallery_image['cover_pic'];
					}
					else
					{
						$img_name = $get_selling_gallery_image['path']."".$get_selling_gallery_image['image_name'];
					}
					//echo "Azhar gallery yes</br>";
					$addto_cart_song = $add_cart_fan->Add_to_Cart($get_seller_id['general_user_id'],$get_buyer['general_user_id'],$get_selling_gallery['id'],$img_name,$get_selling_gallery['title'],$get_seller['price'],0,$get_seller['price'],$get_seller['tbl'],$type_id);
					$add_cart_all = $add_cart_fan->Add_to_Cart_all($get_seller_id['general_user_id'],$get_buyer['general_user_id'],$get_selling_gallery['id'],$get_selling_gallery['title'],$get_seller['tbl'],$type_id);
				}
			}
		}
		for($media_channel_count=0;$media_channel_count<count($media_channel);$media_channel_count++)
		{
			if($media_channel[$media_channel_count]!="")
			{
				$get_selling_channel = $add_cart_fan->get_media_sell_channel($media_channel[$media_channel_count]);
				if($get_selling_channel['id']=="")
				{
					//echo "Azhar Channel no</br>";
					$get_streaming_channel = $add_cart_fan->get_media_stream_channel($media_channel[$media_channel_count]);
					$add_cart_all = $add_cart_fan->Add_to_Cart_all($get_seller_id['general_user_id'],$get_buyer['general_user_id'],$media_channel[$media_channel_count],$get_streaming_channel['title'],$get_seller['tbl'],$type_id);
					continue;
				}
				else
				{
					$get_selling_channel_image = $add_cart_fan->get_media_sell_channel_image($get_seller['tbl'],$id_name,$type_id);
					if($get_selling_channel_image !="")
					{
						if($get_seller['tbl']=="general_community"){$path="http://comjcropthumb.s3.amazonaws.com/";}
						if($get_seller['tbl']=="general_artist"){$path="http://artjcropthumb.s3.amazonaws.com/";}
						$img_name =  $path."".$get_selling_channel_image['listing_image_name'];
					}
					//echo "Azhar Channel yes</br>";
					$addto_cart_song = $add_cart_fan->Add_to_Cart($get_seller_id['general_user_id'],$get_buyer['general_user_id'],$get_selling_channel['id'],$img_name,$get_selling_channel['title'],$get_seller['price'],0,$get_seller['price'],$get_seller['tbl'],$type_id);
					$add_cart_all = $add_cart_fan->Add_to_Cart_all($get_seller_id['general_user_id'],$get_buyer['general_user_id'],$get_selling_channel['id'],$get_selling_channel['title'],$get_seller['tbl'],$type_id);
				}
			}
		}
	}
	elseif(isset($_GET['don_tips_al_pro']) && $get_seller['type']=="free_club")
	{
		$media_song = explode(",",$get_seller['free_club_song']);
		$media_video = explode(",",$get_seller['free_club_video']);
		$media_gallery = explode(",",$get_seller['free_club_gallery']);
		$media_channel = explode(",",$get_seller['free_club_channel']);
		if($get_seller['tbl']=="general_community" || $get_seller['tbl']=="community_project")
		{
			$get_seller_id = $add_cart_fan->get_seller_id("community_id",$get_seller['community_id']);
			if($get_seller['tbl']=="general_community"){$type_id = $get_seller['community_id'];$id_name="community_id";}
			else{$type_id = $get_seller['id'];$id_name="id";}
		}
		if($get_seller['tbl']=="general_artist" || $get_seller['tbl']=="artist_project")
		{
			$get_seller_id = $add_cart_fan->get_seller_id("artist_id",$get_seller['artist_id']);
			if($get_seller['tbl']=="general_artist"){$type_id = $get_seller['artist_id'];$id_name="artist_id";}
			else{$type_id = $get_seller['id'];$id_name="id";}
		}
		for($media_song_count=0;$media_song_count<count($media_song);$media_song_count++)
		{
			if($media_song[$media_song_count]!="")
			{
				$get_selling_songs = $add_cart_fan->get_media_sell_song($media_song[$media_song_count]);
				if($get_selling_songs['id']=="")
				{
					//echo "Azhar Song no</br>";
					$get_stream_songs = $add_cart_fan->get_media_stream_song($media_song[$media_song_count]);
					if(!isset($_SESSION['login_email'])){ //if($_SESSION['login_email']==null)
						$add_cart_all = $add_cart_fan->Add_to_Cart_all_notlogin($get_seller_id['general_user_id'],$_SERVER["REMOTE_ADDR"],$media_song[$media_song_count],$get_stream_songs['title'],$get_seller['tbl'],$type_id);
						
						if($_GET['don_tips_al_pro']!="" && $_GET['don_tips_al_pro']!=0 && $get_seller_id['general_user_id']!="" && $type_id!="" && $get_seller['tbl']!="")
						{
							$update_cart = $add_cart_fan->update_tip_from_own_not($_GET['don_tips_al_pro'],$get_seller_id['general_user_id'],$type_id,$get_seller['tbl'],$_SERVER["REMOTE_ADDR"]);
						}
					}
					else
					{
						$add_cart_all = $add_cart_fan->Add_to_Cart_all($get_seller_id['general_user_id'],$get_buyer['general_user_id'],$media_song[$media_song_count],$get_stream_songs['title'],$get_seller['tbl'],$type_id);
						
						if($_GET['don_tips_al_pro']!="" && $_GET['don_tips_al_pro']!=0 && $get_seller_id['general_user_id']!="" && $get_buyer['general_user_id']!="" && $type_id!="" && $get_seller['tbl']!="")
						{
							$update_cart = $add_cart_fan->update_tip_from_own($_GET['don_tips_al_pro'],$get_seller_id['general_user_id'],$get_buyer['general_user_id'],$type_id,$get_seller['tbl']);
						}
					}
					continue;
				}
				else
				{
					$get_selling_songs_image = $add_cart_fan->get_media_sell_song_image($get_selling_songs['id']);
					if(isset($get_selling_songs_image['nogal_image_name']))
					{
						$img_name = $get_selling_songs_image['nogal_image_name'];
					}
					else
					{
						if($get_selling_songs_image['cover_pic']=="" || $get_selling_songs_image['image_name']=="")
						{
							$img_name ="http://medgalthumb.s3.amazonaws.com/Noimage.png";
						}
						else
						{
							$img_name = $get_selling_songs_image['path']."".$get_selling_songs_image['cover_pic'];
						}
					}
					//echo "Azhar Song yes</br>";
					if($_SESSION['login_email']==null){
						$addto_cart_song = $add_cart_fan->Add_to_Cart($get_seller_id['general_user_id'],$get_buyer['general_user_id'],$get_selling_songs['id'],$img_name,$get_selling_songs['title'],$get_seller['price'],0,$get_seller['price'],$get_seller['tbl'],$type_id);
						$add_cart_all = $add_cart_fan->Add_to_Cart_all_notlogin($get_seller_id['general_user_id'],$_SERVER["REMOTE_ADDR"],$get_selling_songs['id'],$get_selling_songs['title'],$get_seller['tbl'],$type_id);
						
						if($_GET['don_tips_al_pro']!="" && $_GET['don_tips_al_pro']!=0 && $get_seller_id['general_user_id']!="" && $type_id!="" && $get_seller['tbl']!="")
						{
							$update_cart = $add_cart_fan->update_tip_from_own_not($_GET['don_tips_al_pro'],$get_seller_id['general_user_id'],$type_id,$get_seller['tbl'],$_SERVER["REMOTE_ADDR"]);
						}
					}
					else
					{
						$addto_cart_song = $add_cart_fan->Add_to_Cart($get_seller_id['general_user_id'],$get_buyer['general_user_id'],$get_selling_songs['id'],$img_name,$get_selling_songs['title'],$get_seller['price'],0,$get_seller['price'],$get_seller['tbl'],$type_id);
						$add_cart_all = $add_cart_fan->Add_to_Cart_all($get_seller_id['general_user_id'],$get_buyer['general_user_id'],$get_selling_songs['id'],$get_selling_songs['title'],$get_seller['tbl'],$type_id);
						
						if($_GET['don_tips_al_pro']!="" && $_GET['don_tips_al_pro']!=0 && $get_seller_id['general_user_id']!="" && $get_buyer['general_user_id']!="" && $type_id!="" && $get_seller['tbl']!="")
						{
							$update_cart = $add_cart_fan->update_tip_from_own($_GET['don_tips_al_pro'],$get_seller_id['general_user_id'],$get_buyer['general_user_id'],$type_id,$get_seller['tbl']);
						}
					}
				}
			}
		}


		for($media_video_count=0;$media_video_count<count($media_video);$media_video_count++)
		{
			if($media_video[$media_video_count]!="")
			{
				$get_selling_video = $add_cart_fan->get_media_sell_video($media_video[$media_video_count]);
				if($get_selling_video['id']=="")
				{
					//echo "Azhar Video no</br>";
					$get_streaming_video = $add_cart_fan->get_media_stream_video($media_video[$media_video_count]);
					if($_SESSION['login_email']==null){
						$add_cart_all = $add_cart_fan->Add_to_Cart_all_notlogin($get_seller_id['general_user_id'],$_SERVER["REMOTE_ADDR"],$media_video[$media_video_count],$get_streaming_video['title'],$get_seller['tbl'],$type_id);
						
						if($_GET['don_tips_al_pro']!="" && $_GET['don_tips_al_pro']!=0 && $get_seller_id['general_user_id']!="" && $type_id!="" && $get_seller['tbl']!="")
						{
							$update_cart = $add_cart_fan->update_tip_from_own_not($_GET['don_tips_al_pro'],$get_seller_id['general_user_id'],$type_id,$get_seller['tbl'],$_SERVER["REMOTE_ADDR"]);
						}
					}
					else
					{
						$add_cart_all = $add_cart_fan->Add_to_Cart_all($get_seller_id['general_user_id'],$get_buyer['general_user_id'],$media_video[$media_video_count],$get_streaming_video['title'],$get_seller['tbl'],$type_id);
						
						if($_GET['don_tips_al_pro']!="" && $_GET['don_tips_al_pro']!=0 && $get_seller_id['general_user_id']!="" && $get_buyer['general_user_id']!="" && $type_id!="" && $get_seller['tbl']!="")
						{
							$update_cart = $add_cart_fan->update_tip_from_own($_GET['don_tips_al_pro'],$get_seller_id['general_user_id'],$get_buyer['general_user_id'],$type_id,$get_seller['tbl']);
						}
					}
					continue;
				}
				else
				{
					$get_selling_video_image = $add_cart_fan->get_media_sell_video_image($get_selling_video['id']);
					if(isset($get_selling_video_image['nogal_image_name']))
					{
						$img_name = $get_selling_video_image['nogal_image_name'];
					}
					else
					{
						if($get_selling_video_image['cover_pic']=="" || $get_selling_video_image['image_name']=="")
						{
							$img_name = $get_selling_video_image['path'].""."Noimage.png";
						}
						else
						{
							$img_name = $get_selling_video_image['path']."".$get_selling_video_image['cover_pic'];
						}
					}
					//echo "Azhar Video yes</br>";
					if($_SESSION['login_email']==null){
						$addto_cart_song = $add_cart_fan->Add_to_Cart($get_seller_id['general_user_id'],$get_buyer['general_user_id'],$get_selling_video['id'],$img_name,$get_selling_video['title'],$get_seller['price'],0,$get_seller['price'],$get_seller['tbl'],$type_id);
						$add_cart_all = $add_cart_fan->Add_to_Cart_all_notlogin($get_seller_id['general_user_id'],$_SERVER["REMOTE_ADDR"],$get_selling_video['id'],$get_selling_video['title'],$get_seller['tbl'],$type_id);
						
						if($_GET['don_tips_al_pro']!="" && $_GET['don_tips_al_pro']!=0 && $get_seller_id['general_user_id']!="" && $type_id!="" && $get_seller['tbl']!="")
						{
							$update_cart = $add_cart_fan->update_tip_from_own_not($_GET['don_tips_al_pro'],$get_seller_id['general_user_id'],$type_id,$get_seller['tbl'],$_SERVER["REMOTE_ADDR"]);
						}
					}
					else
					{
						$addto_cart_song = $add_cart_fan->Add_to_Cart($get_seller_id['general_user_id'],$get_buyer['general_user_id'],$get_selling_video['id'],$img_name,$get_selling_video['title'],$get_seller['price'],0,$get_seller['price'],$get_seller['tbl'],$type_id);
						$add_cart_all = $add_cart_fan->Add_to_Cart_all($get_seller_id['general_user_id'],$get_buyer['general_user_id'],$get_selling_video['id'],$get_selling_video['title'],$get_seller['tbl'],$type_id);
						
						if($_GET['don_tips_al_pro']!="" && $_GET['don_tips_al_pro']!=0 && $get_seller_id['general_user_id']!="" && $get_buyer['general_user_id']!="" && $type_id!="" && $get_seller['tbl']!="")
						{
							$update_cart = $add_cart_fan->update_tip_from_own($_GET['don_tips_al_pro'],$get_seller_id['general_user_id'],$get_buyer['general_user_id'],$type_id,$get_seller['tbl']);
						}
					}
				}
			}
		}

		for($media_gallery_count=0;$media_gallery_count<count($media_gallery);$media_gallery_count++)
		{
			if($media_gallery[$media_gallery_count]!="")
			{
				$get_selling_gallery = $add_cart_fan->get_media_sell_gallery($media_gallery[$media_gallery_count]);
				if($get_selling_gallery['id']=="")
				{
					//echo "Azhar Gallery no</br>";
					$get_streaming_gallery = $add_cart_fan->get_media_stream_gallery($media_gallery[$media_gallery_count]);
					if($_SESSION['login_email']==null){
						$add_cart_all = $add_cart_fan->Add_to_Cart_all_notlogin($get_seller_id['general_user_id'],$_SERVER["REMOTE_ADDR"],$media_gallery[$media_gallery_count],$get_streaming_gallery['title'],$get_seller['tbl'],$type_id);
						
						if($_GET['don_tips_al_pro']!="" && $_GET['don_tips_al_pro']!=0 && $get_seller_id['general_user_id']!="" && $type_id!="" && $get_seller['tbl']!="")
						{
							$update_cart = $add_cart_fan->update_tip_from_own_not($_GET['don_tips_al_pro'],$get_seller_id['general_user_id'],$type_id,$get_seller['tbl'],$_SERVER["REMOTE_ADDR"]);
						}
					}
					else
					{
						$add_cart_all = $add_cart_fan->Add_to_Cart_all($get_seller_id['general_user_id'],$get_buyer['general_user_id'],$media_gallery[$media_gallery_count],$get_streaming_gallery['title'],$get_seller['tbl'],$type_id);
						
						if($_GET['don_tips_al_pro']!="" && $_GET['don_tips_al_pro']!=0 && $get_seller_id['general_user_id']!="" && $get_buyer['general_user_id']!="" && $type_id!="" && $get_seller['tbl']!="")
						{
							$update_cart = $add_cart_fan->update_tip_from_own($_GET['don_tips_al_pro'],$get_seller_id['general_user_id'],$get_buyer['general_user_id'],$type_id,$get_seller['tbl']);
						}
					}
					continue;
				}
				else
				{
					$get_selling_gallery_image = $add_cart_fan->get_media_sell_gallery_image($get_selling_gallery['id']);
					if($get_selling_gallery_image['cover_pic']!="")
					{
						$img_name =  $get_selling_gallery_image['path']."".$get_selling_gallery_image['cover_pic'];
					}
					else
					{
						$img_name = $get_selling_gallery_image['path']."".$get_selling_gallery_image['image_name'];
					}
					//echo "Azhar gallery yes</br>";
					if($_SESSION['login_email']==null){
						$addto_cart_song = $add_cart_fan->Add_to_Cart($get_seller_id['general_user_id'],$get_buyer['general_user_id'],$get_selling_gallery['id'],$img_name,$get_selling_gallery['title'],$get_seller['price'],0,$get_seller['price'],$get_seller['tbl'],$type_id);
						$add_cart_all = $add_cart_fan->Add_to_Cart_all_notlogin($get_seller_id['general_user_id'],$_SERVER["REMOTE_ADDR"],$get_selling_gallery['id'],$get_selling_gallery['title'],$get_seller['tbl'],$type_id);
						
						if($_GET['don_tips_al_pro']!="" && $_GET['don_tips_al_pro']!=0 && $get_seller_id['general_user_id']!="" && $type_id!="" && $get_seller['tbl']!="")
						{
							$update_cart = $add_cart_fan->update_tip_from_own_not($_GET['don_tips_al_pro'],$get_seller_id['general_user_id'],$type_id,$get_seller['tbl'],$_SERVER["REMOTE_ADDR"]);
						}
					}
					else
					{
						$addto_cart_song = $add_cart_fan->Add_to_Cart($get_seller_id['general_user_id'],$get_buyer['general_user_id'],$get_selling_gallery['id'],$img_name,$get_selling_gallery['title'],$get_seller['price'],0,$get_seller['price'],$get_seller['tbl'],$type_id);
						$add_cart_all = $add_cart_fan->Add_to_Cart_all($get_seller_id['general_user_id'],$get_buyer['general_user_id'],$get_selling_gallery['id'],$get_selling_gallery['title'],$get_seller['tbl'],$type_id);
						
						if($_GET['don_tips_al_pro']!="" && $_GET['don_tips_al_pro']!=0 && $get_seller_id['general_user_id']!="" && $get_buyer['general_user_id']!="" && $type_id!="" && $get_seller['tbl']!="")
						{
							$update_cart = $add_cart_fan->update_tip_from_own($_GET['don_tips_al_pro'],$get_seller_id['general_user_id'],$get_buyer['general_user_id'],$type_id,$get_seller['tbl']);
						}
					}
				}
			}
		}
		for($media_channel_count=0;$media_channel_count<count($media_channel);$media_channel_count++)
		{
			if($media_channel[$media_channel_count]!="")
			{
				$get_selling_channel = $add_cart_fan->get_media_sell_channel($media_channel[$media_channel_count]);
				if($get_selling_channel['id']=="")
				{
					//echo "Azhar Channel no</br>";
					$get_streaming_channel = $add_cart_fan->get_media_stream_channel($media_channel[$media_channel_count]);
					if($_SESSION['login_email']==null){
						$add_cart_all = $add_cart_fan->Add_to_Cart_all_notlogin($get_seller_id['general_user_id'],$_SERVER["REMOTE_ADDR"],$media_channel[$media_channel_count],$get_streaming_channel['title'],$get_seller['tbl'],$type_id);
						
						if($_GET['don_tips_al_pro']!="" && $_GET['don_tips_al_pro']!=0 && $get_seller_id['general_user_id']!="" && $type_id!="" && $get_seller['tbl']!="")
						{
							$update_cart = $add_cart_fan->update_tip_from_own_not($_GET['don_tips_al_pro'],$get_seller_id['general_user_id'],$type_id,$get_seller['tbl'],$_SERVER["REMOTE_ADDR"]);
						}
					}
					else
					{
						$add_cart_all = $add_cart_fan->Add_to_Cart_all($get_seller_id['general_user_id'],$get_buyer['general_user_id'],$media_channel[$media_channel_count],$get_streaming_channel['title'],$get_seller['tbl'],$type_id);
						
						if($_GET['don_tips_al_pro']!="" && $_GET['don_tips_al_pro']!=0 && $get_seller_id['general_user_id']!="" && $get_buyer['general_user_id']!="" && $type_id!="" && $get_seller['tbl']!="")
						{
							$update_cart = $add_cart_fan->update_tip_from_own($_GET['don_tips_al_pro'],$get_seller_id['general_user_id'],$get_buyer['general_user_id'],$type_id,$get_seller['tbl']);
						}
					}
					continue;
				}
				else
				{
					$get_selling_channel_image = $add_cart_fan->get_media_sell_channel_image($get_seller['tbl'],$id_name,$type_id);
					if($get_selling_channel_image !="")
					{
						if($get_seller['tbl']=="general_community"){$path="http://comjcropthumb.s3.amazonaws.com/";}
						if($get_seller['tbl']=="general_artist"){$path="http://artjcropthumb.s3.amazonaws.com/";}
						$img_name =  $path."".$get_selling_channel_image['listing_image_name'];
					}
					//echo "Azhar Channel yes</br>";
					if($_SESSION['login_email']==null){
						$addto_cart_song = $add_cart_fan->Add_to_Cart($get_seller_id['general_user_id'],$get_buyer['general_user_id'],$get_selling_channel['id'],$img_name,$get_selling_channel['title'],$get_seller['price'],0,$get_seller['price'],$get_seller['tbl'],$type_id);
						$add_cart_all = $add_cart_fan->Add_to_Cart_all_notlogin($get_seller_id['general_user_id'],$_SERVER["REMOTE_ADDR"],$get_selling_channel['id'],$get_selling_channel['title'],$get_seller['tbl'],$type_id);
						
						if($_GET['don_tips_al_pro']!="" && $_GET['don_tips_al_pro']!=0 && $get_seller_id['general_user_id']!="" && $type_id!="" && $get_seller['tbl']!="")
						{
							$update_cart = $add_cart_fan->update_tip_from_own_not($_GET['don_tips_al_pro'],$get_seller_id['general_user_id'],$type_id,$get_seller['tbl'],$_SERVER["REMOTE_ADDR"]);
						}
					}
					else
					{
						$addto_cart_song = $add_cart_fan->Add_to_Cart($get_seller_id['general_user_id'],$get_buyer['general_user_id'],$get_selling_channel['id'],$img_name,$get_selling_channel['title'],$get_seller['price'],0,$get_seller['price'],$get_seller['tbl'],$type_id);
						$add_cart_all = $add_cart_fan->Add_to_Cart_all($get_seller_id['general_user_id'],$get_buyer['general_user_id'],$get_selling_channel['id'],$get_selling_channel['title'],$get_seller['tbl'],$type_id);
						
						if($_GET['don_tips_al_pro']!="" && $_GET['don_tips_al_pro']!=0 && $get_seller_id['general_user_id']!="" && $get_buyer['general_user_id']!="" && $type_id!="" && $get_seller['tbl']!="")
						{
							$update_cart = $add_cart_fan->update_tip_from_own($_GET['don_tips_al_pro'],$get_seller_id['general_user_id'],$get_buyer['general_user_id'],$type_id,$get_seller['tbl']);
						}
					}
				}
			}
		}
	}
	
	if($get_seller['type']=="for_sale")
	{
		$media_song = explode(",",$get_seller['sale_song']);
		
		for($media_song_count=0;$media_song_count<count($media_song);$media_song_count++)
		{
			if($media_song[$media_song_count]!="")
			{
				$get_selling_songs = $add_cart_fan->get_media_sell_song($media_song[$media_song_count]);
				if($get_selling_songs['id']=="")
				{
					if(!isset($_SESSION['login_email'])){ //if($_SESSION['login_email']==null)
						$get_stream_songs = $add_cart_fan->get_media_stream_song($media_song[$media_song_count]);
						$add_cart_all = $add_cart_fan->Add_to_Cart_all_notlogin($get_seller_id['general_user_id'],$_SERVER["REMOTE_ADDR"],$media_song[$media_song_count],$get_stream_songs['title'],$get_seller['tbl'],$type_id);
						continue;
					}else{
						$get_stream_songs = $add_cart_fan->get_media_stream_song($media_song[$media_song_count]);
						$add_cart_all = $add_cart_fan->Add_to_Cart_all($get_seller_id['general_user_id'],$get_buyer['general_user_id'],$media_song[$media_song_count],$get_stream_songs['title'],$get_seller['tbl'],$type_id);
						continue;
					}
					
				}
				else
				{
					$get_selling_songs_image = $add_cart_fan->get_media_sell_song_image($get_selling_songs['id']);
					if(isset($get_selling_songs_image['nogal_image_name']))
					{
						$img_name = $get_selling_songs_image['nogal_image_name'];
					}
					else
					{
						if($get_selling_songs_image['cover_pic']=="" || $get_selling_songs_image['image_name']=="")
						{
							$img_name ="http://medgalthumb.s3.amazonaws.com/Noimage.png";
						}
						else
						{
							$img_name = $get_selling_songs_image['path']."".$get_selling_songs_image['cover_pic'];
						}
					}
					//echo "Azhar Song yes</br>";
					if($_SESSION['login_email']==null){
						$addto_cart_song = $add_cart_fan->Add_to_Cart($get_seller_id['general_user_id'],$get_buyer['general_user_id'],$get_selling_songs['id'],$img_name,$get_selling_songs['title'],$get_seller['price'],0,$get_seller['price'],$get_seller['tbl'],$type_id);
						$add_cart_all = $add_cart_fan->Add_to_Cart_all_notlogin($get_seller_id['general_user_id'],$_SERVER["REMOTE_ADDR"],$get_selling_songs['id'],$get_selling_songs['title'],$get_seller['tbl'],$type_id);
					}else{
						$addto_cart_song = $add_cart_fan->Add_to_Cart($get_seller_id['general_user_id'],$get_buyer['general_user_id'],$get_selling_songs['id'],$img_name,$get_selling_songs['title'],$get_seller['price'],0,$get_seller['price'],$get_seller['tbl'],$type_id);
						$add_cart_all = $add_cart_fan->Add_to_Cart_all($get_seller_id['general_user_id'],$get_buyer['general_user_id'],$get_selling_songs['id'],$get_selling_songs['title'],$get_seller['tbl'],$type_id);
					}
					
				}
			}
		}
	}
}

if(isset($_GET['update_id']) && $_GET['update_id'] !="")
{
	$update_cart = $add_cart_fan->update_cart($_GET['update_id'],$_GET['update_val'],$_GET['table_name']);
}if(isset($_GET['donate_purify']) && $_GET['donate_purify'] !="")
{
	$donate_purify = $add_cart_fan->donate_to_purify($_GET['amount']);
}

if(!isset($_SESSION['login_email'])){ //if($_SESSION['login_email']==null)
$get_all_data = $add_cart_fan->get_Cart_notlogin($_SERVER["REMOTE_ADDR"]);
$gets_alls = $add_cart_fan->get_items_for_fan_notlogin($_SERVER["REMOTE_ADDR"]);
}else{
$get_all_data = $add_cart_fan->get_Cart($get_buyer['general_user_id']);
$gets_alls = $add_cart_fan->get_items_for_fan($get_buyer['general_user_id']);
}
$purify_donate = $add_cart_fan->get_purify_donation($get_buyer['general_user_id']);

?>

<html xmlns="http://www.w3.org/1999/xhtml" style="height: 100%;">
<head>
<link href="includes/purify.css" rel="stylesheet" />
</head>
<body style="margin:0; padding:0;">
<div>

<div id="cartWrapper">
<div class="fancy_header">
	Shopping Cart
</div>
    <!--<div id="cartHeader">
        <div class="logo"><img src="images/4.gif" width="202" height="35" /></div>
    </div>-->
    <div id="tempd" class="cartWrapperwhole">
    <!-- PlayList Coding -->
        <div id="playlistView">
			<div id="totalCont" style="border-bottom:none;">
				<!--<div class="subtotal">
                    <div class="title">Payment Mode:</div><div class="amount">
						<select name="payment_mode" id="payment_mode" class="dropdown" >
							<option value="select">Select</option>
							<option value="pay_pal">PayPal</option>
							<option value="Credit_card">Credit Card</option>
						</select>
					</div>
                </div>-->
				<?php 
					if(!isset($_SESSION['login_email']) || $_SESSION['login_email']==""){ //if($_SESSION['login_email']=="" || $_SESSION['login_email']==null)
					?>
						<div class="subtotal">
							<div class="title">Name:</div>
							<div class="amount" style="width:150px;">
							<div class="cart_doll">&nbsp;</div>
							<input type="text" class="field" id="user_name_notlog" value="<?php if(isset($_SESSION['guest_name'])){ echo $_SESSION['guest_name']; }?>"  style="width:107px; height:18px; font-size:11px;"/>
							</div>
						</div>
						<div class="subtotal">
							<div class="title">Email :</div>
							<div class="amount" style="width:150px;">
							<div class="cart_doll">&nbsp;</div>
							<input type="text" class="field" id="email_notlog" value="<?php if(isset($_SESSION['guest_email'])){ echo $_SESSION['guest_email']; }?>" style="width:107px; height:18px; font-size:11px;"/>
							</div>
						</div>
					<?php
					}
					?>
                <div class="subtotal">
					<?php 
					$total = 0;
					if(mysql_num_rows($get_all_data)>0){
						while($get_cart = mysql_fetch_assoc($get_all_data)){
							$total  = $total + $get_cart['price'] + $get_cart['donate'];
						}
					}
					if(mysql_num_rows($gets_alls)>0){
						while($ans_alls = mysql_fetch_assoc($gets_alls)){
							$total  = $total + $ans_alls['price'] + $ans_alls['donate'];
						}
					}
					?>
                    <div class="title">Subtotal:</div><div class="amount" style="width:150px">$<?php echo $total;?>
					</div>
                </div>
				 <div class="subtotal">
                    <div class="title">Donate to Purify Art:</div><div class="amount" style="width:200px;"><div class="cart_doll">$</div><input type="text" id="donate_purify" value="<?php echo $purify_donate['amount'];?>" class="field" style="height:18px;width:107px;font-size: 11px;"/>
					<!--<input type="button" value="Update" onclick="donate_to_purify()"/>-->
					<a onclick="donate_to_purify()" href="javascript:void(0);">Update</a>
					</div>
                </div>
				<div class="total_amount_Cont" style="border-top:1px solid #CCCCCC;height:35px;width:400px">
					<div class="subtotal">
					<?php
						$total = $total + $purify_donate['amount'];
					?>
						<div class="title">Total:</div><div class="amount">$<?php echo $total;?>
						<input type="hidden" id="grand_total" value="<?php echo $total;?>"/>
						</div>
						
					</div>
				</div>
            </div>
			
			<div id="totalCont_new">
				<div class="shopping">
					<input type="button" class="button" value="Continue Shopping" onclick="continue_shopping()" style="margin-bottom:10px;"/>
					<input type="button" id="chk_out_org" class="button" value="Checkout" onclick="get_payment_form()" style="float:right;" />
					<!--<input type="button" id="chk_out_dup" style="float:right;display:none" onclick="alerttoupdate()" value="Checkout" class="button">-->
				</div>
			</div>
			
			<div id="listFiles" style="height:35px;">
				<div class="titleCont">
                    <div class="blkB">Title</div>
                    <div class="blkC">Price</div>
                    <div class="blkD">Tip</div>
                    <div class="blkE">Total</div>
                    <div class="blkF"><a href="addtocart.php?removeall=1&<?php if($get_buyer['general_user_id'] == null || $_SESSION['login_email']==null){ echo "buyer_id=$_SERVER[REMOTE_ADDR]&notlogin=yes"; }else{ echo "buyer_id=$get_buyer[general_user_id]"; }?>&from_url=<?php echo $_GET['from_url']; ?>" onclick="return confirmdelete()">Remove All</a></div>
                </div>
			</div>
            <div id="listFiles">
                
				<?php
				if(!isset($_SESSION['login_email'])){ //if($_SESSION['login_email']==null)
				$get_all_data = $add_cart_fan->get_Cart_notlogin($_SERVER["REMOTE_ADDR"]);
				$gets_alls = $add_cart_fan->get_items_for_fan_notlogin($_SERVER["REMOTE_ADDR"]);
				}else{
				$get_all_data = $add_cart_fan->get_Cart($get_buyer['general_user_id']);
				$gets_alls = $add_cart_fan->get_items_for_fan($get_buyer['general_user_id']);
				}
				$get_cart_price = 0;
				$sub_total = 0;
				while($get_cart = mysql_fetch_assoc($get_all_data))
				{
					$get_seller_name = $add_cart_fan->Get_seller_name($get_cart['seller_id']);
					if($get_cart['table_name']=="general_community"){$id_name= "community_id";}
					if($get_cart['table_name']=="community_project"){$id_name= "id";}
					if($get_cart['table_name']=="general_artist"){$id_name= "artist_id";}
					if($get_cart['table_name']=="artist_project"){$id_name= "id";}
				
					$get_cart_data = $add_cart_fan->get_cart_data($get_cart['table_name'],$id_name,$get_cart['type_id']);
					if($get_cart['table_name']=="general_community"){$image_name = "http://comjcropthumb.s3.amazonaws.com/".$get_cart_data['listing_image_name'];$title = $get_cart_data['name'];$creator_disp = $get_cart_data['name'];$creator_url = $get_cart_data['profile_url'];}
					if($get_cart['table_name']=="community_project"){$image_name = $get_cart_data['listing_image_name'];$title = $get_cart_data['title']; 
						if($get_cart_data['type']=="for_sale"){
							$exp_creator_info = explode("|",$get_cart_data['creators_info']);
							$get_creator_url = $add_cart_fan->get_creator_profile_url($exp_creator_info[0],$exp_creator_info[1]);
							$creator_url = $get_creator_url['profile_url'];
							$creator_disp = $get_cart_data['creator'];
							$title_url = $get_cart_data['profile_url'];
						}else{
							$creator_disp = $get_cart_data['title'];
							$creator_url = $get_cart_data['profile_url'];
						}
						
					}
					if($get_cart['table_name']=="general_artist"){$image_name = "http://artjcropthumb.s3.amazonaws.com/".$get_cart_data['listing_image_name'];$title = $get_cart_data['name'];$creator_disp = $get_cart_data['name'];$creator_url = $get_cart_data['profile_url'];}
					if($get_cart['table_name']=="artist_project"){$image_name = $get_cart_data['listing_image_name'];$title = $get_cart_data['title'];
						if($get_cart_data['type']=="for_sale"){
							$creator_disp = $get_cart_data['creator'];
							$exp_creator_info = explode("|",$get_cart_data['creators_info']);
							$get_creator_url = $add_cart_fan->get_creator_profile_url($exp_creator_info[0],$exp_creator_info[1]);
							$creator_url = $get_creator_url['profile_url'];
							$title_url = $get_cart_data['profile_url'];
						}else{
							$creator_disp = $get_cart_data['title'];
							$creator_url = $get_cart_data['profile_url'];
						}
					}
					//$get_cart_price = $add_cart_fan->get_cart_price($get_cart['seller_id'],$get_cart['buyer_id'],$get_cart['type_id'],$get_cart['table_name']);
					$get_cart_price_new = $add_cart_fan->get_cart_price_new($get_cart['table_name'],$get_cart['type_id'],$id_name);
					$title = str_replace("\'","'",$title);
				?>
					<div class="tableCont">
						<div class="blkB"><img src="<?php echo $image_name;?>" width="50" height="50" /><?php if($get_cart_data['type']=="for_sale"){ if(strlen($title)>26){ echo "<a href='javascript:void(0)' onclick=click_profile_url('$title_url')>".substr($title,0,26)."..</a>";}else{echo "<a href='javascript:void(0)' onclick=click_profile_url('$title_url')>".$title."</a>";} }else{ $title = $title ." Fan Club Membership"; if(strlen($title)>26){ echo substr($title,0,26)."..";}else{ echo $title;}}?><br /><a href="javascript:void(0)" onclick="click_profile_url('<?php echo $creator_url;?>')"><?php echo $creator_disp;?></a></div>
						<div class="blkC">$<?php echo $get_cart['price'];?></div>
						<div class="blkD">$<input type="text" id="donate<?php echo $get_cart['id'];?>" value="<?php echo $get_cart['donate'];?>" class="field" onblur="chk_for_update('donate<?php echo $get_cart['id'];?>','<?php echo $get_cart['id'];?>','add_to_cart_fan_pro')" /></div>
						<div class="blkE" id="total_fan">$<?php echo $get_cart['total'];?></div>
						<div class="blkF"><a href="javascript:void(0);" onclick="update_cart('<?php echo $get_cart['id'];?>','add_to_cart_fan_pro')" >Update</a> | <a href="addtocart.php?remove=<?php echo $get_cart['id'];?>&remove_type=fan_club&buyer_id=<?php if(isset($_SESSION['gen_user_id'])){ echo $_SESSION['gen_user_id']; }?>&from_url=<?php echo $_GET['from_url']; ?>" onclick ="return confirm_remove()">Remove</a></div>
						<!--<span style="color:red;display:none;font-size:9px;font-weight:bold;" id="error<?php //echo $get_cart['id'];?>">Item is not updated.</span>-->
					</div>
				<?php
				//$sub_total = $sub_total + $get_cart_data['price'] + $get_cart_data['donate'];
				$sub_total = $sub_total + $get_cart_price;
				
				}
				if(mysql_num_rows($gets_alls)>0)
				{
					while($ans_alls = mysql_fetch_assoc($gets_alls))
					{
						//$get_seller_name = $add_cart_fan->Get_seller_name($ans_alls['seller_id']);
						$get_seller_name = $add_cart_fan->Get_media_creator($ans_alls['media_id']);
						$exp_creator_info = explode("|",$get_seller_name['creator_info']);
						$get_creator_url = $add_cart_fan->get_creator_profile_url($exp_creator_info[0],$exp_creator_info[1]);
						$ans_alls['title'] = str_replace("\'","'",$ans_alls['title']);
				?>
					<div class="tableCont">
						<div class="blkB"><img src="<?php echo $ans_alls['image_name'];?>" width="50" height="50" /><?php if(strlen($ans_alls['title'])>26){ echo substr($ans_alls['title'],0,26)."..";}else{ echo $ans_alls['title']; }?><br /><a href="javascript:void(0)" onclick="click_profile_url('<?php echo $get_creator_url['profile_url'];?>')"><?php echo $get_seller_name['creator'];?></a></div>
						<div class="blkC">$<?php echo $ans_alls['price'];?></div>
						<div class="blkD">$<input type="text" id="donate_cart<?php echo $ans_alls['id'];?>" value="<?php echo $ans_alls['donate'];?>" class="field" style="width:25px;" onblur="chk_for_update('donate_cart<?php echo $ans_alls['id'];?>','<?php echo $ans_alls['id'];?>','addtocart')" /></div>
						<div class="blkE">$<?php echo $ans_alls['price'] + $ans_alls['donate'];?></div>
						<div class="blkF"><a href="javascript:void(0);" onclick="update_donate_cart('<?php echo $ans_alls['id'];?>','addtocart')" >Update</a> | <a href="addtocart.php?remove=<?php echo $ans_alls['id'];?>&buyer_id=<?php echo $_SESSION['gen_user_id'];?>&from_url=<?php echo $_GET['from_url']; ?>" onclick ="return confirm_remove()">Remove</a></div>
						<!--<span style="color:red;display:none;font-size:9px;font-weight:bold;" id="error<?php //echo $ans_alls['id'];?>">Item is not updated.</span>-->
					</div>
				<?php
					}
				}
				
				?>
               
            </div>
        </div>
    </div>
</div>
</div>
<div style="display:none">
<form action="add_to_cart_fan.php" method="POST" id="fan_cart_member">
	<input type="text" name="email" id="email" class="fieldText" value="<?php echo $_GET['email'];?>"  />
	<input type="text" name="name" id="name" class="fieldText" value="<?php echo $_GET['name'];?>" />
	<input type="text" name="rel_id" id="rel_id" class="fieldText" value="<?php echo $_GET['rel_id'];?>" />
	<input type="text" name="rel_type" id="rel_type" class="fieldText" value="<?php echo $_GET['rel_type'];?>" />
</form>
</div>

<a href="fanclub_fan_credit.php?amt=<?php echo $sub_total;?>" id="paypal_through_credit_card" style="display:none;"> cred</a>
<input type="hidden" id="idandvalue"/>
</body>
<script type="text/javascript">
function update_cart(id,tbl_name)
{
	var donate = document.getElementById("donate"+id).value;
	location.href = "add_to_cart_fan.php?update_id="+id+"&update_val="+donate+"&buyer_id=<?php echo $get_buyer['general_user_id'];?>"+"&table_name="+tbl_name+"&from_url="+'<?php echo $_GET['from_url']; ?>';
}
function update_donate_cart(id,tbl_name)
{
	var donate = document.getElementById("donate_cart"+id).value;
	location.href = "add_to_cart_fan.php?update_id="+id+"&update_val="+donate+"&buyer_id=<?php echo $get_buyer['general_user_id'];?>"+"&table_name="+tbl_name+"&from_url="+'<?php echo $_GET['from_url']; ?>';
}
function donate_to_purify()
{
	var donate = document.getElementById("donate_purify").value;
	location.href = "add_to_cart_fan.php?donate_purify=1&amount="+donate;
}
function confirm_remove()
{
	var conf = confirm("Are You Sure You Want To Remove This Item From Cart.");
	if(conf == false)
	{
		return false;
	}
}

function IsValidEmail_fan(strValue)
{
 //var objRegExp  =/(^[a-z0-9]([a-z0-9_\.]*)@([a-z0-9_\-\.]*)([.][a-z]{3})$)|(^[a-z0-9]([a-z0-9_\.]*)@([a-z0-9_\-\.]*)(\.[a-z]{2,3})(\.[a-z]{2})*$)/i;
 var objRegExp  = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
 return objRegExp.test(strValue);
}


function get_payment_form()
{
	var donate = document.getElementById("idandvalue").value;
	var donate_purify = document.getElementById("donate_purify").value;
	var cart_items = document.getElementById("grand_total").value;
	var expdonate = donate.split("|");
	var expdonate2 = expdonate[0].split(",");
	if(donate_purify>0 || cart_items>0 || expdonate2[1]>0){
		<?php 
		if(!isset($_SESSION['login_email'])){ //if($_SESSION['login_email']==null)
		?>
			var notlog_user = document.getElementById("user_name_notlog").value;
			var notlog_email = document.getElementById("email_notlog").value;
			if(notlog_user == "" || notlog_email == "" || !IsValidEmail_fan(notlog_email))
			{
				alert("You need to provide you name and email address so we can contact you.");
			}else{
				if(donate_purify !="" || donate_purify !=0 || donate_purify >0){
					openurl = "updatecart.php?alldata="+donate+"&buyer_id="+'<?php echo $get_buyer['general_user_id']; ?>'+"&from_url="+'<?php echo $from_url; ?>&donate_purify=1&amount='+donate_purify+'&notlog_user_name='+notlog_user+'&notlog_user_email='+notlog_email;
				}else{
					openurl = "updatecart.php?alldata="+donate+"&buyer_id="+'<?php echo $get_buyer['general_user_id']; ?>'+"&from_url="+'<?php echo $from_url; ?>'+'&notlog_user_name='+notlog_user+'&notlog_user_email='+notlog_email;
				}
				window.parent.open(openurl, '_blank');
			}
		<?php
		}else{
		?>
		
			//openurl = "https://purifyart.net/donate_pay.php?chkout="+donate+"&session="+'<?php echo $_SESSION['login_email'];?>'+"&buyer_id="+'<?php echo $get_buyer['general_user_id']; ?>'+"&from_url="+'<?php echo $_GET['from_url']; ?>';
			if(donate_purify !="" || donate_purify !=0 || donate_purify>0){
				openurl = "updatecart.php?alldata="+donate+"&buyer_id="+'<?php echo $get_buyer['general_user_id']; ?>'+"&from_url="+'<?php echo $_GET['from_url']; ?>&donate_purify=1&amount='+donate_purify;
			}else{
				openurl = "updatecart.php?alldata="+donate+"&buyer_id="+'<?php echo $get_buyer['general_user_id']; ?>'+"&from_url="+'<?php echo $_GET['from_url']; ?>';
			}
			window.parent.open(openurl, '_blank');
			//parent.window.location.href = "https://purifyart.net/donate_pay.php?chkout="+donate+"&session="+'<?php echo $_SESSION['login_email'];?>'+"&buyer_id="+'<?php echo $get_buyer['general_user_id']; ?>'+"&from_url="+'<?php echo $_GET['from_url']; ?>';
		<?php
		}
		?>
	}else{
		alert("There are no items in your cart to checkout.");
	}
}

function chk_for_update(id,val,tblname)
{
	var a = document.getElementById(id);
	var sendval = document.getElementById("idandvalue");
	if(a.defaultValue==0 && a.value>0){
		sendval.value = sendval.value +","+ a.value +"|"+ val +"|"+tblname;
	}else if(a.defaultValue != a.value){
		sendval.value = sendval.value +","+ a.value +"|"+ val +"|"+tblname;
	}
}
function alerttoupdate(){

//alert("Please update first.");
}

function continue_shopping(){
	var donate = document.getElementById("idandvalue").value;
	var donate_purify = document.getElementById("donate_purify").value;
	if(donate_purify !=0 || donate_purify!="" || donate_purify>0){
		window.parent.location.href ="updatecart.php?alldata="+donate+"&buyer_id="+'<?php echo $get_buyer['general_user_id']; ?>'+"&back_url="+'<?php echo $_GET['from_url']; ?>&donate_purify=1&amount='+donate_purify;
	}else{
		window.parent.location.href ="updatecart.php?alldata="+donate+"&buyer_id="+'<?php echo $get_buyer['general_user_id']; ?>'+"&back_url="+'<?php echo $_GET['from_url']; ?>';
	}
	
//parent.jQuery.fancybox.close();
}
function confirmdelete()
{
	var conf = confirm("Are you sure you want to remove all items from your cart.");
	if(conf == false)
	{
		return false;
	}
}

function click_profile_url(url)
{
	openurl = "/"+url;
	window.parent.open(openurl, '_self');
}

/*function get_payment_form()
{
	if($("#payment_mode").val() == "pay_pal")
	{
		//var pay = <?php if(isset($_REQUEST['order']) && $_REQUEST['order']!=""){ echo $_REQUEST['order'];} else {echo "0";}?>;
		//if(pay != "success")
		//{
			var email = document.getElementById("email").value;
			var name = document.getElementById("name").value;
			var rel_id = document.getElementById("rel_id").value;
			var rel_type = document.getElementById("rel_type").value;
			window.parent.location = "fanclub_fan_paypal.php?email="+email+"&name="+name+"&rel_id="+rel_id+"&rel_type="+rel_type+"&amt="+"<?php echo $sub_total;?>"+"&url_match="+"<?php echo $_GET['data']; ?>";
			//parent.jQuery.fancybox.close();
			//window.parent.location = "fanclub_paypal.php";
		//}
	}
	if($("#payment_mode").val() == "Credit_card")
	{
		$("#paypal_through_credit_card").click();
	}
	if($("#payment_mode").val() == "select")
	{
		alert("Please Select Payment Mode");
		return false;
	}
}*/
/*$(document).ready(function() {
		$("#paypal_through_credit_card").fancybox({
		'autoScale'			: true,
		'transitionIn'		: 'none',
		'transitionOut'		: 'none',
		'type'				: 'iframe'
	});
});*/
</script>
</html>
<?php
if(isset($_POST) && $_POST != null)
{
	$purchase_date = date('Y-m-d');
	$expiry_date = strtotime(date("Y-m-d", strtotime($purchase_date)) . " +1 year");
	$expiry_date = date('Y-m-d', $expiry_date);
	//echo $expiry_date;
	
	if($_POST['rel_type']!='artist' && $_POST['rel_type']!='community')
	{
		$rel_id_p = explode('_',$_POST['rel_id']);
		$rel_type_p = explode('_',$_POST['rel_type']);
		$sql_check_same_fan = $get_fan_club->same_user_check($_POST['email'],$rel_id_p['0'],$rel_type_p['0']);///////////// 0:-Same user and 1:-diff user
	}
	else
	{
		$sql_check_same_fan = $get_fan_club->same_user_check($_POST['email'],$_POST['rel_id'],$_POST['rel_type']);///////////// 0:-Same user and 1:-diff user
	}
	
	if($sql_check_same_fan==0)
	{
		echo "You are this user only.";
	}
	else
	{
		$Get_list = $get_fan_club->get_cart_list($get_buyer['general_user_id']);
		while($row_fan_data = mysql_fetch_assoc($Get_list))
		{
			if($row_fan_data['table_name']=="general_artist"){$rel_type = "artist";$rel_id=$row_fan_data['type_id'];}
			if($row_fan_data['table_name']=="general_community"){$rel_type = "community";$rel_id=$row_fan_data['type_id'];}
			if($row_fan_data['table_name']=="artist_project"){$rel_type = $row_fan_data['table_name'];
				$get_rel = $get_fan_club->get_cart_related_id($row_fan_data['table_name'],$row_fan_data['type_id']);
				$rel_id = $get_rel['artist_id']."_".$row_fan_data['type_id'];
			}
			if($row_fan_data['table_name']=="community_project"){$rel_type = $row_fan_data['table_name'];
				$get_rel = $get_fan_club->get_cart_related_id($row_fan_data['table_name'],$row_fan_data['type_id']);
				$rel_id = $get_rel['community_id']."_".$row_fan_data['type_id'];
			}
			
			$add_fan = $get_fan_club->become_fan_fanclub($_POST['name'],$_POST['email'],$rel_id,$rel_type,$purchase_date,$expiry_date,$row_fan_data['id']);
			//die;
			
			echo "You have successfully added $_POST[name] as a member. They have been sent a mail to verify their membership.";
			
			$to = $_POST['email'];
			$subject = "Fan Club Membership Successfull.";
			
			$chk_registered = $get_fan_club->chk_registered($_POST['email']);
			if($chk_registered==1)			//////// 0:-Not Registered and 1:-Registered User
			{	
				
				if($rel_type!='artist' && $rel_type!='community')
				{
					$rel_id_p = explode('_',$rel_id);
					$rel_type_p = explode('_',$rel_type);
					
					$sql_type_mail = $get_fan_club->get_general_user_id($rel_id_p['0'],$rel_type_p['0']);
				}
				else
				{
					$sql_type_mail = $get_fan_club->get_general_user_id($rel_id,$rel_type);
				}
			
				/////////////////////////////////MAIL BODY///////////////////////////////////////////
				if($rel_type=='artist' || $rel_type=='community')
				{
					$message = "Hello ".$_POST['name'].","."\n \n"."\t\tYou successfully became Fan of ".ucfirst($rel_type)." Profile of '".$sql_type_mail['fname']."'.\n\n\t\tTo view your upgraded Profile please Login To www.purifyart.com.";
				}
				if($rel_type=='artist_project' || $rel_type=='community_project')
				{
					$message = "Hello ".$_POST['sub_name'].","."\n \n"."\t\tYou have successfully became Fan of ".ucfirst($rel_type_p['1'])." Profile of '".ucfirst($rel_type_p['0']).' '.$sql_type_mail['fname']."'.\n\n\t\tTo view your upgraded Profile please Login To www.purifyart.com.";
				}
			}
			else
			{
				if($rel_type!='artist' && $rel_type!='community')
				{
					$rel_id_p = explode('_',$rel_id);
					$rel_type_p = explode('_',$rel_type);
					
					$sql_type_mail = $get_fan_club->get_general_user_id($rel_id_p['0'],$rel_type_p['0']);
				}
				else
				{
					$sql_type_mail = $get_fan_club->get_general_user_id($rel_id,$rel_type);
				}
				
				if($rel_type=='artist' || $rel_type=='community')
				{
					$message = "Hello ".$_POST['name'].","."\n \n"."\t\tYou successfully became Fan of ".ucfirst($rel_type)." Profile of '".$sql_type_mail['fname']."'.\n\n\t\tTo view your upgraded Profile please Sign Up and Login To www.purifyart.com.";
				}
				if($rel_type=='artist_project' || $rel_type=='community_project')
				{
					$message ="Hello ".$_POST['sub_name'].","."\n \n"."\t\tYou have successfully became Fan of ".ucfirst($rel_type_p['1'])." Profile of '".ucfirst($rel_type_p['0']).' '.$sql_type_mail['fname']."'.\n\n\t\tTo view your upgraded Profile please Sign Up and Login To www.purifyart.com.";
				}
			}
			
			$from = "Purify Art < no-reply@purifyart.com>";
			$headers = "From:" . $from;
			
			$url = "admin/newsletter/"; 
			$myFile = $url . "other_templates.txt";
			$fh = fopen($myFile, 'r');
			$theData = fread($fh, filesize($myFile));
			fclose($fh);
			
			$data_email = explode('|~|', $theData);
			
			$ns_user_n = str_replace('{::message::}', $message, $data_email['1']);
			
			$body_email = $data_email['0'] . $ns_user_n . $data_email['2'];
			
			$mail_grid = new SendGrid\Mail();
			
			$mail_grid->addTo($to)->
					setFromName('Purify Art')->
					setFrom('no-reply@purifyart.com')->
					setSubject($subject)->
					setHtml($body_email);
					
			//$sendgrid->web->send($mail_grid);
			$sendgrid -> smtp -> send($mail_grid);
			
			//mail($to,$subject,$message,$headers);
		
			
			$fan_to_user = $sql_type_mail['email'];
			$fan_subject = "User became your fan.";
			
			if($_POST['rel_type_sub']=='artist' || $_POST['rel_type_sub']=='community')
			{
				$fan_message = "Hello ".$sql_type_mail['fname'].","."\n\n\t\tUser '".$_POST['name']."' became Fan of your's ".ucfirst($_POST['rel_type'])." profile successfully.";
			}
			else
			{
				$fan_message = "Hello ".$sql_type_mail['fname'].","."\n\n\t\tUser '".$_POST['name']."' became Fan of your's ".ucfirst($rel_type_p['1'])." of ".ucfirst($rel_type_p['0'])." profile successfully.";
			}
			
			$url = "admin/newsletter/"; 
			$myFile = $url . "other_templates.txt";
			$fh = fopen($myFile, 'r');
			$theData = fread($fh, filesize($myFile));
			fclose($fh);
			
			$data_email = explode('|~|', $theData);
			
			$ns_user_n = str_replace('{::message::}', $fan_message, $data_email['1']);
			
			$body_email = $data_email['0'] . $ns_user_n . $data_email['2'];
			
			$mail_grid = new SendGrid\Mail();
			
			$mail_grid->addTo($fan_to_user)->
					setFromName('Purify Art')->
					setFrom('no-reply@purifyart.com')->
					setSubject($fan_subject)->
					setHtml($body_email);
					
			//$sendgrid->web->send($mail_grid);
			$sendgrid -> smtp -> send($mail_grid);
			
			//mail($fan_to_user,$fan_subject,$fan_message,$headers);
		}
		?>
		<script>
		location.href = "fan_club.php?credit_pay=1";
		</script>
		<?php
	}
}

if($_GET['name']!="" && $_GET['email']!="" && $_GET['rel_id']!="" && $_GET['rel_type']!="" && isset($_GET['url_match']))
{
	$purchase_date = date('Y-m-d');
	$expiry_date = strtotime(date("Y-m-d", strtotime($purchase_date)) . " +1 year");
	$expiry_date = date('Y-m-d', $expiry_date);
	//echo $expiry_date;
	
	if($_GET['rel_type']!='artist' && $_GET['rel_type']!='community')
	{
		$rel_id_p = explode('_',$_GET['rel_id']);
		$rel_type_p = explode('_',$_GET['rel_type']);
		$sql_check_same_fan = $get_fan_club->same_user_check($_GET['email'],$rel_id_p['0'],$rel_type_p['0']);///////////// 0:-Same user and 1:-diff user
	}
	else
	{
		$sql_check_same_fan = $get_fan_club->same_user_check($_GET['email'],$_GET['rel_id'],$_GET['rel_type']);///////////// 0:-Same user and 1:-diff user
	}
	
	if($sql_check_same_fan==0)
	{
		echo "You are this user only.";
	}
	else
	{
		$Get_list = $get_fan_club->get_cart_list($get_buyer['general_user_id']);
		while($row_fan_data = mysql_fetch_assoc($Get_list))
		{
			if($row_fan_data['table_name']=="general_artist"){$rel_type = "artist";$rel_id=$row_fan_data['type_id'];}
			if($row_fan_data['table_name']=="general_community"){$rel_type = "community";$rel_id=$row_fan_data['type_id'];}
			if($row_fan_data['table_name']=="artist_project"){$rel_type = $row_fan_data['table_name'];
				$get_rel = $get_fan_club->get_cart_related_id($row_fan_data['table_name'],$row_fan_data['type_id']);
				$rel_id = $get_rel['artist_id']."_".$row_fan_data['type_id'];
			}
			if($row_fan_data['table_name']=="community_project"){$rel_type = $row_fan_data['table_name'];
				$get_rel = $get_fan_club->get_cart_related_id($row_fan_data['table_name'],$row_fan_data['type_id']);
				$rel_id = $get_rel['community_id']."_".$row_fan_data['type_id'];
			}
		
			$add_fan = $get_fan_club->become_fan_fanclub($_GET['name'],$_GET['email'],$rel_id,$rel_type,$purchase_date,$expiry_date,$row_fan_data['id']);
			
			echo "You have successfully added $_GET[name] as a member. They have been sent a mail to verify their membership.";
			
			$to = $_GET['email'];
			$subject = "Fan Club Membership Successfull.";
			
			$chk_registered = $get_fan_club->chk_registered($_GET['email']);
			if($chk_registered==1)			//////// 0:-Not Registered and 1:-Registered User
			{	
				
				if($rel_type!='artist' && $rel_type!='community')
				{
					$rel_id_p = explode('_',$rel_id);
					$rel_type_p = explode('_',$rel_type);
					
					$sql_type_mail = $get_fan_club->get_general_user_id($rel_id_p['0'],$rel_type_p['0']);
				}
				else
				{
					$sql_type_mail = $get_fan_club->get_general_user_id($rel_id,$rel_type);
				}
			
				/////////////////////////////////MAIL BODY///////////////////////////////////////////
				if($rel_type=='artist' || $rel_type=='community')
				{
					$message = "Hello ".$_GET['name'].","."\n \n"."\t\tYou successfully became Fan of ".ucfirst($rel_type)." Profile of '".$sql_type_mail['fname']."'.\n\n\t\tTo view your upgraded Profile please Login To www.purifyart.com.";
				}
				if($rel_type=='artist_project' || $rel_type=='community_project')
				{
					$message = "Hello ".$_GET['sub_name'].","."\n \n"."\t\tYou have successfully became Fan of ".ucfirst($rel_type_p['1'])." Profile of '".ucfirst($rel_type_p['0']).' '.$sql_type_mail['fname']."'.\n\n\t\tTo view your upgraded Profile please Login To www.purifyart.com.";
				}
			}
			else
			{
				if($rel_type!='artist' && $rel_type!='community')
				{
					$rel_id_p = explode('_',$rel_id);
					$rel_type_p = explode('_',$rel_type);
					
					$sql_type_mail = $get_fan_club->get_general_user_id($rel_id_p['0'],$rel_type_p['0']);
				}
				else
				{
					$sql_type_mail = $get_fan_club->get_general_user_id($rel_id,$rel_type);
				}
				
				if($rel_type=='artist' || $rel_type=='community')
				{
					$message = "Hello ".$_GET['name'].","."\n \n"."\t\tYou successfully became Fan of ".ucfirst($rel_type)." Profile of '".$sql_type_mail['fname']."'.\n\n\t\tTo view your upgraded Profile please Sign Up and Login To www.purifyart.com.";
				}
				if($rel_type=='artist_project' || $rel_type=='community_project')
				{
					$message ="Hello ".$_GET['sub_name'].","."\n \n"."\t\tYou have successfully became Fan of ".ucfirst($rel_type_p['1'])." Profile of '".ucfirst($rel_type_p['0']).' '.$sql_type_mail['fname']."'.\n\n\t\tTo view your upgraded Profile please Sign Up and Login To www.purifyart.com.";
				}
			}
			
			$from = "Purify Art < no-reply@purifyart.com>";
			$headers = "From:" . $from;
			
			$url = "admin/newsletter/"; 
			$myFile = $url . "other_templates.txt";
			$fh = fopen($myFile, 'r');
			$theData = fread($fh, filesize($myFile));
			fclose($fh);
			
			$data_email = explode('|~|', $theData);
			
			$ns_user_n = str_replace('{::message::}', $message, $data_email['1']);
			
			$body_email = $data_email['0'] . $ns_user_n . $data_email['2'];
			
			$mail_grid = new SendGrid\Mail();
			
			$mail_grid->addTo($to)->
					setFromName('Purify Art')->
					setFrom('no-reply@purifyart.com')->
					setSubject($subject)->
					setHtml($body_email);
					
			//$sendgrid->web->send($mail_grid);
			$sendgrid -> smtp -> send($mail_grid);
			
			//mail($to,$subject,$message,$headers);
		
			
			$fan_to_user = $sql_type_mail['email'];
			$fan_subject = "User became your fan.";
			
			if($_GET['rel_type_sub']=='artist' || $_GET['rel_type_sub']=='community')
			{
				$fan_message = "Hello ".$sql_type_mail['fname'].","."\n\n\t\tUser '".$_GET['name']."' became Fan of your's ".ucfirst($rel_type)." profile successfully.";
			}
			else
			{
				$fan_message = "Hello ".$sql_type_mail['fname'].","."\n\n\t\tUser '".$_GET['name']."' became Fan of your's ".ucfirst($rel_type_p['1'])." of ".ucfirst($rel_type_p['0'])." profile successfully.";
			}
			
			$url = "admin/newsletter/"; 
			$myFile = $url . "other_templates.txt";
			$fh = fopen($myFile, 'r');
			$theData = fread($fh, filesize($myFile));
			fclose($fh);
			
			$data_email = explode('|~|', $theData);
			
			$ns_user_n = str_replace('{::message::}', $fan_message, $data_email['1']);
			
			$body_email = $data_email['0'] . $ns_user_n . $data_email['2'];
			
			$mail_grid = new SendGrid\Mail();
			
			$mail_grid->addTo($fan_to_user)->
					setFromName('Purify Art')->
					setFrom('no-reply@purifyart.com')->
					setSubject($fan_subject)->
					setHtml($body_email);
					
			//$sendgrid->web->send($mail_grid);
			$sendgrid -> smtp -> send($mail_grid);
			
			//mail($fan_to_user,$fan_subject,$fan_message,$headers);
			?>
			<script>
				location.href="<?php echo $_GET['url_match'];?>";
			</script>
			<?php
			//header("Location: ".$_GET['url_match']);
		}
	}
}
}
?>
