<?php
	include_once('commons/session_check.php');
	include_once('classes/Team.php');
	include_once('classes/Type.php');
	include_once('commons/db.php');
	//var_dump($_SERVER['HTTPS']);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "//www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="//www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<title>Purify Entertainment: About</title>
<?php include_once('includes/header.php'); ?>
<div id="outerContainer"></div>
<?php 
	$obj= new Team();
	$objType=new Type();

	//$rs=$obj->displayTeam();
	$rs=$obj->displayAllTeam();
	$rs_direc=$obj->displaydirector();
	//echo "Printing Data<br/>";
	/*if(isset($rs))   //Check sub type is set
		{
			foreach(mysql_fetch_assoc($rs) as $v)
			{
				print_r($v);
				echo "<br>";
			}
		}*/
	//print_r(mysql_fetch_assoc($rs));
	
	$flag1=0;
	$flag2=0;
	$flag3=0;
	$flag4=0;
	$flag_5=0;
	
?>
	
  	<div id="contentContainer" >
  	  <!--<div id="subNavigation">
        <div id="sectionTitle"><a href="../Demo/about.html">About Us</a></div>
  	    <ul>
          <li><a href="../Demo/about_affiliates.html">Affiliate Program</a></li>
          <li><a href="../Demo/about_agreements.html">Agreements</a><a href="busplan.html"></a></li>
          <li><a href="../Demo/about_contacts.html">Contact Us</a></li>
          <li><a href="../Design/about_news.php">News History</a></li>
          <li><a href="../Design/about_suggestions.php">Suggestions</a></li>
          <li><a href="../Design/about_team.php">Team</a></li>
        </ul>
      </div>-->
  	  	<div id="actualContent" class="sidepages">
      <h1>Purify Team</h1>
       <p>Purify Art's team has Board of Directors, Advisors, Ambassadors and Developers. Below is a list of our team members:</p>
		
<?php 
		if(mysql_num_rows($rs_direc)>0)
		{
			while($row = mysql_fetch_assoc($rs_direc))
			{
		//var_dump($row);
	
			$type=$objType->getTypeById($row['type']);
			
			//GET IMAGE WIDTH & HEIGHT
			if($type=='Director')
			{
			
if($flag_5==0){
$flag_5=1;

?>	
        <h3>Board of Directors</h3><?php } ?>
		<div class="teamItem">
		<h5><span class="featuredPhoto">
		<?php
			if(isset($row['image_name']) && !empty($row['image_name']) && $row['image_name']!="")
			{
				$img_name = explode('_',$row['image_name']);
				if($img_name[0]=='thumbnail')
				{
		?>
					<img src="https://teamjcroppro.s3.amazonaws.com/<?php echo $row['image_name']; ?>" alt="<?php echo $row['name']; ?>" width="100" height="100" />
		<?php
				}
				else
				{
		?>
					<img src="https://arteventprofile.s3.amazonaws.com/Noimage.png" alt="<?php echo $row['name']; ?>" width="100" height="100" />
		<?php
				}
			}
			else
			{
		?>
				<img src="https://arteventprofile.s3.amazonaws.com/Noimage.png" alt="<?php echo $row['name']; ?>" width="100" height="100" />
		<?php
			}
		?>
		</span>     		
			<?php echo ucwords($row['name']); ?></h5>
      		<!--<p>Since<?php //echo " ".$row['date_time']; ?></p>-->
      		
			<?php echo $row['bio'];
			//echo substr($row['bio'],0,600);
				if(strlen($row['bio'])>1000)
				{
					//echo '...';
				}
			?>
</div>
           
		   <?php 
		   }
		   	}
		}	
		while($row=mysql_fetch_assoc($rs))
		{	
			$type=$objType->getTypeById($row['type']);
			if($type=='Adviser')
			{
			
if($flag1==0){
$flag1=1;
?>	
		
        <h3>Advisors</h3><?php }?>
       	<div class="teamItem">
		<h5><span class="featuredPhoto">
		<?php
			if(isset($row['image_name']) && !empty($row['image_name']) && $row['image_name']!="")
			{
				$img_name = explode('_',$row['image_name']);
				if($img_name[0]=='thumbnail')
				{
		?>
					<img src="https://teamjcroppro.s3.amazonaws.com/<?php echo $row['image_name']; ?>" alt="<?php echo $row['name']; ?>" width="100" height="100" />
		<?php
				}
				else
				{
		?>
					<img src="https://arteventprofile.s3.amazonaws.com/Noimage.png" alt="<?php echo $row['name']; ?>" width="100" height="100" />
		<?php
				}
			}
			else
			{
		?>
				<img src="https://arteventprofile.s3.amazonaws.com/Noimage.png" alt="<?php echo $row['name']; ?>" width="100" height="100" />
		<?php
			}
		?>
		</span>     		
			<?php echo ucwords($row['name']); ?></h5>
      		<!--<p>Since<?php //echo " ".$row['date_time']; ?></p>-->
      		
			<?php echo $row['bio'];
			//echo substr($row['bio'],0,600);
				if(strlen($row['bio'])>1000)
				{
					//echo '...';
				}
			?>
 <!--<p>&nbsp;</p>-->
</div>
		   <?php 
		   	}
		  
		   
		   if($type=='Programmer')
			{
			
			if($flag2==0){
			
			$flag2=1;
?>	
        <h3>Programmer</h3><?php } ?>
    
       	<div class="teamItem">
		<h5><span class="featuredPhoto">
		<?php
			if(isset($row['image_name']) && !empty($row['image_name']) && $row['image_name']!="")
			{
				$img_name = explode('_',$row['image_name']);
				if($img_name[0]=='thumbnail')
				{
		?>
					<img src="https://teamjcroppro.s3.amazonaws.com/<?php echo $row['image_name']; ?>" alt="<?php echo $row['name']; ?>" width="100" height="100" />
		<?php
				}
				else
				{
		?>
					<img src="https://arteventprofile.s3.amazonaws.com/Noimage.png" alt="<?php echo $row['name']; ?>" width="100" height="100" />
		<?php
				}
			}
			else
			{
		?>
				<img src="https://arteventprofile.s3.amazonaws.com/Noimage.png" alt="<?php echo $row['name']; ?>" width="100" height="100" />
		<?php
			}
		?>
			</span>
			<?php echo ucwords($row['name']); ?></h5>
      		<!--<p>Since<?php //echo " ".$row['date_time']; ?></p>-->
      		
			<?php echo $row['bio'];
			//echo substr($row['bio'],0,600);
				if(strlen($row['bio'])>1000)
				{
					//echo '...';
				}
			?>
 <!--<p>&nbsp;</p>-->
  </div>        
		   <?php 
		   	}
		  
		   
		   if($type=='Ambassador')
			{
				
			if($flag3==0){
			$flag3=1;
?>	
        <h3>Ambassadors</h3><?php }?>
        	<div class="teamItem">
		<h5><span class="featuredPhoto">
		<?php
			if(isset($row['image_name']) && !empty($row['image_name']) && $row['image_name']!="")
			{
				$img_name = explode('_',$row['image_name']);
				if($img_name[0]=='thumbnail')
				{
		?>
					<img src="https://teamjcroppro.s3.amazonaws.com/<?php echo $row['image_name']; ?>" alt="<?php echo $row['name']; ?>" width="100" height="100" />    		
		<?php
				}
				else
				{
		?>
					<img src="https://arteventprofile.s3.amazonaws.com/Noimage.png" alt="<?php echo $row['name']; ?>" width="100" height="100" />
		<?php
				}
			}
			else
			{
		?>
				<img src="https://arteventprofile.s3.amazonaws.com/Noimage.png" alt="<?php echo $row['name']; ?>" width="100" height="100" />
		<?php
			}
		?>
			</span> 
			<?php echo ucwords($row['name']); ?></h5>
      		<!--<p>Since<?php //echo " ".$row['date_time']; ?></p>-->
      		
			<?php echo $row['bio']; //echo substr($row['bio'],0,600);
				if(strlen($row['bio'])>1000)
				{
					//echo '...';
				}
			?>

          <!-- <p>&nbsp;</p>-->
		   	</div>
		   <?php 
		   	}
			
			
			if($type=='Developer')
			{
				if($flag4==0)
				{
					$flag4=1;
			?>	
					<h3>Developers</h3>
				<div class="teamItem">
			<?php
				}
			?>
				<h5><span class="featuredPhoto">
			<?php
					if(isset($row['image_name']) && !empty($row['image_name']) && $row['image_name']!="")
					{
						$img_name = explode('_',$row['image_name']);
						if($img_name[0]=='thumbnail')
						{
				?>
							<img src="https://teamjcroppro.s3.amazonaws.com/<?php echo $row['image_name']; ?>" alt="<?php echo $row['name']; ?>" width="100" height="100" />
				<?php
						}
						else
						{
				?>
							<img src="https://arteventprofile.s3.amazonaws.com/Noimage.png" alt="<?php echo $row['name']; ?>" width="100" height="100" />
				<?php
						}
					}
					else
					{
			?>
						<img src="https://arteventprofile.s3.amazonaws.com/Noimage.png" alt="<?php echo $row['name']; ?>" width="100" height="100" />
			<?php
					}
			?>
					</span><?php echo ucwords($row['name']); ?>
				</h5>
				<p>
				<?php
					$data = str_replace("<p>","",$row['bio']);
					$finaldata = str_replace("</p>","<br/>",$data);
					echo substr($finaldata,0,605);
				?>
				</p>
				<!--<p>&nbsp;  </p>-->
				</div>
				<?php
		   	}
		 
		}
		?>  
             
            
      </div>
		<div id="snipeSidebar">
    	<div class="snipe">
      	<h2>What is Purify Art?</h2>
        <p>Purify Art provides fans, artists, musicians, and businesses tools and opportunities to access the arts.  <a href="about.php">More ></a></p>
      </div>
      <div class="snipe">
      	<h2>Donate Now!</h2>
        <p>Donate to Purify Art and further the development of our online services for artists. <a href="help_donate.php">More ></a></p>
      </div>
    </div>
    <div class="clearMe"></div>
  </div>
  

<?php include_once('includes/footer.php'); ?>