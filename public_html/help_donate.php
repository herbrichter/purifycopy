<?php
	include_once("commons/db.php");
	include_once("classes/Donate_Purify.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "//www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="//www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<title>Purify Art: Donate</title>
<?php
$donate_obj = new Donate_Purify();

if(!isset($_SESSION['login_email']) || $_SESSION['login_email']==null)
{
	$total = 0;
	$get_all_data = $donate_obj->Get_all_cart_data_not_login($_SERVER["REMOTE_ADDR"]);
	$purify_donate = $donate_obj->get_purify_donation();
	if(mysql_num_rows($get_all_data)>0){
		while($ans_alls = mysql_fetch_assoc($get_all_data)){
			$total  = $total + $ans_alls['price'] + $ans_alls['donate'];
		}
	}
	$total = $total + $purify_donate['amount'];
}else{
	$get_login_det = $donate_obj->get_login_cart_detail();
	$get_all_data_fan = $donate_obj->get_Cart($get_login_det['general_user_id']);
	$get_all_data = $donate_obj->Get_all_cart_data($get_login_det['general_user_id']);
	$purify_donate = $donate_obj->get_purify_donation();
	if(mysql_num_rows($get_all_data_fan)>0){
		while($get_cart = mysql_fetch_assoc($get_all_data_fan)){
			$total  = $total + $get_cart['price'] + $get_cart['donate'];
		}
	}
	if(mysql_num_rows($get_all_data)>0){
		while($ans_alls = mysql_fetch_assoc($get_all_data)){
			$total  = $total + $ans_alls['price'] + $ans_alls['donate'];
		}
	}
	$total = $total + $purify_donate['amount'];
}
//echo $total;

//session_start();
include("includes/header.php");
?>
<script>
	/* $(document).ready(function() {
		$("#donates").fancybox({
		'autoScale'			: true,
		'transitionIn'		: 'none',
		'transitionOut'		: 'none',
		'type'				: 'iframe'
		});
	}); */
</script>
<script>
	function validate_fields_don()
	{
		var amount_d = document.getElementById("don_amount").value;
		/*var email_d = document.getElementById("don_email").value;
		var name_d = document.getElementById("don_name").value;
		
		
		var atpos = email_d.indexOf("@");
		var dotpos = email_d.lastIndexOf(".");
		if (atpos<1 || dotpos<atpos+2 || dotpos+2>=email_d.lenght || email_d=="" || email_d==null)
		{
			alert("Please enter a valid email address.");
			return false;
		}*/
		
		if(amount_d=="" || amount_d==null)
		{
			alert("Please enter some amount.");
			return false;
		}
		alert(isNaN(amount_d));
		return false;
		/* else
		{
			alert("You Donation is done successfully. Thanks for your contribution.");
		} */
	}
function change_action()
{
	var amount_d = document.getElementById("don_amount").value;
	if(amount_d=="" || amount_d.trim()=="" || amount_d==null)
	{
		alert("Please enter some amount.");
		return false;
	}
	else if(amount_d<=0){
		alert("You may not enter $0 in the Donate to Purify Art field, either enter nothing or a value greater than 0.");
		return false;
	}
	if(isNaN(amount_d)==true)
	{
		alert("Please enter a valid amount.");
		return false;
	}
		
	var frm_obj=document.getElementById("donation_form");
	var frm_txt_val = document.getElementById("don_amount");
	var old_val = '<?php echo $total;?>';
	var new_val = parseInt(old_val) + parseInt(frm_txt_val.value);
	frm_obj.action = "donate_pay.php?chkout="+new_val+"&session=<?php echo $_SESSION['login_email'];?>&buyer_id=<?php echo $get_login_det['general_user_id'];?>";
	frm_obj.submit();
}
</script>

	<div id="outerContainer">
</div>
<div id="contentContainer" >
	<div id="actualContent" class="sidepages">
      <h1>Donate</h1>
      <p>A donation to Purify Art supports the creative freedom and independence artists need to thrive. Your contribution helps us to continually improve our <a href="about_services.php">services</a> for fans, artists, and musicians, including social media, career management tools, and digital sales from one web based platform. Your gift will support Purify Art serving your global artistic community. Together we are a movement that empowers people to follow their dreams.</p>
    </div>
    <div id="snipeSidebar">
    	<div class="snipe">
      	<h2>ART WITH INTEGRITY</h2>
        <p>Find and promote music, art and video on our ad-free platform.<a href="about.php"> More></a></p>
      </div>
    </div>
	<div style="float: left;">
			<div id="actualContent" style="width:500px;">
	<form target="_blank" id="donation_form" action="donate_pay.php" method="POST" onSubmit="return validate_fields_don();">
					<!--<div class="fieldCont" style="">
						<div class="fieldTitle">Email</div>
						<?php
							/*if(isset($_SESSION['login_email']))
							{
						?>
								<input style="height:23px;" disabled="true" value="<?php echo $_SESSION['login_email']; ?>" type="text" name="don_email" id="don_email" class="fieldText" />
						<?php
							}
							else
							{
						?>
								<input style="height:23px;" type="text" name="don_email" id="don_email" class="fieldText" />
						<?php
							}
						?>
					</div>
					<div class="fieldCont" style="">
						<div class="fieldTitle">Name</div>
						<?php
							if(isset($_SESSION['name']) && isset($_SESSION['login_email']))
							{
						?>
								<input style="height:23px;" disabled value="<?php echo $_SESSION['name']; ?>" type="text" name="don_name" id="don_name" class="fieldText" />
						<?php
							}
							else
							{
						?>
								<input style="height:23px;" type="text" name="don_name" id="don_name" class="fieldText" />
						<?php
							}*/
						?>
					</div>-->
					<div class="fieldCont" style="width:335px;">
						<!--<div class="fieldTitle" style="padding: 0 0 0 0;">Amount:</div>-->
                        <div style="padding: 0 0 0 0; width:20px; font-size:15px; float:left; margin-top:9px;">$</div>
                        <div style="padding: 0 0 0 0; float:left;">
						<input style="height:25px; width:85px; float:left;" type="text" name="don_amount" id="don_amount" class="fieldText" /></div>
                        <div style="width:200px; float:left; margin-left:10px;">
                        <input  type="button"  value="Donate" onClick="change_action()" id="donate_button" style="color: rgb(255, 255, 255); font-family: 'futura-pt',sans-serif; font-size: 12px; font-weight: bold; padding: 7px 9px; float: left; margin-right: 10px; border: 0px none; background-color: rgb(0, 0, 0); float:left; border-radius:5px; -webkit-border-radius:5px; -moz-border-radius:5px; -ms-border-radius:5px; -o-border-radius:5px;"/> </div>
                       
					</div>
					
				</form>
				</div>
				</div>
                
    <div id="actualContent" class="sidepages">
    <br /><br />
      <h1>Supporters</h1>
      <p>Thank you to the following people who have supported Purify Art with generous contributions.</p>
      <div class="suppCont">      	
        <div class="tableRow">
        	<div class="suppCol">Bryan Richter</div>
            <div class="suppCol">Herb Richter</div>
            <div class="suppCol">Nateon Ajello</div>
        </div>
        <div class="tableRow">
        	<div class="suppCol">Michael Ricci</div>
            <div class="suppCol">Anonymous</div>
            <div class="suppCol">Rex Yarwood</div>
        </div>
         <div class="tableRow">
        	<div class="suppCol">Brendan Hilley</div>
            <div class="suppCol">Dan Louth</div>
            <div class="suppCol">Rob Smiley</div>
        </div>
         <div class="tableRow">
        	<div class="suppCol">John Hilley</div>
            <div class="suppCol">Jay Trexler</div>
            <div class="suppCol">Anonymous</div>
        </div>
         <div class="tableRow">
        	<div class="suppCol">Stevan Richter</div>
            <div class="suppCol">Anonymous</div>
            <div class="suppCol">Eric Barry</div>
        </div>        
         <div class="tableRow">
        	<div class="suppCol">Anonymous</div>
            <div class="suppCol">Lila Meyer</div>
            <div class="suppCol">Michael Pisani</div>
        </div>
         <div class="tableRow">
        	<div class="suppCol">Rose Bennington</div>
            <div class="suppCol">Maria Conrad</div>
            <div class="suppCol">Maggie Shinnerer</div>
        </div>
         <div class="tableRow">
        	<div class="suppCol">Anonymous</div>
            <div class="suppCol">Adrian Giovenco</div>
        </div>
      </div>
    </div>
    <div class="clearMe"></div>
</div>

</div><?php include_once("displayfooter.php"); ?></body>
</html><?php
if(!empty($_POST))
{
	
	//if(isset($_SESSION['login_email']))
	//{
		//$donate = $donate_obj->insert_donate($_SESSION['login_id'],$_SESSION['login_email'],$_SESSION['loname'],$_POST['don_amount']);
	//}
	//else
	//{
		$id = 0;
		$donate = $donate_obj->insert_donate($_POST['don_amount']);
	//}
	
	//echo "You Donation is done successfully. Thanks for your contribution.";
}

/* if(isset($_POST) && $_POST!="" && $_POST!= null)
{
	$gen_info = mysql_query("select * from general_user where email ='".$_SESSION['login_email']."' AND delete_status=0 AND active=0");
	if(mysql_num_rows($gen_info)>0)
	{
		$row = mysql_fetch_assoc($gen_info);
		$find_data = mysql_query("select * from donate where general_user_id = '".$row['general_user_id']."'");
		if(mysql_num_rows($find_data)>0)
		{
			mysql_query("update donate set donate_amount = '".$_POST['donate']."' where general_user_id = '".$row['general_user_id']."'");
		}
		else
		{
			mysql_query("insert into donate (general_user_id,donate_amount)values('".$row['general_user_id']."','".$_POST['donate']."')");
		}
	}
	?>
		<script>
			window.location = 'help_donate.php';
		</script>
	<?php
} */