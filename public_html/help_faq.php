<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "//www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="//www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<title>Purify Art: Help: FAQ</title>

<?php include("includes/header.php");?>
<div id="outerContainer"></div>
 <div id="contentContainer" >
	<div id="actualContent" class="sidepages">
      <h1>Frequently Asked Questions</h1>
      <p><a href="#General">General</a><br />
        <a href="#Registration">Registration</a><br />
        Sharing, Viewing, and Selling Media<br />
        Selling Fan Club Memberships<br />
        Distribution and Publishing</p>
<h3><a name="General" id="General"></a>General</h3>
      <h5>How is  Purify Art a &quot;Not for Profit&quot;?</h5>
      <p> Purify Art is operated as a not for profit public benefit organization. This means that it was created for public benefit and not for the private gain of any person(s). The majority of employees are independent contractors paid fair wages and managment will be paid modest salaries. <u>Purify Art is not, and never will be for sale;  you can trust that our services will not degrade or be adultarated for the sake of profits.</u></p>
      <h5>If there are no Ads, how does Purify Art get paid?</h5>
      <p> Purify Art is able to give users a web presence and services with integrity, ad free, by raising revenue in three ways: 1. Purify Art membership sales 2. Revenue shares from transactions through the Purify Store (5% after transcaction fee) and 3. Donations and Grants. </p>
      <h5>Are  payment processing fees included in the revenue share rates?</h5>
      <p> Yes, we pay all payment processing fees. You receive 95% of your asking price, no hidden fees.</p>
      <h5>Currency  conversion?</h5>
      <p> All transactions on Purify Art are in US Dollars. For purchasing currency conversion is handled  automatically by our payment processing service. For non check payments currency conversion is handled by either Paypal or Xoom. Check payments are only in US Dollars.</p>
      <h5>If I do not renew my Purify Membership, are my items still for sale?</h5>
      <p> Yes, all items chosen to be sold in the Purify Art store will remain for sale. On the day your balance becomes $25 or more your membership will automatically renew and you will be sent an email. At that time you may take your media down for sale if you wish or continue using the membership.</p>
      <h5>Can I remove my items for sale?</h5>
      <p> Yes, all items chosen to be sold in the Purify Art store can be taken down by editing your profiles at any time.</p>
      <h3><a name="Registration" id="Registration"></a>Registration</h3>
      <p>Coming Soon</p>
    </div>
    <div id="snipeSidebar">
    	<div class="snipe">
      	<h2>What is Purify Art?</h2>
        <p>Purify Art provides fans, artists, musicians, and businesses tools and opportunities to access the arts. <a href="about.php">More ></a></p>
      </div>
      <div class="snipe">
      	<h2>Pricing?</h2>
        <p>What is free and what premium services are included with an annual membership? <a href="help_pricing.php">More ></a></p>
      </div>
    </div>
    
    <div class="clearMe"></div>
    
  </div>

</div><?php include_once("displayfooter.php"); ?>
</body>
</html>