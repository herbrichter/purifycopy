<?php
    ob_start();
	include_once('login_includes.php');
	include_once('classes/User.php');
	include_once('classes/UnregUser.php');
	
	$verification_no=$_GET['id'];
	$unreg_user_id=$_GET['uid'];
	
	if($verification_no)
	{
		$obj=new User();
		if($obj->updateNewsletter($verification_no))
			$msg="You have been successfully unsubscribed from our newsletter service.";
		else
			$msg="Error in the process. Please try again.";
	}
	else if($unreg_user_id)
	{
		$obj2=new UnregUser();
		if($obj2->updateNewsletter($unreg_user_id))
			$msg="You have been successfully unsubscribed from our newsletter service.";
		else
			$msg="Error in the process. Please try again.";
	}
	else
	{
		$msg="Invalid request. Please enusre you have complete url.";
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<title>Purify Entertainment: Six Feature</title>
<link rel="stylesheet" type="text/css" media="screen" href="includes/purify.css" />
<?php include_once('login_js.php'); ?>
</head>
<body>

<div id="outerContainer">


  <div id="purifyMasthead">
    <div id="purifyLogo"><a href="../Demo/index.html"><img src="images/4.gif" alt="Purify Entertainment" width="302" height="53" border="0" /></a></div>
	<?php include("login_area.php"); ?>
  </div>

  
	<div id="toplevelNav">
    <ul>
      <li><a href="../Demo/index.html">Home</a></li>
      <li><a href="../Demo/about.html">About</a></li>
      <li><a href="../Demo/artists.html">Artists</a></li>
      <li><a href="../Demo/projects.html">Projects</a></li>
      <li><a href="../Demo/events.html">Events</a></li>
      <li><a href="../Demo/community.html">Community</a></li>
      <li><a href="../Demo/services.html">Services</a></li>
      <li><a href="../Demo/search.html">Search</a></li>
    </ul>
  </div>

	
  <div id="contentContainer">
    <h1>Newsletter Subscription</h1>
    <p><?php echo $msg; ?></p>
  </div>
  

<div id="footer"><strong>2008 Purify Entertainment</strong> This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/3.0/">Creative Commons Attribution-Share Alike 3.0 Unported License</a>.<br />
  <a href="/Demo/privacypolicy.html">Privacy Policy</a> / <a href="/Demo/useragreement.html">User Agreement</a> / <a href="/Demo/originalityagreement.html">Originality Agreement</a> / <a href="/Demo/busplan.html">Business Plan</a> </div>

<!-- end of main container -->
</div>

</body>
</html>