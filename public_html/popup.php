<?php
include_once('commons/db.php');
session_start();
?>
<link href="includes/purify.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript">
function userstatus()
{
	var a=document.getElementById("getstatuspop").value;
	window.parent.document.getElementById("editstatus").value = a;	
	window.parent.document.getElementById("dis_status_text").innerHTML = a;	
	parent.jQuery.fancybox.close();
}
function userfname()
{
	var a=document.getElementById("getfirstnamepop").value;
	window.parent.document.getElementById("editfname").value = a;	
	parent.jQuery.fancybox.close();
}
function userlname()
{
	var a=document.getElementById("getlastnamepop").value;
	window.parent.document.getElementById("editlname").value = a;	
	parent.jQuery.fancybox.close();
}
function useremail()
{
	var a=document.getElementById("getemail").value;
	window.parent.document.getElementById("editemail").value = a;	
	parent.jQuery.fancybox.close();
}
function userphone()
{
	var a=document.getElementById("getphone").value;
	window.parent.document.getElementById("editphone").value = a;	
	parent.jQuery.fancybox.close();
}
function usercity()
{
	var a=document.getElementById("getcity").value;
	window.parent.document.getElementById("editcity").value = a;	
	parent.jQuery.fancybox.close();
}
function userstreet()
{
	var a=document.getElementById("getstreetpop").value;
	window.parent.document.getElementById("editaddress").value = a;	
	parent.jQuery.fancybox.close();
}
function userpostal()
{
	var a=document.getElementById("getpostalpop").value;
	window.parent.document.getElementById("edit_postal_code").value = a;	
	parent.jQuery.fancybox.close();
}
function clickartistimage()
{
	var a=document.getElementById("artist_image").src;
	window.parent.document.getElementById("disprofileimage").src = a;
	var str = a.split("/");
	window.parent.document.getElementById("profileimagename").value =str[6];	
	parent.jQuery.fancybox.close();
}
function clickcommunityimage()
{
	var a=document.getElementById("community_image").src;
	window.parent.document.getElementById("disprofileimage").src = a;
	var str = a.split("/");
	window.parent.document.getElementById("profileimagename").value =str[6];	
	parent.jQuery.fancybox.close();
}
function close_fancy_box()
{
	parent.jQuery.fancybox.close();
}
function delete_account()
{
	parent.window.location.href="add_general_info.php?delete=1";
}
</script>
<?php
$a = $_GET['whom'];
//echo $_GET['whom'];
//echo $a;
if($a=='postal')
{
$postal=$_GET['postal'];
?>
Postal Code : <input type="text" id="getpostalpop" value="<?php echo $postal;?>" /> <button onclick="userpostal()" >Save</button>
<?php
}
if($a=='street')
{
$street=$_GET['street'];
?>
<table><tr><td>Street Address :</td><td><textarea type="text" id="getstreetpop" ><?php echo $street;?></textarea></td><td><button onclick="userstreet()" >Save</button></td></tr></table>
 
<?php
}
if($a=='status')
{
$status=$_GET['status'];
?>

<div>
	<div class="fancy_header">
		Change Status!
	</div>
	<div class="fancy_box_remove_content">
		Status : <input type="text" id="getstatuspop" style="height:24px;" value="<?php echo $status;?>" /> <input type="button" value="Save" onclick="userstatus()" style="background-color: #000; border: 0 none; color: #FFFFFF; font-family: 'futura-pt',sans-serif; font-size: 0.825em; font-weight: bold; padding: 6px 10px;border-radius:5px;" />
	</div>
</div>


<?php
}
if($a=='fname')
{
$fname=$_GET['firstname'];
?>
First Name : <input type="text" id="getfirstnamepop" value="<?php echo $fname;?>" /> <button onclick="userfname()" >Save</button>
<?php
}
if($a=='lname')
{
$lname=$_GET['lastname'];
?>
Last Name : <input type="text" id="getlastnamepop" value="<?php echo $lname;?>" /> <button onclick="userlname()" >Save</button>
<?php
}
if($a=='email')
{
$email=$_GET['Emailid'];
?>
Email : <input type="text" id="getemail" value="<?php echo $email;?>" /> <button onclick="useremail()">Save</button>
<?php
}
if($a=='phone')
{
$phone=$_GET['phoneno'];
?>
Phone : <input type="text" id="getphone" value="<?php echo $phone;?>" /> <button onclick="userphone()">Save</button>
<?php
}
if($a=='city')
{
$city=$_GET['cityname'];
?>
City : <input type="text" id="getcity" value="<?php echo $city;?>" /> <button onclick="usercity()">Save</button>
<?php
}
if($a=='profileimage')
{
	$sql=mysql_query("select * from general_user where email='".$_SESSION['login_email']."'");
	$res=mysql_fetch_assoc($sql);
	if($res['community_id']>0)
	{
		$sel_community_image=mysql_query("select * from general_community where community_id=$res[community_id]");
		$get_community_image=mysql_fetch_assoc($sel_community_image);
		$pos = strpos($get_community_image['image_name'], "thumbnail");
		if($pos>0)
		{
			?>
				<a><img  id="community_image" src="uploads/profile_pic/<?php echo $get_community_image['image_name'];?>" onclick="clickcommunityimage()"/></a>
			<?php
		}
		/*else
		{
			?>
				<a><img id="community_image" src="uploads/profile_pic/Noimage.png" onclick="clickcommunityimage()" height="100" width="114" /></a>
			<?php
		}*/
	}
	if($res['artist_id'] > 0)
	{
		$sel_artist_image=mysql_query("select * from general_artist where artist_id=$res[artist_id]");
		$get_artist_image=mysql_fetch_assoc($sel_artist_image);
		$pos = strpos($get_artist_image['image_name'], "thumbnail");
		if($pos>0)
		{
			
			?>
				<a><img id="artist_image" onclick="clickartistimage()" src="uploads/profile_pic/<?php echo $get_artist_image['image_name'];?>"/></a>
			<?php
		}
		/*else
		{
			?>
				<a><img id="artist_image" src="uploads/profile_pic/Noimage.png" height="100" onclick="clickartistimage()" width="114" /></a>
			<?php
		}*/
	}
	
		$sel_community_image=mysql_query("select * from general_community where community_id=$res[community_id]");
		$get_community_image=mysql_fetch_assoc($sel_community_image);
		
		$sel_artist_image=mysql_query("select * from general_artist where artist_id=$res[artist_id]");
		$get_artist_image=mysql_fetch_assoc($sel_artist_image);
		
	if($get_artist_image['image_name']=="" && $get_community_image['image_name']=="")
	{
		?>
			<a><img id="artist_image" src="uploads/profile_pic/Noimage.png" height="100" onclick="clickartistimage()" width="114" /></a>
		<?php
	}
}

if($a=='removeaccount')
{
?>
<div>
	<div class="fancy_header">
		Remove My Account!
	</div>
	<div class="fancy_box_remove_content">
		If you close your account you will no longer be able to login and all profiles you are the creator of will no longer display. To reactivate your account you will need to email Bryan@PurifyArt.net or reregister and build a new account from scratch.
	<br/>
	<a href="javascript:void(0)" onclick="delete_account();" style="text-decoration:none;">
			<input type="button" value="Remove my Account" style="background: none repeat scroll 0 0 #000000;border: 0 none;margin:10px 10px 0 0;padding:5px;color: #FFFFFF;font-size: 0.825em;font-weight: bold;text-decoration: none;border-radius:5px;"/></a>
		<a href="javascript:void(0)" onclick="close_fancy_box();" style="text-decoration:none;"><input type="button" value = "Cancel" style="background: none repeat scroll 0 0 #000000;border: 0 none;color: #FFFFFF;font-size: 0.825em;font-weight: bold;text-decoration: none;margin:10px 10px 0 0;padding:5px;border-radius:5px;"/></a>
	</div>
</div>
<?php
}
?>
<style>
body{margin:0}
</style>