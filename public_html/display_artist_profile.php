<?php

function getJsonAsObject($in){
    $asObjects = json_decode($in);

    switch (json_last_error()) {
        case JSON_ERROR_NONE:
            //echo ' - No errors';
        break;
        case JSON_ERROR_DEPTH:
            echo ' - Maximum stack depth exceeded';
        break;
        case JSON_ERROR_STATE_MISMATCH:
            echo ' - Underflow or the modes mismatch';
        break;
        case JSON_ERROR_CTRL_CHAR:
            echo ' - Unexpected control character found';
        break;
        case JSON_ERROR_SYNTAX:
            echo ' - Syntax error, malformed JSON';
        break;
        case JSON_ERROR_UTF8:
            echo ' - Malformed UTF-8 characters, possibly incorrectly encoded';
        break;
        default:
            echo ' - Unknown error';
        break;
    }
    return $asObjects;
}

function getJsonAsAssociativeArray($in){
// echo 'Decoding: ' . $in;

    $asAssociativeArray = json_decode($in,true);

    switch (json_last_error()) {
        case JSON_ERROR_NONE:
            //echo ' - No errors';
        break;
        case JSON_ERROR_DEPTH:
            echo ' - Maximum stack depth exceeded';
        break;
        case JSON_ERROR_STATE_MISMATCH:
            echo ' - Underflow or the modes mismatch';
        break;
        case JSON_ERROR_CTRL_CHAR:
            echo ' - Unexpected control character found';
        break;
        case JSON_ERROR_SYNTAX:
            echo ' - Syntax error, malformed JSON';
        break;
        case JSON_ERROR_UTF8:
            echo ' - Malformed UTF-8 characters, possibly incorrectly encoded';
        break;
        default:
            echo ' - Unknown error';
        break;
    }
    return $asAssociativeArray;
}

function printr ( $object , $name = '' ) {

    print ( '\'' . $name . '\' : ' ) ;

    if ( is_array ( $object ) ) {
        print ( '<pre>' )  ;
        print_r ( $object ) ; 
        print ( '</pre>' ) ;
    } else {
        var_dump ( $object ) ;
    }

}

function print_r_tree($data)
{
    // capture the output of print_r
    $out = print_r($data, true);

    // replace something like '[element] => <newline> (' with <a href="javascript:toggleDisplay('...');">...</a><div id="..." style="display: none;">
    $out = preg_replace('/([ \t]*)(\[[^\]]+\][ \t]*\=\>[ \t]*[a-z0-9 \t_]+)\n[ \t]*\(/iUe',"'\\1<a href=\"javascript:toggleDisplay(\''.(\$id = substr(md5(rand().'\\0'), 0, 7)).'\');\">\\2</a><div id=\"'.\$id.'\" style=\"display: none;\">'", $out);

    // replace ')' on its own on a new line (surrounded by whitespace is ok) with '</div>
    $out = preg_replace('/^\s*\)\s*$/m', '</div>', $out);

    // print the javascript function toggleDisplay() and then the transformed output
    echo '<script language="Javascript">function toggleDisplay(id) { document.getElementById(id).style.display = (document.getElementById(id).style.display == "block") ? "none" : "block"; }</script>'."\n$out";
}

function array_to_json( $array ){

    if( !is_array( $array ) ){
        return false;
    }

    $associative = count( array_diff( array_keys($array), array_keys( array_keys( $array )) ));
    if( $associative ){

        $construct = array();
        foreach( $array as $key => $value ){

            // We first copy each key/value pair into a staging array,
            // formatting each key and value properly as we go.

            // Format the key:
            if( is_numeric($key) ){
                $key = "key_$key";
            }
            $key = "\"".addslashes($key)."\"";

            // Format the value:
            if( is_array( $value )){
                $value = array_to_json( $value );
            } else if( !is_numeric( $value ) || is_string( $value ) ){
                $value = "\"".addslashes($value)."\"";
            }

            // Add to staging array:
            $construct[] = "$key: $value";
        }

        // Then we collapse the staging array into the JSON form:
        $result = "{ " . implode( ", ", $construct ) . " }";

    } else { // If the array is a vector (not associative):

        $construct = array();
        foreach( $array as $value ){

            // Format the value:
            if( is_array( $value )){
                $value = array_to_json( $value );
            } else if( !is_numeric( $value ) || is_string( $value ) ){
                $value = "'".addslashes($value)."'";
            }

            // Add to staging array:
            $construct[] = $value;
        }

        // Then we collapse the staging array into the JSON form:
        $result = "[ " . implode( ", ", $construct ) . " ]";
    }
	
    return $result;
}
?>
<?php
//$generaluser=array();
//$generalartist=array();
// set HTTP header
$headers = array(
    'Content-Type: application/json',
);

if(isset($_REQUEST['profile'])) {
    $profileurl=$_REQUEST['profile'];
    }else{
    $profileurl='kaz';
    }
        
// set POST params
$fields = array(
    'profile' => $profileurl,
    'format' => 'json'
);


$fieldJson=json_encode($fields);
$domainname=$_SERVER['SERVER_NAME'];
$url = "http://".$domainname."/get_artist_profile.php";
//$url = 'http://purifyart.com/testpost.php' ;

// Open connection
$ch = curl_init();

// Set the url, number of POST vars, POST data
curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_POST, true);
//curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', 'Content-Length: ' . strlen($fieldJson)));
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true );
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
curl_setopt($ch, CURLOPT_POSTFIELDS, $fieldJson);

// Execute request
$result = curl_exec($ch);

// Close connection
curl_close($ch);

// get the result and parse to array
$asAssociativeArray = getJsonAsAssociativeArray($result, true);
print ( '<pre>' )  ;
print_r_tree($asAssociativeArray);
print ( '</pre>' )  ;

foreach ($asAssociativeArray as $key => $val) {    
    if ($key==0){$generaluser = $val['generaluser'];}
    if ($key==1){$generalartist = $val['generalartist'];}
    if ($key==2){$country = $val['country'];}
    if ($key==3){$state = $val['state'];}
    if ($key==4){$type = $val['type'];}
    if ($key==5){$subtype = $val['subtype'];}
    if ($key==6){$metatype = $val['metatype'];}
    if ($key==7){$projects = $val['projects'];} 
    if ($key==8){$upcomingevents = $val['upcomingevents'];}
    if ($key==9){$recordedevents = $val['recordedevents'];}    
    if ($key==10){$mediasong = $val['mediasong'];}
    if ($key==11){$songimagefile = $val['songimagefile'];}
    if ($key==12){$alluserartistsforproject = $val['alluserartistsforproject'];}    
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "https://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="https://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<head>

</head>
<body>
 <div id="similar_as_below" >
  <div id="contentContainer" >
    <!-- PROFILE START -->
    <div id="profileHeader">
		<div id="profilePhoto" style="position:relative;">
<img src="http://artjcropprofile.s3.amazonaws.com/<?php echo $generalartist['image_name'] ?>" width="450" height="450" />
				
		</div>
      <div id="profileInfo">
        <h2><?php echo $type['name'];?>; <?php echo $subtype['name']; ?></h2>        
        <h1 style="margin: 0 0 3px;"><?php echo $generalartist['name']; ?></h1>
        <h3 style="margin: 0 0 6px;"><!-- Where: --> <?php echo $generalartist['city']; ?>, <?php echo $state['state_name']; ?>, <?php echo $country['country_name']; ?><br/></h3>
					<!-- Type: -->
	    <?php
			if(isset($metatype[0]['name']) && !empty($metatype[0]['name'])) { echo $metatype[0]['name']; }
			if(isset($metatype[1]['name']) && !empty($metatype[1]['name'])) { if(!empty($metatype[0]['name'])) { echo ', '.$metatype[1]['name']; } else { echo $metatype[1]['name']; } }
			if(isset($metatype[2]['name']) && !empty($metatype[2]['name'])) {  if(!empty($metatype[0]['name']) || !empty($metatype[1]['name'])) { echo ', '.$metatype[2]['name']; } else { echo $metatype[2]['name']; } }
		?> <br/>
		</h3>
          <script>
		  $("document").ready(function(){
			$("#contentContainer p").css({"width":"437px"});
		  });
		  </script>
        <p style="width:463px; font-size:14px;"><?php echo $generalartist['bio']; ?></p>
      </div>
                     <h1>Artists</h1>
         <?php
         for($i=0;$i<count($alluserartistsforproject);$i++){
            ?>
            <div id="artists">
            <a href=
            <?php
            echo $alluserartistsforproject[$i]['profile_url'];            
            ?>>
            <?php
            echo $alluserartistsforproject[$i]['fname']." ".$alluserartistsforproject[$i]['lname'];            
            ?>
            </a
            <br>
            </div>  
            <?php
         }     
         ?>
      
         <h1>Albums</h1>
         <?php
         for($i=0;$i<count($projects);$i++)
         {
            ?>
            <div id="albums">
            <a href=
            <?php
            echo $projects[$i]['profile_url'];            
            ?>>
            <?php
            echo $projects[$i]['title'];            
            ?>
            </a
            <br>
            </div>  
            <?php
         }     
         ?>

                    <h1>Upcoming Events</h1>
         <?php
         for($i=0;$i<count($upcomingevents);$i++)
         {
            ?>
            <div id="upcomingevents">
            <a href=
            <?php
            echo $upcomingevents[$i]['profile_url'];            
            ?>>
            <?php
            echo $upcomingevents[$i]['title'];            
            ?>
            </a
            <br>
            </div>  
            <?php
         }     
         ?>
         
                  <h1>Recorded Events</h1>
         <?php
         for($i=0;$i<count($recordedevents);$i++)
         {
            ?>
            <div id="recordedevents">
            <a href=
            <?php
            echo $recordedevents[$i]['profile_url'];            
            ?>>
            <?php
            echo $recordedevents[$i]['title'];            
            ?>
            </a
            <br>
            </div>  
            <?php
         }     
         ?>
         

         


        <h1>Songs</h1>
         <?php
         
         for($i=0;$i<count($mediasong);$i++)
         {
            if($mediasong[$i]['song_name']!="")
            {            
            ?>
                <div id="songs">
                <a href=
                <?php
                echo '"'."https://medaudio.s3.amazonaws.com/".$mediasong[$i]['song_name'].'">';            
                
                echo $mediasong[$i]['track'].' ' .$mediasong[$i]['songtitle'] . ' from ' .$mediasong[$i]['from'];            
                
                ?>
                </a
                <br>
                </div>  
                <?php
            }
         }     
         ?>
</body>
</html>
