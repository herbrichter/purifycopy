<?php
session_start();
include('loggedin_includes.php');
include_once('classes/Media.php');
include_once('classes/ProfileeditCommunity.php');
include_once('classes/Commontabs.php');
include_once('classes/Promotions.php');
include_once('classes/EditArtist.php');

$domainname=$_SERVER['SERVER_NAME'];
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "https://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="https://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<meta https-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Purify Art: Community Profile</title>
<!--<script type="text/javascript" src="includes/jquery.js"></script>-->

<script type="text/javascript">var switchTo5x=true;</script>
<script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script>
<script type="text/javascript">stLight.options({popup:'true', publisher: "65a11f7c-e04e-4326-b62a-efc51e5dd2b3"});</script>

<?php
$newtab=new Commontabs();
include("header.php");

$date=date("Y-m-d");
 
$newgeneral = new ProfileeditCommunity();
$editprofile_artist=new EditArtist();
$res = $newgeneral->selgeneral();

/********Code For Displaying Accepted Media*********/

$acc_media=explode(',',$res['accepted_media_id']);

$media_avail = 0;

for($k=0;$k<count($acc_media);$k++)
{
	if($acc_media[$k]!="")
	{
		$media_avail = $media_avail + 1;
	}
}

$get_artist_member=$newgeneral->Get_artist_member_Id();


$find_member = $newgeneral->Get_purify_member();

$update_event = $newgeneral->selectUpcomingEvents();
$update_ans = mysql_fetch_assoc($update_event);
$ans_exp = explode(',',$update_ans['taggedupcomingevents']);
$new_array = array();
for($i=0;$i<count($ans_exp);$i++)
{
	$check_artist = $newgeneral->selectCommunityEvents($ans_exp[$i]);
	$check_ans = mysql_fetch_assoc($check_artist);
	if($check_ans['date']>=$date)
	{
		$new_array[$i] = $ans_exp[$i];
	}
	else
	{
		$sql_update_up = $newgeneral->selectRecordedEvents();
		$sql_update_ans = mysql_fetch_assoc($sql_update_up);
		$all = $sql_update_ans['taggedrecordedevents'];
		$final_value = $all.','.$ans_exp[$i];
		if($ans_exp[$i]!="")
		{
			$update_upc = $newgeneral->updatedRecEvents($final_value);
		}
	}
//die;
}

if(isset($new_array) && count($new_array)>0 && $new_array!=NULL)
{
	$new_array1 = implode(',',$new_array);
	$update_rec = $newgeneral->updatedUpcEvents($new_array1);
}


$acc_event=explode(',',$res['taggedcommunityevents']);
$event_avail = 0;
for($k=0;$k<count($acc_event);$k++)
{
	if($acc_event[$k]!="")
	{
		$event_avail = $event_avail + 1;
	}
}

$acc_aevent=explode(',',$res['taggedartistevents']);
$event_aavail = 0;
for($k=0;$k<count($acc_aevent);$k++)
{
	if($acc_aevent[$k]!="")
	{
		$event_aavail = $event_aavail + 1;
	}
}

$acc_project = explode(',',$res['taggedcommunityprojects']);
$project_avail = 0;
for($k=0;$k<count($acc_project);$k++)
{
	if($acc_project[$k]!="")
	{
		$project_avail = $project_avail + 1;
	}
}

$acc_aproject = explode(',',$res['taggedartistprojects']);
$project_aavail = 0;
for($k=0;$k<count($acc_aproject);$k++)
{
	if($acc_aproject[$k]!="")
	{
		$project_aavail = $project_aavail + 1;
	}
}

//var_dump($acc_event);

/********Code For Displaying Accepted Media Ends Here*********/

$selcomunity = $newgeneral->selcommunity();
$res1=mysql_fetch_assoc($selcomunity);
//var_dump($res1);

/**********Code For Displaying selected options in list box for tagged************/

$get_select_songs = explode(',',$res1['taggedsongs']);
$get_select_songs_fan = explode(',',$res1['fan_song']);
//var_dump($get_select_songs);

$get_select_videos = explode(',',$res1['taggedvideos']);
$get_select_videos_fan = explode(',',$res1['fan_video']);
//var_dump($get_select_songs);

$get_select_channels = explode(',',$res1['taggedchannels']);
$get_select_channels_fan = explode(',',$res1['fan_channel']);
//var_dump($get_select_channels);

$get_select_galleries = explode(',',$res1['taggedgalleries']);
$get_select_galleries_fan = explode(',',$res1['fan_gallery']);

$get_select_events = explode(',',$res1['taggedupcomingevents']);

$get_select_events_rec = explode(',',$res1['taggedrecordedevents']);

$get_select_projects= explode(',',$res1['taggedprojects']);

//var_dump($get_select_songs);

/**********Code For Displaying selected options in list box for tagged ends here************/


/********Functions For Tagged channel video song *******/

$get_community_channel_id=$newgeneral->get_community_channel_id();

$get_community_song_register=$newgeneral->get_community_song_at_register();
$get_community_video_register=$newgeneral->get_community_video_at_register();
$get_community_gallery_register=$newgeneral->get_community_gallery_name_at_register();

$get_project = $newgeneral->getAllProjects();
$get_cproject = $newgeneral->getAllA_Projects();

$get_sh_project = $newgeneral->getAllProjects();
$get_sh_cproject = $newgeneral->getAllA_Projects();

$get_event = $newgeneral->getEventUpcoming();
$get_sh_event = $newgeneral->getEventUpcoming();

$get_eventrec = $newgeneral->getEventRecorded();
$get_sh_eventrec = $newgeneral->getEventRecorded();

$get_aeevent = $newgeneral->getArtEventUpcoming();
$get_sh_aeevent = $newgeneral->getArtEventUpcoming();

$get_aeeventrec = $newgeneral->getArtEventRecorded();
$get_sh_aeeventrec = $newgeneral->getArtEventRecorded();

$get_levent = $newgeneral->getEventUpcoming();
$get_leventrec = $newgeneral->getEventRecorded();

$get_laeevent = $newgeneral->getArtEventUpcoming();
$get_laeeventrec = $newgeneral->getArtEventRecorded();

$get_all_countries = $editprofile_artist->get_all_countries();
$get_all_states = $editprofile_artist->get_all_states($res1['country_id']);

if($get_community_song_register!=null)
{
	$res_song = mysql_fetch_assoc($get_community_song_register);
}
if($get_community_video_register!=null)
{
	$res_video = mysql_fetch_assoc($get_community_video_register);
}
if($get_community_gallery_register!=null)
{
	$res_gal = mysql_fetch_assoc($get_community_gallery_register);
}

/********Ends here ******/	

$selcomunityprofile = $newgeneral->selcommunityprofile();
//var_dump($selcomunityprofile);
$seltypeid = $newgeneral->selTypeid();

$seltype = $newgeneral->selType();
$meta = $newgeneral->selmetaTypeid();
/*while($res=mysql_fetch_assoc($meta))
{
	var_dump($res);
}*/
$res4 = $newgeneral->selsubTypeid();
//var_dump($res4);
$selsubtype = $newgeneral->selsubType();

$getproject=$newgeneral->selcommunity_project();
$getproject_artist=$editprofile_artist->GetProject();

$getevent=$newgeneral->selcommunity_event();

$getname=$newgeneral->selgeneral_community();

$get_primary_type=$newgeneral->Get_community_primarytype();

$nameres=mysql_fetch_assoc($getname);

$getmeta=$newgeneral->selgeneral_community_profile();

$arr=Array();
while($gmeta=mysql_fetch_assoc($getmeta))	
{
	//var_dump($gmeta);
	$arr[]=$gmeta;
}
?>
<div id="outerContainer">

<!--<p><a href="community_profile.php">View <?php //echo $res['fname']; ?> Profile</a><br />-->
<!--<a href="../logout.php">Logout</a></p>
</div>
</div>
</div>
</div>-->

<!--<link rel="stylesheet" type="text/css" media="screen" href="includes/purify.css" />
<script src='https://ajax.googleapis.com/ajax/libs/jquery/1.6.1/jquery.min.js'></script>-->
<!--css file for displaying types
<link rel="stylesheet" type="text/css" media="screen" href="includes/multiple_select.css" />
<!--css file for displaying types Ends here-->
<!--<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1/jquery.js"></script>
<script src="ui/jquery-1.7.2.js"></script>
	<script src="ui/jquery.ui.core.js"></script>
	<script src="ui/jquery.ui.widget.js"></script>
	<script src="ui/jquery.ui.mouse.js"></script>
	<script src="ui/jquery.ui.resizable.js"></script>
<script type="text/javascript" src="ckeditor.js"></script>
<script type="text/javascript" src="jquery.js"></script>
<script src="sample.js" type="text/javascript"></script>
<link href="sample.css" rel="stylesheet" type="text/css" />-->
<!--date picker includes
	<link rel="stylesheet" href="includes/jquery.ui.all.css"/>
	<!--<script src="includes/jquery.ui.core.js"></script>
	<script src="includes/jquery.ui.widget.js"></script>
	<script src="includes/jquery.ui.datepicker.js"></script>-->
<!--date picker includes Ends here-->

<script type="text/javascript">
	//<![CDATA[

$(function()
{
	var config = {
		toolbar:
		[
			['Bold', 'Italic', '-', 'NumberedList', 'BulletedList', '-', 'Link', 'Unlink'],
			['UIColor']
		]
	};

	// Initialize the editor.
	// Callback function can be passed and executed after full instance creation.
	$('.jquery_ckeditor').ckeditor(config);
	config.forcePasteAsPlainText = true;
});

	//]]>
	</script>	
<!--Ends here
<script src="includes/organictabs-jquery.js"></script>-->
<script>
	$(function() {

		$("#communityTab").organicTabs();
	});
	$(document).ready(function(){
		$(".fieldText").css("height" , "27px");
	});
</script>

    <script>
	$(function() {
		//$( "#datepicker, #datepickerB" ).datepicker();
	});
	</script>
	
	<script>
$(function() {
		$( "#popupContact" ).resizable();
	});
</script>

<script>
var myUploader = null;
var myAudioUploader = null;
var iMaxUploadSize = 10485760; //10MB
	window.onload = function(){ 
		$.post("State.php", { country_id:$("#countrySelect").val() },
			function(data)
			{
					//alert("Data Loaded: " + data);											
					$("#stateSelect").html(data);
			}
		);
	}
$("document").ready(function(){
$("#selectCountry").change(function ()
{
	$.post("State.php", { country_id:$("#selectCountry").val() },
		function(data)
		{
			$("#selectState").html(data);
		}
	);
});
});
	$(document).ready(
					function()
					{
						/*$("#addType").click(
							function () 
							{	
								if ($("#type-select").val()!=0 )
								{
									//alert("type selected");
									if(!$("div").hasClass($("#type-select").val()))
									{
									//alert("no div for type");
										$("#selected-types").append('<div class="'+$("#type-select").val()+' box1" id="'+$("#type-select").val()+'">'+'<input type="checkbox" name="type-checked" value="'+$("#type-select").val()+'" checked onclick="$(this).parent().remove()" />'+$("#type-select option:selected").attr("chkval")+'</div>');
									}
								
								}
								else
								{
										alert("Select PageType");
								}	
																
								//alert("hi");
								if($("#subtype-select").val()!=0)
								{
									//alert("subtype selected");
									if(!$("div").hasClass($("#type-select").val()+'_'+$("#subtype-select").val()))
									{
										//alert("no div for type subtype");
										//if($("div").hasClass($("#type-select").val())
										$("."+$("#type-select").val()).append('<div id="'+$("#subtype-select").val()+'" class="'+$("#type-select").val()+'_'+$("#subtype-select").val() + ' box1A"><input type="checkbox" name="subtype-checked" value="'+$("#type-select").val()+'_'+$("#subtype-select").val() + '" checked  onclick="$(this).parent().remove()"/>'+$("#subtype-select option:selected").attr("chkval")+'<br>'+'</div>');
									}
								}
								
								if($("#metatype-select").val()!=0)
								{
									//alert("metatype selected");
									if(!$("div").hasClass($("#type-select").val()+'_'+$("#subtype-select").val()+'_'+$("#metatype-select").val()))
									{
										//alert("no div for type subtype metatype");
										//if($("div").hasClass($("#type-select").val())
										$("."+$("#type-select").val()+'_'+$("#subtype-select").val()).append('<div class="'+$("#type-select").val()+'_'+$("#subtype-select").val()+'_'+$("#metatype-select").val() + ' box1B"><input type="checkbox" name="metatype_checked[]" value="'+$("#type-select").val()+'_'+$("#subtype-select").val()+'_'+$("#metatype-select").val() +'" checked  onclick="$(this).parent().remove()"/>'+$("#metatype-select option:selected").attr("chkval")+'</div>');
									}
								}
							}
						);*/
						
	//Meta type displaying starts here
	
       //meta type displaying ends here//
       	   
	   
	   $("#addType").click(function () 
       {//alert("hiType");
         //alert($("#type-select option:selected").length);
         //alert($("#subtype-select option:selected").length);
         //alert($("#metatype-select option:selected").length);
         //alert($("#type-select option:selected").attr("key"));
         if ($("#type-select").val()!=0 )
         {
          //alert("type selected");
			  if(!$("div").hasClass($("#type-select").val()))
			  {
				  //alert("no div for type");
				   $("#selected-types").append('<div class="'+$("#type-select").val()+' box1" id="'+$("#type-select").val()+'">'+'<input type="checkbox" name="type_array['+$("#type-select").val() +']" value="'+$("#type-select").val()+'" checked onclick="$(this).parent().remove()" />'+$("#type-select option:selected").attr("chkval")+'<input type="hidden" name="typeVals[]" id="typeVals[]" value="'+$("#type-select").val()+'_0_0"/></div>');
				  /* $("#selected-types-hide").append('<div style="display:none;" class="'+$("#type-select").val()+' box1 type-select-hide" id="'+$("#type-select").val()+'">'+'<input type="checkbox" name="type_array['+$("#type-select").val() +']" value="'+$("#type-select").val()+'" checked onclick="$(this).parent().remove()" />'+$("#type-select option:selected").attr("chkval")+'<input type="hidden" name="typeVals[]" value="'+$("#type-select").val()+'_0_0"/></div>');*/
				   
			  }
         
         }
         else
         {
			   alert("Select PageType");
         } 
                 
         //alert("hi");
         if($("#subtype-select").val()!=0)
         {
         // alert("subtype selected");
			  if(!$("div").hasClass($("#type-select").val()+'_'+$("#subtype-select").val()))
			  {
			   
			   $("."+$("#type-select").val()).append('<div id="'+$("#subtype-select").val()+'" class="'+$("#type-select").val()+'_'+$("#subtype-select").val() + ' box1A"><input type="checkbox" name="type_array['+$("#type-select").val() +']['+$("#subtype-select").val()+']" value="'+$("#type-select").val()+'_'+$("#subtype-select").val() + '" checked  onclick="$(this).parent().remove()"/>'+$("#subtype-select option:selected").attr("chkval")+'<br>'+'<input type="hidden" name="subTypeVals[]" value="'+$("#type-select").val()+'_'+$("#subtype-select").val()+'_0"/></div>');
			  /* $("#type-select-hide").append('<div style="display:none;" id="'+$("#subtype-select").val()+'" class="'+$("#type-select").val()+'_'+$("#subtype-select").val() + ' box1A subtype-select-hide"><input type="checkbox" name="type_array['+$("#type-select").val() +']['+$("#subtype-select").val()+']" value="'+$("#type-select").val()+'_'+$("#subtype-select").val() + '" checked  onclick="$(this).parent().remove()"/>'+$("#subtype-select option:selected").attr("chkval")+'<br>'+'<input type="hidden" name="subTypeVals[]" value="'+$("#type-select").val()+'_'+$("#subtype-select").val()+'_0"/></div>');*/
			  
			  }
         
         }
         
         if($("#metatype-select").val()!=0)
         {
         //alert("metatype selected");
			  if(!$("div").hasClass($("#type-select").val()+'_'+$("#subtype-select").val()+'_'+$("#metatype-select").val()))
			  {
				if($("#metatype-select").val()== null)
			   {
				return false;
			   }
			   $("."+$("#type-select").val()+'_'+$("#subtype-select").val()).append('<div class="'+$("#type-select").val()+'_'+$("#subtype-select").val()+'_'+$("#metatype-select").val() + ' box1B"><input type="checkbox" name="metatype_checked[]" value="'+$("#type-select").val()+'_'+$("#subtype-select").val()+'_'+$("#metatype-select").val() +'" checked  onclick="$(this).parent().remove()"/>'+$("#metatype-select option:selected").attr("chkval")+'<input type="hidden" name="metaTypeVals[]" id="metaTypeVals[]" value="'+$("#type-select").val()+'_'+$("#subtype-select").val()+'_'+$("#metatype-select").val() +'"/></div>');
			   //alert($("#type-select").val()+'_'+$("#subtype-select").val()+'_'+$("#metatype-select").val());
			  /* $("#subtype-select-hide").append('<div style="display:none;" class="'+$("#type-select").val()+'_'+$("#subtype-select").val()+'_'+$("#metatype-select").val() + ' box1B"><input type="checkbox" name="metatype_checked[]" value="'+$("#type-select").val()+'_'+$("#subtype-select").val()+'_'+$("#metatype-select").val() +'" checked  onclick="$(this).parent().remove()"/>'+$("#metatype-select option:selected").attr("chkval")+'<input type="hidden" name="metaTypeVals[]" value="'+$("#type-select").val()+'_'+$("#subtype-select").val()+'_'+$("#metatype-select").val() +'"/></div>');*/
			  
			  }
         
         }
                 
        });
		  
	   					
						$("#type-select").change(
							function ()
							{
								//alert($("#type-select").val());
								$.post("registration_block_subtype.php", { type_id:$("#type-select").val() },
									function(data)
									{
										if(data == " " || data == null || data == "")
										{
											document.getElementById("subtype-select").disabled=true;
										}else{
											document.getElementById("subtype-select").disabled=false;
											//alert("Data Loaded: " + data);
											$("#metatype-select").html("<option value='0' selected='selected'>Select Metatype</option>");
											$("#subtype-select").html(data);
										}
									}
								);
							}
						);
										
						$("#subtype-select").change(
							function ()
							{
								//alert($("#subtype-select").val());
								$.post("registration_block_metatype.php", { subtype_id:$("#subtype-select").val() },
									function(data)
									{
										if(data == " " || data == null || data == "")
										{
											document.getElementById("metatype-select").disabled=true;
										}else{
											document.getElementById("metatype-select").disabled=false;
											//alert("Data Loaded: " + data);
											$("#metatype-select").html(data);
										}
									}
								);
							}	
						);
							//Select State o change in values of Country
							
						$("#countrySelect").change(
							function ()
							{
								//alert($("#type-select").val());
								$.post("State.php", { country_id:$("#countrySelect").val() },
									function(data)
									{
									//alert("Data Loaded: " + data);											
										$("#stateSelect").html(data);
									}
								);
							}
						);
							
						$('#selectuploadtype').change(
							function() 
							{
								$("#imgUploadDiv").fadeOut();
								$("#audioUploadDiv").fadeOut();
								$("#videoUploadDiv").fadeOut();
								$("#audiofileform").fadeOut();
								$("#audiolinkform").fadeOut();
								  var val = $('#selectuploadtype').val();
								switch(val)
								{
									
									case 'Image':
											$("#imgUploadDiv").fadeIn();
											$("#uploadedImages").val('');
											clearFields();
										if(!myUploader){
											myUploader = {
													uploadify : function(){
														$('#file_upload').uploadify({
															'uploader'  : './uploadify/uploadify.swf',
															'script'    : './uploadify/uploadFiles.php',
															'cancelImg' : './uploadify/cancel.png',
															'folder'    : './uploads/',
															'auto'      : true,
															'multi'		: true,
															'removeCompleted' : true,
															'wmode'		: 'transparent',
															'buttonText': 'Upload Gallery',
															'fileExt'     : '*.jpg;*.gif;*.png',
															'fileDesc'    : 'Image Files',
															'simUploadLimit' : 4,
															'sizeLimit'	: iMaxUploadSize, //10 MB size
															'onComplete': function(event, ID, fileObj, response, data) {
																// On File Upload Completion			
																//location = 'uploadtos3.php?uploads=complete';
																$("#uploadedImages").val($("#uploadedImages").val() + response + "|");
															},
															'onSelect' : function (event, ID, fileObj){
															},
															'onSelectOnce' : function(event, data)
															{
																// This function fires after onSelect
																// Checks total size of queue and warns user if too big
																iTotFileSize = data.allBytesTotal;
																//alert("iTotFileSize = " + iTotFileSize);
																if(iTotFileSize >= iMaxUploadSize){
																	var iOverSize = (iTotFileSize - iMaxUploadSize) / 1024 / 1024; // Size, in MB
																	//alert("Total file size in queue exceeds maximum 10MB allowed.  Please remove " + Math.ceil(iOverSize) + " MB worth.");
																	$("#divGalleryFileSize").show().html("Maximum 10MB allowed. Total queue size approximately: <font color='red'>" + Math.ceil(iTotFileSize/1024/1024) + " (MB)</font>");
																	$('#file_upload').uploadifyClearQueue();
																}
																else
																{
																   $("#divGalleryFileSize").hide() 
																}
															},
															'onOpen'	: function() {
																//hide overly
															}
															/*,
															'onError'     : function (event,ID,fileObj,errorObj) {
															  alert(errorObj.type + ' Error: ' + errorObj.info);
															},
															'onProgress'  : function(event,ID,fileObj,data) {
															  var bytes = Math.round(data.bytesLoaded / 1024);
															  $('#containerResult').html(' - ' + bytes + 'KB Uploaded');
															  return false;
															}*/

															});
														}
													};
													myUploader.uploadify();
												}
											break;
									case 'Audio':
											clearFields();
											$("#audioUploadDiv").fadeIn();
											break;
									case 'Video':
											clearFields();
											$("#videoUploadDiv").fadeIn();
											break;
									default :
											alert("Default");
											break;
											
								}
							}
						);

						
						$("input[name^=audiouploadoption]").click(
							function()
							{
								$("#audiofileform").fadeOut();
								$("#audiolinkform").fadeOut();
								$("#uploadedAudio").val('');
								if($(this).val()=="file"){
									$("#audiofileform").fadeIn();
									if(!myAudioUploader){
									myAudioUploader = {
										uploadify : function(){
											$('#file_upload_audio').uploadify({
												'uploader'  : './uploadify/uploadify.swf',
												'script'    : './uploadify/uploadFilesAudio.php',
												'cancelImg' : './uploadify/cancel.png',
												'folder'    : './uploads/',
												'auto'      : true,
												'multi'		: false,
												'removeCompleted' : true,
												'wmode'		: 'transparent',
												'buttonText': 'Upload Media',
												'fileExt'     : '*.mp3',
												'fileDesc'    : 'Audio Files',
												'simUploadLimit' : 4,
												'sizeLimit'	: iMaxUploadSize, //10 MB size
												'onComplete': function(event, ID, fileObj, response, data) {
													// On File Upload Completion			
													//location = 'uploadtos3.php?uploads=complete';
													alert(response);
													$("#uploadedAudio").val($("#uploadedAudio").val() + response + "|");
												},
												'onSelectOnce' : function(event, data)
												{
													// This function fires after onSelect
													// Checks total size of queue and warns user if too big
													iTotFileSize = data.allBytesTotal;
													//alert("iTotFileSize = " + iTotFileSize);
													if(iTotFileSize >= iMaxUploadSize){
														var iOverSize = (iTotFileSize - iMaxUploadSize) / 1024 / 1024; // Size, in MB
														//alert("Total file size in queue exceeds maximum 10MB allowed.  Please remove " + Math.ceil(iOverSize) + " MB worth.");
														$("#divAudioFileSize").show().html("Maximum 10MB allowed. Total queue size approximately: <font color='red'>" + Math.ceil(iTotFileSize/1024/1024) + " (MB)</font>");
														$('#file_upload_audio').uploadifyClearQueue();
													}
													else
													{
													   $("#divGalleryFileSize").hide() 
													}
												},
												'onOpen'	: function() {
													//hide overly
												}
												/*,
												'onError'     : function (event,ID,fileObj,errorObj) {
												  alert(errorObj.type + ' Error: ' + errorObj.info);
												},
												'onProgress'  : function(event,ID,fileObj,data) {
												  var bytes = Math.round(data.bytesLoaded / 1024);
												  $('#containerResult').html(' - ' + bytes + 'KB Uploaded');
												  return false;
												}*/

												});
											}
										};
										myAudioUploader.uploadify();
									}
								}else if($(this).val()=="link"){
									$("#audiolinkform").fadeIn();							
								}
							}
						);

						$("#chk_avail_link").click(
							function()
							{
								$.post("registration_user_availablity.php", { name:$("#username").val() },
								function(data)
								{
									$("#user_name_msg").html(data);
									$("#register").focus();	
									$("#username").focus();																	
								}
								);
							}
						);	
						$("#chk_avail_link1").click(
							function()
							{
								$.post("registration_artist_email.php", { email:$("#email").val() },
								function(data)
								{
									$("#user_email_msg").html(data);
									$("#register").focus();	
									$("#email").focus();																	
								}
								);
							}
						);			
		
						$("#register_btn").click(
							function()
							{
								var v = validate();
								if(v){
									$('#selected-types').clone().appendTo('#selected-types-hide');
									document.registration_form.register.value = "1";
									document.registration_form.submit();
								}
							}
						);

						
						$("#audioLinkUrl").blur(
							function()
							{
								clearFields();
								$("#uploadedAudioLink").val($("#audioLinkUrl").val());
								$("#uploadedAudioLinkTitle").val($("#audioLinkName").val());
							}
						);

						$("#videoLinkUrl").blur(
							function()
							{
								clearFields();
								$("#uploadedVideoLink").val($("#videoLinkUrl").val());
								$("#uploadedVideoLinkTitle").val($("#videoLinkName").val());
							}
						);

						$("#audioLinkName").blur(
							function()
							{
								clearFields();
								$("#uploadedAudioLink").val($("#audioLinkUrl").val());
								$("#uploadedAudioLinkTitle").val($("#audioLinkName").val());
							}
						);

						$("#videoLinkName").blur(
							function()
							{
								clearFields();
								$("#uploadedVideoLink").val($("#videoLinkUrl").val());
								$("#uploadedVideoLinkTitle").val($("#videoLinkName").val());
							}
						);

						$("#audioFileName").blur(
							function()
							{
								var v = $("#uploadedAudio").val();
								clearFields();
								$("#uploadedAudio").val(v);
								$("#audioFileNameTitle").val($("#audioFileName").val());
							}
						);
						$("#galleryFileName").blur(
							function()
							{
								var v = $("#uploadedImages").val();
								clearFields();
								$("#uploadedImages").val(v);
								$("#galleryFileNameTitle").val($("#galleryFileName").val());
							}
						);

						/*$("#mediatype").change(
							function ()
							{
								alert($("#mediatype option:checked").val);
								switch($("#mediatype option:checked").val)
								{
									
									case 'Image':
											alert("Image");
											break;
									case 'Audio':
											alert("Audio");
											break;
									case 'Video':
											alert("Audio");
											break;
									default :
											alert("Default");
											break;
											
								}
								
							}	
						);*/	

					}
				);

function clearFields(){
	$("#uploadedImages").val('');
	$("#uploadedAudio").val('');
	$("#uploadedAudioLink").val('');
	$("#uploadedVideoLink").val('');
	$("#uploadedAudioLinkTitle").val('');
	$("#uploadedVideoLinkTitle").val('');
	$("#audioFileNameTitle").val('');
	$("#galleryFileNameTitle").val('');
}
function chk_profile_url(s)
{
	if(s=="")
	{
		aletr("To View The Profile You Have To Enter Profile URL.");
		return false;
	}
}
</script>

<script type="text/javascript">
 /*function selectmedia()
{
	var select2=document.getElementById("featured_media").value;
	window.location = "profileedit_artist.php?sel_val="+document.getElementById("featured_media").value;
} */
/*$("document").ready(function(){

	 $("#featured_media").change(function() 
    {
	//alert("sss");
	var selected = $("#featured_media").val();
	var dataString = 'reg='+ selected;
		
	 $.ajax({	 
		
			type: 'POST',
            url : 'CommunityAjaxActions.php',
            data: dataString,
            success: function(data) 
			{
                $("#fieldCont_media").html(data);
			}
		
		
	});
}); 

*/

function forsale_check(x)
{
	if(x==1){
		$("#fan_club").css({"display":"block"});
	}
	if(x==0){
		$("#fan_club").css({"display":"none"});
	}
	/*if(x=="no"){
		$("#member_error").click();
		document.getElementById("type2").checked = false;
		document.getElementById("type1").checked = false;
	}*/
	
}
/*
$(document).ready(function() {
		$("#member_error").fancybox({
		'width'				: '35%',
		'height'			: '15%',
		'autoScale'			: true,
		'transitionIn'		: 'none',
		'transitionOut'		: 'none',
		'type'				: 'iframe'
	});
});*/

$("document").ready(function(){

	 $("#featured_media").change(function() 
    {
	//alert("sss");
	var selected = $("#featured_media").val();
	var dataString = 'reg='+ selected;
	$("#loader").show();
	$("#fieldCont_media").hide();
	 $.ajax({	 
		
			type: 'POST',
            url : 'CommunityAjaxActions.php',
            data: dataString,
            success: function(data) 
			{
                $("#fieldCont_media").html(data);
				$("#loader").hide();
				$("#fieldCont_media").show();
			}
		
		
	});
}); 


$("#subdomain").blur(function(){
		var text_value = document.getElementById("subdomain").value;
		//alert(text_value);
		
		var text_data = 'reg='+ text_value;
		
	 $.ajax({	 
		
			type: 'POST',
            url : 'CommunityCheckDomain.php',
            data: text_data,
            success: function(data) 
			{
                $("#fieldCont_domain").html(data);
			}
		
		
	});

	});

});

function selected_feature(sel)
{
	var selected = sel;
	var dataString = 'reg='+ selected;
	$("#loader").show();
	$("#fieldCont_media").hide();
	 $.ajax({	 
		
			type: 'POST',
            url : 'CommunityAjaxActions.php',
            data: dataString,
            success: function(data) 
			{
                $("#fieldCont_media").html(data);
				$("#loader").hide();
				$("#fieldCont_media").show();
			}
		
		
	});

}
</script>
<script type="text/javascript">
$("document").ready(function(){
<?php 
for($i=0;$i<count($arr);$i++)
{
?>
if(!$("div").hasClass(<?php echo $arr[$i]['type_id'];?>))
{
	<?php $qu = mysql_query("select name from type where type_id='".$arr[$i]['type_id']."'");
	$name=mysql_fetch_array($qu);
	//$name=$newgeneral->seltype_name($arr);
	if($name[0]!="")
	{
	?>
		$("#selected-types").append('<div class="'
		+<?php echo $arr[$i]['type_id'];?>+' box1" id="'
		+<?php echo $arr[$i]['type_id'];?>+'">'
		+'<input type="checkbox" name="type_array['
		+<?php echo $arr[$i]['type_id'];?>+']" value="'
		+<?php echo $arr[$i]['type_id'];?>
		+'" checked onclick="$(this).parent().remove()" />'
		+"<?php echo $name[0];
		//echo $arr[$i]['type_id'];
		?>"
		+'<input type="hidden" name="typeVals[]" value="'
		+<?php echo $arr[$i]['type_id'];?>+'_0_0"/></div>');
	<?php
	}
	?>
}

if(!$("div").hasClass(<?php echo $arr[$i]['type_id'];?>+'_'+<?php echo $arr[$i]['subtype_id'];?>))
{
	<?php $qu = mysql_query("select name from subtype where subtype_id='".$arr[$i]['subtype_id']."'");
	$name=mysql_fetch_array($qu);
	if($name[0]!="")
	{
	?>
		 $("."+<?php echo $arr[$i]['type_id'];?>).append('<div id="'
		 +<?php echo $arr[$i]['subtype_id'];?>+'" class="'
		 +<?php echo $arr[$i]['type_id'];?>+'_'+<?php echo $arr[$i]['subtype_id'];?>
		 + ' box1A"><input type="checkbox" name="type_array['+<?php echo $arr[$i]['type_id'];?>
		 +']['+<?php echo $arr[$i]['subtype_id'];?>
		 +']" value="'+$("#showtype-select").val()
		 +'_'+<?php echo $arr[$i]['subtype_id'];?>
		 + '" checked  onclick="$(this).parent().remove()"/>'
		 +"<?php echo $name[0];
			//echo $arr[$i]['subtype_id'];
			?>"
		 +'<br>'+'<input type="hidden" name="subTypeVals[]" value="'
		 +<?php echo $arr[$i]['type_id'];?>+'_'+<?php echo $arr[$i]['subtype_id'];?>+'_0"/></div>');
	<?php
	}
	?>
}

if(!$("div").hasClass(<?php echo $arr[$i]['type_id'];?>+'_'+<?php echo $arr[$i]['subtype_id'];?>+'_'+<?php echo $arr[$i]['metatype_id'];?>))
{
<?php $qu = mysql_query("select name from meta_type where meta_id='".$arr[$i]['metatype_id']."'");
	$name=mysql_fetch_array($qu);
	if($name[0]!=null)
	{
	?>
		$("."+<?php echo $arr[$i]['type_id'];?>+'_'
		+<?php echo $arr[$i]['subtype_id'];?>).append('<div class="'
		+<?php echo $arr[$i]['type_id'];?>+'_'+<?php echo $arr[$i]['subtype_id'];?>
		+'_'+<?php echo $arr[$i]['metatype_id'];?>
		+ ' box1B"><input type="checkbox" name="metatype_checked[]" value="'
		+<?php echo $arr[$i]['type_id'];?>+'_'+<?php echo $arr[$i]['subtype_id'];?>
		+'_'+<?php echo $arr[$i]['metatype_id'];?>
		+'" checked  onclick="$(this).parent().remove()"/>'
		+"<?php echo $name[0];
			//echo $arr[$i]['metatype_id'];
			?>"
		+'<input type="hidden" name="metaTypeVals[]" value="'
		+<?php echo $arr[$i]['type_id'];?>+'_'+<?php echo $arr[$i]['subtype_id'];?>
		+'_'+<?php echo $arr[$i]['metatype_id'];?>+'"/></div>');
	<?php
	}
	?>
}
<?php
}
?>
});
</script>
<script>
function handleSelection(choice) {
//document.getElementById('select').disabled=true;
// if(choice=='venue')
	// {
	  // document.getElementById(choice).style.display="";
	  // document.getElementById('nodisplay').style.display="none";
	// }
	// else
	// {
	  // document.getElementById(choice).style.display="";
	  // document.getElementById('venue').style.display="none";
	// }
	
	if(choice=='all_profiles_share')
	{
		document.getElementById(choice).style.display="block";
		document.getElementById('projects_profiles_share').style.display="none";
		document.getElementById('events_profiles_share').style.display="none";
	}
	
	if(choice=='projects_profiles_share')
	{
		document.getElementById(choice).style.display="block";
		document.getElementById('all_profiles_share').style.display="none";
		document.getElementById('events_profiles_share').style.display="none";
	}
	
	if(choice=='events_profiles_share')
	{
		document.getElementById(choice).style.display="block";
		document.getElementById('all_profiles_share').style.display="none";
		document.getElementById('projects_profiles_share').style.display="none";
	}		
}

</script>

<script type="text/javascript">
$("document").ready(function(){
/*
bio1 = $('#bio').val();
if(bio1!="")
{
	 len = bio1.length - 9;
	 $("#count_for_bio").text(len + "");
}
var bio1,len,len1=0,len2=0,len3=0,len4=0,oldlen0=0,oldlen=0,oldlen1=0,oldlen2=0,oldlen3=0;
	var editor = $('#bio').ckeditorGet();
	editor.on( 'key', function(ev){
		 bio1 = $('#bio').val();

		len = bio1.length - 9;
		$("#count_for_bio").text(len  + "");
		
		var res_bio1=bio1.split("<p>");
		var editor1 = $('#bio').ckeditorGet();
		editor1.document.on( 'keydown', function (evt)
		{
			var key_code = evt.data.getKey();
			
			if(key_code == 13)
			{
				res_bio1=bio1.split("<p>");
				if(res_bio1[1] != null)
				{
					len1 = (res_bio1[1].length - 7);
					oldlen0 = (75 - len1);
				}
				if(res_bio1[2] != null)
				{
					len2 = (res_bio1[2].length - 7);
					oldlen = (75 - len2);
				}
				if(res_bio1[3] != null)
				{
					len3 = (res_bio1[3].length - 7);
					oldlen2 = (75 - len3);
				}
				if(res_bio1[4] != null)
				{
					len4 = (res_bio1[4].length - 6);
					oldlen3 = (75 - len4);
				}
				oldlen1 = (len2 + oldlen + oldlen0 + len1 + oldlen2 + len3 + oldlen3 + len4);
				$("#count_for_bio").text(oldlen1  + "");
			}
		});
		
		if(res_bio1.length > 2)
		{
			oldlen = (75 - (res_bio1[1].length - 7));
			oldlen1 = (res_bio1[1].length - 7) + oldlen;
			$("#count_for_bio").text((oldlen1 + (res_bio1[2].length - 6)) + "");
		}
		if(res_bio1.length > 3)
		{
			oldlen0 = (75 - (res_bio1[1].length - 7));
			oldlen = (75 - (res_bio1[2].length - 7));
			oldlen1 = (res_bio1[2].length - 7) + oldlen + oldlen0 + (res_bio1[1].length - 7);
			$("#count_for_bio").text((oldlen1 + (res_bio1[3].length - 6)) + "");
		}
		if(res_bio1.length > 4)
		{
			oldlen0 = (75 - (res_bio1[1].length - 7));
			oldlen = (75 - (res_bio1[2].length - 7));
			oldlen2 = (75 - (res_bio1[3].length - 7));
			oldlen1 = (res_bio1[2].length - 7) + oldlen + oldlen0 + (res_bio1[1].length - 7) + oldlen2 + (res_bio1[3].length - 7);
			$("#count_for_bio").text((oldlen1 + (res_bio1[4].length - 6)) + "");
		}
		if(res_bio1.length > 5)
		{
			$("#count_for_bio").text(300  + "");
		}
		
		
	});
	*/
});


function validate()
{
	var name = document.getElementById("name");
	if(name.value=="" || name.value==null)
	{
		alert("Please enter name.");
		return false;
	}
	var a = document.getElementById("selected-types");
	if(a.innerHTML =="" || a.innerHTML ==null)
	{
		alert("Please select at least one type and click the add button.");
		return false;
	}
	
	/*var url=document.getElementById("website").value;
	if(!(/^(www)((\.[A-Z0-9][A-Z0-9_-]*)+.(com|org|net|dk|at|us|tv|info|uk|co.uk|biz|se)$)(:(\d+))?\/?/i.test(url)))
	{
		alert("Please enter valid URL for website.");
		return false;
	}*/
	
/**********Ck Editor Validation **************/
/*
	var bio=$("#bio").val();
	var index;
	var res_bio=bio.split("<p>");

	/*if(bio=="")
	{
		alert("You Can Not Leave Description Blank");
		return false;
	}
	//alert(bio.length-10);
	if(bio.length-10 > 300)
	{
		alert("Please Enter Bio Less Than 300 Characters.");
		return false;
	}
	if(res_bio.length-1 > 4)
	{
		alert("Please Enter only Four lines.");
		return false;
	}*/
/*
	if(res_bio.length-1 == 3)
	{
		for(index=1;index <= res_bio.length-1 ; index++)
		{
			if(res_bio[index].length-7 > 75)
			{
				alert("Please Enter only 75 characters in one line");
				return false;
			}
		}
	}
	*/
/**********Ck Editor Validation Ends Here**************/
	
	var profile_url = $("#fieldCont_domain").html();
	var profile_field = document.getElementById("subdomain").value;
	if(profile_url == "Domain Is Not Available.")
	{
		alert("Please Choose Another Profile URL.");
		return false;
	}
	if(profile_field == "" || profile_field ==" ")
	{
		alert("Please Choose Any Profile URL.");
		return false;
	}
	var profile_field_new_comm = profile_field.trim();
	if (/\s/g.test(profile_field_new_comm))
	{
        alert("Please Check Your Profile URL For Spaces.");
        return false;
    }
	
	var profile_image_new = document.getElementById("profile_image_new").getAttribute('src');
	if(profile_image_new.indexOf('Noimage')>0)
	{
		alert("Please select your Profile Picture.");
		return false;
	}
	
	var listing_image_new = document.getElementById("listing_image_new").getAttribute('src');
	if(listing_image_new.indexOf('Noimage')>0)
	{
		alert("Please select your Listing Picture.");
		return false;
	}
	
	var type1_chk = document.getElementById("type1");
	if(type1_chk.checked==true)
	{
		var price_chk = document.getElementById("fan_price").value;
		if(price_chk =="" || price_chk ==null){
			alert("Please Enter Price.");
			return false;
		}
		else if(price_chk==0)
		{
			alert('You can not have a price of $0.');
			return false;
		}
	}
}
function confirmdelete(y)
{
	var r =confirm("Are You Sure You Want To Delete "+y+"?");
	if(r==false)
	{
		return false;
	}
}
</script>
<script src="chk_form_change.js"></script>

  
  
  <div id="toplevelNav"></div>
  <div>
      <div id="profileTabs">
          <ul>
            <?php
		   $new_artist=$newtab->artist_status();
		   $new_community=$newtab->community_status();
		   $new_team=$newtab->team_status();
		   
		   
		   if(($newres1['artist_id']==0 && $new_artist['status']==1) && $newres1['member_id']==0 && ($newres1['community_id']==0 && $new_community['status']==1) && ($newres1['team_id']==0 && $new_team['status']==1))
		  {		  
		  ?>
            <li><a href="profileedit.php">HOME</a></li>		
           <!--<li><a href="profileedit_support.php">SUPPORT</a></li>-->
		   <?php
		   }
		   elseif(($newres1['artist_id']!=0 && $new_artist['status']==0 && $new_artist['active']==0) && ($newres1['community_id']!=0 && $new_community['status']==0 && $new_community['active']==0) && ($newres1['team_id']!=0 && $new_team['status']==0))
		   {
		   ?>
		   <li><a href="profileedit.php">HOME</a></li>
		   <li><a href="profileedit_media.php">MEDIA</a></li>
		   <li><a href="profileedit_artist.php">ARTIST</a></li>
		   <!--<li><a href="profileedit_community.php">COMPANY</a></li>-->
		   <li class="active">COMPANY</li>
		   <li><a href="profileedit_team.php">TEAM</a></li>
          <!--<li><a href="profileedit_support.php">SUPPORT</a></li>-->
		   <?php
		   }
		   elseif(($newres1['artist_id']!=0 && $new_artist['status']==0 && $new_artist['active']==0) && ($newres1['community_id']!=0 && $new_community['status']==0 && $new_community['active']==0))
		   {
		   ?>
		   <li><a href="profileedit.php">HOME</a></li>
<li><a href="profileedit_media.php">MEDIA</a></li>		   
		   <li><a href="profileedit_artist.php">ARTIST</a></li>
		   <!--<li><a href="profileedit_community.php">COMPANY</a></li>-->
		   <li class="active">COMPANY</li>
           <!--<li><a href="profileedit_support.php">SUPPORT</a></li>-->
		   <?php
		   }
		   elseif(($newres1['artist_id']!=0 && $new_artist['status']==0 && $new_artist['active']==0) && ($newres1['team_id']!=0 && $new_team['status']==0))
		   {
		   ?>
			<li><a href="profileedit.php">HOME</a></li>	
<li><a href="profileedit_media.php">MEDIA</a></li>			
		    <li><a href="profileedit_artist.php">ARTIST</a></li>
			<li><a href="profileedit_team.php">TEAM</a></li>
            <!--<li><a href="profileedit_support.php">SUPPORT</a></li>-->
		   <?php
		   }
		   elseif(($newres1['community_id']!=0 && $new_community['status']==0 && $new_community['active']==0) && ($newres1['team_id']!=0 && $new_team['status']==0))
		   {
		   ?>
		   <li><a href="profileedit.php">HOME</a></li>
		   <li><a href="profileedit_media.php">MEDIA</a></li>
		   <!--<li><a href="profileedit_community.php">COMPANY</a></li>-->
		   <li class="active">COMPANY</li>
		   <li><a href="profileedit_team.php">TEAM</a></li>
           <!--<li><a href="profileedit_support.php">SUPPORT</a></li>-->
		   <?php
		   }
		   elseif($newres1['artist_id']!=0 && $new_artist['status']==0 && $new_artist['active']==0)
		   {?>
		    <li><a href="profileedit.php">HOME</a></li>	
<li><a href="profileedit_media.php">MEDIA</a></li>			
			<li><a href="profileedit_artist.php">ARTIST</a></li>
           <!--<li><a href="profileedit_support.php">SUPPORT</a></li>-->
		   <?php
		   }
		   elseif($newres1['member_id']!=0)
		   {
		   ?>
		   <?php
		   }
		   elseif($newres1['community_id']!=0 && $new_community['status']==0 && $new_community['active']==0)
		   {
		   ?>
		    <li><a href="profileedit.php">HOME</a></li>	
<li><a href="profileedit_media.php">MEDIA</a></li>			
			<!--<li><a href="profileedit_community.php">COMPANY</a></li>-->
			<li class="active">COMPANY</li>
            <!--<li><a href="profileedit_support.php">SUPPORT</a></li>-->
		   <?php
		   }
		   elseif($newres1['team_id']!=0 && $new_team['status']==0)
		   {
		   ?>
		    <li><a href="profileedit.php">HOME</a></li>	
<li><a href="profileedit_media.php">MEDIA</a></li>			
			<li><a href="profileedit_team.php">TEAM</a></li>
            <!--<li><a href="profileedit_support.php">SUPPORT</a></li>-->
`		   <?php
		   }
		   else
		   {
		   ?>
		   <li class="active">HOME</li>		
            <!--<li><a href="profileedit_support.php">SUPPORT</a></li>-->
		   <?php
		   }
			?>
          </ul>
      </div>
      
<script type="text/javascript">
function confirm_saving()
{
	/*var r=confirm("Do you want to save your changes.");
	if (r==true)
	{*/
		submit_form();
	//}
}

function submit_form()
{
	var a=document.getElementById("clickbutton");
	a.click();
	//document.forms["registration_form"].submit();
}
</script>
<!-- Files adde for fancy box2-->
<script type="text/javascript" src="new_fancy_box/jquery.fancybox.js?v=2.1.4"></script>
<link rel="stylesheet" type="text/css" href="new_fancy_box/jquery.fancybox.css?v=2.1.4" media="screen" />
<script type="text/javascript" src="new_fancy_box/helpers/jquery.fancybox-media.js?v=1.0.5"></script>
<!-- Files adde for fancy box2 ends here-->
          <div id="communityTab">
            <div id="subNavigation" class="tabs">
              <ul class="nav">
                <li><a href="#profile" class="current">Profile</a></li>
                <li><a href="#event" class="current">Events</a></li>
                <!--<li><a href="#services">Add Service</a></li>-->
                <!--<li><a href="#memberships">Fan Club</a></li>-->
                <li><a href="#projects">Projects</a></li>
                <li><a href="#promotions">Promotions</a></li>
              </ul>
            </div>
            <div class="list-wrap">
            	<div class="subTabs" id="profile" style="display:none;">
                	<h1> <?php echo $res1['name']; ?> Profile</h1>
					<div id="mediaContent">
                    	<div class="topLinks" style="border-bottom:1px dotted #999;">
                            <div class="links" style="float:left;">
								<ul>
									<li><a href="/<?php echo $res1['profile_url']; ?>" onclick ="return check_changed('<?php echo $res1['profile_url']; ?>');" style="margin-left:0px;"><input type="button" value="View Profile" style="border-radius:5px; -webkit-border-radius:5px; -moz-border-radius:5px; -ms-border-radius:5px; -o-border-radius:5px;color: rgb(255, 255, 255); font-family: 'futura-pt',sans-serif; font-size: 0.825em; font-weight: bold; padding: 6px 10px; float: left; margin-right: 10px; border: 0px none; background-color: rgb(0, 0, 0);" /></a></li>
									<li><a href="javascript:void(0);" onclick="return confirm_saving()"><input type="button" value=" Save "  style="border-radius:5px; -webkit-border-radius:5px; -moz-border-radius:5px; -ms-border-radius:5px; -o-border-radius:5px;color: rgb(255, 255, 255); font-family: 'futura-pt',sans-serif; font-size: 0.825em; font-weight: bold; padding: 6px 10px; float: left; margin-right: 10px; border: 0px none; background-color: rgb(0, 0, 0);" /></a></li>
								</ul>
							</div>
						</div>
					</div>
                	<div style="width:665px; float:left;">
						<div id="actualContent">
						 <div class="fieldCont">
                             <div title="Select an image to crop and display on your profile page." class="fieldTitle">*Profile Picture</div>
							 <?php
							 //echo $res1['image_name'];
								if(isset($res1['image_name']))
								{
									$name_image = $res1['image_name'];
									$new_image = strpos($name_image,'bnail');
								}
								else
								{
									$new_image = "";
								}
								//echo $new_image;
							 ?>
						     <img id="profile_image_new" src="https://comjcropprofile.s3.amazonaws.com/<?php 
							if($res1['image_name']=="" || $new_image==0)
							{
								echo "Noimage.png";
							}
							else
							{
								echo $res1['image_name'];
							}	
							?>" width="100"/>
							 <?php	include_once('community_jcrop/community.php'); ?>
						</div>
						<div class="fieldCont">
                            <div title="Select an image to crop and display on when your profile is listed on other profile pages and in the search engine." class="fieldTitle">*Listing Picture</div>
							<img width="100" id="listing_image_new" src="https://comjcropthumb.s3.amazonaws.com/<?php
							if($res1['listing_image_name']=="")
							{
								echo "Noimage.png";
							}
							else
							{
								echo $res1['listing_image_name'];
							}	
							?>" />
							<?php include_once('community_jcrop/communityThumb.php');?>						
						</div>
                          <form id="registration_form" name="registration_form" method="post" action="add_general_community.php" enctype="multipart/form-data" onSubmit="return validate()">
							<input type="submit" id="clickbutton" value="save" style="display:none"/>
                                <div class="fieldCont">
                                    <div id="user_email_msg" class="hint" style="color:#FF0000">
                                                  
                                    </div>           
                                </div>
                            
                                <div class="fieldCont">
                                  <div title="Select a unique name to display on your profile page and across the site when listed." class="fieldTitle">*Name</div>
                                  <input title="Select a unique name to display on your profile page and across the site when listed." name="name" type="text" class="fieldText" id="name" value="<?php echo $res1['name'];?>">
                                  <div class="hint" style="width:166px;">Full Business/Organization Name</div>
                                </div>

                                <!--<div class="fieldCont">
                                  <div class="fieldTitle">Email</div>
                                  <input name="email" type="text" disabled="disabled" class="fieldText" id="email" value="<?php //echo $res['email'];?>">
                                </div>-->
                                
                                
                                <div class="fieldCont">
                                  <div title="Select a unique URL to be used for accessing your profile page." class="fieldTitle">*Profile URL</div>
								  <div class="urlTitle"><?php echo $domainname; ?>/</div>
                                  <input title="Select a unique URL to be used for accessing your profile page." style="height:27px;" name="subdomain" type="text" class="urlField" id="subdomain" value="<?php echo $res1['profile_url'];?>">
                                <div class="hintnew"><span class="hintR" id="fieldCont_domain"></span>(<?php echo $domainname; ?>/leerick)</div>
                                </div>
                                
                                <div class="fieldCont">
                                  <div title="Add your own website to link to from your profile page." class="fieldTitle">Website</div>
								  <?php
								  if($res1['homepage']=="")
								  {
								  ?>
									<div class="urlTitle">https://www.</div>
								  <?php
								  }
								  ?>
                                  <input title="Add your own website to link to from your profile page." name="website" type="text" class="<?php if(isset($res1['homepage']) && $res1['homepage']!=""){echo "fieldText";}else {echo "urlField";}?>" id="website" value="<?php if(isset($res1['homepage']) && $res1['homepage']!=""){ if(strpos($res1['homepage'],':')>0){echo $res1['homepage'];}else {echo "http://www.".$res1['homepage'];}} ?>" style="height:27px;">
                                <div class="hintnew">Example : <?php echo $domainname; ?></div>
								</div>
								
								<div class="fieldCont">
                                <div class="fieldTitle">Country</div>
                                <select name="selectCountry" id="selectCountry" class="dropdown">
									<option value="0">Select</option>
                                    <?php
									while($get_country = mysql_fetch_assoc($get_all_countries))
									{
										if($res1['country_id'] == $get_country['country_id'])
										{	
										?>
											<option value="<?php echo $get_country['country_id']?>" Selected><?php echo $get_country['country_name'];?></option>
										<?php	
										}
									?>
										<option value="<?php echo $get_country['country_id']?>"><?php echo $get_country['country_name'];?></option>
									<?php
									}
									
									?>
                                </select>
                            </div>
                            <div class="fieldCont">
                                <div class="fieldTitle">State / Province</div>
                                <select name="selectState" id="selectState" class="dropdown">
									<option value="0">Select State</option>
                                    <?php
									//if(isset($_GET['id']))
									//{
										while($getstate = mysql_fetch_assoc($get_all_states))
										{
											if($getstate['state_id'] == $res1['state_id'])
											{
										?>
												<option value="<?php echo $getstate['state_id']; ?>" selected><?php echo $getstate['state_name']; ?></option>
										<?php
											}
											else
											{
											?>
												<option value="<?php echo $getstate['state_id']; ?>" ><?php echo $getstate['state_name']; ?></option>
											<?php
											}
										}
									//}
									?>
                                </select>
                            </div>
                            <div class="fieldCont">
                                <div class="fieldTitle">City</div>
                                <input name="editCity" id="editCity" class="fieldText" value="<?php echo $res1['city']; ?>"/>
                                </input>
                            </div>
							
								<div class="fieldCont">
                                  <div title="Select a media file to feature on your profile page and when your profile is listed on other profiles and in search engine." class="fieldTitle">Featured Media</div>
                                 <select title="Select a media file to feature on your profile page and when your profile is listed on other profiles and in search engine." name="featured_media" class="dropdown" id="featured_media">
                                    <option value="nodisplay" >Select Featured Media</option>
                                    <option value="Gallery" <?php if(isset($res1['featured_media']) && $res1['featured_media']=="Gallery") echo "selected";?> >Gallery</option>	
                                    <option value="Song" <?php if(isset($res1['featured_media']) && $res1['featured_media']=="Song") echo "selected";?> >Song</option>
                                    <option value="Video" <?php if(isset($res1['featured_media']) && $res1['featured_media']=="Video") echo "selected";?> >Video</option>		
                                    <!--<option value="Channel" <?php /*if(isset($res1['featured_media']) && $res1['featured_media']=="Channel") echo "selected";*/?> >Channel</option>-->
                                </select>
								<div id="loader" style="display:none;">
									<img src="images/ajax_loader_large.gif" style="height: 25px; position: relative; right: 120px; top: 34px; width: 25px;">
								</div>
								<div id="fieldCont_media" style="display:none;">
								</div>
                                </div>
								<?php 
								if(isset($res1['featured_media']))
								{
								?>
								<script type="text/javascript">
									selected_feature("<?php echo $res1['featured_media'];?>");
								</script>
								<?php
								}
								?>
								
								<div class="fieldCont">
                                  <div class="fieldTitle">Bio</div>
                                  <!--<input name="bio" type="text" class="fieldText" id="bio" value="<?php //echo $res1['bio'];?>">-->
								 <!-- <textarea  class="jquery_ckeditor" cols="4" id="bio" name="bio" rows="10"><?php echo $res1['bio'];?></textarea>-->
								 <textarea spellcheck="true" class="" cols="65" id="bio" name="bio" rows="10"><?php echo $res1['bio'];?></textarea>
								  <!--<div id="count_for_bio" class="counts"></div>-->
                                </div>   

								<!--<div class="fieldCont">
                                  <div class="fieldTitle">Featured Media</div>
                                  <select name="featured_media" class="dropdown" id="featured_media">
                                    <option value="nodisplay" >Select Featured Media</option>
                                    <option value="Gallery" >Gallery</option>	
                                    <option value="Song" >Song</option>
                                    <option value="Video" >Video</option>		
                                    <option value="Channel" >Channel</option>					
								</select>
								<!--<div id="fieldCont_media" style="float: left; margin-bottom: 0px; width: 500px;">
								</div>-->
								
								
								
								<!--<div class="fieldCont">
                                  <div class="fieldTitle">Featured Media</div>
                                 <select name="featured_media" class="dropdown" id="featured_media">
                                    <option value="nodisplay" >Select Featured Media</option>
                                    <option value="Gallery" >Gallery</option>	
                                    <option value="Song" >Song</option>
                                    <option value="Video" >Video</option>		
                                    <option value="Channel" >Channel</option>					
                                </select>
								<div id="fieldCont_media">
								</div>
                                </div>-->
								  
							<h4>Type Association</h4>
                                <div class="fieldCont" style="font-size:12px;">Select type associations to represent your profile. Your first selections will display on your profile page, all others will be used for search engines. If you would like to change your display than remove all types and select your desired display type first.</br> </br>Suggest a new type or subtype by emailing info@purifyart.com</div>
								<div  style="float:right;" id="selected-types"></div>
                                <div class="fieldCont">
                              <div class="fieldTitle">*Type</div>
                              <select name="select-category-artist" class="dropdown" id="type-select">
									<option value="0">Select Primary Type </option>
									<?php
									while($gettype=mysql_fetch_assoc($seltype))
									{
										/*if($gettype['type_id'] == $seltypeid['type_id'])
										{
										?>
											<option value="<?php echo $gettype['type_id']; ?>" selected="selected" chkval="<?php echo $gettype['name']; ?>"><?php echo $gettype['name']; ?></option>
										<?php
										}
										else
										{*/
										?>
										<option value="<?php echo $gettype['type_id']; ?>"  chkval="<?php echo $gettype['name']; ?>"><?php echo $gettype['name']; ?></option>
										<?php
										//}
									}
									if(!mysql_fetch_assoc($seltype))
									{
									?>
									<option value="<?php echo $seltypeid['type_id']; ?>"  chkval="<?php echo $seltypeid['name']; ?>"><?php echo $seltypeid['name']; ?></option>
									<?php
									}
									?>
									
                     </select>
                              
                              <div class="hint">Must add at least one.</div>
                            </div>
                                <div class="fieldCont">
                                  <div class="fieldTitle">Sub Type</div>
                                         <select name="select-category-artist-subtype" class="dropdown" id="subtype-select" disabled>
										 <option value="0">Select Sub Type </option>
												<?php
												while($res5=mysql_fetch_assoc($res4))
												{
													/*if($res5['subtype_id']==$get_primary_type['subtype_id'])
													{
													?>
														<option value="<?php echo $res5['subtype_id'];?>" chkval="<?php echo $res5['name']; ?>" selected="selected"><?php echo $res5['name'];?></option>	
													<?php
													}
													else
													{*/
													?>
														<option value="<?php echo $res5['subtype_id'];?>" chkval="<?php echo $res5['name']; ?>" ><?php echo $res5['name'];?></option>
													<?php
													//}
												}
												?>
										 </select>
                    
                                  <!--<div class="hint"><input name="ADD" type="button" id="addType" value=" Add " /></div>-->
                                </div>
								  								
                                <div class="fieldCont">
                                <div class="fieldTitle">Meta Type</div>
                                    <select name="metatype-select" class="dropdown" id="metatype-select" disabled>
									<option value="0">Select Meta Type </option>                                       
									   <?php
												while($metares=mysql_fetch_assoc($meta))
												{
													/*if($metares['meta_id']==$get_primary_type['metatype_id'])
													{
													?>
														<option value="<?php echo $metares['meta_id'];?>" chkval="<?php echo $metares['name'];?>" selected="selected"><?php echo $metares['name'];?></option>					
													<?php
													}
													else
													{*/
													?>
														<option value="<?php echo $metares['meta_id'];?>" chkval="<?php echo $metares['name'];?>" ><?php echo $metares['name'];?></option>					
													<?php
													//}
												}
												?>
                                    </select>
                                  
                                  <div class="hint"><input name="ADD" type="button" id="addType" value=" Add "></div>
                                </div>
								  
								 <!-- <div ><img  src="<?php //echo "https://".$_SERVER['HTTP_HOST']."/purifyart/uploads/profile_pic/".$res1['image_name']; ?>"/></div>
                                  <input name="image" type="file" class="fieldText" id="imgupload" accept="image/*" value="">	
                                  <div class="hintUpload">Upload a .jpg/.png/.gif image no larger than 10MB.</div>
                                  
                                </div>-->
                                <!--<div class="fieldCont">
                                  <div class="fieldTitle">*Listing Picture</div>
                                  <input name="image" type="file" class="fieldText" id="imgupload" accept="image/*">
                                </div>-->
<!--Code For Fan Club and Sale Starts Here-->
					<?php  ?>			<h4>Fan Club</h4>
									<?php
									if(!isset($find_member['general_user_id']) && $find_member['general_user_id']==null)
									{
									?>
									<script type="text/javascript">
									$(document).ready(function(){
									$("#fanclub_fields_none").css({"display":"block"});
									$("#fanclub_fields").css({"display":"none"});
									});
									</script>
									<?php
									}else{
									?>
									<script type="text/javascript">
									$(document).ready(function(){
									$("#fanclub_fields_none").css({"display":"none"});
									$("#fanclub_fields").css({"display":"block"});
									});
									</script>
									<?php
									}
									?>
									<div class="fieldCont" id="fanclub_fields_none" style="display:none">
										Only Purify Art members can sell media or fan club memberships. Purchase or renew your membership <a href="profileedit.php#Become_a_member">here</a>.
									</div>
									<div class="fieldCont" id="fanclub_fields" style="display:none">
										<div title="Sell fan club memberships to users who will be able to access exclusive media you select below." class="fieldTitle">Fan Club</div>
										<div class="chkCont">
											<input title="Sell fan club memberships to users who will be able to access exclusive media you select below." type="radio" name="type" class="chk" value="fan_club_yes" id="type1" onclick="forsale_check(1)" <?php if($res1['type'] =="fan_club"){echo "checked";}?>>
											<div title="Sell fan club memberships to users who will be able to access exclusive media you select below." class="radioTitle">Yes</div>
										</div>
										<div class="chkCont">
											<input title="Sell fan club memberships to users who will be able to access exclusive media you select below." type="radio" name="type" class="chk" value="fan_club_no" id="type2" onclick="forsale_check(0)" <?php if($res1['type'] =="for_sale"){echo "checked";}?>>
											<div title="Sell fan club memberships to users who will be able to access exclusive media you select below." class="radioTitle">No</div>
										</div>
										<a href="notmembererror.php" style="display:none;" id="member_error"></a>
									</div>
							<?php
							//if(isset($_GET['id']) && $_GET['id']!="")
							//{
								if($res1['type'] == "fan_club_yes")
								{
								?>
								<script>
								$(document).ready(function(){
									$("#type1").click();
								});
								</script>
								<?php	
								}
								if($res1['type'] == "fan_club_no")
								{
								?>
								<script>
									$("#type2").click();
								</script>
								<?php	
								}
								
							//}
							if(isset($find_member['general_user_id']) && $find_member['general_user_id']!=null)
							{
									$get_comm_member=$newgeneral->Get_comm_member_Id();
							?>
									<div id="fan_club" style="display:none">
										<div class="fieldCont" style="display:none">
											<div title="Choose to sell annual or lifetime fan club memberships to users who will be able to access exclusive media you select below." class="fieldTitle">Frequency</div>
											<select title="Choose to sell annual or lifetime fan club memberships to users who will be able to access exclusive media you select below." name="frequency" class="dropdown" id="frequency">
												<option value="0">Select</option>
												<option value="annual" <?php if($get_comm_member['frequency']=="annual"){echo "selected"; }?>>Annual</option>
												<option value="lifetime" <?php if($get_comm_member['frequency']=="lifetime"){echo "selected"; }?>>LifeTime</option>
											</select>
										</div>
										<div class="fieldCont">
											<div title="Sell fan club memberships to users who will be able to access exclusive media you select below." class="fieldTitle">Price</div>
											<input title="Sell fan club memberships to users who will be able to access exclusive media you select below." type="text" class="fieldText" name="fan_price" id="fan_price" value="<?php echo $res1['price'];?>"><div class="hintnew">/ Year</div>
										</div>
										<div class="fieldCont">
											<div title="Sell fan club memberships to users who will be able to access exclusive media you select below." class="fieldTitle">Description</div>
											<textarea title="Sell fan club memberships to users who will be able to access exclusive media you select below."  class="jquery_ckeditor" cols="4" id="description" name="description" rows="10"><?php echo $get_comm_member['description'];?></textarea>
										</div>
										<div class="fieldCont">
										  <div class="fieldTitle">Songs</div>
											<select class="list" id="fan_club_song[]" multiple="multiple" size="5" name="fan_club_song[]">
												<?php
												$get_song_id = array();
												$get_community_song_id=$newgeneral->get_community_song_id();
												while($get_song_ids = mysql_fetch_assoc($get_community_song_id))
												{
													$get_song_id[] = $get_song_ids;
												}
													
												/*This function will display the user in which profile he is creator and display media where that profile was creator*/
												$new_medi = $newgeneral->get_creator_heis();
												$new_creator_creator_media_array = array();
												if(!empty($new_medi))
												{
													for($count_cre_m=0;$count_cre_m<count($new_medi);$count_cre_m++)
													{
														if($new_medi[$count_cre_m]['media_type']==114){
															$new_creator_creator_media_array[] = $new_medi[$count_cre_m];
														}
													}
												}
												//var_dump($new_creator_creator_media_array);
												/*This function will display the user in which profile he is creator and display media where that profile was creator Ends here*/
												
												/*This function will display the user in which profile he is from and display media where that profile was creator*/
												$new_medi_from = $newgeneral->get_from_heis();
												$new_from_from_media_array = array();
												if(!empty($new_medi_from))
												{
													for($count_cre_m=0;$count_cre_m<count($new_medi_from);$count_cre_m++)
													{
														if($new_medi_from[$count_cre_m]['media_type']==114){
														$new_from_from_media_array[] = $new_medi_from[$count_cre_m];
														}
													}
												}
												//var_dump($new_creator_creator_media_array);
												/*This function will display the user in which profile he is from and display media where that profile was creator Ends here*/
												
												
												/*This function will display the user in which profile he is from or creator and tagged and display media where that profile was creator*/
												$new_medi_tagged = $newgeneral->get_the_user_tagin_project($_SESSION['login_email']);
												$new_tagged_from_media_array = array();
												if(!empty($new_medi_tagged))
												{
													for($count_cre_m=0;$count_cre_m<count($new_medi_tagged);$count_cre_m++)
													{
														if($new_medi_tagged[$count_cre_m]['media_type']==114){
														$new_tagged_from_media_array[] = $new_medi_tagged[$count_cre_m];
														}
													}
												}
												//var_dump($new_creator_creator_media_array);
												/*This function will display the user in which profile he is from or creator and tagged and display media where that profile was creator Ends here*/
												
												
												/*This function will display in which media the curent user was creator*/
												$get_where_creator_media = $newgeneral->get_media_where_creator_or_from();
												$new_creator_media_array = array();
												if(!empty($get_where_creator_media))
												{
													for($count_tag_m=0;$count_tag_m<count($get_where_creator_media);$count_tag_m++)
													{
														if($get_where_creator_media[$count_tag_m]['media_type']==114){
															$new_creator_media_array[] = $get_where_creator_media[$count_tag_m];
														}
													}
												}
												/*This function will display in which media the curent user was creator ends here*/
												
											
												$acc_medss = array();
												for($acc_vis=0;$acc_vis<count($acc_media);$acc_vis++)
												{
													if($acc_media[$acc_vis]!="")
													{
														$sels_tagged = $newgeneral->get_community_media_type_tagged_by_other_user($acc_media[$acc_vis]);
														if(!empty($sels_tagged))
														{
															
															$acc_medss[] = $sels_tagged;
														}
													}
												}
												$get_songs_final_ct = array();
												$type = '114';
												$cre_frm_med = array();
												$cre_frm_med = $newgeneral->get_media_create($type);
												
												$get_songs_pnew_w = array_merge($acc_medss,$get_song_id,$get_songs_final_ct,$cre_frm_med,$new_creator_creator_media_array,$new_from_from_media_array,$new_tagged_from_media_array,$new_creator_media_array);
												
												for($acc_song1=0;$acc_song1<count($get_songs_pnew_w);$acc_song1++)
												{
													if($get_songs_pnew_w[$acc_song1]['id']!="" && $get_songs_pnew_w[$acc_song1]['id']!=0)
													{
														$get_artist_song1 = $newgeneral->get_community_song($get_songs_pnew_w[$acc_song1]['id']);
														$getsong1 = mysql_fetch_assoc($get_artist_song1);
														$ids = $get_songs_pnew_w[$acc_song1]['id'];
														$get_songs_pnew_w[$acc_song1]['track'] = $getsong1['track'];
													}
												}
												
												$sort = array();
												foreach($get_songs_pnew_w as $k=>$v) {
												$end_c1 = $v['creator'];
													//$create_c = strpos($v['creator'],'(');
													//$end_c1 = substr($v['creator'],0,$create_c);
													$end_c = trim($end_c1);
													
													$end_f1 = $v['from'];
													//$create_f = strpos($v['from'],'(');
													//$end_f1 = substr($v['from'],0,$create_f);
													$end_f = trim($end_f1);
													
													$sort['creator'][$k] = strtolower($end_c);
													$sort['from'][$k] = strtolower($end_f);
													$sort['track'][$k] = $v['track'];
													$sort['title'][$k] = strtolower($v['title']);
												}
												//var_dump($sort);
												if(!empty($sort))
												{
													array_multisort($sort['creator'], SORT_ASC, $sort['from'], SORT_ASC, $sort['track'], SORT_ASC,$sort['title'], SORT_ASC,$get_songs_pnew_w);
												}
												//$get_songs_pnew = array_unique($get_songs_pnew_w);
												//var_dump($get_songs_pnew);
												
												$dummy_s = array();
												$get_all_del_id = $newgeneral->get_all_del_media_id();
												$deleted_id = explode(",",$get_all_del_id);
												for($count_del=0;$count_del<=count($get_songs_pnew_w);$count_del++)
												{
													if($get_songs_pnew_w[$count_del]==""){continue;}
													else{
														for($count_deleted=0;$count_deleted<=count($deleted_id);$count_deleted++)
														{
															if($deleted_id[$count_deleted]==""){continue;}
															else{
																if($deleted_id[$count_deleted] == $get_songs_pnew_w[$count_del]['id'])
																{
																	$get_songs_pnew_w[$count_del]="";
																}
															}
														}
													}
												}
												for($acc_song=0;$acc_song<count($get_songs_pnew_w);$acc_song++)
												{
													if($get_songs_pnew_w[$acc_song]['delete_status']==0){
														if(isset($get_songs_pnew_w[$acc_song]['id']))
														{
															if(in_array($get_songs_pnew_w[$acc_song]['id'],$dummy_s))
															{
																
															}
															else
															{
																$dummy_s[] = $get_songs_pnew_w[$acc_song]['id'];
																if($get_songs_pnew_w[$acc_song]['id']!="" && $get_songs_pnew_w[$acc_song]['media_type']==114)
																{
																	//$create_c = strpos($get_songs_pnew_w[$acc_song]['creator'],'(');
																	//$end_c = substr($get_songs_pnew_w[$acc_song]['creator'],0,$create_c);
																	$end_c = $get_songs_pnew_w[$acc_song]['creator'];
																	$end_f = $get_songs_pnew_w[$acc_song]['from'];
																	//$create_f = strpos($get_songs_pnew_w[$acc_song]['from'],'(');
																	//$end_f = substr($get_songs_pnew_w[$acc_song]['from'],0,$create_f);
																	
																	$eceks = "";
																	if($end_c!="")
																	{
																		$eceks = $end_c;
																	}
																	if($end_f!="")
																	{
																		if($end_c!="")
																		{
																			$eceks .= " : ".$end_f;
																		}
																		else
																		{
																			$eceks .= $end_f;
																		}
																	}
																	if($get_songs_pnew_w[$acc_song]['track']!="")
																	{
																		if($end_c!="" || $end_f!="")
																		{
																			$eceks .= " : ".$get_songs_pnew_w[$acc_song]['track'];
																		}
																		else
																		{
																			$eceks .= $get_songs_pnew_w[$acc_song]['track'];
																		}
																	}
																	if($get_songs_pnew_w[$acc_song]['title']!="")
																	{
																		if($end_c!="" || $end_f!="" || $get_songs_pnew_w[$acc_song]['track']!="")
																		{
																			$eceks .= " : ".$get_songs_pnew_w[$acc_song]['title'];
																		}
																		else
																		{
																			$eceks .= $get_songs_pnew_w[$acc_song]['title'];
																		}
																	}
																	
																	if($get_select_songs_fan!="")
																	{
																		$get_select_songs_fan = array_unique($get_select_songs_fan);
																		$yes_com_tag_pro_song =0;
																		for($count_sel=0;$count_sel<=count($get_select_songs_fan);$count_sel++)
																		{
																			if($get_select_songs_fan[$count_sel]==""){continue;}
																			else
																			{
																				if($get_select_songs_fan[$count_sel] == $get_songs_pnew_w[$acc_song]['id'])
																				{
																					$yes_com_tag_pro_song = 1;
													?>
																					<option value="<?php echo $get_songs_pnew_w[$acc_song]['id'];?>" selected><?php echo stripslashes($eceks); ?></option>
													<?php
																				}
																			}
																		}
																		if($yes_com_tag_pro_song == 0)
																		{													
													?>
																			<option value="<?php echo $get_songs_pnew_w[$acc_song]['id'];?>"><?php echo stripslashes($eceks); ?></option>
													<?php
																		}
																	}
																	else
																	{
						?>
																		<option value="<?php echo $get_songs_pnew_w[$acc_song]['id'];?>" ><?php echo stripslashes($eceks); ?></option>
						<?php
																	}
																}
															}
														}
													}
												}
												
												//if(mysql_num_rows($get_community_song_id)<=0 && $media_song=="" && $res_song==null && count($get_media_cre_art_pro)<=0 && mysql_num_rows($get_media_tag_com_pro)<=0 && mysql_num_rows($get_media_tag_art_pro)<=0)
												if(empty($get_songs_pnew_w))
												{
												?>
													<option value="" >No Listings</option>
												<?php	
												}
												?>
											</select>
										</div>
										<div class="fieldCont">
										  <div class="fieldTitle">Videos</div>
											<select class="list" id="fan_club_video[]" multiple="multiple" size="5" name="fan_club_video[]">
												<?php
												$get_video_id = array();
												$get_community_video_id=$newgeneral->get_community_video_id();
												while($get_video_ids = mysql_fetch_assoc($get_community_video_id))
												{
													$get_video_id[] = $get_video_ids;
												}
												
												/*This function will display the user in which profile he is creator and display media where that profile was creator*/
												$new_medi = $newgeneral->get_creator_heis();
												$new_creator_creator_media_array = array();
												if(!empty($new_medi))
												{
													for($count_cre_m=0;$count_cre_m<count($new_medi);$count_cre_m++)
													{
														if($new_medi[$count_cre_m]['media_type']==115){
															$new_creator_creator_media_array[] = $new_medi[$count_cre_m];
														}
													}
												}
												//var_dump($new_creator_creator_media_array);
												/*This function will display the user in which profile he is creator and display media where that profile was creator Ends here*/
												
												/*This function will display the user in which profile he is from and display media where that profile was creator*/
												$new_medi_from = $newgeneral->get_from_heis();
												$new_from_from_media_array = array();
												if(!empty($new_medi_from))
												{
													for($count_cre_m=0;$count_cre_m<count($new_medi_from);$count_cre_m++)
													{
														if($new_medi_from[$count_cre_m]['media_type']==115){
														$new_from_from_media_array[] = $new_medi_from[$count_cre_m];
														}
													}
												}
												//var_dump($new_creator_creator_media_array);
												/*This function will display the user in which profile he is from and display media where that profile was creator Ends here*/
												
												
												/*This function will display the user in which profile he is from or creator and tagged and display media where that profile was creator*/
												$new_medi_tagged = $newgeneral->get_the_user_tagin_project($_SESSION['login_email']);
												$new_tagged_from_media_array = array();
												if(!empty($new_medi_tagged))
												{
													for($count_cre_m=0;$count_cre_m<count($new_medi_tagged);$count_cre_m++)
													{
														if($new_medi_tagged[$count_cre_m]['media_type']==115){
														$new_tagged_from_media_array[] = $new_medi_tagged[$count_cre_m];
														}
													}
												}
												//var_dump($new_creator_creator_media_array);
												/*This function will display the user in which profile he is from or creator and tagged and display media where that profile was creator Ends here*/
												
												
												/*This function will display in which media the curent user was creator*/
												$get_where_creator_media = $newgeneral->get_media_where_creator_or_from();
												$new_creator_media_array = array();
												if(!empty($get_where_creator_media))
												{
													for($count_tag_m=0;$count_tag_m<count($get_where_creator_media);$count_tag_m++)
													{
														if($get_where_creator_media[$count_tag_m]['media_type']==115){
															$new_creator_media_array[] = $get_where_creator_media[$count_tag_m];
														}
													}
												}
												/*This function will display in which media the curent user was creator ends here*/
												
												$acc_medsv = array();
												for($acc_vis=0;$acc_vis<count($acc_media);$acc_vis++)
												{
													if($acc_media[$acc_vis]!="")
													{
														$sel_tagged = $newgeneral->get_community_media_type_tagged_by_other_user($acc_media[$acc_vis]);
														if(!empty($sel_tagged))
														{
															$acc_medsv[] = $sel_tagged;
														}
													}
												}
												$get_vids_final_ct = array();
												$type = '115';
												$cre_frm_med = array();
												$cre_frm_med = $newgeneral->get_media_create($type);
												
												$get_vids_pnew_w = array_merge($acc_medsv,$get_video_id,$get_vids_final_ct,$cre_frm_med,$new_creator_creator_media_array,$new_from_from_media_array,$new_tagged_from_media_array,$new_creator_media_array);
												
												$sort = array();
												foreach($get_vids_pnew_w as $k=>$v) {
												
													$end_c1 = $v['creator'];
													//$create_c = strpos($v['creator'],'(');
													//$end_c1 = substr($v['creator'],0,$create_c);
													$end_c = trim($end_c1);
													
													$end_f1 = $v['from'];
													//$create_f = strpos($v['from'],'(');
													//$end_f1 = substr($v['from'],0,$create_f);
													$end_f = trim($end_f1);
													
													$sort['creator'][$k] = strtolower($end_c);
													$sort['from'][$k] = strtolower($end_f);
													//$sort['track'][$k] = $v['track'];
													$sort['title'][$k] = strtolower($v['title']);
												}
												//var_dump($sort);
												if(!empty($sort))
												{
													array_multisort($sort['creator'], SORT_ASC, $sort['from'], SORT_ASC, $sort['title'], SORT_ASC,$get_vids_pnew_w);
												}
												
												$dummy_v = array();
												$get_all_del_id = $newgeneral->get_all_del_media_id();
												$deleted_id = explode(",",$get_all_del_id);
												for($count_del=0;$count_del<=count($get_vids_pnew_w);$count_del++)
												{
													if($get_vids_pnew_w[$count_del]==""){continue;}
													else{
														for($count_deleted=0;$count_deleted<=count($deleted_id);$count_deleted++)
														{
															if($deleted_id[$count_deleted]==""){continue;}
															else{
																if($deleted_id[$count_deleted] == $get_vids_pnew_w[$count_del]['id'])
																{
																	$get_vids_pnew_w[$count_del]="";
																}
															}
														}
													}
												}
												for($acc_vide=0;$acc_vide<count($get_vids_pnew_w);$acc_vide++)
												{
													if($get_vids_pnew_w[$acc_vide]['delete_status']==0)
													{
														if(isset($get_vids_pnew_w[$acc_vide]['id']))
														{
															if(in_array($get_vids_pnew_w[$acc_vide]['id'],$dummy_v))
															{
																
															}
															else
															{
																$dummy_v[] = $get_vids_pnew_w[$acc_vide]['id'];
																if($get_vids_pnew_w[$acc_vide]['id']!="" && $get_vids_pnew_w[$acc_vide]['media_type']==115)
																{
																	//$create_c = strpos($get_vids_pnew_w[$acc_vide]['creator'],'(');
																	//$end_c = substr($get_vids_pnew_w[$acc_vide]['creator'],0,$create_c);
																	$end_c = $get_vids_pnew_w[$acc_vide]['creator'];
																	$end_f = $get_vids_pnew_w[$acc_vide]['from'];
																	//$create_f = strpos($get_vids_pnew_w[$acc_vide]['from'],'(');
																	//$end_f = substr($get_vids_pnew_w[$acc_vide]['from'],0,$create_f);
																	
																	$eceksv = "";
																	if($end_c!="")
																	{
																		$eceksv = $end_c;
																	}
																	if($end_f!="")
																	{
																		if($end_c!="")
																		{
																			$eceksv .= " : ".$end_f;
																		}
																		else
																		{
																			$eceksv .= $end_f;
																		}
																	}
																	if($get_vids_pnew_w[$acc_vide]['title']!="")
																	{
																		if($end_c!="" || $end_f!="")
																		{
																			$eceksv .= " : ".$get_vids_pnew_w[$acc_vide]['title'];
																		}
																		else
																		{
																			$eceksv .= $get_vids_pnew_w[$acc_vide]['title'];
																		}
																	}
																	
																	if($get_select_videos_fan!="")
																	{
																		$get_select_videos_fan = array_unique($get_select_videos_fan);
																		$yes_com_tag_pro_song =0;
																		for($count_sel=0;$count_sel<=count($get_select_videos_fan);$count_sel++)
																		{
																			if($get_select_videos_fan[$count_sel]==""){continue;}
																			else
																			{
																				if($get_select_videos_fan[$count_sel] == $get_vids_pnew_w[$acc_vide]['id'])
																				{
																					$yes_com_tag_pro_song = 1;
													?>
																					<option value="<?php echo $get_vids_pnew_w[$acc_vide]['id'];?>" selected><?php echo stripslashes($eceksv); ?></option>
													<?php
																				}
																			}
																		}
																		if($yes_com_tag_pro_song == 0)
																		{													
													?>
																			<option value="<?php echo $get_vids_pnew_w[$acc_vide]['id'];?>"><?php echo stripslashes($eceksv); ?></option>
													<?php
																		}
																	}
																	else
																	{
						?>
																		<option value="<?php echo $get_vids_pnew_w[$acc_vide]['id'];?>" ><?php echo stripslashes($eceksv); ?></option>
						<?php
																	}
																}
															}
														}
													}
												}
												
												//if(mysql_num_rows($get_community_video_id)<=0 && $media_video=="" && $res_video==null && count($get_media_cre_art_pro)<=0 && mysql_num_rows($get_media_tag_com_pro)<=0 && mysql_num_rows($get_media_tag_art_pro)<=0)
												if(empty($get_vids_pnew_w))
												{
												?>
													<option value="" >No Listings</option>
												<?php	
												}
												?>			
											</select>
										</div>
										<div class="fieldCont">
										  <div class="fieldTitle">Galleries</div>
											<select class="list" id="fan_club_gallery[]" multiple="multiple" size="5" name="fan_club_gallery[]">
												<?php
													$get_gallery_id = array();
													$get_community_gallery_id=$newgeneral->get_community_gallery_id();
													while($get_gallery_ids = mysql_fetch_assoc($get_community_gallery_id))
													{
														$get_gallery_id[] = $get_gallery_ids;
													}
													
													/*This function will display the user in which profile he is creator and display media where that profile was creator*/
													$new_medi = $newgeneral->get_creator_heis();
													$new_creator_creator_media_array = array();
													if(!empty($new_medi))
													{
														for($count_cre_m=0;$count_cre_m<count($new_medi);$count_cre_m++)
														{
															if($new_medi[$count_cre_m]['media_type']==113){
																$new_creator_creator_media_array[] = $new_medi[$count_cre_m];
															}
														}
													}
													//var_dump($new_creator_creator_media_array);
													/*This function will display the user in which profile he is creator and display media where that profile was creator Ends here*/
													
													/*This function will display the user in which profile he is from and display media where that profile was creator*/
													$new_medi_from = $newgeneral->get_from_heis();
													$new_from_from_media_array = array();
													if(!empty($new_medi_from))
													{
														for($count_cre_m=0;$count_cre_m<count($new_medi_from);$count_cre_m++)
														{
															if($new_medi_from[$count_cre_m]['media_type']==113){
															$new_from_from_media_array[] = $new_medi_from[$count_cre_m];
															}
														}
													}
													//var_dump($new_creator_creator_media_array);
													/*This function will display the user in which profile he is from and display media where that profile was creator Ends here*/
													
													
													/*This function will display the user in which profile he is from or creator and tagged and display media where that profile was creator*/
													$new_medi_tagged = $newgeneral->get_the_user_tagin_project($_SESSION['login_email']);
													$new_tagged_from_media_array = array();
													if(!empty($new_medi_tagged))
													{
														for($count_cre_m=0;$count_cre_m<count($new_medi_tagged);$count_cre_m++)
														{
															if($new_medi_tagged[$count_cre_m]['media_type']==113){
															$new_tagged_from_media_array[] = $new_medi_tagged[$count_cre_m];
															}
														}
													}
													//var_dump($new_creator_creator_media_array);
													/*This function will display the user in which profile he is from or creator and tagged and display media where that profile was creator Ends here*/
													
													
													/*This function will display in which media the curent user was creator*/
													$get_where_creator_media = $newgeneral->get_media_where_creator_or_from();
													$new_creator_media_array = array();
													if(!empty($get_where_creator_media))
													{
														for($count_tag_m=0;$count_tag_m<count($get_where_creator_media);$count_tag_m++)
														{
															if($get_where_creator_media[$count_tag_m]['media_type']==113){
																$new_creator_media_array[] = $get_where_creator_media[$count_tag_m];
															}
														}
													}
													/*This function will display in which media the curent user was creator ends here*/
													
													$acc_medsg = array();
													for($acc_vis=0;$acc_vis<count($acc_media);$acc_vis++)
													{
														if($acc_media[$acc_vis]!="")
														{
															$segl_tagged = $newgeneral->get_community_media_type_tagged_by_other_user($acc_media[$acc_vis]);
															if(!empty($segl_tagged))
															{
																$acc_medsg[] = $segl_tagged;
															}
														}
													}
													$get_gals_final_ct = array();
													$type = '113';
													$cre_frm_med = array();
													$cre_frm_med = $newgeneral->get_media_create($type);
													
													$get_vgals_pnew_w = array_merge($acc_medsg,$get_gallery_id,$get_gals_final_ct,$cre_frm_med,$new_creator_creator_media_array,$new_from_from_media_array,$new_tagged_from_media_array,$new_creator_media_array);
													
													$sort = array();
													foreach($get_vgals_pnew_w as $k=>$v) {
													
														$end_c1 = $v['creator'];
														//$create_c = strpos($v['creator'],'(');
														//$end_c1 = substr($v['creator'],0,$create_c);
														$end_c = trim($end_c1);
														
														$end_f1 = $v['from'];
														//$create_f = strpos($v['from'],'(');
														//$end_f1 = substr($v['from'],0,$create_f);
														$end_f = trim($end_f1);
														
														$sort['creator'][$k] = strtolower($end_c);
														$sort['from'][$k] = strtolower($end_f);
														//$sort['track'][$k] = $v['track'];
														$sort['title'][$k] = strtolower($v['title']);
													}
													//var_dump($sort);
													//$sort = array_unique($sort['id']);
													if(!empty($sort))
													{
														array_multisort($sort['creator'], SORT_ASC, $sort['from'], SORT_ASC, $sort['title'], SORT_ASC,$get_vgals_pnew_w);									
													}
													
													$dummy_g = array();
													$get_all_del_id = $newgeneral->get_all_del_media_id();
													$deleted_id = explode(",",$get_all_del_id);
													for($count_del=0;$count_del<=count($get_vgals_pnew_w);$count_del++)
													{
														if($get_vgals_pnew_w[$count_del]==""){continue;}
														else{
															for($count_deleted=0;$count_deleted<=count($deleted_id);$count_deleted++)
															{
																if($deleted_id[$count_deleted]==""){continue;}
																else{
																	if($deleted_id[$count_deleted] == $get_vgals_pnew_w[$count_del]['id'])
																	{
																		$get_vgals_pnew_w[$count_del]="";
																	}
																}
															}
														}
													}
													for($acc_glae=0;$acc_glae<count($get_vgals_pnew_w);$acc_glae++)
													{
														if($get_vgals_pnew_w[$acc_glae]['delete_status']==0)
														{
															if(isset($get_vgals_pnew_w[$acc_glae]['id']))
															{
																if(in_array($get_vgals_pnew_w[$acc_glae]['id'],$dummy_g))
																{
																	
																}
																else
																{
																	$dummy_g[] = $get_vgals_pnew_w[$acc_glae]['id'];
																	if($get_vgals_pnew_w[$acc_glae]['id']!="" && $get_vgals_pnew_w[$acc_glae]['media_type']==113)
																	{
																		//$create_c = strpos($get_vgals_pnew_w[$acc_glae]['creator'],'(');
																		//$end_c = substr($get_vgals_pnew_w[$acc_glae]['creator'],0,$create_c);
																		$end_c = $get_vgals_pnew_w[$acc_glae]['creator'];
																		$end_f = $get_vgals_pnew_w[$acc_glae]['from'];
																		//$create_f = strpos($get_vgals_pnew_w[$acc_glae]['from'],'(');
																		//$end_f = substr($get_vgals_pnew_w[$acc_glae]['from'],0,$create_f);
																		
																		$eceksg = "";
																		if($end_c!="")
																		{
																			$eceksg = $end_c;
																		}
																		if($end_f!="")
																		{
																			if($end_c!="")
																			{
																				$eceksg .= " : ".$end_f;
																			}
																			else
																			{
																				$eceksg .= $end_f;
																			}
																		}
																		if($get_vgals_pnew_w[$acc_glae]['title']!="")
																		{
																			if($end_c!="" || $end_f!="")
																			{
																				$eceksg .= " : ".$get_vgals_pnew_w[$acc_glae]['title'];
																			}
																			else
																			{
																				$eceksg .= $get_vgals_pnew_w[$acc_glae]['title'];
																			}
																		}
																		
																		if($get_select_galleries_fan!="")
																		{
																			$get_select_galleries_fan = array_unique($get_select_galleries_fan);
																			$yes_com_tag_pro_song =0;
																			for($count_sel=0;$count_sel<=count($get_select_galleries_fan);$count_sel++)
																			{
																				if($get_select_galleries_fan[$count_sel]==""){continue;}
																				else
																				{
																					if($get_select_galleries_fan[$count_sel] == $get_vgals_pnew_w[$acc_glae]['id'])
																					{
																						$yes_com_tag_pro_song = 1;
														?>
																						<option value="<?php echo $get_vgals_pnew_w[$acc_glae]['id'];?>" selected><?php echo stripslashes($eceksg); ?></option>
														<?php
																					}
																				}
																			}
																			if($yes_com_tag_pro_song == 0)
																			{													
														?>
																				<option value="<?php echo $get_vgals_pnew_w[$acc_glae]['id'];?>"><?php echo stripslashes($eceksg); ?></option>
														<?php
																			}
																		}
																		else
																		{
							?>
																			<option value="<?php echo $get_vgals_pnew_w[$acc_glae]['id'];?>" ><?php echo stripslashes($eceksg); ?></option>
							<?php
																		}
																	}
																}
															}
														}
													}

													//if(mysql_num_rows($get_community_gallery_id)<=0 && $media_gal=="" && $res_gal==null && count($get_media_cre_art_pro)<=0 && mysql_num_rows($get_media_tag_com_pro)<=0 && mysql_num_rows($get_media_tag_art_pro)<=0)
													if(empty($get_vgals_pnew_w))
													{
													?>
														<option value="" >No Listings</option>
													<?php	
													}
													
													?>
											</select>
										</div>
										<div class="fieldCont">
										  <div class="fieldTitle">Playlist</div>
											<select class="list" id="fan_club_channel[]" multiple="multiple" size="5" name="fan_club_channel[]">
												<?php
													$get_channel_id = array();
													while($get_channel_ids = mysql_fetch_assoc($get_community_channel_id))
													{
														$get_channel_id[] = $get_channel_ids;
													}
													
													/*This function will display the user in which profile he is creator and display media where that profile was creator*/
													$new_medi = $newgeneral->get_creator_heis();
													$new_creator_creator_media_array = array();
													if(!empty($new_medi))
													{
														for($count_cre_m=0;$count_cre_m<count($new_medi);$count_cre_m++)
														{
															if($new_medi[$count_cre_m]['media_type']==116){
																$new_creator_creator_media_array[] = $new_medi[$count_cre_m];
															}
														}
													}
													//var_dump($new_creator_creator_media_array);
													/*This function will display the user in which profile he is creator and display media where that profile was creator Ends here*/
													
													/*This function will display the user in which profile he is from and display media where that profile was creator*/
													$new_medi_from = $newgeneral->get_from_heis();
													$new_from_from_media_array = array();
													if(!empty($new_medi_from))
													{
														for($count_cre_m=0;$count_cre_m<count($new_medi_from);$count_cre_m++)
														{
															if($new_medi_from[$count_cre_m]['media_type']==116){
															$new_from_from_media_array[] = $new_medi_from[$count_cre_m];
															}
														}
													}
													//var_dump($new_creator_creator_media_array);
													/*This function will display the user in which profile he is from and display media where that profile was creator Ends here*/
													
													
													/*This function will display the user in which profile he is from or creator and tagged and display media where that profile was creator*/
													$new_medi_tagged = $newgeneral->get_the_user_tagin_project($_SESSION['login_email']);
													$new_tagged_from_media_array = array();
													if(!empty($new_medi_tagged))
													{
														for($count_cre_m=0;$count_cre_m<count($new_medi_tagged);$count_cre_m++)
														{
															if($new_medi_tagged[$count_cre_m]['media_type']==116){
															$new_tagged_from_media_array[] = $new_medi_tagged[$count_cre_m];
															}
														}
													}
													//var_dump($new_creator_creator_media_array);
													/*This function will display the user in which profile he is from or creator and tagged and display media where that profile was creator Ends here*/
													
													
													/*This function will display in which media the curent user was creator*/
													$get_where_creator_media = $newgeneral->get_media_where_creator_or_from();
													$new_creator_media_array = array();
													if(!empty($get_where_creator_media))
													{
														for($count_tag_m=0;$count_tag_m<count($get_where_creator_media);$count_tag_m++)
														{
															if($get_where_creator_media[$count_tag_m]['media_type']==116){
																$new_creator_media_array[] = $get_where_creator_media[$count_tag_m];
															}
														}
													}
													/*This function will display in which media the curent user was creator ends here*/
													
													$acc_medsg = array();
													for($acc_vis=0;$acc_vis<count($acc_media);$acc_vis++)
													{
														if($acc_media[$acc_vis]!="")
														{
															$segl_tagged = $newgeneral->get_community_media_type_tagged_by_other_user($acc_media[$acc_vis]);
															if(!empty($segl_tagged))
															{
																$acc_medsg[] = $segl_tagged;
															}
														}
													}
													$get_gals_final_ct = array();
													$type = '116';
													$cre_frm_med = array();
													$cre_frm_med = $newgeneral->get_media_create($type);
													
													$get_vgals_pnew_w = array_merge($acc_medsg,$get_channel_id,$get_gals_final_ct,$cre_frm_med,$new_creator_creator_media_array,$new_from_from_media_array,$new_tagged_from_media_array,$new_creator_media_array);
													
													$sort = array();
													foreach($get_vgals_pnew_w as $k=>$v) {
														
														$end_c1 = $v['creator'];
														//$create_c = strpos($v['creator'],'(');
														//$end_c1 = substr($v['creator'],0,$create_c);
														$end_c = trim($end_c1);
														
														$end_f1 = $v['from'];
														//$create_f = strpos($v['from'],'(');
														//$end_f1 = substr($v['from'],0,$create_f);
														$end_f = trim($end_f1);
														
														$sort['creator'][$k] = strtolower($end_c);
														$sort['from'][$k] = strtolower($end_f);
														//$sort['track'][$k] = $v['track'];
														$sort['title'][$k] = strtolower($v['title']);
													}
													//var_dump($sort);
													//$sort = array_unique($sort['id']);
													if(!empty($sort))
													{
														array_multisort($sort['creator'], SORT_ASC, $sort['from'], SORT_ASC, $sort['title'], SORT_ASC,$get_vgals_pnew_w);									
													}
													$dummy_g = array();
													$get_all_del_id = $newgeneral->get_all_del_media_id();
													$deleted_id = explode(",",$get_all_del_id);
													for($count_del=0;$count_del<=count($get_vgals_pnew_w);$count_del++)
													{
														if($get_vgals_pnew_w[$count_del]==""){continue;}
														else{
															for($count_deleted=0;$count_deleted<=count($deleted_id);$count_deleted++)
															{
																if($deleted_id[$count_deleted]==""){continue;}
																else{
																	if($deleted_id[$count_deleted] == $get_vgals_pnew_w[$count_del]['id'])
																	{
																		$get_vgals_pnew_w[$count_del]="";
																	}
																}
															}
														}
													}
													for($acc_glae=0;$acc_glae<count($get_vgals_pnew_w);$acc_glae++)
													{
														if($get_vgals_pnew_w[$acc_glae]['delete_status']==0)
														{
															if(isset($get_vgals_pnew_w[$acc_glae]['id']))
															{
																if(in_array($get_vgals_pnew_w[$acc_glae]['id'],$dummy_g))
																{
																	
																}
																else
																{
																	$dummy_g[] = $get_vgals_pnew_w[$acc_glae]['id'];
																	if($get_vgals_pnew_w[$acc_glae]['id']!="" && $get_vgals_pnew_w[$acc_glae]['media_type']==116)
																	{
																		//$create_c = strpos($get_vgals_pnew_w[$acc_glae]['creator'],'(');
																		//$end_c = substr($get_vgals_pnew_w[$acc_glae]['creator'],0,$create_c);
																		$end_c = $get_vgals_pnew_w[$acc_glae]['creator'];
																		$end_f = $get_vgals_pnew_w[$acc_glae]['from'];
																		//$create_f = strpos($get_vgals_pnew_w[$acc_glae]['from'],'(');
																		//$end_f = substr($get_vgals_pnew_w[$acc_glae]['from'],0,$create_f);
																		
																		$eceksg = "";
																		if($end_c!="")
																		{
																			$eceksg = $end_c;
																		}
																		if($end_f!="")
																		{
																			if($end_c!="")
																			{
																				$eceksg .= " : ".$end_f;
																			}
																			else
																			{
																				$eceksg .= $end_f;
																			}
																		}
																		if($get_vgals_pnew_w[$acc_glae]['title']!="")
																		{
																			if($end_c!="" || $end_f!="")
																			{
																				$eceksg .= " : ".$get_vgals_pnew_w[$acc_glae]['title'];
																			}
																			else
																			{
																				$eceksg .= $get_vgals_pnew_w[$acc_glae]['title'];
																			}
																		}
																		
																		if($get_select_channels_fan!="")
																		{
																			$get_select_channels_fan = array_unique($get_select_channels_fan);
																			$yes_com_tag_pro_song =0;
																			for($count_sel=0;$count_sel<=count($get_select_channels_fan);$count_sel++)
																			{
																				if($get_select_channels_fan[$count_sel]==""){continue;}
																				else
																				{
																					if($get_select_channels_fan[$count_sel] == $get_vgals_pnew_w[$acc_glae]['id'])
																					{
																						$yes_com_tag_pro_song = 1;
														?>
																						<option value="<?php echo $get_vgals_pnew_w[$acc_glae]['id'];?>" selected><?php echo stripslashes($eceksg); ?></option>
														<?php
																					}
																				}
																			}
																			if($yes_com_tag_pro_song == 0)
																			{													
														?>
																				<option value="<?php echo $get_vgals_pnew_w[$acc_glae]['id'];?>"><?php echo stripslashes($eceksg); ?></option>
														<?php
																			}
																		}
																		else
																		{
							?>
																			<option value="<?php echo $get_vgals_pnew_w[$acc_glae]['id'];?>" ><?php echo stripslashes($eceksg); ?></option>
							<?php
																		}
																	}
																}
															}
														}
													}

													//if(mysql_num_rows($get_community_gallery_id)<=0 && $media_gal=="" && $res_gal==null && count($get_media_cre_art_pro)<=0 && mysql_num_rows($get_media_tag_com_pro)<=0 && mysql_num_rows($get_media_tag_art_pro)<=0)
													if(empty($get_vgals_pnew_w))
													{
													?>
														<option value="" >No Listings</option>
													<?php	
													}
													
													?>
											</select>
										</div>
									</div>	
									<!--<div id="for_sale_yes" style="display:none">
										<div class="fieldCont">
											<div class="fieldTitle">Price</div>
											<input type="text" class="fieldText" name="sale_price" id="sale_price" value="<?php echo $res1['price'];?>">
										</div>
										<div class="fieldCont">
										  <div class="fieldTitle">Songs</div>
											<select class="list" id="sale_song[]" multiple="multiple" size="5" name="sale_song[]">
												<?php
											/*	$get_sale_club = $newgeneral->get_forsale_media();
												while($row_sale = mysql_fetch_assoc($get_sale_club))
												{
													if($row_sale['media_type']==114)
													{
														$sale_song=1;
															$sale_song_sel =0;
															$exp_sale_song = explode(",",$res1['sale_song']);
															for($count_ss_exp=0;$count_ss_exp<count($exp_sale_song);$count_ss_exp++)
															{
																//
																if($exp_sale_song[$count_ss_exp]=="")
																{
																	continue;
																}
																else if($row_sale['id'] == $exp_sale_song[$count_ss_exp])
																{
																	$sale_song_sel=1;
																	?>
																		<option value="<?php echo $row_sale['id'];?>" selected><?php echo $row_sale['title'];?></option>
																	<?php
																}
															}
															if($sale_song_sel !=1){
														?>
															<option value="<?php echo $row_sale['id'];?>"><?php echo $row_sale['title'];?></option>
														<?php
															}
													}
												}
												if($sale_song==0)
												{
													?>
													<option selected value="0">Select Song</option>
													<?php
												}
												?>	
											</select>
										</div>
										
										<div class="fieldCont">
										  <div class="fieldTitle">Galleries</div>
											<select class="list" id="sale_gallery[]" multiple="multiple" size="5" name="sale_gallery[]">
												<?php
												$get_sale_club = $newgeneral->get_forsale_media();
												while($row_sale = mysql_fetch_assoc($get_sale_club))
												{
													if($row_sale['media_type']==113)
													{
															$sale_gallery=1;
															$sale_gallery_sel =0;
															$exp_sale_gallery = explode(",",$res1['sale_gallery']);
															for($count_sg_exp=0;$count_sg_exp<count($exp_sale_gallery);$count_sg_exp++)
															{
																if($exp_sale_gallery[$count_sg_exp]=="")
																{
																	continue;
																}
																else if($row_sale['id'] == $exp_sale_gallery[$count_sg_exp])
																{
																	$sale_gallery_sel=1;
																	?>
																		<option value="<?php echo $row_sale['id'];?>" selected><?php echo $row_sale['title'];?></option>
																	<?php
																}
															}
															if($sale_gallery_sel !=1){
														?>
															<option value="<?php echo $row_sale['id'];?>"><?php echo $row_sale['title'];?></option>
														<?php
															}
													}
												}
												if($sale_gallery==0)
												{
													?>
													<option selected value="0">Select Gallery</option>
													<?php
												}*/
												?>						
											</select>
										</div>
									</div>-->
							<?php
							}
							?>

<!--Code For Fan Club and Sale Ends Here-->
								<h4>Share Links</h4>
						
                                <div class="fieldCont">
									<div title="" class="fieldTitle">Facebook</div>
									<div class="urlTitle_share">facebook.com/</div>
									<input title="Select a unique URL to be used for accessing your profile page." name="fb_share" type="text" class="urlField" id="fb_share" value="<?php echo $res1['fb_share']; ?>" />
                                </div>
								
								<div class="fieldCont">
									<div title="" class="fieldTitle">Twitter</div>
									<div class="urlTitle_share" >twitter.com/</div>
									<input title="Select a unique URL to be used for accessing your profile page." name="twit_share" type="text" class="urlField" id="twit_share" value="<?php echo $res1['twit_share']; ?>" />
                                </div>
								
								<div class="fieldCont">
									<div title="" class="fieldTitle">Google+</div>
									<div class="urlTitle_share" >plus.google.com/</div>
									<input title="Select a unique URL to be used for accessing your profile page." name="gplus_share" type="text" class="urlField" id="gplus_share" value="<?php echo $res1['gplus_share']; ?>" />
                                </div>
								
								<div class="fieldCont">
									<div title="" class="fieldTitle">Tumblr</div>
									<input title="Select a unique URL to be used for accessing your profile page." name="tubm_share" type="text" class="urlField" id="tubm_share" value="<?php echo $res1['tubm_share']; ?>" />
									<div class="urlTitle_share" >.tumblr.com</div>
                                </div>
								
								<div class="fieldCont">
									<div title="" class="fieldTitle">StumbleUpon</div>
									<div class="urlTitle_share" >stumbleupon.com/stumbler/</div>
									<input title="Select a unique URL to be used for accessing your profile page." name="stbu_share" type="text" class="urlField" id="stbu_share" value="<?php echo $res1['stbu_share']; ?>" />
                                </div>
								
								<div class="fieldCont">
									<div title="" class="fieldTitle">Pinterest</div>
									<div class="urlTitle_share" >pinterest.com/</div>
									<input title="Select a unique URL to be used for accessing your profile page." name="pin_share" type="text" class="urlField" id="pin_share" value="<?php echo $res1['pin_share']; ?>" />
                                </div>
								
								<div class="fieldCont">
									<div title="" class="fieldTitle">Youtube</div>
									<div class="urlTitle_share" >youtube.com/</div>
									<input title="Select a unique URL to be used for accessing your profile page." name="you_share" type="text" class="urlField" id="you_share" value="<?php echo $res1['you_share']; ?>" />
                                </div>
								
								<div class="fieldCont">
									<div title="" class="fieldTitle">Vimeo</div>
									<div class="urlTitle_share" >vimeo.com/</div>
									<input title="Select a unique URL to be used for accessing your profile page." name="vimeo_share" type="text" class="urlField" id="vimeo_share" value="<?php echo $res1['vimeo_share']; ?>" />
                                </div>
								
								<div class="fieldCont">
									<div title="" class="fieldTitle">Soundcloud</div>
									<div class="urlTitle_share" >soundcloud.com/</div>
									<input title="Select a unique URL to be used for accessing your profile page." name="sdcl_share" type="text" class="urlField" id="sdcl_share" value="<?php echo $res1['sdcl_share']; ?>" />
                                </div>
								
								<div class="fieldCont">
									<div title="" class="fieldTitle">Instagram</div>
									<div class="urlTitle_share" >instagram.com/</div>
									<input title="Select a unique URL to be used for accessing your profile page." name="ints_share" type="text" class="urlField" id="ints_share" value="<?php echo $res1['ints_share']; ?>" />
                                </div>


								<h4>Tag Profiles</h4>
								<div class="fieldCont" style="font-size:12px;">Select all projects, media and events that you would like to list on this profiles display page.</div>
                                <div class="fieldCont">
                                  <div title="Select media and profiles to display on your profile page." class="fieldTitle">Projects</div>
                                        <?php
										//var_dump($acc_event);
										//	die;
										/* while($get_channel_id=mysql_fetch_assoc($get_project))
										{	//$get_artist_channel=$editprofile->get_artist_channel($get_channel_id['id']);
											//$getchannel=mysql_fetch_assoc($get_artist_channel);
											if($get_select_projects[0]!="")
											{
												for($i=0;$i<count($get_select_projects);$i++)
												{
													if($get_select_projects[$i]==$get_channel_id['id'])
													{
														$j_chan=1;
											?>
												<option value="<?php echo $get_channel_id['id'];?>" selected ><?php echo $get_channel_id['title'];?></option>
											<?php
														continue;
													}
												}
												if($j_chan==1)
												{
													$j_chan=0;
													continue;
												}
											}
											
										?>
											<option value="<?php echo $get_channel_id['id'];?>" ><?php echo $get_channel_id['title'];?></option>
										<?php	
										} */
										$all_community_friend_project = array();
										$get_all_friends = $newgeneral->Get_all_friends();
										if($get_all_friends!="")
										{
											while($res_all_friends = mysql_fetch_assoc($get_all_friends))
											{
												$get_friend_art_info = $newgeneral->get_friend_art_info($res_all_friends['fgeneral_user_id']);
												$get_all_tag_pro = $newgeneral->get_all_tagged_pro($get_friend_art_info);
												if(mysql_num_rows($get_all_tag_pro)>0)
												{
													while($res_projects = mysql_fetch_assoc($get_all_tag_pro))
													{
														//$get_project_type = $newgeneral->Get_Project_type($res_projects['id']);
														$exp_creator = explode('(',$res_projects['creator']);
														$exp_tagged_email = explode(",",$res_projects['tagged_user_email']);
														for($exp_count = 0;$exp_count<count($exp_tagged_email);$exp_count++)
														{
															if($exp_tagged_email[$exp_count]==""){continue;}
															else{
																if($exp_tagged_email[$exp_count]==$_SESSION['login_email'])
																{
																	//$res_projects['project_type_name'] = $get_project_type['name'];
																	$res_projects['whos_project'] = "tag_pro_com";
																	$res_projects['id'] = $res_projects['id']."~com";
																	$all_community_friend_project[] = $res_projects;
																	?>
																	<!--<div id="AccordionContainer" class="tableCont">
																		<div class="blkG" style="width:200px;"><?php /*if($res_projects['title']!=""){echo $res_projects['title'];}else{?>&nbsp;<?php }?></div>
																		<div class="blkB" style="width:200px;"><a href=""><?php if($exp_creator[0]!=""){echo $exp_creator[0];}else{?>&nbsp;<?php }?></a></div>
																		<div class="blkB" style="width:100px;"><?php if($get_project_type['name']!=""){echo $get_project_type['name'];}else{?>&nbsp;<?php }?></div>
																		<div class="blkD">&nbsp;</div>
																		<div class="icon">&nbsp;</div>
																		<div class="icon"><a onclick="return confirmdelete('<?php echo $res_projects['title'];?>')" href="insertcommunityproject.php?delete=<?php echo $res_projects['id'];*/?>"><img src="images/profile/delete.png" /></a></div>
																	</div>-->
																	<?php
																}
															}
															
														}
													}
												}
											}
										}
										
										$all_artist_friend_project = array();
										$get_all_friends = $editprofile_artist->Get_all_friends();
										if($get_all_friends !="")
										{
											while($res_all_friends = mysql_fetch_assoc($get_all_friends))
											{
												$get_friend_art_info = $editprofile_artist->get_friend_art_info($res_all_friends['fgeneral_user_id']);
												$get_all_tag_pro = $editprofile_artist->get_all_tagged_pro($get_friend_art_info);
												if(mysql_num_rows($get_all_tag_pro)>0)
												{
													while($res_projects = mysql_fetch_assoc($get_all_tag_pro))
													{
														//$get_project_type = $editprofile_artist->Get_Project_type($res_projects['id']);
														$exp_creator = explode('(',$res_projects['creator']);
														$exp_tagged_email = explode(",",$res_projects['tagged_user_email']);
														for($exp_count = 0;$exp_count<count($exp_tagged_email);$exp_count++)
														{
															if($exp_tagged_email[$exp_count]==""){continue;}
															else{
																if($exp_tagged_email[$exp_count]==$_SESSION['login_email'])
																{
																	//$res_projects['project_type_name'] = $get_project_type['name'];
																	$res_projects['whos_project'] = "tag_pro_art";
																	$res_projects['id'] = $res_projects['id']."~art";
																	$all_artist_friend_project[] = $res_projects;
																	?>
																	<!--<div id="AccordionContainer" class="tableCont">
																		<div class="blkG" style="width:200px;"><?php /*if($res_projects['title']!=""){echo $res_projects['title'];}else{?>&nbsp;<?php }?></div>
																		<div class="blkB" style="width:200px;"><a href=""><?php if($exp_creator[0]!=""){echo $exp_creator[0];}else{?>&nbsp;<?php }?></a></div>
																		<div class="blkB" style="width:100px;"><?php if($get_project_type['name']!=""){echo $get_project_type['name'];}else{?>&nbsp;<?php }?></div>
																		<div class="blkD">&nbsp;</div>
																		<div class="icon">&nbsp;</div>
																		<div class="icon"><a onclick="return confirmdelete('<?php echo $res_projects['title'];?>')" href="insertartistproject.php?delete=<?php echo $res_projects['id'];*/?>"><img src="images/profile/delete.png" /></a></div>
																	</div>-->
																	<?php
																}
															}
															
														}
													}
												}
											}
										}
										
										
										$get_onlycre_id = array();
										$get_onlycre_id = $newgeneral->only_creator_this();
										
										$get_only2cre = array();
										$get_only2cre = $newgeneral->only_creator2pr_this();
										
										$get_proa_id = array();
										while($get_pros_ids = mysql_fetch_assoc($get_project))
										{
											$get_pros_ids['id'] = $get_pros_ids['id'].'~'.'com';
											$get_proa_id[] = $get_pros_ids;
										}
										//$project_avail = 0;
										$get_proc_id = array();
										while($get_proc_ids = mysql_fetch_assoc($get_cproject))
										{
											$get_proc_ids['id'] = $get_proc_ids['id'].'~'.'art';
											$get_proc_id[] = $get_proc_ids;
										}
										
										$sel_comtagged = array();
										if($project_avail!=0)
										{
											$acc_project = array_unique($acc_project);
											for($acc_pro=0;$acc_pro<count($acc_project);$acc_pro++)
											{
												if($acc_project[$acc_pro]!="" && $acc_project[$acc_pro]!=0)
												{
													$dum_com = $newgeneral->get_project_tagged_by_other_user($acc_project[$acc_pro]);
													if(!empty($dum_com) && $dum_com['id']!="" && $dum_com['id']!="")
													{
														$dum_com['id'] = $dum_com['id'].'~'.'com';
														$sel_comtagged[] = $dum_com;
													}
												}
											}
										}
										
										$sel_arttagged = array();
										if($project_aavail!=0)
										{
											$acc_aproject = array_unique($acc_aproject);
											for($acc_pro=0;$acc_pro<count($acc_aproject);$acc_pro++)
											{
												if($acc_aproject[$acc_pro]!="" && $acc_aproject[$acc_pro]!=0)
												{
													$dum_art = $newgeneral->get_aproject_tagged_by_other_user($acc_aproject[$acc_pro]);
													if(!empty($dum_art) && $dum_art['id']!="" && $dum_art['id']!="")
													{
														$dum_art['id'] = $dum_art['id'].'~'.'art';
														$sel_arttagged[] = $dum_art;
													}
												}
											}
										}
										
										$sel_com2tagged = array();
										if($project_avail!=0)
										{
											$acc_project = array_unique($acc_project);
											for($acc_pro=0;$acc_pro<count($acc_project);$acc_pro++)
											{
												if($acc_project[$acc_pro]!="" && $acc_project[$acc_pro]!=0)
												{
													$sql_1_cre = $newgeneral->get_project_tagged_by_other2_user($acc_project[$acc_pro]);
													if($sql_1_cre!="")
													{
														if(mysql_num_rows($sql_1_cre)>0)
														{
															while($run = mysql_fetch_assoc($sql_1_cre))
															{
																$run['id'] = $run['id'].'~'.'art';
																$sel_com2tagged[] = $run;
															}
														}											
													}
													
													$sql_2_cre = $newgeneral->get_project_atagged_by_other2_user($acc_project[$acc_pro]);
													if($sql_2_cre!="")
													{
														if(mysql_num_rows($sql_2_cre)>0)
														{
															while($run_c = mysql_fetch_assoc($sql_2_cre))
															{
																$run_c['id'] = $run_c['id'].'~'.'com';
																$sel_com2tagged[] = $run_c;
															}
														}										
													}
												}
											}
										}
										
										$sel_art2tagged = array();
										if($project_aavail!=0)
										{
											$acc_aproject = array_unique($acc_aproject);
											for($acc_pro=0;$acc_pro<count($acc_aproject);$acc_pro++)
											{
												if($acc_aproject[$acc_pro]!="" && $acc_aproject[$acc_pro]!=0)
												{
													$sql_1_cre = $newgeneral->get_aproject_tagged_by_other2_user($acc_aproject[$acc_pro]);
													if($sql_1_cre!="")
													{
														if(mysql_num_rows($sql_1_cre)>0)
														{
															while($run = mysql_fetch_assoc($sql_1_cre))
															{
																$run['id'] = $run['id'].'~'.'art';
																$sel_art2tagged[] = $run;
															}
														}
													}
													
													$sql_2_cre = $newgeneral->get_aproject_atagged_by_other2_user($acc_aproject[$acc_pro]);
													if($sql_2_cre!="")
													{
														if(mysql_num_rows($sql_2_cre)>0)
														{
															while($run_c = mysql_fetch_assoc($sql_2_cre))
															{
																$run_c['id'] = $run_c['id'].'~'.'com';
																$sel_art2tagged[] = $run_c;
															}
														}
													}
												}
											}
										}
										
										$final_arrs = array_merge($get_proa_id,$get_proc_id,$sel_arttagged,$sel_comtagged,$get_onlycre_id,$get_only2cre,$sel_art2tagged,$sel_com2tagged,$all_artist_friend_project,$all_community_friend_project);
										
										$sort = array();
										foreach($final_arrs as $k=>$v) {
										
											$end_c1 = $v['creator'];
											//$create_c = strpos($v['creator'],'(');
											//$end_c1 = substr($v['creator'],0,$create_c);
											$end_c = trim($end_c1);
											
											$sort['creator'][$k] = strtolower($end_c);
											//$sort['from'][$k] = $end_f;
											//$sort['track'][$k] = $v['track'];
											$sort['title'][$k] = strtolower($v['title']);
										}
										//var_dump($sort);
										if(!empty($sort))
										{
											array_multisort($sort['creator'], SORT_ASC, $sort['title'], SORT_ASC,$final_arrs);
										}
										
										$dummy_pr = array();
										$get_all_del_id = $newgeneral->get_deleted_project_id();
										$exp_del_id = explode(",",$get_all_del_id['deleted_project_id']);
										for($count_all=0;$count_all<count($final_arrs);$count_all++){
											for($count_del=0;$count_del<count($exp_del_id);$count_del++){
												if($exp_del_id[$count_del]!=""){
													if($final_arrs[$count_all]['id'] == $exp_del_id[$count_del]){
														$final_arrs[$count_all] ="";
													}
												}
											}
										}
										
										if(!empty($final_arrs))
										{
				?>
											<select title="Select media and profiles to display on your profile page." name="tagged_projects[]" size="5" multiple="multiple" id="tagged_projects[]" class="list">
				<?php
										for($f_i=0;$f_i<count($final_arrs);$f_i++)
										{
											if($final_arrs[$f_i]!="")
											{
												if(in_array($final_arrs[$f_i]['id'],$dummy_pr))
												{}
												else
												{
													$dummy_pr[] = $final_arrs[$f_i]['id'];
													
													//$create_c = strpos($final_arrs[$f_i]['creator'],'(');
													//$end_c = substr($final_arrs[$f_i]['creator'],0,$create_c);
													$end_c = $final_arrs[$f_i]['creator'];
													
													$ecei = "";
													if($end_c!="")
													{
														$ecei = $end_c;
														$ecei .= ' : '.$final_arrs[$f_i]['title'];
													}
													else
													{
														$ecei = $final_arrs[$f_i]['title'];
													}
													
													
													if($get_select_projects[0]!="")
													{
														$get_select_projects = array_unique($get_select_projects);
														for($i=0;$i<count($get_select_projects);$i++)
														{
															if($get_select_projects[$i]==$final_arrs[$f_i]['id'])
															{
																$j_chan=1;
													?>
																<option value="<?php echo $final_arrs[$f_i]['id'];?>" selected ><?php echo stripslashes($ecei);?></option>
													<?php
																continue;
															}
														}
														if($j_chan==1)
														{
															$j_chan=0;
															continue;
														}
													}
												?>
													<option value="<?php echo $final_arrs[$f_i]['id'];?>" ><?php echo stripslashes($ecei);?></option>
												<?php
												}
											}
										}
										
										/* if($project_avail!=0)
										{
											for($acc_pro=1;$acc_pro<count($acc_project);$acc_pro++)
											{
													//$sel_tagged="";
												$sel_tagged=$newgeneral->get_project_tagged_by_other_user($acc_project[$acc_pro]);
												
												if(isset($sel_tagged) && ($sel_tagged['title']!="" || $sel_tagged['title']!=null))
												{
													if($get_select_projects[0]!="")
													{
														for($i=0;$i<count($get_select_projects);$i++)
														{
															if($get_select_projects[$i]==$sel_tagged['id'])
															{
																$j_chan=1;
													?>
														<option value="<?php echo $sel_tagged['id'];?>" selected ><?php echo $sel_tagged['title'];?></option>
													<?php	
																continue;
															}
														}
													
														if($j_chan==1)
														{
															$j_chan=0;
															continue;
														}
													}
												?>
													<option value="<?php echo $sel_tagged['id'];?>" ><?php echo $sel_tagged['title'];?></option>
												<?php
												}
											}
										} */
										
										
										
										//if(mysql_num_rows($get_project)<=0 && $project_avail==0 && mysql_num_rows($get_cproject)<=0 && $project_aavail==0)
			?>
											</select>
			<?php
										}
										
										if(empty($final_arrs))
										{
										?>
											<a style="border-radius:5px; -webkit-border-radius:5px; -moz-border-radius:5px; -ms-border-radius:5px; -o-border-radius:5px; background-color: #000000; border: 0 none; color: #FFFFFF; font-size: 13px; margin-left: 5px; padding: 6px 8px; text-decoration: none;" href="add_community_project.php">Add Project</a>
										<?php	
										}
										?>
                                    
                                </div>
                                <div class="fieldCont">
                                  <div title="Select media and profiles to display on your profile page." class="fieldTitle">Galleries</div>
                                   
                                         <?php
										/* if(isset($res_gal) && $res_gal!=null)
											{
												//$get_community_gallery_name=$newgeneral->get_community_gallery_name_at_register();
												for($rg=0;$rg<count($get_select_galleries);$rg++)
												{
													if($get_select_galleries[$rg]==$res_gal['gallery_id'])
													{
														$reg_gal=1;
												?>
													<option value="<?php echo $res_gal['gallery_id'];?>" selected ><?php echo substr($newres1['fname'].' '.$newres1['lname'],0,20) ." : ". $res_gal['gallery_title'];?></option>
												<?php
														continue;
													}
												}
												if($reg_gal==1)	
												{
													$reg_gal=0;
												}
												else
												{
												?>
													<option value="<?php echo $res_gal['gallery_id'];?>" ><?php echo substr($newres1['fname'].' '.$newres1['lname'],0,20) ." : ". $res_gal['gallery_title'];?></option>
												<?php
												}
											} */
										$get_gallery_id = array();
										$get_community_gallery_id=$newgeneral->get_community_gallery_id();
										while($get_gallery_ids = mysql_fetch_assoc($get_community_gallery_id))
										{
											$get_gallery_id[] = $get_gallery_ids;
										}
										
										 $get_media_tag_art_pro = $newgeneral->get_artist_project_tag($_SESSION['login_email']);
										if(mysql_num_rows($get_media_tag_art_pro)>0)
										{
											$is_g = 0;
											while($art_pro_tag = mysql_fetch_assoc($get_media_tag_art_pro))
											{
												$get_gallery = explode(",",$art_pro_tag['tagged_galleries']);
												for($count_art_t1_song=0;$count_art_t1_song<count($get_gallery);$count_art_t1_song++)
												{
													if($get_gallery[$count_art_t1_song]!="")
													{
														$get_gal_new[$is_g] = $get_gallery[$count_art_t1_song];
														$is_g = $is_g + 1;
													}
												}
											}
											if(!empty($get_gal_new))
											{
												$get_gal_n = array_unique($get_gal_new);
											}
										}
										
										$get_media_tag_art_eve = $newgeneral->get_artist_event_tag($_SESSION['login_email']);
										if(mysql_num_rows($get_media_tag_art_eve)>0)
										{
											$is_g = 0;
											while($art_pro_tag = mysql_fetch_assoc($get_media_tag_art_eve))
											{
												$get_gallery = explode(",",$art_pro_tag['tagged_galleries']);
												for($count_art_t1_song=0;$count_art_t1_song<count($get_gallery);$count_art_t1_song++)
												{
													if($get_gallery[$count_art_t1_song]!="")
													{
														$get_gal_newe[$is_g] = $get_gallery[$count_art_t1_song];
														$is_g = $is_g + 1;
													}
												}
											}
											if(!empty($get_gal_newe))
											{
												$get_gal_en = array_unique($get_gal_newe);
											}
										}
										
										$get_media_tag_com_pro = $newgeneral->get_community_project_tag($_SESSION['login_email']);
										if(mysql_num_rows($get_media_tag_com_pro)>0)
										{
											$is_gc = 0;
											while($com_pro_tag = mysql_fetch_assoc($get_media_tag_com_pro))
											{
												$get_com_gallery = explode(",",$com_pro_tag['tagged_galleries']);
												for($cou1_com_tag_song=0;$cou1_com_tag_song<count($get_com_gallery);$cou1_com_tag_song++)
												{
													if($get_com_gallery[$cou1_com_tag_song]!="")
													{
														$get_gal_new_c[$is_gc] = $get_com_gallery[$cou1_com_tag_song];
														$is_gc = $is_gc + 1;
													}
												}
											}
											if(!empty($get_gal_new_c))
											{
												$get_gal_n_c = array_unique($get_gal_new_c);
											}
										}
										
										$get_media_tag_com_eve = $newgeneral->get_community_event_tag($_SESSION['login_email']);
										if(mysql_num_rows($get_media_tag_com_eve)>0)
										{
											$is_gc = 0;
											while($com_pro_tag = mysql_fetch_assoc($get_media_tag_com_eve))
											{
												$get_com_gallery = explode(",",$com_pro_tag['tagged_galleries']);
												for($cou1_com_tag_song=0;$cou1_com_tag_song<count($get_com_gallery);$cou1_com_tag_song++)
												{
													if($get_com_gallery[$cou1_com_tag_song]!="")
													{
														$get_gal_neew_ec[$is_gc] = $get_com_gallery[$cou1_com_tag_song];
														$is_gc = $is_gc + 1;
													}
												}
											}
											if(!empty($get_gal_neew_ec))
											{
												$get_gale_n_c = array_unique($get_gal_neew_ec);
											}
										}
										
										$get_media_cre_art_pro = $newgeneral->get_project_create();
										
										if(!empty($get_media_cre_art_pro))
										{
											$is_gp = 0;
											//while($art_pro_tag = mysql_fetch_assoc($get_media_cre_art_pro))
											for($cra_i=0;$cra_i<count($get_media_cre_art_pro);$cra_i++)
											{
												$get_cgal = explode(",",$get_media_cre_art_pro[$cra_i]['tagged_galleries']);
												for($count_art_cr_g=0;$count_art_cr_g<count($get_cgal);$count_art_cr_g++)
												{
													if($get_cgal[$count_art_cr_g]!="")
													{
														$get_gal_pn[$is_gp] = $get_cgal[$count_art_cr_g];
														$is_gp = $is_gp + 1;
													}
												}
											}
											if(!empty($get_gal_pn))
											{
												$get_gal_p_new = array_unique($get_gal_pn);
											}
										} 
										
										if(!isset($get_gal_n))
										{
											$get_gal_n = array();
										}
										
										if(!isset($get_gale_n_c))
										{
											$get_gale_n_c = array();
										}
										
										if(!isset($get_gal_en))
										{
											$get_gal_en = array();
										}
										
										if(!isset($get_gal_n_c))
										{
											$get_gal_n_c = array();
										}
										
										if(!isset($get_gal_p_new))
										{
											$get_gal_p_new = array();
										}
										
										/*$find_creator_heis = $newgeneral -> get_creator_heis();
										$get_genral_user_info = $newgeneral -> selgeneral();
										$where_creator = array();
										if(!empty($find_creator_heis)){
											for($new_cre_med=0;$new_cre_med<=count($find_creator_heis);$new_cre_med++)
											{
												if($find_creator_heis[$new_cre_med]['general_user_id']!= $get_genral_user_info['general_user_id']){
													if($find_creator_heis[$new_cre_med]['media_type']==113){
													$where_creator[] = $find_creator_heis[$new_cre_med];}
												}
											}
										}
										
										$find_tag_heis = $newgeneral -> get_tag_where_heis($_SESSION['login_email']);
										$get_genral_user_info = $newgeneral -> selgeneral();
										$where_tagged = array();
										if(!empty($find_tag_heis)){
											for($new_cre_med=0;$new_cre_med<=count($find_tag_heis);$new_cre_med++)
											{
												if($find_tag_heis[$new_cre_med]['general_user_id']!= $get_genral_user_info['general_user_id']){
													if($find_tag_heis[$new_cre_med]['media_type']==113){
													$where_tagged[] = $find_tag_heis[$new_cre_med];}
												}
											}
										}*/
										/*This function will display the user in which profile he is creator and display media where that profile was creator*/
										$new_medi = $newgeneral->get_creator_heis();
										$new_creator_creator_media_array = array();
										if(!empty($new_medi))
										{
											for($count_cre_m=0;$count_cre_m<count($new_medi);$count_cre_m++)
											{
												if($new_medi[$count_cre_m]['media_type']==113){
													$new_creator_creator_media_array[] = $new_medi[$count_cre_m];
												}
											}
										}
										//var_dump($new_creator_creator_media_array);
										/*This function will display the user in which profile he is creator and display media where that profile was creator Ends here*/
										
										/*This function will display the user in which profile he is from and display media where that profile was creator*/
										$new_medi_from = $newgeneral->get_from_heis();
										$new_from_from_media_array = array();
										if(!empty($new_medi_from))
										{
											for($count_cre_m=0;$count_cre_m<count($new_medi_from);$count_cre_m++)
											{
												if($new_medi_from[$count_cre_m]['media_type']==113){
												$new_from_from_media_array[] = $new_medi_from[$count_cre_m];
												}
											}
										}
										//var_dump($new_creator_creator_media_array);
										/*This function will display the user in which profile he is from and display media where that profile was creator Ends here*/
										
										
										/*This function will display the user in which profile he is from or creator and tagged and display media where that profile was creator*/
										$new_medi_tagged = $newgeneral->get_the_user_tagin_project($_SESSION['login_email']);
										$new_tagged_from_media_array = array();
										if(!empty($new_medi_tagged))
										{
											for($count_cre_m=0;$count_cre_m<count($new_medi_tagged);$count_cre_m++)
											{
												if($new_medi_tagged[$count_cre_m]['media_type']==113){
												$new_tagged_from_media_array[] = $new_medi_tagged[$count_cre_m];
												}
											}
										}
										//var_dump($new_creator_creator_media_array);
										/*This function will display the user in which profile he is from or creator and tagged and display media where that profile was creator Ends here*/
										
										
										/*This function will display in which media the curent user was creator*/
										$get_where_creator_media = $newgeneral->get_media_where_creator_or_from();
										$new_creator_media_array = array();
										if(!empty($get_where_creator_media))
										{
											for($count_tag_m=0;$count_tag_m<count($get_where_creator_media);$count_tag_m++)
											{
												if($get_where_creator_media[$count_tag_m]['media_type']==113){
													$new_creator_media_array[] = $get_where_creator_media[$count_tag_m];
												}
											}
										}
										/*This function will display in which media the curent user was creator ends here*/
										
										$get_gals_pngew_w = array_merge($get_gal_n,$get_gal_n_c,$get_gal_p_new,$get_gal_en,$get_gale_n_c);
										
										$get_gals_pnew_g = array_unique($get_gals_pngew_w);
										$get_gals_final_ct = array();
										
										for($count_art_cr_gs=0;$count_art_cr_gs<count($get_gals_pnew_g);$count_art_cr_gs++)
										{
											if($get_gals_pnew_g[$count_art_cr_gs]!="")
											{
												$get_media_tag_g_art = $newgeneral->get_tag_media_info($get_gals_pnew_g[$count_art_cr_gs]);
												$get_gals_final_ct[] = $get_media_tag_g_art;
											}
										}
										
										$acc_medsg = array();
										for($acc_vis=0;$acc_vis<count($acc_media);$acc_vis++)
										{
											if($acc_media[$acc_vis]!="")
											{
												$segl_tagged = $newgeneral->get_community_media_type_tagged_by_other_user($acc_media[$acc_vis]);
												if(!empty($segl_tagged))
												{
													$acc_medsg[] = $segl_tagged;
												}
											}
										}
										$get_gals_final_ct = array();
										$type = '113';
										$cre_frm_med = array();
										$cre_frm_med = $newgeneral->get_media_create($type);
										
										$get_vgals_pnew_w = array_merge($acc_medsg,$get_gallery_id,$get_gals_final_ct,$cre_frm_med,$new_creator_creator_media_array,$new_from_from_media_array,$new_tagged_from_media_array,$new_creator_media_array);
										
										$sort = array();
										foreach($get_vgals_pnew_w as $k=>$v) {
											
											$end_c1 = $v['creator'];
											//$create_c = strpos($v['creator'],'(');
											//$end_c1 = substr($v['creator'],0,$create_c);
											$end_c = trim($end_c1);
											
											$end_f1 = $v['from'];
											//$create_f = strpos($v['from'],'(');
											//$end_f1 = substr($v['from'],0,$create_f);
											$end_f = trim($end_f1);
											
											$sort['creator'][$k] = strtolower($end_c);
											$sort['from'][$k] = strtolower($end_f);
											//$sort['track'][$k] = $v['track'];
											$sort['title'][$k] = strtolower($v['title']);
										}
										//var_dump($sort);
										//$sort = array_unique($sort['id']);
										if(!empty($sort))
										{
											array_multisort($sort['creator'], SORT_ASC, $sort['from'], SORT_ASC, $sort['title'], SORT_ASC,$get_vgals_pnew_w);									
										}
										
										if(!empty($get_vgals_pnew_w))
										{
									?>
											<select title="Select media and profiles to display on your profile page." name="tagged_gallery[]" size="5" multiple="multiple" id="tagged_gallery[]" class="list">
									<?php
										$dummy_g = array();
										$get_all_del_id = $newgeneral->get_all_del_media_id();
										$deleted_id = explode(",",$get_all_del_id);
										for($count_del=0;$count_del<=count($get_vgals_pnew_w);$count_del++)
										{
											if($get_vgals_pnew_w[$count_del]==""){continue;}
											else{
												for($count_deleted=0;$count_deleted<=count($deleted_id);$count_deleted++)
												{
													if($deleted_id[$count_deleted]==""){continue;}
													else{
														if($deleted_id[$count_deleted] == $get_vgals_pnew_w[$count_del]['id'])
														{
															$get_vgals_pnew_w[$count_del]="";
														}
													}
												}
											}
										}
										for($acc_glae=0;$acc_glae<count($get_vgals_pnew_w);$acc_glae++)
										{
											if($get_vgals_pnew_w[$acc_glae]['delete_status']==0)
											{
												if(isset($get_vgals_pnew_w[$acc_glae]['id']))
												{
													if(in_array($get_vgals_pnew_w[$acc_glae]['id'],$dummy_g))
													{
														
													}
													else
													{
														$dummy_g[] = $get_vgals_pnew_w[$acc_glae]['id'];
														if($get_vgals_pnew_w[$acc_glae]['id']!="" && $get_vgals_pnew_w[$acc_glae]['media_type']==113)
														{
															//$create_c = strpos($get_vgals_pnew_w[$acc_glae]['creator'],'(');
															//$end_c = substr($get_vgals_pnew_w[$acc_glae]['creator'],0,$create_c);
															$end_c = $get_vgals_pnew_w[$acc_glae]['creator'];
															$end_f = $get_vgals_pnew_w[$acc_glae]['from'];
															//$create_f = strpos($get_vgals_pnew_w[$acc_glae]['from'],'(');
															//$end_f = substr($get_vgals_pnew_w[$acc_glae]['from'],0,$create_f);
															
															$eceksg = "";
															if($end_c!="")
															{
																$eceksg = $end_c;
															}
															if($end_f!="")
															{
																if($end_c!="")
																{
																	$eceksg .= " : ".$end_f;
																}
																else
																{
																	$eceksg .= $end_f;
																}
															}
															if($get_vgals_pnew_w[$acc_glae]['title']!="")
															{
																if($end_c!="" || $end_f!="")
																{
																	$eceksg .= " : ".$get_vgals_pnew_w[$acc_glae]['title'];
																}
																else
																{
																	$eceksg .= $get_vgals_pnew_w[$acc_glae]['title'];
																}
															}
															
															if($get_select_galleries!="")
															{
																$get_select_galleries = array_unique($get_select_galleries);
																$yes_com_tag_pro_song =0;
																for($count_sel=0;$count_sel<=count($get_select_galleries);$count_sel++)
																{
																	if($get_select_galleries[$count_sel]==""){continue;}
																	else
																	{
																		if($get_select_galleries[$count_sel] == $get_vgals_pnew_w[$acc_glae]['id'])
																		{
																			$yes_com_tag_pro_song = 1;
											?>
																			<option value="<?php echo $get_vgals_pnew_w[$acc_glae]['id'];?>" selected><?php echo stripslashes($eceksg); ?></option>
											<?php
																		}
																	}
																}
																if($yes_com_tag_pro_song == 0)
																{													
											?>
																	<option value="<?php echo $get_vgals_pnew_w[$acc_glae]['id'];?>"><?php echo stripslashes($eceksg); ?></option>
											<?php
																}
															}
															else
															{
				?>
																<option value="<?php echo $get_vgals_pnew_w[$acc_glae]['id'];?>" ><?php echo stripslashes($eceksg); ?></option>
				<?php
															}
														}
													}
												}
											}
										}
												
										/* for($count_art_cr_g=0;$count_art_cr_g<count($get_gals_pnew);$count_art_cr_g++)
										{
											if($get_gals_pnew[$count_art_cr_g]!=""){
											
											$art_coms_g = '';
											if($get_media_cre_art_pro[$cra_i]['artist_id']!=0 && $get_media_cre_art_pro[$cra_i]['artist_id']!="")
											{
												$art_coms_g = 'artist~'.$get_media_cre_art_pro[$cra_i]['artist_id'];
											}
											elseif($get_media_cre_art_pro[$cra_i]['community_id']!=0 && $get_media_cre_art_pro[$cra_i]['community_id']!="")
											{
												$art_coms_g = 'community~'.$get_media_cre_art_pro[$cra_i]['community_id'];
											}
											$reg_meds = $newgeneral->get_media_reg_info($get_gals_pnew[$count_art_cr_g],$art_coms_g);
											if($reg_meds!=NULL && $reg_meds!='')
											{
												if($reg_meds=='general_artist_gallery')
												{
													$sql_a = mysql_query("SELECT * FROM general_artist_gallery_list WHERE gallery_id='".$get_gals_pnew[$count_art_cr_g]."'");
													if(mysql_num_rows($sql_a)>0)
													{
														$ans_a = mysql_fetch_assoc($sql_a);
														$sql_gens = $newgeneral->sel_general_aid($get_media_cre_art_pro[$cra_i]['artist_id']);
														
														if($get_select_galleries!="")
														{
															//$get_sel_song =$newgeneral->get_sel_songs($_GET['edit']);
															//$exp_song_id = explode(",",$get_sel_song['songs']);
															$yes_com_tag_pro_song =0;
															for($count_sela=0;$count_sela<=count($get_select_galleries);$count_sela++)
															{
																if($get_select_galleries[$count_sela]==""){continue;}
																else
																{
																	if($get_select_galleries[$count_sela] == $ans_a['gallery_id'])
																	{
																		$yes_com_tag_pro_song = 1;
					?>															
																		<option value="<?php echo $ans_a['gallery_id'];?>" selected><?php echo substr($sql_gens['fname'] ." ". $sql_gens['lname'],0,20) ." : ".  $ans_a['gallery_title'];?></option>
					<?php
																	}
																}
															}
															if($yes_com_tag_pro_song == 0)
															{												
					?>
																<option value="<?php echo $ans_a['gallery_id'];?>"><?php echo substr($sql_gens['fname'] ." ". $sql_gens['lname'],0,20) ." : ".  $ans_a['gallery_title'];?></option>
					<?php
															}
														}
														else
														{
					?>
															<option value="<?php echo $ans_a['gallery_id'];?>"><?php echo substr($sql_gens['fname'] ." ". $sql_gens['lname'],0,20) ." : ".  $ans_a['gallery_title'];?></option>
					<?php
														}
													}
												}
												if($reg_meds=='general_community_gallery')
												{
													$sql_a = mysql_query("SELECT * FROM general_community_gallery_list WHERE gallery_id='".$get_gals_pnew[$count_art_cr_g]."'");
													if(mysql_num_rows($sql_a)>0)
													{
														$ans_a = mysql_fetch_assoc($sql_a);
														$sql_gens = $newgeneral->sel_general_cid($get_media_cre_art_pro[$cra_i]['community_id']);
														
														if($get_select_galleries!="")
														{
															//$get_sel_song =$newgeneral->get_sel_songs($_GET['edit']);
															//$exp_song_id = explode(",",$get_sel_song['songs']);
															$yes_com_tag_pro_song =0;
															for($count_selc=0;$count_selc<=count($get_select_galleries);$count_selc++)
															{
																if($get_select_galleries[$count_selc]==""){continue;}
																else
																{
																	if($get_select_galleries[$count_selc] == $ans_a['gallery_id'])
																	{
																		$yes_com_tag_pro_song = 1;
						?>															
																		<option value="<?php echo $ans_a['gallery_id'];?>" selected><?php echo substr($sql_gens['fname'] ." ". $sql_gens['lname'],0,20) ." : ".  $ans_a['gallery_title'];?></option>
						<?php
																	}
																}
															}
															if($yes_com_tag_pro_song == 0)
															{
						?>
																<option value="<?php echo $ans_a['gallery_id'];?>"><?php echo substr($sql_gens['fname'] ." ". $sql_gens['lname'],0,20) ." : ".  $ans_a['gallery_title'];?></option>
						<?php
															}
														}
														else
														{
						?>
															<option value="<?php echo $ans_a['gallery_id'];?>"><?php echo substr($sql_gens['fname'] ." ". $sql_gens['lname'],0,20) ." : ".  $ans_a['gallery_title'];?></option>
						<?php
														}
													}
												}
											}
											else
											{
												$get_media_tag_art_pro_song = $newgeneral->get_tag_media_info($get_gals_pnew[$count_art_cr_g]);
														
												if($get_select_galleries!="")
												{
													//$get_sel_song =$newgeneral->get_sel_songs($_GET['edit']);
													//$exp_song_id = explode(",",$get_sel_song['songs']);
													$yes_com_tag_pro_song =0;
													for($count_sel=0;$count_sel<=count($get_select_galleries);$count_sel++)
													{
														if($get_select_galleries[$count_sel]==""){continue;}
														else
														{
															if($get_select_galleries[$count_sel] == $get_media_tag_art_pro_song['id'])
															{
																$yes_com_tag_pro_song = 1;
						?>
																<option value="<?php echo $get_media_tag_art_pro_song['id'];?>" selected><?php echo substr($get_media_tag_art_pro_song['creator'],0,20) ." : ". substr($get_media_tag_art_pro_song['from'],0,20) ." : ".  $get_media_tag_art_pro_song['title'];?></option>
						<?php
															}
														}
													}
													if($yes_com_tag_pro_song == 0)
													{
						?>
														<option value="<?php echo $get_media_tag_art_pro_song['id'];?>"><?php echo substr($get_media_tag_art_pro_song['creator'],0,20) ." : ". substr($get_media_tag_art_pro_song['from'],0,20) ." : ".  $get_media_tag_art_pro_song['title'];?></option>
						<?php
													}
												}
												else
												{
						?>
													<option value="<?php echo $get_media_tag_art_pro_song['id'];?>"><?php echo substr($get_media_tag_art_pro_song['creator'],0,20) ." : ". substr($get_media_tag_art_pro_song['from'],0,20) ." : ".  $get_media_tag_art_pro_song['title'];?></option>
						<?php
												}
											}
										}
									} */
										
										//if(mysql_num_rows($get_community_gallery_id)<=0 && $media_gal=="" && $res_gal==null && count($get_media_cre_art_pro)<=0 && mysql_num_rows($get_media_tag_com_pro)<=0 && mysql_num_rows($get_media_tag_art_pro)<=0)
									?>
											</select>
									<?php
										}
										if(empty($get_vgals_pnew_w))
										{
										?>
											<a style="border-radius:5px; -webkit-border-radius:5px; -moz-border-radius:5px; -ms-border-radius:5px; -o-border-radius:5px; background-color: #000000; border: 0 none; color: #FFFFFF; font-size: 13px; margin-left: 5px; padding: 6px 8px; text-decoration: none;" href="add_media.php?adds=113">Add Gallery</a>
										<?php	
										}
										
										?>
                                    
                                </div>
                                <div class="fieldCont">
                                  <div title="Select media and profiles to display on your profile page." class="fieldTitle">Songs</div>
                                        <?php
											/* if(isset($res_song) && $res_song!=null)
											{
												for($i=0;$i<count($get_select_songs);$i++)
												{
													if($get_select_songs[$i]==$res_song['audio_id'])
													{
												?>
													<option value="<?php echo $res_song['audio_id'];?>" selected ><?php echo substr($newres1['fname'].' '.$newres1['lname'],0,20) ." : ". $res_song['audio_name'];?></option>
												<?php	
													}
													else
															{
															 ?>
															 <option value="<?php echo $res_song['audio_id'];?>" ><?php echo substr($newres1['fname'].' '.$newres1['lname'],0,20) ." : ". $res_song['audio_name'];?></option>
															 <?php 
															}
												}
											} */
										$get_song_id = array();
										$get_community_song_id=$newgeneral->get_community_song_id();
										while($get_song_ids = mysql_fetch_assoc($get_community_song_id))
										{
											$get_song_id[] = $get_song_ids;
										}
											
										$get_media_tag_art_pro = $newgeneral->get_artist_project_tag($_SESSION['login_email']);
										if(mysql_num_rows($get_media_tag_art_pro)>0)
										{
											$is = 0;
											while($art_pro_tag = mysql_fetch_assoc($get_media_tag_art_pro))
											{
												$get_songs = explode(",",$art_pro_tag['tagged_songs']);
												for($count_art_tag_song=0;$count_art_tag_song<count($get_songs);$count_art_tag_song++)
												{
													if($get_songs[$count_art_tag_song]!="")
													{
														$get_songs_new[$is] = $get_songs[$count_art_tag_song];
														$is = $is + 1;
													}
												}
											}
											if(!empty($get_songs_new))
											{
												$get_songs_n = array_unique($get_songs_new);
											}
										}
										
										$get_media_tag_art_eve = $newgeneral->get_artist_event_tag($_SESSION['login_email']);
										if(mysql_num_rows($get_media_tag_art_eve)>0)
										{
											$is = 0;
											while($art_pro_tag = mysql_fetch_assoc($get_media_tag_art_eve))
											{
												$get_songs = explode(",",$art_pro_tag['tagged_songs']);
												for($count_art_tag_song=0;$count_art_tag_song<count($get_songs);$count_art_tag_song++)
												{
													if($get_songs[$count_art_tag_song]!="")
													{
														$get_songs_newe[$is] = $get_songs[$count_art_tag_song];
														$is = $is + 1;
													}
												}
											}
											if(!empty($get_songs_newe))
											{
												$get_songs_edn = array_unique($get_songs_newe);
											}
										}
										
										$get_media_tag_com_pro = $newgeneral->get_community_project_tag($_SESSION['login_email']);
										if(mysql_num_rows($get_media_tag_com_pro)>0)
										{
											$is_c = 0;
											while($com_pro_tag = mysql_fetch_assoc($get_media_tag_com_pro))
											{
												$get_com_songs = explode(",",$com_pro_tag['tagged_songs']);
												for($count_com_tag_song=0;$count_com_tag_song<count($get_com_songs);$count_com_tag_song++)
												{
													if($get_com_songs[$count_com_tag_song]!="")
													{
														$get_songs_new_c[$is_c] = $get_com_songs[$count_com_tag_song];
														$is_c = $is_c + 1;
													}
												}
											}
											if(!empty($get_songs_new_c))
											{
												$get_songs_n_c = array_unique($get_songs_new_c);
											}
										}
										
										$get_media_tag_com_eve = $newgeneral->get_community_event_tag($_SESSION['login_email']);
										if(mysql_num_rows($get_media_tag_com_eve)>0)
										{
											$is_c = 0;
											while($com_pro_tag = mysql_fetch_assoc($get_media_tag_com_eve))
											{
												$get_com_songs = explode(",",$com_pro_tag['tagged_songs']);
												for($count_com_tag_song=0;$count_com_tag_song<count($get_com_songs);$count_com_tag_song++)
												{
													if($get_com_songs[$count_com_tag_song]!="")
													{
														$get_sonegs_new_c[$is_c] = $get_com_songs[$count_com_tag_song];
														$is_c = $is_c + 1;
													}
												}
											}
											if(!empty($get_sonegs_new_c))
											{
												$get_songs_en_ec = array_unique($get_sonegs_new_c);
											}
										}
										
										$get_media_cre_art_pro = $newgeneral->get_project_create();
										if(!empty($get_media_cre_art_pro))
										{
											//while($art_pro_tag = mysql_fetch_assoc($get_media_cre_art_pro))
											$is_p = 0;
											for($cr_i=0;$cr_i<count($get_media_cre_art_pro);$cr_i++)
											{
												$get_songs = explode(",",$get_media_cre_art_pro[$cr_i]['tagged_songs']);
												for($count_art_cr_song=0;$count_art_cr_song<count($get_songs);$count_art_cr_song++)
												{
													if($get_songs[$count_art_cr_song]!="")
													{
														$get_songs_pn[$is_p] = $get_songs[$count_art_cr_song];
														$is_p = $is_p + 1;
													}
												}
											}	
											if(!empty($get_songs_pn))
											{
												$get_songs_p_new = array_unique($get_songs_pn);
											}
										}	
										
										if(!isset($get_songs_n))
										{
											$get_songs_n = array();
										}
										
										if(!isset($get_songs_en_ec))
										{
											$get_songs_en_ec = array();
										}
										
										if(!isset($get_songs_edn))
										{
											$get_songs_edn = array();
										}
										
										if(!isset($get_songs_n_c))
										{
											$get_songs_n_c = array();
										}
										
										if(!isset($get_songs_p_new))
										{
											$get_songs_p_new = array();
										}
										
										/*$find_creator_heis = $newgeneral -> get_creator_heis();
										$get_genral_user_info = $newgeneral -> selgeneral();
										$where_creator = array();
										if(!empty($find_creator_heis)){
											for($new_cre_med=0;$new_cre_med<=count($find_creator_heis);$new_cre_med++)
											{
												if($find_creator_heis[$new_cre_med]['general_user_id']!= $get_genral_user_info['general_user_id']){
													if($find_creator_heis[$new_cre_med]['media_type']==114){
													$where_creator[] = $find_creator_heis[$new_cre_med];}
												}
											}
										}
										
										$find_tag_heis = $newgeneral -> get_tag_where_heis($_SESSION['login_email']);
										$get_genral_user_info = $newgeneral -> selgeneral();
										$where_tagged = array();
										if(!empty($find_tag_heis)){
											for($new_cre_med=0;$new_cre_med<=count($find_tag_heis);$new_cre_med++)
											{
												if($find_tag_heis[$new_cre_med]['general_user_id']!= $get_genral_user_info['general_user_id']){
													if($find_tag_heis[$new_cre_med]['media_type']==114){
													$where_tagged[] = $find_tag_heis[$new_cre_med];}
												}
											}
										}*/
										
										/*This function will display the user in which profile he is creator and display media where that profile was creator*/
										$new_medi = $newgeneral->get_creator_heis();
										$new_creator_creator_media_array = array();
										if(!empty($new_medi))
										{
											for($count_cre_m=0;$count_cre_m<count($new_medi);$count_cre_m++)
											{
												if($new_medi[$count_cre_m]['media_type']==114){
													$new_creator_creator_media_array[] = $new_medi[$count_cre_m];
												}
											}
										}
										//var_dump($new_creator_creator_media_array);
										/*This function will display the user in which profile he is creator and display media where that profile was creator Ends here*/
										
										/*This function will display the user in which profile he is from and display media where that profile was creator*/
										$new_medi_from = $newgeneral->get_from_heis();
										$new_from_from_media_array = array();
										if(!empty($new_medi_from))
										{
											for($count_cre_m=0;$count_cre_m<count($new_medi_from);$count_cre_m++)
											{
												if($new_medi_from[$count_cre_m]['media_type']==114){
												$new_from_from_media_array[] = $new_medi_from[$count_cre_m];
												}
											}
										}
										//var_dump($new_creator_creator_media_array);
										/*This function will display the user in which profile he is from and display media where that profile was creator Ends here*/
										
										
										/*This function will display the user in which profile he is from or creator and tagged and display media where that profile was creator*/
										$new_medi_tagged = $newgeneral->get_the_user_tagin_project($_SESSION['login_email']);
										$new_tagged_from_media_array = array();
										if(!empty($new_medi_tagged))
										{
											for($count_cre_m=0;$count_cre_m<count($new_medi_tagged);$count_cre_m++)
											{
												if($new_medi_tagged[$count_cre_m]['media_type']==114){
												$new_tagged_from_media_array[] = $new_medi_tagged[$count_cre_m];
												}
											}
										}
										//var_dump($new_creator_creator_media_array);
										/*This function will display the user in which profile he is from or creator and tagged and display media where that profile was creator Ends here*/
										
										
										/*This function will display in which media the curent user was creator*/
										$get_where_creator_media = $newgeneral->get_media_where_creator_or_from();
										$new_creator_media_array = array();
										if(!empty($get_where_creator_media))
										{
											for($count_tag_m=0;$count_tag_m<count($get_where_creator_media);$count_tag_m++)
											{
												if($get_where_creator_media[$count_tag_m]['media_type']==114){
													$new_creator_media_array[] = $get_where_creator_media[$count_tag_m];
												}
											}
										}
										/*This function will display in which media the curent user was creator ends here*/
										
										$get_songs_pnsew_w = array_merge($get_songs_n,$get_songs_n_c,$get_songs_p_new,$get_songs_edn,$get_songs_en_ec);
										
										$get_songs_pnew_ct = array_unique($get_songs_pnsew_w);
										$get_songs_final_ct = array();
										
										for($count_art_cr_song=0;$count_art_cr_song<count($get_songs_pnew_ct);$count_art_cr_song++)
										{
											if($get_songs_pnew_ct[$count_art_cr_song]!="")
											{
												$get_media_tag_art_pro_song = $newgeneral->get_tag_media_info($get_songs_pnew_ct[$count_art_cr_song]);
												$get_songs_final_ct[] = $get_media_tag_art_pro_song;
											}
										}
										
										$acc_medss = array();
										for($acc_vis=0;$acc_vis<count($acc_media);$acc_vis++)
										{
											if($acc_media[$acc_vis]!="")
											{
												$sels_tagged = $newgeneral->get_community_media_type_tagged_by_other_user($acc_media[$acc_vis]);
												if(!empty($sels_tagged))
												{
													
													$acc_medss[] = $sels_tagged;
												}
											}
										}
										$get_songs_final_ct = array();
										$type = '114';
										$cre_frm_med = array();
										$cre_frm_med = $newgeneral->get_media_create($type);
										
										$get_songs_pnew_w = array_merge($acc_medss,$get_song_id,$get_songs_final_ct,$cre_frm_med,$new_creator_creator_media_array,$new_from_from_media_array,$new_tagged_from_media_array,$new_creator_media_array);
										
										for($acc_song1=0;$acc_song1<count($get_songs_pnew_w);$acc_song1++)
										{
											if($get_songs_pnew_w[$acc_song1]['id']!="" && $get_songs_pnew_w[$acc_song1]['id']!=0)
											{
												$get_artist_song1 = $newgeneral->get_community_song($get_songs_pnew_w[$acc_song1]['id']);
												$getsong1 = mysql_fetch_assoc($get_artist_song1);
												$ids = $get_songs_pnew_w[$acc_song1]['id'];
												$get_songs_pnew_w[$acc_song1]['track'] = $getsong1['track'];
											}
										}
										
										$sort = array();
										foreach($get_songs_pnew_w as $k=>$v) {
											
											$end_c1 = $v['creator'];
											//$create_c = strpos($v['creator'],'(');
											//$end_c1 = substr($v['creator'],0,$create_c);
											$end_c = trim($end_c1);
											
											$end_f1 = $v['from'];
											//$create_f = strpos($v['from'],'(');
											//$end_f1 = substr($v['from'],0,$create_f);
											$end_f = trim($end_f1);
											
											$sort['creator'][$k] = strtolower($end_c);
											$sort['from'][$k] = strtolower($end_f);
											$sort['track'][$k] = $v['track'];
											$sort['title'][$k] = strtolower($v['title']);
										}
										//var_dump($sort);
										if(!empty($sort))
										{
											array_multisort($sort['creator'], SORT_ASC, $sort['from'], SORT_ASC, $sort['track'], SORT_ASC,$sort['title'], SORT_ASC,$get_songs_pnew_w);
										}
										//$get_songs_pnew = array_unique($get_songs_pnew_w);
										//var_dump($get_songs_pnew);
										
										if(!empty($get_songs_pnew_w))
										{
									?>
											<select title="Select media and profiles to display on your profile page." name="tagged_songs[]" size="5" multiple="multiple" id="tagged_songs[]" class="list">
									<?php
										$dummy_s = array();
										$get_all_del_id = $newgeneral->get_all_del_media_id();
										$deleted_id = explode(",",$get_all_del_id);
										for($count_del=0;$count_del<=count($get_songs_pnew_w);$count_del++)
										{
											if($get_songs_pnew_w[$count_del]==""){continue;}
											else{
												for($count_deleted=0;$count_deleted<=count($deleted_id);$count_deleted++)
												{
													if($deleted_id[$count_deleted]==""){continue;}
													else{
														if($deleted_id[$count_deleted] == $get_songs_pnew_w[$count_del]['id'])
														{
															$get_songs_pnew_w[$count_del]="";
														}
													}
												}
											}
										}
										for($acc_song=0;$acc_song<count($get_songs_pnew_w);$acc_song++)
										{
											if($get_songs_pnew_w[$acc_song]['delete_status']==0)
											{
												if(isset($get_songs_pnew_w[$acc_song]['id']))
												{
													if(in_array($get_songs_pnew_w[$acc_song]['id'],$dummy_s))
													{
														
													}
													else
													{
														$dummy_s[] = $get_songs_pnew_w[$acc_song]['id'];
														if($get_songs_pnew_w[$acc_song]['id']!="" && $get_songs_pnew_w[$acc_song]['media_type']==114)
														{
															//$create_c = strpos($get_songs_pnew_w[$acc_song]['creator'],'(');
															//$end_c = substr($get_songs_pnew_w[$acc_song]['creator'],0,$create_c);
															$end_c = $get_songs_pnew_w[$acc_song]['creator'];
															$end_f = $get_songs_pnew_w[$acc_song]['from'];
															//$create_f = strpos($get_songs_pnew_w[$acc_song]['from'],'(');
															//$end_f = substr($get_songs_pnew_w[$acc_song]['from'],0,$create_f);
															
															$eceks = "";
															if($end_c!="")
															{
																$eceks = $end_c;
															}
															if($end_f!="")
															{
																if($end_c!="")
																{
																	$eceks .= " : ".$end_f;
																}
																else
																{
																	$eceks .= $end_f;
																}
															}
															if($get_songs_pnew_w[$acc_song]['track']!="")
															{
																if($end_c!="" || $end_f!="")
																{
																	$eceks .= " : ".$get_songs_pnew_w[$acc_song]['track'];
																}
																else
																{
																	$eceks .= $get_songs_pnew_w[$acc_song]['track'];
																}
															}
															if($get_songs_pnew_w[$acc_song]['title']!="")
															{
																if($end_c!="" || $end_f!="" || $get_songs_pnew_w[$acc_song]['track']!="")
																{
																	$eceks .= " : ".$get_songs_pnew_w[$acc_song]['title'];
																}
																else
																{
																	$eceks .= $get_songs_pnew_w[$acc_song]['title'];
																}
															}
															
															if($get_select_songs!="")
															{
																$get_select_songs = array_unique($get_select_songs);
																$yes_com_tag_pro_song =0;
																for($count_sel=0;$count_sel<=count($get_select_songs);$count_sel++)
																{
																	if($get_select_songs[$count_sel]==""){continue;}
																	else
																	{
																		if($get_select_songs[$count_sel] == $get_songs_pnew_w[$acc_song]['id'])
																		{
																			$yes_com_tag_pro_song = 1;
											?>
																			<option value="<?php echo $get_songs_pnew_w[$acc_song]['id'];?>" selected><?php echo stripslashes($eceks); ?></option>
											<?php
																		}
																	}
																}
																if($yes_com_tag_pro_song == 0)
																{													
											?>
																	<option value="<?php echo $get_songs_pnew_w[$acc_song]['id'];?>"><?php echo stripslashes($eceks); ?></option>
											<?php
																}
															}
															else
															{
				?>
																<option value="<?php echo $get_songs_pnew_w[$acc_song]['id'];?>" ><?php echo stripslashes($eceks); ?></option>
				<?php
															}
														}
													}
												}
											}
										}
										
										/* for($count_art_cr_song=0;$count_art_cr_song<count($get_songs_pnew);$count_art_cr_song++)
										{
											if($get_songs_pnew[$count_art_cr_song]!="")
											{	
												$art_coms_a = '';
												if($get_media_cre_art_pro[$cr_i]['artist_id']!=0 && $get_media_cre_art_pro[$cr_i]['artist_id']!="")
												{
													$art_coms_a = 'artist~'.$get_media_cre_art_pro[$cr_i]['artist_id'];
												}
												elseif($get_media_cre_art_pro[$cr_i]['community_id']!=0 && $get_media_cre_art_pro[$cr_i]['community_id']!="")
												{
													$art_coms_a = 'community~'.$get_media_cre_art_pro[$cr_i]['community_id'];
												}
														
												$reg_meds = $newgeneral->get_media_reg_info($get_songs_pnew[$count_art_cr_song],$art_coms_a);
												if($reg_meds!=NULL && $reg_meds!='')
												{
													if($reg_meds=='general_artist_audio')
													{
														$sql_a = mysql_query("SELECT * FROM general_artist_audio WHERE audio_id='".$get_songs_pnew[$count_art_cr_song]."'");
														if(mysql_num_rows($sql_a)>0)
														{
															$ans_a = mysql_fetch_assoc($sql_a);
															$sql_gens = $newgeneral->sel_general_aid($get_media_cre_art_pro[$cr_i]['artist_id']);
																	
															if($get_select_songs!="")
															{
																//$get_sel_song =$newgeneral->get_sel_songs($_GET['edit']);
																//$exp_song_id = explode(",",$get_sel_song['songs']);
																$yes_com_tag_pro_song =0;
																for($count_sela=0;$count_sela<=count($get_select_songs);$count_sela++)
																{
																	if($get_select_songs[$count_sela]=="")
																	{continue;}
																	else
																	{
																		if($get_select_songs[$count_sela] == $ans_a['audio_id'])
																		{
																			$yes_com_tag_pro_song = 1;
								?>															
																			<option value="<?php echo $ans_a['audio_id'];?>" selected><?php echo substr($sql_gens['fname'] ." ". $sql_gens['lname'],0,20) ." : ".  $ans_a['audio_name'];?></option>
								<?php
																		}
																	}
																}
																if($yes_com_tag_pro_song == 0)
																{			
								?>
																	<option value="<?php echo $ans_a['audio_id'];?>"><?php echo substr($sql_gens['fname'] ." ". $sql_gens['lname'],0,20) ." : ".  $ans_a['audio_name'];?></option>
								<?php
																}
															}
															else
															{
								?>
																<option value="<?php echo $ans_a['audio_id'];?>"><?php echo substr($sql_gens['fname'] ." ". $sql_gens['lname'],0,20) ." : ".  $ans_a['audio_name'];?></option>
																	<?php
															}
														}
													}
													if($reg_meds=='general_community_audio')
													{
														$sql_a = mysql_query("SELECT * FROM general_community_audio WHERE audio_id='".$get_songs_pnew[$count_art_cr_song]."'");
														if(mysql_num_rows($sql_a)>0)
														{
															$ans_a = mysql_fetch_assoc($sql_a);
															$sql_gens = $newgeneral->sel_general_cid($get_media_cre_art_pro[$cr_i]['community_id']);
															
															if($get_select_songs!="")
															{
																//$get_sel_song =$newgeneral->get_sel_songs($_GET['edit']);
																//$exp_song_id = explode(",",$get_sel_song['songs']);
																$yes_com_tag_pro_song =0;
																for($count_selc=0;$count_selc<=count($get_select_songs);$count_selc++)
																{
																	if($get_select_songs[$count_selc]=="")
																	{continue;}
																	else
																	{
																		if($get_select_songs[$count_selc] == $ans_a['audio_id'])
																		{
																			$yes_com_tag_pro_song = 1;
							?>															
																			<option value="<?php echo $ans_a['audio_id'];?>" selected><?php echo substr($sql_gens['fname'] ." ". $sql_gens['lname'],0,20) ." : ".  $ans_a['audio_name'];?></option>
							<?php
																		}
																	}
																}
																if($yes_com_tag_pro_song == 0)
																{									
							?>
																	<option value="<?php echo $ans_a['audio_id'];?>"><?php echo substr($sql_gens['fname'] ." ". $sql_gens['lname'],0,20) ." : ".  $ans_a['audio_name'];?></option>
							<?php
																}
															}
															else
															{
							?>
																<option value="<?php echo $ans_a['audio_id'];?>"><?php echo substr($sql_gens['fname'] ." ". $sql_gens['lname'],0,20) ." : ".  $ans_a['audio_name'];?></option>
							<?php
															}
														}
													}
												}
												else
												{
													$get_media_tag_art_pro_song = $newgeneral->get_tag_media_info($get_songs_pnew[$count_art_cr_song]);
													if($get_select_songs!="")
													{
														//$get_sel_song =$newgeneral->get_sel_songs($_GET['edit']);
														//$exp_song_id = explode(",",$get_sel_song['songs']);
														$yes_com_tag_pro_song =0;
														for($count_sel=0;$count_sel<=count($get_select_songs);$count_sel++)
														{
															if($get_select_songs[$count_sel]==""){continue;}
															else{
																if($get_select_songs[$count_sel] == $get_media_tag_art_pro_song['id'])
																{
																	$yes_com_tag_pro_song = 1;
																?>
																	<option value="<?php echo $get_media_tag_art_pro_song['id'];?>" selected><?php echo substr($get_media_tag_art_pro_song['creator'],0,20) ." : ". substr($get_media_tag_art_pro_song['from'],0,20) ." : ".  $get_media_tag_art_pro_song['title'];?></option>
																<?php
																}
															}
														}
														if($yes_com_tag_pro_song == 0)
														{
															
														?>
															<option value="<?php echo $get_media_tag_art_pro_song['id'];?>"><?php echo substr($get_media_tag_art_pro_song['creator'],0,20) ." : ". substr($get_media_tag_art_pro_song['from'],0,20) ." : ".  $get_media_tag_art_pro_song['title'];?></option>
														<?php
														}
													}
													else
													{
					?>
														<option value="<?php echo $get_media_tag_art_pro_song['id'];?>"><?php echo substr($get_media_tag_art_pro_song['creator'],0,20) ." : ". substr($get_media_tag_art_pro_song['from'],0,20) ." : ".  $get_media_tag_art_pro_song['title'];?></option>
													<?php
													}
												}
											}
										} */
												
											//}
										//}
											
										//if(mysql_num_rows($get_community_song_id)<=0 && $media_song=="" && $res_song==null && count($get_media_cre_art_pro)<=0 && mysql_num_rows($get_media_tag_com_pro)<=0 && mysql_num_rows($get_media_tag_art_pro)<=0)
									?>
											</select>
									<?php	
										}
										
										if(empty($get_songs_pnew_w))
										{
										?>
											<a style="border-radius:5px; -webkit-border-radius:5px; -moz-border-radius:5px; -ms-border-radius:5px; -o-border-radius:5px; background-color: #000000; border: 0 none; color: #FFFFFF; font-size: 13px; margin-left: 5px; padding: 6px 8px; text-decoration: none;" href="add_media.php?adds=114">Add Song</a>
										<?php	
										}
										?>
                                    
                                </div>
                                <div class="fieldCont">
                                  <div title="Select media and profiles to display on your profile page." class="fieldTitle">Videos</div>
                                        <?php
											/* if(isset($res_video) && $res_video!=null)
											{
												for($i=0;$i<count($get_select_videos);$i++)
												{
													if($get_select_videos[$i]==$res_video['video_id'])
													{
												?>
													<option value="<?php echo $res_video['video_id'];?>" selected ><?php echo substr($newres1['fname'].' '.$newres1['lname'],0,20) ." : ". $res_video['video_name'];?></option>
												<?php	
														continue;
													}
												}
												?>
													<option value="<?php echo $res_video['video_id'];?>" ><?php echo substr($newres1['fname'].' '.$newres1['lname'],0,20) ." : ". $res_video['video_name'];?></option>
												<?php		
													
											} */
											
										$get_video_id = array();
										$get_community_video_id=$newgeneral->get_community_video_id();
										while($get_video_ids = mysql_fetch_assoc($get_community_video_id))
										{
											$get_video_id[] = $get_video_ids;
										}
										
										$get_media_tag_art_pro = $newgeneral->get_artist_project_tag($_SESSION['login_email']);
										if(mysql_num_rows($get_media_tag_art_pro)>0)
										{
											$is_v = 0;
											while($art_pro_tag = mysql_fetch_assoc($get_media_tag_art_pro))
											{
												$get_videos = explode(",",$art_pro_tag['tagged_videos']);
												for($count_art_tag_video=0;$count_art_tag_video<count($get_videos);$count_art_tag_video++)
												{
													if($get_videos[$count_art_tag_video]!="")
													{
														$get_videos_new[$is_v] = $get_videos[$count_art_tag_video];
														$is_v = $is_v + 1;
													}
												}
											}
											if($get_videos_new!="")
											{
												$get_videos_n = array_unique($get_videos_new);
											}
										}
										
										$get_media_tag_art_eve = $newgeneral->get_artist_event_tag($_SESSION['login_email']);
										if(mysql_num_rows($get_media_tag_art_eve)>0)
										{
											$is_v = 0;
											while($art_pro_tag = mysql_fetch_assoc($get_media_tag_art_eve))
											{
												$get_videos = explode(",",$art_pro_tag['tagged_videos']);
												for($count_art_tag_video=0;$count_art_tag_video<count($get_videos);$count_art_tag_video++)
												{
													if($get_videos[$count_art_tag_video]!="")
													{
														$get_videos_neew[$is_v] = $get_videos[$count_art_tag_video];
														$is_v = $is_v + 1;
													}
												}
											}
											if($get_videos_neew!="")
											{
												$get_videoes_en = array_unique($get_videos_neew);
											}
										}
										
										$get_media_tag_com_pro = $newgeneral->get_community_project_tag($_SESSION['login_email']);
										if(mysql_num_rows($get_media_tag_com_pro)>0)
										{
											$is_vc = 0;
											while($com_pro_tag = mysql_fetch_assoc($get_media_tag_com_pro))
											{
												$get_com_videos = explode(",",$com_pro_tag['tagged_videos']);
												for($count_com_tag_video=0;$count_com_tag_video<count($get_com_videos);$count_com_tag_video++)
												{
													if($get_com_videos[$count_com_tag_video]!="")
													{
														$get_videos_c_new[$is_vc] = $get_com_videos[$count_com_tag_video];
														$is_vc = $is_vc + 1;
													}
												}
											}
											if($get_videos_c_new!="")
											{
												$get_videos_c_n = array_unique($get_videos_c_new);
											}
										}
										
										$get_media_tag_com_eve = $newgeneral->get_community_event_tag($_SESSION['login_email']);
										if(mysql_num_rows($get_media_tag_com_eve)>0)
										{
											$is_vc = 0;
											while($com_pro_tag = mysql_fetch_assoc($get_media_tag_com_eve))
											{
												$get_com_videos = explode(",",$com_pro_tag['tagged_videos']);
												for($count_com_tag_video=0;$count_com_tag_video<count($get_com_videos);$count_com_tag_video++)
												{
													if($get_com_videos[$count_com_tag_video]!="")
													{
														$get_videeos_c_new[$is_vc] = $get_com_videos[$count_com_tag_video];
														$is_vc = $is_vc + 1;
													}
												}
											}
											if($get_videeos_c_new!="")
											{
												$geet_videos_c_en = array_unique($get_videeos_c_new);
											}
										}
										
										$get_media_cre_art_pro = $newgeneral->get_project_create();
										if(!empty($get_media_cre_art_pro))
										{
											$is_v_c = 0;
											//while($art_pro_tag = mysql_fetch_assoc($get_media_cre_art_pro))
											for($cr_ei=0;$cr_ei<count($get_media_cre_art_pro);$cr_ei++)
											{
												$get_videos = explode(",",$get_media_cre_art_pro[$cr_ei]['tagged_videos']);

												for($count_art_cr_video=0;$count_art_cr_video<count($get_videos);$count_art_cr_video++)
												{
													if($get_videos[$count_art_cr_video]!="")
													{
														$get_videos__new[$is_v_c] = $get_videos[$count_art_cr_video];
														$is_v_c = $is_v_c + 1;
													}
												}
											}
											if($get_videos__new!="")
											{
												$get_videos__n = array_unique($get_videos__new);
											}
										}
										
										if(!isset($get_videos_n))
										{
											$get_videos_n = array();
										}
										
										if(!isset($geet_videos_c_en))
										{
											$geet_videos_c_en = array();
										}
										
										if(!isset($get_videoes_en))
										{
											$get_videoes_en = array();
										}
										
										if(!isset($get_videos_c_n))
										{
											$get_videos_c_n = array();
										}
										
										if(!isset($get_videos__n))
										{
											$get_videos__n = array();
										}
										
									/*	$find_creator_heis = $newgeneral -> get_creator_heis();
										$get_genral_user_info = $newgeneral -> selgeneral();
										$where_creator = array();
										if(!empty($find_creator_heis)){
											for($new_cre_med=0;$new_cre_med<=count($find_creator_heis);$new_cre_med++)
											{
												if($find_creator_heis[$new_cre_med]['general_user_id']!= $get_genral_user_info['general_user_id']){
													if($find_creator_heis[$new_cre_med]['media_type']==115){
													$where_creator[] = $find_creator_heis[$new_cre_med];}
												}
											}
										}
										
										$find_tag_heis = $newgeneral -> get_tag_where_heis($_SESSION['login_email']);
										$get_genral_user_info = $newgeneral -> selgeneral();
										$where_tagged = array();
										if(!empty($find_tag_heis)){
											for($new_cre_med=0;$new_cre_med<=count($find_tag_heis);$new_cre_med++)
											{
												if($find_tag_heis[$new_cre_med]['general_user_id']!= $get_genral_user_info['general_user_id']){
													if($find_tag_heis[$new_cre_med]['media_type']==115){
													$where_tagged[] = $find_tag_heis[$new_cre_med];}
												}
											}
										}*/
										
										/*This function will display the user in which profile he is creator and display media where that profile was creator*/
										$new_medi = $newgeneral->get_creator_heis();
										$new_creator_creator_media_array = array();
										if(!empty($new_medi))
										{
											for($count_cre_m=0;$count_cre_m<count($new_medi);$count_cre_m++)
											{
												if($new_medi[$count_cre_m]['media_type']==115){
													$new_creator_creator_media_array[] = $new_medi[$count_cre_m];
												}
											}
										}
										//var_dump($new_creator_creator_media_array);
										/*This function will display the user in which profile he is creator and display media where that profile was creator Ends here*/
										
										/*This function will display the user in which profile he is from and display media where that profile was creator*/
										$new_medi_from = $newgeneral->get_from_heis();
										$new_from_from_media_array = array();
										if(!empty($new_medi_from))
										{
											for($count_cre_m=0;$count_cre_m<count($new_medi_from);$count_cre_m++)
											{
												if($new_medi_from[$count_cre_m]['media_type']==115){
												$new_from_from_media_array[] = $new_medi_from[$count_cre_m];
												}
											}
										}
										//var_dump($new_creator_creator_media_array);
										/*This function will display the user in which profile he is from and display media where that profile was creator Ends here*/
										
										
										/*This function will display the user in which profile he is from or creator and tagged and display media where that profile was creator*/
										$new_medi_tagged = $newgeneral->get_the_user_tagin_project($_SESSION['login_email']);
										$new_tagged_from_media_array = array();
										if(!empty($new_medi_tagged))
										{
											for($count_cre_m=0;$count_cre_m<count($new_medi_tagged);$count_cre_m++)
											{
												if($new_medi_tagged[$count_cre_m]['media_type']==115){
												$new_tagged_from_media_array[] = $new_medi_tagged[$count_cre_m];
												}
											}
										}
										//var_dump($new_creator_creator_media_array);
										/*This function will display the user in which profile he is from or creator and tagged and display media where that profile was creator Ends here*/
										
										
										/*This function will display in which media the curent user was creator*/
										$get_where_creator_media = $newgeneral->get_media_where_creator_or_from();
										$new_creator_media_array = array();
										if(!empty($get_where_creator_media))
										{
											for($count_tag_m=0;$count_tag_m<count($get_where_creator_media);$count_tag_m++)
											{
												if($get_where_creator_media[$count_tag_m]['media_type']==115){
													$new_creator_media_array[] = $get_where_creator_media[$count_tag_m];
												}
											}
										}
										/*This function will display in which media the curent user was creator ends here*/
										
										$get_videos_n_n = array_merge($get_videos_n,$get_videos_c_n,$get_videos__n,$get_videoes_en,$geet_videos_c_en);
										
										$get_vids_pnew_ct = array_unique($get_videos_n_n);
										$get_vids_final_ct = array();
										
										for($count_art_cr_video=0;$count_art_cr_video<count($get_vids_pnew_ct);$count_art_cr_video++)
										{
											if($get_vids_pnew_ct[$count_art_cr_video]!="")
											{
												$get_media_tag_art_pro_vi = $newgeneral->get_tag_media_info($get_vids_pnew_ct[$count_art_cr_video]);
												$get_vids_final_ct[] = $get_media_tag_art_pro_vi;
											}
										}
										
										$acc_medsv = array();
										for($acc_vis=0;$acc_vis<count($acc_media);$acc_vis++)
										{
											if($acc_media[$acc_vis]!="")
											{
												$sel_tagged = $newgeneral->get_community_media_type_tagged_by_other_user($acc_media[$acc_vis]);
												if(!empty($sel_tagged))
												{
													$acc_medsv[] = $sel_tagged;
												}
											}
										}
										$get_vids_final_ct = array();
										$type = '115';
										$cre_frm_med = array();
										$cre_frm_med = $newgeneral->get_media_create($type);
										
										$get_vids_pnew_w = array_merge($acc_medsv,$get_video_id,$get_vids_final_ct,$cre_frm_med,$new_creator_creator_media_array,$new_from_from_media_array,$new_tagged_from_media_array,$new_creator_media_array);
										
										$sort = array();
										foreach($get_vids_pnew_w as $k=>$v) {
											
											$end_c1 = $v['creator'];
											//$create_c = strpos($v['creator'],'(');
											//$end_c1 = substr($v['creator'],0,$create_c);
											$end_c = trim($end_c1);
											
											$end_f1 = $v['from'];
											//$create_f = strpos($v['from'],'(');
											//$end_f1 = substr($v['from'],0,$create_f);
											$end_f = trim($end_f1);
											
											$sort['creator'][$k] = strtolower($end_c);
											$sort['from'][$k] = strtolower($end_f);
											//$sort['track'][$k] = $v['track'];
											$sort['title'][$k] = strtolower($v['title']);
										}
										//var_dump($sort);
										if(!empty($sort))
										{
											array_multisort($sort['creator'], SORT_ASC, $sort['from'], SORT_ASC, $sort['title'], SORT_ASC,$get_vids_pnew_w);
										}
										
										if(!empty($get_vids_pnew_w))
										{
									?>
											<select title="Select media and profiles to display on your profile page." name="tagged_videos[]" size="5" multiple="multiple" id="tagged_videos[]" class="list">
									<?php
										$dummy_v = array();
										$get_all_del_id = $newgeneral->get_all_del_media_id();
										$deleted_id = explode(",",$get_all_del_id);
										for($count_del=0;$count_del<=count($get_vids_pnew_w);$count_del++)
										{
											if($get_vids_pnew_w[$count_del]==""){continue;}
											else{
												for($count_deleted=0;$count_deleted<=count($deleted_id);$count_deleted++)
												{
													if($deleted_id[$count_deleted]==""){continue;}
													else{
														if($deleted_id[$count_deleted] == $get_vids_pnew_w[$count_del]['id'])
														{
															$get_vids_pnew_w[$count_del]="";
														}
													}
												}
											}
										}
										for($acc_vide=0;$acc_vide<count($get_vids_pnew_w);$acc_vide++)
										{
											if($get_vids_pnew_w[$acc_vide]['delete_status']==0)
											{
												if(isset($get_vids_pnew_w[$acc_vide]['id']))
												{
													if(in_array($get_vids_pnew_w[$acc_vide]['id'],$dummy_v))
													{
														
													}
													else
													{
														$dummy_v[] = $get_vids_pnew_w[$acc_vide]['id'];
														if($get_vids_pnew_w[$acc_vide]['id']!="" && $get_vids_pnew_w[$acc_vide]['media_type']==115)
														{
															//$create_c = strpos($get_vids_pnew_w[$acc_vide]['creator'],'(');
															//$end_c = substr($get_vids_pnew_w[$acc_vide]['creator'],0,$create_c);
															$end_c = $get_vids_pnew_w[$acc_vide]['creator'];
															$end_f = $get_vids_pnew_w[$acc_vide]['from'];
															//$create_f = strpos($get_vids_pnew_w[$acc_vide]['from'],'(');
															//$end_f = substr($get_vids_pnew_w[$acc_vide]['from'],0,$create_f);
															
															$eceksv = "";
															if($end_c!="")
															{
																$eceksv = $end_c;
															}
															if($end_f!="")
															{
																if($end_c!="")
																{
																	$eceksv .= " : ".$end_f;
																}
																else
																{
																	$eceksv .= $end_f;
																}
															}
															if($get_vids_pnew_w[$acc_vide]['title']!="")
															{
																if($end_c!="" || $end_f!="")
																{
																	$eceksv .= " : ".$get_vids_pnew_w[$acc_vide]['title'];
																}
																else
																{
																	$eceksv .= $get_vids_pnew_w[$acc_vide]['title'];
																}
															}
															
															if($get_select_videos!="")
															{
																$get_select_videos = array_unique($get_select_videos);
																$yes_com_tag_pro_song =0;
																for($count_sel=0;$count_sel<=count($get_select_videos);$count_sel++)
																{
																	if($get_select_videos[$count_sel]==""){continue;}
																	else
																	{
																		if($get_select_videos[$count_sel] == $get_vids_pnew_w[$acc_vide]['id'])
																		{
																			$yes_com_tag_pro_song = 1;
											?>
																			<option value="<?php echo $get_vids_pnew_w[$acc_vide]['id'];?>" selected><?php echo stripslashes($eceksv); ?></option>
											<?php
																		}
																	}
																}
																if($yes_com_tag_pro_song == 0)
																{													
											?>
																	<option value="<?php echo $get_vids_pnew_w[$acc_vide]['id'];?>"><?php echo stripslashes($eceksv); ?></option>
											<?php
																}
															}
															else
															{
				?>
																<option value="<?php echo $get_vids_pnew_w[$acc_vide]['id'];?>" ><?php echo stripslashes($eceksv); ?></option>
				<?php
															}
														}
													}
												}
											}
										}
										
										/* for($count_art_cr_video=0;$count_art_cr_video<count($get_videos_n_new);$count_art_cr_video++)
										{
											if($get_videos_n_new[$count_art_cr_video]!=""){
											
											$art_coms_v = '';
											if($get_media_cre_art_pro[$cr_ei]['artist_id']!=0 && $get_media_cre_art_pro[$cr_ei]['artist_id']!="")
											{
												$art_coms_v = 'artist~'.$get_media_cre_art_pro[$cr_ei]['artist_id'];
											}
											elseif($get_media_cre_art_pro[$cr_ei]['community_id']!=0 && $get_media_cre_art_pro[$cr_ei]['community_id']!="")
											{
												$art_coms_v = 'community~'.$get_media_cre_art_pro[$cr_ei]['community_id'];
											}
											$reg_meds = $newgeneral->get_media_reg_info($get_videos_n_new[$count_art_cr_video],$art_coms_v);
											
											if($reg_meds!=NULL && $reg_meds!='')
											{
												if($reg_meds=='general_artist_video')
												{
													$sql_a = mysql_query("SELECT * FROM general_artist_video WHERE video_id='".$get_videos_n_new[$count_art_cr_video]."'");
													if(mysql_num_rows($sql_a)>0)
													{
														$ans_a = mysql_fetch_assoc($sql_a);
														$sql_gens = $newgeneral->sel_general_aid($get_media_cre_art_pro[$cr_ei]['artist_id']);
														
														if($get_select_videos!="")
														{
															//$get_sel_song =$newgeneral->get_sel_songs($_GET['edit']);
															//$exp_song_id = explode(",",$get_sel_song['songs']);
															$yes_com_tag_pro_song =0;
															for($count_selv=0;$count_selv<=count($get_select_videos);$count_selv++)
															{
																if($get_select_videos[$count_selv]==""){continue;}
																else
																{
																	if($get_select_videos[$count_selv] == $ans_a['video_id'])
																	{
																		$yes_com_tag_pro_song = 1;
						?>															
																		<option value="<?php echo $ans_a['video_id'];?>" selected><?php echo substr($sql_gens['fname'] ." ". $sql_gens['lname'],0,20) ." : ".  $ans_a['video_name'];?></option>
						<?php
																	}
																}
															}
															if($yes_com_tag_pro_song == 0)
															{
						?>
																<option value="<?php echo $ans_a['video_id'];?>"><?php echo substr($sql_gens['fname'] ." ". $sql_gens['lname'],0,20) ." : ".  $ans_a['video_name'];?></option>
						<?php
															}
														}
														else
														{
						?>
															<option value="<?php echo $ans_a['video_id'];?>"><?php echo substr($sql_gens['fname'] ." ". $sql_gens['lname'],0,20) ." : ".  $ans_a['video_name'];?></option>
						<?php
														}
													}
												}
												if($reg_meds=='general_community_video')
												{
													$sql_a = mysql_query("SELECT * FROM general_community_video WHERE video_id='".$get_videos_n_new[$count_art_cr_video]."'");
													if(mysql_num_rows($sql_a)>0)
													{
														$ans_a = mysql_fetch_assoc($sql_a);
														$sql_gens = $newgeneral->sel_general_cid($get_media_cre_art_pro[$cr_ei]['community_id']);
																
														if($get_select_videos!="")
														{
															//$get_sel_song =$newgeneral->get_sel_songs($_GET['edit']);
															//$exp_song_id = explode(",",$get_sel_song['songs']);
															$yes_com_tag_pro_song =0;
															for($count_selvc=0;$count_selvc<=count($get_select_videos);$count_selvc++)
															{
																if($get_select_videos[$count_selvc]==""){continue;}
																else
																{
																	if($get_select_videos[$count_selvc] == $ans_a['video_id'])
																	{
																		$yes_com_tag_pro_song = 1;
						?>															
																		<option value="<?php echo $ans_a['video_id'];?>" selected><?php echo substr($sql_gens['fname'] ." ". $sql_gens['lname'],0,20) ." : ".  $ans_a['video_name'];?></option>
						<?php
																	}
																}
															}
															if($yes_com_tag_pro_song == 0)
															{
						?>
																<option value="<?php echo $ans_a['video_id'];?>"><?php echo substr($sql_gens['fname'] ." ". $sql_gens['lname'],0,20) ." : ".  $ans_a['video_name'];?></option>
						<?php
															}
														}
														else
														{
						?>
															<option value="<?php echo $ans_a['video_id'];?>"><?php echo substr($sql_gens['fname'] ." ". $sql_gens['lname'],0,20) ." : ".  $ans_a['video_name'];?></option>
						<?php
														}
													}
												}
											}
											else
											{
											$get_media_tag_art_pro_video = $newgeneral->get_tag_media_info($get_videos_n_new[$count_art_cr_video]);
											if($get_select_videos!="")
											{
												//$get_sel_song =$newgeneral->get_sel_songs($_GET['edit']);
												//$exp_video_id = explode(",",$get_sel_video['taggedvideos']);
												$yes_tag_pro_video =0;
												for($count_sel=0;$count_sel<=count($get_select_videos);$count_sel++)
												{
													if($get_select_videos[$count_sel]==""){continue;}
													else
													{
														if($get_select_videos[$count_sel] == $get_media_tag_art_pro_video['id'])
														{
															$yes_tag_pro_video = 1;
						?>
															<option value="<?php echo $get_media_tag_art_pro_video['id'];?>" selected><?php echo substr($get_media_tag_art_pro_video['creator'],0,20) ." : ". substr($get_media_tag_art_pro_video['from'],0,20) ." : ".  $get_media_tag_art_pro_video['title'];?></option>
						<?php
														}
													}
												}
												if($yes_tag_pro_video == 0)
												{
						?>
													<option value="<?php echo $get_media_tag_art_pro_video['id'];?>"><?php echo substr($get_media_tag_art_pro_video['creator'],0,20) ." : ". substr($get_media_tag_art_pro_video['from'],0,20) ." : ".  $get_media_tag_art_pro_video['title'];?></option>
						<?php
												}
											}
											else
											{
						?>
												<option value="<?php echo $get_media_tag_art_pro_video['id'];?>"><?php echo substr($get_media_tag_art_pro_video['creator'],0,20) ." : ". substr($get_media_tag_art_pro_video['from'],0,20) ." : ".  $get_media_tag_art_pro_video['title'];?></option>
						<?php
											}
										}
									}
								} */

										//if(mysql_num_rows($get_community_video_id)<=0 && $media_video=="" && $res_video==null && count($get_media_cre_art_pro)<=0 && mysql_num_rows($get_media_tag_com_pro)<=0 && mysql_num_rows($get_media_tag_art_pro)<=0)
									?>
											</select>
									<?php
										}
										if(empty($get_vids_pnew_w))
										{
										?>
											<a style="border-radius:5px; -webkit-border-radius:5px; -moz-border-radius:5px; -ms-border-radius:5px; -o-border-radius:5px; background-color: #000000; border: 0 none; color: #FFFFFF; font-size: 13px; margin-left: 5px; padding: 6px 8px; text-decoration: none;" href="add_media.php?adds=115">Add Video</a>
										<?php	
										}
										?>
                                    
                                </div>
                                <!--<div class="fieldCont">
                                  <div class="fieldTitle">Channels</div>
                                    <select name="tagged_channels[]" size="5" multiple="multiple" id="tagged_channels[]" class="list">
                                        <?php
											/*while($get_channel_id=mysql_fetch_assoc($get_community_channel_id))
											{
												//$get_community_channel=$newgeneral->get_community_channel($get_channel_id['id']);
												//$getchannel=mysql_fetch_assoc($get_community_channel);
												for($ch=0;$ch<count($get_select_channels);$ch++)
												{
													if($get_select_channels[$ch]==$get_channel_id['id'])
													{
														$ch_sel=1;
											?>
												<option value="<?php echo $get_channel_id['id'];?>" selected><?php echo substr($get_channel_id['creator'],0,20) ." : ".substr($get_channel_id['from'],0,20) ." : ". $get_channel_id['title'];?></option>
											<?php
														continue;
													}
												}
												if($ch_sel==1)
												{
													$ch_sel=0;
													continue;
												}
												?>
													<option value="<?php echo $get_channel_id['id'];?>" ><?php echo substr($get_channel_id['creator'],0,20) ." : ".substr($get_channel_id['from'],0,20) ." : ".  $get_channel_id['title'];?></option>
											<?php
											}
											for($ac_ch=0;$ac_ch<count($acc_media);$ac_ch++)
											{
												$sel_tagged=$newgeneral->get_community_media_type_tagged_by_other_user($acc_media[$ac_ch]);
												if($sel_tagged['media_type']==116)
												{
													$media_chan="true";
													for($se_ch=0;$se_ch<count($get_select_channels);$se_ch++)
													{
														if($get_select_channels[$se_ch]==$sel_tagged['id'])
														{
															$sel=1;
												?>
													<option value="<?php echo $sel_tagged['id'];?>" selected ><?php echo substr($sel_tagged['creator'],0,20) ." : ".substr($sel_tagged['from'],0,20) ." : ". $sel_tagged['title'];?></option>
												<?php	
															continue;
														}
													}
													if($sel==1)	
													{
														$sel=0;
														continue;
													}
												?>
													<option value="<?php echo $sel_tagged['id'];?>" ><?php echo substr($sel_tagged['creator'],0,20) ." : ".substr($sel_tagged['from'],0,20) ." : ". $sel_tagged['title'];?></option>
												<?php		
												}
											}
										if(mysql_num_rows($get_community_channel_id)==0 && $media_chan=="")
										{
										?>
											<option value="" >No Listings</option>
										<?php	
										}*/
										?>
                                    </select>
                                </div>-->
								
								<div class="fieldCont">
                                  <div title="Select media and profiles to display on your profile page." class="fieldTitle">Upcoming Events</div>
                                        <?php
										$eve_comm_check = 0;
										$get_upev_id = array();
										while($get_upev_ids = mysql_fetch_assoc($get_event))
										{
											$get_upev_ids['id'] = $get_upev_ids['id'].'~com';
											$get_upev_id[] = $get_upev_ids;
										}
										
										$get_aupev_id = array();
										while($get_aupev_ids = mysql_fetch_assoc($get_aeevent))
										{
											$get_aupev_ids['id'] = $get_aupev_ids['id'].'~art';
											$get_aupev_id[] = $get_aupev_ids;
										}

										$get_onlycre_ev = array();
										$get_onlycre_ev = $newgeneral->only_creator_upevent();
										
										$get_only2cre_ev = array();
										$get_only2cre_ev = $newgeneral->only_creator2ev_upevent();
										
										$selcoms_tagged = array();
										if($event_avail!=0)
										{
											$acc_event = array_unique($acc_event);
											for($h=0;$h<count($acc_event);$h++)
											{
												if($acc_event[$h]!="" && $acc_event[$h]!=0)
												{
													$dumcomu = $newgeneral->get_event_tagged_by_other_user($acc_event[$h]);
													if(!empty($dumcomu) && $dumcomu['id']!="" && $dumcomu['id']!="")
													{
														$dumcomu['id'] = $dumcomu['id'].'~'.'com';
														$selcoms_tagged[] = $dumcomu;
													}	
												}
											}
										}
										
										$selarts_tagged = array();
										if($event_aavail!=0)
										{
											$acc_aevent = array_unique($acc_aevent);
											for($h=0;$h<count($acc_aevent);$h++)
											{
												if($acc_aevent[$h]!="" && $acc_aevent[$h]!=0)
												{
													$dumartu = $newgeneral->get_aevent_tagged_by_other_user($acc_aevent[$h]);
													if(!empty($dumartu) && $dumartu['id']!="" && $dumartu['id']!="")
													{
														$dumartu['id'] = $dumartu['id'].'~'.'art';
														$selarts_tagged[] = $dumartu;
													}	
												}
											}
										}
										
										$selarts2_tagged = array();
										if($project_aavail!=0)
										{
											$acc_aproject = array_unique($acc_aproject);
											for($acc_pro=0;$acc_pro<count($acc_aproject);$acc_pro++)
											{												
												if($acc_aproject[$acc_pro]!="" && $acc_aproject[$acc_pro]!=0)
												{													
													$sql_1_cre = $newgeneral->get_event_tagged_by_other2_user($acc_aproject[$acc_pro]);
													if($sql_1_cre!="")
													{
														if(mysql_num_rows($sql_1_cre)>0)
														{
															while($run = mysql_fetch_assoc($sql_1_cre))
															{
																$run['id'] = $run['id'].'~'.'art';
																//$ans_a_als[] = $run;
																$selarts2_tagged[] = $run;
															}
														}
													}
													
													$sql_2_cre = $newgeneral->get_event_atagged_by_other2_user($acc_aproject[$acc_pro]);
													if($sql_2_cre !="" && $sql_2_cre!= Null)
													{
														if(mysql_num_rows($sql_2_cre)>0)
														{
															while($run_c = mysql_fetch_assoc($sql_2_cre))
															{
																$run_c['id'] = $run_c['id'].'~'.'com';
																//$ans_c_als[]
																$selarts2_tagged[] = $run_c;
															}
														}
													}
												}
											}
										}
										
										$selcoms2_tagged = array();
										if($project_avail!=0)
										{
											$acc_project = array_unique($acc_project);
											for($acc_pro=0;$acc_pro<count($acc_project);$acc_pro++)
											{												
												if($acc_project[$acc_pro]!="" && $acc_project[$acc_pro]!=0)
												{													
													$sql_1_cre = $newgeneral->get_aevent_tagged_by_other2_user($acc_project[$acc_pro]);
													if($sql_1_cre!="")
													{
														if(mysql_num_rows($sql_1_cre)>0)
														{
															while($run = mysql_fetch_assoc($sql_1_cre))
															{
																$run['id'] = $run['id'].'~'.'art';
																//$ans_a_als[] = $run;
																$selcoms2_tagged[] = $run;
															}
														}
													}
													
													$sql_2_cre = $newgeneral->get_aevent_atagged_by_other2_user($acc_project[$acc_pro]);
													if($sql_2_cre !="" && $sql_2_cre!= Null)
													{
														if(mysql_num_rows($sql_2_cre)>0)
														{
															while($run_c = mysql_fetch_assoc($sql_2_cre))
															{
																$run_c['id'] = $run_c['id'].'~'.'com';
																//$ans_c_als[]
																$selcoms2_tagged[] = $run_c;
															}
														}
													}
												}
											}
										}
										
										$final_evesup = array_merge($get_upev_id,$selarts_tagged,$selcoms_tagged,$get_onlycre_ev,$get_aupev_id,$get_only2cre_ev,$selarts2_tagged,$selcoms2_tagged);
										
										$sort = array();
										foreach($final_evesup as $k=>$v) {
										
											//$create_c = strpos($v['creator'],'(');
											//$end_c = substr($v['creator'],0,$create_c);
											
											//$sort['creator'][$k] = $end_c;
											//$sort['from'][$k] = $end_f;
											//$sort['track'][$k] = $v['track'];
											$sort['date'][$k] = $v['date'];
										}
										//var_dump($sort);
										if(!empty($sort))
										{
											array_multisort($sort['date'], SORT_DESC,$final_evesup);
										}
										
										$dummy_uev = array();
										$get_all_del_id = $newgeneral->get_deleted_project_id();
										$exp_del_id = explode(",",$get_all_del_id['deleted_event_id']);
										for($count_all=0;$count_all<count($final_evesup);$count_all++){
											for($count_del=0;$count_del<count($exp_del_id);$count_del++){
												if($exp_del_id[$count_del]!=""){
													if($final_evesup[$count_all]['id'] == $exp_del_id[$count_del]){
														$final_evesup[$count_all] ="";
													}
												}
											}
										}
										
										if(!empty($final_evesup))
										{
				?>
											 <select title="Select media and profiles to display on your profile page." name="tagged_upcoming_events[]" size="5" multiple="multiple" id="tagged_upcoming_events[]" class="list">
				<?php
										for($ev_f=0;$ev_f<count($final_evesup);$ev_f++)
										{
											if($final_evesup[$ev_f]!="")
											{
												if(in_array($final_evesup[$ev_f]['id'],$dummy_uev))
												{}
												else
												{
													$dummy_uev[] = $final_evesup[$ev_f]['id'];
													if($get_select_events[0]!="")
													{
														$get_select_events = array_unique($get_select_events);
														for($f=0;$f<count($get_select_events);$f++)
														{
															if($get_select_events[$f]==$final_evesup[$ev_f]['id'])
															{
																$j_comm_acc=1;
			?>
																<option value="<?php echo $final_evesup[$ev_f]['id'];?>" selected ><?php echo date("m.d.y", strtotime($final_evesup[$ev_f]['date'])).' '.stripslashes($final_evesup[$ev_f]['title']);?></option>
			<?php	
																continue;
															}
														}	
														
														if($j_comm_acc==1)
														{
															$j_comm_acc=0;
															continue;
														}
													}
			?>
													<option value="<?php echo $final_evesup[$ev_f]['id'];?>" ><?php echo date("m.d.y", strtotime($final_evesup[$ev_f]['date'])).' '.stripslashes($final_evesup[$ev_f]['title']);?></option>
			<?php
												}
											}
										}
										
										//if(mysql_num_rows($get_event)<=0 && empty($dummy_uev) && mysql_num_rows($get_aeevent)<=0)
			?>
											</select>
			<?php
										}
										if(empty($final_evesup))
										{
										?>
											<a style="border-radius:5px; -webkit-border-radius:5px; -moz-border-radius:5px; -ms-border-radius:5px; -o-border-radius:5px; background-color: #000000; border: 0 none; color: #FFFFFF; font-size: 13px; margin-left: 5px; padding: 6px 8px; text-decoration: none;" href="add_community_event.php">Add Event</a>
										<?php	
										}
										?>
                                    
                                </div>
								<div class="fieldCont">
                                  <div title="Select media and profiles to display on your profile page." class="fieldTitle">Recorded Events</div>
                                        <?php
										//var_dump($acc_event);
										//	die;
										$eve_comm_checkx = 0;
										$get_ever_id = array();
										while($get_ever_ids = mysql_fetch_assoc($get_eventrec))
										{
											$get_ever_ids['id'] = $get_ever_ids['id'].'~com';
											$get_ever_id[] = $get_ever_ids;											
										}
										
										$get_aever_id = array();
										while($get_aever_ids = mysql_fetch_assoc($get_aeeventrec))
										{
											$get_aever_ids['id'] = $get_aever_ids['id'].'~art';
											$get_aever_id[] = $get_aever_ids;											
										}
										
										$get_onlycre_rev = array();
										$get_onlycre_rev = $newgeneral->only_creator_recevent();
										
										$get_only2cre_rev = array();
										$get_only2cre_rev = $newgeneral->only_creator2_recevent();
										
										$selcom_stagged = array();
										if($event_avail!=0)
										{
											$acc_event = array_unique($acc_event);
											for($k=0;$k<count($acc_event);$k++)
											{
												if($acc_event[$k]!="" && $acc_event[$k]!=0)
												{
													$dumec = $newgeneral->get_event_tagged_by_other_userrec($acc_event[$k]);
													if(!empty($dumec) && $dumec['id']!="" && $dumec['id']!="")
													{
														$dumec['id'] = $dumec['id'].'~'.'com';
														$selcom_stagged[] = $dumec;
													}												
												}
											}
										}
										
										$selart_stagged = array();
										if($event_aavail!=0)
										{
											$acc_aevent = array_unique($acc_aevent);
											for($ki=0;$ki<count($acc_aevent);$ki++)
											{
												if($acc_aevent[$ki]!="" && $acc_aevent[$ki]!=0)
												{
													$dumc = $newgeneral->get_aevent_tagged_by_other_userrec($acc_aevent[$ki]);
													if(!empty($dumc) && $dumc['id']!="" && $dumc['id']!="")
													{
														$dumc['id'] = $dumc['id'].'~'.'art';
														$selart_stagged[] = $dumc;
													}												
												}
											}
										}
										
										$selarts2_tagged = array();
										if($project_aavail!=0)
										{
											$acc_aproject = array_unique($acc_aproject);
											for($acc_pro=0;$acc_pro<count($acc_aproject);$acc_pro++)
											{												
												if($acc_aproject[$acc_pro]!="" && $acc_aproject[$acc_pro]!=0)
												{													
													$sql_1_cre = $newgeneral->get_event_tagged_by_other2_userrec($acc_aproject[$acc_pro]);
													if($sql_1_cre!="")
													{
														if(mysql_num_rows($sql_1_cre)>0)
														{
															while($run = mysql_fetch_assoc($sql_1_cre))
															{
																$run['id'] = $run['id'].'~'.'art';
																//$ans_a_als[] = $run;
																$selarts2_tagged[] = $run;
															}
														}
													}
													
													$sql_2_cre = $newgeneral->get_event_atagged_by_other2_userrec($acc_aproject[$acc_pro]);
													if($sql_2_cre !="" && $sql_2_cre!= Null)
													{
														if(mysql_num_rows($sql_2_cre)>0)
														{
															while($run_c = mysql_fetch_assoc($sql_2_cre))
															{
																$run_c['id'] = $run_c['id'].'~'.'com';
																//$ans_c_als[]
																$selarts2_tagged[] = $run_c;
															}
														}
													}
												}
											}
										}
										
										$selcoms2_tagged = array();
										if($project_avail!=0)
										{
											$acc_project = array_unique($acc_project);
											for($acc_pro=0;$acc_pro<count($acc_project);$acc_pro++)
											{												
												if($acc_project[$acc_pro]!="" && $acc_project[$acc_pro]!=0)
												{													
													$sql_1_cre = $newgeneral->get_aevent_tagged_by_other2_userrec($acc_project[$acc_pro]);
													if($sql_1_cre!="")
													{
														if(mysql_num_rows($sql_1_cre)>0)
														{
															while($run = mysql_fetch_assoc($sql_1_cre))
															{
																$run['id'] = $run['id'].'~'.'art';
																//$ans_a_als[] = $run;
																$selcoms2_tagged[] = $run;
															}
														}
													}
													
													$sql_2_cre = $newgeneral->get_aevent_atagged_by_other2_userrec($acc_project[$acc_pro]);
													if($sql_2_cre !="" && $sql_2_cre!= Null)
													{
														if(mysql_num_rows($sql_2_cre)>0)
														{
															while($run_c = mysql_fetch_assoc($sql_2_cre))
															{
																$run_c['id'] = $run_c['id'].'~'.'com';
																//$ans_c_als[]
																$selcoms2_tagged[] = $run_c;
															}
														}
													}
												}
											}
										}
										
										$final_eves = array_merge($get_ever_id,$selart_stagged,$selcom_stagged,$get_onlycre_rev,$get_aever_id,$get_only2cre_rev,$selarts2_tagged,$selcoms2_tagged);
										
										$sort = array();
										foreach($final_eves as $k=>$v) {
										
											//$create_c = strpos($v['creator'],'(');
											//$end_c = substr($v['creator'],0,$create_c);
											
											//$sort['creator'][$k] = $end_c;
											//$sort['from'][$k] = $end_f;
											//$sort['track'][$k] = $v['track'];
											$sort['date'][$k] = $v['date'];
										}
										//var_dump($sort);
										if(!empty($sort))
										{
											array_multisort($sort['date'], SORT_DESC,$final_eves);
										}
										
										$dummy_ev = array();
										$get_all_del_id = $newgeneral->get_deleted_project_id();
										$exp_del_id = explode(",",$get_all_del_id['deleted_event_id']);
										for($count_all=0;$count_all<count($final_eves);$count_all++){
											for($count_del=0;$count_del<count($exp_del_id);$count_del++){
												if($exp_del_id[$count_del]!=""){
													if($final_eves[$count_all]['id'] == $exp_del_id[$count_del]){
														$final_eves[$count_all] ="";
													}
												}
											}
										}
										
										if(!empty($final_eves))
										{
			?>
											<select title="Select media and profiles to display on your profile page." name="tagged_recorded_events[]" size="5" multiple="multiple" id="tagged_recorded_events[]" class="list">
			<?php
										for($ev_f=0;$ev_f<count($final_eves);$ev_f++)
										{
											if($final_eves[$ev_f]!="")
											{
												if(in_array($final_eves[$ev_f]['id'],$dummy_ev))
												{}
												else
												{
													$dummy_ev[] = $final_eves[$ev_f]['id'];
													if($get_select_events_rec[0]!="")
													{
														$get_select_events_rec = array_unique($get_select_events_rec);
														for($f=0;$f<count($get_select_events_rec);$f++)
														{
															if($get_select_events_rec[$f]==$final_eves[$ev_f]['id'])
															{
																$j_comm_acc=1;
			?>
																<option value="<?php echo $final_eves[$ev_f]['id'];?>" selected ><?php echo date("m.d.y", strtotime($final_eves[$ev_f]['date'])).' '.stripslashes($final_eves[$ev_f]['title']);?></option>
			<?php	
																continue;
															}
														}	
														
														if($j_comm_acc==1)
														{
															$j_comm_acc=0;
															continue;
														}
													}
			?>
													<option value="<?php echo $final_eves[$ev_f]['id'];?>" ><?php echo date("m.d.y", strtotime($final_eves[$ev_f]['date'])).' '.stripslashes($final_eves[$ev_f]['title']);?></option>
			<?php
												}
											}
										}
										
										//if(mysql_num_rows($get_eventrec)<=0 && empty($dummy_ev) && mysql_num_rows($get_aeeventrec)<=0)
			?>
										</select>
			<?php
									}
										if(empty($final_eves))
										{
										?>
											<a style="border-radius:5px; -webkit-border-radius:5px; -moz-border-radius:5px; -ms-border-radius:5px; -o-border-radius:5px; background-color: #000000; border: 0 none; color: #FFFFFF; font-size: 13px; margin-left: 5px; padding: 6px 8px; text-decoration: none;" href="add_community_event.php">Add Event</a>
										<?php	
										}
										?>
                                    
                                </div>
								
                                <div class="fieldCont">
                                    <div class="fieldTitle"></div>
                                    <a href="javascript:void(0);" onclick="return confirm_saving();"><input type="button" class="register" value="Save" style="border-radius:5px; -webkit-border-radius:5px; -moz-border-radius:5px; -ms-border-radius:5px; -o-border-radius:5px;" /></a>
                                   <!-- <input type="button" class="register" value="Publish" />-->
                                </div>
								<!--<div  style="float:right;" id="selected-types"></div>-->
                             </form>
                                 
                             
                        
						</div>
                    </div>
                </div>
      <!---End Of Profilie DIV-->
      
      
                <div class="subTabs" id="event" style="display:none;">
                	<script>
                        function test() {
							if (document.getElementById('venue').value == '1') {
								document.getElementById('venueDetails').style.display = 'block';
							} else {
								document.getElementById('venueDetails').style.display = 'none';
							}
						}
                    </script>
					
				 
					<h1><?php echo $res1['name']; ?> Events</h1>
					<div style="float:right;">
					<div id="mediaContent" style="width:685px;">
                    	<div class="topLinks" style="border-bottom:1px dotted #999;">
                            <div class="links" style="float:left;">
								<ul>
									<li><a href="add_community_event.php"><input type="button" value="Add New Event" style="border-radius:5px; -webkit-border-radius:5px; -moz-border-radius:5px; -ms-border-radius:5px; -o-border-radius:5px; color: rgb(255, 255, 255); font-family: 'futura-pt',sans-serif; font-size: 0.825em; font-weight: bold; padding: 6px 10px; float: left; margin-right: 10px; border: 0px none; background-color: rgb(0, 0, 0);" /></a></li>
								</ul>
							</div>
						</div>
					</div>
					<!--<a href="add_community_event.php">ADD NEW EVENT</a>--></div>	
                    <!--<div style="width:665px; float:left;">-->
						<div id="mediaContent" style="width:685px;">
                        	<div class="titleCont">
								<div class="blkB" style="width:60px; padding-left:6px;">Date</div>
								<div class="blkB" style="width:160px;">Title</div>
								<div class="blkB" style="width:110px;">Time</div>
								<div class="blkB" style="width:140px;">Venue</div>
								<!--<div class="blkB" style="width:200px">Creator</div>-->
								<div class="blkI" style="width:110px;">Type</div>
                            </div>
						</div>
						<div id="mediaContent" style="width:685px; height:400px;overflow-x:hidden; overflow-y:auto;">
							<div class="tableCont" style="border-bottom:none;">
							<?php
								/* while($showevent=mysql_fetch_assoc($getevent))
								{
									$get_event_type=$newgeneral->Get_Event_type($showevent['id']);
								?>
								<div id="AccordionContainer" class="tableCont">
									<div class="blkB" style=""><?php if($showevent['date']!=""){ echo date("d.m.y", strtotime($showevent['date'])); } else{?>&nbsp;<?php } ?></div>
									<div class="blkB" style="width:200px"><a href="/<?php echo $showevent['profile_url'];?>" onclick="return chk_profile_url('<?php echo $showevent['profile_url'];?>')"><?php if($showevent['title']!=""){echo $showevent['title'];}else{?>&nbsp;<?php }?></a></div>
									<!--<div class="blkB" style="width:200px"><a href=""><?php if($nameres['name']!=""){echo $nameres['name'];}else{?>&nbsp;<?php }?></a></div>-->
									<div class="blkB" style="width:100px"><a href=""><?php if($get_event_type['name']!=""){echo $get_event_type['name'];}else{?>&nbsp;<?php }?></a></div>
									<div class="blkD"><a href="/<?php echo $showevent['profile_url'];?>" onclick ='return chk_profile_url('<?php echo $showevent['profile_url'];?>')'>Display</a></div>
									<div class="icon"><a href="add_community_event.php?id=<?php echo $showevent['id'];?>"><img src="images/profile/edit.png" /></a></div>
									<div class="icon"><a onclick="return confirmdelete('<?php echo $showevent['title'];?>')" href="insertcommunityevent.php?delete=<?php echo $showevent['id'];?>"><img src="images/profile/delete.png" /></a></div>
								</div>
								<?php
								}
								$get_all_friends_event = $newgeneral->Get_all_friends();
								while($res_all_friends_event = mysql_fetch_assoc($get_all_friends_event))
								{
									$get_friend_art_info_event = $newgeneral->get_friend_art_info($res_all_friends_event['fgeneral_user_id']);
									$get_all_tag_event = $newgeneral->get_all_tagged_event($get_friend_art_info_event);
									if(mysql_num_rows($get_all_tag_event)>0)
									{
										while($res_events = mysql_fetch_assoc($get_all_tag_event))
										{
											$get_event_type = $newgeneral->Get_Event_type($res_events['id']);
											$exp_creator = explode('(',$res_events['creator']);
											$exp_tagged_email = explode(",",$res_events['tagged_user_email']);
											for($exp_count = 0;$exp_count<count($exp_tagged_email);$exp_count++)
											{
												if($exp_tagged_email[$exp_count]==""){continue;}
												else{
													if($exp_tagged_email[$exp_count]==$_SESSION['login_email'])
													{
														?>
														<div id="AccordionContainer" class="tableCont">
															<div class="blkB" style=""><?php if($showevent['date']!=""){ echo date("d.m.y", strtotime($showevent['date'])); } else{?>&nbsp;<?php } ?></div>
															<div class="blkB" style="width:200px;"><?php if($res_events['title']!=""){echo $res_events['title'];}else{?>&nbsp;<?php }?></div>
															<!--<div class="blkB" style="width:200px;"><a href=""><?php if($exp_creator[0]!=""){echo $exp_creator[0];}else{?>&nbsp;<?php }?></a></div>-->
															<div class="blkB" style="width:100px;"><?php if($get_event_type['name']!=""){echo $get_event_type['name'];}else{?>&nbsp;<?php }?></div>
															<div class="blkD">&nbsp;</div>
															<div class="icon">&nbsp;</div>
															<div class="icon"><a onclick="return confirmdelete('<?php echo $res_events['title'];?>')" href="insertcommunityevent.php?delete=<?php echo $res_events['id'];?>"><img src="images/profile/delete.png" /></a></div>
														</div>
														<?php
													}
												}
												
											}
										}
									}
								} */
								
								$gett_id = array();
								while($get_upev_ids = mysql_fetch_assoc($get_levent))
								{
									//$get_upev_ids['id'] = $get_upev_ids['id'].'~com';
									$get_upev_ids['edit_com'] = 'yes';
									$get_upev_ids['whos_event'] = "own_com_event";
									$get_upev_ids['id'] = $get_upev_ids['id'] ."~com";
									$gett_id[] = $get_upev_ids;
								}
								
								$getcrq_id = array();
								while($get_aupev_ids = mysql_fetch_assoc($get_laeevent))
								{
									//$get_aupev_ids['id'] = $get_aupev_ids['id'].'~art';
									$get_aupev_ids['whos_event'] = "own_art_event";
									$get_aupev_ids['id'] = $get_aupev_ids['id'] ."~art";
									$getcrq_id[] = $get_aupev_ids;
								}

								$getat_ev = array();
								$getat_ev = $newgeneral->only_creator_oupevent();
								
								$sel_ged = array();
								if($event_avail!=0)
								{
									$acc_event = array_unique($acc_event);
									for($h=0;$h<count($acc_event);$h++)
									{
										if($acc_event[$h]!="" && $acc_event[$h]!=0)
										{
											$dumcomu = $newgeneral->get_event_tagged_by_other_user($acc_event[$h]);
											if(!empty($dumcomu) && $dumcomu['id']!="" && $dumcomu['id']!="")
											{
												//$dumcomu['id'] = $dumcomu['id'].'~'.'com';
												$dumcomu['edit'] = 'yes';
												$dumcomu['edit_com'] = 'yes';
												$dumcomu['whos_event'] = "com_tag_eve";
												$dumcomu['id'] = $dumcomu['id'] ."~com";
												$sel_ged[] = $dumcomu;
											}	
										}
									}
								}
								
								$sela_ged = array();
								if($event_aavail!=0)
								{
									$acc_aevent = array_unique($acc_aevent);
									for($h=0;$h<count($acc_aevent);$h++)
									{
										if($acc_aevent[$h]!="" && $acc_aevent[$h]!=0)
										{
											$dumartu = $newgeneral->get_aevent_tagged_by_other_user($acc_aevent[$h]);
											if(!empty($dumartu) && $dumartu['id']!="" && $dumartu['id']!="")
											{
												//$dumartu['id'] = $dumartu['id'].'~'.'art';
												$dumartu['edit'] = 'yes';
												$dumartu['whos_event'] = "art_tag_eve";
												$dumartu['id'] = $dumartu['id'] ."~art";
												$sela_ged[] = $dumartu;
											}	
										}
									}
								}
								
								$getcr_id = array();
								while($get_ever_ids = mysql_fetch_assoc($get_leventrec))
								{
									//$get_ever_ids['id'] = $get_ever_ids['id'].'~com';
									$get_ever_ids['edit_com'] = 'yes';
									$get_ever_ids['whos_event'] = "own_com_event";
									$get_ever_ids['id'] = $get_ever_ids['id'] ."~com";
									$getcr_id[] = $get_ever_ids;											
								}
								
								$getarer_id = array();
								while($get_aever_ids = mysql_fetch_assoc($get_laeeventrec))
								{
									//$get_aever_ids['id'] = $get_aever_ids['id'].'~art';
									$get_aever_ids['whos_event'] = "own_art_event";
									$get_aever_ids['id'] = $get_aever_ids['id'] ."~art";
									$getarer_id[] = $get_aever_ids;											
								}
								
								$getgve_rev = array();
								$getgve_rev = $newgeneral->only_creator_orecevent();
								
								$get_only2cre_rev = array();
								//$get_only2cre_rev = $newgeneral->only_creator2_recevent();
								$get_only2cre_rev = $newgeneral->only_creator2_orecevent();
								
								$get_only2cre_ev = array();
								//$get_only2cre_ev = $newgeneral->only_creator2ev_upevent();
								$get_only2cre_ev = $newgeneral->only_creator2ev_oupevent();
								
								$selcomu_tc = array();
								if($event_avail!=0)
								{
									$acc_event = array_unique($acc_event);
									for($k=0;$k<count($acc_event);$k++)
									{
										if($acc_event[$k]!="" && $acc_event[$k]!=0)
										{
											$dumec = $newgeneral->get_event_tagged_by_other_userrec($acc_event[$k]);
											if(!empty($dumec) && $dumec['id']!="" && $dumec['id']!="")
											{
												if($newres1['community_id']!=$dumec['community_id'])
												{
													$dumec['id'] = $dumec['id'].'~'.'com';
													$dumec['edit'] = 'yes';
													$dumec['edit_com'] = 'yes';
													$dumec['whos_event'] = "com_tag_eve";
													$selcomu_tc[] = $dumec;
												}
											}												
										}
									}
								}
								
								$selart_yi = array();
								if($event_aavail!=0)
								{
									$acc_aevent = array_unique($acc_aevent);
									for($ki=0;$ki<count($acc_aevent);$ki++)
									{
										if($acc_aevent[$ki]!="" && $acc_aevent[$ki]!=0)
										{
											$dumc = $newgeneral->get_aevent_tagged_by_other_userrec($acc_aevent[$ki]);
											if(!empty($dumc) && $dumc['id']!="" && $dumc['id']!="")
											{
												if($newres1['artist_id']!=$dumc['artist_id'])
												{
													$dumc['id'] = $dumc['id'].'~'.'art';
													$dumc['edit'] = 'yes';
													$dumc['whos_event'] = "art_tag_eve";
													$selart_yi[] = $dumc;
												}
											}												
										}
									}
								}
								
								$selarts2u_tagged = array();
								if($project_aavail!=0)
								{
									$acc_aproject = array_unique($acc_aproject);
									for($acc_pro=0;$acc_pro<count($acc_aproject);$acc_pro++)
									{												
										if($acc_aproject[$acc_pro]!="" && $acc_aproject[$acc_pro]!=0)
										{													
											$sql_1_cre = $newgeneral->get_event_tagged_by_other2_user($acc_aproject[$acc_pro]);
											if($sql_1_cre!="")
											{
												if(mysql_num_rows($sql_1_cre)>0)
												{
													while($run = mysql_fetch_assoc($sql_1_cre))
													{
														if($newres1['artist_id']!=$run['artist_id'])
														{
															$run['whos_event'] = "art_creator_tag_eve";
															$run['id'] = $run['id'].'~'.'art';
															$selarts2u_tagged[] = $run;
														}
													}
												}
											}
											
											$sql_2_cre = $newgeneral->get_event_atagged_by_other2_user($acc_aproject[$acc_pro]);
											if($sql_2_cre!="")
											{
												if(mysql_num_rows($sql_2_cre)>0)
												{
													while($run_c = mysql_fetch_assoc($sql_2_cre))
													{
														if($newres1['community_id']!=$run_c['community_id'])
														{
															$run_c['whos_event'] = "com_creator_tag_eve";
															$run_c['id'] = $run_c['id'].'~'.'com';
															$selarts2u_tagged[] = $run_c;
														}
													}
												}
											}
										}
									}
								}
								
								$selcoms2u_tagged = array();
								if($project_avail!=0)
								{
									$acc_project = array_unique($acc_project);
									for($acc_pro=0;$acc_pro<count($acc_project);$acc_pro++)
									{												
										if($acc_project[$acc_pro]!="" && $acc_project[$acc_pro]!=0)
										{													
											$sql_1_cre = $newgeneral->get_aevent_tagged_by_other2_user($acc_project[$acc_pro]);
											if($sql_1_cre!="")
											{
												if(mysql_num_rows($sql_1_cre)>0)
												{
													while($run = mysql_fetch_assoc($sql_1_cre))
													{
														if($newres1['artist_id']!=$run['artist_id'])
														{
															$run['whos_event'] = "art_creator_tag_eve";
															$run['id'] = $run['id'].'~'.'art';
															$selcoms2u_tagged[] = $run;
														}
													}
												}
											}
											
											$sql_2_cre = $newgeneral->get_aevent_atagged_by_other2_user($acc_project[$acc_pro]);
											if($sql_2_cre !="" && $sql_2_cre!= Null)
											{
												if(mysql_num_rows($sql_2_cre)>0)
												{
													while($run_c = mysql_fetch_assoc($sql_2_cre))
													{
														if($newres1['community_id']!=$run_c['community_id'])
														{
															$run_c['whos_event'] = "com_creator_tag_eve";
															$run_c['id'] = $run_c['id'].'~'.'com';
															$selcoms2u_tagged[] = $run_c;
														}
													}
												}
											}
										}
									}
								}
								
								$selarts2r_tagged = array();
								if($project_aavail!=0)
								{
									$acc_aproject = array_unique($acc_aproject);
									for($acc_pro=0;$acc_pro<count($acc_aproject);$acc_pro++)
									{												
										if($acc_aproject[$acc_pro]!="" && $acc_aproject[$acc_pro]!=0)
										{													
											$sql_1_cre = $newgeneral->get_event_tagged_by_other2_userrec($acc_aproject[$acc_pro]);
											if($sql_1_cre!="")
											{
												if(mysql_num_rows($sql_1_cre)>0)
												{
													while($run = mysql_fetch_assoc($sql_1_cre))
													{
														if($newres1['artist_id']!=$run['artist_id'])
														{
															$run['whos_event'] = "art_creator_tag_eve";
															$run['id'] = $run['id'].'~'.'art';
															$selarts2r_tagged[] = $run;
														}
													}
												}
											}
											
											$sql_2_cre = $newgeneral->get_event_atagged_by_other2_userrec($acc_aproject[$acc_pro]);
											if($sql_2_cre!="")
											{
												if(mysql_num_rows($sql_2_cre)>0)
												{
													while($run_c = mysql_fetch_assoc($sql_2_cre))
													{
														if($newres1['community_id']!=$run_c['community_id'])
														{
															$run_c['whos_event'] = "com_creator_tag_eve";
															$run_c['id'] = $run_c['id'].'~'.'com';
															$selarts2r_tagged[] = $run_c;
														}
													}
												}
											}
										}
									}
								}
								
								$selcoms2r_tagged = array();
								if($project_avail!=0)
								{
									$acc_project = array_unique($acc_project);
									for($acc_pro=0;$acc_pro<count($acc_project);$acc_pro++)
									{												
										if($acc_project[$acc_pro]!="" && $acc_project[$acc_pro]!=0)
										{													
											$sql_1_cre = $newgeneral->get_aevent_tagged_by_other2_userrec($acc_project[$acc_pro]);
											if($sql_1_cre!="")
											{
												if(mysql_num_rows($sql_1_cre)>0)
												{
													while($run = mysql_fetch_assoc($sql_1_cre))
													{
														if($newres1['artist_id']!=$run['artist_id'])
														{
															$run['whos_event'] = "art_creator_tag_eve";
															$run['id'] = $run['id'].'~'.'art';
															$selcoms2r_tagged[] = $run;
														}
													}
												}
											}
											
											$sql_2_cre = $newgeneral->get_aevent_atagged_by_other2_userrec($acc_project[$acc_pro]);
											if($sql_2_cre !="" && $sql_2_cre!= Null)
											{
												if(mysql_num_rows($sql_2_cre)>0)
												{
													while($run_c = mysql_fetch_assoc($sql_2_cre))
													{
														if($newres1['community_id']!=$run_c['community_id'])
														{
															$run_c['whos_event'] = "com_creator_tag_eve";
															$run_c['id'] = $run_c['id'].'~'.'com';
															$selcoms2r_tagged[] = $run_c;
														}
													}
												}
											}
										}
									}
								}
								
								$finalc_eve = array_merge($selart_yi,$selcomu_tc,$getgve_rev,$getarer_id,$getcr_id,$sela_ged,$sel_ged,$getat_ev,$getcrq_id,$gett_id,$get_only2cre_rev,$get_only2cre_ev,$selcoms2u_tagged,$selarts2u_tagged,$selcoms2r_tagged,$selarts2r_tagged);
								
								$sort = array();
								foreach($finalc_eve as $k=>$v) {
								
									//$create_c = strpos($v['creator'],'(');
									//$end_c = substr($v['creator'],0,$create_c);
									
									//$sort['creator'][$k] = $end_c;
									//$sort['from'][$k] = $end_f;
									//$sort['track'][$k] = $v['track'];
									$sort['date'][$k] = $v['date'];
								}
								//var_dump($sort);
								if(!empty($sort))
								{
									array_multisort($sort['date'], SORT_DESC,$finalc_eve);
								}
								
								$dumyca_ev = array();
								$dumycc_ev = array();
								$get_all_del_id = $newgeneral->get_deleted_project_id();
								$exp_del_id = explode(",",$get_all_del_id['deleted_event_id']);
								for($count_all=0;$count_all<count($finalc_eve);$count_all++){
									for($count_del=0;$count_del<count($exp_del_id);$count_del++){
										if($exp_del_id[$count_del]!=""){
											if($finalc_eve[$count_all]['id'] == $exp_del_id[$count_del]){
												$finalc_eve[$count_all] ="";
											}
										}
									}
								}
								for($egp_f=0;$egp_f<count($finalc_eve);$egp_f++)
								{
									if($finalc_eve[$egp_f]!="")
									{
										$chk_ev_pe = 0;
										if(isset($finalc_eve[$egp_f]['community_id']))
										{
											$get_event_type = $newgeneral->Get_Event_subtype($finalc_eve[$egp_f]['id']);
											if(in_array($finalc_eve[$egp_f]['id'],$dumyca_ev))
											{
												$chk_ev_pe = 1;
											}
											else
											{
												$dumyca_ev[] = $finalc_eve[$egp_f]['id'];
											}
										}
										elseif(isset($finalc_eve[$egp_f]['artist_id']))
										{
											$get_event_type = $newgeneral->Get_ACEvent_subtype($finalc_eve[$egp_f]['id']);
											if(in_array($finalc_eve[$egp_f]['id'],$dumycc_ev))
											{
												$chk_ev_pe = 1;
											}
											else
											{
												$dumycc_ev[] = $finalc_eve[$egp_f]['id'];
											}
										}
										if($chk_ev_pe==1)
										{}
										else
										{										
			?>
											<div id="AccordionContainer" class="tableCont">
												<div class="blkB" style="width:60px;"><?php if($finalc_eve[$egp_f]['date']!=""){ echo date("m.d.y", strtotime($finalc_eve[$egp_f]['date'])); } else{?>&nbsp;<?php } ?></div>
												<div class="blkB" style="width:160px;"><?php 
												if($finalc_eve[$egp_f]['profile_url']!="")
												{ 
												?><a href="/<?php echo $finalc_eve[$egp_f]['profile_url'];?>" onclick="return chk_profile_url('<?php echo $finalc_eve[$egp_f]['profile_url'];?>')"><?php if($finalc_eve[$egp_f]['title']!=""){echo $finalc_eve[$egp_f]['title'];}else{?>&nbsp;<?php }?></a>
												<?php
												}
												else{ if($finalc_eve[$egp_f]['title']!=""){echo $finalc_eve[$egp_f]['title'];}else{?>&nbsp;<?php } }
												?>
												</div>
												<div class="blkB" style="width:110px;"><?php if($finalc_eve[$egp_f]['start_time']!="" && $finalc_eve[$egp_f]['end_time']!="") { echo $finalc_eve[$egp_f]['start_time'].$finalc_eve[$egp_f]['start_timeap'].' - '.$finalc_eve[$egp_f]['end_time'].$finalc_eve[$egp_f]['end_timeap']; } else{?>&nbsp;<?php } ?></div>
												<div class="blkB" style="width:140px;"><?php
												if($finalc_eve[$egp_f]['venue_name']!="")
												{
													if($finalc_eve[$egp_f]['venue_website']!="" && $finalc_eve[$egp_f]['venue_website']!="http://" && $finalc_eve[$egp_f]['venue_website']!="https://")
													{
														echo '<a target="_blank" href='.$finalc_eve[$egp_f]['venue_website'].'>'.$finalc_eve[$egp_f]['venue_name'].'</a>';
													}
													else
													{
														echo $finalc_eve[$egp_f]['venue_name']; 
													}
												}
												else{?>&nbsp;<?php } ?></div>
												<!--<div class="blkB" style="width:200px"><a href=""><?php if($nameres['name']!=""){echo $nameres['name'];}else{?>&nbsp;<?php }?></a></div>-->
												<div class="blkB" style="width:110px"><a href=""><?php if($get_event_type['name']!=""){echo $get_event_type['name'];}else{?>&nbsp;<?php }?></a></div>
												<!--<div class="blkD"><a href="/<?php echo $finalc_eve[$egp_f]['profile_url'];?>" onclick="return chk_profile_url('<?php echo $finalc_eve[$egp_f]['profile_url'];?>')">Display</a></div>-->
												<?php
												if($finalc_eve[$egp_f]['whos_event']=="own_art_event")
												{
													$exp_art_id = explode("~",$finalc_eve[$egp_f]['id']);
												?>
												<div class="icon"><a href="add_artist_event.php?id=<?php echo $exp_art_id[0];?>"><img src="images/profile/edit.png" /></a></div>
												<div class="icon"><a onclick="return confirmdelete('<?php echo $finalc_eve[$egp_f]['title'];?>')" href="insertartistevent.php?delete=<?php echo $exp_art_id[0];?>&whos_event=<?php echo $finalc_eve[$egp_f]['whos_event'];?>"><img src="images/profile/delete.png" /></a></div>
												<?php
												}else if($finalc_eve[$egp_f]['whos_event']=="own_com_event")
												{
													$exp_com_id = explode("~",$finalc_eve[$egp_f]['id']);
												?>
												<div class="icon"><a href="add_community_event.php?id=<?php echo $exp_com_id[0];?>"><img src="images/profile/edit.png" /></a></div>
												<div class="icon"><a onclick="return confirmdelete('<?php echo $finalc_eve[$egp_f]['title'];?>')" href="insertcommunityevent.php?delete=<?php echo $exp_com_id[0];?>&whos_event=<?php echo $finalc_eve[$egp_f]['whos_event'];?>"><img src="images/profile/delete.png" /></a></div>
												<?php
												}else if($finalc_eve[$egp_f]['whos_event']=="com_creator_tag_eve" || $finalc_eve[$egp_f]['whos_event']=="art_creator_tag_eve" || $finalc_eve[$egp_f]['whos_event']=="com_tag_eve" || $finalc_eve[$egp_f]['whos_event']=="art_tag_eve" || $finalc_eve[$egp_f]['whos_event']=="only_creator" || $finalc_eve[$egp_f]['whos_event']=="art_creator_creator_eve" || $finalc_eve[$egp_f]['whos_event']=="com_creator_creator_eve")
												{
												?>
												<div class="icon">&nbsp;</div>
												<div class="icon"><a onclick="return confirmdelete('<?php echo $finalc_eve[$egp_f]['title'];?>')" href="insertcommunityevent.php?delete=<?php echo $finalc_eve[$egp_f]['id'];?>&whos_event=<?php echo $finalc_eve[$egp_f]['whos_event'];?>"><img src="images/profile/delete.png" /></a></div>
												<?php
												}
												if($finalc_eve[$egp_f]['profile_url']!="")
												{ 
												?>
													<div class="icon"><a href="/<?php echo $finalc_eve[$egp_f]['profile_url'];?>" onclick="return chk_profile_url('<?php echo $finalc_eve[$egp_f]['profile_url'];?>')"><img width="20" height="20" src="images/magnifying_glass_icon.gif" style=""/></a></div>
												<?php 
												}
												else
												{
												?>
													<div class="icon"><img width="20" height="20" src="images/magnifying_glass_icon.gif" style="position:relative;top:5px;"/></div>
												<?php
												}
												
												
												/*
												Mithesh Code
												if(!isset($finalc_eve[$egp_f]['edit']))
												{
			?>
													<div class="blkD"><a href="/<?php echo $finalc_eve[$egp_f]['profile_url'];?>" onclick ='return chk_profile_url('<?php echo $finalc_eve[$egp_f]['profile_url'];?>')'>Display</a></div>
			<?php
													if(!isset($finalc_eve[$egp_f]['edit_com']))
													{
			?>
														<div class="icon"><a href="add_artist_event.php?id=<?php echo $finalc_eve[$egp_f]['id'];?>"><img src="images/profile/edit.png" /></a></div>
			<?php
													}
													else
													{
			?>
														<div class="icon"><a href="add_community_event.php?id=<?php echo $finalc_eve[$egp_f]['id'];?>"><img src="images/profile/edit.png" /></a></div>
			<?php
													}
												}
												else
												{
			?>
													<div class="blkD">&nbsp;</div>
													<div class="icon">&nbsp;</div>
			<?php
												}
												if(!isset($finalc_eve[$egp_f]['edit_com']))
												{
			?>
													<div class="icon"><a onclick="return confirmdelete('<?php echo $finalc_eve[$egp_f]['title'];?>')" href="insertartistevent.php?delete=<?php echo $finalc_eve[$egp_f]['id'];?>&whos_event=<?php echo $finalc_eve[$egp_f]['whos_event'];?>"><img src="images/profile/delete.png" /></a></div>
			<?php
												}
												else
												{
			?>
													<div class="icon"><a onclick="return confirmdelete('<?php echo $finalc_eve[$egp_f]['title'];?>')" href="insertcommunityevent.php?delete=<?php echo $finalc_eve[$egp_f]['id'];?>&whos_event=<?php echo $finalc_eve[$egp_f]['whos_event'];?>"><img src="images/profile/delete.png" /></a></div>
			<?php
												}*/
			?>
											</div>
			<?php
										}
									}
								}
							?>
								
                            </div>
                        </div>
                    <!--</div>-->
					<!--<p><strong>Projects Published </strong></p>
					<p>Lee Rick Un-Titled<em> <em>(<a href="#">edit</a>/<a href="#">profile</a>) </em></em><br />
					Lee Rick's Remix Vol. 2  <em> <em>(<a href="#">edit</a>/<a href="#">profile</a>) </em><br />
					</em>Lee Rick's Collaborations<em> <em>(<a href="#">edit</a>/<a href="#">profile</a>) </em><br />
					</em>Lee Rick Self-Titled<em> <em>(<a href="#">edit</a>/<a href="#">profile</a>) </em><br />
					</em>Lee Rick's Remix Vol. 1 <em> <em> <em>(<a href="#">edit</a>/<a href="#">profile</a>) </em></em></em></p>
					<p><strong>Add/Edit Project</strong></p>
					<p><strong>Name:</strong> Lee Rick Un-Titled  <br />
					<strong>Title Image:</strong> <em><img src="images/Projects/musicrapapratuscontinued.jpg" width="50" height="50" /></em><br />
					<strong>Description:</strong> Lee Rick's second solo release. Production is better, raps are better, lyrics are better. <br />
					<strong>Email:</strong> mcleerick@gmail.com <br />
					<strong>Homepage:</strong> <a href="https://CypressGroove.com" target="_blank">CypressGroove.com </a><br />
					<strong>Type/Subtypes: </strong>Type Form<br />
					<strong>Country: </strong>US <strong><br />
					State/Province: </strong>CA <strong><br />
					City:</strong> Moss Beach <br />
					<strong>Add Artist: </strong>Choose a Registered Artist<strong><br />
					Artists:</strong><img src="images/Artists/kaz.jpg" alt="leerick" width="50" height="50" /><br />
					<strong>Add Media: </strong>choose from users original media <strong> <br />
					Media:<br />
					</strong><a href="mp3s/Leerick_Goldenchildren.mp3">Lee Rick_UnTitled_01_Anti-Terror.mp3</a>, (delete) <br />
					<a href="mp3s/Leerick_Goldenchildren.mp3">Lee Rick_UnTitled_02_DrumitUp.mp3</a> (delete) <br />
					<a href="mp3s/Leerick_Goldenchildren.mp3">Lee Rick_UnTitled_03_BecomeOne.mp3</a> (delete) <br />
					<a href="mp3s/Leerick_Goldenchildren.mp3">Lee Rick_UnTitled_04_GoldenChildren.mp3</a> (delete) <br />
					<a href="mp3s/Leerick_Goldenchildren.mp3">Lee Rick_UnTitled_05_Sunrise.mp3</a> (delete) <br />
					<a href="mp3s/Leerick_Goldenchildren.mp3">Lee Rick_UnTitled_06_TheGreatEscape.mp3</a> (delete) <br />
					<a href="mp3s/Leerick_Goldenchildren.mp3">Lee Rick_UnTitled_07_TheRatRace.mp3</a> (delete) <br />
					<a href="mp3s/Leerick_Goldenchildren.mp3">Lee Rick_UnTitled_08_TijuanaTrash.mp3</a> (delete) <br />
					<a href="mp3s/Leerick_Goldenchildren.mp3">Lee Rick_UnTitled_09_UnTitled.mp3 </a>(delete) <a href="mp3s/Leerick_Goldenchildren.mp3"><br />
					Lee Rick_UnTitled_10_EssentialElement.mp3</a> (delete) <br />
					<a href="mp3s/Leerick_Goldenchildren.mp3">Lee Rick_UnTitled_01_Euphoric.mp3</a>  (delete) <br />
					<a href="mp3s/Leerick_Goldenchildren.mp3">Lee Rick_UnTitled_Cover.jpeg  </a>(delete) <a href="mp3s/Leerick_Goldenchildren.mp3"><br />
					Lee Rick_UnTitled_TrackListing.jpeg  </a>(delete) <a href="mp3s/Leerick_Goldenchildren.mp3"><br />
					Lee Rick_UnTitled_LeeRick.jpeg  </a>(delete) <a href="mp3s/Leerick_Goldenchildren.mp3"><br />
					Lee Rick_UnTitled_Live.jpeg</a>  (delete) <a href="mp3s/Leerick_Goldenchildren.mp3"><br />
					Video Feed, Live at the Gypsy (link) <br />
					<br />
					</a>
					<strong>Add Community Member:</strong> Choose a Registered Community Member<strong><br />
					Community Members:</strong><img src="images/Friends/studiocypressgroove.jpg" width="100" height="67" /><br />
					<strong>Add Related Projects:</strong> choose a published project <br />
					<strong>Related Projects:</strong><a href="#"> Rapaparatus Cont., Lee Rick Self Titled, Kaz Taking Out The Trash </a><a href="#"><br />
					</a><strong>Add Related Events:</strong> choose a published event<br />
					<strong>Related Events:</strong><a href="#"> 03.24.09 Lee Rick @ El Rincon, SF, CA</a>; <a href="#">03.12.09 Rapaparatus  @ HMB Community Center, HMB, CA </a><br />
					<strong>Add Related Service:</strong> choose a published service<br />
					<strong>Related Services: </strong><a href="#">Cypress Groove Studio Time</a> - <a href="#">Hadley Custom Cermics</a></p>
					<p><strong>Pricing</strong></p>
					<p><strong>Date: </strong>n/a<strong><br />
					Frequency:</strong> One Time  (<a href="#">change</a>) <br />
					<strong>Interval Length</strong>: n/a <br />
					<strong>Cost per Interval: </strong><strong> </strong>100 Purify Points (<a href="#">change</a>)<br />
					<strong>Total Intervals: </strong>n/a<br />
					<strong>Total Cost: </strong>100 Purify Points<br />
					<strong>Point Sharing:</strong> 55% to Me, 25% to Kaz, 25% to Granny, 5% to Purify<br />
					<strong>Description:</strong> Purchase Lee Rick's second solo release, an instant classic, today. </p>
					<p><strong>Publish</strong> <em>(when the project is published it is added to the database and listed on the page username_artist_projects)</em></p>-->
				</div>
		       
                <div class="subTabs" id="services" style="display:none;">
                	<h1>Add/Edit "Artist Name" Service</h1>
                    <div style="width:665px; float:left;">
                    	<div id="actualContent">
                        	<form>
                        	<div class="fieldCont">
                           		<div class="fieldTitle">Service offered</div>
                                <select name="select-category-artist-subtype" size="5" multiple="multiple" id="subtype-select" class="list">
                                   	<option value="0" selected="selected">Select Type</option>					
                    			</select>
                    		</div>
                            <div class="fieldCont">
                           	  	<div class="fieldTitle">Title</div>
                                <input name="homepage" type="text" class="fieldText" id="datepickerB">
                    		</div>
                            <div class="fieldCont">
                           	  	<div class="fieldTitle">Image</div>
                                <input name="homepage" type="file" class="fieldText" id="datepickerB">
                    		</div>
                            <div class="fieldCont">
                           	  	<div class="fieldTitle">Email</div>
                                <input name="homepage" type="text" class="fieldText" id="datepickerB">
                    		</div>
                            <div class="fieldCont">
                           	  	<div class="fieldTitle">Homepage</div>
                                <input name="homepage" type="text" class="fieldText" id="datepickerB">
                    		</div>
                            <div class="fieldCont">
                           	  	<div class="fieldTitle">Phone</div>
                                <input name="homepage" type="text" class="fieldText" id="datepickerB">
                    		</div>
                            <div class="fieldCont">
                                <div class="fieldTitle">Address</div>
                                <textarea class="textbox"></textarea>
                            </div>
                            <div class="fieldCont">
                            	<div class="fieldTitle">Type</div>
                                <select name="select-category-artist-subtype" class="dropdown" id="subtype-select">
                                   	<option value="0" selected="selected">Select Type</option>					
                    			</select>
                    		</div>
                            <div class="fieldCont">
                                <div class="fieldTitle">Sub Type</div>
                                <select name="select-category-artist-metatype" class="dropdown" id="metatype-select">
                                	<option value="0" selected="selected">Select Sybtype</option>					
                    			</select>
                            </div>
                            <div class="fieldCont">
                                <div class="fieldTitle">Description</div>
                                <textarea class="textbox"></textarea>
                            </div>
                            <div class="fieldCont">
                           		<div class="fieldTitle">Add Media</div>
                                <select name="select-category-artist-subtype" size="5" multiple="multiple" id="subtype-select" class="list">
                                   	<option value="0" selected="selected">Select Type</option>					
                    			</select>
                    		</div>
                            <div class="fieldCont">
                           		<div class="fieldTitle">Media</div>
                                <select name="select-category-artist-subtype" size="5" multiple="multiple" id="subtype-select" class="list">
                                   	<option value="0" selected="selected">Select Type</option>					
                    			</select>
                    		</div>
                            <div class="fieldCont">
                           		<div class="fieldTitle">Add Artist &amp; Community Members Worked With</div>
                                <select name="select-category-artist-subtype" size="5" multiple="multiple" id="subtype-select" class="list">
                                   	<option value="0" selected="selected">Select Type</option>					
                    			</select>
                    		</div>
                            <div class="fieldCont">
                           		<div class="fieldTitle">Artist &amp; Community Members Worked With</div>
                                <select name="select-category-artist-subtype" size="5" multiple="multiple" id="subtype-select" class="list">
                                   	<option value="0" selected="selected">Select Type</option>					
                    			</select>
                    		</div>
                            <div class="fieldCont">
                           		<div class="fieldTitle">Related Events</div>
                                <select name="select-category-artist-subtype" size="5" multiple="multiple" id="subtype-select" class="list">
                                   	<option value="0" selected="selected">Select Type</option>					
                    			</select>
                    		</div>
                            <div class="fieldCont">
                           		<div class="fieldTitle">Add Related Events</div>
                                <select name="select-category-artist-subtype" size="5" multiple="multiple" id="subtype-select" class="list">
                                   	<option value="0" selected="selected">Select Type</option>					
                    			</select>
                    		</div>
                            <div class="fieldCont">
                           		<div class="fieldTitle">Add Projects Worked On</div>
                                <select name="select-category-artist-subtype" size="5" multiple="multiple" id="subtype-select" class="list">
                                   	<option value="0" selected="selected">Select Type</option>					
                    			</select>
                    		</div>
                            <div class="fieldCont">
                           		<div class="fieldTitle">Add Related Services</div>
                                <select name="select-category-artist-subtype" size="5" multiple="multiple" id="subtype-select" class="list">
                                   	<option value="0" selected="selected">Select Type</option>					
                    			</select>
                    		</div>
                            <div class="fieldCont">
                           		<div class="fieldTitle">Related Services</div>
                                <select name="select-category-artist-subtype" size="5" multiple="multiple" id="subtype-select" class="list">
                                   	<option value="0" selected="selected">Select Type</option>					
                    			</select>
                    		</div>
                            <div class="fieldCont">
                           		<div class="fieldTitle"><b>Pricing</b></div>
                            </div>
                            <div class="fieldCont">
                           	  	<div class="fieldTitle">Date</div>
                                <input name="homepage" type="text" class="fieldText" id="datepickerB">
                    		</div>
                            <div class="fieldCont">
                           	  	<div class="fieldTitle">Interval</div>
                                <input name="homepage" type="text" class="fieldText" id="datepickerB">
                    		</div>
                            <div class="fieldCont">
                           	  	<div class="fieldTitle">Interval Length</div>
                                <input name="homepage" type="text" class="fieldText" id="datepickerB">
                    		</div>
                            <div class="fieldCont">
                           	  	<div class="fieldTitle">Cost Per Interval</div>
                                <input name="homepage" type="text" class="fieldText" id="datepickerB">
                    		</div>
                            <div class="fieldCont">
                           	  	<div class="fieldTitle">Total Intervals</div>
                                <input name="homepage" type="text" class="fieldText" id="datepickerB">
                    		</div>
                            <div class="fieldCont">
                           	  	<div class="fieldTitle">Total Cost</div>
                                <input name="homepage" type="text" class="fieldText" id="datepickerB">
                    		</div>
                            <div class="fieldCont">
                           		<div class="fieldTitle">Point Sharing</div>
                                <select name="select-category-artist-subtype" size="5" multiple="multiple" id="subtype-select" class="list">
                                   	<option value="0" selected="selected">Select Type</option>					
                    			</select>
                    		</div>
                            <div class="fieldCont">
                                <div class="fieldTitle">Description</div>
                                <textarea class="textbox"></textarea>
                            </div>
                            <div class="fieldCont">
                                <div class="fieldTitle"></div>
                                <input type="button" class="register" style="border-radius:5px; -webkit-border-radius:5px; -moz-border-radius:5px; -ms-border-radius:5px; -o-border-radius:5px;" value="Save" />
                            	<input type="button" style="border-radius:5px; -webkit-border-radius:5px; -moz-border-radius:5px; -ms-border-radius:5px; -o-border-radius:5px;" class="register" value="Publish" />
                            </div>
                            </form>
                        </div>
                        <div style="float:right;" id="selected-types"></div>
                    </div>
                </div>
                
                
				
				
					
					<div class="subTabs" id="memberships" style="display:none;">
					<h1><?php echo $res1['name']; ?> Memberships</h1>
					<div style="width:665px; float:left;">
						
						<div id="actualContent">
							<form action="create_community_member.php" method="POST">
								<div class="fieldCont">
									<div title="Choose to sell annual or lifetime fan club memberships to users who will be able to access exclusive media you select below." class="fieldTitle">Frequency</div>
									<select title="Choose to sell annual or lifetime fan club memberships to users who will be able to access exclusive media you select below." name="frequency" class="dropdown" id="frequency">
										<option value="annual">Annual</option>
										<option value="lifetime">LifeTime</option>
									</select>
								</div>
								<div class="fieldCont">
									<div class="fieldTitle">Cost</div>
									<input name="cost" type="text" class="fieldText" id="cost" />
								</div>
								<div class="fieldCont">
									<div class="fieldTitle">Description</div>
									<!--<input name="description" type="text" class="fieldText" id="description" />
									<textarea  class="jquery_ckeditor" cols="4" id="description" name="description" rows="10"></textarea>-->
								</div>
								<?php $get_community_fan = $newgeneral->Get_community_fan($res1['community_id']);
								$get_community_total_fan = $newgeneral->Get_community_total_fan($res1['community_id']);
								?>
								<div class="fieldCont">Current Fan Club Memberships: <?php echo $get_community_fan;?> ($864/year)</div>
                                <div class="fieldCont">Total Fan Club Memberships: <?php echo $get_community_total_fan;?> ($4068 to Date)</div>
                                
								<div class="fieldCont">
									<div class="fieldTitle"></div>
									<input type="submit" class="register" style="border-radius:5px; -webkit-border-radius:5px; -moz-border-radius:5px; -ms-border-radius:5px; -o-border-radius:5px;" value="Save" />
									<input type="button" style="border-radius:5px; -webkit-border-radius:5px; -moz-border-radius:5px; -ms-border-radius:5px; -o-border-radius:5px;" class="register" value="Publish" />
								</div>
								
							</form>
						</div>
					</div>
				</div>
				
				
				
				
				
                
               <!--	<div class="subTabs" id="memberships" style="display:none;">
					<h1>"Artist Name" Memberships</h1>
					<div style="width:665px; float:left;">
						<div id="actualContent">
							<form>
								<div class="fieldCont">
									<div class="fieldTitle">Frequency</div>
									<select name="select-category-artist-metatype" class="dropdown" id="metatype-select">
										<option value="0">Interval</option>
										<option value="0">LifeTime</option>
									</select>
								</div>
								<div class="fieldCont">
									<div class="fieldTitle">Interval Length</div>
									<input name="homepage" type="text" class="fieldText">
								</div>
								<div class="fieldCont">
									<div class="fieldTitle">Cost</div>
									<input name="homepage" type="text" class="fieldText">
								</div>
								<div class="fieldCont">
									<div class="fieldTitle">Description</div>
									<input name="homepage" type="text" class="fieldText">
								</div>
								<div class="fieldCont">
									<div class="fieldTitle">Point Sharing</div>
									<select name="select-category-artist-subtype" size="5" multiple="multiple" id="subtype-select" class="list">
										<option value="0" selected="selected">Select Type</option>					
									</select>
								</div>
								<div class="fieldCont">
									<div class="fieldTitle"></div>
									<input type="button" class="register" value="Save" />
									<input type="button" class="register" value="Publish" />
								</div>
								<div class="fieldCont">
									<b>Add/Edit Other Membership</b>
								</div>
								<div class="fieldCont">
									<div class="fieldTitle">Frequency</div>
									<select name="select-category-artist-metatype" class="dropdown" id="metatype-select">
										<option value="0">Interval</option>
										<option value="0">LifeTime</option>
									</select>
								</div>
								<div class="fieldCont">
									<div class="fieldTitle">Interval Length</div>
									<input name="homepage" type="text" class="fieldText">
								</div>
								<div class="fieldCont">
									<div class="fieldTitle">Cost</div>
									<input name="homepage" type="text" class="fieldText">
								</div>
								<div class="fieldCont">
									<div class="fieldTitle">Description</div>
									<input name="homepage" type="text" class="fieldText">
								</div>
								<div class="fieldCont">
									<div class="fieldTitle">Point Sharing</div>
									<select name="select-category-artist-subtype" size="5" multiple="multiple" id="subtype-select" class="list">
										<option value="0" selected="selected">Select Type</option>					
									</select>
								</div>
								<div class="fieldCont">
									<div class="fieldTitle"></div>
									<input type="submit" class="register" value="Save" />
									<input type="button" class="register" value="Publish" />
								</div>
							</form>
						</div>
					</div>
					
					<p><strong>Edit Lee Rick Membership </strong></p>
					  <p><strong>Frequency:</strong> Interval or LifeTime <br />
						<strong>Interval Length</strong>: 1Hr (if Interval is chosen) <br />
						<strong>Cost: </strong>300 Purify Points
						<br />
					  <strong>Description:</strong> Text<br />
					  <strong>Share Points: </strong>Select a user and percentage to add <strong><br />
					  Point Sharing<br />
					  </strong>55% to Me<br />
					  25% to Kaz (Edit/Remove)<br />
					  25% to Granny (Edit/Remove)<br />
					  5% to Purify (Edit, Can make 0% but cannot remove)</p>
					  <p>Publish/Save</p>
					  <p><strong>Total Points Received from Lee Rick Membership: 5000 Points </strong></p>
					  <p><strong>Add/Edit Other Membership  </strong></p>
					  <p><strong>Title:<br />
						Frequency:</strong> Interval or LifeTime <br />
						<strong>Interval Length</strong>: 1Hr (if Interval is chosen) <br />
						<strong>Cost: </strong>300 Purify Points <br />
						<strong>Description:</strong> Text<br />
						<strong>Share Points: </strong>Select a user and percentage to add <strong><br />
					Point Sharing<br />
						</strong>55% to Me<br />
					25% to Kaz (Edit/Remove)<br />
					25% to Granny (Edit/Remove)<br />
					5% to Purify (Edit, Can make 0% but cannot remove)</p>
					  <p>Publish/Save</p>
					  <p><strong>Other Membership </strong></p>
					  <p><a href="#">Lee Rick Un-Titled</a> (Edit) <br />
						LifeTime, 100 Points, 30 Subscribers, 3000 Total Points Received (2000 to Lee Rick, 500 to Kaz, 500 to Purify)<br />
						<br />
						<a href="#">Lee Rick Self-Titled</a> (Edit)<a href="#"><br />
						</a>LifeTime, 100 Points, 20 Subscribers, 2000 Total Points Received (1000 to Lee Rick, 750 to Kaz, 250 to Purify)<br />
					<br />
					  <a href="#">03.12.09 Lee Rick Performance</a> (Edit)<br />
					  Interval,<a href="#"> El Rincon Club</a>, 500 Points, <a href="#">Confirm</a>, Paymet Not Received</p>
					  <p><strong>Total Points Received from Other Membership: 5000 Points
						</strong><em><a href="#"><br />
						</a></em></p>
				</div>-->
                
                
                <div class="subTabs" id="projects" style="display:none;">
					<h1><?php echo $new_community['name']; ?> Projects</h1>
					<div style="float:left;">
					<div id="mediaContent"  style="width:685px;">
                    	<div class="topLinks" style="border-bottom:1px dotted #999;">
                            <div class="links" style="float:left;">
								<ul>
									<li><a href="add_community_project.php"><input type="button" value="Add Project" style="border-radius:5px; -webkit-border-radius:5px; -moz-border-radius:5px; -ms-border-radius:5px; -o-border-radius:5px; color: rgb(255, 255, 255); font-family: 'futura-pt',sans-serif; font-size: 0.825em; font-weight: bold; padding: 6px 10px; float: left; margin-right: 10px; border: 0px none; background-color: rgb(0, 0, 0);" /></li>
									<!--<li><a href="profile_artist.html"><input type="button" value="View Profile" /></a></li>-->
								</ul>
							</div>
						</div>
					</div>
					<!--<a href="add_community_project.php">ADD NEW PROJECT</a>-->
					</div>
                   <!-- <div style="width:665px; float:left;">-->
						<div id="mediaContent"  style="width:685px;">
                        	<div class="titleCont">
								<div class="blkG" style="width:272px;">Title</div>
								<div class="blkE" >Creator</div>
								<div class="blkI" style="width:110px">Type</div>
                            </div>
						</div>
						<div id="mediaContent" style="width:685px; height:400px;overflow-x:hidden; overflow-y:auto;">
							<?php
							$all_community_project = array();
							if(mysql_num_rows($getproject)>0)
							{
								while($showproject=mysql_fetch_assoc($getproject))
								{
									$exp_creator= explode('(',$showproject['creator']);
									//$get_project_type=$newgeneral->Get_Project_type($showproject['id']);
									//$showproject['project_type_name'] = $get_project_type['name'];
									$showproject['whos_project'] = "own_community";
									$showproject['id'] = $showproject['id'] . "~com";
									$all_community_project[] = $showproject;
								?>
									<!--<div id="AccordionContainer" class="tableCont">
										<div class="blkG" style="width:200px;"><?php /*if($showproject['title']!=""){echo $showproject['title'];}else{?> &nbsp;<?php }?></div>
										<div class="blkB" style="width:200px;"><a href=""><?php if($exp_creator[0]!=""){echo $exp_creator[0];} else{?> &nbsp;<?php }?></a></div>
										<div class="blkB" style="width:100px"><?php if($get_project_type['name']!=""){echo $get_project_type['name'];}else{?> &nbsp;<?php }?></div>
										<div class="blkD"><a href="/<?php echo $showproject['profile_url'];?>" onclick="return chk_profile_url('<?php echo $showproject['profile_url'];?>')">Display</a></div>
										<div class="icon"><a href="add_community_project.php?id=<?php echo $showproject['id'];?>"><img src="images/profile/edit.png" /></a></div>
										<div class="icon"><a onclick="return confirmdelete('<?php echo $showproject['title']; ?>')" href="insertcommunityproject.php?delete=<?php echo $showproject['id'];*/?>"><img src="images/profile/delete.png" /></a></div>
									</div>-->
								<?php
								}
							}
							
							$all_community_friend_project = array();
							$get_all_friends = $newgeneral->Get_all_friends();
							if($get_all_friends!="")
							{
								while($res_all_friends = mysql_fetch_assoc($get_all_friends))
								{
									$get_friend_art_info = $newgeneral->get_friend_art_info($res_all_friends['fgeneral_user_id']);
									$get_all_tag_pro = $newgeneral->get_all_tagged_pro($get_friend_art_info);
									if(mysql_num_rows($get_all_tag_pro)>0)
									{
										while($res_projects = mysql_fetch_assoc($get_all_tag_pro))
										{
											//$get_project_type = $newgeneral->Get_Project_type($res_projects['id']);
											$exp_creator = explode('(',$res_projects['creator']);
											$exp_tagged_email = explode(",",$res_projects['tagged_user_email']);
											for($exp_count = 0;$exp_count<count($exp_tagged_email);$exp_count++)
											{
												if($exp_tagged_email[$exp_count]==""){continue;}
												else{
													if($exp_tagged_email[$exp_count]==$_SESSION['login_email'])
													{
														//$res_projects['project_type_name'] = $get_project_type['name'];
														$res_projects['whos_project'] = "tag_pro_com";
														$res_projects['id'] = $res_projects['id']."~com";
														$all_community_friend_project[] = $res_projects;
														?>
														<!--<div id="AccordionContainer" class="tableCont">
															<div class="blkG" style="width:200px;"><?php /*if($res_projects['title']!=""){echo $res_projects['title'];}else{?>&nbsp;<?php }?></div>
															<div class="blkB" style="width:200px;"><a href=""><?php if($exp_creator[0]!=""){echo $exp_creator[0];}else{?>&nbsp;<?php }?></a></div>
															<div class="blkB" style="width:100px;"><?php if($get_project_type['name']!=""){echo $get_project_type['name'];}else{?>&nbsp;<?php }?></div>
															<div class="blkD">&nbsp;</div>
															<div class="icon">&nbsp;</div>
															<div class="icon"><a onclick="return confirmdelete('<?php echo $res_projects['title'];?>')" href="insertcommunityproject.php?delete=<?php echo $res_projects['id'];*/?>"><img src="images/profile/delete.png" /></a></div>
														</div>-->
														<?php
													}
												}
												
											}
										}
									}
								}
							}
							
							/*** Projects Coming From Artist ***/
							$all_artist_project = array();
							if($getproject_artist!="")
							{
								while($showproject=mysql_fetch_assoc($getproject_artist))
								{
									$exp_creator = explode('(',$showproject['creator']);
									//$get_project_type = $editprofile_artist->Get_Project_type($showproject['id']);
									
									//$showproject['project_type_name'] = $get_project_type['name'];
									$showproject['whos_project'] = "own_artist";
									$showproject['id'] = $showproject['id'] . "~art";
									$all_artist_project[] = $showproject;
								?>
									<!--<div id="AccordionContainer" class="tableCont">
										<div class="blkG" style="width:200px;"><?php /*if($showproject['title']!=""){ echo $showproject['title'];}else{?>&nbsp;<?php }?></div>
										<div class="blkB" style="width:200px;"><a href=""><?php if($exp_creator[0]!=""){ echo $exp_creator[0];}else{?>&nbsp;<?php }?></a></div>
										<div class="blkB" style="width:100px;"><?php if($get_project_type['name']!=""){echo $get_project_type['name'];}else{?>&nbsp;<?php }?></div>
										<div class="blkD"><a href="/<?php echo $showproject['profile_url'];?>" onclick="return chk_profile_url('<?php echo $showproject['profile_url'];?>')" >Display</a></div>
										<div class="icon"><a href="add_artist_project.php?id=<?php echo $showproject['id'];?>"><img src="images/profile/edit.png" /></a></div>
										<div class="icon"><a onclick="return confirmdelete('<?php echo $showproject['title'];?>')" href="insertartistproject.php?delete=<?php echo $showproject['id'];*/?>"><img src="images/profile/delete.png" /></a></div>
									</div>-->
								<?php
								}
							}
							
							$all_artist_friend_project = array();
							$get_all_friends = $editprofile_artist->Get_all_friends();
							if($get_all_friends !="")
							{
								while($res_all_friends = mysql_fetch_assoc($get_all_friends))
								{
									$get_friend_art_info = $editprofile_artist->get_friend_art_info($res_all_friends['fgeneral_user_id']);
									$get_all_tag_pro = $editprofile_artist->get_all_tagged_pro($get_friend_art_info);
									if(mysql_num_rows($get_all_tag_pro)>0)
									{
										while($res_projects = mysql_fetch_assoc($get_all_tag_pro))
										{
											//$get_project_type = $editprofile_artist->Get_Project_type($res_projects['id']);
											$exp_creator = explode('(',$res_projects['creator']);
											$exp_tagged_email = explode(",",$res_projects['tagged_user_email']);
											for($exp_count = 0;$exp_count<count($exp_tagged_email);$exp_count++)
											{
												if($exp_tagged_email[$exp_count]==""){continue;}
												else{
													if($exp_tagged_email[$exp_count]==$_SESSION['login_email'])
													{
														//$res_projects['project_type_name'] = $get_project_type['name'];
														$res_projects['whos_project'] = "tag_pro_art";
														$res_projects['id'] = $res_projects['id']."~art";
														$all_artist_friend_project[] = $res_projects;
														?>
														<!--<div id="AccordionContainer" class="tableCont">
															<div class="blkG" style="width:200px;"><?php /*if($res_projects['title']!=""){echo $res_projects['title'];}else{?>&nbsp;<?php }?></div>
															<div class="blkB" style="width:200px;"><a href=""><?php if($exp_creator[0]!=""){echo $exp_creator[0];}else{?>&nbsp;<?php }?></a></div>
															<div class="blkB" style="width:100px;"><?php if($get_project_type['name']!=""){echo $get_project_type['name'];}else{?>&nbsp;<?php }?></div>
															<div class="blkD">&nbsp;</div>
															<div class="icon">&nbsp;</div>
															<div class="icon"><a onclick="return confirmdelete('<?php echo $res_projects['title'];?>')" href="insertartistproject.php?delete=<?php echo $res_projects['id'];*/?>"><img src="images/profile/delete.png" /></a></div>
														</div>-->
														<?php
													}
												}
												
											}
										}
									}
								}
							}
							
							$get_onlycre_id = array();
							$get_onlycre_id = $newgeneral->only_creator_this();
							
							$get_only2cre = array();
							//$get_only2cre = $newgeneral->only_creator2pr_this();
							$get_only2cre = $newgeneral->only_creator2pr_othis();
							
							$sel_com2tagged = array();
							if($project_avail!=0)
							{
								$acc_project = array_unique($acc_project);
								for($acc_pro=0;$acc_pro<count($acc_project);$acc_pro++)
								{
									if($acc_project[$acc_pro]!="" && $acc_project[$acc_pro]!=0)
									{
										$sql_1_cre = $newgeneral->get_project_tagged_by_other2_user($acc_project[$acc_pro]);
										if($sql_1_cre!="")
										{
											if(mysql_num_rows($sql_1_cre)>0)
											{
												while($run = mysql_fetch_assoc($sql_1_cre))
												{
													if($newres1['artist_id']!=$run['artist_id'])
													{
														$run['whos_project'] = "art_creator_tag_pro";
														$run['id'] = $run['id'].'~'.'art';
														$sel_com2tagged[] = $run;
													}
												}
											}											
										}
										
										$sql_2_cre = $newgeneral->get_project_atagged_by_other2_user($acc_project[$acc_pro]);
										if($sql_2_cre!="")
										{
											if(mysql_num_rows($sql_2_cre)>0)
											{
												while($run_c = mysql_fetch_assoc($sql_2_cre))
												{
													if($newres1['community_id']!=$run_c['community_id'])
													{
														$run_c['whos_project'] = "com_creator_tag_pro";
														$run_c['id'] = $run_c['id'].'~'.'com';
														$sel_com2tagged[] = $run_c;
													}
												}
											}										
										}
									}
								}
							}
							
							$sel_art2tagged = array();
							if($project_aavail!=0)
							{
								$acc_aproject = array_unique($acc_aproject);
								for($acc_pro=0;$acc_pro<count($acc_aproject);$acc_pro++)
								{
									if($acc_aproject[$acc_pro]!="" && $acc_aproject[$acc_pro]!=0)
									{
										$sql_1_cre = $newgeneral->get_aproject_tagged_by_other2_user($acc_aproject[$acc_pro]);
										if($sql_1_cre!="")
										{
											if(mysql_num_rows($sql_1_cre)>0)
											{
												while($run = mysql_fetch_assoc($sql_1_cre))
												{
													if($newres1['artist_id']!=$run['artist_id'])
													{
														$run['whos_project'] = "art_creator_tag_pro";
														$run['id'] = $run['id'].'~art';
														$sel_art2tagged[] = $run;
													}
												}
											}
										}
										
										$sql_2_cre = $newgeneral->get_aproject_atagged_by_other2_user($acc_aproject[$acc_pro]);
										if($sql_2_cre!="")
										{
											if(mysql_num_rows($sql_2_cre)>0)
											{
												while($run_c = mysql_fetch_assoc($sql_2_cre))
												{
													if($newres1['community_id']!=$run_c['community_id'])
													{
														$run_c['whos_project'] = "com_creator_tag_pro";
														$run_c['id'] = $run_c['id'].'~com';
														$sel_art2tagged[] = $run_c;
													}
												}
											}
										}
									}
								}
							}
							
							$sel_arttagged = array();
							if($project_avail!=0)
							{
								$acc_project = array_unique($acc_project);
								for($acc_pro=0;$acc_pro<count($acc_project);$acc_pro++)
								{
									if($acc_project[$acc_pro]!="" && $acc_project[$acc_pro]!=0)
									{
										$dum_art = $newgeneral->get_project_tagged_by_other_user($acc_project[$acc_pro]);
										if(!empty($dum_art) && $dum_art['id']!="" && $dum_art['id']!="")
										{
											if($newres1['community_id']!=$dum_art['community_id'])
											{
												$dum_art['id'] = $dum_art['id'].'~com';
												$dum_art['whos_project'] = "tag_pro_com";
												$sel_arttagged[] = $dum_art;
											}
										}
									}
								}
							}
							
							$sel_comtagged = array();
							if($project_aavail!=0)
							{
								$acc_aproject = array_unique($acc_aproject);
								for($acc_pro=0;$acc_pro<count($acc_aproject);$acc_pro++)
								{
									if($acc_aproject[$acc_pro]!="" && $acc_aproject[$acc_pro]!=0)
									{
										$dum_com = $newgeneral->get_aproject_tagged_by_other_user($acc_aproject[$acc_pro]);
										if(!empty($dum_com) && $dum_com['id']!="" && $dum_com['id']!="")
										{
											if($newres1['artist_id']!=$dum_com['artist_id'])
											{
												$dum_com['id'] = $dum_com['id'].'~art';
												$dum_com['whos_project'] = "tag_pro_art";
												$sel_comtagged[] = $dum_com;
											}
										}
									}
								}
							}
							
							$all_combine_projects = array_merge($all_artist_project,$all_community_friend_project,$all_community_project,$all_artist_friend_project,$get_onlycre_id,$sel_comtagged,$sel_arttagged,$get_only2cre,$sel_art2tagged,$sel_com2tagged);
							
							foreach($all_combine_projects as $k=>$v)
							{	
								$end_c1 = $v['creator'];
								//$create_c = strpos($v['creator'],'(');
								//$end_c1 = substr($v['creator'],0,$create_c);
								$end_c = trim($end_c1);
								
								$sort1['creator'][$k] = strtolower($end_c);
								//$sort['from'][$k] = $end_f;
								//$sort['track'][$k] = $v['track'];
								$sort1['title'][$k] = strtolower($v['title']);
							}
							
							if(!empty($sort1))
							{
								array_multisort($sort1['creator'], SORT_ASC, $sort1['title'], SORT_ASC,$all_combine_projects);
							}
							//var_dump($all_combine_projects);
							/*$sort = array();
							foreach($all_combine_projects[0] as $k=>$v) {
							
								$create_c = strpos($v['creator'],'(');
								$end_c = substr($v['creator'],0,$create_c);
								
								$create_f = strpos($v['from'],'(');
								$end_f = substr($v['from'],0,$create_f);
								
								$sort['creator'][$k] = strtolower($end_c);
								
								$sort['from'][$k] = strtolower($end_f);
								if($v['track']!=NULL){								
									$sort['track'][$k] = $v['track'];
								}else{									
									$sort['track'][$k] = " ";
								}
								$sort['title'][$k] = strtolower($v['title']);
							}
							array_multisort($sort['creator'], SORT_ASC, $sort['from'], SORT_ASC, $sort['track'], SORT_NUMERIC ,SORT_ASC,$sort['title'], SORT_ASC,$combine_arr[0]);
							$dummy_pr = array();
							for($newtest_count = 0;$newtest_count<=count($combine_arr[$test_count]);$newtest_count++){
								if(in_array($combine_arr[$test_count][$newtest_count]['id'],$dummy_pr))
							    {
								
							    }
							    else
							    {
									$dummy_pr[] = $combine_arr[$test_count][$newtest_count]['id'];
							
							*/
							$first_index = 0;
							$dumpsa_pros = array();
							$dumpsc_pros = array();
							
							$get_all_del_id = $newgeneral->get_deleted_project_id();
							//var_dump($get_all_del_id['deleted_project_id']);
							$exp_del_id = explode(",",$get_all_del_id['deleted_project_id']);
							//var_dump($exp_del_id);
							for($count_all=0;$count_all<count($all_combine_projects);$count_all++){
								for($count_del=0;$count_del<count($exp_del_id);$count_del++){
									if($exp_del_id[$count_del]!=""){
										if($all_combine_projects[$count_all]['id'] == $exp_del_id[$count_del]){
											$all_combine_projects[$count_all] ="";
										}
									}
								}
							}
							//var_dump($all_combine_projects);
							for($count_all=0;$count_all<count($all_combine_projects);$count_all++)
							{
								if($all_combine_projects[$count_all]!="")
								{
									$chk_ac_ep = 0;
									if(isset($all_combine_projects[$count_all]['artist_id']))
									{
										$get_project_type = $newgeneral->Get_ACProject_subtype($all_combine_projects[$count_all]['id']);
										if(in_array($all_combine_projects[$count_all]['id'],$dumpsa_pros))
										{
											$chk_ac_ep = 1;
										}
										else
										{
											$dumpsa_pros[] = $all_combine_projects[$count_all]['id'];
										}
									}
									elseif(isset($all_combine_projects[$count_all]['community_id']))
									{
										$get_project_type = $newgeneral->Get_Project_subtype($all_combine_projects[$count_all]['id']);
										if(in_array($all_combine_projects[$count_all]['id'],$dumpsc_pros))
										{
											$chk_ac_ep = 1;
										}
										else
										{
											$dumpsc_pros[] = $all_combine_projects[$count_all]['id'];
										}
									}
									if($chk_ac_ep==1)
									{}
									else
									{
										
										$exp_creator = explode('(',$all_combine_projects[$count_all]['creator']);
										if($all_combine_projects[$count_all]['creators_info']!="")
										{
											$link_du = explode("|",$all_combine_projects[$count_all]['creators_info']);
											
											if($link_du[0]=='general_artist')
											{
												$sql_li = mysql_query("SELECT * FROM general_artist WHERE artist_id='".$link_du[1]."'");
												$ans_li = mysql_fetch_assoc($sql_li);
											}
											elseif($link_du[0]=='general_community')
											{
												$sql_li = mysql_query("SELECT * FROM general_community WHERE community_id='".$link_du[1]."'");
												$ans_li = mysql_fetch_assoc($sql_li);
											}
											else
											{
												$sql_li = mysql_query("SELECT * FROM $link_du[0] WHERE id='".$link_du[1]."'");
												$ans_li = mysql_fetch_assoc($sql_li);
											}
											
										}
								?>
									<div id="AccordionContainer" class="tableCont">
										<div class="blkG" style="width:272px;"><?php if($all_combine_projects[$count_all]['title']!=""){ echo $all_combine_projects[$count_all]['title'];}else{?>&nbsp;<?php }?></div>
										<div class="blkE" ><a href=""><?php
										if($exp_creator[0]!="")
										{										
											if(isset($ans_li)) 
											{											
												if($ans_li['profile_url']!="")
												{												
													echo "<a href=".$ans_li['profile_url'].">".$exp_creator[0]."</a>";
												}
												else
												{
													echo $exp_creator[0];
												}
											}
											else
											{
												echo $exp_creator[0];
											}
										}
										else
										{?>&nbsp;<?php }?></a></div>
										<div class="blkB" style="width:110px;"><?php if($get_project_type['name']!=""){echo $get_project_type['name'];}else{?>&nbsp;<?php }?></div>
										<div class="blkD" style="width:78px;"><a href="/<?php echo $all_combine_projects[$count_all]['profile_url'];?>" onclick="return chk_profile_url('<?php echo $all_combine_projects[$count_all]['profile_url'];?>')" >Display</a></div>
										<?php
										if($all_combine_projects[$count_all]['whos_project'] == "own_artist")
										{
											$exp_art_id = explode("~",$all_combine_projects[$count_all]['id']);
										?>
											<div class="icon"><a href="add_artist_project.php?id=<?php echo $exp_art_id[0];?>"><img src="images/profile/edit.png" /></a></div>
											<div class="icon"><a onclick="return confirmdelete('<?php echo $all_combine_projects[$count_all]['title'];?>')" href="insertartistproject.php?delete=<?php echo $exp_art_id[0];?>&whos_project=own_artist"><img src="images/profile/delete.png" /></a></div>
										<?php
										}else if($all_combine_projects[$count_all]['whos_project'] == "own_community")
										{
											$exp_com_id = explode("~",$all_combine_projects[$count_all]['id']);
										?>
											<div class="icon"><a href="add_community_project.php?id=<?php echo $exp_com_id[0];?>"><img src="images/profile/edit.png" /></a></div>
											<div class="icon"><a onclick="return confirmdelete('<?php echo $all_combine_projects[$count_all]['title']; ?>')" href="insertcommunityproject.php?delete=<?php echo $exp_com_id[0];?>&whos_project=own_community"><img src="images/profile/delete.png" /></a></div>
										<?php
										}else if($all_combine_projects[$count_all]['whos_project'] == "community_friends")
										{
										?>
											<div class="icon">&nbsp;</div>
											<div class="icon"><a onclick="return confirmdelete('<?php echo $all_combine_projects[$count_all]['title'];?>')" href="insertcommunityproject.php?delete=<?php echo $all_combine_projects[$count_all]['id'];?>&whos_project=tag_pro_com"><img src="images/profile/delete.png" /></a></div>
										<?php
										}else if($all_combine_projects[$count_all]['whos_project'] == "artist_friends")
										{
										?>
											<div class="icon">&nbsp;</div>
											<div class="icon"><a onclick="return confirmdelete('<?php echo $all_combine_projects[$count_all]['title'];?>')" href="insertartistproject.php?delete=<?php echo $all_combine_projects[$count_all]['id'];?>&whos_project=tag_pro_art"><img src="images/profile/delete.png" /></a></div>
										<?php
										}else if($all_combine_projects[$count_all]['whos_project'] == "only_creator")
										{
											$exp_id = explode("~",$all_combine_projects[$count_all]['id']);
											if($exp_id[1]=="art"){
											?>
												<div class="icon">&nbsp;</div>
												<div class="icon"><a onclick="return confirmdelete('<?php echo $all_combine_projects[$count_all]['title'];?>')" href="insertartistproject.php?delete=<?php echo $exp_id[0];?>&whos_project=only_creator"><img src="images/profile/delete.png" /></a></div>
											<?php
											}else if($exp_id[1]=="com"){
										?>
											<div class="icon">&nbsp;</div>
											<div class="icon"><a onclick="return confirmdelete('<?php echo $all_combine_projects[$count_all]['title'];?>')" href="insertcommunityproject.php?delete=<?php echo $exp_id[0];?>&whos_project=only_creator"><img src="images/profile/delete.png" /></a></div>
										<?php
											}
										}else if($all_combine_projects[$count_all]['whos_project'] == "tag_pro_art")
										{
										?>
											<div class="icon">&nbsp;</div>
											<div class="icon"><a onclick="return confirmdelete('<?php echo $all_combine_projects[$count_all]['title'];?>')" href="insertartistproject.php?delete=<?php echo $all_combine_projects[$count_all]['id'];?>&whos_project=tag_pro_art"><img src="images/profile/delete.png" /></a></div>
										<?php
										}else if($all_combine_projects[$count_all]['whos_project'] == "tag_pro_com")
										{
										?>
											<div class="icon">&nbsp;</div>
											<div class="icon"><a onclick="return confirmdelete('<?php echo $all_combine_projects[$count_all]['title'];?>')" href="insertcommunityproject.php?delete=<?php echo $all_combine_projects[$count_all]['id'];?>&whos_project=tag_pro_com"><img src="images/profile/delete.png" /></a></div>
										<?php
										}else if($all_combine_projects[$count_all]['whos_project'] == "art_creator_tag_pro" || $all_combine_projects[$count_all]['whos_project'] == "com_creator_tag_pro" || $all_combine_projects[$count_all]['whos_project'] == "art_creator_creator_pro" || $all_combine_projects[$count_all]['whos_project'] == "com_creator_creator_pro")
										{
										?>
											<div class="icon">&nbsp;</div>
											<div class="icon"><a onclick="return confirmdelete('<?php echo $all_combine_projects[$count_all]['title'];?>')" href="insertartistproject.php?delete=<?php echo $all_combine_projects[$count_all]['id'];?>&whos_project=<?php echo $all_combine_projects[$count_all]['whos_project']; ?>"><img src="images/profile/delete.png" /></a></div>
										<?php
										}
										?>
									</div>
								<?php
									}
								}
							}
							/*** Projects Coming From Artist Ends Here***/
							?>
                        </div>
                    <!--</div>-->
            </div>
			<?php
				$share_promote = new Promotions();
				$chk_del_gen = $share_promote->check_avail_general($_SESSION['login_email']);
				if($chk_del_gen=='1')
				{
					$chk_com_del = $share_promote->check_avail_community($newres1['community_id']);
					if($chk_com_del=='1')
					{
			?>
						<div class="subTabs" id="promotions" style="display:none;">
						<input type="hidden" id="chk_vals_ups"/>
							<h1><?php echo $newrow['name']; ?> Promotions</h1>
							<input type="hidden" id="post_face"/>
							<div id="mediaContent" style="width:685px;">
								<div class="topLinks">
									<div class="links" style="width:665px;">
										<select class="eventdrop" id="share_facebook" onChange="handleSelection(value)"  style="float:right;">
											<option value="all_profiles_share">All Profiles</option>
											<option value="projects_profiles_share">Project Profiles</option>
											<option value="events_profiles_share">Event Profiles</option>
										</select>
									</div>
								</div>
								<div class="titleCont" style="width:680px;">
									<div class="blkA" style="width:150px;">Type</div>
									<div class="blkH" style="width:398px;">Profile Title</div>
								</div>
							</div>
							<div id="mediaContent" style="width:685px; height:400px; overflow-x:hidden; overflow-y:auto;  margin-bottom: 20px;">
								
								
								<div id="all_profiles_share">
								<?php	
									$all_profiles_art = array();
									$all_profiles_art = $share_promote->all_profiles_share_artist($newres1['artist_id']);
									
									$all_profiles_com = array();
									$all_profiles_com = $share_promote->all_profiles_share_community($newres1['community_id']);
									
									$get_upev_id = array();
										while($get_upev_ids = mysql_fetch_assoc($get_sh_event))
										{
											$get_upev_ids['table_name'] = 'community_event';
											//$get_upev_ids['type'] = 'event';
											$get_upev_ids['type'] = $share_promote->get_substypes($get_upev_ids['id'],$get_upev_ids['table_name']);
											$get_upev_id[] = $get_upev_ids;
										}
									
										$get_aupev_id = array();
										while($get_aupev_ids = mysql_fetch_assoc($get_sh_aeevent))
										{
											$get_aupev_ids['table_name'] = 'artist_event';
											$get_aupev_ids['type'] = $share_promote->get_substypes($get_aupev_ids['id'],$get_aupev_ids['table_name']);
											$get_aupev_id[] = $get_aupev_ids;
										}

										$get_onlycre_ev = array();
										$get_onlycre_ev = $newgeneral->only_sh_creator_upevent();
										
										$get_only2cre_ev = array();
										$get_only2cre_ev = $newgeneral->only_sh_creator2ev_upevent();
										
										$selcoms_tagged = array();
										if($event_avail!=0)
										{
											$acc_event = array_unique($acc_event);
											for($h=0;$h<count($acc_event);$h++)
											{
												if($acc_event[$h]!="" && $acc_event[$h]!=0)
												{
													$dumcomu = $newgeneral->get_event_tagged_by_other_user($acc_event[$h]);
													if(!empty($dumcomu) && $dumcomu['id']!="" && $dumcomu['id']!="")
													{
														$dumcomu['table_name'] = 'community_event';
														$dumcomu['type'] = $share_promote->get_substypes($dumcomu['id'],$dumcomu['table_name']);
														$selcoms_tagged[] = $dumcomu;
													}	
												}
											}
										}
										
										$selarts_tagged = array();
										if($event_aavail!=0)
										{
											$acc_aevent = array_unique($acc_aevent);
											for($h=0;$h<count($acc_aevent);$h++)
											{
												if($acc_aevent[$h]!="" && $acc_aevent[$h]!=0)
												{
													$dumartu = $newgeneral->get_aevent_tagged_by_other_user($acc_aevent[$h]);
													if(!empty($dumartu) && $dumartu['id']!="" && $dumartu['id']!="")
													{
														$dumartu['table_name'] = 'artist_event';
														$dumartu['type'] = $share_promote->get_substypes($dumartu['id'],$dumartu['table_name']);
														$selarts_tagged[] = $dumartu;
													}	
												}
											}
										}
										
										$selarts2_tagged = array();
										if($project_aavail!=0)
										{
											$acc_aproject = array_unique($acc_aproject);
											for($acc_pro=0;$acc_pro<count($acc_aproject);$acc_pro++)
											{												
												if($acc_aproject[$acc_pro]!="" && $acc_aproject[$acc_pro]!=0)
												{													
													$sql_1_cre = $newgeneral->get_event_tagged_by_other2_user($acc_aproject[$acc_pro]);
													if($sql_1_cre!="")
													{
														if(mysql_num_rows($sql_1_cre)>0)
														{
															while($run = mysql_fetch_assoc($sql_1_cre))
															{
																$run['table_name'] = 'artist_event';
																$run['type'] = $share_promote->get_substypes($run['id'],$run['table_name']);
																//$ans_a_als[] = $run;
																$selarts2_tagged[] = $run;
															}
														}
													}
													
													$sql_2_cre = $newgeneral->get_event_atagged_by_other2_user($acc_aproject[$acc_pro]);
													if($sql_2_cre !="" && $sql_2_cre!= Null)
													{
														if(mysql_num_rows($sql_2_cre)>0)
														{
															while($run_c = mysql_fetch_assoc($sql_2_cre))
															{
																$run_c['table_name'] = 'community_event';
																$run_c['type'] = $share_promote->get_substypes($run_c['id'],$run_c['table_name']);
																//$ans_c_als[]
																$selarts2_tagged[] = $run_c;
															}
														}
													}
												}
											}
										}
										
										$selcoms2_tagged = array();
										if($project_avail!=0)
										{
											$acc_project = array_unique($acc_project);
											for($acc_pro=0;$acc_pro<count($acc_project);$acc_pro++)
											{												
												if($acc_project[$acc_pro]!="" && $acc_project[$acc_pro]!=0)
												{													
													$sql_1_cre = $newgeneral->get_aevent_tagged_by_other2_user($acc_project[$acc_pro]);
													if($sql_1_cre!="")
													{
														if(mysql_num_rows($sql_1_cre)>0)
														{
															while($run = mysql_fetch_assoc($sql_1_cre))
															{
																$run['table_name'] = 'artist_event';
																$run['type'] = $share_promote->get_substypes($run['id'],$run['table_name']);
																//$ans_a_als[] = $run;
																$selcoms2_tagged[] = $run;
															}
														}
													}
													
													$sql_2_cre = $newgeneral->get_aevent_atagged_by_other2_user($acc_project[$acc_pro]);
													if($sql_2_cre !="" && $sql_2_cre!= Null)
													{
														if(mysql_num_rows($sql_2_cre)>0)
														{
															while($run_c = mysql_fetch_assoc($sql_2_cre))
															{
																$run_c['table_name'] = 'community_event';
																$run_c['type'] = $share_promote->get_substypes($run_c['id'],$run_c['table_name']);
																//$ans_c_als[]
																$selcoms2_tagged[] = $run_c;
															}
														}
													}
												}
											}
										}
										
										$get_ever_id = array();
										while($get_ever_ids = mysql_fetch_assoc($get_sh_eventrec))
										{
											$get_ever_ids['table_name'] = 'community_event';
											$get_ever_ids['type'] = $share_promote->get_substypes($get_ever_ids['id'],$get_ever_ids['table_name']);
											$get_ever_id[] = $get_ever_ids;											
										}
										
										$get_aever_id = array();
										while($get_aever_ids = mysql_fetch_assoc($get_sh_aeeventrec))
										{
											$get_aever_ids['table_name'] = 'artist_event';
											$get_aever_ids['type'] = $share_promote->get_substypes($get_aever_ids['id'],$get_aever_ids['table_name']);
											$get_aever_id[] = $get_aever_ids;											
										}
										
										$get_onlycre_rev = array();
										$get_onlycre_rev = $newgeneral->only_sh_creator_recevent();
										
										$get_only2cre_rev = array();
										$get_only2cre_rev = $newgeneral->only_sh_creator2_recevent();
										
										$selcom_stagged = array();
										if($event_avail!=0)
										{
											$acc_event = array_unique($acc_event);
											for($k=0;$k<count($acc_event);$k++)
											{
												if($acc_event[$k]!="" && $acc_event[$k]!=0)
												{
													$dumec = $newgeneral->get_event_tagged_by_other_userrec($acc_event[$k]);
													if(!empty($dumec) && $dumec['id']!="" && $dumec['id']!="")
													{
														$dumec['table_name'] = 'community_event';
														$dumec['type'] = $share_promote->get_substypes($dumec['id'],$dumec['table_name']);
														$selcom_stagged[] = $dumec;
													}												
												}
											}
										}
										
										$selart_stagged = array();
										if($event_aavail!=0)
										{
											$acc_aevent = array_unique($acc_aevent);
											for($ki=0;$ki<count($acc_aevent);$ki++)
											{
												if($acc_aevent[$ki]!="" && $acc_aevent[$ki]!=0)
												{
													$dumc = $newgeneral->get_aevent_tagged_by_other_userrec($acc_aevent[$ki]);
													if(!empty($dumc) && $dumc['id']!="" && $dumc['id']!="")
													{
														$dumc['table_name'] = 'artist_event';
														$dumc['type'] = $share_promote->get_substypes($dumc['id'],$dumc['table_name']);
														$selart_stagged[] = $dumc;
													}												
												}
											}
										}
										
										$selarts2r_tagged = array();
										if($project_aavail!=0)
										{
											$acc_aproject = array_unique($acc_aproject);
											for($acc_pro=0;$acc_pro<count($acc_aproject);$acc_pro++)
											{												
												if($acc_aproject[$acc_pro]!="" && $acc_aproject[$acc_pro]!=0)
												{													
													$sql_1_cre = $newgeneral->get_event_tagged_by_other2_userrec($acc_aproject[$acc_pro]);
													if($sql_1_cre!="")
													{
														if(mysql_num_rows($sql_1_cre)>0)
														{
															while($run = mysql_fetch_assoc($sql_1_cre))
															{
																$run['table_name'] = 'artist_event';
																$run['type'] = $share_promote->get_substypes($run['id'],$run['table_name']);
																//$ans_a_als[] = $run;
																$selarts2r_tagged[] = $run;
															}
														}
													}
													
													$sql_2_cre = $newgeneral->get_event_atagged_by_other2_userrec($acc_aproject[$acc_pro]);
													if($sql_2_cre !="" && $sql_2_cre!= Null)
													{
														if(mysql_num_rows($sql_2_cre)>0)
														{
															while($run_c = mysql_fetch_assoc($sql_2_cre))
															{
																$run_c['table_name'] = 'community_event';
																$run_c['type'] = $share_promote->get_substypes($run_c['id'],$run_c['table_name']);
																//$ans_c_als[]
																$selarts2r_tagged[] = $run_c;
															}
														}
													}
												}
											}
										}
										
										$selcoms2r_tagged = array();
										if($project_avail!=0)
										{
											$acc_project = array_unique($acc_project);
											for($acc_pro=0;$acc_pro<count($acc_project);$acc_pro++)
											{												
												if($acc_project[$acc_pro]!="" && $acc_project[$acc_pro]!=0)
												{													
													$sql_1_cre = $newgeneral->get_aevent_tagged_by_other2_userrec($acc_project[$acc_pro]);
													if($sql_1_cre!="")
													{
														if(mysql_num_rows($sql_1_cre)>0)
														{
															while($run = mysql_fetch_assoc($sql_1_cre))
															{
																$run['table_name'] = 'artist_event';
																$run['type'] = $share_promote->get_substypes($run['id'],$run['table_name']);
																//$ans_a_als[] = $run;
																$selcoms2r_tagged[] = $run;
															}
														}
													}
													
													$sql_2_cre = $newgeneral->get_aevent_atagged_by_other2_userrec($acc_project[$acc_pro]);
													if($sql_2_cre !="" && $sql_2_cre!= Null)
													{
														if(mysql_num_rows($sql_2_cre)>0)
														{
															while($run_c = mysql_fetch_assoc($sql_2_cre))
															{
																$run_c['table_name'] = 'community_event';
																$run_c['type'] = $share_promote->get_substypes($run_c['id'],$run_c['table_name']);
																//$ans_c_als[]
																$selcoms2r_tagged[] = $run_c;
															}
														}
													}
												}
											}
										}
										
										$get_onlycre_id = array();
										$get_onlycre_id = $newgeneral->only_sh_creator_this();
										
										$get_only2cre = array();
										$get_only2cre = $newgeneral->only_sh_creator2pr_this();
										
										$get_proa_id = array();
										while($get_pros_ids = mysql_fetch_assoc($get_sh_project))
										{
											$get_pros_ids['table_name'] = 'community_project';
											$get_pros_ids['type'] = $share_promote->get_substypes($get_pros_ids['id'],$get_pros_ids['table_name']);
											$get_proa_id[] = $get_pros_ids;
										}
										//$project_avail = 0;
										$get_proc_id = array();
										while($get_proc_ids = mysql_fetch_assoc($get_sh_cproject))
										{
											$get_proc_ids['table_name'] = 'artist_project';
											$get_proc_ids['type'] = $share_promote->get_substypes($get_proc_ids['id'],$get_proc_ids['table_name']);
											$get_proc_id[] = $get_proc_ids;
										}

										$sel_comtagged = array();
										if($project_avail!=0)
										{
											$acc_project = array_unique($acc_project);
											for($acc_pro=0;$acc_pro<count($acc_project);$acc_pro++)
											{
												if($acc_project[$acc_pro]!="" && $acc_project[$acc_pro]!=0)
												{
													$dum_com = $newgeneral->get_project_tagged_by_other_user($acc_project[$acc_pro]);
													if(!empty($dum_com) && $dum_com['id']!="" && $dum_com['id']!="")
													{
														$dum_com['table_name'] = 'community_project';
														$dum_com['type'] = $share_promote->get_substypes($dum_com['id'],$dum_com['table_name']);
														$sel_comtagged[] = $dum_com;
													}
												}
											}
										}
										
										$sel_arttagged = array();
										if($project_aavail!=0)
										{
											$acc_aproject = array_unique($acc_aproject);
											for($acc_pro=0;$acc_pro<count($acc_aproject);$acc_pro++)
											{
												if($acc_aproject[$acc_pro]!="" && $acc_aproject[$acc_pro]!=0)
												{
													$dum_art = $newgeneral->get_aproject_tagged_by_other_user($acc_aproject[$acc_pro]);
													if(!empty($dum_art) && $dum_art['id']!="" && $dum_art['id']!="")
													{
														$dum_art['table_name'] = 'artist_project';
														$dum_art['type'] = $share_promote->get_substypes($dum_art['id'],$dum_art['table_name']);
														$sel_arttagged[] = $dum_art;
													}
												}
											}
										}
										
										$sel_com2tagged = array();
										if($project_avail!=0)
										{
											$acc_project = array_unique($acc_project);
											for($acc_pro=0;$acc_pro<count($acc_project);$acc_pro++)
											{
												if($acc_project[$acc_pro]!="" && $acc_project[$acc_pro]!=0)
												{
													$sql_1_cre = $newgeneral->get_project_tagged_by_other2_user($acc_project[$acc_pro]);
													if($sql_1_cre!="")
													{
														if(mysql_num_rows($sql_1_cre)>0)
														{
															while($run = mysql_fetch_assoc($sql_1_cre))
															{
																$run['table_name'] = 'artist_project';
																$run['type'] = $share_promote->get_substypes($run['id'],$run['table_name']);
																$sel_com2tagged[] = $run;
															}
														}											
													}
													
													$sql_2_cre = $newgeneral->get_project_atagged_by_other2_user($acc_project[$acc_pro]);
													if($sql_2_cre!="")
													{
														if(mysql_num_rows($sql_2_cre)>0)
														{
															while($run_c = mysql_fetch_assoc($sql_2_cre))
															{
																$run_c['table_name'] = 'community_project';
																$run_c['type'] = $share_promote->get_substypes($run_c['id'],$run_c['table_name']);
																$sel_com2tagged[] = $run_c;
															}
														}										
													}
												}
											}
										}
										
										$sel_art2tagged = array();
										if($project_aavail!=0)
										{
											$acc_aproject = array_unique($acc_aproject);
											for($acc_pro=0;$acc_pro<count($acc_aproject);$acc_pro++)
											{
												if($acc_aproject[$acc_pro]!="" && $acc_aproject[$acc_pro]!=0)
												{
													$sql_1_cre = $newgeneral->get_aproject_tagged_by_other2_user($acc_aproject[$acc_pro]);
													if($sql_1_cre!="")
													{
														if(mysql_num_rows($sql_1_cre)>0)
														{
															while($run = mysql_fetch_assoc($sql_1_cre))
															{
																$run['table_name'] = 'artist_project';
																$run['type'] = $share_promote->get_substypes($run['id'],$run['table_name']);
																$sel_art2tagged[] = $run;
															}
														}
													}
													
													$sql_2_cre = $newgeneral->get_aproject_atagged_by_other2_user($acc_aproject[$acc_pro]);
													if($sql_2_cre!="")
													{
														if(mysql_num_rows($sql_2_cre)>0)
														{
															while($run_c = mysql_fetch_assoc($sql_2_cre))
															{
																$run_c['table_name'] = 'community_project';
																$run_c['type'] = $share_promote->get_substypes($run_c['id'],$run_c['table_name']);
																$sel_art2tagged[] = $run_c;
															}
														}
													}
												}
											}
										}
										
										$all_profiles = array_merge($all_profiles_com,$get_upev_id,$selarts_tagged,$selcoms_tagged,$get_onlycre_ev,$get_aupev_id,$get_only2cre_ev,$selarts2_tagged,$selcoms2_tagged,$selcoms2r_tagged,$selarts2r_tagged,$get_ever_id,$get_aever_id,$get_onlycre_rev,$get_only2cre_rev,$selcom_stagged,$selart_stagged,$all_profiles_art,$get_onlycre_id,$sel_art2tagged,$sel_com2tagged,$sel_arttagged,$sel_comtagged,$get_proc_id,$get_proa_id,$get_only2cre);
										
										$sort = array();
										foreach($all_profiles as $k=>$v)
										{
											$sort['type'][$k] = strtolower($v['type']);
											//$sort['from'][$k] = $end_f;
											//$sort['track'][$k] = $v['track'];
											$sort['title'][$k] = strtolower($v['title']);
										}
										//var_dump($sort);
										if(!empty($sort))
										{
											array_multisort($sort['type'], SORT_ASC, $sort['title'], SORT_ASC,$all_profiles);
										}
										
									$dums_sh_aev = array();
									$dums_sh_cev = array();
									$dums_sh_apr = array();
									$dums_sh_cpr = array();
									
									$get_all_del_id = $newgeneral->get_deleted_project_id();
									$exp_del_id = explode(",",$get_all_del_id['deleted_event_id']);
									for($count_all=0;$count_all<count($all_profiles);$count_all++)
									{
										if($all_profiles[$count_all]['table_name']=='artist_event')
										{
											$all_profiles[$count_all]['id'] = $all_profiles[$count_all]['id'].'~art';
										}
										elseif($all_profiles[$count_all]['table_name']=='community_event')
										{
											$all_profiles[$count_all]['id'] = $all_profiles[$count_all]['id'].'~com';
										}
										for($count_del=0;$count_del<count($exp_del_id);$count_del++)
										{
											if($exp_del_id[$count_del]!="")
											{
												if($all_profiles[$count_all]['id'] == $exp_del_id[$count_del])
												{
													$all_profiles[$count_all] ="";
												}
											}
										}
									}
									
									$get_all_del_id = $newgeneral->get_deleted_project_id();
									$exp_del_id = explode(",",$get_all_del_id['deleted_project_id']);
									
									for($count_all=0;$count_all<count($all_profiles);$count_all++)
									{
										if($all_profiles[$count_all]['table_name']=='artist_project')
										{
											$all_profiles[$count_all]['id'] = $all_profiles[$count_all]['id'].'~art';
										}
										elseif($all_profiles[$count_all]['table_name']=='community_project')
										{
											$all_profiles[$count_all]['id'] = $all_profiles[$count_all]['id'].'~com';
										}
										for($count_del=0;$count_del<count($exp_del_id);$count_del++)
										{											
											if($exp_del_id[$count_del]!="")
											{
												if($all_profiles[$count_all]['id'] == $exp_del_id[$count_del])
												{
													$all_profiles[$count_all] ="";
												}
											}
										}
									}
									
									if($all_profiles!="" && count($all_profiles)>0)
									{
										$ups = 0;
										
										for($all_s_p=0;$all_s_p<count($all_profiles);$all_s_p++)
										{
											$chk_vals = 0;
											if($all_profiles[$all_s_p]['table_name']=='artist_event')
											{
												
												if(in_array($all_profiles[$all_s_p]['id'],$dums_sh_aev))
												{
													$chk_vals = 1;
												}
												else
												{
													$dums_sh_aev[] = $all_profiles[$all_s_p]['id'];
												}
											}
											
											if($all_profiles[$all_s_p]['table_name']=='community_event')
											{
												
												if(in_array($all_profiles[$all_s_p]['id'],$dums_sh_cev))
												{
													$chk_vals = 1;
												}
												else
												{
													$dums_sh_cev[] = $all_profiles[$all_s_p]['id'];
												}
											}
											
											if($all_profiles[$all_s_p]['table_name']=='artist_project')
											{
												
												if(in_array($all_profiles[$all_s_p]['id'],$dums_sh_apr))
												{
													$chk_vals = 1;
												}
												else
												{
													$dums_sh_apr[] = $all_profiles[$all_s_p]['id'];
												}
											}
											
											if($all_profiles[$all_s_p]['table_name']=='community_project')
											{
												
												if(in_array($all_profiles[$all_s_p]['id'],$dums_sh_cpr))
												{
													$chk_vals = 1;
												}
												else
												{
													$dums_sh_cpr[] = $all_profiles[$all_s_p]['id'];
												}
											}
											
										if($chk_vals==0 && ($all_profiles[$all_s_p]['name']!="" || $all_profiles[$all_s_p]['title']!=""))
										{
								?>
										<div class="tableCont" style="width:640px;">
											<div class="blkA" style="width:150px;"><?php
											if($all_profiles[$all_s_p]['table_name']=='general_community')
											{
												if($all_profiles[$all_s_p]['type']!="")
												{
													echo $all_profiles[$all_s_p]['type'];
												}
												else
												{
													?>&nbsp;<?php
												}
												$spcl = $all_profiles[$all_s_p]['table_name'].'|'.$all_profiles[$all_s_p]['community_id'];
											}
											elseif($all_profiles[$all_s_p]['table_name']=='general_artist')
											{
												if($all_profiles[$all_s_p]['type']!="")
												{
													echo $all_profiles[$all_s_p]['type'];
												}
												else
												{
													?>&nbsp;<?php
												}
												$spcl = $all_profiles[$all_s_p]['table_name'].'|'.$all_profiles[$all_s_p]['artist_id'];
											}
											elseif($all_profiles[$all_s_p]['table_name']=='community_event' || $all_profiles[$all_s_p]['table_name']=='artist_event')
											{
												if($all_profiles[$all_s_p]['type']!="")
												{
													echo $all_profiles[$all_s_p]['type'];
												}
												else
												{
													?>&nbsp;<?php
												}
												$spcl = $all_profiles[$all_s_p]['table_name'].'|'.$all_profiles[$all_s_p]['id'];
											}
											elseif($all_profiles[$all_s_p]['table_name']=='community_project' || $all_profiles[$all_s_p]['table_name']=='artist_project')
											{
												if($all_profiles[$all_s_p]['type']!="")
												{
													echo $all_profiles[$all_s_p]['type'];
												}
												else
												{
													?>&nbsp;<?php
												}
												$spcl = $all_profiles[$all_s_p]['table_name'].'|'.$all_profiles[$all_s_p]['id'];
											}
											?></div>
											<div class="blkH" style="width:398px;"><?php 
											if($all_profiles[$all_s_p]['name']!="") 
											{
												echo $all_profiles[$all_s_p]['name'];
											}
											elseif($all_profiles[$all_s_p]['title']!="")
											{
												if($all_profiles[$all_s_p]['table_name']=='artist_event' || $all_profiles[$all_s_p]['table_name']=='community_event')
												{
													echo date("m.d.y", strtotime($all_profiles[$all_s_p]['date'])).' '.$all_profiles[$all_s_p]['title'];
												}
												else
												{
													echo $all_profiles[$all_s_p]['title'];
												}
											}
											?></div>
											<!--<div class="blkGF" style="padding-top:5px;">-->
											<div>
												<span style="display:none;" id="faces_<?php echo $ups; ?>" st_url="<?php echo $all_profiles[$all_s_p]['profile_url']; ?>" class='st_facebook_hcount' displayText='Facebook'></span>
											<!--	<span class='st_email_hcount' displayText='Email'></span>-->
											</div>
											<div class="blkC">
												<a title="Send updates on this profile using email newsletters, Purify Art news feeds, and/or Facebook wall posts." onClick="blank_val()" href="Update_facebook.php?spcl=<?php echo $spcl.'|'.$ups; ?>&url_mtch_ups=<?php echo $all_profiles[$all_s_p]['profile_url']; ?>&title_for_socials=<?php if($all_profiles[$all_s_p]['name']!="") 
											{
												echo urlencode($all_profiles[$all_s_p]['name']);
											}else
												{
													echo urlencode($all_profiles[$all_s_p]['title']);
												}?>" class="fancybox fancybox.ajax" id="update_facebook_<?php echo $ups; ?>"><input type="button" style="border-radius:5px; -webkit-border-radius:5px; -moz-border-radius:5px; -ms-border-radius:5px; -o-border-radius:5px; background-color: #000000; border: 0 none; color: #FFFFFF; float: left; font-size: 12px; font-weight: 700;     margin-right: 5px; padding: 8px 15px; text-decoration: none;" value="Send Update" /></a>
											</div>
											<script>
												$(document).ready(function() {
													$("#update_facebook_<?php echo $ups; ?>").fancybox({
													helpers : {
														media : {},
														buttons : {},
														title : null
													},
													afterClose: function() {
															var face_po = document.getElementById("post_face").value; 
															if(face_po!="no" && face_po!="") 
															{ 
																var pot = face_po.split('|');
																
																$('#faces_'+pot[1]+' span.stButton').click(); 
															}
															var chk_vals_ups = document.getElementById("chk_vals_ups").value;
															
															if(chk_vals_ups=='1')
															{
																alert("Your updates have been sent.");
															}
															setTimeout(function(){
																location.reload();
															},2000);
														}
													});
												
													/*$("#update_facebook_<?php echo $ups; ?>").fancybox({
													'width'				: '75%',
													'height'			: '100%',
													'transitionIn'		: 'none',
													'transitionOut'		: 'none',
													'type'				: 'iframe',
													'onClosed'			: function() { 
															var face_po = document.getElementById("post_face").value; 
															if(face_po!="no" && face_po!="") 
															{ 
																var pot = face_po.split('|');
																
																$('#faces_'+pot[1]+' span.stButton').click(); 
															}
															var chk_vals_ups = document.getElementById("chk_vals_ups").value;
															
															if(chk_vals_ups=='1')
															{
																alert("Your updates have been sent.");
															}
															setTimeout(function(){
																location.reload();
															},2000);
														}
													});*/
												});
												
												function blank_val()
												{
													document.getElementById("post_face").value = ""; 
												}
											</script>
								<?php
											$ups = $ups + 1;
											?>
											</div>
											<?php
										}
										}
									}
								?>
								</div>
								
								
								<div id="projects_profiles_share" style="display:none;">
								<?php
									$pro_adumps = array();
									$pro_cdumps = array();
									
									if($all_profiles!="" && count($all_profiles)>0)
									{
										$up_sp = 0;
										for($all_s_p=0;$all_s_p<count($all_profiles);$all_s_p++)
										{
											$pors_chks = 0;
											
											if($all_profiles[$all_s_p]['table_name']=='artist_project')
											{
												if(in_array($all_profiles[$all_s_p]['id'],$pro_adumps))
												{
													$pors_chks = 1;
												}
												else
												{
													$pro_adumps[] = $all_profiles[$all_s_p]['id'];
												}
											}
											elseif($all_profiles[$all_s_p]['table_name']=='community_project')
											{
												if(in_array($all_profiles[$all_s_p]['id'],$pro_cdumps))
												{
													$pors_chks = 1;
												}
												else
												{
													$pro_cdumps[] = $all_profiles[$all_s_p]['id'];
												}
											}
											
											if($pors_chks==0)
											{
											if($all_profiles[$all_s_p]['table_name']=='community_project' || $all_profiles[$all_s_p]['table_name']=='artist_project')
											{
										?>
										<div class="tableCont">
										<?php
											if($all_profiles[$all_s_p]['table_name']=='community_project' || $all_profiles[$all_s_p]['table_name']=='artist_project')
											{	
											?>
												<div class="blkA" style="width:150px;">
											<?php
													if($all_profiles[$all_s_p]['type']!="")
													{
														echo $all_profiles[$all_s_p]['type'];
													}
													else
													{
														?>&nbsp;<?php
													}
											?>
												</div>
											<?php
											}
											
											if($all_profiles[$all_s_p]['table_name']=='community_project' || $all_profiles[$all_s_p]['table_name']=='artist_project')
											{
											?>
												<div class="blkH" style="width:398px;">
											<?php
													echo $all_profiles[$all_s_p]['title'];
											?>
												</div>
											<?php
											}
											
											$spcla = $all_profiles[$all_s_p]['table_name'].'|'.$all_profiles[$all_s_p]['id'];
											
											if($all_profiles[$all_s_p]['table_name']=='community_project' || $all_profiles[$all_s_p]['table_name']=='artist_project')
											{
											?>
												<!--<div class="blkGF" style="padding-top:5px;">
													<span class='st_facebook_hcount' displayText='Facebook'></span>-->
												<div>
													<span style="display:none;" id="facespro_<?php echo $up_sp; ?>" st_url="<?php echo $all_profiles[$all_s_p]['profile_url']; ?>" class='st_facebook_hcount' displayText='Facebook'></span>
													<!--<span class='st_email_hcount' displayText='Email'></span>-->
												</div>
											<?php
											}
											
											if($all_profiles[$all_s_p]['table_name']=='community_project' || $all_profiles[$all_s_p]['table_name']=='artist_project')
											{
											?>
												<div class="blkC">
													<a title="Send updates on this profile using email newsletters, Purify Art news feeds, and/or Facebook wall posts." onClick="blank_val()" href="Update_facebook.php?spcl=<?php echo $spcla.'|'.$up_sp; ?>&url_mtch_ups=<?php echo $all_profiles[$all_s_p]['profile_url']; ?>&title_for_socials=<?php if($all_profiles[$all_s_p]['name']!="") 
											{
												echo urlencode($all_profiles[$all_s_p]['name']);
											}else
												{
													echo urlencode($all_profiles[$all_s_p]['title']);
												}?>" class="fancybox fancybox.ajax" id="updatep_facebook_<?php echo $up_sp; ?>"><input type="button" style="border-radius:5px; -webkit-border-radius:5px; -moz-border-radius:5px; -ms-border-radius:5px; -o-border-radius:5px; background-color: #000000; border: 0 none; color: #FFFFFF; float: left; font-size: 12px; font-weight: 700;     margin-right: 5px; padding: 8px 15px; text-decoration: none;" value="Send Update" /></a>
												</div>
												<script>
												$(document).ready(function() {
												
													$("#updatep_facebook_<?php echo $up_sp; ?>").fancybox({
													helpers : {
														media : {},
														buttons : {},
														title : null
													},
													afterClose: function() { 
															var face_po = document.getElementById("post_face").value; 
															if(face_po!="no" && face_po!="") 
															{ 
																var pot = face_po.split('|');
																
																$('#facespro_'+pot[1]+' span.stButton').click(); 
															}
															var chk_vals_ups = document.getElementById("chk_vals_ups").value;
															
															if(chk_vals_ups=='1')
															{
																alert("Your updates have been sent.");
															}
															setTimeout(function(){
																location.reload();
															},2000);
														}
													});
													/*$("#updatep_facebook_<?php echo $up_sp; ?>").fancybox({
													'width'				: '75%',
													'height'			: '100%',
													'transitionIn'		: 'none',
													'transitionOut'		: 'none',
													'type'				: 'iframe',
													'onClosed'			: function() { 
															var face_po = document.getElementById("post_face").value; 
															if(face_po!="no" && face_po!="") 
															{ 
																var pot = face_po.split('|');
																
																$('#facespro_'+pot[1]+' span.stButton').click(); 
															}
															var chk_vals_ups = document.getElementById("chk_vals_ups").value;
															
															if(chk_vals_ups=='1')
															{
																alert("Your updates have been sent.");
															}
															setTimeout(function(){
																location.reload();
															},2000);
														}
													});*/
												});
												
												function blank_val()
												{
													document.getElementById("post_face").value = ""; 
												}
											</script>
								<?php
											$up_sp = $up_sp + 1;
											}
											?>
											</div>
											<?php
										}
									}
									}
									}
								?>
								</div>
								
								
								<div id="events_profiles_share" style="display:none;">
								<?php
									$eve_adumps = array();
									$eve_cdumps = array();
									
									if($all_profiles!="" && count($all_profiles)>0)
									{
										$up_ep = 0;
										for($all_s_p=0;$all_s_p<count($all_profiles);$all_s_p++)
										{
											$eves_chks = 0;
											
											if($all_profiles[$all_s_p]['table_name']=='artist_event')
											{
												if(in_array($all_profiles[$all_s_p]['id'],$eve_adumps))
												{
													$eves_chks = 1;
												}	
												else
												{
													$eve_adumps[] = $all_profiles[$all_s_p]['id'];
												}
											}
											elseif($all_profiles[$all_s_p]['table_name']=='community_event')
											{
												if(in_array($all_profiles[$all_s_p]['id'],$eve_cdumps))
												{
													$eves_chks = 1;
												}	
												else
												{
													$eve_cdumps[] = $all_profiles[$all_s_p]['id'];
												}
											}
											
											if($eves_chks==0)
											{
											if($all_profiles[$all_s_p]['table_name']=='community_event' || $all_profiles[$all_s_p]['table_name']=='artist_event')
											{
										?>
											<div class="tableCont" id="events_profiles_share">
										<?php
											if($all_profiles[$all_s_p]['table_name']=='community_event' || $all_profiles[$all_s_p]['table_name']=='artist_event')
											{	
											?>
												<div class="blkA" style="width:150px;">
											<?php
													if($all_profiles[$all_s_p]['type']!="")
													{
														echo $all_profiles[$all_s_p]['type'];
													}
													else
													{
														?>&nbsp;<?php
													}
											?>
												</div>
											<?php
											}
											
											if($all_profiles[$all_s_p]['table_name']=='community_event' || $all_profiles[$all_s_p]['table_name']=='artist_event')
											{
											?>
												<div class="blkH" style="width:398px;">
											<?php
													echo date("m.d.y", strtotime($all_profiles[$all_s_p]['date'])).' '.$all_profiles[$all_s_p]['title'];
											?>
												</div>
											<?php
											}
											
											$spcle = $all_profiles[$all_s_p]['table_name'].'|'.$all_profiles[$all_s_p]['id'];
											
											if($all_profiles[$all_s_p]['table_name']=='community_event' || $all_profiles[$all_s_p]['table_name']=='artist_event')
											{
											?>
												<!--<div class="blkGF" style="padding-top:5px;">
													<span class='st_facebook_hcount' displayText='Facebook'></span>-->
												<div>
													<span style="display:none;" id="faceseve_<?php echo $up_ep; ?>" st_url="<?php echo $all_profiles[$all_s_p]['profile_url']; ?>" class='st_facebook_hcount' displayText='Facebook'></span>
													<!--<span class='st_email_hcount' displayText='Email'></span>-->
												</div>
											<?php
											}
											
											if($all_profiles[$all_s_p]['table_name']=='community_event' || $all_profiles[$all_s_p]['table_name']=='artist_event')
											{
											?>
												<div class="blkC">
													<a title="Send updates on this profile using email newsletters, Purify Art news feeds, and/or Facebook wall posts." onClick="blank_val()" href="Update_facebook.php?spcl=<?php echo $spcle.'|'.$up_ep; ?>&url_mtch_ups=<?php echo $all_profiles[$all_s_p]['profile_url']; ?>&title_for_socials=<?php if($all_profiles[$all_s_p]['name']!="") 
											{
												echo urlencode($all_profiles[$all_s_p]['name']);
											}else
												{
													echo urlencode($all_profiles[$all_s_p]['title']);
												}?>" class="fancybox fancybox.ajax" id="updatee_facebook_<?php echo $up_ep; ?>"><input type="button" style="border-radius:5px; -webkit-border-radius:5px; -moz-border-radius:5px; -ms-border-radius:5px; -o-border-radius:5px; background-color: #000000; border: 0 none; color: #FFFFFF; float: left; font-size: 12px; font-weight: 700;     margin-right: 5px; padding: 8px 15px; text-decoration: none;" value="Send Update" /></a>
												</div>
												<script>
												$(document).ready(function() {
												
													$("#updatee_facebook_<?php echo $up_ep; ?>").fancybox({
													helpers : {
														media : {},
														buttons : {},
														title : null
													},
													afterClose: function() { 
															var face_po = document.getElementById("post_face").value; 
															if(face_po!="no" && face_po!="") 
															{ 
																var pot = face_po.split('|');
																
																$('#faceseve_'+pot[1]+' span.stButton').click(); 
															}
															var chk_vals_ups = document.getElementById("chk_vals_ups").value;
															
															if(chk_vals_ups=='1')
															{
																alert("Your updates have been sent.");
															}
															setTimeout(function(){
																location.reload();
															},2000);
														}
													});
													/*$("#updatee_facebook_<?php echo $up_ep; ?>").fancybox({
													'width'				: '75%',
													'height'			: '100%',
													'transitionIn'		: 'none',
													'transitionOut'		: 'none',
													'type'				: 'iframe',
													'onClosed'			: function() { 
															var face_po = document.getElementById("post_face").value; 
															if(face_po!="no" && face_po!="") 
															{ 
																var pot = face_po.split('|');
																
																$('#faceseve_'+pot[1]+' span.stButton').click(); 
															}
															var chk_vals_ups = document.getElementById("chk_vals_ups").value;
															
															if(chk_vals_ups=='1')
															{
																alert("Your updates have been sent.");
															}
															setTimeout(function(){
																location.reload();
															},2000);
														}
													});*/
												});
												
												function blank_val()
												{
													document.getElementById("post_face").value = ""; 
												}
											</script>
								<?php
												$up_ep = $up_ep + 1;
											}
											?>
											</div>
											<?php
										}
									}
									}
									}
								?>
								
							</div>
						</div>
						</div>
			<?php
					}
				}
			?>
                
          </div>  
          
   </div>
</div>

  <!--<div id="footer"> <strong>&copy; 2010 Purify Entertainment</strong> This work is licensed under a <a rel="license" href="https://creativecommons.org/licenses/by-sa/3.0/">Creative Commons Attribution-Share Alike 3.0 Unported License</a>.<br />
    <a href="privacypolicy.html">Privacy Policy</a> / <a href="useragreement.html">User Agreement</a> / <a href="originalityagreement.html">Originality Agreement</a> / <a href="busplan.html">Business Plan</a> </div>
-->
  <!-- end of main container -->
</div><?php include_once("displayfooter.php"); ?>
<script>
	$(document).ready(function(){
	var url_check = document.URL;
		
	 if(location.hash == "" || url_check.indexOf("#profile")>0)
 {

		$("#profile").show();
 }
 
	$("a").each(
    function()
    {    
        if($(this).attr("href") == location.hash)
        {
            $(this).click();
        }
    });
	
	$(window).hashchange(
		function()
		{
			$("a").each(
			function()
			{    
				if($(this).attr("href") == location.hash)
				{
					$(this).click();
				}
			});
		}
	)
	
	});
</script>
<script type="text/javascript">
/*function selectmedia()
{
	var select2=document.getElementById("featured_media").value;
	window.location = "profileedit_community.php?sel_val="+document.getElementById("featured_media").value;
}*/
</script>
<?php //if(isset($_REQUEST['sel_val']))
//{
?>
<script type="text/javascript">
//$("document").ready(function(){
	//var media_gallery_text=document.getElementById("media");
	//var media_video_text=document.getElementById("media_video");
	//var media_audio_text=document.getElementById("media_audio");
		
	//if(media_gallery_text.value!=null)
	//{
	//	alert(media_gallery_text.value);
	//}
	
	/*if(media_video_text.value!=null)
	{
		alert(media_video_text);
	}
	if(media_audio_text.value!=null)
	{
		alert(media_audio_text);
	}
	
	*/
//});
</script>
<?php
//}
?>
</body>
</html>
<script type="text/javascript">
$(window).load(function() {
	$(".list-wrap").css({"height":""});
});
</script>