<?php
include("commons/db.php");
include_once("classes/DeleteMembership.php");
$delete_obj = new DeleteMembership();

if(isset($_GET['delete'])){
$delete_member = $delete_obj->delete_user_member_ship($_GET['delete']);
}
else if(isset($_GET['accept']))
{
$accept_membership = $delete_obj->accept_fan_membership($_GET['accept'],$_GET['status']);
}
else if(isset($_GET['deny']))
{
$deny_membership = $delete_obj->deny_fan_membership($_GET['deny']);
}
else if(isset($_GET['renew']))
{
$renew_membership = $delete_obj->renewfanclub($_GET['renew']);
}
else if(isset($_GET['extend']))
{
$renew_membership = $delete_obj->extendfanclub($_GET['extend']);
}
if(isset($_GET["redirects"]))
{
	if($_GET["redirects"]=="trues")
	{
		$locate = $_SERVER['HTTP_REFERER']."&not=found";
		header("Location: ".$locate);
	}
	else
	{
		header("Location: profileedit.php#mymemberships");
	}
}
else
{
	header("Location: profileedit.php#mymemberships");
}
?>