<?php
    ob_start();
	include_once('login_includes.php');
	include_once('classes/Fan.php');
	
	$verification_no=$_GET['id'];
	$subscription_id=$_GET['s'];
	$reg_status="Complete";
	
	$obj=new Fan();
	if($obj->checkVerificationNo($verification_no))
	{
		$obj->updateVerification($verification_no);
		
		//The line below is added just temporarily to divert user to demo site. This should be removed when actual site is ready.
		header('location:http://www.purifyentertainment.net/Design/index.php');
		//-----------------------------------------------------------------------------------------------------------------------		
		
		switch($subscription_id)
		{
			case 1:	$msg="Thank you for subscribing to our network. When you login above your account will begin with 100 Purify Points and you can start building your library. Enjoy!";
					$subscription_name="Founder Subscription";
					break;
			case 2: $msg="Thank you for subscribing to our network. When you login above your account will begin with 50 Purify Points and you can start building your library. Enjoy!";
					$subscription_name="Life Time Subscription";
					break;
			case 3: $msg="Thank you for subscribing to our network. When you login above your account will begin with 50 Purify Points and you can start building your library. Enjoy!";
					$subscription_name="Annual Subscription";
					break;
			default:$msg="Error in registration. Either your registration request is already in progress or the link is bad.";
					$reg_status="Failed";
					break;
		}
	}
	else
	{
		$reg_status="Failed";
		$msg="Error in registration. Either your registration request is already in progress or the link is bad.";
	}
?>

<?php include_once('includes/header.php'); ?>
<title>Purify Entertainment: Six Feature</title>
	
  <div id="contentContainer">
    <h1><?php echo $subscription_name; ?> Registration <?php echo $reg_status; ?></h1>
    <p><?php echo $msg; ?></p>
  </div>
  
<?php include_once('includes/footer.php'); ?>