<?php
include("commons/db.php");
include('classes/EditGallery.php');
//echo $_GET['gal_id'];
$new_cls_obj=new EditGallery();
$root = realpath($_SERVER["DOCUMENT_ROOT"]);
?>
<!--<script src="ui/jquery-1.7.2.js"></script>-->
<link rel="stylesheet" type="text/css" media="screen" href="includes/multiple_select.css"/>
<script type="text/javascript" src="uploadify/swfobject.js"></script>
<script type="text/javascript" src="uploadify/jquery.uploadify.v2.1.4.min.js"></script>
<link href="uploadify/uploadify.css" rel="stylesheet"/>
<script>
var myUploader = null;
var myAudioUploader = null;
//var iMaxUploadSize = 10485760; //10MB
var iMaxUploadSize = 104857600; //100MB

$("document").ready(function(){

if(!myUploader){
myUploader = {
	uploadify : function(){
		$('#file_upload').uploadify({
			'uploader'  : './uploadify/uploadify.swf',
			'script'    : 'uploadmediagallery.php',
			'cancelImg' : './uploadify/cancel.png',
			'folder'    : './uploads/',
			'auto'      : true,
			'multi'		: true,
			'removeCompleted' : true,
			'wmode'		: 'transparent',
			'buttonText': 'Upload Gallery',
			'fileExt'     : '*.jpg;*.gif;*.png;*.PNG',
			'fileDesc'    : 'Image Files',
			'simUploadLimit' : 50,
			'sizeLimit'	: 1024*1024*100, //100 MB size
			'onComplete': function(event, ID, fileObj, response, data) {
				//alert(response);
				// On File Upload Completion			
				//location = 'uploadtos3.php?uploads=complete';
				//alert(fileObj.size);
				var gim = '';
				if(document.getElementById("galleryimages[]"))
				{
				gim=document.getElementById("galleryimages[]");
				
				gim.value=gim.value + "," + response;
				var gim_sz=document.getElementById("galleryimagessize[]");
				gim_sz.value = gim_sz.value + "," + fileObj.size;
				$("#uploadedImages").val($("#uploadedImages").val() + response + "|");
				$("#file_uploadEQYKRV").style.display = "block";
				}
				//alert(response);
				//$("#uploadedImagessize").val($("#uploadedImagessize").val() + response + "|");
			},
			'onSelect' : function (event, ID, fileObj){
				$.fancybox.update();
			var originalBtn = $("#insert_new_media");
			var newBtn = originalBtn.clone();
			newBtn.attr("type", "button");
			newBtn.insertBefore(originalBtn);
			originalBtn.remove();
			},
			'onAllComplete': function(event, data) {
				
				$(".uploadifyQueueItem .percentage").text("   Finished");
				$("#insert_new_media").enable=true;
				//alert("All  " + data.filesUploaded + "  Images Are Uploaded Successfully");
				var originalBtn = $("#insert_new_media");
				var newBtn = originalBtn.clone();
				newBtn.attr("type", "submit");
				newBtn.insertBefore(originalBtn);
				originalBtn.remove();
				
			},
			'onSelectOnce' : function(event, data)
			{
				// This function fires after onSelect
				// Checks total size of queue and warns user if too big
				iTotFileSize = data.allBytesTotal;
				//alert("iTotFileSize = " + iTotFileSize);
				if(iTotFileSize >= iMaxUploadSize){
					var iOverSize = (iTotFileSize - iMaxUploadSize) / 1024 / 1024; // Size, in MB
					//alert("Total file size in queue exceeds maximum 10MB allowed.  Please remove " + Math.ceil(iOverSize) + " MB worth.");
					$("#divGalleryFileSize").show().html("Maximum 10MB allowed. Total queue size approximately: <font color='red'>" + Math.ceil(iTotFileSize/1024/1024) + " (MB)</font>");
					$('#file_upload').uploadifyClearQueue();
				}
				else
				{
				   $("#divGalleryFileSize").hide() 
				}
			},
			'onOpen'	: function() {
				//hide overly
			}
			});
		}
	};
	myUploader.uploadify();
}
});
</script>


<div class="fancy_header">
	Add Gallery Images
</div>
<div class="add_imagre_whole_content">
<div class="fieldCont" id="imgUploadDiv" >
<div style="float:left;margin-right:15px">Add Images:</div>
	<div id="divGalleryFileSize" style="display:none;"></div>
	<!--<form action="" method="post" enctype="multipart/form-data">-->
		<input name="theFile" type="file" id="file_upload" />
	<!--</form>-->
</div>
<form action="" id="add_gallery_image_form" method="POST" enctype="multipart/form-data">
<!--<input type="file" name ="add_gal_img"/>
<input type="hidden" name ="xyz" value="tested" />-->
<input type="hidden" id="galleryimages[]" name="galleryimages[]" />
<input type="hidden" id="galleryimagessize[]" name="galleryimagessize[]" />
<input type="submit" value=" Submit " id="insert_new_media" class="round_black_button"/>
</form>
</div>

<script type="text/javascript">
	$("#add_gallery_image_form").bind("submit",function(){
		$.ajax({
			type : "POST",
			url  : "add_gallery_images.php?id=<?php echo $_GET['gal_id']; ?>",
			data : $(this).serializeArray(),
			success : function(data){
				parent.jQuery.fancybox.close();
			}
		});
		return false;
	});
</script>
<?php
if(isset($_POST) && $_POST!= null)
{
	$media_id=$_GET['id'];
	/*if($_FILES["add_gal_img"]["error"]>0)
	{
		echo "Error".$_FILES["add_gal_img"]["error"]."</br>";
	}
	else
	{
		$tempFile = $_FILES['add_gal_img']['tmp_name'];
		//$fileName = time().'-'.str_replace(" ","_",$_FILES['add_gal_img']['name']);
		$ext = substr($_FILES['add_gal_img']['name'], strpos($_FILES['add_gal_img']['name'],'.'), strlen($_FILES['add_gal_img']['name'])-1);     
		$fileName = time()."_image".$ext;
        if (!class_exists('S3')) require_once ($root.'/S3.php');
		if (!defined('awsAccessKey')) define('awsAccessKey', '');
		if (!defined('awsSecretKey')) define('awsSecretKey', '');
		$s3 = new S3(awsAccessKey, awsSecretKey);
		$bucket_name = "medgallery";
		$s3->putBucket($bucket_name, S3::ACL_PUBLIC_READ);
		$s3->putObject(S3::inputFile($tempFile),$bucket_name,$fileName, S3::ACL_PUBLIC_READ);
		//move_uploaded_file($_FILES["add_gal_img"]["tmp_name"],"uploads/Media/gallery/" . $fileName);
		$targetPath = "http://medgallery.s3.amazonaws.com/";
		$targetPathThumb = $_SERVER['DOCUMENT_ROOT'] .'/uploads/Media/thumb/';
		$file_size = $_FILES['add_gal_img']['size'];
		createThumbnail($fileName, $targetPath, $targetPathThumb);
	}*/
	
	$get_gal_id =$new_cls_obj->Get_MediaImages($media_id);
	$res_gal_id=mysql_fetch_assoc($get_gal_id);
	
	foreach($_POST['galleryimages'] as $im)
	{
		$image_name=explode(",",$im);
	}
	foreach($_POST['galleryimagessize'] as $im_sz)
	{
		$image_size=explode(",",$im_sz);
	}
	for($i=0;$i<count($image_name);$i++)
	{
		if($image_name[$i]==""){continue;}
		else{
			echo $image_size[$i];
			$file_size_mb = $image_size[$i] / 1048576;
			$add_img=$new_cls_obj->Insert_Media_Images($media_id,$res_gal_id['gallery_id'],$image_name[$i],substr($file_size_mb,0,7),$res_gal_id['image_url'],$res_gal_id['agreement'],$res_gal_id['upload_to'],$res_gal_id['for_sale'],$res_gal_id['profile_image']);
		}
	}
	?>
	<!--<script>
	parent.jQuery.fancybox.close();
	</script>-->
	<?php
	//var_dump($_FILES);
}



/*
function createThumbnail($filename, $path_to_image_directory, $path_to_thumbs_directory) {

	$final_width_of_image = 200;
	//$path_to_image_directory = 'images/fullsized/';
	//$path_to_thumbs_directory = 'images/thumbs/';

	if(preg_match('/[.](jpg)$/', $filename)) {
		$im = imagecreatefromjpeg($path_to_image_directory . $filename);
	} else if (preg_match('/[.](gif)$/', $filename)) {
		$im = imagecreatefromgif($path_to_image_directory . $filename);
	} else if (preg_match('/[.](png)$/', $filename)) {
		$im = imagecreatefrompng($path_to_image_directory . $filename);
	}

	$ox = imagesx($im);
	$oy = imagesy($im);

	$nx = $final_width_of_image;
	$ny = floor($oy * ($final_width_of_image / $ox));

	$nm = imagecreatetruecolor($nx, $ny);

	imagecopyresized($nm, $im, 0,0,0,0,$nx,$ny,$ox,$oy);

	if(!file_exists($path_to_thumbs_directory)) {
	  if(!mkdir($path_to_thumbs_directory)) {
           die("There was a problem. Please try again!");
	  }
       }

	imagejpeg($nm, $path_to_thumbs_directory . $filename);
	
	$tempFile = $nm;
	$bucket_name = "medgalthumb";    
    if (!class_exists('S3')) require_once ($root.'/S3.php');
	if (!defined('awsAccessKey')) define('awsAccessKey', '');
	if (!defined('awsSecretKey')) define('awsSecretKey', '');
	$s3 = new S3(awsAccessKey, awsSecretKey);
	$s3->putBucket($bucket_name, S3::ACL_PUBLIC_READ);
	$s3->putObject(S3::inputFile($path_to_thumbs_directory.$filename),$bucket_name,$filename,S3::ACL_PUBLIC_READ);
	unlink($path_to_thumbs_directory.$filename);
	echo $filename;
	//echo $tn;
}*/
?>