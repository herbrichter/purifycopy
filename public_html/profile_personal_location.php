<?php
	ob_start();
	include_once('commons/session_check.php');
	include_once('classes/User.php');
		
	session_start();
	$username = $_SESSION['username'];
	if(!$username == '')
	{
		$login_flag=1;
	}
	
	if($login_flag) include_once('loggedin_includes.php');
	else header('location:index.php');
	
	$obj=new User();
	$row=$obj->getUserInfo($username);

	if(isset($_POST['location']))
	{
		$country=$_POST['country'];
		$state_or_province=$_POST['state_or_province'];
		$city=$_POST['city'];	
		
		$obj->changeLocation($username,$country,$state_or_province,$city);
		header('location:'.$_SERVER['PHP_SELF']);
	}
?>
<script>
function editInfo()
{
	$.post("edit_location.php",
		function(data)
		{
		//alert("Data Loaded: " + data);
			$("#location").replaceWith(data);
			
		}
	);	
}	
</script>

<?php include_once('includes/header.php'); ?>
  <title>Purify Entertainment: Profile</title>

  <div id="contentContainer">
	<?php include_once('includes/subnavigation_profile.php'); ?>
    <div id="actualContent">
      <h1>Location</h1>
      <div id="location">
		<?php include_once('display_location.php'); ?>
      </div>
    </div>
    
    <div class="clearMe"></div>
    
  </div>

<?php include_once('includes/footer.php'); ?>