    <!--<script src="community_community_community_community_jcrop/js/jquery.min.js" type="text/javascript"></script>-->
    <script src="media_channel_jcrop/js/jquery.Jcrop.js" type="text/javascript"></script>
    <link rel="stylesheet" href="media_channel_jcrop/css/jquery.Jcrop.css" type="text/css" />
    <link rel="stylesheet" href="media_channel_jcrop/demo_files/demos.css" type="text/css" />

	<!--<script src="media_channel_jcrop/js/popup.js" type="text/javascript"></script>
    <link rel="stylesheet" href="media_channel_jcrop/css/popup.css" type="text/css" />-->

	<script type="text/javascript" src="uploadify/swfobject.js"></script>
	<script type="text/javascript" src="uploadify/jquery.uploadify.v2.1.4.min.js"></script>
	<link href="uploadify/uploadify.css" rel="stylesheet"/>

	<?php
		/*if(isset($_SESSION['login_email']))
		{
			$sql = mysql_query("select * from  general_user where email='".$_SESSION['login_email']."'");
			$res=mysql_fetch_assoc($sql);
			//var_dump($res);

			$sql1=mysql_query("select * from  general_community where community_id = '".$res['community_id']."'");
			$res1=mysql_fetch_assoc($sql1);
		}*/
		$random_number = rand(1,1000000);
	?>
	<script type="text/javascript">
	var myUploader_pro_image = null;
	var chk_uploader = 0;
	var iMaxUploadSize = 10485760; //10MB
	var IMG_UPLOAD_PATH = "media_channel_jcrop/croppingFiles/";
	// Create variables (in this scope) to hold the API and image size
	var artist_project_jcrop_api, boundx, boundy;
	var allowResizeCropper = false;
    jQuery(function($){
		$("#cropBtn").click(function(){
			if($('#x').val()!="" && $("#target").attr('src')!=""){
				$.ajax({
					type: "POST",
					url: 'media_channel_jcrop/cropImage.php',
					data: { "action":'crop',"resize":allowResizeCropper,"x":$('#x').val(), "y":$('#y').val(), "w":$('#w').val(), "h":$('#h').val(), "srcImg":$("#target").attr('src')},
					success: function(data){
						//alert($("#target").attr('src'));
						//disablePopup();
						//window.location.href="profileedit_community.php";
						//window.location.reload();
						parent.jQuery.fancybox.close();
						if(allowResizeCropper=="250")
						{
							var aepro_img= new Date();
							//alert(a.getTime());
							data5 = data + "?fet=" + aepro_img.getTime();
							
							$("#resultImgThumb").attr('src',data5);
							$("#resultImgThumb").show();
							
							var pro_pic_thum=window.parent.document.getElementById("med_gallery_image");
							pro_pic_thum.value=data;
							
							$("#Noimagediv_listing").hide();
							$("#stored_listing_image").hide();
						}
						else
						{
							var aepro_img= new Date();
							//alert(a.getTime());
							data5 = data + "?fet=" + aepro_img.getTime();
							
							$("#resultImgThumb").attr('src',data5);
							$("#resultImgThumb").show();
							
							var pro_pic_thum=window.parent.document.getElementById("med_channel_image");
							pro_pic_thum.value=data;
							$("#Noimagediv_listing").hide();
							$("#stored_listing_image").hide();
						}
						
					}
				});
			}
		});
    });


	function generateUploader(){
	
	//alert(myUploader_pro_image);
		if(chk_uploader ==0){
			myUploader_pro_image = {
				uploadify : function(){
					$('#file_upload_image').uploadify({
						'uploader'  : './uploadify/uploadify.swf',
						'script'    : 'media_channel_jcrop/uploadFiles.php?id='+allowResizeCropper,
						'cancelImg' : 'uploadify/cancel.png',
						'folder'    : 'media_channel_jcrop/croppingFiles/',
						'auto'      : true,
						'multi'		: true,
						'removeCompleted' : true,
						'wmode'		: 'transparent',
						'buttonText': 'Upload Image',
						'fileExt'     : '*.jpg;*.gif;*.png',
						'fileDesc'    : 'Image Files',
						'simUploadLimit' : 1,
						'sizeLimit'	: iMaxUploadSize, //10 MB size
						'onComplete': function(event, ID, fileObj, response, data) {
							if(response=="invalid"){
								$("#errMsg").html("Invalid image size.");
							}else{
								$("#errMsg").html('');
								$("#cropperSpace").show();
								//alert(IMG_UPLOAD_PATH);
								//alert(response);
								generateCropperObj(response);
							}
							setTimeout(function(){
								$.fancybox.update();
							},1000);
						},
						'onSelect' : function (event, ID, fileObj){
						},
						'onSelectOnce' : function(event, data)
						{
							/*iTotFileSize = data.allBytesTotal;
							if(iTotFileSize >= iMaxUploadSize){
								var iOverSize = (iTotFileSize - iMaxUploadSize) / 1024 / 1024; // Size, in MB
								$("#divGalleryFileSize").show().html("Maximum 10MB allowed. Total queue size approximately: <font color='red'>" + Math.ceil(iTotFileSize/1024/1024) + " (MB)</font>");
								$('#file_upload').uploadifyClearQueue();
							}
							else
							{
							   $("#divGalleryFileSize").hide() 
							}*/
						},
						'onOpen'	: function() {
							//hide overly
						}
						/*,
						'onError'     : function (event,ID,fileObj,errorObj) {
						  alert(errorObj.type + ' Error: ' + errorObj.info);
						},
						'onProgress'  : function(event,ID,fileObj,data) {
						  var bytes = Math.round(data.bytesLoaded / 1024);
						  $('#containerResult').html(' - ' + bytes + 'KB Uploaded');
						  return false;
						}*/

						});
					}
				};
				myUploader_pro_image.uploadify();
				chk_uploader =1;
		}
	}

function generateCropperObj(img){
//alert(img);
      if(artist_project_jcrop_api){ 
		artist_project_jcrop_api.destroy(); 
	  }
		$("#targetTD").html('<img id="target">');
		$("#target").attr('src',img);
		$("#preview").attr('src',img);

      $('#target').Jcrop({
        onChange: updatePreview,
        onSelect: updateCoords,
		allowResize: allowResizeCropper,
		allowSelect: allowResizeCropper,
        aspectRatio: 1
      },function(){
        // Use the API to get the real image size
        var bounds = this.getBounds();
        boundx = bounds[0];
        boundy = bounds[1];
        artist_project_jcrop_api = this;
		if(!allowResizeCropper){
			artist_project_jcrop_api.animateTo([0,0,450,450]);
			artist_project_jcrop_api.setOptions({ allowSelect: false });
			artist_project_jcrop_api.setOptions({ allowResize: true });
			artist_project_jcrop_api.setOptions(true? {
            minSize: [ 450, 450 ],
            maxSize: [ 0, 0 ]
          }: {
            minSize: [ 0, 0 ],
            maxSize: [ 0, 0 ]
          });
		  $("#hinttext").text("Image size should be more than 450x450.");
		}
		else if(allowResizeCropper=="350"){
			artist_project_jcrop_api.animateTo([0,0,200,200]);
			artist_project_jcrop_api.setOptions({ allowSelect: false });
			artist_project_jcrop_api.setOptions({ allowResize: true });
			artist_project_jcrop_api.setOptions(true? {
            minSize: [ 200, 200 ],
            maxSize: [ 0, 0 ]
          }: {
            minSize: [ 0, 0 ],
            maxSize: [ 0, 0 ]
          });
		  $("#hinttext").text("Image size should be more than 450x450.");
		}
		else if(allowResizeCropper=="250"){
			artist_project_jcrop_api.animateTo([0,0,200,200]);
			artist_project_jcrop_api.setOptions({ allowSelect: false });
			artist_project_jcrop_api.setOptions({ allowResize: true });
			artist_project_jcrop_api.setOptions(true? {
            minSize: [ 200, 200 ],
            maxSize: [ 0, 0 ]
          }: {
            minSize: [ 0, 0 ],
            maxSize: [ 0, 0 ]
          });
		  $("#hinttext").text("Image size should be more than 450x450.");
		}
        // Store the API in the artist_project_jcrop_api variable
      });

	function updateCoords(c)
	{
		$('#x').val(c.x);
		$('#y').val(c.y);
		$('#w').val(c.w);
		$('#h').val(c.h);
		updatePreview(c);
	};

      function updatePreview(c)
      {
        if (parseInt(c.w) > 0)
        {
          var rx = 100 / c.w;
          var ry = 100 / c.h;

          $('#preview').css({
            width: Math.round(rx * boundx) + 'px',
            height: Math.round(ry * boundy) + 'px',
            marginLeft: '-' + Math.round(rx * c.x) + 'px',
            marginTop: '-' + Math.round(ry * c.y) + 'px'
          });
        }
      };
}

function showCropper(resizeFlag){
	allowResizeCropper = resizeFlag;
	if(allowResizeCropper=="350" && myUploader_pro_image=="null")
	{
	//alert("hi");
		$("#cropperSpace").hide();
	}
	else if(allowResizeCropper=="250" && myUploader_pro_image=="null")
	{
	//alert("hi");
		$("#cropperSpace").hide();
	}
	else
	{
		$("#cropperSpace").show();
	}
	//centerPopup();	loadPopup(); 
	generateUploader(); //generateCropperObj();
}


function get_profile_pic()
{
		var pro=document.getElementById("profile_image");
		if(pro.value!="")
		{
			response =pro.value;
			var img=response.split("/");
			var image_name = img[3].substr(10);
			var response = "https://medchannelimage.s3.amazonaws.com/" + image_name;
			
			$("#cropperSpace").show();
			allowResizeCropper=false;
			generateCropperObj(response);
		}
		else
		{
			return;
		}
	
}
function get_listing_pic(newresizeFlag)
{
	$("#media_jcrop_header").html("Playlist Picture");
	showCropper(newresizeFlag);
	updateCoords_manually();
	var list_pro= document.getElementById("med_channel_image");
	if(list_pro.value!="")
	{
		response =list_pro.value;
		var img=response.split("/");
		//var image_name = img[3].substr(10);
		var image_name = img[3];
		var response = "https://medchannelimage.s3.amazonaws.com/" + image_name;
		
		$("#cropperSpace").show();
		allowResizeCropper="350";
		generateCropperObj(response);
		myUploader_pro_image= "Notnull";
		return;
	}
	else
	{
		var list_preview= document.getElementById("preview");
		list_preview.src=" " ;
		myUploader_pro_image="null";
		return;
	}
}
function get_listing_pic_gallery(newresizeFlag)
{
	$("#media_jcrop_header").html("Gallery Picture");
	showCropper(newresizeFlag);
	updateCoords_manually();
	var list_pro= document.getElementById("med_gallery_image");
	if(list_pro.value!="")
	{
		response =list_pro.value;
		var img=response.split("/");
		//var image_name = img[3].substr(10);
		var image_name = img[3];
		var response = "https://medgallery.s3.amazonaws.com/" + image_name;
		
		$("#cropperSpace").show();
		allowResizeCropper="250";
		generateCropperObj(response);
		myUploader_pro_image= "Notnull";
		return;
	}
	else
	{
		var list_preview= document.getElementById("preview");
		list_preview.src=" " ;
		myUploader_pro_image="null";
		return;
	}
}


$(document).ready(function(){
if(!allowResizeCropper)
{
	$("#resultImgProfile").hide();
	$("#resultImgThumb").hide();
}
});

function updateCoords_manually()
{
	$('#x').val(0);
	$('#y').val(0);
	$('#w').val(200);
	$('#h').val(200);
}

/*Script For Detecting Flash*/
$("document").ready(function(){
	var flashEnabled = !!(navigator.mimeTypes["application/x-shockwave-flash"] || window.ActiveXObject && new ActiveXObject('ShockwaveFlash.ShockwaveFlash'));
	if (!flashEnabled) { 
		$("#Flashnotfound_img_crop").css({"display":"block","color":"red","float":"left","width":"300px","position":"relative"});
	}
});
/*Script For Detecting Flash Ends Here*/
  </script>

<div class="fieldCont">
<?php
if(isset($_GET['edit']) && $editmedia['media_type']=="113")
{
?>
	<div id="yrt_1" class="fieldTitle">Gallery Cover</div>
<?php
}
else
{
?>
	<div id="yrt_2" class="fieldTitle">Profile Image</div>
<?php
}
if(isset($_GET['edit']) && $get_edit_mediatype['profile_image']!="")
{
?>
<img src="<?php if(isset($_GET['edit'])) echo $get_edit_mediatype['profile_image'];?>" width="100" height="100" id="stored_listing_image"/>
<img id="resultImgThumb" height="100" width="100"/>
<?php
}
else
{
?>
<img id="resultImgThumb" height="100" width="100"/>
<img id="Noimagediv_listing" src="https://regprofilepic.s3.amazonaws.com/Noimage.png" height="100" width="100"/>
<?php
}
?>
<div id="channel_crop">
<!--<a href="javascript:showCropper(350);" onclick="get_listing_pic()" class="hint" style="text-decoration:none;"><input type="button" value=" Change "/></a>-->
<a href="#popupContact_media_jcrop" style="text-decoration:none;" class="fancybox_media_crop" onClick="get_listing_pic(350)"><input class="blackhint" type="button" value=" Change "/></a>
</div>
<div id="gallery_crop">
<!--<a href="javascript:showCropper(250);" onclick="get_listing_pic_gallery()" class="hint" style="text-decoration:none;"><input type="button" value=" Change "/></a>-->
<a href="#popupContact_media_jcrop" style="text-decoration:none;" class="fancybox_media_crop" onClick="get_listing_pic_gallery(250)"><input class="blackhint" type="button" value=" Change "/></a>
</div>

</div>
<script type="text/javascript">
	$(".fancybox_media_crop").fancybox({
		helpers : {
			media : {},
			buttons : {},
			title : null
		},
		afterShow: function(){
			var resize = setTimeout(function(){
			$.fancybox.update();
			},1000);
		}
	});
</script>
<!-- POPUP BOX START -->
<div id="popupContact_media_jcrop" style="display:none;">
	<div class="fancy_header" id="media_jcrop_header">
	</div>
	<div class="pop_whole_image_content" >
		<div class="popup_image_preview_box">
			Preview
			<div class="popup_image_preview">
				<img src="" id="preview" alt="Preview" class="artist_project_jcrop-preview" />
			</div>
		</div>
		<div id="Flashnotfound_img_crop" style="display:none;">
			Either your flash player plugin is disabled or if you don't have the flash player <a href="http://get.adobe.com/flashplayer/" target="_blank">Click Here</a> to install.
		</div>
		<div class="pop_image_function_button">
			<form action="" methos="post" enctype="multipart/form-data" class="pop_image_form">
				<div class="pop_image_upload_button"><input name="theFile_pro_image" type="file" id="file_upload_image" /></div>
				<div id="hinttext" class="pop_image_upload_hinttext">Image size should be more than 450x450.</div>
			</form>
			
			<div class="pop_image_crop_button">
				<input type="button" value="Crop Image" id="cropBtn" style=" background-color: #404040;border-radius: 3px 3px 3px 3px;color: white;cursor: pointer;padding: 7px 24px;border:none;"/>
				<input type="hidden" id="x" name="x" />
				<input type="hidden" id="y" name="y" />
				<input type="hidden" id="w" name="w" />
				<input type="hidden" id="h" name="h" />
			</div>
		</div>
		<div style="clear:both"></div>
        <div id="errMsg" style="color:red;text-align:center;"></div>
		<table id="cropperSpace" style="display:none;  bottom: 100px;" class="big_image">
			<tr>
				<td rowspan="2" id="targetTD">
					<img src="" style="max-width:400px" id="target"/>				
				</td>
			</tr>
			<!--<tr>
			    <td>Result
					<div style="width:100px;height:100px;overflow:hidden;">
						<img  id="result" alt="Result" class="artist_project_jcrop-result" style="width:100px;height:100px;" />
					</div>
				</td>
		    </tr>-->
		</table>
	</div>
</div>
<div id="backgroundPopup"></div>
<!-- POPUP BOX END -->