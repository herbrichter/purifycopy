<?php
include("../commons/db.php");
error_reporting(0);
/**
 * Jcrop image cropping plugin for jQuery
 * Example cropping script
 * @copyright 2008-2009 Kelly Hallman
 * More info: http://deepliquid.com/content/Jcrop_Implementation_Theory.html
 */
if ($_REQUEST['action'] == 'crop')
{
	if($_REQUEST['resize']=="250")
	{
		$filename = basename($_REQUEST['srcImg']);
		$path_to_image_directory = "./croppedFiles/";
		$filename = $filename;
		$path_to_image_directory = "./croppedFiles/thumb/";
		$targ_w = 200;
		$targ_h = 175;
		$id=explode($filename,'_');

		$jpeg_quality = 100;
		$src = $_REQUEST['srcImg'];//'demo_files/pool.jpg';
		//$src = "../".$src;
		if(!file_exists($_REQUEST['srcImg']))
		{
			//echo $src;
			//echo "file not exists";
		}
		$img_r = imagecreatefromjpeg($src);
		//print_r($img_r);
		$dst_r = ImageCreateTrueColor( $targ_w, $targ_h );

		imagecopyresampled($dst_r,$img_r,0,0,$_POST['x'],$_POST['y'],$targ_w,$targ_h,$_POST['w'],$_POST['h']);

		//	header('Content-type: image/jpeg');
		imagejpeg($dst_r,$path_to_image_directory.$filename,$jpeg_quality);
		
		//echo $dst_r;
		//echo 'artist_project_jcrop/'.$path_to_image_directory.$filename;
		
        $root = realpath($_SERVER["DOCUMENT_ROOT"]);
        if (!class_exists('S3')) require_once ($root.'/S3.php');
		if (!defined('awsAccessKey')) define('awsAccessKey', '');
		if (!defined('awsSecretKey')) define('awsSecretKey', '');
		$s3 = new S3(awsAccessKey, awsSecretKey);
		$bucket_name = "medgalthumb";
		$s3->putObject(S3::inputFile($path_to_image_directory.$filename),$bucket_name,$filename, S3::ACL_PUBLIC_READ);
		unlink($path_to_image_directory.$filename);
		echo "http://medgalthumb.s3.amazonaws.com/" .$filename;
	}
	else{

		$filename = basename($_REQUEST['srcImg']);
	
		$path_to_image_directory = "./croppedFiles/";
		$filename = $filename;
		$path_to_image_directory = "./croppedFiles/thumb/";
		$targ_w = 200;
		$targ_h = 175;
		$id=explode($filename,'_');

		$jpeg_quality = 100;
		$src = $_REQUEST['srcImg'];//'demo_files/pool.jpg';
		//$src = "../".$src;
		if(!file_exists($_REQUEST['srcImg']))
		{
			//echo $src;
			//echo "file not exists";
		}
		$img_r = imagecreatefromjpeg($src);
		//print_r($img_r);
		$dst_r = ImageCreateTrueColor( $targ_w, $targ_h );

		imagecopyresampled($dst_r,$img_r,0,0,$_POST['x'],$_POST['y'],$targ_w,$targ_h,$_POST['w'],$_POST['h']);

	//	header('Content-type: image/jpeg');
		imagejpeg($dst_r,$path_to_image_directory.$filename,$jpeg_quality);
		
		//echo $dst_r;
		//echo 'artist_project_jcrop/'.$path_to_image_directory.$filename;
		
        $root = realpath($_SERVER["DOCUMENT_ROOT"]);
        if (!class_exists('S3')) require_once ($root.'/S3.php');
		if (!defined('awsAccessKey')) define('awsAccessKey', '');
		if (!defined('awsSecretKey')) define('awsSecretKey', '');
		$s3 = new S3(awsAccessKey, awsSecretKey);
		$bucket_name = "medchannelthumb";
		$s3->putObject(S3::inputFile($path_to_image_directory.$filename),$bucket_name,$filename, S3::ACL_PUBLIC_READ);
		unlink($path_to_image_directory.$filename);
		echo "http://medchannelthumb.s3.amazonaws.com/" .$filename;
	}
}
?>