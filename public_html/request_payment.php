<?php
	session_start();
	include_once('commons/db.php');
	include_once('classes/Payment_request.php');
	include_once('sendgrid/SendGrid_loader.php');
	$pay_obj = new Payment_request();
	$sendgrid = new SendGrid('','');
?>
<link rel="stylesheet" type="text/css" media="screen" href="includes/purify.css" />
<!--<script src='https://ajax.googleapis.com/ajax/libs/jquery/1.6.1/jquery.min.js'></script>-->
<script type="text/javascript">
$("#submit_payment_request_chk").bind("submit",function(){
	$.ajax({
		type : "POST",
		url  : "request_payment.php?amt=<?php echo $_GET['amt']; ?>",
		data : $(this).serializeArray(),
		success : function(data){
			parent.jQuery.fancybox.close();
		}
	});
	return false;
});
$("#submit_payment_request_paypal").bind("submit",function(){
	$.ajax({
		type : "POST",
		url  : "request_payment.php?amt=<?php echo $_GET['amt']; ?>",
		data : $(this).serializeArray(),
		success : function(data){
			parent.jQuery.fancybox.close();
		}
	});
	return false;
});
</script>
	<div class="fancy_header">
		Request Payment
	</div>
	<div id="actualContent" class="transaction_content">
<?php
		if(empty($_POST) && (!isset($_POST['email_pay']) || !isset($_POST['addrr_pay'])))
		{
?>
			<div class="fieldCont" style="width:450px;">
				<div class="fieldTitle" style="width:150px;">Payment Request By:</div>
				<div class="chkCont">
					<input type="radio" class="chk" name="pay_chek" onClick="on_values()" value="pal"/>
					<div class="radioTitle" >Paypal</div>
				</div>
				<div class="chkCont">
					<input type="radio" class="chk" name="pay_chek" onClick="off_values()" value="chek"/>
					<div class="radioTitle">Check</div>
				</div>
			</div>
			<p style="margin-bottom:0;">*If you have your bank account added to your paypal account than you will not receive any fee.<br/>
			*Checks can only be sent to locations within the USA and will receive a $10 service fee taken out of the balance requested.</p>
			<form method="POST" action="" id="submit_payment_request_paypal" onSubmit="return validate_pay()" >
				<div class="fieldCont" id="paypal_payment" style="width:450px; display:none;margin-bottom:0;">
					<h3>Paypal Account:</h3>
					<div class="fieldTitle" style="width:150px;">Email:</div>
					<div class="chkCont">
						<input type="text" style="height:25px;" class="fieldText" name="email_pay" id="email_pay" />
					</div>
					<div class="fieldCont" style="width:450px;margin-bottom:0;">
						<input class="round_black_button" type="submit" value="Submit" />
					</div>
				</div>
			</form>
			
			<form method="POST" action="" id="submit_payment_request_chk" onSubmit="return validate_chk()" >
				<div class="fieldCont" id="check_payment" style="width:450px; display:none;margin-bottom:0;">
				<h3>Check payable to:</h3>
					<div class="fieldTitle" style="width:150px;">Name:</div>
					<div class="chkCont">
						<input type="text" style="height:25px;" class="fieldText" name="name_pay" id="name_pay" />
					</div>
					<div class="fieldTitle" style="width:150px;">Address:</div>
					<div class="chkCont">
						<input type="text" style="height:25px;" class="fieldText" name="addrr_pay" id="addrr_pay" />
					</div>
					<div class="fieldTitle" style="width:150px;">City:</div>
					<div class="chkCont">
						<input type="text" style="height:25px;" class="fieldText" name="city_pay" id="city_pay" />
					</div>
					<div class="fieldTitle" style="width:150px;">Zip Code:</div>
					<div class="chkCont">
						<input type="text" style="height:25px;" class="fieldText" name="zip_pay" id="zip_pay" />
					</div>
					<div class="fieldCont" style="width:450px;margin-bottom:0;">
						<input class="round_black_button" type="submit" value="Submit" />
					</div>
				</div>
			</form>
<?php
		}
		else
		{
			$ans = $pay_obj->get_gen_user($_SESSION['login_email']);
			$run_ans = mysql_fetch_assoc($ans);
			if(isset($_POST['email_pay']) && $_POST['email_pay']!="")
			{
				$objs = $pay_obj->Paypal_pay($_GET['amt'],$_POST['email_pay']);
				
				$to = 'info@purifyart.com';
				//$to = 'mithesh@remotedataexchange.com';
				$subject = "Payment Request";
				$header = "from: Purify Art < no-reply@purifyart.com>"."\r\n";
				$message = $run_ans['fname']." ".$run_ans['lname']." has requested payment of ".$_GET['amt']." via Paypal. Their payment information is ".$_POST['email_pay'];
				
				$url = "admin/newsletter/"; 
				$myFile = $url . "other_templates.txt";
				$fh = fopen($myFile, 'r');
				$theData = fread($fh, filesize($myFile));
				fclose($fh);
				
				$data_email = explode('|~|', $theData);
			
				$ns_user_n = str_replace('{::message::}', $message, $data_email['1']);
				
				$body_email = $data_email['0'] . $ns_user_n . $data_email['2'];
				
				$mail_grid = new SendGrid\Mail();
				
				$mail_grid->addTo($to)->
					setFromName('Purify Art')->
					setFrom('no-reply@purifyart.com')->
					setSubject($subject)->
					setHtml($body_email);
					
				//$sendgrid->web->send($mail_grid);
				$sendgrid -> smtp -> send($mail_grid); 
				//$sentmail = mail($to,$subject,$message,$header);
			}
			elseif(isset($_POST['addrr_pay']) && $_POST['addrr_pay']!="")
			{
				$objs = $pay_obj->Check_pay($_GET['amt'],$_POST['name_pay'],$_POST['addrr_pay'],$_POST['city_pay'],$_POST['zip_pay']);
				$newamt1 = $_GET['amt'] -10;
				$to = 'info@purifyart.com';
				//$to = 'mithesh@remotedataexchange.com';
				$subject = "Payment Request";
				$header = "from: Purify Art < no-reply@purifyart.com>"."\r\n";
				$message = $run_ans['fname']." ".$run_ans['lname']." has requested payment of ".$newamt1." via Check. Their payment information is ".$_POST['addrr_pay'].', '.$_POST['city_pay'].', '.$_POST['zip_pay'];
				
				$url = "admin/newsletter/"; 
				$myFile = $url . "other_templates.txt";
				$fh = fopen($myFile, 'r');
				$theData = fread($fh, filesize($myFile));
				fclose($fh);
				
				$data_email = explode('|~|', $theData);
			
				$ns_user_n = str_replace('{::message::}', $message, $data_email['1']);
				
				$body_email = $data_email['0'] . $ns_user_n . $data_email['2'];
				
				$mail_grid = new SendGrid\Mail();
				
				$mail_grid->addTo($to)->
					setFromName('Purify Art')->
					setFrom('no-reply@purifyart.com')->
					setSubject($subject)->
					setHtml($body_email);
					
				//$sendgrid->web->send($mail_grid);
				$sendgrid -> smtp -> send($mail_grid); 
				
				//$sentmail = mail($to,$subject,$message,$header);
			}
			/*if($_GET['amt']>10){
				$newamt = $_GET['amt'] -10;
				echo "<p>You are requesting a check of $$newamt to $_POST[name_pay] at $_POST[addrr_pay], $_POST[city_pay], $_POST[zip_pay]. </p>";
			}else{
				echo "<p>Due to the $10 service charge for check requests you must have at least an $11 balance to request a check.</p>";
			}*/
?>
			
			<p>Your payment request has been received. Please allow 48hrs for your request to be processed. If you have any questions email sales@purifyart.com.</p>
			<input style="text-decoration:none; background-color: #000000; border: 0 none; color: #FFFFFF; float: left; font-family: 'futura-pt',sans-serif; font-size: 0.825em; font-weight: bold; margin-right: 10px; padding: 6px 10px;" type="button" value="Close" onClick="closed_box();" />
			
				<script>
					function closed_box()
					{
						//window.parent.location.reload();
						parent.jQuery.fancybox.close();
						//parent.window.location.reload();
					}
				</script>
			<?php
		}
?>
	</div>
<script>
	function validate_pay()
	{
		var emails = document.getElementById("email_pay").value;
		var atpos = emails.indexOf("@");
		var dotpos = emails.lastIndexOf(".");

		if(atpos<1 || dotpos<atpos+2 || dotpos+2>=emails.lenght || emails=="")
		{
			alert("Please enter a valid email address.");
			return false;
		}
		
		var conf = confirm("You are requesting " + <?php echo $_GET['amt']; ?> + " to be sent to your Paypal email address "+ emails +".");
		if(conf == false)
		{
			return false;
		}		
	}
	
	function validate_chk()
	{
		var name = document.getElementById("name_pay").value;
		var addr = document.getElementById("addrr_pay").value;
		var city = document.getElementById("city_pay").value;
		var zip = document.getElementById("zip_pay").value;

		if(name=="")
		{
			alert("Please enter your name.");
			return false;
		}
		if(addr=="")
		{
			alert("Please enter your address.");
			return false;
		}
		if(city=="")
		{
			alert("Please enter your City.");
			return false;
		}
		if(zip=="")
		{
			alert("Please enter your Zip Code.");
			return false;
		}
		
		//Old one
		//var conf =  confirm("You are requesting a check of " + <?php echo $_GET['amt']; ?> + " to be sent to your address "+ addr +".");
		var amount = <?php echo $_GET['amt']; ?>;
		var conf ="";
		var newamt = 0;
		if(amount>10){
			newamt = parseFloat(amount-10).toFixed(2);
			
			conf = confirm("You are requesting a check of $" + newamt + " to " + name + " at "+ addr +", "+city+", "+zip+".");
		}else{
			conf = confirm("Due to the $10 service charge for check requests you must have at least an $11 balance to request a check.");
			conf = false;
		}
		if(conf == false)
		{
			return false;
		}		
	}
	
	function on_values()
	{
		$("#paypal_payment").css({"display":"block"});
		$("#check_payment").css({"display":"none"});
	}
	
	function off_values()
	{
		$("#paypal_payment").css({"display":"none"});
		$("#check_payment").css({"display":"block"});
	}
</script>
