<?php
session_start();
include_once("commons/db.php");
include("classes/DeleteMedia.php");

$del_media_obj = new DeleteMedia();
$root = realpath($_SERVER["DOCUMENT_ROOT"]);
if (!class_exists('S3')) require_once ($root.'/S3.php');
if (!defined('awsAccessKey')) define('awsAccessKey', '');
if (!defined('awsSecretKey')) define('awsSecretKey', '');
$s3 = new S3(awsAccessKey, awsSecretKey);

if(isset($_GET['id']) && !isset($_GET['type']) && $_GET['whos_media']=="my_media")
{
	$get_media_info = $del_media_obj->get_media_info($_GET['id']);
	if($get_media_info['sharing_preference']!=5)
	{
		if($get_media_info['media_type']==115)
		{
			
			$get_media_video = $del_media_obj->get_media_video_data($_GET['id']);
			if($get_media_video !="")
			{
				if(($get_media_video['videotitle'] !="" || $get_media_video['videolink']!="") && $get_media_video['video_name']=="")
				{
					$update_media_video = $del_media_obj->update_media_video($_GET['id']);
				}
				else
				{
					$bucket = "medvideo";
					$file = $get_media_video['video_name'];
					$s3 ->deleteObject($bucket, $file);
					$del_media = $del_media_obj->delete_media($_GET['id']);
					$del_media_data = $del_media_obj->delete_media_video_data($_GET['id']);
				}
			}
		}
		if($get_media_info['media_type']==113)
		{
			$get_media_gal = $del_media_obj->find_media_gallery($_GET['id']);
			if($get_media_gal['gallery_id']!=0)
			{
				$get_gal_images = $del_media_obj->get_gallery_images($_GET['id']);
				if(mysql_num_rows($get_gal_images)>0)
				{
					while($res_image = mysql_fetch_assoc($get_gal_images))
					{
						$s3 ->deleteObject('medgallery', $res_image['image_name']);
						$s3 ->deleteObject('medgalthumb', $res_image['image_name']);
						$gallery_id = $res_image['gallery_id'];
					}
				}
				$del_gallery = $del_media_obj->delete_gallery($gallery_id);
			}
			else if($get_media_gal['gallery_id'] ==0)
			{
				$get_single_images = $del_media_obj->get_gallery_images($_GET['id']);
				if(mysql_num_rows($get_single_images)>0)
				{
					while($res_single_image = mysql_fetch_assoc($get_single_images))
					{
						$s3 ->deleteObject('medgalhighres', $res_single_image['image_name']);
					}
				}
			}
			$del_media = $del_media_obj->delete_media($_GET['id']);
			$del_media_images = $del_media_obj->delete_media_images($_GET['id']);
		}
		if($get_media_info['media_type']==114)
		{
			$get_media_audio = $del_media_obj->get_audio_info($_GET['id']);
			if($get_media_audio != null || $get_media_audio != "")
			{	
				if($get_media_audio['song_name']=="" && ($get_media_audio['audiolinkname']!="" || $get_media_audio['audiolinkurl']!=""))
				{
					$update_media_audio = $del_media_obj->update_media_video($_GET['id']);
				}
				else
				{
					$s3 ->deleteObject('medaudio', $get_media_audio['song_name']);
					$del_media = $del_media_obj->delete_media($_GET['id']);
					$del_media_data = $del_media_obj->delete_media_audio($_GET['id']);
				}
			}
			else{
				$del_media = $del_media_obj->delete_media($_GET['id']);
			}
		}
		if($get_media_info['media_type']==116)
		{
			$get_media_channel = $del_media_obj->get_channel_image($_GET['id']);
			if($get_media_channel != null || $get_media_channel != "")
			{
				$s3 ->deleteObject('medchannelimage', $get_media_channel['profile_image']);
			}
			$del_media = $del_media_obj->delete_media($_GET['id']);
			$del_media_channel = $del_media_obj->delete_media_channel($_GET['id']);
		}
		$update_other = $del_media_obj->update_other($_GET['id']);
	}
	else
	{
		if($get_media_info['media_type']==113)
		{
			$find_media_sale = $del_media_obj->find_media_sale($_GET['id']);
			if($find_media_sale == "" || $find_media_sale == null || $find_media_sale == 0)
			{
				$get_media_name = $del_media_obj->get_sale_media_data($_GET['id']);
				if($get_media_name !="" || $get_media_name != null)
				{
					$s3 ->deleteObject('medgalhighres', $get_media_name['image_name']);
				}
				$del_media = $del_media_obj->delete_media($_GET['id']);
				$del_media_image = $del_media_obj->delete_media_images($_GET['id']);
				$del_media_sale = $del_media_obj->del_media_sale($_GET['id']);
			}
		}
		if($get_media_info['media_type']==114)
		{
			$find_song_sale = $del_media_obj->find_media_sale($_GET['id']);
			if($find_song_sale == "" || $find_song_sale == null || $find_song_sale == 0)
			{
				$del_media = $del_media_obj->delete_media($_GET['id']);
				$del_media_image = $del_media_obj->delete_media_audio($_GET['id']);
			}
		}
		if($get_media_info['media_type']==115)
		{
			$find_video_sale = $del_media_obj->find_media_sale($_GET['id']);
			if($find_video_sale == "" || $find_video_sale == null || $find_video_sale == 0)
			{
				$del_media = $del_media_obj->delete_media($_GET['id']);
				$del_media_video = $del_media_obj->delete_media_video_data($_GET['id']);
			}
		}
		$update_other = $del_media_obj->update_other($_GET['id']);
	}
	
}
if(isset($_GET['id']) && $_GET['whos_media']=="media_creator")
{
	$del_creator = $del_media_obj -> delete_media_where_heis_creator($_SESSION['login_email'],$_GET['id']);
}
if(isset($_GET['id']) && ($_GET['whos_media']=="media_creator_creator" || $_GET['whos_media']=="media_from_from" || $_GET['whos_media']=="media_creator_from_tag") && isset($_SESSION['login_email']))
{
	$del_creator = $del_media_obj -> delete_media_user_creator_creator($_GET['id'],$_SESSION['login_email']);
}
if((isset($_GET['id']) && $_GET['whos_media']=="email_subscription") || (isset($_GET['id']) && $_GET['whos_media']=="media_coming_fan") || (isset($_GET['id']) && $_GET['whos_media']=="media_coming_buyed"))
{
	$del_subscription = $del_media_obj ->delete_subscription_media($_GET['id'],$_SESSION['login_email']);
}

/*if(isset($_GET['id']) && $_GET['whos_media']=="media_creator_from_tag")
{
	$del_creator_tagged = $del_media_obj -> remove_creator_tag($_GET['id'],$_SESSION['login_email']);
}*/

if(isset($_GET['id']) && isset($_GET['type']))
{
	$del_tagged = $del_media_obj -> delete_tagged($_GET['id'],$_GET['type']);
}
/*$url = "";
if(isset($_SERVER['HTTP_REFERER'])){
	$url = $_SERVER['HTTP_REFERER'];
}*/
header("Location: profileedit_media.php");
?>