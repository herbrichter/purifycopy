<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Purify Entertainment: About</title>
<link rel="stylesheet" type="text/css" media="screen" href="includes/purify.css" />
<script src='http://ajax.googleapis.com/ajax/libs/jquery/1.6.1/jquery.min.js'></script>
<script src="includes/organictabs-jquery.js"></script>
<script>
	$(function() {

		$("#personalTab").organicTabs();
	});

function changeForm()
{

	 document.membership_type.submit();
	 
  	}

</script>
<?php
	ob_start();
	include_once('loggedin_includes.php');
	include_once('classes/ProfileType.php');
	include('recaptchalib.php');
	$publickey = "6Lduu-sSAAAAAHbUkh7_9mmfg3zQucvRxtr2gvbA";
	$privatekey = "6Lduu-sSAAAAALD2YftpY41sm3a1CtIpHeymu3TH";
	
?>





</head>
<body>
<div id="outerContainer">
  <div id="purifyMasthead">
    <div id="purifyLogo"><a href="../../index.php"><img src="images/4.gif" alt="Purify Entertainment" width="302" height="53" border="0" /></a></div>
    <div id="miscNav">
      <div id="loggedin">
        <h2>Logged in as  <?php echo $_SESSION['login_email'];?><!--<a href="#">Username</a>--></h2>
        <p><a href="profiles/profile.html">View Profile</a><br />
        <a href="../Design/demoindex.php">Logout</a></p>
      </div>
    </div>
  </div>
  <div id="toplevelNav"></div>
  <div>
      <div id="tabs">
          <ul>
            <li><a href="profileedit.php">PERSONAL</a></li>		
            <li><a href="profileedit_media.php">MEDIA</a></li>
            <li><a href="profileedit_artist.php">ARTIST</a></li>
            <li><a href="profileedit_community.php">COMMUNITY</a></li>
            <li><a href="profileedit_team.php">TEAM</a></li>
            <li><a href="profileedit_promotions.php">Promotions</a></li>
          </ul>
      </div>
      
          <div id="personalTab">
            <div id="subNavigation" class="tabs">
              <ul class="nav">
                <li><a href="#event" class="current">Events</a></li>
                <li><a href="#friends">Friends</a></li>
                <li><a href="#general">General Info </a></li>
                <li><a href="">Location</a></li>
                <li><a href="">Mail</a></li>        
                <li><a href="">Points</a></li>
                <li><a href="">Privacy</a></li>
                <li><a href="">Registration</a></li>
                <li><a href="">Services</a></li>
                <li><a href="">Statistics</a></li>
                <li><a href="">Subscriptions</a></li>
              </ul>
            </div>
            <div class="list-wrap">
				    <?php 
		
		$profileObj = new ProfileType();
		$profileRes = $profileObj->displayData();
		while($profileList[] = mysql_fetch_array($profileRes));
		
		
		//$temp=$_POST['member_type'];
		//echo $temp;
	?>
	<div class="select_membership">
	
	<form name="membership_type" action="profileedit_personal_registration.php" id="membership_type" method="post">
	
		Participate in Purify Entertainment as a :
		<select name="member_type" id="member_type" onchange="changeForm()">
			<option value="0" >Select One</option>
			<?php foreach($profileList as $profile){ 
					if($profile['type_flag']==1 && $profile['profile_page_name']!=""){
			?>
			<option value="<?php echo $profile['id']; ?>" <?php if($_POST['member_type'] == $profile['id']) echo "selected='true'"; ?>><?php echo $profile['name']; ?></option>
			<?php 	}
				  } ?>
		</select>
	</form>
	</div>
	<br /><div id="membership_form">
<?php
$register_page = "";
foreach($profileList as $profile){ 
	if($profile['id']==$_POST['member_type'] && $profile['type_flag']==1 && $profile['profile_page_name']!=""){
		$register_page = $profile['profile_page_name'];
		break;
	}
}					

	if(isset($_POST['member_type']) && $register_page!="")
	{
		include($register_page);
	}
?>	
<?php
	if($_GET['f']==1)
	{
		if($_GET['e'])
		{
			$error_flag = $_GET['e'];
			include("register_artist.php");
			
		}
	}
	if($_GET['f']==2)
	{
		if($_GET['e'])
		{
			$error_flag = $_GET['e'];
			include("register_fan.php");
		}
	}
	if($_GET['f']==3)
	{
		if($_GET['e'])
		{
			$error_flag = $_GET['e'];
			include("register_community.php");
		}
	}
	if($_GET['f']==4)
	{
		if($_GET['e'])
		{
			$error_flag = $_GET['e'];
			include("register_team.php");
		}
	}
?>  
            </div>
          </div>  
                  
 <p></p>
   </div>
</div>
  <div id="footer"> <strong>&copy; 2010 Purify Entertainment</strong> This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/3.0/">Creative Commons Attribution-Share Alike 3.0 Unported License</a>.<br />
    <a href="privacypolicy.html">Privacy Policy</a> / <a href="useragreement.html">User Agreement</a> / <a href="originalityagreement.html">Originality Agreement</a> / <a href="busplan.html">Business Plan</a> </div>
  <!-- end of main container -->
</div>
</div>
</body>
</html>