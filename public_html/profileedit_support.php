<?php
	session_start();
	ob_start();
	include_once('commons/db.php');
	include_once('loggedin_includes.php');
	include_once('classes/ProfileType.php');
	include_once('classes/Commontabs.php');
	include_once('classes/AddContact.php');
	include_once('classes/GeneralInfo.php');
	include_once('classes/CountryState.php');
	//include('recaptchalib.php');
	include_once('classes/ProfileEvent.php');
	include_once('classes/Mails.php');
	include_once('classes/CreatePurifyMember.php');
	include_once('classes/SubscriptionListings.php');
	include_once('classes/PersonalWallDisplayFeeds.php');
	include_once('sendgrid/SendGrid_loader.php');
	$sendgrid = new SendGrid('','');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "https://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="https://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<meta https-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Purify Art: Support Profile</title>
<!--<script type="text/javascript" src="includes/jquery.js"></script>-->
<?php
	$newtab=new Commontabs();
include_once("header.php");
?>
<div id="outerContainer">
<?php 
$sub_listings = new SubscriptionListings();	
	$mails_object = new Mails();
	if(isset($_GET['mail_delete']))
	{
		$count_inbox_del = 0;
		$delete = $mails_object->delete_mail($_GET['mail_delete'],$count_inbox_del);
		header("Location: profileedit.php#mail");
	}
	if(isset($_GET['delete_sub_stat_art']))
	{
		$delete = $sub_listings->deleteSubscription($_GET['delete_sub_stat_art']);
		header("Location: profileedit.php#subscrriptions");
	}

?>
				<!--<p><a href="../logout.php">Logout</a> </p>
			</div>
		</div>
	</div> 
</div>-->

<!--<link rel="stylesheet" type="text/css" media="screen" href="includes/purify.css" />
<script src='https://ajax.googleapis.com/ajax/libs/jquery/1.6.1/jquery.min.js'></script>
<script src="includes/organictabs-jquery.js"></script>
<script>
	$(function() {

		$("#supportTab").organicTabs();
	});
</script>-->
<script type="text/javascript">
	//<![CDATA[

$(function()
{
	var config = {
		toolbar:
		[
			['Bold', 'Italic', '-', 'NumberedList', 'BulletedList', '-', 'Link', 'Unlink'],
			['UIColor']
		]
	};

	// Initialize the editor.
	// Callback function can be passed and executed after full instance creation.
	$('.jquery_ckeditor').ckeditor(config);
});

	//]]>
function validate_form()
{
	var drop_down = document.getElementById("select_mail_category");
	if(drop_down.value=="" || drop_down.value==0) {
		alert("Please Select The Type");
		return false;
	}
	var editor = document.getElementById("mail_body");
	if(editor.value==""){
		alert("Please Enter Some Text In The Editor.");
		return false;
	}
	var url = document.getElementById("URL");
	if(url.value==""){
		alert("Please Enter URL.");
		return false;
	}
}
	</script>	
<div id="toplevelNav"></div>
  <div>
      <div id="profileTabs">
          <ul>
           <?php
		   $check=new AddContact();
		   
		   $new_artist=$newtab->artist_status();
		   $new_community=$newtab->community_status();
		   $new_team=$newtab->team_status();
		   
		   $sql_fan_chk = mysql_query("SELECT * FROM fan_club_membership WHERE email='".$_SESSION['login_email']."' AND accept=1");
		   if(mysql_num_rows($sql_fan_chk)>0)
		   {
				while($row_fan_chk = mysql_fetch_assoc($sql_fan_chk))
				{
					$fan_var = $fan_var + 1;
				}
		   }
		   
			$gen_det = mysql_query("SELECT * FROM general_user WHERE email='".$_SESSION['login_email']."'");
		   if(mysql_num_rows($gen_det)>0){
			$res_gen_det = mysql_fetch_assoc($gen_det);
		   }
		   $date = date("Y-m-d");
			$sql_member_chk = mysql_query("SELECT * FROM purify_membership WHERE general_user_id='".$res_gen_det['general_user_id']."' AND (expiry_date>'".$date."' OR lifetime=1)");
		   if(mysql_num_rows($sql_member_chk)>0)
		   {
				$member_var = $member_var + 1;
		   }
		  if(($newres1['artist_id']==0 && $new_artist['status']==1) && $newres1['member_id']==0 && ($newres1['community_id']==0 && $new_community['status']==1) && ($newres1['team_id']==0 && $new_team['status']==1))
		  {		  
		  ?>
            <li ><a href="profileedit.php"> HOME</a></li>
            <li class="active">SUPPORT</li>
		   <?php
		   }
		   elseif(($newres1['artist_id']==0 && ($new_artist['status']!=0 ||  $new_artist['status']=="") && $new_artist['active']==0) && ($newres1['community_id']==0 && ($new_community['status']!=0 || $new_community['status']=="")) && ($newres1['team_id']==0 && ($new_team['status']!=0 || $new_team['status']=="")) && $fan_var!=0)
		   {
		   ?>
			<li><a href="profileedit.php">HOME</a></li>
			<li><a href="profileedit_media.php">Media</a></li>
            <li class="active">SUPPORT</li>
		   <?php
		   }
		    elseif(($newres1['artist_id']==0 && ($new_artist['status']!=0 ||  $new_artist['status']=="")) && ($newres1['community_id']==0 && ($new_community['status']!=0 || $new_community['status']=="")) && ($newres1['team_id']==0 && ($new_team['status']!=0 || $new_team['status']=="")) && $member_var!=0)
		   {
		   ?>
			<li><a href="profileedit.php">HOME</a></li>
			<li><a href="profileedit_media.php">Media</a></li>
            <li class="active">SUPPORT</li>
		   <?php
		   }
		    elseif(($newres1['artist_id']!=0 && $new_artist['status']==0 && $new_artist['active']==0) && ($newres1['community_id']!=0 && $new_community['status']==0 && $new_community['active']==0) && ($newres1['team_id']!=0 && $new_team['status']==0))
		   {
		   ?>
		   
		   <li ><a href="profileedit.php"> HOME</a></li>
		   <li><a href="profileedit_media.php">Media</a></li>
		   <li><a href="profileedit_artist.php">ARTIST</a></li>
		   <!--<li><a href="profileedit_community.php">COMPANY</a></li>-->
		   <li><a href="profileedit_community.php">COMPANY</a></li>
		   <li><a href="profileedit_team.php">TEAM</a></li>
			<li class="active">SUPPORT</li>
		   
		   <?php
		   }
		  elseif(($newres1['artist_id']!=0 && $new_artist['status']==0 && $new_artist['active']==0) && ($newres1['community_id']!=0 && $new_community['status']==0 && $new_community['active']==0))
		   {
		   ?>
		   <li ><a href="profileedit.php"> HOME</a></li>
		   <li><a href="profileedit_media.php">Media</a></li>		   
		   <li><a href="profileedit_artist.php">ARTIST</a></li>
		   <!--<li><a href="profileedit_community.php">COMPANY</a></li>-->
		   <li><a href="profileedit_community.php">COMPANY</a></li>
           <li class="active">SUPPORT</li>
		   <?php
		   }
		    elseif(($newres1['artist_id']!=0 && $new_artist['status']==0 && $new_artist['active']==0) && ($newres1['team_id']!=0 && $new_team['status']==0))
		   {
		   ?>
			<li ><a href="profileedit.php"> HOME</a></li>
			<li><a href="profileedit_media.php">Media</a></li>
		    <li><a href="profileedit_artist.php">ARTIST</a></li>
			<li><a href="profileedit_team.php">TEAM</a></li>
            <li class="active">SUPPORT</li>
		   <?php
		   }
		   elseif(($newres1['community_id']!=0 && $new_community['status']==0 && $new_community['active']==0) && ($newres1['team_id']!=0 && $new_team['status']==0))
		   {
		   ?>
		   <li ><a href="profileedit.php"> HOME</a></li>
		   <li><a href="profileedit_media.php">Media</a></li>
		   <!--<li><a href="profileedit_community.php">COMPANY</a></li>-->
		   <li><a href="profileedit_community.php">COMPANY</a></li>
		   <li><a href="profileedit_team.php">TEAM</a></li>
           <li class="active">SUPPORT</li>
		   <?php
		   }
		   elseif($newres1['artist_id']!=0 && $new_artist['status']==0 && $new_artist['active']==0)
		   {?>
		    <li ><a href="profileedit.php"> HOME</a></li>
			<li><a href="profileedit_media.php">Media</a></li>			
			<li><a href="profileedit_artist.php">ARTIST</a></li>
            <li class="active">SUPPORT</li>
		   <?php
		   }
		   elseif($newres1['member_id']!=0)
		   {
		   ?>
		   <?php
		   }
		   elseif($newres1['community_id']!=0 && $new_community['status']==0 && $new_community['active']==0)
		   {
		   ?>
		    <li ><a href="profileedit.php"> HOME</a></li>
			<li><a href="profileedit_media.php">Media</a></li>
			<!--<li>COMPANY</a></li>-->
			<li><a href="profileedit_community.php">COMPANY</a></li>
            <li class="active">SUPPORT</li>
		   <?php
		   }
		   elseif($newres1['team_id']!=0 && $new_team['status']==0)
		   {
		   ?>
		    <li ><a href="profileedit.php"> HOME</a></li>
			<li><a href="profileedit_media.php">Media</a></li>
			<li><a href="profileedit_team.php">TEAM</a></li>
            <li class="active">SUPPORT</li>
`		   <?php
		   }
		   else
		   {
		   ?>
			 <li ><a href="profileedit.php"> HOME</a></li>
            <li class="active">SUPPORT</li>
		   <?php
		   }
			?>
          </ul>
      </div>
      
          <div id="supportTab">
            <div id="subNavigation" class="tabs">
              <ul class="nav">
                <li><a href="help_faq.php" target="_blank">FAQ</a></li>
                <!--<li><a href="support_guide_to_site.php" target="_blank">Guide to Site</a></li>-->
				<li><a href="support_how_to.php" target="_blank">How To</a></li>
				<li style="display:none;"><a href="#support"  class="current">Support</a></li>
              </ul>
            </div>
            <div class="list-wrap">
			<div class="subTabs" id="support">
					<h1>Request Support</h1>
					<div style="width:665px; float:left;">
						<div id="actualContent">
						<!--<form action="" method="POST" Onsubmit ="return validate_form();">-->
						<form action="" method="POST" >
							<div class="fieldCont">
								<div class="fieldTitle">Type</div>
								 <select name="select_mail_category" class="dropdown" id="select_mail_category">
									<option value="0" selected="selected">Select Type</option>					
									<option value="Emails" >Emails</option>
									<option value="Media" >Media</option>					
									<option value="Memberships" >Memberships</option>					
									<option value="Profiles" >Profiles</option>
									<option value="Registration" >Registration</option>
									<option value="Search" >Search</option>
									<option value="Store" >Store</option>
									<option value="Suggestion" >Suggestion</option>
								</select>
							</div>
							<div class="fieldCont">
								<div class="fieldTitle">Description</div>
								<textarea  class="jquery_ckeditor" name="mail_body" id="mail_body"></textarea>
							</div>
							<div class="fieldCont">
								<div class="fieldTitle">URL</div>
								<input name="URL" type="text" class="fieldText" id="URL" value="">
							</div>
							<div class="fieldCont">
                                <div class="fieldTitle"></div>
                                <input type="Submit" class="register" value="Send" />                             
                            </div>
						</form>
						</div>
					</div>
					
				</div>
            	<div class="subTabs" id="faq" style="display:none;">
                	<h1>FAQ</h1>
                    <div id="actualContent"></div>
                </div>
				<div class="subTabs" id="howto" style="display:none;">
                	<h1>How To</h1>
                    <div id="actualContent"></div>
                </div>
                <div class="subTabs" id="guide" style="display:none;">
                	<h1>Guide to Site</h1>
                    <div id="actualContent"></div>
                </div>
                
                
            </div>
          </div>  
          
           
          
            
      
   </div>
</div>
 
</div><?php include_once("displayfooter.php"); ?>
</body>
</html>
<?php
if(isset($_POST) && !empty($_POST))
{
?>
<script>
	alert("Your support request has been sent, you will receive a reply via email shortly. Thanks");
</script>
<?php
/*	if($_POST['select_mail_category'] == 1){
		$mailto = "mithesh.kavrani@iniksha.com";}
	if($_POST['select_mail_category'] == 2){
		$mailto = "azhar.khan@iniksha.com";}
	if($_POST['select_mail_category'] == 3){
		$mailto = "mayur@remotedataexchange.com";}
	if($_POST['select_mail_category'] == 4){
		$mailto = "mayur.nasikkar@gmail.com";}*/
	//$strTo = "azhar@remotedataexchange.com";
	//$strTo = "mithesh@remotedataexchange.com";
	$strTo = "purifyarthelpdesk@gmail.com";
	//$strTo = $mailto;
	//$strTo = "azhar@remotedataexchange.com"; 
	$strSubject = "Support";
	//$strMessage = nl2br($_POST['mail_body']);

	//*** Uniqid Session ***//
	
	
	$strHeader = "";
	$strHeader .= "MIME-Version: 1.0\r\n";
	$strHeader .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
	$strHeader .= "from: Purify Art < no-reply@purifyart.com>"."\r\n";
	
	$strMessage = "~Category='".$_POST['select_mail_category']."'</br>".$_POST['mail_body']."</br>".$_POST['URL'];
	
	$url = "admin/newsletter/"; 
			$myFile = $url . "other_templates.txt";
			$fh = fopen($myFile, 'r');
			$theData = fread($fh, filesize($myFile));
			fclose($fh);
			
			$data_email = explode('|~|', $theData);
			
			$ns_user_n = str_replace('{::message::}', $strMessage, $data_email['1']);
			
			$body_email = $data_email['0'] . $ns_user_n . $data_email['2'];
			
			$mail_grid = new SendGrid\Mail();
			
			$mail_grid->addTo($strTo)->
					setFromName('Purify Art')->
					setFrom('no-reply@purifyart.com')->
					setSubject($strSubject)->
					setHtml($body_email);
					
			//$sendgrid->web->send($mail_grid);
			$sendgrid -> smtp -> send($mail_grid);
	
	//mail($strTo,$strSubject,$strMessage,$strHeader);
}
?>
