<?php
include_once('commons/db.php');

class UserType
{
  function addEntry($user_id,$type_id,$subtype_id)
  {
	$query="insert into user_type (`user_id`,`type_id`,`subtype_id`) values ('$user_id','$type_id','$subtype_id')";
	$rs=mysql_query($query) or die(mysql_error());
	$id=mysql_insert_id();
	return($id);
  }
  
  function getUserList($user_id)
  {
	$query="select * from user_type where user_id='$user_id'";
	$rs=mysql_query($query) or die(mysql_error());
	return($rs);  	
  }
  function removeUserType($type_id)
  {
  	$sql="DELETE FROM user_type WHERE type_id='$type_id'";
	$rs=mysql_query($sql) or die(mysql_error());
	return $rs;  	
  }   
  function removeUserSubType($type_id,$subtype_id)
  {
  	$sql="DELETE FROM user_type WHERE type_id='$type_id' and subtype_id='$subtype_id'";
	$rs=mysql_query($sql) or die(mysql_error());
	return $rs;  	
  }    
  function countUsersByCriteria($search_key,$criteria,$criteria2)
  {
  	if($search_key==NULL)
	{
		$sql="select * from user,user_type,user_parent where user.id=user_type.user_id and user.id=user_parent.user_id and user.del<>'1' and user_type.$criteria<>''";
		$rs=mysql_query($sql) or die(mysql_error());
		return mysql_num_rows($rs);
	}
	else
	{
		$sql="select * from user_type,user,user_parent,$criteria2 where user_type.user_id=user.id and user.id=user_parent.user_id and user_type.$criteria=$criteria2.$criteria and user.del<>'1' and $criteria2.name='$search_key'";
		$rs=mysql_query($sql) or die(mysql_error());
		return mysql_num_rows($rs);	
	}
  }
  function countUsersByCriteriaByType($search_key,$criteria,$criteria2,$utype)
  {
  	if($search_key==NULL)
	{
		$sql="select * from user,user_type,user_parent where user_parent.user_type='$utype' and user.id=user_type.user_id and user.id=user_parent.user_id and user.del<>'1' and user_type.$criteria<>''";
		$rs=mysql_query($sql) or die(mysql_error());
		return mysql_num_rows($rs);
	}
	else
	{
		$sql="select * from user_type,user,user_parent,$criteria2 where user_parent.user_type='$utype' and user_type.user_id=user.id and user.id=user_parent.user_id and user_type.$criteria=$criteria2.$criteria and user.del<>'1' and $criteria2.name='$search_key'";
		$rs=mysql_query($sql) or die(mysql_error());
		return mysql_num_rows($rs);	
	}
  }     
  //Search/sort functions
  function sortByCriteria($search_key,$max,$field,$field2,$orderby,$dir)
  {
	if($search_key==NULL)
	{
		$sql="select * from user,user_type,user_parent where user.id=user_type.user_id and user.id=user_parent.user_id and user.del<>'1' and user_type.$field<>'' order by user.$orderby $dir".$max;
		$rs=mysql_query($sql) or die(mysql_error());
		return($rs);
	}
	else
	{
		$sql="select * from user_type,user,user_parent,$field2 where user_type.user_id=user.id and user.id=user_parent.user_id and user_type.$field=$field2.$field and user.del<>'1' and $field2.name='$search_key' order by user.$orderby $dir".$max;
		$rs=mysql_query($sql) or die(mysql_error());
		return($rs);
	}
  }
  
  function sortByCriteriaByType($search_key,$max,$field,$field2,$orderby,$dir,$utype)
  {
	if($search_key==NULL)
	{
		$sql="select * from user,user_type,user_parent where user_parent.user_type='$utype' and user.id=user_type.user_id and user.id=user_parent.user_id and user.del<>'1' and user_type.$field<>'' order by user.$orderby $dir".$max;
		$rs=mysql_query($sql) or die(mysql_error());
		return($rs);
	}
	else
	{
		$sql="select * from user_type,user,user_parent,$field2 where user_parent.user_type='$utype' and user_type.user_id=user.id and user.id=user_parent.user_id and user_type.$field=$field2.$field and user.del<>'1' and $field2.name='$search_key' order by user.$orderby $dir".$max;
		$rs=mysql_query($sql) or die(mysql_error());
		return($rs);
	}
  }
  
  function abandonUserType($user_id)
  {
	$sql="delete from user_type where user_id='$user_id'";
	$rs=mysql_query($sql) or die(mysql_error()); 
  }
  
  function getTypeByUserId($user_id) 
  {
	$query="select * from user_type where user_id='$user_id'";
	$rs=mysql_query($query) or die(mysql_error());
	$row=mysql_fetch_assoc($rs);
	return $row['type_id'];
  }   
  
  function getSubTypeByType($user_id,$type_id) 
  {
	$query="select * from user_type where type_id='$type_id' AND user_id='$user_id'";
	$rs=mysql_query($query) or die(mysql_error());
	$row=mysql_fetch_assoc($rs);
	return $row['subtype_id'];
  } 
  
  function getUserTypesByUserId($user_id) 
  {
	$query="select * from user_type where user_id='$user_id'";
	$rs=mysql_query($query) or die(mysql_error());
	return $rs;
  }   
}
?>