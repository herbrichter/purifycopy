<?php

include_once('commons/db.php');

class EditArtist
{
	function sel_general()
	{
		$sql="select * from general_user where email='".$_SESSION['login_email']."' AND status=0 AND delete_status=0 AND active=0";
		$row=mysql_query($sql);
		$id=mysql_fetch_assoc($row);
		return($id);
	}
	
	function sel_general_aid($ids)
	{
		$sql="select * from general_user where artist_id='".$ids."' AND delete_status=0 AND active=0 AND status=0";
		$row=mysql_query($sql);
		$id=mysql_fetch_assoc($row);
		return($id);
	}
	
	function sel_general_cid($ids)
	{
		$sql="select * from general_user where community_id='".$ids."' AND delete_status=0 AND active=0 AND status=0";
		$row=mysql_query($sql);
		$id=mysql_fetch_assoc($row);
		return($id);
	}
	
	function Artists()
	{
		$new_id=$this->sel_general();
		$sql1=mysql_query("select * from general_artist where artist_id='".$new_id['artist_id']."'");
		$newrow=mysql_fetch_assoc($sql1);
		return($newrow);
	}
	
	function ArtistProfile()
	{
		$new_id=$this->sel_general();
		$sql2=mysql_query("select * from general_artist_profiles where artist_id='".$new_id['artist_id']."'");
		$newrow2=mysql_fetch_assoc($sql2);
		//$type=$newrow2['type_id'];
		//$subtype=$newrow2['subtype_id'];
		return($newrow2);
	}
	
	function Type()
	{
		$new_typeid=$this->ArtistProfile();
		$sql3=mysql_query("select * from type where type_id='".$new_typeid['type_id']."'");
		$newrow3=mysql_fetch_assoc($sql3);
		return($newrow3);
	}
	
	 
	function MetaType()
	{
		$new_id=$this->sel_general();
		
		$getmeta=mysql_query("select * from general_artist_profiles where artist_id='".$new_id['artist_id']."' ORDER BY general_artist_profiles_id ASC");
		//$metares=mysql_fetch_assoc($getmeta);
		return ($getmeta);
	}
	
	function AccProfileType()
	{
		$newrow3=$this->Type();
		$sql5=mysql_query("select * from type where profile_id='".$newrow3['profile_id']."'");
		return ($sql5);
	}
	
	function AccProfileSubType()
	{
		$newrow3=$this->Type();
		$sql6=mysql_query("select * from subtype where type_id='".$newrow3['type_id']."'");
		return ($sql6);
	}
	
	function AccProfileMetaType()
	{
		$newrow4=$this->SubType();
		$sql7=mysql_query("select * from meta_type where subtype_id='".$newrow4['subtype_id']."'");
		return ($sql7);
	}
	
	function getEventUpcoming()
	{
		$date=date("Y-m-d");
		$new_id=$this->sel_general();
		$getproject=mysql_query("select * from artist_event where artist_id='".$new_id['artist_id']."' AND date>='".$date."' AND del_status=0 AND active=0");
		return ($getproject);
	}
	
	function getEventRecorded()
	{
		$date=date("Y-m-d");
		$new_id=$this->sel_general();
		$getproject=mysql_query("select * from artist_event where artist_id='".$new_id['artist_id']."' AND date<'".$date."' AND del_status=0 AND active=0");
		return ($getproject);
	}
	
	function getComEventUpcoming()
	{
		$date=date("Y-m-d");
		$new_id=$this->sel_general();
		$getproject=mysql_query("select * from community_event where community_id='".$new_id['community_id']."' AND date>='".$date."' AND del_status=0 AND active=0");
		return ($getproject);
	}
	
	function getComEventRecorded()
	{
		$date=date("Y-m-d");
		$new_id=$this->sel_general();
		$getproject=mysql_query("select * from community_event where community_id='".$new_id['community_id']."' AND date<'".$date."' AND del_status=0 AND active=0");
		return ($getproject);
	}
	
	function GetProject()
	{
		$new_id=$this->sel_general();
		$getproject=mysql_query("select * from artist_project where artist_id='".$new_id['artist_id']."' AND del_status=0 AND active=0");
		return ($getproject);
	}
	
	function Get_Project_type($id)
	{
		$getproject_type = mysql_query("SELECT * , MIN( id )
											FROM `artist_project_profiles` app
											LEFT JOIN type ty ON app.type_id = ty.type_id
											WHERE app.artist_project_id ='".$id."'");
		$res_type= mysql_fetch_assoc($getproject_type);
		return ($res_type);
	}
	
	function Get_Project_subtype($id)
	{
		$getproject_type = mysql_query("SELECT * , MIN( id )
											FROM `artist_project_profiles` app
											LEFT JOIN subtype ty ON app.subtype_id = ty.subtype_id
											WHERE app.artist_project_id ='".$id."'");
		$res_type= mysql_fetch_assoc($getproject_type);
		return ($res_type);
	}
	
	function Get_ACProject_subtype($id)
	{
		$getproject_type = mysql_query("SELECT * , MIN( id )
											FROM `community_project_profiles` app
											LEFT JOIN subtype ty ON app.subtype_id = ty.subtype_id
											WHERE app.community_project_id ='".$id."'");
		$res_type= mysql_fetch_assoc($getproject_type);
		return ($res_type);
	}
	
	function Get_Event_type($id)
	{
		$getproject_type = mysql_query("SELECT * , MIN( id )
											FROM `artist_event_profiles` app
											LEFT JOIN type ty ON app.type_id = ty.type_id
											WHERE app.artist_event_id ='".$id."'");
		$res_type= mysql_fetch_assoc($getproject_type);
		return ($res_type);
	}
	
	function Get_ACEvent_type($id)
	{
		$getproject_type = mysql_query("SELECT * , MIN( id )
											FROM `community_event_profiles` app
											LEFT JOIN type ty ON app.type_id = ty.type_id
											WHERE app.community_event_id ='".$id."'");
		$res_type= mysql_fetch_assoc($getproject_type);
		return ($res_type);
	}
	
	function Get_Event_subtype($id)
	{
		$getproject_type = mysql_query("SELECT * , MIN( id )
											FROM `artist_event_profiles` app
											LEFT JOIN subtype ty ON app.subtype_id = ty.subtype_id
											WHERE app.artist_event_id ='".$id."'");
		$res_type= mysql_fetch_assoc($getproject_type);
		return ($res_type);
	}
	
	function Get_ACEvent_subtype($id)
	{
		$getproject_type = mysql_query("SELECT * , MIN( id )
											FROM `community_event_profiles` app
											LEFT JOIN subtype ty ON app.subtype_id = ty.subtype_id
											WHERE app.community_event_id ='".$id."'");
		$res_type= mysql_fetch_assoc($getproject_type);
		return ($res_type);
	}
	
	function getAllProjects()
	{
		$new_id=$this->sel_general();
		$getproject=mysql_query("select * from artist_project where artist_id='".$new_id['artist_id']."' AND del_status=0 AND active=0");
		return ($getproject);
	}
	
	function getAllC_Projects()
	{
		$new_id=$this->sel_general();
		if($new_id['community_id']!=0)
		{
			$getproject=mysql_query("select * from community_project where community_id='".$new_id['community_id']."' AND del_status=0 AND active=0");
		}
		return ($getproject);
	}
	
	function GetEvent()
	{
		$new_id=$this->sel_general();
		$getevent=mysql_query("select * from artist_event where artist_id='".$new_id['artist_id']."' AND del_status=0 AND active=0");
		return ($getevent);
	}
	
	function DisplayType($displaytype)
	{
		$qu = mysql_query("select name from type where type_id='".$displaytype."'");
		return ($qu);
	}
	
	function DisplaySubType($displaysub)
	{
		$qu = mysql_query("select name from subtype where subtype_id='".$displaysub."'");
		return ($qu);
	}
	
	function DisplayMetaType($displaymeta)
	{
		$qu = mysql_query("select name from meta_type where meta_id='".$displaymeta."'");
		return ($qu);
	}
	
	function getBucket($login_email)
	{
		$sql="select bucket_name from general_user where email='".$login_email."'";
		return ($sql);
	}
	
	function insertArtist($name,$country,$state,$city,$homepage,$bucket_name)
	{
		$gen_info =$this->sel_general();
		$art_info ="";
		$com_info ="";
		if($gen_info['community_id']!="" || $gen_info['community_id']!=0)
		{
			$get_det = mysql_query("select * from general_community where community_id='".$gen_info['community_id']."'");
			$res_det = mysql_fetch_assoc($get_det);
			$art_info = $res_det['artist_view_selected'];
			$com_info = $res_det['community_view_selected'];
		}
		$sql="insert into general_artist (name,country_id,state_id,city,homepage,bucket_name,date_time,artist_view_selected,community_view_selected) values('".$name."','".$country."','".$state."','".$city."','".$homepage."','".$bucket_name."',CURDATE(),'".$art_info."','".$com_info."')";
		$query=mysql_query($sql);
		$id=mysql_insert_id();
		return($id);
	}
	
	function updateArtist($id,$login_email)
	{
		$sql="UPDATE general_user SET artist_id='".$id."' WHERE email='".$login_email."'";
		return ($sql);
	}
	
	function insertArtistProfile($id,$r)
	{
		$query="insert into `general_artist_profiles` (`artist_id`, `type_id`, `subtype_id`, `metatype_id`) values ('$id','$r[0]','$r[1]','$r[2]')";
		return ($query);
	}
	
	function insertArtistProfile1($id,$q)
	{
		$query="insert into `general_artist_profiles` (`artist_id`, `type_id`, `subtype_id`, `metatype_id`) values ('$id','$q[0]','$q[1]','0')";
		return ($query);
	}
	
	function insertArtistProfile2($id,$p)
	{
		$query="insert into `general_artist_profiles` (`artist_id`, `type_id`, `subtype_id`, `metatype_id`) values ('$id','$p[0]','0','0')";
		return ($query);
	}
	
	function updateImage($fileName,$id)
	{
		$sql="update general_artist set image_name='$fileName' where artist_id=$id";
		return ($sql);
	}
	
	function updateArtistProfile($name,$subdomain,$website,$artist_id,$tag_video,$tag_song,$tag_channel,$tag_projects,$tag_events_up,$tag_events_rec,$tag_gallery,$bio,$featured_media,$featured_media_gallery,$price,$type,$fan_club_song,$fan_club_gallery,$fan_club_video,$fan_club_channel,$sale_song,$sale_gallery,$me_table,$country,$state,$city,$fb_share,$twit_share,$gplus_share,$tubm_share,$stbu_share,$pin_share,$you_share,$vimeo_share,$sdcl_share,$ints_share)
	{
		$sql="UPDATE general_artist SET name='".$name."',profile_url='".$subdomain."',homepage='".$website."',bio='".$bio."',taggedprojects='',taggedgalleries='".$tag_gallery."',taggedsongs='".$tag_song."',taggedvideos='".$tag_video."',taggedchannels='".$tag_channel."',taggedprojects='".$tag_projects."',taggedupcomingevents='".$tag_events_up."',taggedrecordedevents='".$tag_events_rec."',featured_media='".$featured_media."',media_id='".$featured_media_gallery."'
		,price='".$price."', type='".$type."',fan_song='".$fan_club_song."',fan_gallery='".$fan_club_gallery."',fan_channel='".$fan_club_channel."',fan_video='".$fan_club_video."', sale_song='".$sale_song."',sale_gallery='".$sale_gallery."', featured_media_table='".$me_table."', country_id='".$country."', state_id='".$state."', city='".$city."', fb_share='".$fb_share."', twit_share='".$twit_share."', gplus_share='".$gplus_share."', tubm_share='".$tubm_share."', stbu_share='".$stbu_share."', pin_share='".$pin_share."', you_share='".$you_share."', vimeo_share='".$vimeo_share."', sdcl_share='".$sdcl_share."', ints_share='".$ints_share."'
		WHERE artist_id='".$artist_id."'";
		return ($sql);
	}
	
	function updateGeneralArtistProfile($artist_id,$r)
	{
		$query="insert into `general_artist_profiles` (`artist_id`, `type_id`, `subtype_id`, `metatype_id`) values ('".$artist_id."','$r[0]','$r[1]','$r[2]')";
		return ($query);
	}
	
	function updateGeneralArtistProfile1($artist_id,$q)
	{
		$query="insert into `general_artist_profiles` (`artist_id`, `type_id`, `subtype_id`, `metatype_id`) values ('".$artist_id."','$q[0]','$q[1]','0')";
	//	$res=mysql_query($query);
		return ($query);
	}
	
	function updateGeneralArtistProfile2($artist_id,$p)
	{
		$query="insert into `general_artist_profiles` (`artist_id`, `type_id`, `subtype_id`, `metatype_id`) values ('".$artist_id."','$p[0]','0','0')";
		return ($query);
	}
	function Get_artist_primarytype()
	{
		$new_id=$this->sel_general();
		
		$sql=mysql_query("SELECT MIN( `general_artist_profiles_id` ) FROM `general_artist_profiles` WHERE artist_id =$new_id[artist_id]");
		$res=mysql_fetch_assoc($sql);
		//echo $res['MIN( `general_artist_profiles_id` )'];
		
		$sql2=mysql_query("select * from general_artist_profiles where general_artist_profiles_id='".$res['MIN( `general_artist_profiles_id` )']."'");
		$res2=mysql_fetch_assoc($sql2);
		return($res2);
	}
	function Get_disp_MetaType()
	{
		$sub_id=$this->Get_artist_primarytype();
		$getmeta=mysql_query("select * from meta_type where subtype_id=$sub_id[subtype_id]");
		//$metares=mysql_fetch_assoc($getmeta);
		return ($getmeta);
	}
	function SubType()
	{
		$new_typeid=$this->Get_artist_primarytype();
		$sql4=mysql_query("select * from subtype where type_id='".$new_typeid['type_id']."'");
		//$newrow4=mysql_fetch_assoc($sql4);
		return($sql4);
	}
	
	/*********Functions For Tagged Channel Video Song Gallery*******/
	
	function get_artist_channel_id()
	{
		$new_id=$this->sel_general();
		$sql5=mysql_query("select * from general_media where general_user_id='".$new_id['general_user_id']."' and media_type=116  AND delete_status=0");
		return($sql5);
	}
	function get_artist_channel($id)
	{
		$sql6=mysql_query("select * from media_channel where media_id='".$id."' ");
		return($sql6);
	}
	function get_artist_video_id()
	{
		$new_id=$this->sel_general();
		$sql7=mysql_query("select * from general_media where general_user_id='".$new_id['general_user_id']."' and media_type=115 AND delete_status=0");
		return($sql7);
	}
	function get_artist_video($id)
	{
		$sql8=mysql_query("select * from media_video where media_id='".$id."'");
		return($sql8);
	}
	function get_artist_song_id()
	{
		$new_id=$this->sel_general();
		$sql9=mysql_query("select * from general_media where general_user_id='".$new_id['general_user_id']."' and media_type=114 AND delete_status=0");
		return($sql9);
	}
	function get_artist_song($id)
	{
		$sql10=mysql_query("select * from media_songs where media_id='".$id."' ");
		return($sql10);
	}
	function get_artist_gallery_id()
	{
		$new_id=$this->sel_general();
		$gallery=mysql_query("select * from general_media where general_user_id='".$new_id['general_user_id']."' and media_type=113 AND delete_status=0");
		return($gallery);
	}
	
	
	function get_artist_song_at_register()
	{
		$new_id=$this->sel_general();
		$sql11=mysql_query("select * from general_artist_audio where profile_id='".$new_id['artist_id']."'");
		return($sql11);
	}
	function get_artist_video_at_register()
	{
		$new_id=$this->sel_general();
		$sql12=mysql_query("select * from general_artist_video where profile_id='".$new_id['artist_id']."'");
		return($sql12);
	}
	function get_artist_gallery_at_register()
	{
		$new_id=$this->sel_general();
		$sql14=mysql_query("select * from general_artist_gallery where profile_id='".$new_id['artist_id']."'");
		$sql13 = mysql_fetch_assoc($sql14);
		return($sql13);
	}
	function get_artist_gallery_name_at_register()
	{
		$new_id=$this->get_artist_gallery_at_register();
		$sql13=mysql_query("select * from general_artist_gallery_list where gallery_id='".$new_id['gallery_id']."'");
		return($sql13);
	}
	
	/*********Functions For Tagged Channel Video Song Gallery Ends Here*******/
	
	/*********Functions For Tagged Channel Video Song Gallery By Other User*******/
	
	function get_artist_media_type_tagged_by_other_user($id)
	{
		$sql14=mysql_query("select * from general_media where id='".$id."' AND delete_status=0");
		$res14=mysql_fetch_assoc($sql14);
		return($res14);
	}
	
	/*********Functions For Tagged Channel Video Song Gallery By Other User Ends Here*******/
	
	function get_event_tagged_by_other_user($id)
	{
		$date=date("Y-m-d");
		$sql14=mysql_query("select * from artist_event where id='".$id."' AND date>='".$date."' AND del_status=0 AND active=0");
		$res14=mysql_fetch_assoc($sql14);
		return($res14);
	}
	
	function get_cevent_tagged_by_other_user($id)
	{
		$date=date("Y-m-d");
		$sql14=mysql_query("select * from community_event where id='".$id."' AND date>='".$date."' AND del_status=0 AND active=0");
		$res14=mysql_fetch_assoc($sql14);
		return($res14);
	}
	
	function get_event_tagged_by_other_userrec($id)
	{
		$date=date("Y-m-d");
		$sql14=mysql_query("select * from artist_event where id='".$id."' AND date<'".$date."' AND del_status=0 AND active=0");
		$res14=mysql_fetch_assoc($sql14);
		return($res14);
	}
	
	function get_cevent_tagged_by_other_userrec($id)
	{
		$date=date("Y-m-d");
		$sql14=mysql_query("select * from community_event where id='".$id."' AND date<'".$date."' AND del_status=0 AND active=0");
		$res14=mysql_fetch_assoc($sql14);
		return($res14);
	}
	
	function get_event_tagged_by_other2_user($id)
	{
		$sql_1_cre = "";
		
		$date = date("Y-m-d");
		$sql14 = mysql_query("select * from artist_project where id='".$id."' AND del_status=0 AND active=0");
		
		if(mysql_num_rows($sql14)>0)
		{
			$res14 = mysql_fetch_assoc($sql14);
			//while($res14 = mysql_fetch_assoc($sql14))
			//{
			$cret_info = 'artist_project|'.$res14['id'];
			
			$sql_1_cre = mysql_query("SELECT * FROM artist_event WHERE creators_info='".$cret_info."' AND active=0 AND del_status=0 AND date>='".$date."'");
			return $sql_1_cre;
		}
			
		/*	$sql_2_cre = mysql_query("SELECT * FROM community_event WHERE creators_info='".$cret_info."' AND active=0 AND del_status=0");
			if(mysql_num_rows($sql_2_cre)>0)
			{
				while($run_c = mysql_fetch_assoc($sql_2_cre))
				{
					$run_c['id'] = $run_c['id'].'~'.'com';
					//$ans_c_als[]
					$total[] = $run_c;
				}
			}
		//}
		
		return $total; */
	}
	
	function get_event_ctagged_by_other2_user($id)
	{
		$sql_2_cre = "";
		
		$date = date("Y-m-d");
		$sql14 = mysql_query("select * from artist_project where id='".$id."' AND del_status=0 AND active=0");
		
		if(mysql_num_rows($sql14)>0)
		{
			$res14 = mysql_fetch_assoc($sql14);
			//while($res14 = mysql_fetch_assoc($sql14))
			//{
			$cret_info = 'artist_project|'.$res14['id'];
			
			$sql_2_cre = mysql_query("SELECT * FROM community_event WHERE creators_info='".$cret_info."' AND active=0 AND del_status=0 AND date>='".$date."'");
			return $sql_2_cre;
		}
			
		/*	
			if(mysql_num_rows($sql_2_cre)>0)
			{
				while($run_c = mysql_fetch_assoc($sql_2_cre))
				{
					$run_c['id'] = $run_c['id'].'~'.'com';
					//$ans_c_als[]
					$total[] = $run_c;
				}
			}
		//}
		
		return $total; */
	}
	
	function get_cevent_tagged_by_other2_user($id)
	{
		$sql_1_cre = "";
		
		$date=date("Y-m-d");
		$sql14 = mysql_query("select * from community_project where id='".$id."' AND del_status=0 AND active=0");
		
		if(mysql_num_rows($sql14)>0)
		{
			$res14 = mysql_fetch_assoc($sql14);
			//while($res14 = mysql_fetch_assoc($sql14))
			//{
			$cret_info = 'community_project|'.$res14['id'];
			
			$sql_1_cre = mysql_query("SELECT * FROM artist_event WHERE creators_info='".$cret_info."' AND active=0 AND del_status=0 AND date>='".$date."'");
			return $sql_1_cre;
		}
	}
	
	function get_cevent_ctagged_by_other2_user($id)
	{
		$sql_2_cre = "";
		
		$date=date("Y-m-d");
		$sql14 = mysql_query("select * from community_project where id='".$id."' AND del_status=0 AND active=0");
		
		if(mysql_num_rows($sql14)>0)
		{
			$res14 = mysql_fetch_assoc($sql14);
			//while($res14 = mysql_fetch_assoc($sql14))
			//{
			$cret_info = 'community_project|'.$res14['id'];
			
			$sql_2_cre = mysql_query("SELECT * FROM community_event WHERE creators_info='".$cret_info."' AND active=0 AND del_status=0 AND date>='".$date."'");
			return $sql_2_cre;
		}
	}
	
	function get_event_tagged_by_other2_userrec($id)
	{
		$sql_2_cre = "";
		
		$date=date("Y-m-d");
		$sql14 = mysql_query("select * from artist_project where id='".$id."' AND del_status=0 AND active=0");
		
		if(mysql_num_rows($sql14)>0)
		{
			$res14 = mysql_fetch_assoc($sql14);
			//while($res14 = mysql_fetch_assoc($sql14))
			//{
			$cret_info = 'artist_project|'.$res14['id'];
			
			$sql_2_cre = mysql_query("SELECT * FROM artist_event WHERE creators_info='".$cret_info."' AND active=0 AND del_status=0 AND date<'".$date."'");
			return $sql_2_cre;
		}
	}
	
	function get_event_ctagged_by_other2_userrec($id)
	{
		$sql_2_cre = "";
		
		$date=date("Y-m-d");
		$sql14 = mysql_query("select * from artist_project where id='".$id."' AND del_status=0 AND active=0");
		
		if(mysql_num_rows($sql14)>0)
		{
			$res14 = mysql_fetch_assoc($sql14);
			//while($res14 = mysql_fetch_assoc($sql14))
			//{
			$cret_info = 'artist_project|'.$res14['id'];
			
			$sql_2_cre = mysql_query("SELECT * FROM community_event WHERE creators_info='".$cret_info."' AND active=0 AND del_status=0 AND date<'".$date."'");
			return $sql_2_cre;
		}
	}
	
	function get_cevent_tagged_by_other2_userrec($id)
	{
		$sql_2_cre = "";
		
		$date=date("Y-m-d");
		$sql14 = mysql_query("select * from community_project where id='".$id."' AND del_status=0 AND active=0");
		
		if(mysql_num_rows($sql14)>0)
		{
			$res14 = mysql_fetch_assoc($sql14);
			//while($res14 = mysql_fetch_assoc($sql14))
			//{
			$cret_info = 'community_project|'.$res14['id'];
			
			$sql_2_cre = mysql_query("SELECT * FROM artist_event WHERE creators_info='".$cret_info."' AND active=0 AND del_status=0 AND date<'".$date."'");
			return $sql_2_cre;
		}
	}
	
	function get_cevent_ctagged_by_other2_userrec($id)
	{
		$sql_2_cre = "";
		
		$date=date("Y-m-d");
		$sql14 = mysql_query("select * from community_project where id='".$id."' AND del_status=0 AND active=0");
		
		if(mysql_num_rows($sql14)>0)
		{
			$res14 = mysql_fetch_assoc($sql14);
			//while($res14 = mysql_fetch_assoc($sql14))
			//{
			$cret_info = 'community_project|'.$res14['id'];
			
			$sql_2_cre = mysql_query("SELECT * FROM community_event WHERE creators_info='".$cret_info."' AND active=0 AND del_status=0 AND date<'".$date."'");
			return $sql_2_cre;
		}
	}
	
	function selectUpcomingEvents()
	{
		$new_id=$this->sel_general();
		$update_event = mysql_query("SELECT taggedupcomingevents FROM general_artist WHERE artist_id='".$new_id['artist_id']."'");
		return ($update_event);
	}
	
	function selectArtistEvents($ans_exp)
	{
		$check_artist = mysql_query("SELECT * FROM artist_event WHERE id='".$ans_exp."'");
		return ($check_artist);
	}
	
	function selectRecordedEvents()
	{
		$new_id=$this->sel_general();
		$sql_update_up = mysql_query("SELECT taggedrecordedevents FROM general_artist WHERE artist_id='".$new_id['artist_id']."'");
		return ($sql_update_up);
	}
	
	function updatedRecEvents($final_value)
	{
		$new_id=$this->sel_general();
		$update_upc = mysql_query("UPDATE general_artist SET taggedrecordedevents='".$final_value."' WHERE artist_id='".$new_id['artist_id']."'");
	}
	
	function updatedUpcEvents($new_array1)
	{
		$new_id=$this->sel_general();
		$update_rec = mysql_query("UPDATE general_artist SET taggedupcomingevents='".$new_array1."' WHERE artist_id='".$new_id['artist_id']."'");
	}
	
	function get_project_tagged_by_other_user($id)
	{
		$date=date("Y-m-d");
		$sql14=mysql_query("select * from artist_project where id='".$id."' AND del_status=0 AND active=0");
		$res14=mysql_fetch_assoc($sql14);
		return($res14);
	}
	
	function get_project_tagged_by_other2_user($id)
	{
		$sql_1_cre = "";		
		
		$date = date("Y-m-d");
		$sql14 = mysql_query("select * from artist_project where id='".$id."' AND del_status=0 AND active=0");
		if(mysql_num_rows($sql14)>0)
		{
			$res14 = mysql_fetch_assoc($sql14);
			$cret_info = 'artist_project|'.$res14['id'];
			
			$sql_1_cre = mysql_query("SELECT * FROM artist_project WHERE creators_info='".$cret_info."' AND active=0 AND del_status=0");
			
			return $sql_1_cre;
		}
	}
	
	function get_project_ctagged_by_other2_user($id)
	{
		$sql_2_cre = "";		
		
		$date = date("Y-m-d");
		$sql14 = mysql_query("select * from artist_project where id='".$id."' AND del_status=0 AND active=0");
		if(mysql_num_rows($sql14)>0)
		{
			$res14 = mysql_fetch_assoc($sql14);
			$cret_info = 'artist_project|'.$res14['id'];
			
			$sql_2_cre = mysql_query("SELECT * FROM community_project WHERE creators_info='".$cret_info."' AND active=0 AND del_status=0");
			
			return $sql_2_cre;
		}
	}
	
	function get_cproject_tagged_by_other_user($id)
	{
		$date=date("Y-m-d");
		$sql14=mysql_query("select * from community_project where id='".$id."' AND del_status=0 AND active=0");
		$res14=mysql_fetch_assoc($sql14);
		return($res14);
	}
	
	function get_cproject_tagged_by_other2_user($id)
	{
		$sql_1_cre = "";
		
		$date=date("Y-m-d");
		$sql14 = mysql_query("select * from community_project where id='".$id."' AND del_status=0 AND active=0");
		if(mysql_num_rows($sql14)>0)
		{
			$res14 = mysql_fetch_assoc($sql14);
			$cret_info = 'community_project|'.$res14['id'];
				
			$sql_1_cre = mysql_query("SELECT * FROM artist_project WHERE creators_info='".$cret_info."' AND active=0 AND del_status=0");
			return $sql_1_cre;
		}
	}
	
	function get_cproject_ctagged_by_other2_user($id)
	{
		$sql_2_cre = "";
		
		$date=date("Y-m-d");
		$sql14 = mysql_query("select * from community_project where id='".$id."' AND del_status=0 AND active=0");
		if(mysql_num_rows($sql14)>0)
		{
			$res14 = mysql_fetch_assoc($sql14);
			$cret_info = 'community_project|'.$res14['id'];
			
			$sql_2_cre = mysql_query("SELECT * FROM community_project WHERE creators_info='".$cret_info."' AND active=0 AND del_status=0");
			
			return $sql_2_cre;
		}
	}
	
	function Get_comm_member_Id()
	{
		$row=$this->sel_general();
		$get_member=mysql_query("select * from community_membership where community_id='".$row['community_id']."'");
		$member_res=mysql_fetch_assoc($get_member);
		return($member_res);
	}
	function Get_artist_member_Id()
	{
		$row=$this->sel_general();
		$get_art_member=mysql_query("select * from artist_membership where artist_id='".$row['artist_id']."'");
		if(mysql_num_rows($get_art_member)>0){
			$member_art_res=mysql_fetch_assoc($get_art_member);
		}
		return($member_art_res);
	}
	function Get_purify_member_Id()
	{
		$date= 	date('Y-m-d');
		$row=$this->sel_general();
		$get_purify_member=mysql_query("select * from purify_membership where general_user_id='".$row['general_user_id']."' AND (expiry_date>'".$date."' OR lifetime=1)");
		$member_purify_res=mysql_fetch_assoc($get_purify_member);
		return($member_purify_res);
	}
	function get_fanclub_media()
	{
		$get_gen = $this->sel_general();
		$get_fan = mysql_query("select * from general_media where (sharing_preference = 3 OR sharing_preference = 4) AND general_user_id='".$get_gen['general_user_id']."' AND delete_status=0" );
		return($get_fan);
	}
	function get_forsale_media()
	{
		$get_gen = $this->sel_general();
		$get_sale = mysql_query("select * from general_media where sharing_preference = 5  AND general_user_id='".$get_gen['general_user_id']."' AND delete_status=0" );
		return($get_sale);
	}
	function Get_artist_fan($art_id)
	{
		$get_sale = mysql_query("select * from fan_club_membership where expiry_date>CURDATE()");
		while($row = mysql_fetch_assoc($get_sale))
		{
			$exp = explode("_",$row['related_id']);
			if($exp[0] == $art_id)
			{
				$exp_count +=1;
			}
		}
		return($exp_count);
	}
	function Get_artist_total_fan($art_id)
	{
		$get_sale = mysql_query("select * from fan_club_membership ");
		while($row = mysql_fetch_assoc($get_sale))
		{
			$exp = explode("_",$row['related_id']);
			if($exp[0] == $art_id)
			{
				$exp_count +=1;
			}
		}
		return($exp_count);
	}
	
	function Get_all_friends()
	{
		$res = $this -> sel_general();
		$get_friend = mysql_query("select * from friends where general_user_id ='".$res['general_user_id']."' AND status=0");
		if(mysql_num_rows($get_friend)>0)
		{
			return($get_friend);
		}
	}
	function get_friend_art_info($friend_id)
	{
		$get_gen_info = mysql_query("select * from general_user where general_user_id ='".$friend_id."' AND delete_status=0");
		if(mysql_num_rows($get_gen_info)>0)
		{
			$res_gen_info = mysql_fetch_assoc($get_gen_info);
			if($res_gen_info['artist_id']!="" && $res_gen_info['artist_id']!=0)
			{
				return ($res_gen_info['artist_id']);
			}
		}
	}
	function get_all_tagged_pro($art_id)
	{
		$get_projects = mysql_query("select * from artist_project where artist_id='".$art_id."' AND del_status=0 AND active=0");
		return($get_projects);
	}
	function get_all_tagged_event($art_id)
	{
		$get_events = mysql_query("select * from artist_event where artist_id='".$art_id."' AND del_status=0 AND active=0");
		return($get_events);
	}
	
	function get_media_create($type)
	{
		$id = $this->sel_general();
		
		$var_type_1 = 'general_artist';
		$var_type_2 = 'general_community';
		$var_type_3 = 'artist_project';
		$var_type_4 = 'community_project';
		
		$pal_1 = array();
		$pal_2 = array();
		$pal_3 = array();
		$pal_4 = array();
		
		$final_c_f = array();
		
		if($id['artist_id']!=0 && !empty($id['artist_id']))
		{
			$chk_type_a = $var_type_1.'|'.$id['artist_id'];

			$sql = mysql_query("SELECT * FROM general_media WHERE creator_info='".$chk_type_a."' AND delete_status=0 AND media_status=0 AND media_type='".$type."' AND general_user_id!='".$id['general_user_id']."'");
			if(mysql_num_rows($sql)>0)
			{
				while($row_a = mysql_fetch_assoc($sql))
				{
					$final_c_f[] = $row_a;
				}
			}
		}
		
		/* if($id['community_id']!=0 && !empty($id['community_id']))
		{
			$chk_type_c = $var_type_2.'|'.$id['community_id'];
			
			$sql = mysql_query("SELECT * FROM general_media WHERE creator_info='".$chk_type_c."' AND delete_status=0 AND media_status=0 AND media_type='".$type."' AND general_user_id!='".$id['general_user_id']."'");
			if(mysql_num_rows($sql)>0)
			{
				while($row_c = mysql_fetch_assoc($sql))
				{
					$final_c_f[] = $row_c;
				}				
			}
		} */
		
		/* if($id['artist_id']!=0 && !empty($id['artist_id']))
		{			
			$sql_get_allp = mysql_query("SELECT * FROM artist_project WHERE artist_id='".$id['artist_id']."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_get_allp)>0)
			{
				while($rowal = mysql_fetch_assoc($sql_get_allp))
				{
					$pal_1[] = $rowal;
				}
			}
			
			$sql_get_alle = mysql_query("SELECT * FROM artist_event WHERE artist_id='".$id['artist_id']."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_get_alle)>0)
			{
				while($rowael = mysql_fetch_assoc($sql_get_alle))
				{
					$pal_3[] = $rowael;
				}
			}
			
			if(!empty($pal_1) && count($pal_1))
			{
				for($pl=0;$pl<count($pal_1);$pl++)
				{
					$chk_typep_1 = 'artist_project|'.$pal_1[$pl]['id'];
					
					$sql_as = mysql_query("SELECT * FROM general_media WHERE creator_info='".$chk_typep_1."' AND delete_status=0 AND media_status=0 AND media_type='".$type."' AND general_user_id!='".$id['general_user_id']."'");
					if(mysql_num_rows($sql_as)>0)
					{
						while($row_ap = mysql_fetch_assoc($sql_as))
						{
							$final_c_f[] = $row_ap;
						}
					}
				}
			}
			
			if(!empty($pal_3) && count($pal_3))
			{
				for($pl=0;$pl<count($pal_3);$pl++)
				{
					$chk_typep_1 = 'artist_event|'.$pal_3[$pl]['id'];
					
					$sql_as = mysql_query("SELECT * FROM general_media WHERE from_info='".$chk_typep_1."' AND delete_status=0 AND media_status=0 AND media_type='".$type."' AND general_user_id!='".$id['general_user_id']."'");
					if(mysql_num_rows($sql_as)>0)
					{
						while($row_ae = mysql_fetch_assoc($sql_as))
						{
							$final_c_f[] = $row_ae;
						}
					}
				}
			}
		}
		
		if($id['community_id']!=0 && !empty($id['community_id']))
		{			
			$sql_get_allp = mysql_query("SELECT * FROM community_project WHERE community_id='".$id['community_id']."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_get_allp)>0)
			{
				while($rowal = mysql_fetch_assoc($sql_get_allp))
				{
					$pal_2[] = $rowal;
				}
			}
			
			$sql_get_allew = mysql_query("SELECT * FROM community_event WHERE community_id='".$id['community_id']."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_get_allew)>0)
			{
				while($rowawl = mysql_fetch_assoc($sql_get_allew))
				{
					$pal_4[] = $rowawl;
				}
			}
			
			if(!empty($pal_2) && count($pal_2))
			{
				for($pl=0;$pl<count($pal_1);$pl++)
				{
					$chk_typep_1 = 'community_project|'.$pal_1[$pl]['id'];
					
					$sql_as = mysql_query("SELECT * FROM general_media WHERE creator_info='".$chk_typep_1."' AND delete_status=0 AND media_status=0 AND media_type='".$type."' AND general_user_id!='".$id['general_user_id']."'");
					if(mysql_num_rows($sql_as)>0)
					{
						while($row_cp = mysql_fetch_assoc($sql_as))
						{
							$final_c_f[] = $row_cp;
						}
					}
				}
			}
			
			if(!empty($pal_4) && count($pal_4))
			{
				for($pl=0;$pl<count($pal_4);$pl++)
				{
					$chk_typep_1 = 'community_event|'.$pal_4[$pl]['id'];
					
					$sql_as = mysql_query("SELECT * FROM general_media WHERE from_info='".$chk_typep_1."' AND delete_status=0 AND media_status=0 AND media_type='".$type."' AND general_user_id!='".$id['general_user_id']."'");
					if(mysql_num_rows($sql_as)>0)
					{
						while($row_ce = mysql_fetch_assoc($sql_as))
						{
							$final_c_f[] = $row_ce;
						}
					}
				}
			}
		} */
		//$final_c_f = array_merge($final_c_f1,$final_c_f2);
		return $final_c_f;
	}
	
	function get_project_create()
	{
		$id = $this->sel_general();
		$var_type_1 = 'general_artist';
		$var_type_2 = 'general_community';
		$var_type_3 = 'artist_project';
		$var_type_4 = 'community_project';
		
		$artm = array();
		$arta = array();
		$comm = array();
		$coma = array();
		
		$artme = array();
		$artae = array();
		$comme = array();
		$comae = array();
		
		$art_p_ao = array();
		$com_p_ao = array();
		$art_p_co = array();
		$com_p_co = array();
		
		$art_e_ao = array();
		$com_e_ao = array();
		$art_e_co = array();
		$com_e_co = array();
		
		$pal_1 = array();
		$pal_2 = array();
		
		if($id['artist_id']!=0 && !empty($id['artist_id']))
		{
			$chk_type = $var_type_1.'|'.$id['artist_id'];
			
			$sql_get_allp = mysql_query("SELECT * FROM artist_project WHERE artist_id='".$id['artist_id']."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_get_allp)>0)
			{
				while($rowal = mysql_fetch_assoc($sql_get_allp))
				{
					$pal_1[] = $rowal;
				}
			}
			
			if(!empty($pal_1) && count($pal_1))
			{
				for($pl=0;$pl<count($pal_1);$pl++)
				{
					$chk_typep_1 = 'artist_project|'.$pal_1[$pl]['id'];
					
					$sql = mysql_query("SELECT * FROM artist_project WHERE creators_info='".$chk_typep_1."' AND del_status=0 AND active=0");
					if(mysql_num_rows($sql)>0)
					{
						while($row_po = mysql_fetch_assoc($sql))
						{
							if($row_po['artist_id']!=$id['artist_id'])
							{
								$art_p_ao[] = $row_po;
							}
						}
					}
					
					$sql = mysql_query("SELECT * FROM community_project WHERE creators_info='".$chk_typep_1."' AND del_status=0 AND active=0");
					if(mysql_num_rows($sql)>0)
					{
						while($row_poo = mysql_fetch_assoc($sql))
						{
							if($row_poo['community_id']!=$id['community_id'])
							{
								$com_p_ao[] = $row_poo;
							}
						}
					}
				}
			}
			
			if(!empty($pal_1) && count($pal_1))
			{
				for($pl=0;$pl<count($pal_1);$pl++)
				{
					$chk_typep_1 = 'artist_project|'.$pal_1[$pl]['id'];
					
					$sql = mysql_query("SELECT * FROM artist_event WHERE creators_info='".$chk_typep_1."' AND del_status=0 AND active=0");
					if(mysql_num_rows($sql)>0)
					{
						while($row_po = mysql_fetch_assoc($sql))
						{
							if($row_po['artist_id']!=$id['artist_id'])
							{
								$art_e_ao[] = $row_po;
							}
						}
					}
					
					$sql = mysql_query("SELECT * FROM community_event WHERE creators_info='".$chk_typep_1."' AND del_status=0 AND active=0");
					if(mysql_num_rows($sql)>0)
					{
						while($row_poo = mysql_fetch_assoc($sql))
						{
							if($row_poo['community_id']!=$id['community_id'])
							{
								$com_e_ao[] = $row_poo;
							}
						}
					}
				}
			}

			$sql_pro_arta = mysql_query("SELECT * FROM artist_project WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_pro_arta)>0)
			{
				while($rowaa = mysql_fetch_assoc($sql_pro_arta))
				{
					if($rowaa['artist_id']!=$id['artist_id'])
					{
						$arta[] = $rowaa;
					}
				}
			}
			
			$sql_pro_arta = mysql_query("SELECT * FROM artist_event WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_pro_arta)>0)
			{
				while($rowaa = mysql_fetch_assoc($sql_pro_arta))
				{
					if($rowaa['artist_id']!=$id['artist_id'])
					{
						$artae[] = $rowaa;
					}
				}
			}
			
			$sql_pro_coma = mysql_query("SELECT * FROM community_project WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_pro_coma)>0)
			{
				while($rowam = mysql_fetch_assoc($sql_pro_coma))
				{
					if($rowam['community_id']!=$id['community_id'])
					{
						$coma[] = $rowam;
					}
				}
			}
			
			$sql_pro_coma = mysql_query("SELECT * FROM community_event WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_pro_coma)>0)
			{
				while($rowam = mysql_fetch_assoc($sql_pro_coma))
				{
					if($rowam['community_id']!=$id['community_id'])
					{
						$comae[] = $rowam;
					}
				}
			}
		}
		
		if($id['community_id']!=0 && !empty($id['community_id']))
		{
			$chk_type = $var_type_2.'|'.$id['community_id'];
			$sql_pro_artm = mysql_query("SELECT * FROM artist_project WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_pro_artm)>0)
			{
				while($rowcm = mysql_fetch_assoc($sql_pro_artm))
				{
					if($rowcm['artist_id']!=$id['artist_id'])
					{
						$artm[] = $rowcm;
					}
				}
			}
			
			$chk_type = $var_type_2.'|'.$id['community_id'];
			$sql_pro_artm = mysql_query("SELECT * FROM artist_event WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_pro_artm)>0)
			{
				while($rowcm = mysql_fetch_assoc($sql_pro_artm))
				{
					if($rowcm['artist_id']!=$id['artist_id'])
					{
						$artme[] = $rowcm;
					}
				}
			}
			
			$sql_pro_comm = mysql_query("SELECT * FROM community_project WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_pro_comm)>0)
			{
				while($rowca = mysql_fetch_assoc($sql_pro_comm))
				{
					if($rowca['community_id']!=$id['community_id'])
					{
						$comm[] = $rowca;
					}
				}
			}
			
			$sql_pro_comm = mysql_query("SELECT * FROM community_event WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_pro_comm)>0)
			{
				while($rowca = mysql_fetch_assoc($sql_pro_comm))
				{
					if($rowca['community_id']!=$id['community_id'])
					{
						$comme[] = $rowca;
					}
				}
			}
			
			$sql_get_allp = mysql_query("SELECT * FROM community_project WHERE community_id='".$id['community_id']."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_get_allp)>0)
			{
				while($rowal = mysql_fetch_assoc($sql_get_allp))
				{
					$pal_2[] = $rowal;
				}
			}
			
			if(!empty($pal_2) && count($pal_2))
			{
				for($pl2=0;$pl2<count($pal_2);$pl2++)
				{
					$chk_typep_2 = 'community_project|'.$pal_2[$pl2]['id'];
					$sql = mysql_query("SELECT * FROM artist_project WHERE creators_info='".$chk_typep_2."' AND del_status=0 AND active=0");
					if(mysql_num_rows($sql)>0)
					{
						while($row_po = mysql_fetch_assoc($sql))
						{
							if($row_po['artist_id']!=$id['artist_id'])
							{
								$art_p_co[] = $row_po;
							}
						}
					}
					
					$sql = mysql_query("SELECT * FROM community_project WHERE creators_info='".$chk_typep_2."' AND del_status=0 AND active=0");
					if(mysql_num_rows($sql)>0)
					{
						while($row_poo = mysql_fetch_assoc($sql))
						{
							if($row_poo['community_id']!=$id['community_id'])
							{
								$com_p_co[] = $row_poo;
							}
						}
					}
				}
			}
			
			if(!empty($pal_2) && count($pal_2))
			{
				for($pl2=0;$pl2<count($pal_2);$pl2++)
				{
					$chk_typep_2 = 'community_project|'.$pal_2[$pl2]['id'];
					$sql = mysql_query("SELECT * FROM artist_event WHERE creators_info='".$chk_typep_2."' AND del_status=0 AND active=0");
					if(mysql_num_rows($sql)>0)
					{
						while($row_po = mysql_fetch_assoc($sql))
						{
							if($row_po['artist_id']!=$id['artist_id'])
							{
								$art_e_co[] = $row_po;
							}
						}
					}
					
					$sql = mysql_query("SELECT * FROM community_event WHERE creators_info='".$chk_typep_2."' AND del_status=0 AND active=0");
					if(mysql_num_rows($sql)>0)
					{
						while($row_poo = mysql_fetch_assoc($sql))
						{
							if($row_poo['community_id']!=$id['community_id'])
							{
								$com_e_co[] = $row_poo;
							}
						}
					}
				}
			}
		}
		
		$totals = array();
		$totals = array_merge($artm,$arta,$comm,$coma,$art_p_ao,$com_p_ao,$art_p_co,$com_p_co,$artme,$artae,$comme,$comae,$art_e_ao,$com_e_ao,$art_e_co,$com_e_co);
		return $totals;
	}
	
	/************Functions for displaying tagged media fro projects**********************/
	
	function get_artist_project_tag($email)
	{
		//echo "SELECT * FROM artist_project WHERE del_status=0 AND active=0 AND tagged_user_email LIKE '%".$email."%'";
		$query = mysql_query("SELECT * FROM artist_project WHERE del_status=0 AND active=0 AND tagged_user_email LIKE '%".$email."%'");
		return($query);
	}

	function get_community_project_tag($email)
	{
		$query = mysql_query("SELECT * FROM community_project WHERE del_status=0 AND active=0 AND tagged_user_email LIKE '%".$email."%'");
		return($query);
	}
	
	function get_artist_event_tag($email)
	{
		$query = mysql_query("SELECT * FROM artist_event WHERE del_status=0 AND active=0 AND tagged_user_email LIKE '%".$email."%'");
		return($query);
	}
	
	function get_community_event_tag($email)
	{
		$query = mysql_query("SELECT * FROM community_event WHERE del_status=0 AND active=0 AND tagged_user_email LIKE '%".$email."%'");
		return($query);
	}
	
	function get_tag_media_info($media_id)
	{
		$sql= mysql_query("SELECT general_media. * , type.name, type.type_id FROM general_media,type WHERE general_media.media_type = type.type_id AND general_media.id ='".$media_id."' AND general_media.delete_status=0");
		if(mysql_num_rows($sql)>0)
		{
			$row = mysql_fetch_assoc($sql);
			return($row);
		}
	}
	/************Functions for displaying tagged media fro projects**********************/
	
	function get_sel_songs($id)
	{
		$query = mysql_query("select * from general_artist where media_id='".$id."'");
		$res = mysql_fetch_assoc($query);
		return($res);
	}
	
	function get_media_reg_info($med_id,$types)
	{
		if($types!='')
		{
			$typ_ss = explode('~',$types);
			$id = $this->sel_general();
			$chk_reg_med = '';
			if($typ_ss[0]=='artist' && $typ_ss[1]!='')
			{
				$sql_audr = mysql_query("SELECT * FROM general_artist_audio WHERE audio_id='".$med_id."' AND profile_id='".$typ_ss[1]."'");
				$sql_vidr = mysql_query("SELECT * FROM general_artist_video WHERE video_id='".$med_id."' AND profile_id='".$typ_ss[1]."'");
				$sql_galr = mysql_query("SELECT * FROM general_artist_gallery WHERE gallery_id='".$med_id."' AND profile_id='".$typ_ss[1]."'");
				
				if(mysql_num_rows($sql_audr)>0)
				{
					$chk_reg_med = 'general_artist_audio';
				}
				elseif(mysql_num_rows($sql_vidr)>0)
				{
					$chk_reg_med = 'general_artist_video';
				}
				elseif(mysql_num_rows($sql_galr)>0)
				{
					$chk_reg_med = 'general_artist_gallery';
				}
				
				if($chk_reg_med!="")
				{
					return $chk_reg_med;
				}
			}
			elseif($typ_ss[0]=='community' && $typ_ss[1]!='')
			{
				$sql_audr = mysql_query("SELECT * FROM general_community_audio WHERE audio_id='".$med_id."' AND profile_id='".$typ_ss[1]."'");
				$sql_vidr = mysql_query("SELECT * FROM general_community_video WHERE video_id='".$med_id."' AND profile_id='".$typ_ss[1]."'");
				$sql_galr = mysql_query("SELECT * FROM general_community_gallery WHERE gallery_id='".$med_id."' AND profile_id='".$typ_ss[1]."'");
				
				if(mysql_num_rows($sql_audr)>0)
				{
					$chk_reg_med = 'general_community_audio';
				}
				elseif(mysql_num_rows($sql_vidr)>0)
				{
					$chk_reg_med = 'general_community_video';
				}
				elseif(mysql_num_rows($sql_galr)>0)
				{
					$chk_reg_med = 'general_community_gallery';
				}
				
				if($chk_reg_med!="")
				{
					return $chk_reg_med;
				}
			}
		}
	}
	
	/* function only_creator_this()
	{
		$new_id = $this->sel_general();
		$final_pros = array();
		
		if(isset($new_id['artist_id']) && !empty($new_id['artist_id']) && $new_id['artist_id']!=0)
		{
			$mcth = 'general_artist|'.$new_id['artist_id'];
			$sql_apro = mysql_query("SELECT * FROM artist_project WHERE creators_info='".$mcth."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_apro)>0)
			{
				while($ans_apro = mysql_fetch_assoc($sql_apro))
				{				
					$ans_apro['id'] = $ans_apro['id'].'~art';
					$final_pros[] = $ans_apro;
				}	
			}
			
			$sql_cpro = mysql_query("SELECT * FROM community_project WHERE creators_info='".$mcth."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_cpro)>0)
			{
				while($ans_cpro = mysql_fetch_assoc($sql_cpro))
				{				
					$ans_cpro['id'] = $ans_cpro['id'].'~com';
					$final_pros[] = $ans_cpro;
				}
			}
		}
		
		return $final_pros;
	} */
	
	function only_creator_this()
	{
		$id = $this->sel_general();
		$var_type_1 = 'general_artist';
		$var_type_2 = 'general_community';
		$var_type_3 = 'artist_project';
		$var_type_4 = 'community_project';
		
		$artm = array();
		$arta = array();
		$comm = array();
		$coma = array();
		
		$artme = array();
		$artae = array();
		$comme = array();
		$comae = array();
		
		$art_p_ao = array();
		$com_p_ao = array();
		$art_p_co = array();
		$com_p_co = array();
		
		$art_e_ao = array();
		$com_e_ao = array();
		$art_e_co = array();
		$com_e_co = array();
		
		$pal_1 = array();
		$pal_2 = array();
		
		if($id['artist_id']!=0 && !empty($id['artist_id']))
		{
			$chk_type = $var_type_1.'|'.$id['artist_id'];
			
			$sql_get_allp = mysql_query("SELECT * FROM artist_project WHERE artist_id='".$id['artist_id']."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_get_allp)>0)
			{
				while($rowal = mysql_fetch_assoc($sql_get_allp))
				{
					$rowal['whos_project'] = "only_creator";
					$pal_1[] = $rowal;
				}
			}
			
			if(!empty($pal_1) && count($pal_1))
			{
				for($pl=0;$pl<count($pal_1);$pl++)
				{
					$chk_typep_1 = 'artist_project|'.$pal_1[$pl]['id'];
					
					$sql = mysql_query("SELECT * FROM artist_project WHERE creators_info='".$chk_typep_1."' AND del_status=0 AND active=0");
					if(mysql_num_rows($sql)>0)
					{
						while($row_po = mysql_fetch_assoc($sql))
						{
							if($row_po['artist_id']!=$id['artist_id'])
							{
								$row_po['id'] = $row_po['id'].'~art';
								$row_po['whos_project'] = "only_creator";
								$art_p_ao[] = $row_po;
							}
						}
					}
					
					$sql = mysql_query("SELECT * FROM community_project WHERE creators_info='".$chk_typep_1."' AND del_status=0 AND active=0");
					if(mysql_num_rows($sql)>0)
					{
						while($row_poo = mysql_fetch_assoc($sql))
						{
							if($row_poo['community_id']!=$id['community_id'])
							{
								$row_poo['id'] = $row_poo['id'].'~com';
								$row_poo['whos_project'] = "only_creator";
								$com_p_ao[] = $row_poo;
							}
						}
					}
				}
			}
			
			/* if(!empty($pal_1) && count($pal_1))
			{
				for($pl=0;$pl<count($pal_1);$pl++)
				{
					$chk_typep_1 = 'artist_project|'.$pal_1[$pl]['id'];
					
					$sql = mysql_query("SELECT * FROM artist_event WHERE creators_info='".$chk_typep_1."' AND del_status=0 AND active=0");
					if(mysql_num_rows($sql)>0)
					{
						while($row_po = mysql_fetch_assoc($sql))
						{
							if($row_po['artist_id']!=$id['artist_id'])
							{
								$art_e_ao[] = $row_po;
							}
						}
					}
					
					$sql = mysql_query("SELECT * FROM community_event WHERE creators_info='".$chk_typep_1."' AND del_status=0 AND active=0");
					if(mysql_num_rows($sql)>0)
					{
						while($row_poo = mysql_fetch_assoc($sql))
						{
							if($row_poo['community_id']!=$id['community_id'])
							{
								$com_e_ao[] = $row_poo;
							}
						}
					}
				}
			} */

			$sql_pro_arta = mysql_query("SELECT * FROM artist_project WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_pro_arta)>0)
			{
				while($rowaa = mysql_fetch_assoc($sql_pro_arta))
				{
					if($rowaa['artist_id']!=$id['artist_id'])
					{
						$rowaa['id'] = $rowaa['id'].'~art';
						$rowaa['whos_project'] = "only_creator";
						$arta[] = $rowaa;
					}
				}
			}
			
			/* $sql_pro_arta = mysql_query("SELECT * FROM artist_event WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_pro_arta)>0)
			{
				while($rowaa = mysql_fetch_assoc($sql_pro_arta))
				{
					if($rowaa['artist_id']!=$id['artist_id'])
					{
						$artae[] = $rowaa;
					}
				}
			} */
			
			$sql_pro_coma = mysql_query("SELECT * FROM community_project WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_pro_coma)>0)
			{
				while($rowam = mysql_fetch_assoc($sql_pro_coma))
				{
					if($rowam['community_id']!=$id['community_id'])
					{
						$rowam['id'] = $rowam['id'].'~com';
						$rowam['whos_project'] = "only_creator";
						$coma[] = $rowam;
					}
				}
			}
			
			/* $sql_pro_coma = mysql_query("SELECT * FROM community_event WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_pro_coma)>0)
			{
				while($rowam = mysql_fetch_assoc($sql_pro_coma))
				{
					if($rowam['community_id']!=$id['community_id'])
					{
						$comae[] = $rowam;
					}
				}
			} */
		}
		
		if($id['community_id']!=0 && !empty($id['community_id']))
		{
			$chk_type = $var_type_2.'|'.$id['community_id'];
			$sql_pro_artm = mysql_query("SELECT * FROM artist_project WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_pro_artm)>0)
			{
				while($rowcm = mysql_fetch_assoc($sql_pro_artm))
				{
					if($rowcm['artist_id']!=$id['artist_id'])
					{
						$rowcm['id'] = $rowcm['id'].'~art';
						$rowcm['whos_project'] = "only_creator";
						$artm[] = $rowcm;
					}
				}
			}
			
			/* $chk_type = $var_type_2.'|'.$id['community_id'];
			$sql_pro_artm = mysql_query("SELECT * FROM artist_event WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_pro_artm)>0)
			{
				while($rowcm = mysql_fetch_assoc($sql_pro_artm))
				{
					if($rowcm['artist_id']!=$id['artist_id'])
					{
						$artme[] = $rowcm;
					}
				}
			} */
			
			$sql_pro_comm = mysql_query("SELECT * FROM community_project WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_pro_comm)>0)
			{
				while($rowca = mysql_fetch_assoc($sql_pro_comm))
				{
					if($rowca['community_id']!=$id['community_id'])
					{
						$rowca['id'] = $rowca['id'].'~com';
						$rowca['whos_project'] = "only_creator";
						$comm[] = $rowca;
					}
				}
			}
			
			/* $sql_pro_comm = mysql_query("SELECT * FROM community_event WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_pro_comm)>0)
			{
				while($rowca = mysql_fetch_assoc($sql_pro_comm))
				{
					if($rowca['community_id']!=$id['community_id'])
					{
						$comme[] = $rowca;
					}
				}
			} */
			
			$sql_get_allp = mysql_query("SELECT * FROM community_project WHERE community_id='".$id['community_id']."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_get_allp)>0)
			{
				while($rowal = mysql_fetch_assoc($sql_get_allp))
				{
					$rowal['whos_project'] = "only_creator";
					$pal_2[] = $rowal;
				}
			}
			
			if(!empty($pal_2) && count($pal_2))
			{
				for($pl2=0;$pl2<count($pal_2);$pl2++)
				{
					$chk_typep_2 = 'community_project|'.$pal_2[$pl2]['id'];
					$sql = mysql_query("SELECT * FROM artist_project WHERE creators_info='".$chk_typep_2."' AND del_status=0 AND active=0");
					if(mysql_num_rows($sql)>0)
					{
						while($row_po = mysql_fetch_assoc($sql))
						{
							if($row_po['artist_id']!=$id['artist_id'])
							{
								$row_po['id'] = $row_po['id'].'~art';
								$row_po['whos_project'] = "only_creator";
								$art_p_co[] = $row_po;
							}
						}
					}
					
					$sql = mysql_query("SELECT * FROM community_project WHERE creators_info='".$chk_typep_2."' AND del_status=0 AND active=0");
					if(mysql_num_rows($sql)>0)
					{
						while($row_poo = mysql_fetch_assoc($sql))
						{
							if($row_poo['community_id']!=$id['community_id'])
							{
								$row_poo['id'] = $row_poo['id'].'~com';
								$row_poo['whos_project'] = "only_creator";
								$com_p_co[] = $row_poo;
							}
						}
					}
				}
			}
			
			/* if(!empty($pal_2) && count($pal_2))
			{
				for($pl2=0;$pl2<count($pal_2);$pl2++)
				{
					$chk_typep_2 = 'community_project|'.$pal_2[$pl2]['id'];
					$sql = mysql_query("SELECT * FROM artist_event WHERE creators_info='".$chk_typep_2."' AND del_status=0 AND active=0");
					if(mysql_num_rows($sql)>0)
					{
						while($row_po = mysql_fetch_assoc($sql))
						{
							if($row_po['artist_id']!=$id['artist_id'])
							{
								$art_e_co[] = $row_po;
							}
						}
					}
					
					$sql = mysql_query("SELECT * FROM community_event WHERE creators_info='".$chk_typep_2."' AND del_status=0 AND active=0");
					if(mysql_num_rows($sql)>0)
					{
						while($row_poo = mysql_fetch_assoc($sql))
						{
							if($row_poo['community_id']!=$id['community_id'])
							{
								$com_e_co[] = $row_poo;
							}
						}
					}
				}
			} */
		}
		
		
		$totals = array();
		$totals = array_merge($artm,$arta,$comm,$coma,$art_p_ao,$com_p_ao,$art_p_co,$com_p_co,$artme,$artae,$comme,$comae,$art_e_ao,$com_e_ao,$art_e_co,$com_e_co);
		return $totals;
	}
	
	function only_sh_creator_this()
	{
		$id = $this->sel_general();
		$var_type_1 = 'general_artist';
		$var_type_2 = 'general_community';
		$var_type_3 = 'artist_project';
		$var_type_4 = 'community_project';
		
		$artm = array();
		$arta = array();
		$comm = array();
		$coma = array();
		
		$artme = array();
		$artae = array();
		$comme = array();
		$comae = array();
		
		$art_p_ao = array();
		$com_p_ao = array();
		$art_p_co = array();
		$com_p_co = array();
		
		$art_e_ao = array();
		$com_e_ao = array();
		$art_e_co = array();
		$com_e_co = array();
		
		$pal_1 = array();
		$pal_2 = array();
		
		if($id['artist_id']!=0 && !empty($id['artist_id']))
		{
			$chk_type = $var_type_1.'|'.$id['artist_id'];
			
			$sql_get_allp = mysql_query("SELECT * FROM artist_project WHERE artist_id='".$id['artist_id']."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_get_allp)>0)
			{
				while($rowal = mysql_fetch_assoc($sql_get_allp))
				{
					$rowal['whos_project'] = "only_creator";
					$pal_1[] = $rowal;
				}
			}
			
			if(!empty($pal_1) && count($pal_1))
			{
				for($pl=0;$pl<count($pal_1);$pl++)
				{
					$chk_typep_1 = 'artist_project|'.$pal_1[$pl]['id'];
					
					$sql = mysql_query("SELECT * FROM artist_project WHERE creators_info='".$chk_typep_1."' AND del_status=0 AND active=0");
					if(mysql_num_rows($sql)>0)
					{
						while($row_po = mysql_fetch_assoc($sql))
						{
							if($row_po['artist_id']!=$id['artist_id'])
							{
								$row_po['table_name'] = 'artist_project';
								$sql = mysql_query("SELECT MIN( `id` ) FROM `artist_project_profiles` WHERE artist_project_id='".$row_po['id']."' AND subtype_id!=0");
							
								if(mysql_num_rows($sql)>0)
								{
									$res = mysql_fetch_assoc($sql);
									
									$sql2 = mysql_query("select * from artist_project_profiles where id='".$res['MIN( `id` )']."'");
									$res2 = mysql_fetch_assoc($sql2);

									$sql4 = mysql_query("select * from  subtype where subtype_id = '".$res2['subtype_id']."'");
									if(mysql_num_rows($sql4)>0)
									{
										$res4 = mysql_fetch_assoc($sql4);
										$row_po['type'] = $res4['name'];
									}
									else
									{
										$row_po['type'] = '';
									}
								}
								else
								{
									$row_po['type'] = '';
								}
								$art_p_ao[] = $row_po;
							}
						}
					}
					
					$sql = mysql_query("SELECT * FROM community_project WHERE creators_info='".$chk_typep_1."' AND del_status=0 AND active=0");
					if(mysql_num_rows($sql)>0)
					{
						while($row_poo = mysql_fetch_assoc($sql))
						{
							if($row_poo['community_id']!=$id['community_id'])
							{
								$row_poo['table_name'] = 'community_project';
								$sql = mysql_query("SELECT MIN( `id` ) FROM `community_project_profiles` WHERE community_project_id='".$row_poo['id']."' AND subtype_id!=0");
							
								if(mysql_num_rows($sql)>0)
								{
									$res = mysql_fetch_assoc($sql);
									
									$sql2 = mysql_query("select * from community_project_profiles where id='".$res['MIN( `id` )']."'");
									$res2 = mysql_fetch_assoc($sql2);

									$sql4 = mysql_query("select * from  subtype where subtype_id = '".$res2['subtype_id']."'");
									if(mysql_num_rows($sql4)>0)
									{
										$res4 = mysql_fetch_assoc($sql4);
										$row_poo['type'] = $res4['name'];
									}
									else
									{
										$row_poo['type'] = '';
									}
								}
								else
								{
									$row_poo['type'] = '';
								}
								$com_p_ao[] = $row_poo;
							}
						}
					}
				}
			}
			
			/* if(!empty($pal_1) && count($pal_1))
			{
				for($pl=0;$pl<count($pal_1);$pl++)
				{
					$chk_typep_1 = 'artist_project|'.$pal_1[$pl]['id'];
					
					$sql = mysql_query("SELECT * FROM artist_event WHERE creators_info='".$chk_typep_1."' AND del_status=0 AND active=0");
					if(mysql_num_rows($sql)>0)
					{
						while($row_po = mysql_fetch_assoc($sql))
						{
							if($row_po['artist_id']!=$id['artist_id'])
							{
								$art_e_ao[] = $row_po;
							}
						}
					}
					
					$sql = mysql_query("SELECT * FROM community_event WHERE creators_info='".$chk_typep_1."' AND del_status=0 AND active=0");
					if(mysql_num_rows($sql)>0)
					{
						while($row_poo = mysql_fetch_assoc($sql))
						{
							if($row_poo['community_id']!=$id['community_id'])
							{
								$com_e_ao[] = $row_poo;
							}
						}
					}
				}
			} */

			$sql_pro_arta = mysql_query("SELECT * FROM artist_project WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_pro_arta)>0)
			{
				while($rowaa = mysql_fetch_assoc($sql_pro_arta))
				{
					if($rowaa['artist_id']!=$id['artist_id'])
					{
						$rowaa['table_name'] = 'artist_project';
						$sql = mysql_query("SELECT MIN( `id` ) FROM `artist_project_profiles` WHERE artist_project_id='".$rowaa['id']."' AND subtype_id!=0");
							
						if(mysql_num_rows($sql)>0)
						{
							$res = mysql_fetch_assoc($sql);
							
							$sql2 = mysql_query("select * from artist_project_profiles where id='".$res['MIN( `id` )']."'");
							$res2 = mysql_fetch_assoc($sql2);

							$sql4 = mysql_query("select * from  subtype where subtype_id = '".$res2['subtype_id']."'");
							if(mysql_num_rows($sql4)>0)
							{
								$res4 = mysql_fetch_assoc($sql4);
								$rowaa['type'] = $res4['name'];
							}
							else
							{
								$rowaa['type'] = '';
							}
						}
						else
						{
							$rowaa['type'] = '';
						}
						$arta[] = $rowaa;
					}
				}
			}
			
			/* $sql_pro_arta = mysql_query("SELECT * FROM artist_event WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_pro_arta)>0)
			{
				while($rowaa = mysql_fetch_assoc($sql_pro_arta))
				{
					if($rowaa['artist_id']!=$id['artist_id'])
					{
						$artae[] = $rowaa;
					}
				}
			} */
			
			$sql_pro_coma = mysql_query("SELECT * FROM community_project WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_pro_coma)>0)
			{
				while($rowam = mysql_fetch_assoc($sql_pro_coma))
				{
					if($rowam['community_id']!=$id['community_id'])
					{
						$rowam['table_name'] = 'community_project';
						$sql = mysql_query("SELECT MIN( `id` ) FROM `community_project_profiles` WHERE community_project_id='".$rowam['id']."' AND subtype_id!=0");
							
						if(mysql_num_rows($sql)>0)
						{
							$res = mysql_fetch_assoc($sql);
							
							$sql2 = mysql_query("select * from community_project_profiles where id='".$res['MIN( `id` )']."'");
							$res2 = mysql_fetch_assoc($sql2);

							$sql4 = mysql_query("select * from  subtype where subtype_id = '".$res2['subtype_id']."'");
							if(mysql_num_rows($sql4)>0)
							{
								$res4 = mysql_fetch_assoc($sql4);
								$rowam['type'] = $res4['name'];
							}
							else
							{
								$rowam['type'] = '';
							}
						}
						else
						{
							$rowam['type'] = '';
						}
						$coma[] = $rowam;
					}
				}
			}
			
			/* $sql_pro_coma = mysql_query("SELECT * FROM community_event WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_pro_coma)>0)
			{
				while($rowam = mysql_fetch_assoc($sql_pro_coma))
				{
					if($rowam['community_id']!=$id['community_id'])
					{
						$comae[] = $rowam;
					}
				}
			} */
		}
		
		if($id['community_id']!=0 && !empty($id['community_id']))
		{
			$chk_type = $var_type_2.'|'.$id['community_id'];
			$sql_pro_artm = mysql_query("SELECT * FROM artist_project WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_pro_artm)>0)
			{
				while($rowcm = mysql_fetch_assoc($sql_pro_artm))
				{
					if($rowcm['artist_id']!=$id['artist_id'])
					{
						$rowcm['table_name'] = 'artist_project';
						$sql = mysql_query("SELECT MIN( `id` ) FROM `artist_project_profiles` WHERE artist_project_id='".$rowcm['id']."' AND subtype_id!=0");
							
						if(mysql_num_rows($sql)>0)
						{
							$res = mysql_fetch_assoc($sql);
							
							$sql2 = mysql_query("select * from artist_project_profiles where id='".$res['MIN( `id` )']."'");
							$res2 = mysql_fetch_assoc($sql2);

							$sql4 = mysql_query("select * from  subtype where subtype_id = '".$res2['subtype_id']."'");
							if(mysql_num_rows($sql4)>0)
							{
								$res4 = mysql_fetch_assoc($sql4);
								$rowcm['type'] = $res4['name'];
							}
							else
							{
								$rowcm['type'] = '';
							}
						}
						else
						{
							$rowcm['type'] = '';
						}
						$artm[] = $rowcm;
					}
				}
			}
			
			/* $chk_type = $var_type_2.'|'.$id['community_id'];
			$sql_pro_artm = mysql_query("SELECT * FROM artist_event WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_pro_artm)>0)
			{
				while($rowcm = mysql_fetch_assoc($sql_pro_artm))
				{
					if($rowcm['artist_id']!=$id['artist_id'])
					{
						$artme[] = $rowcm;
					}
				}
			} */
			
			$sql_pro_comm = mysql_query("SELECT * FROM community_project WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_pro_comm)>0)
			{
				while($rowca = mysql_fetch_assoc($sql_pro_comm))
				{
					if($rowca['community_id']!=$id['community_id'])
					{
						$rowca['table_name'] = 'community_project';
						$sql = mysql_query("SELECT MIN( `id` ) FROM `community_project_profiles` WHERE community_project_id='".$rowca['id']."' AND subtype_id!=0");
							
						if(mysql_num_rows($sql)>0)
						{
							$res = mysql_fetch_assoc($sql);
							
							$sql2 = mysql_query("select * from community_project_profiles where id='".$res['MIN( `id` )']."'");
							$res2 = mysql_fetch_assoc($sql2);

							$sql4 = mysql_query("select * from  subtype where subtype_id = '".$res2['subtype_id']."'");
							if(mysql_num_rows($sql4)>0)
							{
								$res4 = mysql_fetch_assoc($sql4);
								$rowca['type'] = $res4['name'];
							}
							else
							{
								$rowca['type'] = '';
							}
						}
						else
						{
							$rowca['type'] = '';
						}
						$comm[] = $rowca;
					}
				}
			}
			
			/* $sql_pro_comm = mysql_query("SELECT * FROM community_event WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_pro_comm)>0)
			{
				while($rowca = mysql_fetch_assoc($sql_pro_comm))
				{
					if($rowca['community_id']!=$id['community_id'])
					{
						$comme[] = $rowca;
					}
				}
			} */
			
			$sql_get_allp = mysql_query("SELECT * FROM community_project WHERE community_id='".$id['community_id']."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_get_allp)>0)
			{
				while($rowal = mysql_fetch_assoc($sql_get_allp))
				{
					$rowal['whos_project'] = "only_creator";
					$pal_2[] = $rowal;
				}
			}
			
			if(!empty($pal_2) && count($pal_2))
			{
				for($pl2=0;$pl2<count($pal_2);$pl2++)
				{
					$chk_typep_2 = 'community_project|'.$pal_2[$pl2]['id'];
					$sql = mysql_query("SELECT * FROM artist_project WHERE creators_info='".$chk_typep_2."' AND del_status=0 AND active=0");
					if(mysql_num_rows($sql)>0)
					{
						while($row_po = mysql_fetch_assoc($sql))
						{
							if($row_po['artist_id']!=$id['artist_id'])
							{
								$row_po['table_name'] = 'artist_project';
								$sql = mysql_query("SELECT MIN( `id` ) FROM `artist_project_profiles` WHERE artist_project_id='".$row_po['id']."' AND subtype_id!=0");
							
								if(mysql_num_rows($sql)>0)
								{
									$res = mysql_fetch_assoc($sql);
									
									$sql2 = mysql_query("select * from artist_project_profiles where id='".$res['MIN( `id` )']."'");
									$res2 = mysql_fetch_assoc($sql2);

									$sql4 = mysql_query("select * from  subtype where subtype_id = '".$res2['subtype_id']."'");
									if(mysql_num_rows($sql4)>0)
									{
										$res4 = mysql_fetch_assoc($sql4);
										$row_po['type'] = $res4['name'];
									}
									else
									{
										$row_po['type'] = '';
									}
								}
								else
								{
									$row_po['type'] = '';
								}
								$art_p_co[] = $row_po;
							}
						}
					}
					
					$sql = mysql_query("SELECT * FROM community_project WHERE creators_info='".$chk_typep_2."' AND del_status=0 AND active=0");
					if(mysql_num_rows($sql)>0)
					{
						while($row_poo = mysql_fetch_assoc($sql))
						{
							if($row_poo['community_id']!=$id['community_id'])
							{
								$row_poo['table_name'] = 'community_project';
								$sql = mysql_query("SELECT MIN( `id` ) FROM `community_project_profiles` WHERE community_project_id='".$row_poo['id']."' AND subtype_id!=0");
							
								if(mysql_num_rows($sql)>0)
								{
									$res = mysql_fetch_assoc($sql);
									
									$sql2 = mysql_query("select * from community_project_profiles where id='".$res['MIN( `id` )']."'");
									$res2 = mysql_fetch_assoc($sql2);

									$sql4 = mysql_query("select * from  subtype where subtype_id = '".$res2['subtype_id']."'");
									if(mysql_num_rows($sql4)>0)
									{
										$res4 = mysql_fetch_assoc($sql4);
										$row_poo['type'] = $res4['name'];
									}
									else
									{
										$row_poo['type'] = '';
									}
								}
								else
								{
									$row_poo['type'] = '';
								}
								$com_p_co[] = $row_poo;
							}
						}
					}
				}
			}
			
			/* if(!empty($pal_2) && count($pal_2))
			{
				for($pl2=0;$pl2<count($pal_2);$pl2++)
				{
					$chk_typep_2 = 'community_project|'.$pal_2[$pl2]['id'];
					$sql = mysql_query("SELECT * FROM artist_event WHERE creators_info='".$chk_typep_2."' AND del_status=0 AND active=0");
					if(mysql_num_rows($sql)>0)
					{
						while($row_po = mysql_fetch_assoc($sql))
						{
							if($row_po['artist_id']!=$id['artist_id'])
							{
								$art_e_co[] = $row_po;
							}
						}
					}
					
					$sql = mysql_query("SELECT * FROM community_event WHERE creators_info='".$chk_typep_2."' AND del_status=0 AND active=0");
					if(mysql_num_rows($sql)>0)
					{
						while($row_poo = mysql_fetch_assoc($sql))
						{
							if($row_poo['community_id']!=$id['community_id'])
							{
								$com_e_co[] = $row_poo;
							}
						}
					}
				}
			} */
		}
		
		
		$totals = array();
		$totals = array_merge($artm,$arta,$comm,$coma,$art_p_ao,$com_p_ao,$art_p_co,$com_p_co,$artme,$artae,$comme,$comae,$art_e_ao,$com_e_ao,$art_e_co,$com_e_co);
		return $totals;
	}
	
	function only_creator2pr_this()
	{
		$id = $this->sel_general();
		$var_type_1 = 'general_artist';
		$var_type_2 = 'general_community';
		$var_type_3 = 'artist_project';
		$var_type_4 = 'community_project';
		
		$art_p_ao = array();
		$art_c_ao = array();
		
		$com_p_ao = array();
		$com_c_ao = array();
		
		$com_a_ao = array();
		$art_a_ao = array();
		
		$art_ap_ao = array();
		$com_cp_ao = array();
		
		$pal_1 = array();
		$pal_2 = array();
		
		if($id['artist_id']!=0 && !empty($id['artist_id']))
		{
			$chk_type = $var_type_1.'|'.$id['artist_id'];
			
			$sql_get_allp = mysql_query("SELECT * FROM artist_project WHERE artist_id='".$id['artist_id']."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_get_allp)>0)
			{
				while($rowal = mysql_fetch_assoc($sql_get_allp))
				{
					//$rowal['whos_project'] = "only_creator";
					$pal_1[] = $rowal;
				}
			}
			
			if(!empty($pal_1) && count($pal_1))
			{
				for($pl=0;$pl<count($pal_1);$pl++)
				{
					$chk_typep_1 = 'artist_project|'.$pal_1[$pl]['id'];
					
					$sql = mysql_query("SELECT * FROM artist_project WHERE (creators_info='".$chk_typep_1."' OR creators_info='".$chk_type."') AND del_status=0 AND active=0");
					if(mysql_num_rows($sql)>0)
					{
						while($row_po = mysql_fetch_assoc($sql))
						{
							//if($row_po['artist_id']!=$id['artist_id'])
							//{
								
								$sql_art2 = mysql_query("SELECT * FROM artist_project WHERE creators_info='artist_project|".$row_po['id']."' AND del_status=0 AND active=0");
								if(mysql_num_rows($sql_art2)>0)
								{
									while($row2_po = mysql_fetch_assoc($sql_art2))
									{
										$row2_po['id'] = $row2_po['id'].'~art';
										$row2_po['whos_project'] = "art_creator_creator_pro";
										$art_p_ao[] = $row2_po;
									}
								}
								
								
								$sql_art3 = mysql_query("SELECT * FROM community_project WHERE creators_info='artist_project|".$row_po['id']."' AND del_status=0 AND active=0");
								if(mysql_num_rows($sql_art3)>0)
								{
									while($row3_po = mysql_fetch_assoc($sql_art3))
									{
										$row3_po['id'] = $row3_po['id'].'~com';
										$row3_po['whos_project'] = "com_creator_creator_pro";
										$art_c_ao[] = $row3_po;
									}
								}
							//}
						}
					}
					
					
					$sql = mysql_query("SELECT * FROM community_project WHERE (creators_info='".$chk_typep_1."' OR creators_info='".$chk_type."') AND del_status=0 AND active=0");
					if(mysql_num_rows($sql)>0)
					{
						while($row_poo = mysql_fetch_assoc($sql))
						{
							//if($row_poo['community_id']!=$id['community_id'])
							//{		
								
								$sql_art2 = mysql_query("SELECT * FROM artist_project WHERE creators_info='community_project|".$row_poo['id']."' AND del_status=0 AND active=0");
								if(mysql_num_rows($sql_art2)>0)
								{
									while($row2_po = mysql_fetch_assoc($sql_art2))
									{
										$row2_po['id'] = $row2_po['id'].'~art';
										$row2_po['whos_project'] = "art_creator_creator_pro";
										$com_p_ao[] = $row2_po;
									}
								}
								
								
								$sql_art3 = mysql_query("SELECT * FROM community_project WHERE creators_info='community_project|".$row_poo['id']."' AND del_status=0 AND active=0");
								if(mysql_num_rows($sql_art3)>0)
								{
									while($row3_po = mysql_fetch_assoc($sql_art3))
									{
										$row3_po['id'] = $row3_po['id'].'~com';
										$row3_po['whos_project'] = "com_creator_creator_pro";
										$com_c_ao[] = $row3_po;
									}
								}
							//}
						}
					}
				}
			}
		}
		
		if($id['community_id']!=0 && !empty($id['community_id']))
		{
			$chk_type = $var_type_2.'|'.$id['community_id'];
			
			$sql_get_allp = mysql_query("SELECT * FROM community_project WHERE community_id='".$id['community_id']."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_get_allp)>0)
			{
				while($rowal = mysql_fetch_assoc($sql_get_allp))
				{
					//$rowal['whos_project'] = "only_creator";
					$pal_2[] = $rowal;
				}
			}
			
			if(!empty($pal_2) && count($pal_2))
			{
				for($pl2=0;$pl2<count($pal_2);$pl2++)
				{
					$chk_typep_2 = 'community_project|'.$pal_2[$pl2]['id'];
					
					
					$sql = mysql_query("SELECT * FROM artist_project WHERE (creators_info='".$chk_typep_2."' OR creators_info='".$chk_type."') AND del_status=0 AND active=0");
					if(mysql_num_rows($sql)>0)
					{
						while($row_po = mysql_fetch_assoc($sql))
						{
							//if($row_po['artist_id']!=$id['artist_id'])
							//{
								
								$sql_art2 = mysql_query("SELECT * FROM artist_project WHERE creators_info='artist_project|".$row_po['id']."' AND del_status=0 AND active=0");
								if(mysql_num_rows($sql_art2)>0)
								{
									while($row2_po = mysql_fetch_assoc($sql_art2))
									{
										$row2_po['id'] = $row2_po['id'].'~art';
										$row2_po['whos_project'] = "art_creator_creator_pro";
										$com_a_ao[] = $row2_po;
									}
								}
								
								
								$sql_art3 = mysql_query("SELECT * FROM community_project WHERE creators_info='artist_project|".$row_po['id']."' AND del_status=0 AND active=0");
								if(mysql_num_rows($sql_art3)>0)
								{
									while($row3_po = mysql_fetch_assoc($sql_art3))
									{
										$row3_po['id'] = $row3_po['id'].'~com';
										$row3_po['whos_project'] = "com_creator_creator_pro";
										$art_a_ao[] = $row3_po;
									}
								}
							//}
						}
					}
					
					
					$sql = mysql_query("SELECT * FROM community_project WHERE (creators_info='".$chk_typep_2."' OR creators_info='".$chk_type."') AND del_status=0 AND active=0");
					if(mysql_num_rows($sql)>0)
					{
						while($row_poo = mysql_fetch_assoc($sql))
						{
							//if($row_poo['community_id']!=$id['community_id'])
							//{
								
								$sql_art2 = mysql_query("SELECT * FROM artist_project WHERE creators_info='community_project|".$row_poo['id']."' AND del_status=0 AND active=0");
								if(mysql_num_rows($sql_art2)>0)
								{
									while($row2_po = mysql_fetch_assoc($sql_art2))
									{
										$row2_po['id'] = $row2_po['id'].'~art';
										$row2_po['whos_project'] = "art_creator_creator_pro";
										$art_ap_ao[] = $row2_po;
									}
								}
								
								
								$sql_art3 = mysql_query("SELECT * FROM community_project WHERE creators_info='community_project|".$row_poo['id']."' AND del_status=0 AND active=0");
								if(mysql_num_rows($sql_art3)>0)
								{
									while($row3_po = mysql_fetch_assoc($sql_art3))
									{
										$row3_po['id'] = $row3_po['id'].'~com';
										$row3_po['whos_project'] = "com_creator_creator_pro";
										$com_cp_ao[] = $row3_po;
									}
								}
							//}
						}
					}
				}
			}
		}		
		
		$totals = array();
		$totals = array_merge($art_p_ao,$art_c_ao,$com_p_ao,$com_c_ao,$com_a_ao,$art_a_ao,$art_ap_ao,$com_cp_ao);
		return $totals;
	}
	
	function only_sh_creator2pr_this()
	{
		$id = $this->sel_general();
		$var_type_1 = 'general_artist';
		$var_type_2 = 'general_community';
		$var_type_3 = 'artist_project';
		$var_type_4 = 'community_project';
		
		$art_p_ao = array();
		$art_c_ao = array();
		
		$com_p_ao = array();
		$com_c_ao = array();
		
		$com_a_ao = array();
		$art_a_ao = array();
		
		$art_ap_ao = array();
		$com_cp_ao = array();
		
		$pal_1 = array();
		$pal_2 = array();
		
		if($id['artist_id']!=0 && !empty($id['artist_id']))
		{
			$chk_type = $var_type_1.'|'.$id['artist_id'];
			
			$sql_get_allp = mysql_query("SELECT * FROM artist_project WHERE artist_id='".$id['artist_id']."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_get_allp)>0)
			{
				while($rowal = mysql_fetch_assoc($sql_get_allp))
				{
					//$rowal['whos_project'] = "only_creator";
					$pal_1[] = $rowal;
				}
			}
			
			if(!empty($pal_1) && count($pal_1))
			{
				for($pl=0;$pl<count($pal_1);$pl++)
				{
					$chk_typep_1 = 'artist_project|'.$pal_1[$pl]['id'];
					
					$sql = mysql_query("SELECT * FROM artist_project WHERE (creators_info='".$chk_typep_1."' OR creators_info='".$chk_type."') AND del_status=0 AND active=0");
					if(mysql_num_rows($sql)>0)
					{
						while($row_po = mysql_fetch_assoc($sql))
						{
							//if($row_po['artist_id']!=$id['artist_id'])
							//{
								
								$sql_art2 = mysql_query("SELECT * FROM artist_project WHERE creators_info='artist_project|".$row_po['id']."' AND del_status=0 AND active=0");
								if(mysql_num_rows($sql_art2)>0)
								{
									while($row2_po = mysql_fetch_assoc($sql_art2))
									{
										$row2_po['table_name'] = 'artist_project';
										$sql = mysql_query("SELECT MIN( `id` ) FROM `artist_project_profiles` WHERE artist_project_id='".$row2_po['id']."' AND subtype_id!=0");
							
										if(mysql_num_rows($sql)>0)
										{
											$res = mysql_fetch_assoc($sql);
											
											$sql2 = mysql_query("select * from artist_project_profiles where id='".$res['MIN( `id` )']."'");
											$res2 = mysql_fetch_assoc($sql2);

											$sql4 = mysql_query("select * from  subtype where subtype_id = '".$res2['subtype_id']."'");
											if(mysql_num_rows($sql4)>0)
											{
												$res4 = mysql_fetch_assoc($sql4);
												$row2_po['type'] = $res4['name'];
											}
											else
											{
												$row2_po['type'] = '';
											}
										}
										else
										{
											$row2_po['type'] = '';
										}
										$art_p_ao[] = $row2_po;
									}
								}
								
								
								$sql_art3 = mysql_query("SELECT * FROM community_project WHERE creators_info='artist_project|".$row_po['id']."' AND del_status=0 AND active=0");
								if(mysql_num_rows($sql_art3)>0)
								{
									while($row3_po = mysql_fetch_assoc($sql_art3))
									{
										$row3_po['table_name'] = 'community_project';
										$sql = mysql_query("SELECT MIN( `id` ) FROM `community_project_profiles` WHERE community_project_id='".$row3_po['id']."' AND subtype_id!=0");
							
										if(mysql_num_rows($sql)>0)
										{
											$res = mysql_fetch_assoc($sql);
											
											$sql2 = mysql_query("select * from community_project_profiles where id='".$res['MIN( `id` )']."'");
											$res2 = mysql_fetch_assoc($sql2);

											$sql4 = mysql_query("select * from  subtype where subtype_id = '".$res2['subtype_id']."'");
											if(mysql_num_rows($sql4)>0)
											{
												$res4 = mysql_fetch_assoc($sql4);
												$row3_po['type'] = $res4['name'];
											}
											else
											{
												$row3_po['type'] = '';
											}
										}
										else
										{
											$row3_po['type'] = '';
										}
										$art_c_ao[] = $row3_po;
									}
								}
							//}
						}
					}
					
					
					$sql = mysql_query("SELECT * FROM community_project WHERE (creators_info='".$chk_typep_1."' OR creators_info='".$chk_type."') AND del_status=0 AND active=0");
					if(mysql_num_rows($sql)>0)
					{
						while($row_poo = mysql_fetch_assoc($sql))
						{
							//if($row_poo['community_id']!=$id['community_id'])
							//{		
								
								$sql_art2 = mysql_query("SELECT * FROM artist_project WHERE creators_info='community_project|".$row_poo['id']."' AND del_status=0 AND active=0");
								if(mysql_num_rows($sql_art2)>0)
								{
									while($row2_po = mysql_fetch_assoc($sql_art2))
									{
										$row2_po['table_name'] = 'artist_project';
										$sql = mysql_query("SELECT MIN( `id` ) FROM `artist_project_profiles` WHERE artist_project_id='".$row2_po['id']."' AND subtype_id!=0");
							
										if(mysql_num_rows($sql)>0)
										{
											$res = mysql_fetch_assoc($sql);
											
											$sql2 = mysql_query("select * from artist_project_profiles where id='".$res['MIN( `id` )']."'");
											$res2 = mysql_fetch_assoc($sql2);

											$sql4 = mysql_query("select * from  subtype where subtype_id = '".$res2['subtype_id']."'");
											if(mysql_num_rows($sql4)>0)
											{
												$res4 = mysql_fetch_assoc($sql4);
												$row2_po['type'] = $res4['name'];
											}
											else
											{
												$row2_po['type'] = '';
											}
										}
										else
										{
											$row2_po['type'] = '';
										}
										$com_p_ao[] = $row2_po;
									}
								}
								
								
								$sql_art3 = mysql_query("SELECT * FROM community_project WHERE creators_info='community_project|".$row_poo['id']."' AND del_status=0 AND active=0");
								if(mysql_num_rows($sql_art3)>0)
								{
									while($row3_po = mysql_fetch_assoc($sql_art3))
									{
										$row3_po['table_name'] = 'community_project';
										$sql = mysql_query("SELECT MIN( `id` ) FROM `community_project_profiles` WHERE community_project_id='".$row3_po['id']."' AND subtype_id!=0");
							
										if(mysql_num_rows($sql)>0)
										{
											$res = mysql_fetch_assoc($sql);
											
											$sql2 = mysql_query("select * from community_project_profiles where id='".$res['MIN( `id` )']."'");
											$res2 = mysql_fetch_assoc($sql2);

											$sql4 = mysql_query("select * from  subtype where subtype_id = '".$res2['subtype_id']."'");
											if(mysql_num_rows($sql4)>0)
											{
												$res4 = mysql_fetch_assoc($sql4);
												$row3_po['type'] = $res4['name'];
											}
											else
											{
												$row3_po['type'] = '';
											}
										}
										else
										{
											$row3_po['type'] = '';
										}
										$com_c_ao[] = $row3_po;
									}
								}
							//}
						}
					}
				}
			}
		}
		
		if($id['community_id']!=0 && !empty($id['community_id']))
		{
			$chk_type = $var_type_2.'|'.$id['community_id'];
			
			$sql_get_allp = mysql_query("SELECT * FROM community_project WHERE community_id='".$id['community_id']."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_get_allp)>0)
			{
				while($rowal = mysql_fetch_assoc($sql_get_allp))
				{
					//$rowal['whos_project'] = "only_creator";
					$pal_2[] = $rowal;
				}
			}
			
			if(!empty($pal_2) && count($pal_2))
			{
				for($pl2=0;$pl2<count($pal_2);$pl2++)
				{
					$chk_typep_2 = 'community_project|'.$pal_2[$pl2]['id'];
					
					
					$sql = mysql_query("SELECT * FROM artist_project WHERE (creators_info='".$chk_typep_2."' OR creators_info='".$chk_type."') AND del_status=0 AND active=0");
					if(mysql_num_rows($sql)>0)
					{
						while($row_po = mysql_fetch_assoc($sql))
						{
							//if($row_po['artist_id']!=$id['artist_id'])
							//{
								
								$sql_art2 = mysql_query("SELECT * FROM artist_project WHERE creators_info='artist_project|".$row_po['id']."' AND del_status=0 AND active=0");
								if(mysql_num_rows($sql_art2)>0)
								{
									while($row2_po = mysql_fetch_assoc($sql_art2))
									{
										$row2_po['table_name'] = 'artist_project';
										$sql = mysql_query("SELECT MIN( `id` ) FROM `artist_project_profiles` WHERE artist_project_id='".$row2_po['id']."' AND subtype_id!=0");
							
										if(mysql_num_rows($sql)>0)
										{
											$res = mysql_fetch_assoc($sql);
											
											$sql2 = mysql_query("select * from artist_project_profiles where id='".$res['MIN( `id` )']."'");
											$res2 = mysql_fetch_assoc($sql2);

											$sql4 = mysql_query("select * from  subtype where subtype_id = '".$res2['subtype_id']."'");
											if(mysql_num_rows($sql4)>0)
											{
												$res4 = mysql_fetch_assoc($sql4);
												$row2_po['type'] = $res4['name'];
											}
											else
											{
												$row2_po['type'] = '';
											}
										}
										else
										{
											$row2_po['type'] = '';
										}
										$com_a_ao[] = $row2_po;
									}
								}
								
								
								$sql_art3 = mysql_query("SELECT * FROM community_project WHERE creators_info='artist_project|".$row_po['id']."' AND del_status=0 AND active=0");
								if(mysql_num_rows($sql_art3)>0)
								{
									while($row3_po = mysql_fetch_assoc($sql_art3))
									{
										$row3_po['table_name'] = 'community_project';
										$sql = mysql_query("SELECT MIN( `id` ) FROM `community_project_profiles` WHERE community_project_id='".$row3_po['id']."' AND subtype_id!=0");
							
										if(mysql_num_rows($sql)>0)
										{
											$res = mysql_fetch_assoc($sql);
											
											$sql2 = mysql_query("select * from community_project_profiles where id='".$res['MIN( `id` )']."'");
											$res2 = mysql_fetch_assoc($sql2);

											$sql4 = mysql_query("select * from  subtype where subtype_id = '".$res2['subtype_id']."'");
											if(mysql_num_rows($sql4)>0)
											{
												$res4 = mysql_fetch_assoc($sql4);
												$row3_po['type'] = $res4['name'];
											}
											else
											{
												$row3_po['type'] = '';
											}
										}
										else
										{
											$row3_po['type'] = '';
										}
										$art_a_ao[] = $row3_po;
									}
								}
							//}
						}
					}
					
					
					$sql = mysql_query("SELECT * FROM community_project WHERE (creators_info='".$chk_typep_2."' OR creators_info='".$chk_type."') AND del_status=0 AND active=0");
					if(mysql_num_rows($sql)>0)
					{
						while($row_poo = mysql_fetch_assoc($sql))
						{
							//if($row_poo['community_id']!=$id['community_id'])
							//{
								
								$sql_art2 = mysql_query("SELECT * FROM artist_project WHERE creators_info='community_project|".$row_poo['id']."' AND del_status=0 AND active=0");
								if(mysql_num_rows($sql_art2)>0)
								{
									while($row2_po = mysql_fetch_assoc($sql_art2))
									{
										$row2_po['table_name'] = 'artist_project';
										$sql = mysql_query("SELECT MIN( `id` ) FROM `artist_project_profiles` WHERE artist_project_id='".$row2_po['id']."' AND subtype_id!=0");
							
										if(mysql_num_rows($sql)>0)
										{
											$res = mysql_fetch_assoc($sql);
											
											$sql2 = mysql_query("select * from artist_project_profiles where id='".$res['MIN( `id` )']."'");
											$res2 = mysql_fetch_assoc($sql2);

											$sql4 = mysql_query("select * from  subtype where subtype_id = '".$res2['subtype_id']."'");
											if(mysql_num_rows($sql4)>0)
											{
												$res4 = mysql_fetch_assoc($sql4);
												$row2_po['type'] = $res4['name'];
											}
											else
											{
												$row2_po['type'] = '';
											}
										}
										else
										{
											$row2_po['type'] = '';
										}
										$art_ap_ao[] = $row2_po;
									}
								}
								
								
								$sql_art3 = mysql_query("SELECT * FROM community_project WHERE creators_info='community_project|".$row_poo['id']."' AND del_status=0 AND active=0");
								if(mysql_num_rows($sql_art3)>0)
								{
									while($row3_po = mysql_fetch_assoc($sql_art3))
									{
										$row3_po['table_name'] = 'community_project';
										$sql = mysql_query("SELECT MIN( `id` ) FROM `community_project_profiles` WHERE community_project_id='".$row3_po['id']."' AND subtype_id!=0");
							
										if(mysql_num_rows($sql)>0)
										{
											$res = mysql_fetch_assoc($sql);
											
											$sql2 = mysql_query("select * from community_project_profiles where id='".$res['MIN( `id` )']."'");
											$res2 = mysql_fetch_assoc($sql2);

											$sql4 = mysql_query("select * from  subtype where subtype_id = '".$res2['subtype_id']."'");
											if(mysql_num_rows($sql4)>0)
											{
												$res4 = mysql_fetch_assoc($sql4);
												$row3_po['type'] = $res4['name'];
											}
											else
											{
												$row3_po['type'] = '';
											}
										}
										else
										{
											$row3_po['type'] = '';
										}
										$com_cp_ao[] = $row3_po;
									}
								}
							//}
						}
					}
				}
			}
		}		
		
		$totals = array();
		$totals = array_merge($art_p_ao,$art_c_ao,$com_p_ao,$com_c_ao,$com_a_ao,$art_a_ao,$art_ap_ao,$com_cp_ao);
		return $totals;
	}	
	
	function only_creator2pr_othis()
	{
		$id = $this->sel_general();
		$var_type_1 = 'general_artist';
		$var_type_2 = 'general_community';
		$var_type_3 = 'artist_project';
		$var_type_4 = 'community_project';
		
		$art_p_ao = array();
		$art_c_ao = array();
		
		$com_p_ao = array();
		$com_c_ao = array();
		
		$com_a_ao = array();
		$art_a_ao = array();
		
		$art_ap_ao = array();
		$com_cp_ao = array();
		
		$pal_1 = array();
		$pal_2 = array();
		
		if($id['artist_id']!=0 && !empty($id['artist_id']))
		{
			$chk_type = $var_type_1.'|'.$id['artist_id'];
			
			$sql_get_allp = mysql_query("SELECT * FROM artist_project WHERE artist_id='".$id['artist_id']."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_get_allp)>0)
			{
				while($rowal = mysql_fetch_assoc($sql_get_allp))
				{
					//$rowal['whos_project'] = "only_creator";
					$pal_1[] = $rowal;
				}
			}
			
			if(!empty($pal_1) && count($pal_1))
			{
				for($pl=0;$pl<count($pal_1);$pl++)
				{
					$chk_typep_1 = 'artist_project|'.$pal_1[$pl]['id'];
					
					$sql = mysql_query("SELECT * FROM artist_project WHERE (creators_info='".$chk_typep_1."' OR creators_info='".$chk_type."') AND del_status=0 AND active=0");
					if(mysql_num_rows($sql)>0)
					{
						while($row_po = mysql_fetch_assoc($sql))
						{
							//if($row_po['artist_id']!=$id['artist_id'])
							//{
								
								$sql_art2 = mysql_query("SELECT * FROM artist_project WHERE creators_info='artist_project|".$row_po['id']."' AND del_status=0 AND active=0");
								if(mysql_num_rows($sql_art2)>0)
								{
									while($row2_po = mysql_fetch_assoc($sql_art2))
									{
										if($row2_po['artist_id']!=$id['artist_id'])
										{
											$row2_po['id'] = $row2_po['id'].'~art';
											$row2_po['whos_project'] = "art_creator_creator_pro";
											$art_p_ao[] = $row2_po;
										}
									}
								}
								
								
								$sql_art3 = mysql_query("SELECT * FROM community_project WHERE creators_info='artist_project|".$row_po['id']."' AND del_status=0 AND active=0");
								if(mysql_num_rows($sql_art3)>0)
								{
									while($row3_po = mysql_fetch_assoc($sql_art3))
									{
										if($row3_po['community_id']!=$id['community_id'])
										{
											$row3_po['id'] = $row3_po['id'].'~com';
											$row3_po['whos_project'] = "com_creator_creator_pro";
											$art_c_ao[] = $row3_po;
										}
									}
								}
							//}
						}
					}
					
					
					$sql = mysql_query("SELECT * FROM community_project WHERE (creators_info='".$chk_typep_1."' OR creators_info='".$chk_type."') AND del_status=0 AND active=0");
					if(mysql_num_rows($sql)>0)
					{
						while($row_poo = mysql_fetch_assoc($sql))
						{
							//if($row_poo['community_id']!=$id['community_id'])
							//{		
								
								$sql_art2 = mysql_query("SELECT * FROM artist_project WHERE creators_info='community_project|".$row_poo['id']."' AND del_status=0 AND active=0");
								if(mysql_num_rows($sql_art2)>0)
								{
									while($row2_po = mysql_fetch_assoc($sql_art2))
									{
										if($row2_po['artist_id']!=$id['artist_id'])
										{
											$row2_po['id'] = $row2_po['id'].'~art';
											$row2_po['whos_project'] = "art_creator_creator_pro";
											$com_p_ao[] = $row2_po;
										}
									}
								}
								
								
								$sql_art3 = mysql_query("SELECT * FROM community_project WHERE creators_info='community_project|".$row_poo['id']."' AND del_status=0 AND active=0");
								if(mysql_num_rows($sql_art3)>0)
								{
									while($row3_po = mysql_fetch_assoc($sql_art3))
									{
										if($row3_po['community_id']!=$id['community_id'])
										{
											$row3_po['id'] = $row3_po['id'].'~com';
											$row3_po['whos_project'] = "com_creator_creator_pro";
											$com_c_ao[] = $row3_po;
										}
									}
								}
							//}
						}
					}
				}
			}
		}
		
		if($id['community_id']!=0 && !empty($id['community_id']))
		{
			$chk_type = $var_type_2.'|'.$id['community_id'];
			
			$sql_get_allp = mysql_query("SELECT * FROM community_project WHERE community_id='".$id['community_id']."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_get_allp)>0)
			{
				while($rowal = mysql_fetch_assoc($sql_get_allp))
				{
					//$rowal['whos_project'] = "only_creator";
					$pal_2[] = $rowal;
				}
			}
			
			if(!empty($pal_2) && count($pal_2))
			{
				for($pl2=0;$pl2<count($pal_2);$pl2++)
				{
					$chk_typep_2 = 'community_project|'.$pal_2[$pl2]['id'];
					
					
					$sql = mysql_query("SELECT * FROM artist_project WHERE (creators_info='".$chk_typep_2."' OR creators_info='".$chk_type."') AND del_status=0 AND active=0");
					if(mysql_num_rows($sql)>0)
					{
						while($row_po = mysql_fetch_assoc($sql))
						{
							//if($row_po['artist_id']!=$id['artist_id'])
							//{
								
								$sql_art2 = mysql_query("SELECT * FROM artist_project WHERE creators_info='artist_project|".$row_po['id']."' AND del_status=0 AND active=0");
								if(mysql_num_rows($sql_art2)>0)
								{
									while($row2_po = mysql_fetch_assoc($sql_art2))
									{
										if($row2_po['artist_id']!=$id['artist_id'])
										{
											$row2_po['id'] = $row2_po['id'].'~art';
											$row2_po['whos_project'] = "art_creator_creator_pro";
											$com_a_ao[] = $row2_po;
										}
									}
								}
								
								
								$sql_art3 = mysql_query("SELECT * FROM community_project WHERE creators_info='artist_project|".$row_po['id']."' AND del_status=0 AND active=0");
								if(mysql_num_rows($sql_art3)>0)
								{
									while($row3_po = mysql_fetch_assoc($sql_art3))
									{
										if($row3_po['community_id']!=$id['community_id'])
										{
											$row3_po['id'] = $row3_po['id'].'~com';
											$row3_po['whos_project'] = "com_creator_creator_pro";
											$art_a_ao[] = $row3_po;
										}
									}
								}
							//}
						}
					}
					
					
					$sql = mysql_query("SELECT * FROM community_project WHERE (creators_info='".$chk_typep_2."' OR creators_info='".$chk_type."') AND del_status=0 AND active=0");
					if(mysql_num_rows($sql)>0)
					{
						while($row_poo = mysql_fetch_assoc($sql))
						{
							//if($row_poo['community_id']!=$id['community_id'])
							//{
								
								$sql_art2 = mysql_query("SELECT * FROM artist_project WHERE creators_info='community_project|".$row_poo['id']."' AND del_status=0 AND active=0");
								if(mysql_num_rows($sql_art2)>0)
								{
									while($row2_po = mysql_fetch_assoc($sql_art2))
									{
										if($row2_po['artist_id']!=$id['artist_id'])
										{
											$row2_po['id'] = $row2_po['id'].'~art';
											$row2_po['whos_project'] = "art_creator_creator_pro";
											$art_ap_ao[] = $row2_po;
										}
									}
								}
								
								
								$sql_art3 = mysql_query("SELECT * FROM community_project WHERE creators_info='community_project|".$row_poo['id']."' AND del_status=0 AND active=0");
								if(mysql_num_rows($sql_art3)>0)
								{
									while($row3_po = mysql_fetch_assoc($sql_art3))
									{
										if($row3_po['community_id']!=$id['community_id'])
										{
											$row3_po['id'] = $row3_po['id'].'~com';
											$row3_po['whos_project'] = "com_creator_creator_pro";
											$com_cp_ao[] = $row3_po;
										}
									}
								}
							//}
						}
					}
				}
			}
		}		
		
		$totals = array();
		$totals = array_merge($art_p_ao,$art_c_ao,$com_p_ao,$com_c_ao,$com_a_ao,$art_a_ao,$art_ap_ao,$com_cp_ao);
		return $totals;
	}
	
	function only_creator2ev_upevent()
	{
		$id = $this->sel_general();
		$date = date("Y-m-d");
		
		$var_type_1 = 'general_artist';
		$var_type_2 = 'general_community';
		$var_type_3 = 'artist_project';
		$var_type_4 = 'community_project';
		
		$art_p_ao = array();
		$art_c_ao = array();
		
		$com_p_ao = array();
		$com_c_ao = array();
		
		$com_a_ao = array();
		$art_a_ao = array();
		
		$art_ap_ao = array();
		$com_cp_ao = array();
		
		$pal_1 = array();
		$pal_2 = array();
		
		if($id['artist_id']!=0 && !empty($id['artist_id']))
		{
			$chk_type = $var_type_1.'|'.$id['artist_id'];
			
			$sql_get_allp = mysql_query("SELECT * FROM artist_project WHERE artist_id='".$id['artist_id']."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_get_allp)>0)
			{
				while($rowal = mysql_fetch_assoc($sql_get_allp))
				{
					//$rowal['whos_project'] = "only_creator";
					$pal_1[] = $rowal;
				}
			}
			
			if(!empty($pal_1) && count($pal_1))
			{
				for($pl=0;$pl<count($pal_1);$pl++)
				{
					$chk_typep_1 = 'artist_project|'.$pal_1[$pl]['id'];
					
					$sql = mysql_query("SELECT * FROM artist_project WHERE (creators_info='".$chk_typep_1."' OR creators_info='".$chk_type."') AND del_status=0 AND active=0");
					if(mysql_num_rows($sql)>0)
					{
						while($row_po = mysql_fetch_assoc($sql))
						{
							//if($row_po['artist_id']!=$id['artist_id'])
							//{
								
								$sql_art2 = mysql_query("SELECT * FROM artist_event WHERE creators_info='artist_project|".$row_po['id']."' AND del_status=0 AND active=0 AND date>='".$date."'");
								if(mysql_num_rows($sql_art2)>0)
								{
									while($row2_po = mysql_fetch_assoc($sql_art2))
									{
										$row2_po['id'] = $row2_po['id'].'~art';
										$row2_po['whos_event'] = "art_creator_creator_eve";
										$art_p_ao[] = $row2_po;
									}
								}
								
								
								$sql_art3 = mysql_query("SELECT * FROM community_event WHERE creators_info='artist_project|".$row_po['id']."' AND del_status=0 AND active=0 AND date>='".$date."'");
								if(mysql_num_rows($sql_art3)>0)
								{
									while($row3_po = mysql_fetch_assoc($sql_art3))
									{
										$row3_po['id'] = $row3_po['id'].'~com';
										$row3_po['whos_event'] = "com_creator_creator_eve";
										$art_c_ao[] = $row3_po;
									}
								}
							//}
						}
					}
					
					
					$sql = mysql_query("SELECT * FROM community_project WHERE (creators_info='".$chk_typep_1."' OR creators_info='".$chk_type."') AND del_status=0 AND active=0");
					if(mysql_num_rows($sql)>0)
					{
						while($row_poo = mysql_fetch_assoc($sql))
						{
							//if($row_poo['community_id']!=$id['community_id'])
							//{		
								
								$sql_art2 = mysql_query("SELECT * FROM artist_event WHERE creators_info='community_project|".$row_poo['id']."' AND del_status=0 AND active=0 AND date>='".$date."'");
								if(mysql_num_rows($sql_art2)>0)
								{
									while($row2_po = mysql_fetch_assoc($sql_art2))
									{
										$row2_po['id'] = $row2_po['id'].'~art';
										$row2_po['whos_event'] = "art_creator_creator_eve";
										$com_p_ao[] = $row2_po;
									}
								}
								
								
								$sql_art3 = mysql_query("SELECT * FROM community_event WHERE creators_info='community_project|".$row_poo['id']."' AND del_status=0 AND active=0 AND date>='".$date."'");
								if(mysql_num_rows($sql_art3)>0)
								{
									while($row3_po = mysql_fetch_assoc($sql_art3))
									{
										$row3_po['id'] = $row3_po['id'].'~com';
										$row3_po['whos_event'] = "com_creator_creator_eve";
										$com_c_ao[] = $row3_po;
									}
								}
							//}
						}
					}
				}
			}
		}
		
		if($id['community_id']!=0 && !empty($id['community_id']))
		{
			$chk_type = $var_type_2.'|'.$id['community_id'];
			
			$sql_get_allp = mysql_query("SELECT * FROM community_project WHERE community_id='".$id['community_id']."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_get_allp)>0)
			{
				while($rowal = mysql_fetch_assoc($sql_get_allp))
				{
					//$rowal['whos_project'] = "only_creator";
					$pal_2[] = $rowal;
				}
			}
			
			if(!empty($pal_2) && count($pal_2))
			{
				for($pl2=0;$pl2<count($pal_2);$pl2++)
				{
					$chk_typep_2 = 'community_project|'.$pal_2[$pl2]['id'];
					
					
					$sql = mysql_query("SELECT * FROM artist_project WHERE (creators_info='".$chk_typep_2."' OR creators_info='".$chk_type."') AND del_status=0 AND active=0");
					if(mysql_num_rows($sql)>0)
					{
						while($row_po = mysql_fetch_assoc($sql))
						{
							//if($row_po['artist_id']!=$id['artist_id'])
							//{
								
								$sql_art2 = mysql_query("SELECT * FROM artist_event WHERE creators_info='artist_project|".$row_po['id']."' AND del_status=0 AND active=0 AND date>='".$date."'");
								if(mysql_num_rows($sql_art2)>0)
								{
									while($row2_po = mysql_fetch_assoc($sql_art2))
									{
										$row2_po['id'] = $row2_po['id'].'~art';
										$row2_po['whos_event'] = "art_creator_creator_eve";
										$com_a_ao[] = $row2_po;
									}
								}
								
								
								$sql_art3 = mysql_query("SELECT * FROM community_event WHERE creators_info='artist_project|".$row_po['id']."' AND del_status=0 AND active=0 AND date>='".$date."'");
								if(mysql_num_rows($sql_art3)>0)
								{
									while($row3_po = mysql_fetch_assoc($sql_art3))
									{
										$row3_po['id'] = $row3_po['id'].'~com';
										$row3_po['whos_event'] = "com_creator_creator_eve";
										$art_a_ao[] = $row3_po;
									}
								}
							//}
						}
					}
					
					
					$sql = mysql_query("SELECT * FROM community_project WHERE (creators_info='".$chk_typep_2."' OR creators_info='".$chk_type."') AND del_status=0 AND active=0");
					if(mysql_num_rows($sql)>0)
					{
						while($row_poo = mysql_fetch_assoc($sql))
						{
							//if($row_poo['community_id']!=$id['community_id'])
							//{
								
								$sql_art2 = mysql_query("SELECT * FROM artist_event WHERE creators_info='community_project|".$row_poo['id']."' AND del_status=0 AND active=0 AND date>='".$date."'");
								if(mysql_num_rows($sql_art2)>0)
								{
									while($row2_po = mysql_fetch_assoc($sql_art2))
									{
										$row2_po['id'] = $row2_po['id'].'~art';
										$row2_po['whos_event'] = "art_creator_creator_eve";
										$art_ap_ao[] = $row2_po;
									}
								}
								
								
								$sql_art3 = mysql_query("SELECT * FROM community_event WHERE creators_info='community_project|".$row_poo['id']."' AND del_status=0 AND active=0 AND date>='".$date."'");
								if(mysql_num_rows($sql_art3)>0)
								{
									while($row3_po = mysql_fetch_assoc($sql_art3))
									{
										$row3_po['id'] = $row3_po['id'].'~com';
										$row3_po['whos_event'] = "com_creator_creator_eve";
										$com_cp_ao[] = $row3_po;
									}
								}
							//}
						}
					}
				}
			}
		}		
		
		$totals = array();
		$totals = array_merge($art_p_ao,$art_c_ao,$com_p_ao,$com_c_ao,$com_a_ao,$art_a_ao,$art_ap_ao,$com_cp_ao);
		return $totals;
	}
	
	function only_creator2ev_sh_upevent()
	{
		$id = $this->sel_general();
		$date = date("Y-m-d");
		
		$var_type_1 = 'general_artist';
		$var_type_2 = 'general_community';
		$var_type_3 = 'artist_project';
		$var_type_4 = 'community_project';
		
		$art_p_ao = array();
		$art_c_ao = array();
		
		$com_p_ao = array();
		$com_c_ao = array();
		
		$com_a_ao = array();
		$art_a_ao = array();
		
		$art_ap_ao = array();
		$com_cp_ao = array();
		
		$pal_1 = array();
		$pal_2 = array();
		
		if($id['artist_id']!=0 && !empty($id['artist_id']))
		{
			$chk_type = $var_type_1.'|'.$id['artist_id'];
			
			$sql_get_allp = mysql_query("SELECT * FROM artist_project WHERE artist_id='".$id['artist_id']."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_get_allp)>0)
			{
				while($rowal = mysql_fetch_assoc($sql_get_allp))
				{
					//$rowal['whos_project'] = "only_creator";
					$pal_1[] = $rowal;
				}
			}
			
			if(!empty($pal_1) && count($pal_1))
			{
				for($pl=0;$pl<count($pal_1);$pl++)
				{
					$chk_typep_1 = 'artist_project|'.$pal_1[$pl]['id'];
					
					$sql = mysql_query("SELECT * FROM artist_project WHERE (creators_info='".$chk_typep_1."' OR creators_info='".$chk_type."') AND del_status=0 AND active=0");
					if(mysql_num_rows($sql)>0)
					{
						while($row_po = mysql_fetch_assoc($sql))
						{
							//if($row_po['artist_id']!=$id['artist_id'])
							//{
								
								$sql_art2 = mysql_query("SELECT * FROM artist_event WHERE creators_info='artist_project|".$row_po['id']."' AND del_status=0 AND active=0 AND date>='".$date."'");
								if(mysql_num_rows($sql_art2)>0)
								{
									while($row2_po = mysql_fetch_assoc($sql_art2))
									{
										$row2_po['table_name'] = 'artist_event';
										$sql = mysql_query("SELECT MIN( `id` ) FROM `artist_event_profiles` WHERE artist_event_id='".$row2_po['id']."' AND subtype_id!=0");
							
										if(mysql_num_rows($sql)>0)
										{
											$res = mysql_fetch_assoc($sql);
											
											$sql2 = mysql_query("select * from artist_event_profiles where id='".$res['MIN( `id` )']."'");
											$res2 = mysql_fetch_assoc($sql2);

											$sql4 = mysql_query("select * from  subtype where subtype_id = '".$res2['subtype_id']."'");
											if(mysql_num_rows($sql4)>0)
											{
												$res4 = mysql_fetch_assoc($sql4);
												$row2_po['type'] = $res4['name'];
											}
											else
											{
												$row2_po['type'] = '';
											}
										}
										else
										{
											$row2_po['type'] = '';
										}
										$art_p_ao[] = $row2_po;
									}
								}
								
								
								$sql_art3 = mysql_query("SELECT * FROM community_event WHERE creators_info='artist_project|".$row_po['id']."' AND del_status=0 AND active=0 AND date>='".$date."'");
								if(mysql_num_rows($sql_art3)>0)
								{
									while($row3_po = mysql_fetch_assoc($sql_art3))
									{
										$row3_po['table_name'] = 'community_event';
										$sql = mysql_query("SELECT MIN( `id` ) FROM `community_event_profiles` WHERE community_event_id='".$row3_po['id']."' AND subtype_id!=0");
							
										if(mysql_num_rows($sql)>0)
										{
											$res = mysql_fetch_assoc($sql);
											
											$sql2 = mysql_query("select * from community_event_profiles where id='".$res['MIN( `id` )']."'");
											$res2 = mysql_fetch_assoc($sql2);

											$sql4 = mysql_query("select * from  subtype where subtype_id = '".$res2['subtype_id']."'");
											if(mysql_num_rows($sql4)>0)
											{
												$res4 = mysql_fetch_assoc($sql4);
												$row3_po['type'] = $res4['name'];
											}
											else
											{
												$row3_po['type'] = '';
											}
										}
										else
										{
											$row3_po['type'] = '';
										}
										$art_c_ao[] = $row3_po;
									}
								}
							//}
						}
					}
					
					
					$sql = mysql_query("SELECT * FROM community_project WHERE (creators_info='".$chk_typep_1."' OR creators_info='".$chk_type."') AND del_status=0 AND active=0");
					if(mysql_num_rows($sql)>0)
					{
						while($row_poo = mysql_fetch_assoc($sql))
						{
							//if($row_poo['community_id']!=$id['community_id'])
							//{		
								
								$sql_art2 = mysql_query("SELECT * FROM artist_event WHERE creators_info='community_project|".$row_poo['id']."' AND del_status=0 AND active=0 AND date>='".$date."'");
								if(mysql_num_rows($sql_art2)>0)
								{
									while($row2_po = mysql_fetch_assoc($sql_art2))
									{
										$row2_po['table_name'] = 'artist_event';
										$sql = mysql_query("SELECT MIN( `id` ) FROM `artist_event_profiles` WHERE artist_event_id='".$row2_po['id']."' AND subtype_id!=0");
							
										if(mysql_num_rows($sql)>0)
										{
											$res = mysql_fetch_assoc($sql);
											
											$sql2 = mysql_query("select * from artist_event_profiles where id='".$res['MIN( `id` )']."'");
											$res2 = mysql_fetch_assoc($sql2);

											$sql4 = mysql_query("select * from  subtype where subtype_id = '".$res2['subtype_id']."'");
											if(mysql_num_rows($sql4)>0)
											{
												$res4 = mysql_fetch_assoc($sql4);
												$row2_po['type'] = $res4['name'];
											}
											else
											{
												$row2_po['type'] = '';
											}
										}
										else
										{
											$row2_po['type'] = '';
										}
										$com_p_ao[] = $row2_po;
									}
								}
								
								
								$sql_art3 = mysql_query("SELECT * FROM community_event WHERE creators_info='community_project|".$row_poo['id']."' AND del_status=0 AND active=0 AND date>='".$date."'");
								if(mysql_num_rows($sql_art3)>0)
								{
									while($row3_po = mysql_fetch_assoc($sql_art3))
									{
										$row3_po['table_name'] = 'community_event';
										$sql = mysql_query("SELECT MIN( `id` ) FROM `community_event_profiles` WHERE community_event_id='".$row3_po['id']."' AND subtype_id!=0");
							
										if(mysql_num_rows($sql)>0)
										{
											$res = mysql_fetch_assoc($sql);
											
											$sql2 = mysql_query("select * from community_event_profiles where id='".$res['MIN( `id` )']."'");
											$res2 = mysql_fetch_assoc($sql2);

											$sql4 = mysql_query("select * from  subtype where subtype_id = '".$res2['subtype_id']."'");
											if(mysql_num_rows($sql4)>0)
											{
												$res4 = mysql_fetch_assoc($sql4);
												$row3_po['type'] = $res4['name'];
											}
											else
											{
												$row3_po['type'] = '';
											}
										}
										else
										{
											$row3_po['type'] = '';
										}
										$com_c_ao[] = $row3_po;
									}
								}
							//}
						}
					}
				}
			}
		}
		
		if($id['community_id']!=0 && !empty($id['community_id']))
		{
			$chk_type = $var_type_2.'|'.$id['community_id'];
			
			$sql_get_allp = mysql_query("SELECT * FROM community_project WHERE community_id='".$id['community_id']."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_get_allp)>0)
			{
				while($rowal = mysql_fetch_assoc($sql_get_allp))
				{
					//$rowal['whos_project'] = "only_creator";
					$pal_2[] = $rowal;
				}
			}
			
			if(!empty($pal_2) && count($pal_2))
			{
				for($pl2=0;$pl2<count($pal_2);$pl2++)
				{
					$chk_typep_2 = 'community_project|'.$pal_2[$pl2]['id'];
					
					
					$sql = mysql_query("SELECT * FROM artist_project WHERE (creators_info='".$chk_typep_2."' OR creators_info='".$chk_type."') AND del_status=0 AND active=0");
					if(mysql_num_rows($sql)>0)
					{
						while($row_po = mysql_fetch_assoc($sql))
						{
							//if($row_po['artist_id']!=$id['artist_id'])
							//{
								
								$sql_art2 = mysql_query("SELECT * FROM artist_event WHERE creators_info='artist_project|".$row_po['id']."' AND del_status=0 AND active=0 AND date>='".$date."'");
								if(mysql_num_rows($sql_art2)>0)
								{
									while($row2_po = mysql_fetch_assoc($sql_art2))
									{
										$row2_po['table_name'] = 'artist_event';
										$sql = mysql_query("SELECT MIN( `id` ) FROM `artist_event_profiles` WHERE artist_event_id='".$row2_po['id']."' AND subtype_id!=0");
							
										if(mysql_num_rows($sql)>0)
										{
											$res = mysql_fetch_assoc($sql);
											
											$sql2 = mysql_query("select * from artist_event_profiles where id='".$res['MIN( `id` )']."'");
											$res2 = mysql_fetch_assoc($sql2);

											$sql4 = mysql_query("select * from  subtype where subtype_id = '".$res2['subtype_id']."'");
											if(mysql_num_rows($sql4)>0)
											{
												$res4 = mysql_fetch_assoc($sql4);
												$row2_po['type'] = $res4['name'];
											}
											else
											{
												$row2_po['type'] = '';
											}
										}
										else
										{
											$row2_po['type'] = '';
										}
										$com_a_ao[] = $row2_po;
									}
								}
								
								
								$sql_art3 = mysql_query("SELECT * FROM community_event WHERE creators_info='artist_project|".$row_po['id']."' AND del_status=0 AND active=0 AND date>='".$date."'");
								if(mysql_num_rows($sql_art3)>0)
								{
									while($row3_po = mysql_fetch_assoc($sql_art3))
									{
										$row3_po['table_name'] = 'community_event';
										$sql = mysql_query("SELECT MIN( `id` ) FROM `community_event_profiles` WHERE community_event_id='".$row3_po['id']."' AND subtype_id!=0");
							
										if(mysql_num_rows($sql)>0)
										{
											$res = mysql_fetch_assoc($sql);
											
											$sql2 = mysql_query("select * from community_event_profiles where id='".$res['MIN( `id` )']."'");
											$res2 = mysql_fetch_assoc($sql2);

											$sql4 = mysql_query("select * from  subtype where subtype_id = '".$res2['subtype_id']."'");
											if(mysql_num_rows($sql4)>0)
											{
												$res4 = mysql_fetch_assoc($sql4);
												$row3_po['type'] = $res4['name'];
											}
											else
											{
												$row3_po['type'] = '';
											}
										}
										else
										{
											$row3_po['type'] = '';
										}
										$art_a_ao[] = $row3_po;
									}
								}
							//}
						}
					}
					
					
					$sql = mysql_query("SELECT * FROM community_project WHERE (creators_info='".$chk_typep_2."' OR creators_info='".$chk_type."') AND del_status=0 AND active=0");
					if(mysql_num_rows($sql)>0)
					{
						while($row_poo = mysql_fetch_assoc($sql))
						{
							//if($row_poo['community_id']!=$id['community_id'])
							//{
								
								$sql_art2 = mysql_query("SELECT * FROM artist_event WHERE creators_info='community_project|".$row_poo['id']."' AND del_status=0 AND active=0 AND date>='".$date."'");
								if(mysql_num_rows($sql_art2)>0)
								{
									while($row2_po = mysql_fetch_assoc($sql_art2))
									{
										$row2_po['table_name'] = 'artist_event';
										$sql = mysql_query("SELECT MIN( `id` ) FROM `artist_event_profiles` WHERE artist_event_id='".$row2_po['id']."' AND subtype_id!=0");
							
										if(mysql_num_rows($sql)>0)
										{
											$res = mysql_fetch_assoc($sql);
											
											$sql2 = mysql_query("select * from artist_event_profiles where id='".$res['MIN( `id` )']."'");
											$res2 = mysql_fetch_assoc($sql2);

											$sql4 = mysql_query("select * from  subtype where subtype_id = '".$res2['subtype_id']."'");
											if(mysql_num_rows($sql4)>0)
											{
												$res4 = mysql_fetch_assoc($sql4);
												$row2_po['type'] = $res4['name'];
											}
											else
											{
												$row2_po['type'] = '';
											}
										}
										else
										{
											$row2_po['type'] = '';
										}
										$art_ap_ao[] = $row2_po;
									}
								}
								
								
								$sql_art3 = mysql_query("SELECT * FROM community_event WHERE creators_info='community_project|".$row_poo['id']."' AND del_status=0 AND active=0 AND date>='".$date."'");
								if(mysql_num_rows($sql_art3)>0)
								{
									while($row3_po = mysql_fetch_assoc($sql_art3))
									{
										$row3_po['table_name'] = 'community_event';
										$sql = mysql_query("SELECT MIN( `id` ) FROM `community_event_profiles` WHERE community_event_id='".$row3_po['id']."' AND subtype_id!=0");
							
										if(mysql_num_rows($sql)>0)
										{
											$res = mysql_fetch_assoc($sql);
											
											$sql2 = mysql_query("select * from community_event_profiles where id='".$res['MIN( `id` )']."'");
											$res2 = mysql_fetch_assoc($sql2);

											$sql4 = mysql_query("select * from  subtype where subtype_id = '".$res2['subtype_id']."'");
											if(mysql_num_rows($sql4)>0)
											{
												$res4 = mysql_fetch_assoc($sql4);
												$row3_po['type'] = $res4['name'];
											}
											else
											{
												$row3_po['type'] = '';
											}
										}
										else
										{
											$row3_po['type'] = '';
										}
										$com_cp_ao[] = $row3_po;
									}
								}
							//}
						}
					}
				}
			}
		}		
		
		$totals = array();
		$totals = array_merge($art_p_ao,$art_c_ao,$com_p_ao,$com_c_ao,$com_a_ao,$art_a_ao,$art_ap_ao,$com_cp_ao);
		return $totals;
	}
	
	function only_creator2ev_oupevent()
	{
		$id = $this->sel_general();
		$date = date("Y-m-d");
		
		$var_type_1 = 'general_artist';
		$var_type_2 = 'general_community';
		$var_type_3 = 'artist_project';
		$var_type_4 = 'community_project';
		
		$art_p_ao = array();
		$art_c_ao = array();
		
		$com_p_ao = array();
		$com_c_ao = array();
		
		$com_a_ao = array();
		$art_a_ao = array();
		
		$art_ap_ao = array();
		$com_cp_ao = array();
		
		$pal_1 = array();
		$pal_2 = array();
		
		if($id['artist_id']!=0 && !empty($id['artist_id']))
		{
			$chk_type = $var_type_1.'|'.$id['artist_id'];
			
			$sql_get_allp = mysql_query("SELECT * FROM artist_project WHERE artist_id='".$id['artist_id']."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_get_allp)>0)
			{
				while($rowal = mysql_fetch_assoc($sql_get_allp))
				{
					//$rowal['whos_project'] = "only_creator";
					$pal_1[] = $rowal;
				}
			}
			
			if(!empty($pal_1) && count($pal_1))
			{
				for($pl=0;$pl<count($pal_1);$pl++)
				{
					$chk_typep_1 = 'artist_project|'.$pal_1[$pl]['id'];
					
					$sql = mysql_query("SELECT * FROM artist_project WHERE (creators_info='".$chk_typep_1."' OR creators_info='".$chk_type."') AND del_status=0 AND active=0");
					if(mysql_num_rows($sql)>0)
					{
						while($row_po = mysql_fetch_assoc($sql))
						{
							//if($row_po['artist_id']!=$id['artist_id'])
							//{
								
								$sql_art2 = mysql_query("SELECT * FROM artist_event WHERE creators_info='artist_project|".$row_po['id']."' AND del_status=0 AND active=0 AND date>='".$date."'");
								if(mysql_num_rows($sql_art2)>0)
								{
									while($row2_po = mysql_fetch_assoc($sql_art2))
									{
										if($row2_po['artist_id']!=$id['artist_id'])
										{
											$row2_po['id'] = $row2_po['id'].'~art';
											$row2_po['whos_event'] = "art_creator_creator_eve";
											$art_p_ao[] = $row2_po;
										}
									}
								}
								
								
								$sql_art3 = mysql_query("SELECT * FROM community_event WHERE creators_info='artist_project|".$row_po['id']."' AND del_status=0 AND active=0 AND date>='".$date."'");
								if(mysql_num_rows($sql_art3)>0)
								{
									while($row3_po = mysql_fetch_assoc($sql_art3))
									{
										if($row3_po['community_id']!=$id['community_id'])
										{
											$row3_po['id'] = $row3_po['id'].'~com';
											$row3_po['whos_event'] = "com_creator_creator_eve";
											$art_c_ao[] = $row3_po;
										}
									}
								}
							//}
						}
					}
					
					
					$sql = mysql_query("SELECT * FROM community_project WHERE (creators_info='".$chk_typep_1."' OR creators_info='".$chk_type."') AND del_status=0 AND active=0");
					if(mysql_num_rows($sql)>0)
					{
						while($row_poo = mysql_fetch_assoc($sql))
						{
							//if($row_poo['community_id']!=$id['community_id'])
							//{		
								
								$sql_art2 = mysql_query("SELECT * FROM artist_event WHERE creators_info='community_project|".$row_poo['id']."' AND del_status=0 AND active=0 AND date>='".$date."'");
								if(mysql_num_rows($sql_art2)>0)
								{
									while($row2_po = mysql_fetch_assoc($sql_art2))
									{
										if($row2_po['artist_id']!=$id['artist_id'])
										{
											$row2_po['id'] = $row2_po['id'].'~art';
											$row2_po['whos_event'] = "art_creator_creator_eve";
											$com_p_ao[] = $row2_po;
										}
									}
								}
								
								
								$sql_art3 = mysql_query("SELECT * FROM community_event WHERE creators_info='community_project|".$row_poo['id']."' AND del_status=0 AND active=0 AND date>='".$date."'");
								if(mysql_num_rows($sql_art3)>0)
								{
									while($row3_po = mysql_fetch_assoc($sql_art3))
									{
										if($row3_po['community_id']!=$id['community_id'])
										{
											$row3_po['id'] = $row3_po['id'].'~com';
											$row3_po['whos_event'] = "com_creator_creator_eve";
											$com_c_ao[] = $row3_po;
										}
									}
								}
							//}
						}
					}
				}
			}
		}
		
		if($id['community_id']!=0 && !empty($id['community_id']))
		{
			$chk_type = $var_type_2.'|'.$id['community_id'];
			
			$sql_get_allp = mysql_query("SELECT * FROM community_project WHERE community_id='".$id['community_id']."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_get_allp)>0)
			{
				while($rowal = mysql_fetch_assoc($sql_get_allp))
				{
					//$rowal['whos_project'] = "only_creator";
					$pal_2[] = $rowal;
				}
			}
			
			if(!empty($pal_2) && count($pal_2))
			{
				for($pl2=0;$pl2<count($pal_2);$pl2++)
				{
					$chk_typep_2 = 'community_project|'.$pal_2[$pl2]['id'];
					
					
					$sql = mysql_query("SELECT * FROM artist_project WHERE (creators_info='".$chk_typep_2."' OR creators_info='".$chk_type."') AND del_status=0 AND active=0");
					if(mysql_num_rows($sql)>0)
					{
						while($row_po = mysql_fetch_assoc($sql))
						{
							//if($row_po['artist_id']!=$id['artist_id'])
							//{
								
								$sql_art2 = mysql_query("SELECT * FROM artist_event WHERE creators_info='artist_project|".$row_po['id']."' AND del_status=0 AND active=0 AND date>='".$date."'");
								if(mysql_num_rows($sql_art2)>0)
								{
									while($row2_po = mysql_fetch_assoc($sql_art2))
									{
										if($row2_po['artist_id']!=$id['artist_id'])
										{
											$row2_po['id'] = $row2_po['id'].'~art';
											$row2_po['whos_event'] = "art_creator_creator_eve";
											$com_a_ao[] = $row2_po;
										}
									}
								}
								
								
								$sql_art3 = mysql_query("SELECT * FROM community_event WHERE creators_info='artist_project|".$row_po['id']."' AND del_status=0 AND active=0 AND date>='".$date."'");
								if(mysql_num_rows($sql_art3)>0)
								{
									while($row3_po = mysql_fetch_assoc($sql_art3))
									{
										if($row3_po['community_id']!=$id['community_id'])
										{
											$row3_po['id'] = $row3_po['id'].'~com';
											$row3_po['whos_event'] = "com_creator_creator_eve";
											$art_a_ao[] = $row3_po;
										}
									}
								}
							//}
						}
					}
					
					
					$sql = mysql_query("SELECT * FROM community_project WHERE (creators_info='".$chk_typep_2."' OR creators_info='".$chk_type."') AND del_status=0 AND active=0");
					if(mysql_num_rows($sql)>0)
					{
						while($row_poo = mysql_fetch_assoc($sql))
						{
							//if($row_poo['community_id']!=$id['community_id'])
							//{
								
								$sql_art2 = mysql_query("SELECT * FROM artist_event WHERE creators_info='community_project|".$row_poo['id']."' AND del_status=0 AND active=0 AND date>='".$date."'");
								if(mysql_num_rows($sql_art2)>0)
								{
									while($row2_po = mysql_fetch_assoc($sql_art2))
									{
										if($row2_po['artist_id']!=$id['artist_id'])
										{
											$row2_po['id'] = $row2_po['id'].'~art';
											$row2_po['whos_event'] = "art_creator_creator_eve";
											$art_ap_ao[] = $row2_po;
										}
									}
								}
								
								
								$sql_art3 = mysql_query("SELECT * FROM community_event WHERE creators_info='community_project|".$row_poo['id']."' AND del_status=0 AND active=0 AND date>='".$date."'");
								if(mysql_num_rows($sql_art3)>0)
								{
									while($row3_po = mysql_fetch_assoc($sql_art3))
									{
										if($row3_po['community_id']!=$id['community_id'])
										{
											$row3_po['id'] = $row3_po['id'].'~com';
											$row3_po['whos_event'] = "com_creator_creator_eve";
											$com_cp_ao[] = $row3_po;
										}
									}
								}
							//}
						}
					}
				}
			}
		}		
		
		$totals = array();
		$totals = array_merge($art_p_ao,$art_c_ao,$com_p_ao,$com_c_ao,$com_a_ao,$art_a_ao,$art_ap_ao,$com_cp_ao);
		return $totals;
	}
	
	function only_creator2_recevent()
	{
		$id = $this->sel_general();
		$date = date("Y-m-d");
		
		$var_type_1 = 'general_artist';
		$var_type_2 = 'general_community';
		$var_type_3 = 'artist_project';
		$var_type_4 = 'community_project';
		
		$art_p_ao = array();
		$art_c_ao = array();
		
		$com_p_ao = array();
		$com_c_ao = array();
		
		$com_a_ao = array();
		$art_a_ao = array();
		
		$art_ap_ao = array();
		$com_cp_ao = array();
		
		$pal_1 = array();
		$pal_2 = array();
		
		if($id['artist_id']!=0 && !empty($id['artist_id']))
		{
			$chk_type = $var_type_1.'|'.$id['artist_id'];
			
			$sql_get_allp = mysql_query("SELECT * FROM artist_project WHERE artist_id='".$id['artist_id']."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_get_allp)>0)
			{
				while($rowal = mysql_fetch_assoc($sql_get_allp))
				{
					$rowal['whos_event'] = "art_creator_creator_eve";
					$pal_1[] = $rowal;
				}
			}
			
			if(!empty($pal_1) && count($pal_1))
			{
				for($pl=0;$pl<count($pal_1);$pl++)
				{
					$chk_typep_1 = 'artist_project|'.$pal_1[$pl]['id'];
					
					$sql = mysql_query("SELECT * FROM artist_project WHERE (creators_info='".$chk_typep_1."' OR creators_info='".$chk_type."') AND del_status=0 AND active=0");
					if(mysql_num_rows($sql)>0)
					{
						while($row_po = mysql_fetch_assoc($sql))
						{
							//if($row_po['artist_id']!=$id['artist_id'])
							//{
								
								$sql_art2 = mysql_query("SELECT * FROM artist_event WHERE creators_info='artist_project|".$row_po['id']."' AND del_status=0 AND active=0 AND date<'".$date."'");
								if(mysql_num_rows($sql_art2)>0)
								{
									while($row2_po = mysql_fetch_assoc($sql_art2))
									{
										$row2_po['id'] = $row2_po['id'].'~art';
										$row2_po['whos_event'] = "art_creator_creator_eve";
										$art_p_ao[] = $row2_po;
									}
								}
								
								
								$sql_art3 = mysql_query("SELECT * FROM community_event WHERE creators_info='artist_project|".$row_po['id']."' AND del_status=0 AND active=0 AND date<'".$date."'");
								if(mysql_num_rows($sql_art3)>0)
								{
									while($row3_po = mysql_fetch_assoc($sql_art3))
									{
										$row3_po['id'] = $row3_po['id'].'~com';
										$row3_po['whos_event'] = "com_creator_creator_eve";
										$art_c_ao[] = $row3_po;
									}
								}
							//}
						}
					}
					
					
					$sql = mysql_query("SELECT * FROM community_project WHERE (creators_info='".$chk_typep_1."' OR creators_info='".$chk_type."') AND del_status=0 AND active=0");
					if(mysql_num_rows($sql)>0)
					{
						while($row_poo = mysql_fetch_assoc($sql))
						{
							//if($row_poo['community_id']!=$id['community_id'])
							//{		
								
								$sql_art2 = mysql_query("SELECT * FROM artist_event WHERE creators_info='community_project|".$row_poo['id']."' AND del_status=0 AND active=0 AND date<'".$date."'");
								if(mysql_num_rows($sql_art2)>0)
								{
									while($row2_po = mysql_fetch_assoc($sql_art2))
									{
										$row2_po['id'] = $row2_po['id'].'~art';
										$row2_po['whos_event'] = "art_creator_creator_eve";
										$com_p_ao[] = $row2_po;
									}
								}
								
								
								$sql_art3 = mysql_query("SELECT * FROM community_event WHERE creators_info='community_project|".$row_poo['id']."' AND del_status=0 AND active=0 AND date<'".$date."'");
								if(mysql_num_rows($sql_art3)>0)
								{
									while($row3_po = mysql_fetch_assoc($sql_art3))
									{
										$row3_po['id'] = $row3_po['id'].'~com';
										$row3_po['whos_event'] = "com_creator_creator_eve";
										$com_c_ao[] = $row3_po;
									}
								}
							//}
						}
					}
				}
			}
		}
		
		if($id['community_id']!=0 && !empty($id['community_id']))
		{
			$chk_type = $var_type_2.'|'.$id['community_id'];
			
			$sql_get_allp = mysql_query("SELECT * FROM community_project WHERE community_id='".$id['community_id']."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_get_allp)>0)
			{
				while($rowal = mysql_fetch_assoc($sql_get_allp))
				{
					//$rowal['whos_project'] = "only_creator";
					$pal_2[] = $rowal;
				}
			}
			
			if(!empty($pal_2) && count($pal_2))
			{
				for($pl2=0;$pl2<count($pal_2);$pl2++)
				{
					$chk_typep_2 = 'community_project|'.$pal_2[$pl2]['id'];
					
					
					$sql = mysql_query("SELECT * FROM artist_project WHERE (creators_info='".$chk_typep_2."' OR creators_info='".$chk_type."') AND del_status=0 AND active=0");
					if(mysql_num_rows($sql)>0)
					{
						while($row_po = mysql_fetch_assoc($sql))
						{
							//if($row_po['artist_id']!=$id['artist_id'])
							//{
								
								$sql_art2 = mysql_query("SELECT * FROM artist_event WHERE creators_info='artist_project|".$row_po['id']."' AND del_status=0 AND active=0 AND date<'".$date."'");
								if(mysql_num_rows($sql_art2)>0)
								{
									while($row2_po = mysql_fetch_assoc($sql_art2))
									{
										$row2_po['id'] = $row2_po['id'].'~art';
										$row2_po['whos_event'] = "art_creator_creator_eve";
										$com_a_ao[] = $row2_po;
									}
								}
								
								
								$sql_art3 = mysql_query("SELECT * FROM community_event WHERE creators_info='artist_project|".$row_po['id']."' AND del_status=0 AND active=0 AND date<'".$date."'");
								if(mysql_num_rows($sql_art3)>0)
								{
									while($row3_po = mysql_fetch_assoc($sql_art3))
									{
										$row3_po['id'] = $row3_po['id'].'~com';
										$row3_po['whos_event'] = "com_creator_creator_eve";
										$art_a_ao[] = $row3_po;
									}
								}
							//}
						}
					}
					
					
					$sql = mysql_query("SELECT * FROM community_project WHERE (creators_info='".$chk_typep_2."' OR creators_info='".$chk_type."') AND del_status=0 AND active=0");
					if(mysql_num_rows($sql)>0)
					{
						while($row_poo = mysql_fetch_assoc($sql))
						{
							//if($row_poo['community_id']!=$id['community_id'])
							//{
								
								$sql_art2 = mysql_query("SELECT * FROM artist_event WHERE creators_info='community_project|".$row_poo['id']."' AND del_status=0 AND active=0 AND date<'".$date."'");
								if(mysql_num_rows($sql_art2)>0)
								{
									while($row2_po = mysql_fetch_assoc($sql_art2))
									{
										$row2_po['id'] = $row2_po['id'].'~art';
										$row2_po['whos_event'] = "art_creator_creator_eve";
										$art_ap_ao[] = $row2_po;
									}
								}
								
								
								$sql_art3 = mysql_query("SELECT * FROM community_event WHERE creators_info='community_project|".$row_poo['id']."' AND del_status=0 AND active=0 AND date<'".$date."'");
								if(mysql_num_rows($sql_art3)>0)
								{
									while($row3_po = mysql_fetch_assoc($sql_art3))
									{
										$row3_po['id'] = $row3_po['id'].'~com';
										$row3_po['whos_event'] = "com_creator_creator_eve";
										$com_cp_ao[] = $row3_po;
									}
								}
							//}
						}
					}
				}
			}
		}		
		
		$totals = array();
		$totals = array_merge($art_p_ao,$art_c_ao,$com_p_ao,$com_c_ao,$com_a_ao,$art_a_ao,$art_ap_ao,$com_cp_ao);
		return $totals;
	}
	
	function only_sh_creator2_recevent()
	{
		$id = $this->sel_general();
		$date = date("Y-m-d");
		
		$var_type_1 = 'general_artist';
		$var_type_2 = 'general_community';
		$var_type_3 = 'artist_project';
		$var_type_4 = 'community_project';
		
		$art_p_ao = array();
		$art_c_ao = array();
		
		$com_p_ao = array();
		$com_c_ao = array();
		
		$com_a_ao = array();
		$art_a_ao = array();
		
		$art_ap_ao = array();
		$com_cp_ao = array();
		
		$pal_1 = array();
		$pal_2 = array();
		
		if($id['artist_id']!=0 && !empty($id['artist_id']))
		{
			$chk_type = $var_type_1.'|'.$id['artist_id'];
			
			$sql_get_allp = mysql_query("SELECT * FROM artist_project WHERE artist_id='".$id['artist_id']."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_get_allp)>0)
			{
				while($rowal = mysql_fetch_assoc($sql_get_allp))
				{
					$rowal['whos_event'] = "art_creator_creator_eve";
					$pal_1[] = $rowal;
				}
			}
			
			if(!empty($pal_1) && count($pal_1))
			{
				for($pl=0;$pl<count($pal_1);$pl++)
				{
					$chk_typep_1 = 'artist_project|'.$pal_1[$pl]['id'];
					
					$sql = mysql_query("SELECT * FROM artist_project WHERE (creators_info='".$chk_typep_1."' OR creators_info='".$chk_type."') AND del_status=0 AND active=0");
					if(mysql_num_rows($sql)>0)
					{
						while($row_po = mysql_fetch_assoc($sql))
						{
							//if($row_po['artist_id']!=$id['artist_id'])
							//{
								
								$sql_art2 = mysql_query("SELECT * FROM artist_event WHERE creators_info='artist_project|".$row_po['id']."' AND del_status=0 AND active=0 AND date<'".$date."'");
								if(mysql_num_rows($sql_art2)>0)
								{
									while($row2_po = mysql_fetch_assoc($sql_art2))
									{
										$row2_po['table_name'] = 'artist_event';
										$sql = mysql_query("SELECT MIN( `id` ) FROM `artist_event_profiles` WHERE artist_event_id='".$row2_po['id']."' AND subtype_id!=0");
							
										if(mysql_num_rows($sql)>0)
										{
											$res = mysql_fetch_assoc($sql);
											
											$sql2 = mysql_query("select * from artist_event_profiles where id='".$res['MIN( `id` )']."'");
											$res2 = mysql_fetch_assoc($sql2);

											$sql4 = mysql_query("select * from  subtype where subtype_id = '".$res2['subtype_id']."'");
											if(mysql_num_rows($sql4)>0)
											{
												$res4 = mysql_fetch_assoc($sql4);
												$row2_po['type'] = $res4['name'];
											}
											else
											{
												$row2_po['type'] = '';
											}
										}
										else
										{
											$row2_po['type'] = '';
										}
										$art_p_ao[] = $row2_po;
									}
								}
								
								
								$sql_art3 = mysql_query("SELECT * FROM community_event WHERE creators_info='artist_project|".$row_po['id']."' AND del_status=0 AND active=0 AND date<'".$date."'");
								if(mysql_num_rows($sql_art3)>0)
								{
									while($row3_po = mysql_fetch_assoc($sql_art3))
									{
										$row3_po['table_name'] = 'community_event';
										$sql = mysql_query("SELECT MIN( `id` ) FROM `community_event_profiles` WHERE community_event_id='".$row3_po['id']."' AND subtype_id!=0");
							
										if(mysql_num_rows($sql)>0)
										{
											$res = mysql_fetch_assoc($sql);
											
											$sql2 = mysql_query("select * from community_event_profiles where id='".$res['MIN( `id` )']."'");
											$res2 = mysql_fetch_assoc($sql2);

											$sql4 = mysql_query("select * from  subtype where subtype_id = '".$res2['subtype_id']."'");
											if(mysql_num_rows($sql4)>0)
											{
												$res4 = mysql_fetch_assoc($sql4);
												$row3_po['type'] = $res4['name'];
											}
											else
											{
												$row3_po['type'] = '';
											}
										}
										else
										{
											$row3_po['type'] = '';
										}
										$art_c_ao[] = $row3_po;
									}
								}
							//}
						}
					}
					
					
					$sql = mysql_query("SELECT * FROM community_project WHERE (creators_info='".$chk_typep_1."' OR creators_info='".$chk_type."') AND del_status=0 AND active=0");
					if(mysql_num_rows($sql)>0)
					{
						while($row_poo = mysql_fetch_assoc($sql))
						{
							//if($row_poo['community_id']!=$id['community_id'])
							//{		
								
								$sql_art2 = mysql_query("SELECT * FROM artist_event WHERE creators_info='community_project|".$row_poo['id']."' AND del_status=0 AND active=0 AND date<'".$date."'");
								if(mysql_num_rows($sql_art2)>0)
								{
									while($row2_po = mysql_fetch_assoc($sql_art2))
									{
										$row2_po['table_name'] = 'artist_event';
										$sql = mysql_query("SELECT MIN( `id` ) FROM `artist_event_profiles` WHERE artist_event_id='".$row2_po['id']."' AND subtype_id!=0");
							
										if(mysql_num_rows($sql)>0)
										{
											$res = mysql_fetch_assoc($sql);
											
											$sql2 = mysql_query("select * from artist_event_profiles where id='".$res['MIN( `id` )']."'");
											$res2 = mysql_fetch_assoc($sql2);

											$sql4 = mysql_query("select * from  subtype where subtype_id = '".$res2['subtype_id']."'");
											if(mysql_num_rows($sql4)>0)
											{
												$res4 = mysql_fetch_assoc($sql4);
												$row2_po['type'] = $res4['name'];
											}
											else
											{
												$row2_po['type'] = '';
											}
										}
										else
										{
											$row2_po['type'] = '';
										}
										$com_p_ao[] = $row2_po;
									}
								}
								
								
								$sql_art3 = mysql_query("SELECT * FROM community_event WHERE creators_info='community_project|".$row_poo['id']."' AND del_status=0 AND active=0 AND date<'".$date."'");
								if(mysql_num_rows($sql_art3)>0)
								{
									while($row3_po = mysql_fetch_assoc($sql_art3))
									{
										$row3_po['table_name'] = 'community_event';
										$sql = mysql_query("SELECT MIN( `id` ) FROM `community_event_profiles` WHERE community_event_id='".$row3_po['id']."' AND subtype_id!=0");
							
										if(mysql_num_rows($sql)>0)
										{
											$res = mysql_fetch_assoc($sql);
											
											$sql2 = mysql_query("select * from community_event_profiles where id='".$res['MIN( `id` )']."'");
											$res2 = mysql_fetch_assoc($sql2);

											$sql4 = mysql_query("select * from  subtype where subtype_id = '".$res2['subtype_id']."'");
											if(mysql_num_rows($sql4)>0)
											{
												$res4 = mysql_fetch_assoc($sql4);
												$row3_po['type'] = $res4['name'];
											}
											else
											{
												$row3_po['type'] = '';
											}
										}
										else
										{
											$row3_po['type'] = '';
										}
										$com_c_ao[] = $row3_po;
									}
								}
							//}
						}
					}
				}
			}
		}
		
		if($id['community_id']!=0 && !empty($id['community_id']))
		{
			$chk_type = $var_type_2.'|'.$id['community_id'];
			
			$sql_get_allp = mysql_query("SELECT * FROM community_project WHERE community_id='".$id['community_id']."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_get_allp)>0)
			{
				while($rowal = mysql_fetch_assoc($sql_get_allp))
				{
					//$rowal['whos_project'] = "only_creator";
					$pal_2[] = $rowal;
				}
			}
			
			if(!empty($pal_2) && count($pal_2))
			{
				for($pl2=0;$pl2<count($pal_2);$pl2++)
				{
					$chk_typep_2 = 'community_project|'.$pal_2[$pl2]['id'];
					
					
					$sql = mysql_query("SELECT * FROM artist_project WHERE (creators_info='".$chk_typep_2."' OR creators_info='".$chk_type."') AND del_status=0 AND active=0");
					if(mysql_num_rows($sql)>0)
					{
						while($row_po = mysql_fetch_assoc($sql))
						{
							//if($row_po['artist_id']!=$id['artist_id'])
							//{
								
								$sql_art2 = mysql_query("SELECT * FROM artist_event WHERE creators_info='artist_project|".$row_po['id']."' AND del_status=0 AND active=0 AND date<'".$date."'");
								if(mysql_num_rows($sql_art2)>0)
								{
									while($row2_po = mysql_fetch_assoc($sql_art2))
									{
										$row2_po['table_name'] = 'artist_event';
										$sql = mysql_query("SELECT MIN( `id` ) FROM `artist_event_profiles` WHERE artist_event_id='".$row2_po['id']."' AND subtype_id!=0");
							
										if(mysql_num_rows($sql)>0)
										{
											$res = mysql_fetch_assoc($sql);
											
											$sql2 = mysql_query("select * from artist_event_profiles where id='".$res['MIN( `id` )']."'");
											$res2 = mysql_fetch_assoc($sql2);

											$sql4 = mysql_query("select * from  subtype where subtype_id = '".$res2['subtype_id']."'");
											if(mysql_num_rows($sql4)>0)
											{
												$res4 = mysql_fetch_assoc($sql4);
												$row2_po['type'] = $res4['name'];
											}
											else
											{
												$row2_po['type'] = '';
											}
										}
										else
										{
											$row2_po['type'] = '';
										}
										$com_a_ao[] = $row2_po;
									}
								}
								
								
								$sql_art3 = mysql_query("SELECT * FROM community_event WHERE creators_info='artist_project|".$row_po['id']."' AND del_status=0 AND active=0 AND date<'".$date."'");
								if(mysql_num_rows($sql_art3)>0)
								{
									while($row3_po = mysql_fetch_assoc($sql_art3))
									{
										$row3_po['table_name'] = 'community_event';
										$sql = mysql_query("SELECT MIN( `id` ) FROM `community_event_profiles` WHERE community_event_id='".$row3_po['id']."' AND subtype_id!=0");
							
										if(mysql_num_rows($sql)>0)
										{
											$res = mysql_fetch_assoc($sql);
											
											$sql2 = mysql_query("select * from community_event_profiles where id='".$res['MIN( `id` )']."'");
											$res2 = mysql_fetch_assoc($sql2);

											$sql4 = mysql_query("select * from  subtype where subtype_id = '".$res2['subtype_id']."'");
											if(mysql_num_rows($sql4)>0)
											{
												$res4 = mysql_fetch_assoc($sql4);
												$row3_po['type'] = $res4['name'];
											}
											else
											{
												$row3_po['type'] = '';
											}
										}
										else
										{
											$row3_po['type'] = '';
										}
										$art_a_ao[] = $row3_po;
									}
								}
							//}
						}
					}
					
					
					$sql = mysql_query("SELECT * FROM community_project WHERE (creators_info='".$chk_typep_2."' OR creators_info='".$chk_type."') AND del_status=0 AND active=0");
					if(mysql_num_rows($sql)>0)
					{
						while($row_poo = mysql_fetch_assoc($sql))
						{
							//if($row_poo['community_id']!=$id['community_id'])
							//{
								
								$sql_art2 = mysql_query("SELECT * FROM artist_event WHERE creators_info='community_project|".$row_poo['id']."' AND del_status=0 AND active=0 AND date<'".$date."'");
								if(mysql_num_rows($sql_art2)>0)
								{
									while($row2_po = mysql_fetch_assoc($sql_art2))
									{
										$row2_po['table_name'] = 'artist_event';
										$sql = mysql_query("SELECT MIN( `id` ) FROM `artist_event_profiles` WHERE artist_event_id='".$row2_po['id']."' AND subtype_id!=0");
							
										if(mysql_num_rows($sql)>0)
										{
											$res = mysql_fetch_assoc($sql);
											
											$sql2 = mysql_query("select * from artist_event_profiles where id='".$res['MIN( `id` )']."'");
											$res2 = mysql_fetch_assoc($sql2);

											$sql4 = mysql_query("select * from  subtype where subtype_id = '".$res2['subtype_id']."'");
											if(mysql_num_rows($sql4)>0)
											{
												$res4 = mysql_fetch_assoc($sql4);
												$row2_po['type'] = $res4['name'];
											}
											else
											{
												$row2_po['type'] = '';
											}
										}
										else
										{
											$row2_po['type'] = '';
										}
										$art_ap_ao[] = $row2_po;
									}
								}
								
								
								$sql_art3 = mysql_query("SELECT * FROM community_event WHERE creators_info='community_project|".$row_poo['id']."' AND del_status=0 AND active=0 AND date<'".$date."'");
								if(mysql_num_rows($sql_art3)>0)
								{
									while($row3_po = mysql_fetch_assoc($sql_art3))
									{
										$row3_po['table_name'] = 'community_event';
										$sql = mysql_query("SELECT MIN( `id` ) FROM `community_event_profiles` WHERE community_event_id='".$row3_po['id']."' AND subtype_id!=0");
							
										if(mysql_num_rows($sql)>0)
										{
											$res = mysql_fetch_assoc($sql);
											
											$sql2 = mysql_query("select * from community_event_profiles where id='".$res['MIN( `id` )']."'");
											$res2 = mysql_fetch_assoc($sql2);

											$sql4 = mysql_query("select * from  subtype where subtype_id = '".$res2['subtype_id']."'");
											if(mysql_num_rows($sql4)>0)
											{
												$res4 = mysql_fetch_assoc($sql4);
												$row3_po['type'] = $res4['name'];
											}
											else
											{
												$row3_po['type'] = '';
											}
										}
										else
										{
											$row3_po['type'] = '';
										}
										$com_cp_ao[] = $row3_po;
									}
								}
							//}
						}
					}
				}
			}
		}		
		
		$totals = array();
		$totals = array_merge($art_p_ao,$art_c_ao,$com_p_ao,$com_c_ao,$com_a_ao,$art_a_ao,$art_ap_ao,$com_cp_ao);
		return $totals;
	}
	
	function only_creator2_orecevent()
	{
		$id = $this->sel_general();
		$date = date("Y-m-d");
		
		$var_type_1 = 'general_artist';
		$var_type_2 = 'general_community';
		$var_type_3 = 'artist_project';
		$var_type_4 = 'community_project';
		
		$art_p_ao = array();
		$art_c_ao = array();
		
		$com_p_ao = array();
		$com_c_ao = array();
		
		$com_a_ao = array();
		$art_a_ao = array();
		
		$art_ap_ao = array();
		$com_cp_ao = array();
		
		$pal_1 = array();
		$pal_2 = array();
		
		if($id['artist_id']!=0 && !empty($id['artist_id']))
		{
			$chk_type = $var_type_1.'|'.$id['artist_id'];
			
			$sql_get_allp = mysql_query("SELECT * FROM artist_project WHERE artist_id='".$id['artist_id']."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_get_allp)>0)
			{
				while($rowal = mysql_fetch_assoc($sql_get_allp))
				{
					$rowal['whos_event'] = "art_creator_creator_eve";
					$pal_1[] = $rowal;
				}
			}
			
			if(!empty($pal_1) && count($pal_1))
			{
				for($pl=0;$pl<count($pal_1);$pl++)
				{
					$chk_typep_1 = 'artist_project|'.$pal_1[$pl]['id'];
					
					$sql = mysql_query("SELECT * FROM artist_project WHERE (creators_info='".$chk_typep_1."' OR creators_info='".$chk_type."') AND del_status=0 AND active=0");
					if(mysql_num_rows($sql)>0)
					{
						while($row_po = mysql_fetch_assoc($sql))
						{
							//if($row_po['artist_id']!=$id['artist_id'])
							//{
								
								$sql_art2 = mysql_query("SELECT * FROM artist_event WHERE creators_info='artist_project|".$row_po['id']."' AND del_status=0 AND active=0 AND date<'".$date."'");
								if(mysql_num_rows($sql_art2)>0)
								{
									while($row2_po = mysql_fetch_assoc($sql_art2))
									{
										if($row2_po['artist_id']!=$id['artist_id'])
										{
											$row2_po['id'] = $row2_po['id'].'~art';
											$row2_po['whos_event'] = "art_creator_creator_eve";
											$art_p_ao[] = $row2_po;
										}
									}
								}
								
								
								$sql_art3 = mysql_query("SELECT * FROM community_event WHERE creators_info='artist_project|".$row_po['id']."' AND del_status=0 AND active=0 AND date<'".$date."'");
								if(mysql_num_rows($sql_art3)>0)
								{
									while($row3_po = mysql_fetch_assoc($sql_art3))
									{
										if($row3_po['community_id']!=$id['community_id'])
										{
											$row3_po['id'] = $row3_po['id'].'~com';
											$row3_po['whos_event'] = "com_creator_creator_eve";
											$art_c_ao[] = $row3_po;
										}
									}
								}
							//}
						}
					}
					
					
					$sql = mysql_query("SELECT * FROM community_project WHERE (creators_info='".$chk_typep_1."' OR creators_info='".$chk_type."') AND del_status=0 AND active=0");
					if(mysql_num_rows($sql)>0)
					{
						while($row_poo = mysql_fetch_assoc($sql))
						{
							//if($row_poo['community_id']!=$id['community_id'])
							//{		
								
								$sql_art2 = mysql_query("SELECT * FROM artist_event WHERE creators_info='community_project|".$row_poo['id']."' AND del_status=0 AND active=0 AND date<'".$date."'");
								if(mysql_num_rows($sql_art2)>0)
								{
									while($row2_po = mysql_fetch_assoc($sql_art2))
									{
										if($row2_po['artist_id']!=$id['artist_id'])
										{
											$row2_po['id'] = $row2_po['id'].'~art';
											$row2_po['whos_event'] = "art_creator_creator_eve";
											$com_p_ao[] = $row2_po;
										}
									}
								}
								
								
								$sql_art3 = mysql_query("SELECT * FROM community_event WHERE creators_info='community_project|".$row_poo['id']."' AND del_status=0 AND active=0 AND date<'".$date."'");
								if(mysql_num_rows($sql_art3)>0)
								{
									while($row3_po = mysql_fetch_assoc($sql_art3))
									{
										if($row3_po['community_id']!=$id['community_id'])
										{
											$row3_po['id'] = $row3_po['id'].'~com';
											$row3_po['whos_event'] = "com_creator_creator_eve";
											$com_c_ao[] = $row3_po;
										}
									}
								}
							//}
						}
					}
				}
			}
		}
		
		if($id['community_id']!=0 && !empty($id['community_id']))
		{
			$chk_type = $var_type_2.'|'.$id['community_id'];
			
			$sql_get_allp = mysql_query("SELECT * FROM community_project WHERE community_id='".$id['community_id']."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_get_allp)>0)
			{
				while($rowal = mysql_fetch_assoc($sql_get_allp))
				{
					//$rowal['whos_project'] = "only_creator";
					$pal_2[] = $rowal;
				}
			}
			
			if(!empty($pal_2) && count($pal_2))
			{
				for($pl2=0;$pl2<count($pal_2);$pl2++)
				{
					$chk_typep_2 = 'community_project|'.$pal_2[$pl2]['id'];
					
					
					$sql = mysql_query("SELECT * FROM artist_project WHERE (creators_info='".$chk_typep_2."' OR creators_info='".$chk_type."') AND del_status=0 AND active=0");
					if(mysql_num_rows($sql)>0)
					{
						while($row_po = mysql_fetch_assoc($sql))
						{
							//if($row_po['artist_id']!=$id['artist_id'])
							//{
								
								$sql_art2 = mysql_query("SELECT * FROM artist_event WHERE creators_info='artist_project|".$row_po['id']."' AND del_status=0 AND active=0 AND date<'".$date."'");
								if(mysql_num_rows($sql_art2)>0)
								{
									while($row2_po = mysql_fetch_assoc($sql_art2))
									{
										if($row2_po['artist_id']!=$id['artist_id'])
										{
											$row2_po['id'] = $row2_po['id'].'~art';
											$row2_po['whos_event'] = "art_creator_creator_eve";
											$com_a_ao[] = $row2_po;
										}
									}
								}
								
								
								$sql_art3 = mysql_query("SELECT * FROM community_event WHERE creators_info='artist_project|".$row_po['id']."' AND del_status=0 AND active=0 AND date<'".$date."'");
								if(mysql_num_rows($sql_art3)>0)
								{
									while($row3_po = mysql_fetch_assoc($sql_art3))
									{
										if($row3_po['community_id']!=$id['community_id'])
										{
											$row3_po['id'] = $row3_po['id'].'~com';
											$row3_po['whos_event'] = "com_creator_creator_eve";
											$art_a_ao[] = $row3_po;
										}
									}
								}
							//}
						}
					}
					
					
					$sql = mysql_query("SELECT * FROM community_project WHERE (creators_info='".$chk_typep_2."' OR creators_info='".$chk_type."') AND del_status=0 AND active=0");
					if(mysql_num_rows($sql)>0)
					{
						while($row_poo = mysql_fetch_assoc($sql))
						{
							//if($row_poo['community_id']!=$id['community_id'])
							//{
								
								$sql_art2 = mysql_query("SELECT * FROM artist_event WHERE creators_info='community_project|".$row_poo['id']."' AND del_status=0 AND active=0 AND date<'".$date."'");
								if(mysql_num_rows($sql_art2)>0)
								{
									while($row2_po = mysql_fetch_assoc($sql_art2))
									{
										if($row2_po['artist_id']!=$id['artist_id'])
										{
											$row2_po['id'] = $row2_po['id'].'~art';
											$row2_po['whos_event'] = "art_creator_creator_eve";
											$art_ap_ao[] = $row2_po;
										}
									}
								}
								
								
								$sql_art3 = mysql_query("SELECT * FROM community_event WHERE creators_info='community_project|".$row_poo['id']."' AND del_status=0 AND active=0 AND date<'".$date."'");
								if(mysql_num_rows($sql_art3)>0)
								{
									while($row3_po = mysql_fetch_assoc($sql_art3))
									{
										if($row3_po['community_id']!=$id['community_id'])
										{
											$row3_po['id'] = $row3_po['id'].'~com';
											$row3_po['whos_event'] = "com_creator_creator_eve";
											$com_cp_ao[] = $row3_po;
										}
									}
								}
							//}
						}
					}
				}
			}
		}		
		
		$totals = array();
		$totals = array_merge($art_p_ao,$art_c_ao,$com_p_ao,$com_c_ao,$com_a_ao,$art_a_ao,$art_ap_ao,$com_cp_ao);
		return $totals;
	}
	/* function only_creator_upevent()
	{
		$date = date("Y-m-d");
		
		$new_id = $this->sel_general();
		$final_pros = array();
		
		if(isset($new_id['artist_id']) && !empty($new_id['artist_id']) && $new_id['artist_id']!=0)
		{
			$mcth = 'general_artist|'.$new_id['artist_id'];
			
			$sql_apro = mysql_query("SELECT * FROM artist_event WHERE creators_info='".$mcth."' AND del_status=0 AND active=0 AND date>='".$date."'");
			if(mysql_num_rows($sql_apro)>0)
			{
				while($ans_apro = mysql_fetch_assoc($sql_apro))
				{				
					$ans_apro['id'] = $ans_apro['id'].'~art';
					$final_pros[] = $ans_apro;
				}	
			}
			
			$sql_cpro = mysql_query("SELECT * FROM community_event WHERE creators_info='".$mcth."' AND del_status=0 AND active=0 AND date>='".$date."'");
			if(mysql_num_rows($sql_cpro)>0)
			{
				while($ans_cpro = mysql_fetch_assoc($sql_cpro))
				{				
					$ans_cpro['id'] = $ans_cpro['id'].'~com';
					$final_pros[] = $ans_cpro;
				}
			}
		}
		
		return $final_pros;
	} */
	
	function only_creator_upevent()
	{
		$id = $this->sel_general();
		$date = date("Y-m-d");
		
		$var_type_1 = 'general_artist';
		$var_type_2 = 'general_community';
		$var_type_3 = 'artist_project';
		$var_type_4 = 'community_project';
		
		$artm = array();
		$arta = array();
		$comm = array();
		$coma = array();
		
		$artme = array();
		$artae = array();
		$comme = array();
		$comae = array();
		
		$art_p_ao = array();
		$com_p_ao = array();
		$art_p_co = array();
		$com_p_co = array();
		
		$art_e_ao = array();
		$com_e_ao = array();
		$art_e_co = array();
		$com_e_co = array();
		
		$pal_1 = array();
		$pal_2 = array();
		
		if($id['artist_id']!=0 && !empty($id['artist_id']))
		{
			$chk_type = $var_type_1.'|'.$id['artist_id'];
			
			$sql_get_allp = mysql_query("SELECT * FROM artist_project WHERE artist_id='".$id['artist_id']."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_get_allp)>0)
			{
				while($rowal = mysql_fetch_assoc($sql_get_allp))
				{
					$pal_1[] = $rowal;
				}
			}
			
			
			
			 if(!empty($pal_1) && count($pal_1))
			{
				for($pl=0;$pl<count($pal_1);$pl++)
				{
					$chk_typep_1 = 'artist_project|'.$pal_1[$pl]['id'];
					
					$sql = mysql_query("SELECT * FROM artist_event WHERE creators_info='".$chk_typep_1."' AND del_status=0 AND active=0 AND date>='".$date."'");
					if(mysql_num_rows($sql)>0)
					{
						while($row_po = mysql_fetch_assoc($sql))
						{
							if($row_po['artist_id']!=$id['artist_id'])
							{
								$row_po['id'] = $row_po['id'].'~art';
								$art_e_ao[] = $row_po;
							}
						}
					}
					
					$sql = mysql_query("SELECT * FROM community_event WHERE creators_info='".$chk_typep_1."' AND del_status=0 AND active=0 AND date>='".$date."'");
					if(mysql_num_rows($sql)>0)
					{
						while($row_poo = mysql_fetch_assoc($sql))
						{
							if($row_poo['community_id']!=$id['community_id'])
							{
								$row_poo['id'] = $row_poo['id'].'~com';
								$com_e_ao[] = $row_poo;
							}
						}
					}
				}
			} 

			/*$sql_pro_arta = mysql_query("SELECT * FROM artist_project WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_pro_arta)>0)
			{
				while($rowaa = mysql_fetch_assoc($sql_pro_arta))
				{
					if($rowaa['artist_id']!=$id['artist_id'])
					{
						$rowaa['id'] = $rowaa['id'].'~art';
						$arta[] = $rowaa;
					}
				}
			}*/
			
			$sql_pro_arta = mysql_query("SELECT * FROM artist_event WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0 AND date>='".$date."'");
			if(mysql_num_rows($sql_pro_arta)>0)
			{
				while($rowaa = mysql_fetch_assoc($sql_pro_arta))
				{
					if($rowaa['artist_id']!=$id['artist_id'])
					{
						$rowaa['id'] = $rowaa['id'].'~art';
						$artae[] = $rowaa;
					}
				}
			} 
			
			/*$sql_pro_coma = mysql_query("SELECT * FROM community_project WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_pro_coma)>0)
			{
				while($rowam = mysql_fetch_assoc($sql_pro_coma))
				{
					if($rowam['community_id']!=$id['community_id'])
					{
						$rowam['id'] = $rowam['id'].'~com';
						$coma[] = $rowam;
					}
				}
			}*/
			
			$sql_pro_coma = mysql_query("SELECT * FROM community_event WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0 AND date>='".$date."'");
			if(mysql_num_rows($sql_pro_coma)>0)
			{
				while($rowam = mysql_fetch_assoc($sql_pro_coma))
				{
					if($rowam['community_id']!=$id['community_id'])
					{
						$rowam['id'] = $rowam['id'].'~com';
						$comae[] = $rowam;
					}
				}
			} 
		}
		
		if($id['community_id']!=0 && !empty($id['community_id']))
		{
			/*$chk_type = $var_type_2.'|'.$id['community_id'];
			$sql_pro_artm = mysql_query("SELECT * FROM artist_project WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_pro_artm)>0)
			{
				while($rowcm = mysql_fetch_assoc($sql_pro_artm))
				{
					if($rowcm['artist_id']!=$id['artist_id'])
					{
						$rowcm['id'] = $rowcm['id'].'~art';
						$artm[] = $rowcm;
					}
				}
			}*/
			
			 $chk_type = $var_type_2.'|'.$id['community_id'];
			$sql_pro_artm = mysql_query("SELECT * FROM artist_event WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0 AND date>='".$date."'");
			if(mysql_num_rows($sql_pro_artm)>0)
			{
				while($rowcm = mysql_fetch_assoc($sql_pro_artm))
				{
					if($rowcm['artist_id']!=$id['artist_id'])
					{
						$rowcm['id'] = $rowcm['id'].'~art';
						$artme[] = $rowcm;
					}
				}
			} 
			
			/*$sql_pro_comm = mysql_query("SELECT * FROM community_project WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_pro_comm)>0)
			{
				while($rowca = mysql_fetch_assoc($sql_pro_comm))
				{
					if($rowca['community_id']!=$id['community_id'])
					{
						$rowca['id'] = $rowca['id'].'~com';
						$comm[] = $rowca;
					}
				}
			}*/
			
			 $sql_pro_comm = mysql_query("SELECT * FROM community_event WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0 AND date>='".$date."'");
			if(mysql_num_rows($sql_pro_comm)>0)
			{
				while($rowca = mysql_fetch_assoc($sql_pro_comm))
				{
					if($rowca['community_id']!=$id['community_id'])
					{
						$rowca['id'] = $rowca['id'].'~com';
						$comme[] = $rowca;
					}
				}
			} 
			
			$sql_get_allp = mysql_query("SELECT * FROM community_project WHERE community_id='".$id['community_id']."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_get_allp)>0)
			{
				while($rowal = mysql_fetch_assoc($sql_get_allp))
				{
					$pal_2[] = $rowal;
				}
			}
			
			/*if(!empty($pal_2) && count($pal_2))
			{
				for($pl2=0;$pl2<count($pal_2);$pl2++)
				{
					$chk_typep_2 = 'community_project|'.$pal_2[$pl2]['id'];
					$sql = mysql_query("SELECT * FROM artist_project WHERE creators_info='".$chk_typep_2."' AND del_status=0 AND active=0");
					if(mysql_num_rows($sql)>0)
					{
						while($row_po = mysql_fetch_assoc($sql))
						{
							if($row_po['artist_id']!=$id['artist_id'])
							{
								$row_po['id'] = $row_po['id'].'~art';
								$art_p_co[] = $row_po;
							}
						}
					}
					
					$sql = mysql_query("SELECT * FROM community_project WHERE creators_info='".$chk_typep_2."' AND del_status=0 AND active=0");
					if(mysql_num_rows($sql)>0)
					{
						while($row_poo = mysql_fetch_assoc($sql))
						{
							if($row_poo['community_id']!=$id['community_id'])
							{
								$row_poo['id'] = $row_poo['id'].'~com';
								$com_p_co[] = $row_poo;
							}
						}
					}
				}
			}*/
			
			 if(!empty($pal_2) && count($pal_2))
			{
				for($pl2=0;$pl2<count($pal_2);$pl2++)
				{
					$chk_typep_2 = 'community_project|'.$pal_2[$pl2]['id'];
					$sql = mysql_query("SELECT * FROM artist_event WHERE creators_info='".$chk_typep_2."' AND del_status=0 AND active=0 AND date>='".$date."'");
					if(mysql_num_rows($sql)>0)
					{
						while($row_po = mysql_fetch_assoc($sql))
						{
							if($row_po['artist_id']!=$id['artist_id'])
							{
								$row_po['id'] = $row_po['id'].'~art';
								$art_e_co[] = $row_po;
							}
						}
					}
					
					$sql = mysql_query("SELECT * FROM community_event WHERE creators_info='".$chk_typep_2."' AND del_status=0 AND active=0 AND date>='".$date."'");
					if(mysql_num_rows($sql)>0)
					{
						while($row_poo = mysql_fetch_assoc($sql))
						{
							if($row_poo['community_id']!=$id['community_id'])
							{
								$row_poo['id'] = $row_poo['id'].'~com';
								$com_e_co[] = $row_poo;
							}
						}
					}
				}
			} 
		}
		
		/* var_dump($artm);
		var_dump($arta);
		var_dump($comm);
		var_dump($coma);
		var_dump($art_p_ao);
		var_dump($com_p_ao);
		var_dump($art_p_co);
		var_dump($com_p_co); */
		
		$totals = array();
		$totals = array_merge($artm,$arta,$comm,$coma,$art_p_ao,$com_p_ao,$art_p_co,$com_p_co,$artme,$artae,$comme,$comae,$art_e_ao,$com_e_ao,$art_e_co,$com_e_co);
		return $totals;
	}
	
	function only_creator_sh_upevent()
	{
		$id = $this->sel_general();
		$date = date("Y-m-d");
		
		$var_type_1 = 'general_artist';
		$var_type_2 = 'general_community';
		$var_type_3 = 'artist_project';
		$var_type_4 = 'community_project';
		
		$artm = array();
		$arta = array();
		$comm = array();
		$coma = array();
		
		$artme = array();
		$artae = array();
		$comme = array();
		$comae = array();
		
		$art_p_ao = array();
		$com_p_ao = array();
		$art_p_co = array();
		$com_p_co = array();
		
		$art_e_ao = array();
		$com_e_ao = array();
		$art_e_co = array();
		$com_e_co = array();
		
		$pal_1 = array();
		$pal_2 = array();
		
		if($id['artist_id']!=0 && !empty($id['artist_id']))
		{
			$chk_type = $var_type_1.'|'.$id['artist_id'];
			
			$sql_get_allp = mysql_query("SELECT * FROM artist_project WHERE artist_id='".$id['artist_id']."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_get_allp)>0)
			{
				while($rowal = mysql_fetch_assoc($sql_get_allp))
				{
					$pal_1[] = $rowal;
				}
			}
			
			
			
			 if(!empty($pal_1) && count($pal_1))
			{
				for($pl=0;$pl<count($pal_1);$pl++)
				{
					$chk_typep_1 = 'artist_project|'.$pal_1[$pl]['id'];
					
					$sql = mysql_query("SELECT * FROM artist_event WHERE creators_info='".$chk_typep_1."' AND del_status=0 AND active=0 AND date>='".$date."'");
					if(mysql_num_rows($sql)>0)
					{
						while($row_po = mysql_fetch_assoc($sql))
						{
							if($row_po['artist_id']!=$id['artist_id'])
							{
								$row_po['table_name'] = 'artist_event';
								$sql = mysql_query("SELECT MIN( `id` ) FROM `artist_event_profiles` WHERE artist_event_id='".$row_po['id']."' AND subtype_id!=0");
							
								if(mysql_num_rows($sql)>0)
								{
									$res = mysql_fetch_assoc($sql);
									
									$sql2 = mysql_query("select * from artist_event_profiles where id='".$res['MIN( `id` )']."'");
									$res2 = mysql_fetch_assoc($sql2);

									$sql4 = mysql_query("select * from  subtype where subtype_id = '".$res2['subtype_id']."'");
									if(mysql_num_rows($sql4)>0)
									{
										$res4 = mysql_fetch_assoc($sql4);
										$row_po['type'] = $res4['name'];
									}
									else
									{
										$row_po['type'] = '';
									}
								}
								else
								{
									$row_po['type'] = '';
								}
								$art_e_ao[] = $row_po;
							}
						}
					}
					
					$sql = mysql_query("SELECT * FROM community_event WHERE creators_info='".$chk_typep_1."' AND del_status=0 AND active=0 AND date>='".$date."'");
					if(mysql_num_rows($sql)>0)
					{
						while($row_poo = mysql_fetch_assoc($sql))
						{
							if($row_poo['community_id']!=$id['community_id'])
							{
								$row_poo['table_name'] = 'community_event';
								$sql = mysql_query("SELECT MIN( `id` ) FROM `community_event_profiles` WHERE community_event_id='".$row_poo['id']."' AND subtype_id!=0");
							
								if(mysql_num_rows($sql)>0)
								{
									$res = mysql_fetch_assoc($sql);
									
									$sql2 = mysql_query("select * from community_event_profiles where id='".$res['MIN( `id` )']."'");
									$res2 = mysql_fetch_assoc($sql2);

									$sql4 = mysql_query("select * from  subtype where subtype_id = '".$res2['subtype_id']."'");
									if(mysql_num_rows($sql4)>0)
									{
										$res4 = mysql_fetch_assoc($sql4);
										$row_poo['type'] = $res4['name'];
									}
									else
									{
										$row_poo['type'] = '';
									}
								}
								else
								{
									$row_poo['type'] = '';
								}
								$com_e_ao[] = $row_poo;
							}
						}
					}
				}
			} 

			/*$sql_pro_arta = mysql_query("SELECT * FROM artist_project WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_pro_arta)>0)
			{
				while($rowaa = mysql_fetch_assoc($sql_pro_arta))
				{
					if($rowaa['artist_id']!=$id['artist_id'])
					{
						$rowaa['id'] = $rowaa['id'].'~art';
						$arta[] = $rowaa;
					}
				}
			}*/
			
			$sql_pro_arta = mysql_query("SELECT * FROM artist_event WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0 AND date>='".$date."'");
			if(mysql_num_rows($sql_pro_arta)>0)
			{
				while($rowaa = mysql_fetch_assoc($sql_pro_arta))
				{
					if($rowaa['artist_id']!=$id['artist_id'])
					{
						$rowaa['table_name'] = 'artist_event';
						$sql = mysql_query("SELECT MIN( `id` ) FROM `artist_event_profiles` WHERE artist_event_id='".$rowaa['id']."' AND subtype_id!=0");
							
						if(mysql_num_rows($sql)>0)
						{
							$res = mysql_fetch_assoc($sql);
							
							$sql2 = mysql_query("select * from artist_event_profiles where id='".$res['MIN( `id` )']."'");
							$res2 = mysql_fetch_assoc($sql2);

							$sql4 = mysql_query("select * from  subtype where subtype_id = '".$res2['subtype_id']."'");
							if(mysql_num_rows($sql4)>0)
							{
								$res4 = mysql_fetch_assoc($sql4);
								$rowaa['type'] = $res4['name'];
							}
							else
							{
								$rowaa['type'] = '';
							}
						}
						else
						{
							$rowaa['type'] = '';
						}
						$artae[] = $rowaa;
					}
				}
			} 
			
			/*$sql_pro_coma = mysql_query("SELECT * FROM community_project WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_pro_coma)>0)
			{
				while($rowam = mysql_fetch_assoc($sql_pro_coma))
				{
					if($rowam['community_id']!=$id['community_id'])
					{
						$rowam['id'] = $rowam['id'].'~com';
						$coma[] = $rowam;
					}
				}
			}*/
			
			$sql_pro_coma = mysql_query("SELECT * FROM community_event WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0 AND date>='".$date."'");
			if(mysql_num_rows($sql_pro_coma)>0)
			{
				while($rowam = mysql_fetch_assoc($sql_pro_coma))
				{
					if($rowam['community_id']!=$id['community_id'])
					{
						$rowam['table_name'] = 'community_event';
						$sql = mysql_query("SELECT MIN( `id` ) FROM `community_event_profiles` WHERE community_event_id='".$rowam['id']."' AND subtype_id!=0");
							
						if(mysql_num_rows($sql)>0)
						{
							$res = mysql_fetch_assoc($sql);
							
							$sql2 = mysql_query("select * from community_event_profiles where id='".$res['MIN( `id` )']."'");
							$res2 = mysql_fetch_assoc($sql2);

							$sql4 = mysql_query("select * from  subtype where subtype_id = '".$res2['subtype_id']."'");
							if(mysql_num_rows($sql4)>0)
							{
								$res4 = mysql_fetch_assoc($sql4);
								$rowam['type'] = $res4['name'];
							}
							else
							{
								$rowam['type'] = '';
							}
						}
						else
						{
							$rowam['type'] = '';
						}
						$comae[] = $rowam;
					}
				}
			} 
		}
		
		if($id['community_id']!=0 && !empty($id['community_id']))
		{
			/*$chk_type = $var_type_2.'|'.$id['community_id'];
			$sql_pro_artm = mysql_query("SELECT * FROM artist_project WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_pro_artm)>0)
			{
				while($rowcm = mysql_fetch_assoc($sql_pro_artm))
				{
					if($rowcm['artist_id']!=$id['artist_id'])
					{
						$rowcm['id'] = $rowcm['id'].'~art';
						$artm[] = $rowcm;
					}
				}
			}*/
			
			$chk_type = $var_type_2.'|'.$id['community_id'];
			$sql_pro_artm = mysql_query("SELECT * FROM artist_event WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0 AND date>='".$date."'");
			if(mysql_num_rows($sql_pro_artm)>0)
			{
				while($rowcm = mysql_fetch_assoc($sql_pro_artm))
				{
					if($rowcm['artist_id']!=$id['artist_id'])
					{
						$rowcm['table_name'] = 'artist_event';
						$sql = mysql_query("SELECT MIN( `id` ) FROM `artist_event_profiles` WHERE artist_event_id='".$rowcm['id']."' AND subtype_id!=0");
							
						if(mysql_num_rows($sql)>0)
						{
							$res = mysql_fetch_assoc($sql);
							
							$sql2 = mysql_query("select * from artist_event_profiles where id='".$res['MIN( `id` )']."'");
							$res2 = mysql_fetch_assoc($sql2);

							$sql4 = mysql_query("select * from  subtype where subtype_id = '".$res2['subtype_id']."'");
							if(mysql_num_rows($sql4)>0)
							{
								$res4 = mysql_fetch_assoc($sql4);
								$rowcm['type'] = $res4['name'];
							}
							else
							{
								$rowcm['type'] = '';
							}
						}
						else
						{
							$rowcm['type'] = '';
						}
						$artme[] = $rowcm;
					}
				}
			} 
			
			/*$sql_pro_comm = mysql_query("SELECT * FROM community_project WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_pro_comm)>0)
			{
				while($rowca = mysql_fetch_assoc($sql_pro_comm))
				{
					if($rowca['community_id']!=$id['community_id'])
					{
						$rowca['id'] = $rowca['id'].'~com';
						$comm[] = $rowca;
					}
				}
			}*/
			
			 $sql_pro_comm = mysql_query("SELECT * FROM community_event WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0 AND date>='".$date."'");
			if(mysql_num_rows($sql_pro_comm)>0)
			{
				while($rowca = mysql_fetch_assoc($sql_pro_comm))
				{
					if($rowca['community_id']!=$id['community_id'])
					{
						$rowca['table_name'] = 'community_event';
						$sql = mysql_query("SELECT MIN( `id` ) FROM `community_event_profiles` WHERE community_event_id='".$rowca['id']."' AND subtype_id!=0");
							
						if(mysql_num_rows($sql)>0)
						{
							$res = mysql_fetch_assoc($sql);
							
							$sql2 = mysql_query("select * from community_event_profiles where id='".$res['MIN( `id` )']."'");
							$res2 = mysql_fetch_assoc($sql2);

							$sql4 = mysql_query("select * from  subtype where subtype_id = '".$res2['subtype_id']."'");
							if(mysql_num_rows($sql4)>0)
							{
								$res4 = mysql_fetch_assoc($sql4);
								$rowca['type'] = $res4['name'];
							}
							else
							{
								$rowca['type'] = '';
							}
						}
						else
						{
							$rowca['type'] = '';
						}
						$comme[] = $rowca;
					}
				}
			} 
			
			$sql_get_allp = mysql_query("SELECT * FROM community_project WHERE community_id='".$id['community_id']."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_get_allp)>0)
			{
				while($rowal = mysql_fetch_assoc($sql_get_allp))
				{
					$pal_2[] = $rowal;
				}
			}
			
			/*if(!empty($pal_2) && count($pal_2))
			{
				for($pl2=0;$pl2<count($pal_2);$pl2++)
				{
					$chk_typep_2 = 'community_project|'.$pal_2[$pl2]['id'];
					$sql = mysql_query("SELECT * FROM artist_project WHERE creators_info='".$chk_typep_2."' AND del_status=0 AND active=0");
					if(mysql_num_rows($sql)>0)
					{
						while($row_po = mysql_fetch_assoc($sql))
						{
							if($row_po['artist_id']!=$id['artist_id'])
							{
								$row_po['id'] = $row_po['id'].'~art';
								$art_p_co[] = $row_po;
							}
						}
					}
					
					$sql = mysql_query("SELECT * FROM community_project WHERE creators_info='".$chk_typep_2."' AND del_status=0 AND active=0");
					if(mysql_num_rows($sql)>0)
					{
						while($row_poo = mysql_fetch_assoc($sql))
						{
							if($row_poo['community_id']!=$id['community_id'])
							{
								$row_poo['id'] = $row_poo['id'].'~com';
								$com_p_co[] = $row_poo;
							}
						}
					}
				}
			}*/
			
			 if(!empty($pal_2) && count($pal_2))
			{
				for($pl2=0;$pl2<count($pal_2);$pl2++)
				{
					$chk_typep_2 = 'community_project|'.$pal_2[$pl2]['id'];
					$sql = mysql_query("SELECT * FROM artist_event WHERE creators_info='".$chk_typep_2."' AND del_status=0 AND active=0 AND date>='".$date."'");
					if(mysql_num_rows($sql)>0)
					{
						while($row_po = mysql_fetch_assoc($sql))
						{
							if($row_po['artist_id']!=$id['artist_id'])
							{
								$row_po['table_name'] = 'artist_event';
								$sql = mysql_query("SELECT MIN( `id` ) FROM `artist_event_profiles` WHERE artist_event_id='".$row_po['id']."' AND subtype_id!=0");
							
								if(mysql_num_rows($sql)>0)
								{
									$res = mysql_fetch_assoc($sql);
									
									$sql2 = mysql_query("select * from artist_event_profiles where id='".$res['MIN( `id` )']."'");
									$res2 = mysql_fetch_assoc($sql2);

									$sql4 = mysql_query("select * from  subtype where subtype_id = '".$res2['subtype_id']."'");
									if(mysql_num_rows($sql4)>0)
									{
										$res4 = mysql_fetch_assoc($sql4);
										$row_po['type'] = $res4['name'];
									}
									else
									{
										$row_po['type'] = '';
									}
								}
								else
								{
									$row_po['type'] = '';
								}
								$art_e_co[] = $row_po;
							}
						}
					}
					
					$sql = mysql_query("SELECT * FROM community_event WHERE creators_info='".$chk_typep_2."' AND del_status=0 AND active=0 AND date>='".$date."'");
					if(mysql_num_rows($sql)>0)
					{
						while($row_poo = mysql_fetch_assoc($sql))
						{
							if($row_poo['community_id']!=$id['community_id'])
							{
								$row_poo['table_name'] = 'community_event';
								$sql = mysql_query("SELECT MIN( `id` ) FROM `community_event_profiles` WHERE community_event_id='".$row_poo['id']."' AND subtype_id!=0");
							
								if(mysql_num_rows($sql)>0)
								{
									$res = mysql_fetch_assoc($sql);
									
									$sql2 = mysql_query("select * from community_event_profiles where id='".$res['MIN( `id` )']."'");
									$res2 = mysql_fetch_assoc($sql2);

									$sql4 = mysql_query("select * from  subtype where subtype_id = '".$res2['subtype_id']."'");
									if(mysql_num_rows($sql4)>0)
									{
										$res4 = mysql_fetch_assoc($sql4);
										$row_poo['type'] = $res4['name'];
									}
									else
									{
										$row_poo['type'] = '';
									}
								}
								else
								{
									$row_poo['type'] = '';
								}
								$com_e_co[] = $row_poo;
							}
						}
					}
				}
			} 
		}
		
		/* var_dump($artm);
		var_dump($arta);
		var_dump($comm);
		var_dump($coma);
		var_dump($art_p_ao);
		var_dump($com_p_ao);
		var_dump($art_p_co);
		var_dump($com_p_co); */
		
		$totals = array();
		$totals = array_merge($artm,$arta,$comm,$coma,$art_p_ao,$com_p_ao,$art_p_co,$com_p_co,$artme,$artae,$comme,$comae,$art_e_ao,$com_e_ao,$art_e_co,$com_e_co);
		return $totals;
	}
	
	function only_creator_oupevent()
	{
		$id = $this->sel_general();
		$date = date("Y-m-d");
		
		$var_type_1 = 'general_artist';
		$var_type_2 = 'general_community';
		$var_type_3 = 'artist_project';
		$var_type_4 = 'community_project';
		
		$artm = array();
		$arta = array();
		$comm = array();
		$coma = array();
		
		$artme = array();
		$artae = array();
		$comme = array();
		$comae = array();
		
		$art_p_ao = array();
		$com_p_ao = array();
		$art_p_co = array();
		$com_p_co = array();
		
		$art_e_ao = array();
		$com_e_ao = array();
		$art_e_co = array();
		$com_e_co = array();
		
		$pal_1 = array();
		$pal_2 = array();
		
		if($id['artist_id']!=0 && !empty($id['artist_id']))
		{
			$chk_type = $var_type_1.'|'.$id['artist_id'];
			
			$sql_get_allp = mysql_query("SELECT * FROM artist_project WHERE artist_id='".$id['artist_id']."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_get_allp)>0)
			{
				while($rowal = mysql_fetch_assoc($sql_get_allp))
				{
					$rowal['whos_event'] = "only_creator";
					$pal_1[] = $rowal;
				}
			}
			
			
			
			 if(!empty($pal_1) && count($pal_1))
			{
				for($pl=0;$pl<count($pal_1);$pl++)
				{
					$chk_typep_1 = 'artist_project|'.$pal_1[$pl]['id'];
					
					$sql = mysql_query("SELECT * FROM artist_event WHERE creators_info='".$chk_typep_1."' AND del_status=0 AND active=0 AND date>='".$date."'");
					if(mysql_num_rows($sql)>0)
					{
						while($row_po = mysql_fetch_assoc($sql))
						{
							if($row_po['artist_id']!=$id['artist_id'])
							{
								$row_po['edit'] = 'yes';	
								$row_po['whos_event'] = "only_creator";
								$row_po['id'] = $row_po['id'] ."~art";
								$art_e_ao[] = $row_po;
							}
						}
					}
					
					$sql = mysql_query("SELECT * FROM community_event WHERE creators_info='".$chk_typep_1."' AND del_status=0 AND active=0 AND date>='".$date."'");
					if(mysql_num_rows($sql)>0)
					{
						while($row_poo = mysql_fetch_assoc($sql))
						{
							if($row_poo['community_id']!=$id['community_id'])
							{
								$row_poo['edit'] = 'yes';
								$row_poo['edit_com'] = 'yes';
								$row_poo['whos_event'] = "only_creator";
								$row_poo['id'] = $row_poo['id'] ."~com";
								$com_e_ao[] = $row_poo;
							}
						}
					}
				}
			} 

			/*$sql_pro_arta = mysql_query("SELECT * FROM artist_project WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_pro_arta)>0)
			{
				while($rowaa = mysql_fetch_assoc($sql_pro_arta))
				{
					if($rowaa['artist_id']!=$id['artist_id'])
					{
						$rowaa['id'] = $rowaa['id'].'~art';
						$arta[] = $rowaa;
					}
				}
			}*/
			
			$sql_pro_arta = mysql_query("SELECT * FROM artist_event WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0 AND date>='".$date."'");
			if(mysql_num_rows($sql_pro_arta)>0)
			{
				while($rowaa = mysql_fetch_assoc($sql_pro_arta))
				{
					if($rowaa['artist_id']!=$id['artist_id'])
					{
						$rowaa['edit'] = 'yes';
						$rowaa['whos_event'] = "only_creator";
						$rowaa['id'] = $rowaa['id'] ."~art";
						$artae[] = $rowaa;
					}
				}
			} 
			
			/*$sql_pro_coma = mysql_query("SELECT * FROM community_project WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_pro_coma)>0)
			{
				while($rowam = mysql_fetch_assoc($sql_pro_coma))
				{
					if($rowam['community_id']!=$id['community_id'])
					{
						$rowam['id'] = $rowam['id'].'~com';
						$coma[] = $rowam;
					}
				}
			}*/
			
			$sql_pro_coma = mysql_query("SELECT * FROM community_event WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0 AND date>='".$date."'");
			if(mysql_num_rows($sql_pro_coma)>0)
			{
				while($rowam = mysql_fetch_assoc($sql_pro_coma))
				{
					if($rowam['community_id']!=$id['community_id'])
					{
						$rowam['edit'] = 'yes';
						$rowam['edit_com'] = 'yes';
						$rowam['whos_event'] = "only_creator";
						$rowam['id'] = $rowam['id'] ."~com";
						$comae[] = $rowam;
					}
				}
			} 
		}
		
		if($id['community_id']!=0 && !empty($id['community_id']))
		{
			/*$chk_type = $var_type_2.'|'.$id['community_id'];
			$sql_pro_artm = mysql_query("SELECT * FROM artist_project WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_pro_artm)>0)
			{
				while($rowcm = mysql_fetch_assoc($sql_pro_artm))
				{
					if($rowcm['artist_id']!=$id['artist_id'])
					{
						$rowcm['id'] = $rowcm['id'].'~art';
						$artm[] = $rowcm;
					}
				}
			}*/
			
			 $chk_type = $var_type_2.'|'.$id['community_id'];
			$sql_pro_artm = mysql_query("SELECT * FROM artist_event WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0 AND date>='".$date."'");
			if(mysql_num_rows($sql_pro_artm)>0)
			{
				while($rowcm = mysql_fetch_assoc($sql_pro_artm))
				{
					if($rowcm['artist_id']!=$id['artist_id'])
					{
						$rowcm['edit'] = 'yes';
						$rowcm['whos_event'] = "only_creator";
						$rowcm['id'] = $rowcm['id'] ."~art";
						$artme[] = $rowcm;
					}
				}
			} 
			
			/*$sql_pro_comm = mysql_query("SELECT * FROM community_project WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_pro_comm)>0)
			{
				while($rowca = mysql_fetch_assoc($sql_pro_comm))
				{
					if($rowca['community_id']!=$id['community_id'])
					{
						$rowca['id'] = $rowca['id'].'~com';
						$comm[] = $rowca;
					}
				}
			}*/
			
			 $sql_pro_comm = mysql_query("SELECT * FROM community_event WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0 AND date>='".$date."'");
			if(mysql_num_rows($sql_pro_comm)>0)
			{
				while($rowca = mysql_fetch_assoc($sql_pro_comm))
				{
					if($rowca['community_id']!=$id['community_id'])
					{
						$rowca['edit'] = 'yes';
						$rowca['edit_com'] = 'yes';
						$rowca['whos_event'] = "only_creator";
						$rowca['id'] = $rowca['id'] ."~com";
						$comme[] = $rowca;
					}
				}
			} 
			
			$sql_get_allp = mysql_query("SELECT * FROM community_project WHERE community_id='".$id['community_id']."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_get_allp)>0)
			{
				while($rowal = mysql_fetch_assoc($sql_get_allp))
				{
					$rowal['whos_event'] = "only_creator";
					$pal_2[] = $rowal;
				}
			}
			
			/*if(!empty($pal_2) && count($pal_2))
			{
				for($pl2=0;$pl2<count($pal_2);$pl2++)
				{
					$chk_typep_2 = 'community_project|'.$pal_2[$pl2]['id'];
					$sql = mysql_query("SELECT * FROM artist_project WHERE creators_info='".$chk_typep_2."' AND del_status=0 AND active=0");
					if(mysql_num_rows($sql)>0)
					{
						while($row_po = mysql_fetch_assoc($sql))
						{
							if($row_po['artist_id']!=$id['artist_id'])
							{
								$row_po['id'] = $row_po['id'].'~art';
								$art_p_co[] = $row_po;
							}
						}
					}
					
					$sql = mysql_query("SELECT * FROM community_project WHERE creators_info='".$chk_typep_2."' AND del_status=0 AND active=0");
					if(mysql_num_rows($sql)>0)
					{
						while($row_poo = mysql_fetch_assoc($sql))
						{
							if($row_poo['community_id']!=$id['community_id'])
							{
								$row_poo['id'] = $row_poo['id'].'~com';
								$com_p_co[] = $row_poo;
							}
						}
					}
				}
			}*/
			
			 if(!empty($pal_2) && count($pal_2))
			{
				for($pl2=0;$pl2<count($pal_2);$pl2++)
				{
					$chk_typep_2 = 'community_project|'.$pal_2[$pl2]['id'];
					$sql = mysql_query("SELECT * FROM artist_event WHERE creators_info='".$chk_typep_2."' AND del_status=0 AND active=0 AND date>='".$date."'");
					if(mysql_num_rows($sql)>0)
					{
						while($row_po = mysql_fetch_assoc($sql))
						{
							if($row_po['artist_id']!=$id['artist_id'])
							{
								$row_po['edit'] = 'yes';
								$row_po['whos_event'] = "only_creator";
								$row_po['id'] = $row_po['id'] ."~art";
								$art_e_co[] = $row_po;
							}
						}
					}
					
					$sql = mysql_query("SELECT * FROM community_event WHERE creators_info='".$chk_typep_2."' AND del_status=0 AND active=0 AND date>='".$date."'");
					if(mysql_num_rows($sql)>0)
					{
						while($row_poo = mysql_fetch_assoc($sql))
						{
							if($row_poo['community_id']!=$id['community_id'])
							{
								$row_poo['edit'] = 'yes';
								$row_poo['edit_com'] = 'yes';
								$row_poo['whos_event'] = "only_creator";
								$row_poo['id'] = $row_poo['id'] ."~com";
								$com_e_co[] = $row_poo;
							}
						}
					}
				}
			} 
		}
		
		/* var_dump($artm);
		var_dump($arta);
		var_dump($comm);
		var_dump($coma);
		var_dump($art_p_ao);
		var_dump($com_p_ao);
		var_dump($art_p_co);
		var_dump($com_p_co); */
		
		$totals = array();
		$totals = array_merge($artm,$arta,$comm,$coma,$art_p_ao,$com_p_ao,$art_p_co,$com_p_co,$artme,$artae,$comme,$comae,$art_e_ao,$com_e_ao,$art_e_co,$com_e_co);
		return $totals;
	}
	
	function only_creator_recevent()
	{
		$id = $this->sel_general();
		$date = date("Y-m-d");
		
		$var_type_1 = 'general_artist';
		$var_type_2 = 'general_community';
		$var_type_3 = 'artist_project';
		$var_type_4 = 'community_project';
		
		$artm = array();
		$arta = array();
		$comm = array();
		$coma = array();
		
		$artme = array();
		$artae = array();
		$comme = array();
		$comae = array();
		
		$art_p_ao = array();
		$com_p_ao = array();
		$art_p_co = array();
		$com_p_co = array();
		
		$art_e_ao = array();
		$com_e_ao = array();
		$art_e_co = array();
		$com_e_co = array();
		
		$pal_1 = array();
		$pal_2 = array();
		
		if($id['artist_id']!=0 && !empty($id['artist_id']))
		{
			$chk_type = $var_type_1.'|'.$id['artist_id'];
			
			$sql_get_allp = mysql_query("SELECT * FROM artist_project WHERE artist_id='".$id['artist_id']."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_get_allp)>0)
			{
				while($rowal = mysql_fetch_assoc($sql_get_allp))
				{
					$pal_1[] = $rowal;
				}
			}
			
			
			
			 if(!empty($pal_1) && count($pal_1))
			{
				for($pl=0;$pl<count($pal_1);$pl++)
				{
					$chk_typep_1 = 'artist_project|'.$pal_1[$pl]['id'];
					
					$sql = mysql_query("SELECT * FROM artist_event WHERE creators_info='".$chk_typep_1."' AND del_status=0 AND active=0 AND date<'".$date."'");
					if(mysql_num_rows($sql)>0)
					{
						while($row_po = mysql_fetch_assoc($sql))
						{
							if($row_po['artist_id']!=$id['artist_id'])
							{
								$row_po['id'] = $row_po['id'].'~art';
								$art_e_ao[] = $row_po;
							}
						}
					}
					
					$sql = mysql_query("SELECT * FROM community_event WHERE creators_info='".$chk_typep_1."' AND del_status=0 AND active=0 AND date<'".$date."'");
					if(mysql_num_rows($sql)>0)
					{
						while($row_poo = mysql_fetch_assoc($sql))
						{
							if($row_poo['community_id']!=$id['community_id'])
							{
								$row_poo['id'] = $row_poo['id'].'~com';
								$com_e_ao[] = $row_poo;
							}
						}
					}
				}
			} 

			/*$sql_pro_arta = mysql_query("SELECT * FROM artist_project WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_pro_arta)>0)
			{
				while($rowaa = mysql_fetch_assoc($sql_pro_arta))
				{
					if($rowaa['artist_id']!=$id['artist_id'])
					{
						$rowaa['id'] = $rowaa['id'].'~art';
						$arta[] = $rowaa;
					}
				}
			}*/
			
			$sql_pro_arta = mysql_query("SELECT * FROM artist_event WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0 AND date<'".$date."'");
			if(mysql_num_rows($sql_pro_arta)>0)
			{
				while($rowaa = mysql_fetch_assoc($sql_pro_arta))
				{
					if($rowaa['artist_id']!=$id['artist_id'])
					{
						$rowaa['id'] = $rowaa['id'].'~art';
						$artae[] = $rowaa;
					}
				}
			} 
			
			/*$sql_pro_coma = mysql_query("SELECT * FROM community_project WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_pro_coma)>0)
			{
				while($rowam = mysql_fetch_assoc($sql_pro_coma))
				{
					if($rowam['community_id']!=$id['community_id'])
					{
						$rowam['id'] = $rowam['id'].'~com';
						$coma[] = $rowam;
					}
				}
			}*/
			
			$sql_pro_coma = mysql_query("SELECT * FROM community_event WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0 AND date<'".$date."'");
			if(mysql_num_rows($sql_pro_coma)>0)
			{
				while($rowam = mysql_fetch_assoc($sql_pro_coma))
				{
					if($rowam['community_id']!=$id['community_id'])
					{
						$rowam['id'] = $rowam['id'].'~com';
						$comae[] = $rowam;
					}
				}
			} 
		}
		
		if($id['community_id']!=0 && !empty($id['community_id']))
		{
			/*$chk_type = $var_type_2.'|'.$id['community_id'];
			$sql_pro_artm = mysql_query("SELECT * FROM artist_project WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_pro_artm)>0)
			{
				while($rowcm = mysql_fetch_assoc($sql_pro_artm))
				{
					if($rowcm['artist_id']!=$id['artist_id'])
					{
						$rowcm['id'] = $rowcm['id'].'~art';
						$artm[] = $rowcm;
					}
				}
			}*/
			
			 $chk_type = $var_type_2.'|'.$id['community_id'];
			$sql_pro_artm = mysql_query("SELECT * FROM artist_event WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0 AND date<'".$date."'");
			if(mysql_num_rows($sql_pro_artm)>0)
			{
				while($rowcm = mysql_fetch_assoc($sql_pro_artm))
				{
					if($rowcm['artist_id']!=$id['artist_id'])
					{
						$rowcm['id'] = $rowcm['id'].'~art';
						$artme[] = $rowcm;
					}
				}
			} 
			
			/*$sql_pro_comm = mysql_query("SELECT * FROM community_project WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_pro_comm)>0)
			{
				while($rowca = mysql_fetch_assoc($sql_pro_comm))
				{
					if($rowca['community_id']!=$id['community_id'])
					{
						$rowca['id'] = $rowca['id'].'~com';
						$comm[] = $rowca;
					}
				}
			}*/
			
			 $sql_pro_comm = mysql_query("SELECT * FROM community_event WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0 AND date<'".$date."'");
			if(mysql_num_rows($sql_pro_comm)>0)
			{
				while($rowca = mysql_fetch_assoc($sql_pro_comm))
				{
					if($rowca['community_id']!=$id['community_id'])
					{
						$rowca['id'] = $rowca['id'].'~com';
						$comme[] = $rowca;
					}
				}
			} 
			
			$sql_get_allp = mysql_query("SELECT * FROM community_project WHERE community_id='".$id['community_id']."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_get_allp)>0)
			{
				while($rowal = mysql_fetch_assoc($sql_get_allp))
				{
					$pal_2[] = $rowal;
				}
			}
			
			/*if(!empty($pal_2) && count($pal_2))
			{
				for($pl2=0;$pl2<count($pal_2);$pl2++)
				{
					$chk_typep_2 = 'community_project|'.$pal_2[$pl2]['id'];
					$sql = mysql_query("SELECT * FROM artist_project WHERE creators_info='".$chk_typep_2."' AND del_status=0 AND active=0");
					if(mysql_num_rows($sql)>0)
					{
						while($row_po = mysql_fetch_assoc($sql))
						{
							if($row_po['artist_id']!=$id['artist_id'])
							{
								$row_po['id'] = $row_po['id'].'~art';
								$art_p_co[] = $row_po;
							}
						}
					}
					
					$sql = mysql_query("SELECT * FROM community_project WHERE creators_info='".$chk_typep_2."' AND del_status=0 AND active=0");
					if(mysql_num_rows($sql)>0)
					{
						while($row_poo = mysql_fetch_assoc($sql))
						{
							if($row_poo['community_id']!=$id['community_id'])
							{
								$row_poo['id'] = $row_poo['id'].'~com';
								$com_p_co[] = $row_poo;
							}
						}
					}
				}
			}*/
			
			 if(!empty($pal_2) && count($pal_2))
			{
				for($pl2=0;$pl2<count($pal_2);$pl2++)
				{
					$chk_typep_2 = 'community_project|'.$pal_2[$pl2]['id'];
					$sql = mysql_query("SELECT * FROM artist_event WHERE creators_info='".$chk_typep_2."' AND del_status=0 AND active=0 AND date<'".$date."'");
					if(mysql_num_rows($sql)>0)
					{
						while($row_po = mysql_fetch_assoc($sql))
						{
							if($row_po['artist_id']!=$id['artist_id'])
							{
								$row_po['id'] = $row_po['id'].'~art';
								$art_e_co[] = $row_po;
							}
						}
					}
					
					$sql = mysql_query("SELECT * FROM community_event WHERE creators_info='".$chk_typep_2."' AND del_status=0 AND active=0 AND date<'".$date."'");
					if(mysql_num_rows($sql)>0)
					{
						while($row_poo = mysql_fetch_assoc($sql))
						{
							if($row_poo['community_id']!=$id['community_id'])
							{
								$row_poo['id'] = $row_poo['id'].'~com';
								$com_e_co[] = $row_poo;
							}
						}
					}
				}
			} 
		}
		
		/* var_dump($artm);
		var_dump($arta);
		var_dump($comm);
		var_dump($coma);
		var_dump($art_p_ao);
		var_dump($com_p_ao);
		var_dump($art_p_co);
		var_dump($com_p_co); */
		
		$totals = array();
		$totals = array_merge($artm,$arta,$comm,$coma,$art_p_ao,$com_p_ao,$art_p_co,$com_p_co,$artme,$artae,$comme,$comae,$art_e_ao,$com_e_ao,$art_e_co,$com_e_co);
		return $totals;
	}
	
	function only_sh_creator_recevent()
	{
		$id = $this->sel_general();
		$date = date("Y-m-d");
		
		$var_type_1 = 'general_artist';
		$var_type_2 = 'general_community';
		$var_type_3 = 'artist_project';
		$var_type_4 = 'community_project';
		
		$artm = array();
		$arta = array();
		$comm = array();
		$coma = array();
		
		$artme = array();
		$artae = array();
		$comme = array();
		$comae = array();
		
		$art_p_ao = array();
		$com_p_ao = array();
		$art_p_co = array();
		$com_p_co = array();
		
		$art_e_ao = array();
		$com_e_ao = array();
		$art_e_co = array();
		$com_e_co = array();
		
		$pal_1 = array();
		$pal_2 = array();
		
		if($id['artist_id']!=0 && !empty($id['artist_id']))
		{
			$chk_type = $var_type_1.'|'.$id['artist_id'];
			
			$sql_get_allp = mysql_query("SELECT * FROM artist_project WHERE artist_id='".$id['artist_id']."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_get_allp)>0)
			{
				while($rowal = mysql_fetch_assoc($sql_get_allp))
				{
					$pal_1[] = $rowal;
				}
			}
			
			
			
			 if(!empty($pal_1) && count($pal_1))
			{
				for($pl=0;$pl<count($pal_1);$pl++)
				{
					$chk_typep_1 = 'artist_project|'.$pal_1[$pl]['id'];
					
					$sql = mysql_query("SELECT * FROM artist_event WHERE creators_info='".$chk_typep_1."' AND del_status=0 AND active=0 AND date<'".$date."'");
					if(mysql_num_rows($sql)>0)
					{
						while($row_po = mysql_fetch_assoc($sql))
						{
							if($row_po['artist_id']!=$id['artist_id'])
							{
								$row_po['table_name'] = 'artist_event';
								$sql = mysql_query("SELECT MIN( `id` ) FROM `artist_event_profiles` WHERE artist_event_id='".$row_po['id']."' AND subtype_id!=0");
							
								if(mysql_num_rows($sql)>0)
								{
									$res = mysql_fetch_assoc($sql);
									
									$sql2 = mysql_query("select * from artist_event_profiles where id='".$res['MIN( `id` )']."'");
									$res2 = mysql_fetch_assoc($sql2);

									$sql4 = mysql_query("select * from  subtype where subtype_id = '".$res2['subtype_id']."'");
									if(mysql_num_rows($sql4)>0)
									{
										$res4 = mysql_fetch_assoc($sql4);
										$row_po['type'] = $res4['name'];
									}
									else
									{
										$row_po['type'] = '';
									}
								}
								else
								{
									$row_po['type'] = '';
								}
								$art_e_ao[] = $row_po;
							}
						}
					}
					
					$sql = mysql_query("SELECT * FROM community_event WHERE creators_info='".$chk_typep_1."' AND del_status=0 AND active=0 AND date<'".$date."'");
					if(mysql_num_rows($sql)>0)
					{
						while($row_poo = mysql_fetch_assoc($sql))
						{
							if($row_poo['community_id']!=$id['community_id'])
							{
								$row_poo['table_name'] = 'community_event';
								$sql = mysql_query("SELECT MIN( `id` ) FROM `community_event_profiles` WHERE community_event_id='".$row_poo['id']."' AND subtype_id!=0");
							
								if(mysql_num_rows($sql)>0)
								{
									$res = mysql_fetch_assoc($sql);
									
									$sql2 = mysql_query("select * from community_event_profiles where id='".$res['MIN( `id` )']."'");
									$res2 = mysql_fetch_assoc($sql2);

									$sql4 = mysql_query("select * from  subtype where subtype_id = '".$res2['subtype_id']."'");
									if(mysql_num_rows($sql4)>0)
									{
										$res4 = mysql_fetch_assoc($sql4);
										$row_poo['type'] = $res4['name'];
									}
									else
									{
										$row_poo['type'] = '';
									}
								}
								else
								{
									$row_poo['type'] = '';
								}
								$com_e_ao[] = $row_poo;
							}
						}
					}
				}
			} 

			/*$sql_pro_arta = mysql_query("SELECT * FROM artist_project WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_pro_arta)>0)
			{
				while($rowaa = mysql_fetch_assoc($sql_pro_arta))
				{
					if($rowaa['artist_id']!=$id['artist_id'])
					{
						$rowaa['id'] = $rowaa['id'].'~art';
						$arta[] = $rowaa;
					}
				}
			}*/
			
			$sql_pro_arta = mysql_query("SELECT * FROM artist_event WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0 AND date<'".$date."'");
			if(mysql_num_rows($sql_pro_arta)>0)
			{
				while($rowaa = mysql_fetch_assoc($sql_pro_arta))
				{
					if($rowaa['artist_id']!=$id['artist_id'])
					{
						$rowaa['table_name'] = 'artist_event';
						$sql = mysql_query("SELECT MIN( `id` ) FROM `artist_event_profiles` WHERE artist_event_id='".$rowaa['id']."' AND subtype_id!=0");
							
						if(mysql_num_rows($sql)>0)
						{
							$res = mysql_fetch_assoc($sql);
							
							$sql2 = mysql_query("select * from artist_event_profiles where id='".$res['MIN( `id` )']."'");
							$res2 = mysql_fetch_assoc($sql2);

							$sql4 = mysql_query("select * from  subtype where subtype_id = '".$res2['subtype_id']."'");
							if(mysql_num_rows($sql4)>0)
							{
								$res4 = mysql_fetch_assoc($sql4);
								$rowaa['type'] = $res4['name'];
							}
							else
							{
								$rowaa['type'] = '';
							}
						}
						else
						{
							$rowaa['type'] = '';
						}
						$artae[] = $rowaa;
					}
				}
			} 
			
			/*$sql_pro_coma = mysql_query("SELECT * FROM community_project WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_pro_coma)>0)
			{
				while($rowam = mysql_fetch_assoc($sql_pro_coma))
				{
					if($rowam['community_id']!=$id['community_id'])
					{
						$rowam['id'] = $rowam['id'].'~com';
						$coma[] = $rowam;
					}
				}
			}*/
			
			$sql_pro_coma = mysql_query("SELECT * FROM community_event WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0 AND date<'".$date."'");
			if(mysql_num_rows($sql_pro_coma)>0)
			{
				while($rowam = mysql_fetch_assoc($sql_pro_coma))
				{
					if($rowam['community_id']!=$id['community_id'])
					{
						$rowam['table_name'] = 'community_event';
						$sql = mysql_query("SELECT MIN( `id` ) FROM `community_event_profiles` WHERE community_event_id='".$rowam['id']."' AND subtype_id!=0");
							
						if(mysql_num_rows($sql)>0)
						{
							$res = mysql_fetch_assoc($sql);
							
							$sql2 = mysql_query("select * from community_event_profiles where id='".$res['MIN( `id` )']."'");
							$res2 = mysql_fetch_assoc($sql2);

							$sql4 = mysql_query("select * from  subtype where subtype_id = '".$res2['subtype_id']."'");
							if(mysql_num_rows($sql4)>0)
							{
								$res4 = mysql_fetch_assoc($sql4);
								$rowam['type'] = $res4['name'];
							}
							else
							{
								$rowam['type'] = '';
							}
						}
						else
						{
							$rowam['type'] = '';
						}
						$comae[] = $rowam;
					}
				}
			} 
		}
		
		if($id['community_id']!=0 && !empty($id['community_id']))
		{
			/*$chk_type = $var_type_2.'|'.$id['community_id'];
			$sql_pro_artm = mysql_query("SELECT * FROM artist_project WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_pro_artm)>0)
			{
				while($rowcm = mysql_fetch_assoc($sql_pro_artm))
				{
					if($rowcm['artist_id']!=$id['artist_id'])
					{
						$rowcm['id'] = $rowcm['id'].'~art';
						$artm[] = $rowcm;
					}
				}
			}*/
			
			 $chk_type = $var_type_2.'|'.$id['community_id'];
			$sql_pro_artm = mysql_query("SELECT * FROM artist_event WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0 AND date<'".$date."'");
			if(mysql_num_rows($sql_pro_artm)>0)
			{
				while($rowcm = mysql_fetch_assoc($sql_pro_artm))
				{
					if($rowcm['artist_id']!=$id['artist_id'])
					{
						$rowcm['table_name'] = 'artist_event';
						$sql = mysql_query("SELECT MIN( `id` ) FROM `artist_event_profiles` WHERE artist_event_id='".$rowcm['id']."' AND subtype_id!=0");
							
						if(mysql_num_rows($sql)>0)
						{
							$res = mysql_fetch_assoc($sql);
							
							$sql2 = mysql_query("select * from artist_event_profiles where id='".$res['MIN( `id` )']."'");
							$res2 = mysql_fetch_assoc($sql2);

							$sql4 = mysql_query("select * from  subtype where subtype_id = '".$res2['subtype_id']."'");
							if(mysql_num_rows($sql4)>0)
							{
								$res4 = mysql_fetch_assoc($sql4);
								$rowcm['type'] = $res4['name'];
							}
							else
							{
								$rowcm['type'] = '';
							}
						}
						else
						{
							$rowcm['type'] = '';
						}
						$artme[] = $rowcm;
					}
				}
			} 
			
			/*$sql_pro_comm = mysql_query("SELECT * FROM community_project WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_pro_comm)>0)
			{
				while($rowca = mysql_fetch_assoc($sql_pro_comm))
				{
					if($rowca['community_id']!=$id['community_id'])
					{
						$rowca['id'] = $rowca['id'].'~com';
						$comm[] = $rowca;
					}
				}
			}*/
			
			 $sql_pro_comm = mysql_query("SELECT * FROM community_event WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0 AND date<'".$date."'");
			if(mysql_num_rows($sql_pro_comm)>0)
			{
				while($rowca = mysql_fetch_assoc($sql_pro_comm))
				{
					if($rowca['community_id']!=$id['community_id'])
					{
						$rowca['table_name'] = 'community_event';
						$sql = mysql_query("SELECT MIN( `id` ) FROM `community_event_profiles` WHERE community_event_id='".$rowca['id']."' AND subtype_id!=0");
							
						if(mysql_num_rows($sql)>0)
						{
							$res = mysql_fetch_assoc($sql);
							
							$sql2 = mysql_query("select * from community_event_profiles where id='".$res['MIN( `id` )']."'");
							$res2 = mysql_fetch_assoc($sql2);

							$sql4 = mysql_query("select * from  subtype where subtype_id = '".$res2['subtype_id']."'");
							if(mysql_num_rows($sql4)>0)
							{
								$res4 = mysql_fetch_assoc($sql4);
								$rowca['type'] = $res4['name'];
							}
							else
							{
								$rowca['type'] = '';
							}
						}
						else
						{
							$rowca['type'] = '';
						}
						$comme[] = $rowca;
					}
				}
			} 
			
			$sql_get_allp = mysql_query("SELECT * FROM community_project WHERE community_id='".$id['community_id']."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_get_allp)>0)
			{
				while($rowal = mysql_fetch_assoc($sql_get_allp))
				{
					$pal_2[] = $rowal;
				}
			}
			
			/*if(!empty($pal_2) && count($pal_2))
			{
				for($pl2=0;$pl2<count($pal_2);$pl2++)
				{
					$chk_typep_2 = 'community_project|'.$pal_2[$pl2]['id'];
					$sql = mysql_query("SELECT * FROM artist_project WHERE creators_info='".$chk_typep_2."' AND del_status=0 AND active=0");
					if(mysql_num_rows($sql)>0)
					{
						while($row_po = mysql_fetch_assoc($sql))
						{
							if($row_po['artist_id']!=$id['artist_id'])
							{
								$row_po['id'] = $row_po['id'].'~art';
								$art_p_co[] = $row_po;
							}
						}
					}
					
					$sql = mysql_query("SELECT * FROM community_project WHERE creators_info='".$chk_typep_2."' AND del_status=0 AND active=0");
					if(mysql_num_rows($sql)>0)
					{
						while($row_poo = mysql_fetch_assoc($sql))
						{
							if($row_poo['community_id']!=$id['community_id'])
							{
								$row_poo['id'] = $row_poo['id'].'~com';
								$com_p_co[] = $row_poo;
							}
						}
					}
				}
			}*/
			
			 if(!empty($pal_2) && count($pal_2))
			{
				for($pl2=0;$pl2<count($pal_2);$pl2++)
				{
					$chk_typep_2 = 'community_project|'.$pal_2[$pl2]['id'];
					$sql = mysql_query("SELECT * FROM artist_event WHERE creators_info='".$chk_typep_2."' AND del_status=0 AND active=0 AND date<'".$date."'");
					if(mysql_num_rows($sql)>0)
					{
						while($row_po = mysql_fetch_assoc($sql))
						{
							if($row_po['artist_id']!=$id['artist_id'])
							{
								$row_po['table_name'] = 'artist_event';
								$sql = mysql_query("SELECT MIN( `id` ) FROM `artist_event_profiles` WHERE artist_event_id='".$row_po['id']."' AND subtype_id!=0");
							
								if(mysql_num_rows($sql)>0)
								{
									$res = mysql_fetch_assoc($sql);
									
									$sql2 = mysql_query("select * from artist_event_profiles where id='".$res['MIN( `id` )']."'");
									$res2 = mysql_fetch_assoc($sql2);

									$sql4 = mysql_query("select * from  subtype where subtype_id = '".$res2['subtype_id']."'");
									if(mysql_num_rows($sql4)>0)
									{
										$res4 = mysql_fetch_assoc($sql4);
										$row_po['type'] = $res4['name'];
									}
									else
									{
										$row_po['type'] = '';
									}
								}
								else
								{
									$row_po['type'] = '';
								}
								$art_e_co[] = $row_po;
							}
						}
					}
					
					$sql = mysql_query("SELECT * FROM community_event WHERE creators_info='".$chk_typep_2."' AND del_status=0 AND active=0 AND date<'".$date."'");
					if(mysql_num_rows($sql)>0)
					{
						while($row_poo = mysql_fetch_assoc($sql))
						{
							if($row_poo['community_id']!=$id['community_id'])
							{
								$row_poo['table_name'] = 'community_event';
								$sql = mysql_query("SELECT MIN( `id` ) FROM `community_event_profiles` WHERE community_event_id='".$row_poo['id']."' AND subtype_id!=0");
							
								if(mysql_num_rows($sql)>0)
								{
									$res = mysql_fetch_assoc($sql);
									
									$sql2 = mysql_query("select * from community_event_profiles where id='".$res['MIN( `id` )']."'");
									$res2 = mysql_fetch_assoc($sql2);

									$sql4 = mysql_query("select * from  subtype where subtype_id = '".$res2['subtype_id']."'");
									if(mysql_num_rows($sql4)>0)
									{
										$res4 = mysql_fetch_assoc($sql4);
										$row_poo['type'] = $res4['name'];
									}
									else
									{
										$row_poo['type'] = '';
									}
								}
								else
								{
									$row_poo['type'] = '';
								}
								$com_e_co[] = $row_poo;
							}
						}
					}
				}
			} 
		}
		
		/* var_dump($artm);
		var_dump($arta);
		var_dump($comm);
		var_dump($coma);
		var_dump($art_p_ao);
		var_dump($com_p_ao);
		var_dump($art_p_co);
		var_dump($com_p_co); */
		
		$totals = array();
		$totals = array_merge($artm,$arta,$comm,$coma,$art_p_ao,$com_p_ao,$art_p_co,$com_p_co,$artme,$artae,$comme,$comae,$art_e_ao,$com_e_ao,$art_e_co,$com_e_co);
		return $totals;
	}
	
	function only_creator_orecevent()
	{
		$id = $this->sel_general();
		$date = date("Y-m-d");
		
		$var_type_1 = 'general_artist';
		$var_type_2 = 'general_community';
		$var_type_3 = 'artist_project';
		$var_type_4 = 'community_project';
		
		$artm = array();
		$arta = array();
		$comm = array();
		$coma = array();
		
		$artme = array();
		$artae = array();
		$comme = array();
		$comae = array();
		
		$art_p_ao = array();
		$com_p_ao = array();
		$art_p_co = array();
		$com_p_co = array();
		
		$art_e_ao = array();
		$com_e_ao = array();
		$art_e_co = array();
		$com_e_co = array();
		
		$pal_1 = array();
		$pal_2 = array();
		
		if($id['artist_id']!=0 && !empty($id['artist_id']))
		{
			$chk_type = $var_type_1.'|'.$id['artist_id'];
			
			$sql_get_allp = mysql_query("SELECT * FROM artist_project WHERE artist_id='".$id['artist_id']."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_get_allp)>0)
			{
				while($rowal = mysql_fetch_assoc($sql_get_allp))
				{
					$rowal['whos_event'] = "only_creator";
					$pal_1[] = $rowal;
				}
			}
			
			
			
			 if(!empty($pal_1) && count($pal_1))
			{
				for($pl=0;$pl<count($pal_1);$pl++)
				{
					$chk_typep_1 = 'artist_project|'.$pal_1[$pl]['id'];
					
					$sql = mysql_query("SELECT * FROM artist_event WHERE creators_info='".$chk_typep_1."' AND del_status=0 AND active=0 AND date<'".$date."'");
					if(mysql_num_rows($sql)>0)
					{
						while($row_po = mysql_fetch_assoc($sql))
						{
							if($row_po['artist_id']!=$id['artist_id'])
							{
								$row_po['edit'] = 'yes';
								$row_po['whos_event'] = "only_creator";
								$row_po['id'] = $row_po['id'] ."~art";
								$art_e_ao[] = $row_po;
							}
						}
					}
					
					$sql = mysql_query("SELECT * FROM community_event WHERE creators_info='".$chk_typep_1."' AND del_status=0 AND active=0 AND date<'".$date."'");
					if(mysql_num_rows($sql)>0)
					{
						while($row_poo = mysql_fetch_assoc($sql))
						{
							if($row_poo['community_id']!=$id['community_id'])
							{
								$row_poo['edit'] = 'yes';
								$row_poo['edit_com'] = 'yes';
								$row_poo['whos_event'] = "only_creator";
								$row_poo['id'] = $row_poo['id'] ."~com";
								$com_e_ao[] = $row_poo;
							}
						}
					}
				}
			} 

			/*$sql_pro_arta = mysql_query("SELECT * FROM artist_project WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_pro_arta)>0)
			{
				while($rowaa = mysql_fetch_assoc($sql_pro_arta))
				{
					if($rowaa['artist_id']!=$id['artist_id'])
					{
						$rowaa['id'] = $rowaa['id'].'~art';
						$arta[] = $rowaa;
					}
				}
			}*/
			
			$sql_pro_arta = mysql_query("SELECT * FROM artist_event WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0 AND date<'".$date."'");
			if(mysql_num_rows($sql_pro_arta)>0)
			{
				while($rowaa = mysql_fetch_assoc($sql_pro_arta))
				{
					if($rowaa['artist_id']!=$id['artist_id'])
					{
						$rowaa['edit'] = 'yes';
						$rowaa['whos_event'] = "only_creator";
						$rowaa['id'] = $rowaa['id'] ."~art";
						$artae[] = $rowaa;
					}
				}
			} 
			
			/*$sql_pro_coma = mysql_query("SELECT * FROM community_project WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_pro_coma)>0)
			{
				while($rowam = mysql_fetch_assoc($sql_pro_coma))
				{
					if($rowam['community_id']!=$id['community_id'])
					{
						$rowam['id'] = $rowam['id'].'~com';
						$coma[] = $rowam;
					}
				}
			}*/
			
			$sql_pro_coma = mysql_query("SELECT * FROM community_event WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0 AND date<'".$date."'");
			if(mysql_num_rows($sql_pro_coma)>0)
			{
				while($rowam = mysql_fetch_assoc($sql_pro_coma))
				{
					if($rowam['community_id']!=$id['community_id'])
					{
						$rowam['edit'] = 'yes';
						$rowam['edit_com'] = 'yes';
						$rowam['whos_event'] = "only_creator";
						$rowam['id'] = $rowam['id'] ."~com";
						$comae[] = $rowam;
					}
				}
			} 
		}
		
		if($id['community_id']!=0 && !empty($id['community_id']))
		{
			/*$chk_type = $var_type_2.'|'.$id['community_id'];
			$sql_pro_artm = mysql_query("SELECT * FROM artist_project WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_pro_artm)>0)
			{
				while($rowcm = mysql_fetch_assoc($sql_pro_artm))
				{
					if($rowcm['artist_id']!=$id['artist_id'])
					{
						$rowcm['id'] = $rowcm['id'].'~art';
						$artm[] = $rowcm;
					}
				}
			}*/
			
			 $chk_type = $var_type_2.'|'.$id['community_id'];
			$sql_pro_artm = mysql_query("SELECT * FROM artist_event WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0 AND date<'".$date."'");
			if(mysql_num_rows($sql_pro_artm)>0)
			{
				while($rowcm = mysql_fetch_assoc($sql_pro_artm))
				{
					if($rowcm['artist_id']!=$id['artist_id'])
					{
						$rowcm['edit'] = 'yes';
						$rowcm['whos_event'] = "only_creator";
						$rowcm['id'] = $rowcm['id'] ."~art";
						$artme[] = $rowcm;
					}
				}
			} 
			
			/*$sql_pro_comm = mysql_query("SELECT * FROM community_project WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_pro_comm)>0)
			{
				while($rowca = mysql_fetch_assoc($sql_pro_comm))
				{
					if($rowca['community_id']!=$id['community_id'])
					{
						$rowca['id'] = $rowca['id'].'~com';
						$comm[] = $rowca;
					}
				}
			}*/
			
			 $sql_pro_comm = mysql_query("SELECT * FROM community_event WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0 AND date<'".$date."'");
			if(mysql_num_rows($sql_pro_comm)>0)
			{
				while($rowca = mysql_fetch_assoc($sql_pro_comm))
				{
					if($rowca['community_id']!=$id['community_id'])
					{
						$rowca['edit'] = 'yes';
						$rowca['edit_com'] = 'yes';
						$rowca['whos_event'] = "only_creator";
						$rowca['id'] = $rowca['id'] ."~com";
						$comme[] = $rowca;
					}
				}
			} 
			
			$sql_get_allp = mysql_query("SELECT * FROM community_project WHERE community_id='".$id['community_id']."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_get_allp)>0)
			{
				while($rowal = mysql_fetch_assoc($sql_get_allp))
				{
					$rowal['whos_event'] = "only_creator";
					$pal_2[] = $rowal;
				}
			}
			
			/*if(!empty($pal_2) && count($pal_2))
			{
				for($pl2=0;$pl2<count($pal_2);$pl2++)
				{
					$chk_typep_2 = 'community_project|'.$pal_2[$pl2]['id'];
					$sql = mysql_query("SELECT * FROM artist_project WHERE creators_info='".$chk_typep_2."' AND del_status=0 AND active=0");
					if(mysql_num_rows($sql)>0)
					{
						while($row_po = mysql_fetch_assoc($sql))
						{
							if($row_po['artist_id']!=$id['artist_id'])
							{
								$row_po['id'] = $row_po['id'].'~art';
								$art_p_co[] = $row_po;
							}
						}
					}
					
					$sql = mysql_query("SELECT * FROM community_project WHERE creators_info='".$chk_typep_2."' AND del_status=0 AND active=0");
					if(mysql_num_rows($sql)>0)
					{
						while($row_poo = mysql_fetch_assoc($sql))
						{
							if($row_poo['community_id']!=$id['community_id'])
							{
								$row_poo['id'] = $row_poo['id'].'~com';
								$com_p_co[] = $row_poo;
							}
						}
					}
				}
			}*/
			
			 if(!empty($pal_2) && count($pal_2))
			{
				for($pl2=0;$pl2<count($pal_2);$pl2++)
				{
					$chk_typep_2 = 'community_project|'.$pal_2[$pl2]['id'];
					$sql = mysql_query("SELECT * FROM artist_event WHERE creators_info='".$chk_typep_2."' AND del_status=0 AND active=0 AND date<'".$date."'");
					if(mysql_num_rows($sql)>0)
					{
						while($row_po = mysql_fetch_assoc($sql))
						{
							if($row_po['artist_id']!=$id['artist_id'])
							{
								$row_po['edit'] = 'yes';
								$row_po['whos_event'] = "only_creator";
								$row_po['id'] = $row_po['id'] ."~art";
								$art_e_co[] = $row_po;
							}
						}
					}
					
					$sql = mysql_query("SELECT * FROM community_event WHERE creators_info='".$chk_typep_2."' AND del_status=0 AND active=0 AND date<'".$date."'");
					if(mysql_num_rows($sql)>0)
					{
						while($row_poo = mysql_fetch_assoc($sql))
						{
							if($row_poo['community_id']!=$id['community_id'])
							{
								$row_poo['edit'] = 'yes';
								$row_poo['edit_com'] = 'yes';
								$row_poo['whos_event'] = "only_creator";
								$row_poo['id'] = $row_poo['id'] ."~com";
								$com_e_co[] = $row_poo;
							}
						}
					}
				}
			} 
		}
		
		/* var_dump($artm);
		var_dump($arta);
		var_dump($comm);
		var_dump($coma);
		var_dump($art_p_ao);
		var_dump($com_p_ao);
		var_dump($art_p_co);
		var_dump($com_p_co); */
		
		$totals = array();
		$totals = array_merge($artm,$arta,$comm,$coma,$art_p_ao,$com_p_ao,$art_p_co,$com_p_co,$artme,$artae,$comme,$comae,$art_e_ao,$com_e_ao,$art_e_co,$com_e_co);
		return $totals;
	}
	
	/* function only_creator_recevent()
	{
		$date = date("Y-m-d");
		
		$new_id = $this->sel_general();
		$final_pros = array();
		
		if(isset($new_id['artist_id']) && !empty($new_id['artist_id']) && $new_id['artist_id']!=0)
		{
			$mcth = 'general_artist|'.$new_id['artist_id'];
			
			$sql_apro = mysql_query("SELECT * FROM artist_event WHERE creators_info='".$mcth."' AND del_status=0 AND active=0 AND date<'".$date."'");
			if(mysql_num_rows($sql_apro)>0)
			{
				while($ans_apro = mysql_fetch_assoc($sql_apro))
				{				
					$ans_apro['id'] = $ans_apro['id'].'~art';
					$final_pros[] = $ans_apro;
				}	
			}
			
			$sql_cpro = mysql_query("SELECT * FROM community_event WHERE creators_info='".$mcth."' AND del_status=0 AND active=0 AND date<'".$date."'");
			if(mysql_num_rows($sql_cpro)>0)
			{
				while($ans_cpro = mysql_fetch_assoc($sql_cpro))
				{				
					$ans_cpro['id'] = $ans_cpro['id'].'~com';
					$final_pros[] = $ans_cpro;
				}
			}
		}
		
		return $final_pros;
	} */
	
	/*function get_creator_heis(){
		$get_det = $this->sel_general();
		$get_media_art_pro1 = array();
		$get_media_art_eve1 = array();
		$get_media_com_pro1 = array();
		$get_media_com_eve1 = array();
		$get_media_art_pro2 = array();
		$get_media_art_eve2 = array();
		$get_media_com_pro2 = array();
		$get_media_com_eve2 = array();
		$get_media_artist_project1 = array();
		$get_media_artist_project2 = array();
		$get_media_artist_project3 = array();
		$get_media_artist_project4 = array();
		$get_media_community_project1 = array();
		$get_media_community_project2 = array();
		$get_media_community_project3 = array();
		$get_media_community_project4 = array();
		//if($get_det['artist_id'] !="" && $get_det['artist_id']!=0){
			$creator_info = $get_det['artist_id'];
			$query = mysql_query("select * from artist_project where artist_id ='".$creator_info."'");
			if(mysql_num_rows($query)>0){
				while($row = mysql_fetch_assoc($query)){
					$new_creator = "artist_project|".$row['id'];
					$find_cretor_med = mysql_query("select * from general_media where creator_info='".$new_creator."' OR from_info='".$new_creator."'");
					if(mysql_num_rows($find_cretor_med)>0){
						while($res_new_det = mysql_fetch_assoc($find_cretor_med)){
							$get_media_art_pro1 = $res_new_det;
						}
					}
				}
			}
			//var_dump($get_media_art_pro1);
			$query1 = mysql_query("select * from artist_event where creators_info ='".$creator_info."'");
			if(mysql_num_rows($query1)>0){
				while($row = mysql_fetch_assoc($query1)){
					$new_creator = "artist_event|".$row['id'];
					$find_cretor_med = mysql_query("select * from general_media where creator_info='".$new_creator."' OR from_info='".$new_creator."'");
					if(mysql_num_rows($find_cretor_med)>0){
						while($res_new_det = mysql_fetch_assoc($find_cretor_med)){
							$get_media_art_eve1 = $res_new_det;
						}
					}
				}
			}
			//var_dump($get_media_art_eve1);
			$query = mysql_query("select * from community_project where creators_info ='".$creator_info."'");
			if(mysql_num_rows($query)>0){
				while($row = mysql_fetch_assoc($query)){
					$new_creator = "community_project|".$row['id'];
					$find_cretor_med = mysql_query("select * from general_media where creator_info='".$new_creator."' OR from_info='".$new_creator."'");
					if(mysql_num_rows($find_cretor_med)>0){
						while($res_new_det = mysql_fetch_assoc($find_cretor_med)){
							$get_media_com_pro1 = $res_new_det;
						}
					}
				}
			}
			//var_dump($get_media_com_pro1);
			$query1 = mysql_query("select * from community_event where creators_info ='".$creator_info."'");
			if(mysql_num_rows($query1)>0){
				while($row = mysql_fetch_assoc($query1)){
					$new_creator = "community_event|".$row['id'];
					$find_cretor_med = mysql_query("select * from general_media where creator_info='".$new_creator."' OR from_info='".$new_creator."'");
					if(mysql_num_rows($find_cretor_med)>0){
						while($res_new_det = mysql_fetch_assoc($find_cretor_med)){
							$get_media_com_eve1 = $res_new_det;
						}
					}
				}
			}
			//var_dump($get_media_com_eve1);
			
			$creator_info = "general_community|".$get_det['community_id'];
			$query = mysql_query("select * from artist_project where creators_info ='".$creator_info."'");
			if(mysql_num_rows($query)>0){
				while($row = mysql_fetch_assoc($query)){
					$new_creator = "artist_project|".$row['id'];
					$find_cretor_med = mysql_query("select * from general_media where creator_info='".$new_creator."' OR from_info='".$new_creator."'");
					if(mysql_num_rows($find_cretor_med)>0){
						while($res_new_det = mysql_fetch_assoc($find_cretor_med)){
							$get_media_art_pro2 = $res_new_det;
						}
					}
				}
			}
			//var_dump($get_media_art_pro2);
			$query1 = mysql_query("select * from artist_event where creators_info ='".$creator_info."'");
			if(mysql_num_rows($query1)>0){
				while($row = mysql_fetch_assoc($query1)){
					$new_creator = "artist_event|".$row['id'];
					$find_cretor_med = mysql_query("select * from general_media where creator_info='".$new_creator."' OR from_info='".$new_creator."'");
					if(mysql_num_rows($find_cretor_med)>0){
						while($res_new_det = mysql_fetch_assoc($find_cretor_med)){
							$get_media_art_eve2 = $res_new_det;
						}
					}
				}
			}
			//var_dump($get_media_art_eve2);
			$query = mysql_query("select * from community_project where creators_info ='".$creator_info."'");
			if(mysql_num_rows($query)>0){
				while($row = mysql_fetch_assoc($query)){
					$new_creator = "community_project|".$row['id'];
					$find_cretor_med = mysql_query("select * from general_media where creator_info='".$new_creator."' OR from_info='".$new_creator."'");
					if(mysql_num_rows($find_cretor_med)>0){
						while($res_new_det = mysql_fetch_assoc($find_cretor_med)){
							$get_media_com_pro2 = $res_new_det;
						}
					}
				}
			}
			//var_dump($get_media_com_pro2);
			$query1 = mysql_query("select * from community_event where creators_info ='".$creator_info."'");
			if(mysql_num_rows($query1)>0){
				while($row = mysql_fetch_assoc($query1)){
					$new_creator = "community_event|".$row['id'];
					$find_cretor_med = mysql_query("select * from general_media where creator_info='".$new_creator."' OR from_info='".$new_creator."'");
					if(mysql_num_rows($find_cretor_med)>0){
						while($res_new_det = mysql_fetch_assoc($find_cretor_med)){
							$get_media_com_eve2 = $res_new_det;
						}
					}
				}
			}
			//var_dump($get_media_com_eve2);
			
			/**get community projects**//*
			$get_compro = mysql_query("select * from community_project where community_id = '".$get_det['community_id']."'");
			if(mysql_num_rows($get_compro)>0){
				while($res_com_pro = mysql_fetch_assoc($get_compro))
				{
					$creator_info = "community_project|".$res_com_pro['id'];
					$query = mysql_query("select * from artist_project where creators_info ='".$creator_info."'");
					if(mysql_num_rows($query)>0){
						while($row = mysql_fetch_assoc($query)){
							$new_creator = "artist_project|".$row['id'];
							$find_cretor_med = mysql_query("select * from general_media where creator_info='".$new_creator."' OR from_info='".$new_creator."'");
							if(mysql_num_rows($find_cretor_med)>0){
								while($res_new_det = mysql_fetch_assoc($find_cretor_med)){
									$get_media_community_project1 = $res_new_det;
								}
							}
						}
					}
					//var_dump($get_media_community_project1);
					$query = mysql_query("select * from artist_event where creators_info ='".$creator_info."'");
					if(mysql_num_rows($query)>0){
						while($row = mysql_fetch_assoc($query)){
							$new_creator = "artist_event|".$row['id'];
							$find_cretor_med = mysql_query("select * from general_media where creator_info='".$new_creator."' OR from_info='".$new_creator."'");
							if(mysql_num_rows($find_cretor_med)>0){
								while($res_new_det = mysql_fetch_assoc($find_cretor_med)){
									$get_media_community_project2 = $res_new_det;
								}
							}
						}
					}
					//var_dump($get_media_community_project2);
					$query = mysql_query("select * from community_project where creators_info ='".$creator_info."'");
					if(mysql_num_rows($query)>0){
						while($row = mysql_fetch_assoc($query)){
							$new_creator = "community_project|".$row['id'];
							$find_cretor_med = mysql_query("select * from general_media where creator_info='".$new_creator."' OR from_info='".$new_creator."'");
							if(mysql_num_rows($find_cretor_med)>0){
								while($res_new_det = mysql_fetch_assoc($find_cretor_med)){
									$get_media_community_project3 = $res_new_det;
								}
							}
						}
					}
					//var_dump($get_media_community_project3);
					$query = mysql_query("select * from community_event where creators_info ='".$creator_info."'");
					if(mysql_num_rows($query)>0){
						while($row = mysql_fetch_assoc($query)){
							$new_creator = "community_event|".$row['id'];
							$find_cretor_med = mysql_query("select * from general_media where creator_info='".$new_creator."' OR from_info='".$new_creator."'");
							if(mysql_num_rows($find_cretor_med)>0){
								while($res_new_det = mysql_fetch_assoc($find_cretor_med)){
									$get_media_community_project4 = $res_new_det;
								}
							}
						}
					}
					//var_dump($get_media_community_project4);
				}
			}
			
			/**get community projects ends here**/
			
			/**get artist projects**//*
			$get_compro = mysql_query("select * from artist_project where artist_id = '".$get_det['artist_id']."'");
			if(mysql_num_rows($get_compro)>0){
				while($res_com_pro = mysql_fetch_assoc($get_compro))
				{
					$creator_info = "artist_project|".$res_com_pro['id'];
					$query = mysql_query("select * from artist_project where creators_info ='".$creator_info."'");
					if(mysql_num_rows($query)>0){
						while($row = mysql_fetch_assoc($query)){
							$new_creator = "artist_project|".$row['id'];
							$find_cretor_med = mysql_query("select * from general_media where creator_info='".$new_creator."' OR from_info='".$new_creator."'");
							if(mysql_num_rows($find_cretor_med)>0){
								while($res_new_det = mysql_fetch_assoc($find_cretor_med)){
									$get_media_artist_project1 = $res_new_det;
								}
							}
						}
					}
					//var_dump($get_media_artist_project1);
					$query = mysql_query("select * from artist_event where creators_info ='".$creator_info."'");
					if(mysql_num_rows($query)>0){
						while($row = mysql_fetch_assoc($query)){
							$new_creator = "artist_event|".$row['id'];
							$find_cretor_med = mysql_query("select * from general_media where creator_info='".$new_creator."' OR from_info='".$new_creator."'");
							if(mysql_num_rows($find_cretor_med)>0){
								while($res_new_det = mysql_fetch_assoc($find_cretor_med)){
									$get_media_artist_project2 = $res_new_det;
								}
							}
						}
					}
					//var_dump($get_media_artist_project2);
					$query = mysql_query("select * from community_project where creators_info ='".$creator_info."'");
					if(mysql_num_rows($query)>0){
						while($row = mysql_fetch_assoc($query)){
							$new_creator = "community_project|".$row['id'];
							$find_cretor_med = mysql_query("select * from general_media where creator_info='".$new_creator."' OR from_info='".$new_creator."'");
							if(mysql_num_rows($find_cretor_med)>0){
								while($res_new_det = mysql_fetch_assoc($find_cretor_med)){
									$get_media_artist_project3 = $res_new_det;
								}
							}
						}
					}
					//var_dump($get_media_artist_project3);
					$query = mysql_query("select * from community_event where creators_info ='".$creator_info."'");
					if(mysql_num_rows($query)>0){
						while($row = mysql_fetch_assoc($query)){
							$new_creator = "community_event|".$row['id'];
							$find_cretor_med = mysql_query("select * from general_media where creator_info='".$new_creator."' OR from_info='".$new_creator."'");
							if(mysql_num_rows($find_cretor_med)>0){
								while($res_new_det = mysql_fetch_assoc($find_cretor_med)){
									$get_media_artist_project4 = $res_new_det;
								}
							}
						}
					}
					//var_dump($get_media_artist_project4);
				}
			}
			
			/**get artist projects ends here**//*
		$get_media = array_merge($get_media_art_pro1,$get_media_art_eve1,$get_media_com_eve1,$get_media_com_pro1,$get_media_art_pro1,$get_media_art_eve2,$get_media_com_eve2,$get_media_com_pro2,$get_media_artist_project1,$get_media_artist_project2,$get_media_artist_project3,$get_media_artist_project4,$get_media_community_project1,$get_media_community_project2,$get_media_community_project3,$get_media_community_project4);
		//var_dump($get_media);
		return($get_media);
	}
	/*function get_tag_where_heis($email){
		$get_det = $this->sel_general();
		$get_media_artist_project1 = array();
		$get_media_artist_event1 = array();
		$get_media_community_project1 = array();
		$get_media_community_event1 = array();
			
			/**get community projects**//*
			$get_all_tag_det = mysql_query("select * from community_project where tagged_user_email LIKE '%".$email."%'");
			if(mysql_num_rows($get_all_tag_det)>0){
				while($row_res = mysql_fetch_assoc($get_all_tag_det)){
					$creator_info = "community_project|".$row_res['id'];
					$find_cretor_med = mysql_query("select * from general_media where creator_info='".$creator_info."' OR from_info='".$creator_info."'");
					if(mysql_num_rows($find_cretor_med)>0){
						while($res_new_det = mysql_fetch_assoc($find_cretor_med)){
							$get_media_community_project1 = $res_new_det;
						}
					}
				}
			}
			
			$get_all_tag_det_com_eve = mysql_query("select * from community_event where tagged_user_email LIKE '%".$email."%'");
			if(mysql_num_rows($get_all_tag_det_com_eve)>0){
				while($row_res_com_eve = mysql_fetch_assoc($get_all_tag_det_com_eve)){
					$creator_info = "community_event|".$row_res_com_eve['id'];
					$find_cretor_med = mysql_query("select * from general_media where creator_info='".$creator_info."' OR from_info='".$creator_info."'");
					if(mysql_num_rows($find_cretor_med)>0){
						while($res_new_det = mysql_fetch_assoc($find_cretor_med)){
							$get_media_community_event1 = $res_new_det;
						}
					}
				}
			}
			
			$get_all_tag_det_art_pro = mysql_query("select * from artist_project where tagged_user_email LIKE '%".$email."%'");
			if(mysql_num_rows($get_all_tag_det_art_pro)>0){
				while($row_res_art_pro = mysql_fetch_assoc($get_all_tag_det_art_pro)){
					$creator_info = "artist_project|".$row_res_art_pro['id'];
					$find_cretor_med = mysql_query("select * from general_media where creator_info='".$creator_info."' OR from_info='".$creator_info."'");
					if(mysql_num_rows($find_cretor_med)>0){
						while($res_new_det = mysql_fetch_assoc($find_cretor_med)){
							$get_media_artist_project1 = $res_new_det;
						}
					}
				}
			}
			
			$get_all_tag_det_art_eve = mysql_query("select * from artist_event where tagged_user_email LIKE '%".$email."%'");
			if(mysql_num_rows($get_all_tag_det_art_eve)>0){
				while($row_res_art_eve = mysql_fetch_assoc($get_all_tag_det_art_eve)){
					$creator_info = "artist_event|".$row_res_art_eve['id'];
					$find_cretor_med = mysql_query("select * from general_media where creator_info='".$creator_info."' OR from_info='".$creator_info."'");
					if(mysql_num_rows($find_cretor_med)>0){
						while($res_new_det = mysql_fetch_assoc($find_cretor_med)){
							$get_media_artist_event1 = $res_new_det;
						}
					}
				}
			}

		$get_media = array_merge($get_media_artist_project1,$get_media_artist_event1,$get_media_community_project1,$get_media_community_event1);
		return($get_media);
	}*/
	
	
	
	function get_media_where_creator_or_from()
	{
		$get_art_media1 = array();
		$get_art_media2 = array();
		$get_art_media3 = array();
		$get_art_media4 = array();
		$get_art_media5 = array();
		$get_art_media6 = array();
		
		$get_gen_det = $this->sel_general();
		if($get_gen_det['artist_id']>0){
			$creator_info = "general_artist|".$get_gen_det['artist_id'];
			$query = mysql_query("select * from general_media where creator_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
			if(mysql_num_rows($query)>0){
				while($row_art = mysql_fetch_assoc($query)){
					if($get_gen_det['general_user_id']!= $row_art['general_user_id']){
						$get_art_media1[] = $row_art;
					}
				}
			}
			$get_creator = mysql_query("select * from artist_project where artist_id='".$get_gen_det['artist_id']."' AND del_status=0 AND active=0");
			if(mysql_num_rows($get_creator)>0)
			{
				while($row_art_pro = mysql_fetch_assoc($get_creator))
				{
					$creator_info = "artist_project|".$row_art_pro["id"];
					$query1 = mysql_query("select * from general_media where creator_info = '".$creator_info."' OR from_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
					if(mysql_num_rows($query1)>0){
						while($row_art_media = mysql_fetch_assoc($query1)){
							if($get_gen_det['general_user_id']!= $row_art_media['general_user_id']){
								$get_art_media2[] = $row_art_media;
							}
						}
					}
				}
			}
			$get_creator = mysql_query("select * from artist_event where artist_id='".$get_gen_det['artist_id']."' AND del_status=0 AND active=0");
			if(mysql_num_rows($get_creator)>0)
			{
				while($row_art_eve = mysql_fetch_assoc($get_creator))
				{
					$creator_info = "artist_event|".$row_art_eve["id"];
					$query1 = mysql_query("select * from general_media where creator_info = '".$creator_info."' OR from_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
					if(mysql_num_rows($query1)>0){
						while($row_art_media = mysql_fetch_assoc($query1)){
							if($get_gen_det['general_user_id']!= $row_art_media['general_user_id']){
								$get_art_media3[] = $row_art_media;
							}
						}
					}
				}
			}
		}
		if($get_gen_det['community_id']>0){
			$creator_info = "general_community|".$get_gen_det['community_id'];
			$query = mysql_query("select * from general_media where creator_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
			if(mysql_num_rows($query)>0){
				while($row_com = mysql_fetch_assoc($query)){
					if($get_gen_det['general_user_id']!= $row_com['general_user_id']){
						$get_art_media4[] = $row_com;
					}
				}
			}
			$get_creator = mysql_query("select * from community_project where community_id='".$get_gen_det['community_id']."' AND del_status=0 AND active=0");
			if(mysql_num_rows($get_creator)>0)
			{
				while($row_com_pro = mysql_fetch_assoc($get_creator))
				{
					$creator_info = "community_project|".$row_com_pro["id"];
					$query1 = mysql_query("select * from general_media where creator_info = '".$creator_info."' OR from_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
					if(mysql_num_rows($query1)>0){
						while($row_com_media = mysql_fetch_assoc($query1)){
							if($get_gen_det['general_user_id']!= $row_com_media['general_user_id']){
								$get_art_media5[] = $row_com_media;
							}
						}
					}
				}
			}
			$get_creator = mysql_query("select * from community_event where community_id='".$get_gen_det['community_id']."' AND del_status=0 AND active=0");
			if(mysql_num_rows($get_creator)>0)
			{
				while($row_com_eve = mysql_fetch_assoc($get_creator))
				{
					$creator_info = "community_event|".$row_com_eve["id"];
					$query1 = mysql_query("select * from general_media where creator_info = '".$creator_info."' OR from_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
					if(mysql_num_rows($query1)>0){
						while($row_com_media = mysql_fetch_assoc($query1)){
							if($get_gen_det['general_user_id']!= $row_com_media['general_user_id']){
								$get_art_media6[] = $row_com_media;
							}
						}
					}
				}
			}
		}
		
		$all_media = array_merge($get_art_media1,$get_art_media2,$get_art_media3,$get_art_media4,$get_art_media5,$get_art_media6);
		return($all_media);
	}
	
	function get_creator_heis()
	{
		$id = $this->sel_general();
        $var_type_1 = 'general_artist';
        $var_type_2 = 'general_community';
        $var_type_3 = 'artist_project';
        $var_type_4 = 'community_project';
		$res_media1 = array();
		$res_media2 = array();
		$res_media3 = array();
		$res_media4 = array();
		$res_media5 = array();
		$res_media6 = array();
		$res_media7 = array();
		$res_media8 = array();
		$res_media9 = array();
		$res_media10 = array();
		$res_media11 = array();
		$res_media12 = array();
		$res_media13 = array();
		$res_media14 = array();
		$res_media15 = array();
		$res_media16 = array();
        if($id['artist_id']!=0 && !empty($id['artist_id']))
        {
            $chk_type = $var_type_1.'|'.$id['artist_id'];
			//echo "select * from artist_project where creators_info='".$chk_type."'";
			$sql_get_pro = mysql_query("select * from artist_project where creators_info='".$chk_type."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_get_pro)>0)
			{
				while($row = mysql_fetch_assoc($sql_get_pro))
				{
					$creator_info = "artist_project|".$row['id'];
					//echo "select * from general_media where creator_info = '".$creator_info."'";
					$get_media = mysql_query("select * from general_media where creator_info = '".$creator_info."'  AND delete_status=0 AND media_status=0");
					if(mysql_num_rows($get_media)>0)
					{
						while($res = mysql_fetch_assoc($get_media))
						{
							$res_media1[] = $res;
						}
					}
				}
			}
			$sql_get_pro1 = mysql_query("select * from community_project where creators_info='".$chk_type."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_get_pro1)>0)
			{
				while($row1 = mysql_fetch_assoc($sql_get_pro1))
				{
					$creator_info = "community_project|".$row1['id'];
					$get_media = mysql_query("select * from general_media where creator_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
					if(mysql_num_rows($get_media)>0)
					{
						while($res = mysql_fetch_assoc($get_media))
						{
							$res_media2[] = $res;
						}
					}
				}
			}
			/*$sql_get_pro2 = mysql_query("select * from artist_event where creators_info='".$chk_type."'");
			if(mysql_num_rows($sql_get_pro2)>0)
			{
				while($row = mysql_fetch_assoc($sql_get_pro2))
				{
					$creator_info = "artist_event|".$row['id'];
					$get_media = mysql_query("select * from general_media where creator_info = '".$creator_info."'");
					if(mysql_num_rows($get_media)>0)
					{
						while($res = mysql_fetch_assoc($get_media))
						{
							$res_media3[] = $res;
						}
					}
				}
			}
			$sql_get_pro3 = mysql_query("select * from community_event where creators_info='".$chk_type."'");
			if(mysql_num_rows($sql_get_pro3)>0)
			{
				while($row1 = mysql_fetch_assoc($sql_get_pro3))
				{
					$creator_info = "community_event|".$row1['id'];
					$get_media = mysql_query("select * from general_media where creator_info = '".$creator_info."'");
					if(mysql_num_rows($get_media)>0)
					{
						while($res = mysql_fetch_assoc($get_media))
						{
							$res_media4[] = $res;
						}
					}
				}
			}*/
			
			$find_project = mysql_query("select * from artist_project where artist_id='".$id['artist_id']."' AND del_status=0 AND active=0");
			if(mysql_num_rows($find_project)>0)
			{
				while($get_all_pro = mysql_fetch_assoc($find_project))
				{
					$new_creator_info = "artist_project|".$get_all_pro['id'];
					$sql_get_pro = mysql_query("select * from artist_project where creators_info='".$new_creator_info."' AND del_status=0 AND active=0");
					if(mysql_num_rows($sql_get_pro)>0)
					{
						while($row = mysql_fetch_assoc($sql_get_pro))
						{
							$creator_info = "artist_project|".$row['id'];
							$get_media = mysql_query("select * from general_media where creator_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
							if(mysql_num_rows($get_media)>0)
							{
								while($res = mysql_fetch_assoc($get_media))
								{
									$res_media5[] = $res;
								}
							}
						}
					}
					$sql_get_pro1 = mysql_query("select * from community_project where creators_info='".$new_creator_info."' AND del_status=0 AND active=0");
					if(mysql_num_rows($sql_get_pro1)>0)
					{
						while($row1 = mysql_fetch_assoc($sql_get_pro1))
						{
							$creator_info = "community_project|".$row1['id'];
							$get_media = mysql_query("select * from general_media where creator_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
							if(mysql_num_rows($get_media)>0)
							{
								while($res = mysql_fetch_assoc($get_media))
								{
									$res_media6[] = $res;
								}
							}
						}
					}
					$sql_get_pro2 = mysql_query("select * from artist_event where creators_info='".$new_creator_info."' AND del_status=0 AND active=0");
					if(mysql_num_rows($sql_get_pro2)>0)
					{
						while($row = mysql_fetch_assoc($sql_get_pro2))
						{
							$creator_info = "artist_event|".$row['id'];
							$get_media = mysql_query("select * from general_media where creator_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
							if(mysql_num_rows($get_media)>0)
							{
								while($res = mysql_fetch_assoc($get_media))
								{
									$res_media7[] = $res;
								}
							}
						}
					}
					$sql_get_pro3 = mysql_query("select * from community_event where creators_info='".$new_creator_info."' AND del_status=0 AND active=0");
					if(mysql_num_rows($sql_get_pro3)>0)
					{
						while($row1 = mysql_fetch_assoc($sql_get_pro3))
						{
							$creator_info = "community_event|".$row1['id'];
							$get_media = mysql_query("select * from general_media where creator_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
							if(mysql_num_rows($get_media)>0)
							{
								while($res = mysql_fetch_assoc($get_media))
								{
									$res_media8[] = $res;
								}
							}
						}
					}
				}
			}
		   
		}
		if($id['community_id']!=0 && !empty($id['community_id']))
        {
            $chk_type = $var_type_2.'|'.$id['community_id'];
			$sql_get_pro = mysql_query("select * from artist_project where creators_info='".$chk_type."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_get_pro)>0)
			{
				while($row = mysql_fetch_assoc($sql_get_pro))
				{
					$creator_info = "artist_project|".$row['id'];
					$get_media = mysql_query("select * from general_media where creator_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
					if(mysql_num_rows($get_media)>0)
					{
						while($res = mysql_fetch_assoc($get_media))
						{
							$res_media9[] = $res;
						}
					}
				}
			}
			$sql_get_pro1 = mysql_query("select * from community_project where creators_info='".$chk_type."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_get_pro1)>0)
			{
				while($row1 = mysql_fetch_assoc($sql_get_pro1))
				{
					$creator_info = "community_project|".$row1['id'];
					$get_media = mysql_query("select * from general_media where creator_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
					if(mysql_num_rows($get_media)>0)
					{
						while($res = mysql_fetch_assoc($get_media))
						{
							$res_media10[] = $res;
						}
					}
				}
			}
			/*$sql_get_pro2 = mysql_query("select * from artist_event where creators_info='".$chk_type."'");
			if(mysql_num_rows($sql_get_pro2)>0)
			{
				while($row = mysql_fetch_assoc($sql_get_pro2))
				{
					$creator_info = "artist_event|".$row['id'];
					$get_media = mysql_query("select * from general_media where creator_info = '".$creator_info."'");
					if(mysql_num_rows($get_media)>0)
					{
						while($res = mysql_fetch_assoc($get_media))
						{
							$res_media11[] = $res;
						}
					}
				}
			}
			$sql_get_pro3 = mysql_query("select * from community_event where creators_info='".$chk_type."'");
			if(mysql_num_rows($sql_get_pro3)>0)
			{
				while($row1 = mysql_fetch_assoc($sql_get_pro3))
				{
					$creator_info = "community_event|".$row1['id'];
					$get_media = mysql_query("select * from general_media where creator_info = '".$creator_info."'");
					if(mysql_num_rows($get_media)>0)
					{
						while($res = mysql_fetch_assoc($get_media))
						{
							$res_media12[] = $res;
						}
					}
				}
			}*/
			$find_project = mysql_query("select * from community_project where community_id='".$id['community_id']."' AND del_status=0 AND active=0");
			if(mysql_num_rows($find_project)>0)
			{
				while($get_all_pro = mysql_fetch_assoc($find_project))
				{
					$new_creator_info = "community_project|".$get_all_pro['id'];
					$sql_get_pro = mysql_query("select * from artist_project where creators_info='".$new_creator_info."' AND del_status=0 AND active=0");
					if(mysql_num_rows($sql_get_pro)>0)
					{
						while($row = mysql_fetch_assoc($sql_get_pro))
						{
							$creator_info = "artist_project|".$row['id'];
							$get_media = mysql_query("select * from general_media where creator_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
							if(mysql_num_rows($get_media)>0)
							{
								while($res = mysql_fetch_assoc($get_media))
								{
									$res_media13[] = $res;
								}
							}
						}
					}
					$sql_get_pro1 = mysql_query("select * from community_project where creators_info='".$new_creator_info."' AND del_status=0 AND active=0");
					if(mysql_num_rows($sql_get_pro1)>0)
					{
						while($row1 = mysql_fetch_assoc($sql_get_pro1))
						{
							$creator_info = "community_project|".$row1['id'];
							$get_media = mysql_query("select * from general_media where creator_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
							if(mysql_num_rows($get_media)>0)
							{
								while($res = mysql_fetch_assoc($get_media))
								{
									$res_media14[] = $res;
								}
							}
						}
					}
					/*$sql_get_pro2 = mysql_query("select * from artist_event where creators_info='".$new_creator_info."'");
					if(mysql_num_rows($sql_get_pro2)>0)
					{
						while($row = mysql_fetch_assoc($sql_get_pro2))
						{
							$creator_info = "artist_event|".$row['id'];
							$get_media = mysql_query("select * from general_media where creator_info = '".$creator_info."'");
							if(mysql_num_rows($get_media)>0)
							{
								while($res = mysql_fetch_assoc($get_media))
								{
									$res_media15[] = $res;
								}
							}
						}
					}
					$sql_get_pro3 = mysql_query("select * from community_event where creators_info='".$new_creator_info."'");
					if(mysql_num_rows($sql_get_pro3)>0)
					{
						while($row1 = mysql_fetch_assoc($sql_get_pro3))
						{
							$creator_info = "community_event|".$row1['id'];
							$get_media = mysql_query("select * from general_media where creator_info = '".$creator_info."'");
							if(mysql_num_rows($get_media)>0)
							{
								while($res = mysql_fetch_assoc($get_media))
								{
									$res_media16[] = $res;
								}
							}
						}
					}*/
				}
			}
		   
		}
		   
		$get_all_media = array_merge($res_media1,$res_media2,$res_media3,$res_media4,$res_media5,$res_media6,$res_media7,$res_media8,$res_media9,$res_media10,$res_media11,$res_media12,$res_media13,$res_media14,$res_media15,$res_media16);
		return($get_all_media);
	}
	function get_from_heis()
	{
		$id = $this->sel_general();
        $var_type_1 = 'general_artist';
        $var_type_2 = 'general_community';
        $var_type_3 = 'artist_project';
        $var_type_4 = 'community_project';
		$res_media1 = array();
		$res_media2 = array();
		$res_media3 = array();
		$res_media4 = array();
		$res_media5 = array();
		$res_media6 = array();
		$res_media7 = array();
		$res_media8 = array();
		$res_media9 = array();
		$res_media10 = array();
		$res_media11 = array();
		$res_media12 = array();
		$res_media13 = array();
		$res_media14 = array();
		$res_media15 = array();
		$res_media16 = array();
        if($id['artist_id']!=0 && !empty($id['artist_id']))
        {
			$chk_type = $var_type_1.'|'.$id['artist_id'];
			$sql_get_pro = mysql_query("select * from artist_project where creators_info='".$chk_type."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_get_pro)>0)
			{
				while($row = mysql_fetch_assoc($sql_get_pro))
				{
					$creator_info = "artist_project|".$row['id'];
					$get_media = mysql_query("select * from general_media where from_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
					if(mysql_num_rows($get_media)>0)
					{
						while($res = mysql_fetch_assoc($get_media))
						{
							$res_media1[] = $res;
						}
					}
				}
			}
			$sql_get_pro1 = mysql_query("select * from community_project where creators_info='".$chk_type."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_get_pro1)>0)
			{
				while($row1 = mysql_fetch_assoc($sql_get_pro1))
				{
					$creator_info = "community_project|".$row1['id'];
					$get_media = mysql_query("select * from general_media where from_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
					if(mysql_num_rows($get_media)>0)
					{
						while($res = mysql_fetch_assoc($get_media))
						{
							$res_media2[] = $res;
						}
					}
				}
			}
			$sql_get_pro1 = mysql_query("select * from community_event where creators_info='".$chk_type."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_get_pro1)>0)
			{
				while($row1 = mysql_fetch_assoc($sql_get_pro1))
				{
					$creator_info = "community_event|".$row1['id'];
					$get_media = mysql_query("select * from general_media where from_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
					if(mysql_num_rows($get_media)>0)
					{
						while($res = mysql_fetch_assoc($get_media))
						{
							$res_media3[] = $res;
						}
					}
				}
			}
			$sql_get_pro1 = mysql_query("select * from artist_event where creators_info='".$chk_type."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_get_pro1)>0)
			{
				while($row1 = mysql_fetch_assoc($sql_get_pro1))
				{
					$creator_info = "artist_event|".$row1['id'];
					$get_media = mysql_query("select * from general_media where from_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
					if(mysql_num_rows($get_media)>0)
					{
						while($res = mysql_fetch_assoc($get_media))
						{
							$res_media4[] = $res;
						}
					}
				}
			}
			
			
			$find_project = mysql_query("select * from artist_project where artist_id='".$id['artist_id']."' AND del_status=0 AND active=0");
			if(mysql_num_rows($find_project)>0)
			{
				while($get_all_pro = mysql_fetch_assoc($find_project))
				{
					$new_creator_info = "artist_project|".$get_all_pro['id'];
					//echo "select * from artist_project where creators_info='".$new_creator_info."'";
					$sql_get_pro = mysql_query("select * from artist_project where creators_info='".$new_creator_info."' AND del_status=0 AND active=0");
					if(mysql_num_rows($sql_get_pro)>0)
					{
						while($row = mysql_fetch_assoc($sql_get_pro))
						{
							$creator_info = "artist_project|".$row['id'];
							$get_media = mysql_query("select * from general_media where from_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
							if(mysql_num_rows($get_media)>0)
							{
								while($res = mysql_fetch_assoc($get_media))
								{
									if($id['general_user_id'] != $res['general_user_id'])
									{
										$res_media5[] = $res;	}
								}
							}
						}
					}
					$sql_get_pro1 = mysql_query("select * from community_project where creators_info='".$new_creator_info."' AND del_status=0 AND active=0");
					if(mysql_num_rows($sql_get_pro1)>0)
					{
						while($row1 = mysql_fetch_assoc($sql_get_pro1))
						{
							$creator_info = "community_project|".$row1['id'];
							$get_media = mysql_query("select * from general_media where from_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
							if(mysql_num_rows($get_media)>0)
							{
								while($res = mysql_fetch_assoc($get_media))
								{
									if($id['general_user_id'] != $res['general_user_id'])
									{
										$res_media6[] = $res;}
								}
							}
						}
					}
					$sql_get_pro2 = mysql_query("select * from artist_event where creators_info='".$new_creator_info."' AND del_status=0 AND active=0");
					if(mysql_num_rows($sql_get_pro2)>0)
					{
						while($row = mysql_fetch_assoc($sql_get_pro2))
						{
							$creator_info = "artist_event|".$row['id'];
							$get_media = mysql_query("select * from general_media where from_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
							if(mysql_num_rows($get_media)>0)
							{
								while($res = mysql_fetch_assoc($get_media))
								{
									if($id['general_user_id'] != $res['general_user_id'])
									{
										$res_media7[] = $res;}
								}
							}
						}
					}
					$sql_get_pro3 = mysql_query("select * from community_event where creators_info='".$new_creator_info."' AND del_status=0 AND active=0");
					if(mysql_num_rows($sql_get_pro3)>0)
					{
						while($row1 = mysql_fetch_assoc($sql_get_pro3))
						{
							$creator_info = "community_event|".$row1['id'];
							$get_media = mysql_query("select * from general_media where from_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
							if(mysql_num_rows($get_media)>0)
							{
								while($res = mysql_fetch_assoc($get_media))
								{
									if($id['general_user_id'] != $res['general_user_id'])
									{
										$res_media8[] = $res;}
								}
							}
						}
					}
				}
			}
			$find_project = mysql_query("select * from artist_event where artist_id='".$id['artist_id']."' AND del_status=0 AND active=0");
			if(mysql_num_rows($find_project)>0)
			{
				while($get_all_pro = mysql_fetch_assoc($find_project))
				{
					$new_creator_info = "artist_event|".$get_all_pro['id'];
					//echo "select * from artist_project where creators_info='".$new_creator_info."'";
					$sql_get_pro = mysql_query("select * from artist_project where creators_info='".$new_creator_info."' AND del_status=0 AND active=0");
					if(mysql_num_rows($sql_get_pro)>0)
					{
						while($row = mysql_fetch_assoc($sql_get_pro))
						{
							$creator_info = "artist_project|".$row['id'];
							$get_media = mysql_query("select * from general_media where from_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
							if(mysql_num_rows($get_media)>0)
							{
								while($res = mysql_fetch_assoc($get_media))
								{
									if($id['general_user_id'] != $res['general_user_id'])
									{
										$res_media5[] = $res;	}
								}
							}
						}
					}
					$sql_get_pro1 = mysql_query("select * from community_project where creators_info='".$new_creator_info."' AND del_status=0 AND active=0");
					if(mysql_num_rows($sql_get_pro1)>0)
					{
						while($row1 = mysql_fetch_assoc($sql_get_pro1))
						{
							$creator_info = "community_project|".$row1['id'];
							$get_media = mysql_query("select * from general_media where from_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
							if(mysql_num_rows($get_media)>0)
							{
								while($res = mysql_fetch_assoc($get_media))
								{
									if($id['general_user_id'] != $res['general_user_id'])
									{
										$res_media6[] = $res;}
								}
							}
						}
					}
					$sql_get_pro2 = mysql_query("select * from artist_event where creators_info='".$new_creator_info."' AND del_status=0 AND active=0");
					if(mysql_num_rows($sql_get_pro2)>0)
					{
						while($row = mysql_fetch_assoc($sql_get_pro2))
						{
							$creator_info = "artist_event|".$row['id'];
							$get_media = mysql_query("select * from general_media where from_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
							if(mysql_num_rows($get_media)>0)
							{
								while($res = mysql_fetch_assoc($get_media))
								{
									if($id['general_user_id'] != $res['general_user_id'])
									{
										$res_media7[] = $res;}
								}
							}
						}
					}
					$sql_get_pro3 = mysql_query("select * from community_event where creators_info='".$new_creator_info."' AND del_status=0 AND active=0");
					if(mysql_num_rows($sql_get_pro3)>0)
					{
						while($row1 = mysql_fetch_assoc($sql_get_pro3))
						{
							$creator_info = "community_event|".$row1['id'];
							$get_media = mysql_query("select * from general_media where from_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
							if(mysql_num_rows($get_media)>0)
							{
								while($res = mysql_fetch_assoc($get_media))
								{
									if($id['general_user_id'] != $res['general_user_id'])
									{
										$res_media8[] = $res;}
								}
							}
						}
					}
				}
			}
		   
		}
		if($id['community_id']!=0 && !empty($id['community_id']))
        {
		
			$chk_type = $var_type_2.'|'.$id['community_id'];
			$sql_get_pro = mysql_query("select * from artist_project where creators_info='".$chk_type."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_get_pro)>0)
			{
				while($row = mysql_fetch_assoc($sql_get_pro))
				{
					$creator_info = "artist_project|".$row['id'];
					$get_media = mysql_query("select * from general_media where from_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
					if(mysql_num_rows($get_media)>0)
					{
						while($res = mysql_fetch_assoc($get_media))
						{
							$res_media9[] = $res;
						}
					}
				}
			}
			$sql_get_pro1 = mysql_query("select * from community_project where creators_info='".$chk_type."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_get_pro1)>0)
			{
				while($row1 = mysql_fetch_assoc($sql_get_pro1))
				{
					$creator_info = "community_project|".$row1['id'];
					$get_media = mysql_query("select * from general_media where from_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
					if(mysql_num_rows($get_media)>0)
					{
						while($res = mysql_fetch_assoc($get_media))
						{
							$res_media10[] = $res;
						}
					}
				}
			}
			$sql_get_pro1 = mysql_query("select * from community_event where creators_info='".$chk_type."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_get_pro1)>0)
			{
				while($row1 = mysql_fetch_assoc($sql_get_pro1))
				{
					$creator_info = "community_event|".$row1['id'];
					$get_media = mysql_query("select * from general_media where from_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
					if(mysql_num_rows($get_media)>0)
					{
						while($res = mysql_fetch_assoc($get_media))
						{
							$res_media11[] = $res;
						}
					}
				}
			}
			$sql_get_pro1 = mysql_query("select * from artist_event where creators_info='".$chk_type."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_get_pro1)>0)
			{
				while($row1 = mysql_fetch_assoc($sql_get_pro1))
				{
					$creator_info = "artist_event|".$row1['id'];
					$get_media = mysql_query("select * from general_media where from_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
					if(mysql_num_rows($get_media)>0)
					{
						while($res = mysql_fetch_assoc($get_media))
						{
							$res_media12[] = $res;
						}
					}
				}
			}
		
			$find_project = mysql_query("select * from community_project where community_id='".$id['community_id']."' AND del_status=0 AND active=0");
			if(mysql_num_rows($find_project)>0)
			{
				while($get_all_pro = mysql_fetch_assoc($find_project))
				{
					$new_creator_info = "community_project|".$get_all_pro['id'];
					$sql_get_pro = mysql_query("select * from artist_project where creators_info='".$new_creator_info."' AND del_status=0 AND active=0");
					if(mysql_num_rows($sql_get_pro)>0)
					{
						while($row = mysql_fetch_assoc($sql_get_pro))
						{
							$creator_info = "artist_project|".$row['id'];
							$get_media = mysql_query("select * from general_media where from_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
							if(mysql_num_rows($get_media)>0)
							{
								while($res = mysql_fetch_assoc($get_media))
								{
									if($id['general_user_id'] != $res['general_user_id'])
									{
										$res_media13[] = $res;}
								}
							}
						}
					}
					$sql_get_pro1 = mysql_query("select * from community_project where creators_info='".$new_creator_info."' AND del_status=0 AND active=0");
					if(mysql_num_rows($sql_get_pro1)>0)
					{
						while($row1 = mysql_fetch_assoc($sql_get_pro1))
						{
							$creator_info = "community_project|".$row1['id'];
							$get_media = mysql_query("select * from general_media where from_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
							if(mysql_num_rows($get_media)>0)
							{
								while($res = mysql_fetch_assoc($get_media))
								{
									if($id['general_user_id'] != $res['general_user_id'])
									{
										$res_media14[] = $res;}
								}
							}
						}
					}
					$sql_get_pro2 = mysql_query("select * from artist_event where creators_info='".$new_creator_info."' AND del_status=0 AND active=0");
					if(mysql_num_rows($sql_get_pro2)>0)
					{
						while($row = mysql_fetch_assoc($sql_get_pro2))
						{
							$creator_info = "artist_event|".$row['id'];
							$get_media = mysql_query("select * from general_media where from_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
							if(mysql_num_rows($get_media)>0)
							{
								while($res = mysql_fetch_assoc($get_media))
								{
									if($id['general_user_id'] != $res['general_user_id'])
									{
										$res_media15[] = $res;}
								}
							}
						}
					}
					$sql_get_pro3 = mysql_query("select * from community_event where creators_info='".$new_creator_info."' AND del_status=0 AND active=0");
					if(mysql_num_rows($sql_get_pro3)>0)
					{
						while($row1 = mysql_fetch_assoc($sql_get_pro3))
						{
							$creator_info = "community_event|".$row1['id'];
							$get_media = mysql_query("select * from general_media where from_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
							if(mysql_num_rows($get_media)>0)
							{
								while($res = mysql_fetch_assoc($get_media))
								{
									if($id['general_user_id'] != $res['general_user_id'])
									{
										$res_media16[] = $res;}
								}
							}
						}
					}
				}
			}
			$find_project = mysql_query("select * from community_event where community_id='".$id['community_id']."' AND del_status=0 AND active=0");
			if(mysql_num_rows($find_project)>0)
			{
				while($get_all_pro = mysql_fetch_assoc($find_project))
				{
					$new_creator_info = "community_event|".$get_all_pro['id'];
					$sql_get_pro = mysql_query("select * from artist_project where creators_info='".$new_creator_info."' AND del_status=0 AND active=0");
					if(mysql_num_rows($sql_get_pro)>0)
					{
						while($row = mysql_fetch_assoc($sql_get_pro))
						{
							$creator_info = "artist_project|".$row['id'];
							$get_media = mysql_query("select * from general_media where from_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
							if(mysql_num_rows($get_media)>0)
							{
								while($res = mysql_fetch_assoc($get_media))
								{
									if($id['general_user_id'] != $res['general_user_id'])
									{
										$res_media13[] = $res;}
								}
							}
						}
					}
					$sql_get_pro1 = mysql_query("select * from community_project where creators_info='".$new_creator_info."' AND del_status=0 AND active=0");
					if(mysql_num_rows($sql_get_pro1)>0)
					{
						while($row1 = mysql_fetch_assoc($sql_get_pro1))
						{
							$creator_info = "community_project|".$row1['id'];
							$get_media = mysql_query("select * from general_media where from_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
							if(mysql_num_rows($get_media)>0)
							{
								while($res = mysql_fetch_assoc($get_media))
								{
									if($id['general_user_id'] != $res['general_user_id'])
									{
										$res_media14[] = $res;}
								}
							}
						}
					}
					$sql_get_pro2 = mysql_query("select * from artist_event where creators_info='".$new_creator_info."' AND del_status=0 AND active=0");
					if(mysql_num_rows($sql_get_pro2)>0)
					{
						while($row = mysql_fetch_assoc($sql_get_pro2))
						{
							$creator_info = "artist_event|".$row['id'];
							$get_media = mysql_query("select * from general_media where from_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
							if(mysql_num_rows($get_media)>0)
							{
								while($res = mysql_fetch_assoc($get_media))
								{
									if($id['general_user_id'] != $res['general_user_id'])
									{
										$res_media15[] = $res;}
								}
							}
						}
					}
					$sql_get_pro3 = mysql_query("select * from community_event where creators_info='".$new_creator_info."' AND del_status=0 AND active=0");
					if(mysql_num_rows($sql_get_pro3)>0)
					{
						while($row1 = mysql_fetch_assoc($sql_get_pro3))
						{
							$creator_info = "community_event|".$row1['id'];
							$get_media = mysql_query("select * from general_media where from_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
							if(mysql_num_rows($get_media)>0)
							{
								while($res = mysql_fetch_assoc($get_media))
								{
									if($id['general_user_id'] != $res['general_user_id'])
									{
										$res_media16[] = $res;}
								}
							}
						}
					}
				}
			}
		   
		}
		   
		$get_all_media = array_merge($res_media1,$res_media2,$res_media3,$res_media4,$res_media5,$res_media6,$res_media7,$res_media8,$res_media9,$res_media10,$res_media11,$res_media12,$res_media13,$res_media14,$res_media15,$res_media16);
		return($get_all_media);
	}
	
	/*
	OLd Function 
	function get_from_heis()
	{
		$id = $this->sel_general();
        $var_type_1 = 'general_artist';
        $var_type_2 = 'general_community';
        $var_type_3 = 'artist_project';
        $var_type_4 = 'community_project';
		$res_media1 = array();
		$res_media2 = array();
		$res_media3 = array();
		$res_media4 = array();
		$res_media5 = array();
		$res_media6 = array();
		$res_media7 = array();
		$res_media8 = array();
		$res_media9 = array();
		$res_media10 = array();
		$res_media11 = array();
		$res_media12 = array();
		$res_media13 = array();
		$res_media14 = array();
		$res_media15 = array();
		$res_media16 = array();
        if($id['artist_id']!=0 && !empty($id['artist_id']))
        {
			$find_project = mysql_query("select * from artist_project where artist_id='".$id['artist_id']."'");
			if(mysql_num_rows($find_project)>0)
			{
				while($get_all_pro = mysql_fetch_assoc($find_project))
				{
					$new_creator_info = "artist_project|".$get_all_pro['id'];
					$sql_get_pro = mysql_query("select * from artist_project where creators_info='".$new_creator_info."'");
					if(mysql_num_rows($sql_get_pro)>0)
					{
						while($row = mysql_fetch_assoc($sql_get_pro))
						{
							$creator_info = "artist_project|".$row['id'];
							$get_media = mysql_query("select * from general_media where from_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
							if(mysql_num_rows($get_media)>0)
							{
								while($res = mysql_fetch_assoc($get_media))
								{
									$res_media5[] = $res;
								}
							}
						}
					}
					$sql_get_pro1 = mysql_query("select * from community_project where creators_info='".$new_creator_info."'");
					if(mysql_num_rows($sql_get_pro1)>0)
					{
						while($row1 = mysql_fetch_assoc($sql_get_pro1))
						{
							$creator_info = "community_project|".$row1['id'];
							$get_media = mysql_query("select * from general_media where from_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
							if(mysql_num_rows($get_media)>0)
							{
								while($res = mysql_fetch_assoc($get_media))
								{
									$res_media6[] = $res;
								}
							}
						}
					}
					$sql_get_pro2 = mysql_query("select * from artist_event where creators_info='".$new_creator_info."'");
					if(mysql_num_rows($sql_get_pro2)>0)
					{
						while($row = mysql_fetch_assoc($sql_get_pro2))
						{
							$creator_info = "artist_event|".$row['id'];
							$get_media = mysql_query("select * from general_media where from_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
							if(mysql_num_rows($get_media)>0)
							{
								while($res = mysql_fetch_assoc($get_media))
								{
									$res_media7[] = $res;
								}
							}
						}
					}
					$sql_get_pro3 = mysql_query("select * from community_event where creators_info='".$new_creator_info."'");
					if(mysql_num_rows($sql_get_pro3)>0)
					{
						while($row1 = mysql_fetch_assoc($sql_get_pro3))
						{
							$creator_info = "community_event|".$row1['id'];
							$get_media = mysql_query("select * from general_media where from_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
							if(mysql_num_rows($get_media)>0)
							{
								while($res = mysql_fetch_assoc($get_media))
								{
									$res_media8[] = $res;
								}
							}
						}
					}
				}
			}
		   
		}
		if($id['community_id']!=0 && !empty($id['community_id']))
        {
			$find_project = mysql_query("select * from community_project where community_id='".$id['community_id']."'");
			if(mysql_num_rows($find_project)>0)
			{
				while($get_all_pro = mysql_fetch_assoc($find_project))
				{
					$new_creator_info = "community_project|".$get_all_pro['id'];
					$sql_get_pro = mysql_query("select * from artist_project where creators_info='".$new_creator_info."'");
					if(mysql_num_rows($sql_get_pro)>0)
					{
						while($row = mysql_fetch_assoc($sql_get_pro))
						{
							$creator_info = "artist_project|".$row['id'];
							$get_media = mysql_query("select * from general_media where from_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
							if(mysql_num_rows($get_media)>0)
							{
								while($res = mysql_fetch_assoc($get_media))
								{
									$res_media13[] = $res;
								}
							}
						}
					}
					$sql_get_pro1 = mysql_query("select * from community_project where creators_info='".$new_creator_info."'");
					if(mysql_num_rows($sql_get_pro1)>0)
					{
						while($row1 = mysql_fetch_assoc($sql_get_pro1))
						{
							$creator_info = "community_project|".$row1['id'];
							$get_media = mysql_query("select * from general_media where from_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
							if(mysql_num_rows($get_media)>0)
							{
								while($res = mysql_fetch_assoc($get_media))
								{
									$res_media14[] = $res;
								}
							}
						}
					}
					$sql_get_pro2 = mysql_query("select * from artist_event where creators_info='".$new_creator_info."'");
					if(mysql_num_rows($sql_get_pro2)>0)
					{
						while($row = mysql_fetch_assoc($sql_get_pro2))
						{
							$creator_info = "artist_event|".$row['id'];
							$get_media = mysql_query("select * from general_media where from_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
							if(mysql_num_rows($get_media)>0)
							{
								while($res = mysql_fetch_assoc($get_media))
								{
									$res_media15[] = $res;
								}
							}
						}
					}
					$sql_get_pro3 = mysql_query("select * from community_event where creators_info='".$new_creator_info."'");
					if(mysql_num_rows($sql_get_pro3)>0)
					{
						while($row1 = mysql_fetch_assoc($sql_get_pro3))
						{
							$creator_info = "community_event|".$row1['id'];
							$get_media = mysql_query("select * from general_media where from_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
							if(mysql_num_rows($get_media)>0)
							{
								while($res = mysql_fetch_assoc($get_media))
								{
									$res_media16[] = $res;
								}
							}
						}
					}
				}
			}
		   
		}
		   
		$get_all_media = array_merge($res_media5,$res_media6,$res_media7,$res_media8,$res_media13,$res_media14,$res_media15,$res_media16);
		return($get_all_media);
		
	}*/
	function get_the_user_tagin_project($email)
	{
		$id = $this->sel_general();
        $var_type_1 = 'general_artist';
        $var_type_2 = 'general_community';
        $var_type_3 = 'artist_project';
        $var_type_4 = 'community_project';
		$res_media1 = array();
		$res_media2 = array();
		$res_media3 = array();
		$res_media4 = array();

		$get_all_art_project = mysql_query("SELECT * FROM artist_project WHERE tagged_user_email LIKE '%".$email."%' AND del_status=0 AND active=0");
		if(mysql_num_rows($get_all_art_project)>0)
		{
			while($res_data = mysql_fetch_assoc($get_all_art_project))
			{
				$creator_info = "artist_project|".$res_data['id'];
				$get_media = mysql_query("select * from general_media where creator_info ='".$creator_info."' OR from_info ='".$creator_info."' AND delete_status=0 AND media_status=0");
				if(mysql_num_rows($get_media)>0)
				{
					while($res_media_data = mysql_fetch_assoc($get_media))
					{
						$res_media1[] = $res_media_data;
					}
				}
			}
		}
		$get_all_art_event = mysql_query("SELECT * FROM artist_event WHERE tagged_user_email LIKE '%".$email."%' AND del_status=0 AND active=0");
		if(mysql_num_rows($get_all_art_event)>0)
		{
			while($res_data = mysql_fetch_assoc($get_all_art_event))
			{
				$creator_info = "artist_event|".$res_data['id'];
				$get_media = mysql_query("select * from general_media where creator_info ='".$creator_info."' OR from_info ='".$creator_info."' AND delete_status=0 AND media_status=0");
				if(mysql_num_rows($get_media)>0)
				{
					while($res_media_data = mysql_fetch_assoc($get_media))
					{
						$res_media2[] = $res_media_data;
					}
				}
			}
		}
		$get_all_com_event = mysql_query("SELECT * FROM community_event WHERE tagged_user_email LIKE '%".$email."%' AND del_status=0 AND active=0");
		if(mysql_num_rows($get_all_com_event)>0)
		{
			while($res_data = mysql_fetch_assoc($get_all_com_event))
			{
				$creator_info = "community_event|".$res_data['id'];
				$get_media = mysql_query("select * from general_media where creator_info ='".$creator_info."' OR from_info ='".$creator_info."' AND delete_status=0 AND media_status=0");
				if(mysql_num_rows($get_media)>0)
				{
					while($res_media_data = mysql_fetch_assoc($get_media))
					{
						$res_media3[] = $res_media_data;
					}
				}
			}
		}
		$get_all_com_project = mysql_query("SELECT * FROM community_project WHERE tagged_user_email LIKE '%".$email."%' AND del_status=0 AND active=0");
		if(mysql_num_rows($get_all_com_project)>0)
		{
			while($res_data = mysql_fetch_assoc($get_all_com_project))
			{
				$creator_info = "community_project|".$res_data['id'];
				$get_media = mysql_query("select * from general_media where creator_info ='".$creator_info."' OR from_info ='".$creator_info."' AND delete_status=0 AND media_status=0");
				if(mysql_num_rows($get_media)>0)
				{
					while($res_media_data = mysql_fetch_assoc($get_media))
					{
						$res_media4[] = $res_media_data;
					}
				}
			}
		}
	   
		$get_all_media = array_merge($res_media1,$res_media2,$res_media3,$res_media4);
		return($get_all_media);
		
	}
	/*Code For Displaying the Fanclub Media*/
	function chk_fan()
	{
		$id=$this->sel_general();
		$chk = mysql_query("select * from add_to_cart_fan_pro where buyer_id='".$id['general_user_id']."' AND buy_status=1 AND del_status=0");
		return($chk);
	}
	function get_fan_media($buyer_id,$seller_id,$table_name,$type_id)
	{
		$get_data = mysql_query("select * from addtocart_all where buyer_id='".$buyer_id."' AND seller_id='".$seller_id."' AND table_name='".$table_name."' AND type_id='".$type_id."' AND del_status=0");
		return($get_data);
	}
	function get_media_data($id)
	{
		$get_data = mysql_query("SELECT general_media. * , type.name, type.type_id FROM general_media,type WHERE general_media.media_type = type.type_id AND general_media.id ='".$id."' AND general_media.delete_status=0");
		if(mysql_num_rows($get_data)>0)
		{
			$res_data = mysql_fetch_assoc($get_data);
			return($res_data);
		}
	}
	/*Code For Displaying the Fanclub Media Ends Here*/
	function get_all_del_media_id()
	{
		$all_deleted_id = "";
		$sql = mysql_query("select * from general_user where email='".$_SESSION['login_email']."'");
		$res = mysql_fetch_assoc($sql);
		$all_deleted_id = $res['deleted_media_by_tag_user'];
		if($res['artist_id'] >0)
		{
			$get_art = mysql_query("select * from general_artist where artist_id='".$res['artist_id']."'");
			if(mysql_num_rows($get_art)>0)
			{
				$res_art = mysql_fetch_assoc($get_art);
				if($res_art['deleted_media_by_creator_creator'] !="" || $res_art['deleted_media_by_from_from']!=""){
				$all_deleted_id = $all_deleted_id ."," . $res_art['deleted_media_by_creator_creator'].",".$res_art['deleted_media_by_from_from'];}
			}
			$get_art_pro = mysql_query("select * from artist_project where artist_id='".$res['artist_id']."'");
			if(mysql_num_rows($get_art_pro)>0)
			{
				while($res_art_pro = mysql_fetch_assoc($get_art_pro))
				{
					if($res_art_pro['deleted_media_by_creator_creator'] !="" || $res_art_pro['deleted_media_by_from_from'] !=""){
					$all_deleted_id = $all_deleted_id ."," . $res_art_pro['deleted_media_by_creator_creator'].",".$res_art_pro['deleted_media_by_from_from'];}
				}
			}
			$get_art_eve = mysql_query("select * from artist_event where artist_id='".$res['artist_id']."'");
			if(mysql_num_rows($get_art_eve)>0)
			{
				while($res_art_eve = mysql_fetch_assoc($get_art_eve))
				{
					if($res_art_eve['deleted_media_by_creator_creator'] !="" || $res_art_eve['deleted_media_by_from_from']!=""){
					$all_deleted_id = $all_deleted_id ."," . $res_art_eve['deleted_media_by_creator_creator'].",".$res_art_eve['deleted_media_by_from_from'];}
				}
			}
		}
		if($res['community_id'] >0)
		{
			$get_com = mysql_query("select * from general_community where community_id='".$res['community_id']."'");
			if(mysql_num_rows($get_com)>0)
			{
				$res_com = mysql_fetch_assoc($get_com);
				if($res_com['deleted_media_by_creator_creator'] !="" || $res_com['deleted_media_by_from_from'] !=""){
				$all_deleted_id = $all_deleted_id ."," . $res_com['deleted_media_by_creator_creator'].",".$res_com['deleted_media_by_from_from'];}
			}
			$get_com_pro = mysql_query("select * from community_project where community_id='".$res['community_id']."'");
			if(mysql_num_rows($get_com_pro)>0)
			{
				while($res_com_pro = mysql_fetch_assoc($get_com_pro))
				{
					if($res_com_pro['deleted_media_by_creator_creator'] !="" || $res_com_pro['deleted_media_by_from_from'] !=""){
					$all_deleted_id = $all_deleted_id ."," . $res_com_pro['deleted_media_by_creator_creator'].",".$res_com_pro['deleted_media_by_from_from'];}
				}
			}
			$get_com_eve = mysql_query("select * from community_event where community_id='".$res['community_id']."'");
			if(mysql_num_rows($get_com_eve)>0)
			{
				while($res_com_eve = mysql_fetch_assoc($get_com_eve))
				{
					if($res_com_eve['deleted_media_by_creator_creator'] !="" || $res_com_eve['deleted_media_by_from_from'] !=""){
					$all_deleted_id = $all_deleted_id ."," . $res_com_eve['deleted_media_by_creator_creator'].",".$res_com_eve['deleted_media_by_from_from'];}
				}
			}
		}
		return($all_deleted_id);
	}
	
	function get_deleted_project_id()
	{
		$get_gen_det = $this->sel_general();
		$res_art_id ="";
		$get_art_id = mysql_query("select * from general_artist where artist_id='".$get_gen_det['artist_id']."'");
		if(mysql_num_rows($get_art_id)>0)
		{
			$res_art_id = mysql_fetch_assoc($get_art_id);
		}
		return($res_art_id);
	}
	
	function create_artist_member($frequency,$cost,$description)
	{
		$row = $this->sel_general();
		$get_art_mem = mysql_query("select * from artist_membership where artist_id='".$row['artist_id']."'");
		if(mysql_num_rows($get_art_mem)>0){
			$art_member=mysql_query("update artist_membership set frequency='".$frequency."',cost='".$cost."',description='".$description."' where artist_id='".$row['artist_id']."'");
		}else{
			$art_member=mysql_query("insert into artist_membership (artist_id,frequency,cost,description)
			values('".$row['artist_id']."','".$frequency."','".$cost."','".$description."')");
		}
	}
	function delete_artist_member()
	{
		$row=$this->sel_general();
		$get_art_mem = mysql_query("delete from artist_membership where artist_id='".$row['artist_id']."'");
	}
	
	function update_meds($id,$title)
	{
		$sql = mysql_query("UPDATE general_media SET creator='".$title."' WHERE creator_info='general_artist|".$id."'");
		$sql = mysql_query("UPDATE general_media SET `from`='".$title."' WHERE from_info='general_artist|".$id."'");
		$sql = mysql_query("UPDATE artist_event SET creator='".$title."' WHERE creators_info='general_artist|".$id."'");
		$sql = mysql_query("UPDATE community_event SET creator='".$title."' WHERE creators_info='general_artist|".$id."'");
		$sql = mysql_query("UPDATE artist_project SET creator='".$title."' WHERE creators_info='general_artist|".$id."'");
		$sql = mysql_query("UPDATE community_project SET creator='".$title."' WHERE creators_info='general_artist|".$id."'");
	}
	
	function get_all_countries()
	{
		$sql_all_country = "SELECT * FROM country";
		$run_all_country = mysql_query($sql_all_country);
		return ($run_all_country);
	}
	
	function get_all_states($cid)
	{
		$sql_all_state = "SELECT * FROM state where country_id='".$cid."'";
		$run_all_state = mysql_query($sql_all_state);
		return ($run_all_state);
	}
	
}
?>