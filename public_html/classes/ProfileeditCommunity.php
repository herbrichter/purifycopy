<?php
include_once('commons/db.php');
class ProfileeditCommunity
{
	function selgeneral()
	{
		$query = "select * from  general_user where email='".$_SESSION['login_email']."' AND delete_status=0 AND active=0 AND status=0";
		$sql=mysql_query($query);
		$res=mysql_fetch_assoc($sql);
		//$new_id=$res['community_id'];
		return($res);
	}
	
	function sel_general_aid($ids)
	{
		$sql="select * from general_user where artist_id='".$ids."' AND delete_status=0 AND active=0 AND status=0";
		$row=mysql_query($sql);
		$id=mysql_fetch_assoc($row);
		return($id);
	}
	
	function sel_general_cid($ids)
	{
		$sql="select * from general_user where community_id='".$ids."' AND delete_status=0 AND active=0 AND status=0";
		$row=mysql_query($sql);
		$id=mysql_fetch_assoc($row);
		return($id);
	}
	
	function selcommunity()
	{	
		$res = $this->selgeneral();
		//$res['community_id'];
		$sql1=mysql_query("select * from  general_community where community_id = '".$res['community_id']."'");
		return($sql1);
	}
	function selcommunityprofile()
	{
		$res = $this->selgeneral();
		$res['community_id'];
		$sql2=mysql_query("select * from  general_community_profiles where community_id = '".$res['community_id']."'");
		$res2=mysql_fetch_assoc($sql2);
		return($res2);
	}
	
	function getEventUpcoming()
	{
		$date=date("Y-m-d");
		$res = $this->selgeneral();
		$getproject=mysql_query("select * from community_event where community_id='".$res['community_id']."' AND date>='".$date."' AND del_status=0 AND active=0");
		return ($getproject);
	}
	
	function getEventRecorded()
	{
		$date=date("Y-m-d");
		$res = $this->selgeneral();
		$getproject=mysql_query("select * from community_event where community_id='".$res['community_id']."' AND date<'".$date."' AND del_status=0 AND active=0");
		return ($getproject);
	}
	
	function getArtEventUpcoming()
	{
		$date=date("Y-m-d");
		$res = $this->selgeneral();
		$getproject=mysql_query("select * from artist_event where artist_id='".$res['artist_id']."' AND date>='".$date."' AND del_status=0 AND active=0");
		return ($getproject);
	}
	
	function getArtEventRecorded()
	{
		$date=date("Y-m-d");
		$res = $this->selgeneral();
		$getproject=mysql_query("select * from artist_event where artist_id='".$res['artist_id']."' AND date<'".$date."' AND del_status=0 AND active=0");
		return ($getproject);
	}
	
	function getAllProjects()
	{
		$res=$this->selgeneral();
		$getproject=mysql_query("select * from community_project where community_id='".$res['community_id']."' AND del_status=0 AND active=0");
		return ($getproject);
	}
	
	function getAllA_Projects()
	{
		$res=$this->selgeneral();
		$getproject=mysql_query("select * from artist_project where artist_id='".$res['artist_id']."' AND del_status=0 AND active=0");
		return ($getproject);
	}
	
	function selsubType()
	{
		$res4 = $this->selsubTypeid();   
		$selsubtype=mysql_query("select * from  subtype where profile_id = '".$res4['profile_id']."'");
		return($selsubtype);
	}
	function selcommunity_project()
	{
		$res = $this->selgeneral();
		$res['community_id'];
		$getproject=mysql_query("select * from community_project where community_id='".$res['community_id']."' AND del_status=0 AND active=0");
		return($getproject);
	}
	
	function Get_Project_type($id)
	{
		$getproject_type = mysql_query("SELECT * , MIN( id )
											FROM `community_project_profiles` app
											LEFT JOIN type ty ON app.type_id = ty.type_id
											WHERE app.community_project_id ='".$id."'");
		if(mysql_num_rows($getproject_type)>0)
		{
			$res_type= mysql_fetch_assoc($getproject_type);
			return ($res_type);
		}
	}
	
	function Get_Project_subtype($id)
	{
		$getproject_type = mysql_query("SELECT * , MIN( id )
											FROM `community_project_profiles` app
											LEFT JOIN subtype ty ON app.subtype_id = ty.subtype_id
											WHERE app.community_project_id ='".$id."'");
		if(mysql_num_rows($getproject_type)>0)
		{
			$res_type= mysql_fetch_assoc($getproject_type);
			return ($res_type);
		}
	}
	
	function Get_ACProject_subtype($id)
	{
		$getproject_type = mysql_query("SELECT * , MIN( id )
											FROM `artist_project_profiles` app
											LEFT JOIN subtype ty ON app.subtype_id = ty.subtype_id
											WHERE app.artist_project_id ='".$id."'");
		if(mysql_num_rows($getproject_type)>0)
		{
			$res_type= mysql_fetch_assoc($getproject_type);
			return ($res_type);
		}
	}
	
	function Get_Event_type($id)
	{
		$getproject_type = mysql_query("SELECT * , MIN( id )
											FROM `community_event_profiles` app
											LEFT JOIN type ty ON app.type_id = ty.type_id
											WHERE app.community_event_id ='".$id."'");
		$res_type= mysql_fetch_assoc($getproject_type);
		return ($res_type);
	}
	
	function Get_ACEvent_type($id)
	{
		$getproject_type = mysql_query("SELECT * , MIN( id )
											FROM `artist_event_profiles` app
											LEFT JOIN type ty ON app.type_id = ty.type_id
											WHERE app.artist_event_id ='".$id."'");
		$res_type= mysql_fetch_assoc($getproject_type);
		return ($res_type);
	}
	
	function Get_Event_subtype($id)
	{
		$getproject_type = mysql_query("SELECT * , MIN( id )
											FROM `community_event_profiles` app
											LEFT JOIN subtype ty ON app.subtype_id = ty.subtype_id
											WHERE app.community_event_id ='".$id."'");
		$res_type= mysql_fetch_assoc($getproject_type);
		return ($res_type);
	}
	
	function Get_ACEvent_subtype($id)
	{
		$getproject_type = mysql_query("SELECT * , MIN( id )
											FROM `artist_event_profiles` app
											LEFT JOIN subtype ty ON app.subtype_id = ty.subtype_id
											WHERE app.artist_event_id ='".$id."'");
		$res_type= mysql_fetch_assoc($getproject_type);
		return ($res_type);
	}
	
	function selcommunity_event()
	{
		$res = $this->selgeneral();
		$res['community_id'];
		$getevent=mysql_query("select * from community_event where community_id='".$res['community_id']."' AND del_status=0 AND active=0");
		return($getevent);
	}
	function selgeneral_community()
	{
		$res = $this->selgeneral();
		$res['community_id'];
		$getname=mysql_query("select * from general_community where community_id='".$res['community_id']."'");
		return($getname);
	}
	function selgeneral_community_profile()
	{
		$res = $this->selgeneral();
		//$res['community_id'];
		$getmeta=mysql_query("select * from general_community_profiles where community_id='".$res['community_id']."' ORDER BY general_community_profiles_id ASC");
		return($getmeta);
	}
	function seltype_name($arr)
	{
		$qu = mysql_query("select name from type where type_id='".$arr[$i]['type_id']."'");
		$name=mysql_fetch_array($qu);
		return($name);
	}
	function insert_general_community($name,$country,$state,$city,$homepage,$bucket_name)
	{
		$gen_info = $this->selgeneral();
		$art_info = "";
		$com_info = "";
		if($gen_info['artist_id'] !="" || $gen_info['artist_id'] !=0)
		{
			$get_det = mysql_query("select * from general_artist where artist_id='".$gen_info['artist_id']."'");
			$res_det = mysql_fetch_assoc($get_det);
			$art_info = $res_det['artist_view_selected'];
			$com_info = $res_det['community_view_selected'];
		}
		$sql="insert into general_community (name,country_id,state_id,city,homepage,bucket_name,date_time,artist_view_selected,community_view_selected) values('$name','$country','$state','$city','$homepage','$bucket_name',CURDATE(),'".$art_info."','".$com_info."')";
		$query=mysql_query($sql);
		$id=mysql_insert_id();
		return($id);
	}
	function update_general_user($id,$login_email)
	{		
		$sql="UPDATE general_user SET community_id='".$id."' WHERE email='".$login_email."'";			
		$query=mysql_query($sql);
		return ($query);
	}
	function insertgeneral_community_profiles($id,$r)
	{
		$query="insert into `general_community_profiles` (`community_id`, `type_id`, `subtype_id`, `metatype_id`) values ('$id','$r[0]','$r[1]','$r[2]')";
		$res=mysql_query($query);
		return ($res);
	}
	
	function insertgeneral_community_profilesq($id,$q)
	{
		$query="insert into `general_community_profiles` (`community_id`, `type_id`, `subtype_id`, `metatype_id`) values ('$id','$q[0]','$q[1]','0')";
		$res=mysql_query($query);
		return ($res);
	}
	
	function insertgeneral_community_profilesforp($id,$p)
	{
		$query="insert into `general_community_profiles` (`community_id`, `type_id`, `subtype_id`, `metatype_id`) values ('$id','$p[0]','0','0')";
		$res=mysql_query($query);
		return ($res);
	}
	function updategeneral_community($fileName,$id)
	{
		$sql="update general_community set image_name='$fileName' where community_id=$id";
		$query=mysql_query($sql);
		return ($query);
	}
	function update_general_community($name,$subdomain,$website,$bio,$tag_video,$tag_song,$tag_channel,$tag_projects,$tag_events_up,$tag_events_rec,$tag_gallery,$featured_media,$featured_media_gallery,$price,$type,$fan_club_song,$fan_club_gallery,$fan_club_video,$fan_club_channel,$sale_song,$sale_gallery,$me_table,$country,$state,$city,$fb_share,$twit_share,$gplus_share,$tubm_share,$stbu_share,$pin_share,$you_share,$vimeo_share,$sdcl_share,$ints_share)
	{
		$getid=$this->selgeneral();
		$update ="update general_community set name='$name',profile_url='$subdomain',homepage='$website',bio='$bio',taggedprojects ='',taggedgalleries='$tag_gallery',taggedsongs='$tag_song',taggedvideos ='$tag_video',taggedchannels='$tag_channel',taggedprojects='".$tag_projects."',taggedupcomingevents='".$tag_events_up."',taggedrecordedevents='".$tag_events_rec."',featured_media='$featured_media',media_id='".$featured_media_gallery."'
		,price='".$price."', type='".$type."', fan_song='".$fan_club_song."', fan_gallery='".$fan_club_gallery."', fan_video='".$fan_club_video."', fan_channel='".$fan_club_channel."', sale_song='".$sale_song."', sale_gallery='".$sale_gallery."', featured_media_table='".$me_table."', country_id='".$country."', state_id='".$state."', city='".$city."', fb_share='".$fb_share."', twit_share='".$twit_share."', gplus_share='".$gplus_share."', tubm_share='".$tubm_share."', stbu_share='".$stbu_share."', pin_share='".$pin_share."', you_share='".$you_share."', vimeo_share='".$vimeo_share."', sdcl_share='".$sdcl_share."', ints_share='".$ints_share."'
		where community_id=$getid[community_id]";
		mysql_query($update);
	}
	function general_community_profiles($r)
	{
		$getid=$this->selgeneral();
		$query="insert into `general_community_profiles` (`community_id`, `type_id`, `subtype_id`, `metatype_id`) values ('$getid[community_id]','$r[0]','$r[1]','$r[2]')";
		$res=mysql_query($query);
		return ($res);
	}
	function general_community_profiles1($q)
	{
		$getid=$this->selgeneral();
		$query="insert into `general_community_profiles` (`community_id`, `type_id`, `subtype_id`, `metatype_id`) values ('$getid[community_id]','$q[0]','$q[1]','0')";
		$res=mysql_query($query);
		return ($res);
	}
	function general_community_profiles2($p)
	{
		$getid=$this->selgeneral();
		$query="insert into `general_community_profiles` (`community_id`, `type_id`, `subtype_id`, `metatype_id`) values ('$getid[community_id]','$p[0]','0','0')";
		$res=mysql_query($query);
		return ($res);
	}
	function Get_community_primarytype()
	{
		$new_id=$this->selgeneral();
		$sql=mysql_query("SELECT MIN( `general_community_profiles_id` ) FROM `general_community_profiles` WHERE community_id =$new_id[community_id]");
		$res=mysql_fetch_assoc($sql);
		$sql2=mysql_query("select * from general_community_profiles where general_community_profiles_id='".$res['MIN( `general_community_profiles_id` )']."'");
		$res2=mysql_fetch_assoc($sql2);
		return($res2);
	}
	function selsubTypeid()
	{
		$res2 = $this->Get_community_primarytype();
		$sql4=mysql_query("select * from  subtype where type_id = '".$res2['type_id']."'");
		//$res4=mysql_fetch_assoc($sql4);
		return($sql4);
	}
	function selmetaTypeid()
	{
		$res2 = $this->Get_community_primarytype();
		$sql4=mysql_query("select * from  meta_type where subtype_id = '".$res2['subtype_id']."'");
		//$res4=mysql_fetch_assoc($sql4);
		return($sql4);
	}
	function selTypeid()
	{
		$res2 = $this->Get_community_primarytype();
		$sql3=mysql_query("select * from  type where type_id = '".$res2['type_id']."'");
		$res3=mysql_fetch_assoc($sql3);
		return($res3);
	}
	function selType()
	{
		$res3=$this->selTypeid();
		$seltype=mysql_query("select * from  type where profile_id = '".$res3['profile_id']."'");
		return($seltype);
	}
	
	/*********Functions For Tagged Channel Video Song Gallery*******/
	
	function get_community_channel_id()
	{
		$new_id=$this->selgeneral();
		$sql5=mysql_query("select * from general_media where general_user_id='".$new_id['general_user_id']."' and media_type=116 AND delete_status=0");
		return($sql5);
	}
	function get_community_channel($id)
	{
		$sql6=mysql_query("select * from media_channel where media_id='".$id."' ");
		return($sql6);
	}
	function get_community_video_id()
	{
		$new_id=$this->selgeneral();
		$sql7=mysql_query("select * from general_media where general_user_id='".$new_id['general_user_id']."' and media_type=115 AND delete_status=0");
		return($sql7);
	}
	function get_community_video($id)
	{
		$sql8=mysql_query("select * from media_video where media_id='".$id."' ");
		return($sql8);
	}
	function get_community_song_id()
	{
		$new_id=$this->selgeneral();
		$sql9=mysql_query("select * from general_media where general_user_id='".$new_id['general_user_id']."' and media_type=114 AND delete_status=0");
		return($sql9);
	}
	function get_community_song($id)
	{
		$sql10=mysql_query("select * from media_songs where media_id='".$id."' ");
		return($sql10);
	}
	function get_community_gallery_id()
	{
		$new_id=$this->selgeneral();
		$gallery=mysql_query("select * from general_media where general_user_id='".$new_id['general_user_id']."' and media_type=113 AND delete_status=0");
		return($gallery);
	}
	
	function get_community_song_at_register()
	{
		$new_id=$this->selgeneral();
		$sql11=mysql_query("select * from general_community_audio where profile_id='".$new_id['community_id']."'");
		return($sql11);
	}
	function get_community_video_at_register()
	{
		$new_id=$this->selgeneral();
		$sql12=mysql_query("select * from general_community_video where profile_id='".$new_id['community_id']."'");
		return($sql12);
	}
	function get_community_gallery_at_register()
	{
		$new_id=$this->selgeneral();
		$sql14=mysql_query("select * from general_community_gallery where profile_id='".$new_id['community_id']."'");
		$sql13 = mysql_fetch_assoc($sql14);
		return($sql13);
	}
	function get_community_gallery_name_at_register()
	{
		$new_id=$this->get_community_gallery_at_register();
		$sql13=mysql_query("select * from general_community_gallery_list where gallery_id='".$new_id['gallery_id']."'");
		return($sql13);
	}

	/*********Functions For Tagged Channel Video Song Gallery Ends Here*******/
	
	/*********Functions For Tagged Channel Video Song Gallery By Other User*******/
	
	function get_community_media_type_tagged_by_other_user($id)
	{
		$sql14=mysql_query("select * from general_media where id='".$id."'  AND delete_status=0 AND media_status=0");
		$res14=mysql_fetch_assoc($sql14);
		return($res14);
		//var_dump($res14);
	}
	
	/*********Functions For Tagged Channel Video Song Gallery By Other User Ends Here*******/
	
	function get_event_tagged_by_other_user($id)
	{
		$date=date("Y-m-d");
		$sql14=mysql_query("select * from community_event where id='".$id."' AND date>='".$date."' AND del_status=0 AND active=0");
		$res14=mysql_fetch_assoc($sql14);
		return($res14);
		//var_dump($res14);
	}
	
	function get_aevent_tagged_by_other_user($id)
	{
		$date=date("Y-m-d");
		//echo "select * from artist_event where id='".$id."' AND date>='".$date."' AND del_status=0 AND active=0";
		$sql14=mysql_query("select * from artist_event where id='".$id."' AND date>='".$date."' AND del_status=0 AND active=0");
		$res14=mysql_fetch_assoc($sql14);
		return($res14);
		//var_dump($res14);
	}
	
	function get_event_tagged_by_other_userrec($id)
	{
		$date=date("Y-m-d");
		$sql14=mysql_query("select * from community_event where id='".$id."' AND date<'".$date."' AND del_status=0 AND active=0");
		$res14=mysql_fetch_assoc($sql14);
		return($res14);
		//var_dump($res14);
	}
	
	function get_aevent_tagged_by_other_userrec($id)
	{
		$date=date("Y-m-d");
		$sql14=mysql_query("select * from artist_event where id='".$id."' AND date<'".$date."' AND del_status=0 AND active=0");
		$res14=mysql_fetch_assoc($sql14);
		return($res14);
		//var_dump($res14);
	}
	
	function selectUpcomingEvents()
	{
		$res = $this->selgeneral();
		$update_event = mysql_query("SELECT taggedupcomingevents FROM general_community WHERE community_id='".$res['community_id']."'");
		return ($update_event);
	}
	
	function selectCommunityEvents($ans_exp)
	{
		$check_artist = mysql_query("SELECT * FROM community_event WHERE id='".$ans_exp."'");
		return ($check_artist);
	}
	
	function selectRecordedEvents()
	{
		$res = $this->selgeneral();
		$sql_update_up = mysql_query("SELECT taggedrecordedevents FROM general_community WHERE community_id='".$res['community_id']."'");
		return ($sql_update_up);
	}
	
	function updatedRecEvents($final_value)
	{
	$res = $this->selgeneral();
		$update_upc = mysql_query("UPDATE general_community SET taggedrecordedevents='".$final_value."' WHERE community_id='".$res['community_id']."'");
	}
	
	function updatedUpcEvents($new_array1)
	{
		$res = $this->selgeneral();
		$update_rec = mysql_query("UPDATE general_community SET taggedupcomingevents='".$new_array1."' WHERE community_id='".$res['community_id']."'");
	}
	
	function get_project_tagged_by_other_user($id)
	{
		$date=date("Y-m-d");
		$sql14=mysql_query("select * from community_project where id='".$id."' AND del_status=0 AND active=0");
		$res14=mysql_fetch_assoc($sql14);
		return($res14);
		//var_dump($res14);
	}
	
	function get_aproject_tagged_by_other_user($id)
	{
		$date=date("Y-m-d");
		$sql14=mysql_query("select * from artist_project where id='".$id."' AND del_status=0 AND active=0");
		$res14=mysql_fetch_assoc($sql14);
		return($res14);
		//var_dump($res14);
	}
	
	function get_project_tagged_by_other2_user($id)
	{
		$sql_1_cre = "";
		
		$date=date("Y-m-d");
		$sql14=mysql_query("select * from community_project where id='".$id."' AND del_status=0 AND active=0");
		
		if(mysql_num_rows($sql14)>0)
		{
			$res14 = mysql_fetch_assoc($sql14);
			$cret_info = 'community_project|'.$res14['id'];
			
			$sql_1_cre = mysql_query("SELECT * FROM artist_project WHERE creators_info='".$cret_info."' AND active=0 AND del_status=0");
			
			return $sql_1_cre;			
		}
	}
	
	function get_project_atagged_by_other2_user($id)
	{
		$sql_2_cre = "";
		
		$date=date("Y-m-d");
		$sql14=mysql_query("select * from community_project where id='".$id."' AND del_status=0 AND active=0");
		
		if(mysql_num_rows($sql14)>0)
		{
			$res14 = mysql_fetch_assoc($sql14);
		
			$cret_info = 'community_project|'.$res14['id'];			
			
			$sql_2_cre = mysql_query("SELECT * FROM community_project WHERE creators_info='".$cret_info."' AND active=0 AND del_status=0");
			
			return $sql_2_cre;
		}
	}
	
	function get_aproject_tagged_by_other2_user($id)
	{
		$sql_1_cre = "";
		
		$date=date("Y-m-d");
		$sql14=mysql_query("select * from artist_project where id='".$id."' AND del_status=0 AND active=0");
		
		if(mysql_num_rows($sql14)>0)
		{
			$res14 = mysql_fetch_assoc($sql14);
		
			$cret_info = 'artist_project|'.$res14['id'];
			
			$sql_1_cre = mysql_query("SELECT * FROM artist_project WHERE creators_info='".$cret_info."' AND active=0 AND del_status=0");
			return $sql_1_cre;			
		}
	}
	
	function get_aproject_atagged_by_other2_user($id)
	{
		$sql_2_cre = "";
		
		$date=date("Y-m-d");
		$sql14=mysql_query("select * from artist_project where id='".$id."' AND del_status=0 AND active=0");
		
		if(mysql_num_rows($sql14)>0)
		{
			$res14 = mysql_fetch_assoc($sql14);
		
			$cret_info = 'artist_project|'.$res14['id'];			
			
			$sql_2_cre = mysql_query("SELECT * FROM community_project WHERE creators_info='".$cret_info."' AND active=0 AND del_status=0");
			return $sql_2_cre;
		}
	}
	
	function get_event_tagged_by_other2_user($id)
	{
		$sql_1_cre = "";
		
		$date = date("Y-m-d");
		$sql14 = mysql_query("select * from artist_project where id='".$id."' AND del_status=0 AND active=0");
		
		if(mysql_num_rows($sql14)>0)
		{
			$res14 = mysql_fetch_assoc($sql14);
			//while($res14 = mysql_fetch_assoc($sql14))
			//{
			$cret_info = 'artist_project|'.$res14['id'];
			
			$sql_1_cre = mysql_query("SELECT * FROM artist_event WHERE creators_info='".$cret_info."' AND active=0 AND del_status=0 AND date>='".$date."'");
			return $sql_1_cre;
		}
			
		/*	$sql_2_cre = mysql_query("SELECT * FROM community_event WHERE creators_info='".$cret_info."' AND active=0 AND del_status=0");
			if(mysql_num_rows($sql_2_cre)>0)
			{
				while($run_c = mysql_fetch_assoc($sql_2_cre))
				{
					$run_c['id'] = $run_c['id'].'~'.'com';
					//$ans_c_als[]
					$total[] = $run_c;
				}
			}
		//}
		
		return $total; */
	}
	
	function get_aevent_tagged_by_other2_user($id)
	{
		$sql_1_cre = "";
		
		$date=date("Y-m-d");
		$sql14 = mysql_query("select * from community_project where id='".$id."' AND del_status=0 AND active=0");
		
		if(mysql_num_rows($sql14)>0)
		{
			$res14 = mysql_fetch_assoc($sql14);
			//while($res14 = mysql_fetch_assoc($sql14))
			//{
			$cret_info = 'community_project|'.$res14['id'];
			
			$sql_1_cre = mysql_query("SELECT * FROM artist_event WHERE creators_info='".$cret_info."' AND active=0 AND del_status=0 AND date>='".$date."'");
			return $sql_1_cre;
		}
	}
	
	function get_aevent_atagged_by_other2_user($id)
	{
		$sql_2_cre = "";
		
		$date=date("Y-m-d");
		$sql14 = mysql_query("select * from community_project where id='".$id."' AND del_status=0 AND active=0");
		
		if(mysql_num_rows($sql14)>0)
		{
			$res14 = mysql_fetch_assoc($sql14);
			//while($res14 = mysql_fetch_assoc($sql14))
			//{
			$cret_info = 'community_project|'.$res14['id'];
			
			$sql_2_cre = mysql_query("SELECT * FROM community_event WHERE creators_info='".$cret_info."' AND active=0 AND del_status=0 AND date>='".$date."'");
			return $sql_2_cre;
		}
	}
	
	function get_event_atagged_by_other2_user($id)
	{
		$sql_2_cre = "";
		
		$date = date("Y-m-d");
		$sql14 = mysql_query("select * from artist_project where id='".$id."' AND del_status=0 AND active=0");
		
		if(mysql_num_rows($sql14)>0)
		{
			$res14 = mysql_fetch_assoc($sql14);
			//while($res14 = mysql_fetch_assoc($sql14))
			//{
			$cret_info = 'artist_project|'.$res14['id'];
			
			$sql_2_cre = mysql_query("SELECT * FROM community_event WHERE creators_info='".$cret_info."' AND active=0 AND del_status=0 AND date>='".$date."'");
			return $sql_2_cre;
		}
			
		/*	
			if(mysql_num_rows($sql_2_cre)>0)
			{
				while($run_c = mysql_fetch_assoc($sql_2_cre))
				{
					$run_c['id'] = $run_c['id'].'~'.'com';
					//$ans_c_als[]
					$total[] = $run_c;
				}
			}
		//}
		
		return $total; */
	}
	
	function get_event_tagged_by_other2_userrec($id)
	{
		$sql_1_cre = "";
		
		$date = date("Y-m-d");
		$sql14 = mysql_query("select * from artist_project where id='".$id."' AND del_status=0 AND active=0");
		
		if(mysql_num_rows($sql14)>0)
		{
			$res14 = mysql_fetch_assoc($sql14);
			//while($res14 = mysql_fetch_assoc($sql14))
			//{
			$cret_info = 'artist_project|'.$res14['id'];
			
			$sql_1_cre = mysql_query("SELECT * FROM artist_event WHERE creators_info='".$cret_info."' AND active=0 AND del_status=0 AND date<'".$date."'");
			return $sql_1_cre;
		}
			
		/*	$sql_2_cre = mysql_query("SELECT * FROM community_event WHERE creators_info='".$cret_info."' AND active=0 AND del_status=0");
			if(mysql_num_rows($sql_2_cre)>0)
			{
				while($run_c = mysql_fetch_assoc($sql_2_cre))
				{
					$run_c['id'] = $run_c['id'].'~'.'com';
					//$ans_c_als[]
					$total[] = $run_c;
				}
			}
		//}
		
		return $total; */
	}
	
	function get_aevent_tagged_by_other2_userrec($id)
	{
		$sql_1_cre = "";
		
		$date=date("Y-m-d");
		$sql14 = mysql_query("select * from community_project where id='".$id."' AND del_status=0 AND active=0");
		
		if(mysql_num_rows($sql14)>0)
		{
			$res14 = mysql_fetch_assoc($sql14);
			//while($res14 = mysql_fetch_assoc($sql14))
			//{
			$cret_info = 'community_project|'.$res14['id'];
			
			$sql_1_cre = mysql_query("SELECT * FROM artist_event WHERE creators_info='".$cret_info."' AND active=0 AND del_status=0 AND date<'".$date."'");
			return $sql_1_cre;
		}
	}
	
	function get_aevent_atagged_by_other2_userrec($id)
	{
		$sql_2_cre = "";
		
		$date=date("Y-m-d");
		$sql14 = mysql_query("select * from community_project where id='".$id."' AND del_status=0 AND active=0");
		
		if(mysql_num_rows($sql14)>0)
		{
			$res14 = mysql_fetch_assoc($sql14);
			//while($res14 = mysql_fetch_assoc($sql14))
			//{
			$cret_info = 'community_project|'.$res14['id'];
			
			$sql_2_cre = mysql_query("SELECT * FROM community_event WHERE creators_info='".$cret_info."' AND active=0 AND del_status=0 AND date<'".$date."'");
			return $sql_2_cre;
		}
	}
	
	function get_event_atagged_by_other2_userrec($id)
	{
		$sql_2_cre = "";
		
		$date = date("Y-m-d");
		$sql14 = mysql_query("select * from artist_project where id='".$id."' AND del_status=0 AND active=0");
		
		if(mysql_num_rows($sql14)>0)
		{
			$res14 = mysql_fetch_assoc($sql14);
			//while($res14 = mysql_fetch_assoc($sql14))
			//{
			$cret_info = 'artist_project|'.$res14['id'];
			
			$sql_2_cre = mysql_query("SELECT * FROM community_event WHERE creators_info='".$cret_info."' AND active=0 AND del_status=0 AND date<'".$date."'");
			return $sql_2_cre;
		}
			
		/*	
			if(mysql_num_rows($sql_2_cre)>0)
			{
				while($run_c = mysql_fetch_assoc($sql_2_cre))
				{
					$run_c['id'] = $run_c['id'].'~'.'com';
					//$ans_c_als[]
					$total[] = $run_c;
				}
			}
		//}
		
		return $total; */
	}
	
	function get_fanclub_media()
	{
		$get_gen = $this->selgeneral();
		$get_fan = mysql_query("select * from general_media where (sharing_preference = 3 OR sharing_preference = 4) AND general_user_id='".$get_gen['general_user_id']."'  AND delete_status=0 AND media_status=0" );
		return($get_fan);
	}
	function get_forsale_media()
	{
		$get_gen = $this->selgeneral();
		$get_sale = mysql_query("select * from general_media where sharing_preference = 5  AND general_user_id='".$get_gen['general_user_id']."'  AND delete_status=0 AND media_status=0" );
		return($get_sale);
	}
	
	function Get_comm_member_Id()
	{
		$row=$this->selgeneral();
		$get_member=mysql_query("select * from community_membership where community_id='".$row['community_id']."'");
		$member_res=mysql_fetch_assoc($get_member);
		return($member_res);
	}
	function Get_artist_member_Id()
	{
		$row=$this->selgeneral();
		$get_art_member=mysql_query("select * from artist_membership where artist_id='".$row['artist_id']."'");
		$member_art_res=mysql_fetch_assoc($get_art_member);
		return($member_art_res);
	}
	function Get_community_fan($com_id)
	{
		$get_sale = mysql_query("select * from fan_club_membership where expiry_date>CURDATE()");
		while($row = mysql_fetch_assoc($get_sale))
		{
			$exp = explode("_",$row['related_id']);
			if($exp[0] == $com_id)
			{
				$exp_count +=1;
			}
		}
		return($exp_count);
	}
	function Get_community_total_fan($com_id)
	{
		$get_sale = mysql_query("select * from fan_club_membership ");
		while($row = mysql_fetch_assoc($get_sale))
		{
			$exp = explode("_",$row['related_id']);
			if($exp[0] == $com_id)
			{
				$exp_count +=1;
			}
		}
		return($exp_count);
	}
	function Get_all_friends()
	{
		$res = $this -> selgeneral();
		$get_friend = mysql_query("select * from friends where general_user_id ='".$res['general_user_id']."' AND status=0");
		if(mysql_num_rows($get_friend)>0)
		{
			return($get_friend);
		}
	}
	function get_friend_art_info($friend_id)
	{
		$get_gen_info = mysql_query("select * from general_user where general_user_id ='".$friend_id."' AND delete_status=0");
		if(mysql_num_rows($get_gen_info)>0)
		{
			$res_gen_info = mysql_fetch_assoc($get_gen_info);
			if($res_gen_info['community_id']!="" && $res_gen_info['community_id']!=0)
			{
				return ($res_gen_info['community_id']);
			}
		}
	}
	function get_all_tagged_pro($art_id)
	{
		$get_projects = mysql_query("select * from community_project where community_id='".$art_id."' AND del_status=0 AND active=0");
		return($get_projects);
	}
	function get_all_tagged_event($art_id)
	{
		$get_event = mysql_query("select * from community_event where community_id='".$art_id."' AND del_status=0 AND active=0");
		return($get_event);
	}
	function Get_purify_member()
	{
		$date= 	date('Y-m-d');
		$row=$this->selgeneral();
		$get_purify_member=mysql_query("select * from purify_membership where general_user_id='".$row['general_user_id']."' AND (expiry_date>'".$date."' OR lifetime=1)");
		$member_purify_res=mysql_fetch_assoc($get_purify_member);
		return($member_purify_res);
	}
	
	function get_media_create($type)
	{
		$id = $this->selgeneral();
		
		$var_type_1 = 'general_artist';
		$var_type_2 = 'general_community';
		$var_type_3 = 'artist_project';
		$var_type_4 = 'community_project';
		
		$pal_1 = array();
		$pal_2 = array();
		$pal_3 = array();
		$pal_4 = array();
		
		$final_c_f = array();
		
		/* if($id['artist_id']!=0 && !empty($id['artist_id']))
		{
			$chk_type_a = $var_type_1.'|'.$id['artist_id'];

			$sql = mysql_query("SELECT * FROM general_media WHERE creator_info='".$chk_type_a."' AND delete_status=0 AND media_status=0 AND media_type='".$type."' AND general_user_id!='".$id['general_user_id']."'");
			if(mysql_num_rows($sql)>0)
			{
				while($row_a = mysql_fetch_assoc($sql))
				{
					$final_c_f[] = $row_a;
				}
			}
		} */
		
		if($id['community_id']!=0 && !empty($id['community_id']))
		{
			$chk_type_c = $var_type_2.'|'.$id['community_id'];
			
			$sql = mysql_query("SELECT * FROM general_media WHERE creator_info='".$chk_type_c."' AND delete_status=0 AND media_status=0 AND media_type='".$type."' AND general_user_id!='".$id['general_user_id']."'");
			if(mysql_num_rows($sql)>0)
			{
				while($row_c = mysql_fetch_assoc($sql))
				{
					$final_c_f[] = $row_c;
				}				
			}
		}
		
		/* if($id['artist_id']!=0 && !empty($id['artist_id']))
		{			
			$sql_get_allp = mysql_query("SELECT * FROM artist_project WHERE artist_id='".$id['artist_id']."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_get_allp)>0)
			{
				while($rowal = mysql_fetch_assoc($sql_get_allp))
				{
					$pal_1[] = $rowal;
				}
			}
			
			$sql_get_alle = mysql_query("SELECT * FROM artist_event WHERE artist_id='".$id['artist_id']."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_get_alle)>0)
			{
				while($rowael = mysql_fetch_assoc($sql_get_alle))
				{
					$pal_3[] = $rowael;
				}
			}
			
			if(!empty($pal_1) && count($pal_1))
			{
				for($pl=0;$pl<count($pal_1);$pl++)
				{
					$chk_typep_1 = 'artist_project|'.$pal_1[$pl]['id'];
					
					$sql_as = mysql_query("SELECT * FROM general_media WHERE creator_info='".$chk_typep_1."' AND delete_status=0 AND media_status=0 AND media_type='".$type."' AND general_user_id!='".$id['general_user_id']."'");
					if(mysql_num_rows($sql_as)>0)
					{
						while($row_ap = mysql_fetch_assoc($sql_as))
						{
							$final_c_f[] = $row_ap;
						}
					}
				}
			}
			
			if(!empty($pal_3) && count($pal_3))
			{
				for($pl=0;$pl<count($pal_3);$pl++)
				{
					$chk_typep_1 = 'artist_event|'.$pal_3[$pl]['id'];
					
					$sql_as = mysql_query("SELECT * FROM general_media WHERE from_info='".$chk_typep_1."' AND delete_status=0 AND media_status=0 AND media_type='".$type."' AND general_user_id!='".$id['general_user_id']."'");
					if(mysql_num_rows($sql_as)>0)
					{
						while($row_ae = mysql_fetch_assoc($sql_as))
						{
							$final_c_f[] = $row_ae;
						}
					}
				}
			}
		}
		
		if($id['community_id']!=0 && !empty($id['community_id']))
		{			
			$sql_get_allp = mysql_query("SELECT * FROM community_project WHERE community_id='".$id['community_id']."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_get_allp)>0)
			{
				while($rowal = mysql_fetch_assoc($sql_get_allp))
				{
					$pal_2[] = $rowal;
				}
			}
			
			$sql_get_allew = mysql_query("SELECT * FROM community_event WHERE community_id='".$id['community_id']."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_get_allew)>0)
			{
				while($rowawl = mysql_fetch_assoc($sql_get_allew))
				{
					$pal_4[] = $rowawl;
				}
			}
			
			if(!empty($pal_2) && count($pal_2))
			{
				for($pl=0;$pl<count($pal_1);$pl++)
				{
					$chk_typep_1 = 'community_project|'.$pal_1[$pl]['id'];
					
					$sql_as = mysql_query("SELECT * FROM general_media WHERE creator_info='".$chk_typep_1."' AND delete_status=0 AND media_status=0 AND media_type='".$type."' AND general_user_id!='".$id['general_user_id']."'");
					if(mysql_num_rows($sql_as)>0)
					{
						while($row_cp = mysql_fetch_assoc($sql_as))
						{
							$final_c_f[] = $row_cp;
						}
					}
				}
			}
			
			if(!empty($pal_4) && count($pal_4))
			{
				for($pl=0;$pl<count($pal_4);$pl++)
				{
					$chk_typep_1 = 'community_event|'.$pal_4[$pl]['id'];
					
					$sql_as = mysql_query("SELECT * FROM general_media WHERE from_info='".$chk_typep_1."' AND delete_status=0 AND media_status=0 AND media_type='".$type."' AND general_user_id!='".$id['general_user_id']."'");
					if(mysql_num_rows($sql_as)>0)
					{
						while($row_ce = mysql_fetch_assoc($sql_as))
						{
							$final_c_f[] = $row_ce;
						}
					}
				}
			}
		} */
		//$final_c_f = array_merge($final_c_f1,$final_c_f2);
		return $final_c_f;
	}
	
	function get_project_create()
	{
		$id = $this->selgeneral();
		$var_type_1 = 'general_artist';
		$var_type_2 = 'general_community';
		$var_type_3 = 'artist_project';
		$var_type_4 = 'community_project';
		
		$artm = array();
		$arta = array();
		$comm = array();
		$coma = array();
		
		$artme = array();
		$artae = array();
		$comme = array();
		$comae = array();
		
		$art_p_ao = array();
		$com_p_ao = array();
		$art_p_co = array();
		$com_p_co = array();
		
		$art_e_ao = array();
		$com_e_ao = array();
		$art_e_co = array();
		$com_e_co = array();
		
		$pal_1 = array();
		$pal_2 = array();
		
		if($id['artist_id']!=0 && !empty($id['artist_id']))
		{
			$chk_type = $var_type_1.'|'.$id['artist_id'];
			
			$sql_get_allp = mysql_query("SELECT * FROM artist_project WHERE artist_id='".$id['artist_id']."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_get_allp)>0)
			{
				while($rowal = mysql_fetch_assoc($sql_get_allp))
				{
					$pal_1[] = $rowal;
				}
			}
			
			if(!empty($pal_1) && count($pal_1))
			{
				for($pl=0;$pl<count($pal_1);$pl++)
				{
					$chk_typep_1 = 'artist_project|'.$pal_1[$pl]['id'];
					$sql = mysql_query("SELECT * FROM artist_project WHERE creators_info='".$chk_typep_1."' AND del_status=0 AND active=0");
					if(mysql_num_rows($sql)>0)
					{
						while($row_po = mysql_fetch_assoc($sql))
						{
							if($row_po['artist_id']!=$id['artist_id'])
							{
								$art_p_ao[] = $row_po;
							}
						}
					}
					
					$sql = mysql_query("SELECT * FROM community_project WHERE creators_info='".$chk_typep_1."' AND del_status=0 AND active=0");
					if(mysql_num_rows($sql)>0)
					{
						while($row_poo = mysql_fetch_assoc($sql))
						{
							if($row_poo['community_id']!=$id['community_id'])
							{
								$com_p_ao[] = $row_poo;
							}
						}
					}
				}
			}
			
			if(!empty($pal_1) && count($pal_1))
			{
				for($pl=0;$pl<count($pal_1);$pl++)
				{
					$chk_typep_1 = 'artist_project|'.$pal_1[$pl]['id'];
					$sql = mysql_query("SELECT * FROM artist_event WHERE creators_info='".$chk_typep_1."' AND del_status=0 AND active=0");
					if(mysql_num_rows($sql)>0)
					{
						while($row_po = mysql_fetch_assoc($sql))
						{
							if($row_po['artist_id']!=$id['artist_id'])
							{
								$art_e_ao[] = $row_po;
							}
						}
					}
					
					$sql = mysql_query("SELECT * FROM community_event WHERE creators_info='".$chk_typep_1."' AND del_status=0 AND active=0");
					if(mysql_num_rows($sql)>0)
					{
						while($row_poo = mysql_fetch_assoc($sql))
						{
							if($row_poo['community_id']!=$id['community_id'])
							{
								$com_e_ao[] = $row_poo;
							}
						}
					}
				}
			}
			
			$sql_pro_arta = mysql_query("SELECT * FROM artist_project WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_pro_arta)>0)
			{
				while($rowaa = mysql_fetch_assoc($sql_pro_arta))
				{
					if($rowaa['artist_id']!=$id['artist_id'])
					{
						$arta[] = $rowaa;
					}
				}
			}
			
			$sql_pro_arta = mysql_query("SELECT * FROM artist_event WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_pro_arta)>0)
			{
				while($rowaa = mysql_fetch_assoc($sql_pro_arta))
				{
					if($rowaa['artist_id']!=$id['artist_id'])
					{
						$artae[] = $rowaa;
					}
				}
			}
			
			$sql_pro_coma = mysql_query("SELECT * FROM community_project WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_pro_coma)>0)
			{
				while($rowam = mysql_fetch_assoc($sql_pro_coma))
				{
					if($rowam['community_id']!=$id['community_id'])
					{
						$coma[] = $rowam;
					}
				}
			}
			
			$sql_pro_coma = mysql_query("SELECT * FROM community_event WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_pro_coma)>0)
			{
				while($rowam = mysql_fetch_assoc($sql_pro_coma))
				{
					if($rowam['community_id']!=$id['community_id'])
					{
						$comae[] = $rowam;
					}
				}
			}
		}
		
		if($id['community_id']!=0 && !empty($id['community_id']))
		{
			$chk_type = $var_type_2.'|'.$id['community_id'];
			$sql_pro_artm = mysql_query("SELECT * FROM artist_project WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_pro_artm)>0)
			{
				while($rowcm = mysql_fetch_assoc($sql_pro_artm))
				{
					if($rowcm['artist_id']!=$id['artist_id'])
					{
						$artm[] = $rowcm;
					}
				}
			}
			
			$chk_type = $var_type_2.'|'.$id['community_id'];
			$sql_pro_artm = mysql_query("SELECT * FROM artist_event WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_pro_artm)>0)
			{
				while($rowcm = mysql_fetch_assoc($sql_pro_artm))
				{
					if($rowcm['artist_id']!=$id['artist_id'])
					{
						$artme[] = $rowcm;
					}
				}
			}
			
			$sql_pro_comm = mysql_query("SELECT * FROM community_project WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_pro_comm)>0)
			{
				while($rowca = mysql_fetch_assoc($sql_pro_comm))
				{
					if($rowca['community_id']!=$id['community_id'])
					{
						$comm[] = $rowca;
					}
				}
			}
			
			$sql_pro_comm = mysql_query("SELECT * FROM community_event WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_pro_comm)>0)
			{
				while($rowca = mysql_fetch_assoc($sql_pro_comm))
				{
					if($rowca['community_id']!=$id['community_id'])
					{
						$comme[] = $rowca;
					}
				}
			}
			
			$sql_get_allp = mysql_query("SELECT * FROM community_project WHERE community_id='".$id['community_id']."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_get_allp)>0)
			{
				while($rowal = mysql_fetch_assoc($sql_get_allp))
				{
					$pal_2[] = $rowal;
				}
			}
			
			if(!empty($pal_2) && count($pal_2))
			{
				for($pl2=0;$pl2<count($pal_2);$pl2++)
				{
					$chk_typep_2 = 'community_project|'.$pal_2[$pl2]['id'];
					$sql = mysql_query("SELECT * FROM artist_project WHERE creators_info='".$chk_typep_2."' AND del_status=0 AND active=0");
					if(mysql_num_rows($sql)>0)
					{
						while($row_po = mysql_fetch_assoc($sql))
						{
							if($row_po['artist_id']!=$id['artist_id'])
							{
								$art_p_co[] = $row_po;
							}
						}
					}
					
					$sql = mysql_query("SELECT * FROM community_project WHERE creators_info='".$chk_typep_2."' AND del_status=0 AND active=0");
					if(mysql_num_rows($sql)>0)
					{
						while($row_poo = mysql_fetch_assoc($sql))
						{
							if($row_poo['community_id']!=$id['community_id'])
							{
								$com_p_co[] = $row_poo;
							}
						}
					}
				}
			}
			
			if(!empty($pal_2) && count($pal_2))
			{
				for($pl2=0;$pl2<count($pal_2);$pl2++)
				{
					$chk_typep_2 = 'community_project|'.$pal_2[$pl2]['id'];
					$sql = mysql_query("SELECT * FROM artist_event WHERE creators_info='".$chk_typep_2."' AND del_status=0 AND active=0");
					if(mysql_num_rows($sql)>0)
					{
						while($row_po = mysql_fetch_assoc($sql))
						{
							if($row_po['artist_id']!=$id['artist_id'])
							{
								$art_e_co[] = $row_po;
							}
						}
					}
					
					$sql = mysql_query("SELECT * FROM community_event WHERE creators_info='".$chk_typep_2."' AND del_status=0 AND active=0");
					if(mysql_num_rows($sql)>0)
					{
						while($row_poo = mysql_fetch_assoc($sql))
						{
							if($row_poo['community_id']!=$id['community_id'])
							{
								$com_e_co[] = $row_poo;
							}
						}
					}
				}
			}
		}
		
		$totals = array();
		$totals = array_merge($artm,$arta,$comm,$coma,$art_p_ao,$com_p_ao,$art_p_co,$com_p_co,$artme,$artae,$comme,$comae,$art_e_ao,$com_e_ao,$art_e_co,$com_e_co);
		return $totals;
	}
	
	/************Functions for displaying tagged media fro projects**********************/
	
	function get_artist_project_tag($email)
	{
		$query = mysql_query("SELECT * FROM artist_project WHERE del_status=0 AND active=0 AND tagged_user_email LIKE '%".$email."%'");
		return($query);
	}
	
	function get_community_project_tag($email)
	{
		$query = mysql_query("SELECT * FROM community_project WHERE del_status=0 AND active=0 AND tagged_user_email LIKE '%".$email."%'");
		return($query);
	}
	
	function get_artist_event_tag($email)
	{
		$query = mysql_query("SELECT * FROM artist_event WHERE del_status=0 AND active=0 AND tagged_user_email LIKE '%".$email."%'");
		return($query);
	}
	
	function get_community_event_tag($email)
	{
		$query = mysql_query("SELECT * FROM community_event WHERE del_status=0 AND active=0 AND tagged_user_email LIKE '%".$email."%'");
		return($query);
	}
	
	function get_tag_media_info($media_id)
	{
		$sql= mysql_query("SELECT general_media. * , type.name, type.type_id FROM general_media,type WHERE general_media.media_type = type.type_id AND general_media.id ='".$media_id."' AND general_media.delete_status=0");
		if(mysql_num_rows($sql)>0)
		{
			$row = mysql_fetch_assoc($sql);
			return($row);
		}
	}
	/************Functions for displaying tagged media fro projects**********************/
	
	function get_sel_songs($id)
	{
		$query = mysql_query("select * from general_community where media_id='".$id."'");
		$res = mysql_fetch_assoc($query);
		return($res);
	}
	
	function get_media_reg_info($med_id,$types)
	{
		if($types!='')
		{
			$typ_ss = explode('~',$types);
			$chk_reg_med = '';
			if($typ_ss[0]=='artist' && $typ_ss[1]!='')
			{
				$sql_audr = mysql_query("SELECT * FROM general_artist_audio WHERE audio_id='".$med_id."' AND profile_id='".$typ_ss[1]."'");
				$sql_vidr = mysql_query("SELECT * FROM general_artist_video WHERE video_id='".$med_id."' AND profile_id='".$typ_ss[1]."'");
				$sql_galr = mysql_query("SELECT * FROM general_artist_gallery WHERE gallery_id='".$med_id."' AND profile_id='".$typ_ss[1]."'");
				
				if(mysql_num_rows($sql_audr)>0)
				{
					$chk_reg_med = 'general_artist_audio';
				}
				elseif(mysql_num_rows($sql_vidr)>0)
				{
					$chk_reg_med = 'general_artist_video';
				}
				elseif(mysql_num_rows($sql_galr)>0)
				{
					$chk_reg_med = 'general_artist_gallery';
				}
				
				if($chk_reg_med!="")
				{
					return $chk_reg_med;
				}
			}
			elseif($typ_ss[0]=='community' && $typ_ss[1]!='')
			{
				$sql_audr = mysql_query("SELECT * FROM general_community_audio WHERE audio_id='".$med_id."' AND profile_id='".$typ_ss[1]."'");
				$sql_vidr = mysql_query("SELECT * FROM general_community_video WHERE video_id='".$med_id."' AND profile_id='".$typ_ss[1]."'");
				$sql_galr = mysql_query("SELECT * FROM general_community_gallery WHERE gallery_id='".$med_id."' AND profile_id='".$typ_ss[1]."'");
				
				if(mysql_num_rows($sql_audr)>0)
				{
					$chk_reg_med = 'general_community_audio';
				}
				elseif(mysql_num_rows($sql_vidr)>0)
				{
					$chk_reg_med = 'general_community_video';
				}
				elseif(mysql_num_rows($sql_galr)>0)
				{
					$chk_reg_med = 'general_community_gallery';
				}
				
				if($chk_reg_med!="")
				{
					return $chk_reg_med;
				}
			}
		}
	}
	
	/* function only_creator_this()
	{
		$new_id = $this->selgeneral();
		$final_pros = array();
		
		if(isset($new_id['community_id']) && !empty($new_id['community_id']) && $new_id['community_id']!=0)
		{
			$mcth = 'general_community|'.$new_id['community_id'];
			$sql_apro = mysql_query("SELECT * FROM artist_project WHERE creators_info='".$mcth."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_apro)>0)
			{
				while($ans_apro = mysql_fetch_assoc($sql_apro))
				{				
					$ans_apro['id'] = $ans_apro['id'].'~art';
					$final_pros[] = $ans_apro;
				}	
			}
			
			$sql_cpro = mysql_query("SELECT * FROM community_project WHERE creators_info='".$mcth."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_cpro)>0)
			{
				while($ans_cpro = mysql_fetch_assoc($sql_cpro))
				{				
					$ans_cpro['id'] = $ans_cpro['id'].'~com';
					$final_pros[] = $ans_cpro;
				}
			}
		}
		return $final_pros;
	} */
	
	function only_creator_this()
	{
		$id = $this->selgeneral();
		$var_type_1 = 'general_artist';
		$var_type_2 = 'general_community';
		$var_type_3 = 'artist_project';
		$var_type_4 = 'community_project';
		
		$artm = array();
		$arta = array();
		$comm = array();
		$coma = array();
		
		$artme = array();
		$artae = array();
		$comme = array();
		$comae = array();
		
		$art_p_ao = array();
		$com_p_ao = array();
		$art_p_co = array();
		$com_p_co = array();
		
		$art_e_ao = array();
		$com_e_ao = array();
		$art_e_co = array();
		$com_e_co = array();
		
		$pal_1 = array();
		$pal_2 = array();
		
		if($id['artist_id']!=0 && !empty($id['artist_id']))
		{
			$chk_type = $var_type_1.'|'.$id['artist_id'];
			
			$sql_get_allp = mysql_query("SELECT * FROM artist_project WHERE artist_id='".$id['artist_id']."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_get_allp)>0)
			{
				while($rowal = mysql_fetch_assoc($sql_get_allp))
				{	
					$rowal['whos_project'] = "only_creator";
					$pal_1[] = $rowal;
				}
			}
			
			if(!empty($pal_1) && count($pal_1))
			{
				for($pl=0;$pl<count($pal_1);$pl++)
				{
					$chk_typep_1 = 'artist_project|'.$pal_1[$pl]['id'];
					$sql = mysql_query("SELECT * FROM artist_project WHERE creators_info='".$chk_typep_1."' AND del_status=0 AND active=0");
					if(mysql_num_rows($sql)>0)
					{
						while($row_po = mysql_fetch_assoc($sql))
						{
							if($row_po['artist_id']!=$id['artist_id'])
							{
								$row_po['id'] = $row_po['id'].'~art';
								$row_po['whos_project'] = "only_creator";
								$art_p_ao[] = $row_po;
							}
						}
					}
					
					$sql = mysql_query("SELECT * FROM community_project WHERE creators_info='".$chk_typep_1."' AND del_status=0 AND active=0");
					if(mysql_num_rows($sql)>0)
					{
						while($row_poo = mysql_fetch_assoc($sql))
						{
							if($row_poo['community_id']!=$id['community_id'])
							{
								$row_poo['id'] = $row_poo['id'].'~com';
								$row_poo['whos_project'] = "only_creator";
								$com_p_ao[] = $row_poo;
							}
						}
					}
				}
			}
			
			/* if(!empty($pal_1) && count($pal_1))
			{
				for($pl=0;$pl<count($pal_1);$pl++)
				{
					$chk_typep_1 = 'artist_project|'.$pal_1[$pl]['id'];
					$sql = mysql_query("SELECT * FROM artist_event WHERE creators_info='".$chk_typep_1."' AND del_status=0 AND active=0");
					if(mysql_num_rows($sql)>0)
					{
						while($row_po = mysql_fetch_assoc($sql))
						{
							if($row_po['artist_id']!=$id['artist_id'])
							{
								$art_e_ao[] = $row_po;
							}
						}
					}
					
					$sql = mysql_query("SELECT * FROM community_event WHERE creators_info='".$chk_typep_1."' AND del_status=0 AND active=0");
					if(mysql_num_rows($sql)>0)
					{
						while($row_poo = mysql_fetch_assoc($sql))
						{
							if($row_poo['community_id']!=$id['community_id'])
							{
								$com_e_ao[] = $row_poo;
							}
						}
					}
				}
			} */
			
			$sql_pro_arta = mysql_query("SELECT * FROM artist_project WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_pro_arta)>0)
			{
				while($rowaa = mysql_fetch_assoc($sql_pro_arta))
				{
					if($rowaa['artist_id']!=$id['artist_id'])
					{
						$rowaa['id'] = $rowaa['id'].'~art';
						$rowaa['whos_project'] = "only_creator";
						$arta[] = $rowaa;
					}
				}
			}
			
			/* $sql_pro_arta = mysql_query("SELECT * FROM artist_event WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_pro_arta)>0)
			{
				while($rowaa = mysql_fetch_assoc($sql_pro_arta))
				{
					if($rowaa['artist_id']!=$id['artist_id'])
					{
						$artae[] = $rowaa;
					}
				}
			} */
			
			$sql_pro_coma = mysql_query("SELECT * FROM community_project WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_pro_coma)>0)
			{
				while($rowam = mysql_fetch_assoc($sql_pro_coma))
				{
					if($rowam['community_id']!=$id['community_id'])
					{
						$rowam['id'] = $rowam['id'].'~com';
						$rowam['whos_project'] = "only_creator";
						$coma[] = $rowam;
					}
				}
			}
			
			/* $sql_pro_coma = mysql_query("SELECT * FROM community_event WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_pro_coma)>0)
			{
				while($rowam = mysql_fetch_assoc($sql_pro_coma))
				{
					if($rowam['community_id']!=$id['community_id'])
					{
						$comae[] = $rowam;
					}
				}
			} */
		}
		
		if($id['community_id']!=0 && !empty($id['community_id']))
		{
			$chk_type = $var_type_2.'|'.$id['community_id'];
			$sql_pro_artm = mysql_query("SELECT * FROM artist_project WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_pro_artm)>0)
			{
				while($rowcm = mysql_fetch_assoc($sql_pro_artm))
				{
					if($rowcm['artist_id']!=$id['artist_id'])
					{
						$rowcm['id'] = $rowcm['id'].'~art';
						$rowcm['whos_project'] = "only_creator";
						$artm[] = $rowcm;
					}
				}
			}
			
			/* $chk_type = $var_type_2.'|'.$id['community_id'];
			$sql_pro_artm = mysql_query("SELECT * FROM artist_event WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_pro_artm)>0)
			{
				while($rowcm = mysql_fetch_assoc($sql_pro_artm))
				{
					if($rowcm['artist_id']!=$id['artist_id'])
					{
						$artme[] = $rowcm;
					}
				}
			} */
			
			$sql_pro_comm = mysql_query("SELECT * FROM community_project WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_pro_comm)>0)
			{
				while($rowca = mysql_fetch_assoc($sql_pro_comm))
				{
					if($rowca['community_id']!=$id['community_id'])
					{
						$rowca['id'] = $rowca['id'].'~com';
						$rowca['whos_project'] = "only_creator";
						$comm[] = $rowca;
					}
				}
			}
			
			/* $sql_pro_comm = mysql_query("SELECT * FROM community_event WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_pro_comm)>0)
			{
				while($rowca = mysql_fetch_assoc($sql_pro_comm))
				{
					if($rowca['community_id']!=$id['community_id'])
					{
						$comme[] = $rowca;
					}
				}
			} */
			
			$sql_get_allp = mysql_query("SELECT * FROM community_project WHERE community_id='".$id['community_id']."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_get_allp)>0)
			{
				while($rowal = mysql_fetch_assoc($sql_get_allp))
				{
					$rowal['whos_project'] = "only_creator";
					$pal_2[] = $rowal;
				}
			}
			
			if(!empty($pal_2) && count($pal_2))
			{
				for($pl2=0;$pl2<count($pal_2);$pl2++)
				{
					$chk_typep_2 = 'community_project|'.$pal_2[$pl2]['id'];
					$sql = mysql_query("SELECT * FROM artist_project WHERE creators_info='".$chk_typep_2."' AND del_status=0 AND active=0");
					if(mysql_num_rows($sql)>0)
					{
						while($row_po = mysql_fetch_assoc($sql))
						{
							if($row_po['artist_id']!=$id['artist_id'])
							{
								$row_po['id'] = $row_po['id'].'~art';
								$row_po['whos_project'] = "only_creator";
								$art_p_co[] = $row_po;
							}
						}
					}
					
					$sql = mysql_query("SELECT * FROM community_project WHERE creators_info='".$chk_typep_2."' AND del_status=0 AND active=0");
					if(mysql_num_rows($sql)>0)
					{
						while($row_poo = mysql_fetch_assoc($sql))
						{
							if($row_poo['community_id']!=$id['community_id'])
							{
								$row_poo['id'] = $row_poo['id'].'~com';
								$row_poo['whos_project'] = "only_creator";
								$com_p_co[] = $row_poo;
							}
						}
					}
				}
			}
			
			/* if(!empty($pal_2) && count($pal_2))
			{
				for($pl2=0;$pl2<count($pal_2);$pl2++)
				{
					$chk_typep_2 = 'community_project|'.$pal_2[$pl2]['id'];
					$sql = mysql_query("SELECT * FROM artist_event WHERE creators_info='".$chk_typep_2."' AND del_status=0 AND active=0");
					if(mysql_num_rows($sql)>0)
					{
						while($row_po = mysql_fetch_assoc($sql))
						{
							if($row_po['artist_id']!=$id['artist_id'])
							{
								$art_e_co[] = $row_po;
							}
						}
					}
					
					$sql = mysql_query("SELECT * FROM community_event WHERE creators_info='".$chk_typep_2."' AND del_status=0 AND active=0");
					if(mysql_num_rows($sql)>0)
					{
						while($row_poo = mysql_fetch_assoc($sql))
						{
							if($row_poo['community_id']!=$id['community_id'])
							{
								$com_e_co[] = $row_poo;
							}
						}
					}
				}
			} */
		}
		
		$totals = array();
		$totals = array_merge($artm,$arta,$comm,$coma,$art_p_ao,$com_p_ao,$art_p_co,$com_p_co,$artme,$artae,$comme,$comae,$art_e_ao,$com_e_ao,$art_e_co,$com_e_co);
		return $totals;
	}
	
	function only_sh_creator_this()
	{
		$id = $this->selgeneral();
		$var_type_1 = 'general_artist';
		$var_type_2 = 'general_community';
		$var_type_3 = 'artist_project';
		$var_type_4 = 'community_project';
		
		$artm = array();
		$arta = array();
		$comm = array();
		$coma = array();
		
		$artme = array();
		$artae = array();
		$comme = array();
		$comae = array();
		
		$art_p_ao = array();
		$com_p_ao = array();
		$art_p_co = array();
		$com_p_co = array();
		
		$art_e_ao = array();
		$com_e_ao = array();
		$art_e_co = array();
		$com_e_co = array();
		
		$pal_1 = array();
		$pal_2 = array();
		
		if($id['artist_id']!=0 && !empty($id['artist_id']))
		{
			$chk_type = $var_type_1.'|'.$id['artist_id'];
			
			$sql_get_allp = mysql_query("SELECT * FROM artist_project WHERE artist_id='".$id['artist_id']."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_get_allp)>0)
			{
				while($rowal = mysql_fetch_assoc($sql_get_allp))
				{
					$pal_1[] = $rowal;
				}
			}
			
			if(!empty($pal_1) && count($pal_1))
			{
				for($pl=0;$pl<count($pal_1);$pl++)
				{
					$chk_typep_1 = 'artist_project|'.$pal_1[$pl]['id'];
					$sql = mysql_query("SELECT * FROM artist_project WHERE creators_info='".$chk_typep_1."' AND del_status=0 AND active=0");
					if(mysql_num_rows($sql)>0)
					{
						while($row_po = mysql_fetch_assoc($sql))
						{
							if($row_po['artist_id']!=$id['artist_id'])
							{
								$row_po['table_name'] = 'artist_project';
								$sql = mysql_query("SELECT MIN( `id` ) FROM `artist_project_profiles` WHERE artist_project_id='".$row_po['id']."' AND subtype_id!=0");

								if(mysql_num_rows($sql)>0)
								{
									$res = mysql_fetch_assoc($sql);
									
									$sql2 = mysql_query("select * from artist_project_profiles where id='".$res['MIN( `id` )']."'");
									$res2 = mysql_fetch_assoc($sql2);

									$sql4 = mysql_query("select * from  subtype where subtype_id = '".$res2['subtype_id']."'");
									if(mysql_num_rows($sql4)>0)
									{
										$res4 = mysql_fetch_assoc($sql4);
										$row_po['type'] = $res4['name'];
									}
									else
									{
										$row_po['type'] = '';
									}
								}
								else
								{
									$row_po['type'] = '';
								}
								$art_p_ao[] = $row_po;
							}
						}
					}
					
					$sql = mysql_query("SELECT * FROM community_project WHERE creators_info='".$chk_typep_1."' AND del_status=0 AND active=0");
					if(mysql_num_rows($sql)>0)
					{
						while($row_poo = mysql_fetch_assoc($sql))
						{
							if($row_poo['community_id']!=$id['community_id'])
							{
								$row_poo['table_name'] = 'community_project';
								$sql = mysql_query("SELECT MIN( `id` ) FROM `community_project_profiles` WHERE community_project_id='".$row_poo['id']."' AND subtype_id!=0");

								if(mysql_num_rows($sql)>0)
								{
									$res = mysql_fetch_assoc($sql);
									
									$sql2 = mysql_query("select * from community_project_profiles where id='".$res['MIN( `id` )']."'");
									$res2 = mysql_fetch_assoc($sql2);

									$sql4 = mysql_query("select * from  subtype where subtype_id = '".$res2['subtype_id']."'");
									if(mysql_num_rows($sql4)>0)
									{
										$res4 = mysql_fetch_assoc($sql4);
										$row_poo['type'] = $res4['name'];
									}
									else
									{
										$row_poo['type'] = '';
									}
								}
								else
								{
									$row_poo['type'] = '';
								}
								$com_p_ao[] = $row_poo;
							}
						}
					}
				}
			}
			
			/* if(!empty($pal_1) && count($pal_1))
			{
				for($pl=0;$pl<count($pal_1);$pl++)
				{
					$chk_typep_1 = 'artist_project|'.$pal_1[$pl]['id'];
					$sql = mysql_query("SELECT * FROM artist_event WHERE creators_info='".$chk_typep_1."' AND del_status=0 AND active=0");
					if(mysql_num_rows($sql)>0)
					{
						while($row_po = mysql_fetch_assoc($sql))
						{
							if($row_po['artist_id']!=$id['artist_id'])
							{
								$art_e_ao[] = $row_po;
							}
						}
					}
					
					$sql = mysql_query("SELECT * FROM community_event WHERE creators_info='".$chk_typep_1."' AND del_status=0 AND active=0");
					if(mysql_num_rows($sql)>0)
					{
						while($row_poo = mysql_fetch_assoc($sql))
						{
							if($row_poo['community_id']!=$id['community_id'])
							{
								$com_e_ao[] = $row_poo;
							}
						}
					}
				}
			} */
			
			$sql_pro_arta = mysql_query("SELECT * FROM artist_project WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_pro_arta)>0)
			{
				while($rowaa = mysql_fetch_assoc($sql_pro_arta))
				{
					if($rowaa['artist_id']!=$id['artist_id'])
					{
						$rowaa['table_name'] = 'artist_project';
						$sql = mysql_query("SELECT MIN( `id` ) FROM `artist_project_profiles` WHERE artist_project_id='".$rowaa['id']."' AND subtype_id!=0");

						if(mysql_num_rows($sql)>0)
						{
							$res = mysql_fetch_assoc($sql);
							
							$sql2 = mysql_query("select * from artist_project_profiles where id='".$res['MIN( `id` )']."'");
							$res2 = mysql_fetch_assoc($sql2);

							$sql4 = mysql_query("select * from  subtype where subtype_id = '".$res2['subtype_id']."'");
							if(mysql_num_rows($sql4)>0)
							{
								$res4 = mysql_fetch_assoc($sql4);
								$rowaa['type'] = $res4['name'];
							}
							else
							{
								$rowaa['type'] = '';
							}
						}
						else
						{
							$rowaa['type'] = '';
						}
						$arta[] = $rowaa;
					}
				}
			}
			
			/* $sql_pro_arta = mysql_query("SELECT * FROM artist_event WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_pro_arta)>0)
			{
				while($rowaa = mysql_fetch_assoc($sql_pro_arta))
				{
					if($rowaa['artist_id']!=$id['artist_id'])
					{
						$artae[] = $rowaa;
					}
				}
			} */
			
			$sql_pro_coma = mysql_query("SELECT * FROM community_project WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_pro_coma)>0)
			{
				while($rowam = mysql_fetch_assoc($sql_pro_coma))
				{
					if($rowam['community_id']!=$id['community_id'])
					{
						$rowam['table_name'] = 'community_project';
						$sql = mysql_query("SELECT MIN( `id` ) FROM `community_project_profiles` WHERE community_project_id='".$rowam['id']."' AND subtype_id!=0");

						if(mysql_num_rows($sql)>0)
						{
							$res = mysql_fetch_assoc($sql);
							
							$sql2 = mysql_query("select * from community_project_profiles where id='".$res['MIN( `id` )']."'");
							$res2 = mysql_fetch_assoc($sql2);

							$sql4 = mysql_query("select * from  subtype where subtype_id = '".$res2['subtype_id']."'");
							if(mysql_num_rows($sql4)>0)
							{
								$res4 = mysql_fetch_assoc($sql4);
								$rowam['type'] = $res4['name'];
							}
							else
							{
								$rowam['type'] = '';
							}
						}
						else
						{
							$rowam['type'] = '';
						}
						$coma[] = $rowam;
					}
				}
			}
			
			/* $sql_pro_coma = mysql_query("SELECT * FROM community_event WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_pro_coma)>0)
			{
				while($rowam = mysql_fetch_assoc($sql_pro_coma))
				{
					if($rowam['community_id']!=$id['community_id'])
					{
						$comae[] = $rowam;
					}
				}
			} */
		}
		
		if($id['community_id']!=0 && !empty($id['community_id']))
		{
			$chk_type = $var_type_2.'|'.$id['community_id'];
			$sql_pro_artm = mysql_query("SELECT * FROM artist_project WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_pro_artm)>0)
			{
				while($rowcm = mysql_fetch_assoc($sql_pro_artm))
				{
					if($rowcm['artist_id']!=$id['artist_id'])
					{
						$rowcm['table_name'] = 'artist_project';
						$sql = mysql_query("SELECT MIN( `id` ) FROM `artist_project_profiles` WHERE artist_project_id='".$rowcm['id']."' AND subtype_id!=0");

						if(mysql_num_rows($sql)>0)
						{
							$res = mysql_fetch_assoc($sql);
							
							$sql2 = mysql_query("select * from artist_project_profiles where id='".$res['MIN( `id` )']."'");
							$res2 = mysql_fetch_assoc($sql2);

							$sql4 = mysql_query("select * from  subtype where subtype_id = '".$res2['subtype_id']."'");
							if(mysql_num_rows($sql4)>0)
							{
								$res4 = mysql_fetch_assoc($sql4);
								$rowcm['type'] = $res4['name'];
							}
							else
							{
								$rowcm['type'] = '';
							}
						}
						else
						{
							$rowcm['type'] = '';
						}
						$artm[] = $rowcm;
					}
				}
			}
			
			/* $chk_type = $var_type_2.'|'.$id['community_id'];
			$sql_pro_artm = mysql_query("SELECT * FROM artist_event WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_pro_artm)>0)
			{
				while($rowcm = mysql_fetch_assoc($sql_pro_artm))
				{
					if($rowcm['artist_id']!=$id['artist_id'])
					{
						$artme[] = $rowcm;
					}
				}
			} */
			
			$sql_pro_comm = mysql_query("SELECT * FROM community_project WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_pro_comm)>0)
			{
				while($rowca = mysql_fetch_assoc($sql_pro_comm))
				{
					if($rowca['community_id']!=$id['community_id'])
					{
						$rowca['table_name'] = 'community_project';
						$sql = mysql_query("SELECT MIN( `id` ) FROM `community_project_profiles` WHERE community_project_id='".$rowca['id']."' AND subtype_id!=0");

						if(mysql_num_rows($sql)>0)
						{
							$res = mysql_fetch_assoc($sql);
							
							$sql2 = mysql_query("select * from community_project_profiles where id='".$res['MIN( `id` )']."'");
							$res2 = mysql_fetch_assoc($sql2);

							$sql4 = mysql_query("select * from  subtype where subtype_id = '".$res2['subtype_id']."'");
							if(mysql_num_rows($sql4)>0)
							{
								$res4 = mysql_fetch_assoc($sql4);
								$rowca['type'] = $res4['name'];
							}
							else
							{
								$rowca['type'] = '';
							}
						}
						else
						{
							$rowca['type'] = '';
						}
						$comm[] = $rowca;
					}
				}
			}
			
			/* $sql_pro_comm = mysql_query("SELECT * FROM community_event WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_pro_comm)>0)
			{
				while($rowca = mysql_fetch_assoc($sql_pro_comm))
				{
					if($rowca['community_id']!=$id['community_id'])
					{
						$comme[] = $rowca;
					}
				}
			} */
			
			$sql_get_allp = mysql_query("SELECT * FROM community_project WHERE community_id='".$id['community_id']."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_get_allp)>0)
			{
				while($rowal = mysql_fetch_assoc($sql_get_allp))
				{
					$pal_2[] = $rowal;
				}
			}
			
			if(!empty($pal_2) && count($pal_2))
			{
				for($pl2=0;$pl2<count($pal_2);$pl2++)
				{
					$chk_typep_2 = 'community_project|'.$pal_2[$pl2]['id'];
					$sql = mysql_query("SELECT * FROM artist_project WHERE creators_info='".$chk_typep_2."' AND del_status=0 AND active=0");
					if(mysql_num_rows($sql)>0)
					{
						while($row_po = mysql_fetch_assoc($sql))
						{
							if($row_po['artist_id']!=$id['artist_id'])
							{
								$row_po['table_name'] = 'artist_project';
								$sql = mysql_query("SELECT MIN( `id` ) FROM `artist_project_profiles` WHERE artist_project_id='".$row_po['id']."' AND subtype_id!=0");

								if(mysql_num_rows($sql)>0)
								{
									$res = mysql_fetch_assoc($sql);
									
									$sql2 = mysql_query("select * from artist_project_profiles where id='".$res['MIN( `id` )']."'");
									$res2 = mysql_fetch_assoc($sql2);

									$sql4 = mysql_query("select * from  subtype where subtype_id = '".$res2['subtype_id']."'");
									if(mysql_num_rows($sql4)>0)
									{
										$res4 = mysql_fetch_assoc($sql4);
										$row_po['type'] = $res4['name'];
									}
									else
									{
										$row_po['type'] = '';
									}
								}
								else
								{
									$row_po['type'] = '';
								}
								$art_p_co[] = $row_po;
							}
						}
					}
					
					$sql = mysql_query("SELECT * FROM community_project WHERE creators_info='".$chk_typep_2."' AND del_status=0 AND active=0");
					if(mysql_num_rows($sql)>0)
					{
						while($row_poo = mysql_fetch_assoc($sql))
						{
							if($row_poo['community_id']!=$id['community_id'])
							{
								$row_poo['table_name'] = 'community_project';
								$sql = mysql_query("SELECT MIN( `id` ) FROM `community_project_profiles` WHERE community_project_id='".$row_poo['id']."' AND subtype_id!=0");

								if(mysql_num_rows($sql)>0)
								{
									$res = mysql_fetch_assoc($sql);
									
									$sql2 = mysql_query("select * from community_project_profiles where id='".$res['MIN( `id` )']."'");
									$res2 = mysql_fetch_assoc($sql2);

									$sql4 = mysql_query("select * from  subtype where subtype_id = '".$res2['subtype_id']."'");
									if(mysql_num_rows($sql4)>0)
									{
										$res4 = mysql_fetch_assoc($sql4);
										$row_poo['type'] = $res4['name'];
									}
									else
									{
										$row_poo['type'] = '';
									}
								}
								else
								{
									$row_poo['type'] = '';
								}
								$com_p_co[] = $row_poo;
							}
						}
					}
				}
			}
			
			/* if(!empty($pal_2) && count($pal_2))
			{
				for($pl2=0;$pl2<count($pal_2);$pl2++)
				{
					$chk_typep_2 = 'community_project|'.$pal_2[$pl2]['id'];
					$sql = mysql_query("SELECT * FROM artist_event WHERE creators_info='".$chk_typep_2."' AND del_status=0 AND active=0");
					if(mysql_num_rows($sql)>0)
					{
						while($row_po = mysql_fetch_assoc($sql))
						{
							if($row_po['artist_id']!=$id['artist_id'])
							{
								$art_e_co[] = $row_po;
							}
						}
					}
					
					$sql = mysql_query("SELECT * FROM community_event WHERE creators_info='".$chk_typep_2."' AND del_status=0 AND active=0");
					if(mysql_num_rows($sql)>0)
					{
						while($row_poo = mysql_fetch_assoc($sql))
						{
							if($row_poo['community_id']!=$id['community_id'])
							{
								$com_e_co[] = $row_poo;
							}
						}
					}
				}
			} */
		}
		
		$totals = array();
		$totals = array_merge($artm,$arta,$comm,$coma,$art_p_ao,$com_p_ao,$art_p_co,$com_p_co,$artme,$artae,$comme,$comae,$art_e_ao,$com_e_ao,$art_e_co,$com_e_co);
		return $totals;
	}
	
	/*function get_creator_heis(){
		$get_det = $this->selgeneral();
		$get_media_art_pro = array();
		$get_media_art_eve = array();
		$get_media_com_pro = array();
		$get_media_com_eve = array();
		$get_media_artist_project1 = array();
		$get_media_artist_project2 = array();
		$get_media_artist_project3 = array();
		$get_media_artist_project4 = array();
		$get_media_community_project1 = array();
		$get_media_community_project2 = array();
		$get_media_community_project3 = array();
		$get_media_community_project4 = array();
		//if($get_det['artist_id'] !="" && $get_det['artist_id']!=0){
			$creator_info = "general_community|".$get_det['community_id'];
			$query = mysql_query("select * from artist_project where creators_info ='".$creator_info."'");
			if(mysql_num_rows($query)>0){
				while($row = mysql_fetch_assoc($query)){
					$new_creator = "artist_project|".$row['id'];
					$find_cretor_med = mysql_query("select * from general_media where creator_info='".$new_creator."' OR from_info='".$new_creator."'");
					if(mysql_num_rows($find_cretor_med)>0){
						while($res_new_det = mysql_fetch_assoc($find_cretor_med)){
							$get_media_art_pro = $res_new_det;
						}
					}
				}
			}
			$query1 = mysql_query("select * from artist_event where creators_info ='".$creator_info."'");
			if(mysql_num_rows($query1)>0){
				while($row = mysql_fetch_assoc($query1)){
					$new_creator = "artist_event|".$row['id'];
					$find_cretor_med = mysql_query("select * from general_media where creator_info='".$new_creator."' OR from_info='".$new_creator."'");
					if(mysql_num_rows($find_cretor_med)>0){
						while($res_new_det = mysql_fetch_assoc($find_cretor_med)){
							$get_media_art_eve = $res_new_det;
						}
					}
				}
			}
			$query = mysql_query("select * from community_project where creators_info ='".$creator_info."'");
			if(mysql_num_rows($query)>0){
				while($row = mysql_fetch_assoc($query)){
					$new_creator = "community_project|".$row['id'];
					$find_cretor_med = mysql_query("select * from general_media where creator_info='".$new_creator."' OR from_info='".$new_creator."'");
					if(mysql_num_rows($find_cretor_med)>0){
						while($res_new_det = mysql_fetch_assoc($find_cretor_med)){
							$get_media_com_pro = $res_new_det;
						}
					}
				}
			}
			$query1 = mysql_query("select * from community_event where creators_info ='".$creator_info."'");
			if(mysql_num_rows($query1)>0){
				while($row = mysql_fetch_assoc($query1)){
					$new_creator = "community_event|".$row['id'];
					$find_cretor_med = mysql_query("select * from general_media where creator_info='".$new_creator."' OR from_info='".$new_creator."'");
					if(mysql_num_rows($find_cretor_med)>0){
						while($res_new_det = mysql_fetch_assoc($find_cretor_med)){
							$get_media_com_eve = $res_new_det;
						}
					}
				}
			}
			
			$creator_info = "general_artist|".$get_det['artist_id'];
			$query = mysql_query("select * from artist_project where creators_info ='".$creator_info."'");
			if(mysql_num_rows($query)>0){
				while($row = mysql_fetch_assoc($query)){
					$new_creator = "artist_project|".$row['id'];
					$find_cretor_med = mysql_query("select * from general_media where creator_info='".$new_creator."' OR from_info='".$new_creator."'");
					if(mysql_num_rows($find_cretor_med)>0){
						while($res_new_det = mysql_fetch_assoc($find_cretor_med)){
							$get_media_art_pro = $res_new_det;
						}
					}
				}
			}
			$query1 = mysql_query("select * from artist_event where creators_info ='".$creator_info."'");
			if(mysql_num_rows($query1)>0){
				while($row = mysql_fetch_assoc($query1)){
					$new_creator = "artist_event|".$row['id'];
					$find_cretor_med = mysql_query("select * from general_media where creator_info='".$new_creator."' OR from_info='".$new_creator."'");
					if(mysql_num_rows($find_cretor_med)>0){
						while($res_new_det = mysql_fetch_assoc($find_cretor_med)){
							$get_media_art_eve = $res_new_det;
						}
					}
				}
			}
			$query = mysql_query("select * from community_project where creators_info ='".$creator_info."'");
			if(mysql_num_rows($query)>0){
				while($row = mysql_fetch_assoc($query)){
					$new_creator = "community_project|".$row['id'];
					$find_cretor_med = mysql_query("select * from general_media where creator_info='".$new_creator."' OR from_info='".$new_creator."'");
					if(mysql_num_rows($find_cretor_med)>0){
						while($res_new_det = mysql_fetch_assoc($find_cretor_med)){
							$get_media_com_pro = $res_new_det;
						}
					}
				}
			}
			$query1 = mysql_query("select * from community_event where creators_info ='".$creator_info."'");
			if(mysql_num_rows($query1)>0){
				while($row = mysql_fetch_assoc($query1)){
					$new_creator = "community_event|".$row['id'];
					$find_cretor_med = mysql_query("select * from general_media where creator_info='".$new_creator."' OR from_info='".$new_creator."'");
					if(mysql_num_rows($find_cretor_med)>0){
						while($res_new_det = mysql_fetch_assoc($find_cretor_med)){
							$get_media_com_eve = $res_new_det;
						}
					}
				}
			}
			/**get community projects**//*
			$get_compro = mysql_query("select * from community_project where community_id = '".$get_det['community_id']."'");
			if(mysql_num_rows($get_compro)>0){
				while($res_com_pro = mysql_fetch_assoc($get_compro))
				{
					$creator_info = "community_project|".$res_com_pro['id'];
					$query = mysql_query("select * from artist_project where creators_info ='".$creator_info."'");
					if(mysql_num_rows($query)>0){
						while($row = mysql_fetch_assoc($query)){
							$new_creator = "artist_project|".$row['id'];
							$find_cretor_med = mysql_query("select * from general_media where creator_info='".$new_creator."'  OR from_info='".$new_creator."'");
							if(mysql_num_rows($find_cretor_med)>0){
								while($res_new_det = mysql_fetch_assoc($find_cretor_med)){
									$get_media_community_project1 = $res_new_det;
								}
							}
						}
					}
					$query = mysql_query("select * from artist_event where creators_info ='".$creator_info."'");
					if(mysql_num_rows($query)>0){
						while($row = mysql_fetch_assoc($query)){
							$new_creator = "artist_event|".$row['id'];
							$find_cretor_med = mysql_query("select * from general_media where creator_info='".$new_creator."' OR from_info='".$new_creator."'");
							if(mysql_num_rows($find_cretor_med)>0){
								while($res_new_det = mysql_fetch_assoc($find_cretor_med)){
									$get_media_community_project2 = $res_new_det;
								}
							}
						}
					}
					$query = mysql_query("select * from community_project where creators_info ='".$creator_info."'");
					if(mysql_num_rows($query)>0){
						while($row = mysql_fetch_assoc($query)){
							$new_creator = "community_project|".$row['id'];
							$find_cretor_med = mysql_query("select * from general_media where creator_info='".$new_creator."' OR from_info='".$new_creator."'");
							if(mysql_num_rows($find_cretor_med)>0){
								while($res_new_det = mysql_fetch_assoc($find_cretor_med)){
									$get_media_community_project3 = $res_new_det;
								}
							}
						}
					}
					$query = mysql_query("select * from community_event where creators_info ='".$creator_info."'");
					if(mysql_num_rows($query)>0){
						while($row = mysql_fetch_assoc($query)){
							$new_creator = "community_event|".$row['id'];
							$find_cretor_med = mysql_query("select * from general_media where creator_info='".$new_creator."' OR from_info='".$new_creator."'");
							if(mysql_num_rows($find_cretor_med)>0){
								while($res_new_det = mysql_fetch_assoc($find_cretor_med)){
									$get_media_community_project4 = $res_new_det;
								}
							}
						}
					}
				}
			}
			
			/**get community projects ends here**/
			
			/**get artist projects**//*
			$get_compro = mysql_query("select * from artist_project where artist_id = '".$get_det['artist_id']."'");
			if(mysql_num_rows($get_compro)>0){
				while($res_com_pro = mysql_fetch_assoc($get_compro))
				{
					$creator_info = "artist_project|".$res_com_pro['id'];
					$query = mysql_query("select * from artist_project where creators_info ='".$creator_info."'");
					if(mysql_num_rows($query)>0){
						while($row = mysql_fetch_assoc($query)){
							$new_creator = "artist_project|".$row['id'];
							$find_cretor_med = mysql_query("select * from general_media where creator_info='".$new_creator."' OR from_info='".$new_creator."'");
							if(mysql_num_rows($find_cretor_med)>0){
								while($res_new_det = mysql_fetch_assoc($find_cretor_med)){
									$get_media_artist_project1 = $res_new_det;
								}
							}
						}
					}
					$query = mysql_query("select * from artist_event where creators_info ='".$creator_info."'");
					if(mysql_num_rows($query)>0){
						while($row = mysql_fetch_assoc($query)){
							$new_creator = "artist_event|".$row['id'];
							$find_cretor_med = mysql_query("select * from general_media where creator_info='".$new_creator."' OR from_info='".$new_creator."'");
							if(mysql_num_rows($find_cretor_med)>0){
								while($res_new_det = mysql_fetch_assoc($find_cretor_med)){
									$get_media_artist_project2 = $res_new_det;
								}
							}
						}
					}
					$query = mysql_query("select * from community_project where creators_info ='".$creator_info."'");
					if(mysql_num_rows($query)>0){
						while($row = mysql_fetch_assoc($query)){
							$new_creator = "community_project|".$row['id'];
							$find_cretor_med = mysql_query("select * from general_media where creator_info='".$new_creator."' OR from_info='".$new_creator."'");
							if(mysql_num_rows($find_cretor_med)>0){
								while($res_new_det = mysql_fetch_assoc($find_cretor_med)){
									$get_media_artist_project3 = $res_new_det;
								}
							}
						}
					}
					$query = mysql_query("select * from community_event where creators_info ='".$creator_info."'");
					if(mysql_num_rows($query)>0){
						while($row = mysql_fetch_assoc($query)){
							$new_creator = "community_event|".$row['id'];
							$find_cretor_med = mysql_query("select * from general_media where creator_info='".$new_creator."' OR from_info='".$new_creator."'");
							if(mysql_num_rows($find_cretor_med)>0){
								while($res_new_det = mysql_fetch_assoc($find_cretor_med)){
									$get_media_artist_project4 = $res_new_det;
								}
							}
						}
					}
				}
			}
			
			/**get artist projects ends here**//*

		$get_media = array_merge($get_media_art_pro,$get_media_art_eve,$get_media_com_eve,$get_media_com_pro,$get_media_artist_project1,$get_media_artist_project2,$get_media_artist_project3,$get_media_artist_project4,$get_media_community_project1,$get_media_community_project2,$get_media_community_project3,$get_media_community_project4);
		return($get_media);
	}*/
	
	/*function get_from_heis(){
		$get_det = $this->selgeneral();
		$get_media_art_pro = array();
		$get_media_art_eve = array();
		$get_media_com_pro = array();
		$get_media_com_eve = array();
		$get_media_artist_project1 = array();
		$get_media_artist_project2 = array();
		$get_media_artist_project3 = array();
		$get_media_artist_project4 = array();
		$get_media_community_project1 = array();
		$get_media_community_project2 = array();
		$get_media_community_project3 = array();
		$get_media_community_project4 = array();
		//if($get_det['artist_id'] !="" && $get_det['artist_id']!=0){
			
			/**get community projects**//*
			$get_compro = mysql_query("select * from community_project where community_id = '".$get_det['community_id']."'");
			if(mysql_num_rows($get_compro)>0){
				while($res_com_pro = mysql_fetch_assoc($get_compro))
				{
					$creator_info = "community_project|".$res_com_pro['id'];
					$query = mysql_query("select * from artist_project where creators_info ='".$creator_info."'");
					if(mysql_num_rows($query)>0){
						while($row = mysql_fetch_assoc($query)){
							$new_creator = "artist_project|".$row['id'];
							$find_cretor_med = mysql_query("select * from general_media where creator_info='".$new_creator."'");
							if(mysql_num_rows($find_cretor_med)>0){
								while($res_new_det = mysql_fetch_assoc($find_cretor_med)){
									$get_media_community_project1 = $res_new_det;
								}
							}
						}
					}
					$query = mysql_query("select * from artist_event where creators_info ='".$creator_info."'");
					if(mysql_num_rows($query)>0){
						while($row = mysql_fetch_assoc($query)){
							$new_creator = "artist_event|".$row['id'];
							$find_cretor_med = mysql_query("select * from general_media where creator_info='".$new_creator."'");
							if(mysql_num_rows($find_cretor_med)>0){
								while($res_new_det = mysql_fetch_assoc($find_cretor_med)){
									$get_media_community_project2 = $res_new_det;
								}
							}
						}
					}
					$query = mysql_query("select * from community_project where creators_info ='".$creator_info."'");
					if(mysql_num_rows($query)>0){
						while($row = mysql_fetch_assoc($query)){
							$new_creator = "community_project|".$row['id'];
							$find_cretor_med = mysql_query("select * from general_media where creator_info='".$new_creator."'");
							if(mysql_num_rows($find_cretor_med)>0){
								while($res_new_det = mysql_fetch_assoc($find_cretor_med)){
									$get_media_community_project3 = $res_new_det;
								}
							}
						}
					}
					$query = mysql_query("select * from community_event where creators_info ='".$creator_info."'");
					if(mysql_num_rows($query)>0){
						while($row = mysql_fetch_assoc($query)){
							$new_creator = "community_event|".$row['id'];
							$find_cretor_med = mysql_query("select * from general_media where creator_info='".$new_creator."'");
							if(mysql_num_rows($find_cretor_med)>0){
								while($res_new_det = mysql_fetch_assoc($find_cretor_med)){
									$get_media_community_project4 = $res_new_det;
								}
							}
						}
					}
				}
			}
			
			/**get community projects ends here**/
			
			/**get artist projects**//*
			$get_compro = mysql_query("select * from artist_project where artist_id = '".$get_det['artist_id']."'");
			if(mysql_num_rows($get_compro)>0){
				while($res_com_pro = mysql_fetch_assoc($get_compro))
				{
					$creator_info = "artist_project|".$res_com_pro['id'];
					$query = mysql_query("select * from artist_project where creators_info ='".$creator_info."'");
					if(mysql_num_rows($query)>0){
						while($row = mysql_fetch_assoc($query)){
							$new_creator = "artist_project|".$row['id'];
							$find_cretor_med = mysql_query("select * from general_media where creator_info='".$new_creator."'");
							if(mysql_num_rows($find_cretor_med)>0){
								while($res_new_det = mysql_fetch_assoc($find_cretor_med)){
									$get_media_artist_project1 = $res_new_det;
								}
							}
						}
					}
					$query = mysql_query("select * from artist_event where creators_info ='".$creator_info."'");
					if(mysql_num_rows($query)>0){
						while($row = mysql_fetch_assoc($query)){
							$new_creator = "artist_event|".$row['id'];
							$find_cretor_med = mysql_query("select * from general_media where creator_info='".$new_creator."'");
							if(mysql_num_rows($find_cretor_med)>0){
								while($res_new_det = mysql_fetch_assoc($find_cretor_med)){
									$get_media_artist_project2 = $res_new_det;
								}
							}
						}
					}
					$query = mysql_query("select * from community_project where creators_info ='".$creator_info."'");
					if(mysql_num_rows($query)>0){
						while($row = mysql_fetch_assoc($query)){
							$new_creator = "community_project|".$row['id'];
							$find_cretor_med = mysql_query("select * from general_media where creator_info='".$new_creator."'");
							if(mysql_num_rows($find_cretor_med)>0){
								while($res_new_det = mysql_fetch_assoc($find_cretor_med)){
									$get_media_artist_project3 = $res_new_det;
								}
							}
						}
					}
					$query = mysql_query("select * from community_event where creators_info ='".$creator_info."'");
					if(mysql_num_rows($query)>0){
						while($row = mysql_fetch_assoc($query)){
							$new_creator = "community_event|".$row['id'];
							$find_cretor_med = mysql_query("select * from general_media where creator_info='".$new_creator."'");
							if(mysql_num_rows($find_cretor_med)>0){
								while($res_new_det = mysql_fetch_assoc($find_cretor_med)){
									$get_media_artist_project4 = $res_new_det;
								}
							}
						}
					}
				}
			}
			
			/**get artist projects ends here**//*

		$get_media = array_merge($get_media_artist_project1,$get_media_artist_project2,$get_media_artist_project3,$get_media_artist_project4,$get_media_community_project1,$get_media_community_project2,$get_media_community_project3,$get_media_community_project4);
		return($get_media);
	}*/
	
	/* function only_creator_upevent()
	{
		$date = date("Y-m-d");
		
		$new_id = $this->selgeneral();
		$final_pros = array();
		
		if(isset($new_id['community_id']) && !empty($new_id['community_id']) && $new_id['community_id']!=0)
		{
			$mcth = 'general_community|'.$new_id['community_id'];
			
			$sql_apro = mysql_query("SELECT * FROM artist_event WHERE creators_info='".$mcth."' AND del_status=0 AND active=0 AND date>='".$date."'");
			if(mysql_num_rows($sql_apro)>0)
			{
				while($ans_apro = mysql_fetch_assoc($sql_apro))
				{				
					$ans_apro['id'] = $ans_apro['id'].'~art';
					$final_pros[] = $ans_apro;
				}	
			}
			
			$sql_cpro = mysql_query("SELECT * FROM community_event WHERE creators_info='".$mcth."' AND del_status=0 AND active=0 AND date>='".$date."'");
			if(mysql_num_rows($sql_cpro)>0)
			{
				while($ans_cpro = mysql_fetch_assoc($sql_cpro))
				{				
					$ans_cpro['id'] = $ans_cpro['id'].'~com';
					$final_pros[] = $ans_cpro;
				}
			}
		}
		
		return $final_pros;
	} */
	
	/* function only_creator_recevent()
	{
		$date = date("Y-m-d");
		
		$new_id = $this->selgeneral();
		$final_pros = array();
		
		if(isset($new_id['community_id']) && !empty($new_id['community_id']) && $new_id['community_id']!=0)
		{
			$mcth = 'general_community|'.$new_id['community_id'];
			
			$sql_apro = mysql_query("SELECT * FROM artist_event WHERE creators_info='".$mcth."' AND del_status=0 AND active=0 AND date<'".$date."'");
			if(mysql_num_rows($sql_apro)>0)
			{
				while($ans_apro = mysql_fetch_assoc($sql_apro))
				{				
					$ans_apro['id'] = $ans_apro['id'].'~art';
					$final_pros[] = $ans_apro;
				}	
			}
			
			$sql_cpro = mysql_query("SELECT * FROM community_event WHERE creators_info='".$mcth."' AND del_status=0 AND active=0 AND date<'".$date."'");
			if(mysql_num_rows($sql_cpro)>0)
			{
				while($ans_cpro = mysql_fetch_assoc($sql_cpro))
				{				
					$ans_cpro['id'] = $ans_cpro['id'].'~com';
					$final_pros[] = $ans_cpro;
				}
			}
		}
		
		return $final_pros;
	} */
	
	function only_creator_upevent()
	{
		$id = $this->selgeneral();
		$date = date("Y-m-d");
		
		$var_type_1 = 'general_artist';
		$var_type_2 = 'general_community';
		$var_type_3 = 'artist_project';
		$var_type_4 = 'community_project';
		
		$artm = array();
		$arta = array();
		$comm = array();
		$coma = array();
		
		$artme = array();
		$artae = array();
		$comme = array();
		$comae = array();
		
		$art_p_ao = array();
		$com_p_ao = array();
		$art_p_co = array();
		$com_p_co = array();
		
		$art_e_ao = array();
		$com_e_ao = array();
		$art_e_co = array();
		$com_e_co = array();
		
		$pal_1 = array();
		$pal_2 = array();
		
		if($id['artist_id']!=0 && !empty($id['artist_id']))
		{
			$chk_type = $var_type_1.'|'.$id['artist_id'];
			
			$sql_get_allp = mysql_query("SELECT * FROM artist_project WHERE artist_id='".$id['artist_id']."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_get_allp)>0)
			{
				while($rowal = mysql_fetch_assoc($sql_get_allp))
				{
					$pal_1[] = $rowal;
				}
			}
			
			
			
			 if(!empty($pal_1) && count($pal_1))
			{
				for($pl=0;$pl<count($pal_1);$pl++)
				{
					$chk_typep_1 = 'artist_project|'.$pal_1[$pl]['id'];
					
					$sql = mysql_query("SELECT * FROM artist_event WHERE creators_info='".$chk_typep_1."' AND del_status=0 AND active=0 AND date>='".$date."'");
					if(mysql_num_rows($sql)>0)
					{
						while($row_po = mysql_fetch_assoc($sql))
						{
							if($row_po['artist_id']!=$id['artist_id'])
							{
								$row_po['id'] = $row_po['id'].'~art';
								$art_e_ao[] = $row_po;
							}
						}
					}
					
					$sql = mysql_query("SELECT * FROM community_event WHERE creators_info='".$chk_typep_1."' AND del_status=0 AND active=0 AND date>='".$date."'");
					if(mysql_num_rows($sql)>0)
					{
						while($row_poo = mysql_fetch_assoc($sql))
						{
							if($row_poo['community_id']!=$id['community_id'])
							{
								$row_poo['id'] = $row_poo['id'].'~com';
								$com_e_ao[] = $row_poo;
							}
						}
					}
				}
			} 

			/*$sql_pro_arta = mysql_query("SELECT * FROM artist_project WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_pro_arta)>0)
			{
				while($rowaa = mysql_fetch_assoc($sql_pro_arta))
				{
					if($rowaa['artist_id']!=$id['artist_id'])
					{
						$rowaa['id'] = $rowaa['id'].'~art';
						$arta[] = $rowaa;
					}
				}
			}*/
			
			$sql_pro_arta = mysql_query("SELECT * FROM artist_event WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0 AND date>='".$date."'");
			if(mysql_num_rows($sql_pro_arta)>0)
			{
				while($rowaa = mysql_fetch_assoc($sql_pro_arta))
				{
					if($rowaa['artist_id']!=$id['artist_id'])
					{
						$rowaa['id'] = $rowaa['id'].'~art';
						$artae[] = $rowaa;
					}
				}
			} 
			
			/*$sql_pro_coma = mysql_query("SELECT * FROM community_project WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_pro_coma)>0)
			{
				while($rowam = mysql_fetch_assoc($sql_pro_coma))
				{
					if($rowam['community_id']!=$id['community_id'])
					{
						$rowam['id'] = $rowam['id'].'~com';
						$coma[] = $rowam;
					}
				}
			}*/
			
			$sql_pro_coma = mysql_query("SELECT * FROM community_event WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0 AND date>='".$date."'");
			if(mysql_num_rows($sql_pro_coma)>0)
			{
				while($rowam = mysql_fetch_assoc($sql_pro_coma))
				{
					if($rowam['community_id']!=$id['community_id'])
					{
						$rowam['id'] = $rowam['id'].'~com';
						$comae[] = $rowam;
					}
				}
			} 
		}
		
		if($id['community_id']!=0 && !empty($id['community_id']))
		{
			/*$chk_type = $var_type_2.'|'.$id['community_id'];
			$sql_pro_artm = mysql_query("SELECT * FROM artist_project WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_pro_artm)>0)
			{
				while($rowcm = mysql_fetch_assoc($sql_pro_artm))
				{
					if($rowcm['artist_id']!=$id['artist_id'])
					{
						$rowcm['id'] = $rowcm['id'].'~art';
						$artm[] = $rowcm;
					}
				}
			}*/
			
			 $chk_type = $var_type_2.'|'.$id['community_id'];
			$sql_pro_artm = mysql_query("SELECT * FROM artist_event WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0 AND date>='".$date."'");
			if(mysql_num_rows($sql_pro_artm)>0)
			{
				while($rowcm = mysql_fetch_assoc($sql_pro_artm))
				{
					if($rowcm['artist_id']!=$id['artist_id'])
					{
						$rowcm['id'] = $rowcm['id'].'~art';
						$artme[] = $rowcm;
					}
				}
			} 
			
			/*$sql_pro_comm = mysql_query("SELECT * FROM community_project WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_pro_comm)>0)
			{
				while($rowca = mysql_fetch_assoc($sql_pro_comm))
				{
					if($rowca['community_id']!=$id['community_id'])
					{
						$rowca['id'] = $rowca['id'].'~com';
						$comm[] = $rowca;
					}
				}
			}*/
			
			 $sql_pro_comm = mysql_query("SELECT * FROM community_event WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0 AND date>='".$date."'");
			if(mysql_num_rows($sql_pro_comm)>0)
			{
				while($rowca = mysql_fetch_assoc($sql_pro_comm))
				{
					if($rowca['community_id']!=$id['community_id'])
					{
						$rowca['id'] = $rowca['id'].'~com';
						$comme[] = $rowca;
					}
				}
			} 
			
			$sql_get_allp = mysql_query("SELECT * FROM community_project WHERE community_id='".$id['community_id']."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_get_allp)>0)
			{
				while($rowal = mysql_fetch_assoc($sql_get_allp))
				{
					$pal_2[] = $rowal;
				}
			}
			
			/*if(!empty($pal_2) && count($pal_2))
			{
				for($pl2=0;$pl2<count($pal_2);$pl2++)
				{
					$chk_typep_2 = 'community_project|'.$pal_2[$pl2]['id'];
					$sql = mysql_query("SELECT * FROM artist_project WHERE creators_info='".$chk_typep_2."' AND del_status=0 AND active=0");
					if(mysql_num_rows($sql)>0)
					{
						while($row_po = mysql_fetch_assoc($sql))
						{
							if($row_po['artist_id']!=$id['artist_id'])
							{
								$row_po['id'] = $row_po['id'].'~art';
								$art_p_co[] = $row_po;
							}
						}
					}
					
					$sql = mysql_query("SELECT * FROM community_project WHERE creators_info='".$chk_typep_2."' AND del_status=0 AND active=0");
					if(mysql_num_rows($sql)>0)
					{
						while($row_poo = mysql_fetch_assoc($sql))
						{
							if($row_poo['community_id']!=$id['community_id'])
							{
								$row_poo['id'] = $row_poo['id'].'~com';
								$com_p_co[] = $row_poo;
							}
						}
					}
				}
			}*/
			
			 if(!empty($pal_2) && count($pal_2))
			{
				for($pl2=0;$pl2<count($pal_2);$pl2++)
				{
					$chk_typep_2 = 'community_project|'.$pal_2[$pl2]['id'];
					$sql = mysql_query("SELECT * FROM artist_event WHERE creators_info='".$chk_typep_2."' AND del_status=0 AND active=0 AND date>='".$date."'");
					if(mysql_num_rows($sql)>0)
					{
						while($row_po = mysql_fetch_assoc($sql))
						{
							if($row_po['artist_id']!=$id['artist_id'])
							{
								$row_po['id'] = $row_po['id'].'~art';
								$art_e_co[] = $row_po;
							}
						}
					}
					
					$sql = mysql_query("SELECT * FROM community_event WHERE creators_info='".$chk_typep_2."' AND del_status=0 AND active=0 AND date>='".$date."'");
					if(mysql_num_rows($sql)>0)
					{
						while($row_poo = mysql_fetch_assoc($sql))
						{
							if($row_poo['community_id']!=$id['community_id'])
							{
								$row_poo['id'] = $row_poo['id'].'~com';
								$com_e_co[] = $row_poo;
							}
						}
					}
				}
			} 
		}
		
		/* var_dump($artm);
		var_dump($arta);
		var_dump($comm);
		var_dump($coma);
		var_dump($art_p_ao);
		var_dump($com_p_ao);
		var_dump($art_p_co);
		var_dump($com_p_co); */
		
		$totals = array();
		$totals = array_merge($artm,$arta,$comm,$coma,$art_p_ao,$com_p_ao,$art_p_co,$com_p_co,$artme,$artae,$comme,$comae,$art_e_ao,$com_e_ao,$art_e_co,$com_e_co);
		return $totals;
	}
	
	function only_sh_creator_upevent()
	{
		$id = $this->selgeneral();
		$date = date("Y-m-d");
		
		$var_type_1 = 'general_artist';
		$var_type_2 = 'general_community';
		$var_type_3 = 'artist_project';
		$var_type_4 = 'community_project';
		
		$artm = array();
		$arta = array();
		$comm = array();
		$coma = array();
		
		$artme = array();
		$artae = array();
		$comme = array();
		$comae = array();
		
		$art_p_ao = array();
		$com_p_ao = array();
		$art_p_co = array();
		$com_p_co = array();
		
		$art_e_ao = array();
		$com_e_ao = array();
		$art_e_co = array();
		$com_e_co = array();
		
		$pal_1 = array();
		$pal_2 = array();
		
		if($id['artist_id']!=0 && !empty($id['artist_id']))
		{
			$chk_type = $var_type_1.'|'.$id['artist_id'];
			
			$sql_get_allp = mysql_query("SELECT * FROM artist_project WHERE artist_id='".$id['artist_id']."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_get_allp)>0)
			{
				while($rowal = mysql_fetch_assoc($sql_get_allp))
				{
					$pal_1[] = $rowal;
				}
			}
			
			
			
			 if(!empty($pal_1) && count($pal_1))
			{
				for($pl=0;$pl<count($pal_1);$pl++)
				{
					$chk_typep_1 = 'artist_project|'.$pal_1[$pl]['id'];
					
					$sql = mysql_query("SELECT * FROM artist_event WHERE creators_info='".$chk_typep_1."' AND del_status=0 AND active=0 AND date>='".$date."'");
					if(mysql_num_rows($sql)>0)
					{
						while($row_po = mysql_fetch_assoc($sql))
						{
							if($row_po['artist_id']!=$id['artist_id'])
							{
								$row_po['table_name'] = 'artist_event';
								$sql = mysql_query("SELECT MIN( `id` ) FROM `artist_event_profiles` WHERE artist_event_id='".$row_po['id']."' AND subtype_id!=0");

								if(mysql_num_rows($sql)>0)
								{
									$res = mysql_fetch_assoc($sql);
									
									$sql2 = mysql_query("select * from artist_event_profiles where id='".$res['MIN( `id` )']."'");
									$res2 = mysql_fetch_assoc($sql2);

									$sql4 = mysql_query("select * from  subtype where subtype_id = '".$res2['subtype_id']."'");
									if(mysql_num_rows($sql4)>0)
									{
										$res4 = mysql_fetch_assoc($sql4);
										$row_po['type'] = $res4['name'];
									}
									else
									{
										$row_po['type'] = '';
									}
								}
								else
								{
									$row_po['type'] = '';
								}
								$art_e_ao[] = $row_po;
							}
						}
					}
					
					$sql = mysql_query("SELECT * FROM community_event WHERE creators_info='".$chk_typep_1."' AND del_status=0 AND active=0 AND date>='".$date."'");
					if(mysql_num_rows($sql)>0)
					{
						while($row_poo = mysql_fetch_assoc($sql))
						{
							if($row_poo['community_id']!=$id['community_id'])
							{
								$row_poo['table_name'] = 'community_event';
								$sql = mysql_query("SELECT MIN( `id` ) FROM `community_event_profiles` WHERE community_event_id='".$row_poo['id']."' AND subtype_id!=0");

								if(mysql_num_rows($sql)>0)
								{
									$res = mysql_fetch_assoc($sql);
									
									$sql2 = mysql_query("select * from community_event_profiles where id='".$res['MIN( `id` )']."'");
									$res2 = mysql_fetch_assoc($sql2);

									$sql4 = mysql_query("select * from  subtype where subtype_id = '".$res2['subtype_id']."'");
									if(mysql_num_rows($sql4)>0)
									{
										$res4 = mysql_fetch_assoc($sql4);
										$row_poo['type'] = $res4['name'];
									}
									else
									{
										$row_poo['type'] = '';
									}
								}
								else
								{
									$row_poo['type'] = '';
								}
								$com_e_ao[] = $row_poo;
							}
						}
					}
				}
			} 

			/*$sql_pro_arta = mysql_query("SELECT * FROM artist_project WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_pro_arta)>0)
			{
				while($rowaa = mysql_fetch_assoc($sql_pro_arta))
				{
					if($rowaa['artist_id']!=$id['artist_id'])
					{
						$rowaa['id'] = $rowaa['id'].'~art';
						$arta[] = $rowaa;
					}
				}
			}*/
			
			$sql_pro_arta = mysql_query("SELECT * FROM artist_event WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0 AND date>='".$date."'");
			if(mysql_num_rows($sql_pro_arta)>0)
			{
				while($rowaa = mysql_fetch_assoc($sql_pro_arta))
				{
					if($rowaa['artist_id']!=$id['artist_id'])
					{
						$rowaa['table_name'] = 'artist_event';
						$sql = mysql_query("SELECT MIN( `id` ) FROM `artist_event_profiles` WHERE artist_event_id='".$rowaa['id']."' AND subtype_id!=0");

						if(mysql_num_rows($sql)>0)
						{
							$res = mysql_fetch_assoc($sql);
							
							$sql2 = mysql_query("select * from artist_event_profiles where id='".$res['MIN( `id` )']."'");
							$res2 = mysql_fetch_assoc($sql2);

							$sql4 = mysql_query("select * from  subtype where subtype_id = '".$res2['subtype_id']."'");
							if(mysql_num_rows($sql4)>0)
							{
								$res4 = mysql_fetch_assoc($sql4);
								$rowaa['type'] = $res4['name'];
							}
							else
							{
								$rowaa['type'] = '';
							}
						}
						else
						{
							$rowaa['type'] = '';
						}
						$artae[] = $rowaa;
					}
				}
			} 
			
			/*$sql_pro_coma = mysql_query("SELECT * FROM community_project WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_pro_coma)>0)
			{
				while($rowam = mysql_fetch_assoc($sql_pro_coma))
				{
					if($rowam['community_id']!=$id['community_id'])
					{
						$rowam['id'] = $rowam['id'].'~com';
						$coma[] = $rowam;
					}
				}
			}*/
			
			$sql_pro_coma = mysql_query("SELECT * FROM community_event WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0 AND date>='".$date."'");
			if(mysql_num_rows($sql_pro_coma)>0)
			{
				while($rowam = mysql_fetch_assoc($sql_pro_coma))
				{
					if($rowam['community_id']!=$id['community_id'])
					{
						$rowam['table_name'] = 'community_event';
						$sql = mysql_query("SELECT MIN( `id` ) FROM `community_event_profiles` WHERE community_event_id='".$rowam['id']."' AND subtype_id!=0");

						if(mysql_num_rows($sql)>0)
						{
							$res = mysql_fetch_assoc($sql);
							
							$sql2 = mysql_query("select * from community_event_profiles where id='".$res['MIN( `id` )']."'");
							$res2 = mysql_fetch_assoc($sql2);

							$sql4 = mysql_query("select * from  subtype where subtype_id = '".$res2['subtype_id']."'");
							if(mysql_num_rows($sql4)>0)
							{
								$res4 = mysql_fetch_assoc($sql4);
								$rowam['type'] = $res4['name'];
							}
							else
							{
								$rowam['type'] = '';
							}
						}
						else
						{
							$rowam['type'] = '';
						}
						$comae[] = $rowam;
					}
				}
			} 
		}
		
		if($id['community_id']!=0 && !empty($id['community_id']))
		{
			/*$chk_type = $var_type_2.'|'.$id['community_id'];
			$sql_pro_artm = mysql_query("SELECT * FROM artist_project WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_pro_artm)>0)
			{
				while($rowcm = mysql_fetch_assoc($sql_pro_artm))
				{
					if($rowcm['artist_id']!=$id['artist_id'])
					{
						$rowcm['id'] = $rowcm['id'].'~art';
						$artm[] = $rowcm;
					}
				}
			}*/
			
			 $chk_type = $var_type_2.'|'.$id['community_id'];
			$sql_pro_artm = mysql_query("SELECT * FROM artist_event WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0 AND date>='".$date."'");
			if(mysql_num_rows($sql_pro_artm)>0)
			{
				while($rowcm = mysql_fetch_assoc($sql_pro_artm))
				{
					if($rowcm['artist_id']!=$id['artist_id'])
					{
						$rowcm['table_name'] = 'artist_event';
						$sql = mysql_query("SELECT MIN( `id` ) FROM `artist_event_profiles` WHERE artist_event_id='".$rowcm['id']."' AND subtype_id!=0");

						if(mysql_num_rows($sql)>0)
						{
							$res = mysql_fetch_assoc($sql);
							
							$sql2 = mysql_query("select * from artist_event_profiles where id='".$res['MIN( `id` )']."'");
							$res2 = mysql_fetch_assoc($sql2);

							$sql4 = mysql_query("select * from  subtype where subtype_id = '".$res2['subtype_id']."'");
							if(mysql_num_rows($sql4)>0)
							{
								$res4 = mysql_fetch_assoc($sql4);
								$rowcm['type'] = $res4['name'];
							}
							else
							{
								$rowcm['type'] = '';
							}
						}
						else
						{
							$rowcm['type'] = '';
						}
						$artme[] = $rowcm;
					}
				}
			} 
			
			/*$sql_pro_comm = mysql_query("SELECT * FROM community_project WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_pro_comm)>0)
			{
				while($rowca = mysql_fetch_assoc($sql_pro_comm))
				{
					if($rowca['community_id']!=$id['community_id'])
					{
						$rowca['id'] = $rowca['id'].'~com';
						$comm[] = $rowca;
					}
				}
			}*/
			
			 $sql_pro_comm = mysql_query("SELECT * FROM community_event WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0 AND date>='".$date."'");
			if(mysql_num_rows($sql_pro_comm)>0)
			{
				while($rowca = mysql_fetch_assoc($sql_pro_comm))
				{
					if($rowca['community_id']!=$id['community_id'])
					{
						$rowca['table_name'] = 'community_event';
						$sql = mysql_query("SELECT MIN( `id` ) FROM `community_event_profiles` WHERE community_event_id='".$rowca['id']."' AND subtype_id!=0");

						if(mysql_num_rows($sql)>0)
						{
							$res = mysql_fetch_assoc($sql);
							
							$sql2 = mysql_query("select * from community_event_profiles where id='".$res['MIN( `id` )']."'");
							$res2 = mysql_fetch_assoc($sql2);

							$sql4 = mysql_query("select * from  subtype where subtype_id = '".$res2['subtype_id']."'");
							if(mysql_num_rows($sql4)>0)
							{
								$res4 = mysql_fetch_assoc($sql4);
								$rowca['type'] = $res4['name'];
							}
							else
							{
								$rowca['type'] = '';
							}
						}
						else
						{
							$rowca['type'] = '';
						}
						$comme[] = $rowca;
					}
				}
			} 
			
			$sql_get_allp = mysql_query("SELECT * FROM community_project WHERE community_id='".$id['community_id']."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_get_allp)>0)
			{
				while($rowal = mysql_fetch_assoc($sql_get_allp))
				{
					$pal_2[] = $rowal;
				}
			}
			
			/*if(!empty($pal_2) && count($pal_2))
			{
				for($pl2=0;$pl2<count($pal_2);$pl2++)
				{
					$chk_typep_2 = 'community_project|'.$pal_2[$pl2]['id'];
					$sql = mysql_query("SELECT * FROM artist_project WHERE creators_info='".$chk_typep_2."' AND del_status=0 AND active=0");
					if(mysql_num_rows($sql)>0)
					{
						while($row_po = mysql_fetch_assoc($sql))
						{
							if($row_po['artist_id']!=$id['artist_id'])
							{
								$row_po['id'] = $row_po['id'].'~art';
								$art_p_co[] = $row_po;
							}
						}
					}
					
					$sql = mysql_query("SELECT * FROM community_project WHERE creators_info='".$chk_typep_2."' AND del_status=0 AND active=0");
					if(mysql_num_rows($sql)>0)
					{
						while($row_poo = mysql_fetch_assoc($sql))
						{
							if($row_poo['community_id']!=$id['community_id'])
							{
								$row_poo['id'] = $row_poo['id'].'~com';
								$com_p_co[] = $row_poo;
							}
						}
					}
				}
			}*/
			
			 if(!empty($pal_2) && count($pal_2))
			{
				for($pl2=0;$pl2<count($pal_2);$pl2++)
				{
					$chk_typep_2 = 'community_project|'.$pal_2[$pl2]['id'];
					$sql = mysql_query("SELECT * FROM artist_event WHERE creators_info='".$chk_typep_2."' AND del_status=0 AND active=0 AND date>='".$date."'");
					if(mysql_num_rows($sql)>0)
					{
						while($row_po = mysql_fetch_assoc($sql))
						{
							if($row_po['artist_id']!=$id['artist_id'])
							{
								$row_po['table_name'] = 'artist_event';
								$sql = mysql_query("SELECT MIN( `id` ) FROM `artist_event_profiles` WHERE artist_event_id='".$row_po['id']."' AND subtype_id!=0");

								if(mysql_num_rows($sql)>0)
								{
									$res = mysql_fetch_assoc($sql);
									
									$sql2 = mysql_query("select * from artist_event_profiles where id='".$res['MIN( `id` )']."'");
									$res2 = mysql_fetch_assoc($sql2);
                                
									$sql4 = mysql_query("select * from  subtype where subtype_id = '".$res2['subtype_id']."'");
									if(mysql_num_rows($sql4)>0)
									{
										$res4 = mysql_fetch_assoc($sql4);
										$row_po['type'] = $res4['name'];
									}
									else
									{
										$row_po['type'] = '';
									}
								}
								else
								{
									$row_po['type'] = '';
								}
								$art_e_co[] = $row_po;
							}
						}
					}
					
					$sql = mysql_query("SELECT * FROM community_event WHERE creators_info='".$chk_typep_2."' AND del_status=0 AND active=0 AND date>='".$date."'");
					if(mysql_num_rows($sql)>0)
					{
						while($row_poo = mysql_fetch_assoc($sql))
						{
							if($row_poo['community_id']!=$id['community_id'])
							{
								$row_poo['table_name'] = 'community_event';
								$sql = mysql_query("SELECT MIN( `id` ) FROM `community_event_profiles` WHERE community_event_id='".$row_poo['id']."' AND subtype_id!=0");

								if(mysql_num_rows($sql)>0)
								{
									$res = mysql_fetch_assoc($sql);
									
									$sql2 = mysql_query("select * from community_event_profiles where id='".$res['MIN( `id` )']."'");
									$res2 = mysql_fetch_assoc($sql2);

									$sql4 = mysql_query("select * from  subtype where subtype_id = '".$res2['subtype_id']."'");
									if(mysql_num_rows($sql4)>0)
									{
										$res4 = mysql_fetch_assoc($sql4);
										$row_poo['type'] = $res4['name'];
									}
									else
									{
										$row_poo['type'] = '';
									}
								}
								else
								{
									$row_poo['type'] = '';
								}
								$com_e_co[] = $row_poo;
							}
						}
					}
				}
			} 
		}
		
		/* var_dump($artm);
		var_dump($arta);
		var_dump($comm);
		var_dump($coma);
		var_dump($art_p_ao);
		var_dump($com_p_ao);
		var_dump($art_p_co);
		var_dump($com_p_co); */
		
		$totals = array();
		$totals = array_merge($artm,$arta,$comm,$coma,$art_p_ao,$com_p_ao,$art_p_co,$com_p_co,$artme,$artae,$comme,$comae,$art_e_ao,$com_e_ao,$art_e_co,$com_e_co);
		return $totals;
	}
	
	function only_creator_recevent()
	{
		$id = $this->selgeneral();
		$date = date("Y-m-d");
		
		$var_type_1 = 'general_artist';
		$var_type_2 = 'general_community';
		$var_type_3 = 'artist_project';
		$var_type_4 = 'community_project';
		
		$artm = array();
		$arta = array();
		$comm = array();
		$coma = array();
		
		$artme = array();
		$artae = array();
		$comme = array();
		$comae = array();
		
		$art_p_ao = array();
		$com_p_ao = array();
		$art_p_co = array();
		$com_p_co = array();
		
		$art_e_ao = array();
		$com_e_ao = array();
		$art_e_co = array();
		$com_e_co = array();
		
		$pal_1 = array();
		$pal_2 = array();
		
		if($id['artist_id']!=0 && !empty($id['artist_id']))
		{
			$chk_type = $var_type_1.'|'.$id['artist_id'];
			
			$sql_get_allp = mysql_query("SELECT * FROM artist_project WHERE artist_id='".$id['artist_id']."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_get_allp)>0)
			{
				while($rowal = mysql_fetch_assoc($sql_get_allp))
				{
					$pal_1[] = $rowal;
				}
			}
			
			
			
			 if(!empty($pal_1) && count($pal_1))
			{
				for($pl=0;$pl<count($pal_1);$pl++)
				{
					$chk_typep_1 = 'artist_project|'.$pal_1[$pl]['id'];
					
					$sql = mysql_query("SELECT * FROM artist_event WHERE creators_info='".$chk_typep_1."' AND del_status=0 AND active=0 AND date<'".$date."'");
					if(mysql_num_rows($sql)>0)
					{
						while($row_po = mysql_fetch_assoc($sql))
						{
							if($row_po['artist_id']!=$id['artist_id'])
							{
								$row_po['id'] = $row_po['id'].'~art';
								$art_e_ao[] = $row_po;
							}
						}
					}
					
					$sql = mysql_query("SELECT * FROM community_event WHERE creators_info='".$chk_typep_1."' AND del_status=0 AND active=0 AND date<'".$date."'");
					if(mysql_num_rows($sql)>0)
					{
						while($row_poo = mysql_fetch_assoc($sql))
						{
							if($row_poo['community_id']!=$id['community_id'])
							{
								$row_poo['id'] = $row_poo['id'].'~com';
								$com_e_ao[] = $row_poo;
							}
						}
					}
				}
			} 

			/*$sql_pro_arta = mysql_query("SELECT * FROM artist_project WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_pro_arta)>0)
			{
				while($rowaa = mysql_fetch_assoc($sql_pro_arta))
				{
					if($rowaa['artist_id']!=$id['artist_id'])
					{
						$rowaa['id'] = $rowaa['id'].'~art';
						$arta[] = $rowaa;
					}
				}
			}*/
			
			$sql_pro_arta = mysql_query("SELECT * FROM artist_event WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0 AND date<'".$date."'");
			if(mysql_num_rows($sql_pro_arta)>0)
			{
				while($rowaa = mysql_fetch_assoc($sql_pro_arta))
				{
					if($rowaa['artist_id']!=$id['artist_id'])
					{
						$rowaa['id'] = $rowaa['id'].'~art';
						$artae[] = $rowaa;
					}
				}
			} 
			
			/*$sql_pro_coma = mysql_query("SELECT * FROM community_project WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_pro_coma)>0)
			{
				while($rowam = mysql_fetch_assoc($sql_pro_coma))
				{
					if($rowam['community_id']!=$id['community_id'])
					{
						$rowam['id'] = $rowam['id'].'~com';
						$coma[] = $rowam;
					}
				}
			}*/
			
			$sql_pro_coma = mysql_query("SELECT * FROM community_event WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0 AND date<'".$date."'");
			if(mysql_num_rows($sql_pro_coma)>0)
			{
				while($rowam = mysql_fetch_assoc($sql_pro_coma))
				{
					if($rowam['community_id']!=$id['community_id'])
					{
						$rowam['id'] = $rowam['id'].'~com';
						$comae[] = $rowam;
					}
				}
			} 
		}
		
		if($id['community_id']!=0 && !empty($id['community_id']))
		{
			/*$chk_type = $var_type_2.'|'.$id['community_id'];
			$sql_pro_artm = mysql_query("SELECT * FROM artist_project WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_pro_artm)>0)
			{
				while($rowcm = mysql_fetch_assoc($sql_pro_artm))
				{
					if($rowcm['artist_id']!=$id['artist_id'])
					{
						$rowcm['id'] = $rowcm['id'].'~art';
						$artm[] = $rowcm;
					}
				}
			}*/
			
			 $chk_type = $var_type_2.'|'.$id['community_id'];
			$sql_pro_artm = mysql_query("SELECT * FROM artist_event WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0 AND date<'".$date."'");
			if(mysql_num_rows($sql_pro_artm)>0)
			{
				while($rowcm = mysql_fetch_assoc($sql_pro_artm))
				{
					if($rowcm['artist_id']!=$id['artist_id'])
					{
						$rowcm['id'] = $rowcm['id'].'~art';
						$artme[] = $rowcm;
					}
				}
			} 
			
			/*$sql_pro_comm = mysql_query("SELECT * FROM community_project WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_pro_comm)>0)
			{
				while($rowca = mysql_fetch_assoc($sql_pro_comm))
				{
					if($rowca['community_id']!=$id['community_id'])
					{
						$rowca['id'] = $rowca['id'].'~com';
						$comm[] = $rowca;
					}
				}
			}*/
			
			 $sql_pro_comm = mysql_query("SELECT * FROM community_event WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0 AND date<'".$date."'");
			if(mysql_num_rows($sql_pro_comm)>0)
			{
				while($rowca = mysql_fetch_assoc($sql_pro_comm))
				{
					if($rowca['community_id']!=$id['community_id'])
					{
						$rowca['id'] = $rowca['id'].'~com';
						$comme[] = $rowca;
					}
				}
			} 
			
			$sql_get_allp = mysql_query("SELECT * FROM community_project WHERE community_id='".$id['community_id']."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_get_allp)>0)
			{
				while($rowal = mysql_fetch_assoc($sql_get_allp))
				{
					$pal_2[] = $rowal;
				}
			}
			
			/*if(!empty($pal_2) && count($pal_2))
			{
				for($pl2=0;$pl2<count($pal_2);$pl2++)
				{
					$chk_typep_2 = 'community_project|'.$pal_2[$pl2]['id'];
					$sql = mysql_query("SELECT * FROM artist_project WHERE creators_info='".$chk_typep_2."' AND del_status=0 AND active=0");
					if(mysql_num_rows($sql)>0)
					{
						while($row_po = mysql_fetch_assoc($sql))
						{
							if($row_po['artist_id']!=$id['artist_id'])
							{
								$row_po['id'] = $row_po['id'].'~art';
								$art_p_co[] = $row_po;
							}
						}
					}
					
					$sql = mysql_query("SELECT * FROM community_project WHERE creators_info='".$chk_typep_2."' AND del_status=0 AND active=0");
					if(mysql_num_rows($sql)>0)
					{
						while($row_poo = mysql_fetch_assoc($sql))
						{
							if($row_poo['community_id']!=$id['community_id'])
							{
								$row_poo['id'] = $row_poo['id'].'~com';
								$com_p_co[] = $row_poo;
							}
						}
					}
				}
			}*/
			
			 if(!empty($pal_2) && count($pal_2))
			{
				for($pl2=0;$pl2<count($pal_2);$pl2++)
				{
					$chk_typep_2 = 'community_project|'.$pal_2[$pl2]['id'];
					$sql = mysql_query("SELECT * FROM artist_event WHERE creators_info='".$chk_typep_2."' AND del_status=0 AND active=0 AND date<'".$date."'");
					if(mysql_num_rows($sql)>0)
					{
						while($row_po = mysql_fetch_assoc($sql))
						{
							if($row_po['artist_id']!=$id['artist_id'])
							{
								$row_po['id'] = $row_po['id'].'~art';
								$art_e_co[] = $row_po;
							}
						}
					}
					
					$sql = mysql_query("SELECT * FROM community_event WHERE creators_info='".$chk_typep_2."' AND del_status=0 AND active=0 AND date<'".$date."'");
					if(mysql_num_rows($sql)>0)
					{
						while($row_poo = mysql_fetch_assoc($sql))
						{
							if($row_poo['community_id']!=$id['community_id'])
							{
								$row_poo['id'] = $row_poo['id'].'~com';
								$com_e_co[] = $row_poo;
							}
						}
					}
				}
			} 
		}
		
		/* var_dump($artm);
		var_dump($arta);
		var_dump($comm);
		var_dump($coma);
		var_dump($art_p_ao);
		var_dump($com_p_ao);
		var_dump($art_p_co);
		var_dump($com_p_co); */
		
		$totals = array();
		$totals = array_merge($artm,$arta,$comm,$coma,$art_p_ao,$com_p_ao,$art_p_co,$com_p_co,$artme,$artae,$comme,$comae,$art_e_ao,$com_e_ao,$art_e_co,$com_e_co);
		return $totals;
	}
	
	function only_sh_creator_recevent()
	{
		$id = $this->selgeneral();
		$date = date("Y-m-d");
		
		$var_type_1 = 'general_artist';
		$var_type_2 = 'general_community';
		$var_type_3 = 'artist_project';
		$var_type_4 = 'community_project';
		
		$artm = array();
		$arta = array();
		$comm = array();
		$coma = array();
		
		$artme = array();
		$artae = array();
		$comme = array();
		$comae = array();
		
		$art_p_ao = array();
		$com_p_ao = array();
		$art_p_co = array();
		$com_p_co = array();
		
		$art_e_ao = array();
		$com_e_ao = array();
		$art_e_co = array();
		$com_e_co = array();
		
		$pal_1 = array();
		$pal_2 = array();
		
		if($id['artist_id']!=0 && !empty($id['artist_id']))
		{
			$chk_type = $var_type_1.'|'.$id['artist_id'];
			
			$sql_get_allp = mysql_query("SELECT * FROM artist_project WHERE artist_id='".$id['artist_id']."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_get_allp)>0)
			{
				while($rowal = mysql_fetch_assoc($sql_get_allp))
				{
					$pal_1[] = $rowal;
				}
			}
			
			
			
			 if(!empty($pal_1) && count($pal_1))
			{
				for($pl=0;$pl<count($pal_1);$pl++)
				{
					$chk_typep_1 = 'artist_project|'.$pal_1[$pl]['id'];
					
					$sql = mysql_query("SELECT * FROM artist_event WHERE creators_info='".$chk_typep_1."' AND del_status=0 AND active=0 AND date<'".$date."'");
					if(mysql_num_rows($sql)>0)
					{
						while($row_po = mysql_fetch_assoc($sql))
						{
							if($row_po['artist_id']!=$id['artist_id'])
							{
								$row_po['table_name'] = 'artist_event';
								$sql = mysql_query("SELECT MIN( `id` ) FROM `artist_event_profiles` WHERE artist_event_id='".$row_po['id']."' AND subtype_id!=0");

								if(mysql_num_rows($sql)>0)
								{
									$res = mysql_fetch_assoc($sql);
									
									$sql2 = mysql_query("select * from artist_event_profiles where id='".$res['MIN( `id` )']."'");
									$res2 = mysql_fetch_assoc($sql2);

									$sql4 = mysql_query("select * from  subtype where subtype_id = '".$res2['subtype_id']."'");
									if(mysql_num_rows($sql4)>0)
									{
										$res4 = mysql_fetch_assoc($sql4);
										$row_po['type'] = $res4['name'];
									}
									else
									{
										$row_po['type'] = '';
									}
								}
								else
								{
									$row_po['type'] = '';
								}
								$art_e_ao[] = $row_po;
							}
						}
					}
					
					$sql = mysql_query("SELECT * FROM community_event WHERE creators_info='".$chk_typep_1."' AND del_status=0 AND active=0 AND date<'".$date."'");
					if(mysql_num_rows($sql)>0)
					{
						while($row_poo = mysql_fetch_assoc($sql))
						{
							if($row_poo['community_id']!=$id['community_id'])
							{
								$row_poo['table_name'] = 'community_event';
								$sql = mysql_query("SELECT MIN( `id` ) FROM `community_event_profiles` WHERE community_event_id='".$row_poo['id']."' AND subtype_id!=0");

								if(mysql_num_rows($sql)>0)
								{
									$res = mysql_fetch_assoc($sql);
									
									$sql2 = mysql_query("select * from community_event_profiles where id='".$res['MIN( `id` )']."'");
									$res2 = mysql_fetch_assoc($sql2);

									$sql4 = mysql_query("select * from  subtype where subtype_id = '".$res2['subtype_id']."'");
									if(mysql_num_rows($sql4)>0)
									{
										$res4 = mysql_fetch_assoc($sql4);
										$row_poo['type'] = $res4['name'];
									}
									else
									{
										$row_poo['type'] = '';
									}
								}
								else
								{
									$row_poo['type'] = '';
								}
								$com_e_ao[] = $row_poo;
							}
						}
					}
				}
			} 

			/*$sql_pro_arta = mysql_query("SELECT * FROM artist_project WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_pro_arta)>0)
			{
				while($rowaa = mysql_fetch_assoc($sql_pro_arta))
				{
					if($rowaa['artist_id']!=$id['artist_id'])
					{
						$rowaa['id'] = $rowaa['id'].'~art';
						$arta[] = $rowaa;
					}
				}
			}*/
			
			$sql_pro_arta = mysql_query("SELECT * FROM artist_event WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0 AND date<'".$date."'");
			if(mysql_num_rows($sql_pro_arta)>0)
			{
				while($rowaa = mysql_fetch_assoc($sql_pro_arta))
				{
					if($rowaa['artist_id']!=$id['artist_id'])
					{
						$rowaa['table_name'] = 'artist_event';
						$sql = mysql_query("SELECT MIN( `id` ) FROM `artist_event_profiles` WHERE artist_event_id='".$rowaa['id']."' AND subtype_id!=0");

						if(mysql_num_rows($sql)>0)
						{
							$res = mysql_fetch_assoc($sql);
							
							$sql2 = mysql_query("select * from artist_event_profiles where id='".$res['MIN( `id` )']."'");
							$res2 = mysql_fetch_assoc($sql2);

							$sql4 = mysql_query("select * from  subtype where subtype_id = '".$res2['subtype_id']."'");
							if(mysql_num_rows($sql4)>0)
							{
								$res4 = mysql_fetch_assoc($sql4);
								$rowaa['type'] = $res4['name'];
							}
							else
							{
								$rowaa['type'] = '';
							}
						}
						else
						{
							$rowaa['type'] = '';
						}
						$artae[] = $rowaa;
					}
				}
			} 
			
			/*$sql_pro_coma = mysql_query("SELECT * FROM community_project WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_pro_coma)>0)
			{
				while($rowam = mysql_fetch_assoc($sql_pro_coma))
				{
					if($rowam['community_id']!=$id['community_id'])
					{
						$rowam['id'] = $rowam['id'].'~com';
						$coma[] = $rowam;
					}
				}
			}*/
			
			$sql_pro_coma = mysql_query("SELECT * FROM community_event WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0 AND date<'".$date."'");
			if(mysql_num_rows($sql_pro_coma)>0)
			{
				while($rowam = mysql_fetch_assoc($sql_pro_coma))
				{
					if($rowam['community_id']!=$id['community_id'])
					{
						$rowam['table_name'] = 'community_event';
						$sql = mysql_query("SELECT MIN( `id` ) FROM `community_event_profiles` WHERE community_event_id='".$rowam['id']."' AND subtype_id!=0");

						if(mysql_num_rows($sql)>0)
						{
							$res = mysql_fetch_assoc($sql);
							
							$sql2 = mysql_query("select * from community_event_profiles where id='".$res['MIN( `id` )']."'");
							$res2 = mysql_fetch_assoc($sql2);

							$sql4 = mysql_query("select * from  subtype where subtype_id = '".$res2['subtype_id']."'");
							if(mysql_num_rows($sql4)>0)
							{
								$res4 = mysql_fetch_assoc($sql4);
								$rowam['type'] = $res4['name'];
							}
							else
							{
								$rowam['type'] = '';
							}
						}
						else
						{
							$rowam['type'] = '';
						}
						$comae[] = $rowam;
					}
				}
			} 
		}
		
		if($id['community_id']!=0 && !empty($id['community_id']))
		{
			/*$chk_type = $var_type_2.'|'.$id['community_id'];
			$sql_pro_artm = mysql_query("SELECT * FROM artist_project WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_pro_artm)>0)
			{
				while($rowcm = mysql_fetch_assoc($sql_pro_artm))
				{
					if($rowcm['artist_id']!=$id['artist_id'])
					{
						$rowcm['id'] = $rowcm['id'].'~art';
						$artm[] = $rowcm;
					}
				}
			}*/
			
			 $chk_type = $var_type_2.'|'.$id['community_id'];
			$sql_pro_artm = mysql_query("SELECT * FROM artist_event WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0 AND date<'".$date."'");
			if(mysql_num_rows($sql_pro_artm)>0)
			{
				while($rowcm = mysql_fetch_assoc($sql_pro_artm))
				{
					if($rowcm['artist_id']!=$id['artist_id'])
					{
						$rowcm['table_name'] = 'artist_event';
						$sql = mysql_query("SELECT MIN( `id` ) FROM `artist_event_profiles` WHERE artist_event_id='".$rowcm['id']."' AND subtype_id!=0");

						if(mysql_num_rows($sql)>0)
						{
							$res = mysql_fetch_assoc($sql);
								
							$sql2 = mysql_query("select * from artist_event_profiles where id='".$res['MIN( `id` )']."'");
							$res2 = mysql_fetch_assoc($sql2);

							$sql4 = mysql_query("select * from  subtype where subtype_id = '".$res2['subtype_id']."'");
							if(mysql_num_rows($sql4)>0)
							{
								$res4 = mysql_fetch_assoc($sql4);
								$rowcm['type'] = $res4['name'];
							}
							else
							{
								$rowcm['type'] = '';
							}
						}
						else
						{
							$rowcm['type'] = '';
						}
						$artme[] = $rowcm;
					}
				}
			} 
			
			/*$sql_pro_comm = mysql_query("SELECT * FROM community_project WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_pro_comm)>0)
			{
				while($rowca = mysql_fetch_assoc($sql_pro_comm))
				{
					if($rowca['community_id']!=$id['community_id'])
					{
						$rowca['id'] = $rowca['id'].'~com';
						$comm[] = $rowca;
					}
				}
			}*/
			
			 $sql_pro_comm = mysql_query("SELECT * FROM community_event WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0 AND date<'".$date."'");
			if(mysql_num_rows($sql_pro_comm)>0)
			{
				while($rowca = mysql_fetch_assoc($sql_pro_comm))
				{
					if($rowca['community_id']!=$id['community_id'])
					{
						$rowca['table_name'] = 'community_event';
						$sql = mysql_query("SELECT MIN( `id` ) FROM `community_event_profiles` WHERE community_event_id='".$rowca['id']."' AND subtype_id!=0");

						if(mysql_num_rows($sql)>0)
						{
							$res = mysql_fetch_assoc($sql);
							
							$sql2 = mysql_query("select * from community_event_profiles where id='".$res['MIN( `id` )']."'");
							$res2 = mysql_fetch_assoc($sql2);

							$sql4 = mysql_query("select * from  subtype where subtype_id = '".$res2['subtype_id']."'");
							if(mysql_num_rows($sql4)>0)
							{
								$res4 = mysql_fetch_assoc($sql4);
								$rowca['type'] = $res4['name'];
							}
							else
							{
								$rowca['type'] = '';
							}
						}
						else
						{
							$rowca['type'] = '';
						}
						$comme[] = $rowca;
					}
				}
			} 
			
			$sql_get_allp = mysql_query("SELECT * FROM community_project WHERE community_id='".$id['community_id']."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_get_allp)>0)
			{
				while($rowal = mysql_fetch_assoc($sql_get_allp))
				{
					$pal_2[] = $rowal;
				}
			}
			
			/*if(!empty($pal_2) && count($pal_2))
			{
				for($pl2=0;$pl2<count($pal_2);$pl2++)
				{
					$chk_typep_2 = 'community_project|'.$pal_2[$pl2]['id'];
					$sql = mysql_query("SELECT * FROM artist_project WHERE creators_info='".$chk_typep_2."' AND del_status=0 AND active=0");
					if(mysql_num_rows($sql)>0)
					{
						while($row_po = mysql_fetch_assoc($sql))
						{
							if($row_po['artist_id']!=$id['artist_id'])
							{
								$row_po['id'] = $row_po['id'].'~art';
								$art_p_co[] = $row_po;
							}
						}
					}
					
					$sql = mysql_query("SELECT * FROM community_project WHERE creators_info='".$chk_typep_2."' AND del_status=0 AND active=0");
					if(mysql_num_rows($sql)>0)
					{
						while($row_poo = mysql_fetch_assoc($sql))
						{
							if($row_poo['community_id']!=$id['community_id'])
							{
								$row_poo['id'] = $row_poo['id'].'~com';
								$com_p_co[] = $row_poo;
							}
						}
					}
				}
			}*/
			
			 if(!empty($pal_2) && count($pal_2))
			{
				for($pl2=0;$pl2<count($pal_2);$pl2++)
				{
					$chk_typep_2 = 'community_project|'.$pal_2[$pl2]['id'];
					$sql = mysql_query("SELECT * FROM artist_event WHERE creators_info='".$chk_typep_2."' AND del_status=0 AND active=0 AND date<'".$date."'");
					if(mysql_num_rows($sql)>0)
					{
						while($row_po = mysql_fetch_assoc($sql))
						{
							if($row_po['artist_id']!=$id['artist_id'])
							{
								$row_po['table_name'] = 'artist_event';
								$sql = mysql_query("SELECT MIN( `id` ) FROM `artist_event_profiles` WHERE artist_event_id='".$row_po['id']."' AND subtype_id!=0");

								if(mysql_num_rows($sql)>0)
								{
									$res = mysql_fetch_assoc($sql);
									
									$sql2 = mysql_query("select * from artist_event_profiles where id='".$res['MIN( `id` )']."'");
									$res2 = mysql_fetch_assoc($sql2);

									$sql4 = mysql_query("select * from  subtype where subtype_id = '".$res2['subtype_id']."'");
									if(mysql_num_rows($sql4)>0)
									{
										$res4 = mysql_fetch_assoc($sql4);
										$row_po['type'] = $res4['name'];
									}
									else
									{
										$row_po['type'] = '';
									}
								}
								else
								{
									$row_po['type'] = '';
								}
								$art_e_co[] = $row_po;
							}
						}
					}
					
					$sql = mysql_query("SELECT * FROM community_event WHERE creators_info='".$chk_typep_2."' AND del_status=0 AND active=0 AND date<'".$date."'");
					if(mysql_num_rows($sql)>0)
					{
						while($row_poo = mysql_fetch_assoc($sql))
						{
							if($row_poo['community_id']!=$id['community_id'])
							{
								$row_poo['table_name'] = 'community_event';
								$sql = mysql_query("SELECT MIN( `id` ) FROM `community_event_profiles` WHERE community_event_id='".$row_poo['id']."' AND subtype_id!=0");

								if(mysql_num_rows($sql)>0)
								{
									$res = mysql_fetch_assoc($sql);
									
									$sql2 = mysql_query("select * from community_event_profiles where id='".$res['MIN( `id` )']."'");
									$res2 = mysql_fetch_assoc($sql2);

									$sql4 = mysql_query("select * from  subtype where subtype_id = '".$res2['subtype_id']."'");
									if(mysql_num_rows($sql4)>0)
									{
										$res4 = mysql_fetch_assoc($sql4);
										$row_poo['type'] = $res4['name'];
									}
									else
									{
										$row_poo['type'] = '';
									}
								}
								else
								{
									$row_poo['type'] = '';
								}
								$com_e_co[] = $row_poo;
							}
						}
					}
				}
			} 
		}
		
		/* var_dump($artm);
		var_dump($arta);
		var_dump($comm);
		var_dump($coma);
		var_dump($art_p_ao);
		var_dump($com_p_ao);
		var_dump($art_p_co);
		var_dump($com_p_co); */
		
		$totals = array();
		$totals = array_merge($artm,$arta,$comm,$coma,$art_p_ao,$com_p_ao,$art_p_co,$com_p_co,$artme,$artae,$comme,$comae,$art_e_ao,$com_e_ao,$art_e_co,$com_e_co);
		return $totals;
	}
	
	function only_creator_oupevent()
	{
		$id = $this->selgeneral();
		$date = date("Y-m-d");
		
		$var_type_1 = 'general_artist';
		$var_type_2 = 'general_community';
		$var_type_3 = 'artist_project';
		$var_type_4 = 'community_project';
		
		$artm = array();
		$arta = array();
		$comm = array();
		$coma = array();
		
		$artme = array();
		$artae = array();
		$comme = array();
		$comae = array();
		
		$art_p_ao = array();
		$com_p_ao = array();
		$art_p_co = array();
		$com_p_co = array();
		
		$art_e_ao = array();
		$com_e_ao = array();
		$art_e_co = array();
		$com_e_co = array();
		
		$pal_1 = array();
		$pal_2 = array();
		
		if($id['artist_id']!=0 && !empty($id['artist_id']))
		{
			$chk_type = $var_type_1.'|'.$id['artist_id'];
			
			$sql_get_allp = mysql_query("SELECT * FROM artist_project WHERE artist_id='".$id['artist_id']."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_get_allp)>0)
			{
				while($rowal = mysql_fetch_assoc($sql_get_allp))
				{
					$rowal['whos_event'] = "only_creator";
					$pal_1[] = $rowal;
				}
			}
			
			
			
			 if(!empty($pal_1) && count($pal_1))
			{
				for($pl=0;$pl<count($pal_1);$pl++)
				{
					$chk_typep_1 = 'artist_project|'.$pal_1[$pl]['id'];
					
					$sql = mysql_query("SELECT * FROM artist_event WHERE creators_info='".$chk_typep_1."' AND del_status=0 AND active=0 AND date>='".$date."'");
					if(mysql_num_rows($sql)>0)
					{
						while($row_po = mysql_fetch_assoc($sql))
						{
							if($row_po['artist_id']!=$id['artist_id'])
							{
								$row_po['edit'] = 'yes';
								$row_po['whos_event'] = "only_creator";
								$row_po['id'] = $row_po['id'] ."~art";
								$art_e_ao[] = $row_po;
							}
						}
					}
					
					$sql = mysql_query("SELECT * FROM community_event WHERE creators_info='".$chk_typep_1."' AND del_status=0 AND active=0 AND date>='".$date."'");
					if(mysql_num_rows($sql)>0)
					{
						while($row_poo = mysql_fetch_assoc($sql))
						{
							if($row_poo['community_id']!=$id['community_id'])
							{
								$row_poo['edit'] = 'yes';
								$row_poo['edit_com'] = 'yes';
								$row_poo['whos_event'] = "only_creator";
								$row_poo['id'] = $row_poo['id'] ."~com";
								$com_e_ao[] = $row_poo;
							}
						}
					}
				}
			} 

			/*$sql_pro_arta = mysql_query("SELECT * FROM artist_project WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_pro_arta)>0)
			{
				while($rowaa = mysql_fetch_assoc($sql_pro_arta))
				{
					if($rowaa['artist_id']!=$id['artist_id'])
					{
						$rowaa['id'] = $rowaa['id'].'~art';
						$arta[] = $rowaa;
					}
				}
			}*/
			
			$sql_pro_arta = mysql_query("SELECT * FROM artist_event WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0 AND date>='".$date."'");
			if(mysql_num_rows($sql_pro_arta)>0)
			{
				while($rowaa = mysql_fetch_assoc($sql_pro_arta))
				{
					if($rowaa['artist_id']!=$id['artist_id'])
					{
						$rowaa['edit'] = 'yes';
						$rowaa['whos_event'] = "only_creator";
						$rowaa['id'] = $rowaa['id'] ."~art";
						$artae[] = $rowaa;
					}
				}
			} 
			
			/*$sql_pro_coma = mysql_query("SELECT * FROM community_project WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_pro_coma)>0)
			{
				while($rowam = mysql_fetch_assoc($sql_pro_coma))
				{
					if($rowam['community_id']!=$id['community_id'])
					{
						$rowam['id'] = $rowam['id'].'~com';
						$coma[] = $rowam;
					}
				}
			}*/
			
			$sql_pro_coma = mysql_query("SELECT * FROM community_event WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0 AND date>='".$date."'");
			if(mysql_num_rows($sql_pro_coma)>0)
			{
				while($rowam = mysql_fetch_assoc($sql_pro_coma))
				{
					if($rowam['community_id']!=$id['community_id'])
					{
						$rowam['edit'] = 'yes';
						$rowam['edit_com'] = 'yes';
						$rowam['whos_event'] = "only_creator";
						$rowam['id'] = $rowam['id'] ."~com";
						$comae[] = $rowam;
					}
				}
			} 
		}
		
		if($id['community_id']!=0 && !empty($id['community_id']))
		{
			/*$chk_type = $var_type_2.'|'.$id['community_id'];
			$sql_pro_artm = mysql_query("SELECT * FROM artist_project WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_pro_artm)>0)
			{
				while($rowcm = mysql_fetch_assoc($sql_pro_artm))
				{
					if($rowcm['artist_id']!=$id['artist_id'])
					{
						$rowcm['id'] = $rowcm['id'].'~art';
						$artm[] = $rowcm;
					}
				}
			}*/
			
			 $chk_type = $var_type_2.'|'.$id['community_id'];
			$sql_pro_artm = mysql_query("SELECT * FROM artist_event WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0 AND date>='".$date."'");
			if(mysql_num_rows($sql_pro_artm)>0)
			{
				while($rowcm = mysql_fetch_assoc($sql_pro_artm))
				{
					if($rowcm['artist_id']!=$id['artist_id'])
					{
						$rowcm['edit'] = 'yes';
						$rowcm['whos_event'] = "only_creator";
						$rowcm['id'] = $rowcm['id'] ."~art";
						$artme[] = $rowcm;
					}
				}
			} 
			
			/*$sql_pro_comm = mysql_query("SELECT * FROM community_project WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_pro_comm)>0)
			{
				while($rowca = mysql_fetch_assoc($sql_pro_comm))
				{
					if($rowca['community_id']!=$id['community_id'])
					{
						$rowca['id'] = $rowca['id'].'~com';
						$comm[] = $rowca;
					}
				}
			}*/
			
			 $sql_pro_comm = mysql_query("SELECT * FROM community_event WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0 AND date>='".$date."'");
			if(mysql_num_rows($sql_pro_comm)>0)
			{
				while($rowca = mysql_fetch_assoc($sql_pro_comm))
				{
					if($rowca['community_id']!=$id['community_id'])
					{
						$rowca['edit'] = 'yes';
						$rowca['edit_com'] = 'yes';
						$rowca['whos_event'] = "only_creator";
						$rowca['id'] = $rowca['id'] ."~com";
						$comme[] = $rowca;
					}
				}
			} 
			
			$sql_get_allp = mysql_query("SELECT * FROM community_project WHERE community_id='".$id['community_id']."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_get_allp)>0)
			{
				while($rowal = mysql_fetch_assoc($sql_get_allp))
				{
					$rowal['whos_event'] = "only_creator";
					$pal_2[] = $rowal;
				}
			}
			
			/*if(!empty($pal_2) && count($pal_2))
			{
				for($pl2=0;$pl2<count($pal_2);$pl2++)
				{
					$chk_typep_2 = 'community_project|'.$pal_2[$pl2]['id'];
					$sql = mysql_query("SELECT * FROM artist_project WHERE creators_info='".$chk_typep_2."' AND del_status=0 AND active=0");
					if(mysql_num_rows($sql)>0)
					{
						while($row_po = mysql_fetch_assoc($sql))
						{
							if($row_po['artist_id']!=$id['artist_id'])
							{
								$row_po['id'] = $row_po['id'].'~art';
								$art_p_co[] = $row_po;
							}
						}
					}
					
					$sql = mysql_query("SELECT * FROM community_project WHERE creators_info='".$chk_typep_2."' AND del_status=0 AND active=0");
					if(mysql_num_rows($sql)>0)
					{
						while($row_poo = mysql_fetch_assoc($sql))
						{
							if($row_poo['community_id']!=$id['community_id'])
							{
								$row_poo['id'] = $row_poo['id'].'~com';
								$com_p_co[] = $row_poo;
							}
						}
					}
				}
			}*/
			
			 if(!empty($pal_2) && count($pal_2))
			{
				for($pl2=0;$pl2<count($pal_2);$pl2++)
				{
					$chk_typep_2 = 'community_project|'.$pal_2[$pl2]['id'];
					$sql = mysql_query("SELECT * FROM artist_event WHERE creators_info='".$chk_typep_2."' AND del_status=0 AND active=0 AND date>='".$date."'");
					if(mysql_num_rows($sql)>0)
					{
						while($row_po = mysql_fetch_assoc($sql))
						{
							if($row_po['artist_id']!=$id['artist_id'])
							{
								$row_po['edit'] = 'yes';
								$row_po['whos_event'] = "only_creator";
								$row_po['id'] = $row_po['id'] ."~art";
								$art_e_co[] = $row_po;
							}
						}
					}
					
					$sql = mysql_query("SELECT * FROM community_event WHERE creators_info='".$chk_typep_2."' AND del_status=0 AND active=0 AND date>='".$date."'");
					if(mysql_num_rows($sql)>0)
					{
						while($row_poo = mysql_fetch_assoc($sql))
						{
							if($row_poo['community_id']!=$id['community_id'])
							{
								$row_poo['edit'] = 'yes';
								$row_poo['edit_com'] = 'yes';
								$row_poo['whos_event'] = "only_creator";
								$row_poo['id'] = $row_poo['id'] ."~com";
								$com_e_co[] = $row_poo;
							}
						}
					}
				}
			} 
		}
		
		/* var_dump($artm);
		var_dump($arta);
		var_dump($comm);
		var_dump($coma);
		var_dump($art_p_ao);
		var_dump($com_p_ao);
		var_dump($art_p_co);
		var_dump($com_p_co); */
		
		$totals = array();
		$totals = array_merge($artm,$arta,$comm,$coma,$art_p_ao,$com_p_ao,$art_p_co,$com_p_co,$artme,$artae,$comme,$comae,$art_e_ao,$com_e_ao,$art_e_co,$com_e_co);
		return $totals;
	}
	
	function only_creator_calup($d,$m,$y)
	{
		$id = $this->selgeneral();
		$date = date("Y-m-d");
		
		$var_type_1 = 'general_artist';
		$var_type_2 = 'general_community';
		$var_type_3 = 'artist_project';
		$var_type_4 = 'community_project';
		
		$artm = array();
		$arta = array();
		$comm = array();
		$coma = array();
		
		$artme = array();
		$artae = array();
		$comme = array();
		$comae = array();
		
		$art_p_ao = array();
		$com_p_ao = array();
		$art_p_co = array();
		$com_p_co = array();
		
		$art_e_ao = array();
		$com_e_ao = array();
		$art_e_co = array();
		$com_e_co = array();
		
		$pal_1 = array();
		$pal_2 = array();
		
		if($id['artist_id']!=0 && !empty($id['artist_id']))
		{
			$chk_type = $var_type_1.'|'.$id['artist_id'];
			
			$sql_get_allp = mysql_query("SELECT * FROM artist_project WHERE artist_id='".$id['artist_id']."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_get_allp)>0)
			{
				while($rowal = mysql_fetch_assoc($sql_get_allp))
				{
					$rowal['whos_event'] = "only_creator";
					$pal_1[] = $rowal;
				}
			}
			
			
			
			 if(!empty($pal_1) && count($pal_1))
			{
				for($pl=0;$pl<count($pal_1);$pl++)
				{
					$chk_typep_1 = 'artist_project|'.$pal_1[$pl]['id'];
					
					$sql = mysql_query("SELECT * FROM artist_event WHERE creators_info='".$chk_typep_1."' AND del_status=0 AND active=0 AND month(date) = ".$m."
	        AND dayofmonth(date) = ".$d." AND
	        year(date)= ".$y."");
					if(mysql_num_rows($sql)>0)
					{
						while($row_po = mysql_fetch_assoc($sql))
						{
							if($row_po['artist_id']!=$id['artist_id'])
							{
								$row_po['edit'] = 'yes';
								$row_po['whos_event'] = "only_creator";
								$art_e_ao[] = $row_po;
							}
						}
					}
					
					$sql = mysql_query("SELECT * FROM community_event WHERE creators_info='".$chk_typep_1."' AND del_status=0 AND active=0 AND month(date) = ".$m."
	        AND dayofmonth(date) = ".$d." AND
	        year(date)= ".$y."");
					if(mysql_num_rows($sql)>0)
					{
						while($row_poo = mysql_fetch_assoc($sql))
						{
							if($row_poo['community_id']!=$id['community_id'])
							{
								$row_poo['edit'] = 'yes';
								$row_poo['edit_com'] = 'yes';
								$row_poo['whos_event'] = "only_creator";
								$com_e_ao[] = $row_poo;
							}
						}
					}
				}
			} 

			/*$sql_pro_arta = mysql_query("SELECT * FROM artist_project WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_pro_arta)>0)
			{
				while($rowaa = mysql_fetch_assoc($sql_pro_arta))
				{
					if($rowaa['artist_id']!=$id['artist_id'])
					{
						$rowaa['id'] = $rowaa['id'].'~art';
						$arta[] = $rowaa;
					}
				}
			}*/
			
			$sql_pro_arta = mysql_query("SELECT * FROM artist_event WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0 AND month(date) = ".$m."
	        AND dayofmonth(date) = ".$d." AND
	        year(date)= ".$y."");
			if(mysql_num_rows($sql_pro_arta)>0)
			{
				while($rowaa = mysql_fetch_assoc($sql_pro_arta))
				{
					if($rowaa['artist_id']!=$id['artist_id'])
					{
						$rowaa['edit'] = 'yes';
						$rowaa['whos_event'] = "only_creator";
						$artae[] = $rowaa;
					}
				}
			} 
			
			/*$sql_pro_coma = mysql_query("SELECT * FROM community_project WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_pro_coma)>0)
			{
				while($rowam = mysql_fetch_assoc($sql_pro_coma))
				{
					if($rowam['community_id']!=$id['community_id'])
					{
						$rowam['id'] = $rowam['id'].'~com';
						$coma[] = $rowam;
					}
				}
			}*/
			
			$sql_pro_coma = mysql_query("SELECT * FROM community_event WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0 AND month(date) = ".$m."
	        AND dayofmonth(date) = ".$d." AND
	        year(date)= ".$y."");
			if(mysql_num_rows($sql_pro_coma)>0)
			{
				while($rowam = mysql_fetch_assoc($sql_pro_coma))
				{
					if($rowam['community_id']!=$id['community_id'])
					{
						$rowam['edit'] = 'yes';
						$rowam['edit_com'] = 'yes';
						$rowam['whos_event'] = "only_creator";
						$comae[] = $rowam;
					}
				}
			} 
		}
		
		if($id['community_id']!=0 && !empty($id['community_id']))
		{
			/*$chk_type = $var_type_2.'|'.$id['community_id'];
			$sql_pro_artm = mysql_query("SELECT * FROM artist_project WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_pro_artm)>0)
			{
				while($rowcm = mysql_fetch_assoc($sql_pro_artm))
				{
					if($rowcm['artist_id']!=$id['artist_id'])
					{
						$rowcm['id'] = $rowcm['id'].'~art';
						$artm[] = $rowcm;
					}
				}
			}*/
			
			 $chk_type = $var_type_2.'|'.$id['community_id'];
			$sql_pro_artm = mysql_query("SELECT * FROM artist_event WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0 AND month(date) = ".$m."
	        AND dayofmonth(date) = ".$d." AND
	        year(date)= ".$y."");
			if(mysql_num_rows($sql_pro_artm)>0)
			{
				while($rowcm = mysql_fetch_assoc($sql_pro_artm))
				{
					if($rowcm['artist_id']!=$id['artist_id'])
					{
						$rowcm['edit'] = 'yes';
						$rowcm['whos_event'] = "only_creator";
						$artme[] = $rowcm;
					}
				}
			} 
			
			/*$sql_pro_comm = mysql_query("SELECT * FROM community_project WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_pro_comm)>0)
			{
				while($rowca = mysql_fetch_assoc($sql_pro_comm))
				{
					if($rowca['community_id']!=$id['community_id'])
					{
						$rowca['id'] = $rowca['id'].'~com';
						$comm[] = $rowca;
					}
				}
			}*/
			
			 $sql_pro_comm = mysql_query("SELECT * FROM community_event WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0 AND month(date) = ".$m."
	        AND dayofmonth(date) = ".$d." AND
	        year(date)= ".$y."");
			if(mysql_num_rows($sql_pro_comm)>0)
			{
				while($rowca = mysql_fetch_assoc($sql_pro_comm))
				{
					if($rowca['community_id']!=$id['community_id'])
					{
						$rowca['edit'] = 'yes';
						$rowca['edit_com'] = 'yes';
						$rowca['whos_event'] = "only_creator";
						$comme[] = $rowca;
					}
				}
			} 
			
			$sql_get_allp = mysql_query("SELECT * FROM community_project WHERE community_id='".$id['community_id']."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_get_allp)>0)
			{
				while($rowal = mysql_fetch_assoc($sql_get_allp))
				{
					$rowal['whos_event'] = "only_creator";
					$pal_2[] = $rowal;
				}
			}
			
			/*if(!empty($pal_2) && count($pal_2))
			{
				for($pl2=0;$pl2<count($pal_2);$pl2++)
				{
					$chk_typep_2 = 'community_project|'.$pal_2[$pl2]['id'];
					$sql = mysql_query("SELECT * FROM artist_project WHERE creators_info='".$chk_typep_2."' AND del_status=0 AND active=0");
					if(mysql_num_rows($sql)>0)
					{
						while($row_po = mysql_fetch_assoc($sql))
						{
							if($row_po['artist_id']!=$id['artist_id'])
							{
								$row_po['id'] = $row_po['id'].'~art';
								$art_p_co[] = $row_po;
							}
						}
					}
					
					$sql = mysql_query("SELECT * FROM community_project WHERE creators_info='".$chk_typep_2."' AND del_status=0 AND active=0");
					if(mysql_num_rows($sql)>0)
					{
						while($row_poo = mysql_fetch_assoc($sql))
						{
							if($row_poo['community_id']!=$id['community_id'])
							{
								$row_poo['id'] = $row_poo['id'].'~com';
								$com_p_co[] = $row_poo;
							}
						}
					}
				}
			}*/
			
			 if(!empty($pal_2) && count($pal_2))
			{
				for($pl2=0;$pl2<count($pal_2);$pl2++)
				{
					$chk_typep_2 = 'community_project|'.$pal_2[$pl2]['id'];
					$sql = mysql_query("SELECT * FROM artist_event WHERE creators_info='".$chk_typep_2."' AND del_status=0 AND active=0 AND month(date) = ".$m."
	        AND dayofmonth(date) = ".$d." AND
	        year(date)= ".$y."");
					if(mysql_num_rows($sql)>0)
					{
						while($row_po = mysql_fetch_assoc($sql))
						{
							if($row_po['artist_id']!=$id['artist_id'])
							{
								$row_po['edit'] = 'yes';
								$row_po['whos_event'] = "only_creator";
								$art_e_co[] = $row_po;
							}
						}
					}
					
					$sql = mysql_query("SELECT * FROM community_event WHERE creators_info='".$chk_typep_2."' AND del_status=0 AND active=0 AND month(date) = ".$m."
	        AND dayofmonth(date) = ".$d." AND
	        year(date)= ".$y."");
					if(mysql_num_rows($sql)>0)
					{
						while($row_poo = mysql_fetch_assoc($sql))
						{
							if($row_poo['community_id']!=$id['community_id'])
							{
								$row_poo['edit'] = 'yes';
								$row_poo['edit_com'] = 'yes';
								$row_poo['whos_event'] = "only_creator";
								$com_e_co[] = $row_poo;
							}
						}
					}
				}
			} 
		}
		
		/* var_dump($artm);
		var_dump($arta);
		var_dump($comm);
		var_dump($coma);
		var_dump($art_p_ao);
		var_dump($com_p_ao);
		var_dump($art_p_co);
		var_dump($com_p_co); */
		
		$totals = array();
		$totals = array_merge($artm,$arta,$comm,$coma,$art_p_ao,$com_p_ao,$art_p_co,$com_p_co,$artme,$artae,$comme,$comae,$art_e_ao,$com_e_ao,$art_e_co,$com_e_co);
		return $totals;
	}
	
	function only_creator_orecevent()
	{
		$id = $this->selgeneral();
		$date = date("Y-m-d");
		
		$var_type_1 = 'general_artist';
		$var_type_2 = 'general_community';
		$var_type_3 = 'artist_project';
		$var_type_4 = 'community_project';
		
		$artm = array();
		$arta = array();
		$comm = array();
		$coma = array();
		
		$artme = array();
		$artae = array();
		$comme = array();
		$comae = array();
		
		$art_p_ao = array();
		$com_p_ao = array();
		$art_p_co = array();
		$com_p_co = array();
		
		$art_e_ao = array();
		$com_e_ao = array();
		$art_e_co = array();
		$com_e_co = array();
		
		$pal_1 = array();
		$pal_2 = array();
		
		if($id['artist_id']!=0 && !empty($id['artist_id']))
		{
			$chk_type = $var_type_1.'|'.$id['artist_id'];
			
			$sql_get_allp = mysql_query("SELECT * FROM artist_project WHERE artist_id='".$id['artist_id']."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_get_allp)>0)
			{
				while($rowal = mysql_fetch_assoc($sql_get_allp))
				{
					$rowal['whos_event'] = "only_creator";
					$pal_1[] = $rowal;
				}
			}
			
			
			
			 if(!empty($pal_1) && count($pal_1))
			{
				for($pl=0;$pl<count($pal_1);$pl++)
				{
					$chk_typep_1 = 'artist_project|'.$pal_1[$pl]['id'];
					
					$sql = mysql_query("SELECT * FROM artist_event WHERE creators_info='".$chk_typep_1."' AND del_status=0 AND active=0 AND date<'".$date."'");
					if(mysql_num_rows($sql)>0)
					{
						while($row_po = mysql_fetch_assoc($sql))
						{
							if($row_po['artist_id']!=$id['artist_id'])
							{
								$row_po['edit'] = 'yes';
								$row_po['whos_event'] = "only_creator";
								$row_po['id'] = $row_po['id'] ."~art";
								$art_e_ao[] = $row_po;
							}
						}
					}
					
					$sql = mysql_query("SELECT * FROM community_event WHERE creators_info='".$chk_typep_1."' AND del_status=0 AND active=0 AND date<'".$date."'");
					if(mysql_num_rows($sql)>0)
					{
						while($row_poo = mysql_fetch_assoc($sql))
						{
							if($row_poo['community_id']!=$id['community_id'])
							{
								$row_poo['edit'] = 'yes';
								$row_poo['edit_com'] = 'yes';
								$row_poo['whos_event'] = "only_creator";
								$row_poo['id'] = $row_poo['id'] ."~com";
								$com_e_ao[] = $row_poo;
							}
						}
					}
				}
			} 

			/*$sql_pro_arta = mysql_query("SELECT * FROM artist_project WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_pro_arta)>0)
			{
				while($rowaa = mysql_fetch_assoc($sql_pro_arta))
				{
					if($rowaa['artist_id']!=$id['artist_id'])
					{
						$rowaa['id'] = $rowaa['id'].'~art';
						$arta[] = $rowaa;
					}
				}
			}*/
			
			$sql_pro_arta = mysql_query("SELECT * FROM artist_event WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0 AND date<'".$date."'");
			if(mysql_num_rows($sql_pro_arta)>0)
			{
				while($rowaa = mysql_fetch_assoc($sql_pro_arta))
				{
					if($rowaa['artist_id']!=$id['artist_id'])
					{
						$rowaa['edit'] = 'yes';
						$rowaa['whos_event'] = "only_creator";
						$rowaa['id'] = $rowaa['id'] ."~art";
						$artae[] = $rowaa;
					}
				}
			} 
			
			/*$sql_pro_coma = mysql_query("SELECT * FROM community_project WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_pro_coma)>0)
			{
				while($rowam = mysql_fetch_assoc($sql_pro_coma))
				{
					if($rowam['community_id']!=$id['community_id'])
					{
						$rowam['id'] = $rowam['id'].'~com';
						$coma[] = $rowam;
					}
				}
			}*/
			
			$sql_pro_coma = mysql_query("SELECT * FROM community_event WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0 AND date<'".$date."'");
			if(mysql_num_rows($sql_pro_coma)>0)
			{
				while($rowam = mysql_fetch_assoc($sql_pro_coma))
				{
					if($rowam['community_id']!=$id['community_id'])
					{
						$rowam['edit'] = 'yes';
						$rowam['edit_com'] = 'yes';
						$rowam['whos_event'] = "only_creator";
						$rowam['id'] = $rowam['id'] ."~com";
						$comae[] = $rowam;
					}
				}
			} 
		}
		
		if($id['community_id']!=0 && !empty($id['community_id']))
		{
			/*$chk_type = $var_type_2.'|'.$id['community_id'];
			$sql_pro_artm = mysql_query("SELECT * FROM artist_project WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_pro_artm)>0)
			{
				while($rowcm = mysql_fetch_assoc($sql_pro_artm))
				{
					if($rowcm['artist_id']!=$id['artist_id'])
					{
						$rowcm['id'] = $rowcm['id'].'~art';
						$artm[] = $rowcm;
					}
				}
			}*/
			
			 $chk_type = $var_type_2.'|'.$id['community_id'];
			$sql_pro_artm = mysql_query("SELECT * FROM artist_event WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0 AND date<'".$date."'");
			if(mysql_num_rows($sql_pro_artm)>0)
			{
				while($rowcm = mysql_fetch_assoc($sql_pro_artm))
				{
					if($rowcm['artist_id']!=$id['artist_id'])
					{
						$rowcm['edit'] = 'yes';
						$rowcm['whos_event'] = "only_creator";
						$rowcm['id'] = $rowcm['id'] ."~art";
						$artme[] = $rowcm;
					}
				}
			} 
			
			/*$sql_pro_comm = mysql_query("SELECT * FROM community_project WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_pro_comm)>0)
			{
				while($rowca = mysql_fetch_assoc($sql_pro_comm))
				{
					if($rowca['community_id']!=$id['community_id'])
					{
						$rowca['id'] = $rowca['id'].'~com';
						$comm[] = $rowca;
					}
				}
			}*/
			
			 $sql_pro_comm = mysql_query("SELECT * FROM community_event WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0 AND date<'".$date."'");
			if(mysql_num_rows($sql_pro_comm)>0)
			{
				while($rowca = mysql_fetch_assoc($sql_pro_comm))
				{
					if($rowca['community_id']!=$id['community_id'])
					{
						$rowca['edit'] = 'yes';
						$rowca['edit_com'] = 'yes';
						$rowca['whos_event'] = "only_creator";
						$rowca['id'] = $rowca['id'] ."~com";
						$comme[] = $rowca;
					}
				}
			} 
			
			$sql_get_allp = mysql_query("SELECT * FROM community_project WHERE community_id='".$id['community_id']."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_get_allp)>0)
			{
				while($rowal = mysql_fetch_assoc($sql_get_allp))
				{
					$rowal['whos_event'] = "only_creator";
					$pal_2[] = $rowal;
				}
			}
			
			/*if(!empty($pal_2) && count($pal_2))
			{
				for($pl2=0;$pl2<count($pal_2);$pl2++)
				{
					$chk_typep_2 = 'community_project|'.$pal_2[$pl2]['id'];
					$sql = mysql_query("SELECT * FROM artist_project WHERE creators_info='".$chk_typep_2."' AND del_status=0 AND active=0");
					if(mysql_num_rows($sql)>0)
					{
						while($row_po = mysql_fetch_assoc($sql))
						{
							if($row_po['artist_id']!=$id['artist_id'])
							{
								$row_po['id'] = $row_po['id'].'~art';
								$art_p_co[] = $row_po;
							}
						}
					}
					
					$sql = mysql_query("SELECT * FROM community_project WHERE creators_info='".$chk_typep_2."' AND del_status=0 AND active=0");
					if(mysql_num_rows($sql)>0)
					{
						while($row_poo = mysql_fetch_assoc($sql))
						{
							if($row_poo['community_id']!=$id['community_id'])
							{
								$row_poo['id'] = $row_poo['id'].'~com';
								$com_p_co[] = $row_poo;
							}
						}
					}
				}
			}*/
			
			 if(!empty($pal_2) && count($pal_2))
			{
				for($pl2=0;$pl2<count($pal_2);$pl2++)
				{
					$chk_typep_2 = 'community_project|'.$pal_2[$pl2]['id'];
					$sql = mysql_query("SELECT * FROM artist_event WHERE creators_info='".$chk_typep_2."' AND del_status=0 AND active=0 AND date<'".$date."'");
					if(mysql_num_rows($sql)>0)
					{
						while($row_po = mysql_fetch_assoc($sql))
						{
							if($row_po['artist_id']!=$id['artist_id'])
							{
								$row_po['edit'] = 'yes';
								$row_po['whos_event'] = "only_creator";
								$row_po['id'] = $row_po['id'] ."~art";
								$art_e_co[] = $row_po;
							}
						}
					}
					
					$sql = mysql_query("SELECT * FROM community_event WHERE creators_info='".$chk_typep_2."' AND del_status=0 AND active=0 AND date<'".$date."'");
					if(mysql_num_rows($sql)>0)
					{
						while($row_poo = mysql_fetch_assoc($sql))
						{
							if($row_poo['community_id']!=$id['community_id'])
							{
								$row_poo['edit'] = 'yes';
								$row_poo['edit_com'] = 'yes';
								$row_poo['whos_event'] = "only_creator";
								$row_poo['id'] = $row_poo['id'] ."~com";
								$com_e_co[] = $row_poo;
							}
						}
					}
				}
			} 
		}
		
		/* var_dump($artm);
		var_dump($arta);
		var_dump($comm);
		var_dump($coma);
		var_dump($art_p_ao);
		var_dump($com_p_ao);
		var_dump($art_p_co);
		var_dump($com_p_co); */
		
		$totals = array();
		$totals = array_merge($artm,$arta,$comm,$coma,$art_p_ao,$com_p_ao,$art_p_co,$com_p_co,$artme,$artae,$comme,$comae,$art_e_ao,$com_e_ao,$art_e_co,$com_e_co);
		return $totals;
	}
	
	function only_creator_calrec($d,$m,$y)
	{
		$id = $this->selgeneral();
		$date = date("Y-m-d");
		
		$var_type_1 = 'general_artist';
		$var_type_2 = 'general_community';
		$var_type_3 = 'artist_project';
		$var_type_4 = 'community_project';
		
		$artm = array();
		$arta = array();
		$comm = array();
		$coma = array();
		
		$artme = array();
		$artae = array();
		$comme = array();
		$comae = array();
		
		$art_p_ao = array();
		$com_p_ao = array();
		$art_p_co = array();
		$com_p_co = array();
		
		$art_e_ao = array();
		$com_e_ao = array();
		$art_e_co = array();
		$com_e_co = array();
		
		$pal_1 = array();
		$pal_2 = array();
		
		if($id['artist_id']!=0 && !empty($id['artist_id']))
		{
			$chk_type = $var_type_1.'|'.$id['artist_id'];
			
			$sql_get_allp = mysql_query("SELECT * FROM artist_project WHERE artist_id='".$id['artist_id']."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_get_allp)>0)
			{
				while($rowal = mysql_fetch_assoc($sql_get_allp))
				{
					$rowal['whos_event'] = "only_creator";
					$pal_1[] = $rowal;
				}
			}
			
			
			
			 if(!empty($pal_1) && count($pal_1))
			{
				for($pl=0;$pl<count($pal_1);$pl++)
				{
					$chk_typep_1 = 'artist_project|'.$pal_1[$pl]['id'];
					
					$sql = mysql_query("SELECT * FROM artist_event WHERE creators_info='".$chk_typep_1."' AND del_status=0 AND active=0 AND month(date) = ".$m."
	        AND dayofmonth(date) = ".$d." AND
	        year(date)= ".$y."");
					if(mysql_num_rows($sql)>0)
					{
						while($row_po = mysql_fetch_assoc($sql))
						{
							if($row_po['artist_id']!=$id['artist_id'])
							{
								$row_po['edit'] = 'yes';
								$row_po['whos_event'] = "only_creator";
							//	$row_po['id'] = $row_po['id'] ."~art";
								$art_e_ao[] = $row_po;
							}
						}
					}
					
					$sql = mysql_query("SELECT * FROM community_event WHERE creators_info='".$chk_typep_1."' AND del_status=0 AND active=0 AND month(date) = ".$m."
	        AND dayofmonth(date) = ".$d." AND
	        year(date)= ".$y."");
					if(mysql_num_rows($sql)>0)
					{
						while($row_poo = mysql_fetch_assoc($sql))
						{
							if($row_poo['community_id']!=$id['community_id'])
							{
								$row_poo['edit'] = 'yes';
								$row_poo['edit_com'] = 'yes';
								$row_poo['whos_event'] = "only_creator";
							//	$row_poo['id'] = $row_poo['id'] ."~com";
								$com_e_ao[] = $row_poo;
							}
						}
					}
				}
			} 

			/*$sql_pro_arta = mysql_query("SELECT * FROM artist_project WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_pro_arta)>0)
			{
				while($rowaa = mysql_fetch_assoc($sql_pro_arta))
				{
					if($rowaa['artist_id']!=$id['artist_id'])
					{
						$rowaa['id'] = $rowaa['id'].'~art';
						$arta[] = $rowaa;
					}
				}
			}*/
			
			$sql_pro_arta = mysql_query("SELECT * FROM artist_event WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0 AND month(date) = ".$m."
	        AND dayofmonth(date) = ".$d." AND
	        year(date)= ".$y."");
			if(mysql_num_rows($sql_pro_arta)>0)
			{
				while($rowaa = mysql_fetch_assoc($sql_pro_arta))
				{
					if($rowaa['artist_id']!=$id['artist_id'])
					{
						$rowaa['edit'] = 'yes';
						$rowaa['whos_event'] = "only_creator";
					//	$rowaa['id'] = $rowaa['id'] ."~art";
						$artae[] = $rowaa;
					}
				}
			} 
			
			/*$sql_pro_coma = mysql_query("SELECT * FROM community_project WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_pro_coma)>0)
			{
				while($rowam = mysql_fetch_assoc($sql_pro_coma))
				{
					if($rowam['community_id']!=$id['community_id'])
					{
						$rowam['id'] = $rowam['id'].'~com';
						$coma[] = $rowam;
					}
				}
			}*/
			
			$sql_pro_coma = mysql_query("SELECT * FROM community_event WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0 AND month(date) = ".$m."
	        AND dayofmonth(date) = ".$d." AND
	        year(date)= ".$y."");
			if(mysql_num_rows($sql_pro_coma)>0)
			{
				while($rowam = mysql_fetch_assoc($sql_pro_coma))
				{
					if($rowam['community_id']!=$id['community_id'])
					{
						$rowam['edit'] = 'yes';
						$rowam['edit_com'] = 'yes';
						$rowam['whos_event'] = "only_creator";
					//	$rowam['id'] = $rowam['id'] ."~com";
						$comae[] = $rowam;
					}
				}
			} 
		}
		
		if($id['community_id']!=0 && !empty($id['community_id']))
		{
			/*$chk_type = $var_type_2.'|'.$id['community_id'];
			$sql_pro_artm = mysql_query("SELECT * FROM artist_project WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_pro_artm)>0)
			{
				while($rowcm = mysql_fetch_assoc($sql_pro_artm))
				{
					if($rowcm['artist_id']!=$id['artist_id'])
					{
						$rowcm['id'] = $rowcm['id'].'~art';
						$artm[] = $rowcm;
					}
				}
			}*/
			
			 $chk_type = $var_type_2.'|'.$id['community_id'];
			$sql_pro_artm = mysql_query("SELECT * FROM artist_event WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0 AND month(date) = ".$m."
	        AND dayofmonth(date) = ".$d." AND
	        year(date)= ".$y."");
			if(mysql_num_rows($sql_pro_artm)>0)
			{
				while($rowcm = mysql_fetch_assoc($sql_pro_artm))
				{
					if($rowcm['artist_id']!=$id['artist_id'])
					{
						$rowcm['edit'] = 'yes';
						$rowcm['whos_event'] = "only_creator";
					//	$rowcm['id'] = $rowcm['id'] ."~art";
						$artme[] = $rowcm;
					}
				}
			} 
			
			/*$sql_pro_comm = mysql_query("SELECT * FROM community_project WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_pro_comm)>0)
			{
				while($rowca = mysql_fetch_assoc($sql_pro_comm))
				{
					if($rowca['community_id']!=$id['community_id'])
					{
						$rowca['id'] = $rowca['id'].'~com';
						$comm[] = $rowca;
					}
				}
			}*/
			
			 $sql_pro_comm = mysql_query("SELECT * FROM community_event WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0 AND month(date) = ".$m."
	        AND dayofmonth(date) = ".$d." AND
	        year(date)= ".$y."");
			if(mysql_num_rows($sql_pro_comm)>0)
			{
				while($rowca = mysql_fetch_assoc($sql_pro_comm))
				{
					if($rowca['community_id']!=$id['community_id'])
					{
						$rowca['edit'] = 'yes';
						$rowca['edit_com'] = 'yes';
						$rowca['whos_event'] = "only_creator";
					//	$rowca['id'] = $rowca['id'] ."~com";
						$comme[] = $rowca;
					}
				}
			} 
			
			$sql_get_allp = mysql_query("SELECT * FROM community_project WHERE community_id='".$id['community_id']."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_get_allp)>0)
			{
				while($rowal = mysql_fetch_assoc($sql_get_allp))
				{
					$rowal['whos_event'] = "only_creator";
					$pal_2[] = $rowal;
				}
			}
			
			/*if(!empty($pal_2) && count($pal_2))
			{
				for($pl2=0;$pl2<count($pal_2);$pl2++)
				{
					$chk_typep_2 = 'community_project|'.$pal_2[$pl2]['id'];
					$sql = mysql_query("SELECT * FROM artist_project WHERE creators_info='".$chk_typep_2."' AND del_status=0 AND active=0");
					if(mysql_num_rows($sql)>0)
					{
						while($row_po = mysql_fetch_assoc($sql))
						{
							if($row_po['artist_id']!=$id['artist_id'])
							{
								$row_po['id'] = $row_po['id'].'~art';
								$art_p_co[] = $row_po;
							}
						}
					}
					
					$sql = mysql_query("SELECT * FROM community_project WHERE creators_info='".$chk_typep_2."' AND del_status=0 AND active=0");
					if(mysql_num_rows($sql)>0)
					{
						while($row_poo = mysql_fetch_assoc($sql))
						{
							if($row_poo['community_id']!=$id['community_id'])
							{
								$row_poo['id'] = $row_poo['id'].'~com';
								$com_p_co[] = $row_poo;
							}
						}
					}
				}
			}*/
			
			 if(!empty($pal_2) && count($pal_2))
			{
				for($pl2=0;$pl2<count($pal_2);$pl2++)
				{
					$chk_typep_2 = 'community_project|'.$pal_2[$pl2]['id'];
					$sql = mysql_query("SELECT * FROM artist_event WHERE creators_info='".$chk_typep_2."' AND del_status=0 AND active=0 AND month(date) = ".$m."
	        AND dayofmonth(date) = ".$d." AND
	        year(date)= ".$y."");
					if(mysql_num_rows($sql)>0)
					{
						while($row_po = mysql_fetch_assoc($sql))
						{
							if($row_po['artist_id']!=$id['artist_id'])
							{
								$row_po['edit'] = 'yes';
								$row_po['whos_event'] = "only_creator";
							//	$row_po['id'] = $row_po['id'] ."~art";
								$art_e_co[] = $row_po;
							}
						}
					}
					
					$sql = mysql_query("SELECT * FROM community_event WHERE creators_info='".$chk_typep_2."' AND del_status=0 AND active=0 AND month(date) = ".$m."
	        AND dayofmonth(date) = ".$d." AND
	        year(date)= ".$y."");
					if(mysql_num_rows($sql)>0)
					{
						while($row_poo = mysql_fetch_assoc($sql))
						{
							if($row_poo['community_id']!=$id['community_id'])
							{
								$row_poo['edit'] = 'yes';
								$row_poo['edit_com'] = 'yes';
								$row_poo['whos_event'] = "only_creator";
							//	$row_poo['id'] = $row_poo['id'] ."~com";
								$com_e_co[] = $row_poo;
							}
						}
					}
				}
			} 
		}
		
		/* var_dump($artm);
		var_dump($arta);
		var_dump($comm);
		var_dump($coma);
		var_dump($art_p_ao);
		var_dump($com_p_ao);
		var_dump($art_p_co);
		var_dump($com_p_co); */
		
		$totals = array();
		$totals = array_merge($artm,$arta,$comm,$coma,$art_p_ao,$com_p_ao,$art_p_co,$com_p_co,$artme,$artae,$comme,$comae,$art_e_ao,$com_e_ao,$art_e_co,$com_e_co);
		return $totals;
	}
	
	/*function get_tag_where_heis($email){
		$get_det = $this->selgeneral();
		$get_media_artist_project1 = array();
		$get_media_artist_event1 = array();
		$get_media_community_project1 = array();
		$get_media_community_event1 = array();
			
			/**get community projects**//*
			$get_all_tag_det = mysql_query("select * from community_project where tagged_user_email LIKE '%".$email."%'");
			if(mysql_num_rows($get_all_tag_det)>0){
				while($row_res = mysql_fetch_assoc($get_all_tag_det)){
					$creator_info = "community_project|".$row_res['id'];
					$find_cretor_med = mysql_query("select * from general_media where creator_info='".$creator_info."' OR from_info='".$creator_info."'");
					if(mysql_num_rows($find_cretor_med)>0){
						while($res_new_det = mysql_fetch_assoc($find_cretor_med)){
							$get_media_community_project1 = $res_new_det;
						}
					}
				}
			}
			
			$get_all_tag_det_com_eve = mysql_query("select * from community_event where tagged_user_email LIKE '%".$email."%'");
			if(mysql_num_rows($get_all_tag_det_com_eve)>0){
				while($row_res_com_eve = mysql_fetch_assoc($get_all_tag_det_com_eve)){
					$creator_info = "community_event|".$row_res_com_eve['id'];
					$find_cretor_med = mysql_query("select * from general_media where creator_info='".$creator_info."' OR from_info='".$creator_info."'");
					if(mysql_num_rows($find_cretor_med)>0){
						while($res_new_det = mysql_fetch_assoc($find_cretor_med)){
							$get_media_community_event1 = $res_new_det;
						}
					}
				}
			}
			
			$get_all_tag_det_art_pro = mysql_query("select * from artist_project where tagged_user_email LIKE '%".$email."%'");
			if(mysql_num_rows($get_all_tag_det_art_pro)>0){
				while($row_res_art_pro = mysql_fetch_assoc($get_all_tag_det_art_pro)){
					$creator_info = "artist_project|".$row_res_art_pro['id'];
					$find_cretor_med = mysql_query("select * from general_media where creator_info='".$creator_info."' OR from_info='".$creator_info."'");
					if(mysql_num_rows($find_cretor_med)>0){
						while($res_new_det = mysql_fetch_assoc($find_cretor_med)){
							$get_media_artist_project1 = $res_new_det;
						}
					}
				}
			}
			
			$get_all_tag_det_art_eve = mysql_query("select * from artist_event where tagged_user_email LIKE '%".$email."%'");
			if(mysql_num_rows($get_all_tag_det_art_eve)>0){
				while($row_res_art_eve = mysql_fetch_assoc($get_all_tag_det_art_eve)){
					$creator_info = "artist_event|".$row_res_art_eve['id'];
					$find_cretor_med = mysql_query("select * from general_media where creator_info='".$creator_info."' OR from_info='".$creator_info."'");
					if(mysql_num_rows($find_cretor_med)>0){
						while($res_new_det = mysql_fetch_assoc($find_cretor_med)){
							$get_media_artist_event1 = $res_new_det;
						}
					}
				}
			}

		$get_media = array_merge($get_media_artist_project1,$get_media_artist_event1,$get_media_community_project1,$get_media_community_event1);
		return($get_media);
	}*/
	
	function get_media_where_creator_or_from()
	{
		$get_art_media1 = array();
		$get_art_media2 = array();
		$get_art_media3 = array();
		$get_art_media4 = array();
		$get_art_media5 = array();
		$get_art_media6 = array();
		
		$get_gen_det = $this->selgeneral();
		if($get_gen_det['artist_id']>0){
			$creator_info = "general_artist|".$get_gen_det['artist_id'];
			$query = mysql_query("select * from general_media where creator_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
			if(mysql_num_rows($query)>0){
				while($row_art = mysql_fetch_assoc($query)){
					if($get_gen_det['general_user_id']!= $row_art['general_user_id']){
					$get_art_media1[] = $row_art;
					}
				}
			}
			$get_creator = mysql_query("select * from artist_project where artist_id='".$get_gen_det['artist_id']."' AND del_status=0 AND active=0");
			if(mysql_num_rows($get_creator)>0)
			{
				while($row_art_pro = mysql_fetch_assoc($get_creator))
				{
					$creator_info = "artist_project|".$row_art_pro["id"];
					$query1 = mysql_query("select * from general_media where creator_info = '".$creator_info."' OR from_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
					if(mysql_num_rows($query1)>0){
						while($row_art_media = mysql_fetch_assoc($query1)){
							if($get_gen_det['general_user_id']!= $row_art_media['general_user_id']){
							$get_art_media2[] = $row_art_media;
							}
						}
					}
				}
			}
			$get_creator = mysql_query("select * from artist_event where artist_id='".$get_gen_det['artist_id']."' AND del_status=0 AND active=0");
			if(mysql_num_rows($get_creator)>0)
			{
				while($row_art_eve = mysql_fetch_assoc($get_creator))
				{
					$creator_info = "artist_event|".$row_art_eve["id"];
					$query1 = mysql_query("select * from general_media where creator_info = '".$creator_info."' OR from_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
					if(mysql_num_rows($query1)>0){
						while($row_art_media = mysql_fetch_assoc($query1)){
							if($get_gen_det['general_user_id']!= $row_art_media['general_user_id']){
							$get_art_media3[] = $row_art_media;
							}
						}
					}
				}
			}
		}
		if($get_gen_det['community_id']>0){
			$creator_info = "general_community|".$get_gen_det['community_id'];
			$query = mysql_query("select * from general_media where creator_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
			if(mysql_num_rows($query)>0){
				while($row_com = mysql_fetch_assoc($query)){
					//echo "azher4";
					if($get_gen_det['general_user_id']!= $row_com['general_user_id']){
					$get_art_media4[] = $row_com;
					}
				}
			}
			$get_creator = mysql_query("select * from community_project where community_id='".$get_gen_det['community_id']."' AND del_status=0 AND active=0");
			if(mysql_num_rows($get_creator)>0)
			{
				while($row_com_pro = mysql_fetch_assoc($get_creator))
				{
					$creator_info = "community_project|".$row_com_pro["id"];
					$query1 = mysql_query("select * from general_media where creator_info = '".$creator_info."' OR from_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
					if(mysql_num_rows($query1)>0){
						while($row_com_media = mysql_fetch_assoc($query1)){
							//echo "azher5";
							if($get_gen_det['general_user_id']!= $row_com_media['general_user_id']){
							$get_art_media5[] = $row_com_media;
							}
						}
					}
				}
			}
			$get_creator = mysql_query("select * from community_event where community_id='".$get_gen_det['community_id']."' AND del_status=0 AND active=0");
			if(mysql_num_rows($get_creator)>0)
			{
				while($row_com_eve = mysql_fetch_assoc($get_creator))
				{
					$creator_info = "community_event|".$row_com_eve["id"];
					$query1 = mysql_query("select * from general_media where creator_info = '".$creator_info."' OR from_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
					if(mysql_num_rows($query1)>0){
						while($row_com_media = mysql_fetch_assoc($query1)){
							if($get_gen_det['general_user_id']!= $row_com_media['general_user_id']){
							$get_art_media6[] = $row_com_media;
							}
						}
					}
				}
			}
		}
		
		$all_media = array_merge($get_art_media1,$get_art_media2,$get_art_media3,$get_art_media4,$get_art_media5,$get_art_media6);
		return($all_media);
	}
	
	function get_creator_heis()
	{
		$id = $this->selgeneral();
        $var_type_1 = 'general_artist';
        $var_type_2 = 'general_community';
        $var_type_3 = 'artist_project';
        $var_type_4 = 'community_project';
		$res_media1 = array();
		$res_media2 = array();
		$res_media3 = array();
		$res_media4 = array();
		$res_media5 = array();
		$res_media6 = array();
		$res_media7 = array();
		$res_media8 = array();
		$res_media9 = array();
		$res_media10 = array();
		$res_media11 = array();
		$res_media12 = array();
		$res_media13 = array();
		$res_media14 = array();
		$res_media15 = array();
		$res_media16 = array();
        if($id['artist_id']!=0 && !empty($id['artist_id']))
        {
            $chk_type = $var_type_1.'|'.$id['artist_id'];
			//echo "select * from artist_project where creators_info='".$chk_type."'";
			$sql_get_pro = mysql_query("select * from artist_project where creators_info='".$chk_type."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_get_pro)>0)
			{
				while($row = mysql_fetch_assoc($sql_get_pro))
				{
					$creator_info = "artist_project|".$row['id'];
					//echo "select * from general_media where creator_info = '".$creator_info."'";
					$get_media = mysql_query("select * from general_media where creator_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
					if(mysql_num_rows($get_media)>0)
					{
						while($res = mysql_fetch_assoc($get_media))
						{
							$res_media1[] = $res;
						}
					}
				}
			}
			$sql_get_pro1 = mysql_query("select * from community_project where creators_info='".$chk_type."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_get_pro1)>0)
			{
				while($row1 = mysql_fetch_assoc($sql_get_pro1))
				{
					$creator_info = "community_project|".$row1['id'];
					$get_media = mysql_query("select * from general_media where creator_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
					if(mysql_num_rows($get_media)>0)
					{
						while($res = mysql_fetch_assoc($get_media))
						{
							$res_media2[] = $res;
						}
					}
				}
			}
			/*$sql_get_pro2 = mysql_query("select * from artist_event where creators_info='".$chk_type."'");
			if(mysql_num_rows($sql_get_pro2)>0)
			{
				while($row = mysql_fetch_assoc($sql_get_pro2))
				{
					$creator_info = "artist_event|".$row['id'];
					$get_media = mysql_query("select * from general_media where creator_info = '".$creator_info."'");
					if(mysql_num_rows($get_media)>0)
					{
						while($res = mysql_fetch_assoc($get_media))
						{
							$res_media3[] = $res;
						}
					}
				}
			}
			$sql_get_pro3 = mysql_query("select * from community_event where creators_info='".$chk_type."'");
			if(mysql_num_rows($sql_get_pro3)>0)
			{
				while($row1 = mysql_fetch_assoc($sql_get_pro3))
				{
					$creator_info = "community_event|".$row1['id'];
					$get_media = mysql_query("select * from general_media where creator_info = '".$creator_info."'");
					if(mysql_num_rows($get_media)>0)
					{
						while($res = mysql_fetch_assoc($get_media))
						{
							$res_media4[] = $res;
						}
					}
				}
			}*/
			
			$find_project = mysql_query("select * from artist_project where artist_id='".$id['artist_id']."' AND del_status=0 AND active=0");
			if(mysql_num_rows($find_project)>0)
			{
				while($get_all_pro = mysql_fetch_assoc($find_project))
				{
					$new_creator_info = "artist_project|".$get_all_pro['id'];
					$sql_get_pro = mysql_query("select * from artist_project where creators_info='".$new_creator_info."' AND del_status=0 AND active=0");
					if(mysql_num_rows($sql_get_pro)>0)
					{
						while($row = mysql_fetch_assoc($sql_get_pro))
						{
							$creator_info = "artist_project|".$row['id'];
							$get_media = mysql_query("select * from general_media where creator_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
							if(mysql_num_rows($get_media)>0)
							{
								while($res = mysql_fetch_assoc($get_media))
								{
									$res_media5[] = $res;
								}
							}
						}
					}
					$sql_get_pro1 = mysql_query("select * from community_project where creators_info='".$new_creator_info."' AND del_status=0 AND active=0");
					if(mysql_num_rows($sql_get_pro1)>0)
					{
						while($row1 = mysql_fetch_assoc($sql_get_pro1))
						{
							$creator_info = "community_project|".$row1['id'];
							$get_media = mysql_query("select * from general_media where creator_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
							if(mysql_num_rows($get_media)>0)
							{
								while($res = mysql_fetch_assoc($get_media))
								{
									$res_media6[] = $res;
								}
							}
						}
					}
					/*$sql_get_pro2 = mysql_query("select * from artist_event where creators_info='".$new_creator_info."'");
					if(mysql_num_rows($sql_get_pro2)>0)
					{
						while($row = mysql_fetch_assoc($sql_get_pro2))
						{
							$creator_info = "artist_event|".$row['id'];
							$get_media = mysql_query("select * from general_media where creator_info = '".$creator_info."'");
							if(mysql_num_rows($get_media)>0)
							{
								while($res = mysql_fetch_assoc($get_media))
								{
									$res_media7[] = $res;
								}
							}
						}
					}
					$sql_get_pro3 = mysql_query("select * from community_event where creators_info='".$new_creator_info."'");
					if(mysql_num_rows($sql_get_pro3)>0)
					{
						while($row1 = mysql_fetch_assoc($sql_get_pro3))
						{
							$creator_info = "community_event|".$row1['id'];
							$get_media = mysql_query("select * from general_media where creator_info = '".$creator_info."'");
							if(mysql_num_rows($get_media)>0)
							{
								while($res = mysql_fetch_assoc($get_media))
								{
									$res_media8[] = $res;
								}
							}
						}
					}*/
				}
			}
		   
		}
		if($id['community_id']!=0 && !empty($id['community_id']))
        {
            $chk_type = $var_type_2.'|'.$id['community_id'];
			$sql_get_pro = mysql_query("select * from artist_project where creators_info='".$chk_type."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_get_pro)>0)
			{
				while($row = mysql_fetch_assoc($sql_get_pro))
				{
					$creator_info = "artist_project|".$row['id'];
					$get_media = mysql_query("select * from general_media where creator_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
					if(mysql_num_rows($get_media)>0)
					{
						while($res = mysql_fetch_assoc($get_media))
						{
							$res_media9[] = $res;
						}
					}
				}
			}
			$sql_get_pro1 = mysql_query("select * from community_project where creators_info='".$chk_type."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_get_pro1)>0)
			{
				while($row1 = mysql_fetch_assoc($sql_get_pro1))
				{
					$creator_info = "community_project|".$row1['id'];
					$get_media = mysql_query("select * from general_media where creator_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
					if(mysql_num_rows($get_media)>0)
					{
						while($res = mysql_fetch_assoc($get_media))
						{
							$res_media10[] = $res;
						}
					}
				}
			}
			/*$sql_get_pro2 = mysql_query("select * from artist_event where creators_info='".$chk_type."'");
			if(mysql_num_rows($sql_get_pro2)>0)
			{
				while($row = mysql_fetch_assoc($sql_get_pro2))
				{
					$creator_info = "artist_event|".$row['id'];
					$get_media = mysql_query("select * from general_media where creator_info = '".$creator_info."'");
					if(mysql_num_rows($get_media)>0)
					{
						while($res = mysql_fetch_assoc($get_media))
						{
							$res_media11[] = $res;
						}
					}
				}
			}
			$sql_get_pro3 = mysql_query("select * from community_event where creators_info='".$chk_type."'");
			if(mysql_num_rows($sql_get_pro3)>0)
			{
				while($row1 = mysql_fetch_assoc($sql_get_pro3))
				{
					$creator_info = "community_event|".$row1['id'];
					$get_media = mysql_query("select * from general_media where creator_info = '".$creator_info."'");
					if(mysql_num_rows($get_media)>0)
					{
						while($res = mysql_fetch_assoc($get_media))
						{
							$res_media12[] = $res;
						}
					}
				}
			}*/
			$find_project = mysql_query("select * from community_project where community_id='".$id['community_id']."' AND del_status=0 AND active=0");
			if(mysql_num_rows($find_project)>0)
			{
				while($get_all_pro = mysql_fetch_assoc($find_project))
				{
					$new_creator_info = "community_project|".$get_all_pro['id'];
					$sql_get_pro = mysql_query("select * from artist_project where creators_info='".$new_creator_info."' AND del_status=0 AND active=0");
					if(mysql_num_rows($sql_get_pro)>0)
					{
						while($row = mysql_fetch_assoc($sql_get_pro))
						{
							$creator_info = "artist_project|".$row['id'];
							$get_media = mysql_query("select * from general_media where creator_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
							if(mysql_num_rows($get_media)>0)
							{
								while($res = mysql_fetch_assoc($get_media))
								{
									$res_media13[] = $res;
								}
							}
						}
					}
					$sql_get_pro1 = mysql_query("select * from community_project where creators_info='".$new_creator_info."' AND del_status=0 AND active=0");
					if(mysql_num_rows($sql_get_pro1)>0)
					{
						while($row1 = mysql_fetch_assoc($sql_get_pro1))
						{
							$creator_info = "community_project|".$row1['id'];
							$get_media = mysql_query("select * from general_media where creator_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
							if(mysql_num_rows($get_media)>0)
							{
								while($res = mysql_fetch_assoc($get_media))
								{
									$res_media14[] = $res;
								}
							}
						}
					}
					/*$sql_get_pro2 = mysql_query("select * from artist_event where creators_info='".$new_creator_info."'");
					if(mysql_num_rows($sql_get_pro2)>0)
					{
						while($row = mysql_fetch_assoc($sql_get_pro2))
						{
							$creator_info = "artist_event|".$row['id'];
							$get_media = mysql_query("select * from general_media where creator_info = '".$creator_info."'");
							if(mysql_num_rows($get_media)>0)
							{
								while($res = mysql_fetch_assoc($get_media))
								{
									$res_media15[] = $res;
								}
							}
						}
					}
					$sql_get_pro3 = mysql_query("select * from community_event where creators_info='".$new_creator_info."'");
					if(mysql_num_rows($sql_get_pro3)>0)
					{
						while($row1 = mysql_fetch_assoc($sql_get_pro3))
						{
							$creator_info = "community_event|".$row1['id'];
							$get_media = mysql_query("select * from general_media where creator_info = '".$creator_info."'");
							if(mysql_num_rows($get_media)>0)
							{
								while($res = mysql_fetch_assoc($get_media))
								{
									$res_media16[] = $res;
								}
							}
						}
					}*/
				}
			}
		   
		}
		   
		$get_all_media = array_merge($res_media1,$res_media2,$res_media3,$res_media4,$res_media5,$res_media6,$res_media7,$res_media8,$res_media9,$res_media10,$res_media11,$res_media12,$res_media13,$res_media14,$res_media15,$res_media16);
		//var_dump($get_all_media);
		return($get_all_media);
	}
	function get_from_heis()
	{
		$id = $this->selgeneral();
        $var_type_1 = 'general_artist';
        $var_type_2 = 'general_community';
        $var_type_3 = 'artist_project';
        $var_type_4 = 'community_project';
		$res_media1 = array();
		$res_media2 = array();
		$res_media3 = array();
		$res_media4 = array();
		$res_media5 = array();
		$res_media6 = array();
		$res_media7 = array();
		$res_media8 = array();
		$res_media9 = array();
		$res_media10 = array();
		$res_media11 = array();
		$res_media12 = array();
		$res_media13 = array();
		$res_media14 = array();
		$res_media15 = array();
		$res_media16 = array();
        if($id['artist_id']!=0 && !empty($id['artist_id']))
        {
			$chk_type = $var_type_1.'|'.$id['artist_id'];
			$sql_get_pro = mysql_query("select * from artist_project where creators_info='".$chk_type."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_get_pro)>0)
			{
				while($row = mysql_fetch_assoc($sql_get_pro))
				{
					$creator_info = "artist_project|".$row['id'];
					$get_media = mysql_query("select * from general_media where from_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
					if(mysql_num_rows($get_media)>0)
					{
						while($res = mysql_fetch_assoc($get_media))
						{
							$res_media1[] = $res;
						}
					}
				}
			}
			$sql_get_pro1 = mysql_query("select * from community_project where creators_info='".$chk_type."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_get_pro1)>0)
			{
				while($row1 = mysql_fetch_assoc($sql_get_pro1))
				{
					$creator_info = "community_project|".$row1['id'];
					$get_media = mysql_query("select * from general_media where from_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
					if(mysql_num_rows($get_media)>0)
					{
						while($res = mysql_fetch_assoc($get_media))
						{
							$res_media2[] = $res;
						}
					}
				}
			}
			$sql_get_pro1 = mysql_query("select * from community_event where creators_info='".$chk_type."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_get_pro1)>0)
			{
				while($row1 = mysql_fetch_assoc($sql_get_pro1))
				{
					$creator_info = "community_event|".$row1['id'];
					$get_media = mysql_query("select * from general_media where from_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
					if(mysql_num_rows($get_media)>0)
					{
						while($res = mysql_fetch_assoc($get_media))
						{
							$res_media3[] = $res;
						}
					}
				}
			}
			$sql_get_pro1 = mysql_query("select * from artist_event where creators_info='".$chk_type."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_get_pro1)>0)
			{
				while($row1 = mysql_fetch_assoc($sql_get_pro1))
				{
					$creator_info = "artist_event|".$row1['id'];
					$get_media = mysql_query("select * from general_media where from_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
					if(mysql_num_rows($get_media)>0)
					{
						while($res = mysql_fetch_assoc($get_media))
						{
							$res_media4[] = $res;
						}
					}
				}
			}
			
			
			$find_project = mysql_query("select * from artist_project where artist_id='".$id['artist_id']."' AND del_status=0 AND active=0");
			if(mysql_num_rows($find_project)>0)
			{
				while($get_all_pro = mysql_fetch_assoc($find_project))
				{
					$new_creator_info = "artist_project|".$get_all_pro['id'];
					//echo "select * from artist_project where creators_info='".$new_creator_info."'";
					$sql_get_pro = mysql_query("select * from artist_project where creators_info='".$new_creator_info."' AND del_status=0 AND active=0");
					if(mysql_num_rows($sql_get_pro)>0)
					{
						while($row = mysql_fetch_assoc($sql_get_pro))
						{
							$creator_info = "artist_project|".$row['id'];
							$get_media = mysql_query("select * from general_media where from_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
							if(mysql_num_rows($get_media)>0)
							{
								while($res = mysql_fetch_assoc($get_media))
								{
									if($id['general_user_id'] != $res['general_user_id'])
									{
										$res_media5[] = $res;	}
								}
							}
						}
					}
					$sql_get_pro1 = mysql_query("select * from community_project where creators_info='".$new_creator_info."' AND del_status=0 AND active=0");
					if(mysql_num_rows($sql_get_pro1)>0)
					{
						while($row1 = mysql_fetch_assoc($sql_get_pro1))
						{
							$creator_info = "community_project|".$row1['id'];
							$get_media = mysql_query("select * from general_media where from_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
							if(mysql_num_rows($get_media)>0)
							{
								while($res = mysql_fetch_assoc($get_media))
								{
									if($id['general_user_id'] != $res['general_user_id'])
									{
										$res_media6[] = $res;}
								}
							}
						}
					}
					$sql_get_pro2 = mysql_query("select * from artist_event where creators_info='".$new_creator_info."' AND del_status=0 AND active=0");
					if(mysql_num_rows($sql_get_pro2)>0)
					{
						while($row = mysql_fetch_assoc($sql_get_pro2))
						{
							$creator_info = "artist_event|".$row['id'];
							$get_media = mysql_query("select * from general_media where from_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
							if(mysql_num_rows($get_media)>0)
							{
								while($res = mysql_fetch_assoc($get_media))
								{
									if($id['general_user_id'] != $res['general_user_id'])
									{
										$res_media7[] = $res;}
								}
							}
						}
					}
					$sql_get_pro3 = mysql_query("select * from community_event where creators_info='".$new_creator_info."' AND del_status=0 AND active=0");
					if(mysql_num_rows($sql_get_pro3)>0)
					{
						while($row1 = mysql_fetch_assoc($sql_get_pro3))
						{
							$creator_info = "community_event|".$row1['id'];
							$get_media = mysql_query("select * from general_media where from_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
							if(mysql_num_rows($get_media)>0)
							{
								while($res = mysql_fetch_assoc($get_media))
								{
									if($id['general_user_id'] != $res['general_user_id'])
									{
										$res_media8[] = $res;}
								}
							}
						}
					}
				}
			}
			$find_project = mysql_query("select * from artist_event where artist_id='".$id['artist_id']."' AND del_status=0 AND active=0");
			if(mysql_num_rows($find_project)>0)
			{
				while($get_all_pro = mysql_fetch_assoc($find_project))
				{
					$new_creator_info = "artist_event|".$get_all_pro['id'];
					//echo "select * from artist_project where creators_info='".$new_creator_info."'";
					$sql_get_pro = mysql_query("select * from artist_project where creators_info='".$new_creator_info."' AND del_status=0 AND active=0");
					if(mysql_num_rows($sql_get_pro)>0)
					{
						while($row = mysql_fetch_assoc($sql_get_pro))
						{
							$creator_info = "artist_project|".$row['id'];
							$get_media = mysql_query("select * from general_media where from_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
							if(mysql_num_rows($get_media)>0)
							{
								while($res = mysql_fetch_assoc($get_media))
								{
									if($id['general_user_id'] != $res['general_user_id'])
									{
										$res_media5[] = $res;	}
								}
							}
						}
					}
					$sql_get_pro1 = mysql_query("select * from community_project where creators_info='".$new_creator_info."' AND del_status=0 AND active=0");
					if(mysql_num_rows($sql_get_pro1)>0)
					{
						while($row1 = mysql_fetch_assoc($sql_get_pro1))
						{
							$creator_info = "community_project|".$row1['id'];
							$get_media = mysql_query("select * from general_media where from_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
							if(mysql_num_rows($get_media)>0)
							{
								while($res = mysql_fetch_assoc($get_media))
								{
									if($id['general_user_id'] != $res['general_user_id'])
									{
										$res_media6[] = $res;}
								}
							}
						}
					}
					$sql_get_pro2 = mysql_query("select * from artist_event where creators_info='".$new_creator_info."' AND del_status=0 AND active=0");
					if(mysql_num_rows($sql_get_pro2)>0)
					{
						while($row = mysql_fetch_assoc($sql_get_pro2))
						{
							$creator_info = "artist_event|".$row['id'];
							$get_media = mysql_query("select * from general_media where from_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
							if(mysql_num_rows($get_media)>0)
							{
								while($res = mysql_fetch_assoc($get_media))
								{
									if($id['general_user_id'] != $res['general_user_id'])
									{
										$res_media7[] = $res;}
								}
							}
						}
					}
					$sql_get_pro3 = mysql_query("select * from community_event where creators_info='".$new_creator_info."' AND del_status=0 AND active=0");
					if(mysql_num_rows($sql_get_pro3)>0)
					{
						while($row1 = mysql_fetch_assoc($sql_get_pro3))
						{
							$creator_info = "community_event|".$row1['id'];
							$get_media = mysql_query("select * from general_media where from_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
							if(mysql_num_rows($get_media)>0)
							{
								while($res = mysql_fetch_assoc($get_media))
								{
									if($id['general_user_id'] != $res['general_user_id'])
									{
										$res_media8[] = $res;}
								}
							}
						}
					}
				}
			}
		   
		}
		if($id['community_id']!=0 && !empty($id['community_id']))
        {
		
			$chk_type = $var_type_2.'|'.$id['community_id'];
			$sql_get_pro = mysql_query("select * from artist_project where creators_info='".$chk_type."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_get_pro)>0)
			{
				while($row = mysql_fetch_assoc($sql_get_pro))
				{
					$creator_info = "artist_project|".$row['id'];
					$get_media = mysql_query("select * from general_media where from_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
					if(mysql_num_rows($get_media)>0)
					{
						while($res = mysql_fetch_assoc($get_media))
						{
							$res_media9[] = $res;
						}
					}
				}
			}
			$sql_get_pro1 = mysql_query("select * from community_project where creators_info='".$chk_type."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_get_pro1)>0)
			{
				while($row1 = mysql_fetch_assoc($sql_get_pro1))
				{
					$creator_info = "community_project|".$row1['id'];
					$get_media = mysql_query("select * from general_media where from_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
					if(mysql_num_rows($get_media)>0)
					{
						while($res = mysql_fetch_assoc($get_media))
						{
							$res_media10[] = $res;
						}
					}
				}
			}
			$sql_get_pro1 = mysql_query("select * from community_event where creators_info='".$chk_type."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_get_pro1)>0)
			{
				while($row1 = mysql_fetch_assoc($sql_get_pro1))
				{
					$creator_info = "community_event|".$row1['id'];
					$get_media = mysql_query("select * from general_media where from_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
					if(mysql_num_rows($get_media)>0)
					{
						while($res = mysql_fetch_assoc($get_media))
						{
							$res_media11[] = $res;
						}
					}
				}
			}
			$sql_get_pro1 = mysql_query("select * from artist_event where creators_info='".$chk_type."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_get_pro1)>0)
			{
				while($row1 = mysql_fetch_assoc($sql_get_pro1))
				{
					$creator_info = "artist_event|".$row1['id'];
					$get_media = mysql_query("select * from general_media where from_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
					if(mysql_num_rows($get_media)>0)
					{
						while($res = mysql_fetch_assoc($get_media))
						{
							$res_media12[] = $res;
						}
					}
				}
			}
		
			$find_project = mysql_query("select * from community_project where community_id='".$id['community_id']."' AND del_status=0 AND active=0");
			if(mysql_num_rows($find_project)>0)
			{
				while($get_all_pro = mysql_fetch_assoc($find_project))
				{
					$new_creator_info = "community_project|".$get_all_pro['id'];
					$sql_get_pro = mysql_query("select * from artist_project where creators_info='".$new_creator_info."' AND del_status=0 AND active=0");
					if(mysql_num_rows($sql_get_pro)>0)
					{
						while($row = mysql_fetch_assoc($sql_get_pro))
						{
							$creator_info = "artist_project|".$row['id'];
							$get_media = mysql_query("select * from general_media where from_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
							if(mysql_num_rows($get_media)>0)
							{
								while($res = mysql_fetch_assoc($get_media))
								{
									if($id['general_user_id'] != $res['general_user_id'])
									{
										$res_media13[] = $res;}
								}
							}
						}
					}
					$sql_get_pro1 = mysql_query("select * from community_project where creators_info='".$new_creator_info."' AND del_status=0 AND active=0");
					if(mysql_num_rows($sql_get_pro1)>0)
					{
						while($row1 = mysql_fetch_assoc($sql_get_pro1))
						{
							$creator_info = "community_project|".$row1['id'];
							$get_media = mysql_query("select * from general_media where from_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
							if(mysql_num_rows($get_media)>0)
							{
								while($res = mysql_fetch_assoc($get_media))
								{
									if($id['general_user_id'] != $res['general_user_id'])
									{
										$res_media14[] = $res;}
								}
							}
						}
					}
					$sql_get_pro2 = mysql_query("select * from artist_event where creators_info='".$new_creator_info."' AND del_status=0 AND active=0");
					if(mysql_num_rows($sql_get_pro2)>0)
					{
						while($row = mysql_fetch_assoc($sql_get_pro2))
						{
							$creator_info = "artist_event|".$row['id'];
							$get_media = mysql_query("select * from general_media where from_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
							if(mysql_num_rows($get_media)>0)
							{
								while($res = mysql_fetch_assoc($get_media))
								{
									if($id['general_user_id'] != $res['general_user_id'])
									{
										$res_media15[] = $res;}
								}
							}
						}
					}
					$sql_get_pro3 = mysql_query("select * from community_event where creators_info='".$new_creator_info."' AND del_status=0 AND active=0");
					if(mysql_num_rows($sql_get_pro3)>0)
					{
						while($row1 = mysql_fetch_assoc($sql_get_pro3))
						{
							$creator_info = "community_event|".$row1['id'];
							$get_media = mysql_query("select * from general_media where from_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
							if(mysql_num_rows($get_media)>0)
							{
								while($res = mysql_fetch_assoc($get_media))
								{
									if($id['general_user_id'] != $res['general_user_id'])
									{
										$res_media16[] = $res;}
								}
							}
						}
					}
				}
			}
			$find_project = mysql_query("select * from community_event where community_id='".$id['community_id']."' AND del_status=0 AND active=0");
			if(mysql_num_rows($find_project)>0)
			{
				while($get_all_pro = mysql_fetch_assoc($find_project))
				{
					$new_creator_info = "community_event|".$get_all_pro['id'];
					$sql_get_pro = mysql_query("select * from artist_project where creators_info='".$new_creator_info."' AND del_status=0 AND active=0");
					if(mysql_num_rows($sql_get_pro)>0)
					{
						while($row = mysql_fetch_assoc($sql_get_pro))
						{
							$creator_info = "artist_project|".$row['id'];
							$get_media = mysql_query("select * from general_media where from_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
							if(mysql_num_rows($get_media)>0)
							{
								while($res = mysql_fetch_assoc($get_media))
								{
									if($id['general_user_id'] != $res['general_user_id'])
									{
										$res_media13[] = $res;}
								}
							}
						}
					}
					$sql_get_pro1 = mysql_query("select * from community_project where creators_info='".$new_creator_info."' AND del_status=0 AND active=0");
					if(mysql_num_rows($sql_get_pro1)>0)
					{
						while($row1 = mysql_fetch_assoc($sql_get_pro1))
						{
							$creator_info = "community_project|".$row1['id'];
							$get_media = mysql_query("select * from general_media where from_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
							if(mysql_num_rows($get_media)>0)
							{
								while($res = mysql_fetch_assoc($get_media))
								{
									if($id['general_user_id'] != $res['general_user_id'])
									{
										$res_media14[] = $res;}
								}
							}
						}
					}
					$sql_get_pro2 = mysql_query("select * from artist_event where creators_info='".$new_creator_info."' AND del_status=0 AND active=0");
					if(mysql_num_rows($sql_get_pro2)>0)
					{
						while($row = mysql_fetch_assoc($sql_get_pro2))
						{
							$creator_info = "artist_event|".$row['id'];
							$get_media = mysql_query("select * from general_media where from_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
							if(mysql_num_rows($get_media)>0)
							{
								while($res = mysql_fetch_assoc($get_media))
								{
									if($id['general_user_id'] != $res['general_user_id'])
									{
										$res_media15[] = $res;}
								}
							}
						}
					}
					$sql_get_pro3 = mysql_query("select * from community_event where creators_info='".$new_creator_info."' AND del_status=0 AND active=0");
					if(mysql_num_rows($sql_get_pro3)>0)
					{
						while($row1 = mysql_fetch_assoc($sql_get_pro3))
						{
							$creator_info = "community_event|".$row1['id'];
							$get_media = mysql_query("select * from general_media where from_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
							if(mysql_num_rows($get_media)>0)
							{
								while($res = mysql_fetch_assoc($get_media))
								{
									if($id['general_user_id'] != $res['general_user_id'])
									{
										$res_media16[] = $res;}
								}
							}
						}
					}
				}
			}
		   
		}
		   
		$get_all_media = array_merge($res_media1,$res_media2,$res_media3,$res_media4,$res_media5,$res_media6,$res_media7,$res_media8,$res_media9,$res_media10,$res_media11,$res_media12,$res_media13,$res_media14,$res_media15,$res_media16);
		return($get_all_media);
	}
	/*
	old function
	function get_from_heis()
	{
		$id = $this->selgeneral();
        $var_type_1 = 'general_artist';
        $var_type_2 = 'general_community';
        $var_type_3 = 'artist_project';
        $var_type_4 = 'community_project';
		$res_media1 = array();
		$res_media2 = array();
		$res_media3 = array();
		$res_media4 = array();
		$res_media5 = array();
		$res_media6 = array();
		$res_media7 = array();
		$res_media8 = array();
		$res_media9 = array();
		$res_media10 = array();
		$res_media11 = array();
		$res_media12 = array();
		$res_media13 = array();
		$res_media14 = array();
		$res_media15 = array();
		$res_media16 = array();
        if($id['artist_id']!=0 && !empty($id['artist_id']))
        {
			$find_project = mysql_query("select * from artist_project where artist_id='".$id['artist_id']."'");
			if(mysql_num_rows($find_project)>0)
			{
				while($get_all_pro = mysql_fetch_assoc($find_project))
				{
					$new_creator_info = "artist_project|".$get_all_pro['id'];
					$sql_get_pro = mysql_query("select * from artist_project where creators_info='".$new_creator_info."'");
					if(mysql_num_rows($sql_get_pro)>0)
					{
						while($row = mysql_fetch_assoc($sql_get_pro))
						{
							$creator_info = "artist_project|".$row['id'];
							$get_media = mysql_query("select * from general_media where from_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
							if(mysql_num_rows($get_media)>0)
							{
								while($res = mysql_fetch_assoc($get_media))
								{
									$res_media5[] = $res;
								}
							}
						}
					}
					$sql_get_pro1 = mysql_query("select * from community_project where creators_info='".$new_creator_info."'");
					if(mysql_num_rows($sql_get_pro1)>0)
					{
						while($row1 = mysql_fetch_assoc($sql_get_pro1))
						{
							$creator_info = "community_project|".$row1['id'];
							$get_media = mysql_query("select * from general_media where from_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
							if(mysql_num_rows($get_media)>0)
							{
								while($res = mysql_fetch_assoc($get_media))
								{
									$res_media6[] = $res;
								}
							}
						}
					}
					$sql_get_pro2 = mysql_query("select * from artist_event where creators_info='".$new_creator_info."'");
					if(mysql_num_rows($sql_get_pro2)>0)
					{
						while($row = mysql_fetch_assoc($sql_get_pro2))
						{
							$creator_info = "artist_event|".$row['id'];
							$get_media = mysql_query("select * from general_media where from_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
							if(mysql_num_rows($get_media)>0)
							{
								while($res = mysql_fetch_assoc($get_media))
								{
									$res_media7[] = $res;
								}
							}
						}
					}
					$sql_get_pro3 = mysql_query("select * from community_event where creators_info='".$new_creator_info."'");
					if(mysql_num_rows($sql_get_pro3)>0)
					{
						while($row1 = mysql_fetch_assoc($sql_get_pro3))
						{
							$creator_info = "community_event|".$row1['id'];
							$get_media = mysql_query("select * from general_media where from_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
							if(mysql_num_rows($get_media)>0)
							{
								while($res = mysql_fetch_assoc($get_media))
								{
									$res_media8[] = $res;
								}
							}
						}
					}
				}
			}
		   
		}
		if($id['community_id']!=0 && !empty($id['community_id']))
        {
			$find_project = mysql_query("select * from community_project where community_id='".$id['community_id']."'");
			if(mysql_num_rows($find_project)>0)
			{
				while($get_all_pro = mysql_fetch_assoc($find_project))
				{
					$new_creator_info = "community_project|".$get_all_pro['id'];
					$sql_get_pro = mysql_query("select * from artist_project where creators_info='".$new_creator_info."'");
					if(mysql_num_rows($sql_get_pro)>0)
					{
						while($row = mysql_fetch_assoc($sql_get_pro))
						{
							$creator_info = "artist_project|".$row['id'];
							$get_media = mysql_query("select * from general_media where from_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
							if(mysql_num_rows($get_media)>0)
							{
								while($res = mysql_fetch_assoc($get_media))
								{
									$res_media13[] = $res;
								}
							}
						}
					}
					$sql_get_pro1 = mysql_query("select * from community_project where creators_info='".$new_creator_info."'");
					if(mysql_num_rows($sql_get_pro1)>0)
					{
						while($row1 = mysql_fetch_assoc($sql_get_pro1))
						{
							$creator_info = "community_project|".$row1['id'];
							$get_media = mysql_query("select * from general_media where from_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
							if(mysql_num_rows($get_media)>0)
							{
								while($res = mysql_fetch_assoc($get_media))
								{
									$res_media14[] = $res;
								}
							}
						}
					}
					$sql_get_pro2 = mysql_query("select * from artist_event where creators_info='".$new_creator_info."'");
					if(mysql_num_rows($sql_get_pro2)>0)
					{
						while($row = mysql_fetch_assoc($sql_get_pro2))
						{
							$creator_info = "artist_event|".$row['id'];
							$get_media = mysql_query("select * from general_media where from_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
							if(mysql_num_rows($get_media)>0)
							{
								while($res = mysql_fetch_assoc($get_media))
								{
									$res_media15[] = $res;
								}
							}
						}
					}
					$sql_get_pro3 = mysql_query("select * from community_event where creators_info='".$new_creator_info."'");
					if(mysql_num_rows($sql_get_pro3)>0)
					{
						while($row1 = mysql_fetch_assoc($sql_get_pro3))
						{
							$creator_info = "community_event|".$row1['id'];
							$get_media = mysql_query("select * from general_media where from_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
							if(mysql_num_rows($get_media)>0)
							{
								while($res = mysql_fetch_assoc($get_media))
								{
									$res_media16[] = $res;
								}
							}
						}
					}
				}
			}
		   
		}
		   
		$get_all_media = array_merge($res_media5,$res_media6,$res_media7,$res_media8,$res_media13,$res_media14,$res_media15,$res_media16);
		return($get_all_media);
	}*/
	function get_the_user_tagin_project($email)
	{
		$id = $this->selgeneral();
        $var_type_1 = 'general_artist';
        $var_type_2 = 'general_community';
        $var_type_3 = 'artist_project';
        $var_type_4 = 'community_project';
		$res_media1 = array();
		$res_media2 = array();
		$res_media3 = array();
		$res_media4 = array();

		$get_all_art_project = mysql_query("SELECT * FROM artist_project WHERE tagged_user_email LIKE '%".$email."%' AND del_status=0 AND active=0");
		if(mysql_num_rows($get_all_art_project)>0)
		{
			while($res_data = mysql_fetch_assoc($get_all_art_project))
			{
				$creator_info = "artist_project|".$res_data['id'];
				$get_media = mysql_query("select * from general_media where creator_info ='".$creator_info."' OR from_info ='".$creator_info."' AND delete_status=0 AND media_status=0");
				if(mysql_num_rows($get_media)>0)
				{
					while($res_media_data = mysql_fetch_assoc($get_media))
					{
						$res_media1[] = $res_media_data;
					}
				}
			}
		}
		$get_all_art_event = mysql_query("SELECT * FROM artist_event WHERE tagged_user_email LIKE '%".$email."%' AND del_status=0 AND active=0");
		if(mysql_num_rows($get_all_art_event)>0)
		{
			while($res_data = mysql_fetch_assoc($get_all_art_event))
			{
				$creator_info = "artist_event|".$res_data['id'];
				$get_media = mysql_query("select * from general_media where creator_info ='".$creator_info."' OR from_info ='".$creator_info."' AND delete_status=0 AND media_status=0");
				if(mysql_num_rows($get_media)>0)
				{
					while($res_media_data = mysql_fetch_assoc($get_media))
					{
						$res_media2[] = $res_media_data;
					}
				}
			}
		}
		$get_all_com_event = mysql_query("SELECT * FROM community_event WHERE tagged_user_email LIKE '%".$email."%' AND del_status=0 AND active=0");
		if(mysql_num_rows($get_all_com_event)>0)
		{
			while($res_data = mysql_fetch_assoc($get_all_com_event))
			{
				$creator_info = "community_event|".$res_data['id'];
				$get_media = mysql_query("select * from general_media where creator_info ='".$creator_info."' OR from_info ='".$creator_info."' AND delete_status=0 AND media_status=0");
				if(mysql_num_rows($get_media)>0)
				{
					while($res_media_data = mysql_fetch_assoc($get_media))
					{
						$res_media3[] = $res_media_data;
					}
				}
			}
		}
		$get_all_com_project = mysql_query("SELECT * FROM community_project WHERE tagged_user_email LIKE '%".$email."%' AND del_status=0 AND active=0");
		if(mysql_num_rows($get_all_com_project)>0)
		{
			while($res_data = mysql_fetch_assoc($get_all_com_project))
			{
				$creator_info = "community_project|".$res_data['id'];
				$get_media = mysql_query("select * from general_media where creator_info ='".$creator_info."' OR from_info ='".$creator_info."' AND delete_status=0 AND media_status=0");
				if(mysql_num_rows($get_media)>0)
				{
					while($res_media_data = mysql_fetch_assoc($get_media))
					{
						$res_media4[] = $res_media_data;
					}
				}
			}
		}
	   
		$get_all_media = array_merge($res_media1,$res_media2,$res_media3,$res_media4);
		//var_dump($get_all_media);
		return($get_all_media);
	}
	function get_all_del_media_id()
	{
		$all_deleted_id = "";
		$sql = mysql_query("select * from general_user where email='".$_SESSION['login_email']."'");
		$res = mysql_fetch_assoc($sql);
		$all_deleted_id = $res['deleted_media_by_tag_user'];
		if($res['artist_id'] >0)
		{
			$get_art = mysql_query("select * from general_artist where artist_id='".$res['artist_id']."'");
			if(mysql_num_rows($get_art)>0)
			{
				$res_art = mysql_fetch_assoc($get_art);
				if($res_art['deleted_media_by_creator_creator'] !="" || $res_art['deleted_media_by_from_from']!=""){
				$all_deleted_id = $all_deleted_id ."," . $res_art['deleted_media_by_creator_creator'].",".$res_art['deleted_media_by_from_from'];}
			}
			$get_art_pro = mysql_query("select * from artist_project where artist_id='".$res['artist_id']."'");
			if(mysql_num_rows($get_art_pro)>0)
			{
				while($res_art_pro = mysql_fetch_assoc($get_art_pro))
				{
					if($res_art_pro['deleted_media_by_creator_creator'] !="" || $res_art_pro['deleted_media_by_from_from'] !=""){
					$all_deleted_id = $all_deleted_id ."," . $res_art_pro['deleted_media_by_creator_creator'].",".$res_art_pro['deleted_media_by_from_from'];}
				}
			}
			$get_art_eve = mysql_query("select * from artist_event where artist_id='".$res['artist_id']."'");
			if(mysql_num_rows($get_art_eve)>0)
			{
				while($res_art_eve = mysql_fetch_assoc($get_art_eve))
				{
					if($res_art_eve['deleted_media_by_creator_creator'] !="" || $res_art_eve['deleted_media_by_from_from']!=""){
					$all_deleted_id = $all_deleted_id ."," . $res_art_eve['deleted_media_by_creator_creator'].",".$res_art_eve['deleted_media_by_from_from'];}
				}
			}
		}
		if($res['community_id'] >0)
		{
			$get_com = mysql_query("select * from general_community where community_id='".$res['community_id']."'");
			if(mysql_num_rows($get_com)>0)
			{
				$res_com = mysql_fetch_assoc($get_com);
				if($res_com['deleted_media_by_creator_creator'] !="" || $res_com['deleted_media_by_from_from'] !=""){
				$all_deleted_id = $all_deleted_id ."," . $res_com['deleted_media_by_creator_creator'].",".$res_com['deleted_media_by_from_from'];}
			}
			$get_com_pro = mysql_query("select * from community_project where community_id='".$res['community_id']."'");
			if(mysql_num_rows($get_com_pro)>0)
			{
				while($res_com_pro = mysql_fetch_assoc($get_com_pro))
				{
					if($res_com_pro['deleted_media_by_creator_creator'] !="" || $res_com_pro['deleted_media_by_from_from'] !=""){
					$all_deleted_id = $all_deleted_id ."," . $res_com_pro['deleted_media_by_creator_creator'].",".$res_com_pro['deleted_media_by_from_from'];}
				}
			}
			$get_com_eve = mysql_query("select * from community_event where community_id='".$res['community_id']."'");
			if(mysql_num_rows($get_com_eve)>0)
			{
				while($res_com_eve = mysql_fetch_assoc($get_com_eve))
				{
					if($res_com_eve['deleted_media_by_creator_creator'] !="" || $res_com_eve['deleted_media_by_from_from'] !=""){
					$all_deleted_id = $all_deleted_id ."," . $res_com_eve['deleted_media_by_creator_creator'].",".$res_com_eve['deleted_media_by_from_from'];}
				}
			}
		}
		return($all_deleted_id);
	}
	
	function delete_where_project_is_creator($creator_info,$pro_id)
	{
		/*get deleting project media*/
		$get_media = mysql_query("select * from general_media where creator_info='".$creator_info."' OR from_info='".$creator_info."'");
		if(mysql_num_rows($get_media)>0)
		{
			while($res_media = mysql_fetch_assoc($get_media))
			{
				$all_media_id = $all_media_id .",". $res_media['id'];
			}
		}
		$exp_all_media_id = explode(",",$all_media_id);
		/*get login user detail*/
		$get_gen_det = mysql_query("select * from general_user where email='".$_SESSION['login_email']."'");
		{
			if(mysql_num_rows($get_gen_det)>0)
			{
				$res_gen_det = mysql_fetch_assoc($get_gen_det);
				$exp_deleted_id = explode(",",$res_gen_det['deleted_media_by_tag_user']);
				for($count_deleted=0;$count_deleted<=count($exp_deleted_id);$count_deleted++)
				{
					if($exp_deleted_id[$count_deleted]==""){
					continue;
					}else{
						for($count_media=0;$count_media<=count($exp_all_media_id);$count_media++)
						{
							if($exp_all_media_id[$count_media]==""){ continue; }else{
								if($exp_all_media_id[$count_media]==$exp_deleted_id[$count_deleted]){
									$exp_deleted_id[$count_deleted] ="";
								}
							}
						}
					}
				}
				$new_deleted_id="";
				for($count_new=0;$count_new<=count($exp_deleted_id);$count_new++)
				{
					if($exp_deleted_id[$count_new]!="")
					{
						$new_deleted_id = $new_deleted_id .",". $exp_deleted_id[$count_new];
					}
				}
				mysql_query("update general_user set deleted_media_by_tag_user='".$new_deleted_id."' where email='".$_SESSION['login_email']."'");
				if($res_gen_det['artist_id']>0)
				{
					$get_art_det = mysql_query("select * from general_artist where artist_id='".$res_gen_det['artist_id']."'");
					if(mysql_num_rows($get_art_det)>0)
					{
						$res_art_det = mysql_fetch_assoc($get_art_det);
						$exp_song_id = explode(",",$res_art_det['taggedsongs']);
						$exp_video_id = explode(",",$res_art_det['taggedvideos']);
						$exp_gallery_id = explode(",",$res_art_det['taggedgalleries']);
						$exp_deleted_cre_id = explode(",",$res_art_det['deleted_media_by_creator_creator']);
						$exp_deleted_fro_id = explode(",",$res_art_det['deleted_media_by_from_from']);
						for($count_song=0;$count_song<=count($exp_song_id);$count_song++){
							if($exp_song_id[$count_song]!=""){
								for($count_media=0;$count_media<=count($exp_all_media_id);$count_media++){
									if($exp_all_media_id[$count_media]!=""){
										if($exp_all_media_id[$count_media] == $exp_song_id[$count_song]){
											$exp_song_id[$count_song] ="";
										}
									}
								}
							}
						}
						$new_song_id ="";
						for($count_new_song=0;$count_new_song<=count($exp_song_id);$count_new_song++){
							if($exp_song_id[$count_new_song]!=""){
								$new_song_id = $new_song_id .",".$exp_song_id[$count_new_song];
							}
						}
						for($count_video=0;$count_video<=count($exp_video_id);$count_video++){
							if($exp_video_id[$count_video]!=""){
								for($count_media=0;$count_media<=count($exp_all_media_id);$count_media++){
									if($exp_all_media_id[$count_media]!=""){
										if($exp_all_media_id[$count_media] == $exp_video_id[$count_video]){
											$exp_video_id[$count_video] ="";
										}
									}
								}
							}
						}
						$new_video_id ="";
						for($count_new_video=0;$count_new_video<=count($exp_video_id);$count_new_video++){
							if($exp_video_id[$count_new_video]!=""){
								$new_video_id = $new_video_id .",".$exp_video_id[$count_new_video];
							}
						}
						for($count_gal=0;$count_gal<=count($exp_gallery_id);$count_gal++){
							if($exp_gallery_id[$count_gal]!=""){
								for($count_media=0;$count_media<=count($exp_all_media_id);$count_media++){
									if($exp_all_media_id[$count_media]!=""){
										if($exp_all_media_id[$count_media] == $exp_gallery_id[$count_gal]){
											$exp_gallery_id[$count_gal] ="";
										}
									}
								}
							}
						}
						$new_gal_id ="";
						for($count_new_gal=0;$count_new_gal<=count($exp_gallery_id);$count_new_gal++){
							if($exp_gallery_id[$count_new_gal]!=""){
								$new_gal_id = $new_gal_id .",".$exp_gallery_id[$count_new_gal];
							}
						}
						for($count_del_cre=0;$count_del_cre<=count($exp_deleted_cre_id);$count_del_cre++){
							if($exp_deleted_cre_id[$count_del_cre]!=""){
								for($count_media=0;$count_media<=count($exp_all_media_id);$count_media++){
									if($exp_all_media_id[$count_media]!=""){
										if($exp_all_media_id[$count_media] == $exp_deleted_cre_id[$count_del_cre]){
											$exp_deleted_cre_id[$count_del_cre] ="";
										}
									}
								}
							}
						}
						$new_del_cre_id ="";
						for($count_new_del_cre=0;$count_new_del_cre<=count($exp_deleted_cre_id);$count_new_del_cre++){
							if($exp_deleted_cre_id[$count_new_del_cre]!=""){
								$new_del_cre_id = $new_del_cre_id .",".$exp_deleted_cre_id[$count_new_del_cre];
							}
						}
						for($count_del_fro=0;$count_del_fro<=count($exp_deleted_fro_id);$count_del_fro++){
							if($exp_deleted_fro_id[$count_del_fro]!=""){
								for($count_media=0;$count_media<=count($exp_all_media_id);$count_media++){
									if($exp_all_media_id[$count_media]!=""){
										if($exp_all_media_id[$count_media] == $exp_deleted_fro_id[$count_del_fro]){
											$exp_deleted_fro_id[$count_del_fro] ="";
										}
									}
								}
							}
						}
						$new_del_fro_id ="";
						for($count_new_del_fro=0;$count_new_del_fro<=count($exp_deleted_fro_id);$count_new_del_fro++){
							if($exp_deleted_cre_id[$count_new_del_fro]!=""){
								$new_del_fro_id = $new_del_fro_id .",".$exp_deleted_cre_id[$count_new_del_fro];
							}
						}
						
						mysql_query("update general_artist set taggedsongs ='".$new_song_id."', taggedvideos ='".$new_video_id."', taggedgalleries ='".$new_gal_id."', deleted_media_by_creator_creator ='".$new_del_cre_id."', deleted_media_by_from_from='".$new_del_fro_id."' where artist_id='".$res_art_det['artist_id']."'");
					}
					$get_artpro_det = mysql_query("select * from artist_project where artist_id='".$res_gen_det['artist_id']."'");
					if(mysql_num_rows($get_artpro_det)>0)
					{
						while($res_artpro_det = mysql_fetch_assoc($get_artpro_det))
						{
							$exp_song_id = explode(",",$res_artpro_det['tagged_songs']);
							$exp_video_id = explode(",",$res_artpro_det['tagged_videos']);
							$exp_gallery_id = explode(",",$res_artpro_det['tagged_galleries']);
							$exp_deleted_cre_id = explode(",",$res_artpro_det['deleted_media_by_creator_creator']);
							$exp_deleted_fro_id = explode(",",$res_artpro_det['deleted_media_by_from_from']);
							for($count_song=0;$count_song<=count($exp_song_id);$count_song++){
								if($exp_song_id[$count_song]!=""){
									for($count_media=0;$count_media<=count($exp_all_media_id);$count_media++){
										if($exp_all_media_id[$count_media]!=""){
											if($exp_all_media_id[$count_media] == $exp_song_id[$count_song]){
												$exp_song_id[$count_song] ="";
											}
										}
									}
								}
							}
							$new_song_id ="";
							for($count_new_song=0;$count_new_song<=count($exp_song_id);$count_new_song++){
								if($exp_song_id[$count_new_song]!=""){
									$new_song_id = $new_song_id .",".$exp_song_id[$count_new_song];
								}
							}
							for($count_video=0;$count_video<=count($exp_video_id);$count_video++){
								if($exp_video_id[$count_video]!=""){
									for($count_media=0;$count_media<=count($exp_all_media_id);$count_media++){
										if($exp_all_media_id[$count_media]!=""){
											if($exp_all_media_id[$count_media] == $exp_video_id[$count_video]){
												$exp_video_id[$count_video] ="";
											}
										}
									}
								}
							}
							$new_video_id ="";
							for($count_new_video=0;$count_new_video<=count($exp_video_id);$count_new_video++){
								if($exp_video_id[$count_new_video]!=""){
									$new_video_id = $new_video_id .",".$exp_video_id[$count_new_video];
								}
							}
							for($count_gal=0;$count_gal<=count($exp_gallery_id);$count_gal++){
								if($exp_gallery_id[$count_gal]!=""){
									for($count_media=0;$count_media<=count($exp_all_media_id);$count_media++){
										if($exp_all_media_id[$count_media]!=""){
											if($exp_all_media_id[$count_media] == $exp_gallery_id[$count_gal]){
												$exp_gallery_id[$count_gal] ="";
											}
										}
									}
								}
							}
							$new_gal_id ="";
							for($count_new_gal=0;$count_new_gal<=count($exp_gallery_id);$count_new_gal++){
								if($exp_gallery_id[$count_new_gal]!=""){
									$new_gal_id = $new_gal_id .",".$exp_gallery_id[$count_new_gal];
								}
							}
							for($count_del_cre=0;$count_del_cre<=count($exp_deleted_cre_id);$count_del_cre++){
								if($exp_deleted_cre_id[$count_del_cre]!=""){
									for($count_media=0;$count_media<=count($exp_all_media_id);$count_media++){
										if($exp_all_media_id[$count_media]!=""){
											if($exp_all_media_id[$count_media] == $exp_deleted_cre_id[$count_del_cre]){
												$exp_deleted_cre_id[$count_del_cre] ="";
											}
										}
									}
								}
							}
							$new_del_cre_id ="";
							for($count_new_del_cre=0;$count_new_del_cre<=count($exp_deleted_cre_id);$count_new_del_cre++){
								if($exp_deleted_cre_id[$count_new_del_cre]!=""){
									$new_del_cre_id = $new_del_cre_id .",".$exp_deleted_cre_id[$count_new_del_cre];
								}
							}
							for($count_del_fro=0;$count_del_fro<=count($exp_deleted_fro_id);$count_del_fro++){
								if($exp_deleted_fro_id[$count_del_fro]!=""){
									for($count_media=0;$count_media<=count($exp_all_media_id);$count_media++){
										if($exp_all_media_id[$count_media]!=""){
											if($exp_all_media_id[$count_media] == $exp_deleted_fro_id[$count_del_fro]){
												$exp_deleted_fro_id[$count_del_fro] ="";
											}
										}
									}
								}
							}
							$new_del_fro_id ="";
							for($count_new_del_fro=0;$count_new_del_fro<=count($exp_deleted_fro_id);$count_new_del_fro++){
								if($exp_deleted_cre_id[$count_new_del_fro]!=""){
									$new_del_fro_id = $new_del_fro_id .",".$exp_deleted_cre_id[$count_new_del_fro];
								}
							}
							
							mysql_query("update artist_project set tagged_songs ='".$new_song_id."', tagged_videos ='".$new_video_id."', tagged_galleries ='".$new_gal_id."', deleted_media_by_creator_creator ='".$new_del_cre_id."', deleted_media_by_from_from='".$new_del_fro_id."' where id='".$res_artpro_det['id']."'");
						}
					}
					$get_arteve_det = mysql_query("select * from artist_event where artist_id='".$res_gen_det['artist_id']."'");
					if(mysql_num_rows($get_arteve_det)>0)
					{
						while($res_arteve_det = mysql_fetch_assoc($get_arteve_det))
						{
							$exp_song_id = explode(",",$res_arteve_det['tagged_songs']);
							$exp_video_id = explode(",",$res_arteve_det['tagged_videos']);
							$exp_gallery_id = explode(",",$res_arteve_det['tagged_galleries']);
							$exp_deleted_cre_id = explode(",",$res_arteve_det['deleted_media_by_creator_creator']);
							$exp_deleted_fro_id = explode(",",$res_arteve_det['deleted_media_by_from_from']);
							for($count_song=0;$count_song<=count($exp_song_id);$count_song++){
								if($exp_song_id[$count_song]!=""){
									for($count_media=0;$count_media<=count($exp_all_media_id);$count_media++){
										if($exp_all_media_id[$count_media]!=""){
											if($exp_all_media_id[$count_media] == $exp_song_id[$count_song]){
												$exp_song_id[$count_song] ="";
											}
										}
									}
								}
							}
							$new_song_id ="";
							for($count_new_song=0;$count_new_song<=count($exp_song_id);$count_new_song++){
								if($exp_song_id[$count_new_song]!=""){
									$new_song_id = $new_song_id .",".$exp_song_id[$count_new_song];
								}
							}
							for($count_video=0;$count_video<=count($exp_video_id);$count_video++){
								if($exp_video_id[$count_video]!=""){
									for($count_media=0;$count_media<=count($exp_all_media_id);$count_media++){
										if($exp_all_media_id[$count_media]!=""){
											if($exp_all_media_id[$count_media] == $exp_video_id[$count_video]){
												$exp_video_id[$count_video] ="";
											}
										}
									}
								}
							}
							$new_video_id ="";
							for($count_new_video=0;$count_new_video<=count($exp_video_id);$count_new_video++){
								if($exp_video_id[$count_new_video]!=""){
									$new_video_id = $new_video_id .",".$exp_video_id[$count_new_video];
								}
							}
							for($count_gal=0;$count_gal<=count($exp_gallery_id);$count_gal++){
								if($exp_gallery_id[$count_gal]!=""){
									for($count_media=0;$count_media<=count($exp_all_media_id);$count_media++){
										if($exp_all_media_id[$count_media]!=""){
											if($exp_all_media_id[$count_media] == $exp_gallery_id[$count_gal]){
												$exp_gallery_id[$count_gal] ="";
											}
										}
									}
								}
							}
							$new_gal_id ="";
							for($count_new_gal=0;$count_new_gal<=count($exp_gallery_id);$count_new_gal++){
								if($exp_gallery_id[$count_new_gal]!=""){
									$new_gal_id = $new_gal_id .",".$exp_gallery_id[$count_new_gal];
								}
							}
							for($count_del_cre=0;$count_del_cre<=count($exp_deleted_cre_id);$count_del_cre++){
								if($exp_deleted_cre_id[$count_del_cre]!=""){
									for($count_media=0;$count_media<=count($exp_all_media_id);$count_media++){
										if($exp_all_media_id[$count_media]!=""){
											if($exp_all_media_id[$count_media] == $exp_deleted_cre_id[$count_del_cre]){
												$exp_deleted_cre_id[$count_del_cre] ="";
											}
										}
									}
								}
							}
							$new_del_cre_id ="";
							for($count_new_del_cre=0;$count_new_del_cre<=count($exp_deleted_cre_id);$count_new_del_cre++){
								if($exp_deleted_cre_id[$count_new_del_cre]!=""){
									$new_del_cre_id = $new_del_cre_id .",".$exp_deleted_cre_id[$count_new_del_cre];
								}
							}
							for($count_del_fro=0;$count_del_fro<=count($exp_deleted_fro_id);$count_del_fro++){
								if($exp_deleted_fro_id[$count_del_fro]!=""){
									for($count_media=0;$count_media<=count($exp_all_media_id);$count_media++){
										if($exp_all_media_id[$count_media]!=""){
											if($exp_all_media_id[$count_media] == $exp_deleted_fro_id[$count_del_fro]){
												$exp_deleted_fro_id[$count_del_fro] ="";
											}
										}
									}
								}
							}
							$new_del_fro_id ="";
							for($count_new_del_fro=0;$count_new_del_fro<=count($exp_deleted_fro_id);$count_new_del_fro++){
								if($exp_deleted_cre_id[$count_new_del_fro]!=""){
									$new_del_fro_id = $new_del_fro_id .",".$exp_deleted_cre_id[$count_new_del_fro];
								}
							}
							
							mysql_query("update artist_event set tagged_songs ='".$new_song_id."', tagged_videos ='".$new_video_id."', tagged_galleries ='".$new_gal_id."', deleted_media_by_creator_creator ='".$new_del_cre_id."', deleted_media_by_from_from='".$new_del_fro_id."' where id='".$res_arteve_det['id']."'");
						}
					}
				}
				
				if($res_gen_det['community_id']>0)
				{
					$get_com_det = mysql_query("select * from general_community where community_id='".$res_gen_det['community_id']."'");
					if(mysql_num_rows($get_com_det)>0)
					{
						$res_com_det = mysql_fetch_assoc($get_com_det);
						$exp_song_id = explode(",",$res_com_det['taggedsongs']);
						$exp_video_id = explode(",",$res_com_det['taggedvideos']);
						$exp_gallery_id = explode(",",$res_com_det['taggedgalleries']);
						$exp_deleted_cre_id = explode(",",$res_com_det['deleted_media_by_creator_creator']);
						$exp_deleted_fro_id = explode(",",$res_com_det['deleted_media_by_from_from']);
						for($count_song=0;$count_song<=count($exp_song_id);$count_song++){
							if($exp_song_id[$count_song]!=""){
								for($count_media=0;$count_media<=count($exp_all_media_id);$count_media++){
									if($exp_all_media_id[$count_media]!=""){
										if($exp_all_media_id[$count_media] == $exp_song_id[$count_song]){
											$exp_song_id[$count_song] ="";
										}
									}
								}
							}
						}
						$new_song_id ="";
						for($count_new_song=0;$count_new_song<=count($exp_song_id);$count_new_song++){
							if($exp_song_id[$count_new_song]!=""){
								$new_song_id = $new_song_id .",".$exp_song_id[$count_new_song];
							}
						}
						for($count_video=0;$count_video<=count($exp_video_id);$count_video++){
							if($exp_video_id[$count_video]!=""){
								for($count_media=0;$count_media<=count($exp_all_media_id);$count_media++){
									if($exp_all_media_id[$count_media]!=""){
										if($exp_all_media_id[$count_media] == $exp_video_id[$count_video]){
											$exp_video_id[$count_video] ="";
										}
									}
								}
							}
						}
						$new_video_id ="";
						for($count_new_video=0;$count_new_video<=count($exp_video_id);$count_new_video++){
							if($exp_video_id[$count_new_video]!=""){
								$new_video_id = $new_video_id .",".$exp_video_id[$count_new_video];
							}
						}
						for($count_gal=0;$count_gal<=count($exp_gallery_id);$count_gal++){
							if($exp_gallery_id[$count_gal]!=""){
								for($count_media=0;$count_media<=count($exp_all_media_id);$count_media++){
									if($exp_all_media_id[$count_media]!=""){
										if($exp_all_media_id[$count_media] == $exp_gallery_id[$count_gal]){
											$exp_gallery_id[$count_gal] ="";
										}
									}
								}
							}
						}
						$new_gal_id ="";
						for($count_new_gal=0;$count_new_gal<=count($exp_gallery_id);$count_new_gal++){
							if($exp_gallery_id[$count_new_gal]!=""){
								$new_gal_id = $new_gal_id .",".$exp_gallery_id[$count_new_gal];
							}
						}
						for($count_del_cre=0;$count_del_cre<=count($exp_deleted_cre_id);$count_del_cre++){
							if($exp_deleted_cre_id[$count_del_cre]!=""){
								for($count_media=0;$count_media<=count($exp_all_media_id);$count_media++){
									if($exp_all_media_id[$count_media]!=""){
										if($exp_all_media_id[$count_media] == $exp_deleted_cre_id[$count_del_cre]){
											$exp_deleted_cre_id[$count_del_cre] ="";
										}
									}
								}
							}
						}
						$new_del_cre_id ="";
						for($count_new_del_cre=0;$count_new_del_cre<=count($exp_deleted_cre_id);$count_new_del_cre++){
							if($exp_deleted_cre_id[$count_new_del_cre]!=""){
								$new_del_cre_id = $new_del_cre_id .",".$exp_deleted_cre_id[$count_new_del_cre];
							}
						}
						for($count_del_fro=0;$count_del_fro<=count($exp_deleted_fro_id);$count_del_fro++){
							if($exp_deleted_fro_id[$count_del_fro]!=""){
								for($count_media=0;$count_media<=count($exp_all_media_id);$count_media++){
									if($exp_all_media_id[$count_media]!=""){
										if($exp_all_media_id[$count_media] == $exp_deleted_fro_id[$count_del_fro]){
											$exp_deleted_fro_id[$count_del_fro] ="";
										}
									}
								}
							}
						}
						$new_del_fro_id ="";
						for($count_new_del_fro=0;$count_new_del_fro<=count($exp_deleted_fro_id);$count_new_del_fro++){
							if($exp_deleted_cre_id[$count_new_del_fro]!=""){
								$new_del_fro_id = $new_del_fro_id .",".$exp_deleted_cre_id[$count_new_del_fro];
							}
						}
						
						mysql_query("update general_community set taggedsongs ='".$new_song_id."', taggedvideos ='".$new_video_id."', taggedgalleries ='".$new_gal_id."', deleted_media_by_creator_creator ='".$new_del_cre_id."', deleted_media_by_from_from='".$new_del_fro_id."' where community_id='".$res_com_det['community_id']."'");
					}
					$get_compro_det = mysql_query("select * from community_project where community_id='".$res_gen_det['community_id']."'");
					if(mysql_num_rows($get_compro_det)>0)
					{
						while($res_compro_det = mysql_fetch_assoc($get_compro_det))
						{
							$exp_song_id = explode(",",$res_compro_det['tagged_songs']);
							$exp_video_id = explode(",",$res_compro_det['tagged_videos']);
							$exp_gallery_id = explode(",",$res_compro_det['tagged_galleries']);
							$exp_deleted_cre_id = explode(",",$res_compro_det['deleted_media_by_creator_creator']);
							$exp_deleted_fro_id = explode(",",$res_compro_det['deleted_media_by_from_from']);
							for($count_song=0;$count_song<=count($exp_song_id);$count_song++){
								if($exp_song_id[$count_song]!=""){
									for($count_media=0;$count_media<=count($exp_all_media_id);$count_media++){
										if($exp_all_media_id[$count_media]!=""){
											if($exp_all_media_id[$count_media] == $exp_song_id[$count_song]){
												$exp_song_id[$count_song] ="";
											}
										}
									}
								}
							}
							$new_song_id ="";
							for($count_new_song=0;$count_new_song<=count($exp_song_id);$count_new_song++){
								if($exp_song_id[$count_new_song]!=""){
									$new_song_id = $new_song_id .",".$exp_song_id[$count_new_song];
								}
							}
							for($count_video=0;$count_video<=count($exp_video_id);$count_video++){
								if($exp_video_id[$count_video]!=""){
									for($count_media=0;$count_media<=count($exp_all_media_id);$count_media++){
										if($exp_all_media_id[$count_media]!=""){
											if($exp_all_media_id[$count_media] == $exp_video_id[$count_video]){
												$exp_video_id[$count_video] ="";
											}
										}
									}
								}
							}
							$new_video_id ="";
							for($count_new_video=0;$count_new_video<=count($exp_video_id);$count_new_video++){
								if($exp_video_id[$count_new_video]!=""){
									$new_video_id = $new_video_id .",".$exp_video_id[$count_new_video];
								}
							}
							for($count_gal=0;$count_gal<=count($exp_gallery_id);$count_gal++){
								if($exp_gallery_id[$count_gal]!=""){
									for($count_media=0;$count_media<=count($exp_all_media_id);$count_media++){
										if($exp_all_media_id[$count_media]!=""){
											if($exp_all_media_id[$count_media] == $exp_gallery_id[$count_gal]){
												$exp_gallery_id[$count_gal] ="";
											}
										}
									}
								}
							}
							$new_gal_id ="";
							for($count_new_gal=0;$count_new_gal<=count($exp_gallery_id);$count_new_gal++){
								if($exp_gallery_id[$count_new_gal]!=""){
									$new_gal_id = $new_gal_id .",".$exp_gallery_id[$count_new_gal];
								}
							}
							for($count_del_cre=0;$count_del_cre<=count($exp_deleted_cre_id);$count_del_cre++){
								if($exp_deleted_cre_id[$count_del_cre]!=""){
									for($count_media=0;$count_media<=count($exp_all_media_id);$count_media++){
										if($exp_all_media_id[$count_media]!=""){
											if($exp_all_media_id[$count_media] == $exp_deleted_cre_id[$count_del_cre]){
												$exp_deleted_cre_id[$count_del_cre] ="";
											}
										}
									}
								}
							}
							$new_del_cre_id ="";
							for($count_new_del_cre=0;$count_new_del_cre<=count($exp_deleted_cre_id);$count_new_del_cre++){
								if($exp_deleted_cre_id[$count_new_del_cre]!=""){
									$new_del_cre_id = $new_del_cre_id .",".$exp_deleted_cre_id[$count_new_del_cre];
								}
							}
							for($count_del_fro=0;$count_del_fro<=count($exp_deleted_fro_id);$count_del_fro++){
								if($exp_deleted_fro_id[$count_del_fro]!=""){
									for($count_media=0;$count_media<=count($exp_all_media_id);$count_media++){
										if($exp_all_media_id[$count_media]!=""){
											if($exp_all_media_id[$count_media] == $exp_deleted_fro_id[$count_del_fro]){
												$exp_deleted_fro_id[$count_del_fro] ="";
											}
										}
									}
								}
							}
							$new_del_fro_id ="";
							for($count_new_del_fro=0;$count_new_del_fro<=count($exp_deleted_fro_id);$count_new_del_fro++){
								if($exp_deleted_cre_id[$count_new_del_fro]!=""){
									$new_del_fro_id = $new_del_fro_id .",".$exp_deleted_cre_id[$count_new_del_fro];
								}
							}
							
							mysql_query("update community_project set tagged_songs ='".$new_song_id."', tagged_videos ='".$new_video_id."', tagged_galleries ='".$new_gal_id."', deleted_media_by_creator_creator ='".$new_del_cre_id."', deleted_media_by_from_from='".$new_del_fro_id."' where id='".$res_compro_det['id']."'");
						}
					}
					$get_comeve_det = mysql_query("select * from community_event where community_id='".$res_gen_det['community_id']."'");
					if(mysql_num_rows($get_comeve_det)>0)
					{
						while($res_comeve_det = mysql_fetch_assoc($get_comeve_det))
						{
							$exp_song_id = explode(",",$res_comeve_det['tagged_songs']);
							$exp_video_id = explode(",",$res_comeve_det['tagged_videos']);
							$exp_gallery_id = explode(",",$res_comeve_det['tagged_galleries']);
							$exp_deleted_cre_id = explode(",",$res_comeve_det['deleted_media_by_creator_creator']);
							$exp_deleted_fro_id = explode(",",$res_comeve_det['deleted_media_by_from_from']);
							for($count_song=0;$count_song<=count($exp_song_id);$count_song++){
								if($exp_song_id[$count_song]!=""){
									for($count_media=0;$count_media<=count($exp_all_media_id);$count_media++){
										if($exp_all_media_id[$count_media]!=""){
											if($exp_all_media_id[$count_media] == $exp_song_id[$count_song]){
												$exp_song_id[$count_song] ="";
											}
										}
									}
								}
							}
							$new_song_id ="";
							for($count_new_song=0;$count_new_song<=count($exp_song_id);$count_new_song++){
								if($exp_song_id[$count_new_song]!=""){
									$new_song_id = $new_song_id .",".$exp_song_id[$count_new_song];
								}
							}
							for($count_video=0;$count_video<=count($exp_video_id);$count_video++){
								if($exp_video_id[$count_video]!=""){
									for($count_media=0;$count_media<=count($exp_all_media_id);$count_media++){
										if($exp_all_media_id[$count_media]!=""){
											if($exp_all_media_id[$count_media] == $exp_video_id[$count_video]){
												$exp_video_id[$count_video] ="";
											}
										}
									}
								}
							}
							$new_video_id ="";
							for($count_new_video=0;$count_new_video<=count($exp_video_id);$count_new_video++){
								if($exp_video_id[$count_new_video]!=""){
									$new_video_id = $new_video_id .",".$exp_video_id[$count_new_video];
								}
							}
							for($count_gal=0;$count_gal<=count($exp_gallery_id);$count_gal++){
								if($exp_gallery_id[$count_gal]!=""){
									for($count_media=0;$count_media<=count($exp_all_media_id);$count_media++){
										if($exp_all_media_id[$count_media]!=""){
											if($exp_all_media_id[$count_media] == $exp_gallery_id[$count_gal]){
												$exp_gallery_id[$count_gal] ="";
											}
										}
									}
								}
							}
							$new_gal_id ="";
							for($count_new_gal=0;$count_new_gal<=count($exp_gallery_id);$count_new_gal++){
								if($exp_gallery_id[$count_new_gal]!=""){
									$new_gal_id = $new_gal_id .",".$exp_gallery_id[$count_new_gal];
								}
							}
							for($count_del_cre=0;$count_del_cre<=count($exp_deleted_cre_id);$count_del_cre++){
								if($exp_deleted_cre_id[$count_del_cre]!=""){
									for($count_media=0;$count_media<=count($exp_all_media_id);$count_media++){
										if($exp_all_media_id[$count_media]!=""){
											if($exp_all_media_id[$count_media] == $exp_deleted_cre_id[$count_del_cre]){
												$exp_deleted_cre_id[$count_del_cre] ="";
											}
										}
									}
								}
							}
							$new_del_cre_id ="";
							for($count_new_del_cre=0;$count_new_del_cre<=count($exp_deleted_cre_id);$count_new_del_cre++){
								if($exp_deleted_cre_id[$count_new_del_cre]!=""){
									$new_del_cre_id = $new_del_cre_id .",".$exp_deleted_cre_id[$count_new_del_cre];
								}
							}
							for($count_del_fro=0;$count_del_fro<=count($exp_deleted_fro_id);$count_del_fro++){
								if($exp_deleted_fro_id[$count_del_fro]!=""){
									for($count_media=0;$count_media<=count($exp_all_media_id);$count_media++){
										if($exp_all_media_id[$count_media]!=""){
											if($exp_all_media_id[$count_media] == $exp_deleted_fro_id[$count_del_fro]){
												$exp_deleted_fro_id[$count_del_fro] ="";
											}
										}
									}
								}
							}
							$new_del_fro_id ="";
							for($count_new_del_fro=0;$count_new_del_fro<=count($exp_deleted_fro_id);$count_new_del_fro++){
								if($exp_deleted_cre_id[$count_new_del_fro]!=""){
									$new_del_fro_id = $new_del_fro_id .",".$exp_deleted_cre_id[$count_new_del_fro];
								}
							}
							
							mysql_query("update community_event set tagged_songs ='".$new_song_id."', tagged_videos ='".$new_video_id."', tagged_galleries ='".$new_gal_id."', deleted_media_by_creator_creator ='".$new_del_cre_id."', deleted_media_by_from_from='".$new_del_fro_id."' where id='".$res_comeve_det['id']."'");
						}
					}
				}
			}
		}
		/*Delete the project*/
		mysql_query("update community_project set creator='', creators_info='' where id='".$pro_id."'");
	}
	
	function delete_where_user_tagin_project($creator_info,$pro_id)
	{
		/*get deleting project media*/
		$get_media = mysql_query("select * from general_media where creator_info='".$creator_info."' OR from_info='".$creator_info."'");
		if(mysql_num_rows($get_media)>0)
		{
			while($res_media = mysql_fetch_assoc($get_media))
			{
				$all_media_id = $all_media_id .",". $res_media['id'];
			}
		}
		$exp_all_media_id = explode(",",$all_media_id);
		/*get login user detail*/
		$get_gen_det = mysql_query("select * from general_user where email='".$_SESSION['login_email']."'");
		{
			if(mysql_num_rows($get_gen_det)>0)
			{
				$res_gen_det = mysql_fetch_assoc($get_gen_det);
				$exp_deleted_id = explode(",",$res_gen_det['deleted_media_by_tag_user']);
				for($count_deleted=0;$count_deleted<=count($exp_deleted_id);$count_deleted++)
				{
					if($exp_deleted_id[$count_deleted]==""){
					continue;
					}else{
						for($count_media=0;$count_media<=count($exp_all_media_id);$count_media++)
						{
							if($exp_all_media_id[$count_media]==""){ continue; }else{
								if($exp_all_media_id[$count_media]==$exp_deleted_id[$count_deleted]){
									$exp_deleted_id[$count_deleted] ="";
								}
							}
						}
					}
				}
				$new_deleted_id="";
				for($count_new=0;$count_new<=count($exp_deleted_id);$count_new++)
				{
					if($exp_deleted_id[$count_new]!="")
					{
						$new_deleted_id = $new_deleted_id .",". $exp_deleted_id[$count_new];
					}
				}
				$exp_deleting_pro_id = explode(",",$res_gen_det['taggedcommunityprojects']);
				for($count_id=0;$count_id<=count($exp_deleting_pro_id);$count_id++)
				{
					if($exp_deleting_pro_id[$count_id]!="")
					{
						if($exp_deleting_pro_id[$count_id] !=$pro_id)
						{
							$all_new_id = $all_new_id .",". $exp_deleting_pro_id[$count_id];
						}
					}
				}
				mysql_query("update general_user set deleted_media_by_tag_user='".$new_deleted_id."', taggedcommunityprojects ='".$all_new_id."' where email='".$_SESSION['login_email']."'");
				if($res_gen_det['artist_id']>0)
				{
					$get_art_det = mysql_query("select * from general_artist where artist_id='".$res_gen_det['artist_id']."'");
					if(mysql_num_rows($get_art_det)>0)
					{
						$res_art_det = mysql_fetch_assoc($get_art_det);
						$exp_song_id = explode(",",$res_art_det['taggedsongs']);
						$exp_video_id = explode(",",$res_art_det['taggedvideos']);
						$exp_gallery_id = explode(",",$res_art_det['taggedgalleries']);
						$exp_deleted_cre_id = explode(",",$res_art_det['deleted_media_by_creator_creator']);
						$exp_deleted_fro_id = explode(",",$res_art_det['deleted_media_by_from_from']);
						for($count_song=0;$count_song<=count($exp_song_id);$count_song++){
							if($exp_song_id[$count_song]!=""){
								for($count_media=0;$count_media<=count($exp_all_media_id);$count_media++){
									if($exp_all_media_id[$count_media]!=""){
										if($exp_all_media_id[$count_media] == $exp_song_id[$count_song]){
											$exp_song_id[$count_song] ="";
										}
									}
								}
							}
						}
						$new_song_id ="";
						for($count_new_song=0;$count_new_song<=count($exp_song_id);$count_new_song++){
							if($exp_song_id[$count_new_song]!=""){
								$new_song_id = $new_song_id .",".$exp_song_id[$count_new_song];
							}
						}
						for($count_video=0;$count_video<=count($exp_video_id);$count_video++){
							if($exp_video_id[$count_video]!=""){
								for($count_media=0;$count_media<=count($exp_all_media_id);$count_media++){
									if($exp_all_media_id[$count_media]!=""){
										if($exp_all_media_id[$count_media] == $exp_video_id[$count_video]){
											$exp_video_id[$count_video] ="";
										}
									}
								}
							}
						}
						$new_video_id ="";
						for($count_new_video=0;$count_new_video<=count($exp_video_id);$count_new_video++){
							if($exp_video_id[$count_new_video]!=""){
								$new_video_id = $new_video_id .",".$exp_video_id[$count_new_video];
							}
						}
						for($count_gal=0;$count_gal<=count($exp_gallery_id);$count_gal++){
							if($exp_gallery_id[$count_gal]!=""){
								for($count_media=0;$count_media<=count($exp_all_media_id);$count_media++){
									if($exp_all_media_id[$count_media]!=""){
										if($exp_all_media_id[$count_media] == $exp_gallery_id[$count_gal]){
											$exp_gallery_id[$count_gal] ="";
										}
									}
								}
							}
						}
						$new_gal_id ="";
						for($count_new_gal=0;$count_new_gal<=count($exp_gallery_id);$count_new_gal++){
							if($exp_gallery_id[$count_new_gal]!=""){
								$new_gal_id = $new_gal_id .",".$exp_gallery_id[$count_new_gal];
							}
						}
						for($count_del_cre=0;$count_del_cre<=count($exp_deleted_cre_id);$count_del_cre++){
							if($exp_deleted_cre_id[$count_del_cre]!=""){
								for($count_media=0;$count_media<=count($exp_all_media_id);$count_media++){
									if($exp_all_media_id[$count_media]!=""){
										if($exp_all_media_id[$count_media] == $exp_deleted_cre_id[$count_del_cre]){
											$exp_deleted_cre_id[$count_del_cre] ="";
										}
									}
								}
							}
						}
						$new_del_cre_id ="";
						for($count_new_del_cre=0;$count_new_del_cre<=count($exp_deleted_cre_id);$count_new_del_cre++){
							if($exp_deleted_cre_id[$count_new_del_cre]!=""){
								$new_del_cre_id = $new_del_cre_id .",".$exp_deleted_cre_id[$count_new_del_cre];
							}
						}
						for($count_del_fro=0;$count_del_fro<=count($exp_deleted_fro_id);$count_del_fro++){
							if($exp_deleted_fro_id[$count_del_fro]!=""){
								for($count_media=0;$count_media<=count($exp_all_media_id);$count_media++){
									if($exp_all_media_id[$count_media]!=""){
										if($exp_all_media_id[$count_media] == $exp_deleted_fro_id[$count_del_fro]){
											$exp_deleted_fro_id[$count_del_fro] ="";
										}
									}
								}
							}
						}
						$new_del_fro_id ="";
						for($count_new_del_fro=0;$count_new_del_fro<=count($exp_deleted_fro_id);$count_new_del_fro++){
							if($exp_deleted_cre_id[$count_new_del_fro]!=""){
								$new_del_fro_id = $new_del_fro_id .",".$exp_deleted_cre_id[$count_new_del_fro];
							}
						}
						
						mysql_query("update general_artist set taggedsongs ='".$new_song_id."', taggedvideos ='".$new_video_id."', taggedgalleries ='".$new_gal_id."', deleted_media_by_creator_creator ='".$new_del_cre_id."', deleted_media_by_from_from='".$new_del_fro_id."' where artist_id='".$res_art_det['artist_id']."'");
					}
					$get_artpro_det = mysql_query("select * from artist_project where artist_id='".$res_gen_det['artist_id']."'");
					if(mysql_num_rows($get_artpro_det)>0)
					{
						while($res_artpro_det = mysql_fetch_assoc($get_artpro_det))
						{
							$exp_song_id = explode(",",$res_artpro_det['tagged_songs']);
							$exp_video_id = explode(",",$res_artpro_det['tagged_videos']);
							$exp_gallery_id = explode(",",$res_artpro_det['tagged_galleries']);
							$exp_deleted_cre_id = explode(",",$res_artpro_det['deleted_media_by_creator_creator']);
							$exp_deleted_fro_id = explode(",",$res_artpro_det['deleted_media_by_from_from']);
							for($count_song=0;$count_song<=count($exp_song_id);$count_song++){
								if($exp_song_id[$count_song]!=""){
									for($count_media=0;$count_media<=count($exp_all_media_id);$count_media++){
										if($exp_all_media_id[$count_media]!=""){
											if($exp_all_media_id[$count_media] == $exp_song_id[$count_song]){
												$exp_song_id[$count_song] ="";
											}
										}
									}
								}
							}
							$new_song_id ="";
							for($count_new_song=0;$count_new_song<=count($exp_song_id);$count_new_song++){
								if($exp_song_id[$count_new_song]!=""){
									$new_song_id = $new_song_id .",".$exp_song_id[$count_new_song];
								}
							}
							for($count_video=0;$count_video<=count($exp_video_id);$count_video++){
								if($exp_video_id[$count_video]!=""){
									for($count_media=0;$count_media<=count($exp_all_media_id);$count_media++){
										if($exp_all_media_id[$count_media]!=""){
											if($exp_all_media_id[$count_media] == $exp_video_id[$count_video]){
												$exp_video_id[$count_video] ="";
											}
										}
									}
								}
							}
							$new_video_id ="";
							for($count_new_video=0;$count_new_video<=count($exp_video_id);$count_new_video++){
								if($exp_video_id[$count_new_video]!=""){
									$new_video_id = $new_video_id .",".$exp_video_id[$count_new_video];
								}
							}
							for($count_gal=0;$count_gal<=count($exp_gallery_id);$count_gal++){
								if($exp_gallery_id[$count_gal]!=""){
									for($count_media=0;$count_media<=count($exp_all_media_id);$count_media++){
										if($exp_all_media_id[$count_media]!=""){
											if($exp_all_media_id[$count_media] == $exp_gallery_id[$count_gal]){
												$exp_gallery_id[$count_gal] ="";
											}
										}
									}
								}
							}
							$new_gal_id ="";
							for($count_new_gal=0;$count_new_gal<=count($exp_gallery_id);$count_new_gal++){
								if($exp_gallery_id[$count_new_gal]!=""){
									$new_gal_id = $new_gal_id .",".$exp_gallery_id[$count_new_gal];
								}
							}
							for($count_del_cre=0;$count_del_cre<=count($exp_deleted_cre_id);$count_del_cre++){
								if($exp_deleted_cre_id[$count_del_cre]!=""){
									for($count_media=0;$count_media<=count($exp_all_media_id);$count_media++){
										if($exp_all_media_id[$count_media]!=""){
											if($exp_all_media_id[$count_media] == $exp_deleted_cre_id[$count_del_cre]){
												$exp_deleted_cre_id[$count_del_cre] ="";
											}
										}
									}
								}
							}
							$new_del_cre_id ="";
							for($count_new_del_cre=0;$count_new_del_cre<=count($exp_deleted_cre_id);$count_new_del_cre++){
								if($exp_deleted_cre_id[$count_new_del_cre]!=""){
									$new_del_cre_id = $new_del_cre_id .",".$exp_deleted_cre_id[$count_new_del_cre];
								}
							}
							for($count_del_fro=0;$count_del_fro<=count($exp_deleted_fro_id);$count_del_fro++){
								if($exp_deleted_fro_id[$count_del_fro]!=""){
									for($count_media=0;$count_media<=count($exp_all_media_id);$count_media++){
										if($exp_all_media_id[$count_media]!=""){
											if($exp_all_media_id[$count_media] == $exp_deleted_fro_id[$count_del_fro]){
												$exp_deleted_fro_id[$count_del_fro] ="";
											}
										}
									}
								}
							}
							$new_del_fro_id ="";
							for($count_new_del_fro=0;$count_new_del_fro<=count($exp_deleted_fro_id);$count_new_del_fro++){
								if($exp_deleted_cre_id[$count_new_del_fro]!=""){
									$new_del_fro_id = $new_del_fro_id .",".$exp_deleted_cre_id[$count_new_del_fro];
								}
							}
							
							mysql_query("update artist_project set tagged_songs ='".$new_song_id."', tagged_videos ='".$new_video_id."', tagged_galleries ='".$new_gal_id."', deleted_media_by_creator_creator ='".$new_del_cre_id."', deleted_media_by_from_from='".$new_del_fro_id."' where id='".$res_artpro_det['id']."'");
						}
					}
					$get_arteve_det = mysql_query("select * from artist_event where artist_id='".$res_gen_det['artist_id']."'");
					if(mysql_num_rows($get_arteve_det)>0)
					{
						while($res_arteve_det = mysql_fetch_assoc($get_arteve_det))
						{
							$exp_song_id = explode(",",$res_arteve_det['tagged_songs']);
							$exp_video_id = explode(",",$res_arteve_det['tagged_videos']);
							$exp_gallery_id = explode(",",$res_arteve_det['tagged_galleries']);
							$exp_deleted_cre_id = explode(",",$res_arteve_det['deleted_media_by_creator_creator']);
							$exp_deleted_fro_id = explode(",",$res_arteve_det['deleted_media_by_from_from']);
							for($count_song=0;$count_song<=count($exp_song_id);$count_song++){
								if($exp_song_id[$count_song]!=""){
									for($count_media=0;$count_media<=count($exp_all_media_id);$count_media++){
										if($exp_all_media_id[$count_media]!=""){
											if($exp_all_media_id[$count_media] == $exp_song_id[$count_song]){
												$exp_song_id[$count_song] ="";
											}
										}
									}
								}
							}
							$new_song_id ="";
							for($count_new_song=0;$count_new_song<=count($exp_song_id);$count_new_song++){
								if($exp_song_id[$count_new_song]!=""){
									$new_song_id = $new_song_id .",".$exp_song_id[$count_new_song];
								}
							}
							for($count_video=0;$count_video<=count($exp_video_id);$count_video++){
								if($exp_video_id[$count_video]!=""){
									for($count_media=0;$count_media<=count($exp_all_media_id);$count_media++){
										if($exp_all_media_id[$count_media]!=""){
											if($exp_all_media_id[$count_media] == $exp_video_id[$count_video]){
												$exp_video_id[$count_video] ="";
											}
										}
									}
								}
							}
							$new_video_id ="";
							for($count_new_video=0;$count_new_video<=count($exp_video_id);$count_new_video++){
								if($exp_video_id[$count_new_video]!=""){
									$new_video_id = $new_video_id .",".$exp_video_id[$count_new_video];
								}
							}
							for($count_gal=0;$count_gal<=count($exp_gallery_id);$count_gal++){
								if($exp_gallery_id[$count_gal]!=""){
									for($count_media=0;$count_media<=count($exp_all_media_id);$count_media++){
										if($exp_all_media_id[$count_media]!=""){
											if($exp_all_media_id[$count_media] == $exp_gallery_id[$count_gal]){
												$exp_gallery_id[$count_gal] ="";
											}
										}
									}
								}
							}
							$new_gal_id ="";
							for($count_new_gal=0;$count_new_gal<=count($exp_gallery_id);$count_new_gal++){
								if($exp_gallery_id[$count_new_gal]!=""){
									$new_gal_id = $new_gal_id .",".$exp_gallery_id[$count_new_gal];
								}
							}
							for($count_del_cre=0;$count_del_cre<=count($exp_deleted_cre_id);$count_del_cre++){
								if($exp_deleted_cre_id[$count_del_cre]!=""){
									for($count_media=0;$count_media<=count($exp_all_media_id);$count_media++){
										if($exp_all_media_id[$count_media]!=""){
											if($exp_all_media_id[$count_media] == $exp_deleted_cre_id[$count_del_cre]){
												$exp_deleted_cre_id[$count_del_cre] ="";
											}
										}
									}
								}
							}
							$new_del_cre_id ="";
							for($count_new_del_cre=0;$count_new_del_cre<=count($exp_deleted_cre_id);$count_new_del_cre++){
								if($exp_deleted_cre_id[$count_new_del_cre]!=""){
									$new_del_cre_id = $new_del_cre_id .",".$exp_deleted_cre_id[$count_new_del_cre];
								}
							}
							for($count_del_fro=0;$count_del_fro<=count($exp_deleted_fro_id);$count_del_fro++){
								if($exp_deleted_fro_id[$count_del_fro]!=""){
									for($count_media=0;$count_media<=count($exp_all_media_id);$count_media++){
										if($exp_all_media_id[$count_media]!=""){
											if($exp_all_media_id[$count_media] == $exp_deleted_fro_id[$count_del_fro]){
												$exp_deleted_fro_id[$count_del_fro] ="";
											}
										}
									}
								}
							}
							$new_del_fro_id ="";
							for($count_new_del_fro=0;$count_new_del_fro<=count($exp_deleted_fro_id);$count_new_del_fro++){
								if($exp_deleted_cre_id[$count_new_del_fro]!=""){
									$new_del_fro_id = $new_del_fro_id .",".$exp_deleted_cre_id[$count_new_del_fro];
								}
							}
							
							mysql_query("update artist_event set tagged_songs ='".$new_song_id."', tagged_videos ='".$new_video_id."', tagged_galleries ='".$new_gal_id."', deleted_media_by_creator_creator ='".$new_del_cre_id."', deleted_media_by_from_from='".$new_del_fro_id."' where id='".$res_arteve_det['id']."'");
						}
					}
				}
				
				if($res_gen_det['community_id']>0)
				{
					$get_com_det = mysql_query("select * from general_community where community_id='".$res_gen_det['community_id']."'");
					if(mysql_num_rows($get_com_det)>0)
					{
						$res_com_det = mysql_fetch_assoc($get_com_det);
						$exp_song_id = explode(",",$res_com_det['taggedsongs']);
						$exp_video_id = explode(",",$res_com_det['taggedvideos']);
						$exp_gallery_id = explode(",",$res_com_det['taggedgalleries']);
						$exp_deleted_cre_id = explode(",",$res_com_det['deleted_media_by_creator_creator']);
						$exp_deleted_fro_id = explode(",",$res_com_det['deleted_media_by_from_from']);
						for($count_song=0;$count_song<=count($exp_song_id);$count_song++){
							if($exp_song_id[$count_song]!=""){
								for($count_media=0;$count_media<=count($exp_all_media_id);$count_media++){
									if($exp_all_media_id[$count_media]!=""){
										if($exp_all_media_id[$count_media] == $exp_song_id[$count_song]){
											$exp_song_id[$count_song] ="";
										}
									}
								}
							}
						}
						$new_song_id ="";
						for($count_new_song=0;$count_new_song<=count($exp_song_id);$count_new_song++){
							if($exp_song_id[$count_new_song]!=""){
								$new_song_id = $new_song_id .",".$exp_song_id[$count_new_song];
							}
						}
						for($count_video=0;$count_video<=count($exp_video_id);$count_video++){
							if($exp_video_id[$count_video]!=""){
								for($count_media=0;$count_media<=count($exp_all_media_id);$count_media++){
									if($exp_all_media_id[$count_media]!=""){
										if($exp_all_media_id[$count_media] == $exp_video_id[$count_video]){
											$exp_video_id[$count_video] ="";
										}
									}
								}
							}
						}
						$new_video_id ="";
						for($count_new_video=0;$count_new_video<=count($exp_video_id);$count_new_video++){
							if($exp_video_id[$count_new_video]!=""){
								$new_video_id = $new_video_id .",".$exp_video_id[$count_new_video];
							}
						}
						for($count_gal=0;$count_gal<=count($exp_gallery_id);$count_gal++){
							if($exp_gallery_id[$count_gal]!=""){
								for($count_media=0;$count_media<=count($exp_all_media_id);$count_media++){
									if($exp_all_media_id[$count_media]!=""){
										if($exp_all_media_id[$count_media] == $exp_gallery_id[$count_gal]){
											$exp_gallery_id[$count_gal] ="";
										}
									}
								}
							}
						}
						$new_gal_id ="";
						for($count_new_gal=0;$count_new_gal<=count($exp_gallery_id);$count_new_gal++){
							if($exp_gallery_id[$count_new_gal]!=""){
								$new_gal_id = $new_gal_id .",".$exp_gallery_id[$count_new_gal];
							}
						}
						for($count_del_cre=0;$count_del_cre<=count($exp_deleted_cre_id);$count_del_cre++){
							if($exp_deleted_cre_id[$count_del_cre]!=""){
								for($count_media=0;$count_media<=count($exp_all_media_id);$count_media++){
									if($exp_all_media_id[$count_media]!=""){
										if($exp_all_media_id[$count_media] == $exp_deleted_cre_id[$count_del_cre]){
											$exp_deleted_cre_id[$count_del_cre] ="";
										}
									}
								}
							}
						}
						$new_del_cre_id ="";
						for($count_new_del_cre=0;$count_new_del_cre<=count($exp_deleted_cre_id);$count_new_del_cre++){
							if($exp_deleted_cre_id[$count_new_del_cre]!=""){
								$new_del_cre_id = $new_del_cre_id .",".$exp_deleted_cre_id[$count_new_del_cre];
							}
						}
						for($count_del_fro=0;$count_del_fro<=count($exp_deleted_fro_id);$count_del_fro++){
							if($exp_deleted_fro_id[$count_del_fro]!=""){
								for($count_media=0;$count_media<=count($exp_all_media_id);$count_media++){
									if($exp_all_media_id[$count_media]!=""){
										if($exp_all_media_id[$count_media] == $exp_deleted_fro_id[$count_del_fro]){
											$exp_deleted_fro_id[$count_del_fro] ="";
										}
									}
								}
							}
						}
						$new_del_fro_id ="";
						for($count_new_del_fro=0;$count_new_del_fro<=count($exp_deleted_fro_id);$count_new_del_fro++){
							if($exp_deleted_cre_id[$count_new_del_fro]!=""){
								$new_del_fro_id = $new_del_fro_id .",".$exp_deleted_cre_id[$count_new_del_fro];
							}
						}
						
						mysql_query("update general_community set taggedsongs ='".$new_song_id."', taggedvideos ='".$new_video_id."', taggedgalleries ='".$new_gal_id."', deleted_media_by_creator_creator ='".$new_del_cre_id."', deleted_media_by_from_from='".$new_del_fro_id."' where community_id='".$res_com_det['community_id']."'");
					}
					$get_compro_det = mysql_query("select * from community_project where community_id='".$res_gen_det['community_id']."'");
					if(mysql_num_rows($get_compro_det)>0)
					{
						while($res_compro_det = mysql_fetch_assoc($get_compro_det))
						{
							$exp_song_id = explode(",",$res_compro_det['tagged_songs']);
							$exp_video_id = explode(",",$res_compro_det['tagged_videos']);
							$exp_gallery_id = explode(",",$res_compro_det['tagged_galleries']);
							$exp_deleted_cre_id = explode(",",$res_compro_det['deleted_media_by_creator_creator']);
							$exp_deleted_fro_id = explode(",",$res_compro_det['deleted_media_by_from_from']);
							for($count_song=0;$count_song<=count($exp_song_id);$count_song++){
								if($exp_song_id[$count_song]!=""){
									for($count_media=0;$count_media<=count($exp_all_media_id);$count_media++){
										if($exp_all_media_id[$count_media]!=""){
											if($exp_all_media_id[$count_media] == $exp_song_id[$count_song]){
												$exp_song_id[$count_song] ="";
											}
										}
									}
								}
							}
							$new_song_id ="";
							for($count_new_song=0;$count_new_song<=count($exp_song_id);$count_new_song++){
								if($exp_song_id[$count_new_song]!=""){
									$new_song_id = $new_song_id .",".$exp_song_id[$count_new_song];
								}
							}
							for($count_video=0;$count_video<=count($exp_video_id);$count_video++){
								if($exp_video_id[$count_video]!=""){
									for($count_media=0;$count_media<=count($exp_all_media_id);$count_media++){
										if($exp_all_media_id[$count_media]!=""){
											if($exp_all_media_id[$count_media] == $exp_video_id[$count_video]){
												$exp_video_id[$count_video] ="";
											}
										}
									}
								}
							}
							$new_video_id ="";
							for($count_new_video=0;$count_new_video<=count($exp_video_id);$count_new_video++){
								if($exp_video_id[$count_new_video]!=""){
									$new_video_id = $new_video_id .",".$exp_video_id[$count_new_video];
								}
							}
							for($count_gal=0;$count_gal<=count($exp_gallery_id);$count_gal++){
								if($exp_gallery_id[$count_gal]!=""){
									for($count_media=0;$count_media<=count($exp_all_media_id);$count_media++){
										if($exp_all_media_id[$count_media]!=""){
											if($exp_all_media_id[$count_media] == $exp_gallery_id[$count_gal]){
												$exp_gallery_id[$count_gal] ="";
											}
										}
									}
								}
							}
							$new_gal_id ="";
							for($count_new_gal=0;$count_new_gal<=count($exp_gallery_id);$count_new_gal++){
								if($exp_gallery_id[$count_new_gal]!=""){
									$new_gal_id = $new_gal_id .",".$exp_gallery_id[$count_new_gal];
								}
							}
							for($count_del_cre=0;$count_del_cre<=count($exp_deleted_cre_id);$count_del_cre++){
								if($exp_deleted_cre_id[$count_del_cre]!=""){
									for($count_media=0;$count_media<=count($exp_all_media_id);$count_media++){
										if($exp_all_media_id[$count_media]!=""){
											if($exp_all_media_id[$count_media] == $exp_deleted_cre_id[$count_del_cre]){
												$exp_deleted_cre_id[$count_del_cre] ="";
											}
										}
									}
								}
							}
							$new_del_cre_id ="";
							for($count_new_del_cre=0;$count_new_del_cre<=count($exp_deleted_cre_id);$count_new_del_cre++){
								if($exp_deleted_cre_id[$count_new_del_cre]!=""){
									$new_del_cre_id = $new_del_cre_id .",".$exp_deleted_cre_id[$count_new_del_cre];
								}
							}
							for($count_del_fro=0;$count_del_fro<=count($exp_deleted_fro_id);$count_del_fro++){
								if($exp_deleted_fro_id[$count_del_fro]!=""){
									for($count_media=0;$count_media<=count($exp_all_media_id);$count_media++){
										if($exp_all_media_id[$count_media]!=""){
											if($exp_all_media_id[$count_media] == $exp_deleted_fro_id[$count_del_fro]){
												$exp_deleted_fro_id[$count_del_fro] ="";
											}
										}
									}
								}
							}
							$new_del_fro_id ="";
							for($count_new_del_fro=0;$count_new_del_fro<=count($exp_deleted_fro_id);$count_new_del_fro++){
								if($exp_deleted_cre_id[$count_new_del_fro]!=""){
									$new_del_fro_id = $new_del_fro_id .",".$exp_deleted_cre_id[$count_new_del_fro];
								}
							}
							
							mysql_query("update community_project set tagged_songs ='".$new_song_id."', tagged_videos ='".$new_video_id."', tagged_galleries ='".$new_gal_id."', deleted_media_by_creator_creator ='".$new_del_cre_id."', deleted_media_by_from_from='".$new_del_fro_id."' where id='".$res_compro_det['id']."'");
						}
					}
					$get_comeve_det = mysql_query("select * from community_event where community_id='".$res_gen_det['community_id']."'");
					if(mysql_num_rows($get_comeve_det)>0)
					{
						while($res_comeve_det = mysql_fetch_assoc($get_comeve_det))
						{
							$exp_song_id = explode(",",$res_comeve_det['tagged_songs']);
							$exp_video_id = explode(",",$res_comeve_det['tagged_videos']);
							$exp_gallery_id = explode(",",$res_comeve_det['tagged_galleries']);
							$exp_deleted_cre_id = explode(",",$res_comeve_det['deleted_media_by_creator_creator']);
							$exp_deleted_fro_id = explode(",",$res_comeve_det['deleted_media_by_from_from']);
							for($count_song=0;$count_song<=count($exp_song_id);$count_song++){
								if($exp_song_id[$count_song]!=""){
									for($count_media=0;$count_media<=count($exp_all_media_id);$count_media++){
										if($exp_all_media_id[$count_media]!=""){
											if($exp_all_media_id[$count_media] == $exp_song_id[$count_song]){
												$exp_song_id[$count_song] ="";
											}
										}
									}
								}
							}
							$new_song_id ="";
							for($count_new_song=0;$count_new_song<=count($exp_song_id);$count_new_song++){
								if($exp_song_id[$count_new_song]!=""){
									$new_song_id = $new_song_id .",".$exp_song_id[$count_new_song];
								}
							}
							for($count_video=0;$count_video<=count($exp_video_id);$count_video++){
								if($exp_video_id[$count_video]!=""){
									for($count_media=0;$count_media<=count($exp_all_media_id);$count_media++){
										if($exp_all_media_id[$count_media]!=""){
											if($exp_all_media_id[$count_media] == $exp_video_id[$count_video]){
												$exp_video_id[$count_video] ="";
											}
										}
									}
								}
							}
							$new_video_id ="";
							for($count_new_video=0;$count_new_video<=count($exp_video_id);$count_new_video++){
								if($exp_video_id[$count_new_video]!=""){
									$new_video_id = $new_video_id .",".$exp_video_id[$count_new_video];
								}
							}
							for($count_gal=0;$count_gal<=count($exp_gallery_id);$count_gal++){
								if($exp_gallery_id[$count_gal]!=""){
									for($count_media=0;$count_media<=count($exp_all_media_id);$count_media++){
										if($exp_all_media_id[$count_media]!=""){
											if($exp_all_media_id[$count_media] == $exp_gallery_id[$count_gal]){
												$exp_gallery_id[$count_gal] ="";
											}
										}
									}
								}
							}
							$new_gal_id ="";
							for($count_new_gal=0;$count_new_gal<=count($exp_gallery_id);$count_new_gal++){
								if($exp_gallery_id[$count_new_gal]!=""){
									$new_gal_id = $new_gal_id .",".$exp_gallery_id[$count_new_gal];
								}
							}
							for($count_del_cre=0;$count_del_cre<=count($exp_deleted_cre_id);$count_del_cre++){
								if($exp_deleted_cre_id[$count_del_cre]!=""){
									for($count_media=0;$count_media<=count($exp_all_media_id);$count_media++){
										if($exp_all_media_id[$count_media]!=""){
											if($exp_all_media_id[$count_media] == $exp_deleted_cre_id[$count_del_cre]){
												$exp_deleted_cre_id[$count_del_cre] ="";
											}
										}
									}
								}
							}
							$new_del_cre_id ="";
							for($count_new_del_cre=0;$count_new_del_cre<=count($exp_deleted_cre_id);$count_new_del_cre++){
								if($exp_deleted_cre_id[$count_new_del_cre]!=""){
									$new_del_cre_id = $new_del_cre_id .",".$exp_deleted_cre_id[$count_new_del_cre];
								}
							}
							for($count_del_fro=0;$count_del_fro<=count($exp_deleted_fro_id);$count_del_fro++){
								if($exp_deleted_fro_id[$count_del_fro]!=""){
									for($count_media=0;$count_media<=count($exp_all_media_id);$count_media++){
										if($exp_all_media_id[$count_media]!=""){
											if($exp_all_media_id[$count_media] == $exp_deleted_fro_id[$count_del_fro]){
												$exp_deleted_fro_id[$count_del_fro] ="";
											}
										}
									}
								}
							}
							$new_del_fro_id ="";
							for($count_new_del_fro=0;$count_new_del_fro<=count($exp_deleted_fro_id);$count_new_del_fro++){
								if($exp_deleted_cre_id[$count_new_del_fro]!=""){
									$new_del_fro_id = $new_del_fro_id .",".$exp_deleted_cre_id[$count_new_del_fro];
								}
							}
							
							mysql_query("update community_event set tagged_songs ='".$new_song_id."', tagged_videos ='".$new_video_id."', tagged_galleries ='".$new_gal_id."', deleted_media_by_creator_creator ='".$new_del_cre_id."', deleted_media_by_from_from='".$new_del_fro_id."' where id='".$res_comeve_det['id']."'");
						}
					}
				}
			}
		}
		/*Delete the project*/
		$get_del_pro_det = mysql_query("select * from community_project where id='".$pro_id."'");
		if(mysql_num_rows($get_del_pro_det)>0)
		{
			$new_email = "";
			$new_user = "";
			$res_del_pro_det = mysql_fetch_assoc($get_del_pro_det);
			$exp_tag_users = explode(",",$res_del_pro_det['users_tagged']);
			$exp_tag_user_email = explode(",",$res_del_pro_det['tagged_user_email']);
			for($count_email=0;$count_email<=count($exp_tag_user_email);$count_email++)
			{
				if($exp_tag_user_email[$count_email]!="")
				{
					if($exp_tag_user_email[$count_email] != $_SESSION['login_email']){
						$new_email = $new_email .",". $exp_tag_user_email[$count_email];
						$new_user = $new_user .",". $exp_tag_users[$count_email];
					}
				}
			}
			mysql_query("update community_project set users_tagged ='".$new_user."', tagged_user_email ='".$new_email."' where id='".$pro_id."'");
		}
	}
	function delete_artist_project_relations($creator_info,$pro_id)
	{
		$get_pro_det = mysql_query("select * from community_project where id='".$pro_id."'");
		if(mysql_num_rows($get_pro_det)>0)
		{
			$res_pro_det = mysql_fetch_assoc($get_pro_det);
			$get_all_mediaid ="";
			$get_all_pro_media = mysql_query("select * from general_media where creator_info = '".$creator_info."' OR from_info = '".$creator_info."'");
			if(mysql_num_rows($get_all_pro_media)>0){
				while($res_all_pro_media = mysql_fetch_assoc($get_all_pro_media))
				{
					$get_all_mediaid = $get_all_mediaid .",". $res_all_pro_media['id'];
				}
			}
			$exp_tag_email = explode(",",$res_pro_det['tagged_user_email']);
			for($count_tag=0;$count_tag<=count($exp_tag_email);$count_tag++)
			{
				if($exp_tag_email[$count_tag]!="")
				{
					$get_tag_det = mysql_query("select * from general_user where email='".$exp_tag_email[$count_tag]."'");
					if(mysql_num_rows($get_tag_det)>0)
					{
						$res_tag_det = mysql_fetch_assoc($get_tag_det);
						if($res_tag_det['artist_id']>0)
						{
							$get_tag_art_det = mysql_query("select * from general_artist where artist_id='".$res_tag_det['artist_id']."'");
							if(mysql_num_rows($get_tag_art_det)>0)
							{
								$res_tag_art_det = mysql_fetch_assoc($get_tag_art_det);
								$exp_tag_song = explode(",",$res_tag_art_det['taggedsongs']);
								$exp_tag_video = explode(",",$res_tag_art_det['taggedvideos']);
								$exp_tag_gallery = explode(",",$res_tag_art_det['taggedgalleries']);
								if($get_all_mediaid!="")
								{
									$exp_all_mediaid = explode(",",$get_all_mediaid);
									for($count_media=0;$count_media<=count($exp_all_mediaid);$count_media++)
									{
										if($exp_all_mediaid[$count_media]==""){continue;}
										else{
											for($count_tag=0;$count_tag<=count($exp_tag_song);$count_tag++)
											{
												if($exp_tag_song[$count_tag]==""){continue;}
												else{
													if($exp_tag_song[$count_tag] == $exp_all_mediaid[$count_media])
													{
														$exp_tag_song[$count_tag]="";
													}
												}
											}
										}
									}
									for($count_media=0;$count_media<=count($exp_all_mediaid);$count_media++)
									{
										if($exp_all_mediaid[$count_media]==""){continue;}
										else{
											for($count_tag_video=0;$count_tag_video<=count($exp_tag_video);$count_tag_video++)
											{
												if($exp_tag_video[$count_tag_video]==""){continue;}
												else{
													if($exp_tag_video[$count_tag_video] == $exp_all_mediaid[$count_media])
													{
														$exp_tag_video[$count_tag_video]="";
													}
												}
											}
										}
									}
									for($count_media=0;$count_media<=count($exp_all_mediaid);$count_media++)
									{
										if($exp_all_mediaid[$count_media]==""){continue;}
										else{
											for($count_tag_gal=0;$count_tag_gal<=count($exp_tag_gallery);$count_tag_gal++)
											{
												if($exp_tag_gallery[$count_tag_gal]==""){continue;}
												else{
													if($exp_tag_gallery[$count_tag_gal] == $exp_all_mediaid[$count_media])
													{
														$exp_tag_gallery[$count_tag_gal]="";
													}
												}
											}
										}
									}
									$new_song_id="";
									for($count_song_id=0;$count_song_id<=count($exp_tag_song);$count_song_id++)
									{
										if($exp_tag_song[$count_song_id]!=""){
										$new_song_id = $new_song_id .",". $exp_tag_song[$count_song_id];}
									}
									$new_video_id="";
									for($count_video_id=0;$count_video_id<=count($exp_tag_video);$count_video_id++)
									{
										if($exp_tag_video[$count_video_id]!=""){
										$new_video_id = $new_video_id .",". $exp_tag_video[$count_video_id];}
									}
									$new_gallery_id="";
									for($count_gallery_id=0;$count_gallery_id<=count($exp_tag_gallery);$count_gallery_id++)
									{
										if($exp_tag_gallery[$count_gallery_id]!=""){
										$new_gallery_id = $new_gallery_id .",". $exp_tag_gallery[$count_gallery_id];}
									}
									mysql_query("update general_artist set taggedsongs='".$new_song_id."', taggedvideos='".$new_video_id."', taggedgalleries='".$new_gallery_id."' where artist_id='".$res_tag_art_det['artist_id']."'");
								}
							}
							$get_tag_artpro_det = mysql_query("select * from artist_project where artist_id='".$res_tag_det['artist_id']."'");
							if(mysql_num_rows($get_tag_artpro_det)>0)
							{
								while($res_tag_artpro_det = mysql_fetch_assoc($get_tag_artpro_det))
								{
									$exp_tag_song = explode(",",$res_tag_artpro_det['tagged_songs']);
									$exp_tag_video = explode(",",$res_tag_artpro_det['tagged_videos']);
									$exp_tag_gallery = explode(",",$res_tag_artpro_det['tagged_galleries']);
									if($get_all_mediaid!="")
									{
										$exp_all_mediaid = explode(",",$get_all_mediaid);
										for($count_media=0;$count_media<=count($exp_all_mediaid);$count_media++)
										{
											if($exp_all_mediaid[$count_media]==""){continue;}
											else{
												for($count_tag=0;$count_tag<=count($exp_tag_song);$count_tag++)
												{
													if($exp_tag_song[$count_tag]==""){continue;}
													else{
														if($exp_tag_song[$count_tag] == $exp_all_mediaid[$count_media])
														{
															$exp_tag_song[$count_tag]="";
														}
													}
												}
											}
										}
										for($count_media=0;$count_media<=count($exp_all_mediaid);$count_media++)
										{
											if($exp_all_mediaid[$count_media]==""){continue;}
											else{
												for($count_tag_video=0;$count_tag_video<=count($exp_tag_video);$count_tag_video++)
												{
													if($exp_tag_video[$count_tag_video]==""){continue;}
													else{
														if($exp_tag_video[$count_tag_video] == $exp_all_mediaid[$count_media])
														{
															$exp_tag_video[$count_tag_video]="";
														}
													}
												}
											}
										}
										for($count_media=0;$count_media<=count($exp_all_mediaid);$count_media++)
										{
											if($exp_all_mediaid[$count_media]==""){continue;}
											else{
												for($count_tag_gal=0;$count_tag_gal<=count($exp_tag_gallery);$count_tag_gal++)
												{
													if($exp_tag_gallery[$count_tag_gal]==""){continue;}
													else{
														if($exp_tag_gallery[$count_tag_gal] == $exp_all_mediaid[$count_media])
														{
															$exp_tag_gallery[$count_tag_gal]="";
														}
													}
												}
											}
										}
										$new_song_id="";
										for($count_song_id=0;$count_song_id<=count($exp_tag_song);$count_song_id++)
										{
											if($exp_tag_song[$count_song_id]!=""){
											$new_song_id = $new_song_id .",". $exp_tag_song[$count_song_id];}
										}
										$new_video_id="";
										for($count_video_id=0;$count_video_id<=count($exp_tag_video);$count_video_id++)
										{
											if($exp_tag_video[$count_video_id]!=""){
											$new_video_id = $new_video_id .",". $exp_tag_video[$count_video_id];}
										}
										$new_gallery_id="";
										for($count_gallery_id=0;$count_gallery_id<=count($exp_tag_gallery);$count_gallery_id++)
										{
											if($exp_tag_gallery[$count_gallery_id]!=""){
											$new_gallery_id = $new_gallery_id .",". $exp_tag_gallery[$count_gallery_id];}
										}
										mysql_query("update artist_project set tagged_songs='".$new_song_id."', tagged_videos='".$new_video_id."', tagged_galleries='".$new_gallery_id."' where id='".$res_tag_artpro_det['id']."'");
									}
								}
							}
							$get_tag_arteve_det = mysql_query("select * from artist_event where artist_id='".$res_tag_det['artist_id']."'");
							if(mysql_num_rows($get_tag_arteve_det)>0)
							{
								while($res_tag_arteve_det = mysql_fetch_assoc($get_tag_arteve_det))
								{
									$exp_tag_song = explode(",",$res_tag_arteve_det['tagged_songs']);
									$exp_tag_video = explode(",",$res_tag_arteve_det['tagged_videos']);
									$exp_tag_gallery = explode(",",$res_tag_arteve_det['tagged_galleries']);
									if($get_all_mediaid!="")
									{
										$exp_all_mediaid = explode(",",$get_all_mediaid);
										for($count_media=0;$count_media<=count($exp_all_mediaid);$count_media++)
										{
											if($exp_all_mediaid[$count_media]==""){continue;}
											else{
												for($count_tag=0;$count_tag<=count($exp_tag_song);$count_tag++)
												{
													if($exp_tag_song[$count_tag]==""){continue;}
													else{
														if($exp_tag_song[$count_tag] == $exp_all_mediaid[$count_media])
														{
															$exp_tag_song[$count_tag]="";
														}
													}
												}
											}
										}
										for($count_media=0;$count_media<=count($exp_all_mediaid);$count_media++)
										{
											if($exp_all_mediaid[$count_media]==""){continue;}
											else{
												for($count_tag_video=0;$count_tag_video<=count($exp_tag_video);$count_tag_video++)
												{
													if($exp_tag_video[$count_tag_video]==""){continue;}
													else{
														if($exp_tag_video[$count_tag_video] == $exp_all_mediaid[$count_media])
														{
															$exp_tag_video[$count_tag_video]="";
														}
													}
												}
											}
										}
										for($count_media=0;$count_media<=count($exp_all_mediaid);$count_media++)
										{
											if($exp_all_mediaid[$count_media]==""){continue;}
											else{
												for($count_tag_gal=0;$count_tag_gal<=count($exp_tag_gallery);$count_tag_gal++)
												{
													if($exp_tag_gallery[$count_tag_gal]==""){continue;}
													else{
														if($exp_tag_gallery[$count_tag_gal] == $exp_all_mediaid[$count_media])
														{
															$exp_tag_gallery[$count_tag_gal]="";
														}
													}
												}
											}
										}
										$new_song_id="";
										for($count_song_id=0;$count_song_id<=count($exp_tag_song);$count_song_id++)
										{
											if($exp_tag_song[$count_song_id]!=""){
											$new_song_id = $new_song_id .",". $exp_tag_song[$count_song_id];}
										}
										$new_video_id="";
										for($count_video_id=0;$count_video_id<=count($exp_tag_video);$count_video_id++)
										{
											if($exp_tag_video[$count_video_id]!=""){
											$new_video_id = $new_video_id .",". $exp_tag_video[$count_video_id];}
										}
										$new_gallery_id="";
										for($count_gallery_id=0;$count_gallery_id<=count($exp_tag_gallery);$count_gallery_id++)
										{
											if($exp_tag_gallery[$count_gallery_id]!=""){
											$new_gallery_id = $new_gallery_id .",". $exp_tag_gallery[$count_gallery_id];}
										}
										mysql_query("update artist_event set tagged_songs='".$new_song_id."', tagged_videos='".$new_video_id."', tagged_galleries='".$new_gallery_id."' where id='".$res_tag_arteve_det['id']."'");
									}
								}
							}
						}
						
						if($res_tag_det['community_id']>0)
						{
							$get_tag_com_det = mysql_query("select * from general_community where community_id='".$res_tag_det['community_id']."'");
							if(mysql_num_rows($get_tag_com_det)>0)
							{
								$res_tag_com_det = mysql_fetch_assoc($get_tag_com_det);
								$exp_tag_song = explode(",",$res_tag_com_det['taggedsongs']);
								$exp_tag_video = explode(",",$res_tag_com_det['taggedvideos']);
								$exp_tag_gallery = explode(",",$res_tag_com_det['taggedgalleries']);
								if($get_all_mediaid!="")
								{
									$exp_all_mediaid = explode(",",$get_all_mediaid);
									for($count_media=0;$count_media<=count($exp_all_mediaid);$count_media++)
									{
										if($exp_all_mediaid[$count_media]==""){continue;}
										else{
											for($count_tag=0;$count_tag<=count($exp_tag_song);$count_tag++)
											{
												if($exp_tag_song[$count_tag]==""){continue;}
												else{
													if($exp_tag_song[$count_tag] == $exp_all_mediaid[$count_media])
													{
														$exp_tag_song[$count_tag]="";
													}
												}
											}
										}
									}
									for($count_media=0;$count_media<=count($exp_all_mediaid);$count_media++)
									{
										if($exp_all_mediaid[$count_media]==""){continue;}
										else{
											for($count_tag_video=0;$count_tag_video<=count($exp_tag_video);$count_tag_video++)
											{
												if($exp_tag_video[$count_tag_video]==""){continue;}
												else{
													if($exp_tag_video[$count_tag_video] == $exp_all_mediaid[$count_media])
													{
														$exp_tag_video[$count_tag_video]="";
													}
												}
											}
										}
									}
									for($count_media=0;$count_media<=count($exp_all_mediaid);$count_media++)
									{
										if($exp_all_mediaid[$count_media]==""){continue;}
										else{
											for($count_tag_gal=0;$count_tag_gal<=count($exp_tag_gallery);$count_tag_gal++)
											{
												if($exp_tag_gallery[$count_tag_gal]==""){continue;}
												else{
													if($exp_tag_gallery[$count_tag_gal] == $exp_all_mediaid[$count_media])
													{
														$exp_tag_gallery[$count_tag_gal]="";
													}
												}
											}
										}
									}
									$new_song_id="";
									for($count_song_id=0;$count_song_id<=count($exp_tag_song);$count_song_id++)
									{
										if($exp_tag_song[$count_song_id]!=""){
										$new_song_id = $new_song_id .",". $exp_tag_song[$count_song_id];}
									}
									$new_video_id="";
									for($count_video_id=0;$count_video_id<=count($exp_tag_video);$count_video_id++)
									{
										if($exp_tag_video[$count_video_id]!=""){
										$new_video_id = $new_video_id .",". $exp_tag_video[$count_video_id];}
									}
									$new_gallery_id="";
									for($count_gallery_id=0;$count_gallery_id<=count($exp_tag_gallery);$count_gallery_id++)
									{
										if($exp_tag_gallery[$count_gallery_id]!=""){
										$new_gallery_id = $new_gallery_id .",". $exp_tag_gallery[$count_gallery_id];}
									}
									mysql_query("update general_community set taggedsongs='".$new_song_id."', taggedvideos='".$new_video_id."', taggedgalleries='".$new_gallery_id."' where community_id='".$res_tag_com_det['community_id']."'");
								}
							}
							$get_tag_compro_det = mysql_query("select * from community_project where community_id='".$res_tag_det['community_id']."'");
							if(mysql_num_rows($get_tag_compro_det)>0)
							{
								while($res_tag_compro_det = mysql_fetch_assoc($get_tag_compro_det))
								{
									$exp_tag_song = explode(",",$res_tag_compro_det['tagged_songs']);
									$exp_tag_video = explode(",",$res_tag_compro_det['tagged_videos']);
									$exp_tag_gallery = explode(",",$res_tag_compro_det['tagged_galleries']);
									if($get_all_mediaid!="")
									{
										$exp_all_mediaid = explode(",",$get_all_mediaid);
										for($count_media=0;$count_media<=count($exp_all_mediaid);$count_media++)
										{
											if($exp_all_mediaid[$count_media]==""){continue;}
											else{
												for($count_tag=0;$count_tag<=count($exp_tag_song);$count_tag++)
												{
													if($exp_tag_song[$count_tag]==""){continue;}
													else{
														if($exp_tag_song[$count_tag] == $exp_all_mediaid[$count_media])
														{
															$exp_tag_song[$count_tag]="";
														}
													}
												}
											}
										}
										for($count_media=0;$count_media<=count($exp_all_mediaid);$count_media++)
										{
											if($exp_all_mediaid[$count_media]==""){continue;}
											else{
												for($count_tag_video=0;$count_tag_video<=count($exp_tag_video);$count_tag_video++)
												{
													if($exp_tag_video[$count_tag_video]==""){continue;}
													else{
														if($exp_tag_video[$count_tag_video] == $exp_all_mediaid[$count_media])
														{
															$exp_tag_video[$count_tag_video]="";
														}
													}
												}
											}
										}
										for($count_media=0;$count_media<=count($exp_all_mediaid);$count_media++)
										{
											if($exp_all_mediaid[$count_media]==""){continue;}
											else{
												for($count_tag_gal=0;$count_tag_gal<=count($exp_tag_gallery);$count_tag_gal++)
												{
													if($exp_tag_gallery[$count_tag_gal]==""){continue;}
													else{
														if($exp_tag_gallery[$count_tag_gal] == $exp_all_mediaid[$count_media])
														{
															$exp_tag_gallery[$count_tag_gal]="";
														}
													}
												}
											}
										}
										$new_song_id="";
										for($count_song_id=0;$count_song_id<=count($exp_tag_song);$count_song_id++)
										{
											if($exp_tag_song[$count_song_id]!=""){
											$new_song_id = $new_song_id .",". $exp_tag_song[$count_song_id];}
										}
										$new_video_id="";
										for($count_video_id=0;$count_video_id<=count($exp_tag_video);$count_video_id++)
										{
											if($exp_tag_video[$count_video_id]!=""){
											$new_video_id = $new_video_id .",". $exp_tag_video[$count_video_id];}
										}
										$new_gallery_id="";
										for($count_gallery_id=0;$count_gallery_id<=count($exp_tag_gallery);$count_gallery_id++)
										{
											if($exp_tag_gallery[$count_gallery_id]!=""){
											$new_gallery_id = $new_gallery_id .",". $exp_tag_gallery[$count_gallery_id];}
										}
										mysql_query("update community_project set tagged_songs='".$new_song_id."', tagged_videos='".$new_video_id."', tagged_galleries='".$new_gallery_id."' where id='".$res_tag_compro_det['id']."'");
									}
								}
							}
							$get_tag_comeve_det = mysql_query("select * from community_event where community_id='".$res_tag_det['community_id']."'");
							if(mysql_num_rows($get_tag_comeve_det)>0)
							{
								while($res_tag_comeve_det = mysql_fetch_assoc($get_tag_comeve_det))
								{
									$exp_tag_song = explode(",",$res_tag_comeve_det['tagged_songs']);
									$exp_tag_video = explode(",",$res_tag_comeve_det['tagged_videos']);
									$exp_tag_gallery = explode(",",$res_tag_comeve_det['tagged_galleries']);
									if($get_all_mediaid!="")
									{
										$exp_all_mediaid = explode(",",$get_all_mediaid);
										for($count_media=0;$count_media<=count($exp_all_mediaid);$count_media++)
										{
											if($exp_all_mediaid[$count_media]==""){continue;}
											else{
												for($count_tag=0;$count_tag<=count($exp_tag_song);$count_tag++)
												{
													if($exp_tag_song[$count_tag]==""){continue;}
													else{
														if($exp_tag_song[$count_tag] == $exp_all_mediaid[$count_media])
														{
															$exp_tag_song[$count_tag]="";
														}
													}
												}
											}
										}
										for($count_media=0;$count_media<=count($exp_all_mediaid);$count_media++)
										{
											if($exp_all_mediaid[$count_media]==""){continue;}
											else{
												for($count_tag_video=0;$count_tag_video<=count($exp_tag_video);$count_tag_video++)
												{
													if($exp_tag_video[$count_tag_video]==""){continue;}
													else{
														if($exp_tag_video[$count_tag_video] == $exp_all_mediaid[$count_media])
														{
															$exp_tag_video[$count_tag_video]="";
														}
													}
												}
											}
										}
										for($count_media=0;$count_media<=count($exp_all_mediaid);$count_media++)
										{
											if($exp_all_mediaid[$count_media]==""){continue;}
											else{
												for($count_tag_gal=0;$count_tag_gal<=count($exp_tag_gallery);$count_tag_gal++)
												{
													if($exp_tag_gallery[$count_tag_gal]==""){continue;}
													else{
														if($exp_tag_gallery[$count_tag_gal] == $exp_all_mediaid[$count_media])
														{
															$exp_tag_gallery[$count_tag_gal]="";
														}
													}
												}
											}
										}
										$new_song_id="";
										for($count_song_id=0;$count_song_id<=count($exp_tag_song);$count_song_id++)
										{
											if($exp_tag_song[$count_song_id]!=""){
											$new_song_id = $new_song_id .",". $exp_tag_song[$count_song_id];}
										}
										$new_video_id="";
										for($count_video_id=0;$count_video_id<=count($exp_tag_video);$count_video_id++)
										{
											if($exp_tag_video[$count_video_id]!=""){
											$new_video_id = $new_video_id .",". $exp_tag_video[$count_video_id];}
										}
										$new_gallery_id="";
										for($count_gallery_id=0;$count_gallery_id<=count($exp_tag_gallery);$count_gallery_id++)
										{
											if($exp_tag_gallery[$count_gallery_id]!=""){
											$new_gallery_id = $new_gallery_id .",". $exp_tag_gallery[$count_gallery_id];}
										}
										mysql_query("update community_event set tagged_songs='".$new_song_id."', tagged_videos='".$new_video_id."', tagged_galleries='".$new_gallery_id."' where id='".$res_tag_comeve_det['id']."'");
									}
								}
							}
						}
					}
				}
			}
			$exp_creator = explode("|",$res_pro_det['creator_info']);
			if($exp_creator[0]=="general_artist"){
				$get_creator_in_det = mysql_query("select * from general_user where artist_id='".$exp_creator[1]."'");
			}if($exp_creator[0]=="general_community"){
				$get_creator_in_det = mysql_query("select * from general_user where community_id='".$exp_creator[1]."'");
			}if($exp_creator[0]=="artist_project"){
				$get_prodet = mysql_query("select * from artist_project where id='".$exp_creator[1]."'");
				$res_prodet = mysql_fetch_assoc($get_prodet);
				$get_creator_in_det = mysql_query("select * from general_user where artist_id='".$res_prodet['artist_id']."'");
			}if($exp_creator[0]=="community_project"){
				$get_prodet = mysql_query("select * from community_project where id='".$exp_creator[1]."'");
				$res_prodet = mysql_fetch_assoc($get_prodet);
				$get_creator_in_det = mysql_query("select * from general_user where community_id='".$res_prodet['community_id']."'");
			}
			if(mysql_num_rows($get_creator_in_det)>0)
			{
				$res_creator_in_det = mysql_fetch_assoc($get_creator_in_det);
				if($res_creator_in_det['artist_id']>0)
				{
					$get_tag_art_det = mysql_query("select * from general_artist where artist_id='".$res_creator_in_det['artist_id']."'");
					if(mysql_num_rows($get_tag_art_det)>0)
					{
						$res_tag_art_det = mysql_fetch_assoc($get_tag_art_det);
						$exp_tag_song = explode(",",$res_tag_art_det['taggedsongs']);
						$exp_tag_video = explode(",",$res_tag_art_det['taggedvideos']);
						$exp_tag_gallery = explode(",",$res_tag_art_det['taggedgalleries']);
						if($get_all_mediaid!="")
						{
							$exp_all_mediaid = explode(",",$get_all_mediaid);
							for($count_media=0;$count_media<=count($exp_all_mediaid);$count_media++)
							{
								if($exp_all_mediaid[$count_media]==""){continue;}
								else{
									for($count_tag=0;$count_tag<=count($exp_tag_song);$count_tag++)
									{
										if($exp_tag_song[$count_tag]==""){continue;}
										else{
											if($exp_tag_song[$count_tag] == $exp_all_mediaid[$count_media])
											{
												$exp_tag_song[$count_tag]="";
											}
										}
									}
								}
							}
							for($count_media=0;$count_media<=count($exp_all_mediaid);$count_media++)
							{
								if($exp_all_mediaid[$count_media]==""){continue;}
								else{
									for($count_tag_video=0;$count_tag_video<=count($exp_tag_video);$count_tag_video++)
									{
										if($exp_tag_video[$count_tag_video]==""){continue;}
										else{
											if($exp_tag_video[$count_tag_video] == $exp_all_mediaid[$count_media])
											{
												$exp_tag_video[$count_tag_video]="";
											}
										}
									}
								}
							}
							for($count_media=0;$count_media<=count($exp_all_mediaid);$count_media++)
							{
								if($exp_all_mediaid[$count_media]==""){continue;}
								else{
									for($count_tag_gal=0;$count_tag_gal<=count($exp_tag_gallery);$count_tag_gal++)
									{
										if($exp_tag_gallery[$count_tag_gal]==""){continue;}
										else{
											if($exp_tag_gallery[$count_tag_gal] == $exp_all_mediaid[$count_media])
											{
												$exp_tag_gallery[$count_tag_gal]="";
											}
										}
									}
								}
							}
							$new_song_id="";
							for($count_song_id=0;$count_song_id<=count($exp_tag_song);$count_song_id++)
							{
								if($exp_tag_song[$count_song_id]!=""){
								$new_song_id = $new_song_id .",". $exp_tag_song[$count_song_id];}
							}
							$new_video_id="";
							for($count_video_id=0;$count_video_id<=count($exp_tag_video);$count_video_id++)
							{
								if($exp_tag_video[$count_video_id]!=""){
								$new_video_id = $new_video_id .",". $exp_tag_video[$count_video_id];}
							}
							$new_gallery_id="";
							for($count_gallery_id=0;$count_gallery_id<=count($exp_tag_gallery);$count_gallery_id++)
							{
								if($exp_tag_gallery[$count_gallery_id]!=""){
								$new_gallery_id = $new_gallery_id .",". $exp_tag_gallery[$count_gallery_id];}
							}
							mysql_query("update general_artist set taggedsongs='".$new_song_id."', taggedvideos='".$new_video_id."', taggedgalleries='".$new_gallery_id."' where artist_id='".$res_tag_art_det['artist_id']."'");
						}
					}
					$get_tag_artpro_det = mysql_query("select * from artist_project where artist_id='".$res_creator_in_det['artist_id']."'");
					if(mysql_num_rows($get_tag_artpro_det)>0)
					{
						while($res_tag_artpro_det = mysql_fetch_assoc($get_tag_artpro_det))
						{
							$exp_tag_song = explode(",",$res_tag_artpro_det['tagged_songs']);
							$exp_tag_video = explode(",",$res_tag_artpro_det['tagged_videos']);
							$exp_tag_gallery = explode(",",$res_tag_artpro_det['tagged_galleries']);
							if($get_all_mediaid!="")
							{
								$exp_all_mediaid = explode(",",$get_all_mediaid);
								for($count_media=0;$count_media<=count($exp_all_mediaid);$count_media++)
								{
									if($exp_all_mediaid[$count_media]==""){continue;}
									else{
										for($count_tag=0;$count_tag<=count($exp_tag_song);$count_tag++)
										{
											if($exp_tag_song[$count_tag]==""){continue;}
											else{
												if($exp_tag_song[$count_tag] == $exp_all_mediaid[$count_media])
												{
													$exp_tag_song[$count_tag]="";
												}
											}
										}
									}
								}
								for($count_media=0;$count_media<=count($exp_all_mediaid);$count_media++)
								{
									if($exp_all_mediaid[$count_media]==""){continue;}
									else{
										for($count_tag_video=0;$count_tag_video<=count($exp_tag_video);$count_tag_video++)
										{
											if($exp_tag_video[$count_tag_video]==""){continue;}
											else{
												if($exp_tag_video[$count_tag_video] == $exp_all_mediaid[$count_media])
												{
													$exp_tag_video[$count_tag_video]="";
												}
											}
										}
									}
								}
								for($count_media=0;$count_media<=count($exp_all_mediaid);$count_media++)
								{
									if($exp_all_mediaid[$count_media]==""){continue;}
									else{
										for($count_tag_gal=0;$count_tag_gal<=count($exp_tag_gallery);$count_tag_gal++)
										{
											if($exp_tag_gallery[$count_tag_gal]==""){continue;}
											else{
												if($exp_tag_gallery[$count_tag_gal] == $exp_all_mediaid[$count_media])
												{
													$exp_tag_gallery[$count_tag_gal]="";
												}
											}
										}
									}
								}
								$new_song_id="";
								for($count_song_id=0;$count_song_id<=count($exp_tag_song);$count_song_id++)
								{
									if($exp_tag_song[$count_song_id]!=""){
									$new_song_id = $new_song_id .",". $exp_tag_song[$count_song_id];}
								}
								$new_video_id="";
								for($count_video_id=0;$count_video_id<=count($exp_tag_video);$count_video_id++)
								{
									if($exp_tag_video[$count_video_id]!=""){
									$new_video_id = $new_video_id .",". $exp_tag_video[$count_video_id];}
								}
								$new_gallery_id="";
								for($count_gallery_id=0;$count_gallery_id<=count($exp_tag_gallery);$count_gallery_id++)
								{
									if($exp_tag_gallery[$count_gallery_id]!=""){
									$new_gallery_id = $new_gallery_id .",". $exp_tag_gallery[$count_gallery_id];}
								}
								mysql_query("update artist_project set tagged_songs='".$new_song_id."', tagged_videos='".$new_video_id."', tagged_galleries='".$new_gallery_id."' where id='".$res_tag_artpro_det['id']."'");
							}
						}
					}
					$get_tag_arteve_det = mysql_query("select * from artist_event where artist_id='".$res_creator_in_det['artist_id']."'");
					if(mysql_num_rows($get_tag_arteve_det)>0)
					{
						while($res_tag_arteve_det = mysql_fetch_assoc($get_tag_arteve_det))
						{
							$exp_tag_song = explode(",",$res_tag_arteve_det['tagged_songs']);
							$exp_tag_video = explode(",",$res_tag_arteve_det['tagged_videos']);
							$exp_tag_gallery = explode(",",$res_tag_arteve_det['tagged_galleries']);
							if($get_all_mediaid!="")
							{
								$exp_all_mediaid = explode(",",$get_all_mediaid);
								for($count_media=0;$count_media<=count($exp_all_mediaid);$count_media++)
								{
									if($exp_all_mediaid[$count_media]==""){continue;}
									else{
										for($count_tag=0;$count_tag<=count($exp_tag_song);$count_tag++)
										{
											if($exp_tag_song[$count_tag]==""){continue;}
											else{
												if($exp_tag_song[$count_tag] == $exp_all_mediaid[$count_media])
												{
													$exp_tag_song[$count_tag]="";
												}
											}
										}
									}
								}
								for($count_media=0;$count_media<=count($exp_all_mediaid);$count_media++)
								{
									if($exp_all_mediaid[$count_media]==""){continue;}
									else{
										for($count_tag_video=0;$count_tag_video<=count($exp_tag_video);$count_tag_video++)
										{
											if($exp_tag_video[$count_tag_video]==""){continue;}
											else{
												if($exp_tag_video[$count_tag_video] == $exp_all_mediaid[$count_media])
												{
													$exp_tag_video[$count_tag_video]="";
												}
											}
										}
									}
								}
								for($count_media=0;$count_media<=count($exp_all_mediaid);$count_media++)
								{
									if($exp_all_mediaid[$count_media]==""){continue;}
									else{
										for($count_tag_gal=0;$count_tag_gal<=count($exp_tag_gallery);$count_tag_gal++)
										{
											if($exp_tag_gallery[$count_tag_gal]==""){continue;}
											else{
												if($exp_tag_gallery[$count_tag_gal] == $exp_all_mediaid[$count_media])
												{
													$exp_tag_gallery[$count_tag_gal]="";
												}
											}
										}
									}
								}
								$new_song_id="";
								for($count_song_id=0;$count_song_id<=count($exp_tag_song);$count_song_id++)
								{
									if($exp_tag_song[$count_song_id]!=""){
									$new_song_id = $new_song_id .",". $exp_tag_song[$count_song_id];}
								}
								$new_video_id="";
								for($count_video_id=0;$count_video_id<=count($exp_tag_video);$count_video_id++)
								{
									if($exp_tag_video[$count_video_id]!=""){
									$new_video_id = $new_video_id .",". $exp_tag_video[$count_video_id];}
								}
								$new_gallery_id="";
								for($count_gallery_id=0;$count_gallery_id<=count($exp_tag_gallery);$count_gallery_id++)
								{
									if($exp_tag_gallery[$count_gallery_id]!=""){
									$new_gallery_id = $new_gallery_id .",". $exp_tag_gallery[$count_gallery_id];}
								}
								mysql_query("update artist_event set tagged_songs='".$new_song_id."', tagged_videos='".$new_video_id."', tagged_galleries='".$new_gallery_id."' where id='".$res_tag_arteve_det['id']."'");
							}
						}
					}
				}
				
				if($res_tag_det['community_id']>0)
				{
					$get_tag_com_det = mysql_query("select * from general_community where community_id='".$res_creator_in_det['community_id']."'");
					if(mysql_num_rows($get_tag_com_det)>0)
					{
						$res_tag_com_det = mysql_fetch_assoc($get_tag_com_det);
						$exp_tag_song = explode(",",$res_tag_com_det['taggedsongs']);
						$exp_tag_video = explode(",",$res_tag_com_det['taggedvideos']);
						$exp_tag_gallery = explode(",",$res_tag_com_det['taggedgalleries']);
						if($get_all_mediaid!="")
						{
							$exp_all_mediaid = explode(",",$get_all_mediaid);
							for($count_media=0;$count_media<=count($exp_all_mediaid);$count_media++)
							{
								if($exp_all_mediaid[$count_media]==""){continue;}
								else{
									for($count_tag=0;$count_tag<=count($exp_tag_song);$count_tag++)
									{
										if($exp_tag_song[$count_tag]==""){continue;}
										else{
											if($exp_tag_song[$count_tag] == $exp_all_mediaid[$count_media])
											{
												$exp_tag_song[$count_tag]="";
											}
										}
									}
								}
							}
							for($count_media=0;$count_media<=count($exp_all_mediaid);$count_media++)
							{
								if($exp_all_mediaid[$count_media]==""){continue;}
								else{
									for($count_tag_video=0;$count_tag_video<=count($exp_tag_video);$count_tag_video++)
									{
										if($exp_tag_video[$count_tag_video]==""){continue;}
										else{
											if($exp_tag_video[$count_tag_video] == $exp_all_mediaid[$count_media])
											{
												$exp_tag_video[$count_tag_video]="";
											}
										}
									}
								}
							}
							for($count_media=0;$count_media<=count($exp_all_mediaid);$count_media++)
							{
								if($exp_all_mediaid[$count_media]==""){continue;}
								else{
									for($count_tag_gal=0;$count_tag_gal<=count($exp_tag_gallery);$count_tag_gal++)
									{
										if($exp_tag_gallery[$count_tag_gal]==""){continue;}
										else{
											if($exp_tag_gallery[$count_tag_gal] == $exp_all_mediaid[$count_media])
											{
												$exp_tag_gallery[$count_tag_gal]="";
											}
										}
									}
								}
							}
							$new_song_id="";
							for($count_song_id=0;$count_song_id<=count($exp_tag_song);$count_song_id++)
							{
								if($exp_tag_song[$count_song_id]!=""){
								$new_song_id = $new_song_id .",". $exp_tag_song[$count_song_id];}
							}
							$new_video_id="";
							for($count_video_id=0;$count_video_id<=count($exp_tag_video);$count_video_id++)
							{
								if($exp_tag_video[$count_video_id]!=""){
								$new_video_id = $new_video_id .",". $exp_tag_video[$count_video_id];}
							}
							$new_gallery_id="";
							for($count_gallery_id=0;$count_gallery_id<=count($exp_tag_gallery);$count_gallery_id++)
							{
								if($exp_tag_gallery[$count_gallery_id]!=""){
								$new_gallery_id = $new_gallery_id .",". $exp_tag_gallery[$count_gallery_id];}
							}
							mysql_query("update general_community set taggedsongs='".$new_song_id."', taggedvideos='".$new_video_id."', taggedgalleries='".$new_gallery_id."' where community_id='".$res_tag_com_det['community_id']."'");
						}
					}
					$get_tag_compro_det = mysql_query("select * from community_project where community_id='".$res_creator_in_det['community_id']."'");
					if(mysql_num_rows($get_tag_compro_det)>0)
					{
						while($res_tag_compro_det = mysql_fetch_assoc($get_tag_compro_det))
						{
							$exp_tag_song = explode(",",$res_tag_compro_det['tagged_songs']);
							$exp_tag_video = explode(",",$res_tag_compro_det['tagged_videos']);
							$exp_tag_gallery = explode(",",$res_tag_compro_det['tagged_galleries']);
							if($get_all_mediaid!="")
							{
								$exp_all_mediaid = explode(",",$get_all_mediaid);
								for($count_media=0;$count_media<=count($exp_all_mediaid);$count_media++)
								{
									if($exp_all_mediaid[$count_media]==""){continue;}
									else{
										for($count_tag=0;$count_tag<=count($exp_tag_song);$count_tag++)
										{
											if($exp_tag_song[$count_tag]==""){continue;}
											else{
												if($exp_tag_song[$count_tag] == $exp_all_mediaid[$count_media])
												{
													$exp_tag_song[$count_tag]="";
												}
											}
										}
									}
								}
								for($count_media=0;$count_media<=count($exp_all_mediaid);$count_media++)
								{
									if($exp_all_mediaid[$count_media]==""){continue;}
									else{
										for($count_tag_video=0;$count_tag_video<=count($exp_tag_video);$count_tag_video++)
										{
											if($exp_tag_video[$count_tag_video]==""){continue;}
											else{
												if($exp_tag_video[$count_tag_video] == $exp_all_mediaid[$count_media])
												{
													$exp_tag_video[$count_tag_video]="";
												}
											}
										}
									}
								}
								for($count_media=0;$count_media<=count($exp_all_mediaid);$count_media++)
								{
									if($exp_all_mediaid[$count_media]==""){continue;}
									else{
										for($count_tag_gal=0;$count_tag_gal<=count($exp_tag_gallery);$count_tag_gal++)
										{
											if($exp_tag_gallery[$count_tag_gal]==""){continue;}
											else{
												if($exp_tag_gallery[$count_tag_gal] == $exp_all_mediaid[$count_media])
												{
													$exp_tag_gallery[$count_tag_gal]="";
												}
											}
										}
									}
								}
								$new_song_id="";
								for($count_song_id=0;$count_song_id<=count($exp_tag_song);$count_song_id++)
								{
									if($exp_tag_song[$count_song_id]!=""){
									$new_song_id = $new_song_id .",". $exp_tag_song[$count_song_id];}
								}
								$new_video_id="";
								for($count_video_id=0;$count_video_id<=count($exp_tag_video);$count_video_id++)
								{
									if($exp_tag_video[$count_video_id]!=""){
									$new_video_id = $new_video_id .",". $exp_tag_video[$count_video_id];}
								}
								$new_gallery_id="";
								for($count_gallery_id=0;$count_gallery_id<=count($exp_tag_gallery);$count_gallery_id++)
								{
									if($exp_tag_gallery[$count_gallery_id]!=""){
									$new_gallery_id = $new_gallery_id .",". $exp_tag_gallery[$count_gallery_id];}
								}
								mysql_query("update community_project set tagged_songs='".$new_song_id."', tagged_videos='".$new_video_id."', tagged_galleries='".$new_gallery_id."' where id='".$res_tag_compro_det['id']."'");
							}
						}
					}
					$get_tag_comeve_det = mysql_query("select * from community_event where community_id='".$res_creator_in_det['community_id']."'");
					if(mysql_num_rows($get_tag_comeve_det)>0)
					{
						while($res_tag_comeve_det = mysql_fetch_assoc($get_tag_comeve_det))
						{
							$exp_tag_song = explode(",",$res_tag_comeve_det['tagged_songs']);
							$exp_tag_video = explode(",",$res_tag_comeve_det['tagged_videos']);
							$exp_tag_gallery = explode(",",$res_tag_comeve_det['tagged_galleries']);
							if($get_all_mediaid!="")
							{
								$exp_all_mediaid = explode(",",$get_all_mediaid);
								for($count_media=0;$count_media<=count($exp_all_mediaid);$count_media++)
								{
									if($exp_all_mediaid[$count_media]==""){continue;}
									else{
										for($count_tag=0;$count_tag<=count($exp_tag_song);$count_tag++)
										{
											if($exp_tag_song[$count_tag]==""){continue;}
											else{
												if($exp_tag_song[$count_tag] == $exp_all_mediaid[$count_media])
												{
													$exp_tag_song[$count_tag]="";
												}
											}
										}
									}
								}
								for($count_media=0;$count_media<=count($exp_all_mediaid);$count_media++)
								{
									if($exp_all_mediaid[$count_media]==""){continue;}
									else{
										for($count_tag_video=0;$count_tag_video<=count($exp_tag_video);$count_tag_video++)
										{
											if($exp_tag_video[$count_tag_video]==""){continue;}
											else{
												if($exp_tag_video[$count_tag_video] == $exp_all_mediaid[$count_media])
												{
													$exp_tag_video[$count_tag_video]="";
												}
											}
										}
									}
								}
								for($count_media=0;$count_media<=count($exp_all_mediaid);$count_media++)
								{
									if($exp_all_mediaid[$count_media]==""){continue;}
									else{
										for($count_tag_gal=0;$count_tag_gal<=count($exp_tag_gallery);$count_tag_gal++)
										{
											if($exp_tag_gallery[$count_tag_gal]==""){continue;}
											else{
												if($exp_tag_gallery[$count_tag_gal] == $exp_all_mediaid[$count_media])
												{
													$exp_tag_gallery[$count_tag_gal]="";
												}
											}
										}
									}
								}
								$new_song_id="";
								for($count_song_id=0;$count_song_id<=count($exp_tag_song);$count_song_id++)
								{
									if($exp_tag_song[$count_song_id]!=""){
									$new_song_id = $new_song_id .",". $exp_tag_song[$count_song_id];}
								}
								$new_video_id="";
								for($count_video_id=0;$count_video_id<=count($exp_tag_video);$count_video_id++)
								{
									if($exp_tag_video[$count_video_id]!=""){
									$new_video_id = $new_video_id .",". $exp_tag_video[$count_video_id];}
								}
								$new_gallery_id="";
								for($count_gallery_id=0;$count_gallery_id<=count($exp_tag_gallery);$count_gallery_id++)
								{
									if($exp_tag_gallery[$count_gallery_id]!=""){
									$new_gallery_id = $new_gallery_id .",". $exp_tag_gallery[$count_gallery_id];}
								}
								mysql_query("update community_event set tagged_songs='".$new_song_id."', tagged_videos='".$new_video_id."', tagged_galleries='".$new_gallery_id."' where id='".$res_tag_comeve_det['id']."'");
							}
						}
					}
				}
			}
		}
		$query = mysql_query("select * from general_media where creator_info = '".$creator_info."' OR from_info = '".$creator_info."'");
		if(mysql_num_rows($query)>0)
		{
			while($res_query = mysql_fetch_assoc($query))
			{
				if($res_query['creator_info']==$creator_info){
					mysql_query("update general_media set creator='', creator_info='' where id='".$res_query['id']."'");
				}if($res_query['from_info']==$creator_info){
					mysql_query("update general_media set from='', from_info='' where id='".$res_query['id']."'");
				}
			}
		}
	}
	
	function only_creator2pr_this()
	{
		$id = $this->selgeneral();
		$var_type_1 = 'general_artist';
		$var_type_2 = 'general_community';
		$var_type_3 = 'artist_project';
		$var_type_4 = 'community_project';
		
		$art_p_ao = array();
		$art_c_ao = array();
		
		$com_p_ao = array();
		$com_c_ao = array();
		
		$com_a_ao = array();
		$art_a_ao = array();
		
		$art_ap_ao = array();
		$com_cp_ao = array();
		
		$pal_1 = array();
		$pal_2 = array();
		
		if($id['artist_id']!=0 && !empty($id['artist_id']))
		{
			$chk_type = $var_type_1.'|'.$id['artist_id'];
			
			$sql_get_allp = mysql_query("SELECT * FROM artist_project WHERE artist_id='".$id['artist_id']."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_get_allp)>0)
			{
				while($rowal = mysql_fetch_assoc($sql_get_allp))
				{
					//$rowal['whos_project'] = "only_creator";
					$pal_1[] = $rowal;
				}
			}
			
			if(!empty($pal_1) && count($pal_1))
			{
				for($pl=0;$pl<count($pal_1);$pl++)
				{
					$chk_typep_1 = 'artist_project|'.$pal_1[$pl]['id'];
					
					$sql = mysql_query("SELECT * FROM artist_project WHERE (creators_info='".$chk_typep_1."' OR creators_info='".$chk_type."') AND del_status=0 AND active=0");
					if(mysql_num_rows($sql)>0)
					{
						while($row_po = mysql_fetch_assoc($sql))
						{
							//if($row_po['artist_id']!=$id['artist_id'])
							//{
								
								$sql_art2 = mysql_query("SELECT * FROM artist_project WHERE creators_info='artist_project|".$row_po['id']."' AND del_status=0 AND active=0");
								if(mysql_num_rows($sql_art2)>0)
								{
									while($row2_po = mysql_fetch_assoc($sql_art2))
									{
										$row2_po['id'] = $row2_po['id'].'~art';
										$row2_po['whos_project'] = "art_creator_creator_pro";
										$art_p_ao[] = $row2_po;
									}
								}
								
								
								$sql_art3 = mysql_query("SELECT * FROM community_project WHERE creators_info='artist_project|".$row_po['id']."' AND del_status=0 AND active=0");
								if(mysql_num_rows($sql_art3)>0)
								{
									while($row3_po = mysql_fetch_assoc($sql_art3))
									{
										$row3_po['id'] = $row3_po['id'].'~com';
										$row3_po['whos_project'] = "com_creator_creator_pro";
										$art_c_ao[] = $row3_po;
									}
								}
							//}
						}
					}
					
					
					$sql = mysql_query("SELECT * FROM community_project WHERE (creators_info='".$chk_typep_1."' OR creators_info='".$chk_type."') AND del_status=0 AND active=0");
					if(mysql_num_rows($sql)>0)
					{
						while($row_poo = mysql_fetch_assoc($sql))
						{
							//if($row_poo['community_id']!=$id['community_id'])
							//{		
								
								$sql_art2 = mysql_query("SELECT * FROM artist_project WHERE creators_info='community_project|".$row_poo['id']."' AND del_status=0 AND active=0");
								if(mysql_num_rows($sql_art2)>0)
								{
									while($row2_po = mysql_fetch_assoc($sql_art2))
									{
										$row2_po['id'] = $row2_po['id'].'~art';
										$row2_po['whos_project'] = "art_creator_creator_pro";
										$com_p_ao[] = $row2_po;
									}
								}
								
								
								$sql_art3 = mysql_query("SELECT * FROM community_project WHERE creators_info='community_project|".$row_poo['id']."' AND del_status=0 AND active=0");
								if(mysql_num_rows($sql_art3)>0)
								{
									while($row3_po = mysql_fetch_assoc($sql_art3))
									{
										$row3_po['id'] = $row3_po['id'].'~com';
										$row3_po['whos_project'] = "com_creator_creator_pro";
										$com_c_ao[] = $row3_po;
									}
								}
							//}
						}
					}
				}
			}
		}
		
		if($id['community_id']!=0 && !empty($id['community_id']))
		{
			$chk_type = $var_type_2.'|'.$id['community_id'];
			
			$sql_get_allp = mysql_query("SELECT * FROM community_project WHERE community_id='".$id['community_id']."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_get_allp)>0)
			{
				while($rowal = mysql_fetch_assoc($sql_get_allp))
				{
					//$rowal['whos_project'] = "only_creator";
					$pal_2[] = $rowal;
				}
			}
			
			if(!empty($pal_2) && count($pal_2))
			{
				for($pl2=0;$pl2<count($pal_2);$pl2++)
				{
					$chk_typep_2 = 'community_project|'.$pal_2[$pl2]['id'];
					
					
					$sql = mysql_query("SELECT * FROM artist_project WHERE (creators_info='".$chk_typep_2."' OR creators_info='".$chk_type."') AND del_status=0 AND active=0");
					if(mysql_num_rows($sql)>0)
					{
						while($row_po = mysql_fetch_assoc($sql))
						{
							//if($row_po['artist_id']!=$id['artist_id'])
							//{
								
								$sql_art2 = mysql_query("SELECT * FROM artist_project WHERE creators_info='artist_project|".$row_po['id']."' AND del_status=0 AND active=0");
								if(mysql_num_rows($sql_art2)>0)
								{
									while($row2_po = mysql_fetch_assoc($sql_art2))
									{
										$row2_po['id'] = $row2_po['id'].'~art';
										$row2_po['whos_project'] = "art_creator_creator_pro";
										$com_a_ao[] = $row2_po;
									}
								}
								
								
								$sql_art3 = mysql_query("SELECT * FROM community_project WHERE creators_info='artist_project|".$row_po['id']."' AND del_status=0 AND active=0");
								if(mysql_num_rows($sql_art3)>0)
								{
									while($row3_po = mysql_fetch_assoc($sql_art3))
									{
										$row3_po['id'] = $row3_po['id'].'~com';
										$row3_po['whos_project'] = "com_creator_creator_pro";
										$art_a_ao[] = $row3_po;
									}
								}
							//}
						}
					}
					
					
					$sql = mysql_query("SELECT * FROM community_project WHERE (creators_info='".$chk_typep_2."' OR creators_info='".$chk_type."') AND del_status=0 AND active=0");
					if(mysql_num_rows($sql)>0)
					{
						while($row_poo = mysql_fetch_assoc($sql))
						{
							//if($row_poo['community_id']!=$id['community_id'])
							//{
								
								$sql_art2 = mysql_query("SELECT * FROM artist_project WHERE creators_info='community_project|".$row_poo['id']."' AND del_status=0 AND active=0");
								if(mysql_num_rows($sql_art2)>0)
								{
									while($row2_po = mysql_fetch_assoc($sql_art2))
									{
										$row2_po['id'] = $row2_po['id'].'~art';
										$row2_po['whos_project'] = "art_creator_creator_pro";
										$art_ap_ao[] = $row2_po;
									}
								}
								
								
								$sql_art3 = mysql_query("SELECT * FROM community_project WHERE creators_info='community_project|".$row_poo['id']."' AND del_status=0 AND active=0");
								if(mysql_num_rows($sql_art3)>0)
								{
									while($row3_po = mysql_fetch_assoc($sql_art3))
									{
										$row3_po['id'] = $row3_po['id'].'~com';
										$row3_po['whos_project'] = "com_creator_creator_pro";
										$com_cp_ao[] = $row3_po;
									}
								}
							//}
						}
					}
				}
			}
		}		
		
		$totals = array();
		$totals = array_merge($art_p_ao,$art_c_ao,$com_p_ao,$com_c_ao,$com_a_ao,$art_a_ao,$art_ap_ao,$com_cp_ao);
		return $totals;
	}
	
	function only_creator2pr_othis()
	{
		$id = $this->selgeneral();
		$var_type_1 = 'general_artist';
		$var_type_2 = 'general_community';
		$var_type_3 = 'artist_project';
		$var_type_4 = 'community_project';
		
		$art_p_ao = array();
		$art_c_ao = array();
		
		$com_p_ao = array();
		$com_c_ao = array();
		
		$com_a_ao = array();
		$art_a_ao = array();
		
		$art_ap_ao = array();
		$com_cp_ao = array();
		
		$pal_1 = array();
		$pal_2 = array();
		
		if($id['artist_id']!=0 && !empty($id['artist_id']))
		{
			$chk_type = $var_type_1.'|'.$id['artist_id'];
			
			$sql_get_allp = mysql_query("SELECT * FROM artist_project WHERE artist_id='".$id['artist_id']."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_get_allp)>0)
			{
				while($rowal = mysql_fetch_assoc($sql_get_allp))
				{
					//$rowal['whos_project'] = "only_creator";
					$pal_1[] = $rowal;
				}
			}
			
			if(!empty($pal_1) && count($pal_1))
			{
				for($pl=0;$pl<count($pal_1);$pl++)
				{
					$chk_typep_1 = 'artist_project|'.$pal_1[$pl]['id'];
					
					$sql = mysql_query("SELECT * FROM artist_project WHERE (creators_info='".$chk_typep_1."' OR creators_info='".$chk_type."') AND del_status=0 AND active=0");
					if(mysql_num_rows($sql)>0)
					{
						while($row_po = mysql_fetch_assoc($sql))
						{
							//if($row_po['artist_id']!=$id['artist_id'])
							//{
								
								$sql_art2 = mysql_query("SELECT * FROM artist_project WHERE creators_info='artist_project|".$row_po['id']."' AND del_status=0 AND active=0");
								if(mysql_num_rows($sql_art2)>0)
								{
									while($row2_po = mysql_fetch_assoc($sql_art2))
									{
										if($row2_po['artist_id']!=$id['artist_id'])
										{
											$row2_po['id'] = $row2_po['id'].'~art';
											$row2_po['whos_project'] = "art_creator_creator_pro";
											$art_p_ao[] = $row2_po;
										}
									}
								}
								
								
								$sql_art3 = mysql_query("SELECT * FROM community_project WHERE creators_info='artist_project|".$row_po['id']."' AND del_status=0 AND active=0");
								if(mysql_num_rows($sql_art3)>0)
								{
									while($row3_po = mysql_fetch_assoc($sql_art3))
									{
										if($row3_po['community_id']!=$id['community_id'])
										{
											$row3_po['id'] = $row3_po['id'].'~com';
											$row3_po['whos_project'] = "com_creator_creator_pro";
											$art_c_ao[] = $row3_po;
										}
									}
								}
							//}
						}
					}
					
					
					$sql = mysql_query("SELECT * FROM community_project WHERE (creators_info='".$chk_typep_1."' OR creators_info='".$chk_type."') AND del_status=0 AND active=0");
					if(mysql_num_rows($sql)>0)
					{
						while($row_poo = mysql_fetch_assoc($sql))
						{
							//if($row_poo['community_id']!=$id['community_id'])
							//{		
								
								$sql_art2 = mysql_query("SELECT * FROM artist_project WHERE creators_info='community_project|".$row_poo['id']."' AND del_status=0 AND active=0");
								if(mysql_num_rows($sql_art2)>0)
								{
									while($row2_po = mysql_fetch_assoc($sql_art2))
									{
										if($row2_po['artist_id']!=$id['artist_id'])
										{
											$row2_po['id'] = $row2_po['id'].'~art';
											$row2_po['whos_project'] = "art_creator_creator_pro";
											$com_p_ao[] = $row2_po;
										}
									}
								}
								
								
								$sql_art3 = mysql_query("SELECT * FROM community_project WHERE creators_info='community_project|".$row_poo['id']."' AND del_status=0 AND active=0");
								if(mysql_num_rows($sql_art3)>0)
								{
									while($row3_po = mysql_fetch_assoc($sql_art3))
									{
										if($row3_po['community_id']!=$id['community_id'])
										{
											$row3_po['id'] = $row3_po['id'].'~com';
											$row3_po['whos_project'] = "com_creator_creator_pro";
											$com_c_ao[] = $row3_po;
										}
									}
								}
							//}
						}
					}
				}
			}
		}
		
		if($id['community_id']!=0 && !empty($id['community_id']))
		{
			$chk_type = $var_type_2.'|'.$id['community_id'];
			
			$sql_get_allp = mysql_query("SELECT * FROM community_project WHERE community_id='".$id['community_id']."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_get_allp)>0)
			{
				while($rowal = mysql_fetch_assoc($sql_get_allp))
				{
					//$rowal['whos_project'] = "only_creator";
					$pal_2[] = $rowal;
				}
			}
			
			if(!empty($pal_2) && count($pal_2))
			{
				for($pl2=0;$pl2<count($pal_2);$pl2++)
				{
					$chk_typep_2 = 'community_project|'.$pal_2[$pl2]['id'];
					
					
					$sql = mysql_query("SELECT * FROM artist_project WHERE (creators_info='".$chk_typep_2."' OR creators_info='".$chk_type."') AND del_status=0 AND active=0");
					if(mysql_num_rows($sql)>0)
					{
						while($row_po = mysql_fetch_assoc($sql))
						{
							//if($row_po['artist_id']!=$id['artist_id'])
							//{
								
								$sql_art2 = mysql_query("SELECT * FROM artist_project WHERE creators_info='artist_project|".$row_po['id']."' AND del_status=0 AND active=0");
								if(mysql_num_rows($sql_art2)>0)
								{
									while($row2_po = mysql_fetch_assoc($sql_art2))
									{
										if($row2_po['artist_id']!=$id['artist_id'])
										{
											$row2_po['id'] = $row2_po['id'].'~art';
											$row2_po['whos_project'] = "art_creator_creator_pro";
											$com_a_ao[] = $row2_po;
										}
									}
								}
								
								
								$sql_art3 = mysql_query("SELECT * FROM community_project WHERE creators_info='artist_project|".$row_po['id']."' AND del_status=0 AND active=0");
								if(mysql_num_rows($sql_art3)>0)
								{
									while($row3_po = mysql_fetch_assoc($sql_art3))
									{
										if($row3_po['community_id']!=$id['community_id'])
										{
											$row3_po['id'] = $row3_po['id'].'~com';
											$row3_po['whos_project'] = "com_creator_creator_pro";
											$art_a_ao[] = $row3_po;
										}
									}
								}
							//}
						}
					}
					
					
					$sql = mysql_query("SELECT * FROM community_project WHERE (creators_info='".$chk_typep_2."' OR creators_info='".$chk_type."') AND del_status=0 AND active=0");
					if(mysql_num_rows($sql)>0)
					{
						while($row_poo = mysql_fetch_assoc($sql))
						{
							//if($row_poo['community_id']!=$id['community_id'])
							//{
								
								$sql_art2 = mysql_query("SELECT * FROM artist_project WHERE creators_info='community_project|".$row_poo['id']."' AND del_status=0 AND active=0");
								if(mysql_num_rows($sql_art2)>0)
								{
									while($row2_po = mysql_fetch_assoc($sql_art2))
									{
										if($row2_po['artist_id']!=$id['artist_id'])
										{
											$row2_po['id'] = $row2_po['id'].'~art';
											$row2_po['whos_project'] = "art_creator_creator_pro";
											$art_ap_ao[] = $row2_po;
										}
									}
								}
								
								
								$sql_art3 = mysql_query("SELECT * FROM community_project WHERE creators_info='community_project|".$row_poo['id']."' AND del_status=0 AND active=0");
								if(mysql_num_rows($sql_art3)>0)
								{
									while($row3_po = mysql_fetch_assoc($sql_art3))
									{
										if($row3_po['community_id']!=$id['community_id'])
										{
											$row3_po['id'] = $row3_po['id'].'~com';
											$row3_po['whos_project'] = "com_creator_creator_pro";
											$com_cp_ao[] = $row3_po;
										}
									}
								}
							//}
						}
					}
				}
			}
		}		
		
		$totals = array();
		$totals = array_merge($art_p_ao,$art_c_ao,$com_p_ao,$com_c_ao,$com_a_ao,$art_a_ao,$art_ap_ao,$com_cp_ao);
		return $totals;
	}
	
	function only_sh_creator2pr_this()
	{
		$id = $this->selgeneral();
		$var_type_1 = 'general_artist';
		$var_type_2 = 'general_community';
		$var_type_3 = 'artist_project';
		$var_type_4 = 'community_project';
		
		$art_p_ao = array();
		$art_c_ao = array();
		
		$com_p_ao = array();
		$com_c_ao = array();
		
		$com_a_ao = array();
		$art_a_ao = array();
		
		$art_ap_ao = array();
		$com_cp_ao = array();
		
		$pal_1 = array();
		$pal_2 = array();
		
		if($id['artist_id']!=0 && !empty($id['artist_id']))
		{
			$chk_type = $var_type_1.'|'.$id['artist_id'];
			
			$sql_get_allp = mysql_query("SELECT * FROM artist_project WHERE artist_id='".$id['artist_id']."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_get_allp)>0)
			{
				while($rowal = mysql_fetch_assoc($sql_get_allp))
				{
					//$rowal['whos_project'] = "only_creator";
					$pal_1[] = $rowal;
				}
			}
			
			if(!empty($pal_1) && count($pal_1))
			{
				for($pl=0;$pl<count($pal_1);$pl++)
				{
					$chk_typep_1 = 'artist_project|'.$pal_1[$pl]['id'];
					
					$sql = mysql_query("SELECT * FROM artist_project WHERE (creators_info='".$chk_typep_1."' OR creators_info='".$chk_type."') AND del_status=0 AND active=0");
					if(mysql_num_rows($sql)>0)
					{
						while($row_po = mysql_fetch_assoc($sql))
						{
							//if($row_po['artist_id']!=$id['artist_id'])
							//{
								
								$sql_art2 = mysql_query("SELECT * FROM artist_project WHERE creators_info='artist_project|".$row_po['id']."' AND del_status=0 AND active=0");
								if(mysql_num_rows($sql_art2)>0)
								{
									while($row2_po = mysql_fetch_assoc($sql_art2))
									{
										$row2_po['table_name'] = 'artist_project';
										
										$sql = mysql_query("SELECT MIN( `id` ) FROM `artist_project_profiles` WHERE artist_project_id='".$row2_po['id']."' AND subtype_id!=0");

										if(mysql_num_rows($sql)>0)
										{
											$res = mysql_fetch_assoc($sql);
											
											$sql2 = mysql_query("select * from artist_project_profiles where id='".$res['MIN( `id` )']."'");
											$res2 = mysql_fetch_assoc($sql2);
											
											$sql4 = mysql_query("select * from  subtype where subtype_id = '".$res2['subtype_id']."'");
											if(mysql_num_rows($sql4)>0)
											{
												$res4 = mysql_fetch_assoc($sql4);
												$row2_po['type'] = $res4['name'];
											}
											else
											{
												$row2_po['type'] = '';
											}
										}
										else
										{
											$row2_po['type'] = '';
										}
										//$row_po['whos_project'] = "only_creator";
										$art_p_ao[] = $row2_po;
									}
								}
								
								
								$sql_art3 = mysql_query("SELECT * FROM community_project WHERE creators_info='artist_project|".$row_po['id']."' AND del_status=0 AND active=0");
								if(mysql_num_rows($sql_art3)>0)
								{
									while($row3_po = mysql_fetch_assoc($sql_art3))
									{
										$row3_po['table_name'] = 'community_project';
										$sql = mysql_query("SELECT MIN( `id` ) FROM `community_project_profiles` WHERE community_project_id='".$row3_po['id']."' AND subtype_id!=0");

										if(mysql_num_rows($sql)>0)
										{
											$res = mysql_fetch_assoc($sql);
											
											$sql2 = mysql_query("select * from community_project_profiles where id='".$res['MIN( `id` )']."'");
											$res2 = mysql_fetch_assoc($sql2);

											$sql4 = mysql_query("select * from  subtype where subtype_id = '".$res2['subtype_id']."'");
											if(mysql_num_rows($sql4)>0)
											{
												$res4 = mysql_fetch_assoc($sql4);
												$row3_po['type'] = $res4['name'];
											}
											else
											{
												$row3_po['type'] = '';
											}
										}
										else
										{
											$row3_po['type'] = '';
										}
										//$row_po['whos_project'] = "only_creator";
										$art_c_ao[] = $row3_po;
									}
								}
							//}
						}
					}
					
					
					$sql = mysql_query("SELECT * FROM community_project WHERE (creators_info='".$chk_typep_1."' OR creators_info='".$chk_type."') AND del_status=0 AND active=0");
					if(mysql_num_rows($sql)>0)
					{
						while($row_poo = mysql_fetch_assoc($sql))
						{
							//if($row_poo['community_id']!=$id['community_id'])
							//{		
								
								$sql_art2 = mysql_query("SELECT * FROM artist_project WHERE creators_info='community_project|".$row_poo['id']."' AND del_status=0 AND active=0");
								if(mysql_num_rows($sql_art2)>0)
								{
									while($row2_po = mysql_fetch_assoc($sql_art2))
									{
										$row2_po['table_name'] = 'artist_project';
										$sql = mysql_query("SELECT MIN( `id` ) FROM `artist_project_profiles` WHERE artist_project_id='".$row2_po['id']."' AND subtype_id!=0");

										if(mysql_num_rows($sql)>0)
										{
											$res = mysql_fetch_assoc($sql);
											
											$sql2 = mysql_query("select * from artist_project_profiles where id='".$res['MIN( `id` )']."'");
											$res2 = mysql_fetch_assoc($sql2);

											$sql4 = mysql_query("select * from  subtype where subtype_id = '".$res2['subtype_id']."'");
											if(mysql_num_rows($sql4)>0)
											{
												$res4 = mysql_fetch_assoc($sql4);
												$row2_po['type'] = $res4['name'];
											}
											else
											{
												$row2_po['type'] = '';
											}
										}
										else
										{
											$row2_po['type'] = '';
										}
										//$row_poo['whos_project'] = "only_creator";
										$com_p_ao[] = $row2_po;
									}
								}
								
								
								$sql_art3 = mysql_query("SELECT * FROM community_project WHERE creators_info='community_project|".$row_poo['id']."' AND del_status=0 AND active=0");
								if(mysql_num_rows($sql_art3)>0)
								{
									while($row3_po = mysql_fetch_assoc($sql_art3))
									{
										$row3_po['table_name'] = 'community_project';
										$sql = mysql_query("SELECT MIN( `id` ) FROM `community_project_profiles` WHERE community_project_id='".$row3_po['id']."' AND subtype_id!=0");

										if(mysql_num_rows($sql)>0)
										{
											$res = mysql_fetch_assoc($sql);
											
											$sql2 = mysql_query("select * from community_project_profiles where id='".$res['MIN( `id` )']."'");
											$res2 = mysql_fetch_assoc($sql2);

											$sql4 = mysql_query("select * from  subtype where subtype_id = '".$res2['subtype_id']."'");
											if(mysql_num_rows($sql4)>0)
											{
												$res4 = mysql_fetch_assoc($sql4);
												$row3_po['type'] = $res4['name'];
											}
											else
											{
												$row3_po['type'] = '';
											}
										}
										else
										{
											$row3_po['type'] = '';
										}
										//$row_poo['whos_project'] = "only_creator";
										$com_c_ao[] = $row3_po;
									}
								}
							//}
						}
					}
				}
			}
		}
		
		if($id['community_id']!=0 && !empty($id['community_id']))
		{
			$chk_type = $var_type_2.'|'.$id['community_id'];
			
			$sql_get_allp = mysql_query("SELECT * FROM community_project WHERE community_id='".$id['community_id']."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_get_allp)>0)
			{
				while($rowal = mysql_fetch_assoc($sql_get_allp))
				{
					//$rowal['whos_project'] = "only_creator";
					$pal_2[] = $rowal;
				}
			}
			
			if(!empty($pal_2) && count($pal_2))
			{
				for($pl2=0;$pl2<count($pal_2);$pl2++)
				{
					$chk_typep_2 = 'community_project|'.$pal_2[$pl2]['id'];
					
					$sql = mysql_query("SELECT * FROM artist_project WHERE (creators_info='".$chk_typep_2."' OR creators_info='".$chk_type."') AND del_status=0 AND active=0");
					if(mysql_num_rows($sql)>0)
					{
						while($row_po = mysql_fetch_assoc($sql))
						{
							//if($row_po['artist_id']!=$id['artist_id'])
							//{
								
								$sql_art2 = mysql_query("SELECT * FROM artist_project WHERE creators_info='artist_project|".$row_po['id']."' AND del_status=0 AND active=0");
								if(mysql_num_rows($sql_art2)>0)
								{
									while($row2_po = mysql_fetch_assoc($sql_art2))
									{
										$row2_po['table_name'] = 'artist_project';
										
										$sql = mysql_query("SELECT MIN( `id` ) FROM `artist_project_profiles` WHERE artist_project_id='".$row2_po['id']."' AND subtype_id!=0");

										if(mysql_num_rows($sql)>0)
										{
											$res = mysql_fetch_assoc($sql);
											
											$sql2 = mysql_query("select * from artist_project_profiles where id='".$res['MIN( `id` )']."'");
											$res2 = mysql_fetch_assoc($sql2);

											$sql4 = mysql_query("select * from  subtype where subtype_id = '".$res2['subtype_id']."'");
											if(mysql_num_rows($sql4)>0)
											{
												$res4 = mysql_fetch_assoc($sql4);
												$row2_po['type'] = $res4['name'];
											}
											else
											{
												$row2_po['type'] = '';
											}
										}
										else
										{
											$row2_po['type'] = '';
										}
										//$row_po['whos_project'] = "only_creator";
										$com_a_ao[] = $row2_po;
									}
								}
								
								
								$sql_art3 = mysql_query("SELECT * FROM community_project WHERE creators_info='artist_project|".$row_po['id']."' AND del_status=0 AND active=0");
								if(mysql_num_rows($sql_art3)>0)
								{
									while($row3_po = mysql_fetch_assoc($sql_art3))
									{
										$row3_po['table_name'] = 'community_project';
										$sql = mysql_query("SELECT MIN( `id` ) FROM `community_project_profiles` WHERE community_project_id='".$row3_po['id']."' AND subtype_id!=0");

										if(mysql_num_rows($sql)>0)
										{
											$res = mysql_fetch_assoc($sql);
											
											$sql2 = mysql_query("select * from community_project_profiles where id='".$res['MIN( `id` )']."'");
											$res2 = mysql_fetch_assoc($sql2);

											$sql4 = mysql_query("select * from  subtype where subtype_id = '".$res2['subtype_id']."'");
											if(mysql_num_rows($sql4)>0)
											{
												$res4 = mysql_fetch_assoc($sql4);
												$row3_po['type'] = $res4['name'];
											}
											else
											{
												$row3_po['type'] = '';
											}
										}
										else
										{
											$row3_po['type'] = '';
										}
										//$row_po['whos_project'] = "only_creator";
										$art_a_ao[] = $row3_po;
									}
								}
							//}
						}
					}
					
					$sql = mysql_query("SELECT * FROM community_project WHERE (creators_info='".$chk_typep_2."' OR creators_info='".$chk_type."') AND del_status=0 AND active=0");
					if(mysql_num_rows($sql)>0)
					{
						while($row_poo = mysql_fetch_assoc($sql))
						{
							//if($row_poo['community_id']!=$id['community_id'])
							//{
								$sql_art2 = mysql_query("SELECT * FROM artist_project WHERE creators_info='community_project|".$row_poo['id']."' AND del_status=0 AND active=0");
								if(mysql_num_rows($sql_art2)>0)
								{
									while($row2_po = mysql_fetch_assoc($sql_art2))
									{
										$row2_po['table_name'] = 'artist_project';

										$sql = mysql_query("SELECT MIN( `id` ) FROM `artist_project_profiles` WHERE artist_project_id='".$row2_po['id']."' AND subtype_id!=0");

										if(mysql_num_rows($sql)>0)
										{
											$res = mysql_fetch_assoc($sql);
											
											$sql2 = mysql_query("select * from artist_project_profiles where id='".$res['MIN( `id` )']."'");
											$res2 = mysql_fetch_assoc($sql2);

											$sql4 = mysql_query("select * from  subtype where subtype_id = '".$res2['subtype_id']."'");
											if(mysql_num_rows($sql4)>0)
											{
												$res4 = mysql_fetch_assoc($sql4);
												$row2_po['type'] = $res4['name'];
											}
											else
											{
												$row2_po['type'] = '';
											}
										}
										else
										{
											$row2_po['type'] = '';
										}
										//$row_poo['whos_project'] = "only_creator";
										$art_ap_ao[] = $row2_po;
									}
								}
								
								
								$sql_art3 = mysql_query("SELECT * FROM community_project WHERE creators_info='community_project|".$row_poo['id']."' AND del_status=0 AND active=0");
								if(mysql_num_rows($sql_art3)>0)
								{
									while($row3_po = mysql_fetch_assoc($sql_art3))
									{
										$row3_po['table_name'] = 'community_project';
										$sql = mysql_query("SELECT MIN( `id` ) FROM `community_project_profiles` WHERE community_project_id='".$row3_po['id']."' AND subtype_id!=0");

										if(mysql_num_rows($sql)>0)
										{
											$res = mysql_fetch_assoc($sql);
											
											$sql2 = mysql_query("select * from community_project_profiles where id='".$res['MIN( `id` )']."'");
											$res2 = mysql_fetch_assoc($sql2);

											$sql4 = mysql_query("select * from  subtype where subtype_id = '".$res2['subtype_id']."'");
											if(mysql_num_rows($sql4)>0)
											{
												$res4 = mysql_fetch_assoc($sql4);
												$row3_po['type'] = $res4['name'];
											}
											else
											{
												$row3_po['type'] = '';
											}
										}
										else
										{
											$row3_po['type'] = '';
										}
										//$row_poo['whos_project'] = "only_creator";
										$com_cp_ao[] = $row3_po;
									}
								}
							//}
						}
					}
				}
			}
		}		
		
		$totals = array();
		$totals = array_merge($art_p_ao,$art_c_ao,$com_p_ao,$com_c_ao,$com_a_ao,$art_a_ao,$art_ap_ao,$com_cp_ao);
		return $totals;
	}
	
	function only_creator2ev_upevent()
	{
		$id = $this->selgeneral();
		$date = date("Y-m-d");
		
		$var_type_1 = 'general_artist';
		$var_type_2 = 'general_community';
		$var_type_3 = 'artist_project';
		$var_type_4 = 'community_project';
		
		$art_p_ao = array();
		$art_c_ao = array();
		
		$com_p_ao = array();
		$com_c_ao = array();
		
		$com_a_ao = array();
		$art_a_ao = array();
		
		$art_ap_ao = array();
		$com_cp_ao = array();
		
		$pal_1 = array();
		$pal_2 = array();
		
		if($id['artist_id']!=0 && !empty($id['artist_id']))
		{
			$chk_type = $var_type_1.'|'.$id['artist_id'];
			
			$sql_get_allp = mysql_query("SELECT * FROM artist_project WHERE artist_id='".$id['artist_id']."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_get_allp)>0)
			{
				while($rowal = mysql_fetch_assoc($sql_get_allp))
				{
					//$rowal['whos_project'] = "only_creator";
					$pal_1[] = $rowal;
				}
			}
			
			if(!empty($pal_1) && count($pal_1))
			{
				for($pl=0;$pl<count($pal_1);$pl++)
				{
					$chk_typep_1 = 'artist_project|'.$pal_1[$pl]['id'];
					
					$sql = mysql_query("SELECT * FROM artist_project WHERE (creators_info='".$chk_typep_1."' OR creators_info='".$chk_type."') AND del_status=0 AND active=0");
					if(mysql_num_rows($sql)>0)
					{
						while($row_po = mysql_fetch_assoc($sql))
						{
							//if($row_po['artist_id']!=$id['artist_id'])
							//{
								
								$sql_art2 = mysql_query("SELECT * FROM artist_event WHERE creators_info='artist_project|".$row_po['id']."' AND del_status=0 AND active=0 AND date>='".$date."'");
								if(mysql_num_rows($sql_art2)>0)
								{
									while($row2_po = mysql_fetch_assoc($sql_art2))
									{
										$row2_po['id'] = $row2_po['id'].'~art';
										$row2_po['whos_event'] = "art_creator_creator_eve";
										$art_p_ao[] = $row2_po;
									}
								}
								
								
								$sql_art3 = mysql_query("SELECT * FROM community_event WHERE creators_info='artist_project|".$row_po['id']."' AND del_status=0 AND active=0 AND date>='".$date."'");
								if(mysql_num_rows($sql_art3)>0)
								{
									while($row3_po = mysql_fetch_assoc($sql_art3))
									{
										$row3_po['id'] = $row3_po['id'].'~com';
										$row3_po['whos_event'] = "com_creator_creator_eve";
										$art_c_ao[] = $row3_po;
									}
								}
							//}
						}
					}
					
					
					$sql = mysql_query("SELECT * FROM community_project WHERE (creators_info='".$chk_typep_1."' OR creators_info='".$chk_type."') AND del_status=0 AND active=0");
					if(mysql_num_rows($sql)>0)
					{
						while($row_poo = mysql_fetch_assoc($sql))
						{
							//if($row_poo['community_id']!=$id['community_id'])
							//{		
								
								$sql_art2 = mysql_query("SELECT * FROM artist_event WHERE creators_info='community_project|".$row_poo['id']."' AND del_status=0 AND active=0 AND date>='".$date."'");
								if(mysql_num_rows($sql_art2)>0)
								{
									while($row2_po = mysql_fetch_assoc($sql_art2))
									{
										$row2_po['id'] = $row2_po['id'].'~art';
										$row2_po['whos_event'] = "art_creator_creator_eve";
										$com_p_ao[] = $row2_po;
									}
								}
								
								
								$sql_art3 = mysql_query("SELECT * FROM community_event WHERE creators_info='community_project|".$row_poo['id']."' AND del_status=0 AND active=0 AND date>='".$date."'");
								if(mysql_num_rows($sql_art3)>0)
								{
									while($row3_po = mysql_fetch_assoc($sql_art3))
									{
										$row3_po['id'] = $row3_po['id'].'~com';
										$row3_po['whos_event'] = "com_creator_creator_eve";
										$com_c_ao[] = $row3_po;
									}
								}
							//}
						}
					}
				}
			}
		}
		
		if($id['community_id']!=0 && !empty($id['community_id']))
		{
			$chk_type = $var_type_2.'|'.$id['community_id'];
			
			$sql_get_allp = mysql_query("SELECT * FROM community_project WHERE community_id='".$id['community_id']."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_get_allp)>0)
			{
				while($rowal = mysql_fetch_assoc($sql_get_allp))
				{
					//$rowal['whos_project'] = "only_creator";
					$pal_2[] = $rowal;
				}
			}
			
			if(!empty($pal_2) && count($pal_2))
			{
				for($pl2=0;$pl2<count($pal_2);$pl2++)
				{
					$chk_typep_2 = 'community_project|'.$pal_2[$pl2]['id'];
					
					
					$sql = mysql_query("SELECT * FROM artist_project WHERE (creators_info='".$chk_typep_2."' OR creators_info='".$chk_type."') AND del_status=0 AND active=0");
					if(mysql_num_rows($sql)>0)
					{
						while($row_po = mysql_fetch_assoc($sql))
						{
							//if($row_po['artist_id']!=$id['artist_id'])
							//{
								
								$sql_art2 = mysql_query("SELECT * FROM artist_event WHERE creators_info='artist_project|".$row_po['id']."' AND del_status=0 AND active=0 AND date>='".$date."'");
								if(mysql_num_rows($sql_art2)>0)
								{
									while($row2_po = mysql_fetch_assoc($sql_art2))
									{
										$row2_po['id'] = $row2_po['id'].'~art';
										$row2_po['whos_event'] = "art_creator_creator_eve";
										$com_a_ao[] = $row2_po;
									}
								}
								
								
								$sql_art3 = mysql_query("SELECT * FROM community_event WHERE creators_info='artist_project|".$row_po['id']."' AND del_status=0 AND active=0 AND date>='".$date."'");
								if(mysql_num_rows($sql_art3)>0)
								{
									while($row3_po = mysql_fetch_assoc($sql_art3))
									{
										$row3_po['id'] = $row3_po['id'].'~com';
										$row3_po['whos_event'] = "com_creator_creator_eve";
										$art_a_ao[] = $row3_po;
									}
								}
							//}
						}
					}
					
					
					$sql = mysql_query("SELECT * FROM community_project WHERE (creators_info='".$chk_typep_2."' OR creators_info='".$chk_type."') AND del_status=0 AND active=0");
					if(mysql_num_rows($sql)>0)
					{
						while($row_poo = mysql_fetch_assoc($sql))
						{
							//if($row_poo['community_id']!=$id['community_id'])
							//{
								
								$sql_art2 = mysql_query("SELECT * FROM artist_event WHERE creators_info='community_project|".$row_poo['id']."' AND del_status=0 AND active=0 AND date>='".$date."'");
								if(mysql_num_rows($sql_art2)>0)
								{
									while($row2_po = mysql_fetch_assoc($sql_art2))
									{
										$row2_po['id'] = $row2_po['id'].'~art';
										$row2_po['whos_event'] = "art_creator_creator_eve";
										$art_ap_ao[] = $row2_po;
									}
								}
								
								
								$sql_art3 = mysql_query("SELECT * FROM community_event WHERE creators_info='community_project|".$row_poo['id']."' AND del_status=0 AND active=0 AND date>='".$date."'");
								if(mysql_num_rows($sql_art3)>0)
								{
									while($row3_po = mysql_fetch_assoc($sql_art3))
									{
										$row3_po['id'] = $row3_po['id'].'~com';
										$row3_po['whos_event'] = "com_creator_creator_eve";
										$com_cp_ao[] = $row3_po;
									}
								}
							//}
						}
					}
				}
			}
		}		
		
		$totals = array();
		$totals = array_merge($art_p_ao,$art_c_ao,$com_p_ao,$com_c_ao,$com_a_ao,$art_a_ao,$art_ap_ao,$com_cp_ao);
		return $totals;
	}
	
	function only_creator2ev_oupevent()
	{
		$id = $this->selgeneral();
		$date = date("Y-m-d");
		
		$var_type_1 = 'general_artist';
		$var_type_2 = 'general_community';
		$var_type_3 = 'artist_project';
		$var_type_4 = 'community_project';
		
		$art_p_ao = array();
		$art_c_ao = array();
		
		$com_p_ao = array();
		$com_c_ao = array();
		
		$com_a_ao = array();
		$art_a_ao = array();
		
		$art_ap_ao = array();
		$com_cp_ao = array();
		
		$pal_1 = array();
		$pal_2 = array();
		
		if($id['artist_id']!=0 && !empty($id['artist_id']))
		{
			$chk_type = $var_type_1.'|'.$id['artist_id'];
			
			$sql_get_allp = mysql_query("SELECT * FROM artist_project WHERE artist_id='".$id['artist_id']."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_get_allp)>0)
			{
				while($rowal = mysql_fetch_assoc($sql_get_allp))
				{
					//$rowal['whos_project'] = "only_creator";
					$pal_1[] = $rowal;
				}
			}
			
			if(!empty($pal_1) && count($pal_1))
			{
				for($pl=0;$pl<count($pal_1);$pl++)
				{
					$chk_typep_1 = 'artist_project|'.$pal_1[$pl]['id'];
					
					$sql = mysql_query("SELECT * FROM artist_project WHERE (creators_info='".$chk_typep_1."' OR creators_info='".$chk_type."') AND del_status=0 AND active=0");
					if(mysql_num_rows($sql)>0)
					{
						while($row_po = mysql_fetch_assoc($sql))
						{
							//if($row_po['artist_id']!=$id['artist_id'])
							//{
								
								$sql_art2 = mysql_query("SELECT * FROM artist_event WHERE creators_info='artist_project|".$row_po['id']."' AND del_status=0 AND active=0 AND date>='".$date."'");
								if(mysql_num_rows($sql_art2)>0)
								{
									while($row2_po = mysql_fetch_assoc($sql_art2))
									{
										if($row2_po['artist_id']!=$id['artist_id'])
										{
											$row2_po['id'] = $row2_po['id'].'~art';
											$row2_po['whos_event'] = "art_creator_creator_eve";
											$art_p_ao[] = $row2_po;
										}
									}
								}
								
								
								$sql_art3 = mysql_query("SELECT * FROM community_event WHERE creators_info='artist_project|".$row_po['id']."' AND del_status=0 AND active=0 AND date>='".$date."'");
								if(mysql_num_rows($sql_art3)>0)
								{
									while($row3_po = mysql_fetch_assoc($sql_art3))
									{
										if($row3_po['community_id']!=$id['community_id'])
										{
											$row3_po['id'] = $row3_po['id'].'~com';
											$row3_po['whos_event'] = "com_creator_creator_eve";
											$art_c_ao[] = $row3_po;
										}
									}
								}
							//}
						}
					}
					
					
					$sql = mysql_query("SELECT * FROM community_project WHERE (creators_info='".$chk_typep_1."' OR creators_info='".$chk_type."') AND del_status=0 AND active=0");
					if(mysql_num_rows($sql)>0)
					{
						while($row_poo = mysql_fetch_assoc($sql))
						{
							//if($row_poo['community_id']!=$id['community_id'])
							//{		
								
								$sql_art2 = mysql_query("SELECT * FROM artist_event WHERE creators_info='community_project|".$row_poo['id']."' AND del_status=0 AND active=0 AND date>='".$date."'");
								if(mysql_num_rows($sql_art2)>0)
								{
									while($row2_po = mysql_fetch_assoc($sql_art2))
									{
										if($row2_po['artist_id']!=$id['artist_id'])
										{
											$row2_po['id'] = $row2_po['id'].'~art';
											$row2_po['whos_event'] = "art_creator_creator_eve";
											$com_p_ao[] = $row2_po;
										}
									}
								}
								
								
								$sql_art3 = mysql_query("SELECT * FROM community_event WHERE creators_info='community_project|".$row_poo['id']."' AND del_status=0 AND active=0 AND date>='".$date."'");
								if(mysql_num_rows($sql_art3)>0)
								{
									while($row3_po = mysql_fetch_assoc($sql_art3))
									{
										if($row3_po['community_id']!=$id['community_id'])
										{
											$row3_po['id'] = $row3_po['id'].'~com';
											$row3_po['whos_event'] = "com_creator_creator_eve";
											$com_c_ao[] = $row3_po;
										}
									}
								}
							//}
						}
					}
				}
			}
		}
		
		if($id['community_id']!=0 && !empty($id['community_id']))
		{
			$chk_type = $var_type_2.'|'.$id['community_id'];
			
			$sql_get_allp = mysql_query("SELECT * FROM community_project WHERE community_id='".$id['community_id']."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_get_allp)>0)
			{
				while($rowal = mysql_fetch_assoc($sql_get_allp))
				{
					//$rowal['whos_project'] = "only_creator";
					$pal_2[] = $rowal;
				}
			}
			
			if(!empty($pal_2) && count($pal_2))
			{
				for($pl2=0;$pl2<count($pal_2);$pl2++)
				{
					$chk_typep_2 = 'community_project|'.$pal_2[$pl2]['id'];
					
					
					$sql = mysql_query("SELECT * FROM artist_project WHERE (creators_info='".$chk_typep_2."' OR creators_info='".$chk_type."') AND del_status=0 AND active=0");
					if(mysql_num_rows($sql)>0)
					{
						while($row_po = mysql_fetch_assoc($sql))
						{
							//if($row_po['artist_id']!=$id['artist_id'])
							//{
								
								$sql_art2 = mysql_query("SELECT * FROM artist_event WHERE creators_info='artist_project|".$row_po['id']."' AND del_status=0 AND active=0 AND date>='".$date."'");
								if(mysql_num_rows($sql_art2)>0)
								{
									while($row2_po = mysql_fetch_assoc($sql_art2))
									{
										if($row2_po['artist_id']!=$id['artist_id'])
										{
											$row2_po['id'] = $row2_po['id'].'~art';
											$row2_po['whos_event'] = "art_creator_creator_eve";
											$com_a_ao[] = $row2_po;
										}
									}
								}
								
								
								$sql_art3 = mysql_query("SELECT * FROM community_event WHERE creators_info='artist_project|".$row_po['id']."' AND del_status=0 AND active=0 AND date>='".$date."'");
								if(mysql_num_rows($sql_art3)>0)
								{
									while($row3_po = mysql_fetch_assoc($sql_art3))
									{
										if($row3_po['community_id']!=$id['community_id'])
										{
											$row3_po['id'] = $row3_po['id'].'~com';
											$row3_po['whos_event'] = "com_creator_creator_eve";
											$art_a_ao[] = $row3_po;
										}
									}
								}
							//}
						}
					}
					
					
					$sql = mysql_query("SELECT * FROM community_project WHERE (creators_info='".$chk_typep_2."' OR creators_info='".$chk_type."') AND del_status=0 AND active=0");
					if(mysql_num_rows($sql)>0)
					{
						while($row_poo = mysql_fetch_assoc($sql))
						{
							//if($row_poo['community_id']!=$id['community_id'])
							//{
								
								$sql_art2 = mysql_query("SELECT * FROM artist_event WHERE creators_info='community_project|".$row_poo['id']."' AND del_status=0 AND active=0 AND date>='".$date."'");
								if(mysql_num_rows($sql_art2)>0)
								{
									while($row2_po = mysql_fetch_assoc($sql_art2))
									{
										if($row2_po['artist_id']!=$id['artist_id'])
										{
											$row2_po['id'] = $row2_po['id'].'~art';
											$row2_po['whos_event'] = "art_creator_creator_eve";
											$art_ap_ao[] = $row2_po;
										}
									}
								}
								
								
								$sql_art3 = mysql_query("SELECT * FROM community_event WHERE creators_info='community_project|".$row_poo['id']."' AND del_status=0 AND active=0 AND date>='".$date."'");
								if(mysql_num_rows($sql_art3)>0)
								{
									while($row3_po = mysql_fetch_assoc($sql_art3))
									{
										if($row3_po['community_id']!=$id['community_id'])
										{
											$row3_po['id'] = $row3_po['id'].'~com';
											$row3_po['whos_event'] = "com_creator_creator_eve";
											$com_cp_ao[] = $row3_po;
										}
									}
								}
							//}
						}
					}
				}
			}
		}		
		
		$totals = array();
		$totals = array_merge($art_p_ao,$art_c_ao,$com_p_ao,$com_c_ao,$com_a_ao,$art_a_ao,$art_ap_ao,$com_cp_ao);
		return $totals;
	}
	
	function only_creator2ev_calup($d,$m,$y)
	{
		$id = $this->selgeneral();
		$date = date("Y-m-d");
		
		$var_type_1 = 'general_artist';
		$var_type_2 = 'general_community';
		$var_type_3 = 'artist_project';
		$var_type_4 = 'community_project';
		
		$art_p_ao = array();
		$art_c_ao = array();
		
		$com_p_ao = array();
		$com_c_ao = array();
		
		$com_a_ao = array();
		$art_a_ao = array();
		
		$art_ap_ao = array();
		$com_cp_ao = array();
		
		$pal_1 = array();
		$pal_2 = array();
		
		if($id['artist_id']!=0 && !empty($id['artist_id']))
		{
			$chk_type = $var_type_1.'|'.$id['artist_id'];
			
			$sql_get_allp = mysql_query("SELECT * FROM artist_project WHERE artist_id='".$id['artist_id']."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_get_allp)>0)
			{
				while($rowal = mysql_fetch_assoc($sql_get_allp))
				{
					//$rowal['whos_project'] = "only_creator";
					$pal_1[] = $rowal;
				}
			}
			
			if(!empty($pal_1) && count($pal_1))
			{
				for($pl=0;$pl<count($pal_1);$pl++)
				{
					$chk_typep_1 = 'artist_project|'.$pal_1[$pl]['id'];
					
					$sql = mysql_query("SELECT * FROM artist_project WHERE (creators_info='".$chk_typep_1."' OR creators_info='".$chk_type."') AND del_status=0 AND active=0");
					if(mysql_num_rows($sql)>0)
					{
						while($row_po = mysql_fetch_assoc($sql))
						{
							//if($row_po['artist_id']!=$id['artist_id'])
							//{
								
								$sql_art2 = mysql_query("SELECT * FROM artist_event WHERE creators_info='artist_project|".$row_po['id']."' AND del_status=0 AND active=0 AND month(date) = ".$m."
	        AND dayofmonth(date) = ".$d." AND
	        year(date)= ".$y."");
								if(mysql_num_rows($sql_art2)>0)
								{
									while($row2_po = mysql_fetch_assoc($sql_art2))
									{
									//	$row2_po['id'] = $row2_po['id'].'~art';
										$row2_po['whos_event'] = "art_creator_creator_eve";
										$art_p_ao[] = $row2_po;
									}
								}
								
								
								$sql_art3 = mysql_query("SELECT * FROM community_event WHERE creators_info='artist_project|".$row_po['id']."' AND del_status=0 AND active=0 AND month(date) = ".$m."
	        AND dayofmonth(date) = ".$d." AND
	        year(date)= ".$y."");
								if(mysql_num_rows($sql_art3)>0)
								{
									while($row3_po = mysql_fetch_assoc($sql_art3))
									{
									//	$row3_po['id'] = $row3_po['id'].'~com';
										$row3_po['whos_event'] = "com_creator_creator_eve";
										$art_c_ao[] = $row3_po;
									}
								}
							//}
						}
					}
					
					
					$sql = mysql_query("SELECT * FROM community_project WHERE (creators_info='".$chk_typep_1."' OR creators_info='".$chk_type."') AND del_status=0 AND active=0");
					if(mysql_num_rows($sql)>0)
					{
						while($row_poo = mysql_fetch_assoc($sql))
						{
							//if($row_poo['community_id']!=$id['community_id'])
							//{		
								
								$sql_art2 = mysql_query("SELECT * FROM artist_event WHERE creators_info='community_project|".$row_poo['id']."' AND del_status=0 AND active=0 AND month(date) = ".$m."
	        AND dayofmonth(date) = ".$d." AND
	        year(date)= ".$y."");
								if(mysql_num_rows($sql_art2)>0)
								{
									while($row2_po = mysql_fetch_assoc($sql_art2))
									{
									//	$row2_po['id'] = $row2_po['id'].'~art';
										$row2_po['whos_event'] = "art_creator_creator_eve";
										$com_p_ao[] = $row2_po;
									}
								}
								
								
								$sql_art3 = mysql_query("SELECT * FROM community_event WHERE creators_info='community_project|".$row_poo['id']."' AND del_status=0 AND active=0 AND month(date) = ".$m."
	        AND dayofmonth(date) = ".$d." AND
	        year(date)= ".$y."");
								if(mysql_num_rows($sql_art3)>0)
								{
									while($row3_po = mysql_fetch_assoc($sql_art3))
									{
									//	$row3_po['id'] = $row3_po['id'].'~com';
										$row3_po['whos_event'] = "com_creator_creator_eve";
										$com_c_ao[] = $row3_po;
									}
								}
							//}
						}
					}
				}
			}
		}
		
		if($id['community_id']!=0 && !empty($id['community_id']))
		{
			$chk_type = $var_type_2.'|'.$id['community_id'];
			
			$sql_get_allp = mysql_query("SELECT * FROM community_project WHERE community_id='".$id['community_id']."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_get_allp)>0)
			{
				while($rowal = mysql_fetch_assoc($sql_get_allp))
				{
					//$rowal['whos_project'] = "only_creator";
					$pal_2[] = $rowal;
				}
			}
			
			if(!empty($pal_2) && count($pal_2))
			{
				for($pl2=0;$pl2<count($pal_2);$pl2++)
				{
					$chk_typep_2 = 'community_project|'.$pal_2[$pl2]['id'];
					
					
					$sql = mysql_query("SELECT * FROM artist_project WHERE (creators_info='".$chk_typep_2."' OR creators_info='".$chk_type."') AND del_status=0 AND active=0");
					if(mysql_num_rows($sql)>0)
					{
						while($row_po = mysql_fetch_assoc($sql))
						{
							//if($row_po['artist_id']!=$id['artist_id'])
							//{
								
								$sql_art2 = mysql_query("SELECT * FROM artist_event WHERE creators_info='artist_project|".$row_po['id']."' AND del_status=0 AND active=0 AND month(date) = ".$m."
	        AND dayofmonth(date) = ".$d." AND
	        year(date)= ".$y."");
								if(mysql_num_rows($sql_art2)>0)
								{
									while($row2_po = mysql_fetch_assoc($sql_art2))
									{
									//	$row2_po['id'] = $row2_po['id'].'~art';
										$row2_po['whos_event'] = "art_creator_creator_eve";
										$com_a_ao[] = $row2_po;
									}
								}
								
								
								$sql_art3 = mysql_query("SELECT * FROM community_event WHERE creators_info='artist_project|".$row_po['id']."' AND del_status=0 AND active=0 AND month(date) = ".$m."
	        AND dayofmonth(date) = ".$d." AND
	        year(date)= ".$y."");
								if(mysql_num_rows($sql_art3)>0)
								{
									while($row3_po = mysql_fetch_assoc($sql_art3))
									{
									//	$row3_po['id'] = $row3_po['id'].'~com';
										$row3_po['whos_event'] = "com_creator_creator_eve";
										$art_a_ao[] = $row3_po;
									}
								}
							//}
						}
					}
					
					
					$sql = mysql_query("SELECT * FROM community_project WHERE (creators_info='".$chk_typep_2."' OR creators_info='".$chk_type."') AND del_status=0 AND active=0");
					if(mysql_num_rows($sql)>0)
					{
						while($row_poo = mysql_fetch_assoc($sql))
						{
							//if($row_poo['community_id']!=$id['community_id'])
							//{
								
								$sql_art2 = mysql_query("SELECT * FROM artist_event WHERE creators_info='community_project|".$row_poo['id']."' AND del_status=0 AND active=0 AND month(date) = ".$m."
	        AND dayofmonth(date) = ".$d." AND
	        year(date)= ".$y."");
								if(mysql_num_rows($sql_art2)>0)
								{
									while($row2_po = mysql_fetch_assoc($sql_art2))
									{
									//	$row2_po['id'] = $row2_po['id'].'~art';
										$row2_po['whos_event'] = "art_creator_creator_eve";
										$art_ap_ao[] = $row2_po;
									}
								}
								
								
								$sql_art3 = mysql_query("SELECT * FROM community_event WHERE creators_info='community_project|".$row_poo['id']."' AND del_status=0 AND active=0 AND month(date) = ".$m."
	        AND dayofmonth(date) = ".$d." AND
	        year(date)= ".$y."");
								if(mysql_num_rows($sql_art3)>0)
								{
									while($row3_po = mysql_fetch_assoc($sql_art3))
									{
									//	$row3_po['id'] = $row3_po['id'].'~com';
										$row3_po['whos_event'] = "com_creator_creator_eve";
										$com_cp_ao[] = $row3_po;
									}
								}
							//}
						}
					}
				}
			}
		}		
		
		$totals = array();
		$totals = array_merge($art_p_ao,$art_c_ao,$com_p_ao,$com_c_ao,$com_a_ao,$art_a_ao,$art_ap_ao,$com_cp_ao);
		return $totals;
	}
	
	function only_sh_creator2ev_upevent()
	{
		$id = $this->selgeneral();
		$date = date("Y-m-d");
		
		$var_type_1 = 'general_artist';
		$var_type_2 = 'general_community';
		$var_type_3 = 'artist_project';
		$var_type_4 = 'community_project';
		
		$art_p_ao = array();
		$art_c_ao = array();
		
		$com_p_ao = array();
		$com_c_ao = array();
		
		$com_a_ao = array();
		$art_a_ao = array();
		
		$art_ap_ao = array();
		$com_cp_ao = array();
		
		$pal_1 = array();
		$pal_2 = array();
		
		if($id['artist_id']!=0 && !empty($id['artist_id']))
		{
			$chk_type = $var_type_1.'|'.$id['artist_id'];
			
			$sql_get_allp = mysql_query("SELECT * FROM artist_project WHERE artist_id='".$id['artist_id']."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_get_allp)>0)
			{
				while($rowal = mysql_fetch_assoc($sql_get_allp))
				{
					//$rowal['whos_project'] = "only_creator";
					$pal_1[] = $rowal;
				}
			}
			
			if(!empty($pal_1) && count($pal_1))
			{
				for($pl=0;$pl<count($pal_1);$pl++)
				{
					$chk_typep_1 = 'artist_project|'.$pal_1[$pl]['id'];
					
					$sql = mysql_query("SELECT * FROM artist_project WHERE (creators_info='".$chk_typep_1."' OR creators_info='".$chk_type."') AND del_status=0 AND active=0");
					if(mysql_num_rows($sql)>0)
					{
						while($row_po = mysql_fetch_assoc($sql))
						{
							//if($row_po['artist_id']!=$id['artist_id'])
							//{
								
								$sql_art2 = mysql_query("SELECT * FROM artist_event WHERE creators_info='artist_project|".$row_po['id']."' AND del_status=0 AND active=0 AND date>='".$date."'");
								if(mysql_num_rows($sql_art2)>0)
								{
									while($row2_po = mysql_fetch_assoc($sql_art2))
									{
										$row2_po['table_name'] = 'artist_event';
										$sql = mysql_query("SELECT MIN( `id` ) FROM `artist_event_profiles` WHERE artist_event_id='".$row2_po['id']."' AND subtype_id!=0");

										if(mysql_num_rows($sql)>0)
										{
											$res = mysql_fetch_assoc($sql);
											
											$sql2 = mysql_query("select * from artist_event_profiles where id='".$res['MIN( `id` )']."'");
											$res2 = mysql_fetch_assoc($sql2);

											$sql4 = mysql_query("select * from  subtype where subtype_id = '".$res2['subtype_id']."'");
											if(mysql_num_rows($sql4)>0)
											{
												$res4 = mysql_fetch_assoc($sql4);
												$row2_po['type'] = $res4['name'];
											}
											else
											{
												$row2_po['type'] = '';
											}
										}
										else
										{
											$row2_po['type'] = '';
										}
										//$row_po['whos_project'] = "only_creator";
										$art_p_ao[] = $row2_po;
									}
								}
								
								
								$sql_art3 = mysql_query("SELECT * FROM community_event WHERE creators_info='artist_project|".$row_po['id']."' AND del_status=0 AND active=0 AND date>='".$date."'");
								if(mysql_num_rows($sql_art3)>0)
								{
									while($row3_po = mysql_fetch_assoc($sql_art3))
									{
										$row3_po['table_name'] = 'community_event';
										$sql = mysql_query("SELECT MIN( `id` ) FROM `community_event_profiles` WHERE community_event_id='".$row3_po['id']."' AND subtype_id!=0");

										if(mysql_num_rows($sql)>0)
										{
											$res = mysql_fetch_assoc($sql);
											
											$sql2 = mysql_query("select * from community_event_profiles where id='".$res['MIN( `id` )']."'");
											$res2 = mysql_fetch_assoc($sql2);

											$sql4 = mysql_query("select * from  subtype where subtype_id = '".$res2['subtype_id']."'");
											if(mysql_num_rows($sql4)>0)
											{
												$res4 = mysql_fetch_assoc($sql4);
												$row3_po['type'] = $res4['name'];
											}
											else
											{
												$row3_po['type'] = '';
											}
										}
										else
										{
											$row3_po['type'] = '';
										}
										//$row_po['whos_project'] = "only_creator";
										$art_c_ao[] = $row3_po;
									}
								}
							//}
						}
					}
					
					
					$sql = mysql_query("SELECT * FROM community_project WHERE (creators_info='".$chk_typep_1."' OR creators_info='".$chk_type."') AND del_status=0 AND active=0");
					if(mysql_num_rows($sql)>0)
					{
						while($row_poo = mysql_fetch_assoc($sql))
						{
							//if($row_poo['community_id']!=$id['community_id'])
							//{		
								
								$sql_art2 = mysql_query("SELECT * FROM artist_event WHERE creators_info='community_project|".$row_poo['id']."' AND del_status=0 AND active=0 AND date>='".$date."'");
								if(mysql_num_rows($sql_art2)>0)
								{
									while($row2_po = mysql_fetch_assoc($sql_art2))
									{
										$row2_po['table_name'] = 'artist_event';
										$sql = mysql_query("SELECT MIN( `id` ) FROM `artist_event_profiles` WHERE artist_event_id='".$row2_po['id']."' AND subtype_id!=0");

										if(mysql_num_rows($sql)>0)
										{
											$res = mysql_fetch_assoc($sql);
											
											$sql2 = mysql_query("select * from artist_event_profiles where id='".$res['MIN( `id` )']."'");
											$res2 = mysql_fetch_assoc($sql2);

											$sql4 = mysql_query("select * from  subtype where subtype_id = '".$res2['subtype_id']."'");
											if(mysql_num_rows($sql4)>0)
											{
												$res4 = mysql_fetch_assoc($sql4);
												$row2_po['type'] = $res4['name'];
											}
											else
											{
												$row2_po['type'] = '';
											}
										}
										else
										{
											$row2_po['type'] = '';
										}
										//$row_poo['whos_project'] = "only_creator";
										$com_p_ao[] = $row2_po;
									}
								}
								
								
								$sql_art3 = mysql_query("SELECT * FROM community_event WHERE creators_info='community_project|".$row_poo['id']."' AND del_status=0 AND active=0 AND date>='".$date."'");
								if(mysql_num_rows($sql_art3)>0)
								{
									while($row3_po = mysql_fetch_assoc($sql_art3))
									{
										$row3_po['table_name'] = 'community_event';
										$sql = mysql_query("SELECT MIN( `id` ) FROM `community_event_profiles` WHERE community_event_id='".$row3_po['id']."' AND subtype_id!=0");

										if(mysql_num_rows($sql)>0)
										{
											$res = mysql_fetch_assoc($sql);
											
											$sql2 = mysql_query("select * from community_event_profiles where id='".$res['MIN( `id` )']."'");
											$res2 = mysql_fetch_assoc($sql2);

											$sql4 = mysql_query("select * from  subtype where subtype_id = '".$res2['subtype_id']."'");
											if(mysql_num_rows($sql4)>0)
											{
												$res4 = mysql_fetch_assoc($sql4);
												$row3_po['type'] = $res4['name'];
											}
											else
											{
												$row3_po['type'] = '';
											}
										}
										else
										{
											$row3_po['type'] = '';
										}
										//$row_poo['whos_project'] = "only_creator";
										$com_c_ao[] = $row3_po;
									}
								}
							//}
						}
					}
				}
			}
		}
		
		if($id['community_id']!=0 && !empty($id['community_id']))
		{
			$chk_type = $var_type_2.'|'.$id['community_id'];
			
			$sql_get_allp = mysql_query("SELECT * FROM community_project WHERE community_id='".$id['community_id']."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_get_allp)>0)
			{
				while($rowal = mysql_fetch_assoc($sql_get_allp))
				{
					//$rowal['whos_project'] = "only_creator";
					$pal_2[] = $rowal;
				}
			}
			
			if(!empty($pal_2) && count($pal_2))
			{
				for($pl2=0;$pl2<count($pal_2);$pl2++)
				{
					$chk_typep_2 = 'community_project|'.$pal_2[$pl2]['id'];
					
					
					$sql = mysql_query("SELECT * FROM artist_project WHERE (creators_info='".$chk_typep_2."' OR creators_info='".$chk_type."') AND del_status=0 AND active=0");
					if(mysql_num_rows($sql)>0)
					{
						while($row_po = mysql_fetch_assoc($sql))
						{
							//if($row_po['artist_id']!=$id['artist_id'])
							//{
								
								$sql_art2 = mysql_query("SELECT * FROM artist_event WHERE creators_info='artist_project|".$row_po['id']."' AND del_status=0 AND active=0 AND date>='".$date."'");
								if(mysql_num_rows($sql_art2)>0)
								{
									while($row2_po = mysql_fetch_assoc($sql_art2))
									{
										$row2_po['table_name'] = 'artist_event';
										$sql = mysql_query("SELECT MIN( `id` ) FROM `artist_event_profiles` WHERE artist_event_id='".$row2_po['id']."' AND subtype_id!=0");

										if(mysql_num_rows($sql)>0)
										{
											$res = mysql_fetch_assoc($sql);
											
											$sql2 = mysql_query("select * from artist_event_profiles where id='".$res['MIN( `id` )']."'");
											$res2 = mysql_fetch_assoc($sql2);

											$sql4 = mysql_query("select * from  subtype where subtype_id = '".$res2['subtype_id']."'");
											if(mysql_num_rows($sql4)>0)
											{
												$res4 = mysql_fetch_assoc($sql4);
												$row2_po['type'] = $res4['name'];
											}
											else
											{
												$row2_po['type'] = '';
											}
										}
										else
										{
											$row2_po['type'] = '';
										}
										//$row_po['whos_project'] = "only_creator";
										$com_a_ao[] = $row2_po;
									}
								}
								
								
								$sql_art3 = mysql_query("SELECT * FROM community_event WHERE creators_info='artist_project|".$row_po['id']."' AND del_status=0 AND active=0 AND date>='".$date."'");
								if(mysql_num_rows($sql_art3)>0)
								{
									while($row3_po = mysql_fetch_assoc($sql_art3))
									{
										$row3_po['table_name'] = 'community_event';
										$sql = mysql_query("SELECT MIN( `id` ) FROM `community_event_profiles` WHERE community_event_id='".$row3_po['id']."' AND subtype_id!=0");

										if(mysql_num_rows($sql)>0)
										{
											$res = mysql_fetch_assoc($sql);
											
											$sql2 = mysql_query("select * from community_event_profiles where id='".$res['MIN( `id` )']."'");
											$res2 = mysql_fetch_assoc($sql2);

											$sql4 = mysql_query("select * from  subtype where subtype_id = '".$res2['subtype_id']."'");
											if(mysql_num_rows($sql4)>0)
											{
												$res4 = mysql_fetch_assoc($sql4);
												$row3_po['type'] = $res4['name'];
											}
											else
											{
												$row3_po['type'] = '';
											}
										}
										else
										{
											$row3_po['type'] = '';
										}
										//$row_po['whos_project'] = "only_creator";
										$art_a_ao[] = $row3_po;
									}
								}
							//}
						}
					}
					
					
					$sql = mysql_query("SELECT * FROM community_project WHERE (creators_info='".$chk_typep_2."' OR creators_info='".$chk_type."') AND del_status=0 AND active=0");
					if(mysql_num_rows($sql)>0)
					{
						while($row_poo = mysql_fetch_assoc($sql))
						{
							//if($row_poo['community_id']!=$id['community_id'])
							//{
								
								$sql_art2 = mysql_query("SELECT * FROM artist_event WHERE creators_info='community_project|".$row_poo['id']."' AND del_status=0 AND active=0 AND date>='".$date."'");
								if(mysql_num_rows($sql_art2)>0)
								{
									while($row2_po = mysql_fetch_assoc($sql_art2))
									{
										$row2_po['table_name'] = 'artist_event';
										$sql = mysql_query("SELECT MIN( `id` ) FROM `artist_event_profiles` WHERE artist_event_id='".$row2_po['id']."' AND subtype_id!=0");

										if(mysql_num_rows($sql)>0)
										{
											$res = mysql_fetch_assoc($sql);
											
											$sql2 = mysql_query("select * from artist_event_profiles where id='".$res['MIN( `id` )']."'");
											$res2 = mysql_fetch_assoc($sql2);

											$sql4 = mysql_query("select * from  subtype where subtype_id = '".$res2['subtype_id']."'");
											if(mysql_num_rows($sql4)>0)
											{
												$res4 = mysql_fetch_assoc($sql4);
												$row2_po['type'] = $res4['name'];
											}
											else
											{
												$row2_po['type'] = '';
											}
										}
										else
										{
											$row2_po['type'] = '';
										}
										//$row_poo['whos_project'] = "only_creator";
										$art_ap_ao[] = $row2_po;
									}
								}
								
								
								$sql_art3 = mysql_query("SELECT * FROM community_event WHERE creators_info='community_project|".$row_poo['id']."' AND del_status=0 AND active=0 AND date>='".$date."'");
								if(mysql_num_rows($sql_art3)>0)
								{
									while($row3_po = mysql_fetch_assoc($sql_art3))
									{
										$row3_po['table_name'] = 'community_event';
										$sql = mysql_query("SELECT MIN( `id` ) FROM `community_event_profiles` WHERE community_event_id='".$row3_po['id']."' AND subtype_id!=0");

										if(mysql_num_rows($sql)>0)
										{
											$res = mysql_fetch_assoc($sql);
											
											$sql2 = mysql_query("select * from community_event_profiles where id='".$res['MIN( `id` )']."'");
											$res2 = mysql_fetch_assoc($sql2);

											$sql4 = mysql_query("select * from  subtype where subtype_id = '".$res2['subtype_id']."'");
											if(mysql_num_rows($sql4)>0)
											{
												$res4 = mysql_fetch_assoc($sql4);
												$row3_po['type'] = $res4['name'];
											}
											else
											{
												$row3_po['type'] = '';
											}
										}
										else
										{
											$row3_po['type'] = '';
										}
										//$row_poo['whos_project'] = "only_creator";
										$com_cp_ao[] = $row3_po;
									}
								}
							//}
						}
					}
				}
			}
		}		
		
		$totals = array();
		$totals = array_merge($art_p_ao,$art_c_ao,$com_p_ao,$com_c_ao,$com_a_ao,$art_a_ao,$art_ap_ao,$com_cp_ao);
		return $totals;
	}
	
	function only_creator2_recevent()
	{
		$id = $this->selgeneral();
		$date = date("Y-m-d");
		
		$var_type_1 = 'general_artist';
		$var_type_2 = 'general_community';
		$var_type_3 = 'artist_project';
		$var_type_4 = 'community_project';
		
		$art_p_ao = array();
		$art_c_ao = array();
		
		$com_p_ao = array();
		$com_c_ao = array();
		
		$com_a_ao = array();
		$art_a_ao = array();
		
		$art_ap_ao = array();
		$com_cp_ao = array();
		
		$pal_1 = array();
		$pal_2 = array();
		
		if($id['artist_id']!=0 && !empty($id['artist_id']))
		{
			$chk_type = $var_type_1.'|'.$id['artist_id'];
			
			$sql_get_allp = mysql_query("SELECT * FROM artist_project WHERE artist_id='".$id['artist_id']."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_get_allp)>0)
			{
				while($rowal = mysql_fetch_assoc($sql_get_allp))
				{
					//$rowal['whos_project'] = "only_creator";
					$pal_1[] = $rowal;
				}
			}
			
			if(!empty($pal_1) && count($pal_1))
			{
				for($pl=0;$pl<count($pal_1);$pl++)
				{
					$chk_typep_1 = 'artist_project|'.$pal_1[$pl]['id'];
					
					$sql = mysql_query("SELECT * FROM artist_project WHERE (creators_info='".$chk_typep_1."' OR creators_info='".$chk_type."') AND del_status=0 AND active=0");
					if(mysql_num_rows($sql)>0)
					{
						while($row_po = mysql_fetch_assoc($sql))
						{
							//if($row_po['artist_id']!=$id['artist_id'])
							//{
								
								$sql_art2 = mysql_query("SELECT * FROM artist_event WHERE creators_info='artist_project|".$row_po['id']."' AND del_status=0 AND active=0 AND date<'".$date."'");
								if(mysql_num_rows($sql_art2)>0)
								{
									while($row2_po = mysql_fetch_assoc($sql_art2))
									{
										$row2_po['id'] = $row2_po['id'].'~art';
										$row2_po['whos_event'] = "art_creator_creator_eve";
										$art_p_ao[] = $row2_po;
									}
								}
								
								
								$sql_art3 = mysql_query("SELECT * FROM community_event WHERE creators_info='artist_project|".$row_po['id']."' AND del_status=0 AND active=0 AND date<'".$date."'");
								if(mysql_num_rows($sql_art3)>0)
								{
									while($row3_po = mysql_fetch_assoc($sql_art3))
									{
										$row3_po['id'] = $row3_po['id'].'~com';
										$row3_po['whos_event'] = "com_creator_creator_eve";
										$art_c_ao[] = $row3_po;
									}
								}
							//}
						}
					}
					
					
					$sql = mysql_query("SELECT * FROM community_project WHERE (creators_info='".$chk_typep_1."' OR creators_info='".$chk_type."') AND del_status=0 AND active=0");
					if(mysql_num_rows($sql)>0)
					{
						while($row_poo = mysql_fetch_assoc($sql))
						{
							//if($row_poo['community_id']!=$id['community_id'])
							//{		
								
								$sql_art2 = mysql_query("SELECT * FROM artist_event WHERE creators_info='community_project|".$row_poo['id']."' AND del_status=0 AND active=0 AND date<'".$date."'");
								if(mysql_num_rows($sql_art2)>0)
								{
									while($row2_po = mysql_fetch_assoc($sql_art2))
									{
										$row2_po['id'] = $row2_po['id'].'~art';
										$row2_po['whos_event'] = "art_creator_creator_eve";
										$com_p_ao[] = $row2_po;
									}
								}
								
								
								$sql_art3 = mysql_query("SELECT * FROM community_event WHERE creators_info='community_project|".$row_poo['id']."' AND del_status=0 AND active=0 AND date<'".$date."'");
								if(mysql_num_rows($sql_art3)>0)
								{
									while($row3_po = mysql_fetch_assoc($sql_art3))
									{
										$row3_po['id'] = $row3_po['id'].'~com';
										$row3_po['whos_event'] = "com_creator_creator_eve";
										$com_c_ao[] = $row3_po;
									}
								}
							//}
						}
					}
				}
			}
		}
		
		if($id['community_id']!=0 && !empty($id['community_id']))
		{
			$chk_type = $var_type_2.'|'.$id['community_id'];
			
			$sql_get_allp = mysql_query("SELECT * FROM community_project WHERE community_id='".$id['community_id']."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_get_allp)>0)
			{
				while($rowal = mysql_fetch_assoc($sql_get_allp))
				{
					//$rowal['whos_project'] = "only_creator";
					$pal_2[] = $rowal;
				}
			}
			
			if(!empty($pal_2) && count($pal_2))
			{
				for($pl2=0;$pl2<count($pal_2);$pl2++)
				{
					$chk_typep_2 = 'community_project|'.$pal_2[$pl2]['id'];
					
					
					$sql = mysql_query("SELECT * FROM artist_project WHERE (creators_info='".$chk_typep_2."' OR creators_info='".$chk_type."') AND del_status=0 AND active=0");
					if(mysql_num_rows($sql)>0)
					{
						while($row_po = mysql_fetch_assoc($sql))
						{
							//if($row_po['artist_id']!=$id['artist_id'])
							//{
								
								$sql_art2 = mysql_query("SELECT * FROM artist_event WHERE creators_info='artist_project|".$row_po['id']."' AND del_status=0 AND active=0 AND date<'".$date."'");
								if(mysql_num_rows($sql_art2)>0)
								{
									while($row2_po = mysql_fetch_assoc($sql_art2))
									{
										$row2_po['id'] = $row2_po['id'].'~art';
										$row2_po['whos_event'] = "art_creator_creator_eve";
										$com_a_ao[] = $row2_po;
									}
								}
								
								
								$sql_art3 = mysql_query("SELECT * FROM community_event WHERE creators_info='artist_project|".$row_po['id']."' AND del_status=0 AND active=0 AND date<'".$date."'");
								if(mysql_num_rows($sql_art3)>0)
								{
									while($row3_po = mysql_fetch_assoc($sql_art3))
									{
										$row3_po['id'] = $row3_po['id'].'~com';
										$row3_po['whos_event'] = "com_creator_creator_eve";
										$art_a_ao[] = $row3_po;
									}
								}
							//}
						}
					}
					
					
					$sql = mysql_query("SELECT * FROM community_project WHERE (creators_info='".$chk_typep_2."' OR creators_info='".$chk_type."') AND del_status=0 AND active=0");
					if(mysql_num_rows($sql)>0)
					{
						while($row_poo = mysql_fetch_assoc($sql))
						{
							//if($row_poo['community_id']!=$id['community_id'])
							//{
								
								$sql_art2 = mysql_query("SELECT * FROM artist_event WHERE creators_info='community_project|".$row_poo['id']."' AND del_status=0 AND active=0 AND date<'".$date."'");
								if(mysql_num_rows($sql_art2)>0)
								{
									while($row2_po = mysql_fetch_assoc($sql_art2))
									{
										$row2_po['id'] = $row2_po['id'].'~art';
										$row2_po['whos_event'] = "art_creator_creator_eve";
										$art_ap_ao[] = $row2_po;
									}
								}
								
								
								$sql_art3 = mysql_query("SELECT * FROM community_event WHERE creators_info='community_project|".$row_poo['id']."' AND del_status=0 AND active=0 AND date<'".$date."'");
								if(mysql_num_rows($sql_art3)>0)
								{
									while($row3_po = mysql_fetch_assoc($sql_art3))
									{
										$row3_po['id'] = $row3_po['id'].'~com';
										$row3_po['whos_event'] = "com_creator_creator_eve";
										$com_cp_ao[] = $row3_po;
									}
								}
							//}
						}
					}
				}
			}
		}		
		
		$totals = array();
		$totals = array_merge($art_p_ao,$art_c_ao,$com_p_ao,$com_c_ao,$com_a_ao,$art_a_ao,$art_ap_ao,$com_cp_ao);
		return $totals;
	}
	
	function only_creator2_calrec($d,$m,$y)
	{
		$id = $this->selgeneral();
		$date = date("Y-m-d");
		
		$var_type_1 = 'general_artist';
		$var_type_2 = 'general_community';
		$var_type_3 = 'artist_project';
		$var_type_4 = 'community_project';
		
		$art_p_ao = array();
		$art_c_ao = array();
		
		$com_p_ao = array();
		$com_c_ao = array();
		
		$com_a_ao = array();
		$art_a_ao = array();
		
		$art_ap_ao = array();
		$com_cp_ao = array();
		
		$pal_1 = array();
		$pal_2 = array();
		
		if($id['artist_id']!=0 && !empty($id['artist_id']))
		{
			$chk_type = $var_type_1.'|'.$id['artist_id'];
			
			$sql_get_allp = mysql_query("SELECT * FROM artist_project WHERE artist_id='".$id['artist_id']."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_get_allp)>0)
			{
				while($rowal = mysql_fetch_assoc($sql_get_allp))
				{
					//$rowal['whos_project'] = "only_creator";
					$pal_1[] = $rowal;
				}
			}
			
			if(!empty($pal_1) && count($pal_1))
			{
				for($pl=0;$pl<count($pal_1);$pl++)
				{
					$chk_typep_1 = 'artist_project|'.$pal_1[$pl]['id'];
					
					$sql = mysql_query("SELECT * FROM artist_project WHERE (creators_info='".$chk_typep_1."' OR creators_info='".$chk_type."') AND del_status=0 AND active=0");
					if(mysql_num_rows($sql)>0)
					{
						while($row_po = mysql_fetch_assoc($sql))
						{
							//if($row_po['artist_id']!=$id['artist_id'])
							//{
								
								$sql_art2 = mysql_query("SELECT * FROM artist_event WHERE creators_info='artist_project|".$row_po['id']."' AND del_status=0 AND active=0  AND month(date) = ".$m."
	        AND dayofmonth(date) = ".$d." AND
	        year(date)= ".$y."");
								if(mysql_num_rows($sql_art2)>0)
								{
									while($row2_po = mysql_fetch_assoc($sql_art2))
									{
										//$row2_po['id'] = $row2_po['id'].'~art';
										$row2_po['whos_event'] = "art_creator_creator_eve";
										$art_p_ao[] = $row2_po;
									}
								}
								
								
								$sql_art3 = mysql_query("SELECT * FROM community_event WHERE creators_info='artist_project|".$row_po['id']."' AND del_status=0 AND active=0  AND month(date) = ".$m."
	        AND dayofmonth(date) = ".$d." AND
	        year(date)= ".$y."");
								if(mysql_num_rows($sql_art3)>0)
								{
									while($row3_po = mysql_fetch_assoc($sql_art3))
									{
										//$row3_po['id'] = $row3_po['id'].'~com';
										$row3_po['whos_event'] = "com_creator_creator_eve";
										$art_c_ao[] = $row3_po;
									}
								}
							//}
						}
					}
					
					
					$sql = mysql_query("SELECT * FROM community_project WHERE (creators_info='".$chk_typep_1."' OR creators_info='".$chk_type."') AND del_status=0 AND active=0");
					if(mysql_num_rows($sql)>0)
					{
						while($row_poo = mysql_fetch_assoc($sql))
						{
							//if($row_poo['community_id']!=$id['community_id'])
							//{		
								
								$sql_art2 = mysql_query("SELECT * FROM artist_event WHERE creators_info='community_project|".$row_poo['id']."' AND del_status=0 AND active=0 AND month(date) = ".$m."
	        AND dayofmonth(date) = ".$d." AND
	        year(date)= ".$y."");
								if(mysql_num_rows($sql_art2)>0)
								{
									while($row2_po = mysql_fetch_assoc($sql_art2))
									{
									//	$row2_po['id'] = $row2_po['id'].'~art';
										$row2_po['whos_event'] = "art_creator_creator_eve";
										$com_p_ao[] = $row2_po;
									}
								}
								
								
								$sql_art3 = mysql_query("SELECT * FROM community_event WHERE creators_info='community_project|".$row_poo['id']."' AND del_status=0 AND active=0 AND month(date) = ".$m."
	        AND dayofmonth(date) = ".$d." AND
	        year(date)= ".$y."");
								if(mysql_num_rows($sql_art3)>0)
								{
									while($row3_po = mysql_fetch_assoc($sql_art3))
									{
									//	$row3_po['id'] = $row3_po['id'].'~com';
										$row3_po['whos_event'] = "com_creator_creator_eve";
										$com_c_ao[] = $row3_po;
									}
								}
							//}
						}
					}
				}
			}
		}
		
		if($id['community_id']!=0 && !empty($id['community_id']))
		{
			$chk_type = $var_type_2.'|'.$id['community_id'];
			
			$sql_get_allp = mysql_query("SELECT * FROM community_project WHERE community_id='".$id['community_id']."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_get_allp)>0)
			{
				while($rowal = mysql_fetch_assoc($sql_get_allp))
				{
					//$rowal['whos_project'] = "only_creator";
					$pal_2[] = $rowal;
				}
			}
			
			if(!empty($pal_2) && count($pal_2))
			{
				for($pl2=0;$pl2<count($pal_2);$pl2++)
				{
					$chk_typep_2 = 'community_project|'.$pal_2[$pl2]['id'];
					
					
					$sql = mysql_query("SELECT * FROM artist_project WHERE (creators_info='".$chk_typep_2."' OR creators_info='".$chk_type."') AND del_status=0 AND active=0");
					if(mysql_num_rows($sql)>0)
					{
						while($row_po = mysql_fetch_assoc($sql))
						{
							//if($row_po['artist_id']!=$id['artist_id'])
							//{
								
								$sql_art2 = mysql_query("SELECT * FROM artist_event WHERE creators_info='artist_project|".$row_po['id']."' AND del_status=0 AND active=0  AND month(date) = ".$m."
	        AND dayofmonth(date) = ".$d." AND
	        year(date)= ".$y."");
								if(mysql_num_rows($sql_art2)>0)
								{
									while($row2_po = mysql_fetch_assoc($sql_art2))
									{
									//	$row2_po['id'] = $row2_po['id'].'~art';
										$row2_po['whos_event'] = "art_creator_creator_eve";
										$com_a_ao[] = $row2_po;
									}
								}
								
								
								$sql_art3 = mysql_query("SELECT * FROM community_event WHERE creators_info='artist_project|".$row_po['id']."' AND del_status=0 AND active=0  AND month(date) = ".$m."
	        AND dayofmonth(date) = ".$d." AND
	        year(date)= ".$y."");
								if(mysql_num_rows($sql_art3)>0)
								{
									while($row3_po = mysql_fetch_assoc($sql_art3))
									{
									//	$row3_po['id'] = $row3_po['id'].'~com';
										$row3_po['whos_event'] = "com_creator_creator_eve";
										$art_a_ao[] = $row3_po;
									}
								}
							//}
						}
					}
					
					
					$sql = mysql_query("SELECT * FROM community_project WHERE (creators_info='".$chk_typep_2."' OR creators_info='".$chk_type."') AND del_status=0 AND active=0");
					if(mysql_num_rows($sql)>0)
					{
						while($row_poo = mysql_fetch_assoc($sql))
						{
							//if($row_poo['community_id']!=$id['community_id'])
							//{
								
								$sql_art2 = mysql_query("SELECT * FROM artist_event WHERE creators_info='community_project|".$row_poo['id']."' AND del_status=0 AND active=0 AND month(date) = ".$m."
	        AND dayofmonth(date) = ".$d." AND
	        year(date)= ".$y."");
								if(mysql_num_rows($sql_art2)>0)
								{
									while($row2_po = mysql_fetch_assoc($sql_art2))
									{
									//	$row2_po['id'] = $row2_po['id'].'~art';
										$row2_po['whos_event'] = "art_creator_creator_eve";
										$art_ap_ao[] = $row2_po;
									}
								}
								
								
								$sql_art3 = mysql_query("SELECT * FROM community_event WHERE creators_info='community_project|".$row_poo['id']."' AND del_status=0 AND active=0 AND month(date) = ".$m."
	        AND dayofmonth(date) = ".$d." AND
	        year(date)= ".$y."");
								if(mysql_num_rows($sql_art3)>0)
								{
									while($row3_po = mysql_fetch_assoc($sql_art3))
									{
									//	$row3_po['id'] = $row3_po['id'].'~com';
										$row3_po['whos_event'] = "com_creator_creator_eve";
										$com_cp_ao[] = $row3_po;
									}
								}
							//}
						}
					}
				}
			}
		}		
		
		$totals = array();
		$totals = array_merge($art_p_ao,$art_c_ao,$com_p_ao,$com_c_ao,$com_a_ao,$art_a_ao,$art_ap_ao,$com_cp_ao);
		return $totals;
	}	
	
	function only_creator2_orecevent()
	{
		$id = $this->selgeneral();
		$date = date("Y-m-d");
		
		$var_type_1 = 'general_artist';
		$var_type_2 = 'general_community';
		$var_type_3 = 'artist_project';
		$var_type_4 = 'community_project';
		
		$art_p_ao = array();
		$art_c_ao = array();
		
		$com_p_ao = array();
		$com_c_ao = array();
		
		$com_a_ao = array();
		$art_a_ao = array();
		
		$art_ap_ao = array();
		$com_cp_ao = array();
		
		$pal_1 = array();
		$pal_2 = array();
		
		if($id['artist_id']!=0 && !empty($id['artist_id']))
		{
			$chk_type = $var_type_1.'|'.$id['artist_id'];
			
			$sql_get_allp = mysql_query("SELECT * FROM artist_project WHERE artist_id='".$id['artist_id']."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_get_allp)>0)
			{
				while($rowal = mysql_fetch_assoc($sql_get_allp))
				{
					//$rowal['whos_project'] = "only_creator";
					$pal_1[] = $rowal;
				}
			}
			
			if(!empty($pal_1) && count($pal_1))
			{
				for($pl=0;$pl<count($pal_1);$pl++)
				{
					$chk_typep_1 = 'artist_project|'.$pal_1[$pl]['id'];
					
					$sql = mysql_query("SELECT * FROM artist_project WHERE (creators_info='".$chk_typep_1."' OR creators_info='".$chk_type."') AND del_status=0 AND active=0");
					if(mysql_num_rows($sql)>0)
					{
						while($row_po = mysql_fetch_assoc($sql))
						{
							//if($row_po['artist_id']!=$id['artist_id'])
							//{
								
								$sql_art2 = mysql_query("SELECT * FROM artist_event WHERE creators_info='artist_project|".$row_po['id']."' AND del_status=0 AND active=0 AND date<'".$date."'");
								if(mysql_num_rows($sql_art2)>0)
								{
									while($row2_po = mysql_fetch_assoc($sql_art2))
									{
										if($row2_po['artist_id']!=$id['artist_id'])
										{
											$row2_po['id'] = $row2_po['id'].'~art';
											$row2_po['whos_event'] = "art_creator_creator_eve";
											$art_p_ao[] = $row2_po;
										}
									}
								}
								
								
								$sql_art3 = mysql_query("SELECT * FROM community_event WHERE creators_info='artist_project|".$row_po['id']."' AND del_status=0 AND active=0 AND date<'".$date."'");
								if(mysql_num_rows($sql_art3)>0)
								{
									while($row3_po = mysql_fetch_assoc($sql_art3))
									{
										if($row3_po['community_id']!=$id['community_id'])
										{
											$row3_po['id'] = $row3_po['id'].'~com';
											$row3_po['whos_event'] = "com_creator_creator_eve";
											$art_c_ao[] = $row3_po;
										}
									}
								}
							//}
						}
					}
					
					
					$sql = mysql_query("SELECT * FROM community_project WHERE (creators_info='".$chk_typep_1."' OR creators_info='".$chk_type."') AND del_status=0 AND active=0");
					if(mysql_num_rows($sql)>0)
					{
						while($row_poo = mysql_fetch_assoc($sql))
						{
							//if($row_poo['community_id']!=$id['community_id'])
							//{		
								
								$sql_art2 = mysql_query("SELECT * FROM artist_event WHERE creators_info='community_project|".$row_poo['id']."' AND del_status=0 AND active=0 AND date<'".$date."'");
								if(mysql_num_rows($sql_art2)>0)
								{
									while($row2_po = mysql_fetch_assoc($sql_art2))
									{
										if($row2_po['artist_id']!=$id['artist_id'])
										{
											$row2_po['id'] = $row2_po['id'].'~art';
											$row2_po['whos_event'] = "art_creator_creator_eve";
											$com_p_ao[] = $row2_po;
										}
									}
								}
								
								
								$sql_art3 = mysql_query("SELECT * FROM community_event WHERE creators_info='community_project|".$row_poo['id']."' AND del_status=0 AND active=0 AND date<'".$date."'");
								if(mysql_num_rows($sql_art3)>0)
								{
									while($row3_po = mysql_fetch_assoc($sql_art3))
									{
										if($row3_po['community_id']!=$id['community_id'])
										{
											$row3_po['id'] = $row3_po['id'].'~com';
											$row3_po['whos_event'] = "com_creator_creator_eve";
											$com_c_ao[] = $row3_po;
										}
									}
								}
							//}
						}
					}
				}
			}
		}
		
		if($id['community_id']!=0 && !empty($id['community_id']))
		{
			$chk_type = $var_type_2.'|'.$id['community_id'];
			
			$sql_get_allp = mysql_query("SELECT * FROM community_project WHERE community_id='".$id['community_id']."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_get_allp)>0)
			{
				while($rowal = mysql_fetch_assoc($sql_get_allp))
				{
					//$rowal['whos_project'] = "only_creator";
					$pal_2[] = $rowal;
				}
			}
			
			if(!empty($pal_2) && count($pal_2))
			{
				for($pl2=0;$pl2<count($pal_2);$pl2++)
				{
					$chk_typep_2 = 'community_project|'.$pal_2[$pl2]['id'];
					
					
					$sql = mysql_query("SELECT * FROM artist_project WHERE (creators_info='".$chk_typep_2."' OR creators_info='".$chk_type."') AND del_status=0 AND active=0");
					if(mysql_num_rows($sql)>0)
					{
						while($row_po = mysql_fetch_assoc($sql))
						{
							//if($row_po['artist_id']!=$id['artist_id'])
							//{
								
								$sql_art2 = mysql_query("SELECT * FROM artist_event WHERE creators_info='artist_project|".$row_po['id']."' AND del_status=0 AND active=0 AND date<'".$date."'");
								if(mysql_num_rows($sql_art2)>0)
								{
									while($row2_po = mysql_fetch_assoc($sql_art2))
									{
										if($row2_po['artist_id']!=$id['artist_id'])
										{
											$row2_po['id'] = $row2_po['id'].'~art';
											$row2_po['whos_event'] = "art_creator_creator_eve";
											$com_a_ao[] = $row2_po;
										}
									}
								}
								
								
								$sql_art3 = mysql_query("SELECT * FROM community_event WHERE creators_info='artist_project|".$row_po['id']."' AND del_status=0 AND active=0 AND date<'".$date."'");
								if(mysql_num_rows($sql_art3)>0)
								{
									while($row3_po = mysql_fetch_assoc($sql_art3))
									{
										if($row3_po['community_id']!=$id['community_id'])
										{
											$row3_po['id'] = $row3_po['id'].'~com';
											$row3_po['whos_event'] = "com_creator_creator_eve";
											$art_a_ao[] = $row3_po;
										}
									}
								}
							//}
						}
					}
					
					
					$sql = mysql_query("SELECT * FROM community_project WHERE (creators_info='".$chk_typep_2."' OR creators_info='".$chk_type."') AND del_status=0 AND active=0");
					if(mysql_num_rows($sql)>0)
					{
						while($row_poo = mysql_fetch_assoc($sql))
						{
							//if($row_poo['community_id']!=$id['community_id'])
							//{
								
								$sql_art2 = mysql_query("SELECT * FROM artist_event WHERE creators_info='community_project|".$row_poo['id']."' AND del_status=0 AND active=0 AND date<'".$date."'");
								if(mysql_num_rows($sql_art2)>0)
								{
									while($row2_po = mysql_fetch_assoc($sql_art2))
									{
										if($row2_po['artist_id']!=$id['artist_id'])
										{
											$row2_po['id'] = $row2_po['id'].'~art';
											$row2_po['whos_event'] = "art_creator_creator_eve";
											$art_ap_ao[] = $row2_po;
										}
									}
								}
								
								
								$sql_art3 = mysql_query("SELECT * FROM community_event WHERE creators_info='community_project|".$row_poo['id']."' AND del_status=0 AND active=0 AND date<'".$date."'");
								if(mysql_num_rows($sql_art3)>0)
								{
									while($row3_po = mysql_fetch_assoc($sql_art3))
									{
										if($row3_po['community_id']!=$id['community_id'])
										{
											$row3_po['id'] = $row3_po['id'].'~com';
											$row3_po['whos_event'] = "com_creator_creator_eve";
											$com_cp_ao[] = $row3_po;
										}
									}
								}
							//}
						}
					}
				}
			}
		}		
		
		$totals = array();
		$totals = array_merge($art_p_ao,$art_c_ao,$com_p_ao,$com_c_ao,$com_a_ao,$art_a_ao,$art_ap_ao,$com_cp_ao);
		return $totals;
	}
	
	function only_sh_creator2_recevent()
	{
		$id = $this->selgeneral();
		$date = date("Y-m-d");
		
		$var_type_1 = 'general_artist';
		$var_type_2 = 'general_community';
		$var_type_3 = 'artist_project';
		$var_type_4 = 'community_project';
		
		$art_p_ao = array();
		$art_c_ao = array();
		
		$com_p_ao = array();
		$com_c_ao = array();
		
		$com_a_ao = array();
		$art_a_ao = array();
		
		$art_ap_ao = array();
		$com_cp_ao = array();
		
		$pal_1 = array();
		$pal_2 = array();
		
		if($id['artist_id']!=0 && !empty($id['artist_id']))
		{
			$chk_type = $var_type_1.'|'.$id['artist_id'];
			
			$sql_get_allp = mysql_query("SELECT * FROM artist_project WHERE artist_id='".$id['artist_id']."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_get_allp)>0)
			{
				while($rowal = mysql_fetch_assoc($sql_get_allp))
				{
					//$rowal['whos_project'] = "only_creator";
					$pal_1[] = $rowal;
				}
			}
			
			if(!empty($pal_1) && count($pal_1))
			{
				for($pl=0;$pl<count($pal_1);$pl++)
				{
					$chk_typep_1 = 'artist_project|'.$pal_1[$pl]['id'];
					
					$sql = mysql_query("SELECT * FROM artist_project WHERE (creators_info='".$chk_typep_1."' OR creators_info='".$chk_type."') AND del_status=0 AND active=0");
					if(mysql_num_rows($sql)>0)
					{
						while($row_po = mysql_fetch_assoc($sql))
						{
							//if($row_po['artist_id']!=$id['artist_id'])
							//{
								
								$sql_art2 = mysql_query("SELECT * FROM artist_event WHERE creators_info='artist_project|".$row_po['id']."' AND del_status=0 AND active=0 AND date<'".$date."'");
								if(mysql_num_rows($sql_art2)>0)
								{
									while($row2_po = mysql_fetch_assoc($sql_art2))
									{
										$row2_po['table_name'] = 'artist_event';
										$sql = mysql_query("SELECT MIN( `id` ) FROM `artist_event_profiles` WHERE artist_event_id='".$row2_po['id']."' AND subtype_id!=0");

										if(mysql_num_rows($sql)>0)
										{
											$res = mysql_fetch_assoc($sql);
											
											$sql2 = mysql_query("select * from artist_event_profiles where id='".$res['MIN( `id` )']."'");
											$res2 = mysql_fetch_assoc($sql2);

											$sql4 = mysql_query("select * from  subtype where subtype_id = '".$res2['subtype_id']."'");
											if(mysql_num_rows($sql4)>0)
											{
												$res4 = mysql_fetch_assoc($sql4);
												$row2_po['type'] = $res4['name'];
											}
											else
											{
												$row2_po['type'] = '';
											}
										}
										else
										{
											$row2_po['type'] = '';
										}
										//$row_po['whos_project'] = "only_creator";
										$art_p_ao[] = $row2_po;
									}
								}
								
								
								$sql_art3 = mysql_query("SELECT * FROM community_event WHERE creators_info='artist_project|".$row_po['id']."' AND del_status=0 AND active=0 AND date<'".$date."'");
								if(mysql_num_rows($sql_art3)>0)
								{
									while($row3_po = mysql_fetch_assoc($sql_art3))
									{
										$row3_po['table_name'] = 'community_event';
										$sql = mysql_query("SELECT MIN( `id` ) FROM `community_event_profiles` WHERE community_event_id='".$row3_po['id']."' AND subtype_id!=0");

										if(mysql_num_rows($sql)>0)
										{
											$res = mysql_fetch_assoc($sql);
											
											$sql2 = mysql_query("select * from community_event_profiles where id='".$res['MIN( `id` )']."'");
											$res2 = mysql_fetch_assoc($sql2);

											$sql4 = mysql_query("select * from  subtype where subtype_id = '".$res2['subtype_id']."'");
											if(mysql_num_rows($sql4)>0)
											{
												$res4 = mysql_fetch_assoc($sql4);
												$row3_po['type'] = $res4['name'];
											}
											else
											{
												$row3_po['type'] = '';
											}
										}
										else
										{
											$row3_po['type'] = '';
										}
										//$row_po['whos_project'] = "only_creator";
										$art_c_ao[] = $row3_po;
									}
								}
							//}
						}
					}
					
					
					$sql = mysql_query("SELECT * FROM community_project WHERE (creators_info='".$chk_typep_1."' OR creators_info='".$chk_type."') AND del_status=0 AND active=0");
					if(mysql_num_rows($sql)>0)
					{
						while($row_poo = mysql_fetch_assoc($sql))
						{
							//if($row_poo['community_id']!=$id['community_id'])
							//{		
								
								$sql_art2 = mysql_query("SELECT * FROM artist_event WHERE creators_info='community_project|".$row_poo['id']."' AND del_status=0 AND active=0 AND date<'".$date."'");
								if(mysql_num_rows($sql_art2)>0)
								{
									while($row2_po = mysql_fetch_assoc($sql_art2))
									{
										$row2_po['table_name'] = 'artist_event';
										$sql = mysql_query("SELECT MIN( `id` ) FROM `artist_event_profiles` WHERE artist_event_id='".$row2_po['id']."' AND subtype_id!=0");

										if(mysql_num_rows($sql)>0)
										{
											$res = mysql_fetch_assoc($sql);
											
											$sql2 = mysql_query("select * from artist_event_profiles where id='".$res['MIN( `id` )']."'");
											$res2 = mysql_fetch_assoc($sql2);

											$sql4 = mysql_query("select * from  subtype where subtype_id = '".$res2['subtype_id']."'");
											if(mysql_num_rows($sql4)>0)
											{
												$res4 = mysql_fetch_assoc($sql4);
												$row2_po['type'] = $res4['name'];
											}
											else
											{
												$row2_po['type'] = '';
											}
										}
										else
										{
											$row2_po['type'] = '';
										}
										//$row_poo['whos_project'] = "only_creator";
										$com_p_ao[] = $row2_po;
									}
								}
								
								
								$sql_art3 = mysql_query("SELECT * FROM community_event WHERE creators_info='community_project|".$row_poo['id']."' AND del_status=0 AND active=0 AND date<'".$date."'");
								if(mysql_num_rows($sql_art3)>0)
								{
									while($row3_po = mysql_fetch_assoc($sql_art3))
									{
										$row3_po['table_name'] = 'community_event';
										$sql = mysql_query("SELECT MIN( `id` ) FROM `community_event_profiles` WHERE community_event_id='".$row3_po['id']."' AND subtype_id!=0");

										if(mysql_num_rows($sql)>0)
										{
											$res = mysql_fetch_assoc($sql);
											
											$sql2 = mysql_query("select * from community_event_profiles where id='".$res['MIN( `id` )']."'");
											$res2 = mysql_fetch_assoc($sql2);

											$sql4 = mysql_query("select * from  subtype where subtype_id = '".$res2['subtype_id']."'");
											if(mysql_num_rows($sql4)>0)
											{
												$res4 = mysql_fetch_assoc($sql4);
												$row3_po['type'] = $res4['name'];
											}
											else
											{
												$row3_po['type'] = '';
											}
										}
										else
										{
											$row3_po['type'] = '';
										}
										//$row_poo['whos_project'] = "only_creator";
										$com_c_ao[] = $row3_po;
									}
								}
							//}
						}
					}
				}
			}
		}
		
		if($id['community_id']!=0 && !empty($id['community_id']))
		{
			$chk_type = $var_type_2.'|'.$id['community_id'];
			
			$sql_get_allp = mysql_query("SELECT * FROM community_project WHERE community_id='".$id['community_id']."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_get_allp)>0)
			{
				while($rowal = mysql_fetch_assoc($sql_get_allp))
				{
					//$rowal['whos_project'] = "only_creator";
					$pal_2[] = $rowal;
				}
			}
			
			if(!empty($pal_2) && count($pal_2))
			{
				for($pl2=0;$pl2<count($pal_2);$pl2++)
				{
					$chk_typep_2 = 'community_project|'.$pal_2[$pl2]['id'];
					
					$sql = mysql_query("SELECT * FROM artist_project WHERE (creators_info='".$chk_typep_2."' OR creators_info='".$chk_type."') AND del_status=0 AND active=0");
					if(mysql_num_rows($sql)>0)
					{
						while($row_po = mysql_fetch_assoc($sql))
						{
							//if($row_po['artist_id']!=$id['artist_id'])
							//{
								
								$sql_art2 = mysql_query("SELECT * FROM artist_event WHERE creators_info='artist_project|".$row_po['id']."' AND del_status=0 AND active=0 AND date<'".$date."'");
								if(mysql_num_rows($sql_art2)>0)
								{
									while($row2_po = mysql_fetch_assoc($sql_art2))
									{
										$row2_po['table_name'] = 'artist_event';
										$sql = mysql_query("SELECT MIN( `id` ) FROM `artist_event_profiles` WHERE artist_event_id='".$row2_po['id']."' AND subtype_id!=0");

										if(mysql_num_rows($sql)>0)
										{
											$res = mysql_fetch_assoc($sql);
											
											$sql2 = mysql_query("select * from artist_event_profiles where id='".$res['MIN( `id` )']."'");
											$res2 = mysql_fetch_assoc($sql2);

											$sql4 = mysql_query("select * from  subtype where subtype_id = '".$res2['subtype_id']."'");
											if(mysql_num_rows($sql4)>0)
											{
												$res4 = mysql_fetch_assoc($sql4);
												$row2_po['type'] = $res4['name'];
											}
											else
											{
												$row2_po['type'] = '';
											}
										}
										else
										{
											$row2_po['type'] = '';
										}
										//$row_po['whos_project'] = "only_creator";
										$com_a_ao[] = $row2_po;
									}
								}
								
								
								$sql_art3 = mysql_query("SELECT * FROM community_event WHERE creators_info='artist_project|".$row_po['id']."' AND del_status=0 AND active=0 AND date<'".$date."'");
								if(mysql_num_rows($sql_art3)>0)
								{
									while($row3_po = mysql_fetch_assoc($sql_art3))
									{
										$row3_po['table_name'] = 'community_event';
										$sql = mysql_query("SELECT MIN( `id` ) FROM `community_event_profiles` WHERE community_event_id='".$row3_po['id']."' AND subtype_id!=0");

										if(mysql_num_rows($sql)>0)
										{
											$res = mysql_fetch_assoc($sql);
											
											$sql2 = mysql_query("select * from community_event_profiles where id='".$res['MIN( `id` )']."'");
											$res2 = mysql_fetch_assoc($sql2);

											$sql4 = mysql_query("select * from  subtype where subtype_id = '".$res2['subtype_id']."'");
											if(mysql_num_rows($sql4)>0)
											{
												$res4 = mysql_fetch_assoc($sql4);
												$row3_po['type'] = $res4['name'];
											}
											else
											{
												$row3_po['type'] = '';
											}
										}
										else
										{
											$row3_po['type'] = '';
										}
										//$row_po['whos_project'] = "only_creator";
										$art_a_ao[] = $row3_po;
									}
								}
							//}
						}
					}
					
					
					$sql = mysql_query("SELECT * FROM community_project WHERE (creators_info='".$chk_typep_2."' OR creators_info='".$chk_type."') AND del_status=0 AND active=0");
					if(mysql_num_rows($sql)>0)
					{
						while($row_poo = mysql_fetch_assoc($sql))
						{
							//if($row_poo['community_id']!=$id['community_id'])
							//{
								
								$sql_art2 = mysql_query("SELECT * FROM artist_event WHERE creators_info='community_project|".$row_poo['id']."' AND del_status=0 AND active=0 AND date<'".$date."'");
								if(mysql_num_rows($sql_art2)>0)
								{
									while($row2_po = mysql_fetch_assoc($sql_art2))
									{
										$row2_po['table_name'] = 'artist_event';
										$sql = mysql_query("SELECT MIN( `id` ) FROM `artist_event_profiles` WHERE artist_event_id='".$row2_po['id']."' AND subtype_id!=0");

										if(mysql_num_rows($sql)>0)
										{
											$res = mysql_fetch_assoc($sql);
											
											$sql2 = mysql_query("select * from artist_event_profiles where id='".$res['MIN( `id` )']."'");
											$res2 = mysql_fetch_assoc($sql2);

											$sql4 = mysql_query("select * from  subtype where subtype_id = '".$res2['subtype_id']."'");
											if(mysql_num_rows($sql4)>0)
											{
												$res4 = mysql_fetch_assoc($sql4);
												$row2_po['type'] = $res4['name'];
											}
											else
											{
												$row2_po['type'] = '';
											}
										}
										else
										{
											$row2_po['type'] = '';
										}
										//$row_poo['whos_project'] = "only_creator";
										$art_ap_ao[] = $row2_po;
									}
								}
								
								
								$sql_art3 = mysql_query("SELECT * FROM community_event WHERE creators_info='community_project|".$row_poo['id']."' AND del_status=0 AND active=0 AND date<'".$date."'");
								if(mysql_num_rows($sql_art3)>0)
								{
									while($row3_po = mysql_fetch_assoc($sql_art3))
									{
										$row3_po['table_name'] = 'community_event';
										$sql = mysql_query("SELECT MIN( `id` ) FROM `community_event_profiles` WHERE community_event_id='".$row3_po['id']."' AND subtype_id!=0");

										if(mysql_num_rows($sql)>0)
										{
											$res = mysql_fetch_assoc($sql);
											
											$sql2 = mysql_query("select * from community_event_profiles where id='".$res['MIN( `id` )']."'");
											$res2 = mysql_fetch_assoc($sql2);

											$sql4 = mysql_query("select * from  subtype where subtype_id = '".$res2['subtype_id']."'");
											if(mysql_num_rows($sql4)>0)
											{
												$res4 = mysql_fetch_assoc($sql4);
												$row3_po['type'] = $res4['name'];
											}
											else
											{
												$row3_po['type'] = '';
											}
										}
										else
										{
											$row3_po['type'] = '';
										}
										//$row_poo['whos_project'] = "only_creator";
										$com_cp_ao[] = $row3_po;
									}
								}
							//}
						}
					}
				}
			}
		}		
		
		$totals = array();
		$totals = array_merge($art_p_ao,$art_c_ao,$com_p_ao,$com_c_ao,$com_a_ao,$art_a_ao,$art_ap_ao,$com_cp_ao);
		return $totals;
	}
	
	function get_deleted_project_id()
	{
		$get_gen_det = $this->selgeneral();
		$res_art_id ="";
		$get_art_id = mysql_query("select * from general_community where community_id='".$get_gen_det['community_id']."'");
		if(mysql_num_rows($get_art_id)>0)
		{
			$res_art_id = mysql_fetch_assoc($get_art_id);
		}
		return($res_art_id);
	}
	function create_community_member($frequency,$cost,$description)
	{
		$row = $this->selgeneral();
		$get_art_mem = mysql_query("select * from community_membership where community_id='".$row['community_id']."'");
		if(mysql_num_rows($get_art_mem)>0){
			$art_member=mysql_query("update community_membership set frequency='".$frequency."',cost='".$cost."',description='".$description."' where community_id='".$row['community_id']."'");
		}else{
			$art_member=mysql_query("insert into community_membership (community_id,frequency,cost,description)
			values('".$row['community_id']."','".$frequency."','".$cost."','".$description."')");
		}
	}
	function delete_community_member()
	{
		$row=$this->selgeneral();
		$get_art_mem = mysql_query("delete from community_membership where community_id='".$row['community_id']."'");
	}
	
	function update_meds($title)
	{
		$getid=$this->selgeneral();
		$sql = mysql_query("UPDATE general_media SET creator='".$title."' WHERE creator_info='general_community|".$getid['community_id']."'");
		$sql = mysql_query("UPDATE general_media SET `from`='".$title."' WHERE from_info='general_community|".$getid['community_id']."'");
		$sql = mysql_query("UPDATE artist_event SET creator='".$title."' WHERE creators_info='general_community|".$getid['community_id']."'");
		$sql = mysql_query("UPDATE community_event SET creator='".$title."' WHERE creators_info='general_community|".$getid['community_id']."'");
		$sql = mysql_query("UPDATE artist_project SET creator='".$title."' WHERE creators_info='general_community|".$getid['community_id']."'");
		$sql = mysql_query("UPDATE community_project SET creator='".$title."' WHERE creators_info='general_community|".$getid['community_id']."'");
	}
}
?>