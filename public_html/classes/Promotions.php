<?php
	include_once('commons/db.php');
	
	class Promotions
	{
		function check_avail_general($email)
		{
			$sql_chlk = mysql_query("SELECT * FROM general_user WHERE email='".$email."' AND status=0 AND delete_status=0 AND active=0");
			if(mysql_num_rows($sql_chlk)>0)
			{
				return 1;
			}
		}
		
		function check_avail_artist($artist_id)
		{
			$sql_art = mysql_query("SELECT * FROM general_artist WHERE artist_id='".$artist_id."' AND del_status=0 AND status=0 AND active=0");
			if(mysql_num_rows($sql_art)>0)
			{
				return 1;
			}
		}
		
		function check_avail_community($artist_id)
		{
			$sql_art = mysql_query("SELECT * FROM general_community WHERE community_id='".$artist_id."' AND del_status=0 AND status=0 AND active=0");
			if(mysql_num_rows($sql_art)>0)
			{
				return 1;
			}
		}
		
		function all_profiles_share_artist($artist_id)
		{
			$artist_data = array();
			$event_data = array();
			$project_data = array();
			
			$sql_all_art = mysql_query("SELECT * FROM general_artist WHERE artist_id='".$artist_id."' AND del_status=0 AND status=0 AND active=0");
			//$sql_all_eve = mysql_query("SELECT * FROM artist_event WHERE artist_id='".$artist_id."' AND del_status=0");
			//$sql_all_pro = mysql_query("SELECT * FROM artist_project WHERE artist_id='".$artist_id."' AND del_status=0");
			
			if(mysql_num_rows($sql_all_art)>0)
			{
				while($row_art = mysql_fetch_assoc($sql_all_art))
				{
					$row_art['table_name'] = 'general_artist';
					$row_art['typefan'] = $row_art['type'];
					$row_art['type'] = 'Artist';
					//$row_art['type'] = 'artist';
					/* $sql = mysql_query("SELECT MIN( `general_artist_profiles_id` ) FROM `general_artist_profiles` WHERE artist_id='".$artist_id."' AND subtype_id!=0");
					
					if(mysql_num_rows($sql)>0)
					{
						$res = mysql_fetch_assoc($sql);
						
						$sql2 = mysql_query("select * from general_artist_profiles where general_artist_profiles_id='".$res['MIN( `general_artist_profiles_id` )']."'");
						$res2 = mysql_fetch_assoc($sql2);

						$sql4 = mysql_query("select * from  subtype where subtype_id = '".$res2['subtype_id']."'");
						if(mysql_num_rows($sql4)>0)
						{
							$res4 = mysql_fetch_assoc($sql4);
							$row_art['type'] = $res4['name'];
						}
						else
						{
							$row_art['type'] = '';
						}
					}
					else
					{
						$row_art['type'] = '';
					} */
					$artist_data[] = $row_art;
				}
			}
			
			/* if(mysql_num_rows($sql_all_eve)>0)
			{
				while($row_eve = mysql_fetch_assoc($sql_all_eve))
				{
					$row_eve['table_name'] = 'artist_event';
					$event_data[] = $row_eve;
				}
			}
			
			if(mysql_num_rows($sql_all_pro)>0)
			{
				while($row_pro = mysql_fetch_assoc($sql_all_pro))
				{
					$row_pro['table_name'] = 'artist_project';
					$project_data[] = $row_pro;
				}
			} */
			
			$all_main = array_merge($artist_data,$event_data,$project_data);
			return $all_main;
		}
		
		function all_profiles_share_community($community_id)
		{
			$community_data = array();
			$event_data = array();
			$project_data = array();
			
			$sql_all_com = mysql_query("SELECT * FROM general_community WHERE community_id='".$community_id."' AND del_status=0 AND active=0 AND status=0");
			//$sql_all_eve = mysql_query("SELECT * FROM community_event WHERE community_id='".$community_id."' AND del_status=0 AND active=0");
			//$sql_all_pro = mysql_query("SELECT * FROM community_project WHERE community_id='".$community_id."' AND del_status=0 AND active=0");
			
			if(mysql_num_rows($sql_all_com)>0)
			{
				while($row_com = mysql_fetch_assoc($sql_all_com))
				{
					$row_com['table_name'] = 'general_community';
					$row_com['typefan'] = $row_com['type'];
					$row_com['type'] = 'Community';
					/* $sql = mysql_query("SELECT MIN( `general_community_profiles_id` ) FROM `general_community_profiles` WHERE community_id='".$community_id."' AND subtype_id!=0"); */
					
					/* if(mysql_num_rows($sql)>0)
					{
						$res = mysql_fetch_assoc($sql);
						
						$sql2 = mysql_query("select * from general_community_profiles where general_community_profiles_id='".$res['MIN( `general_community_profiles_id` )']."'");
						$res2 = mysql_fetch_assoc($sql2);

						$sql4 = mysql_query("select * from  subtype where subtype_id = '".$res2['subtype_id']."'");
						if(mysql_num_rows($sql4)>0)
						{
							$res4 = mysql_fetch_assoc($sql4);
							$row_com['type'] = $res4['name'];
						}
						else
						{
							$row_com['type'] = '';
						}
					}
					else
					{
						$row_com['type'] = '';
					} */
					$community_data[] = $row_com;
				}
			}
			
			/* if(mysql_num_rows($sql_all_eve)>0)
			{
				while($row_eve = mysql_fetch_assoc($sql_all_eve))
				{
					$row_eve['table_name'] = 'community_event';
					$event_data[] = $row_eve;
				}
			}
			
			if(mysql_num_rows($sql_all_pro)>0)
			{
				while($row_pro = mysql_fetch_assoc($sql_all_pro)) 
				{
					$row_pro['table_name'] = 'community_project';
					$project_data[] = $row_pro;
				}
			} */
			
			$all_main = array_merge($community_data,$event_data,$project_data);
			return $all_main;
		}
		
		function email_all()
		{
			/* $all_mails = array();
			
			$arts_f = array();
			$coms_f = array();
			$others_f = array();
			
			$arts_s = array();
			$coms_s = array();
			$others_s = array();
			
			$fans = array(); */
			//$coms_fi = array();
			//$others_fi = array();
			
			/* $chks_a = $this->check_avail_general($_SESSION['login_email']);
			if($chks_a=='1')
			{
				$sql_chks = mysql_query("SELECT * FROM general_user WHERE email='".$_SESSION['login_email']."'");
				$chks = mysql_fetch_assoc($sql_chks);
				
				$sql_fan = mysql_query("SELECT * FROM fan_club_membership WHERE email!='".$_SESSION['login_email']."'");
				if(mysql_num_rows($sql_fan)>0)
				{
					while($all_fans = mysql_fetch_assoc($sql_fan))
					{
						if($chks['artist_id']!=0 && $all_fans['related_type']=='artist')
						{
							if($all_fans['related_id'] == $chks['artist_id'])
							{
								$arts_f[] = $all_fans['email'];
							}
						}
						
						if($chks['community_id']!=0 && $all_fans['related_type']=='community')
						{
							if($all_fans['related_id'] == $chks['community_id'])
							{
								$coms_f[] = $all_fans['email'];  
							}
						}
						
						if($all_fans['related_type']=='community_project' || $all_fans['related_type']=='community_event' || $all_fans['related_type']=='artist_event' || $all_fans['related_type']=='artist_project')
						{
							$ids = explode('_',$all_fans['related_id']);
							if($chks['artist_id']!=0)
							{
								if($chks['artist_id'] == $ids[0])
								{
									$others_f[] = $all_fans['email'];
								}
							}
							
							if($chks['community_id']!=0)
							{
								if($chks['community_id'] == $ids[0])
								{
									$others_f[] = $all_fans['email'];
								}
							}
						}
					}
				}
				
				$sql_sub = mysql_query("SELECT * FROM email_subscription WHERE email!='".$_SESSION['login_email']."'");
				if(mysql_num_rows($sql_sub)>0)
				{
					while($all_subs = mysql_fetch_assoc($sql_sub))
					{
						if($chks['artist_id']!=0 && $all_subs['related_type']=='artist')
						{
							if($all_subs['related_id'] == $chks['artist_id'])
							{
								$arts_s[] = $all_subs['email'];
							}
						}
						
						if($chks['community_id']!=0 && $all_subs['related_type']=='community')
						{
							if($all_subs['related_id'] == $chks['community_id'])
							{
								$coms_s[] = $all_subs['email'];  
							}
						}
						
						if($all_subs['related_type']=='community_project' || $all_subs['related_type']=='community_event' || $all_subs['related_type']=='artist_event' || $all_subs['related_type']=='artist_project')
						{
							$id_s = explode('_',$all_subs['related_id']);
							if($chks['artist_id']!=0)
							{
								if($chks['artist_id'] == $id_s[0])
								{
									$others_s[] = $all_subs['email'];
								}
							}
							
							if($chks['community_id']!=0)
							{
								if($chks['community_id'] == $id_s[0])
								{
									$others_s[] = $all_subs['email'];
								}
							}
						}
					}
				}
				
				$sql_fri = mysql_query("SELECT * FROM friends WHERE general_user_id='".$chks['general_user_id']."' AND status=0");
				if(mysql_num_rows($sql_fri)>0)
				{
					while($fris = mysql_fetch_assoc($sql_fri))
					{
						$sql_fet_email = mysql_query("SELECT * FROM general_user WHERE general_user_id='".$fris['fgeneral_user_id']."'");
						$ans_fet_email = mysql_fetch_assoc($sql_fet_email);
						$fans[] = $ans_fet_email['email'];
					}
				}
			}
			/*var_dump($arts_f);
			var_dump($coms_f);
			var_dump($others_f);
			
			var_dump($arts_s);
			var_dump($coms_s);
			var_dump($others_s);
			
			var_dump($fans);
			$all_emails = array_merge($arts_f,$coms_f,$others_f,$arts_s,$coms_s,$others_s,$fans);
			
			$new_array = array();
			foreach ($all_emails as $key => $value) {
				if(isset($new_array[$value]))
					$new_array[$value] += 1;
				else
					$new_array[$value] = 1;
			}
			foreach ($new_array as $uid => $n) {
			$ex_uid1=$ex_uid1.','.$uid;
			}
			$all_emails=explode(',',$ex_uid1); */
			$users_ans_all = array();
			$users_ans = array();
			$fans_ans = array();
			
			$sql_mail_all = "SELECT * FROM add_address_book WHERE `email`!='".$_SESSION['login_email']."' AND `general_user_id`='".$_SESSION['login_id']."'";
			
			$sql_fans = "select * from email_subscription where subscribe_to_id='".$_SESSION['login_id']."'";
			
			if($sql_mail_all!="")
			{
				$users = mysql_query($sql_mail_all);
				if($users!="")
				{
					if(mysql_num_rows($users)>0)
					{
						while($row = mysql_fetch_assoc($users))
						{
							$users_ans[] = $row['email'];
						}
					}
				}
			}
			
			if($sql_fans!="")
			{
				$fans = mysql_query($sql_fans);
				if($fans!="")
				{
					if(mysql_num_rows($fans)>0)
					{
						while($row = mysql_fetch_assoc($fans))
						{
							$fans_ans[] = $row['email'];
						}
					}
				}
			}	
			
			$users_ans_all = array_merge($users_ans,$fans_ans);
			$ex_uid2 = "";
			
			if(!empty($users_ans_all))
			{
				$new_array = array();
				foreach($users_ans_all as $key => $value)
				{
					if(isset($new_array[$value]))
					{
						$new_array[$value] +=1;
					}
					else
					{
						$new_array[$value] = 1;
					}
				}
				foreach ($new_array as $uid => $n)
				{
					$ex_uid1 =$ex_uid1.','.$uid;
				}
				$ex_uid2 = explode(",",$ex_uid1);
			}

					return $ex_uid2;
		}
		function email_all_artist()
		{
			$all_mails = array();
			$arts_f = array();
			$coms_f = array();
			$others_f = array();
			
			$arts_s = array();
			$coms_s = array();
			$others_s = array();
			
			$fans = array();
			//$coms_fi = array();
			//$others_fi = array();
			
			$chks_a = $this->check_avail_general($_SESSION['login_email']);
			if($chks_a=='1')
			{
				$sql_chks = mysql_query("SELECT * FROM general_user WHERE email='".$_SESSION['login_email']."'");
				$chks = mysql_fetch_assoc($sql_chks);
				
				/* $sql_fan = mysql_query("SELECT * FROM fan_club_membership WHERE email!='".$_SESSION['login_email']."'");
				if(mysql_num_rows($sql_fan)>0)
				{
					while($all_fans = mysql_fetch_assoc($sql_fan))
					{
						if($chks['artist_id']!=0 && $all_fans['related_type']=='artist')
						{
							$all_fans['related_id'] == $chks['artist_id'];
							$arts_f[] = $all_fans['email'];
						}
					}
				} */
				
				$sql_sub = mysql_query("SELECT * FROM email_subscription WHERE email!='".$_SESSION['login_email']."' AND delete_status=1 AND email_update=1");
				if(mysql_num_rows($sql_sub)>0)
				{
					while($all_subs = mysql_fetch_assoc($sql_sub))
					{
						if($chks['artist_id']!=0 && $all_subs['related_type']=='artist')
						{
							if($all_subs['related_id'] == $chks['artist_id'])
							{
								$arts_s[] = $all_subs['email'];
							}
						}
					}
				}
				
				/* $sql_fri = mysql_query("SELECT * FROM friends WHERE general_user_id='".$chks['general_user_id']."' AND status=0");
				if(mysql_num_rows($sql_fri)>0)
				{
					while($fris = mysql_fetch_assoc($sql_fri))
					{
						$sql_fet_email = mysql_query("SELECT * FROM general_user WHERE general_user_id='".$fris['fgeneral_user_id']."'");
						$ans_fet_email = mysql_fetch_assoc($sql_fet_email);
						$fans[] = $ans_fet_email['email'];
					}
				} */
			}
			/*var_dump($arts_f);
			var_dump($coms_f);
			var_dump($others_f);
			
			var_dump($arts_s);
			var_dump($coms_s);
			var_dump($others_s);
			
			var_dump($fans);*/
			$all_emails = array_merge($arts_f,$coms_f,$others_f,$arts_s,$coms_s,$others_s,$fans);
			
			$new_array = array();
			foreach ($all_emails as $key => $value) {
				if(isset($new_array[$value]))
					$new_array[$value] += 1;
				else
					$new_array[$value] = 1;
			}
			foreach ($new_array as $uid => $n) {
			$ex_uid1=$ex_uid1.','.$uid;
			}
			$all_emails=explode(',',$ex_uid1);


			return $all_emails;
		}
		function email_all_community()
		{
			$all_mails = array();
			
			$arts_f = array();
			$coms_f = array();
			$others_f = array();
			
			$arts_s = array();
			$coms_s = array();
			$others_s = array();
			
			$fans = array();
			//$coms_fi = array();
			//$others_fi = array();
			
			$chks_a = $this->check_avail_general($_SESSION['login_email']);
			if($chks_a=='1')
			{
				$sql_chks = mysql_query("SELECT * FROM general_user WHERE email='".$_SESSION['login_email']."'");
				$chks = mysql_fetch_assoc($sql_chks);
				
				$sql_fan = mysql_query("SELECT * FROM fan_club_membership WHERE email!='".$_SESSION['login_email']."'");
				/* if(mysql_num_rows($sql_fan)>0)
				{
					while($all_fans = mysql_fetch_assoc($sql_fan))
					{
						
						
						if($chks['community_id']!=0 && $all_fans['related_type']=='community')
						{
							if($all_fans['related_id'] == $chks['community_id'])
							{
								$coms_f[] = $all_fans['email'];  
							}
						}
					}
				} */
				
				$sql_sub = mysql_query("SELECT * FROM email_subscription WHERE email!='".$_SESSION['login_email']."' AND delete_status=1 AND email_update=1");
				if(mysql_num_rows($sql_sub)>0)
				{
					while($all_subs = mysql_fetch_assoc($sql_sub))
					{
						
						if($chks['community_id']!=0 && $all_subs['related_type']=='community')
						{
							if($all_subs['related_id'] == $chks['community_id'])
							{
								$coms_s[] = $all_subs['email'];  
							}
						}
					}
				}
				
				/* $sql_fri = mysql_query("SELECT * FROM friends WHERE general_user_id='".$chks['general_user_id']."' AND status=0");
				if(mysql_num_rows($sql_fri)>0)
				{
					while($fris = mysql_fetch_assoc($sql_fri))
					{
						$sql_fet_email = mysql_query("SELECT * FROM general_user WHERE general_user_id='".$fris['fgeneral_user_id']."'");
						$ans_fet_email = mysql_fetch_assoc($sql_fet_email);
						$fans[] = $ans_fet_email['email'];
					}
				} */
			}
			/*var_dump($arts_f);
			var_dump($coms_f);
			var_dump($others_f);
			
			var_dump($arts_s);
			var_dump($coms_s);
			var_dump($others_s);
			
			var_dump($fans);*/
			$all_emails = array_merge($arts_f,$coms_f,$others_f,$arts_s,$coms_s,$others_s,$fans);
			
			$new_array = array();
			foreach ($all_emails as $key => $value) {
				if(isset($new_array[$value]))
					$new_array[$value] += 1;
				else
					$new_array[$value] = 1;
			}
			foreach ($new_array as $uid => $n) {
			$ex_uid1=$ex_uid1.','.$uid;
			}
			$all_emails=explode(',',$ex_uid1);


			return $all_emails;
		}
		
		
		function email_all_artist_fans()
		{
			$all_mails = array();
			
			$arts_f = array();
			$coms_f = array();
			$others_f = array();
			
			$arts_s = array();
			$coms_s = array();
			$others_s = array();
			
			$fans = array();
			//$coms_fi = array();
			//$others_fi = array();
			
			$chks_a = $this->check_avail_general($_SESSION['login_email']);
			if($chks_a=='1')
			{
				$sql_chks = mysql_query("SELECT * FROM general_user WHERE email='".$_SESSION['login_email']."'");
				$chks = mysql_fetch_assoc($sql_chks);
				
				$sql_fan = mysql_query("SELECT * FROM fan_club_membership WHERE email!='".$_SESSION['login_email']."' AND del_status=0");
				if(mysql_num_rows($sql_fan)>0)
				{
					while($all_fans = mysql_fetch_assoc($sql_fan))
					{
						if($chks['artist_id']!=0 && $all_fans['related_type']=='artist')
						{
							if($all_fans['related_id'] == $chks['artist_id'])
							{								
								$sql_subs_apro = mysql_query("SELECT * FROM email_subscription WHERE email='".$all_fans['email']."' AND email_update=1 AND delete_status=1 AND related_type='artist' AND related_id='".$all_fans['related_id']."'");
								
								if($sql_subs_apro!="")
								{
									if(mysql_num_rows($sql_subs_apro)>0)
									{
										$arts_f[] = $all_fans['email'];
									}
								}
							}
						}
					}
				}
			}
			/*var_dump($arts_f);
			var_dump($coms_f);
			var_dump($others_f);
			
			var_dump($arts_s);
			var_dump($coms_s);
			var_dump($others_s);
			
			var_dump($fans);*/
			$all_emails = array_merge($arts_f,$coms_f,$others_f,$arts_s,$coms_s,$others_s,$fans);
			
			$new_array = array();
			foreach ($all_emails as $key => $value) {
				if(isset($new_array[$value]))
					$new_array[$value] += 1;
				else
					$new_array[$value] = 1;
			}
			foreach ($new_array as $uid => $n) {
			$ex_uid1=$ex_uid1.','.$uid;
			}
			$all_emails=explode(',',$ex_uid1);


			return $all_emails;
		}
		
						
		
		function email_all_community_fans()
		{
			$all_mails = array();
			
			$arts_f = array();
			$coms_f = array();
			$others_f = array();
			
			$arts_s = array();
			$coms_s = array();
			$others_s = array();
			
			$fans = array();
			//$coms_fi = array();
			//$others_fi = array();
			
			$chks_a = $this->check_avail_general($_SESSION['login_email']);
			if($chks_a=='1')
			{
				$sql_chks = mysql_query("SELECT * FROM general_user WHERE email='".$_SESSION['login_email']."'");
				$chks = mysql_fetch_assoc($sql_chks);
				
				$sql_fan = mysql_query("SELECT * FROM fan_club_membership WHERE email!='".$_SESSION['login_email']."' AND del_status=0");
				if(mysql_num_rows($sql_fan)>0)
				{
					while($all_fans = mysql_fetch_assoc($sql_fan))
					{
						if($chks['community_id']!=0 && $all_fans['related_type']=='community')
						{
							if($all_fans['related_id'] == $chks['community_id'])
							{
								$sql_subs_cpro = mysql_query("SELECT * FROM email_subscription WHERE email='".$all_fans['email']."' AND email_update=1 AND delete_status=1 AND related_type='community' AND related_id='".$all_fans['related_id']."'");
								
								if($sql_subs_cpro!="")
								{
									if(mysql_num_rows($sql_subs_cpro)>0)
									{
										$coms_f[] = $all_fans['email'];  
									}
								}
							}
						}

					}
				}
			}
			/*var_dump($arts_f);
			var_dump($coms_f);
			var_dump($others_f);
			
			var_dump($arts_s);
			var_dump($coms_s);
			var_dump($others_s);
			
			var_dump($fans);*/
			$all_emails = array_merge($arts_f,$coms_f,$others_f,$arts_s,$coms_s,$others_s,$fans);
			
			$new_array = array();
			foreach ($all_emails as $key => $value) {
				if(isset($new_array[$value]))
					$new_array[$value] += 1;
				else
					$new_array[$value] = 1;
			}
			foreach ($new_array as $uid => $n) {
			$ex_uid1=$ex_uid1.','.$uid;
			}
			$all_emails=explode(',',$ex_uid1);


			return $all_emails;
		}
		function email_all_artistpro_fans($id)
		{
			$all_mails = array();
			
			$arts_f = array();
			$coms_f = array();
			$others_f = array();
			
			$arts_s = array();
			$coms_s = array();
			$others_s = array();
			
			$fans = array();
			//$coms_fi = array();
			//$others_fi = array();
			
			$chks_a = $this->check_avail_general($_SESSION['login_email']);
			if($chks_a=='1')
			{
				$sql_fan = mysql_query("SELECT * FROM fan_club_membership WHERE email!='".$_SESSION['login_email']."' AND del_status=0");
				if(mysql_num_rows($sql_fan)>0)
				{
					while($all_fans = mysql_fetch_assoc($sql_fan))
					{
						if($id!=0 && $all_fans['related_type']=='artist_project')
						{
							if($all_fans['related_id'] == $id)
							{
								$sql_pros_art = mysql_query("SELECT * FROM artist_project WHERE id='".$id."'");
								$ans_pros_art = mysql_fetch_assoc($sql_pros_art);
								
								$sql_art_prosubs = mysql_query("SELECT * FROM email_subscription WHERE email='".$all_fans['email']."' AND email_update=1 AND delete_status=1 AND related_type='artist_project' AND related_id='".$ans_pros_art['artist_id']."_".$id."'");
								if($sql_art_prosubs!="")
								{
									if(mysql_num_rows($sql_art_prosubs)>0)
									{
										$coms_f[] = $all_fans['email'];  
									}
								}
							}
						}

					}
				}
			}
			/*var_dump($arts_f);
			var_dump($coms_f);
			var_dump($others_f);
			
			var_dump($arts_s);
			var_dump($coms_s);
			var_dump($others_s);
			
			var_dump($fans);*/
			$all_emails = array_merge($arts_f,$coms_f,$others_f,$arts_s,$coms_s,$others_s,$fans);
			
			$new_array = array();
			foreach ($all_emails as $key => $value) {
				if(isset($new_array[$value]))
					$new_array[$value] += 1;
				else
					$new_array[$value] = 1;
			}
			foreach ($new_array as $uid => $n) {
			$ex_uid1=$ex_uid1.','.$uid;
			}
			$all_emails=explode(',',$ex_uid1);


			return $all_emails;
		}
		function email_all_communitypro_fans($id)
		{
			$all_mails = array();
			
			$arts_f = array();
			$coms_f = array();
			$others_f = array();
			
			$arts_s = array();
			$coms_s = array();
			$others_s = array();
			
			$fans = array();
			//$coms_fi = array();
			//$others_fi = array();
			
			$chks_a = $this->check_avail_general($_SESSION['login_email']);
			if($chks_a=='1')
			{
				$sql_fan = mysql_query("SELECT * FROM fan_club_membership WHERE email!='".$_SESSION['login_email']."' AND del_status=0");
				if(mysql_num_rows($sql_fan)>0)
				{
					while($all_fans = mysql_fetch_assoc($sql_fan))
					{
						if($id!=0 && $all_fans['related_type']=='community_project')
						{
							if($all_fans['related_id'] == $id)
							{
								$sql_pros_com = mysql_query("SELECT * FROM community_project WHERE id='".$id."'");
								$ans_pros_com = mysql_fetch_assoc($sql_pros_com);
								
								$sql_subs = mysql_query("SELECT * FROM email_subscription WHERE email='".$all_fans['email']."' AND email_update=1 AND delete_status=1 AND related_type='community_project' AND related_id='".$ans_pros_com['community_id']."_".$id."'");
								if($sql_subs!="")
								{
									if(mysql_num_rows($sql_subs)>0)
									{
										$coms_f[] = $all_fans['email'];  
									}
								}
							}
						}
					}
				}
			}
			/*var_dump($arts_f);
			var_dump($coms_f);
			die;
			var_dump($others_f);
			
			var_dump($arts_s);
			var_dump($coms_s);
			var_dump($others_s);
			
			var_dump($fans);*/
			$all_emails = array_merge($arts_f,$coms_f,$others_f,$arts_s,$coms_s,$others_s,$fans);
			
			$new_array = array();
			foreach ($all_emails as $key => $value) {
				if(isset($new_array[$value]))
					$new_array[$value] += 1;
				else
					$new_array[$value] = 1;
			}
			foreach ($new_array as $uid => $n) {
			$ex_uid1=$ex_uid1.','.$uid;
			}
			$all_emails=explode(',',$ex_uid1);


			return $all_emails;
		}
		function add_news_feed($description,$scspl,$list_em)
		{
			$chks_a = $this->check_avail_general($_SESSION['login_email']);
			if($chks_a=='1')
			{
				$sql_chks = mysql_query("SELECT * FROM general_user WHERE email='".$_SESSION['login_email']."'");
				$chks = mysql_fetch_assoc($sql_chks);
				
				$date_time = date('Y-m-d h:i:s');
				
				$sql = mysql_query("insert into post_info (general_user_id,profile_type,media,description,display_to,date_time) values('".$chks['general_user_id']."','".$scspl."','".$list_em."','".$description."','All','".$date_time."')");
			}
		}
		
		function get_substypes($id,$type)
		{
			$types_re = '';
			if($type=='community_event')
			{
				$sql = mysql_query("SELECT MIN( `id` ) FROM `community_event_profiles` WHERE community_event_id='".$id."' AND subtype_id!=0");
							
				if(mysql_num_rows($sql)>0)
				{
					$res = mysql_fetch_assoc($sql);
					
					$sql2 = mysql_query("select * from community_event_profiles where id='".$res['MIN( `id` )']."'");
					$res2 = mysql_fetch_assoc($sql2);

					$sql4 = mysql_query("select * from  subtype where subtype_id = '".$res2['subtype_id']."'");
					if(mysql_num_rows($sql4)>0)
					{
						$res4 = mysql_fetch_assoc($sql4);
						$types_re = $res4['name'];
					}
					else
					{
						$types_re = '';
					}
				}
				else
				{
					$types_re = '';
				}
			}
			elseif($type=='community_project')
			{
				$sql = mysql_query("SELECT MIN( `id` ) FROM `community_project_profiles` WHERE community_project_id='".$id."' AND subtype_id!=0");
							
				if(mysql_num_rows($sql)>0)
				{
					$res = mysql_fetch_assoc($sql);
					
					$sql2 = mysql_query("select * from community_project_profiles where id='".$res['MIN( `id` )']."'");
					$res2 = mysql_fetch_assoc($sql2);

					$sql4 = mysql_query("select * from  subtype where subtype_id = '".$res2['subtype_id']."'");
					if(mysql_num_rows($sql4)>0)
					{
						$res4 = mysql_fetch_assoc($sql4);
						$types_re = $res4['name'];
					}
					else
					{
						$types_re = '';
					}
				}
				else
				{
					$types_re = '';
				}
			}
			elseif($type=='artist_project')
			{
				$sql = mysql_query("SELECT MIN( `id` ) FROM `artist_project_profiles` WHERE artist_project_id='".$id."' AND subtype_id!=0");
							
				if(mysql_num_rows($sql)>0)
				{
					$res = mysql_fetch_assoc($sql);
					
					$sql2 = mysql_query("select * from artist_project_profiles where id='".$res['MIN( `id` )']."'");
					$res2 = mysql_fetch_assoc($sql2);

					$sql4 = mysql_query("select * from  subtype where subtype_id = '".$res2['subtype_id']."'");
					if(mysql_num_rows($sql4)>0)
					{
						$res4 = mysql_fetch_assoc($sql4);
						$types_re = $res4['name'];
					}
					else
					{
						$types_re = '';
					}
				}
				else
				{
					$types_re = '';
				}
			}
			elseif($type=='artist_event')
			{
				$sql = mysql_query("SELECT MIN( `id` ) FROM `artist_event_profiles` WHERE artist_event_id='".$id."' AND subtype_id!=0");
							
				if(mysql_num_rows($sql)>0)
				{
					$res = mysql_fetch_assoc($sql);
					
					$sql2 = mysql_query("select * from artist_event_profiles where id='".$res['MIN( `id` )']."'");
					$res2 = mysql_fetch_assoc($sql2);

					$sql4 = mysql_query("select * from  subtype where subtype_id = '".$res2['subtype_id']."'");
					if(mysql_num_rows($sql4)>0)
					{
						$res4 = mysql_fetch_assoc($sql4);
						$types_re = $res4['name'];
					}
					else
					{
						$types_re = '';
					}
				}
				else
				{
					$types_re = '';
				}
			}
			
			return $types_re;
		}
		function Get_Artist_Info_e($id)
		{
			$get_art = mysql_query("select * from general_artist where artist_id='".$id."'");
			if(mysql_num_rows($get_art)>0){
				$res_art = mysql_fetch_assoc($get_art);
					return ($res_art);
			}
		}
		
		function Get_community_Info_e($id)
		{
			$get_art = mysql_query("select * from general_community where community_id='".$id."'");
			if(mysql_num_rows($get_art)>0){
				$res_art = mysql_fetch_assoc($get_art);
					return ($res_art);
			}
		}
		function get_login_user_details()
		{
			$query = mysql_query("select * from general_user where email='".$_SESSION['login_email']."'");
			if(mysql_num_rows($query)>0){
				$res = mysql_fetch_assoc($query);
				return($res);
			}
		}
		function find_artist_fans()
		{
			$get_det = $this->get_login_user_details();
			$sql = mysql_query("select * from fan_club_membership where related_id='".$get_det['artist_id']."' AND related_type='artist'");
			if(mysql_num_rows($sql)>0){
			return 1;
			}
		}
		function find_community_fans()
		{
			$get_det = $this->get_login_user_details();
			$sql = mysql_query("select * from fan_club_membership where related_id='".$get_det['community_id']."' AND related_type='community'");
			if(mysql_num_rows($sql)>0){
			return 1;
			}
		}
		function get_all_artist_projects()
		{
			$get_det = $this->get_login_user_details();
			$query = mysql_query("select * from artist_project where artist_id='".$get_det['artist_id']."' AND del_status=0 AND active=0");
			if(mysql_num_rows($query)>0){
				return($query);
			}
		}
		function get_all_community_projects()
		{
			$get_det = $this->get_login_user_details();
			$query = mysql_query("select * from community_project where community_id='".$get_det['community_id']."' AND del_status=0 AND active=0");
			if(mysql_num_rows($query)>0){
				return($query);
			}
		}
		function find_projects_fans($id,$type)
		{
			$get_det = $this->get_login_user_details();
			$sql = mysql_query("select * from fan_club_membership where related_id='".$id."' AND related_type='".$type."'");
			if(mysql_num_rows($sql)>0){
				return 1;
			}
		}
		
		function get_creators($id)
		{
			$ans = "";
			$exp_id = explode('|',$id);
			if($exp_id[0]=='general_artist')
			{
				$sql = mysql_query("SELECT * FROM general_artist WHERE artist_id='".$exp_id[1]."'");
			}
			elseif($exp_id[0]=='general_community')
			{
				$sql = mysql_query("SELECT * FROM general_community WHERE community_id='".$exp_id[1]."'");
			}
			elseif($exp_id[0]=='artist_project' || $exp_id[0]=='community_project')
			{
				$sql = mysql_query("SELECT * FROM $exp_id[0] WHERE id='".$exp_id[1]."'");
			}
			
			if(mysql_num_rows($sql)>0)
			{
				while($row = mysql_fetch_assoc($sql))
				{
					$ans = $row;
				}
			}
			
			return($ans);
		}
	}
?>