<?php 
session_start();
include_once('../commons/db.php');

class Media
{
	function addGalleryImagesEntry($title,$files,$profile_id,$profile_type)
	{
		$query="insert into general_".$profile_type."_gallery_list (`gallery_title`,`created_date`) values ('".$title."',CURDATE())";
		$rs=mysql_query($query) or die(mysql_error());
		$gallery_id=mysql_insert_id();
		if($gallery_id){
			$fileList = explode("|",$files);
			for($i=0;$i<count($fileList);$i++){
				if($fileList[$i]!=""){
					$query="insert into general_".$profile_type."_gallery (`profile_id`,`gallery_id`,`image_name`,`created_date`) values ('$profile_id','".$gallery_id."','".$fileList[$i]."',CURDATE())";
					$rs=mysql_query($query) or die(mysql_error());
				}
			}
		}
	}

	function addMediaAudiosEntry($title,$files,$profile_id,$profile_type)
	{
		$fileList = explode("|",$files);
		for($i=0;$i<count($fileList);$i++){
			if($fileList[$i]!=""){
				$query="insert into general_".$profile_type."_audio (`profile_id`,`audio_name`,`audio_file_name`,`created_date`) values ('$profile_id','".$title."','".$fileList[$i]."',CURDATE())";
				$rs=mysql_query($query) or die(mysql_error());
			}
		}
	}

	function addMediaAudioLinkEntry($title,$link,$profile_id,$profile_type)
	{
		if($link!=""){
			$query="insert into general_".$profile_type."_audio (`profile_id`,`audio_name`,`audio_link`,`created_date`) values ('$profile_id','".$title."','".$link."',CURDATE())";
			$rs=mysql_query($query) or die(mysql_error());
		}
	}

	function addMediaVideoLinkEntry($title,$link,$profile_id,$profile_type)
	{
		if($link!=""){
			$query="insert into general_".$profile_type."_video (`profile_id`,`video_name`,`video_link`,`created_date`) values ('$profile_id','".$title."','".$link."',CURDATE())";
			$rs=mysql_query($query) or die(mysql_error());
		}
	}

	function getGalleryImagesByProfileId($profile_id,$profile_type)
	{
		$rs = "";
		$sql="select image_name from general_".$profile_type."_gallery where profile_id='".$profile_id."'";
		$rs=mysql_query($sql) or die(mysql_error());
		if($rs!="")
		{
			return mysql_fetch_assoc($rs);	  	
		}
		else{
			return $rs;
		}
	}  

	function searchMediaByProfileId($profile_id,$profile_type){
		$mediaType = "";
		$resultMediaArray = Array();
		$sql="SELECT L.*,G.image_name,G.image_title FROM general_".$profile_type."_gallery as G, general_".$profile_type."_gallery_list as L 
			WHERE L.gallery_id=G.gallery_id 
			AND G.profile_id='".$profile_id."' GROUP BY L.gallery_id";
		$rs=mysql_query($sql) or die(mysql_error());
		if($rs!="")
		{
			if(mysql_num_rows($rs)){
				$mediaType = "gallery";
				$resultArray = Array();
				$gCount = 0;
				while ($row = mysql_fetch_assoc($rs)) {
					$resultArray[$gCount][0] = $row;
					$imagesArr = Array();
					$sql1="SELECT G.* FROM general_".$profile_type."_gallery as G, general_".$profile_type."_gallery_list as L 
						WHERE L.gallery_id=G.gallery_id 
						AND G.profile_id='".$profile_id."' AND L.gallery_id='".$row['gallery_id']."'";
					$rs1=mysql_query($sql1) or die(mysql_error());
					if($rs1!="")
					{
						if(mysql_num_rows($rs1)){
							while ($row1 = mysql_fetch_assoc($rs1)) {						
								$imagesArr[] = $row1;						
							}
							$resultArray[$gCount][1] = $imagesArr;
							$gCount++;
						}
					}
				}

				$resultMediaArray[0][0] = $mediaType;
				$resultMediaArray[0][1] = $resultArray;
			}
		}
		
		$sql="SELECT * FROM general_".$profile_type."_audio WHERE profile_id='".$profile_id."'";
		$rs=mysql_query($sql) or die(mysql_error());
		if($rs!="")
		{
			if(mysql_num_rows($rs)){
				$mediaType = "audio";
				$resultArray = Array();
				while ($row = mysql_fetch_assoc($rs)) {
					$resultArray[] = $row;
				}			
				$resultMediaArray[1][0] = $mediaType;
				$resultMediaArray[1][1] = $resultArray;
			}
		}
				
		$sql="SELECT * FROM general_".$profile_type."_video WHERE profile_id='".$profile_id."'";
		$rs=mysql_query($sql) or die(mysql_error());
		if($rs!="")
		{
			if(mysql_num_rows($rs)){
				$mediaType = "video";
				$resultArray = Array();
				while ($row = mysql_fetch_assoc($rs)) {
					$resultArray[] = $row;
				}			
				$resultMediaArray[2][0] = $mediaType;
				$resultMediaArray[2][1] = $resultArray;
			}
		}
				
		return $resultMediaArray;
	}
	
	function getMediaById($type,$media_id,$profile_id,$profile_type){
		switch($type){
			case "video":
			case "v":
						$sql="SELECT * FROM general_".$profile_type."_video WHERE video_id='".$media_id."'";
						$rs=mysql_query($sql) or die(mysql_error());
						if($rs!="")
						{
							if(mysql_num_rows($rs)){
								$resultArray = Array();
								while ($row = mysql_fetch_assoc($rs)) {
									$resultArray[] = $row;
								}			
								return $resultArray;
							}
						}
					break;
			case "audio":
			case "a":
						$sql="SELECT * FROM general_".$profile_type."_audio WHERE audio_id='".$media_id."'";
						$rs=mysql_query($sql) or die(mysql_error());
						if($rs!="")
						{
							if(mysql_num_rows($rs)){
								$resultArray = Array();
								while ($row = mysql_fetch_assoc($rs)) {
									if($row['gallery_relation']>0){
										$sql1 = "SELECT G.*,GL.gallery_title, 'gallery' as image_type FROM general_".$profile_type."_gallery as G, general_".$profile_type."_gallery_list as GL WHERE G.gallery_id=GL.gallery_id AND  G.gallery_id='".$row['gallery_relation']."'";
										$rs1=mysql_query($sql1) or die(mysql_error());
										if($rs1!="")
										{
											if(mysql_num_rows($rs1)){
												$galleryArray = Array();
												while ($row1 = mysql_fetch_assoc($rs1)) {
													$galleryArray[] = $row1;
												}
											}
										}
									}else{
										$sql1 = "SELECT '1' as img_id, image_name, name as image_title, 'profile_pic' as image_type FROM general_".$profile_type." WHERE artist_id='".$profile_id."'";
										$rs1=mysql_query($sql1) or die(mysql_error());
										if($rs1!="")
										{
											if(mysql_num_rows($rs1)){
												$galleryArray = Array();
												while ($row1 = mysql_fetch_assoc($rs1)) {
													$galleryArray[] = $row1;
												}
											}
										}
									}
									$row['gallery_files'] = $galleryArray;
									$resultArray[] = $row;
								}			
								return $resultArray;
							}
						}
					break;
		}
		
	}

	function getUserBucket($user_id){
		$rs = "";
		$sql="select bucket_name from general_user where general_user_id='".$user_id."'";
		$rs=mysql_query($sql) or die(mysql_error());
		if($rs!="")
		{
			$row = mysql_fetch_assoc($rs);	  			
			return $row['bucket_name'];
		}
		else
		{
			return $rs;
		}
	}

	function getMediaListByArtistId($type,$artist_id,$profile_type){
		switch($type){
			case "audio":
			case "a":
						$sql="SELECT * FROM general_".$profile_type."_audio WHERE profile_id='".$artist_id."'";
						$rs=mysql_query($sql) or die(mysql_error());
						if($rs!="")
						{
							if(mysql_num_rows($rs)){
								$resultArray = Array();
								while ($row = mysql_fetch_assoc($rs)) {
									if($row['gallery_relation']>0){
										$sql1 = "SELECT *, 'gallery' as image_type FROM general_".$profile_type."_gallery WHERE gallery_id='".$row['gallery_relation']."'";
										$rs1=mysql_query($sql1) or die(mysql_error());
										if($rs1!="")
										{
											if(mysql_num_rows($rs1)){
												$galleryArray = Array();
												while ($row1 = mysql_fetch_assoc($rs1)) {
													$galleryArray[] = $row1;
												}
											}
										}
									}else{
										$sql1 = "SELECT '1' as img_id, image_name, name as image_title, 'profile_pic' as image_type FROM general_".$profile_type." ";
										$rs1=mysql_query($sql1) or die(mysql_error());
										if($rs1!="")
										{
											if(mysql_num_rows($rs1)){
												$galleryArray = Array();
												while ($row1 = mysql_fetch_assoc($rs1)) {
													$galleryArray[] = $row1;
												}
											}
										}
									}
									$row['gallery_files'] = $galleryArray;
									$resultArray[] = $row;
								}			
								return $resultArray;
							}
						}
				break;
			case "video":
			case "v":
						$sql="SELECT * FROM general_".$profile_type."_video WHERE profile_id='".$artist_id."'";
						$rs=mysql_query($sql) or die(mysql_error());
						if($rs!="")
						{
							if(mysql_num_rows($rs)){
								$resultArray = Array();
								while ($row = mysql_fetch_assoc($rs)) {
									$resultArray[] = $row;
								}			
								return $resultArray;
							}
						}
				break;
		}
	}
	
	function newgetMediaListByArtistId($type,$artist_id,$profile_type,$media_id){
		switch($type){
			case "audio":
			case "a":
						//$sql="SELECT * FROM general_media WHERE general_user_id	='".$artist_id."' AND id='".$media_id."'";
						$sql="SELECT * FROM general_media WHERE id='".$media_id."'";
						$rs=mysql_query($sql) or die(mysql_error());
						if($rs!="")
						{
							if(mysql_num_rows($rs)){
								$resultArray = Array();
								while ($row = mysql_fetch_assoc($rs)) {
								if($row['from_info']!="" && $row['from_info']!=null)
								{
									$exp_from =explode("|",$row['from_info']);
									$sql_from = mysql_query("select * from $exp_from[0] where id=$exp_from[1]");
									if($sql_from!="")
									{
										$galleryArray = Array();
										$res_from = mysql_fetch_assoc($sql_from);
										if($exp_from[0] == "general_artist")
										{
											$res_from['img_path'] = "http://artjcropprofile.s3.amazonaws.com/";
										}
										else if($exp_from[0] == "general_community")
										{
											$res_from['img_path'] = "http://comjcropprofile.s3.amazonaws.com/";
										}
										else
										{
											$parse = parse_url($res_from['image_name']);
											$res_from['img_path'] = 'http://'.$parse['host'].'/';
											$res_from['image_name'] = basename($res_from['image_name']);
										}
										$galleryArray[] = $res_from;
									}
								}
								if($row['creator_info']!="" && $row['creator_info']!=null)
								{
									$exp_creator =explode("|",$row['creator_info']);
									if($exp_creator[0] == "general_community"){
										$id="community_id";}
									else if($exp_creator[0] == "general_artist"){
										$id="artist_id";}
									else {
										$id="id";}
									
									$sql_creator = mysql_query("select * from $exp_creator[0] where $id=$exp_creator[1]");
									if($sql_creator!="")
									{
										
										$res_creator = mysql_fetch_assoc($sql_creator);
										if(!isset($galleryArray) || empty($galleryArray))
										{
											$galleryArray = Array();
											if($exp_creator[0] == "general_artist")
											{
												$res_creator['img_path'] = "http://artjcropprofile.s3.amazonaws.com/";
											}
											else if($exp_creator[0] == "general_community")
											{
												$res_creator['img_path'] = "http://comjcropprofile.s3.amazonaws.com/";
											}
											else
											{
												$parse = parse_url($res_creator['image_name']);
												$res_creator['img_path'] = 'http://'.$parse['host'].'/';
												$res_creator['image_name'] = basename($res_creator['image_name']);
											}
											$galleryArray[] = $res_creator;
										}
									}
								}
								
									$sql_media = mysql_query("SELECT * FROM media_songs WHERE media_id='".$media_id."'");
									if($sql_media!="")
									{
										$ans_run_media = mysql_fetch_assoc($sql_media);
									}
									
									if($ans_run_media['gallery_id']>0 && $ans_run_media['gallery_at']=="general_media" ){
										$sql1 = "SELECT *, 'gallery' as image_type FROM media_images WHERE media_id='".$ans_run_media['gallery_id']."'";
										$rs1=mysql_query($sql1) or die(mysql_error());
										if($rs1!="")
										{
											if(mysql_num_rows($rs1)){
												$galleryArray = Array();
												while ($row1 = mysql_fetch_assoc($rs1)) {
													$row1['img_path'] = "http://medgallery.s3.amazonaws.com/";
													$galleryArray[] = $row1;
												}
												
												$sql_title_gallery_new = mysql_query("SELECT * FROM media_gallery WHERE gallery_id='".$galleryArray[0]['gallery_id']."'");
												if($sql_title_gallery_new!="")
												{
													$run_title_gallery_new = mysql_fetch_assoc($sql_title_gallery_new);
												}
											}
										}
									}
									else if($ans_run_media['gallery_id']>0 && $ans_run_media['gallery_at']!="general_media" ){
										$sql1 = "SELECT *, 'gallery' as image_type FROM ".$ans_run_media['gallery_at']."_gallery WHERE gallery_id='".$ans_run_media['gallery_id']."'";
										$rs1=mysql_query($sql1) or die(mysql_error());
										if($rs1!="")
										{
											if(mysql_num_rows($rs1)){
												$galleryArray = Array();
												while ($row1 = mysql_fetch_assoc($rs1)) {
													$row1['img_path'] = "http://reggallery.s3.amazonaws.com/";
													$galleryArray[] = $row1;
												}
												
												//$sql_title_gallery_new = mysql_query("SELECT * FROM media_gallery WHERE gallery_id='".$galleryArray[0]['gallery_id']."'");
												//$run_title_gallery_new = mysql_fetch_assoc($sql_title_gallery_new);
											}
										}
									}
									else if($ans_run_media['gallery_id']==0 || $ans_run_media['gallery_at']==" " ){
										if(!isset($galleryArray))
										{
											$get_det = mysql_query("select * from general_media where id='".$media_id."'");
											if($get_det!="")
											{
												if(mysql_num_rows($get_det)>0){
													$res_det = mysql_fetch_assoc($get_det);
													$get_pro = mysql_query("select * from general_user where general_user_id = '".$res_det['general_user_id']."' AND delete_status=0");
													if($get_pro!="")
													{
														if(mysql_num_rows($get_pro)>0){
															$res_gen_det = mysql_fetch_assoc($get_pro);
															if($res_gen_det['artist_id']>0 || $res_gen_det['artist_id']!="")
															{
																$get_artist_detail = mysql_query("select * from general_artist where artist_id='".$res_gen_det['artist_id']."' AND status=0 AND del_status=0 AND active=0");
																if($get_artist_detail!="")
																{
																	if(mysql_num_rows($get_artist_detail)>0){
																		$res_artist_detail = mysql_fetch_assoc($get_artist_detail);
																		$res_artist_detail['img_path'] = "http://artjcropprofile.s3.amazonaws.com/";
																		$galleryArray[] = $res_artist_detail;
																	}
																}
															}
															if(empty($res_artist_detail) && ($res_gen_det['community_id']>0 || $res_gen_det['community_id']!=""))
															{
																$get_community_detail = mysql_query("select * from general_community where community_id='".$res_gen_det['community_id']."' AND status=0 AND del_status=0 AND active=0");
																if($get_community_detail!="")
																{
																	if(mysql_num_rows($get_community_detail)>0){
																		$res_community_detail = mysql_fetch_assoc($get_community_detail);
																		$res_community_detail['img_path'] = "http://comjcropprofile.s3.amazonaws.com/";
																		$galleryArray[] = $res_community_detail;
																	}
																}
															}
														}
													}
												}
											}
										}
									}
									else{
										$sql1 = "SELECT '1' as img_id, image_name, name as image_title, 'profile_pic' as image_type FROM general_".$profile_type." ";
										$rs1=mysql_query($sql1) or die(mysql_error());
										if($rs1!="")
										{
											if(mysql_num_rows($rs1)){
												$galleryArray = Array();
												while ($row1 = mysql_fetch_assoc($rs1)) {
													$galleryArray[] = $row1;
												}
											}
										}
									}
									$ans_run_media['gallery_files'] = $galleryArray;
									$ans_run_media['song_title_read'] = $row['title'];
									$ans_run_media['media_creator'] = $row['creator'];
									$ans_run_media['media_from'] = $row['from'];
									$ans_run_media['sharing_preference'] = $row['sharing_preference'];
									$ans_run_media['from_profileurl'] = $res_from['profile_url'];
									$ans_run_media['creator_profileurl'] = $res_creator['profile_url'];
									if(isset($run_title_gallery_new['gallery_title'])){
									$ans_run_media['gallery_title_new'] = $run_title_gallery_new['gallery_title']; }else{
										$ans_run_media['gallery_title_new'] = "";
									}
									$resultArray[] = $ans_run_media;
								}		
								//var_dump($resultArray);
								return $resultArray;
							}
						}
				break;
			case "video":
			case "v":
						$sql="SELECT mv. * , gm.creator, gm.from, gm.creator_info, gm.from_info FROM media_video mv LEFT JOIN general_media gm ON mv.media_id = gm.id WHERE mv.media_id ='".$media_id."'";
						$rs=mysql_query($sql) or die(mysql_error());
						if($rs!="")
						{
							if(mysql_num_rows($rs)){
								$resultArray = Array();
								while ($row = mysql_fetch_assoc($rs)) {
									if($row['from_info']!="" && $row['from_info']!=null)
									{
										$exp_from =explode("|",$row['from_info']);
										if($exp_from[0] !="" && $exp_from[1]!="")
										{
											$sql_from = mysql_query("select * from $exp_from[0] where id=$exp_from[1]");
											if($sql_from!="")
											{
												$res_from = mysql_fetch_assoc($sql_from);
											}
										}
									}
									if($row['creator_info']!="" && $row['creator_info']!=null)
									{
										$exp_creator =explode("|",$row['creator_info']);
										if($exp_creator[0] == "general_community"){
											$id="community_id";}
										else if($exp_creator[0] == "general_artist"){
											$id="artist_id";}
										else {
											$id="id";}
										if($exp_creator[1]!="" && $exp_creator[0]!="")
										{
											$sql_creator = mysql_query("select * from $exp_creator[0] where $id=$exp_creator[1]");
											if($sql_creator!="")
											{
												$res_creator = mysql_fetch_assoc($sql_creator);
											}
										}
									}
									if(isset($res_from['profile_url'])){ $row['from_profileurl']= $res_from['profile_url']; }else{
										$row['from_profileurl']= ""; }
									$row['creator_profileurl']= $res_creator['profile_url'];
									$resultArray[] = $row;
								}			
								return $resultArray;
							}
						}						
				break;
		}
	}

	function checkUserLogin($uname,$upass){ //Check for general user & common user login
		$sql="SELECT * FROM general_user WHERE email='".mysql_real_escape_string($uname)."' AND pass='".mysql_real_escape_string($upass)."'";
		$rs=mysql_query($sql) or die(mysql_error());
		if(mysql_num_rows($rs))
			return TRUE;
		else
			return FALSE;
	}
	
	
	
	/*function newgetAddMediaListByArtistId($type,$artist_id,$profile_type,$media_id){
		switch($type){
			case "audio":
			case "a":
				$sql="SELECT * FROM general_media WHERE general_user_id	='".$artist_id."' AND id='".$media_id."'";
				$rs=mysql_query($sql) or die(mysql_error());
				if(mysql_num_rows($rs)){
					$resultArray = Array();
					while ($row = mysql_fetch_assoc($rs)) {
						$sql_media = mysql_query("SELECT * FROM media_songs WHERE media_id='".$media_id."'");
						$ans_run_media = mysql_fetch_assoc($sql_media);
						$resultArray[] = $ans_run_media;
					}		
					//var_dump($resultArray);
					return $resultArray;
				}
			break;
		}
	
	}*/
	function newAddMediaListByArtistId($type,$artist_id,$profile_type,$media_id){
		switch($type){
			case "add":
						$sql="SELECT * FROM general_media WHERE id='".$media_id."'";
						$rs=mysql_query($sql) or die(mysql_error());
						if($rs!="")
						{
							if(mysql_num_rows($rs)){
								$resultArray = Array();
								while ($row = mysql_fetch_assoc($rs)) {
								if($row['from_info']!="" && $row['from_info']!=null)
								{
									$exp_from =explode("|",$row['from_info']);
									$sql_from = mysql_query("select * from $exp_from[0] where id=$exp_from[1]");
									if($sql_from!="")
									{
										$galleryArray = Array();
										$res_from = mysql_fetch_assoc($sql_from);
										if($exp_from[0] == "general_artist")
										{
											$res_from['img_path'] = "http://artjcropprofile.s3.amazonaws.com/";
										}
										else if($exp_from[0] == "general_community")
										{
											$res_from['img_path'] = "http://comjcropprofile.s3.amazonaws.com/";
										}
										else
										{
											$parse = parse_url($res_from['image_name']);
											$res_from['img_path'] = 'http://'.$parse['host'].'/';
											$res_from['image_name'] = basename($res_from['image_name']);
										}
										$galleryArray[] = $res_from;
									}
								}
								if($row['creator_info']!="" && $row['creator_info']!=null)
								{
									$exp_creator =explode("|",$row['creator_info']);
									if($exp_creator[0] == "general_community"){
										$id="community_id";}
									else if($exp_creator[0] == "general_artist"){
										$id="artist_id";}
									else {
										$id="id";}
									
									$sql_creator = mysql_query("select * from $exp_creator[0] where $id=$exp_creator[1]");
									if($sql_creator!="")
									{
										$res_creator = mysql_fetch_assoc($sql_creator);
										
										if(!isset($galleryArray) || empty($galleryArray))
										{
											$galleryArray = Array();
											if($exp_creator[0] == "general_artist")
											{
												$res_creator['img_path'] = "http://artjcropprofile.s3.amazonaws.com/";
											}
											else if($exp_creator[0] == "general_community")
											{
												$res_creator['img_path'] = "http://comjcropprofile.s3.amazonaws.com/";
											}
											else
											{
												$parse = parse_url($res_creator['image_name']);
												$res_creator['img_path'] = 'http://'.$parse['host'].'/';
												$res_creator['image_name'] = basename($res_creator['image_name']);
											}
											$galleryArray[] = $res_creator;
										}
									}
								}
								
									$sql_media = mysql_query("SELECT * FROM media_songs WHERE media_id='".$media_id."'");
									if($sql_media!="")
									{
										$ans_run_media = mysql_fetch_assoc($sql_media);
										if($ans_run_media['gallery_id']>0 && $ans_run_media['gallery_at'] == "general_media"){
											$sql1 = "SELECT *, 'gallery' as image_type FROM media_images WHERE media_id='".$ans_run_media['gallery_id']."'";
											$rs1=mysql_query($sql1) or die(mysql_error());
											if($rs1!="")
											{
												if(mysql_num_rows($rs1)){
													$galleryArray = Array();
													while ($row1 = mysql_fetch_assoc($rs1)) {
														$row1['img_path'] = "http://medgallery.s3.amazonaws.com/";
														$galleryArray[] = $row1;
													}
													
													$sql_title_gallery_new = mysql_query("SELECT * FROM media_gallery WHERE gallery_id='".$galleryArray[0]['gallery_id']."'");
													if($sql_title_gallery_new!="")
													{
														$run_title_gallery_new = mysql_fetch_assoc($sql_title_gallery_new);
													}
												}
											}
										}
										else if($ans_run_media['gallery_id']>0 && $ans_run_media['gallery_at'] != "general_media"){
											$sql1 = "SELECT *, 'gallery' as image_type FROM ".$ans_run_media['gallery_at']."_gallery WHERE gallery_id='".$ans_run_media['gallery_id']."'";
											$rs1=mysql_query($sql1) or die(mysql_error());
											if($rs1!="")
											{
												if(mysql_num_rows($rs1)){
													$galleryArray = Array();
													while ($row1 = mysql_fetch_assoc($rs1)) {
														$row1['img_path'] = "http://reggallery.s3.amazonaws.com/";
														$galleryArray[] = $row1;
													}
													
													//$sql_title_gallery_new = mysql_query("SELECT * FROM media_gallery WHERE gallery_id='".$galleryArray[0]['gallery_id']."'");
													//$run_title_gallery_new = mysql_fetch_assoc($sql_title_gallery_new);
												}
											}
										}
										else if($ans_run_media['gallery_id']==0 || $ans_run_media['gallery_at']==" " || $ans_run_media['gallery_at']!="general_media"){
										if(!isset($galleryArray))
										{
											$get_det = mysql_query("select * from general_media where id='".$media_id."'");
											if($get_det!="")
											{
												if(mysql_num_rows($get_det)>0){
													$res_det = mysql_fetch_assoc($get_det);
													$get_pro = mysql_query("select * from general_user where general_user_id = '".$res_det['general_user_id']."' AND delete_status=0");
													if($get_pro!="")
													{
														if(mysql_num_rows($get_pro)>0){
															$res_gen_det = mysql_fetch_assoc($get_pro);
															if($res_gen_det['artist_id']>0 || $res_gen_det['artist_id']!="")
															{
																$get_artist_detail = mysql_query("select * from general_artist where artist_id='".$res_gen_det['artist_id']."' AND status=0 AND del_status=0 AND active=0");
																if($get_artist_detail!="")
																{
																	if(mysql_num_rows($get_artist_detail)>0){
																		$res_artist_detail = mysql_fetch_assoc($get_artist_detail);
																		$res_artist_detail['img_path'] = "http://artjcropprofile.s3.amazonaws.com/";
																		$galleryArray[] = $res_artist_detail;
																	}
																}
															}
															if(empty($res_artist_detail) && ($res_gen_det['community_id']>0 || $res_gen_det['community_id']!=""))
															{
																$get_community_detail = mysql_query("select * from general_community where community_id='".$res_gen_det['community_id']."' AND status=0 AND del_status=0 AND active=0");
																if($get_community_detail!="")
																{
																	if(mysql_num_rows($get_community_detail)>0){
																		$res_community_detail = mysql_fetch_assoc($get_community_detail);
																		$res_community_detail['img_path'] = "http://comjcropprofile.s3.amazonaws.com/";
																		$galleryArray[] = $res_community_detail;
																	}
																}
															}
														}
													}
												}
											}
										}
									}
										else{
											$sql1 = "SELECT '1' as img_id, image_name, name as image_title, 'profile_pic' as image_type FROM general_".$profile_type." ";
											$rs1=mysql_query($sql1) or die(mysql_error());
											if($rs1!="")
											{
												if(mysql_num_rows($rs1)){
													$galleryArray = Array();
													while ($row1 = mysql_fetch_assoc($rs1)) {
														//$row1['img_path'] = "http://azhar.s3.amazonaws.com/";
														$galleryArray[] = $row1;
													}
												}
											}
										}
										$ans_run_media['gallery_files'] = $galleryArray;
										$ans_run_media['song_title_read'] = $row['title'];
										$ans_run_media['media_creator'] = $row['creator'];
										$ans_run_media['media_from'] = $row['from'];
										$ans_run_media['sharing_preference'] = $row['sharing_preference'];
										$ans_run_media['from_profileurl'] = $res_from['profile_url'];
										$ans_run_media['creator_profileurl'] = $res_creator['profile_url'];
										$ans_run_media['gallery_title_new'] = $run_title_gallery_new['gallery_title'];
										$resultArray[] = $ans_run_media;
									}
								}		
								//var_dump($resultArray);
								return $resultArray;
							}
						}
				break;
			case "addvi":
						$sql="SELECT mv. * , gm.creator, gm.from, gm.creator_info, gm.from_info FROM media_video mv LEFT JOIN general_media gm ON mv.media_id = gm.id WHERE media_id ='".$media_id."'";
						$rs=mysql_query($sql) or die(mysql_error());
						if($rs!="")
						{
							if(mysql_num_rows($rs)){
								$resultArray = Array();
								while ($row = mysql_fetch_assoc($rs)) {
									if($row['from_info']!="" && $row['from_info']!=null)
									{
										$exp_from =explode("|",$row['from_info']);
										$sql_from = mysql_query("select * from $exp_from[0] where id=$exp_from[1]");
										if($sql_from!="")
										{
											$res_from = mysql_fetch_assoc($sql_from);
										}
									}
									if($row['creator_info']!="" && $row['creator_info']!=null)
									{
										$exp_creator =explode("|",$row['creator_info']);
										if($exp_creator[0] == "general_community"){
											$id="community_id";}
										else if($exp_creator[0] == "general_artist"){
											$id="artist_id";}
										else {
											$id="id";}
										
										$sql_creator = mysql_query("select * from $exp_creator[0] where $id=$exp_creator[1]");
										if($sql_creator!="")
										{
											$res_creator = mysql_fetch_assoc($sql_creator);
										}
									}
									$row['from_profileurl']= $res_from['profile_url'];
									$row['creator_profileurl']= $res_creator['profile_url'];
									$resultArray[] = $row;
								}			
								return $resultArray;
							}
						}
				break;
		}
	}
	function get_registration_song($type,$table_name,$id)
	{
		switch($type){
			case "audio":
			case "a":
				$sql="SELECT * FROM ".$table_name." WHERE audio_id='".$id."'";
				//echo $sql;
				$rs=mysql_query($sql) or die(mysql_error());
				if($rs!="")
				{
					if(mysql_num_rows($rs)){
						$resultArray = Array();
						while ($row = mysql_fetch_assoc($rs)) {
							$resultArray[] = $row;
						}		
						//var_dump($resultArray);
						return $resultArray;
					}
				}
				break;
			case "video":
			case "v":
						$sql="SELECT * FROM ".$table_name." WHERE video_id='".$id."'";
						//echo $sql;
						$rs=mysql_query($sql) or die(mysql_error());
						if($rs!="")
						{
							if(mysql_num_rows($rs)){
								$resultArray = Array();
								while ($row = mysql_fetch_assoc($rs)) {
									
									$resultArray[] = $row;
								}			
								return $resultArray;
							}
						}						
				break;
		}
	}
	function newAdd_registration_media($type,$table_name,$id){
		switch($type){
			case "add_reg":
				$sql="SELECT * FROM ".$table_name." WHERE audio_id='".$id."'";
				//echo $sql;
				$rs=mysql_query($sql) or die(mysql_error());
				if($rs!="")
				{
					if(mysql_num_rows($rs)){
						$resultArray = Array();
						while ($row = mysql_fetch_assoc($rs)) {
							$resultArray[] = $row;
						}		
						//var_dump($resultArray);
						return $resultArray;
					}
				}
				break;
			case "addvi_reg":
			$sql="SELECT * FROM ".$table_name." WHERE video_id='".$id."'";
			//echo $sql;
			$rs=mysql_query($sql) or die(mysql_error());
			if($rs!="")
			{
				if(mysql_num_rows($rs)){
					$resultArray = Array();
					while ($row = mysql_fetch_assoc($rs)) {
						
						$resultArray[] = $row;
					}			
					return $resultArray;
				}
			}
			break;
		}
	}
	function update_media($id)
	{
		$query = mysql_query("select * from general_media where id ='".$id."' AND delete_status=0");
		if($query!="")
		{
			if(mysql_num_rows($query)>0)
			{
				$res_media = mysql_fetch_assoc($query);
				$count_tot = $res_media['play_count'] + 1;
				$sql = mysql_query("update general_media set play_count ='".$count_tot."' where id= '".$id."'");
			}
		}
	}
	function update_login_media()
	{
		if(isset($_SESSION['login_email']))
		{
			$get_data = mysql_query("select * from general_user where email='".$_SESSION['login_email']."'");
			if($get_data!="")
			{
				if(mysql_num_rows($get_data)>0)
				{
					$res = mysql_fetch_assoc($get_data);
					$play = $res['play_count'] + 1;
					mysql_query("update general_user set play_count = '".$play."' where email ='".$_SESSION['login_email']."'");
				}
			}
		}
	}
	
	function checkdownloaded($ids,$user_id)
	{
		$sql = mysql_query("SELECT * FROM addtocart WHERE media_id='".$ids."' AND buyer_id='".$user_id."' AND buy_status=1");
		$sql_fan = mysql_query("SELECT * FROM addtocart_all WHERE media_id='".$ids."' AND buyer_id='".$user_id."' AND buy_status=1");
		if(($sql!="" && $sql!=Null) || ($sql_fan!="" && $sql_fan!=Null))
		{
			if(mysql_num_rows($sql)>0 || mysql_num_rows($sql_fan)>0)
			{
				return 1;
			}
			else
			{
				return 0;
			}
		}
		else
		{
			return 0;
		}
	}
}
?>