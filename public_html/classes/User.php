<?php
include_once('commons/db.php');
include_once('PasswordHash.php');

class User
{
  function checkAvailability($username)
  {
    $query="select * from user where user_name='$username'";
	$rs=mysql_query($query) or die(mysql_error());
	$num=mysql_num_rows($rs);
	return($num);  	
  }
  
  function checkEmail_artist($email)
  {
    $query="select * from user LEFT JOIN user_parent on user.id = user_parent.user_id where user.email='$email' AND user_parent.user_type = 'artist'";
	$rs=mysql_query($query) or die(mysql_error());
	$num=mysql_num_rows($rs);
	return($num);  	
  }
  
  function checkEmail_community($email)
  {
    $query="select * from user LEFT JOIN user_parent on user.id = user_parent.user_id where user.email='$email' AND user_parent.user_type = 'community'";
	$rs=mysql_query($query) or die(mysql_error());
	$num=mysql_num_rows($rs);
	return($num);  	
  }   
  
  function checkEmail_team($email)
  {
    $query="select * from user LEFT JOIN user_parent on user.id = user_parent.user_id where user.email='$email' AND user_parent.user_type = 'team'";
	$rs=mysql_query($query) or die(mysql_error());
	$num=mysql_num_rows($rs);
	return($num);  	
  }
  
  function checkEmail_fan($email)
  {
    $query="select * from user LEFT JOIN user_parent on user.id = user_parent.user_id where user.email='$email' AND user_parent.user_type = 'fan'";
	$rs=mysql_query($query) or die(mysql_error());
	$num=mysql_num_rows($rs);
	return($num);  	
  }
  
  function checkVerificationNo($verification_no)
  {
	$sql="select * from user where verification_no='$verification_no' and verified!='1'";
	$rs=mysql_query($sql) or die(mysql_error());
	return mysql_num_rows($rs);
  }
  
  function checkRefNo($ref_no)
  {
	$sql="select * from user where ref_no='$ref_no'";
	$rs=mysql_query($sql) or die(mysql_error());
	return mysql_num_rows($rs);
  }
  
  function updateReference($ref_by)
  {
  	$sql1="SELECT * FROM user WHERE ref_no='$ref_by'";
	$rs=mysql_query($sql1) or die(mysql_error());
	$row=mysql_fetch_assoc($rs);
	$people_ref=$row['people_ref'];
	$people_ref++;
	$sql2="UPDATE user SET people_ref='$people_ref' where ref_no='$ref_by'";
	$rs=mysql_query($sql2) or die(mysql_error());
  }
  
  function verify($username,$password)
  {
	$sql="select password from user where user_name='$username'";
	$rs=mysql_query($sql) or die(mysql_error());
	$row=mysql_fetch_assoc($rs);
	$password2=$row['password'];
  	$t_hasher = new PasswordHash(8, FALSE);
	$check = $t_hasher->CheckPassword($password, $password2);
	if ($check)
	{	
		$row=$this->getUserInfo($username);
		if($row['user_type']=='fan')
		{
			$sql2="select * from user,user_parent where user.user_name='$username' && user.password='$password2' && user.id=user_parent.user_id && user.del='0'";
			$rs2=mysql_query($sql2) or die(mysql_error());
			return mysql_num_rows($rs2);			
		}
		else
		{
			$sql2="select * from user,user_parent where user.user_name='$username' && user.password='$password2' && user.id=user_parent.user_id && user.status='1' && user.del='0'";
			$rs2=mysql_query($sql2) or die(mysql_error());
			return mysql_num_rows($rs2);
		}
	}
	else
	{
		return 0;
	}
  }
  
  function getUserInfo($username)
  {
	$sql="select * from user,user_parent where user.user_name='$username' and user.id=user_parent.user_id";
	$rs=mysql_query($sql) or die(mysql_error());
	return mysql_fetch_assoc($rs);	  	
  }
  
  function abandonUser($user_id)
  {
	$sql="delete from user where user_id='$user_id'";
	$rs=mysql_query($sql) or die(mysql_error()); 
  }
  
  function abandonUserParent($user_id)
  {
	$sql="delete from user_parent where user_id='$user_id'";
	$rs=mysql_query($sql) or die(mysql_error()); 
  }    
  
  function updateNewsletter($verification_no)
  {
  	$query="UPDATE user SET newsletter='0' WHERE verification_no='$verification_no'";
	$rs=mysql_query($query) or die(mysql_error());
	return mysql_affected_rows();
  }
  
  function getUserById($user_id)
  {
	$sql="select * from user,user_parent,user_img where user.id='$user_id' and user.id=user_parent.user_id and user.id=user_img.user_id";
	$rs=mysql_query($sql) or die(mysql_error());
	return mysql_fetch_assoc($rs);	  
  }
  
  function changeLocation($username,$country,$state_or_province,$city)
  {
  	$now = date("F j, Y, g:i a");
	$sql="UPDATE user SET country='$country', state_or_province='$state_or_province', city='$city', update_date='$now' WHERE username='$username'";
	$rs=mysql_query($sql) or die(mysql_error()); 
	
	
  }  
  
   function markAsFeaturedArtist($user_id)
  {
  	$sql0="SELECT user_type FROM user_parent WHERE user_id='$user_id'";
	$rs0=mysql_query($sql0) or die(mysql_error());
	$row0=mysql_fetch_row($rs0);
	$user_type=$row0[0];
  
  	if($user_type=='artist')
	{
  		$sql="UPDATE user_parent SET featured_parent='1' WHERE user_id='$user_id' and del<>'1'";
		$rs=mysql_query($sql) or die(mysql_error());
		//return(mysql_affected_rows());
		//A line above commented & a line below added to overcome 'multiple times adding' check.
		return 1;
	}
	else
		return 0;
  }  
}
?>