<?php
class viewArtistProject
{
	function get_generalUser()
	{
		$sql=mysql_query("select * from general_user where email='".$_SESSION['login_email']."'");
		$res=mysql_fetch_assoc($sql);
		return($res);
	}
	
	function get_ArtistProject($id)
	{
		$res=$this->get_generalUser();
		$sql4=mysql_query("select * from artist_project where artist_id='".$res['artist_id']."' AND id='".$id."' AND del_status=0");
		$res4=mysql_fetch_assoc($sql4);
		return($res4);
	}
	
	function getState($id)
	{
		$state_id = $this->get_ArtistProject($id);
		$sql_state = mysql_query("SELECT * FROM state WHERE state_id='".$state_id['state_id']."'");
		$state_ans = mysql_fetch_assoc($sql_state);
		return ($state_ans);
	}
	
	function getCountry($id)
	{
		$country_id = $this->get_ArtistProject($id);
		$sql_country = mysql_query("SELECT * FROM country WHERE country_id='".$country_id['country_id']."'");
		$state_ans = mysql_fetch_assoc($sql_country);
		return ($state_ans);
	}
	
	function getMinType($id)
	{
		$sql_min_type = mysql_query("SELECT MIN(id) FROM artist_project_profiles WHERE artist_project_id='".$id."'");
		$type_min_ans = mysql_fetch_assoc($sql_min_type);
		
		$sql_type_id = mysql_query("SELECT * FROM artist_project_profiles WHERE id=".$type_min_ans['MIN(id)']."");
		$sql_id_ans = mysql_fetch_assoc($sql_type_id);
		return ($sql_id_ans);
	}
	
	function getType($id)
	{
		$res = $this->getMinType($id);
		$sql_type = mysql_query("SELECT * FROM type WHERE type_id=".$res['type_id']."");
		$type_ans = mysql_fetch_assoc($sql_type);
		return ($type_ans);
	}
	
	function getSubType($id)
	{
		$res = $this->getMinType($id);
		$sql_type = mysql_query("SELECT * FROM subtype WHERE subtype_id=".$res['subtype_id']."");
		$type_ans = mysql_fetch_assoc($sql_type);
		return ($type_ans);
	}
	
	function getMetaType($id)
	{
		$sql16=mysql_query("select DISTINCT(metatype_id) from artist_project_profiles where artist_project_id='".$id."' AND metatype_id!=0 LIMIT 3");
		$new_ans = array();
		$new_ans1 = array();
		
		if(mysql_num_rows($sql16)>0)
		{
			while($row = mysql_fetch_assoc($sql16))
			{
				$new_ans[] = $row;
			}
			
			for($i=0;$i<count($new_ans);$i++)
			{
				$sql17 = mysql_query("SELECT name FROM meta_type WHERE meta_id='".$new_ans[$i]['metatype_id']."'");
				$new_ans1[$i] = mysql_fetch_assoc($sql17);
			}
		}
		return ($new_ans1);
	}
	
	function get_all_project_media($id)
	{
		$sql_all = mysql_query("SELECT * FROM artist_project WHERE id=".$id." AND del_status=0");
		$all_ans = mysql_fetch_assoc($sql_all);
		return ($all_ans);
	}
	function get_Video($video_id)
	{
		$sql_video = mysql_query("SELECT * FROM general_media WHERE id='".$video_id."' AND delete_status=0");
		$video_ans = mysql_fetch_assoc($sql_video);
		return ($video_ans);
	}
	function get_Songs_regis($reg_song_id)
	{
		$sql_song = mysql_query("SELECT * FROM general_artist_audio WHERE audio_id='".$reg_song_id."'");
		$song_ans = mysql_fetch_assoc($sql_song);
		return ($song_ans);
	}
	function get_Songs_media($media_id)
	{
		$sql_song_media = mysql_query("SELECT * FROM general_media WHERE id='".$media_id."' AND media_type=114 AND delete_status=0");
		$song_ans_media = mysql_fetch_assoc($sql_song_media);
		return ($song_ans_media);
	}
	function get_gallery($gal_id)
	{
		$sql_gal = mysql_query("SELECT * FROM general_media WHERE id='".$gal_id."' AND delete_status=0");
		$gal_ans = mysql_fetch_assoc($sql_gal);
		return ($gal_ans);
	}
	function get_media_cover_pic($media_id)
	{
		$sql16=mysql_query("select * from media_images where media_id='".$media_id."'");
		$res16=mysql_fetch_assoc($sql16);
		return($res16);
	}
	
	
	function get_artist_pro_Gallery_at_register($tag_gal)
	{
		$res=$this->get_generalUser();
		$get_gal=mysql_query("select * from general_artist_gallery where gallery_id='".$tag_gal."' AND profile_id='".$res['artist_id']."'");
		return($get_gal);
	}
	function get_count_artist_pro_Gallery_at_register($tag_gal)
	{
		$res=$this->get_generalUser();
		$get_gal=mysql_query("select count(*) from general_artist_gallery where gallery_id='".$tag_gal."' AND profile_id='".$res['artist_id']."'");
		$res_gal=mysql_fetch_assoc($get_gal);
		return($res_gal);
	}
	function get_artist_pro_Gallery_title_at_register($tag_gal)
	{
		$get_gal_title=mysql_query("select * from general_artist_gallery_list where gallery_id='".$tag_gal."'");
		$res_gal_title=mysql_fetch_assoc($get_gal_title);
		return($res_gal_title);
	}
	
	/******Functions for recorded events and upcoming events*******/
	
	function recordedEvents($date,$id)
	{
		$sql_recorded_artist=mysql_query("SELECT * FROM artist_event WHERE id='".$id."' AND del_status=0");
		return($sql_recorded_artist);
	}
	
	function upcomingEvents($date,$id)
	{
		$sql_upcoming_artist=mysql_query("SELECT * FROM artist_event WHERE id='".$id."' AND del_status=0");
		return($sql_upcoming_artist);
	}
	
	
	/******Functions for recorded events and upcoming events Ends Here*******/
	
	/******Functions for Projects*******/
	function get_Projects($id)
	{
		$get_project=mysql_query("SELECT * FROM artist_project WHERE id='".$id."' AND del_status=0");
		return($get_project);
	}
	/******Functions for Projects Ends Here*******/
	
	function get_All_Artist($art_id)
	{
		$sql_all_artist = mysql_query("SELECT * FROM general_artist WHERE artist_id='".$art_id."' AND del_status=0");
		return ($sql_all_artist);
	}
	function get_songtypes($media_id)
	{
		$sql = mysql_query("select * from general_media where id = '".$media_id."' AND delete_status=0");
		$res = mysql_fetch_assoc($sql);
		return($res);
	}
	
	function chk_for_artist_tab($email)
	{
		$exp_email = explode(",",$email);
		for($count_mail = 0;$count_mail<count($exp_email);$count_mail++)
		{
			$chk_art  = mysql_query("SELECT * FROM general_user WHERE email ='".$exp_email[$count_mail]."' AND delete_status=0");
			if(mysql_num_rows($chk_art)>0)
			{
				$res_chk_art = mysql_fetch_assoc($chk_art);
				if($res_chk_art['artist_id']!="" && $res_chk_art['artist_id']!= 0)
				{
					return("artist");
				}
			}
		}
	}
	function get_all_artist_info($email)
	{
		$query = mysql_query("select * from general_user where email='".$email."' AND delete_status=0");
		if(mysql_num_rows($query)>0)
		{
			$res_query = mysql_fetch_assoc($query);
			if($res_query['artist_id']!="" && $res_query['artist_id']!=0)
			{
				$get_art = mysql_query("select * from general_artist where artist_id ='".$res_query['artist_id']."' AND del_status=0");
				if(mysql_num_rows($get_art)>0)
				{
					$res_art = mysql_fetch_assoc($get_art);
					return($res_art);
				}
			}
		}
	}
}
?>