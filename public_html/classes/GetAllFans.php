<?php
	include_once('commons/db.php');
	class GetAllFans
	{
		function getartist_fans($artist_id)
		{
			$ans_get_fans_a = array();
			$ans_get_fans_pro = array();
			
			$sql_get_fans_a = mysql_query("SELECT * FROM fan_club_membership WHERE related_id='".$artist_id."'");
			if(mysql_num_rows($sql_get_fans_a)>0)
			{
				while($row_get_fans_a = mysql_fetch_assoc($sql_get_fans_a))
				{
					//$row_get_fans_a['type'] = 'artist';
					$ans_get_fans_a[] = $row_get_fans_a;
				}
			}
			
			$sql_get_art_pro = mysql_query("SELECT * FROM fan_club_membership WHERE related_type='artist_project'");
			if(mysql_num_rows($sql_get_art_pro)>0)
			{
				while($row_get_art_pro = mysql_fetch_assoc($sql_get_art_pro))
				{
					$exp_type_id = explode('_',$row_get_art_pro['related_id']);
					if($exp_type_id[0]==$artist_id)
					{
						//$row_get_art_pro['type'] = 'project';
						$ans_get_fans_pro[] = $row_get_art_pro;
					}
				}
			}
			
			$main_art_gans_all = array_merge($ans_get_fans_a,$ans_get_fans_pro);
			$m_s_i_new = array();
			$m_s_i = array();
			$new_array_art = array();
			if(count($main_art_gans_all)>0)
			{
				for($j_m_f=0;$j_m_f<count($main_art_gans_all);$j_m_f++)
				{
					$m_s_i[] = $main_art_gans_all[$j_m_f]['email'];
					//$m_s_i_new[] = $main_art_gans_all[$j_m_f]['related_type'];
				}
				
				//$m_new_c = 0;
				//var_dump($m_s_i_new);
				foreach($m_s_i as $key => $value)
				{
					if(isset($new_array_art[$value]))
					{
						$new_array_art[$value] += 1;
					}
					else
					{
						$new_array_art[$value] = 1;
						//$new_array_art['type'.$m_new_c] = $m_s_i_new[$m_new_c];
					}
					//$m_new_c = $m_new_c + 1;
				}
			}
			//var_dump($new_array_art);
			return $new_array_art;
		}
		
		function get_types_art($art_id,$fan_email)
		{
			$ans_all_fans_pro = array();
			$sql_pro_all = mysql_query("SELECT * FROM fan_club_membership WHERE related_type='artist_project'");
			if(mysql_num_rows($sql_pro_all)>0)
			{
				while($row_get_art_pro = mysql_fetch_assoc($sql_pro_all))
				{
					$exp_type_id = explode('_',$row_get_art_pro['related_id']);
					if($exp_type_id[0]==$art_id)
					{
						$ans_all_fans_pro[] = $row_get_art_pro;
					}
				}
			}
			
			$sql_type = mysql_query("SELECT * FROM fan_club_membership WHERE related_id='".$art_id."' AND email='".$fan_email."'");
			
			if(mysql_num_rows($sql_type)>0 && count($ans_all_fans_pro)>0)
			{
				$num = mysql_num_rows($sql_type);
				
				$type = 'Artist';
				return $type;
			}
			elseif(mysql_num_rows($sql_type)==0 && count($ans_all_fans_pro)>0)
			{
				$type = 'Fan';
				return $type;
			}
			elseif(mysql_num_rows($sql_type)>0 && count($ans_all_fans_pro)==0)
			{
				$type = 'Artist';
				return $type;
			}
			else
			{
				return 0;
			}
		}
		
		function getcommunity_fans($community_id)
		{
			$ans_get_fans_c = array();
			$ans_get_fans_pro_c = array();
			
			$sql_get_fans_c = mysql_query("SELECT * FROM fan_club_membership WHERE related_id='".$community_id."'");
			if(mysql_num_rows($sql_get_fans_c)>0)
			{
				while($row_get_fans_a = mysql_fetch_assoc($sql_get_fans_c))
				{
					//$row_get_fans_a['type'] = 'artist';
					$ans_get_fans_c[] = $row_get_fans_a;
				}
			}
			
			$sql_get_art_pro = mysql_query("SELECT * FROM fan_club_membership WHERE related_type='community_project'");
			if(mysql_num_rows($sql_get_art_pro)>0)
			{
				while($row_get_art_pro = mysql_fetch_assoc($sql_get_art_pro))
				{
					$exp_type_id = explode('_',$row_get_art_pro['related_id']);
					if($exp_type_id[0]==$community_id)
					{
						//$row_get_art_pro['type'] = 'project';
						$ans_get_fans_pro_c[] = $row_get_art_pro;
					}
				}
			}
			
			$main_art_gans_all = array_merge($ans_get_fans_c,$ans_get_fans_pro_c);
			$m_s_i_new = array();
			$m_s_i = array();
			$new_array_com = array();
			if(count($main_art_gans_all)>0)
			{
				for($j_c_m_f=0;$j_c_m_f<count($main_art_gans_all);$j_c_m_f++)
				{
					$m_s_i[] = $main_art_gans_all[$j_c_m_f]['email'];
					//$m_s_i_new[] = $main_art_gans_all[$j_c_m_f]['related_type'];
				}
				
				//$m_new_c = 0;
				//var_dump($m_s_i_new);
				foreach($m_s_i as $key => $value)
				{
					if(isset($new_array_com[$value]))
					{
						$new_array_com[$value] += 1;
					}
					else
					{
						$new_array_com[$value] = 1;
						//$new_array_com['type'.$m_new_c] = $m_s_i_new[$m_new_c];
					}
					//$m_new_c = $m_new_c + 1;
				}
			}
			//var_dump($new_array_com);
			return $new_array_com;
		}
		
		function get_types_com($com_id,$fan_email)
		{
			$ans_all_fans_pro = array();
			$sql_pro_all = mysql_query("SELECT * FROM fan_club_membership WHERE related_type='community_project'");
			if(mysql_num_rows($sql_pro_all)>0)
			{
				while($row_get_art_pro = mysql_fetch_assoc($sql_pro_all))
				{
					$exp_type_id = explode('_',$row_get_art_pro['related_id']);
					if($exp_type_id[0]==$com_id)
					{
						$ans_all_fans_pro[] = $row_get_art_pro;
					}
				}
			}
			
			$sql_type = mysql_query("SELECT * FROM fan_club_membership WHERE related_id='".$com_id."' AND email='".$fan_email."'");
			
			if(mysql_num_rows($sql_type)>0 && count($ans_all_fans_pro)>0)
			{
				$num = mysql_num_rows($sql_type);
				
				$type = 'Community';
				return $type;
			}
			elseif(mysql_num_rows($sql_type)==0 && count($ans_all_fans_pro)>0)
			{
				$type = 'Fan';
				return $type;
			}
			elseif(mysql_num_rows($sql_type)>0 && count($ans_all_fans_pro)==0)
			{
				$type = 'Community';
				return $type;
			}
			else
			{
				return 0;
			}
		}
		
		function user_gen_fan($gen_id)
		{
			$sql_gen = mysql_query("SELECT ga. * , s.state_name FROM general_user ga LEFT JOIN state s ON ga.state_id = s.state_id WHERE ga.general_user_id ='".$gen_id."'");
			if(mysql_num_rows($sql_gen)>0)
			{
				$ans_gen = mysql_fetch_assoc($sql_gen);
				return $ans_gen;
			}
		}
	}
?>