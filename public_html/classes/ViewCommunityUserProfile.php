<?php
class ViewCommunityUserProfile
{
	function get_user_info()
	{
		$sql=mysql_query("select * from general_user where email='".$_SESSION['login_email']."'");
		$res=mysql_fetch_assoc($sql);
		return($res);
	}
	function get_user_community_profile()
	{
		$res=$this->get_user_info();
		$sql4=mysql_query("select * from general_community where community_id='".$res['community_id']."' AND del_status=0");
		$res4=mysql_fetch_assoc($sql4);
		return($res4);
	}
	function get_user_country()
	{
		$res=$this->get_user_community_profile();
		$sql2=mysql_query("select * from country where country_id='".$res['country_id']."'");
		$res2=mysql_fetch_assoc($sql2);
		return($res2);
	}
	function get_user_state()
	{
		$res=$this->get_user_community_profile();
		$sql3=mysql_query("select * from state where state_id='".$res['state_id']."'");
		$res3=mysql_fetch_assoc($sql3);
		return($res3);
	}
	function get_community_user()
	{
		$res=$this->get_user_info();
		$sql5=mysql_query("select MIN(general_community_profiles_id) from general_community_profiles where community_id='".$res['community_id']."'");
		$res5=mysql_fetch_assoc($sql5);
		
		$sql6=mysql_query("select * from general_community_profiles where general_community_profiles_id='".$res5['MIN(general_community_profiles_id)']."'");
		$res6=mysql_fetch_assoc($sql6);
		return($res6);
	}
	function get_community_user_type()
	{
		$res1=$this->get_community_user();
		$sql7=mysql_query("select * from type where type_id='".$res1['type_id']."'");
		$res7=mysql_fetch_assoc($sql7);
		return($res7);
	}
	function get_community_user_subtype()
	{
		$res1=$this->get_community_user();
		$sql8=mysql_query("select * from subtype where subtype_id='".$res1['subtype_id']."'");
		$res8=mysql_fetch_assoc($sql8);
		return($res8);
	}
	function get_artist_user_metatype()
	{
		$res=$this->get_user_info();
		$sql16=mysql_query("select DISTINCT(metatype_id) from general_community_profiles where community_id='".$res['community_id']."' AND metatype_id!=0 LIMIT 3");
		$new_ans = array();
		$new_ans1 = array();
		if(mysql_num_rows($sql16)>0)
		{
			while($row = mysql_fetch_assoc($sql16))
			{
				$new_ans[] = $row; 
			}
			
			for($i=0;$i<count($new_ans);$i++)
			{
				$sql17 = mysql_query("SELECT name FROM meta_type WHERE meta_id='".$new_ans[$i]['metatype_id']."'");
				$new_ans1[$i] = mysql_fetch_assoc($sql17);
			}
		}
		return($new_ans1);
	}
	function get_community_projects($id)
	{
		$sql9=mysql_query("select * from community_project where id='".$id."' AND del_status=0");
		return($sql9);
	}
	
	/********Function for Displaying video and songs*****/
	
	function get_community_Info()
	{
		$res=$this->get_user_info();
		$sql10=mysql_query("select * from general_community where community_id='".$res['community_id']."' AND del_status=0");
		$res10=mysql_fetch_assoc($sql10);
		return($res10);
	}
	function get_community_Video($tag_video)
	{
		$sql11=mysql_query("select * from media_video where media_id='".$tag_video."'");
		$res11=mysql_fetch_assoc($sql11);
		return($res11);
	}
	function get_community_Video_title($tag_video)
	{
		$sql12=mysql_query("select * from general_media where id='".$tag_video."' AND delete_status=0");
		$res12=mysql_fetch_assoc($sql12);
		return($res12);
	}
	function get_community_Song($tag_song)
	{
		$sql13=mysql_query("select * from media_songs where media_id='".$tag_song."'");
		$res13=mysql_fetch_assoc($sql13);
		return($res13);
	}
	function get_community_Song_title($tag_song)
	{
		$sql14=mysql_query("select * from general_media where id='".$tag_song."' AND media_type=114 AND delete_status=0" );
		$res14=mysql_fetch_assoc($sql14);
		return($res14);
	}
	function get_community_Song_regis($tag_song)
	{
		$sql15=mysql_query("select * from general_community_audio where audio_id='".$tag_song."'");
		$res15=mysql_fetch_assoc($sql15);
		return($res15);
	}
	
	function recordedEvents($date,$id)
	{
		$sql_recorded_community=mysql_query("SELECT * FROM community_event WHERE id='".$id."'  AND del_status=0");
		return($sql_recorded_community);
	}
	function count_recordedEvents()
	{
		$res=$this->get_user_info();
		$sql_count_recorded_community=mysql_query("SELECT taggedrecordedevents FROM general_community WHERE community_id='".$res['community_id']."' AND del_status=0");
		$res_sql_count_recorded_community=mysql_fetch_assoc($sql_count_recorded_community);
		return($res_sql_count_recorded_community);
	}
	function upcomingEvents($date,$id)
	{
		$sql_upcoming_community=mysql_query("SELECT * FROM community_event WHERE id='".$id."' AND del_status=0");
		return($sql_upcoming_community);
	}
	function count_upcomingEvents()
	{
		$res=$this->get_user_info();
		$sql_count_upcoming_community=mysql_query("SELECT taggedupcomingevents FROM general_community WHERE community_id='".$res['community_id']."' AND del_status=0");
		$res_sql_count_upcoming_community=mysql_fetch_assoc($sql_count_upcoming_community);
		return($res_sql_count_upcoming_community);
	}
	
	
	function get_community_gallery($tag_gal)
	{
		$sql16=mysql_query("select * from general_media where id='".$tag_gal."' AND media_type=113 AND delete_status=0");
		$res16=mysql_fetch_assoc($sql16);
		return($res16);
	}
	function get_media_cover_pic($media_id)
	{
		$sql16=mysql_query("select * from media_images where media_id='".$media_id."'");
		$res16=mysql_fetch_assoc($sql16);
		return($res16);
	}
	
	function get_community_Gallery_at_register($tag_gal)
	{
		$res=$this->get_user_info();
		$get_gal=mysql_query("select * from general_community_gallery where gallery_id='".$tag_gal."' AND profile_id='".$res['community_id']."'");
		return($get_gal);
	}
	function get_count_community_Gallery_at_register($tag_gal)
	{
		$res=$this->get_user_info();
		$get_gal=mysql_query("select count(*) from general_community_gallery where gallery_id='".$tag_gal."' AND profile_id='".$res['community_id']."'");
		$res_gal=mysql_fetch_assoc($get_gal);
		return($res_gal);
	}
	function get_community_Gallery_title_at_register($tag_gal)
	{
		$get_gal_title=mysql_query("select * from general_community_gallery_list where gallery_id='".$tag_gal."'");
		$res_gal_title=mysql_fetch_assoc($get_gal_title);
		return($res_gal_title);
	}
	
	function get_community_Gallery_at_register_for_song($tag_gal)
	{
		//$res=$this->get_user_info();
		$get_gal=mysql_query("select * from general_community_gallery where gallery_id='".$tag_gal."'");
		return($get_gal);
	}
	
	function get_media_Gallery($media_id)
	{
		//$res=$this->get_user_info();
		$get_gal=mysql_query("select * from media_images where media_id='".$media_id."' ");
		return($get_gal);
	}
	
	function get_from_list_pic($tbl_name,$id)
	{
		$get_list = mysql_query("select * from ".$tbl_name." where id = '".$id."'");
		$res_list = mysql_fetch_assoc($get_list);
		return($res_list);
	}
	function get_creator_list_pic($tbl_name,$id,$id_name)
	{
		$get_list = mysql_query("select * from ".$tbl_name." where ".$id_name." = '".$id."'");
		$res_list = mysql_fetch_assoc($get_list);
		return($res_list);
	}
	
	function get_count_Gallery_at_register($id,$tag_gal)
	{
		$res=$this->get_user_info($id);
		$get_gal=mysql_query("select count(*) from general_artist_gallery where gallery_id='".$tag_gal."' AND profile_id='".$res['artist_id']."'");
		$res_gal=mysql_fetch_assoc($get_gal);
		return($res_gal);
	}
	function get_featured_media_gallery_at_register($id,$type)
	{
		$get_feature_media_images=mysql_query("select * from general_".$type."_gallery where gallery_id='".$id."'");
		return($get_feature_media_images);
	}
	function get_featured_media_gallery($id)
	{
		$get_feature_media_images=mysql_query("select * from media_images where media_id='".$id."'");
		return($get_feature_media_images);
	}
	function get_artist_friends($art_id)
	{
		$get_art = mysql_query("select * from general_artist where artist_id = '".$art_id."' AND del_status=0");
		return($get_art);
	}
	function get_community_friends($com_id)
	{
		$get_com = mysql_query("select * from general_community where community_id = '".$com_id."' AND del_status=0");
		return($get_com);
	}
	
}
?>