<?php

class ProfileeditMedia
{
	function sel_general()
	{
		$sql=mysql_query("select * from general_user where email='".$_SESSION['login_email']."'");
		$res=mysql_fetch_assoc($sql);
		return($res);
	}
	function sel_all_media_ordered($order,$type)
	{
		$id=$this->sel_general();
		$sql2=mysql_query("SELECT *
							FROM general_media GM
							LEFT JOIN type ty ON GM.media_type = ty.type_id
							WHERE GM.general_user_id ='".$id['general_user_id']."' AND delete_status=0 ORDER BY ".$type." ".$order."");
			//echo $sql2;
		return($sql2);
	}
	function sel_all_media()
	{
		$id=$this->sel_general();
		//$sql2=mysql_query("select * from general_media where general_user_id='".$id['general_user_id']."'");
		//return($sql2);
		$sql2=mysql_query("SELECT general_media . * , type.name, type.type_id FROM general_media,type
			WHERE general_media.media_type = type.type_id AND general_user_id='".$id['general_user_id']."' AND delete_status=0");
			//echo $sql2;
		return($sql2);
	}
	function sel_all_media_by_category($type)
	{
		$id=$this->sel_general();
		$sql4="SELECT general_media. * , type.name, type.type_id FROM general_media,type WHERE general_media.media_type = type.type_id
		AND `general_user_id` ='".$id['general_user_id']."'
		AND `media_type` ='".$type."' AND delete_status=0";
		//echo $sql4;
		$res4=mysql_query($sql4);
		return($res4);
	}
	function sel_all_media_by_category_order($type,$ordertype,$order)
	{
		$id=$this->sel_general();
		$sql4="SELECT *
							FROM general_media GM
							LEFT JOIN type ty ON GM.media_type = ty.type_id
							WHERE GM.general_user_id ='".$id['general_user_id']."' AND `media_type` ='".$type."' AND delete_status=0 ORDER BY ".$ordertype." ".$order."";
		//echo $sql4;
		$res4=mysql_query($sql4);
		return($res4);
	}
	
	function sel_all_media_by_category2($type)
	{
		$id=$this->sel_general();
		$sql4="SELECT general_media. * , type.name, type.type_id FROM general_media,type WHERE general_media.media_type = type.type_id
		AND `general_user_id` ='".$id['general_user_id']."'
		AND `media_type` ='".$type."' AND delete_status=0";
		//echo $sql4;
		$res4=mysql_query($sql4);
		return($res4);
	}

	function delete_media($id)
	{
		$sql3=mysql_query("update general_media set delete_status=1 where id='".$id."'");
	}
	function sel_all_tagged_media($id)
	{
		$get_tagged=mysql_query("select * from general_media where id='".$id."' AND delete_status=0 AND media_status=0");
		//echo "select * from general_media where id='".$id."'";
		return($get_tagged);
	}
	function sel_all_tagged_media_ordered($id)
	{
		$get_tagged=mysql_query("SELECT * FROM general_media GM LEFT JOIN type ty ON GM.media_type = ty.type_id WHERE GM.id ='".$id."' AND GM.delete_status=0 AND GM.media_status=0");
		//echo "select * from general_media where id='".$id."'";
		return($get_tagged);
	}
	function sel_type_of_tagged_media($id)
	{
		$res_type = "";
		$get_type=mysql_query("select * from type where type_id=$id");
		if($get_type!="" && $get_type!=NULL)
		{
			$res_type=mysql_fetch_assoc($get_type);
		}
		return($res_type);
	}
	
	function get_media_types()
	{
		$sql5=mysql_query("select * from type where profile_id=49");
		return($sql5);
	}
	function get_registration_media()
	{
		$artist_id=$this->sel_general();
		
		$res_media_gal = "";
		$res_media_audio = "";
		$res_media_video = "";
		
		$reg_media_gal=mysql_query("select * from general_artist_gallery where profile_id='".$artist_id['artist_id']."'");
		if($reg_media_gal!="" && $reg_media_gal!=NULL)
		{
			$res_media_gal=mysql_fetch_assoc($reg_media_gal);
		}
		
		$reg_media_audio=mysql_query("select * from general_artist_audio where profile_id='".$artist_id['artist_id']."'");
		if($reg_media_audio!="" && $reg_media_audio!=NULL)
		{
			$res_media_audio=mysql_fetch_assoc($reg_media_audio);
		}
		
		$reg_media_video=mysql_query("select * from general_artist_video where profile_id='".$artist_id['artist_id']."'");
		if($reg_media_video!="" && $reg_media_video!=NULL)
		{
			$res_media_video=mysql_fetch_assoc($reg_media_video);
		}
		
		if($res_media_gal!=null)
		{
			return($res_media_gal);
		}
		else if($res_media_audio!=null)
		{
			return($res_media_audio);
		}
		else if($res_media_video!=null)
		{
			return($res_media_video);
		}
		
	}
	
	function get_gallery_info()
	{
		$reg_media_gal=$this->get_registration_media();
		$res_gal_name = "";
		$Get_gal_name=mysql_query("select * from general_artist_gallery_list where gallery_id='".$reg_media_gal['gallery_id']."'");
		if($Get_gal_name!="" && $Get_gal_name!=NULL)
		{
			$res_gal_name=mysql_fetch_assoc($Get_gal_name);
		}
		return($res_gal_name);
	}
	
	
	function get_registration_media_community()
	{
		$community_id=$this->sel_general();
		
		$res_media_gal = "";
		$res_media_audio = "";
		$res_media_video = "";
		
		$reg_media_gal=mysql_query("select * from general_community_gallery where profile_id='".$community_id['community_id']."'");
		if($reg_media_gal!="" && $reg_media_gal!=NULL)
		{
			$res_media_gal=mysql_fetch_assoc($reg_media_gal);
		}
		
		$reg_media_audio=mysql_query("select * from general_community_audio where profile_id='".$community_id['community_id']."'");
		if($reg_media_audio!="" && $reg_media_audio!=NULL)
		{
			$res_media_audio=mysql_fetch_assoc($reg_media_audio);
		}
		
		$reg_media_video=mysql_query("select * from general_community_video where profile_id='".$community_id['community_id']."'");
		if($reg_media_video!="" && $reg_media_video!=NULL)
		{
			$res_media_video=mysql_fetch_assoc($reg_media_video);
		}
		
		if($res_media_gal!=null)
		{
			return($res_media_gal);
		}
		else if($res_media_audio!=null)
		{
			return($res_media_audio);
		}
		else if($res_media_video!=null)
		{
			return($res_media_video);
		}
		
	}
	
	
	function get_gallery_info_community()
	{
		$res_gal_name= "";
		$reg_media_gal=$this->get_registration_media_community();
		$Get_gal_name=mysql_query("select * from general_community_gallery_list where gallery_id='".$reg_media_gal['gallery_id']."'");
		if($Get_gal_name!="" && $Get_gal_name!=NULL)
		{
			$res_gal_name=mysql_fetch_assoc($Get_gal_name);
		}
		return($res_gal_name);
	}
	
	function get_gallery_editable($id)
	{
		$sql_gal = "";
		$sql_gal=mysql_query("select * from media_images where media_id='".$id."'");
		if($sql_gal!="" && $sql_gal!=NULL)
		{
			$res_gal=mysql_fetch_assoc($sql_gal);
		}
		return($res_gal);
	}
	function get_media_track($id)
	{
		$res_trac = "";
		$sql_trac=mysql_query("select * from media_songs where media_id='".$id."'");
		if($sql_trac!="" && $sql_trac!=NULL)
		{
			$res_trac=mysql_fetch_assoc($sql_trac);
		}
		return($res_trac);
	}
	/*Code For Displaying the Fanclub Media*/
	function chk_fan()
	{
		$id=$this->sel_general();
		$chk = mysql_query("select * from add_to_cart_fan_pro where buyer_id='".$id['general_user_id']."' AND buy_status=1 AND del_status=0");
		return($chk);
	}
	function get_fan_media($buyer_id,$seller_id,$table_name,$type_id)
	{
		$get_data = mysql_query("select * from addtocart_all where buyer_id='".$buyer_id."' AND seller_id='".$seller_id."' AND table_name='".$table_name."' AND type_id='".$type_id."' AND del_status=0");
		return($get_data);
	}
	function get_sale_media()
	{
		$id=$this->sel_general();
		//echo "select * from addtocart where buyer_id='".$id['general_user_id']."' AND del_status=0 AND buy_status=1";
		$get_data = mysql_query("select * from addtocart where buyer_id='".$id['general_user_id']."' AND del_status=0 AND buy_status=1");
		return($get_data);
	}
	
	function get_all_fan_mediadetail($id)
	{
		$gen_id=$this->sel_general();
		$newres ="";
		$res = "";
		$query = mysql_query("select * from general_media where id = '".$id."'");
		if($query!="" && $query!=NULL)
		{
			$res = mysql_fetch_assoc($query);
			if($gen_id['general_user_id']!=$res['general_user_id']){
				return($res);
			}else{
				return($newres);
			}
		}
	}
	
	function get_media_data($id)
	{
		$res_data = "";
		$get_data = mysql_query("SELECT general_media. * , type.name, type.type_id FROM general_media,type WHERE general_media.media_type = type.type_id AND general_media.id ='".$id."' AND general_media.delete_status=0");
		if($get_data!="" && $get_data!=NULL)
		{
			if(mysql_num_rows($get_data)>0)
			{
				$res_data = mysql_fetch_assoc($get_data);
				
			}
		}
		return($res_data);
	}
	/*Code For Displaying the Fanclub Media Ends Here*/
	
	function get_all_user_s3()
	{
		$row = $this -> sel_general();
		$get_data = mysql_query("select * from add_user_s3 where general_user_id='".$row['general_user_id']."'");
		return($get_data);
	}
	function get_loggedin_info($email){
		$res = "";
		$sql = mysql_query("select * from general_user where email='".$email."'");
		if($sql!="" && $sql!=NULL)
		{
			$res = mysql_fetch_assoc($sql);
		}
		return($res);
	}
	function get_tagged_user_info($type,$id)
	{
		$res = "";
		$query = mysql_query("select * from general_user where ".$type."_id ='".$id."'");
		if($query!="" && $query!=NULL)
		{
			$res = mysql_fetch_assoc($query);
		}
		return($res);
	}
	function get_artist_project_tag($email)
	{
		//echo "SELECT * FROM artist_project WHERE tagged_user_email LIKE '%".$email."%'";
		$query = mysql_query("SELECT * FROM artist_project WHERE del_status=0 AND active=0 AND tagged_user_email LIKE '%".$email."%'");
		return($query);
	}
	function get_community_project_tag($email)
	{
		//echo "SELECT * FROM artist_project WHERE tagged_user_email LIKE '%".$email."%'";
		$query = mysql_query("SELECT * FROM community_project WHERE tagged_user_email LIKE '%".$email."%'");
		return($query);
	}
	function get_tag_media_info($media_id)
	{
		$sql= mysql_query("SELECT general_media. * , type.name, type.type_id FROM general_media,type WHERE general_media.media_type = type.type_id AND general_media.id ='".$media_id."' AND general_media.delete_status=0");
		if($sql!="" && $sql!=NULL)
		{
			if(mysql_num_rows($sql)>0)
			{
				$row = mysql_fetch_assoc($sql);
				return($row);
			}
		}
	}
	
	
	/*Old Function
	function get_project_create()
	{
		$id = $this->sel_general();
		$var_type_1 = 'general_artist';
		$var_type_2 = 'general_community';
		$var_type_3 = 'artist_project';
		$var_type_4 = 'community_project';
		
		$artm = array();
		$arta = array();
		$comm = array();
		$coma = array();
		$art_p_ao = array();
		$com_p_ao = array();
		$art_p_co = array();
		$com_p_co = array();
		
		$pal_1 = array();
		$pal_2 = array();
		
		if($id['artist_id']!=0 && !empty($id['artist_id']))
		{
			$chk_type = $var_type_1.'|'.$id['artist_id'];
			
			$sql_get_allp = mysql_query("SELECT * FROM artist_project WHERE artist_id='".$id['artist_id']."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_get_allp)>0)
			{
				while($rowal = mysql_fetch_assoc($sql_get_allp))
				{
					$pal_1[] = $rowal;
				}
			}
			
			if(!empty($pal_1) && count($pal_1))
			{
				for($pl=0;$pl<count($pal_1);$pl++)
				{
					$chk_typep_1 = 'artist_project|'.$pal_1[$pl]['id'];
					$sql = mysql_query("SELECT * FROM artist_project WHERE creators_info='".$chk_typep_1."' AND del_status=0 AND active=0");
					if(mysql_num_rows($sql)>0)
					{
						while($row_po = mysql_fetch_assoc($sql))
						{
							if($row_po['artist_id']!=$id['artist_id'])
							{
								$art_p_ao[] = $row_po;
							}
						}
					}
					
					$sql = mysql_query("SELECT * FROM community_project WHERE creators_info='".$chk_typep_1."' AND del_status=0 AND active=0");
					if(mysql_num_rows($sql)>0)
					{
						while($row_poo = mysql_fetch_assoc($sql))
						{
							if($row_poo['community_id']!=$id['community_id'])
							{
								$com_p_ao[] = $row_poo;
							}
						}
					}
				}
			}
			
			$sql_pro_arta = mysql_query("SELECT * FROM artist_project WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_pro_arta)>0)
			{
				while($rowaa = mysql_fetch_assoc($sql_pro_arta))
				{
					if($rowaa['artist_id']!=$id['artist_id'])
					{
						$arta[] = $rowaa;
					}
				}
			}
			
			$sql_pro_coma = mysql_query("SELECT * FROM community_project WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_pro_coma)>0)
			{
				while($rowam = mysql_fetch_assoc($sql_pro_coma))
				{
					if($rowam['community_id']!=$id['community_id'])
					{
						$coma[] = $rowam;
					}
				}
			}
		}
		
		if($id['community_id']!=0 && !empty($id['community_id']))
		{
			$chk_type = $var_type_2.'|'.$id['community_id'];
			$sql_pro_artm = mysql_query("SELECT * FROM artist_project WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_pro_artm)>0)
			{
				while($rowcm = mysql_fetch_assoc($sql_pro_artm))
				{
					if($rowcm['artist_id']!=$id['artist_id'])
					{
						$artm[] = $rowcm;
					}
				}
			}
			
			$sql_pro_comm = mysql_query("SELECT * FROM community_project WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_pro_comm)>0)
			{
				while($rowca = mysql_fetch_assoc($sql_pro_comm))
				{
					if($rowca['community_id']!=$id['community_id'])
					{
						$comm[] = $rowca;
					}
				}
			}
			
			$sql_get_allp = mysql_query("SELECT * FROM community_project WHERE community_id='".$id['community_id']."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_get_allp)>0)
			{
				while($rowal = mysql_fetch_assoc($sql_get_allp))
				{
					$pal_2[] = $rowal;
				}
			}
			
			if(!empty($pal_2) && count($pal_2))
			{
				for($pl2=0;$pl2<count($pal_2);$pl2++)
				{
					$chk_typep_2 = 'community_project|'.$pal_2[$pl2]['id'];
					$sql = mysql_query("SELECT * FROM artist_project WHERE creators_info='".$chk_typep_2."' AND del_status=0 AND active=0");
					if(mysql_num_rows($sql)>0)
					{
						while($row_po = mysql_fetch_assoc($sql))
						{
							if($row_po['artist_id']!=$id['artist_id'])
							{
								$art_p_co[] = $row_po;
							}
						}
					}
					
					$sql = mysql_query("SELECT * FROM community_project WHERE creators_info='".$chk_typep_2."' AND del_status=0 AND active=0");
					if(mysql_num_rows($sql)>0)
					{
						while($row_poo = mysql_fetch_assoc($sql))
						{
							if($row_poo['community_id']!=$id['community_id'])
							{
								$com_p_co[] = $row_poo;
							}
						}
					}
				}
			}
		}
		
		$totals = array();
		$totals = array_merge($artm,$arta,$comm,$coma,$art_p_ao,$com_p_ao,$art_p_co,$com_p_co);
		return $totals;
	}*/
	
	function get_project_create()
	{
		$id = $this->sel_general();
		$var_type_1 = 'general_artist';
		$var_type_2 = 'general_community';
		$var_type_3 = 'artist_project';
		$var_type_4 = 'community_project';
		
		$artm = array();
		$arta = array();
		$comm = array();
		$coma = array();
		
		$artme = array();
		$artae = array();
		$comme = array();
		$comae = array();
		
		$art_p_ao = array();
		$com_p_ao = array();
		$art_p_co = array();
		$com_p_co = array();
		
		$art_e_ao = array();
		$com_e_ao = array();
		$art_e_co = array();
		$com_e_co = array();
		
		$pal_1 = array();
		$pal_2 = array();
		
		if($id['artist_id']!=0 && !empty($id['artist_id']))
		{
			$chk_type = $var_type_1.'|'.$id['artist_id'];
			
			$sql_get_allp = mysql_query("SELECT * FROM artist_project WHERE artist_id='".$id['artist_id']."' AND del_status=0 AND active=0");
			if($sql_get_allp!="" && $sql_get_allp!=NULL)
			{
				if(mysql_num_rows($sql_get_allp)>0)
				{
					while($rowal = mysql_fetch_assoc($sql_get_allp))
					{
						$pal_1[] = $rowal;
					}
				}
			}
			
			if(!empty($pal_1) && count($pal_1))
			{
				for($pl=0;$pl<count($pal_1);$pl++)
				{
					$chk_typep_1 = 'artist_project|'.$pal_1[$pl]['id'];
					
					$sql = mysql_query("SELECT * FROM artist_project WHERE creators_info='".$chk_typep_1."' AND del_status=0 AND active=0");
					if($sql!="" && $sql!=NULL)
					{
						if(mysql_num_rows($sql)>0)
						{
							while($row_po = mysql_fetch_assoc($sql))
							{
								if($row_po['artist_id']!=$id['artist_id'])
								{
									$art_p_ao[] = $row_po;
								}
							}
						}
					}
					
					$sql = mysql_query("SELECT * FROM community_project WHERE creators_info='".$chk_typep_1."' AND del_status=0 AND active=0");
					if($sql!="" && $sql!=NULL)
					{
						if(mysql_num_rows($sql)>0)
						{
							while($row_poo = mysql_fetch_assoc($sql))
							{
								if($row_poo['community_id']!=$id['community_id'])
								{
									$com_p_ao[] = $row_poo;
								}
							}
						}
					}
				}
			}
			
			if(!empty($pal_1) && count($pal_1))
			{
				for($pl=0;$pl<count($pal_1);$pl++)
				{
					$chk_typep_1 = 'artist_project|'.$pal_1[$pl]['id'];
					
					$sql = mysql_query("SELECT * FROM artist_event WHERE creators_info='".$chk_typep_1."' AND del_status=0 AND active=0");
					if($sql!="" && $sql!=NULL)
					{
						if(mysql_num_rows($sql)>0)
						{
							while($row_po = mysql_fetch_assoc($sql))
							{
								if($row_po['artist_id']!=$id['artist_id'])
								{
									$art_e_ao[] = $row_po;
								}
							}
						}
					}
					
					$sql = mysql_query("SELECT * FROM community_event WHERE creators_info='".$chk_typep_1."' AND del_status=0 AND active=0");
					if($sql!="" && $sql!=NULL)
					{
						if(mysql_num_rows($sql)>0)
						{
							while($row_poo = mysql_fetch_assoc($sql))
							{
								if($row_poo['community_id']!=$id['community_id'])
								{
									$com_e_ao[] = $row_poo;
								}
							}
						}
					}
				}
			}

			$sql_pro_arta = mysql_query("SELECT * FROM artist_project WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0");
			if($sql_pro_arta!="" && $sql_pro_arta!=NULL)
			{
				if(mysql_num_rows($sql_pro_arta)>0)
				{
					while($rowaa = mysql_fetch_assoc($sql_pro_arta))
					{
						if($rowaa['artist_id']!=$id['artist_id'])
						{
							$arta[] = $rowaa;
						}
					}
				}
			}
			
			$sql_pro_arta = mysql_query("SELECT * FROM artist_event WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0");
			if($sql_pro_arta!="" && $sql_pro_arta!=NULL)
			{
				if(mysql_num_rows($sql_pro_arta)>0)
				{
					while($rowaa = mysql_fetch_assoc($sql_pro_arta))
					{
						if($rowaa['artist_id']!=$id['artist_id'])
						{
							$artae[] = $rowaa;
						}
					}
				}
			}
			
			$sql_pro_coma = mysql_query("SELECT * FROM community_project WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0");
			if($sql_pro_coma!="" && $sql_pro_coma!=NULL)
			{
				if(mysql_num_rows($sql_pro_coma)>0)
				{
					while($rowam = mysql_fetch_assoc($sql_pro_coma))
					{
						if($rowam['community_id']!=$id['community_id'])
						{
							$coma[] = $rowam;
						}
					}
				}
			}
			
			$sql_pro_coma = mysql_query("SELECT * FROM community_event WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0");
			if($sql_pro_coma!="" && $sql_pro_coma!=NULL)
			{
				if(mysql_num_rows($sql_pro_coma)>0)
				{
					while($rowam = mysql_fetch_assoc($sql_pro_coma))
					{
						if($rowam['community_id']!=$id['community_id'])
						{
							$comae[] = $rowam;
						}
					}
				}
			}
		}
		
		if($id['community_id']!=0 && !empty($id['community_id']))
		{
			$chk_type = $var_type_2.'|'.$id['community_id'];
			$sql_pro_artm = mysql_query("SELECT * FROM artist_project WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0");
			if($sql_pro_artm!="" && $sql_pro_artm!=NULL)
			{
				if(mysql_num_rows($sql_pro_artm)>0)
				{
					while($rowcm = mysql_fetch_assoc($sql_pro_artm))
					{
						if($rowcm['artist_id']!=$id['artist_id'])
						{
							$artm[] = $rowcm;
						}
					}
				}
			}
			
			$chk_type = $var_type_2.'|'.$id['community_id'];
			$sql_pro_artm = mysql_query("SELECT * FROM artist_event WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0");
			if($sql_pro_artm!="" && $sql_pro_artm!=NULL)
			{
				if(mysql_num_rows($sql_pro_artm)>0)
				{
					while($rowcm = mysql_fetch_assoc($sql_pro_artm))
					{
						if($rowcm['artist_id']!=$id['artist_id'])
						{
							$artme[] = $rowcm;
						}
					}
				}
			}
			
			$sql_pro_comm = mysql_query("SELECT * FROM community_project WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0");
			if($sql_pro_comm!="" && $sql_pro_comm!=NULL)
			{
				if(mysql_num_rows($sql_pro_comm)>0)
				{
					while($rowca = mysql_fetch_assoc($sql_pro_comm))
					{
						if($rowca['community_id']!=$id['community_id'])
						{
							$comm[] = $rowca;
						}
					}
				}
			}
			
			$sql_pro_comm = mysql_query("SELECT * FROM community_event WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0");
			if($sql_pro_comm!="" && $sql_pro_comm!=NULL)
			{
				if(mysql_num_rows($sql_pro_comm)>0)
				{
					while($rowca = mysql_fetch_assoc($sql_pro_comm))
					{
						if($rowca['community_id']!=$id['community_id'])
						{
							$comme[] = $rowca;
						}
					}
				}
			}
			
			$sql_get_allp = mysql_query("SELECT * FROM community_project WHERE community_id='".$id['community_id']."' AND del_status=0 AND active=0");
			if($sql_get_allp!="" && $sql_get_allp!=NULL)
			{
				if(mysql_num_rows($sql_get_allp)>0)
				{
					while($rowal = mysql_fetch_assoc($sql_get_allp))
					{
						$pal_2[] = $rowal;
					}
				}
			}
			
			if(!empty($pal_2) && count($pal_2))
			{
				for($pl2=0;$pl2<count($pal_2);$pl2++)
				{
					$chk_typep_2 = 'community_project|'.$pal_2[$pl2]['id'];
					$sql = mysql_query("SELECT * FROM artist_project WHERE creators_info='".$chk_typep_2."' AND del_status=0 AND active=0");
					if($sql!="" && $sql!=NULL)
					{
						if(mysql_num_rows($sql)>0)
						{
							while($row_po = mysql_fetch_assoc($sql))
							{
								if($row_po['artist_id']!=$id['artist_id'])
								{
									$art_p_co[] = $row_po;
								}
							}
						}
					}
					
					$sql = mysql_query("SELECT * FROM community_project WHERE creators_info='".$chk_typep_2."' AND del_status=0 AND active=0");
					if($sql!="" && $sql!=NULL)
					{
						if(mysql_num_rows($sql)>0)
						{
							while($row_poo = mysql_fetch_assoc($sql))
							{
								if($row_poo['community_id']!=$id['community_id'])
								{
									$com_p_co[] = $row_poo;
								}
							}
						}
					}
				}
			}
			
			if(!empty($pal_2) && count($pal_2))
			{
				for($pl2=0;$pl2<count($pal_2);$pl2++)
				{
					$chk_typep_2 = 'community_project|'.$pal_2[$pl2]['id'];
					$sql = mysql_query("SELECT * FROM artist_event WHERE creators_info='".$chk_typep_2."' AND del_status=0 AND active=0");
					if($sql!="" && $sql!=NULL)
					{
						if(mysql_num_rows($sql)>0)
						{
							while($row_po = mysql_fetch_assoc($sql))
							{
								if($row_po['artist_id']!=$id['artist_id'])
								{
									$art_e_co[] = $row_po;
								}
							}
						}
					}
					
					$sql = mysql_query("SELECT * FROM community_event WHERE creators_info='".$chk_typep_2."' AND del_status=0 AND active=0");
					if($sql!="" && $sql!=NULL)
					{
						if(mysql_num_rows($sql)>0)
						{
							while($row_poo = mysql_fetch_assoc($sql))
							{
								if($row_poo['community_id']!=$id['community_id'])
								{
									$com_e_co[] = $row_poo;
								}
							}
						}
					}
				}
			}
		}
		
		$totals = array();
		$totals = array_merge($artm,$arta,$comm,$coma,$art_p_ao,$com_p_ao,$art_p_co,$com_p_co,$artme,$artae,$comme,$comae,$art_e_ao,$com_e_ao,$art_e_co,$com_e_co);
		return $totals;
	}
	
	function get_all_gallery_images($id)
	{
		$sql= mysql_query("select * from media_images where media_id='".$id."'");
		$new_array = array();
		if($sql!="" && $sql!=NULL)
		{
			if(mysql_num_rows($sql)>0)
			{
				while($row = mysql_fetch_assoc($sql)){
					$new_array[] = $row;
				}
			}
		}
		return($new_array);
	}
	function get_gallery_reg_image($id)
	{
		$sql= mysql_query("select * from general_artist_gallery where gallery_id = '".$id."'");
		$new_array = array();
		if($sql!="" && $sql!=NULL)
		{
			if(mysql_num_rows($sql)>0){
				while($row=mysql_fetch_assoc($sql)){
					$new_array[] = $row;
				}
			}
		}
		return($new_array);
	}
	function get_gallery_reg_image_com($id)
	{
		$sql= mysql_query("select * from general_community_gallery where gallery_id = '".$id."'");
		$new_array = array();
		if($sql!="" && $sql!=NULL)
		{
			if(mysql_num_rows($sql)>0){
				while($row=mysql_fetch_assoc($sql)){
					$new_array[] = $row;
				}
			}
		}
		return($new_array);
	}
	
	function get_media_where_creator_or_from()
	{
		$get_art_media1 = array();
		$get_art_media2 = array();
		$get_art_media3 = array();
		$get_art_media4 = array();
		$get_art_media5 = array();
		$get_art_media6 = array();
		
		$get_gen_det = $this->sel_general();
		if($get_gen_det['artist_id']>0){
			$creator_info = "general_artist|".$get_gen_det['artist_id'];
			$query = mysql_query("select * from general_media where creator_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
			if($query!="" && $query!=NULL)
			{
				if(mysql_num_rows($query)>0){
					while($row_art = mysql_fetch_assoc($query)){
						if($get_gen_det['general_user_id']!= $row_art['general_user_id']){
						$get_art_media1[] = $row_art;
						}
					}
				}
			}
			
			$get_creator = mysql_query("select * from artist_project where artist_id='".$get_gen_det['artist_id']."' AND del_status=0 AND active=0");
			if($get_creator!="" && $get_creator!=NULL)
			{
				if(mysql_num_rows($get_creator)>0)
				{
					while($row_art_pro = mysql_fetch_assoc($get_creator))
					{
						$creator_info = "artist_project|".$row_art_pro["id"];
						$query1 = mysql_query("select * from general_media where creator_info = '".$creator_info."' OR from_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
						if(mysql_num_rows($query1)>0){
							while($row_art_media = mysql_fetch_assoc($query1)){
								if($get_gen_det['general_user_id']!= $row_art_media['general_user_id']){
								$get_art_media2[] = $row_art_media;
								}
							}
						}
					}
				}
			}
			
			$get_creator = mysql_query("select * from artist_event where artist_id='".$get_gen_det['artist_id']."' AND del_status=0 AND active=0");
			if($get_creator!="" && $get_creator!=NULL)
			{
				if(mysql_num_rows($get_creator)>0)
				{
					while($row_art_eve = mysql_fetch_assoc($get_creator))
					{
						$creator_info = "artist_event|".$row_art_eve["id"];
						$query1 = mysql_query("select * from general_media where creator_info = '".$creator_info."' OR from_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
						if(mysql_num_rows($query1)>0){
							while($row_art_media = mysql_fetch_assoc($query1)){
								if($get_gen_det['general_user_id']!= $row_art_media['general_user_id']){
								$get_art_media3[] = $row_art_media;
								}
							}
						}
					}
				}
			}
		}
		if($get_gen_det['community_id']>0){
			$creator_info = "general_community|".$get_gen_det['community_id'];
			$query = mysql_query("select * from general_media where creator_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
			if($query!="" && $query!=NULL)
			{
				if(mysql_num_rows($query)>0){
					while($row_com = mysql_fetch_assoc($query)){
						//echo "azher4";
						if($get_gen_det['general_user_id']!= $row_com['general_user_id']){
						$get_art_media4[] = $row_com;
						}
					}
				}
			}
			
			$get_creator = mysql_query("select * from community_project where community_id='".$get_gen_det['community_id']."' AND del_status=0 AND active=0");
			if($get_creator!="" && $get_creator!=NULL)
			{
				if(mysql_num_rows($get_creator)>0)
				{
					while($row_com_pro = mysql_fetch_assoc($get_creator))
					{
						$creator_info = "community_project|".$row_com_pro["id"];
						$query1 = mysql_query("select * from general_media where creator_info = '".$creator_info."' OR from_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
						if(mysql_num_rows($query1)>0){
							while($row_com_media = mysql_fetch_assoc($query1)){
								//echo "azher5";
								if($get_gen_det['general_user_id']!= $row_com_media['general_user_id']){
								$get_art_media5[] = $row_com_media;
								}
							}
						}
					}
				}
			}
			
			$get_creator = mysql_query("select * from community_event where community_id='".$get_gen_det['community_id']."' AND del_status=0 AND active=0");
			if($get_creator!="" && $get_creator!=NULL)
			{
				if(mysql_num_rows($get_creator)>0)
				{
					while($row_com_eve = mysql_fetch_assoc($get_creator))
					{
						$creator_info = "community_event|".$row_com_eve["id"];
						$query1 = mysql_query("select * from general_media where creator_info = '".$creator_info."' OR from_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
						if(mysql_num_rows($query1)>0){
							while($row_com_media = mysql_fetch_assoc($query1)){
								if($get_gen_det['general_user_id']!= $row_com_media['general_user_id']){
								$get_art_media6[] = $row_com_media;
								}
							}
						}
					}
				}
			}
		}
		$all_media = array_merge($get_art_media1,$get_art_media2,$get_art_media3,$get_art_media4,$get_art_media5,$get_art_media6);
		return($all_media);
	}
	
	function get_creator_heis()
	{
		$id = $this->sel_general();
        $var_type_1 = 'general_artist';
        $var_type_2 = 'general_community';
        $var_type_3 = 'artist_project';
        $var_type_4 = 'community_project';
		$res_media1 = array();
		$res_media2 = array();
		$res_media3 = array();
		$res_media4 = array();
		$res_media5 = array();
		$res_media6 = array();
		$res_media7 = array();
		$res_media8 = array();
		$res_media9 = array();
		$res_media10 = array();
		$res_media11 = array();
		$res_media12 = array();
		$res_media13 = array();
		$res_media14 = array();
		$res_media15 = array();
		$res_media16 = array();
        if($id['artist_id']!=0 && !empty($id['artist_id']))
        {
            $chk_type = $var_type_1.'|'.$id['artist_id'];
			$sql_get_pro = mysql_query("select * from artist_project where creators_info='".$chk_type."' AND del_status=0 AND active=0");
			if($sql_get_pro!="" && $sql_get_pro!=NULL)
			{
				if(mysql_num_rows($sql_get_pro)>0)
				{
					while($row = mysql_fetch_assoc($sql_get_pro))
					{
						$creator_info = "artist_project|".$row['id'];
						$get_media = mysql_query("select * from general_media where creator_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
						if($get_media!="" && $get_media!=NULL)
						{
							if(mysql_num_rows($get_media)>0)
							{
								while($res = mysql_fetch_assoc($get_media))
								{
									if($res['general_user_id']!=$id['general_user_id'])
									{
										$res_media1[] = $res;
									}
								}
							}
						}
					}
				}
			}
			$sql_get_pro1 = mysql_query("select * from community_project where creators_info='".$chk_type."' AND del_status=0 AND active=0");
			if($sql_get_pro1!="" && $sql_get_pro1!=NULL)
			{
				if(mysql_num_rows($sql_get_pro1)>0)
				{
					while($row1 = mysql_fetch_assoc($sql_get_pro1))
					{
						$creator_info = "community_project|".$row1['id'];
						$get_media = mysql_query("select * from general_media where creator_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
						if($get_media!="" && $get_media!=NULL)
						{
							if(mysql_num_rows($get_media)>0)
							{
								while($res = mysql_fetch_assoc($get_media))
								{
									if($res['general_user_id']!=$id['general_user_id'])
									{
										$res_media2[] = $res;}
								}
							}
						}
					}
				}
			}
			/*$sql_get_pro2 = mysql_query("select * from artist_event where creators_info='".$chk_type."'");
			if(mysql_num_rows($sql_get_pro2)>0)
			{
				while($row = mysql_fetch_assoc($sql_get_pro2))
				{
					$creator_info = "artist_event|".$row['id'];
					$get_media = mysql_query("select * from general_media where creator_info = '".$creator_info."'");
					if(mysql_num_rows($get_media)>0)
					{
						while($res = mysql_fetch_assoc($get_media))
						{
							$res_media3[] = $res;
						}
					}
				}
			}
			$sql_get_pro3 = mysql_query("select * from community_event where creators_info='".$chk_type."'");
			if(mysql_num_rows($sql_get_pro3)>0)
			{
				while($row1 = mysql_fetch_assoc($sql_get_pro3))
				{
					$creator_info = "community_event|".$row1['id'];
					$get_media = mysql_query("select * from general_media where creator_info = '".$creator_info."'");
					if(mysql_num_rows($get_media)>0)
					{
						while($res = mysql_fetch_assoc($get_media))
						{
							$res_media4[] = $res;
						}
					}
				}
			}*/
			
			$find_project = mysql_query("select * from artist_project where artist_id='".$id['artist_id']."' AND del_status=0 AND active=0");
			if($find_project!="" && $find_project!=NULL)
			{
				if(mysql_num_rows($find_project)>0)
				{
					while($get_all_pro = mysql_fetch_assoc($find_project))
					{
						$new_creator_info = "artist_project|".$get_all_pro['id'];
						$sql_get_pro = mysql_query("select * from artist_project where creators_info='".$new_creator_info."' AND del_status=0 AND active=0");
						if($sql_get_pro!="" && $sql_get_pro!=NULL)
						{
							if(mysql_num_rows($sql_get_pro)>0)
							{
								while($row = mysql_fetch_assoc($sql_get_pro))
								{
									$creator_info = "artist_project|".$row['id'];
									$get_media = mysql_query("select * from general_media where creator_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
									if($get_media!="" && $get_media!=NULL)
									{
										if(mysql_num_rows($get_media)>0)
										{
											while($res = mysql_fetch_assoc($get_media))
											{
												if($res['general_user_id']!=$id['general_user_id'])
												{
													$res_media5[] = $res;}
											}
										}
									}
								}
							}
						}
						//var_dump($res_media5);
						$sql_get_pro1 = mysql_query("select * from community_project where creators_info='".$new_creator_info."' AND del_status=0 AND active=0");
						if($sql_get_pro1!="" && $sql_get_pro1!=NULL)
						{
							if(mysql_num_rows($sql_get_pro1)>0)
							{
								while($row1 = mysql_fetch_assoc($sql_get_pro1))
								{
									$creator_info = "community_project|".$row1['id'];
									$get_media = mysql_query("select * from general_media where creator_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
									if($get_media!="" && $get_media!=NULL)
									{
										if(mysql_num_rows($get_media)>0)
										{
											while($res = mysql_fetch_assoc($get_media))
											{
												if($res['general_user_id']!=$id['general_user_id'])
												{
													$res_media6[] = $res;}
											}
										}
									}
								}
							}
						}
						$sql_get_pro2 = mysql_query("select * from artist_event where creators_info='".$new_creator_info."' AND del_status=0 AND active=0");
						if($sql_get_pro2!="" && $sql_get_pro2!=NULL)
						{
							if(mysql_num_rows($sql_get_pro2)>0)
							{
								while($row = mysql_fetch_assoc($sql_get_pro2))
								{
									$creator_info = "artist_event|".$row['id'];
									$get_media = mysql_query("select * from general_media where creator_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
									if($get_media!="" && $get_media!=NULL)
									{
										if(mysql_num_rows($get_media)>0)
										{
											while($res = mysql_fetch_assoc($get_media))
											{
												if($res['general_user_id']!=$id['general_user_id'])
												{
													$res_media7[] = $res;}
											}
										}
									}
								}
							}
						}
						$sql_get_pro3 = mysql_query("select * from community_event where creators_info='".$new_creator_info."' AND del_status=0 AND active=0");
						if($sql_get_pro3!="" && $sql_get_pro3!=NULL)
						{
							if(mysql_num_rows($sql_get_pro3)>0)
							{
								while($row1 = mysql_fetch_assoc($sql_get_pro3))
								{
									$creator_info = "community_event|".$row1['id'];
									$get_media = mysql_query("select * from general_media where creator_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
									if($get_media!="" && $get_media!=NULL)
									{
										if(mysql_num_rows($get_media)>0)
										{
											while($res = mysql_fetch_assoc($get_media))
											{
												if($res['general_user_id']!=$id['general_user_id'])
												{
													$res_media8[] = $res;}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
		if($id['community_id']!=0 && !empty($id['community_id']))
        {
            $chk_type = $var_type_2.'|'.$id['community_id'];
			$sql_get_pro = mysql_query("select * from artist_project where creators_info='".$chk_type."' AND del_status=0 AND active=0");
			if($sql_get_pro!="" && $sql_get_pro!=NULL)
			{
				if(mysql_num_rows($sql_get_pro)>0)
				{
					while($row = mysql_fetch_assoc($sql_get_pro))
					{
						$creator_info = "artist_project|".$row['id'];
						$get_media = mysql_query("select * from general_media where creator_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
						if($get_media!="" && $get_media!=NULL)
						{
							if(mysql_num_rows($get_media)>0)
							{
								while($res = mysql_fetch_assoc($get_media))
								{
									if($res['general_user_id']!=$id['general_user_id'])
									{
										$res_media9[] = $res;}
								}
							}
						}
					}
				}
			}
			$sql_get_pro1 = mysql_query("select * from community_project where creators_info='".$chk_type."' AND del_status=0 AND active=0");
			if($sql_get_pro1!="" && $sql_get_pro1!=NULL)
			{
				if(mysql_num_rows($sql_get_pro1)>0)
				{
					while($row1 = mysql_fetch_assoc($sql_get_pro1))
					{
						$creator_info = "community_project|".$row1['id'];
						$get_media = mysql_query("select * from general_media where creator_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
						if($get_media!="" && $get_media!=NULL)
						{
							if(mysql_num_rows($get_media)>0)
							{
								while($res = mysql_fetch_assoc($get_media))
								{
									if($res['general_user_id']!=$id['general_user_id'])
									{
										$res_media10[] = $res;}
								}
							}
						}
					}
				}
			}
			/*$sql_get_pro2 = mysql_query("select * from artist_event where creators_info='".$chk_type."'");
			if(mysql_num_rows($sql_get_pro2)>0)
			{
				while($row = mysql_fetch_assoc($sql_get_pro2))
				{
					$creator_info = "artist_event|".$row['id'];
					$get_media = mysql_query("select * from general_media where creator_info = '".$creator_info."'");
					if(mysql_num_rows($get_media)>0)
					{
						while($res = mysql_fetch_assoc($get_media))
						{
							$res_media11[] = $res;
						}
					}
				}
			}
			$sql_get_pro3 = mysql_query("select * from community_event where creators_info='".$chk_type."'");
			if(mysql_num_rows($sql_get_pro3)>0)
			{
				while($row1 = mysql_fetch_assoc($sql_get_pro3))
				{
					$creator_info = "community_event|".$row1['id'];
					$get_media = mysql_query("select * from general_media where creator_info = '".$creator_info."'");
					if(mysql_num_rows($get_media)>0)
					{
						while($res = mysql_fetch_assoc($get_media))
						{
							$res_media12[] = $res;
						}
					}
				}
			}*/
			$find_project = mysql_query("select * from community_project where community_id='".$id['community_id']."' AND del_status=0 AND active=0");
			if($find_project!="" && $find_project!=NULL)
			{
				if(mysql_num_rows($find_project)>0)
				{
					while($get_all_pro = mysql_fetch_assoc($find_project))
					{
						$new_creator_info = "community_project|".$get_all_pro['id'];
						$sql_get_pro = mysql_query("select * from artist_project where creators_info='".$new_creator_info."' AND del_status=0 AND active=0");
						if($sql_get_pro!="" && $sql_get_pro!=NULL)
						{
							if(mysql_num_rows($sql_get_pro)>0)
							{
								while($row = mysql_fetch_assoc($sql_get_pro))
								{
									$creator_info = "artist_project|".$row['id'];
									$get_media = mysql_query("select * from general_media where creator_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
									if($get_media!="" && $get_media!=NULL)
									{
										if(mysql_num_rows($get_media)>0)
										{
											while($res = mysql_fetch_assoc($get_media))
											{
												if($res['general_user_id']!=$id['general_user_id'])
												{
													$res_media13[] = $res;}
											}
										}
									}
								}
							}
						}
						$sql_get_pro1 = mysql_query("select * from community_project where creators_info='".$new_creator_info."' AND del_status=0 AND active=0");
						if($sql_get_pro1!="" && $sql_get_pro1!=NULL)
						{
							if(mysql_num_rows($sql_get_pro1)>0)
							{
								while($row1 = mysql_fetch_assoc($sql_get_pro1))
								{
									$creator_info = "community_project|".$row1['id'];
									$get_media = mysql_query("select * from general_media where creator_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
									if($get_media!="" && $get_media!=NULL)
									{
										if(mysql_num_rows($get_media)>0)
										{
											while($res = mysql_fetch_assoc($get_media))
											{
												if($res['general_user_id']!=$id['general_user_id'])
												{
													$res_media14[] = $res;}
											}
										}
									}
								}
							}
						}
						/*$sql_get_pro2 = mysql_query("select * from artist_event where creators_info='".$new_creator_info."'");
						if(mysql_num_rows($sql_get_pro2)>0)
						{
							while($row = mysql_fetch_assoc($sql_get_pro2))
							{
								$creator_info = "artist_event|".$row['id'];
								$get_media = mysql_query("select * from general_media where creator_info = '".$creator_info."'");
								if(mysql_num_rows($get_media)>0)
								{
									while($res = mysql_fetch_assoc($get_media))
									{
										$res_media15[] = $res;
									}
								}
							}
						}
						$sql_get_pro3 = mysql_query("select * from community_event where creators_info='".$new_creator_info."'");
						if(mysql_num_rows($sql_get_pro3)>0)
						{
							while($row1 = mysql_fetch_assoc($sql_get_pro3))
							{
								$creator_info = "community_event|".$row1['id'];
								$get_media = mysql_query("select * from general_media where creator_info = '".$creator_info."'");
								if(mysql_num_rows($get_media)>0)
								{
									while($res = mysql_fetch_assoc($get_media))
									{
										$res_media16[] = $res;
									}
								}
							}
						}*/
					}
				}
			}
		}
		   
		$get_all_media = array_merge($res_media1,$res_media2,$res_media3,$res_media4,$res_media5,$res_media6,$res_media7,$res_media8,$res_media9,$res_media10,$res_media11,$res_media12,$res_media13,$res_media14,$res_media15,$res_media16);
		//var_dump($get_all_media);
		return($get_all_media);
	}
	function get_from_heis()
	{
		$id = $this->sel_general();
        $var_type_1 = 'general_artist';
        $var_type_2 = 'general_community';
        $var_type_3 = 'artist_project';
        $var_type_4 = 'community_project';
		$res_media1 = array();
		$res_media2 = array();
		$res_media3 = array();
		$res_media4 = array();
		$res_media5 = array();
		$res_media6 = array();
		$res_media7 = array();
		$res_media8 = array();
		$res_media9 = array();
		$res_media10 = array();
		$res_media11 = array();
		$res_media12 = array();
		$res_media13 = array();
		$res_media14 = array();
		$res_media15 = array();
		$res_media16 = array();
        if($id['artist_id']!=0 && !empty($id['artist_id']))
        {
			$chk_type = $var_type_1.'|'.$id['artist_id'];
			$sql_get_pro = mysql_query("select * from artist_project where creators_info='".$chk_type."' AND del_status=0 AND active=0");
			if($sql_get_pro!="" && $sql_get_pro!=NULL)
			{
				if(mysql_num_rows($sql_get_pro)>0)
				{
					while($row = mysql_fetch_assoc($sql_get_pro))
					{
						$creator_info = "artist_project|".$row['id'];
						$get_media = mysql_query("select * from general_media where from_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
						if($get_media!="" && $get_media!=NULL)
						{
							if(mysql_num_rows($get_media)>0)
							{
								while($res = mysql_fetch_assoc($get_media))
								{
									if($id['general_user_id'] != $res['general_user_id'])
									{
									$res_media1[] = $res;}
								}
							}
						}
					}
				}
			}
			$sql_get_pro1 = mysql_query("select * from community_project where creators_info='".$chk_type."' AND del_status=0 AND active=0");
			if($sql_get_pro1!="" && $sql_get_pro1!=NULL)
			{
				if(mysql_num_rows($sql_get_pro1)>0)
				{
					while($row1 = mysql_fetch_assoc($sql_get_pro1))
					{
						$creator_info = "community_project|".$row1['id'];
						$get_media = mysql_query("select * from general_media where from_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
						if($get_media!="" && $get_media!=NULL)
						{
							if(mysql_num_rows($get_media)>0)
							{
								while($res = mysql_fetch_assoc($get_media))
								{
									if($id['general_user_id'] != $res['general_user_id'])
									{
										$res_media2[] = $res;}
								}
							}
						}
					}
				}
			}
			$sql_get_pro1 = mysql_query("select * from community_event where creators_info='".$chk_type."' AND del_status=0 AND active=0");
			if($sql_get_pro1!="" && $sql_get_pro1!=NULL)
			{
				if(mysql_num_rows($sql_get_pro1)>0)
				{
					while($row1 = mysql_fetch_assoc($sql_get_pro1))
					{
						$creator_info = "community_event|".$row1['id'];
						$get_media = mysql_query("select * from general_media where from_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
						if($get_media!="" && $get_media!=NULL)
						{
							if(mysql_num_rows($get_media)>0)
							{
								while($res = mysql_fetch_assoc($get_media))
								{
									if($id['general_user_id'] != $res['general_user_id'])
									{
										$res_media3[] = $res;}
								}
							}
						}
					}
				}
			}
			$sql_get_pro1 = mysql_query("select * from artist_event where creators_info='".$chk_type."' AND del_status=0 AND active=0");
			if($sql_get_pro1!="" && $sql_get_pro1!=NULL)
			{
				if(mysql_num_rows($sql_get_pro1)>0)
				{
					while($row1 = mysql_fetch_assoc($sql_get_pro1))
					{
						$creator_info = "artist_event|".$row1['id'];
						$get_media = mysql_query("select * from general_media where from_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
						if($get_media!="" && $get_media!=NULL)
						{
							if(mysql_num_rows($get_media)>0)
							{
								while($res = mysql_fetch_assoc($get_media))
								{
									if($id['general_user_id'] != $res['general_user_id'])
									{
									$res_media4[] = $res;
									}
								}
							}
						}
					}
				}
			}
			
			$find_project = mysql_query("select * from artist_project where artist_id='".$id['artist_id']."' AND del_status=0 AND active=0");
			if($find_project!="" && $find_project!=NULL)
			{
				if(mysql_num_rows($find_project)>0)
				{
					while($get_all_pro = mysql_fetch_assoc($find_project))
					{
						$new_creator_info = "artist_project|".$get_all_pro['id'];
						//echo "select * from artist_project where creators_info='".$new_creator_info."'";
						$sql_get_pro = mysql_query("select * from artist_project where creators_info='".$new_creator_info."' AND del_status=0 AND active=0");
						if($sql_get_pro!="" && $sql_get_pro!=NULL)
						{
							if(mysql_num_rows($sql_get_pro)>0)
							{
								while($row = mysql_fetch_assoc($sql_get_pro))
								{
									$creator_info = "artist_project|".$row['id'];
									$get_media = mysql_query("select * from general_media where from_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
									if($get_media!="" && $get_media!=NULL)
									{
										if(mysql_num_rows($get_media)>0)
										{
											while($res = mysql_fetch_assoc($get_media))
											{
												if($id['general_user_id'] != $res['general_user_id'])
												{
													$res_media5[] = $res;	}
											}
										}
									}
								}
							}
						}
						$sql_get_pro1 = mysql_query("select * from community_project where creators_info='".$new_creator_info."' AND del_status=0 AND active=0");
						if($sql_get_pro1!="" && $sql_get_pro1!=NULL)
						{
							if(mysql_num_rows($sql_get_pro1)>0)
							{
								while($row1 = mysql_fetch_assoc($sql_get_pro1))
								{
									$creator_info = "community_project|".$row1['id'];
									$get_media = mysql_query("select * from general_media where from_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
									if($get_media!="" && $get_media!=NULL)
									{
										if(mysql_num_rows($get_media)>0)
										{
											while($res = mysql_fetch_assoc($get_media))
											{
												if($id['general_user_id'] != $res['general_user_id'])
												{
													$res_media6[] = $res;}
											}
										}
									}
								}
							}
						}
						$sql_get_pro2 = mysql_query("select * from artist_event where creators_info='".$new_creator_info."' AND del_status=0 AND active=0");
						if($sql_get_pro2!="" && $sql_get_pro2!=NULL)
						{
							if(mysql_num_rows($sql_get_pro2)>0)
							{
								while($row = mysql_fetch_assoc($sql_get_pro2))
								{
									$creator_info = "artist_event|".$row['id'];
									$get_media = mysql_query("select * from general_media where from_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
									if($get_media!="" && $get_media!=NULL)
									{
										if(mysql_num_rows($get_media)>0)
										{
											while($res = mysql_fetch_assoc($get_media))
											{
												if($id['general_user_id'] != $res['general_user_id'])
												{
													$res_media7[] = $res;}
											}
										}
									}
								}
							}
						}
						$sql_get_pro3 = mysql_query("select * from community_event where creators_info='".$new_creator_info."' AND del_status=0 AND active=0");
						if($sql_get_pro3!="" && $sql_get_pro3!=NULL)
						{
							if(mysql_num_rows($sql_get_pro3)>0)
							{
								while($row1 = mysql_fetch_assoc($sql_get_pro3))
								{
									$creator_info = "community_event|".$row1['id'];
									$get_media = mysql_query("select * from general_media where from_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
									if($get_media!="" && $get_media!=NULL)
									{
										if(mysql_num_rows($get_media)>0)
										{
											while($res = mysql_fetch_assoc($get_media))
											{
												if($id['general_user_id'] != $res['general_user_id'])
												{
													$res_media8[] = $res;}
											}
										}
									}
								}
							}
						}
					}
				}
			}
			$find_project = mysql_query("select * from artist_event where artist_id='".$id['artist_id']."' AND del_status=0 AND active=0");
			if($find_project!="" && $find_project!=NULL)
			{
				if(mysql_num_rows($find_project)>0)
				{
					while($get_all_pro = mysql_fetch_assoc($find_project))
					{
						$new_creator_info = "artist_event|".$get_all_pro['id'];
						//echo "select * from artist_project where creators_info='".$new_creator_info."'";
						$sql_get_pro = mysql_query("select * from artist_project where creators_info='".$new_creator_info."' AND del_status=0 AND active=0");
						if($sql_get_pro!="" && $sql_get_pro!=NULL)
						{
							if(mysql_num_rows($sql_get_pro)>0)
							{
								while($row = mysql_fetch_assoc($sql_get_pro))
								{
									$creator_info = "artist_project|".$row['id'];
									$get_media = mysql_query("select * from general_media where from_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
									if($get_media!="" && $get_media!=NULL)
									{
										if(mysql_num_rows($get_media)>0)
										{
											while($res = mysql_fetch_assoc($get_media))
											{
												if($id['general_user_id'] != $res['general_user_id'])
												{
													$res_media5[] = $res;	}
											}
										}
									}
								}
							}
						}
						$sql_get_pro1 = mysql_query("select * from community_project where creators_info='".$new_creator_info."' AND del_status=0 AND active=0");
						if($sql_get_pro1!="" && $sql_get_pro1!=NULL)
						{
							if(mysql_num_rows($sql_get_pro1)>0)
							{
								while($row1 = mysql_fetch_assoc($sql_get_pro1))
								{
									$creator_info = "community_project|".$row1['id'];
									$get_media = mysql_query("select * from general_media where from_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
									if($get_media!="" && $get_media!=NULL)
									{
										if(mysql_num_rows($get_media)>0)
										{
											while($res = mysql_fetch_assoc($get_media))
											{
												if($id['general_user_id'] != $res['general_user_id'])
												{
													$res_media6[] = $res;}
											}
										}
									}
								}
							}
						}
						$sql_get_pro2 = mysql_query("select * from artist_event where creators_info='".$new_creator_info."' AND del_status=0 AND active=0");
						if($sql_get_pro2!="" && $sql_get_pro2!=NULL)
						{
							if(mysql_num_rows($sql_get_pro2)>0)
							{
								while($row = mysql_fetch_assoc($sql_get_pro2))
								{
									$creator_info = "artist_event|".$row['id'];
									$get_media = mysql_query("select * from general_media where from_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
									if($get_media!="" && $get_media!=NULL)
									{
										if(mysql_num_rows($get_media)>0)
										{
											while($res = mysql_fetch_assoc($get_media))
											{
												if($id['general_user_id'] != $res['general_user_id'])
												{
													$res_media7[] = $res;}
											}
										}
									}
								}
							}
						}
						$sql_get_pro3 = mysql_query("select * from community_event where creators_info='".$new_creator_info."' AND del_status=0 AND active=0");
						if($sql_get_pro3!="" && $sql_get_pro3!=NULL)
						{
							if(mysql_num_rows($sql_get_pro3)>0)
							{
								while($row1 = mysql_fetch_assoc($sql_get_pro3))
								{
									$creator_info = "community_event|".$row1['id'];
									$get_media = mysql_query("select * from general_media where from_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
									if($get_media!="" && $get_media!=NULL)
									{
										if(mysql_num_rows($get_media)>0)
										{
											while($res = mysql_fetch_assoc($get_media))
											{
												if($id['general_user_id'] != $res['general_user_id'])
												{
													$res_media8[] = $res;}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
		if($id['community_id']!=0 && !empty($id['community_id']))
        {
		
			$chk_type = $var_type_2.'|'.$id['community_id'];
			$sql_get_pro = mysql_query("select * from artist_project where creators_info='".$chk_type."' AND del_status=0 AND active=0");
			if($sql_get_pro!="" && $sql_get_pro!=NULL)
			{
				if(mysql_num_rows($sql_get_pro)>0)
				{
					while($row = mysql_fetch_assoc($sql_get_pro))
					{
						$creator_info = "artist_project|".$row['id'];
						$get_media = mysql_query("select * from general_media where from_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
						if($get_media!="" && $get_media!=NULL)
						{
							if(mysql_num_rows($get_media)>0)
							{
								while($res = mysql_fetch_assoc($get_media))
								{
									if($id['general_user_id'] != $res['general_user_id'])
									{
										$res_media9[] = $res;}
								}
							}
						}
					}
				}
			}
			$sql_get_pro1 = mysql_query("select * from community_project where creators_info='".$chk_type."' AND del_status=0 AND active=0");
			if($sql_get_pro1!="" && $sql_get_pro1!=NULL)
			{
				if(mysql_num_rows($sql_get_pro1)>0)
				{
					while($row1 = mysql_fetch_assoc($sql_get_pro1))
					{
						$creator_info = "community_project|".$row1['id'];
						$get_media = mysql_query("select * from general_media where from_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
						if($get_media!="" && $get_media!=NULL)
						{
							if(mysql_num_rows($get_media)>0)
							{
								while($res = mysql_fetch_assoc($get_media))
								{
									if($id['general_user_id'] != $res['general_user_id'])
									{
										$res_media10[] = $res;}
								}
							}
						}
					}
				}
			}
			$sql_get_pro1 = mysql_query("select * from community_event where creators_info='".$chk_type."' AND del_status=0 AND active=0");
			if($sql_get_pro1!="" && $sql_get_pro1!=NULL)
			{
				if(mysql_num_rows($sql_get_pro1)>0)
				{
					while($row1 = mysql_fetch_assoc($sql_get_pro1))
					{
						$creator_info = "community_event|".$row1['id'];
						$get_media = mysql_query("select * from general_media where from_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
						if($get_media!="" && $get_media!=NULL)
						{
							if(mysql_num_rows($get_media)>0)
							{
								while($res = mysql_fetch_assoc($get_media))
								{
									if($id['general_user_id'] != $res['general_user_id'])
									{
										$res_media11[] = $res;}
								}
							}
						}
					}
				}
			}
			$sql_get_pro1 = mysql_query("select * from artist_event where creators_info='".$chk_type."' AND del_status=0 AND active=0");
			if($sql_get_pro1!="" && $sql_get_pro1!=NULL)
			{
				if(mysql_num_rows($sql_get_pro1)>0)
				{
					while($row1 = mysql_fetch_assoc($sql_get_pro1))
					{
						$creator_info = "artist_event|".$row1['id'];
						$get_media = mysql_query("select * from general_media where from_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
						if($get_media!="" && $get_media!=NULL)
						{
							if(mysql_num_rows($get_media)>0)
							{
								while($res = mysql_fetch_assoc($get_media))
								{
									if($id['general_user_id'] != $res['general_user_id'])
									{
										$res_media12[] = $res;}
								}
							}
						}
					}
				}
			}
			
			$find_project = mysql_query("select * from community_project where community_id='".$id['community_id']."' AND del_status=0 AND active=0");
			if($find_project!="" && $find_project!=NULL)
			{
				if(mysql_num_rows($find_project)>0)
				{
					while($get_all_pro = mysql_fetch_assoc($find_project))
					{
						$new_creator_info = "community_project|".$get_all_pro['id'];
						$sql_get_pro = mysql_query("select * from artist_project where creators_info='".$new_creator_info."' AND del_status=0 AND active=0");
						if($sql_get_pro!="" && $sql_get_pro!=NULL)
						{
							if(mysql_num_rows($sql_get_pro)>0)
							{
								while($row = mysql_fetch_assoc($sql_get_pro))
								{
									$creator_info = "artist_project|".$row['id'];
									$get_media = mysql_query("select * from general_media where from_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
									if($get_media!="" && $get_media!=NULL)
									{
										if(mysql_num_rows($get_media)>0)
										{
											while($res = mysql_fetch_assoc($get_media))
											{
												if($id['general_user_id'] != $res['general_user_id'])
												{
													$res_media13[] = $res;}
											}
										}
									}
								}
							}
						}
						$sql_get_pro1 = mysql_query("select * from community_project where creators_info='".$new_creator_info."' AND del_status=0 AND active=0");
						if($sql_get_pro1!="" && $sql_get_pro1!=NULL)
						{
							if(mysql_num_rows($sql_get_pro1)>0)
							{
								while($row1 = mysql_fetch_assoc($sql_get_pro1))
								{
									$creator_info = "community_project|".$row1['id'];
									$get_media = mysql_query("select * from general_media where from_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
									if($get_media!="" && $get_media!=NULL)
									{
										if(mysql_num_rows($get_media)>0)
										{
											while($res = mysql_fetch_assoc($get_media))
											{
												if($id['general_user_id'] != $res['general_user_id'])
												{
													$res_media14[] = $res;}
											}
										}
									}
								}
							}
						}
						$sql_get_pro2 = mysql_query("select * from artist_event where creators_info='".$new_creator_info."' AND del_status=0 AND active=0");
						if($sql_get_pro2!="" && $sql_get_pro2!=NULL)
						{
							if(mysql_num_rows($sql_get_pro2)>0)
							{
								while($row = mysql_fetch_assoc($sql_get_pro2))
								{
									$creator_info = "artist_event|".$row['id'];
									$get_media = mysql_query("select * from general_media where from_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
									if($get_media!="" && $get_media!=NULL)
									{
										if(mysql_num_rows($get_media)>0)
										{
											while($res = mysql_fetch_assoc($get_media))
											{
												if($id['general_user_id'] != $res['general_user_id'])
												{
													$res_media15[] = $res;}
											}
										}
									}
								}
							}
						}
						$sql_get_pro3 = mysql_query("select * from community_event where creators_info='".$new_creator_info."' AND del_status=0 AND active=0");
						if($sql_get_pro3!="" && $sql_get_pro3!=NULL)
						{
							if(mysql_num_rows($sql_get_pro3)>0)
							{
								while($row1 = mysql_fetch_assoc($sql_get_pro3))
								{
									$creator_info = "community_event|".$row1['id'];
									$get_media = mysql_query("select * from general_media where from_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
									if($get_media!="" && $get_media!=NULL)
									{
										if(mysql_num_rows($get_media)>0)
										{
											while($res = mysql_fetch_assoc($get_media))
											{
												if($id['general_user_id'] != $res['general_user_id'])
												{
													$res_media16[] = $res;}
											}
										}
									}
								}
							}
						}
					}
				}
			}
			$find_project = mysql_query("select * from community_event where community_id='".$id['community_id']."' AND del_status=0 AND active=0");
			if($find_project!="" && $find_project!=NULL)
			{
				if(mysql_num_rows($find_project)>0)
				{
					while($get_all_pro = mysql_fetch_assoc($find_project))
					{
						$new_creator_info = "community_event|".$get_all_pro['id'];
						$sql_get_pro = mysql_query("select * from artist_project where creators_info='".$new_creator_info."' AND del_status=0 AND active=0");
						if($sql_get_pro!="" && $sql_get_pro!=NULL)
						{
							if(mysql_num_rows($sql_get_pro)>0)
							{
								while($row = mysql_fetch_assoc($sql_get_pro))
								{
									$creator_info = "artist_project|".$row['id'];
									$get_media = mysql_query("select * from general_media where from_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
									if($get_media!="" && $get_media!=NULL)
									{
										if(mysql_num_rows($get_media)>0)
										{
											while($res = mysql_fetch_assoc($get_media))
											{
												if($id['general_user_id'] != $res['general_user_id'])
												{
													$res_media13[] = $res;}
											}
										}
									}
								}
							}
						}
						$sql_get_pro1 = mysql_query("select * from community_project where creators_info='".$new_creator_info."' AND del_status=0 AND active=0");
						if($sql_get_pro1!="" && $sql_get_pro1!=NULL)
						{
							if(mysql_num_rows($sql_get_pro1)>0)
							{
								while($row1 = mysql_fetch_assoc($sql_get_pro1))
								{
									$creator_info = "community_project|".$row1['id'];
									$get_media = mysql_query("select * from general_media where from_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
									if($get_media!="" && $get_media!=NULL)
									{
										if(mysql_num_rows($get_media)>0)
										{
											while($res = mysql_fetch_assoc($get_media))
											{
												if($id['general_user_id'] != $res['general_user_id'])
												{
													$res_media14[] = $res;}
											}
										}
									}
								}
							}
						}
						$sql_get_pro2 = mysql_query("select * from artist_event where creators_info='".$new_creator_info."' AND del_status=0 AND active=0");
						if($sql_get_pro2!="" && $sql_get_pro2!=NULL)
						{
							if(mysql_num_rows($sql_get_pro2)>0)
							{
								while($row = mysql_fetch_assoc($sql_get_pro2))
								{
									$creator_info = "artist_event|".$row['id'];
									$get_media = mysql_query("select * from general_media where from_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
									if($get_media!="" && $get_media!=NULL)
									{
										if(mysql_num_rows($get_media)>0)
										{
											while($res = mysql_fetch_assoc($get_media))
											{
												if($id['general_user_id'] != $res['general_user_id'])
												{
													$res_media15[] = $res;}
											}
										}
									}
								}
							}
						}
						$sql_get_pro3 = mysql_query("select * from community_event where creators_info='".$new_creator_info."' AND del_status=0 AND active=0");
						if($sql_get_pro3!="" && $sql_get_pro3!=NULL)
						{
							if(mysql_num_rows($sql_get_pro3)>0)
							{
								while($row1 = mysql_fetch_assoc($sql_get_pro3))
								{
									$creator_info = "community_event|".$row1['id'];
									$get_media = mysql_query("select * from general_media where from_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
									if($get_media!="" && $get_media!=NULL)
									{
										if(mysql_num_rows($get_media)>0)
										{
											while($res = mysql_fetch_assoc($get_media))
											{
												if($id['general_user_id'] != $res['general_user_id'])
												{
													$res_media16[] = $res;}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
		   
		$get_all_media = array_merge($res_media1,$res_media2,$res_media3,$res_media4,$res_media5,$res_media6,$res_media7,$res_media8,$res_media9,$res_media10,$res_media11,$res_media12,$res_media13,$res_media14,$res_media15,$res_media16);
		return($get_all_media);
	}
	function get_the_user_tagin_project($email)
	{
		$id = $this->sel_general();
        $var_type_1 = 'general_artist';
        $var_type_2 = 'general_community';
        $var_type_3 = 'artist_project';
        $var_type_4 = 'community_project';
		$res_media1 = array();
		$res_media2 = array();
		$res_media3 = array();
		$res_media4 = array();

		$get_all_art_project = mysql_query("SELECT * FROM artist_project WHERE tagged_user_email LIKE '%".$email."%' AND del_status=0 AND active=0");
		if($get_all_art_project!="" && $get_all_art_project!=NULL)
		{
			if(mysql_num_rows($get_all_art_project)>0)
			{
				while($res_data = mysql_fetch_assoc($get_all_art_project))
				{
					$creator_info = "artist_project|".$res_data['id'];
					$get_media = mysql_query("select * from general_media where creator_info ='".$creator_info."' OR from_info ='".$creator_info."' AND delete_status=0 AND media_status=0");
					if($get_media!="" && $get_media!=NULL)
					{
						if(mysql_num_rows($get_media)>0)
						{
							while($res_media_data = mysql_fetch_assoc($get_media))
							{
								if($id['general_user_id']!=$res_media_data['general_user_id'])
								{
									$res_media1[] = $res_media_data;}
							}
						}
					}
				}
			}
		}
		$get_all_art_event = mysql_query("SELECT * FROM artist_event WHERE tagged_user_email LIKE '%".$email."%' AND del_status=0 AND active=0");
		if($get_all_art_event!="" && $get_all_art_event!=NULL)
		{
			if(mysql_num_rows($get_all_art_event)>0)
			{
				while($res_data = mysql_fetch_assoc($get_all_art_event))
				{
					$creator_info = "artist_event|".$res_data['id'];
					$get_media = mysql_query("select * from general_media where creator_info ='".$creator_info."' OR from_info ='".$creator_info."' AND delete_status=0 AND media_status=0");
					if($get_media!="" && $get_media!=NULL)
					{
						if(mysql_num_rows($get_media)>0)
						{
							while($res_media_data = mysql_fetch_assoc($get_media))
							{
								if($id['general_user_id']!=$res_media_data['general_user_id'])
								{
									$res_media2[] = $res_media_data;}
							}
						}
					}
				}
			}
		}
		$get_all_com_event = mysql_query("SELECT * FROM community_event WHERE tagged_user_email LIKE '%".$email."%' AND del_status=0 AND active=0");
		if($get_all_com_event!="" && $get_all_com_event!=NULL)
		{
			if(mysql_num_rows($get_all_com_event)>0)
			{
				while($res_data = mysql_fetch_assoc($get_all_com_event))
				{
					$creator_info = "community_event|".$res_data['id'];
					$get_media = mysql_query("select * from general_media where creator_info ='".$creator_info."' OR from_info ='".$creator_info."' AND delete_status=0 AND media_status=0");
					if($get_media!="" && $get_media!=NULL)
					{
						if(mysql_num_rows($get_media)>0)
						{
							while($res_media_data = mysql_fetch_assoc($get_media))
							{
								if($id['general_user_id']!=$res_media_data['general_user_id'])
								{
									$res_media3[] = $res_media_data;}
							}
						}
					}
				}
			}
		}
		$get_all_com_project = mysql_query("SELECT * FROM community_project WHERE tagged_user_email LIKE '%".$email."%' AND del_status=0 AND active=0");
		if($get_all_com_project!="" && $get_all_com_project!=NULL)
		{
			if(mysql_num_rows($get_all_com_project)>0)
			{
				while($res_data = mysql_fetch_assoc($get_all_com_project))
				{
					$creator_info = "community_project|".$res_data['id'];
					$get_media = mysql_query("select * from general_media where creator_info ='".$creator_info."' OR from_info ='".$creator_info."' AND delete_status=0 AND media_status=0");
					if($get_media!="" && $get_media!=NULL)
					{
						if(mysql_num_rows($get_media)>0)
						{
							while($res_media_data = mysql_fetch_assoc($get_media))
							{
								if($id['general_user_id']!=$res_media_data['general_user_id'])
								{
									$res_media4[] = $res_media_data;}
							}
						}
					}
				}
			}
		}
		
		$get_all_media = array_merge($res_media1,$res_media2,$res_media3,$res_media4);
		//var_dump($get_all_media);
		return($get_all_media);
	}
	function get_media_create()
	{
		$id = $this->sel_general();
		
		$var_type_1 = 'general_artist';
		$var_type_2 = 'general_community';
		$var_type_3 = 'artist_project';
		$var_type_4 = 'community_project';
		
		$pal_1 = array();
		$pal_2 = array();
		$pal_3 = array();
		$pal_4 = array();
		
		$final_c_f = array();
		
		if($id['artist_id']!=0 && !empty($id['artist_id']))
		{
			$chk_type_a = $var_type_1.'|'.$id['artist_id'];

			$sql = mysql_query("SELECT * FROM general_media WHERE creator_info='".$chk_type_a."' AND delete_status=0 AND media_status=0 AND general_user_id!='".$id['general_user_id']."'");
			if($sql!="" && $sql!=NULL)
			{
				if(mysql_num_rows($sql)>0)
				{
					while($row_a = mysql_fetch_assoc($sql))
					{
						$type_new_id = $this->sel_type_of_tagged_media($row_a['media_type']);
						if($row_a['media_type']==='114'){
							$media_track = $this->get_media_track($row_a['id']);
							$row_a['track'] = $media_track['track'];
						}
						$row_a['name'] = $type_new_id['name'];
						$row_a['media_from'] = "media_create_new";
						$final_c_f[] = $row_a;
					}
				}
			}
		}
		
		/* if($id['community_id']!=0 && !empty($id['community_id']))
		{
			$chk_type_c = $var_type_2.'|'.$id['community_id'];
			
			$sql = mysql_query("SELECT * FROM general_media WHERE creator_info='".$chk_type_c."' AND delete_status=0 AND media_status=0 AND media_type='".$type."' AND general_user_id!='".$id['general_user_id']."'");
			if(mysql_num_rows($sql)>0)
			{
				while($row_c = mysql_fetch_assoc($sql))
				{
					$final_c_f[] = $row_c;
				}				
			}
		} */
		
		/* if($id['artist_id']!=0 && !empty($id['artist_id']))
		{			
			$sql_get_allp = mysql_query("SELECT * FROM artist_project WHERE artist_id='".$id['artist_id']."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_get_allp)>0)
			{
				while($rowal = mysql_fetch_assoc($sql_get_allp))
				{
					$pal_1[] = $rowal;
				}
			}
			
			$sql_get_alle = mysql_query("SELECT * FROM artist_event WHERE artist_id='".$id['artist_id']."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_get_alle)>0)
			{
				while($rowael = mysql_fetch_assoc($sql_get_alle))
				{
					$pal_3[] = $rowael;
				}
			}
			
			if(!empty($pal_1) && count($pal_1))
			{
				for($pl=0;$pl<count($pal_1);$pl++)
				{
					$chk_typep_1 = 'artist_project|'.$pal_1[$pl]['id'];
					
					$sql_as = mysql_query("SELECT * FROM general_media WHERE creator_info='".$chk_typep_1."' AND delete_status=0 AND media_status=0 AND media_type='".$type."' AND general_user_id!='".$id['general_user_id']."'");
					if(mysql_num_rows($sql_as)>0)
					{
						while($row_ap = mysql_fetch_assoc($sql_as))
						{
							$final_c_f[] = $row_ap;
						}
					}
				}
			}
			
			if(!empty($pal_3) && count($pal_3))
			{
				for($pl=0;$pl<count($pal_3);$pl++)
				{
					$chk_typep_1 = 'artist_event|'.$pal_3[$pl]['id'];
					
					$sql_as = mysql_query("SELECT * FROM general_media WHERE from_info='".$chk_typep_1."' AND delete_status=0 AND media_status=0 AND media_type='".$type."' AND general_user_id!='".$id['general_user_id']."'");
					if(mysql_num_rows($sql_as)>0)
					{
						while($row_ae = mysql_fetch_assoc($sql_as))
						{
							$final_c_f[] = $row_ae;
						}
					}
				}
			}
		}
		
		if($id['community_id']!=0 && !empty($id['community_id']))
		{			
			$sql_get_allp = mysql_query("SELECT * FROM community_project WHERE community_id='".$id['community_id']."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_get_allp)>0)
			{
				while($rowal = mysql_fetch_assoc($sql_get_allp))
				{
					$pal_2[] = $rowal;
				}
			}
			
			$sql_get_allew = mysql_query("SELECT * FROM community_event WHERE community_id='".$id['community_id']."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_get_allew)>0)
			{
				while($rowawl = mysql_fetch_assoc($sql_get_allew))
				{
					$pal_4[] = $rowawl;
				}
			}
			
			if(!empty($pal_2) && count($pal_2))
			{
				for($pl=0;$pl<count($pal_1);$pl++)
				{
					$chk_typep_1 = 'community_project|'.$pal_1[$pl]['id'];
					
					$sql_as = mysql_query("SELECT * FROM general_media WHERE creator_info='".$chk_typep_1."' AND delete_status=0 AND media_status=0 AND media_type='".$type."' AND general_user_id!='".$id['general_user_id']."'");
					if(mysql_num_rows($sql_as)>0)
					{
						while($row_cp = mysql_fetch_assoc($sql_as))
						{
							$final_c_f[] = $row_cp;
						}
					}
				}
			}
			
			if(!empty($pal_4) && count($pal_4))
			{
				for($pl=0;$pl<count($pal_4);$pl++)
				{
					$chk_typep_1 = 'community_event|'.$pal_4[$pl]['id'];
					
					$sql_as = mysql_query("SELECT * FROM general_media WHERE from_info='".$chk_typep_1."' AND delete_status=0 AND media_status=0 AND media_type='".$type."' AND general_user_id!='".$id['general_user_id']."'");
					if(mysql_num_rows($sql_as)>0)
					{
						while($row_ce = mysql_fetch_assoc($sql_as))
						{
							$final_c_f[] = $row_ce;
						}
					}
				}
			}
		} */
		//$final_c_f = array_merge($final_c_f1,$final_c_f2);
		return $final_c_f;
	}
	function get_media_create_with_type($type)
	{
		$id = $this->sel_general();
		
		$var_type_1 = 'general_artist';
		$var_type_2 = 'general_community';
		$var_type_3 = 'artist_project';
		$var_type_4 = 'community_project';
		
		$pal_1 = array();
		$pal_2 = array();
		$pal_3 = array();
		$pal_4 = array();
		
		$final_c_f = array();
		
		if($id['artist_id']!=0 && !empty($id['artist_id']))
		{
			$chk_type_a = $var_type_1.'|'.$id['artist_id'];

			$sql = mysql_query("SELECT * FROM general_media WHERE creator_info='".$chk_type_a."' AND media_type='".$type."' AND delete_status=0 AND media_status=0 AND general_user_id!='".$id['general_user_id']."'");
			if($sql!="" && $sql!=NULL)
			{
				if(mysql_num_rows($sql)>0)
				{
					while($row_a = mysql_fetch_assoc($sql))
					{
						$type_new_id = $this->sel_type_of_tagged_media($row_a['media_type']);
						if($row_a['media_type']==='114'){
							$media_track = $this->get_media_track($row_a['id']);
							$row_a['track'] = $media_track['track'];
						}
						$row_a['name'] = $type_new_id['name'];
						$row_a['media_from'] = "media_create_new";
						$final_c_f[] = $row_a;
					}
				}
			}
		}
		
		/* if($id['community_id']!=0 && !empty($id['community_id']))
		{
			$chk_type_c = $var_type_2.'|'.$id['community_id'];
			
			$sql = mysql_query("SELECT * FROM general_media WHERE creator_info='".$chk_type_c."' AND delete_status=0 AND media_status=0 AND media_type='".$type."' AND general_user_id!='".$id['general_user_id']."'");
			if(mysql_num_rows($sql)>0)
			{
				while($row_c = mysql_fetch_assoc($sql))
				{
					$final_c_f[] = $row_c;
				}				
			}
		} */
		
		/* if($id['artist_id']!=0 && !empty($id['artist_id']))
		{			
			$sql_get_allp = mysql_query("SELECT * FROM artist_project WHERE artist_id='".$id['artist_id']."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_get_allp)>0)
			{
				while($rowal = mysql_fetch_assoc($sql_get_allp))
				{
					$pal_1[] = $rowal;
				}
			}
			
			$sql_get_alle = mysql_query("SELECT * FROM artist_event WHERE artist_id='".$id['artist_id']."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_get_alle)>0)
			{
				while($rowael = mysql_fetch_assoc($sql_get_alle))
				{
					$pal_3[] = $rowael;
				}
			}
			
			if(!empty($pal_1) && count($pal_1))
			{
				for($pl=0;$pl<count($pal_1);$pl++)
				{
					$chk_typep_1 = 'artist_project|'.$pal_1[$pl]['id'];
					
					$sql_as = mysql_query("SELECT * FROM general_media WHERE creator_info='".$chk_typep_1."' AND delete_status=0 AND media_status=0 AND media_type='".$type."' AND general_user_id!='".$id['general_user_id']."'");
					if(mysql_num_rows($sql_as)>0)
					{
						while($row_ap = mysql_fetch_assoc($sql_as))
						{
							$final_c_f[] = $row_ap;
						}
					}
				}
			}
			
			if(!empty($pal_3) && count($pal_3))
			{
				for($pl=0;$pl<count($pal_3);$pl++)
				{
					$chk_typep_1 = 'artist_event|'.$pal_3[$pl]['id'];
					
					$sql_as = mysql_query("SELECT * FROM general_media WHERE from_info='".$chk_typep_1."' AND delete_status=0 AND media_status=0 AND media_type='".$type."' AND general_user_id!='".$id['general_user_id']."'");
					if(mysql_num_rows($sql_as)>0)
					{
						while($row_ae = mysql_fetch_assoc($sql_as))
						{
							$final_c_f[] = $row_ae;
						}
					}
				}
			}
		}
		
		if($id['community_id']!=0 && !empty($id['community_id']))
		{			
			$sql_get_allp = mysql_query("SELECT * FROM community_project WHERE community_id='".$id['community_id']."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_get_allp)>0)
			{
				while($rowal = mysql_fetch_assoc($sql_get_allp))
				{
					$pal_2[] = $rowal;
				}
			}
			
			$sql_get_allew = mysql_query("SELECT * FROM community_event WHERE community_id='".$id['community_id']."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_get_allew)>0)
			{
				while($rowawl = mysql_fetch_assoc($sql_get_allew))
				{
					$pal_4[] = $rowawl;
				}
			}
			
			if(!empty($pal_2) && count($pal_2))
			{
				for($pl=0;$pl<count($pal_1);$pl++)
				{
					$chk_typep_1 = 'community_project|'.$pal_1[$pl]['id'];
					
					$sql_as = mysql_query("SELECT * FROM general_media WHERE creator_info='".$chk_typep_1."' AND delete_status=0 AND media_status=0 AND media_type='".$type."' AND general_user_id!='".$id['general_user_id']."'");
					if(mysql_num_rows($sql_as)>0)
					{
						while($row_cp = mysql_fetch_assoc($sql_as))
						{
							$final_c_f[] = $row_cp;
						}
					}
				}
			}
			
			if(!empty($pal_4) && count($pal_4))
			{
				for($pl=0;$pl<count($pal_4);$pl++)
				{
					$chk_typep_1 = 'community_event|'.$pal_4[$pl]['id'];
					
					$sql_as = mysql_query("SELECT * FROM general_media WHERE from_info='".$chk_typep_1."' AND delete_status=0 AND media_status=0 AND media_type='".$type."' AND general_user_id!='".$id['general_user_id']."'");
					if(mysql_num_rows($sql_as)>0)
					{
						while($row_ce = mysql_fetch_assoc($sql_as))
						{
							$final_c_f[] = $row_ce;
						}
					}
				}
			}
		} */
		//$final_c_f = array_merge($final_c_f1,$final_c_f2);
		return $final_c_f;
	}
	function get_artist_event_tag($email)
	{
		$query = mysql_query("SELECT * FROM artist_event WHERE del_status=0 AND active=0 AND tagged_user_email LIKE '%".$email."%'");
		return($query);
	}
	
	function get_community_event_tag($email)
	{
		$query = mysql_query("SELECT * FROM community_event WHERE del_status=0 AND active=0 AND tagged_user_email LIKE '%".$email."%'");
		return($query);
	}
	function get_creator_url($tbl_name,$id)
	{
		$res = "";
		if($tbl_name =="general_artist"){
			$id_name = "artist_id";}
		if($tbl_name =="general_community"){
			$id_name = "community_id";}
		if($tbl_name =="artist_project" || $tbl_name =="community_project" || $tbl_name =="artist_event" || $tbl_name =="community_event"){
			$id_name = "id";}
		$sql = mysql_query("select * from ".$tbl_name." where ".$id_name."='".$id."'");
		if($sql!="" && $sql!=NULL)
		{
			$res = mysql_fetch_assoc($sql);
			return($res);
		}
	}
	function get_from_url($tbl_name,$id)
	{
		$sql = "";
		if($tbl_name =="general_artist"){
			$id_name = "artist_id";}
		if($tbl_name =="general_community"){
			$id_name = "community_id";}
		if($tbl_name =="artist_project" || $tbl_name =="community_project" || $tbl_name =="artist_event" || $tbl_name =="community_event"){
			$id_name = "id";}
		$sql = mysql_query("select * from ".$tbl_name." where ".$id_name."='".$id."'");
		if($sql!="" && $sql!=NULL)
		{
			$res = mysql_fetch_assoc($sql);
			return($res);
		}
	}
	
	function get_all_del_media_id()
	{
		$all_deleted_id = "";
		$sql = mysql_query("select * from general_user where email='".$_SESSION['login_email']."'");
		if($sql!="" && $sql!=NULL)
		{
			$res = mysql_fetch_assoc($sql);
			$all_deleted_id = $res['deleted_media_id'];
		
		
		/*$get_subscription = mysql_query("select * from email_subscription where email='".$_SESSION['login_email']."'");
		$res_get_subscription = mysql_fetch_assoc($get_subscription);
		$all_deleted_id = $all_deleted_id .",". $res_get_subscription['deleted_media_id'];
		if($res['artist_id'] >0)
		{
			$get_art = mysql_query("select * from general_artist where artist_id='".$res['artist_id']."'");
			if(mysql_num_rows($get_art)>0)
			{
				$res_art = mysql_fetch_assoc($get_art);
				if($res_art['deleted_media_by_creator_creator'] !="" || $res_art['deleted_media_by_from_from']!=""){
				$all_deleted_id = $all_deleted_id ."," . $res_art['deleted_media_by_creator_creator'].",".$res_art['deleted_media_by_from_from'];}
			}
			$get_art_pro = mysql_query("select * from artist_project where artist_id='".$res['artist_id']."'");
			if(mysql_num_rows($get_art_pro)>0)
			{
				while($res_art_pro = mysql_fetch_assoc($get_art_pro))
				{
					if($res_art_pro['deleted_media_by_creator_creator'] !="" || $res_art_pro['deleted_media_by_from_from'] !=""){
					$all_deleted_id = $all_deleted_id ."," . $res_art_pro['deleted_media_by_creator_creator'].",".$res_art_pro['deleted_media_by_from_from'];}
				}
			}
			$get_art_eve = mysql_query("select * from artist_event where artist_id='".$res['artist_id']."'");
			if(mysql_num_rows($get_art_eve)>0)
			{
				while($res_art_eve = mysql_fetch_assoc($get_art_eve))
				{
					if($res_art_eve['deleted_media_by_creator_creator'] !="" || $res_art_eve['deleted_media_by_from_from']!=""){
					$all_deleted_id = $all_deleted_id ."," . $res_art_eve['deleted_media_by_creator_creator'].",".$res_art_eve['deleted_media_by_from_from'];}
				}
			}
		}
		if($res['community_id'] >0)
		{
			$get_com = mysql_query("select * from general_community where community_id='".$res['community_id']."'");
			if(mysql_num_rows($get_com)>0)
			{
				$res_com = mysql_fetch_assoc($get_com);
				if($res_com['deleted_media_by_creator_creator'] !="" || $res_com['deleted_media_by_from_from'] !=""){
				$all_deleted_id = $all_deleted_id ."," . $res_com['deleted_media_by_creator_creator'].",".$res_com['deleted_media_by_from_from'];}
			}
			$get_com_pro = mysql_query("select * from community_project where community_id='".$res['community_id']."'");
			if(mysql_num_rows($get_com_pro)>0)
			{
				while($res_com_pro = mysql_fetch_assoc($get_com_pro))
				{
					if($res_com_pro['deleted_media_by_creator_creator'] !="" || $res_com_pro['deleted_media_by_from_from'] !=""){
					$all_deleted_id = $all_deleted_id ."," . $res_com_pro['deleted_media_by_creator_creator'].",".$res_com_pro['deleted_media_by_from_from'];}
				}
			}
			$get_com_eve = mysql_query("select * from community_event where community_id='".$res['community_id']."'");
			if(mysql_num_rows($get_com_eve)>0)
			{
				while($res_com_eve = mysql_fetch_assoc($get_com_eve))
				{
					if($res_com_eve['deleted_media_by_creator_creator'] !="" || $res_com_eve['deleted_media_by_from_from'] !=""){
					$all_deleted_id = $all_deleted_id ."," . $res_com_eve['deleted_media_by_creator_creator'].",".$res_com_eve['deleted_media_by_from_from'];}
				}
			}
		}*/
		}
		return($all_deleted_id);
	}
	
	function get_subscription_media()
	{
		$date = date("Y-m-d");
		$art_media = array();
		$com_media = array();
		$artcom_media = array();
		$all_combine_media = array();
		$get_gen_det = $this->sel_general();
		$sql_sub = mysql_query("SELECT * FROM email_subscription WHERE email='".$_SESSION['login_email']."' AND delete_status=1 AND expiry_date>'".$date."'");
		if($sql_sub!="" && $sql_sub!=NULL)
		{
			if(mysql_num_rows($sql_sub)>0)
			{
				while($ans_sub = mysql_fetch_assoc($sql_sub))
				{
					if($ans_sub['related_type']=='artist')
					{
						$sql_meds = mysql_query("SELECT * FROM general_media WHERE (creator_info='general_artist|".$ans_sub['related_id']."' OR from_info='general_artist|".$ans_sub['related_id']."') AND (sharing_preference=1 OR sharing_preference=2) AND media_status=0 AND delete_status=0");     
						if($sql_meds!="" && $sql_meds!=NULL)
						{
							if(mysql_num_rows($sql_meds)>0)
							{
								while($res_media = mysql_fetch_assoc($sql_meds))
								{
									if($get_gen_det['general_user_id']!=$res_media['general_user_id']){
										$art_media[] = $res_media;
									}
								}
							}
						}
					}
					elseif($ans_sub['related_type']=='community')
					{
						$sql_meds = mysql_query("SELECT * FROM general_media WHERE (creator_info='general_community|".$ans_sub['related_id']."' OR from_info='general_community|".$ans_sub['related_id']."') AND (sharing_preference=1 OR sharing_preference=2) AND media_status=0 AND delete_status=0");
						if($sql_meds!="" && $sql_meds!=NULL)
						{
							if(mysql_num_rows($sql_meds)>0)
							{
								while($res_media = mysql_fetch_assoc($sql_meds))
								{
									if($get_gen_det['general_user_id']!=$res_media['general_user_id']){
										$com_media[] = $res_media;
									}
								}
							}
						}
					}
					elseif($ans_sub['related_type']=='community_project' || $ans_sub['related_type']=='community_event' || $ans_sub['related_type']=='artist_project' || $ans_sub['related_type']=='artist_event')
					{
						$rel_id = explode('_',$ans_sub['related_id']);
						$sql_meds = mysql_query("SELECT * FROM general_media WHERE (creator_info='".$ans_sub['related_type']."|".$rel_id[1]."' OR from_info='".$ans_sub['related_type']."|".$rel_id[1]."') AND (sharing_preference=1 OR sharing_preference=2) AND media_status=0 AND delete_status=0");
						if($sql_meds!="" && $sql_meds!=NULL)
						{
							if(mysql_num_rows($sql_meds)>0)
							{
								while($res_media = mysql_fetch_assoc($sql_meds))
								{
									if($get_gen_det['general_user_id']!=$res_media['general_user_id']){
										$artcom_media[] = $res_media;
									}
								}
							}
						}
					}
				}
			}
		}
		
		$all_combine_media = array_merge($art_media,$com_media,$artcom_media);
		return($all_combine_media);
	}
	
	function get_all_media_selected_channel($media_id)
	{
		$get_chan_media = mysql_query("select * from media_channel where media_id='".$media_id."'");
		if(mysql_num_rows($get_chan_media)>0)
		{
			$res_chan_media = mysql_fetch_assoc($get_chan_media);
			return($res_chan_media);
		}
	}
	function get_media_storage()
	{
		$media_songs = "";
		$media_videos = "";
		$media_galleries = "";
		$all_size = "";
		$gen_info = $this->sel_general();
		$chk_user_unlimit_admin = mysql_query("select * from general_user where general_user_id='".$gen_info['general_user_id']."'");
		if(mysql_num_rows($chk_user_unlimit_admin)>0){
			$res_user_unlimit_admin = mysql_fetch_assoc($chk_user_unlimit_admin);
		}
		$get_users_s3 = mysql_query("select * from user_keys where general_user_id='".$gen_info['general_user_id']."' AND sourcesystem='AmazonS3'");
		if(mysql_num_rows($get_users_s3)>0){
			$all_size = "s3integrated";
		}else if(!empty($res_user_unlimit_admin['media_limit']) && $res_user_unlimit_admin['media_limit']=="unlimited"){
			$all_size = "adminset";
		}else{
			$get_media = mysql_query("select * from general_media where general_user_id='".$gen_info['general_user_id']."' AND media_status=0 AND delete_status=0 AND sharing_preference!=5");
			if($get_media!="" && $get_media!=NULL){
				if(mysql_num_rows($get_media)>0){
					while($res_media = mysql_fetch_assoc($get_media)){
						if($res_media['media_type']=='114'){
							$get_media_song = mysql_query("select * from media_songs where media_id='".$res_media['id']."'");
							if($get_media_song!="" && $get_media_song!=NULL)
							{
								$res_media_song = mysql_fetch_assoc($get_media_song);
								$media_songs = $media_songs + $res_media_song['size'];
							}
						}if($res_media['media_type']=='113'){
							$get_media_img = mysql_query("select * from media_images where media_id='".$res_media['id']."'");
							if($get_media_img!="" && $get_media_img!=NULL)
							{
								if(mysql_num_rows($get_media_img)>0){
									while($res_media_img = mysql_fetch_assoc($get_media_img)){
										$media_galleries = $media_galleries + $res_media_img['size'];
									}
								}
							}
						}if($res_media['media_type']=='115'){
							$get_media_video = mysql_query("select * from media_videos where media_id='".$res_media['id']."'");
							if($get_media_video!="" && $get_media_video!=NULL)
							{
								$res_media_video = mysql_fetch_assoc($get_media_video);
								$media_videos = $media_videos + $res_media_video['size'];
							}
						}
					}
				}
			}
			$all_size = ($media_videos + $media_galleries + $media_songs);
		}
		
		return($all_size);
	}
	function get_audio_img($audio_id)
	{
		$image_name ="";
		$sql_media_img = mysql_query("SELECT * FROM general_media WHERE id='".$audio_id."' AND delete_status=0 AND media_status=0");
		if($sql_media_img!="" && $sql_media_img!=NULL){
			if(mysql_num_rows($sql_media_img)>0)
			{
				$res_media_img = mysql_fetch_assoc($sql_media_img);
				if($res_media_img['from_info']!="")
				{
					$exp_from_info = explode("|",$res_media_img['from_info']);
					$get_img = mysql_query("select * from ".$exp_from_info[0]." where id=".$exp_from_info[1]."");
					if($get_img!="" && $get_img!=NULL)
					{
						if(mysql_num_rows($get_img)>0)
						{
							$res_img = mysql_fetch_assoc($get_img);
							$image_name = $res_img['image_name'];
							
						}
					}
				}
				if($res_media_img['from_info'] =="" && $res_media_img['creator_info']!="")
				{
					$exp_creator_info = explode("|",$res_media_img['creator_info']);
					if($exp_creator_info[0]=="general_artist"){$id_name ="artist_id";
					$image_name ="http://artjcropprofile.s3.amazonaws.com/";
					}
					if($exp_creator_info[0]=="general_community"){$id_name ="community_id";
					$image_name ="http://comjcropprofile.s3.amazonaws.com/";
					}
					if($exp_creator_info[0]=="community_project" || $exp_creator_info[0]=="artist_project"){$id_name ="id";
					$image_name ="";
					}
					
					$get_img = mysql_query("select * from ".$exp_creator_info[0]." where ".$id_name."=".$exp_creator_info[1]."");
					if($get_img!="" && $get_img!=NULL)
					{
						if(mysql_num_rows($get_img)>0)
						{
							$res_img = mysql_fetch_assoc($get_img);
							$image_name = $image_name .$res_img['image_name'];
						}
					}
				}
				if($res_media_img['from_info'] =="" && $res_media_img['creator_info'] =="")
				{
					$get_gen_det = mysql_query("select * from general_user where general_user_id='".$res_media_img['general_user_id']."'");
					if($get_gen_det!="" && $get_gen_det!=NULL)
					{
						if(mysql_num_rows($get_gen_det)>0){
							$res_gen_det = mysql_fetch_assoc($get_gen_det);
							if($res_gen_det['artist_id']!="" && $res_gen_det['artist_id']!=0){
								$get_art_info = mysql_query("select * from general_artist where artist_id='".$res_gen_det['artist_id']."'");
								if($get_art_info!="" && $get_art_info!=NULL)
								{
									if(mysql_num_rows($get_art_info)>0){
										$image_name ="http://artjcropprofile.s3.amazonaws.com/";
										$res_art_info = mysql_fetch_assoc($get_art_info);
										$image_name = $image_name .$res_art_info['image_name'];
									}
								}
							}
							if($res_gen_det['community_id']!="" && $res_gen_det['community_id']!=0){
								$get_com_info = mysql_query("select * from general_community where community_id='".$res_gen_det['community_id']."'");
								if($get_com_info!="" && $get_com_info!=NULL)
								{
									if(mysql_num_rows($get_com_info)>0){
										$image_name ="http://comjcropprofile.s3.amazonaws.com/";
										$res_com_info = mysql_fetch_assoc($get_com_info);
										$image_name = $image_name .$res_com_info['image_name'];
									}
								}
							}
						}
					}
				}
			}
		}
		return($image_name);
	}
	function getvideoimage($media_id)
	{
		$sql = mysql_query("select * from media_video where media_id ='".$media_id."'");
		if($sql!="" && $sql!=NULL)
		{
			if(mysql_num_rows($sql)>0)
			{
				$res = mysql_fetch_assoc($sql);
				preg_match("#(?<=v=)[a-zA-Z0-9-]+(?=&)|(?<=v\/)[^&\n]+(?=\?)|(?<=v=)[^&\n]+|(?<=youtu.be/)[^&\n]+#", $res['videolink'], $matches);
				$videoId = $matches[0];
				return($videoId);
			}
		}
	}
	function get_gallery_info2($id){
		$query = mysql_query("select * from media_images where media_id='".$id."'");
		if($query!="" && $query!=NULL)
		{
			if(mysql_num_rows($query)>0){
			$res = mysql_fetch_assoc($query);
			}
		}
		return($res);
	}
}


?>