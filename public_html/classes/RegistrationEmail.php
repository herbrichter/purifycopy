<?php
	class RegistrationEmail
	{
		function sendArtist()
		{
			$sendgrid = new SendGrid('','');
			$this->from="web@purifyentertainment.net";
			$this->subject="Thank you for registering with Purify Entertainment!";
			$this->header="From: Purify Entertainment <".$this->from.">";
			$this->msg=<<<EOD
Hello $this->name,
	
Thank you for registering with Purify Entertainment. Follow the link below to verify your email address.

$this->elink 

After your account has been activated we will email you with notification that your account is ready for login.

Username: $this->username
Reference no.: $this->ref_no

Refer new users and receive Purify Points. Have your friends and family enter your reference number when registering or purchasing a membership. For every Member you refer, you will receive 10 Points in your account.

------------------------------------------------------------------------------------------------------------------------------------------
Purify Entertainment
EOD;
			$url = "../admin/newsletter/";  
			$myFile = $url . "other_templates.txt";
			$fh = fopen($myFile, 'r');
			$theData = fread($fh, filesize($myFile));
			fclose($fh);
			
			$data_email = explode('|~|', $theData);
			
			$ns_user_n = str_replace('{::message::}', $this->msg, $data_email['1']);
			
			$body_email = $data_email['0'] . $ns_user_n . $data_email['2'];
			
			$mail_grid = new SendGrid\Mail();
			
			$mail_grid->addTo($this->recipient)->
					setFromName('Purify Art')->
					setFrom('no-reply@purifyart.com')->
					setSubject($this->subject)->
					setHtml($body_email);
					
			//$sendgrid->web->send($mail_grid);
			$sendgrid -> smtp -> send($mail_grid);
			
			//mail($this->recipient,$this->subject,$this->msg,$this->header);
		}
		
		function sendCommunity()
		{
			$sendgrid = new SendGrid('','');
			$this->from="web@purifyentertainment.net";
			$this->subject="Thank you for registering with Purify Entertainment!";
			$this->header="From: Purify Entertainment <".$this->from.">";
			$this->msg=<<<EOD
Hello $this->name,
	
Thank you for registering with Purify Entertainment. Follow the link below to verify your email address.

$this->elink

After your account has been activated we will email you with notification that your account is ready for login.

Username: $this->username
Reference no.: $this->ref_no

Refer new users and receive Purify Points. Have your friends and family enter your reference number when registering or purchasing a membership. For every Member you refer, you will receive 10 Points in your account.

------------------------------------------------------------------------------------------------------------------------------------------
Purify Entertainment
EOD;
			$url = "../admin/newsletter/";  
			$myFile = $url . "other_templates.txt";
			$fh = fopen($myFile, 'r');
			$theData = fread($fh, filesize($myFile));
			fclose($fh);
			
			$data_email = explode('|~|', $theData);
			
			$ns_user_n = str_replace('{::message::}', $this->msg, $data_email['1']);
			
			$body_email = $data_email['0'] . $ns_user_n . $data_email['2'];
			
			$mail_grid = new SendGrid\Mail();
			
			$mail_grid->addTo($this->recipient)->
					setFromName('Purify Art')->
					setFrom('no-reply@purifyart.com')->
					setSubject($this->subject)->
					setHtml($body_email);
					
			//$sendgrid->web->send($mail_grid);
			$sendgrid -> smtp -> send($mail_grid);
			
			//mail($this->recipient,$this->subject,$this->msg,$this->header);
		}
		
		function sendTeam()
		{
			$sendgrid = new SendGrid('','');
			$this->from="web@purifyentertainment.net";
			$this->subject="Thank you for registering with Purify Entertainment!";
			$this->header="From: Purify Entertainment <".$this->from.">";
			/*switch($category_id)
			{
				case 1:	$this->category="Director";
						break;
				case 2: $this->category="Team Programmer";
						break;
				case 3: $this->category="Team Promoter";
						break;
			}*/
			$this->msg=<<<EOD
Hello $this->name,
	
Thank you for registering with Purify Entertainment. Follow the link below to verify your email address.

$this->elink 

After your account has been activated we will email you with notification that your account is ready for login.

Username: $this->username
Reference no.: $this->ref_no

Refer new users and receive Purify Points. Have your friends and family enter your reference number when registering or purchasing a membership. For every Member you refer, you will receive 10 Points in your account.

------------------------------------------------------------------------------------------------------------------------------------------
Purify Entertainment
EOD;
			$url = "../admin/newsletter/";  
			$myFile = $url . "other_templates.txt";
			$fh = fopen($myFile, 'r');
			$theData = fread($fh, filesize($myFile));
			fclose($fh);
			
			$data_email = explode('|~|', $theData);
			
			$ns_user_n = str_replace('{::message::}', $this->msg, $data_email['1']);
			
			$body_email = $data_email['0'] . $ns_user_n . $data_email['2'];
			
			$mail_grid = new SendGrid\Mail();
			
			$mail_grid->addTo($this->recipient)->
					setFromName('Purify Art')->
					setFrom('no-reply@purifyart.com')->
					setSubject($this->subject)->
					setHtml($body_email);
					
			//$sendgrid->web->send($mail_grid);
			$sendgrid -> smtp -> send($mail_grid);
			
			//mail($this->recipient,$this->subject,$this->msg,$this->header);
		}
		
		function sendFan($subscription_id)
		{
			$sendgrid = new SendGrid('','');
			$this->from="web@purifyentertainment.net";
			$this->subject="Thank you for registering with Purify Entertainment!";
			$this->header="From: Purify Entertainment <".$this->from.">";
			switch($subscription_id)
			{
				case 1:	$this->subscription="Purify Founder";
						break;
				case 2: $this->subscription="Life Time Subscriber";
						break;
				case 3: $this->subscription="Purify Fan";
						break;
			}
			$this->msg=<<<EOD
Hello $this->name,
	
Thank you for registering with Purify Entertainment. Follow the link below to verify your email address.

$this->elink 

After your account has been activated we will email you with notification that your account is ready for login.

Username: $this->username
Reference no.: $this->ref_no

Refer new users and receive Purify Points. Have your friends and family enter your reference number when registering or purchasing a membership. For every Member you refer, you will receive 10 Points in your account.

------------------------------------------------------------------------------------------------------------------------------------------
Purify Entertainment
EOD;
			$url = "../admin/newsletter/";  
			$myFile = $url . "other_templates.txt";
			$fh = fopen($myFile, 'r');
			$theData = fread($fh, filesize($myFile));
			fclose($fh);
			
			$data_email = explode('|~|', $theData);
			
			$ns_user_n = str_replace('{::message::}', $this->msg, $data_email['1']);
			
			$body_email = $data_email['0'] . $ns_user_n . $data_email['2'];
			
			$mail_grid = new SendGrid\Mail();
			
			$mail_grid->addTo($this->recipient)->
					setFromName('Purify Art')->
					setFrom('no-reply@purifyart.com')->
					setSubject($this->subject)->
					setHtml($body_email);
					
			//$sendgrid->web->send($mail_grid);
			$sendgrid -> smtp -> send($mail_grid);
			
			//mail($this->recipient,$this->subject,$this->msg,$this->header);
		}
	}
?>
