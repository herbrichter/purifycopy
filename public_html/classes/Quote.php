<?php
include_once('commons/db.php');

class Quote
{
	function addQuote($quote_para,$source,$source_link,$author,$author_link)
	{
		$posted_date= date("F j, Y");
		$sql2="INSERT INTO quote (`quote_para`,`source`,`source_link`,`author`,`author_link`,`posted_date`,`status`) VALUES ('$quote_para','$source','$source_link','$author','$author_link','$posted_date','0')";
		$rs2=mysql_query($sql2) or die(mysql_error());
		return mysql_insert_id();
	}

	function getRandomQuote()
	{
		$sql="SELECT * FROM quote WHERE status='1' ORDER BY rand()";
		$rs=mysql_query($sql) or die(mysql_error());
		$row=mysql_fetch_assoc($rs);
		return($row);
	}
	
	function getQuote()
	{
		$sql="SELECT * FROM quote WHERE status='1' ORDER BY quote_id DESC";
		$rs=mysql_query($sql) or die(mysql_error());
		return($rs);	
	}
	
	function notifyAdmin()
	{
		$this->recipient="purifyentertainment@gmail.com";
		$this->subject="New quote suggested.";
		$this->header="From: purifyentertainment.net";
		$this->msg=<<<EOD

Hello Administrator,
--------------------------
You have a new suggested Quote. Kindly login to accept/reject it.
Thanks.
------------------------------------------------------------------------------------------------------------------------------------------
EOD;
			
		mail($this->recipient,$this->subject,$this->msg,$this->header);		
	}
}
?>