<?php
error_reporting(0);
class DisplayStatistics
{
	function get_gen_info()
	{
		$get_user = mysql_query("select * from general_user where email='".$_SESSION['login_email']."' AND delete_status=0");
		if($get_user!="" && $get_user!=Null)
		{
			if(mysql_num_rows($get_user)>0)
			{
				$res_user = mysql_fetch_assoc($get_user);
				return($res_user);
			}
		}else{
			$res_user =Array();
			return($res_user);
		}
	}
	
	function get_art_info()
	{
		$row = $this->get_gen_info();
		if(!empty($row) && $row['artist_id']!=0)
		{
			$get_user = mysql_query("select * from general_artist where artist_id='".$row['artist_id']."' AND del_status=0");
			if($get_user!="" && $get_user!=Null)
			{
				if(mysql_num_rows($get_user)>0)
				{
					$res_user = mysql_fetch_assoc($get_user);
					return($res_user);
				}
			}else{
				$res_user =Array();
				return($res_user);
			}
		}
	}
	
	function get_com_info()
	{
		$row = $this->get_gen_info();
		if(!empty($row) && $row['community_id']!=0)
		{
			$get_user = mysql_query("select * from general_community where community_id='".$row['community_id']."' AND del_status=0");
			if($get_user!="" && $get_user!=Null)
			{
				if(mysql_num_rows($get_user)>0)
				{
					$res_user = mysql_fetch_assoc($get_user);
					return($res_user);
				}
			}else{
				$res_user =Array();
				return($res_user);
			}
		}
	}
	
	function get_contacts()
	{
		$row = $this->get_gen_info();
		if(!empty($row))
		{
			$get_cont = mysql_query("SELECT * FROM add_address_book WHERE general_user_id='".$row['general_user_id']."' AND del_status=0");
			if($get_cont!="" && $get_cont!=Null)
			{
				if(mysql_num_rows($get_cont)>0)
				{
					while($row_c = mysql_fetch_assoc($get_cont))
					{
						$ex_uid[] = $row_c['email'];
					}
				}
			}
				
			//$ex_uid=explode(',',$uid);
			
			if(isset($ex_uid) && !empty($ex_uid))
			{
				$new_array = array();
				foreach ($ex_uid as $key => $value)
				{
					if(isset($new_array[$value]))
						$new_array[$value] += 1;
					else
						$new_array[$value] = 1;
				}
				foreach ($new_array as $uid => $n)
				{
					$ex_uid1=$ex_uid1.','.$uid;
				}
				
				$ex_uid = explode(',',$ex_uid1);
				$count = count($ex_uid);
				return $count;
			}
		}
	}
	
	function get_friends()
	{
		$row = $this->get_gen_info();
		$count = 0;
		if(!empty($row))
		{
			$get_cont = mysql_query("SELECT * FROM friends WHERE general_user_id='".$row['general_user_id']."' AND status=0");
			if($get_cont!="" && $get_cont!=Null)
			{
				if(mysql_num_rows($get_cont)>0)
				{
					while($ans_cont = mysql_fetch_assoc($get_cont))
					{
						$sql_gens = mysql_query("SELECT * FROM general_user WHERE general_user_id='".$ans_cont['fgeneral_user_id']."'");
						$ans_gens = mysql_fetch_assoc($sql_gens);
						if($ans_gens['artist_id']!=0)
						{
							$sql_art = mysql_query("SELECT * FROM general_artist WHERE artist_id='".$ans_gens['artist_id']."' AND status=0 AND active=0 AND del_status=0");
							if(mysql_num_rows($sql_art)>0)
							{
								$count = $count + 1;
							}
						}
						if($ans_gens['community_id']!=0)
						{
							$sql_com = mysql_query("SELECT * FROM general_community WHERE community_id='".$ans_gens['community_id']."' AND status=0 AND active=0 AND del_status=0");
							if(mysql_num_rows($sql_com)>0)
							{
								$count = $count + 1;
							}
						}
					}
					//$count = mysql_num_rows($get_cont);
					return $count;
				}
			}else{
				$count ="";
				return $count;
			}
		}
	}
	
	function get_memberships()
	{
		$exp_date = date('Y-m-d');
		$row = $this->get_gen_info();
		if(!empty($row))
		{
			$get_cont = mysql_query("SELECT * FROM fan_club_membership WHERE email='".$_SESSION['login_email']."' AND del_status=0 AND expiry_date>'".$exp_date."'");
			if($get_cont!="" && $get_cont!=Null)
			{
				if(mysql_num_rows($get_cont)>0)
				{
					while($res_cont = mysql_fetch_assoc($get_cont))
					{
						$res_count['count'] = $res_count['count'] + 1;
						$get_fan_price = mysql_query("select * from add_to_cart_fan_pro where fan_member_id='".$res_cont['id']."'");
						if(mysql_num_rows($get_fan_price)>0){
							$res_fan_price = mysql_fetch_assoc($get_fan_price);
							$res_count['price'] = $res_count['price'] + $res_fan_price['price'];
						}
					}
				}
			}else{
				$res_count ="";
			}
		}
		return $res_count;
	}
	
	function get_subscriptions()
	{
		$exp_date = date('Y-m-d');
		$row = $this->get_gen_info();
		if(!empty($row))
		{
			$get_cont = mysql_query("SELECT * FROM  email_subscription WHERE email='".$_SESSION['login_email']."' AND delete_status=1 AND expiry_date>'".$exp_date."'");
			if($get_cont!="" && $get_cont!=Null)
			{
				if(mysql_num_rows($get_cont)>0)
				{
					$count = mysql_num_rows($get_cont);
					return $count;
				}
			}else{	
				$count =""; 
				return $count;
			}
		}
	}
	
	function get_art_subscriptions()
	{
		$exp_date = date('Y-m-d');
		$row = $this->get_gen_info();
		//if($row!="" && $row!=NULL)
		if(!empty($row))
		{
			$get_cont = mysql_query("SELECT * FROM  email_subscription WHERE email='".$_SESSION['login_email']."' AND delete_status=1 AND expiry_date>'".$exp_date."' AND related_type='artist'");
			if($get_cont!="" && $get_cont!=Null)
			{
				if(mysql_num_rows($get_cont)>0)
				{
					$count = mysql_num_rows($get_cont);
					return $count;
				}
			}else{
				$count ="";
				return $count;
			}
		}
	}
	
	function get_com_subscriptions()
	{
		$exp_date = date('Y-m-d');
		$row = $this->get_gen_info();
		if(!empty($row))
		{
			$get_cont = mysql_query("SELECT * FROM  email_subscription WHERE email='".$_SESSION['login_email']."' AND delete_status=1 AND expiry_date>'".$exp_date."' AND related_type='community'");
			if($get_cont!="" && $get_cont!=Null)
			{
				if(mysql_num_rows($get_cont)>0)
				{
					$count = mysql_num_rows($get_cont);
					return $count;
				}
			}else{
				$count ="";
				return $count;
			}
		}
	}
	
	function get_pro_subscriptions()
	{
		$exp_date = date('Y-m-d');
		$row = $this->get_gen_info();
		if(!empty($row))
		{
			$get_cont = mysql_query("SELECT * FROM  email_subscription WHERE email='".$_SESSION['login_email']."' AND delete_status=1 AND expiry_date>'".$exp_date."' AND (related_type='community_project' OR related_type='artist_project')");
			if($get_cont!="" && $get_cont!=Null)
			{
				if(mysql_num_rows($get_cont)>0)
				{
					$count = mysql_num_rows($get_cont);
					return $count;
				}
			}else{
				$count =""; 
				return $count;
			}
		}
	}
	
	function get_eve_subscriptions()
	{
		$exp_date = date('Y-m-d');
		$row = $this->get_gen_info();
		if(!empty($row))
		{
			$get_cont = mysql_query("SELECT * FROM  email_subscription WHERE email='".$_SESSION['login_email']."' AND delete_status=1 AND expiry_date>'".$exp_date."' AND (related_type='community_event' OR related_type='artist_event')");
			if($get_cont!="" && $get_cont!=Null)
			{
				if(mysql_num_rows($get_cont)>0)
				{
					$count = mysql_num_rows($get_cont);
					return $count;
				}
			}else{
				$count =""; 
				return $count;
			}
		}
	}
	
	function get_artist_eve()
	{
		$row = $this->get_gen_info();
		if(!empty($row))
		{
			$get_cont = mysql_query("SELECT * FROM artist_event WHERE artist_id='".$row['artist_id']."' AND del_status=0");
			if($get_cont!="" && $get_cont!=Null)
			{
				if(mysql_num_rows($get_cont)>0)
				{
					$count = mysql_num_rows($get_cont);
					return $count;
				}
			}else{
				$count ="";
				return $count;
			}
		}
	}
	
	function get_artist_pro()
	{
		$row = $this->get_gen_info();
		if(!empty($row))
		{
			$get_cont = mysql_query("SELECT * FROM artist_project WHERE artist_id='".$row['artist_id']."' AND del_status=0");
			if($get_cont!="" && $get_cont!=Null)
			{
				if(mysql_num_rows($get_cont)>0)
				{
					$count = mysql_num_rows($get_cont);
					return $count;
				}
			}else{
				$count ="";
				return $count;
			}
		}
	}
	
	function get_community_eve()
	{
		$row = $this->get_gen_info();
		if(!empty($row))
		{
			$get_cont = mysql_query("SELECT * FROM community_event WHERE community_id='".$row['community_id']."' AND del_status=0");
			if($get_cont!="" && $get_cont!=Null)
			{
				if(mysql_num_rows($get_cont)>0)
				{
					$count = mysql_num_rows($get_cont);
					return $count;
				}
			}else{
				$count ="";
				return $count;
			}
		}
	}
	
	function get_community_pro()
	{
		$row = $this->get_gen_info();
		if(!empty($row))
		{
			$get_cont = mysql_query("SELECT * FROM community_project WHERE community_id='".$row['community_id']."' AND del_status=0");
			if($get_cont!="" && $get_cont!=Null)
			{
				if(mysql_num_rows($get_cont)>0)
				{
					$count = mysql_num_rows($get_cont);
					return $count;
				}
			}else{
				$count ="" ;
				return $count;
			}
		}
	}
	
	function get_allevents($type)
	{
		if($type=='artist')
		{
			$sql_all = mysql_query("SELECT * FROM artist_event WHERE del_status=0");
		}
		elseif($type=='community')
		{
			$sql_all = mysql_query("SELECT * FROM community_event WHERE del_status=0");
		}
		if($sql_all!="" && $sql_all!=Null)
		{
			if(mysql_num_rows($sql_all)>0)
			{
				$count = mysql_num_rows($sql_all);
				return $count;
			}
		}else{
			$count ="";
			return $count;
		}
	}
	
	function get_allprojects($type)
	{
		if($type=='artist')
		{
			$sql_all = mysql_query("SELECT * FROM artist_project WHERE del_status=0");
		}
		elseif($type=='community')
		{
			$sql_all = mysql_query("SELECT * FROM community_project WHERE del_status=0");
		}
		if($sql_all!="" && $sql_all!=Null)
		{
			if(mysql_num_rows($sql_all)>0)
			{
				$count = mysql_num_rows($sql_all);
				return $count;
			}
		}else{
			$count ="";
			return $count;
		}
	}
	
	function get_all_media()
	{
		$row = $this->get_gen_info();
		$all_media = 0;
		$count_tag = 0;
		$sql = mysql_query("select count(*) from general_media where general_user_id='".$row['general_user_id']."' AND delete_status=0");
		if($sql!="" && $sql!=Null)
		{
			if(mysql_num_rows($sql)>0)
			{
				$get_media = mysql_fetch_assoc($sql);
				$all_media = $get_media['count(*)'];
			}
		}
		$query = mysql_query("select * from general_media where delete_status=0");
		if($query!="" && $query!=Null)
		{
			if(mysql_num_rows($query)>0)
			{
				while($get_data = mysql_fetch_assoc($query))
				{
					$exp_email = explode(",",$get_data['tagged_user_email']);
					for($count_tag_email=0;$count_tag_email<count($exp_email);$count_tag_email++)
					{
						if($exp_email[$count_tag_email]==""){continue;}
						else{
							if($exp_email[$count_tag_email]==$_SESSION['login_email'])
							{
								$count_tag +=1;  
							}
						}
					}
				}
			}
		}
		return ($count_tag + $all_media);
	}
	
	function get_all_media_stored_onpurify()
	{
		$row = $this->get_gen_info();
		$count_sound_cloud = 0;
		$get_media = mysql_query("select * from general_media where general_user_id='".$row['general_user_id']."' AND delete_status=0 AND media_status=0");
		if($get_media!="" && $get_media!=Null)
		{
			if(mysql_num_rows($get_media)>0)
			{
				while($get_data = mysql_fetch_assoc($get_media))
				{
					if($get_data['media_type']==114)
					{
						$get_sound = mysql_query("select * from media_songs where media_id='".$get_data['id']."'");
						if($get_sound!="" && $get_sound!=Null)
						{
							if(mysql_num_rows($get_sound)>0)
							{
								$res_sound = mysql_fetch_assoc($get_sound);
								if($res_sound['song_name'] !="" && ($res_sound['audiolinkname'] =="" || $res_sound['audiolinkurl']==""))
								{
									$count_sound['count'] +=1;
									if($get_data['sharing_preference']!=5){
										$count_sound['size'] = $count_sound['size'] + $res_sound['size'];
									}
								}
							}
						}
					}
					if($get_data['media_type']==115)
					{
						$get_video = mysql_query("select * from media_video where media_id='".$get_data['id']."'");
						if($get_video!="" && $get_video!=Null)
						{
							if(mysql_num_rows($get_video)>0)
							{
								$res_video = mysql_fetch_assoc($get_video);
								if($res_video['video_name'] !="" && ($res_video['videolink'] =="" || $res_video['videotitle']==""))
								{
									$count_sound['count'] +=1;
									if($get_data['sharing_preference']!=5){
										$count_sound['size'] = $count_sound['size'] + $res_video['size'];
									}
								}
							}
						}
					}
					if($get_data['media_type']==113)
					{
						$get_images = mysql_query("select * from media_images where media_id='".$get_data['id']."'");
						if($get_images!="" && $get_images!=Null)
						{
							if(mysql_num_rows($get_images)>0)
							{
								if($get_data['sharing_preference']!=5){
									while($res_images = mysql_fetch_assoc($get_images)){
										$count_sound['size'] = $count_sound['size'] + $res_images['size'];
									}
								}
								$count_sound['count'] +=1;
							}
						}
					}
				}
			}
		}
		return($count_sound);
	}
	
	function get_soundcloud_media()
	{
		$row = $this->get_gen_info();
		$count_sound_cloud = 0;
		$get_media = mysql_query("select * from general_media where general_user_id='".$row['general_user_id']."' AND delete_status=0");
		if($get_media!="" && $get_media!=Null)
		{
			if(mysql_num_rows($get_media)>0)
			{
				while($get_data = mysql_fetch_assoc($get_media))
				{
					if($get_data['media_type']==114)
					{
						$get_sound = mysql_query("select * from media_songs where media_id='".$get_data['id']."'");
						if($get_sound!="" && $get_sound!=Null)
						{
							if(mysql_num_rows($get_sound)>0)
							{
								$res_sound = mysql_fetch_assoc($get_sound);
								if($res_sound['song_name'] =="" &&($res_sound['audiolinkname'] !="" || $res_sound['audiolinkurl']!=""))
								{
									$count_sound_cloud +=1;
								}
							}
						}
					}
				}
			}
		}
		return($count_sound_cloud);
	}
	
	function get_youtube_media()
	{
		$row = $this->get_gen_info();
		$count_youtube_media = 0;
		$get_media = mysql_query("select * from general_media where general_user_id='".$row['general_user_id']."' AND delete_status=0");
		if($get_media!="" && $get_media!=Null)
		{
			if(mysql_num_rows($get_media)>0)
			{
				while($get_data = mysql_fetch_assoc($get_media))
				{
					if($get_data['media_type']==115)
					{
						$get_video = mysql_query("select * from media_video where media_id='".$get_data['id']."'");
						if($get_video!="" && $get_video!=Null)
						{
							if(mysql_num_rows($get_video)>0)
							{
								$res_video = mysql_fetch_assoc($get_video);
								if($res_video['video_name'] =="" &&($res_video['videolink'] !="" || $res_video['videotitle']!=""))
								{
									$count_youtube_media +=1;
								}
							}
						}
					}
				}
			}
		}
		return($count_youtube_media);
	}
	
	function get_artist_fanclub()
	{
		$exp_date = date('Y-m-d');
		$row = $this->get_gen_info();
		$get_data = mysql_query("select * from fan_club_membership where related_id='".$row['artist_id']."' AND expiry_date>'".$exp_date."' AND accept=1");
		if($get_data!="" && $get_data!=Null)
		{
			if(mysql_num_rows($get_data)>0)
			{
				while($res_data = mysql_fetch_assoc($get_data)){
					$get_art_count['count'] = $get_art_count['count'] + 1;
					$get_price = mysql_query("select * from general_artist where artist_id='".$row['artist_id']."'");
					if($get_price!="" && $get_price!=Null)
					{
						if(mysql_num_rows($get_price)>0){
							$res_price = mysql_fetch_assoc($get_price);
							$get_art_count['price'] = $res_price['price'];
						}
					}
					/*$get_price = mysql_query("select * from add_to_cart_fan_pro where fan_member_id='".$res_data['id']."'");
					if(mysql_num_rows($get_price)>0){
						$res_price = mysql_fetch_assoc($get_price);
						$get_art_count['price'] = $get_art_count['price'] + $res_price['price'];
					}*/
				}
			}
		}else{
			$get_art_count=array();
		}
		/*$get_data = mysql_query("select count(*) from fan_club_membership where related_id='".$row['artist_id']."' AND expiry_date>'".$exp_date."'");
		if(mysql_num_rows($get_data)>0)
		{
			$res_data = mysql_fetch_assoc($get_data);
			$get_art_count = $res_data['count(*)'];
		}
		$get_project = mysql_query("select * from fan_club_membership where related_type='artist_project' AND expiry_date>'".$exp_date."'");
		if(mysql_num_rows($get_project)>0)
		{
			while($res_project = mysql_fetch_assoc($get_project))
			{
				$exp_id = explode("_",$res_project['related_id']);
				if($exp_id[0] == $row['artist_id'])
				{
					$get_project_count +=1;
				}
			}
		}
		return($get_art_count + $get_project_count);*/
		return($get_art_count);
	}
	
	function get_all_artist_fanclub()
	{
		$exp_date = date('Y-m-d');
		$row = $this->get_gen_info();
		$get_data = mysql_query("select * from fan_club_membership where related_id='".$row['artist_id']."' AND accept=1");
		if($get_data!="" && $get_data!=Null)
		{
			if(mysql_num_rows($get_data)>0)
			{
				while($res_data = mysql_fetch_assoc($get_data)){
					$get_art_count['count'] = $get_art_count['count'] + 1;
					$get_price = mysql_query("select * from add_to_cart_fan_pro where fan_member_id='".$res_data['id']."'");
					if($get_price!="" && $get_price!=Null)
					{
						if(mysql_num_rows($get_price)>0){
							$res_price = mysql_fetch_assoc($get_price);
							$get_art_count['price'] = $get_art_count['price'] + $res_price['price'];
						}
					}
				}
			}
		}else{
			$get_art_count =array();
		}
		/*$get_data = mysql_query("select count(*) from fan_club_membership where related_id='".$row['artist_id']."'");
		if(mysql_num_rows($get_data)>0)
		{
			$res_data = mysql_fetch_assoc($get_data);
			$get_art_count = $res_data['count(*)'];
		}
		$get_project = mysql_query("select * from fan_club_membership where related_type='artist_project' ");
		if(mysql_num_rows($get_project)>0)
		{
			while($res_project = mysql_fetch_assoc($get_project))
			{
				$exp_id = explode("_",$res_project['related_id']);
				if($exp_id[0] == $row['artist_id'])
				{
					$get_project_count +=1;
				}
			}
		}
		return($get_art_count + $get_project_count);*/
		return($get_art_count);
	}
	
	/*function get_all_artist_event()
	{
		$row = $this->get_gen_info();
		$count_tag_event = "";
		$total_event = "";
		$send[0]="";
		$send[1]="";
		$get_event = mysql_query("select count(*) from artist_event where artist_id='".$row['artist_id']."' AND del_status=0");
		if(mysql_num_rows($get_event)>0)
		{
			$res_event = mysql_fetch_assoc($get_event);
			$total_event = $res_event['count(*)'];
		}
		$get_tag_event = mysql_query("select * from artist_event");
		if(mysql_num_rows($get_tag_event)>0)
		{
			while($res_tag_event = mysql_fetch_assoc($get_tag_event))
			{
				$exp_email = explode(",",$res_tag_event['tagged_user_email']);
				for($count_email =0;$count_email<count($exp_email);$count_email++)
				{
					if($exp_email[$count_email]==""){continue;}
					else
					{
						if($exp_email[$count_email] == $_SESSION['login_email'])
						{
							$count_tag_event +=1;
						}
					}
					
				}
			}
		}
		$send[0] = ($count_tag_event + $total_event);
		$send[1] = $total_event;
		return($send);
	}*/
	function get_all_artist_event_createdbyme()
	{
		$row = $this->get_gen_info();
		$get_event = mysql_query("select count(*) from artist_event where artist_id='".$row['artist_id']."' AND del_status=0 AND active=0");
		if($get_event!="" && $get_event!=Null)
		{
			if(mysql_num_rows($get_event)>0)
			{
				$res_event = mysql_fetch_assoc($get_event);
				$total_event = $res_event['count(*)'];
			}
			return($total_event);
		}else{
			$total_event = Array();
			return($total_event);
		}
	}
	
	function get_all_artist_project_createdbyme()
	{
		$row = $this->get_gen_info();
		$get_event = mysql_query("select count(*) from artist_project where artist_id='".$row['artist_id']."' AND del_status=0 AND active=0");
		if($get_event!="" && $get_event!=Null)
		{
			if(mysql_num_rows($get_event)>0)
			{
				$res_event = mysql_fetch_assoc($get_event);
				$total_event = $res_event['count(*)'];
			}
			return($total_event);
		}else{
			$total_event =Array();
			return($total_event);
		}	
	}
	
	/*function get_all_artist_project()
	{
		$row = $this->get_gen_info();
		$count_tag_project = "";
		$total_project = "";
		$send[0]="";
		$send[1]="";
		$get_project = mysql_query("select count(*) from artist_project where artist_id='".$row['artist_id']."' AND del_status=0");
		if(mysql_num_rows($get_project)>0)
		{
			$res_project = mysql_fetch_assoc($get_project);
			$total_project = $res_project['count(*)'];
		}
		$get_tag_project = mysql_query("select * from artist_project");
		if(mysql_num_rows($get_tag_project)>0)
		{
			while($res_tag_project = mysql_fetch_assoc($get_tag_project))
			{
				$exp_email = explode(",",$res_tag_project['tagged_user_email']);
				for($count_email =0;$count_email<count($exp_email);$count_email++)
				{
					if($exp_email[$count_email]==""){continue;}
					else
					{
						if($exp_email[$count_email] == $_SESSION['login_email'])
						{
							$count_tag_project +=1;
						}
					}
					
				}
			}
		}
		$send[0] = ($count_tag_project + $total_project);
		$send[1] = $total_project;
		return($send);
	}*/
	
	function get_community_fanclub()
	{
		$exp_date = date('Y-m-d');
		$row = $this->get_gen_info();
		$get_data = mysql_query("select * from fan_club_membership where related_id='".$row['community_id']."' AND expiry_date>'".$exp_date."' AND accept=1");
		if($get_data!="" && $get_data!=Null)
		{
			if(mysql_num_rows($get_data)>0)
			{
				while($res_data = mysql_fetch_assoc($get_data)){
					$get_art_count['count'] = $get_art_count['count'] + 1;
					$get_price = mysql_query("select * from general_community where community_id='".$row['community_id']."'");
					if($get_price!="" && $get_price!=Null)
					{
						if(mysql_num_rows($get_price)>0){
							$res_price = mysql_fetch_assoc($get_price);
							$get_art_count['price'] = $res_price['price'];
						}
					}
					/*$get_price = mysql_query("select * from add_to_cart_fan_pro where fan_member_id='".$res_data['id']."'");
					if(mysql_num_rows($get_price)>0){
						$res_price = mysql_fetch_assoc($get_price);
						$get_art_count['price'] = $get_art_count['price'] + $res_price['price'];
					}*/
				}
			}
		}else{
			$get_art_count =Array();
		}
		
		/*$get_data = mysql_query("select count(*) from fan_club_membership where related_id='".$row['artist_id']."' AND expiry_date>'".$exp_date."'");
		if(mysql_num_rows($get_data)>0)
		{
			$res_data = mysql_fetch_assoc($get_data);
			$get_art_count = $res_data['count(*)'];
		}
		$get_project = mysql_query("select * from fan_club_membership where related_type='artist_project' AND expiry_date>'".$exp_date."'");
		if(mysql_num_rows($get_project)>0)
		{
			while($res_project = mysql_fetch_assoc($get_project))
			{
				$exp_id = explode("_",$res_project['related_id']);
				if($exp_id[0] == $row['artist_id'])
				{
					$get_project_count +=1;
				}
			}
		}
		return($get_art_count + $get_project_count);*/
		return($get_art_count);
	}
	
	function get_all_community_fanclub()
	{
		$exp_date = date('Y-m-d');
		$row = $this->get_gen_info();
		$get_data = mysql_query("select * from fan_club_membership where related_id='".$row['community_id']."' AND accept=1");
		if($get_data!="" && $get_data!=Null)
		{
			if(mysql_num_rows($get_data)>0)
			{
				while($res_data = mysql_fetch_assoc($get_data)){
					$get_art_count['count'] = $get_art_count['count'] + 1;
					$get_price = mysql_query("select * from add_to_cart_fan_pro where fan_member_id='".$res_data['id']."'");
					if($get_price!="" && $get_price!=Null)
					{
						if(mysql_num_rows($get_price)>0){
							$res_price = mysql_fetch_assoc($get_price);
							$get_art_count['price'] = $get_art_count['price'] + $res_price['price'];
						}
					}
				}
			}
		}
		else{
			$get_art_count =Array();
		}
		/*$get_data = mysql_query("select count(*) from fan_club_membership where related_id='".$row['artist_id']."'");
		if(mysql_num_rows($get_data)>0)
		{
			$res_data = mysql_fetch_assoc($get_data);
			$get_art_count = $res_data['count(*)'];
		}
		$get_project = mysql_query("select * from fan_club_membership where related_type='artist_project' ");
		if(mysql_num_rows($get_project)>0)
		{
			while($res_project = mysql_fetch_assoc($get_project))
			{
				$exp_id = explode("_",$res_project['related_id']);
				if($exp_id[0] == $row['artist_id'])
				{
					$get_project_count +=1;
				}
			}
		}
		return($get_art_count + $get_project_count);*/
		return($get_art_count);
	}
	
	/*function get_all_community_event()
	{
		$row = $this->get_gen_info();
		$count_tag_event = "";
		$total_event = "";
		$send[0]="";
		$send[1]="";
		$get_event = mysql_query("select count(*) from community_event where community_id='".$row['community_id']."' AND del_status=0");
		if(mysql_num_rows($get_event)>0)
		{
			$res_event = mysql_fetch_assoc($get_event);
			$total_event = $res_event['count(*)'];
		}
		$get_tag_event = mysql_query("select * from community_event");
		if(mysql_num_rows($get_tag_event)>0)
		{
			while($res_tag_event = mysql_fetch_assoc($get_tag_event))
			{
				$exp_email = explode(",",$res_tag_event['tagged_user_email']);
				for($count_email =0;$count_email<count($exp_email);$count_email++)
				{
					if($exp_email[$count_email]==""){continue;}
					else
					{
						if($exp_email[$count_email] == $_SESSION['login_email'])
						{
							$count_tag_event +=1;
						}
					}
					
				}
			}
		}
		$send[0] = ($count_tag_event + $total_event);
		$send[1] = $total_event;
		return($send);
	}*/
	function get_all_community_event_createdbyme()
	{
		$row = $this->get_gen_info();
		$get_event = mysql_query("select count(*) from community_event where community_id='".$row['community_id']."' AND del_status=0 AND active=0");
		if($get_event!="" && $get_event!=Null)
		{
			if(mysql_num_rows($get_event)>0)
			{
				$res_event = mysql_fetch_assoc($get_event);
				$total_event = $res_event['count(*)'];
			}
			return($total_event);
		}else{
			$total_event =Array();
			return($total_event);
		}
	}
	
	function get_all_community_project_createdbyme()
	{
		$row = $this->get_gen_info();
		$get_event = mysql_query("select count(*) from community_project where community_id='".$row['community_id']."' AND del_status=0 AND active=0");
		if($get_event!="" && $get_event!=Null)
		{
			if(mysql_num_rows($get_event)>0)
			{
				$res_event = mysql_fetch_assoc($get_event);
				$total_event = $res_event['count(*)'];
			}
			return($total_event);
		}else{
			$total_event =Array();
			return($total_event);
		}
	}
	
	/*function get_all_community_project()
	{
		$row = $this->get_gen_info();
		$count_tag_project = "";
		$total_project = "";
		$send[0]="";
		$send[1]="";
		$get_project = mysql_query("select count(*) from community_project where community_id='".$row['community_id']."' AND del_status=0");
		if(mysql_num_rows($get_project)>0)
		{
			$res_project = mysql_fetch_assoc($get_project);
			$total_project = $res_project['count(*)'];
		}
		$get_tag_project = mysql_query("select * from community_project");
		if(mysql_num_rows($get_tag_project)>0)
		{
			while($res_tag_project = mysql_fetch_assoc($get_tag_project))
			{
				$exp_email = explode(",",$res_tag_project['tagged_user_email']);
				for($count_email =0;$count_email<count($exp_email);$count_email++)
				{
					if($exp_email[$count_email]==""){continue;}
					else
					{
						if($exp_email[$count_email] == $_SESSION['login_email'])
						{
							$count_tag_project +=1;
						}
					}
					
				}
			}
		}
		$send[0] = ($count_tag_project + $total_project);
		$send[1] = $total_project;
		return($send);
	}*/
	
	function get_all_media_played()
	{
		$row = $this->get_gen_info();
		$all_media = 0;
		$count_tag = 0;
		$total_play_count = 0;
		$total_tag_play_count = 0;
		$creator_info = "general_artist|".$row['artist_id'];
		$get_play = mysql_query("select * from general_media where creator_info ='".$creator_info."' AND delete_status=0");
		if($get_play!="" && $get_play!=Null)
		{
			if(mysql_num_rows($get_play)>0)
			{
				while($result = mysql_fetch_assoc($get_play))
				{
					$total_play_count = $total_play_count + $result['play_count'];
				}
			}
		}
		$sql = mysql_query("select count(*) from general_media where creator_info ='".$creator_info."' AND delete_status=0");
		if($sql!="" && $sql!=Null)
		{
			if(mysql_num_rows($sql)>0)
			{
				$get_media = mysql_fetch_assoc($sql);
				$all_media = $get_media['count(*)'];
			}
		}
		$query = mysql_query("select * from general_media where delete_status=0");
		if($query!="" && $query!=Null)
		{
			if(mysql_num_rows($query)>0)
			{
				while($get_data = mysql_fetch_assoc($query))
				{
					$exp_email = explode(",",$get_data['tagged_user_email']);
					for($count_tag_email=0;$count_tag_email<count($exp_email);$count_tag_email++)
					{
						if($exp_email[$count_tag_email]==""){continue;}
						else{
							if($exp_email[$count_tag_email]==$_SESSION['login_email'])
							{
								$count_tag +=1;  
								$total_tag_play_count = $total_tag_play_count + $get_data['play_count'];
							}
						}
					}
				}
			}
		}
		$array_pass[0] = $all_media;
		$array_pass[1] = $total_tag_play_count + $total_play_count;
		return ($array_pass);
	}
	function get_all_media_played_community()
	{
		$row = $this->get_gen_info();
		$all_media = 0;
		$count_tag = 0;
		$total_play_count = 0;
		$total_tag_play_count = 0;
		$creator_info = "general_community|".$row['community_id'];
		$get_play = mysql_query("select * from general_media where creator_info='".$creator_info."' AND delete_status=0");
		if($get_play!="" && $get_play!=Null)
		{
			if(mysql_num_rows($get_play)>0)
			{
				while($result = mysql_fetch_assoc($get_play))
				{
					$total_play_count = $total_play_count + $result['play_count'];
				}
			}
		}
		$sql = mysql_query("select count(*) from general_media where creator_info='".$creator_info."' AND delete_status=0");
		if($sql!="" && $sql!=Null)
		{
			if(mysql_num_rows($sql)>0)
			{
				$get_media = mysql_fetch_assoc($sql);
				$all_media = $get_media['count(*)'];
			}
		}
		$query = mysql_query("select * from general_media where delete_status=0");
		if($query!="" && $query!=Null)
		{
			if(mysql_num_rows($query)>0)
			{
				while($get_data = mysql_fetch_assoc($query))
				{
					$exp_email = explode(",",$get_data['tagged_user_email']);
					for($count_tag_email=0;$count_tag_email<count($exp_email);$count_tag_email++)
					{
						if($exp_email[$count_tag_email]==""){continue;}
						else{
							if($exp_email[$count_tag_email]==$_SESSION['login_email'])
							{
								$count_tag +=1;  
								$total_tag_play_count = $total_tag_play_count + $get_data['play_count'];
							}
						}
					}
				}
			}
		}
		$array_pass[0] = $all_media;
		$array_pass[1] = $total_tag_play_count + $total_play_count;
		return ($array_pass);
	}
	
	function get_media_play_me()
	{
		$sql = mysql_query("select * from general_user where email ='".$_SESSION['login_email']."'");
		if($sql!="" && $sql!=Null)
		{
			$res = mysql_fetch_assoc($sql);
			return($res);
		}else{
			$res =Array();
			return($res);
		}
	}
	
	function get__subcs($ids)
	{
		if(!empty($ids))
		{
			$ids_exp = explode('~',$ids);
			$sql_subs = mysql_query("SELECT * FROM email_subscription WHERE related_id='".$ids_exp[0]."' AND related_type='".$ids_exp[1]."' AND delete_status=1");
			if($sql_subs!="" && $sql_subs!=Null)
			{
				if(mysql_num_rows($sql_subs)>0)
				{
					$counts = mysql_num_rows($sql_subs);
					return $counts;
				}
				else
				{
					return 0;
				}
			}
			else
			{
				return 0;
			}
		}
		else
		{
			return 0;
		}
	}
	
	function get__membs($ids)
	{
		if(!empty($ids))
		{
			$ids_exp = explode('~',$ids);
			$sql_subs = mysql_query("SELECT * FROM fan_club_membership WHERE related_id='".$ids_exp[0]."' AND related_type='".$ids_exp[1]."' AND del_status=0");
			if($sql_subs!="" && $sql_subs!=Null)
			{
				if(mysql_num_rows($sql_subs)>0)
				{
					$counts = mysql_num_rows($sql_subs);
					return $counts;
				}
				else
				{
					return 0;
				}
			}
			else
			{
				return 0;
			}
		}
		else
		{
			return 0;
		}
	}
	function getS3_account()
	{
		$res = $this->get_gen_info();
		$query = mysql_query("select * from add_user_s3 where general_user_id='".$res['general_user_id']."'");
		if($query!="" && $query!=Null)
		{
			if(mysql_num_rows($query)>0)
			{
				$row = mysql_fetch_assoc($query);
				return($row);
			}else
			{
				return("blank");
			}
		}else
		{
			return("blank");
		}
		
	}
	
	/** Code fro displaying events **/
	function getEventUpcoming()
	{
		$date=date("Y-m-d");
		$new_id=$this->get_gen_info();
		$getproject=mysql_query("select * from artist_event where artist_id='".$new_id['artist_id']."' AND date>='".$date."' AND del_status=0 AND active=0");
		return ($getproject);
	}
	function only_creator_oupevent()
	{
		$id = $this->get_gen_info();
		$date = date("Y-m-d");
		
		$var_type_1 = 'general_artist';
		$var_type_2 = 'general_community';
		$var_type_3 = 'artist_project';
		$var_type_4 = 'community_project';
		
		$artm = array();
		$arta = array();
		$comm = array();
		$coma = array();
		
		$artme = array();
		$artae = array();
		$comme = array();
		$comae = array();
		
		$art_p_ao = array();
		$com_p_ao = array();
		$art_p_co = array();
		$com_p_co = array();
		
		$art_e_ao = array();
		$com_e_ao = array();
		$art_e_co = array();
		$com_e_co = array();
		
		$pal_1 = array();
		$pal_2 = array();
		
		if($id['artist_id']!=0 && !empty($id['artist_id']))
		{
			$chk_type = $var_type_1.'|'.$id['artist_id'];
			
			$sql_get_allp = mysql_query("SELECT * FROM artist_project WHERE artist_id='".$id['artist_id']."' AND del_status=0 AND active=0");
			if($sql_get_allp!="" && $sql_get_allp!=Null)
			{
				if(mysql_num_rows($sql_get_allp)>0)
				{
					while($rowal = mysql_fetch_assoc($sql_get_allp))
					{
						$rowal['whos_event'] = "only_creator";
						$pal_1[] = $rowal;
					}
				}
			}
			
			
			
			 if(!empty($pal_1) && count($pal_1))
			{
				for($pl=0;$pl<count($pal_1);$pl++)
				{
					$chk_typep_1 = 'artist_project|'.$pal_1[$pl]['id'];
					
					$sql = mysql_query("SELECT * FROM artist_event WHERE creators_info='".$chk_typep_1."' AND del_status=0 AND active=0 AND date>='".$date."'");
					if($sql!="" && $sql!=Null)
					{
						if(mysql_num_rows($sql)>0)
						{
							while($row_po = mysql_fetch_assoc($sql))
							{
								if($row_po['artist_id']!=$id['artist_id'])
								{
									$row_po['edit'] = 'yes';	
									$row_po['whos_event'] = "only_creator";
									$row_po['id'] = $row_po['id'] ."~art";
									$art_e_ao[] = $row_po;
								}
							}
						}
					}
					
					$sql = mysql_query("SELECT * FROM community_event WHERE creators_info='".$chk_typep_1."' AND del_status=0 AND active=0 AND date>='".$date."'");
					if($sql!="" && $sql!=Null)
					{
						if(mysql_num_rows($sql)>0)
						{
							while($row_poo = mysql_fetch_assoc($sql))
							{
								if($row_poo['community_id']!=$id['community_id'])
								{
									$row_poo['edit'] = 'yes';
									$row_poo['edit_com'] = 'yes';
									$row_poo['whos_event'] = "only_creator";
									$row_poo['id'] = $row_poo['id'] ."~com";
									$com_e_ao[] = $row_poo;
								}
							}
						}
					}
				}
			} 

			/*$sql_pro_arta = mysql_query("SELECT * FROM artist_project WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_pro_arta)>0)
			{
				while($rowaa = mysql_fetch_assoc($sql_pro_arta))
				{
					if($rowaa['artist_id']!=$id['artist_id'])
					{
						$rowaa['id'] = $rowaa['id'].'~art';
						$arta[] = $rowaa;
					}
				}
			}*/
			
			$sql_pro_arta = mysql_query("SELECT * FROM artist_event WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0 AND date>='".$date."'");
			if($sql_pro_arta!="" && $sql_pro_arta!=Null)
			{
				if(mysql_num_rows($sql_pro_arta)>0)
				{
					while($rowaa = mysql_fetch_assoc($sql_pro_arta))
					{
						if($rowaa['artist_id']!=$id['artist_id'])
						{
							$rowaa['edit'] = 'yes';
							$rowaa['whos_event'] = "only_creator";
							$rowaa['id'] = $rowaa['id'] ."~art";
							$artae[] = $rowaa;
						}
					}
				} 
			}
			
			/*$sql_pro_coma = mysql_query("SELECT * FROM community_project WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_pro_coma)>0)
			{
				while($rowam = mysql_fetch_assoc($sql_pro_coma))
				{
					if($rowam['community_id']!=$id['community_id'])
					{
						$rowam['id'] = $rowam['id'].'~com';
						$coma[] = $rowam;
					}
				}
			}*/
			
			$sql_pro_coma = mysql_query("SELECT * FROM community_event WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0 AND date>='".$date."'");
			if($sql_pro_coma!="" && $sql_pro_coma!=Null)
			{
				if(mysql_num_rows($sql_pro_coma)>0)
				{
					while($rowam = mysql_fetch_assoc($sql_pro_coma))
					{
						if($rowam['community_id']!=$id['community_id'])
						{
							$rowam['edit'] = 'yes';
							$rowam['edit_com'] = 'yes';
							$rowam['whos_event'] = "only_creator";
							$rowam['id'] = $rowam['id'] ."~com";
							$comae[] = $rowam;
						}
					}
				} 
			}
		}
		
		if($id['community_id']!=0 && !empty($id['community_id']))
		{
			/*$chk_type = $var_type_2.'|'.$id['community_id'];
			$sql_pro_artm = mysql_query("SELECT * FROM artist_project WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_pro_artm)>0)
			{
				while($rowcm = mysql_fetch_assoc($sql_pro_artm))
				{
					if($rowcm['artist_id']!=$id['artist_id'])
					{
						$rowcm['id'] = $rowcm['id'].'~art';
						$artm[] = $rowcm;
					}
				}
			}*/
			
			 $chk_type = $var_type_2.'|'.$id['community_id'];
			$sql_pro_artm = mysql_query("SELECT * FROM artist_event WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0 AND date>='".$date."'");
			if($sql_pro_artm!="" && $sql_pro_artm!=Null)
			{
				if(mysql_num_rows($sql_pro_artm)>0)
				{
					while($rowcm = mysql_fetch_assoc($sql_pro_artm))
					{
						if($rowcm['artist_id']!=$id['artist_id'])
						{
							$rowcm['edit'] = 'yes';
							$rowcm['whos_event'] = "only_creator";
							$rowcm['id'] = $rowcm['id'] ."~art";
							$artme[] = $rowcm;
						}
					}
				} 
			}
			
			/*$sql_pro_comm = mysql_query("SELECT * FROM community_project WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_pro_comm)>0)
			{
				while($rowca = mysql_fetch_assoc($sql_pro_comm))
				{
					if($rowca['community_id']!=$id['community_id'])
					{
						$rowca['id'] = $rowca['id'].'~com';
						$comm[] = $rowca;
					}
				}
			}*/
			
			$sql_pro_comm = mysql_query("SELECT * FROM community_event WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0 AND date>='".$date."'");
			if($sql_pro_comm!="" && $sql_pro_comm!=Null)
			{
				if(mysql_num_rows($sql_pro_comm)>0)
				{
					while($rowca = mysql_fetch_assoc($sql_pro_comm))
					{
						if($rowca['community_id']!=$id['community_id'])
						{
							$rowca['edit'] = 'yes';
							$rowca['edit_com'] = 'yes';
							$rowca['whos_event'] = "only_creator";
							$rowca['id'] = $rowca['id'] ."~com";
							$comme[] = $rowca;
						}
					}
				} 
			}
			
			$sql_get_allp = mysql_query("SELECT * FROM community_project WHERE community_id='".$id['community_id']."' AND del_status=0 AND active=0");
			if($sql_get_allp!="" && $sql_get_allp!=Null)
			{
				if(mysql_num_rows($sql_get_allp)>0)
				{
					while($rowal = mysql_fetch_assoc($sql_get_allp))
					{
						$rowal['whos_event'] = "only_creator";
						$pal_2[] = $rowal;
					}
				}
			}
			
			/*if(!empty($pal_2) && count($pal_2))
			{
				for($pl2=0;$pl2<count($pal_2);$pl2++)
				{
					$chk_typep_2 = 'community_project|'.$pal_2[$pl2]['id'];
					$sql = mysql_query("SELECT * FROM artist_project WHERE creators_info='".$chk_typep_2."' AND del_status=0 AND active=0");
					if(mysql_num_rows($sql)>0)
					{
						while($row_po = mysql_fetch_assoc($sql))
						{
							if($row_po['artist_id']!=$id['artist_id'])
							{
								$row_po['id'] = $row_po['id'].'~art';
								$art_p_co[] = $row_po;
							}
						}
					}
					
					$sql = mysql_query("SELECT * FROM community_project WHERE creators_info='".$chk_typep_2."' AND del_status=0 AND active=0");
					if(mysql_num_rows($sql)>0)
					{
						while($row_poo = mysql_fetch_assoc($sql))
						{
							if($row_poo['community_id']!=$id['community_id'])
							{
								$row_poo['id'] = $row_poo['id'].'~com';
								$com_p_co[] = $row_poo;
							}
						}
					}
				}
			}*/
			
			 if(!empty($pal_2) && count($pal_2))
			{
				for($pl2=0;$pl2<count($pal_2);$pl2++)
				{
					$chk_typep_2 = 'community_project|'.$pal_2[$pl2]['id'];
					$sql = mysql_query("SELECT * FROM artist_event WHERE creators_info='".$chk_typep_2."' AND del_status=0 AND active=0 AND date>='".$date."'");
					if($sql!="" && $sql!=Null)
					{
						if(mysql_num_rows($sql)>0)
						{
							while($row_po = mysql_fetch_assoc($sql))
							{
								if($row_po['artist_id']!=$id['artist_id'])
								{
									$row_po['edit'] = 'yes';
									$row_po['whos_event'] = "only_creator";
									$row_po['id'] = $row_po['id'] ."~art";
									$art_e_co[] = $row_po;
								}
							}
						}
					}
					
					$sql = mysql_query("SELECT * FROM community_event WHERE creators_info='".$chk_typep_2."' AND del_status=0 AND active=0 AND date>='".$date."'");
					if($sql!="" && $sql!=Null)
					{
						if(mysql_num_rows($sql)>0)
						{
							while($row_poo = mysql_fetch_assoc($sql))
							{
								if($row_poo['community_id']!=$id['community_id'])
								{
									$row_poo['edit'] = 'yes';
									$row_poo['edit_com'] = 'yes';
									$row_poo['whos_event'] = "only_creator";
									$row_poo['id'] = $row_poo['id'] ."~com";
									$com_e_co[] = $row_poo;
								}
							}
						}
					}
				}
			} 
		}
		
		/* var_dump($artm);
		var_dump($arta);
		var_dump($comm);
		var_dump($coma);
		var_dump($art_p_ao);
		var_dump($com_p_ao);
		var_dump($art_p_co);
		var_dump($com_p_co); */
		
		$totals = array();
		$totals = array_merge($artm,$arta,$comm,$coma,$art_p_ao,$com_p_ao,$art_p_co,$com_p_co,$artme,$artae,$comme,$comae,$art_e_ao,$com_e_ao,$art_e_co,$com_e_co);
		return $totals;
	}
	function get_event_tagged_by_other_user($id)
	{
		$date=date("Y-m-d");
		$sql14=mysql_query("select * from artist_event where id='".$id."' AND date>='".$date."' AND del_status=0 AND active=0");
		if($sql14!="" && $sql14!=Null)
		{
			$res14=mysql_fetch_assoc($sql14);
			return($res14);
		}else{
			$res14 =Array();
			return($res14);
		}
	}
	function get_cevent_tagged_by_other_user($id)
	{
		$date=date("Y-m-d");
		$sql14=mysql_query("select * from community_event where id='".$id."' AND date>='".$date."' AND del_status=0 AND active=0");
		if($sql14!="" && $sql14!=Null)
		{
			$res14=mysql_fetch_assoc($sql14);
			return($res14);
		}else{
			$res14 =Array();
			return($res14);
		}
	}
	function getEventRecorded()
	{
		$date=date("Y-m-d");
		$new_id=$this->get_gen_info();
		$getproject=mysql_query("select * from artist_event where artist_id='".$new_id['artist_id']."' AND date<'".$date."' AND del_status=0 AND active=0");
		return ($getproject);
	}
	function getComEventRecorded()
	{
		$date=date("Y-m-d");
		$new_id=$this->get_gen_info();
		$getproject=mysql_query("select * from community_event where community_id='".$new_id['community_id']."' AND date<'".$date."' AND del_status=0 AND active=0");
		return ($getproject);
	}
	function only_creator_orecevent()
	{
		$id = $this->get_gen_info();
		$date = date("Y-m-d");
		
		$var_type_1 = 'general_artist';
		$var_type_2 = 'general_community';
		$var_type_3 = 'artist_project';
		$var_type_4 = 'community_project';
		
		$artm = array();
		$arta = array();
		$comm = array();
		$coma = array();
		
		$artme = array();
		$artae = array();
		$comme = array();
		$comae = array();
		
		$art_p_ao = array();
		$com_p_ao = array();
		$art_p_co = array();
		$com_p_co = array();
		
		$art_e_ao = array();
		$com_e_ao = array();
		$art_e_co = array();
		$com_e_co = array();
		
		$pal_1 = array();
		$pal_2 = array();
		
		if($id['artist_id']!=0 && !empty($id['artist_id']))
		{
			$chk_type = $var_type_1.'|'.$id['artist_id'];
			
			$sql_get_allp = mysql_query("SELECT * FROM artist_project WHERE artist_id='".$id['artist_id']."' AND del_status=0 AND active=0");
			if($sql_get_allp!="" && $sql_get_allp!=Null)
			{
				if(mysql_num_rows($sql_get_allp)>0)
				{
					while($rowal = mysql_fetch_assoc($sql_get_allp))
					{
						$rowal['whos_event'] = "only_creator";
						$pal_1[] = $rowal;
					}
				}
			}
			
			
			
			 if(!empty($pal_1) && count($pal_1))
			{
				for($pl=0;$pl<count($pal_1);$pl++)
				{
					$chk_typep_1 = 'artist_project|'.$pal_1[$pl]['id'];
					
					$sql = mysql_query("SELECT * FROM artist_event WHERE creators_info='".$chk_typep_1."' AND del_status=0 AND active=0 AND date<'".$date."'");
					if($sql!="" && $sql!=Null)
					{
						if(mysql_num_rows($sql)>0)
						{
							while($row_po = mysql_fetch_assoc($sql))
							{
								if($row_po['artist_id']!=$id['artist_id'])
								{
									$row_po['edit'] = 'yes';
									$row_po['whos_event'] = "only_creator";
									$row_po['id'] = $row_po['id'] ."~art";
									$art_e_ao[] = $row_po;
								}
							}
						}
					}
					
					$sql = mysql_query("SELECT * FROM community_event WHERE creators_info='".$chk_typep_1."' AND del_status=0 AND active=0 AND date<'".$date."'");
					if($sql!="" && $sql!=Null)
					{
						if(mysql_num_rows($sql)>0)
						{
							while($row_poo = mysql_fetch_assoc($sql))
							{
								if($row_poo['community_id']!=$id['community_id'])
								{
									$row_poo['edit'] = 'yes';
									$row_poo['edit_com'] = 'yes';
									$row_poo['whos_event'] = "only_creator";
									$row_poo['id'] = $row_poo['id'] ."~com";
									$com_e_ao[] = $row_poo;
								}
							}
						}
					}
				}
			} 

			/*$sql_pro_arta = mysql_query("SELECT * FROM artist_project WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_pro_arta)>0)
			{
				while($rowaa = mysql_fetch_assoc($sql_pro_arta))
				{
					if($rowaa['artist_id']!=$id['artist_id'])
					{
						$rowaa['id'] = $rowaa['id'].'~art';
						$arta[] = $rowaa;
					}
				}
			}*/
			
			$sql_pro_arta = mysql_query("SELECT * FROM artist_event WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0 AND date<'".$date."'");
			if($sql_pro_arta!="" && $sql_pro_arta!=Null)
			{
				if(mysql_num_rows($sql_pro_arta)>0)
				{
					while($rowaa = mysql_fetch_assoc($sql_pro_arta))
					{
						if($rowaa['artist_id']!=$id['artist_id'])
						{
							$rowaa['edit'] = 'yes';
							$rowaa['whos_event'] = "only_creator";
							$rowaa['id'] = $rowaa['id'] ."~art";
							$artae[] = $rowaa;
						}
					}
				} 
			}
			
			/*$sql_pro_coma = mysql_query("SELECT * FROM community_project WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_pro_coma)>0)
			{
				while($rowam = mysql_fetch_assoc($sql_pro_coma))
				{
					if($rowam['community_id']!=$id['community_id'])
					{
						$rowam['id'] = $rowam['id'].'~com';
						$coma[] = $rowam;
					}
				}
			}*/
			
			$sql_pro_coma = mysql_query("SELECT * FROM community_event WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0 AND date<'".$date."'");
			if($sql_pro_coma!="" && $sql_pro_coma!=Null)
			{
				if(mysql_num_rows($sql_pro_coma)>0)
				{
					while($rowam = mysql_fetch_assoc($sql_pro_coma))
					{
						if($rowam['community_id']!=$id['community_id'])
						{
							$rowam['edit'] = 'yes';
							$rowam['edit_com'] = 'yes';
							$rowam['whos_event'] = "only_creator";
							$rowam['id'] = $rowam['id'] ."~com";
							$comae[] = $rowam;
						}
					}
				}
			}
		}
		
		if($id['community_id']!=0 && !empty($id['community_id']))
		{
			/*$chk_type = $var_type_2.'|'.$id['community_id'];
			$sql_pro_artm = mysql_query("SELECT * FROM artist_project WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_pro_artm)>0)
			{
				while($rowcm = mysql_fetch_assoc($sql_pro_artm))
				{
					if($rowcm['artist_id']!=$id['artist_id'])
					{
						$rowcm['id'] = $rowcm['id'].'~art';
						$artm[] = $rowcm;
					}
				}
			}*/
			
			 $chk_type = $var_type_2.'|'.$id['community_id'];
			$sql_pro_artm = mysql_query("SELECT * FROM artist_event WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0 AND date<'".$date."'");
			if($sql_pro_artm!="" && $sql_pro_artm!=Null)
			{
				if(mysql_num_rows($sql_pro_artm)>0)
				{
					while($rowcm = mysql_fetch_assoc($sql_pro_artm))
					{
						if($rowcm['artist_id']!=$id['artist_id'])
						{
							$rowcm['edit'] = 'yes';
							$rowcm['whos_event'] = "only_creator";
							$rowcm['id'] = $rowcm['id'] ."~art";
							$artme[] = $rowcm;
						}
					}
				} 
			}
			
			/*$sql_pro_comm = mysql_query("SELECT * FROM community_project WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_pro_comm)>0)
			{
				while($rowca = mysql_fetch_assoc($sql_pro_comm))
				{
					if($rowca['community_id']!=$id['community_id'])
					{
						$rowca['id'] = $rowca['id'].'~com';
						$comm[] = $rowca;
					}
				}
			}*/
			
			 $sql_pro_comm = mysql_query("SELECT * FROM community_event WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0 AND date<'".$date."'");
			if($sql_pro_comm!="" && $sql_pro_comm!=Null)
			{
				if(mysql_num_rows($sql_pro_comm)>0)
				{
					while($rowca = mysql_fetch_assoc($sql_pro_comm))
					{
						if($rowca['community_id']!=$id['community_id'])
						{
							$rowca['edit'] = 'yes';
							$rowca['edit_com'] = 'yes';
							$rowca['whos_event'] = "only_creator";
							$rowca['id'] = $rowca['id'] ."~com";
							$comme[] = $rowca;
						}
					}
				} 
			}
			
			$sql_get_allp = mysql_query("SELECT * FROM community_project WHERE community_id='".$id['community_id']."' AND del_status=0 AND active=0");
			if($sql_get_allp!="" && $sql_get_allp!=Null)
			{
				if(mysql_num_rows($sql_get_allp)>0)
				{
					while($rowal = mysql_fetch_assoc($sql_get_allp))
					{
						$rowal['whos_event'] = "only_creator";
						$pal_2[] = $rowal;
					}
				}
			}
			
			/*if(!empty($pal_2) && count($pal_2))
			{
				for($pl2=0;$pl2<count($pal_2);$pl2++)
				{
					$chk_typep_2 = 'community_project|'.$pal_2[$pl2]['id'];
					$sql = mysql_query("SELECT * FROM artist_project WHERE creators_info='".$chk_typep_2."' AND del_status=0 AND active=0");
					if(mysql_num_rows($sql)>0)
					{
						while($row_po = mysql_fetch_assoc($sql))
						{
							if($row_po['artist_id']!=$id['artist_id'])
							{
								$row_po['id'] = $row_po['id'].'~art';
								$art_p_co[] = $row_po;
							}
						}
					}
					
					$sql = mysql_query("SELECT * FROM community_project WHERE creators_info='".$chk_typep_2."' AND del_status=0 AND active=0");
					if(mysql_num_rows($sql)>0)
					{
						while($row_poo = mysql_fetch_assoc($sql))
						{
							if($row_poo['community_id']!=$id['community_id'])
							{
								$row_poo['id'] = $row_poo['id'].'~com';
								$com_p_co[] = $row_poo;
							}
						}
					}
				}
			}*/
			
			 if(!empty($pal_2) && count($pal_2))
			{
				for($pl2=0;$pl2<count($pal_2);$pl2++)
				{
					$chk_typep_2 = 'community_project|'.$pal_2[$pl2]['id'];
					$sql = mysql_query("SELECT * FROM artist_event WHERE creators_info='".$chk_typep_2."' AND del_status=0 AND active=0 AND date<'".$date."'");
					if($sql!="" && $sql!=Null)
					{
						if(mysql_num_rows($sql)>0)
						{
							while($row_po = mysql_fetch_assoc($sql))
							{
								if($row_po['artist_id']!=$id['artist_id'])
								{
									$row_po['edit'] = 'yes';
									$row_po['whos_event'] = "only_creator";
									$row_po['id'] = $row_po['id'] ."~art";
									$art_e_co[] = $row_po;
								}
							}
						}
					}
					
					$sql = mysql_query("SELECT * FROM community_event WHERE creators_info='".$chk_typep_2."' AND del_status=0 AND active=0 AND date<'".$date."'");
					if($sql!="" && $sql!=Null)
					{
						if(mysql_num_rows($sql)>0)
						{
							while($row_poo = mysql_fetch_assoc($sql))
							{
								if($row_poo['community_id']!=$id['community_id'])
								{
									$row_poo['edit'] = 'yes';
									$row_poo['edit_com'] = 'yes';
									$row_poo['whos_event'] = "only_creator";
									$row_poo['id'] = $row_poo['id'] ."~com";
									$com_e_co[] = $row_poo;
								}
							}
						}
					}
				}
			} 
		}
		
		/* var_dump($artm);
		var_dump($arta);
		var_dump($comm);
		var_dump($coma);
		var_dump($art_p_ao);
		var_dump($com_p_ao);
		var_dump($art_p_co);
		var_dump($com_p_co); */
		
		$totals = array();
		$totals = array_merge($artm,$arta,$comm,$coma,$art_p_ao,$com_p_ao,$art_p_co,$com_p_co,$artme,$artae,$comme,$comae,$art_e_ao,$com_e_ao,$art_e_co,$com_e_co);
		return $totals;
	}
	function only_creator2_orecevent()
	{
		$id = $this->get_gen_info();
		$date = date("Y-m-d");
		
		$var_type_1 = 'general_artist';
		$var_type_2 = 'general_community';
		$var_type_3 = 'artist_project';
		$var_type_4 = 'community_project';
		
		$art_p_ao = array();
		$art_c_ao = array();
		
		$com_p_ao = array();
		$com_c_ao = array();
		
		$com_a_ao = array();
		$art_a_ao = array();
		
		$art_ap_ao = array();
		$com_cp_ao = array();
		
		$pal_1 = array();
		$pal_2 = array();
		
		if($id['artist_id']!=0 && !empty($id['artist_id']))
		{
			$chk_type = $var_type_1.'|'.$id['artist_id'];
			
			$sql_get_allp = mysql_query("SELECT * FROM artist_project WHERE artist_id='".$id['artist_id']."' AND del_status=0 AND active=0");
			if($sql_get_allp!="" && $sql_get_allp!=Null)
			{
				if(mysql_num_rows($sql_get_allp)>0)
				{
					while($rowal = mysql_fetch_assoc($sql_get_allp))
					{
						$rowal['whos_event'] = "art_creator_creator_eve";
						$pal_1[] = $rowal;
					}
				}
			}
			
			if(!empty($pal_1) && count($pal_1))
			{
				for($pl=0;$pl<count($pal_1);$pl++)
				{
					$chk_typep_1 = 'artist_project|'.$pal_1[$pl]['id'];
					
					$sql = mysql_query("SELECT * FROM artist_project WHERE (creators_info='".$chk_typep_1."' OR creators_info='".$chk_type."') AND del_status=0 AND active=0");
					if($sql!="" && $sql!=Null)
					{
						if(mysql_num_rows($sql)>0)
						{
							while($row_po = mysql_fetch_assoc($sql))
							{
								//if($row_po['artist_id']!=$id['artist_id'])
								//{
									
									$sql_art2 = mysql_query("SELECT * FROM artist_event WHERE creators_info='artist_project|".$row_po['id']."' AND del_status=0 AND active=0 AND date<'".$date."'");
									if($sql_art2!="" && $sql_art2!=Null)
									{
										if(mysql_num_rows($sql_art2)>0)
										{
											while($row2_po = mysql_fetch_assoc($sql_art2))
											{
												if($row2_po['artist_id']!=$id['artist_id'])
												{
													$row2_po['id'] = $row2_po['id'].'~art';
													$row2_po['whos_event'] = "art_creator_creator_eve";
													$art_p_ao[] = $row2_po;
												}
											}
										}
									}
									
									
									$sql_art3 = mysql_query("SELECT * FROM community_event WHERE creators_info='artist_project|".$row_po['id']."' AND del_status=0 AND active=0 AND date<'".$date."'");
									if($sql_art3!="" && $sql_art3!=Null)
									{
										if(mysql_num_rows($sql_art3)>0)
										{
											while($row3_po = mysql_fetch_assoc($sql_art3))
											{
												if($row3_po['community_id']!=$id['community_id'])
												{
													$row3_po['id'] = $row3_po['id'].'~com';
													$row3_po['whos_event'] = "com_creator_creator_eve";
													$art_c_ao[] = $row3_po;
												}
											}
										}
									}
								//}
							}
						}
					}
					
					
					$sql = mysql_query("SELECT * FROM community_project WHERE (creators_info='".$chk_typep_1."' OR creators_info='".$chk_type."') AND del_status=0 AND active=0");
					if($sql!="" && $sql!=Null)
					{
						if(mysql_num_rows($sql)>0)
						{
							while($row_poo = mysql_fetch_assoc($sql))
							{
								//if($row_poo['community_id']!=$id['community_id'])
								//{		
									
									$sql_art2 = mysql_query("SELECT * FROM artist_event WHERE creators_info='community_project|".$row_poo['id']."' AND del_status=0 AND active=0 AND date<'".$date."'");
									if($sql_art2!="" && $sql_art2!=Null)
									{
										if(mysql_num_rows($sql_art2)>0)
										{
											while($row2_po = mysql_fetch_assoc($sql_art2))
											{
												if($row2_po['artist_id']!=$id['artist_id'])
												{
													$row2_po['id'] = $row2_po['id'].'~art';
													$row2_po['whos_event'] = "art_creator_creator_eve";
													$com_p_ao[] = $row2_po;
												}
											}
										}
									}
									
									
									$sql_art3 = mysql_query("SELECT * FROM community_event WHERE creators_info='community_project|".$row_poo['id']."' AND del_status=0 AND active=0 AND date<'".$date."'");
									if($sql_art3!="" && $sql_art3!=Null)
									{
										if(mysql_num_rows($sql_art3)>0)
										{
											while($row3_po = mysql_fetch_assoc($sql_art3))
											{
												if($row3_po['community_id']!=$id['community_id'])
												{
													$row3_po['id'] = $row3_po['id'].'~com';
													$row3_po['whos_event'] = "com_creator_creator_eve";
													$com_c_ao[] = $row3_po;
												}
											}
										}
									}
								//}
							}
						}
					}
				}
			}
		}
		
		if($id['community_id']!=0 && !empty($id['community_id']))
		{
			$chk_type = $var_type_2.'|'.$id['community_id'];
			
			$sql_get_allp = mysql_query("SELECT * FROM community_project WHERE community_id='".$id['community_id']."' AND del_status=0 AND active=0");
			if($sql_get_allp!="" && $sql_get_allp!=Null)
			{
				if(mysql_num_rows($sql_get_allp)>0)
				{
					while($rowal = mysql_fetch_assoc($sql_get_allp))
					{
						//$rowal['whos_project'] = "only_creator";
						$pal_2[] = $rowal;
					}
				}
			}
			
			if(!empty($pal_2) && count($pal_2))
			{
				for($pl2=0;$pl2<count($pal_2);$pl2++)
				{
					$chk_typep_2 = 'community_project|'.$pal_2[$pl2]['id'];
					
					
					$sql = mysql_query("SELECT * FROM artist_project WHERE (creators_info='".$chk_typep_2."' OR creators_info='".$chk_type."') AND del_status=0 AND active=0");
					if($sql!="" && $sql!=Null)
					{
						if(mysql_num_rows($sql)>0)
						{
							while($row_po = mysql_fetch_assoc($sql))
							{
								//if($row_po['artist_id']!=$id['artist_id'])
								//{
									
									$sql_art2 = mysql_query("SELECT * FROM artist_event WHERE creators_info='artist_project|".$row_po['id']."' AND del_status=0 AND active=0 AND date<'".$date."'");
									if($sql_art2!="" && $sql_art2!=Null)
									{
										if(mysql_num_rows($sql_art2)>0)
										{
											while($row2_po = mysql_fetch_assoc($sql_art2))
											{
												if($row2_po['artist_id']!=$id['artist_id'])
												{
													$row2_po['id'] = $row2_po['id'].'~art';
													$row2_po['whos_event'] = "art_creator_creator_eve";
													$com_a_ao[] = $row2_po;
												}
											}
										}
									}
									
									
									$sql_art3 = mysql_query("SELECT * FROM community_event WHERE creators_info='artist_project|".$row_po['id']."' AND del_status=0 AND active=0 AND date<'".$date."'");
									if($sql_art3!="" && $sql_art3!=Null)
									{
										if(mysql_num_rows($sql_art3)>0)
										{
											while($row3_po = mysql_fetch_assoc($sql_art3))
											{
												if($row3_po['community_id']!=$id['community_id'])
												{
													$row3_po['id'] = $row3_po['id'].'~com';
													$row3_po['whos_event'] = "com_creator_creator_eve";
													$art_a_ao[] = $row3_po;
												}
											}
										}
									}
								//}
							}
						}
					}
					
					
					$sql = mysql_query("SELECT * FROM community_project WHERE (creators_info='".$chk_typep_2."' OR creators_info='".$chk_type."') AND del_status=0 AND active=0");
					if($sql!="" && $sql!=Null)
					{
						if(mysql_num_rows($sql)>0)
						{
							while($row_poo = mysql_fetch_assoc($sql))
							{
								//if($row_poo['community_id']!=$id['community_id'])
								//{
									
									$sql_art2 = mysql_query("SELECT * FROM artist_event WHERE creators_info='community_project|".$row_poo['id']."' AND del_status=0 AND active=0 AND date<'".$date."'");
									if($sql_art2!="" && $sql_art2!=Null)
									{
										if(mysql_num_rows($sql_art2)>0)
										{
											while($row2_po = mysql_fetch_assoc($sql_art2))
											{
												if($row2_po['artist_id']!=$id['artist_id'])
												{
													$row2_po['id'] = $row2_po['id'].'~art';
													$row2_po['whos_event'] = "art_creator_creator_eve";
													$art_ap_ao[] = $row2_po;
												}
											}
										}
									}
									
									
									$sql_art3 = mysql_query("SELECT * FROM community_event WHERE creators_info='community_project|".$row_poo['id']."' AND del_status=0 AND active=0 AND date<'".$date."'");
									if($sql_art3!="" && $sql_art3!=Null)
									{
										if(mysql_num_rows($sql_art3)>0)
										{
											while($row3_po = mysql_fetch_assoc($sql_art3))
											{
												if($row3_po['community_id']!=$id['community_id'])
												{
													$row3_po['id'] = $row3_po['id'].'~com';
													$row3_po['whos_event'] = "com_creator_creator_eve";
													$com_cp_ao[] = $row3_po;
												}
											}
										}
									}
								//}
							}
						}
					}
				}
			}
		}		
		
		$totals = array();
		$totals = array_merge($art_p_ao,$art_c_ao,$com_p_ao,$com_c_ao,$com_a_ao,$art_a_ao,$art_ap_ao,$com_cp_ao);
		return $totals;
	}
	function only_creator2ev_oupevent()
	{
		$id = $this->get_gen_info();
		$date = date("Y-m-d");
		
		$var_type_1 = 'general_artist';
		$var_type_2 = 'general_community';
		$var_type_3 = 'artist_project';
		$var_type_4 = 'community_project';
		
		$art_p_ao = array();
		$art_c_ao = array();
		
		$com_p_ao = array();
		$com_c_ao = array();
		
		$com_a_ao = array();
		$art_a_ao = array();
		
		$art_ap_ao = array();
		$com_cp_ao = array();
		
		$pal_1 = array();
		$pal_2 = array();
		
		if($id['artist_id']!=0 && !empty($id['artist_id']))
		{
			$chk_type = $var_type_1.'|'.$id['artist_id'];
			
			$sql_get_allp = mysql_query("SELECT * FROM artist_project WHERE artist_id='".$id['artist_id']."' AND del_status=0 AND active=0");
			if($sql_get_allp!="" && $sql_get_allp!=Null)
			{
				if(mysql_num_rows($sql_get_allp)>0)
				{
					while($rowal = mysql_fetch_assoc($sql_get_allp))
					{
						//$rowal['whos_project'] = "only_creator";
						$pal_1[] = $rowal;
					}
				}
			}
			
			if(!empty($pal_1) && count($pal_1))
			{
				for($pl=0;$pl<count($pal_1);$pl++)
				{
					$chk_typep_1 = 'artist_project|'.$pal_1[$pl]['id'];
					
					$sql = mysql_query("SELECT * FROM artist_project WHERE (creators_info='".$chk_typep_1."' OR creators_info='".$chk_type."') AND del_status=0 AND active=0");
					if($sql!="" && $sql!=Null)
					{
						if(mysql_num_rows($sql)>0)
						{
							while($row_po = mysql_fetch_assoc($sql))
							{
								//if($row_po['artist_id']!=$id['artist_id'])
								//{
									
									$sql_art2 = mysql_query("SELECT * FROM artist_event WHERE creators_info='artist_project|".$row_po['id']."' AND del_status=0 AND active=0 AND date>='".$date."'");
									if($sql_art2!="" && $sql_art2!=Null)
									{
										if(mysql_num_rows($sql_art2)>0)
										{
											while($row2_po = mysql_fetch_assoc($sql_art2))
											{
												if($row2_po['artist_id']!=$id['artist_id'])
												{
													$row2_po['id'] = $row2_po['id'].'~art';
													$row2_po['whos_event'] = "art_creator_creator_eve";
													$art_p_ao[] = $row2_po;
												}
											}
										}
									}
									
									
									$sql_art3 = mysql_query("SELECT * FROM community_event WHERE creators_info='artist_project|".$row_po['id']."' AND del_status=0 AND active=0 AND date>='".$date."'");
									if($sql_art3!="" && $sql_art3!=Null)
									{
										if(mysql_num_rows($sql_art3)>0)
										{
											while($row3_po = mysql_fetch_assoc($sql_art3))
											{
												if($row3_po['community_id']!=$id['community_id'])
												{
													$row3_po['id'] = $row3_po['id'].'~com';
													$row3_po['whos_event'] = "com_creator_creator_eve";
													$art_c_ao[] = $row3_po;
												}
											}
										}
									}
								//}
							}
						}
					}
					
					
					$sql = mysql_query("SELECT * FROM community_project WHERE (creators_info='".$chk_typep_1."' OR creators_info='".$chk_type."') AND del_status=0 AND active=0");
					if($sql!="" && $sql!=Null)
					{
						if(mysql_num_rows($sql)>0)
						{
							while($row_poo = mysql_fetch_assoc($sql))
							{
								//if($row_poo['community_id']!=$id['community_id'])
								//{		
									
									$sql_art2 = mysql_query("SELECT * FROM artist_event WHERE creators_info='community_project|".$row_poo['id']."' AND del_status=0 AND active=0 AND date>='".$date."'");
									if($sql_art2!="" && $sql_art2!=Null)
									{
										if(mysql_num_rows($sql_art2)>0)
										{
											while($row2_po = mysql_fetch_assoc($sql_art2))
											{
												if($row2_po['artist_id']!=$id['artist_id'])
												{
													$row2_po['id'] = $row2_po['id'].'~art';
													$row2_po['whos_event'] = "art_creator_creator_eve";
													$com_p_ao[] = $row2_po;
												}
											}
										}
									}
									
									
									$sql_art3 = mysql_query("SELECT * FROM community_event WHERE creators_info='community_project|".$row_poo['id']."' AND del_status=0 AND active=0 AND date>='".$date."'");
									if($sql_art3!="" && $sql_art3!=Null)
									{
										if(mysql_num_rows($sql_art3)>0)
										{
											while($row3_po = mysql_fetch_assoc($sql_art3))
											{
												if($row3_po['community_id']!=$id['community_id'])
												{
													$row3_po['id'] = $row3_po['id'].'~com';
													$row3_po['whos_event'] = "com_creator_creator_eve";
													$com_c_ao[] = $row3_po;
												}
											}
										}
									}
								//}
							}
						}
					}
				}
			}
		}
		
		if($id['community_id']!=0 && !empty($id['community_id']))
		{
			$chk_type = $var_type_2.'|'.$id['community_id'];
			
			$sql_get_allp = mysql_query("SELECT * FROM community_project WHERE community_id='".$id['community_id']."' AND del_status=0 AND active=0");
			if($sql_get_allp!="" && $sql_get_allp!=Null)
			{
				if(mysql_num_rows($sql_get_allp)>0)
				{
					while($rowal = mysql_fetch_assoc($sql_get_allp))
					{
						//$rowal['whos_project'] = "only_creator";
						$pal_2[] = $rowal;
					}
				}
			}
			
			if(!empty($pal_2) && count($pal_2))
			{
				for($pl2=0;$pl2<count($pal_2);$pl2++)
				{
					$chk_typep_2 = 'community_project|'.$pal_2[$pl2]['id'];
					
					
					$sql = mysql_query("SELECT * FROM artist_project WHERE (creators_info='".$chk_typep_2."' OR creators_info='".$chk_type."') AND del_status=0 AND active=0");
					if($sql!="" && $sql!=Null)
					{
						if(mysql_num_rows($sql)>0)
						{
							while($row_po = mysql_fetch_assoc($sql))
							{
								//if($row_po['artist_id']!=$id['artist_id'])
								//{
									
									$sql_art2 = mysql_query("SELECT * FROM artist_event WHERE creators_info='artist_project|".$row_po['id']."' AND del_status=0 AND active=0 AND date>='".$date."'");
									if($sql_art2!="" && $sql_art2!=Null)
									{
										if(mysql_num_rows($sql_art2)>0)
										{
											while($row2_po = mysql_fetch_assoc($sql_art2))
											{
												if($row2_po['artist_id']!=$id['artist_id'])
												{
													$row2_po['id'] = $row2_po['id'].'~art';
													$row2_po['whos_event'] = "art_creator_creator_eve";
													$com_a_ao[] = $row2_po;
												}
											}
										}
									}
									
									
									$sql_art3 = mysql_query("SELECT * FROM community_event WHERE creators_info='artist_project|".$row_po['id']."' AND del_status=0 AND active=0 AND date>='".$date."'");
									if($sql_art3!="" && $sql_art3!=Null)
									{
										if(mysql_num_rows($sql_art3)>0)
										{
											while($row3_po = mysql_fetch_assoc($sql_art3))
											{
												if($row3_po['community_id']!=$id['community_id'])
												{
													$row3_po['id'] = $row3_po['id'].'~com';
													$row3_po['whos_event'] = "com_creator_creator_eve";
													$art_a_ao[] = $row3_po;
												}
											}
										}
									}
								//}
							}
						}
					}
					
					
					$sql = mysql_query("SELECT * FROM community_project WHERE (creators_info='".$chk_typep_2."' OR creators_info='".$chk_type."') AND del_status=0 AND active=0");
					if($sql!="" && $sql!=Null)
					{
						if(mysql_num_rows($sql)>0)
						{
							while($row_poo = mysql_fetch_assoc($sql))
							{
								//if($row_poo['community_id']!=$id['community_id'])
								//{
									
									$sql_art2 = mysql_query("SELECT * FROM artist_event WHERE creators_info='community_project|".$row_poo['id']."' AND del_status=0 AND active=0 AND date>='".$date."'");
									if($sql_art2!="" && $sql_art2!=Null)
									{
										if(mysql_num_rows($sql_art2)>0)
										{
											while($row2_po = mysql_fetch_assoc($sql_art2))
											{
												if($row2_po['artist_id']!=$id['artist_id'])
												{
													$row2_po['id'] = $row2_po['id'].'~art';
													$row2_po['whos_event'] = "art_creator_creator_eve";
													$art_ap_ao[] = $row2_po;
												}
											}
										}
									}
									
									
									$sql_art3 = mysql_query("SELECT * FROM community_event WHERE creators_info='community_project|".$row_poo['id']."' AND del_status=0 AND active=0 AND date>='".$date."'");
									if($sql_art3!="" && $sql_art3!=Null)
									{
										if(mysql_num_rows($sql_art3)>0)
										{
											while($row3_po = mysql_fetch_assoc($sql_art3))
											{
												if($row3_po['community_id']!=$id['community_id'])
												{
													$row3_po['id'] = $row3_po['id'].'~com';
													$row3_po['whos_event'] = "com_creator_creator_eve";
													$com_cp_ao[] = $row3_po;
												}
											}
										}
									}
								//}
							}
						}
					}
				}
			}
		}		
		
		$totals = array();
		$totals = array_merge($art_p_ao,$art_c_ao,$com_p_ao,$com_c_ao,$com_a_ao,$art_a_ao,$art_ap_ao,$com_cp_ao);
		return $totals;
	}
	function get_event_tagged_by_other2_user($id)
	{
		$sql_1_cre = "";
		
		$date = date("Y-m-d");
		$sql14 = mysql_query("select * from artist_project where id='".$id."' AND del_status=0 AND active=0");
		if($sql14!="" && $sql14!=Null)
		{
			if(mysql_num_rows($sql14)>0)
			{
				$res14 = mysql_fetch_assoc($sql14);
				//while($res14 = mysql_fetch_assoc($sql14))
				//{
				$cret_info = 'artist_project|'.$res14['id'];
				
				$sql_1_cre = mysql_query("SELECT * FROM artist_event WHERE creators_info='".$cret_info."' AND active=0 AND del_status=0 AND date>='".$date."'");
				return $sql_1_cre;
			}
		}else{
			return $sql_1_cre;
		}
			
		/*	$sql_2_cre = mysql_query("SELECT * FROM community_event WHERE creators_info='".$cret_info."' AND active=0 AND del_status=0");
			if(mysql_num_rows($sql_2_cre)>0)
			{
				while($run_c = mysql_fetch_assoc($sql_2_cre))
				{
					$run_c['id'] = $run_c['id'].'~'.'com';
					//$ans_c_als[]
					$total[] = $run_c;
				}
			}
		//}
		
		return $total; */
	}
	function get_event_ctagged_by_other2_user($id)
	{
		$sql_2_cre = "";
		
		$date = date("Y-m-d");
		$sql14 = mysql_query("select * from artist_project where id='".$id."' AND del_status=0 AND active=0");
		
		if($sql14!="" && $sql14!=Null)
		{
			if(mysql_num_rows($sql14)>0)
			{
				$res14 = mysql_fetch_assoc($sql14);
				//while($res14 = mysql_fetch_assoc($sql14))
				//{
				$cret_info = 'artist_project|'.$res14['id'];
				
				$sql_2_cre = mysql_query("SELECT * FROM community_event WHERE creators_info='".$cret_info."' AND active=0 AND del_status=0 AND date>='".$date."'");
				return $sql_2_cre;
			}
		}else{
			return $sql_2_cre;
		}
			
		/*	
			if(mysql_num_rows($sql_2_cre)>0)
			{
				while($run_c = mysql_fetch_assoc($sql_2_cre))
				{
					$run_c['id'] = $run_c['id'].'~'.'com';
					//$ans_c_als[]
					$total[] = $run_c;
				}
			}
		//}
		
		return $total; */
	}
	function get_cevent_tagged_by_other2_userrec($id)
	{
		$sql_2_cre = "";
		
		$date=date("Y-m-d");
		$sql14 = mysql_query("select * from community_project where id='".$id."' AND del_status=0 AND active=0");
		if($sql14!="" && $sql14!=Null)
		{
			if(mysql_num_rows($sql14)>0)
			{
				$res14 = mysql_fetch_assoc($sql14);
				//while($res14 = mysql_fetch_assoc($sql14))
				//{
				$cret_info = 'community_project|'.$res14['id'];
				
				$sql_2_cre = mysql_query("SELECT * FROM artist_event WHERE creators_info='".$cret_info."' AND active=0 AND del_status=0 AND date<'".$date."'");
				return $sql_2_cre;
			}
		}else{
			return $sql_2_cre;
		}
	}
	function get_cevent_ctagged_by_other2_userrec($id)
	{
		$sql_2_cre = "";
		
		$date=date("Y-m-d");
		$sql14 = mysql_query("select * from community_project where id='".$id."' AND del_status=0 AND active=0");
		if($sql14!="" && $sql14!=Null)
		{
			if(mysql_num_rows($sql14)>0)
			{
				$res14 = mysql_fetch_assoc($sql14);
				//while($res14 = mysql_fetch_assoc($sql14))
				//{
				$cret_info = 'community_project|'.$res14['id'];
				
				$sql_2_cre = mysql_query("SELECT * FROM community_event WHERE creators_info='".$cret_info."' AND active=0 AND del_status=0 AND date<'".$date."'");
				return $sql_2_cre;
			}
		}else{
			return $sql_2_cre;
		}
	}
	function get_deleted_project_id()
	{
		$get_gen_det = $this->get_gen_info();
		$res_art_id ="";
		$get_art_id = mysql_query("select * from general_artist where artist_id='".$get_gen_det['artist_id']."'");
		if($get_art_id!="" && $get_art_id!=Null)
		{
			if(mysql_num_rows($get_art_id)>0)
			{
				$res_art_id = mysql_fetch_assoc($get_art_id);
			}
			return($res_art_id);
		}else{
			return($res_art_id);
		}
	}
	function get_cevent_tagged_by_other2_user($id)
	{
		$sql_1_cre = "";
		
		$date=date("Y-m-d");
		$sql14 = mysql_query("select * from community_project where id='".$id."' AND del_status=0 AND active=0");
		if($sql14!="" && $sql14!=Null)
		{
			if(mysql_num_rows($sql14)>0)
			{
				$res14 = mysql_fetch_assoc($sql14);
				//while($res14 = mysql_fetch_assoc($sql14))
				//{
				$cret_info = 'community_project|'.$res14['id'];
				
				$sql_1_cre = mysql_query("SELECT * FROM artist_event WHERE creators_info='".$cret_info."' AND active=0 AND del_status=0 AND date>='".$date."'");
				return $sql_1_cre;
			}
		}else{
			return $sql_1_cre;
		}
	}
	function get_cevent_ctagged_by_other2_user($id)
	{
		$sql_2_cre = "";
		
		$date=date("Y-m-d");
		$sql14 = mysql_query("select * from community_project where id='".$id."' AND del_status=0 AND active=0");
		if($sql14!="" && $sql14!=Null)
		{
			if(mysql_num_rows($sql14)>0)
			{
				$res14 = mysql_fetch_assoc($sql14);
				//while($res14 = mysql_fetch_assoc($sql14))
				//{
				$cret_info = 'community_project|'.$res14['id'];
				
				$sql_2_cre = mysql_query("SELECT * FROM community_event WHERE creators_info='".$cret_info."' AND active=0 AND del_status=0 AND date>='".$date."'");
				return $sql_2_cre;
			}
		}else{
			return $sql_2_cre;
		}
	}
	function get_event_tagged_by_other_userrec($id)
	{
		$date=date("Y-m-d");
		$sql14=mysql_query("select * from artist_event where id='".$id."' AND date<'".$date."' AND del_status=0 AND active=0");
		if($sql14!="" && $sql14!=Null)
		{
			$res14=mysql_fetch_assoc($sql14);
			return($res14);
		}else{
			$res14 =Array();
			return($res14);
		}
	}
	function get_cevent_tagged_by_other_userrec($id)
	{
		$date=date("Y-m-d");
		$sql14=mysql_query("select * from community_event where id='".$id."' AND date<'".$date."' AND del_status=0 AND active=0");
		if($sql14!="" && $sql14!=Null)
		{
			$res14=mysql_fetch_assoc($sql14);
			return($res14);
		}else{
			$res14 = Array();
			return($res14);
		}
	}
	function get_event_tagged_by_other2_userrec($id)
	{
		$sql_2_cre = "";
		
		$date=date("Y-m-d");
		$sql14 = mysql_query("select * from artist_project where id='".$id."' AND del_status=0 AND active=0");
		if($sql14!="" && $sql14!=Null)
		{
			if(mysql_num_rows($sql14)>0)
			{
				$res14 = mysql_fetch_assoc($sql14);
				//while($res14 = mysql_fetch_assoc($sql14))
				//{
				$cret_info = 'artist_project|'.$res14['id'];
				
				$sql_2_cre = mysql_query("SELECT * FROM artist_event WHERE creators_info='".$cret_info."' AND active=0 AND del_status=0 AND date<'".$date."'");
				return $sql_2_cre;
			}
		}
		else{
			return $sql_2_cre;
		}
	}
	function get_event_ctagged_by_other2_userrec($id)
	{
		$sql_2_cre = "";
		
		$date=date("Y-m-d");
		$sql14 = mysql_query("select * from artist_project where id='".$id."' AND del_status=0 AND active=0");
		if($sql14!="" && $sql14!=Null)
		{
			if(mysql_num_rows($sql14)>0)
			{
				$res14 = mysql_fetch_assoc($sql14);
				//while($res14 = mysql_fetch_assoc($sql14))
				//{
				$cret_info = 'artist_project|'.$res14['id'];
				
				$sql_2_cre = mysql_query("SELECT * FROM community_event WHERE creators_info='".$cret_info."' AND active=0 AND del_status=0 AND date<'".$date."'");
				return $sql_2_cre;
			}
		}else{
			return $sql_2_cre;
		}
	}
	function Get_Event_subtype($id)
	{
		$getproject_type = mysql_query("SELECT * , MIN( id )
											FROM `artist_event_profiles` app
											LEFT JOIN subtype ty ON app.subtype_id = ty.subtype_id
											WHERE app.artist_event_id ='".$id."'");
		if($getproject_type!="" && $getproject_type!=Null)
		{
			$res_type= mysql_fetch_assoc($getproject_type);
			return ($res_type);
		}else{
			$res_type =Array();
			return ($res_type);
		}
	}
	function Get_ACEvent_subtype($id)
	{
		$getproject_type = mysql_query("SELECT * , MIN( id )
											FROM `community_event_profiles` app
											LEFT JOIN subtype ty ON app.subtype_id = ty.subtype_id
											WHERE app.community_event_id ='".$id."'");
		if($getproject_type!="" && $getproject_type!=Null)
		{
			$res_type= mysql_fetch_assoc($getproject_type);
			return ($res_type);
		}else{
			$res_type =Array();
			return ($res_type);
		}
	}
	/** Code fro displaying events ends here**/
	/** Code fro displaying project **/
	function GetProject()
	{
		$new_id=$this->get_gen_info();
		$getproject=mysql_query("select * from artist_project where artist_id='".$new_id['artist_id']."' AND del_status=0 AND active=0");
		return ($getproject);
	}
	function Get_all_friends()
	{
		$res = $this -> get_gen_info();
		$get_friend = mysql_query("select * from friends where general_user_id ='".$res['general_user_id']."' AND status=0");
		if($get_friend!="" && $get_friend!=Null)
		{
			if(mysql_num_rows($get_friend)>0)
			{
				return($get_friend);
			}
		}else{
			$get_friend ="";
			return($get_friend);
		}
	}
	function get_friend_art_info($friend_id)
	{
		$get_gen_info = mysql_query("select * from general_user where general_user_id ='".$friend_id."' AND delete_status=0");
		if($get_gen_info!="" && $get_gen_info!=Null)
		{
			if(mysql_num_rows($get_gen_info)>0)
			{
				$res_gen_info = mysql_fetch_assoc($get_gen_info);
				if($res_gen_info['artist_id']!="" && $res_gen_info['artist_id']!=0)
				{
					return ($res_gen_info['artist_id']);
				}
			}
		}else{
			$res_gen_info['artist_id']="";
			return ($res_gen_info['artist_id']);
		}
	}
	function get_all_tagged_pro($art_id)
	{
		$get_projects = mysql_query("select * from artist_project where artist_id='".$art_id."' AND del_status=0 AND active=0");
		return($get_projects);
	}
	function selcommunity_project()
	{
		$res = $this->get_gen_info();
		$res['community_id'];
		$getproject=mysql_query("select * from community_project where community_id='".$res['community_id']."' AND del_status=0 AND active=0");
		return($getproject);
	}
	function get_friend_community_info($friend_id)
	{
		$get_gen_info = mysql_query("select * from general_user where general_user_id ='".$friend_id."' AND delete_status=0");
		if($get_gen_info!="" && $get_gen_info!=Null)
		{
			if(mysql_num_rows($get_gen_info)>0)
			{
				$res_gen_info = mysql_fetch_assoc($get_gen_info);
				if($res_gen_info['community_id']!="" && $res_gen_info['community_id']!=0)
				{
					return ($res_gen_info['community_id']);
				}
			}
		}else{
			$res_gen_info['community_id'] ="";
			return ($res_gen_info['community_id']);
		}
	}
	function get_all_tagged_community_pro($art_id)
	{
		$get_projects = mysql_query("select * from community_project where community_id='".$art_id."' AND del_status=0 AND active=0");
		return($get_projects);
	}
	function only_creator_this()
	{
		$id = $this->get_gen_info();
		$var_type_1 = 'general_artist';
		$var_type_2 = 'general_community';
		$var_type_3 = 'artist_project';
		$var_type_4 = 'community_project';
		
		$artm = array();
		$arta = array();
		$comm = array();
		$coma = array();
		
		$artme = array();
		$artae = array();
		$comme = array();
		$comae = array();
		
		$art_p_ao = array();
		$com_p_ao = array();
		$art_p_co = array();
		$com_p_co = array();
		
		$art_e_ao = array();
		$com_e_ao = array();
		$art_e_co = array();
		$com_e_co = array();
		
		$pal_1 = array();
		$pal_2 = array();
		
		if($id['artist_id']!=0 && !empty($id['artist_id']))
		{
			$chk_type = $var_type_1.'|'.$id['artist_id'];
			
			$sql_get_allp = mysql_query("SELECT * FROM artist_project WHERE artist_id='".$id['artist_id']."' AND del_status=0 AND active=0");
			if($sql_get_allp!="" && $sql_get_allp!=Null)
			{
				if(mysql_num_rows($sql_get_allp)>0)
				{
					while($rowal = mysql_fetch_assoc($sql_get_allp))
					{
						$rowal['whos_project'] = "only_creator";
						$pal_1[] = $rowal;
					}
				}
			}
			
			if(!empty($pal_1) && count($pal_1))
			{
				for($pl=0;$pl<count($pal_1);$pl++)
				{
					$chk_typep_1 = 'artist_project|'.$pal_1[$pl]['id'];
					
					$sql = mysql_query("SELECT * FROM artist_project WHERE creators_info='".$chk_typep_1."' AND del_status=0 AND active=0");
					if($sql!="" && $sql!=Null)
					{
						if(mysql_num_rows($sql)>0)
						{
							while($row_po = mysql_fetch_assoc($sql))
							{
								if($row_po['artist_id']!=$id['artist_id'])
								{
									$row_po['id'] = $row_po['id'].'~art';
									$row_po['whos_project'] = "only_creator";
									$art_p_ao[] = $row_po;
								}
							}
						}
					}
					
					$sql = mysql_query("SELECT * FROM community_project WHERE creators_info='".$chk_typep_1."' AND del_status=0 AND active=0");
					if($sql!="" && $sql!=Null)
					{
						if(mysql_num_rows($sql)>0)
						{
							while($row_poo = mysql_fetch_assoc($sql))
							{
								if($row_poo['community_id']!=$id['community_id'])
								{
									$row_poo['id'] = $row_poo['id'].'~com';
									$row_poo['whos_project'] = "only_creator";
									$com_p_ao[] = $row_poo;
								}
							}
						}
					}
				}
			}
			
			/* if(!empty($pal_1) && count($pal_1))
			{
				for($pl=0;$pl<count($pal_1);$pl++)
				{
					$chk_typep_1 = 'artist_project|'.$pal_1[$pl]['id'];
					
					$sql = mysql_query("SELECT * FROM artist_event WHERE creators_info='".$chk_typep_1."' AND del_status=0 AND active=0");
					if(mysql_num_rows($sql)>0)
					{
						while($row_po = mysql_fetch_assoc($sql))
						{
							if($row_po['artist_id']!=$id['artist_id'])
							{
								$art_e_ao[] = $row_po;
							}
						}
					}
					
					$sql = mysql_query("SELECT * FROM community_event WHERE creators_info='".$chk_typep_1."' AND del_status=0 AND active=0");
					if(mysql_num_rows($sql)>0)
					{
						while($row_poo = mysql_fetch_assoc($sql))
						{
							if($row_poo['community_id']!=$id['community_id'])
							{
								$com_e_ao[] = $row_poo;
							}
						}
					}
				}
			} */

			$sql_pro_arta = mysql_query("SELECT * FROM artist_project WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0");
			if($sql_pro_arta!="" && $sql_pro_arta!=Null)
			{
				if(mysql_num_rows($sql_pro_arta)>0)
				{
					while($rowaa = mysql_fetch_assoc($sql_pro_arta))
					{
						if($rowaa['artist_id']!=$id['artist_id'])
						{
							$rowaa['id'] = $rowaa['id'].'~art';
							$rowaa['whos_project'] = "only_creator";
							$arta[] = $rowaa;
						}
					}
				}
			}
			
			/* $sql_pro_arta = mysql_query("SELECT * FROM artist_event WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_pro_arta)>0)
			{
				while($rowaa = mysql_fetch_assoc($sql_pro_arta))
				{
					if($rowaa['artist_id']!=$id['artist_id'])
					{
						$artae[] = $rowaa;
					}
				}
			} */
			
			$sql_pro_coma = mysql_query("SELECT * FROM community_project WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0");
			if($sql_pro_coma!="" && $sql_pro_coma!=Null)
			{
				if(mysql_num_rows($sql_pro_coma)>0)
				{
					while($rowam = mysql_fetch_assoc($sql_pro_coma))
					{
						if($rowam['community_id']!=$id['community_id'])
						{
							$rowam['id'] = $rowam['id'].'~com';
							$rowam['whos_project'] = "only_creator";
							$coma[] = $rowam;
						}
					}
				}
			}
			
			/* $sql_pro_coma = mysql_query("SELECT * FROM community_event WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_pro_coma)>0)
			{
				while($rowam = mysql_fetch_assoc($sql_pro_coma))
				{
					if($rowam['community_id']!=$id['community_id'])
					{
						$comae[] = $rowam;
					}
				}
			} */
		}
		
		if($id['community_id']!=0 && !empty($id['community_id']))
		{
			$chk_type = $var_type_2.'|'.$id['community_id'];
			$sql_pro_artm = mysql_query("SELECT * FROM artist_project WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0");
			if($sql_pro_artm!="" && $sql_pro_artm!=Null)
			{
				if(mysql_num_rows($sql_pro_artm)>0)
				{
					while($rowcm = mysql_fetch_assoc($sql_pro_artm))
					{
						if($rowcm['artist_id']!=$id['artist_id'])
						{
							$rowcm['id'] = $rowcm['id'].'~art';
							$rowcm['whos_project'] = "only_creator";
							$artm[] = $rowcm;
						}
					}
				}
			}
			
			/* $chk_type = $var_type_2.'|'.$id['community_id'];
			$sql_pro_artm = mysql_query("SELECT * FROM artist_event WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_pro_artm)>0)
			{
				while($rowcm = mysql_fetch_assoc($sql_pro_artm))
				{
					if($rowcm['artist_id']!=$id['artist_id'])
					{
						$artme[] = $rowcm;
					}
				}
			} */
			
			$sql_pro_comm = mysql_query("SELECT * FROM community_project WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0");
			if($sql_pro_comm!="" && $sql_pro_comm!=Null)
			{
				if(mysql_num_rows($sql_pro_comm)>0)
				{
					while($rowca = mysql_fetch_assoc($sql_pro_comm))
					{
						if($rowca['community_id']!=$id['community_id'])
						{
							$rowca['id'] = $rowca['id'].'~com';
							$rowca['whos_project'] = "only_creator";
							$comm[] = $rowca;
						}
					}
				}
			}
			
			/* $sql_pro_comm = mysql_query("SELECT * FROM community_event WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_pro_comm)>0)
			{
				while($rowca = mysql_fetch_assoc($sql_pro_comm))
				{
					if($rowca['community_id']!=$id['community_id'])
					{
						$comme[] = $rowca;
					}
				}
			} */
			
			$sql_get_allp = mysql_query("SELECT * FROM community_project WHERE community_id='".$id['community_id']."' AND del_status=0 AND active=0");
			if($sql_get_allp!="" && $sql_get_allp!=Null)
			{
				if(mysql_num_rows($sql_get_allp)>0)
				{
					while($rowal = mysql_fetch_assoc($sql_get_allp))
					{
						$rowal['whos_project'] = "only_creator";
						$pal_2[] = $rowal;
					}
				}
			}
			
			if(!empty($pal_2) && count($pal_2))
			{
				for($pl2=0;$pl2<count($pal_2);$pl2++)
				{
					$chk_typep_2 = 'community_project|'.$pal_2[$pl2]['id'];
					$sql = mysql_query("SELECT * FROM artist_project WHERE creators_info='".$chk_typep_2."' AND del_status=0 AND active=0");
					if($sql!="" && $sql!=Null)
					{
						if(mysql_num_rows($sql)>0)
						{
							while($row_po = mysql_fetch_assoc($sql))
							{
								if($row_po['artist_id']!=$id['artist_id'])
								{
									$row_po['id'] = $row_po['id'].'~art';
									$row_po['whos_project'] = "only_creator";
									$art_p_co[] = $row_po;
								}
							}
						}
					}
					
					$sql = mysql_query("SELECT * FROM community_project WHERE creators_info='".$chk_typep_2."' AND del_status=0 AND active=0");
					if($sql!="" && $sql!=Null)
					{
						if(mysql_num_rows($sql)>0)
						{
							while($row_poo = mysql_fetch_assoc($sql))
							{
								if($row_poo['community_id']!=$id['community_id'])
								{
									$row_poo['id'] = $row_poo['id'].'~com';
									$row_poo['whos_project'] = "only_creator";
									$com_p_co[] = $row_poo;
								}
							}
						}
					}
				}
			}
			
			/* if(!empty($pal_2) && count($pal_2))
			{
				for($pl2=0;$pl2<count($pal_2);$pl2++)
				{
					$chk_typep_2 = 'community_project|'.$pal_2[$pl2]['id'];
					$sql = mysql_query("SELECT * FROM artist_event WHERE creators_info='".$chk_typep_2."' AND del_status=0 AND active=0");
					if(mysql_num_rows($sql)>0)
					{
						while($row_po = mysql_fetch_assoc($sql))
						{
							if($row_po['artist_id']!=$id['artist_id'])
							{
								$art_e_co[] = $row_po;
							}
						}
					}
					
					$sql = mysql_query("SELECT * FROM community_event WHERE creators_info='".$chk_typep_2."' AND del_status=0 AND active=0");
					if(mysql_num_rows($sql)>0)
					{
						while($row_poo = mysql_fetch_assoc($sql))
						{
							if($row_poo['community_id']!=$id['community_id'])
							{
								$com_e_co[] = $row_poo;
							}
						}
					}
				}
			} */
		}
		
		
		$totals = array();
		$totals = array_merge($artm,$arta,$comm,$coma,$art_p_ao,$com_p_ao,$art_p_co,$com_p_co,$artme,$artae,$comme,$comae,$art_e_ao,$com_e_ao,$art_e_co,$com_e_co);
		return $totals;
	}
	function only_creator2pr_othis()
	{
		$id = $this->get_gen_info();
		$var_type_1 = 'general_artist';
		$var_type_2 = 'general_community';
		$var_type_3 = 'artist_project';
		$var_type_4 = 'community_project';
		
		$art_p_ao = array();
		$art_c_ao = array();
		
		$com_p_ao = array();
		$com_c_ao = array();
		
		$com_a_ao = array();
		$art_a_ao = array();
		
		$art_ap_ao = array();
		$com_cp_ao = array();
		
		$pal_1 = array();
		$pal_2 = array();
		
		if($id['artist_id']!=0 && !empty($id['artist_id']))
		{
			$chk_type = $var_type_1.'|'.$id['artist_id'];
			
			$sql_get_allp = mysql_query("SELECT * FROM artist_project WHERE artist_id='".$id['artist_id']."' AND del_status=0 AND active=0");
			if($sql_get_allp!="" && $sql_get_allp!=Null)
			{
				if(mysql_num_rows($sql_get_allp)>0)
				{
					while($rowal = mysql_fetch_assoc($sql_get_allp))
					{
						//$rowal['whos_project'] = "only_creator";
						$pal_1[] = $rowal;
					}
				}
			}
			
			if(!empty($pal_1) && count($pal_1))
			{
				for($pl=0;$pl<count($pal_1);$pl++)
				{
					$chk_typep_1 = 'artist_project|'.$pal_1[$pl]['id'];
					
					$sql = mysql_query("SELECT * FROM artist_project WHERE (creators_info='".$chk_typep_1."' OR creators_info='".$chk_type."') AND del_status=0 AND active=0");
					if($sql!="" && $sql!=Null)
					{
						if(mysql_num_rows($sql)>0)
						{
							while($row_po = mysql_fetch_assoc($sql))
							{
								//if($row_po['artist_id']!=$id['artist_id'])
								//{
									
									$sql_art2 = mysql_query("SELECT * FROM artist_project WHERE creators_info='artist_project|".$row_po['id']."' AND del_status=0 AND active=0");
									if($sql_art2!="" && $sql_art2!=Null)
									{
										if(mysql_num_rows($sql_art2)>0)
										{
											while($row2_po = mysql_fetch_assoc($sql_art2))
											{
												if($row2_po['artist_id']!=$id['artist_id'])
												{
													$row2_po['id'] = $row2_po['id'].'~art';
													$row2_po['whos_project'] = "art_creator_creator_pro";
													$art_p_ao[] = $row2_po;
												}
											}
										}
									}
									
									
									$sql_art3 = mysql_query("SELECT * FROM community_project WHERE creators_info='artist_project|".$row_po['id']."' AND del_status=0 AND active=0");
									if($sql_art3!="" && $sql_art3!=Null)
									{
										if(mysql_num_rows($sql_art3)>0)
										{
											while($row3_po = mysql_fetch_assoc($sql_art3))
											{
												if($row3_po['community_id']!=$id['community_id'])
												{
													$row3_po['id'] = $row3_po['id'].'~com';
													$row3_po['whos_project'] = "com_creator_creator_pro";
													$art_c_ao[] = $row3_po;
												}
											}
										}
									}
								//}
							}
						}
					}
					
					
					$sql = mysql_query("SELECT * FROM community_project WHERE (creators_info='".$chk_typep_1."' OR creators_info='".$chk_type."') AND del_status=0 AND active=0");
					if($sql!="" && $sql!=Null)
					{
						if(mysql_num_rows($sql)>0)
						{
							while($row_poo = mysql_fetch_assoc($sql))
							{
								//if($row_poo['community_id']!=$id['community_id'])
								//{		
									
									$sql_art2 = mysql_query("SELECT * FROM artist_project WHERE creators_info='community_project|".$row_poo['id']."' AND del_status=0 AND active=0");
									if($sql_art2!="" && $sql_art2!=Null)
									{
										if(mysql_num_rows($sql_art2)>0)
										{
											while($row2_po = mysql_fetch_assoc($sql_art2))
											{
												if($row2_po['artist_id']!=$id['artist_id'])
												{
													$row2_po['id'] = $row2_po['id'].'~art';
													$row2_po['whos_project'] = "art_creator_creator_pro";
													$com_p_ao[] = $row2_po;
												}
											}
										}
									}
									
									
									$sql_art3 = mysql_query("SELECT * FROM community_project WHERE creators_info='community_project|".$row_poo['id']."' AND del_status=0 AND active=0");
									if($sql_art3!="" && $sql_art3!=Null)
									{
										if(mysql_num_rows($sql_art3)>0)
										{
											while($row3_po = mysql_fetch_assoc($sql_art3))
											{
												if($row3_po['community_id']!=$id['community_id'])
												{
													$row3_po['id'] = $row3_po['id'].'~com';
													$row3_po['whos_project'] = "com_creator_creator_pro";
													$com_c_ao[] = $row3_po;
												}
											}
										}
									}
								//}
							}
						}
					}
				}
			}
		}
		
		if($id['community_id']!=0 && !empty($id['community_id']))
		{
			$chk_type = $var_type_2.'|'.$id['community_id'];
			
			$sql_get_allp = mysql_query("SELECT * FROM community_project WHERE community_id='".$id['community_id']."' AND del_status=0 AND active=0");
			if($sql_get_allp!="" && $sql_get_allp!=Null)
			{
				if(mysql_num_rows($sql_get_allp)>0)
				{
					while($rowal = mysql_fetch_assoc($sql_get_allp))
					{
						//$rowal['whos_project'] = "only_creator";
						$pal_2[] = $rowal;
					}
				}
			}
			
			if(!empty($pal_2) && count($pal_2))
			{
				for($pl2=0;$pl2<count($pal_2);$pl2++)
				{
					$chk_typep_2 = 'community_project|'.$pal_2[$pl2]['id'];
					
					
					$sql = mysql_query("SELECT * FROM artist_project WHERE (creators_info='".$chk_typep_2."' OR creators_info='".$chk_type."') AND del_status=0 AND active=0");
					if($sql!="" && $sql!=Null)
					{
						if(mysql_num_rows($sql)>0)
						{
							while($row_po = mysql_fetch_assoc($sql))
							{
								//if($row_po['artist_id']!=$id['artist_id'])
								//{
									
									$sql_art2 = mysql_query("SELECT * FROM artist_project WHERE creators_info='artist_project|".$row_po['id']."' AND del_status=0 AND active=0");
									if($sql_art2!="" && $sql_art2!=Null)
									{
										if(mysql_num_rows($sql_art2)>0)
										{
											while($row2_po = mysql_fetch_assoc($sql_art2))
											{
												if($row2_po['artist_id']!=$id['artist_id'])
												{
													$row2_po['id'] = $row2_po['id'].'~art';
													$row2_po['whos_project'] = "art_creator_creator_pro";
													$com_a_ao[] = $row2_po;
												}
											}
										}
									}
									
									
									$sql_art3 = mysql_query("SELECT * FROM community_project WHERE creators_info='artist_project|".$row_po['id']."' AND del_status=0 AND active=0");
									if($sql_art3!="" && $sql_art3!=Null)
									{
										if(mysql_num_rows($sql_art3)>0)
										{
											while($row3_po = mysql_fetch_assoc($sql_art3))
											{
												if($row3_po['community_id']!=$id['community_id'])
												{
													$row3_po['id'] = $row3_po['id'].'~com';
													$row3_po['whos_project'] = "com_creator_creator_pro";
													$art_a_ao[] = $row3_po;
												}
											}
										}
									}
								//}
							}
						}
					}
					
					
					$sql = mysql_query("SELECT * FROM community_project WHERE (creators_info='".$chk_typep_2."' OR creators_info='".$chk_type."') AND del_status=0 AND active=0");
					if($sql!="" && $sql!=Null)
					{
						if(mysql_num_rows($sql)>0)
						{
							while($row_poo = mysql_fetch_assoc($sql))
							{
								//if($row_poo['community_id']!=$id['community_id'])
								//{
									
									$sql_art2 = mysql_query("SELECT * FROM artist_project WHERE creators_info='community_project|".$row_poo['id']."' AND del_status=0 AND active=0");
									if($sql_art2!="" && $sql_art2!=Null)
									{
										if(mysql_num_rows($sql_art2)>0)
										{
											while($row2_po = mysql_fetch_assoc($sql_art2))
											{
												if($row2_po['artist_id']!=$id['artist_id'])
												{
													$row2_po['id'] = $row2_po['id'].'~art';
													$row2_po['whos_project'] = "art_creator_creator_pro";
													$art_ap_ao[] = $row2_po;
												}
											}
										}
									}
									
									
									$sql_art3 = mysql_query("SELECT * FROM community_project WHERE creators_info='community_project|".$row_poo['id']."' AND del_status=0 AND active=0");
									if($sql_art3!="" && $sql_art3!=Null)
									{
										if(mysql_num_rows($sql_art3)>0)
										{
											while($row3_po = mysql_fetch_assoc($sql_art3))
											{
												if($row3_po['community_id']!=$id['community_id'])
												{
													$row3_po['id'] = $row3_po['id'].'~com';
													$row3_po['whos_project'] = "com_creator_creator_pro";
													$com_cp_ao[] = $row3_po;
												}
											}
										}
									}
								//}
							}
						}
					}
				}
			}
		}		
		
		$totals = array();
		$totals = array_merge($art_p_ao,$art_c_ao,$com_p_ao,$com_c_ao,$com_a_ao,$art_a_ao,$art_ap_ao,$com_cp_ao);
		return $totals;
	}
	function get_project_tagged_by_other2_user($id)
	{
		$sql_1_cre = "";		
		
		$date = date("Y-m-d");
		$sql14 = mysql_query("select * from artist_project where id='".$id."' AND del_status=0 AND active=0");
		if($sql14!="" && $sql14!=Null)
		{
			if(mysql_num_rows($sql14)>0)
			{
				$res14 = mysql_fetch_assoc($sql14);
				$cret_info = 'artist_project|'.$res14['id'];
				
				$sql_1_cre = mysql_query("SELECT * FROM artist_project WHERE creators_info='".$cret_info."' AND active=0 AND del_status=0");
				
				return $sql_1_cre;
			}
		}else{
			return $sql_1_cre;
		}
	}
	function get_project_ctagged_by_other2_user($id)
	{
		$sql_2_cre = "";		
		
		$date = date("Y-m-d");
		$sql14 = mysql_query("select * from artist_project where id='".$id."' AND del_status=0 AND active=0");
		if($sql14!="" && $sql14!=Null)
		{
			if(mysql_num_rows($sql14)>0)
			{
				$res14 = mysql_fetch_assoc($sql14);
				$cret_info = 'artist_project|'.$res14['id'];
				
				$sql_2_cre = mysql_query("SELECT * FROM community_project WHERE creators_info='".$cret_info."' AND active=0 AND del_status=0");
				
				return $sql_2_cre;
			}
		}else{
			return $sql_2_cre;
		}
	}
	function get_cproject_tagged_by_other2_user($id)
	{
		$sql_1_cre = "";
		
		$date=date("Y-m-d");
		$sql14 = mysql_query("select * from community_project where id='".$id."' AND del_status=0 AND active=0");
		if($sql14!="" && $sql14!=Null)
		{
			if(mysql_num_rows($sql14)>0)
			{
				$res14 = mysql_fetch_assoc($sql14);
				$cret_info = 'community_project|'.$res14['id'];
					
				$sql_1_cre = mysql_query("SELECT * FROM artist_project WHERE creators_info='".$cret_info."' AND active=0 AND del_status=0");
				return $sql_1_cre;
			}
		}else{
			return $sql_1_cre;
		}
	}
	function get_cproject_ctagged_by_other2_user($id)
	{
		$sql_2_cre = "";
		
		$date=date("Y-m-d");
		$sql14 = mysql_query("select * from community_project where id='".$id."' AND del_status=0 AND active=0");
		if($sql14!="" && $sql14!=Null)
		{
			if(mysql_num_rows($sql14)>0)
			{
				$res14 = mysql_fetch_assoc($sql14);
				$cret_info = 'community_project|'.$res14['id'];
				
				$sql_2_cre = mysql_query("SELECT * FROM community_project WHERE creators_info='".$cret_info."' AND active=0 AND del_status=0");
				
				return $sql_2_cre;
			}
		}else{
			return $sql_2_cre;
		}
	}
	function get_project_tagged_by_other_user($id)
	{
		$date=date("Y-m-d");
		$sql14=mysql_query("select * from artist_project where id='".$id."' AND del_status=0 AND active=0");
		if($sql14!="" && $sql14!=Null)
		{
			$res14=mysql_fetch_assoc($sql14);
			return($res14);
		}else{
			$res14 = Array();
			return($res14);
		}
	}
	function get_cproject_tagged_by_other_user($id)
	{
		$date=date("Y-m-d");
		$sql14=mysql_query("select * from community_project where id='".$id."' AND del_status=0 AND active=0");
		if($sql14!="" && $sql14!=Null)
		{
			$res14=mysql_fetch_assoc($sql14);
			return($res14);
		}else{
			$res14 = Array();
			return($res14);
		}
	}
	function Get_Project_subtype($id)
	{
		$getproject_type = mysql_query("SELECT * , MIN( id )
											FROM `artist_project_profiles` app
											LEFT JOIN subtype ty ON app.subtype_id = ty.subtype_id
											WHERE app.artist_project_id ='".$id."'");
		if($getproject_type!="" && $getproject_type!=Null)
		{
			$res_type= mysql_fetch_assoc($getproject_type);
			return ($res_type);
		}else{
			$res_type = Array();
			return ($res_type);
		}
	}
	function Get_ACProject_subtype($id)
	{
		$getproject_type = mysql_query("SELECT * , MIN( id )
											FROM `community_project_profiles` app
											LEFT JOIN subtype ty ON app.subtype_id = ty.subtype_id
											WHERE app.community_project_id ='".$id."'");
		if($getproject_type!="" && $getproject_type!=Null)
		{
			$res_type= mysql_fetch_assoc($getproject_type);
			return ($res_type);
		}else{
			$res_type = Array();
			return ($res_type);
		}
	}
	
	/** Code for displaying project ends here**/
}
?>