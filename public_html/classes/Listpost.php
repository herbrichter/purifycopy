<?php
	class Listpost
	{
		function sel_general()
		{
			$sql="select * from general_user where email='".$_SESSION['login_email']."' AND status=0 AND delete_status=0 AND active=0";
			$row=mysql_query($sql);
			$id=mysql_fetch_assoc($row);
			return($id);
		}
	
		function get_all_events($log_id,$a_id,$c_id)
		{
			$ans_eve_a = array();
			$ans_eve_c = array();
			$an_eve_art_data = array();
			$an_eve_com_data = array();
			$selcoms2_tagged = array();
			$selarts2_tagged = array();
			
			if($a_id!=0 && isset($a_id) && $a_id!="")
			{
				$sql_eve_a = mysql_query("SELECT * FROM artist_event WHERE artist_id='".$a_id."' AND del_status=0 AND active=0");
				if(mysql_num_rows($sql_eve_a)>0)
				{
					while($row_eve_a = mysql_fetch_assoc($sql_eve_a))
					{
						$row_eve_a['table_name'] = 'general_artist';
						$ans_eve_a[] = $row_eve_a;
					}
				}
			}
			
			if($c_id!=0 && isset($c_id) && $c_id!="")
			{
				$sql_eve_c = mysql_query("SELECT * FROM community_event WHERE community_id='".$c_id."' AND del_status=0 AND active=0");
				if(mysql_num_rows($sql_eve_c)>0)
				{
					while($row_eve_c = mysql_fetch_assoc($sql_eve_c))
					{
						$row_eve_c['table_name'] = 'general_community';
						$ans_eve_c[] = $row_eve_c;
					}
				}
			}
			
			if($log_id!=0 && isset($log_id) && $log_id!="")
			{
				$sql_gen_eve = mysql_query("SELECT * FROM general_user WHERE general_user_id='".$log_id."' AND delete_status=0 AND active=0 AND status=0");
				if(mysql_num_rows($sql_gen_eve)>0)
				{
					$row_gen_data = mysql_fetch_assoc($sql_gen_eve);
					if($row_gen_data['taggedartistevents']!="")
					{
						$exp_art_eve = explode(',',$row_gen_data['taggedartistevents']);
						if(count($exp_art_eve)>0 && $exp_art_eve!="")
						{
							for($e_i_c=0;$e_i_c<count($exp_art_eve);$e_i_c++)
							{
								if($exp_art_eve[$e_i_c]!="")
								{
									$eve_art_data = mysql_query("SELECT * FROM artist_event WHERE id='".$exp_art_eve[$e_i_c]."' AND del_status=0 AND active=0");
									if(mysql_num_rows($eve_art_data)>0)
									{
										while($reve_art_data = mysql_fetch_assoc($eve_art_data))
										{
											$reve_art_data['table_name'] = 'general_artist';
											$an_eve_art_data[] = $reve_art_data;
										}
									}
								}
							}
						}
					}

					if($row_gen_data['taggedcommunityevents']!="")
					{
						$exp_com_eve = explode(',',$row_gen_data['taggedcommunityevents']);
						if(count($exp_com_eve)>0 && $exp_com_eve!="")
						{
							for($e_i_a=0;$e_i_a<count($exp_com_eve);$e_i_a++)
							{
								if($exp_com_eve[$e_i_a]!="")
								{
									$eve_com_data = mysql_query("SELECT * FROM community_event WHERE id='".$exp_com_eve[$e_i_a]."' AND del_status=0 AND active=0");
									if(mysql_num_rows($eve_com_data)>0)
									{
										while($reve_com_data = mysql_fetch_assoc($eve_com_data))
										{
											$reve_com_data['table_name'] = 'general_community';
											$an_eve_com_data[] = $reve_com_data;
										}
									}
								}
							}
						}
					}
				}
			}
			
			if($log_id!=0 && isset($log_id) && $log_id!="")
			{
				$sql_gen_pro = mysql_query("SELECT * FROM general_user WHERE general_user_id='".$log_id."' AND delete_status=0 AND active=0 AND status=0");
				if(mysql_num_rows($sql_gen_pro)>0)
				{
					$row_gen_data = mysql_fetch_assoc($sql_gen_pro);
					if($row_gen_data['taggedartistprojects']!="")
					{
						$exp_art_pro = explode(',',$row_gen_data['taggedartistprojects']);
						$exp_art_pro = array_unique($exp_art_pro);
						
						if(count($exp_art_pro)>0 && $exp_art_pro!="")
						{
							for($p_i_c=0;$p_i_c<count($exp_art_pro);$p_i_c++)
							{
								if($exp_art_pro[$p_i_c]!="" && $exp_art_pro[$p_i_c]!=0)
								{
									$sql_1_cre = $this->get_event_tagged_by_other2_userrec($exp_art_pro[$p_i_c]);
									if($sql_1_cre!="")
									{
										if(mysql_num_rows($sql_1_cre)>0)
										{
											while($run = mysql_fetch_assoc($sql_1_cre))
											{
												$run['table'] = 'general_artist';
												//$ans_a_als[] = $run;
												$selarts2_tagged[] = $run;
											}
										}
									}
									
									$sql_2_cre = $this->get_event_ctagged_by_other2_userrec($exp_art_pro[$p_i_c]);
									if(mysql_num_rows($sql_2_cre)>0)
									{
										while($run_c = mysql_fetch_assoc($sql_2_cre))
										{
											$run_c['table'] = 'general_community';
											//$ans_c_als[]
											$selarts2_tagged[] = $run_c;
										}
									}
									
									$sql_3_cre = $this->get_event_tagged_by_other2_user($exp_art_pro[$p_i_c]);
									if($sql_3_cre!="")
									{
										if(mysql_num_rows($sql_3_cre)>0)
										{
											while($run = mysql_fetch_assoc($sql_3_cre))
											{
												$run['table'] = 'general_artist';
												//$ans_a_als[] = $run;
												$selarts2_tagged[] = $run;
											}
										}
									}
									
									$sql_4_cre = $this->get_event_ctagged_by_other2_user($exp_art_pro[$p_i_c]);
									if(mysql_num_rows($sql_4_cre)>0)
									{
										while($run_c = mysql_fetch_assoc($sql_4_cre))
										{
											$run_c['table'] = 'general_community';
											//$ans_c_als[]
											$selarts2_tagged[] = $run_c;
										}
									}
								}
							}
						}
					}
					
					if($row_gen_data['taggedcommunityprojects']!="")
					{
						$exp_com_pro = explode(',',$row_gen_data['taggedcommunityprojects']);
						if(count($exp_com_pro)>0 && $exp_com_pro!="")
						{
							for($p_i_a=0;$p_i_a<count($exp_com_pro);$p_i_a++)
							{
								if($exp_com_pro[$p_i_a]!="" && $exp_com_pro[$p_i_a]!=0)
								{
									$sql_1_cre = $this->get_cevent_tagged_by_other2_userrec($exp_com_pro[$p_i_a]);
									if($sql_1_cre!="")
									{
										if(mysql_num_rows($sql_1_cre)>0)
										{
											while($run = mysql_fetch_assoc($sql_1_cre))
											{
												$run['table'] = 'general_artist';
												//$ans_a_als[] = $run;
												$selcoms2_tagged[] = $run;
											}
										}
									}
									
									$sql_2_cre = $this->get_cevent_ctagged_by_other2_userrec($exp_com_pro[$p_i_a]);
									if(mysql_num_rows($sql_2_cre)>0)
									{
										while($run_c = mysql_fetch_assoc($sql_2_cre))
										{
											$run_c['table'] = 'general_community';
											//$ans_c_als[]
											$selcoms2_tagged[] = $run_c;
										}
									}
									
									$sql_3_cre = $this->get_cevent_tagged_by_other2_user($exp_com_pro[$p_i_a]);
									if($sql_3_cre!="")
									{
										if(mysql_num_rows($sql_3_cre)>0)
										{
											while($run = mysql_fetch_assoc($sql_3_cre))
											{
												$run_c['table'] = 'general_artist';
												//$ans_a_als[] = $run;
												$selcoms2_tagged[] = $run;
											}
										}
									}
									
									$sql_4_cre = $this->get_cevent_ctagged_by_other2_user($exp_com_pro[$p_i_a]);
									if(mysql_num_rows($sql_4_cre)>0)
									{
										while($run_c = mysql_fetch_assoc($sql_4_cre))
										{
											$run_c['table'] = 'general_community';
											//$ans_c_als[]
											$selcoms2_tagged[] = $run_c;
										}
									}
								}
							}
						}
					}
				}
			}
			
			$all_events = array_merge($ans_eve_a,$ans_eve_c,$an_eve_art_data,$an_eve_com_data,$selcoms2_tagged,$selarts2_tagged);
			return $all_events;
		}
		
		function get_all_projects($log_id,$a_id,$c_id)
		{
			$ans_pro_a = array();
			$ans_pro_c = array();
			$an_pro_art_data = array();
			$an_pro_com_data = array();
			$sel_art2tagged = array();
			$sel_com2tagged = array();
			
			if($a_id!=0 && isset($a_id) && $a_id!="")
			{
				$sql_pro_a = mysql_query("SELECT * FROM artist_project WHERE artist_id='".$a_id."' AND del_status=0 AND active=0");
				if(mysql_num_rows($sql_pro_a)>0)
				{
					while($row_pro_a = mysql_fetch_assoc($sql_pro_a))
					{
						$row_pro_a['table_name'] = 'general_artist';
						$ans_pro_a[] = $row_pro_a;
					}
				}
			}
			
			if($c_id!=0 && isset($c_id) && $c_id!="")
			{
				$sql_pro_c = mysql_query("SELECT * FROM community_project WHERE community_id='".$c_id."' AND del_status=0 AND active=0");
				if(mysql_num_rows($sql_pro_c)>0)
				{
					while($row_pro_c = mysql_fetch_assoc($sql_pro_c))
					{
						$row_pro_c['table_name'] = 'general_community';
						$ans_pro_c[] = $row_pro_c;
					}
				}
			}
			
			if($log_id!=0 && isset($log_id) && $log_id!="")
			{
				$sql_gen_pro = mysql_query("SELECT * FROM general_user WHERE general_user_id='".$log_id."' AND delete_status=0 AND active=0 AND status=0");
				if(mysql_num_rows($sql_gen_pro)>0)
				{
					$row_gen_data = mysql_fetch_assoc($sql_gen_pro);
					if($row_gen_data['taggedartistprojects']!="")
					{
						$exp_art_pro = explode(',',$row_gen_data['taggedartistprojects']);
						$exp_art_pro = array_unique($exp_art_pro);
						
						if(count($exp_art_pro)>0 && $exp_art_pro!="")
						{
							for($p_i_c=0;$p_i_c<count($exp_art_pro);$p_i_c++)
							{
								if($exp_art_pro[$p_i_c]!="" && $exp_art_pro[$p_i_c]!=0)
								{
									$pro_art_data = mysql_query("SELECT * FROM artist_project WHERE id='".$exp_art_pro[$p_i_c]."' AND del_status=0 AND active=0");
									if(mysql_num_rows($pro_art_data)>0)
									{
										while($rpro_art_data = mysql_fetch_assoc($pro_art_data))
										{
											$rpro_art_data['table_name'] = 'general_artist';
											$an_pro_art_data[] = $rpro_art_data;
										}
									}
									
									$sql_1_cre = $this->get_project_tagged_by_other2_user($exp_art_pro[$p_i_c]);
									if($sql_1_cre!="")
									{
										if(mysql_num_rows($sql_1_cre)>0)
										{
											while($run = mysql_fetch_assoc($sql_1_cre))
											{
												if($newres1['artist_id']!=$run['artist_id'])
												{
													$run['table_name'] = 'general_artist';
													$sel_art2tagged[] = $run;
												}
											}
										}
									}
									
									$sql_2_cre = $this->get_project_ctagged_by_other2_user($exp_art_pro[$p_i_c]);
									if($sql_2_cre!="")
									{
										if(mysql_num_rows($sql_2_cre)>0)
										{
											while($run_c = mysql_fetch_assoc($sql_2_cre))
											{
												if($newres1['community_id']!=$run_c['community_id'])
												{
													$run_c['table_name'] = 'general_community';
													$sel_art2tagged[] = $run_c;
												}
											}
										}
									}
								}
							}
						}
					}

					if($row_gen_data['taggedcommunityprojects']!="")
					{
						$exp_com_pro = explode(',',$row_gen_data['taggedcommunityprojects']);
						if(count($exp_com_pro)>0 && $exp_com_pro!="")
						{
							for($p_i_a=0;$p_i_a<count($exp_com_pro);$p_i_a++)
							{
								if($exp_com_pro[$p_i_a]!="" && $exp_com_pro[$p_i_a]!=0)
								{
									$pro_com_data = mysql_query("SELECT * FROM community_project WHERE id='".$exp_com_pro[$p_i_a]."' AND del_status=0 AND active=0");
									if(mysql_num_rows($pro_com_data)>0)
									{
										while($rpro_com_data = mysql_fetch_assoc($pro_com_data))
										{
											$rpro_com_data['table_name'] = 'general_community';
											$an_pro_com_data[] = $rpro_com_data;
										}
									}
									
									$sql_1_cre = $this->get_cproject_tagged_by_other2_user($exp_com_pro[$p_i_a]);
									if($sql_1_cre!="")
									{
										if(mysql_num_rows($sql_1_cre)>0)
										{
											while($run = mysql_fetch_assoc($sql_1_cre))
											{
												if($newres1['artist_id']!=$run['artist_id'])
												{
													$run['table_name'] = 'general_artist';
													$sel_com2tagged[] = $run;
												}
											}
										}
									}
									
									$sql_2_cre = $this->get_cproject_ctagged_by_other2_user($exp_com_pro[$p_i_a]);
									if($sql_2_cre!="")
									{
										if(mysql_num_rows($sql_2_cre)>0)
										{
											while($run_c = mysql_fetch_assoc($sql_2_cre))
											{
												if($newres1['community_id']!=$run_c['community_id'])
												{
													$run_c['table_name'] = 'general_community';
													$sel_com2tagged[] = $run_c;
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
			
			$all_projects = array_merge($ans_pro_a,$ans_pro_c,$an_pro_art_data,$an_pro_com_data,$sel_art2tagged,$sel_com2tagged);
			return $all_projects;
		}
		
		function get_all_galleries($log_id,$a_id,$c_id)
		{
			$ans_art_reg_gal = array();
			$ans_com_reg_gal = array();
			$ans_med_gal = array();
			$tag_med_gal = array();
			
			/* if($a_id!=0 && isset($a_id) && $a_id!="")
			{
				$sql_gal_reg_a = mysql_query("SELECT * FROM general_artist_gallery WHERE profile_id='".$a_id."'");
				if(mysql_num_rows($sql_gal_reg_a)>0)
				{	
					$row_a_reg = mysql_fetch_assoc($sql_gal_reg_a);
					
					$sql_gal_reg_a_t = mysql_query("SELECT * FROM general_artist_gallery_list WHERE gallery_id='".$row_a_reg['gallery_id']."'");
					
					while($row_art_reg_gal = mysql_fetch_assoc($sql_gal_reg_a_t))
					{
						$row_art_reg_gal['table_name'] = 'general_artist';
						$ans_art_reg_gal[] = $row_art_reg_gal;
					}
					
				}
			}
			
			if($c_id!=0 && isset($c_id) && $c_id!="")
			{	
				$sql_gal_reg_c = mysql_query("SELECT * FROM general_community_gallery WHERE profile_id='".$c_id."'");
				if(mysql_num_rows($sql_gal_reg_c)>0)
				{	
					$row_c_reg = mysql_fetch_assoc($sql_gal_reg_c);
					
					$sql_gal_reg_c_t = mysql_query("SELECT * FROM general_community_gallery_list WHERE gallery_id='".$row_c_reg['gallery_id']."'");
					while($row_com_reg_gal = mysql_fetch_assoc($sql_gal_reg_c_t))
					{
						$row_com_reg_gal['table_name'] = 'general_community';
						$ans_com_reg_gal[] = $row_com_reg_gal;
					}
					
				}
			} */
			
			if($log_id!=0 && isset($log_id) && $log_id!="")
			{
				$sql_gal_med = mysql_query("SELECT * FROM general_media WHERE general_user_id='".$log_id."' AND media_type='113' AND delete_status=0 AND media_status=0");
				if(mysql_num_rows($sql_gal_med)>0)
				{
					while($row_med = mysql_fetch_assoc($sql_gal_med))
					{
						$row_med['table_name'] = 'general_media';
						$ans_med_gal[] = $row_med;
					}
				}
				
				$sql_tag_gal = mysql_query("SELECT * FROM general_user WHERE general_user_id='".$log_id."' AND delete_status=0 AND active=0 AND status=0");
				if(mysql_num_rows($sql_tag_gal)>0)
				{
					$row_gen_data = mysql_fetch_assoc($sql_tag_gal);
					if($row_gen_data['accepted_media_id']!="")
					{
						$exp_med_tag = explode(',',$row_gen_data['accepted_media_id']);
						if(count($exp_med_tag)>0 && $exp_med_tag!="")
						{
							for($tag_gm=0;$tag_gm<count($exp_med_tag);$tag_gm++)
							{
								$sql_get_gal = mysql_query("SELECT * FROM general_media WHERE id='".$exp_med_tag[$tag_gm]."' AND media_type='113' AND delete_status=0 AND media_status=0");
								if(mysql_num_rows($sql_get_gal)>0)
								{
									while($row_med_tag = mysql_fetch_assoc($sql_get_gal))
									{
										$row_med_tag['table_name'] = 'general_media';
										$tag_med_gal[] = $row_med_tag;
									}
								}
							}
						}
					}
				}
			}
			
			$all_galleries = array_merge($ans_art_reg_gal,$ans_com_reg_gal,$ans_med_gal,$tag_med_gal);
			return $all_galleries;
		}
		
		function get_all_songs($g_id,$a_id,$c_id)
		{
			$song_art_reg = array();
			$song_com_reg = array();
			$song_gen_reg = array();
			$tag_med_song = array();
			
			/* if($a_id!=0 && isset($a_id) && $a_id!="")
			{
				$sql_son_reg_a = mysql_query("SELECT * FROM general_artist_audio WHERE profile_id='".$a_id."'");
				if(mysql_num_rows($sql_son_reg_a)>0)
				{
					while($row_art_reg = mysql_fetch_assoc($sql_son_reg_a))
					{
						$row_art_reg['table_name'] = 'general_artist';
						$song_art_reg[] = $row_art_reg;
					}
				}
			}
			
			if($c_id!=0 && isset($c_id) && $c_id!="")
			{	
				$sql_son_reg_c = mysql_query("SELECT * FROM general_community_audio WHERE profile_id='".$c_id."'");
				if(mysql_num_rows($sql_son_reg_c)>0)
				{
					while($row_com_reg = mysql_fetch_assoc($sql_son_reg_c))
					{
						$row_com_reg['table_name'] = 'general_community';
						$song_com_reg[] = $row_com_reg;
					}
				}
			} */
			
			if($g_id!=0 && isset($g_id) && $g_id!="")
			{
				$sql_media = mysql_query("SELECT * FROM general_media WHERE general_user_id='".$g_id."' AND media_type='114' AND delete_status=0 AND media_status=0");
				if(mysql_num_rows($sql_media)>0)
				{
					while($row_gen_reg = mysql_fetch_assoc($sql_media))
					{
						$row_gen_reg['table_name'] = 'general_media';
						$trck = $this->get_media_track($row_gen_reg['id']);
						$row_gen_reg['track'] = $trck['track'];
						$song_gen_reg[] = $row_gen_reg;
					}
				}
				
				$sql_tag_song = mysql_query("SELECT * FROM general_user WHERE general_user_id='".$g_id."' AND delete_status=0 AND active=0 AND status=0");
				if(mysql_num_rows($sql_tag_song)>0)
				{
					$row_gen_data = mysql_fetch_assoc($sql_tag_song);
					if($row_gen_data['accepted_media_id']!="")
					{
						$exp_med_tag = explode(',',$row_gen_data['accepted_media_id']);
						if(count($exp_med_tag)>0 && $exp_med_tag!="")
						{
							for($tag_sm=0;$tag_sm<count($exp_med_tag);$tag_sm++)
							{
								$sql_get_song = mysql_query("SELECT * FROM general_media WHERE id='".$exp_med_tag[$tag_sm]."' AND media_type='114' AND delete_status=0 AND media_status=0");
								if(mysql_num_rows($sql_get_song)>0)
								{
									while($row_med_tag = mysql_fetch_assoc($sql_get_song))
									{
										$row_med_tag['table_name'] = 'general_media';
										$trck = $this->get_media_track($row_med_tag['id']);
										$row_med_tag['track'] = $trck['track'];
										$tag_med_song[] = $row_med_tag;
									}
								}
							}
						}
					}
				}
			}
			
			/* $date = date("Y-m-d");
			echo $date;
			
			echo "SELECT * FROM email_subscription WHERE email='".$_SESSION['login_email']."' AND delete_status=1";
			$sql_sub = mysql_query("SELECT * FROM email_subscription WHERE email='".$_SESSION['login_email']."' AND delete_status=1 AND expirydate>$date");
			if(mysql_num_rows($sql_sub)>0)
			{
				$ans_sub = mysql_fetch_assoc($sql_sub);
				
				if($ans_sub['related_type']=='artist')
				{
					$sql_meds = mysql_query("SELECT * FROM general_media WHERE (creator_info='general_artist|".$ans_sub['related_id']."' OR from_info='general_artist|".$ans_sub['related_id']."') AND (sharing_preference=1 OR sharing_preference=2) AND media_status=0 AND delete_status=0");					
				}
				elseif($ans_sub['related_type']=='community')
				{
					$sql_meds = mysql_query("SELECT * FROM general_media WHERE (creator_info='general_community|".$ans_sub['related_id']."' OR from_info='general_community|".$ans_sub['related_id']."') AND (sharing_preference=1 OR sharing_preference=2) AND media_status=0 AND delete_status=0");
				}
				elseif($ans_sub['related_type']=='community_project' || $ans_sub['related_type']=='community_event' || $ans_sub['related_type']=='artist_project' || $ans_sub['related_type']=='artist_event')
				{
					$rel_id = explode('_',$ans_sub['related_id']);
					echo "SELECT * FROM general_media WHERE (creator_info='".$ans_sub['related_type']."|".$rel_id[1]."' OR from_info='".$ans_sub['related_type']."|".$rel_id[1]."') AND (sharing_preference=1 OR sharing_preference=2) AND media_status=0 AND delete_status=0";
					
					$sql_meds = mysql_query("SELECT * FROM general_media WHERE (creator_info='".$ans_sub['related_type']."|".$rel_id[1]."' OR from_info='".$ans_sub['related_type']."|".$rel_id[1]."') AND (sharing_preference=1 OR sharing_preference=2) AND media_status=0 AND delete_status=0");
				} */
				
				/* if(isset($sql_meds))
				{
					if(mysql_num_rows($sql_meds)>0)
					{
						
					}
				} 
			} */	
			
			$all_songs = array_merge($song_art_reg,$song_com_reg,$song_gen_reg,$tag_med_song);
			return $all_songs;
		}
		
		function get_all_videos($log_id,$a_id,$c_id)
		{
			$video_art_reg = array();
			$video_com_reg = array();
			$video_gen_reg = array();
			$tag_med_video = array();
			
			/* if($a_id!=0 && isset($a_id) && $a_id!="")
			{
				$sql_vid_reg_a = mysql_query("SELECT * FROM general_artist_video WHERE profile_id='".$a_id."'");
				if(mysql_num_rows($sql_vid_reg_a)>0)
				{
					while($row_art_reg = mysql_fetch_assoc($sql_vid_reg_a))
					{
						$row_art_reg['table_name'] = 'general_artist';
						$video_art_reg[] = $row_art_reg;
					}
				}
			}
			
			if($c_id!=0 && isset($c_id) && $c_id!="")
			{	
				$sql_vid_reg_c = mysql_query("SELECT * FROM general_community_video WHERE profile_id='".$c_id."'");
				if(mysql_num_rows($sql_vid_reg_c)>0)
				{
					while($row_com_reg = mysql_fetch_assoc($sql_vid_reg_c))
					{
						$row_com_reg['table_name'] = 'general_community';
						$video_com_reg[] = $row_com_reg;
					}
				}
			} */
			
			if($log_id!=0 && isset($log_id) && $log_id!="")
			{
				$sql_media = mysql_query("SELECT * FROM general_media WHERE general_user_id='".$log_id."' AND media_type='115' AND delete_status=0 AND media_status=0");
				if(mysql_num_rows($sql_media)>0)
				{
					while($row_gen_reg = mysql_fetch_assoc($sql_media))
					{
						$row_gen_reg['table_name'] = 'general_media';
						$video_gen_reg[] = $row_gen_reg;
					}
				}
				
				$sql_tag_video = mysql_query("SELECT * FROM general_user WHERE general_user_id='".$log_id."' AND delete_status=0 AND active=0 AND status=0");
				if(mysql_num_rows($sql_tag_video)>0)
				{
					$row_gen_data = mysql_fetch_assoc($sql_tag_video);
					if($row_gen_data['accepted_media_id']!="")
					{
						$exp_med_tag = explode(',',$row_gen_data['accepted_media_id']);
						if(count($exp_med_tag)>0 && $exp_med_tag!="")
						{
							for($tag_vm=0;$tag_vm<count($exp_med_tag);$tag_vm++)
							{
								$sql_get_vid = mysql_query("SELECT * FROM general_media WHERE id='".$exp_med_tag[$tag_vm]."' AND media_type='115' AND delete_status=0 AND media_status=0");
								if(mysql_num_rows($sql_get_vid)>0)
								{
									while($row_med_tag = mysql_fetch_assoc($sql_get_vid))
									{
										$row_med_tag['table_name'] = 'general_media';
										$tag_med_video[] = $row_med_tag;
									}
								}
							}
						}
					}
				}
			}
			
			$all_videos = array_merge($video_art_reg,$video_com_reg,$video_gen_reg,$tag_med_video);
			return $all_videos;
		}
		
		function insert_posts($login_id,$post_list,$list_epm,$description_post,$allowh_post)
		{
			$date_time = date('Y-m-d H:i:s');
			$sql_posts = mysql_query("INSERT INTO post_info (general_user_id,profile_type,media,description,display_to,date_time) VALUES ('$login_id','".$post_list."','".$list_epm."','".$description_post."','".$allowh_post."','".$date_time."')");
		}
		
		function update_posts($post_list,$list_epm,$description_post,$allowh_post,$post_id)
		{
			$date_time = date('Y-m-d H:i:s');
			$sql_posts = mysql_query("update post_info set profile_type = '".$post_list."',media = '".$list_epm."',description = '".$description_post."',display_to = '".$allowh_post."',date_time = '".$date_time."' where id='".$post_id."'");
		}
		
		function only_creator_upevent()
		{
			$id = $this->sel_general();
			$date = date("Y-m-d");
			
			$var_type_1 = 'general_artist';
			$var_type_2 = 'general_community';
			$var_type_3 = 'artist_project';
			$var_type_4 = 'community_project';
			
			$artm = array();
			$arta = array();
			$comm = array();
			$coma = array();
			
			$artme = array();
			$artae = array();
			$comme = array();
			$comae = array();
			
			$art_p_ao = array();
			$com_p_ao = array();
			$art_p_co = array();
			$com_p_co = array();
			
			$art_e_ao = array();
			$com_e_ao = array();
			$art_e_co = array();
			$com_e_co = array();
			
			$pal_1 = array();
			$pal_2 = array();
			
			if($id['artist_id']!=0 && !empty($id['artist_id']))
			{
				$chk_type = $var_type_1.'|'.$id['artist_id'];
				
				$sql_get_allp = mysql_query("SELECT * FROM artist_project WHERE artist_id='".$id['artist_id']."' AND del_status=0 AND active=0");
				if(mysql_num_rows($sql_get_allp)>0)
				{
					while($rowal = mysql_fetch_assoc($sql_get_allp))
					{
						$pal_1[] = $rowal;
					}
				}
				
				
				
				 if(!empty($pal_1) && count($pal_1))
				{
					for($pl=0;$pl<count($pal_1);$pl++)
					{
						$chk_typep_1 = 'artist_project|'.$pal_1[$pl]['id'];
						
						$sql = mysql_query("SELECT * FROM artist_event WHERE creators_info='".$chk_typep_1."' AND del_status=0 AND active=0 AND date>='".$date."'");
						if(mysql_num_rows($sql)>0)
						{
							while($row_po = mysql_fetch_assoc($sql))
							{
								if($row_po['artist_id']!=$id['artist_id'])
								{
									$row_po['table_name'] = 'general_artist';
									$art_e_ao[] = $row_po;
								}
							}
						}
						
						$sql = mysql_query("SELECT * FROM community_event WHERE creators_info='".$chk_typep_1."' AND del_status=0 AND active=0 AND date>='".$date."'");
						if(mysql_num_rows($sql)>0)
						{
							while($row_poo = mysql_fetch_assoc($sql))
							{
								if($row_poo['community_id']!=$id['community_id'])
								{
									$row_poo['table_name'] = 'general_community';
									$com_e_ao[] = $row_poo;
								}
							}
						}
					}
				} 

				/*$sql_pro_arta = mysql_query("SELECT * FROM artist_project WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0");
				if(mysql_num_rows($sql_pro_arta)>0)
				{
					while($rowaa = mysql_fetch_assoc($sql_pro_arta))
					{
						if($rowaa['artist_id']!=$id['artist_id'])
						{
							$rowaa['id'] = $rowaa['id'].'~art';
							$arta[] = $rowaa;
						}
					}
				}*/
				
				$sql_pro_arta = mysql_query("SELECT * FROM artist_event WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0 AND date>='".$date."'");
				if(mysql_num_rows($sql_pro_arta)>0)
				{
					while($rowaa = mysql_fetch_assoc($sql_pro_arta))
					{
						if($rowaa['artist_id']!=$id['artist_id'])
						{
							$rowaa['table_name'] = 'general_artist';
							$artae[] = $rowaa;
						}
					}
				} 
				
				/*$sql_pro_coma = mysql_query("SELECT * FROM community_project WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0");
				if(mysql_num_rows($sql_pro_coma)>0)
				{
					while($rowam = mysql_fetch_assoc($sql_pro_coma))
					{
						if($rowam['community_id']!=$id['community_id'])
						{
							$rowam['id'] = $rowam['id'].'~com';
							$coma[] = $rowam;
						}
					}
				}*/
				
				$sql_pro_coma = mysql_query("SELECT * FROM community_event WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0 AND date>='".$date."'");
				if(mysql_num_rows($sql_pro_coma)>0)
				{
					while($rowam = mysql_fetch_assoc($sql_pro_coma))
					{
						if($rowam['community_id']!=$id['community_id'])
						{
							$rowam['table_name'] = 'general_community';
							$comae[] = $rowam;
						}
					}
				} 
			}
			
			if($id['community_id']!=0 && !empty($id['community_id']))
			{
				/*$chk_type = $var_type_2.'|'.$id['community_id'];
				$sql_pro_artm = mysql_query("SELECT * FROM artist_project WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0");
				if(mysql_num_rows($sql_pro_artm)>0)
				{
					while($rowcm = mysql_fetch_assoc($sql_pro_artm))
					{
						if($rowcm['artist_id']!=$id['artist_id'])
						{
							$rowcm['id'] = $rowcm['id'].'~art';
							$artm[] = $rowcm;
						}
					}
				}*/
				
				 $chk_type = $var_type_2.'|'.$id['community_id'];
				$sql_pro_artm = mysql_query("SELECT * FROM artist_event WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0 AND date>='".$date."'");
				if(mysql_num_rows($sql_pro_artm)>0)
				{
					while($rowcm = mysql_fetch_assoc($sql_pro_artm))
					{
						if($rowcm['artist_id']!=$id['artist_id'])
						{
							$rowcm['table_name'] = 'general_artist';
							$artme[] = $rowcm;
						}
					}
				} 
				
				/*$sql_pro_comm = mysql_query("SELECT * FROM community_project WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0");
				if(mysql_num_rows($sql_pro_comm)>0)
				{
					while($rowca = mysql_fetch_assoc($sql_pro_comm))
					{
						if($rowca['community_id']!=$id['community_id'])
						{
							$rowca['id'] = $rowca['id'].'~com';
							$comm[] = $rowca;
						}
					}
				}*/
				
				 $sql_pro_comm = mysql_query("SELECT * FROM community_event WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0 AND date>='".$date."'");
				if(mysql_num_rows($sql_pro_comm)>0)
				{
					while($rowca = mysql_fetch_assoc($sql_pro_comm))
					{
						if($rowca['community_id']!=$id['community_id'])
						{
							$rowca['table_name'] = 'general_community';
							$comme[] = $rowca;
						}
					}
				} 
				
				$sql_get_allp = mysql_query("SELECT * FROM community_project WHERE community_id='".$id['community_id']."' AND del_status=0 AND active=0");
				if(mysql_num_rows($sql_get_allp)>0)
				{
					while($rowal = mysql_fetch_assoc($sql_get_allp))
					{
						$pal_2[] = $rowal;
					}
				}
				
				/*if(!empty($pal_2) && count($pal_2))
				{
					for($pl2=0;$pl2<count($pal_2);$pl2++)
					{
						$chk_typep_2 = 'community_project|'.$pal_2[$pl2]['id'];
						$sql = mysql_query("SELECT * FROM artist_project WHERE creators_info='".$chk_typep_2."' AND del_status=0 AND active=0");
						if(mysql_num_rows($sql)>0)
						{
							while($row_po = mysql_fetch_assoc($sql))
							{
								if($row_po['artist_id']!=$id['artist_id'])
								{
									$row_po['id'] = $row_po['id'].'~art';
									$art_p_co[] = $row_po;
								}
							}
						}
						
						$sql = mysql_query("SELECT * FROM community_project WHERE creators_info='".$chk_typep_2."' AND del_status=0 AND active=0");
						if(mysql_num_rows($sql)>0)
						{
							while($row_poo = mysql_fetch_assoc($sql))
							{
								if($row_poo['community_id']!=$id['community_id'])
								{
									$row_poo['id'] = $row_poo['id'].'~com';
									$com_p_co[] = $row_poo;
								}
							}
						}
					}
				}*/
				
				 if(!empty($pal_2) && count($pal_2))
				{
					for($pl2=0;$pl2<count($pal_2);$pl2++)
					{
						$chk_typep_2 = 'community_project|'.$pal_2[$pl2]['id'];
						$sql = mysql_query("SELECT * FROM artist_event WHERE creators_info='".$chk_typep_2."' AND del_status=0 AND active=0 AND date>='".$date."'");
						if(mysql_num_rows($sql)>0)
						{
							while($row_po = mysql_fetch_assoc($sql))
							{
								if($row_po['artist_id']!=$id['artist_id'])
								{
									$row_po['table_name'] = 'general_artist';
									$art_e_co[] = $row_po;
								}
							}
						}
						
						$sql = mysql_query("SELECT * FROM community_event WHERE creators_info='".$chk_typep_2."' AND del_status=0 AND active=0 AND date>='".$date."'");
						if(mysql_num_rows($sql)>0)
						{
							while($row_poo = mysql_fetch_assoc($sql))
							{
								if($row_poo['community_id']!=$id['community_id'])
								{
									$row_poo['table_name'] = 'general_community';
									$com_e_co[] = $row_poo;
								}
							}
						}
					}
				} 
			}
			
			/* var_dump($artm);
			var_dump($arta);
			var_dump($comm);
			var_dump($coma);
			var_dump($art_p_ao);
			var_dump($com_p_ao);
			var_dump($art_p_co);
			var_dump($com_p_co); */
			
			$totals = array();
			$totals = array_merge($artm,$arta,$comm,$coma,$art_p_ao,$com_p_ao,$art_p_co,$com_p_co,$artme,$artae,$comme,$comae,$art_e_ao,$com_e_ao,$art_e_co,$com_e_co);
			return $totals;
		}
		
		function only_creator_recevent()
		{
			$id = $this->sel_general();
			$date = date("Y-m-d");
			
			$var_type_1 = 'general_artist';
			$var_type_2 = 'general_community';
			$var_type_3 = 'artist_project';
			$var_type_4 = 'community_project';
			
			$artm = array();
			$arta = array();
			$comm = array();
			$coma = array();
			
			$artme = array();
			$artae = array();
			$comme = array();
			$comae = array();
			
			$art_p_ao = array();
			$com_p_ao = array();
			$art_p_co = array();
			$com_p_co = array();
			
			$art_e_ao = array();
			$com_e_ao = array();
			$art_e_co = array();
			$com_e_co = array();
			
			$pal_1 = array();
			$pal_2 = array();
			
			if($id['artist_id']!=0 && !empty($id['artist_id']))
			{
				$chk_type = $var_type_1.'|'.$id['artist_id'];
				
				$sql_get_allp = mysql_query("SELECT * FROM artist_project WHERE artist_id='".$id['artist_id']."' AND del_status=0 AND active=0");
				if(mysql_num_rows($sql_get_allp)>0)
				{
					while($rowal = mysql_fetch_assoc($sql_get_allp))
					{
						$pal_1[] = $rowal;
					}
				}
				
				
				
				 if(!empty($pal_1) && count($pal_1))
				{
					for($pl=0;$pl<count($pal_1);$pl++)
					{
						$chk_typep_1 = 'artist_project|'.$pal_1[$pl]['id'];
						
						$sql = mysql_query("SELECT * FROM artist_event WHERE creators_info='".$chk_typep_1."' AND del_status=0 AND active=0 AND date<'".$date."'");
						if(mysql_num_rows($sql)>0)
						{
							while($row_po = mysql_fetch_assoc($sql))
							{
								if($row_po['artist_id']!=$id['artist_id'])
								{
									$row_po['table_name'] = 'general_artist';
									$art_e_ao[] = $row_po;
								}
							}
						}
						
						$sql = mysql_query("SELECT * FROM community_event WHERE creators_info='".$chk_typep_1."' AND del_status=0 AND active=0 AND date<'".$date."'");
						if(mysql_num_rows($sql)>0)
						{
							while($row_poo = mysql_fetch_assoc($sql))
							{
								if($row_poo['community_id']!=$id['community_id'])
								{
									$row_poo['table_name'] = 'general_community';
									$com_e_ao[] = $row_poo;
								}
							}
						}
					}
				} 

				/*$sql_pro_arta = mysql_query("SELECT * FROM artist_project WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0");
				if(mysql_num_rows($sql_pro_arta)>0)
				{
					while($rowaa = mysql_fetch_assoc($sql_pro_arta))
					{
						if($rowaa['artist_id']!=$id['artist_id'])
						{
							$rowaa['id'] = $rowaa['id'].'~art';
							$arta[] = $rowaa;
						}
					}
				}*/
				
				$sql_pro_arta = mysql_query("SELECT * FROM artist_event WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0 AND date<'".$date."'");
				if(mysql_num_rows($sql_pro_arta)>0)
				{
					while($rowaa = mysql_fetch_assoc($sql_pro_arta))
					{
						if($rowaa['artist_id']!=$id['artist_id'])
						{
							$rowaa['table_name'] = 'general_artist';
							$artae[] = $rowaa;
						}
					}
				} 
				
				/*$sql_pro_coma = mysql_query("SELECT * FROM community_project WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0");
				if(mysql_num_rows($sql_pro_coma)>0)
				{
					while($rowam = mysql_fetch_assoc($sql_pro_coma))
					{
						if($rowam['community_id']!=$id['community_id'])
						{
							$rowam['id'] = $rowam['id'].'~com';
							$coma[] = $rowam;
						}
					}
				}*/
				
				$sql_pro_coma = mysql_query("SELECT * FROM community_event WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0 AND date<'".$date."'");
				if(mysql_num_rows($sql_pro_coma)>0)
				{
					while($rowam = mysql_fetch_assoc($sql_pro_coma))
					{
						if($rowam['community_id']!=$id['community_id'])
						{
							$rowam['table_name'] = 'general_community';
							$comae[] = $rowam;
						}
					}
				} 
			}
			
			if($id['community_id']!=0 && !empty($id['community_id']))
			{
				/*$chk_type = $var_type_2.'|'.$id['community_id'];
				$sql_pro_artm = mysql_query("SELECT * FROM artist_project WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0");
				if(mysql_num_rows($sql_pro_artm)>0)
				{
					while($rowcm = mysql_fetch_assoc($sql_pro_artm))
					{
						if($rowcm['artist_id']!=$id['artist_id'])
						{
							$rowcm['id'] = $rowcm['id'].'~art';
							$artm[] = $rowcm;
						}
					}
				}*/
				
				 $chk_type = $var_type_2.'|'.$id['community_id'];
				$sql_pro_artm = mysql_query("SELECT * FROM artist_event WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0 AND date<'".$date."'");
				if(mysql_num_rows($sql_pro_artm)>0)
				{
					while($rowcm = mysql_fetch_assoc($sql_pro_artm))
					{
						if($rowcm['artist_id']!=$id['artist_id'])
						{
							$rowcm['table_name'] = 'general_artist';
							$artme[] = $rowcm;
						}
					}
				} 
				
				/*$sql_pro_comm = mysql_query("SELECT * FROM community_project WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0");
				if(mysql_num_rows($sql_pro_comm)>0)
				{
					while($rowca = mysql_fetch_assoc($sql_pro_comm))
					{
						if($rowca['community_id']!=$id['community_id'])
						{
							$rowca['id'] = $rowca['id'].'~com';
							$comm[] = $rowca;
						}
					}
				}*/
				
				 $sql_pro_comm = mysql_query("SELECT * FROM community_event WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0 AND date<'".$date."'");
				if(mysql_num_rows($sql_pro_comm)>0)
				{
					while($rowca = mysql_fetch_assoc($sql_pro_comm))
					{
						if($rowca['community_id']!=$id['community_id'])
						{
							$rowca['table_name'] = 'general_community';
							$comme[] = $rowca;
						}
					}
				} 
				
				$sql_get_allp = mysql_query("SELECT * FROM community_project WHERE community_id='".$id['community_id']."' AND del_status=0 AND active=0");
				if(mysql_num_rows($sql_get_allp)>0)
				{
					while($rowal = mysql_fetch_assoc($sql_get_allp))
					{
						$pal_2[] = $rowal;
					}
				}
				
				/*if(!empty($pal_2) && count($pal_2))
				{
					for($pl2=0;$pl2<count($pal_2);$pl2++)
					{
						$chk_typep_2 = 'community_project|'.$pal_2[$pl2]['id'];
						$sql = mysql_query("SELECT * FROM artist_project WHERE creators_info='".$chk_typep_2."' AND del_status=0 AND active=0");
						if(mysql_num_rows($sql)>0)
						{
							while($row_po = mysql_fetch_assoc($sql))
							{
								if($row_po['artist_id']!=$id['artist_id'])
								{
									$row_po['id'] = $row_po['id'].'~art';
									$art_p_co[] = $row_po;
								}
							}
						}
						
						$sql = mysql_query("SELECT * FROM community_project WHERE creators_info='".$chk_typep_2."' AND del_status=0 AND active=0");
						if(mysql_num_rows($sql)>0)
						{
							while($row_poo = mysql_fetch_assoc($sql))
							{
								if($row_poo['community_id']!=$id['community_id'])
								{
									$row_poo['id'] = $row_poo['id'].'~com';
									$com_p_co[] = $row_poo;
								}
							}
						}
					}
				}*/
				
				 if(!empty($pal_2) && count($pal_2))
				{
					for($pl2=0;$pl2<count($pal_2);$pl2++)
					{
						$chk_typep_2 = 'community_project|'.$pal_2[$pl2]['id'];
						$sql = mysql_query("SELECT * FROM artist_event WHERE creators_info='".$chk_typep_2."' AND del_status=0 AND active=0 AND date<'".$date."'");
						if(mysql_num_rows($sql)>0)
						{
							while($row_po = mysql_fetch_assoc($sql))
							{
								if($row_po['artist_id']!=$id['artist_id'])
								{
									$row_po['table_name'] = 'general_artist';
									$art_e_co[] = $row_po;
								}
							}
						}
						
						$sql = mysql_query("SELECT * FROM community_event WHERE creators_info='".$chk_typep_2."' AND del_status=0 AND active=0 AND date<'".$date."'");
						if(mysql_num_rows($sql)>0)
						{
							while($row_poo = mysql_fetch_assoc($sql))
							{
								if($row_poo['community_id']!=$id['community_id'])
								{
									$row_poo['table_name'] = 'general_community';
									$com_e_co[] = $row_poo;
								}
							}
						}
					}
				} 
			}
			
			/* var_dump($artm);
			var_dump($arta);
			var_dump($comm);
			var_dump($coma);
			var_dump($art_p_ao);
			var_dump($com_p_ao);
			var_dump($art_p_co);
			var_dump($com_p_co); */
			
			$totals = array();
			$totals = array_merge($artm,$arta,$comm,$coma,$art_p_ao,$com_p_ao,$art_p_co,$com_p_co,$artme,$artae,$comme,$comae,$art_e_ao,$com_e_ao,$art_e_co,$com_e_co);
			return $totals;
		}
		
		function only_creator_this()
		{
			$id = $this->sel_general();
			$var_type_1 = 'general_artist';
			$var_type_2 = 'general_community';
			$var_type_3 = 'artist_project';
			$var_type_4 = 'community_project';
			
			$artm = array();
			$arta = array();
			$comm = array();
			$coma = array();
			
			$artme = array();
			$artae = array();
			$comme = array();
			$comae = array();
			
			$art_p_ao = array();
			$com_p_ao = array();
			$art_p_co = array();
			$com_p_co = array();
			
			$art_e_ao = array();
			$com_e_ao = array();
			$art_e_co = array();
			$com_e_co = array();
			
			$pal_1 = array();
			$pal_2 = array();
			
			if($id['artist_id']!=0 && !empty($id['artist_id']))
			{
				$chk_type = $var_type_1.'|'.$id['artist_id'];
				
				$sql_get_allp = mysql_query("SELECT * FROM artist_project WHERE artist_id='".$id['artist_id']."' AND del_status=0 AND active=0");
				if(mysql_num_rows($sql_get_allp)>0)
				{
					while($rowal = mysql_fetch_assoc($sql_get_allp))
					{
						$pal_1[] = $rowal;
					}
				}
				
				if(!empty($pal_1) && count($pal_1))
				{
					for($pl=0;$pl<count($pal_1);$pl++)
					{
						$chk_typep_1 = 'artist_project|'.$pal_1[$pl]['id'];
						
						$sql = mysql_query("SELECT * FROM artist_project WHERE creators_info='".$chk_typep_1."' AND del_status=0 AND active=0");
						if(mysql_num_rows($sql)>0)
						{
							while($row_po = mysql_fetch_assoc($sql))
							{
								if($row_po['artist_id']!=$id['artist_id'])
								{
									$row_po['table_name'] = 'general_artist';
									$art_p_ao[] = $row_po;
								}
							}
						}
						
						$sql = mysql_query("SELECT * FROM community_project WHERE creators_info='".$chk_typep_1."' AND del_status=0 AND active=0");
						if(mysql_num_rows($sql)>0)
						{
							while($row_poo = mysql_fetch_assoc($sql))
							{
								if($row_poo['community_id']!=$id['community_id'])
								{
									$row_poo['table_name'] = 'general_community';
									$com_p_ao[] = $row_poo;
								}
							}
						}
					}
				}
				
				/* if(!empty($pal_1) && count($pal_1))
				{
					for($pl=0;$pl<count($pal_1);$pl++)
					{
						$chk_typep_1 = 'artist_project|'.$pal_1[$pl]['id'];
						
						$sql = mysql_query("SELECT * FROM artist_event WHERE creators_info='".$chk_typep_1."' AND del_status=0 AND active=0");
						if(mysql_num_rows($sql)>0)
						{
							while($row_po = mysql_fetch_assoc($sql))
							{
								if($row_po['artist_id']!=$id['artist_id'])
								{
									$art_e_ao[] = $row_po;
								}
							}
						}
						
						$sql = mysql_query("SELECT * FROM community_event WHERE creators_info='".$chk_typep_1."' AND del_status=0 AND active=0");
						if(mysql_num_rows($sql)>0)
						{
							while($row_poo = mysql_fetch_assoc($sql))
							{
								if($row_poo['community_id']!=$id['community_id'])
								{
									$com_e_ao[] = $row_poo;
								}
							}
						}
					}
				} */

				$sql_pro_arta = mysql_query("SELECT * FROM artist_project WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0");
				if(mysql_num_rows($sql_pro_arta)>0)
				{
					while($rowaa = mysql_fetch_assoc($sql_pro_arta))
					{
						if($rowaa['artist_id']!=$id['artist_id'])
						{
							$rowaa['table_name'] = 'general_artist';
							$arta[] = $rowaa;
						}
					}
				}
				
				/* $sql_pro_arta = mysql_query("SELECT * FROM artist_event WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0");
				if(mysql_num_rows($sql_pro_arta)>0)
				{
					while($rowaa = mysql_fetch_assoc($sql_pro_arta))
					{
						if($rowaa['artist_id']!=$id['artist_id'])
						{
							$artae[] = $rowaa;
						}
					}
				} */
				
				$sql_pro_coma = mysql_query("SELECT * FROM community_project WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0");
				if(mysql_num_rows($sql_pro_coma)>0)
				{
					while($rowam = mysql_fetch_assoc($sql_pro_coma))
					{
						if($rowam['community_id']!=$id['community_id'])
						{
							$rowam['table_name'] = 'general_community';
							$coma[] = $rowam;
						}
					}
				}
				
				/* $sql_pro_coma = mysql_query("SELECT * FROM community_event WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0");
				if(mysql_num_rows($sql_pro_coma)>0)
				{
					while($rowam = mysql_fetch_assoc($sql_pro_coma))
					{
						if($rowam['community_id']!=$id['community_id'])
						{
							$comae[] = $rowam;
						}
					}
				} */
			}
			
			if($id['community_id']!=0 && !empty($id['community_id']))
			{
				$chk_type = $var_type_2.'|'.$id['community_id'];
				$sql_pro_artm = mysql_query("SELECT * FROM artist_project WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0");
				if(mysql_num_rows($sql_pro_artm)>0)
				{
					while($rowcm = mysql_fetch_assoc($sql_pro_artm))
					{
						if($rowcm['artist_id']!=$id['artist_id'])
						{
							$rowcm['table_name'] = 'general_artist';
							$artm[] = $rowcm;
						}
					}
				}
				
				/* $chk_type = $var_type_2.'|'.$id['community_id'];
				$sql_pro_artm = mysql_query("SELECT * FROM artist_event WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0");
				if(mysql_num_rows($sql_pro_artm)>0)
				{
					while($rowcm = mysql_fetch_assoc($sql_pro_artm))
					{
						if($rowcm['artist_id']!=$id['artist_id'])
						{
							$artme[] = $rowcm;
						}
					}
				} */
				
				$sql_pro_comm = mysql_query("SELECT * FROM community_project WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0");
				if(mysql_num_rows($sql_pro_comm)>0)
				{
					while($rowca = mysql_fetch_assoc($sql_pro_comm))
					{
						if($rowca['community_id']!=$id['community_id'])
						{
							$rowca['table_name'] = 'general_community';
							$comm[] = $rowca;
						}
					}
				}
				
				/* $sql_pro_comm = mysql_query("SELECT * FROM community_event WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0");
				if(mysql_num_rows($sql_pro_comm)>0)
				{
					while($rowca = mysql_fetch_assoc($sql_pro_comm))
					{
						if($rowca['community_id']!=$id['community_id'])
						{
							$comme[] = $rowca;
						}
					}
				} */
				
				$sql_get_allp = mysql_query("SELECT * FROM community_project WHERE community_id='".$id['community_id']."' AND del_status=0 AND active=0");
				if(mysql_num_rows($sql_get_allp)>0)
				{
					while($rowal = mysql_fetch_assoc($sql_get_allp))
					{
						$pal_2[] = $rowal;
					}
				}
				
				if(!empty($pal_2) && count($pal_2))
				{
					for($pl2=0;$pl2<count($pal_2);$pl2++)
					{
						$chk_typep_2 = 'community_project|'.$pal_2[$pl2]['id'];
						$sql = mysql_query("SELECT * FROM artist_project WHERE creators_info='".$chk_typep_2."' AND del_status=0 AND active=0");
						if(mysql_num_rows($sql)>0)
						{
							while($row_po = mysql_fetch_assoc($sql))
							{
								if($row_po['artist_id']!=$id['artist_id'])
								{
									$row_po['table_name'] = 'general_artist';
									$art_p_co[] = $row_po;
								}
							}
						}
						
						$sql = mysql_query("SELECT * FROM community_project WHERE creators_info='".$chk_typep_2."' AND del_status=0 AND active=0");
						if(mysql_num_rows($sql)>0)
						{
							while($row_poo = mysql_fetch_assoc($sql))
							{
								if($row_poo['community_id']!=$id['community_id'])
								{
									$row_poo['table_name'] = 'general_community';
									$com_p_co[] = $row_poo;
								}
							}
						}
					}
				}
				
				/* if(!empty($pal_2) && count($pal_2))
				{
					for($pl2=0;$pl2<count($pal_2);$pl2++)
					{
						$chk_typep_2 = 'community_project|'.$pal_2[$pl2]['id'];
						$sql = mysql_query("SELECT * FROM artist_event WHERE creators_info='".$chk_typep_2."' AND del_status=0 AND active=0");
						if(mysql_num_rows($sql)>0)
						{
							while($row_po = mysql_fetch_assoc($sql))
							{
								if($row_po['artist_id']!=$id['artist_id'])
								{
									$art_e_co[] = $row_po;
								}
							}
						}
						
						$sql = mysql_query("SELECT * FROM community_event WHERE creators_info='".$chk_typep_2."' AND del_status=0 AND active=0");
						if(mysql_num_rows($sql)>0)
						{
							while($row_poo = mysql_fetch_assoc($sql))
							{
								if($row_poo['community_id']!=$id['community_id'])
								{
									$com_e_co[] = $row_poo;
								}
							}
						}
					}
				} */
			}
			
			
			$totals = array();
			$totals = array_merge($artm,$arta,$comm,$coma,$art_p_ao,$com_p_ao,$art_p_co,$com_p_co,$artme,$artae,$comme,$comae,$art_e_ao,$com_e_ao,$art_e_co,$com_e_co);
			return $totals;
		}
		function get_creator_heis()
		{
			$id = $this->sel_general();
			$var_type_1 = 'general_artist';
			$var_type_2 = 'general_community';
			$var_type_3 = 'artist_project';
			$var_type_4 = 'community_project';
			$res_media1 = array();
			$res_media2 = array();
			$res_media3 = array();
			$res_media4 = array();
			$res_media5 = array();
			$res_media6 = array();
			$res_media7 = array();
			$res_media8 = array();
			$res_media9 = array();
			$res_media10 = array();
			$res_media11 = array();
			$res_media12 = array();
			$res_media13 = array();
			$res_media14 = array();
			$res_media15 = array();
			$res_media16 = array();
			if($id['artist_id']!=0 && !empty($id['artist_id']))
			{
				$chk_type = $var_type_1.'|'.$id['artist_id'];
				$sql_get_pro = mysql_query("select * from artist_project where creators_info='".$chk_type."' AND del_status=0 AND active=0");
				if(mysql_num_rows($sql_get_pro)>0)
				{
					while($row = mysql_fetch_assoc($sql_get_pro))
					{
						$creator_info = "artist_project|".$row['id'];
						$get_media = mysql_query("select * from general_media where creator_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
						if(mysql_num_rows($get_media)>0)
						{
							while($res = mysql_fetch_assoc($get_media))
							{
								if($res['general_user_id']!=$id['general_user_id'])
								{
									$res['table_name'] = 'general_media';
									$res_media1[] = $res;
								}
							}
						}
					}
				}
				$sql_get_pro1 = mysql_query("select * from community_project where creators_info='".$chk_type."' AND del_status=0 AND active=0");
				if(mysql_num_rows($sql_get_pro1)>0)
				{
					while($row1 = mysql_fetch_assoc($sql_get_pro1))
					{
						$creator_info = "community_project|".$row1['id'];
						$get_media = mysql_query("select * from general_media where creator_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
						if(mysql_num_rows($get_media)>0)
						{
							while($res = mysql_fetch_assoc($get_media))
							{
								if($res['general_user_id']!=$id['general_user_id'])
								{
									$res['table_name'] = 'general_media';
									$res_media2[] = $res;
								}
							}
						}
					}
				}
				/*$sql_get_pro2 = mysql_query("select * from artist_event where creators_info='".$chk_type."'");
				if(mysql_num_rows($sql_get_pro2)>0)
				{
					while($row = mysql_fetch_assoc($sql_get_pro2))
					{
						$creator_info = "artist_event|".$row['id'];
						$get_media = mysql_query("select * from general_media where creator_info = '".$creator_info."'");
						if(mysql_num_rows($get_media)>0)
						{
							while($res = mysql_fetch_assoc($get_media))
							{
								$res_media3[] = $res;
							}
						}
					}
				}
				$sql_get_pro3 = mysql_query("select * from community_event where creators_info='".$chk_type."'");
				if(mysql_num_rows($sql_get_pro3)>0)
				{
					while($row1 = mysql_fetch_assoc($sql_get_pro3))
					{
						$creator_info = "community_event|".$row1['id'];
						$get_media = mysql_query("select * from general_media where creator_info = '".$creator_info."'");
						if(mysql_num_rows($get_media)>0)
						{
							while($res = mysql_fetch_assoc($get_media))
							{
								$res_media4[] = $res;
							}
						}
					}
				}*/
				
				$find_project = mysql_query("select * from artist_project where artist_id='".$id['artist_id']."' AND del_status=0 AND active=0");
				if(mysql_num_rows($find_project)>0)
				{
					while($get_all_pro = mysql_fetch_assoc($find_project))
					{
						$new_creator_info = "artist_project|".$get_all_pro['id'];
						$sql_get_pro = mysql_query("select * from artist_project where creators_info='".$new_creator_info."' AND del_status=0 AND active=0");
						if(mysql_num_rows($sql_get_pro)>0)
						{
							while($row = mysql_fetch_assoc($sql_get_pro))
							{
								$creator_info = "artist_project|".$row['id'];
								$get_media = mysql_query("select * from general_media where creator_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
								if(mysql_num_rows($get_media)>0)
								{
									while($res = mysql_fetch_assoc($get_media))
									{
										if($res['general_user_id']!=$id['general_user_id'])
										{
											$res['table_name'] = 'general_media';
											$res_media5[] = $res;
										}
									}
								}
							}
						}
						//var_dump($res_media5);
						$sql_get_pro1 = mysql_query("select * from community_project where creators_info='".$new_creator_info."' AND del_status=0 AND active=0");
						if(mysql_num_rows($sql_get_pro1)>0)
						{
							while($row1 = mysql_fetch_assoc($sql_get_pro1))
							{
								$creator_info = "community_project|".$row1['id'];
								$get_media = mysql_query("select * from general_media where creator_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
								if(mysql_num_rows($get_media)>0)
								{
									while($res = mysql_fetch_assoc($get_media))
									{
										if($res['general_user_id']!=$id['general_user_id'])
										{
											$res['table_name'] = 'general_media';
											$res_media6[] = $res;
										}
									}
								}
							}
						}
						$sql_get_pro2 = mysql_query("select * from artist_event where creators_info='".$new_creator_info."' AND del_status=0 AND active=0");
						if(mysql_num_rows($sql_get_pro2)>0)
						{
							while($row = mysql_fetch_assoc($sql_get_pro2))
							{
								$creator_info = "artist_event|".$row['id'];
								$get_media = mysql_query("select * from general_media where creator_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
								if(mysql_num_rows($get_media)>0)
								{
									while($res = mysql_fetch_assoc($get_media))
									{
										if($res['general_user_id']!=$id['general_user_id'])
										{
											$res['table_name'] = 'general_media';
											$res_media7[] = $res;
										}
									}
								}
							}
						}
						$sql_get_pro3 = mysql_query("select * from community_event where creators_info='".$new_creator_info."' AND del_status=0 AND active=0");
						if(mysql_num_rows($sql_get_pro3)>0)
						{
							while($row1 = mysql_fetch_assoc($sql_get_pro3))
							{
								$creator_info = "community_event|".$row1['id'];
								$get_media = mysql_query("select * from general_media where creator_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
								if(mysql_num_rows($get_media)>0)
								{
									while($res = mysql_fetch_assoc($get_media))
									{
										if($res['general_user_id']!=$id['general_user_id'])
										{
											$res['table_name'] = 'general_media';
											$res_media8[] = $res;
										}
									}
								}
							}
						}
					}
				}
			   
			}
			if($id['community_id']!=0 && !empty($id['community_id']))
			{
				$chk_type = $var_type_2.'|'.$id['community_id'];
				$sql_get_pro = mysql_query("select * from artist_project where creators_info='".$chk_type."' AND del_status=0 AND active=0");
				if(mysql_num_rows($sql_get_pro)>0)
				{
					while($row = mysql_fetch_assoc($sql_get_pro))
					{
						$creator_info = "artist_project|".$row['id'];
						$get_media = mysql_query("select * from general_media where creator_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
						if(mysql_num_rows($get_media)>0)
						{
							while($res = mysql_fetch_assoc($get_media))
							{
								if($res['general_user_id']!=$id['general_user_id'])
								{
									$res['table_name'] = 'general_media';
									$res_media9[] = $res;
								}
							}
						}
					}
				}
				$sql_get_pro1 = mysql_query("select * from community_project where creators_info='".$chk_type."' AND del_status=0 AND active=0");
				if(mysql_num_rows($sql_get_pro1)>0)
				{
					while($row1 = mysql_fetch_assoc($sql_get_pro1))
					{
						$creator_info = "community_project|".$row1['id'];
						$get_media = mysql_query("select * from general_media where creator_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
						if(mysql_num_rows($get_media)>0)
						{
							while($res = mysql_fetch_assoc($get_media))
							{
								if($res['general_user_id']!=$id['general_user_id'])
								{
									$res['table_name'] = 'general_media';
									$res_media10[] = $res;
								}
							}
						}
					}
				}
				/*$sql_get_pro2 = mysql_query("select * from artist_event where creators_info='".$chk_type."'");
				if(mysql_num_rows($sql_get_pro2)>0)
				{
					while($row = mysql_fetch_assoc($sql_get_pro2))
					{
						$creator_info = "artist_event|".$row['id'];
						$get_media = mysql_query("select * from general_media where creator_info = '".$creator_info."'");
						if(mysql_num_rows($get_media)>0)
						{
							while($res = mysql_fetch_assoc($get_media))
							{
								$res_media11[] = $res;
							}
						}
					}
				}
				$sql_get_pro3 = mysql_query("select * from community_event where creators_info='".$chk_type."'");
				if(mysql_num_rows($sql_get_pro3)>0)
				{
					while($row1 = mysql_fetch_assoc($sql_get_pro3))
					{
						$creator_info = "community_event|".$row1['id'];
						$get_media = mysql_query("select * from general_media where creator_info = '".$creator_info."'");
						if(mysql_num_rows($get_media)>0)
						{
							while($res = mysql_fetch_assoc($get_media))
							{
								$res_media12[] = $res;
							}
						}
					}
				}*/
				$find_project = mysql_query("select * from community_project where community_id='".$id['community_id']."' AND del_status=0 AND active=0");
				if(mysql_num_rows($find_project)>0)
				{
					while($get_all_pro = mysql_fetch_assoc($find_project))
					{
						$new_creator_info = "community_project|".$get_all_pro['id'];
						$sql_get_pro = mysql_query("select * from artist_project where creators_info='".$new_creator_info."' AND del_status=0 AND active=0");
						if(mysql_num_rows($sql_get_pro)>0)
						{
							while($row = mysql_fetch_assoc($sql_get_pro))
							{
								$creator_info = "artist_project|".$row['id'];
								$get_media = mysql_query("select * from general_media where creator_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
								if(mysql_num_rows($get_media)>0)
								{
									while($res = mysql_fetch_assoc($get_media))
									{
										if($res['general_user_id']!=$id['general_user_id'])
										{
											$res['table_name'] = 'general_media';
											$res_media13[] = $res;
										}
									}
								}
							}
						}
						$sql_get_pro1 = mysql_query("select * from community_project where creators_info='".$new_creator_info."' AND del_status=0 AND active=0");
						if(mysql_num_rows($sql_get_pro1)>0)
						{
							while($row1 = mysql_fetch_assoc($sql_get_pro1))
							{
								$creator_info = "community_project|".$row1['id'];
								$get_media = mysql_query("select * from general_media where creator_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
								if(mysql_num_rows($get_media)>0)
								{
									while($res = mysql_fetch_assoc($get_media))
									{
										if($res['general_user_id']!=$id['general_user_id'])
										{
											$res['table_name'] = 'general_media';
											$res_media14[] = $res;
										}
									}
								}
							}
						}
						/*$sql_get_pro2 = mysql_query("select * from artist_event where creators_info='".$new_creator_info."'");
						if(mysql_num_rows($sql_get_pro2)>0)
						{
							while($row = mysql_fetch_assoc($sql_get_pro2))
							{
								$creator_info = "artist_event|".$row['id'];
								$get_media = mysql_query("select * from general_media where creator_info = '".$creator_info."'");
								if(mysql_num_rows($get_media)>0)
								{
									while($res = mysql_fetch_assoc($get_media))
									{
										$res_media15[] = $res;
									}
								}
							}
						}
						$sql_get_pro3 = mysql_query("select * from community_event where creators_info='".$new_creator_info."'");
						if(mysql_num_rows($sql_get_pro3)>0)
						{
							while($row1 = mysql_fetch_assoc($sql_get_pro3))
							{
								$creator_info = "community_event|".$row1['id'];
								$get_media = mysql_query("select * from general_media where creator_info = '".$creator_info."'");
								if(mysql_num_rows($get_media)>0)
								{
									while($res = mysql_fetch_assoc($get_media))
									{
										$res_media16[] = $res;
									}
								}
							}
						}*/
					}
				}
			   
			}
			   
			$get_all_media = array_merge($res_media1,$res_media2,$res_media3,$res_media4,$res_media5,$res_media6,$res_media7,$res_media8,$res_media9,$res_media10,$res_media11,$res_media12,$res_media13,$res_media14,$res_media15,$res_media16);
			//var_dump($get_all_media);
			return($get_all_media);
		}
		function get_from_heis()
		{
			$id = $this->sel_general();
			$var_type_1 = 'general_artist';
			$var_type_2 = 'general_community';
			$var_type_3 = 'artist_project';
			$var_type_4 = 'community_project';
			$res_media1 = array();
			$res_media2 = array();
			$res_media3 = array();
			$res_media4 = array();
			$res_media5 = array();
			$res_media6 = array();
			$res_media7 = array();
			$res_media8 = array();
			$res_media9 = array();
			$res_media10 = array();
			$res_media11 = array();
			$res_media12 = array();
			$res_media13 = array();
			$res_media14 = array();
			$res_media15 = array();
			$res_media16 = array();
			if($id['artist_id']!=0 && !empty($id['artist_id']))
			{
				$chk_type = $var_type_1.'|'.$id['artist_id'];
				$sql_get_pro = mysql_query("select * from artist_project where creators_info='".$chk_type."' AND del_status=0 AND active=0");
				if(mysql_num_rows($sql_get_pro)>0)
				{
					while($row = mysql_fetch_assoc($sql_get_pro))
					{
						$creator_info = "artist_project|".$row['id'];
						$get_media = mysql_query("select * from general_media where from_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
						if(mysql_num_rows($get_media)>0)
						{
							while($res = mysql_fetch_assoc($get_media))
							{
								if($id['general_user_id'] != $res['general_user_id'])
								{
									$res['table_name'] = 'general_media';
									$res_media1[] = $res;
								}
							}
						}
					}
				}
				$sql_get_pro1 = mysql_query("select * from community_project where creators_info='".$chk_type."' AND del_status=0 AND active=0");
				if(mysql_num_rows($sql_get_pro1)>0)
				{
					while($row1 = mysql_fetch_assoc($sql_get_pro1))
					{
						$creator_info = "community_project|".$row1['id'];
						$get_media = mysql_query("select * from general_media where from_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
						if(mysql_num_rows($get_media)>0)
						{
							while($res = mysql_fetch_assoc($get_media))
							{
								if($id['general_user_id'] != $res['general_user_id'])
								{
									$res['table_name'] = 'general_media';
									$res_media2[] = $res;
								}
							}
						}
					}
				}
				$sql_get_pro1 = mysql_query("select * from community_event where creators_info='".$chk_type."' AND del_status=0 AND active=0");
				if(mysql_num_rows($sql_get_pro1)>0)
				{
					while($row1 = mysql_fetch_assoc($sql_get_pro1))
					{
						$creator_info = "community_event|".$row1['id'];
						$get_media = mysql_query("select * from general_media where from_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
						if(mysql_num_rows($get_media)>0)
						{
							while($res = mysql_fetch_assoc($get_media))
							{
								if($id['general_user_id'] != $res['general_user_id'])
								{
									$res['table_name'] = 'general_media';
									$res_media3[] = $res;
								}
							}
						}
					}
				}
				$sql_get_pro1 = mysql_query("select * from artist_event where creators_info='".$chk_type."' AND del_status=0 AND active=0");
				if(mysql_num_rows($sql_get_pro1)>0)
				{
					while($row1 = mysql_fetch_assoc($sql_get_pro1))
					{
						$creator_info = "artist_event|".$row1['id'];
						$get_media = mysql_query("select * from general_media where from_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
						if(mysql_num_rows($get_media)>0)
						{
							while($res = mysql_fetch_assoc($get_media))
							{
								$res['table_name'] = 'general_media';
								$res_media4[] = $res;
							}
						}
					}
				}
				
				
				$find_project = mysql_query("select * from artist_project where artist_id='".$id['artist_id']."' AND del_status=0 AND active=0");
				if(mysql_num_rows($find_project)>0)
				{
					while($get_all_pro = mysql_fetch_assoc($find_project))
					{
						$new_creator_info = "artist_project|".$get_all_pro['id'];
						//echo "select * from artist_project where creators_info='".$new_creator_info."'";
						$sql_get_pro = mysql_query("select * from artist_project where creators_info='".$new_creator_info."' AND del_status=0 AND active=0");
						if(mysql_num_rows($sql_get_pro)>0)
						{
							while($row = mysql_fetch_assoc($sql_get_pro))
							{
								$creator_info = "artist_project|".$row['id'];
								$get_media = mysql_query("select * from general_media where from_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
								if(mysql_num_rows($get_media)>0)
								{
									while($res = mysql_fetch_assoc($get_media))
									{
										if($id['general_user_id'] != $res['general_user_id'])
										{
											$res['table_name'] = 'general_media';
											$res_media5[] = $res;
										}
									}
								}
							}
						}
						$sql_get_pro1 = mysql_query("select * from community_project where creators_info='".$new_creator_info."' AND del_status=0 AND active=0");
						if(mysql_num_rows($sql_get_pro1)>0)
						{
							while($row1 = mysql_fetch_assoc($sql_get_pro1))
							{
								$creator_info = "community_project|".$row1['id'];
								$get_media = mysql_query("select * from general_media where from_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
								if(mysql_num_rows($get_media)>0)
								{
									while($res = mysql_fetch_assoc($get_media))
									{
										if($id['general_user_id'] != $res['general_user_id'])
										{
											$res['table_name'] = 'general_media';
											$res_media6[] = $res;
										}
									}
								}
							}
						}
						$sql_get_pro2 = mysql_query("select * from artist_event where creators_info='".$new_creator_info."' AND del_status=0 AND active=0");
						if(mysql_num_rows($sql_get_pro2)>0)
						{
							while($row = mysql_fetch_assoc($sql_get_pro2))
							{
								$creator_info = "artist_event|".$row['id'];
								$get_media = mysql_query("select * from general_media where from_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
								if(mysql_num_rows($get_media)>0)
								{
									while($res = mysql_fetch_assoc($get_media))
									{
										if($id['general_user_id'] != $res['general_user_id'])
										{
											$res['table_name'] = 'general_media';
											$res_media7[] = $res;
										}
									}
								}
							}
						}
						$sql_get_pro3 = mysql_query("select * from community_event where creators_info='".$new_creator_info."' AND del_status=0 AND active=0");
						if(mysql_num_rows($sql_get_pro3)>0)
						{
							while($row1 = mysql_fetch_assoc($sql_get_pro3))
							{
								$creator_info = "community_event|".$row1['id'];
								$get_media = mysql_query("select * from general_media where from_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
								if(mysql_num_rows($get_media)>0)
								{
									while($res = mysql_fetch_assoc($get_media))
									{
										if($id['general_user_id'] != $res['general_user_id'])
										{
											$res['table_name'] = 'general_media';
											$res_media8[] = $res;
										}
									}
								}
							}
						}
					}
				}
				$find_project = mysql_query("select * from artist_event where artist_id='".$id['artist_id']."' AND del_status=0 AND active=0");
				if(mysql_num_rows($find_project)>0)
				{
					while($get_all_pro = mysql_fetch_assoc($find_project))
					{
						$new_creator_info = "artist_event|".$get_all_pro['id'];
						//echo "select * from artist_project where creators_info='".$new_creator_info."'";
						$sql_get_pro = mysql_query("select * from artist_project where creators_info='".$new_creator_info."' AND del_status=0 AND active=0");
						if(mysql_num_rows($sql_get_pro)>0)
						{
							while($row = mysql_fetch_assoc($sql_get_pro))
							{
								$creator_info = "artist_project|".$row['id'];
								$get_media = mysql_query("select * from general_media where from_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
								if(mysql_num_rows($get_media)>0)
								{
									while($res = mysql_fetch_assoc($get_media))
									{
										if($id['general_user_id'] != $res['general_user_id'])
										{
											$res['table_name'] = 'general_media';
											$res_media5[] = $res;
										}
									}
								}
							}
						}
						$sql_get_pro1 = mysql_query("select * from community_project where creators_info='".$new_creator_info."' AND del_status=0 AND active=0");
						if(mysql_num_rows($sql_get_pro1)>0)
						{
							while($row1 = mysql_fetch_assoc($sql_get_pro1))
							{
								$creator_info = "community_project|".$row1['id'];
								$get_media = mysql_query("select * from general_media where from_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
								if(mysql_num_rows($get_media)>0)
								{
									while($res = mysql_fetch_assoc($get_media))
									{
										if($id['general_user_id'] != $res['general_user_id'])
										{
											$res['table_name'] = 'general_media';
											$res_media6[] = $res;
										}
									}
								}
							}
						}
						$sql_get_pro2 = mysql_query("select * from artist_event where creators_info='".$new_creator_info."' AND del_status=0 AND active=0");
						if(mysql_num_rows($sql_get_pro2)>0)
						{
							while($row = mysql_fetch_assoc($sql_get_pro2))
							{
								$creator_info = "artist_event|".$row['id'];
								$get_media = mysql_query("select * from general_media where from_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
								if(mysql_num_rows($get_media)>0)
								{
									while($res = mysql_fetch_assoc($get_media))
									{
										if($id['general_user_id'] != $res['general_user_id'])
										{
											$res['table_name'] = 'general_media';
											$res_media7[] = $res;
										}
									}
								}
							}
						}
						$sql_get_pro3 = mysql_query("select * from community_event where creators_info='".$new_creator_info."' AND del_status=0 AND active=0");
						if(mysql_num_rows($sql_get_pro3)>0)
						{
							while($row1 = mysql_fetch_assoc($sql_get_pro3))
							{
								$creator_info = "community_event|".$row1['id'];
								$get_media = mysql_query("select * from general_media where from_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
								if(mysql_num_rows($get_media)>0)
								{
									while($res = mysql_fetch_assoc($get_media))
									{
										if($id['general_user_id'] != $res['general_user_id'])
										{
											$res['table_name'] = 'general_media';
											$res_media8[] = $res;
										}
									}
								}
							}
						}
					}
				}
			   
			}
			if($id['community_id']!=0 && !empty($id['community_id']))
			{
			
				$chk_type = $var_type_2.'|'.$id['community_id'];
				$sql_get_pro = mysql_query("select * from artist_project where creators_info='".$chk_type."' AND del_status=0 AND active=0");
				if(mysql_num_rows($sql_get_pro)>0)
				{
					while($row = mysql_fetch_assoc($sql_get_pro))
					{
						$creator_info = "artist_project|".$row['id'];
						$get_media = mysql_query("select * from general_media where from_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
						if(mysql_num_rows($get_media)>0)
						{
							while($res = mysql_fetch_assoc($get_media))
							{
								if($id['general_user_id'] != $res['general_user_id'])
								{
									$res['table_name'] = 'general_media';
									$res_media9[] = $res;
								}
							}
						}
					}
				}
				$sql_get_pro1 = mysql_query("select * from community_project where creators_info='".$chk_type."' AND del_status=0 AND active=0");
				if(mysql_num_rows($sql_get_pro1)>0)
				{
					while($row1 = mysql_fetch_assoc($sql_get_pro1))
					{
						$creator_info = "community_project|".$row1['id'];
						$get_media = mysql_query("select * from general_media where from_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
						if(mysql_num_rows($get_media)>0)
						{
							while($res = mysql_fetch_assoc($get_media))
							{
								if($id['general_user_id'] != $res['general_user_id'])
								{
									$res['table_name'] = 'general_media';
									$res_media10[] = $res;
								}
							}
						}
					}
				}
				$sql_get_pro1 = mysql_query("select * from community_event where creators_info='".$chk_type."' AND del_status=0 AND active=0");
				if(mysql_num_rows($sql_get_pro1)>0)
				{
					while($row1 = mysql_fetch_assoc($sql_get_pro1))
					{
						$creator_info = "community_event|".$row1['id'];
						$get_media = mysql_query("select * from general_media where from_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
						if(mysql_num_rows($get_media)>0)
						{
							while($res = mysql_fetch_assoc($get_media))
							{
								if($id['general_user_id'] != $res['general_user_id'])
								{
									$res['table_name'] = 'general_media';
									$res_media11[] = $res;
								}
							}
						}
					}
				}
				$sql_get_pro1 = mysql_query("select * from artist_event where creators_info='".$chk_type."' AND del_status=0 AND active=0");
				if(mysql_num_rows($sql_get_pro1)>0)
				{
					while($row1 = mysql_fetch_assoc($sql_get_pro1))
					{
						$creator_info = "artist_event|".$row1['id'];
						$get_media = mysql_query("select * from general_media where from_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
						if(mysql_num_rows($get_media)>0)
						{
							while($res = mysql_fetch_assoc($get_media))
							{
								if($id['general_user_id'] != $res['general_user_id'])
								{
									$res['table_name'] = 'general_media';
									$res_media12[] = $res;
								}
							}
						}
					}
				}
			
				$find_project = mysql_query("select * from community_project where community_id='".$id['community_id']."' AND del_status=0 AND active=0");
				if(mysql_num_rows($find_project)>0)
				{
					while($get_all_pro = mysql_fetch_assoc($find_project))
					{
						$new_creator_info = "community_project|".$get_all_pro['id'];
						$sql_get_pro = mysql_query("select * from artist_project where creators_info='".$new_creator_info."' AND del_status=0 AND active=0");
						if(mysql_num_rows($sql_get_pro)>0)
						{
							while($row = mysql_fetch_assoc($sql_get_pro))
							{
								$creator_info = "artist_project|".$row['id'];
								$get_media = mysql_query("select * from general_media where from_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
								if(mysql_num_rows($get_media)>0)
								{
									while($res = mysql_fetch_assoc($get_media))
									{
										if($id['general_user_id'] != $res['general_user_id'])
										{
											$res['table_name'] = 'general_media';
											$res_media13[] = $res;
										}
									}
								}
							}
						}
						$sql_get_pro1 = mysql_query("select * from community_project where creators_info='".$new_creator_info."' AND del_status=0 AND active=0");
						if(mysql_num_rows($sql_get_pro1)>0)
						{
							while($row1 = mysql_fetch_assoc($sql_get_pro1))
							{
								$creator_info = "community_project|".$row1['id'];
								$get_media = mysql_query("select * from general_media where from_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
								if(mysql_num_rows($get_media)>0)
								{
									while($res = mysql_fetch_assoc($get_media))
									{
										if($id['general_user_id'] != $res['general_user_id'])
										{
											$res['table_name'] = 'general_media';
											$res_media14[] = $res;
										}
									}
								}
							}
						}
						$sql_get_pro2 = mysql_query("select * from artist_event where creators_info='".$new_creator_info."' AND del_status=0 AND active=0");
						if(mysql_num_rows($sql_get_pro2)>0)
						{
							while($row = mysql_fetch_assoc($sql_get_pro2))
							{
								$creator_info = "artist_event|".$row['id'];
								$get_media = mysql_query("select * from general_media where from_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
								if(mysql_num_rows($get_media)>0)
								{
									while($res = mysql_fetch_assoc($get_media))
									{
										if($id['general_user_id'] != $res['general_user_id'])
										{
											$res['table_name'] = 'general_media';
											$res_media15[] = $res;
										}
									}
								}
							}
						}
						$sql_get_pro3 = mysql_query("select * from community_event where creators_info='".$new_creator_info."' AND del_status=0 AND active=0");
						if(mysql_num_rows($sql_get_pro3)>0)
						{
							while($row1 = mysql_fetch_assoc($sql_get_pro3))
							{
								$creator_info = "community_event|".$row1['id'];
								$get_media = mysql_query("select * from general_media where from_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
								if(mysql_num_rows($get_media)>0)
								{
									while($res = mysql_fetch_assoc($get_media))
									{
										if($id['general_user_id'] != $res['general_user_id'])
										{
											$res['table_name'] = 'general_media';
											$res_media16[] = $res;
										}
									}
								}
							}
						}
					}
				}
				$find_project = mysql_query("select * from community_event where community_id='".$id['community_id']."' AND del_status=0 AND active=0");
				if(mysql_num_rows($find_project)>0)
				{
					while($get_all_pro = mysql_fetch_assoc($find_project))
					{
						$new_creator_info = "community_event|".$get_all_pro['id'];
						$sql_get_pro = mysql_query("select * from artist_project where creators_info='".$new_creator_info."' AND del_status=0 AND active=0");
						if(mysql_num_rows($sql_get_pro)>0)
						{
							while($row = mysql_fetch_assoc($sql_get_pro))
							{
								$creator_info = "artist_project|".$row['id'];
								$get_media = mysql_query("select * from general_media where from_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
								if(mysql_num_rows($get_media)>0)
								{
									while($res = mysql_fetch_assoc($get_media))
									{
										if($id['general_user_id'] != $res['general_user_id'])
										{
											$res['table_name'] = 'general_media';
											$res_media13[] = $res;
										}
									}
								}
							}
						}
						$sql_get_pro1 = mysql_query("select * from community_project where creators_info='".$new_creator_info."' AND del_status=0 AND active=0");
						if(mysql_num_rows($sql_get_pro1)>0)
						{
							while($row1 = mysql_fetch_assoc($sql_get_pro1))
							{
								$creator_info = "community_project|".$row1['id'];
								$get_media = mysql_query("select * from general_media where from_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
								if(mysql_num_rows($get_media)>0)
								{
									while($res = mysql_fetch_assoc($get_media))
									{
										if($id['general_user_id'] != $res['general_user_id'])
										{
											$res['table_name'] = 'general_media';
											$res_media14[] = $res;
										}
									}
								}
							}
						}
						$sql_get_pro2 = mysql_query("select * from artist_event where creators_info='".$new_creator_info."' AND del_status=0 AND active=0");
						if(mysql_num_rows($sql_get_pro2)>0)
						{
							while($row = mysql_fetch_assoc($sql_get_pro2))
							{
								$creator_info = "artist_event|".$row['id'];
								$get_media = mysql_query("select * from general_media where from_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
								if(mysql_num_rows($get_media)>0)
								{
									while($res = mysql_fetch_assoc($get_media))
									{
										if($id['general_user_id'] != $res['general_user_id'])
										{
											$res['table_name'] = 'general_media';
											$res_media15[] = $res;
										}
									}
								}
							}
						}
						$sql_get_pro3 = mysql_query("select * from community_event where creators_info='".$new_creator_info."' AND del_status=0 AND active=0");
						if(mysql_num_rows($sql_get_pro3)>0)
						{
							while($row1 = mysql_fetch_assoc($sql_get_pro3))
							{
								$creator_info = "community_event|".$row1['id'];
								$get_media = mysql_query("select * from general_media where from_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
								if(mysql_num_rows($get_media)>0)
								{
									while($res = mysql_fetch_assoc($get_media))
									{
										if($id['general_user_id'] != $res['general_user_id'])
										{
											$res['table_name'] = 'general_media';
											$res_media16[] = $res;
										}
									}
								}
							}
						}
					}
				}
			   
			}
			   
			$get_all_media = array_merge($res_media1,$res_media2,$res_media3,$res_media4,$res_media5,$res_media6,$res_media7,$res_media8,$res_media9,$res_media10,$res_media11,$res_media12,$res_media13,$res_media14,$res_media15,$res_media16);
			return($get_all_media);
		}
		function get_the_user_tagin_project($email)
		{
			$id = $this->sel_general();
			$var_type_1 = 'general_artist';
			$var_type_2 = 'general_community';
			$var_type_3 = 'artist_project';
			$var_type_4 = 'community_project';
			$res_media1 = array();
			$res_media2 = array();
			$res_media3 = array();
			$res_media4 = array();

			$get_all_art_project = mysql_query("SELECT * FROM artist_project WHERE tagged_user_email LIKE '%".$email."%' AND del_status=0 AND active=0");
			if(mysql_num_rows($get_all_art_project)>0)
			{
				while($res_data = mysql_fetch_assoc($get_all_art_project))
				{
					$creator_info = "artist_project|".$res_data['id'];
					$get_media = mysql_query("select * from general_media where creator_info ='".$creator_info."' OR from_info ='".$creator_info."' AND delete_status=0 AND media_status=0");
					if(mysql_num_rows($get_media)>0)
					{
						while($res_media_data = mysql_fetch_assoc($get_media))
						{
							if($id['general_user_id']!=$res_media_data['general_user_id'])
							{
								$res_media_data['table_name'] = 'general_media';
								$res_media1[] = $res_media_data;
							}
						}
					}
				}
			}
			$get_all_art_event = mysql_query("SELECT * FROM artist_event WHERE tagged_user_email LIKE '%".$email."%' AND del_status=0 AND active=0");
			if(mysql_num_rows($get_all_art_event)>0)
			{
				while($res_data = mysql_fetch_assoc($get_all_art_event))
				{
					$creator_info = "artist_event|".$res_data['id'];
					$get_media = mysql_query("select * from general_media where creator_info ='".$creator_info."' OR from_info ='".$creator_info."' AND delete_status=0 AND media_status=0");
					if(mysql_num_rows($get_media)>0)
					{
						while($res_media_data = mysql_fetch_assoc($get_media))
						{
							if($id['general_user_id']!=$res_media_data['general_user_id'])
							{
								$res_media_data['table_name'] = 'general_media';
								$res_media2[] = $res_media_data;
							}
						}
					}
				}
			}
			$get_all_com_event = mysql_query("SELECT * FROM community_event WHERE tagged_user_email LIKE '%".$email."%' AND del_status=0 AND active=0");
			if(mysql_num_rows($get_all_com_event)>0)
			{
				while($res_data = mysql_fetch_assoc($get_all_com_event))
				{
					$creator_info = "community_event|".$res_data['id'];
					$get_media = mysql_query("select * from general_media where creator_info ='".$creator_info."' OR from_info ='".$creator_info."' AND delete_status=0 AND media_status=0");
					if(mysql_num_rows($get_media)>0)
					{
						while($res_media_data = mysql_fetch_assoc($get_media))
						{
							if($id['general_user_id']!=$res_media_data['general_user_id'])
							{
								$res_media_data['table_name'] = 'general_media';
								$res_media3[] = $res_media_data;
							}
						}
					}
				}
			}
			$get_all_com_project = mysql_query("SELECT * FROM community_project WHERE tagged_user_email LIKE '%".$email."%' AND del_status=0 AND active=0");
			if(mysql_num_rows($get_all_com_project)>0)
			{
				while($res_data = mysql_fetch_assoc($get_all_com_project))
				{
					$creator_info = "community_project|".$res_data['id'];
					$get_media = mysql_query("select * from general_media where creator_info ='".$creator_info."' OR from_info ='".$creator_info."' AND delete_status=0 AND media_status=0");
					if(mysql_num_rows($get_media)>0)
					{
						while($res_media_data = mysql_fetch_assoc($get_media))
						{
							if($id['general_user_id']!=$res_media_data['general_user_id'])
							{
								$res_media_data['table_name'] = 'general_media';
								$res_media4[] = $res_media_data;
							}
						}
					}
				}
			}
		   
			$get_all_media = array_merge($res_media1,$res_media2,$res_media3,$res_media4);
			//var_dump($get_all_media);
			return($get_all_media);
		}
		function sel_type_of_tagged_media($id)
		{
			$get_type=mysql_query("select * from type where type_id=$id");
			$res_type=mysql_fetch_assoc($get_type);
			return($res_type);
		}
		function get_media_track($id)
		{
			$sql_trac=mysql_query("select * from media_songs where media_id='".$id."'");
			$res_trac=mysql_fetch_assoc($sql_trac);
			return($res_trac);
		}
		function get_media_where_creator_or_from()
		{
			$get_art_media1 = array();
			$get_art_media2 = array();
			$get_art_media3 = array();
			$get_art_media4 = array();
			$get_art_media5 = array();
			$get_art_media6 = array();
			
			$get_gen_det = $this->sel_general();
			if($get_gen_det['artist_id']>0){
				$creator_info = "general_artist|".$get_gen_det['artist_id'];
				$query = mysql_query("select * from general_media where creator_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
				if(mysql_num_rows($query)>0){
					while($row_art = mysql_fetch_assoc($query)){
						if($get_gen_det['general_user_id']!= $row_art['general_user_id']){
						$get_art_media1[] = $row_art;
						}
					}
				}
				$get_creator = mysql_query("select * from artist_project where artist_id='".$get_gen_det['artist_id']."' AND del_status=0 AND active=0");
				if(mysql_num_rows($get_creator)>0)
				{
					while($row_art_pro = mysql_fetch_assoc($get_creator))
					{
						$creator_info = "artist_project|".$row_art_pro["id"];
						$query1 = mysql_query("select * from general_media where creator_info = '".$creator_info."' OR from_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
						if(mysql_num_rows($query1)>0){
							while($row_art_media = mysql_fetch_assoc($query1)){
								if($get_gen_det['general_user_id']!= $row_art_media['general_user_id'])
								{
									$res_media_data['table_name'] = 'general_media';
									$get_art_media2[] = $row_art_media;
								}
							}
						}
					}
				}
				$get_creator = mysql_query("select * from artist_event where artist_id='".$get_gen_det['artist_id']."' AND del_status=0 AND active=0");
				if(mysql_num_rows($get_creator)>0)
				{
					while($row_art_eve = mysql_fetch_assoc($get_creator))
					{
						$creator_info = "artist_event|".$row_art_eve["id"];
						$query1 = mysql_query("select * from general_media where creator_info = '".$creator_info."' OR from_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
						if(mysql_num_rows($query1)>0){
							while($row_art_media = mysql_fetch_assoc($query1)){
								if($get_gen_det['general_user_id']!= $row_art_media['general_user_id'])
								{
									$row_art_media['table_name'] = 'general_media';
									$get_art_media3[] = $row_art_media;
								}
							}
						}
					}
				}
			}
			if($get_gen_det['community_id']>0){
				$creator_info = "general_community|".$get_gen_det['community_id'];
				$query = mysql_query("select * from general_media where creator_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
				if(mysql_num_rows($query)>0){
					while($row_com = mysql_fetch_assoc($query)){
						//echo "azher4";
						if($get_gen_det['general_user_id']!= $row_com['general_user_id'])
						{
							$row_com['table_name'] = 'general_media';
							$get_art_media4[] = $row_com;
						}
					}
				}
				$get_creator = mysql_query("select * from community_project where community_id='".$get_gen_det['community_id']."' AND del_status=0 AND active=0");
				if(mysql_num_rows($get_creator)>0)
				{
					while($row_com_pro = mysql_fetch_assoc($get_creator))
					{
						$creator_info = "community_project|".$row_com_pro["id"];
						$query1 = mysql_query("select * from general_media where creator_info = '".$creator_info."' OR from_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
						if(mysql_num_rows($query1)>0){
							while($row_com_media = mysql_fetch_assoc($query1)){
								//echo "azher5";
								if($get_gen_det['general_user_id']!= $row_com_media['general_user_id'])
								{
									$row_com_media['table_name'] = 'general_media';
									$get_art_media5[] = $row_com_media;
								}
							}
						}
					}
				}
				$get_creator = mysql_query("select * from community_event where community_id='".$get_gen_det['community_id']."' AND del_status=0 AND active=0");
				if(mysql_num_rows($get_creator)>0)
				{
					while($row_com_eve = mysql_fetch_assoc($get_creator))
					{
						$creator_info = "community_event|".$row_com_eve["id"];
						$query1 = mysql_query("select * from general_media where creator_info = '".$creator_info."' OR from_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
						if(mysql_num_rows($query1)>0){
							while($row_com_media = mysql_fetch_assoc($query1)){
								if($get_gen_det['general_user_id']!= $row_com_media['general_user_id'])
								{
									$row_com_media['table_name'] = 'general_media';
									$get_art_media6[] = $row_com_media;
								}
							}
						}
					}
				}
			}
			
			$all_media = array_merge($get_art_media1,$get_art_media2,$get_art_media3,$get_art_media4,$get_art_media5,$get_art_media6);
			return($all_media);
		}
		function get_media_create($media_type)
		{
			$id = $this->sel_general();
			
			$var_type_1 = 'general_artist';
			$var_type_2 = 'general_community';
			$var_type_3 = 'artist_project';
			$var_type_4 = 'community_project';
			
			$pal_1 = array();
			$pal_2 = array();
			$pal_3 = array();
			$pal_4 = array();
			
			$final_c_f = array();
			
			if($id['artist_id']!=0 && !empty($id['artist_id']))
			{
				$chk_type_a = $var_type_1.'|'.$id['artist_id'];

				$sql = mysql_query("SELECT * FROM general_media WHERE creator_info='".$chk_type_a."' AND delete_status=0 AND media_status=0 AND media_type='".$media_type."' AND general_user_id!='".$id['general_user_id']."'");
				if(mysql_num_rows($sql)>0)
				{
					while($row_a = mysql_fetch_assoc($sql))
					{
						$type_new_id = $this->sel_type_of_tagged_media($row_a['media_type']);
						$row_a['name'] = $type_new_id['name'];
						$row_a['media_from'] = "media_create_new";
						$row_a['table_name'] = 'general_media';
						$final_c_f[] = $row_a;
					}
				}
			}
			
			/* if($id['community_id']!=0 && !empty($id['community_id']))
			{
				$chk_type_c = $var_type_2.'|'.$id['community_id'];
				
				$sql = mysql_query("SELECT * FROM general_media WHERE creator_info='".$chk_type_c."' AND delete_status=0 AND media_status=0 AND media_type='".$type."' AND general_user_id!='".$id['general_user_id']."'");
				if(mysql_num_rows($sql)>0)
				{
					while($row_c = mysql_fetch_assoc($sql))
					{
						$final_c_f[] = $row_c;
					}				
				}
			} */
			
			/* if($id['artist_id']!=0 && !empty($id['artist_id']))
			{			
				$sql_get_allp = mysql_query("SELECT * FROM artist_project WHERE artist_id='".$id['artist_id']."' AND del_status=0 AND active=0");
				if(mysql_num_rows($sql_get_allp)>0)
				{
					while($rowal = mysql_fetch_assoc($sql_get_allp))
					{
						$pal_1[] = $rowal;
					}
				}
				
				$sql_get_alle = mysql_query("SELECT * FROM artist_event WHERE artist_id='".$id['artist_id']."' AND del_status=0 AND active=0");
				if(mysql_num_rows($sql_get_alle)>0)
				{
					while($rowael = mysql_fetch_assoc($sql_get_alle))
					{
						$pal_3[] = $rowael;
					}
				}
				
				if(!empty($pal_1) && count($pal_1))
				{
					for($pl=0;$pl<count($pal_1);$pl++)
					{
						$chk_typep_1 = 'artist_project|'.$pal_1[$pl]['id'];
						
						$sql_as = mysql_query("SELECT * FROM general_media WHERE creator_info='".$chk_typep_1."' AND delete_status=0 AND media_status=0 AND media_type='".$type."' AND general_user_id!='".$id['general_user_id']."'");
						if(mysql_num_rows($sql_as)>0)
						{
							while($row_ap = mysql_fetch_assoc($sql_as))
							{
								$final_c_f[] = $row_ap;
							}
						}
					}
				}
				
				if(!empty($pal_3) && count($pal_3))
				{
					for($pl=0;$pl<count($pal_3);$pl++)
					{
						$chk_typep_1 = 'artist_event|'.$pal_3[$pl]['id'];
						
						$sql_as = mysql_query("SELECT * FROM general_media WHERE from_info='".$chk_typep_1."' AND delete_status=0 AND media_status=0 AND media_type='".$type."' AND general_user_id!='".$id['general_user_id']."'");
						if(mysql_num_rows($sql_as)>0)
						{
							while($row_ae = mysql_fetch_assoc($sql_as))
							{
								$final_c_f[] = $row_ae;
							}
						}
					}
				}
			}
			
			if($id['community_id']!=0 && !empty($id['community_id']))
			{			
				$sql_get_allp = mysql_query("SELECT * FROM community_project WHERE community_id='".$id['community_id']."' AND del_status=0 AND active=0");
				if(mysql_num_rows($sql_get_allp)>0)
				{
					while($rowal = mysql_fetch_assoc($sql_get_allp))
					{
						$pal_2[] = $rowal;
					}
				}
				
				$sql_get_allew = mysql_query("SELECT * FROM community_event WHERE community_id='".$id['community_id']."' AND del_status=0 AND active=0");
				if(mysql_num_rows($sql_get_allew)>0)
				{
					while($rowawl = mysql_fetch_assoc($sql_get_allew))
					{
						$pal_4[] = $rowawl;
					}
				}
				
				if(!empty($pal_2) && count($pal_2))
				{
					for($pl=0;$pl<count($pal_1);$pl++)
					{
						$chk_typep_1 = 'community_project|'.$pal_1[$pl]['id'];
						
						$sql_as = mysql_query("SELECT * FROM general_media WHERE creator_info='".$chk_typep_1."' AND delete_status=0 AND media_status=0 AND media_type='".$type."' AND general_user_id!='".$id['general_user_id']."'");
						if(mysql_num_rows($sql_as)>0)
						{
							while($row_cp = mysql_fetch_assoc($sql_as))
							{
								$final_c_f[] = $row_cp;
							}
						}
					}
				}
				
				if(!empty($pal_4) && count($pal_4))
				{
					for($pl=0;$pl<count($pal_4);$pl++)
					{
						$chk_typep_1 = 'community_event|'.$pal_4[$pl]['id'];
						
						$sql_as = mysql_query("SELECT * FROM general_media WHERE from_info='".$chk_typep_1."' AND delete_status=0 AND media_status=0 AND media_type='".$type."' AND general_user_id!='".$id['general_user_id']."'");
						if(mysql_num_rows($sql_as)>0)
						{
							while($row_ce = mysql_fetch_assoc($sql_as))
							{
								$final_c_f[] = $row_ce;
							}
						}
					}
				}
			} */
			//$final_c_f = array_merge($final_c_f1,$final_c_f2);
			return $final_c_f;
		}
		function get_all_del_media_id()
		{
			$all_deleted_id = "";
			$sql = mysql_query("select * from general_user where email='".$_SESSION['login_email']."'");
			if($sql!="")
			{
				if(mysql_num_rows($sql)>0)
				{
					$res = mysql_fetch_assoc($sql);
					if(isset($res['deleted_media_by_tag_user'])) {
					
						$all_deleted_id = $res['deleted_media_by_tag_user'];
						if($res['artist_id'] >0)
						{
							$get_art = mysql_query("select * from general_artist where artist_id='".$res['artist_id']."'");
							if(mysql_num_rows($get_art)>0)
							{
								$res_art = mysql_fetch_assoc($get_art);
								if(isset($res_art['deleted_media_by_creator_creator']) || isset($res_art['deleted_media_by_from_from']))
								{
									if(isset($res_art['deleted_media_by_from_from']))
									{
										if($res_art['deleted_media_by_from_from']!="")
										{
											$all_deleted_id = $all_deleted_id ."," .$res_art['deleted_media_by_from_from'];
										}
									}
									if(isset($res_art['deleted_media_by_creator_creator']))
									{
										if($res_art['deleted_media_by_creator_creator'] !="")
										{
											$all_deleted_id = $all_deleted_id ."," .$res_art['deleted_media_by_creator_creator'];
										}
									}
								}
							}
							$get_art_pro = mysql_query("select * from artist_project where artist_id='".$res['artist_id']."'");
							if(mysql_num_rows($get_art_pro)>0)
							{
								while($res_art_pro = mysql_fetch_assoc($get_art_pro))
								{
									/* if($res_art_pro['deleted_media_by_creator_creator'] !="" || $res_art_pro['deleted_media_by_from_from'] !=""){
									$all_deleted_id = $all_deleted_id ."," . $res_art_pro['deleted_media_by_creator_creator'].",".$res_art_pro['deleted_media_by_from_from'];} */
									if(isset($res_art_pro['deleted_media_by_creator_creator']) || isset($res_art_pro['deleted_media_by_from_from']))
									{
										if(isset($res_art_pro['deleted_media_by_from_from']))
										{
											if($res_art_pro['deleted_media_by_from_from']!="")
											{
												$all_deleted_id = $all_deleted_id ."," .$res_art_pro['deleted_media_by_from_from'];
											}
										}
										if(isset($res_art_pro['deleted_media_by_creator_creator']))
										{
											if($res_art_pro['deleted_media_by_creator_creator'] !="")
											{
												$all_deleted_id = $all_deleted_id ."," .$res_art_pro['deleted_media_by_creator_creator'];
											}
										}
									}
								}
							}
							$get_art_eve = mysql_query("select * from artist_event where artist_id='".$res['artist_id']."'");
							if(mysql_num_rows($get_art_eve)>0)
							{
								while($res_art_eve = mysql_fetch_assoc($get_art_eve))
								{
									/* if($res_art_eve['deleted_media_by_creator_creator'] !="" || $res_art_eve['deleted_media_by_from_from']!=""){
									$all_deleted_id = $all_deleted_id ."," . $res_art_eve['deleted_media_by_creator_creator'].",".$res_art_eve['deleted_media_by_from_from'];} */
									if(isset($res_art_eve['deleted_media_by_creator_creator']) || isset($res_art_eve['deleted_media_by_from_from']))
									{
										if(isset($res_art_eve['deleted_media_by_from_from']))
										{
											if($res_art_eve['deleted_media_by_from_from']!="")
											{
												$all_deleted_id = $all_deleted_id ."," .$res_art_eve['deleted_media_by_from_from'];
											}
										}
										if(isset($res_art_eve['deleted_media_by_creator_creator']))
										{
											if($res_art_eve['deleted_media_by_creator_creator'] !="")
											{
												$all_deleted_id = $all_deleted_id ."," .$res_art_eve['deleted_media_by_creator_creator'];
											}
										}
									}
								}
							}
						}
						if($res['community_id'] >0)
						{
							$get_com = mysql_query("select * from general_community where community_id='".$res['community_id']."'");
							if(mysql_num_rows($get_com)>0)
							{
								$res_com = mysql_fetch_assoc($get_com);
								/* if($res_com['deleted_media_by_creator_creator'] !="" || $res_com['deleted_media_by_from_from'] !=""){
								$all_deleted_id = $all_deleted_id ."," . $res_com['deleted_media_by_creator_creator'].",".$res_com['deleted_media_by_from_from'];} */
								if(isset($res_com['deleted_media_by_creator_creator']) || isset($res_com['deleted_media_by_from_from']))
									{
										if(isset($res_com['deleted_media_by_from_from']))
										{
											if($res_com['deleted_media_by_from_from']!="")
											{
												$all_deleted_id = $all_deleted_id ."," .$res_com['deleted_media_by_from_from'];
											}
										}
										if(isset($res_com['deleted_media_by_creator_creator']))
										{
											if($res_com['deleted_media_by_creator_creator'] !="")
											{
												$all_deleted_id = $all_deleted_id ."," .$res_com['deleted_media_by_creator_creator'];
											}
										}
									}
							}
							$get_com_pro = mysql_query("select * from community_project where community_id='".$res['community_id']."'");
							if(mysql_num_rows($get_com_pro)>0)
							{
								while($res_com_pro = mysql_fetch_assoc($get_com_pro))
								{
									/* if($res_com_pro['deleted_media_by_creator_creator'] !="" || $res_com_pro['deleted_media_by_from_from'] !=""){
									$all_deleted_id = $all_deleted_id ."," . $res_com_pro['deleted_media_by_creator_creator'].",".$res_com_pro['deleted_media_by_from_from'];} */
									if(isset($res_com_pro['deleted_media_by_creator_creator']) || isset($res_com_pro['deleted_media_by_from_from']))
									{
										if(isset($res_com_pro['deleted_media_by_from_from']))
										{
											if($res_com_pro['deleted_media_by_from_from']!="")
											{
												$all_deleted_id = $all_deleted_id ."," .$res_com_pro['deleted_media_by_from_from'];
											}
										}
										if(isset($res_com_pro['deleted_media_by_creator_creator']))
										{
											if($res_com_pro['deleted_media_by_creator_creator'] !="")
											{
												$all_deleted_id = $all_deleted_id ."," .$res_com_pro['deleted_media_by_creator_creator'];
											}
										}
									}
								}
							}
							$get_com_eve = mysql_query("select * from community_event where community_id='".$res['community_id']."'");
							if(mysql_num_rows($get_com_eve)>0)
							{
								while($res_com_eve = mysql_fetch_assoc($get_com_eve))
								{
									/* if($res_com_eve['deleted_media_by_creator_creator'] !="" || $res_com_eve['deleted_media_by_from_from'] !=""){
									$all_deleted_id = $all_deleted_id ."," . $res_com_eve['deleted_media_by_creator_creator'].",".$res_com_eve['deleted_media_by_from_from'];} */
									if(isset($res_com_eve['deleted_media_by_creator_creator']) || isset($res_com_eve['deleted_media_by_from_from']))
									{
										if(isset($res_com_eve['deleted_media_by_from_from']))
										{
											if($res_com_eve['deleted_media_by_from_from']!="")
											{
												$all_deleted_id = $all_deleted_id ."," .$res_com_eve['deleted_media_by_from_from'];
											}
										}
										if(isset($res_com_eve['deleted_media_by_creator_creator']))
										{
											if($res_com_eve['deleted_media_by_creator_creator'] !="")
											{
												$all_deleted_id = $all_deleted_id ."," .$res_com_eve['deleted_media_by_creator_creator'];
											}
										}
									}
								}
							}
						}
					}
					return($all_deleted_id);
				}
			}
		}
		
		function only_creator2pr_this()
		{
			$id = $this->sel_general();
			$var_type_1 = 'general_artist';
			$var_type_2 = 'general_community';
			$var_type_3 = 'artist_project';
			$var_type_4 = 'community_project';
			
			$art_p_ao = array();
			$art_c_ao = array();
			
			$com_p_ao = array();
			$com_c_ao = array();
			
			$com_a_ao = array();
			$art_a_ao = array();
			
			$art_ap_ao = array();
			$com_cp_ao = array();
			
			$pal_1 = array();
			$pal_2 = array();
			
			if($id['artist_id']!=0 && !empty($id['artist_id']))
			{
				$chk_type = $var_type_1.'|'.$id['artist_id'];
				
				$sql_get_allp = mysql_query("SELECT * FROM artist_project WHERE artist_id='".$id['artist_id']."' AND del_status=0 AND active=0");
				if(mysql_num_rows($sql_get_allp)>0)
				{
					while($rowal = mysql_fetch_assoc($sql_get_allp))
					{
						//$rowal['whos_project'] = "only_creator";
						$pal_1[] = $rowal;
					}
				}
				
				if(!empty($pal_1) && count($pal_1))
				{
					for($pl=0;$pl<count($pal_1);$pl++)
					{
						$chk_typep_1 = 'artist_project|'.$pal_1[$pl]['id'];
						
						$sql = mysql_query("SELECT * FROM artist_project WHERE (creators_info='".$chk_typep_1."' OR creators_info='".$chk_type."') AND del_status=0 AND active=0");
						if(mysql_num_rows($sql)>0)
						{
							while($row_po = mysql_fetch_assoc($sql))
							{
								//if($row_po['artist_id']!=$id['artist_id'])
								//{
									
									$sql_art2 = mysql_query("SELECT * FROM artist_project WHERE creators_info='artist_project|".$row_po['id']."' AND del_status=0 AND active=0");
									if(mysql_num_rows($sql_art2)>0)
									{
										while($row2_po = mysql_fetch_assoc($sql_art2))
										{
											$row2_po['table_name'] = 'general_artist';
											$row2_po['type'] = 'project';
											$art_p_ao[] = $row2_po;
										}
									}
									
									
									$sql_art3 = mysql_query("SELECT * FROM community_project WHERE creators_info='artist_project|".$row_po['id']."' AND del_status=0 AND active=0");
									if(mysql_num_rows($sql_art3)>0)
									{
										while($row3_po = mysql_fetch_assoc($sql_art3))
										{
											$row3_po['table_name'] = 'general_community';
											$row3_po['type'] = 'project';
											$art_c_ao[] = $row3_po;
										}
									}
								//}
							}
						}
						
						
						$sql = mysql_query("SELECT * FROM community_project WHERE (creators_info='".$chk_typep_1."' OR creators_info='".$chk_type."') AND del_status=0 AND active=0");
						if(mysql_num_rows($sql)>0)
						{
							while($row_poo = mysql_fetch_assoc($sql))
							{
								//if($row_poo['community_id']!=$id['community_id'])
								//{		
									
									$sql_art2 = mysql_query("SELECT * FROM artist_project WHERE creators_info='community_project|".$row_poo['id']."' AND del_status=0 AND active=0");
									if(mysql_num_rows($sql_art2)>0)
									{
										while($row2_po = mysql_fetch_assoc($sql_art2))
										{
											$row2_po['table_name'] = 'general_artist';
											$row2_po['type'] = 'project';
											$com_p_ao[] = $row2_po;
										}
									}
									
									
									$sql_art3 = mysql_query("SELECT * FROM community_project WHERE creators_info='community_project|".$row_poo['id']."' AND del_status=0 AND active=0");
									if(mysql_num_rows($sql_art3)>0)
									{
										while($row3_po = mysql_fetch_assoc($sql_art3))
										{
											$row3_po['table_name'] = 'general_community';
											$row3_po['type'] = 'project';
											$com_c_ao[] = $row3_po;
										}
									}
								//}
							}
						}
					}
				}
			}
			
			if($id['community_id']!=0 && !empty($id['community_id']))
			{
				$chk_type = $var_type_2.'|'.$id['community_id'];
				
				$sql_get_allp = mysql_query("SELECT * FROM community_project WHERE community_id='".$id['community_id']."' AND del_status=0 AND active=0");
				if(mysql_num_rows($sql_get_allp)>0)
				{
					while($rowal = mysql_fetch_assoc($sql_get_allp))
					{
						//$rowal['whos_project'] = "only_creator";
						$pal_2[] = $rowal;
					}
				}
				
				if(!empty($pal_2) && count($pal_2))
				{
					for($pl2=0;$pl2<count($pal_2);$pl2++)
					{
						$chk_typep_2 = 'community_project|'.$pal_2[$pl2]['id'];
						
						
						$sql = mysql_query("SELECT * FROM artist_project WHERE (creators_info='".$chk_typep_2."' OR creators_info='".$chk_type."') AND del_status=0 AND active=0");
						if(mysql_num_rows($sql)>0)
						{
							while($row_po = mysql_fetch_assoc($sql))
							{
								//if($row_po['artist_id']!=$id['artist_id'])
								//{
									
									$sql_art2 = mysql_query("SELECT * FROM artist_project WHERE creators_info='artist_project|".$row_po['id']."' AND del_status=0 AND active=0");
									if(mysql_num_rows($sql_art2)>0)
									{
										while($row2_po = mysql_fetch_assoc($sql_art2))
										{
											$row2_po['table_name'] = 'general_artist';
											$row2_po['type'] = 'project';
											$com_a_ao[] = $row2_po;
										}
									}
									
									
									$sql_art3 = mysql_query("SELECT * FROM community_project WHERE creators_info='artist_project|".$row_po['id']."' AND del_status=0 AND active=0");
									if(mysql_num_rows($sql_art3)>0)
									{
										while($row3_po = mysql_fetch_assoc($sql_art3))
										{
											$row3_po['table_name'] = 'general_community';
											$row3_po['type'] = 'project';
											$art_a_ao[] = $row3_po;
										}
									}
								//}
							}
						}
						
						
						$sql = mysql_query("SELECT * FROM community_project WHERE (creators_info='".$chk_typep_2."' OR creators_info='".$chk_type."') AND del_status=0 AND active=0");
						if(mysql_num_rows($sql)>0)
						{
							while($row_poo = mysql_fetch_assoc($sql))
							{
								//if($row_poo['community_id']!=$id['community_id'])
								//{
									
									$sql_art2 = mysql_query("SELECT * FROM artist_project WHERE creators_info='community_project|".$row_poo['id']."' AND del_status=0 AND active=0");
									if(mysql_num_rows($sql_art2)>0)
									{
										while($row2_po = mysql_fetch_assoc($sql_art2))
										{
											$row2_po['table_name'] = 'general_artist';
											$row2_po['type'] = 'project';
											$art_ap_ao[] = $row2_po;
										}
									}
									
									
									$sql_art3 = mysql_query("SELECT * FROM community_project WHERE creators_info='community_project|".$row_poo['id']."' AND del_status=0 AND active=0");
									if(mysql_num_rows($sql_art3)>0)
									{
										while($row3_po = mysql_fetch_assoc($sql_art3))
										{
											$row3_po['table_name'] = 'general_community';
											$row3_po['type'] = 'project';
											$com_cp_ao[] = $row3_po;
										}
									}
								//}
							}
						}
					}
				}
			}		
			
			$totals = array();
			$totals = array_merge($art_p_ao,$art_c_ao,$com_p_ao,$com_c_ao,$com_a_ao,$art_a_ao,$art_ap_ao,$com_cp_ao);
			return $totals;
		}
		
		function get_project_tagged_by_other2_user($id)
		{
			$sql_1_cre = "";		
			
			$date = date("Y-m-d");
			$sql14 = mysql_query("select * from artist_project where id='".$id."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql14)>0)
			{
				$res14 = mysql_fetch_assoc($sql14);
				$cret_info = 'artist_project|'.$res14['id'];
				
				$sql_1_cre = mysql_query("SELECT * FROM artist_project WHERE creators_info='".$cret_info."' AND active=0 AND del_status=0");
				
				return $sql_1_cre;
			}
		}
		
		function get_project_ctagged_by_other2_user($id)
		{
			$sql_2_cre = "";		
			
			$date = date("Y-m-d");
			$sql14 = mysql_query("select * from artist_project where id='".$id."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql14)>0)
			{
				$res14 = mysql_fetch_assoc($sql14);
				$cret_info = 'artist_project|'.$res14['id'];
				
				$sql_2_cre = mysql_query("SELECT * FROM community_project WHERE creators_info='".$cret_info."' AND active=0 AND del_status=0");
				
				return $sql_2_cre;
			}
		}
		
		function get_cproject_tagged_by_other2_user($id)
		{
			$sql_1_cre = "";
			
			$date=date("Y-m-d");
			$sql14 = mysql_query("select * from community_project where id='".$id."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql14)>0)
			{
				$res14 = mysql_fetch_assoc($sql14);
				$cret_info = 'community_project|'.$res14['id'];
					
				$sql_1_cre = mysql_query("SELECT * FROM artist_project WHERE creators_info='".$cret_info."' AND active=0 AND del_status=0");
				return $sql_1_cre;
			}
		}
		
		function get_cproject_ctagged_by_other2_user($id)
		{
			$sql_2_cre = "";
			
			$date=date("Y-m-d");
			$sql14 = mysql_query("select * from community_project where id='".$id."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql14)>0)
			{
				$res14 = mysql_fetch_assoc($sql14);
				$cret_info = 'community_project|'.$res14['id'];
				
				$sql_2_cre = mysql_query("SELECT * FROM community_project WHERE creators_info='".$cret_info."' AND active=0 AND del_status=0");
				
				return $sql_2_cre;
			}
		}
		
		function only_creator2ev_upevent()
		{
			$id = $this->sel_general();
			$date = date("Y-m-d");
			
			$var_type_1 = 'general_artist';
			$var_type_2 = 'general_community';
			$var_type_3 = 'artist_project';
			$var_type_4 = 'community_project';
			
			$art_p_ao = array();
			$art_c_ao = array();
			
			$com_p_ao = array();
			$com_c_ao = array();
			
			$com_a_ao = array();
			$art_a_ao = array();
			
			$art_ap_ao = array();
			$com_cp_ao = array();
			
			$pal_1 = array();
			$pal_2 = array();
			
			if($id['artist_id']!=0 && !empty($id['artist_id']))
			{
				$chk_type = $var_type_1.'|'.$id['artist_id'];
				
				$sql_get_allp = mysql_query("SELECT * FROM artist_project WHERE artist_id='".$id['artist_id']."' AND del_status=0 AND active=0");
				if(mysql_num_rows($sql_get_allp)>0)
				{
					while($rowal = mysql_fetch_assoc($sql_get_allp))
					{
						//$rowal['whos_project'] = "only_creator";
						$pal_1[] = $rowal;
					}
				}
				
				if(!empty($pal_1) && count($pal_1))
				{
					for($pl=0;$pl<count($pal_1);$pl++)
					{
						$chk_typep_1 = 'artist_project|'.$pal_1[$pl]['id'];
						
						$sql = mysql_query("SELECT * FROM artist_project WHERE (creators_info='".$chk_typep_1."' OR creators_info='".$chk_type."') AND del_status=0 AND active=0");
						if(mysql_num_rows($sql)>0)
						{
							while($row_po = mysql_fetch_assoc($sql))
							{
								//if($row_po['artist_id']!=$id['artist_id'])
								//{
									
									$sql_art2 = mysql_query("SELECT * FROM artist_event WHERE creators_info='artist_project|".$row_po['id']."' AND del_status=0 AND active=0 AND date>='".$date."'");
									if(mysql_num_rows($sql_art2)>0)
									{
										while($row2_po = mysql_fetch_assoc($sql_art2))
										{
											$row2_po['table_name'] = 'general_artist';
											$art_p_ao[] = $row2_po;
										}
									}
									
									
									$sql_art3 = mysql_query("SELECT * FROM community_event WHERE creators_info='artist_project|".$row_po['id']."' AND del_status=0 AND active=0 AND date>='".$date."'");
									if(mysql_num_rows($sql_art3)>0)
									{
										while($row3_po = mysql_fetch_assoc($sql_art3))
										{
											$row3_po['table_name'] = 'general_community';
											$art_c_ao[] = $row3_po;
										}
									}
								//}
							}
						}
						
						
						$sql = mysql_query("SELECT * FROM community_project WHERE (creators_info='".$chk_typep_1."' OR creators_info='".$chk_type."') AND del_status=0 AND active=0");
						if(mysql_num_rows($sql)>0)
						{
							while($row_poo = mysql_fetch_assoc($sql))
							{
								//if($row_poo['community_id']!=$id['community_id'])
								//{		
									
									$sql_art2 = mysql_query("SELECT * FROM artist_event WHERE creators_info='community_project|".$row_poo['id']."' AND del_status=0 AND active=0 AND date>='".$date."'");
									if(mysql_num_rows($sql_art2)>0)
									{
										while($row2_po = mysql_fetch_assoc($sql_art2))
										{
											$row2_po['table_name'] = 'general_artist';
											$com_p_ao[] = $row2_po;
										}
									}
									
									
									$sql_art3 = mysql_query("SELECT * FROM community_event WHERE creators_info='community_project|".$row_poo['id']."' AND del_status=0 AND active=0 AND date>='".$date."'");
									if(mysql_num_rows($sql_art3)>0)
									{
										while($row3_po = mysql_fetch_assoc($sql_art3))
										{
											$row3_po['table_name'] = 'general_community';
											$com_c_ao[] = $row3_po;
										}
									}
								//}
							}
						}
					}
				}
			}
			
			if($id['community_id']!=0 && !empty($id['community_id']))
			{
				$chk_type = $var_type_2.'|'.$id['community_id'];
				
				$sql_get_allp = mysql_query("SELECT * FROM community_project WHERE community_id='".$id['community_id']."' AND del_status=0 AND active=0");
				if(mysql_num_rows($sql_get_allp)>0)
				{
					while($rowal = mysql_fetch_assoc($sql_get_allp))
					{
						//$rowal['whos_project'] = "only_creator";
						$pal_2[] = $rowal;
					}
				}
				
				if(!empty($pal_2) && count($pal_2))
				{
					for($pl2=0;$pl2<count($pal_2);$pl2++)
					{
						$chk_typep_2 = 'community_project|'.$pal_2[$pl2]['id'];
						
						
						$sql = mysql_query("SELECT * FROM artist_project WHERE (creators_info='".$chk_typep_2."' OR creators_info='".$chk_type."') AND del_status=0 AND active=0");
						if(mysql_num_rows($sql)>0)
						{
							while($row_po = mysql_fetch_assoc($sql))
							{
								//if($row_po['artist_id']!=$id['artist_id'])
								//{
									
									$sql_art2 = mysql_query("SELECT * FROM artist_event WHERE creators_info='artist_project|".$row_po['id']."' AND del_status=0 AND active=0 AND date>='".$date."'");
									if(mysql_num_rows($sql_art2)>0)
									{
										while($row2_po = mysql_fetch_assoc($sql_art2))
										{
											$row2_po['table_name'] = 'general_artist';
											$com_a_ao[] = $row2_po;
										}
									}
									
									
									$sql_art3 = mysql_query("SELECT * FROM community_event WHERE creators_info='artist_project|".$row_po['id']."' AND del_status=0 AND active=0 AND date>='".$date."'");
									if(mysql_num_rows($sql_art3)>0)
									{
										while($row3_po = mysql_fetch_assoc($sql_art3))
										{
											$row3_po['table_name'] = 'general_community';
											$art_a_ao[] = $row3_po;
										}
									}
								//}
							}
						}
						
						
						$sql = mysql_query("SELECT * FROM community_project WHERE (creators_info='".$chk_typep_2."' OR creators_info='".$chk_type."') AND del_status=0 AND active=0");
						if(mysql_num_rows($sql)>0)
						{
							while($row_poo = mysql_fetch_assoc($sql))
							{
								//if($row_poo['community_id']!=$id['community_id'])
								//{
									
									$sql_art2 = mysql_query("SELECT * FROM artist_event WHERE creators_info='community_project|".$row_poo['id']."' AND del_status=0 AND active=0 AND date>='".$date."'");
									if(mysql_num_rows($sql_art2)>0)
									{
										while($row2_po = mysql_fetch_assoc($sql_art2))
										{
											$row2_po['table_name'] = 'general_artist';
											$art_ap_ao[] = $row2_po;
										}
									}
									
									
									$sql_art3 = mysql_query("SELECT * FROM community_event WHERE creators_info='community_project|".$row_poo['id']."' AND del_status=0 AND active=0 AND date>='".$date."'");
									if(mysql_num_rows($sql_art3)>0)
									{
										while($row3_po = mysql_fetch_assoc($sql_art3))
										{
											$row3_po['table_name'] = 'general_community';
											$com_cp_ao[] = $row3_po;
										}
									}
								//}
							}
						}
					}
				}
			}		
			
			$totals = array();
			$totals = array_merge($art_p_ao,$art_c_ao,$com_p_ao,$com_c_ao,$com_a_ao,$art_a_ao,$art_ap_ao,$com_cp_ao);
			return $totals;
		}
		
		function only_creator2_recevent()
		{
			$id = $this->sel_general();
			$date = date("Y-m-d");
			
			$var_type_1 = 'general_artist';
			$var_type_2 = 'general_community';
			$var_type_3 = 'artist_project';
			$var_type_4 = 'community_project';
			
			$art_p_ao = array();
			$art_c_ao = array();
			
			$com_p_ao = array();
			$com_c_ao = array();
			
			$com_a_ao = array();
			$art_a_ao = array();
			
			$art_ap_ao = array();
			$com_cp_ao = array();
			
			$pal_1 = array();
			$pal_2 = array();
			
			if($id['artist_id']!=0 && !empty($id['artist_id']))
			{
				$chk_type = $var_type_1.'|'.$id['artist_id'];
				
				$sql_get_allp = mysql_query("SELECT * FROM artist_project WHERE artist_id='".$id['artist_id']."' AND del_status=0 AND active=0");
				if(mysql_num_rows($sql_get_allp)>0)
				{
					while($rowal = mysql_fetch_assoc($sql_get_allp))
					{
						//$rowal['whos_project'] = "only_creator";
						$pal_1[] = $rowal;
					}
				}
				
				if(!empty($pal_1) && count($pal_1))
				{
					for($pl=0;$pl<count($pal_1);$pl++)
					{
						$chk_typep_1 = 'artist_project|'.$pal_1[$pl]['id'];
						
						$sql = mysql_query("SELECT * FROM artist_project WHERE (creators_info='".$chk_typep_1."' OR creators_info='".$chk_type."') AND del_status=0 AND active=0");
						if(mysql_num_rows($sql)>0)
						{
							while($row_po = mysql_fetch_assoc($sql))
							{
								//if($row_po['artist_id']!=$id['artist_id'])
								//{
									
									$sql_art2 = mysql_query("SELECT * FROM artist_event WHERE creators_info='artist_project|".$row_po['id']."' AND del_status=0 AND active=0 AND date<'".$date."'");
									if(mysql_num_rows($sql_art2)>0)
									{
										while($row2_po = mysql_fetch_assoc($sql_art2))
										{
											$row2_po['table_name'] = 'general_artist';
											$art_p_ao[] = $row2_po;
										}
									}
									
									
									$sql_art3 = mysql_query("SELECT * FROM community_event WHERE creators_info='artist_project|".$row_po['id']."' AND del_status=0 AND active=0 AND date<'".$date."'");
									if(mysql_num_rows($sql_art3)>0)
									{
										while($row3_po = mysql_fetch_assoc($sql_art3))
										{
											$row3_po['table_name'] = 'general_community';
											$art_c_ao[] = $row3_po;
										}
									}
								//}
							}
						}
						
						
						$sql = mysql_query("SELECT * FROM community_project WHERE (creators_info='".$chk_typep_1."' OR creators_info='".$chk_type."') AND del_status=0 AND active=0");
						if(mysql_num_rows($sql)>0)
						{
							while($row_poo = mysql_fetch_assoc($sql))
							{
								//if($row_poo['community_id']!=$id['community_id'])
								//{		
									
									$sql_art2 = mysql_query("SELECT * FROM artist_event WHERE creators_info='community_project|".$row_poo['id']."' AND del_status=0 AND active=0 AND date<'".$date."'");
									if(mysql_num_rows($sql_art2)>0)
									{
										while($row2_po = mysql_fetch_assoc($sql_art2))
										{
											$row2_po['table_name'] = 'general_artist';
											$com_p_ao[] = $row2_po;
										}
									}
									
									
									$sql_art3 = mysql_query("SELECT * FROM community_event WHERE creators_info='community_project|".$row_poo['id']."' AND del_status=0 AND active=0 AND date<'".$date."'");
									if(mysql_num_rows($sql_art3)>0)
									{
										while($row3_po = mysql_fetch_assoc($sql_art3))
										{
											$row3_po['table_name'] = 'general_community';
											$com_c_ao[] = $row3_po;
										}
									}
								//}
							}
						}
					}
				}
			}
			
			if($id['community_id']!=0 && !empty($id['community_id']))
			{
				$chk_type = $var_type_2.'|'.$id['community_id'];
				
				$sql_get_allp = mysql_query("SELECT * FROM community_project WHERE community_id='".$id['community_id']."' AND del_status=0 AND active=0");
				if(mysql_num_rows($sql_get_allp)>0)
				{
					while($rowal = mysql_fetch_assoc($sql_get_allp))
					{
						//$rowal['whos_project'] = "only_creator";
						$pal_2[] = $rowal;
					}
				}
				
				if(!empty($pal_2) && count($pal_2))
				{
					for($pl2=0;$pl2<count($pal_2);$pl2++)
					{
						$chk_typep_2 = 'community_project|'.$pal_2[$pl2]['id'];
						
						
						$sql = mysql_query("SELECT * FROM artist_project WHERE (creators_info='".$chk_typep_2."' OR creators_info='".$chk_type."') AND del_status=0 AND active=0");
						if(mysql_num_rows($sql)>0)
						{
							while($row_po = mysql_fetch_assoc($sql))
							{
								//if($row_po['artist_id']!=$id['artist_id'])
								//{
									
									$sql_art2 = mysql_query("SELECT * FROM artist_event WHERE creators_info='artist_project|".$row_po['id']."' AND del_status=0 AND active=0 AND date<'".$date."'");
									if(mysql_num_rows($sql_art2)>0)
									{
										while($row2_po = mysql_fetch_assoc($sql_art2))
										{
											$row2_po['table_name'] = 'general_artist';
											$com_a_ao[] = $row2_po;
										}
									}
									
									
									$sql_art3 = mysql_query("SELECT * FROM community_event WHERE creators_info='artist_project|".$row_po['id']."' AND del_status=0 AND active=0 AND date<'".$date."'");
									if(mysql_num_rows($sql_art3)>0)
									{
										while($row3_po = mysql_fetch_assoc($sql_art3))
										{
											$row3_po['table_name'] = 'general_community';
											$art_a_ao[] = $row3_po;
										}
									}
								//}
							}
						}
						
						
						$sql = mysql_query("SELECT * FROM community_project WHERE (creators_info='".$chk_typep_2."' OR creators_info='".$chk_type."') AND del_status=0 AND active=0");
						if(mysql_num_rows($sql)>0)
						{
							while($row_poo = mysql_fetch_assoc($sql))
							{
								//if($row_poo['community_id']!=$id['community_id'])
								//{
									
									$sql_art2 = mysql_query("SELECT * FROM artist_event WHERE creators_info='community_project|".$row_poo['id']."' AND del_status=0 AND active=0 AND date<'".$date."'");
									if(mysql_num_rows($sql_art2)>0)
									{
										while($row2_po = mysql_fetch_assoc($sql_art2))
										{
											$row2_po['table_name'] = 'general_artist';
											$art_ap_ao[] = $row2_po;
										}
									}
									
									
									$sql_art3 = mysql_query("SELECT * FROM community_event WHERE creators_info='community_project|".$row_poo['id']."' AND del_status=0 AND active=0 AND date<'".$date."'");
									if(mysql_num_rows($sql_art3)>0)
									{
										while($row3_po = mysql_fetch_assoc($sql_art3))
										{
											$row3_po['table_name'] = 'general_community';
											$com_cp_ao[] = $row3_po;
										}
									}
								//}
							}
						}
					}
				}
			}		
			
			$totals = array();
			$totals = array_merge($art_p_ao,$art_c_ao,$com_p_ao,$com_c_ao,$com_a_ao,$art_a_ao,$art_ap_ao,$com_cp_ao);
			return $totals;
		}
		
		function get_deleted_project_id()
		{
			$get_gen_det = $this->sel_general();
			$res_art_id ="";
			$get_art_id = mysql_query("select * from general_community where community_id='".$get_gen_det['community_id']."'");
			$get_art_id_1 = mysql_query("select * from general_artist where artist_id='".$get_gen_det['artist_id']."'");
			if(mysql_num_rows($get_art_id)>0)
			{
				$res_art_id = mysql_fetch_assoc($get_art_id);
			}
			elseif(mysql_num_rows($get_art_id_1)>0)
			{
				$res_art_id = mysql_fetch_assoc($get_art_id_1);
			}
			return($res_art_id);
		}
		
		function get_event_tagged_by_other2_userrec($id)
		{
			$sql_2_cre = "";
			
			$date=date("Y-m-d");
			$sql14 = mysql_query("select * from artist_project where id='".$id."' AND del_status=0 AND active=0");
			
			if(mysql_num_rows($sql14)>0)
			{
				$res14 = mysql_fetch_assoc($sql14);
				//while($res14 = mysql_fetch_assoc($sql14))
				//{
				$cret_info = 'artist_project|'.$res14['id'];
				
				$sql_2_cre = mysql_query("SELECT * FROM artist_event WHERE creators_info='".$cret_info."' AND active=0 AND del_status=0 AND date<'".$date."'");
				return $sql_2_cre;
			}
		}
		
		function get_event_ctagged_by_other2_userrec($id)
		{
			$sql_2_cre = "";
			
			$date=date("Y-m-d");
			$sql14 = mysql_query("select * from artist_project where id='".$id."' AND del_status=0 AND active=0");
			
			if(mysql_num_rows($sql14)>0)
			{
				$res14 = mysql_fetch_assoc($sql14);
				//while($res14 = mysql_fetch_assoc($sql14))
				//{
				$cret_info = 'artist_project|'.$res14['id'];
				
				$sql_2_cre = mysql_query("SELECT * FROM community_event WHERE creators_info='".$cret_info."' AND active=0 AND del_status=0 AND date<'".$date."'");
				return $sql_2_cre;
			}
		}
		
		function get_cevent_tagged_by_other2_userrec($id)
		{
			$sql_2_cre = "";
			
			$date=date("Y-m-d");
			$sql14 = mysql_query("select * from community_project where id='".$id."' AND del_status=0 AND active=0");
			
			if(mysql_num_rows($sql14)>0)
			{
				$res14 = mysql_fetch_assoc($sql14);
				//while($res14 = mysql_fetch_assoc($sql14))
				//{
				$cret_info = 'community_project|'.$res14['id'];
				
				$sql_2_cre = mysql_query("SELECT * FROM artist_event WHERE creators_info='".$cret_info."' AND active=0 AND del_status=0 AND date<'".$date."'");
				return $sql_2_cre;
			}
		}
		
		function get_cevent_ctagged_by_other2_userrec($id)
		{
			$sql_2_cre = "";
			
			$date=date("Y-m-d");
			$sql14 = mysql_query("select * from community_project where id='".$id."' AND del_status=0 AND active=0");
			
			if(mysql_num_rows($sql14)>0)
			{
				$res14 = mysql_fetch_assoc($sql14);
				//while($res14 = mysql_fetch_assoc($sql14))
				//{
				$cret_info = 'community_project|'.$res14['id'];
				
				$sql_2_cre = mysql_query("SELECT * FROM community_event WHERE creators_info='".$cret_info."' AND active=0 AND del_status=0 AND date<'".$date."'");
				return $sql_2_cre;
			}
		}
		
		function get_cevent_tagged_by_other2_user($id)
		{
			$sql_1_cre = "";
			
			$date=date("Y-m-d");
			$sql14 = mysql_query("select * from community_project where id='".$id."' AND del_status=0 AND active=0");
			
			if(mysql_num_rows($sql14)>0)
			{
				$res14 = mysql_fetch_assoc($sql14);
				//while($res14 = mysql_fetch_assoc($sql14))
				//{
				$cret_info = 'community_project|'.$res14['id'];
				
				$sql_1_cre = mysql_query("SELECT * FROM artist_event WHERE creators_info='".$cret_info."' AND active=0 AND del_status=0 AND date>='".$date."'");
				return $sql_1_cre;
			}
		}
		
		function get_cevent_ctagged_by_other2_user($id)
		{
			$sql_2_cre = "";
			
			$date=date("Y-m-d");
			$sql14 = mysql_query("select * from community_project where id='".$id."' AND del_status=0 AND active=0");
			
			if(mysql_num_rows($sql14)>0)
			{
				$res14 = mysql_fetch_assoc($sql14);
				//while($res14 = mysql_fetch_assoc($sql14))
				//{
				$cret_info = 'community_project|'.$res14['id'];
				
				$sql_2_cre = mysql_query("SELECT * FROM community_event WHERE creators_info='".$cret_info."' AND active=0 AND del_status=0 AND date>='".$date."'");
				return $sql_2_cre;
			}
		}
		
		function get_event_tagged_by_other2_user($id)
		{
			$sql_1_cre = "";
			
			$date = date("Y-m-d");
			$sql14 = mysql_query("select * from artist_project where id='".$id."' AND del_status=0 AND active=0");
			
			if(mysql_num_rows($sql14)>0)
			{
				$res14 = mysql_fetch_assoc($sql14);
				//while($res14 = mysql_fetch_assoc($sql14))
				//{
				$cret_info = 'artist_project|'.$res14['id'];
				
				$sql_1_cre = mysql_query("SELECT * FROM artist_event WHERE creators_info='".$cret_info."' AND active=0 AND del_status=0 AND date>='".$date."'");
				return $sql_1_cre;
			}
				
			/*	$sql_2_cre = mysql_query("SELECT * FROM community_event WHERE creators_info='".$cret_info."' AND active=0 AND del_status=0");
				if(mysql_num_rows($sql_2_cre)>0)
				{
					while($run_c = mysql_fetch_assoc($sql_2_cre))
					{
						$run_c['id'] = $run_c['id'].'~'.'com';
						//$ans_c_als[]
						$total[] = $run_c;
					}
				}
			//}
			
			return $total; */
		}
		
		function get_event_ctagged_by_other2_user($id)
		{
			$sql_2_cre = "";
			
			$date = date("Y-m-d");
			$sql14 = mysql_query("select * from artist_project where id='".$id."' AND del_status=0 AND active=0");
			
			if(mysql_num_rows($sql14)>0)
			{
				$res14 = mysql_fetch_assoc($sql14);
				//while($res14 = mysql_fetch_assoc($sql14))
				//{
				$cret_info = 'artist_project|'.$res14['id'];
				
				$sql_2_cre = mysql_query("SELECT * FROM community_event WHERE creators_info='".$cret_info."' AND active=0 AND del_status=0 AND date>='".$date."'");
				return $sql_2_cre;
			}
				
			/*	
				if(mysql_num_rows($sql_2_cre)>0)
				{
					while($run_c = mysql_fetch_assoc($sql_2_cre))
					{
						$run_c['id'] = $run_c['id'].'~'.'com';
						//$ans_c_als[]
						$total[] = $run_c;
					}
				}
			//}
			
			return $total; */
		}
		
		function check_facebook_connection($uid){
			$get_detail = mysql_query("select * from facebook_tokens where general_user_id='".$uid."' AND status=1");
			return($get_detail);
		}
		
		function select_twitter_accounts($user_id)
		{
			$res = "";
			$get_detail = mysql_query("select * from twitter_tokens where general_user_id='".$user_id."' AND status=1");
			if(mysql_num_rows($get_detail)>0){
				$res = mysql_fetch_assoc($get_detail);
			}
			return($res);
		}
		
		function get_message($id){
			$exp_id = explode("~",$id);
			$res_images = array(); $res = array(); $pass_array = array(); $songs_detail = array(); $video_detail = array(); $event_detail = array(); $project_detail = array();
			if($exp_id[1] == "reg_med"){
				$get_title = mysql_query("select * from general_media where id='".$exp_id[0]."'");
				if(mysql_num_rows($get_title)>0){
					$res[] = mysql_fetch_assoc($get_title);
					if($res[0]['media_type']==113){
						if(isset($res[0]['from_info']) && !empty($res[0]['from_info'])){
							$from_info = explode("|",$res[0]['from_info']);
							$get_link = mysql_query("select * from ".$from_info[0]." where id='".$from_info[1]."'");
							if(mysql_num_rows($get_link)>0){
								$res_images[] = mysql_fetch_assoc($get_link);
							}
						}
						else if(isset($res[0]['creator_info']) && !empty($res[0]['creator_info'])){
							$creator_info = explode("|",$res[0]['creator_info']);
							if($creator_info[0]=='general_artist'){ $sql = mysql_query("SELECT * FROM general_artist WHERE artist_id='".$creator_info[1]."'"); }
							if($creator_info[0]=='general_community'){ $sql = mysql_query("SELECT * FROM general_community WHERE community_id='".$creator_info[1]."'");}
							if($creator_info[0]=='artist_project' || $creator_info[0]=='community_project'){ $sql = mysql_query("SELECT * FROM $naam WHERE id='".$creator_info[1]."'"); }
							if(mysql_num_rows($sql)>0){
								$res_images[] = mysql_fetch_assoc($sql);
							}
						}
						else{
							$get_gen_detail = mysql_query("select * from general_user where general_user_id='".$res[0]['general_user_id']."'");
							if(mysql_num_rows($get_gen_detail)>0){
								$res_gen_detail = mysql_fetch_assoc($get_gen_detail);
								if(isset($res_gen_detail['artist_id']) && !empty($res_gen_detail['artist_id'])){
									$get_artist_detail = mysql_query("select * from general_artist where artist_id='".$res_gen_detail['artist_id']."'");
									if(mysql_num_rows($get_artist_detail)>0){
										$res_images[] = mysql_fetch_assoc($get_artist_detail);
									}
								}
								else if(isset($res_gen_detail['community_id']) && !empty($res_gen_detail['community_id'])){
									$get_community_detail = mysql_query("select * from general_community where community_id='".$res_gen_detail['community_id']."'");
									if(mysql_num_rows($get_community_detail)>0){
										$res_images[] = mysql_fetch_assoc($get_community_detail);
									}
								}
							}
						}
						$get_image = mysql_query("select * from media_images where media_id='".$res[0]['id']."'");
						if(mysql_num_rows($get_image)>0){
							$res_images[] = mysql_fetch_assoc($get_image);
						}
					}
					if($res[0]['media_type']==114){
					
						if(isset($res[0]['from_info']) && !empty($res[0]['from_info'])){
							$from_info = explode("|",$res[0]['from_info']);
							$get_link = mysql_query("select * from ".$from_info[0]." where id='".$from_info[1]."'");
							if(mysql_num_rows($get_link)>0){
								$songs_detail[] = mysql_fetch_assoc($get_link);
							}
						}
						else if(isset($res[0]['creator_info']) && !empty($res[0]['creator_info'])){
							$creator_info = explode("|",$res[0]['creator_info']);
							if($creator_info[0]=='general_artist'){ $sql = mysql_query("SELECT * FROM general_artist WHERE artist_id='".$creator_info[1]."'"); }
							if($creator_info[0]=='general_community'){ $sql = mysql_query("SELECT * FROM general_community WHERE community_id='".$creator_info[1]."'");}
							if($creator_info[0]=='artist_project' || $creator_info[0]=='community_project'){ $sql = mysql_query("SELECT * FROM $naam WHERE id='".$creator_info[1]."'"); }
							if(mysql_num_rows($sql)>0){
								$songs_detail[] = mysql_fetch_assoc($sql);
							}
						}
						else{
							$get_gen_detail = mysql_query("select * from general_user where general_user_id='".$res[0]['general_user_id']."'");
							if(mysql_num_rows($get_gen_detail)>0){
								$res_gen_detail = mysql_fetch_assoc($get_gen_detail);
								if(isset($res_gen_detail['artist_id']) && !empty($res_gen_detail['artist_id'])){
									$get_artist_detail = mysql_query("select * from general_artist where artist_id='".$res_gen_detail['artist_id']."'");
									if(mysql_num_rows($get_artist_detail)>0){
										$songs_detail[] = mysql_fetch_assoc($get_artist_detail);
									}
								}
								else if(isset($res_gen_detail['community_id']) && !empty($res_gen_detail['community_id'])){
									$get_community_detail = mysql_query("select * from general_community where community_id='".$res_gen_detail['community_id']."'");
									if(mysql_num_rows($get_community_detail)>0){
										$songs_detail[] = mysql_fetch_assoc($get_community_detail);
									}
								}
							}
						}
					
						$get_song = mysql_query("select * from media_songs where media_id='".$res[0]['id']."'");
						if(mysql_num_rows($get_song)>0){
							$songs_detail[] = mysql_fetch_assoc($get_song);
							if($songs_detail[1]['track']!="" && !empty($songs_detail[1]['track'])){
								//$songs_detail['song_detail_view'] = $songs_detail['track']." ";
							}
							if($res[0]['title']!="" && !empty($res[0]['title'])){
								$songs_detail[1]['song_detail_view'] = $songs_detail[1]['song_detail_view'].$res[0]['title'];
							}
							if($res[0]['creator']!="" && !empty($res[0]['creator'])){
								$songs_detail[1]['song_detail_view'] = $songs_detail[1]['song_detail_view']." by ".$res[0]['creator'];
							}
							if($res[0]['from']!="" && !empty($res[0]['from'])){
								$songs_detail[1]['song_detail_view'] = $songs_detail[1]['song_detail_view']." from ".$res[0]['from'];
							}
						}
					}
					if($res[0]['media_type']==115){
						if(isset($res[0]['from_info']) && !empty($res[0]['from_info'])){
							$from_info = explode("|",$res[0]['from_info']);
							$get_link = mysql_query("select * from ".$from_info[0]." where id='".$from_info[1]."'");
							if(mysql_num_rows($get_link)>0){
								$video_detail[] = mysql_fetch_assoc($get_link);
							}
						}
						else if(isset($res[0]['creator_info']) && !empty($res[0]['creator_info'])){
							$creator_info = explode("|",$res[0]['creator_info']);
							if($creator_info[0]=='general_artist'){ $sql = mysql_query("SELECT * FROM general_artist WHERE artist_id='".$creator_info[1]."'"); }
							if($creator_info[0]=='general_community'){ $sql = mysql_query("SELECT * FROM general_community WHERE community_id='".$creator_info[1]."'");}
							if($creator_info[0]=='artist_project' || $creator_info[0]=='community_project'){ $sql = mysql_query("SELECT * FROM $naam WHERE id='".$creator_info[1]."'"); }
							if(mysql_num_rows($sql)>0){
								$video_detail[] = mysql_fetch_assoc($sql);
							}
						}
						else{
							$get_gen_detail = mysql_query("select * from general_user where general_user_id='".$res[0]['general_user_id']."'");
							if(mysql_num_rows($get_gen_detail)>0){
								$res_gen_detail = mysql_fetch_assoc($get_gen_detail);
								if(isset($res_gen_detail['artist_id']) && !empty($res_gen_detail['artist_id'])){
									$get_artist_detail = mysql_query("select * from general_artist where artist_id='".$res_gen_detail['artist_id']."'");
									if(mysql_num_rows($get_artist_detail)>0){
										$video_detail[] = mysql_fetch_assoc($get_artist_detail);
									}
								}
								else if(isset($res_gen_detail['community_id']) && !empty($res_gen_detail['community_id'])){
									$get_community_detail = mysql_query("select * from general_community where community_id='".$res_gen_detail['community_id']."'");
									if(mysql_num_rows($get_community_detail)>0){
										$video_detail[] = mysql_fetch_assoc($get_community_detail);
									}
								}
							}
						}
						
						$get_video = mysql_query("select * from media_video where media_id='".$res[0]['id']."'");
						if(mysql_num_rows($get_video)>0){
							$video_detail[] = mysql_fetch_assoc($get_video);
						}
					}
				}
			}
			
			if($exp_id[1] == "community_event" || $exp_id[1] == "artist_event"){
			//echo "select * from ".$exp_id[1]." where id='".$exp_id[0]."'";
				$get_event_details = mysql_query("select * from ".$exp_id[1]." where id='".$exp_id[0]."'");
				if(mysql_num_rows($get_event_details)>0){
					$event_detail = mysql_fetch_assoc($get_event_details);
					$event_detail['whatis'] ="event";
				}
			}
			if($exp_id[1] == "community_project" || $exp_id[1] == "artist_project"){
				$get_project_details = mysql_query("select * from ".$exp_id[1]." where id='".$exp_id[0]."'");
				if(mysql_num_rows($get_project_details)>0){
					$project_detail = mysql_fetch_assoc($get_project_details);
					$project_detail['whatis'] ="project";
				}
			}
			$pass_array = array_merge($res_images,$res,$songs_detail,$video_detail,$event_detail,$project_detail);
			return($pass_array);
		}
		
		function get_user_link($id){
			$res_link ="";
			$get_general_det = mysql_query("select * from general_user where general_user_id='".$id."'");
			if(mysql_num_rows($get_general_det)>0){
				$res_general_det = mysql_fetch_assoc($get_general_det);
				if($res_general_det['artist_id']!=0 && !empty($res_general_det['artist_id'])){
					$get_link = mysql_query("select * from general_artist where artist_id='".$res_general_det['artist_id']."'");
					if(mysql_num_rows($get_link)>0){
						$res_link = mysql_fetch_assoc($get_link);
					}
				
				}else if($res_general_det['artist_id'] == 0 && $res_general_det['community_id']!=0 && !empty($res_general_det['community_id'])){
					$get_link = mysql_query("select * from general_community where community_id='".$res_general_det['community_id']."'");
					if(mysql_num_rows($get_link)>0){
						$res_link = mysql_fetch_assoc($get_link);
					}
				}
			}
			return($res_link);
		}
		
	}
?>