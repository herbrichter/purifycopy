<?php 
header('Content-Type: text/html; charset=utf-8'); 
mb_internal_encoding('utf-8');
include_once(dirname(__FILE__).'/../commons/db.php');

class Media
{

	function addGalleryImagesEntry($title,$files,$profile_id,$profile_type,$size)
	{
		$query="insert into general_".$profile_type."_gallery_list (gallery_title,created_date) values ('".$title."',CURDATE())";
		$rs=mysql_query($query) or die(mysql_error());
		$gallery_id=mysql_insert_id();
		if($gallery_id){
			$fileList = explode("|",$files);
			$newsize = explode("|",$size);
				for($i=0;$i<count($fileList);$i++){
				if($fileList[$i]!=""){
					$newsize_mb = $newsize[$i]/1048576;
					$newsize_mb1=substr($newsize_mb,0,7);
					$imgtostore = $this->tofilename($fileList[$i], '_', TRUE);
					//mysql_query( "SET NAMES utf8" );
					//mysql_query( "SET CHARACTER SET utf8" );
					//$string = iconv("UTF-8","UTF-8//IGNORE",$fileList[$i]);
									
	 			$query="insert into general_".$profile_type."_gallery (profile_id,gallery_id,image_name,created_date,size)
					values ('".$profile_id."','".$gallery_id."','".mysql_real_escape_string($imgtostore)."',CURDATE(),'".$newsize_mb1."')";
					$rs=mysql_query($query) or die(mysql_error());
				}
			}
		}
	}

	function addMediaAudiosEntry($title,$files,$profile_id,$profile_type,$size)
	{
		$fileList = explode("|",$files);
		$filesize = explode("|",$size);
		for($i=0;$i<count($fileList);$i++){
			if($fileList[$i]!=""){
				$new_file_size_mb = $filesize[$i]/1048576;
				$new_file_size_mb1=substr($new_file_size_mb,0,7);
				$audio_name = $this->tofilename($fileList[$i], '_', TRUE);
				$query="insert into general_".$profile_type."_audio (profile_id,audio_name,audio_file_name,created_date,size) values ('".$profile_id."','".$title."','".$audio_name."',CURDATE(),'".$new_file_size_mb1."')";
				$rs=mysql_query($query) or die(mysql_error());
			}
		}
	}

	function addMediaAudioLinkEntry($title,$link,$profile_id,$profile_type)
	{
		if($link!=""){
			$query="insert into general_".$profile_type."_audio (profile_id,audio_name,audio_link,created_date) values ('".$profile_id."','".$title."','".$link."',CURDATE())";
			$rs=mysql_query($query) or die(mysql_error());
		}
	}

	function addMediaVideoLinkEntry($title,$link,$profile_id,$profile_type)
	{
		if($link!=""){
			$query="insert into general_".$profile_type."_video (profile_id,video_name,video_link,created_date) values ('$profile_id','".$title."','".$link."',CURDATE())";
			$rs=mysql_query($query) or die(mysql_error());
		}
	}

	function getGalleryImagesByProfileId($profile_id,$profile_type)
	{
		$sql="select image_name from general_".$profile_type."_gallery where profile_id='".$profile_id."'";
		$rs=mysql_query($sql) or die(mysql_error());
		if($rs!="" && $rs!=Null){
			return mysql_fetch_assoc($rs);
		}else{
			$rs="";
			return ($rs);
		}
		
	}  

	function searchMediaByProfileId($profile_id,$profile_type){
		$mediaType = "";
		$resultMediaArray = Array();
		$sql="SELECT L.*,G.image_name,G.image_title FROM general_".$profile_type."_gallery as G, general_".$profile_type."_gallery_list as L 
			WHERE L.gallery_id=G.gallery_id 
			AND G.profile_id='".$profile_id."' GROUP BY L.gallery_id";
		$rs=mysql_query($sql) or die(mysql_error());
		if($rs!="" && $rs!=Null)
		{
			if(mysql_num_rows($rs)){
				$mediaType = "gallery";
				$resultArray = Array();
				$gCount = 0;
				while ($row = mysql_fetch_assoc($rs)) {
					$resultArray[$gCount][0] = $row;
					$imagesArr = Array();
					$sql1="SELECT G.* FROM general_".$profile_type."_gallery as G, general_".$profile_type."_gallery_list as L 
						WHERE L.gallery_id=G.gallery_id 
						AND G.profile_id='".$profile_id."' AND L.gallery_id='".$row['gallery_id']."'";
					$rs1=mysql_query($sql1) or die(mysql_error());
					if($rs1!="" && $rs1!=Null)
					{
						if(mysql_num_rows($rs1)){
							while ($row1 = mysql_fetch_assoc($rs1)) {						
								$imagesArr[] = $row1;						
							}
							$resultArray[$gCount][1] = $imagesArr;
							$gCount++;
						}
					}
				}

				$resultMediaArray[0][0] = $mediaType;
				$resultMediaArray[0][1] = $resultArray;
			}
		}else{
			$resultMediaArray[0][1] ="";
		}
		
		$sql="SELECT * FROM general_".$profile_type."_audio WHERE profile_id='".$profile_id."'";
		$rs=mysql_query($sql) or die(mysql_error());
		if($rs!="" && $rs!=Null)
		{
			if(mysql_num_rows($rs)){
				$mediaType = "audio";
				$resultArray = Array();
				while ($row = mysql_fetch_assoc($rs)) {
					$resultArray[] = $row;
				}			
				$resultMediaArray[1][0] = $mediaType;
				$resultMediaArray[1][1] = $resultArray;
			}
		}else{
			$resultMediaArray[1][1] ="";
		}
				
		$sql="SELECT * FROM general_".$profile_type."_video WHERE profile_id='".$profile_id."'";
		$rs=mysql_query($sql) or die(mysql_error());
		if($rs !="" && $rs !=Null)
		{
			if(mysql_num_rows($rs)){
				$mediaType = "video";
				$resultArray = Array();
				while ($row = mysql_fetch_assoc($rs)) {
					$resultArray[] = $row;
				}			
				$resultMediaArray[2][0] = $mediaType;
				$resultMediaArray[2][1] = $resultArray;
			}
		}else{
			$resultMediaArray[2][1] ="";
		}
				
		return $resultMediaArray;
	}
	
	function getMediaById($type,$media_id,$profile_id,$profile_type){
		switch($type){
			case "video":
			case "v":
						$sql="SELECT * FROM general_".$profile_type."_video WHERE video_id='".$media_id."'";
						$rs=mysql_query($sql) or die(mysql_error());
						if($rs!="" && $rs!=Null)
						{
							if(mysql_num_rows($rs)){
								$resultArray = Array();
								while ($row = mysql_fetch_assoc($rs)) {
									$resultArray[] = $row;
								}			
								return $resultArray;
							}
						}else{
							$resultArray = Array();
							return $resultArray;
						}
					break;
			case "audio":
			case "a":
						$sql="SELECT * FROM general_".$profile_type."_audio WHERE audio_id='".$media_id."'";
						$rs=mysql_query($sql) or die(mysql_error());
						if($rs!="" && $rs!=Null)
						{
							if(mysql_num_rows($rs)){
								$resultArray = Array();
								while ($row = mysql_fetch_assoc($rs)) {
									if($row['gallery_relation']>0){
										$sql1 = "SELECT *, 'gallery' as image_type FROM general_".$profile_type."_gallery WHERE gallery_id='".$row['gallery_relation']."'";
										$rs1=mysql_query($sql1) or die(mysql_error());
										if($rs1!="" && $rs1!=Null)
										{
											if(mysql_num_rows($rs1)){
												$galleryArray = Array();
												while ($row1 = mysql_fetch_assoc($rs1)) {
													$galleryArray[] = $row1;
												}
											}
										}else{
											$galleryArray = Array();
										}
									}else{
										$sql1 = "SELECT '1' as img_id, image_name, name as image_title, 'profile_pic' as image_type FROM general_".$profile_type." WHERE artist_id='".$profile_id."'";
										$rs1=mysql_query($sql1) or die(mysql_error());
										if($rs1!="" && $rs1!=Null)
										{
											if(mysql_num_rows($rs1)){
												$galleryArray = Array();
												while ($row1 = mysql_fetch_assoc($rs1)) {
													$galleryArray[] = $row1;
												}
											}
										}else{
											$galleryArray = Array();
										}
									}
									$row['gallery_files'] = $galleryArray;
									$resultArray[] = $row;
								}			
								return $resultArray;
							}
						}else{
							$resultArray = Array();
							return $resultArray;
						}
					break;
		}
		
	}

	function getUserBucket($user_id){
		$sql="select bucket_name from general_user where general_user_id='".$user_id."'";
		$rs=mysql_query($sql) or die(mysql_error());
		if($rs!="" && $rs!=Null){
			$row = mysql_fetch_assoc($rs);
			return $row['bucket_name'];
		}
	}

	function getMediaListByArtistId($type,$artist_id,$profile_type){
		switch($type){
			case "audio":
			case "a":
						$sql="SELECT * FROM general_".$profile_type."_audio WHERE profile_id='".$artist_id."'";
						$rs=mysql_query($sql) or die(mysql_error());
						if($rs!="" && $rs!=Null){
							if(mysql_num_rows($rs)){
								$resultArray = Array();
								while ($row = mysql_fetch_assoc($rs)) {
									if($row['gallery_relation']>0){
										$sql1 = "SELECT *, 'gallery' as image_type FROM general_".$profile_type."_gallery WHERE gallery_id='".$row['gallery_relation']."'";
										$rs1=mysql_query($sql1) or die(mysql_error());
										if($rs1!="" && $rs1!=Null)
										{
											if(mysql_num_rows($rs1)){
												$galleryArray = Array();
												while ($row1 = mysql_fetch_assoc($rs1)) {
													$galleryArray[] = $row1;
												}
											}
										}else{
											$galleryArray = Array();
										}
									}else{
										$sql1 = "SELECT '1' as img_id, image_name, name as image_title, 'profile_pic' as image_type FROM general_".$profile_type." ";
										$rs1=mysql_query($sql1) or die(mysql_error());
										if($rs1!="" && $rs1!=Null)
										{
											if(mysql_num_rows($rs1)){
												$galleryArray = Array();
												while ($row1 = mysql_fetch_assoc($rs1)) {
													$galleryArray[] = $row1;
												}
											}
										}else{
											$galleryArray = Array();
										}
									}
									$row['gallery_files'] = $galleryArray;
									$resultArray[] = $row;
								}			
								return $resultArray;
							}
						}
				break;
			case "video":
			case "v":
						$sql="SELECT * FROM general_".$profile_type."_video WHERE profile_id='".$artist_id."'";
						$rs=mysql_query($sql) or die(mysql_error());
						if($rs!="" && $rs!=Null)
						{
							if(mysql_num_rows($rs)){
								$resultArray = Array();
								while ($row = mysql_fetch_assoc($rs)) {
									$resultArray[] = $row;
								}			
								return $resultArray;
							}
						}else{
							$resultArray = Array();
							return $resultArray;
						}
				break;
		}
	}

	function checkUserLogin($uname,$upass){ //Check for general user & common user login
		$sql="SELECT * FROM general_user WHERE email='".$uname."' AND pass='".$upass."'";
		$rs=mysql_query($sql) or die(mysql_error());
		$data = mysql_fetch_assoc($rs);
		if($rs!="" && $rs!=Null)
		{
			if(mysql_num_rows($rs))
			return $data;
			else
				return FALSE;
		}
		else
			return FALSE;
	}




function tofilename($title, $separator = '-', $ascii_only = FALSE)
{
	if ($ascii_only === TRUE)
	{
		// Transliterate non-ASCII characters
		//$title = UTF8::transliterate_to_ascii($title);

		// Remove all characters that are not the separator, a-z, 0-9, or whitespace
		$title = preg_replace('![^.a-zA-Z0-9\s_-]+!', '', $title);
		//$title = preg_replace('#(?<!\\\\)(\\$|\\\\)#', '', $title);
		//echo $title;
		//die;
	}
	else
	{
		// Remove all characters that are not the separator, letters, numbers, or whitespace
		$title = preg_replace('![^.'.preg_quote($separator).'\pL\pN\s]+!u', '', $title);
	}

	// Replace all separator characters and whitespace by a single separator
	//$title = preg_replace('![.'.preg_quote($separator).'\s_-]+!u', $separator, $title);

	// Trim separators from the beginning and end
	return trim($title, $separator);
}
}
?>