<?php

class EmailSubscription
{
	function get_general_info($email)
	{
		$sql = mysql_query("select * from general_user where email='".$email."'");
		$res = mysql_fetch_assoc($sql);
		return($res);
	}
	
	function get_all_org_sub($email)
	{
		$row = $this->get_general_info($email);
		$get_data = mysql_query("select * from email_subscription where email = '".$email."' AND delete_status=1");
		return($get_data);
	}
	
	function get_subscriber_info_org($ids,$types)
	{
		if($types=='artist')
		{
			$sql_que = mysql_query("SELECT * FROM general_artist WHERE artist_id='".$ids."' AND status=0 AND active=0 AND del_status=0");
		}
		elseif($types=='community')
		{
			$sql_que = mysql_query("SELECT * FROM general_community WHERE community_id='".$ids."' AND status=0 AND active=0 AND del_status=0");
		}
		elseif($types=='community_project')
		{
			$sql_que = mysql_query("SELECT * FROM community_project WHERE id='".$ids."' AND active=0 AND del_status=0");
		}
		elseif($types=='artist_project')
		{
			$sql_que = mysql_query("SELECT * FROM artist_project WHERE id='".$ids."' AND active=0 AND del_status=0");
		}
		elseif($types=='artist_event')
		{
			$sql_que = mysql_query("SELECT * FROM artist_event WHERE id='".$ids."' AND active=0 AND del_status=0");
		}
		elseif($types=='community_event')
		{
			$sql_que = mysql_query("SELECT * FROM community_event WHERE id='".$ids."' AND active=0 AND del_status=0");
		}
		
		if(isset($sql_que))
		{
			if(mysql_num_rows($sql_que)>0)
			{
				$ans_que = mysql_fetch_assoc($sql_que);
				return $ans_que;
			}
		}
	}
	
	function artist_communitySubscribedTo($type,$rel_type_id)
	{
		$sql_sub_to_user = "SELECT * FROM `general_user` WHERE ".$type."_id = $rel_type_id";
		$run_sub_to_user = mysql_query($sql_sub_to_user);
		$ans_sub_to_user = array();
		if($run_sub_to_user!="" && $run_sub_to_user!=NULL)
		{
			$ans_sub_to_user = mysql_fetch_assoc($run_sub_to_user);
		}
		return ($ans_sub_to_user);
	}
	
	function get_artist_user($id)
	{
		//$res=$this->get_user_info($id);
		$sql5=mysql_query("select MIN(general_artist_profiles_id) from general_artist_profiles where artist_id='".$id."' AND subtype_id!=0");
		$res5=mysql_fetch_assoc($sql5);
		
		$sql6=mysql_query("select * from general_artist_profiles where general_artist_profiles_id='".$res5['MIN(general_artist_profiles_id)']."'");
		$res6=mysql_fetch_assoc($sql6);
		return($res6);
	}
	
	function get_aproject_user($id)
	{
		//$res=$this->get_user_info($id);
		$sql5=mysql_query("select MIN(id) from artist_project_profiles where artist_project_id='".$id."' AND subtype_id!=0");
		$res5=mysql_fetch_assoc($sql5);
		
		$sql6=mysql_query("select * from artist_project_profiles where id='".$res5['MIN(id)']."'");
		$res6=mysql_fetch_assoc($sql6);
		return($res6);
	}
	
	function get_community_user($id)
	{
		//$res=$this->get_user_info($id);
		$sql5=mysql_query("select MIN(general_community_profiles_id) from general_community_profiles where community_id='".$id."' AND subtype_id!=0");
		$res5=mysql_fetch_assoc($sql5);
		
		$sql6=mysql_query("select * from general_community_profiles where general_community_profiles_id='".$res5['MIN(general_community_profiles_id)']."'");
		$res6=mysql_fetch_assoc($sql6);
		return($res6);
	}
	
	function get_aevent_user($id)
	{
		//$res=$this->get_user_info($id);
		$sql5=mysql_query("select MIN(id) from artist_event_profiles where artist_event_id='".$id."' AND subtype_id!=0");
		$res5=mysql_fetch_assoc($sql5);
		
		$sql6=mysql_query("select * from artist_event_profiles where id='".$res5['MIN(id)']."'");
		$res6=mysql_fetch_assoc($sql6);
		return($res6);
	}
	
	function get_cproject_user($id)
	{
		//$res=$this->get_user_info($id);
		$sql5=mysql_query("select MIN(id) from community_project_profiles where community_project_id='".$id."' AND subtype_id!=0");
		$res5=mysql_fetch_assoc($sql5);
		
		$sql6=mysql_query("select * from community_project_profiles where id='".$res5['MIN(id)']."'");
		$res6=mysql_fetch_assoc($sql6);
		return($res6);
	}
	
	function get_cevent_user($id)
	{
		//$res=$this->get_user_info($id);
		$sql5=mysql_query("select MIN(id) from community_event_profiles where community_event_id='".$id."' AND subtype_id!=0");
		$res5=mysql_fetch_assoc($sql5);
		
		$sql6=mysql_query("select * from community_event_profiles where id='".$res5['MIN(id)']."'");
		$res6=mysql_fetch_assoc($sql6);
		return($res6);
	}
	
	function get_artist_user_subtype($id)
	{
		$res1=$this->get_artist_user($id);
		$sql8=mysql_query("select * from subtype where subtype_id='".$res1['subtype_id']."'");
		$res8=mysql_fetch_assoc($sql8);
		return($res8);
	}
	
	function get_community_user_subtype($id)
	{
		$res1=$this->get_community_user($id);
		$sql8=mysql_query("select * from subtype where subtype_id='".$res1['subtype_id']."'");
		$res8=mysql_fetch_assoc($sql8);
		return($res8);
	}
	
	function get_artistp_user_subtype($id)
	{
		$res1=$this->get_aproject_user($id);
		$sql8=mysql_query("select * from subtype where subtype_id='".$res1['subtype_id']."'");
		$res8=mysql_fetch_assoc($sql8);
		return($res8);
	}
	
	function get_artiste_user_subtype($id)
	{
		$res1=$this->get_aevent_user($id);
		$sql8=mysql_query("select * from subtype where subtype_id='".$res1['subtype_id']."'");
		$res8=mysql_fetch_assoc($sql8);
		return($res8);
	}
	
	function get_communityp_user_subtype($id)
	{
		$res1=$this->get_cproject_user($id);
		$sql8=mysql_query("select * from subtype where subtype_id='".$res1['subtype_id']."'");
		$res8=mysql_fetch_assoc($sql8);
		return($res8);
	}
	
	function get_communitye_user_subtype($id)
	{
		$res1=$this->get_cevent_user($id);
		$sql8=mysql_query("select * from subtype where subtype_id='".$res1['subtype_id']."'");
		$res8=mysql_fetch_assoc($sql8);
		return($res8);
	}
}

?>