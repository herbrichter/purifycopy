<?php
	include_once('commons/db.php');
	$root = realpath($_SERVER["DOCUMENT_ROOT"]);
	include_once($root.'/classes/user_keys.php');
	if (!class_exists('S3')) require_once ($root.'/S3.php');
	
	//$user_keys_data = new user_keys();
	class SuggestSearch
	{
		function count_suggest($q)
		{
			$sql_art = "SELECT count(*) FROM general_artist WHERE name LIKE '%$q%' AND status=0 AND active=0 AND del_status=0 AND image_name!='' AND profile_url!=''";
			$sql_comm = "SELECT count(*) FROM general_community WHERE name LIKE '%$q%' AND status=0 AND active=0 AND del_status=0 AND image_name!='' AND profile_url!=''";
			$sql_arte = "SELECT count(*) FROM artist_event WHERE title LIKE '%$q%' AND del_status=0 AND active=0 AND image_name!='' AND profile_url!=''";
			$sql_artp = "SELECT count(*) FROM artist_project WHERE title LIKE '%$q%' AND del_status=0 AND active=0 AND image_name!='' AND profile_url!=''";
			$sql_come = "SELECT count(*) FROM community_event WHERE title LIKE '%$q%' AND del_status=0 AND active=0 AND image_name!='' AND profile_url!=''";
			$sql_comp = "SELECT count(*) FROM community_project WHERE title LIKE '%$q%' AND del_status=0 AND active=0 AND image_name!='' AND profile_url!=''";
			$sql_med = "SELECT count(*) FROM general_media WHERE title LIKE '$q%' AND delete_status=0 AND media_status=0";
			$sql_type = "SELECT count(*) FROM `type` WHERE `name` LIKE '$q%' AND `delete`='0'";
			$sql_sutype = "SELECT count(*) FROM `subtype` WHERE `name` LIKE '$q%' AND `delete`='0'";
			$sql_metype = "SELECT count(*) FROM `meta_type` WHERE `name` LIKE '$q%' AND `delete`='0'";
			
			$sql_reg_video_art = "SELECT * FROM general_artist_video WHERE video_name LIKE '%$q%'";
			$sql_reg_audio_art = "SELECT * FROM general_artist_audio WHERE audio_name LIKE '%$q%'";
			$sql_reg_gallery_art = "SELECT * FROM general_artist_gallery_list WHERE gallery_title LIKE '%$q%'";

			$sql_reg_video_com = "SELECT * FROM general_community_video WHERE video_name LIKE '%$q%'";
			$sql_reg_audio_com = "SELECT * FROM general_community_audio WHERE audio_name LIKE '%$q%'";
			$sql_reg_gallery_com = "SELECT * FROM general_community_gallery_list WHERE gallery_title LIKE '%$q%'";
			
			$run_art = mysql_query($sql_art); 
			$run_comm = mysql_query($sql_comm);
			$run_arte = mysql_query($sql_arte);
			$run_artp = mysql_query($sql_artp);
			$run_come = mysql_query($sql_come);
            $run_comp = mysql_query($sql_comp);
			$run_med = mysql_query($sql_med);
			$run_type = mysql_query($sql_type);
			$run_sutype = mysql_query($sql_sutype);
			$run_metype = mysql_query($sql_metype);
			
			$run_reg_video_art = mysql_query($sql_reg_video_art);
			$run_reg_audio_art = mysql_query($sql_reg_audio_art);
			$run_reg_gallery_art = mysql_query($sql_reg_gallery_art);
			$run_reg_video_com = mysql_query($sql_reg_video_com);
			$run_reg_audio_com = mysql_query($sql_reg_audio_com);
			$run_reg_gallery_com = mysql_query($sql_reg_gallery_com);
			
			$ans_art = array();
            $ans_comm = array();
            $ans_arte = array();
            $ans_artp = array();
            $ans_come = array();
            $ans_comp = array();
			$ans_med = array();
			$ans_type = array();
			$ans_sutype = array();
			$ans_metype = array();
			
			$ans_reg_video_art = array();
			$ans_reg_audio_art = array();
			$ans_reg_gallery_art = array();
			$ans_reg_video_com = array();
			$ans_reg_audio_com = array();
			$ans_reg_gallery_com = array();
			
			if(mysql_num_rows($run_art)>0)
			{
				while($row_art = mysql_fetch_assoc($run_art))
				{
					$ans_art[] = $row_art;
				}
			}
			
			if(mysql_num_rows($run_comm)>0)
			{
				while($row_comm = mysql_fetch_assoc($run_comm))
				{
					$ans_comm[] = $row_comm;
				}
			}
			
			if(mysql_num_rows($run_arte)>0)
			{
				while($row_arte = mysql_fetch_assoc($run_arte))
				{
					$ans_arte[] = $row_arte;
				}
			}
			
			if(mysql_num_rows($run_artp)>0)
			{
				while($row_artp = mysql_fetch_assoc($run_artp))
				{
					$ans_artp[] = $row_artp;
				}
			}
			
			if(mysql_num_rows($run_come)>0)
			{
				while($row_come = mysql_fetch_assoc($run_come))
				{
					$ans_come[] = $row_come;
				}
			}
			
			if(mysql_num_rows($run_comp)>0)
			{
				while($row_comp = mysql_fetch_assoc($run_comp))
				{
					$ans_comp[] = $row_comp;
				}
			}
			
			if(mysql_num_rows($run_med)>0)
			{
				while($row_med = mysql_fetch_assoc($run_med))
				{
					$ans_med[] = $row_med;
				}
			}
			
			if(mysql_num_rows($run_type)>0)
			{
				while($row_type = mysql_fetch_assoc($run_type))
				{
					//$pre_chk = $this->type_chk_sugg($row_type['type_id']);
					//if($pre_chk=='1')
					//{
						//$row_type["table_name"] = 'type';
						$ans_type[] = $row_type;
					//}
				}
			}
			
			if(mysql_num_rows($run_sutype)>0)
			{
				while($row_type = mysql_fetch_assoc($run_sutype))
				{
					//$pre_suchk = "";
					//$pre_suchk = $this->subtype_chk_sugg($row_type['subtype_id']);
					//if($pre_suchk=='1')
					//{
						//$row_type["table_name"] = 'subtype';
						$ans_sutype[] = $row_type;
					//}
				}
			}
			
			if(mysql_num_rows($run_metype)>0)
			{
				while($row_type = mysql_fetch_assoc($run_metype))
				{
					//$pre_mechk = "";
					//$pre_mechk = $this->metatype_chk_sugg($row_type['meta_id']);
					//if($pre_mechk=='1')
					//{
						//$row_type["table_name"] = 'meta_type';
						$ans_metype[] = $row_type;
					//}
				}
			}
			
			$main_count_match = array_merge($ans_art,$ans_comm,$ans_arte,$ans_artp,$ans_come,$ans_comp,$ans_med,$ans_reg_video_art,$ans_reg_audio_art,$ans_reg_gallery_art,$ans_reg_video_com,$ans_reg_audio_com,$ans_reg_gallery_com,$ans_metype,$ans_sutype,$ans_type);
			return $main_count_match;
		}
		
		function suggest_match($q)
		{
			//For all results add a % sign in starting of q also
			$sql_art = "SELECT * FROM general_artist WHERE name LIKE '%$q%' AND status=0 AND active=0 AND del_status=0 AND image_name!='' AND profile_url!=''";
			$sql_comm = "SELECT * FROM general_community WHERE name LIKE '%$q%' AND status=0 AND active=0 AND del_status=0 AND image_name!='' AND profile_url!=''";
			$sql_arte = "SELECT * FROM artist_event WHERE title LIKE '%$q%' AND del_status=0 AND active=0 AND image_name!='' AND profile_url!=''";
			$sql_artp = "SELECT * FROM artist_project WHERE title LIKE '%$q%' AND del_status=0 AND active=0 AND image_name!='' AND profile_url!=''";
			$sql_come = "SELECT * FROM community_event WHERE title LIKE '%$q%' AND del_status=0 AND active=0 AND image_name!='' AND profile_url!=''";
			$sql_comp = "SELECT * FROM community_project WHERE title LIKE '%$q%' AND del_status=0 AND active=0 AND image_name!='' AND profile_url!=''";
			$sql_med = "SELECT * FROM general_media WHERE title LIKE '$q%' AND delete_status=0 AND media_status=0";
			$sql_type = "SELECT * FROM `type` WHERE `name` LIKE '$q%' AND `delete`='0'";
			$sql_sutype = "SELECT * FROM `subtype` WHERE `name` LIKE '$q%' AND `delete`='0'";
			$sql_metype = "SELECT * FROM `meta_type` WHERE `name` LIKE '$q%' AND `delete`='0'";
			
			$sql_reg_video_art = "SELECT * FROM general_artist_video WHERE video_name LIKE '%$q%'";
			$sql_reg_audio_art = "SELECT * FROM general_artist_audio WHERE audio_name LIKE '%$q%'";
			$sql_reg_gallery_art = "SELECT * FROM general_artist_gallery_list WHERE gallery_title LIKE '%$q%'";
					
			$sql_reg_video_com = "SELECT * FROM general_community_video WHERE video_name LIKE '%$q%'";
			$sql_reg_audio_com = "SELECT * FROM general_community_audio WHERE audio_name LIKE '%$q%'";
			$sql_reg_gallery_com = "SELECT * FROM general_community_gallery_list WHERE gallery_title LIKE '%$q%'";
			
			$run_art = mysql_query($sql_art); 
			$run_comm = mysql_query($sql_comm);
			$run_arte = mysql_query($sql_arte);
			$run_artp = mysql_query($sql_artp);
			$run_come = mysql_query($sql_come);
            $run_comp = mysql_query($sql_comp);
			$run_med = mysql_query($sql_med);
			$run_type = mysql_query($sql_type);
			$run_sutype = mysql_query($sql_sutype);
			$run_metype = mysql_query($sql_metype);
			
			$run_reg_video_art = mysql_query($sql_reg_video_art);
			$run_reg_audio_art = mysql_query($sql_reg_audio_art);
			$run_reg_gallery_art = mysql_query($sql_reg_gallery_art);
			$run_reg_video_com = mysql_query($sql_reg_video_com);
			$run_reg_audio_com = mysql_query($sql_reg_audio_com);
			$run_reg_gallery_com = mysql_query($sql_reg_gallery_com);
			
			$ans_art = array();
            $ans_comm = array();
            $ans_arte = array();
            $ans_artp = array();
            $ans_come = array();
            $ans_comp = array();
			$ans_med = array();
			$ans_type = array();
			$ans_sutype = array();
			$ans_metype = array();
			
			$ans_reg_video_art = array();
			$ans_reg_audio_art = array();
			$ans_reg_gallery_art = array();
			$ans_reg_video_com = array();
			$ans_reg_audio_com = array();
			$ans_reg_gallery_com = array();
			
			// $ans_art['table_name'] = "general_artist";
			// $ans_comm['table_name'] = "general_community";
			// $ans_arte['table_name'] = "artist_event";
			// $ans_artp['table_name'] = "artist_project";
			// $ans_artp['table_name'] = "artist_project";
			// $ans_come['table_name'] = "community_event";
			// $ans_comp['table_name'] = "community_project";
			
			if(mysql_num_rows($run_art)>0)
			{
				while($row_art = mysql_fetch_assoc($run_art))
				{
					$row_art["table_name"] = 'general_artist';
					$ans_art[] = $row_art;
					
				}
				
				//$ans_art[] = $row_art["table_name"];
			}
			
			if(mysql_num_rows($run_comm)>0)
			{
				while($row_comm = mysql_fetch_assoc($run_comm))
				{
					$row_comm["table_name"] = 'general_community';
					$ans_comm[] = $row_comm;
				}
			}
			
			if(mysql_num_rows($run_arte)>0)
			{
				while($row_arte = mysql_fetch_assoc($run_arte))
				{
					$row_arte["table_name"] = 'artist_event';
					$ans_arte[] = $row_arte;
				}
			}
			
			if(mysql_num_rows($run_artp)>0)
			{
				while($row_artp = mysql_fetch_assoc($run_artp))
				{
					$row_artp["table_name"] = 'artist_project';
					$ans_artp[] = $row_artp;
				}
			}
			
			if(mysql_num_rows($run_come)>0)
			{
				while($row_come = mysql_fetch_assoc($run_come))
				{
					$row_come["table_name"] = 'community_event';
					$ans_come[] = $row_come;
				}
			}
			
			if(mysql_num_rows($run_comp)>0)
			{
				while($row_comp = mysql_fetch_assoc($run_comp))
				{
					$row_comp["table_name"] = 'community_project';
					$ans_comp[] = $row_comp;
				}
			}
			
			if(mysql_num_rows($run_med)>0)
			{
				while($row_med = mysql_fetch_assoc($run_med))
				{
					$row_med["table_name"] = 'general_media';
					$ans_med[] = $row_med;
				}
			}
			
			if(mysql_num_rows($run_type)>0)
			{
				$chk_dbs_type = array();
				while($row_type = mysql_fetch_assoc($run_type))
				{
					$pre_chk = "";
					$pre_chk = $this->type_chk_sugg($row_type['type_id']);
					$parent_name = $this->get_profile_name($row_type['profile_id']);
					if($pre_chk=='1')
					{
						//if(in_array($row_type['name'],$chk_dbs_type))
						//{}
						//else
						//{
							$chk_dbs_type[] = $row_type['name'];
							$row_type["table_name"] = 'type';
							$row_type["parent_name"] = $parent_name['name'];
							$ans_type[] = $row_type;
						//}
					}
				}
			}
			
			if(mysql_num_rows($run_sutype)>0)
			{
				$chk_dbs_sutype = array();
				while($row_type = mysql_fetch_assoc($run_sutype))
				{
					$pre_suchk = "";
					$pre_suchk = $this->subtype_chk_sugg($row_type['subtype_id']);
					$parent_name = $this->get_type_name($row_type['type_id']);
					$parent_name_1 = $this->get_profile_name($parent_name['profile_id']);
					if($pre_suchk=='1')
					{
						//if(in_array($row_type['name'],$chk_dbs_sutype))
						//{}
						//else
						//{
							$chk_dbs_sutype[] = $row_type['name'];
							$row_type["table_name"] = 'subtype';
							$row_type["parent_name"] = $parent_name['name'];
							$row_type["parent_name_1"] = $parent_name_1['name'];
							$ans_sutype[] = $row_type;
						//}
					}
				}
			}
			
			if(mysql_num_rows($run_metype)>0)
			{
				$chk_dbs_metype = array();
				while($row_type = mysql_fetch_assoc($run_metype))
				{
					$pre_mechk = "";
					$pre_mechk = $this->metatype_chk_sugg($row_type['meta_id']);
					$parent_name = $this->get_subtype_name($row_type['subtype_id']);
					$parent_name_1 = $this->get_type_name($parent_name['type_id']);
					$parent_name_2 = $this->get_profile_name($parent_name_1['profile_id']);
					if($pre_mechk=='1')
					{
						//if(in_array($row_type['name'],$chk_dbs_metype))
						//{}
						//else
						//{
							$chk_dbs_metype[] = $row_type['name'];
							$row_type["table_name"] = 'meta_type';
							$row_type["parent_name"] = $parent_name['name'];
							$row_type["parent_name_1"] = $parent_name_1['name'];
							$row_type["parent_name_2"] = $parent_name_2['name'];
							$ans_metype[] = $row_type;
						//}
					}
				}
			}
			//var_dump($ans_metype);
			/* if(mysql_num_rows($run_reg_video_art)>0)
			{
				while($v_arow_med = mysql_fetch_assoc($run_reg_video_art))
				{
					$v_arow_med["table_name"] = 'general_artist_video';
					$ans_reg_video_art[] = $v_arow_med;
				}
			}
			
			if(mysql_num_rows($run_reg_audio_art)>0)
			{
				while($a_arow_med = mysql_fetch_assoc($run_reg_audio_art))
				{
					$a_arow_med["table_name"] = 'general_artist_audio';
					$ans_reg_audio_art[] = $a_arow_med;
				}
			}
			
			if(mysql_num_rows($run_reg_gallery_art)>0)
			{
				while($g_arow_med = mysql_fetch_assoc($run_reg_gallery_art))
				{
					$g_arow_med["table_name"] = 'general_artist_gallery_list';
					$ans_reg_gallery_art[] = $g_arow_med;
				}
			}
			
			if(mysql_num_rows($run_reg_video_com)>0)
			{
				while($v_crow_med = mysql_fetch_assoc($run_reg_video_com))
				{
					$v_crow_med["table_name"] = 'general_community_video';
					$ans_reg_video_com[] = $v_crow_med;
				}
			}
			
			if(mysql_num_rows($run_reg_audio_com)>0)
			{
				while($a_crow_med = mysql_fetch_assoc($run_reg_audio_com))
				{
					$a_crow_med["table_name"] = 'general_community_audio';
					$ans_reg_audio_com[] = $a_crow_med;
				}
			}
			
			if(mysql_num_rows($run_reg_gallery_com)>0)
			{
				while($g_crow_med = mysql_fetch_assoc($run_reg_gallery_com))
				{
					$g_crow_med["table_name"] = 'general_community_gallery_list';
					$ans_reg_gallery_com[] = $g_crow_med;
				}
			} */
			
			$main_match = array_merge($ans_art,$ans_comm,$ans_arte,$ans_artp,$ans_come,$ans_comp,$ans_med,$ans_reg_video_art,$ans_reg_audio_art,$ans_reg_gallery_art,$ans_reg_video_com,$ans_reg_audio_com,$ans_reg_gallery_com,$ans_type,$ans_sutype,$ans_metype);
			//var_dump($ans_art);
			//var_dump($main_match);
			return $main_match;
			
		}
		
		function get_table_info($table_data,$table_id)
		{
			if($table_data=='general_artist')
			{
				$sql_get_tables = mysql_query("SELECT * FROM $table_data WHERE artist_id='".$table_id."' AND active=0 AND status=0");
			}
			elseif($table_data=='general_community')
			{
				$sql_get_tables = mysql_query("SELECT * FROM $table_data WHERE community_id='".$table_id."' AND active=0 AND status=0");
			}
			elseif($table_data=='community_event' || $table_data=='artist_event' || $table_data=='artist_project' || $table_data=='community_project')
			{
				$sql_get_tables = mysql_query("SELECT * FROM $table_data WHERE id='".$table_id."' AND del_status=0 AND active=0");
			}
			$sql_get_tables = mysql_fetch_assoc($sql_get_tables);
			return ($sql_get_tables);
		}
		
		function searching($profile_type_search,$page_type_search,$sub_type_search,$meta_type_search,$country_search,$state_search,$city_search,$match_string,$match_table,$match_id,$alpha,$tab_name)
		{
			////////////////////////////////////Only Profile Search////////////////////////////////////
			/* echo $profile_type_search;
			echo $page_type_search;
			echo $sub_type_search;
			echo $meta_type_search;
			echo $country_search;
			echo $state_search;
			echo $city_search;
			echo $match_string;
			echo $match_table;
			echo $match_id;
			echo $alpha; */
			if($profile_type_search!="select" && $page_type_search==0 && $sub_type_search==0 && $meta_type_search==0 && $country_search==0 && $state_search==0 && $city_search=="" && $match_string=="Search" && $match_table=="" && $match_id=="" && $alpha=="")
			{
				// echo "f9";
				 // $re = "f9";
				 // return $re;
				if($profile_type_search=='artist' || $profile_type_search=='community')
				{	
					$sql_profile = "SELECT * FROM general_$profile_type_search WHERE status=0 AND active=0 AND image_name!='' AND profile_url!=''";
					$run_profile = mysql_query($sql_profile);
					$profile = array();
					if(mysql_num_rows($run_profile)>0)
					{
						while($row_profile = mysql_fetch_assoc($run_profile))
						{
							$profile[] = $row_profile;
						}
					}
					return $profile;
				}
				elseif($profile_type_search=='event')
				{
					$profile_type_search_art = 'artist_event';
					$profile_type_search_com = 'community_event';
					$sql_profile_art = "SELECT * FROM $profile_type_search_art WHERE del_status=0 AND active=0 AND image_name!='' AND profile_url!=''";
					$sql_profile_com = "SELECT * FROM $profile_type_search_com WHERE del_status=0 AND active=0 AND image_name!='' AND profile_url!=''";
					$run_profile_art = mysql_query($sql_profile_art);
					$run_profile_com = mysql_query($sql_profile_com);
					$ans_profile_art = array();
					$ans_profile_com = array();
					if(mysql_num_rows($run_profile_art)>0)
					{
						while($row_art = mysql_fetch_assoc($run_profile_art))
						{
							$row_art['table_name'] = 'artist_event';
							$ans_profile_art[] = $row_art;
						}
					}
					
					if(mysql_num_rows($run_profile_com)>0)
					{
						while($row_com = mysql_fetch_assoc($run_profile_com))
						{
							$row_com['table_name'] = 'community_event';
							$ans_profile_com[] = $row_com;
						}
					}
					
					$main_ans = array_merge($ans_profile_art,$ans_profile_com);
					return $main_ans;
				}
				elseif($profile_type_search=='project')
				{
					$profile_type_search_art = 'artist_project';
					$profile_type_search_com = 'community_project';
					$sql_profile_art = "SELECT * FROM $profile_type_search_art WHERE del_status=0 AND active=0 AND image_name!='' AND profile_url!=''";
					$sql_profile_com = "SELECT * FROM $profile_type_search_com WHERE del_status=0 AND active=0 AND image_name!='' AND profile_url!=''";
					$run_profile_art = mysql_query($sql_profile_art);
					$run_profile_com = mysql_query($sql_profile_com);
					$ans_profile_art = array();
					$ans_profile_com = array();
					if(mysql_num_rows($run_profile_art)>0)
					{
						while($row_art = mysql_fetch_assoc($run_profile_art))
						{
							$row_art['table_name'] = 'artist_project';
							$ans_profile_art[] = $row_art;
						}
					}
					
					if(mysql_num_rows($run_profile_com)>0)
					{
						while($row_com = mysql_fetch_assoc($run_profile_com))
						{
							$row_com['table_name'] = 'community_project';
							$ans_profile_com[] = $row_com;
						}
					}
					
					$main_ans = array_merge($ans_profile_art,$ans_profile_com);
					return $main_ans;
				}
				elseif($profile_type_search=='media')
				{
					$ans_reg_video_art = array();
					$ans_reg_audio_art = array();
					$ans_reg_gallery_art = array();
					
					$ans_reg_video_com = array();
					$ans_reg_audio_com = array();
					$ans_reg_gallery_com = array();
					
					$ans_gen_media = array();
					
					$sql_reg_video_art = mysql_query("SELECT * FROM general_artist_video");
					$sql_reg_audio_art = mysql_query("SELECT * FROM general_artist_audio");
					$sql_reg_gallery_art = mysql_query("SELECT * FROM general_artist_gallery_list");
					
					$sql_reg_video_com = mysql_query("SELECT * FROM general_community_video");
					$sql_reg_audio_com = mysql_query("SELECT * FROM general_community_audio");
					$sql_reg_gallery_com = mysql_query("SELECT * FROM general_community_gallery_list");
					
					$sql_gen_media = mysql_query("SELECT * FROM general_media WHERE delete_status=0 AND media_status=0");
					
					/* if(mysql_num_rows($sql_reg_video_art)>0)
					{
						while($row_v_art = mysql_fetch_assoc($sql_reg_video_art))
						{
							$row_v_art["table_name"] = "general_artist_video";
							$ans_reg_video_art[] = $row_v_art;
						}
					}
					
					if(mysql_num_rows($sql_reg_audio_art)>0)
					{
						while($row_a_art = mysql_fetch_assoc($sql_reg_audio_art))
						{
							$row_a_art["table_name"] = "general_artist_audio";
							$ans_reg_audio_art[] = $row_a_art;
						}
					}
					
					if(mysql_num_rows($sql_reg_gallery_art)>0)
					{
						while($row_g_art = mysql_fetch_assoc($sql_reg_gallery_art))
						{
							$row_g_art["table_name"] = "general_artist_gallery_list";
							$ans_reg_gallery_art[] = $row_g_art;
						}
					}
					
					if(mysql_num_rows($sql_reg_video_com)>0)
					{
						while($row_v_com = mysql_fetch_assoc($sql_reg_video_com))
						{
							$row_v_com["table_name"] = "general_community_video";
							$ans_reg_video_com[] = $row_v_com;
						}
					}
					
					if(mysql_num_rows($sql_reg_gallery_com)>0)
					{
						while($row_g_com = mysql_fetch_assoc($sql_reg_gallery_com))
						{
							$row_g_com["table_name"] = "general_community_gallery_list";
							$ans_reg_gallery_com[] = $row_g_com;
						}
					} */
					
					if(mysql_num_rows($sql_gen_media)>0)
					{
						while($gen_m = mysql_fetch_assoc($sql_gen_media))
						{
							$gen_m['table_name'] = 'general_media';
							$ans_gen_media[] = $gen_m;
						}
					}
					
					/* if(mysql_num_rows($sql_reg_audio_com)>0)
					{
						while($row_a_com = mysql_fetch_assoc($sql_reg_audio_com))
						{
							$row_a_com["table_name"] = "general_community_audio";
							$ans_reg_audio_com[] = $row_a_com;
						}
					} */
					
					$main_ans = array_merge($ans_reg_video_art,$ans_reg_audio_art,$ans_reg_gallery_art,$ans_reg_video_com,$ans_reg_audio_com,$ans_reg_gallery_com,$ans_gen_media);
					return $main_ans;
				}
			}
			
			////////////////////////////////////Only Country Search////////////////////////////////////
			if($profile_type_search=="select" && $page_type_search==0 && $sub_type_search==0 && $meta_type_search==0 && $country_search!=0 && $match_string=="Search" && $match_table=="" && $match_id=="" && $alpha=="")
			{
				// echo "f8";
				// $re = "f8";
				// return $re;
				$ans_artc = array();
				$ans_comc = array();
				$ans_artpc = array();
				$ans_compc = array();
				$ans_artea = array();
				$ans_comec = array();
				$ans_media = array();
				
				$ans_reg_video_art = array();
				$ans_reg_audio_art = array();
				$ans_reg_gallery_art = array();
				
				$ans_reg_video_com = array();
				$ans_reg_audio_com = array();
				$ans_reg_gallery_com = array();
				
				if($state_search==0 && $city_search=="")
				{
					$sql_art = mysql_query("SELECT * FROM general_artist WHERE country_id='".$country_search."' AND status=0 AND active=0 AND image_name!='' AND profile_url!=''");
					$sql_com = mysql_query("SELECT * FROM general_community WHERE country_id='".$country_search."' AND status=0 AND active=0 AND image_name!='' AND profile_url!=''");
					$sql_artp = mysql_query("SELECT * FROM artist_project WHERE country_id='".$country_search."' AND del_status=0 AND active=0 AND image_name!='' AND profile_url!=''");
					$sql_comp = mysql_query("SELECT * FROM community_project WHERE country_id='".$country_search."' AND del_status=0 AND active=0 AND image_name!='' AND profile_url!=''");
					$sql_arte = mysql_query("SELECT * FROM artist_event WHERE country_id='".$country_search."' AND image_name!='' AND profile_url!=''");
					$sql_come = mysql_query("SELECT * FROM community_event WHERE country_id='".$country_search."' AND image_name!='' AND profile_url!=''");
					
					$sql_reg_video_art_cou = mysql_query("SELECT * FROM general_artist_video");
					$sql_reg_audio_art_cou = mysql_query("SELECT * FROM general_artist_audio");
					$sql_reg_gallery_art_cou = mysql_query("SELECT * FROM general_artist_gallery_list");
					
					$sql_reg_video_com_cou = mysql_query("SELECT * FROM general_community_video");
					$sql_reg_audio_com_cou = mysql_query("SELECT * FROM general_community_audio");
					$sql_reg_gallery_com_cou = mysql_query("SELECT * FROM general_community_gallery_list");
					
					/* if(mysql_num_rows($sql_reg_video_art_cou)>0)
					{
						while($row_reg_video_art = mysql_fetch_assoc($sql_reg_video_art_cou))
						{
							$sql_art_vi_cou = mysql_query("SELECT * FROM general_artist WHERE country_id='".$country_search."' AND status=0 AND active=0 AND artist_id='".$row_reg_video_art['profile_id']."'");
							if(mysql_num_rows($sql_art_vi_cou)>0)
							{
								$row_reg_video_art["table_name"] = 'general_artist_video';
								$ans_reg_video_art[] = $row_reg_video_art;
							}
						}
					}
					
					if(mysql_num_rows($sql_reg_audio_art_cou)>0)
					{
						while($row_reg_audio_art_cou = mysql_fetch_assoc($sql_reg_audio_art_cou))
						{
							$sql_art_au_cou = mysql_query("SELECT * FROM general_artist WHERE country_id='".$country_search."' AND status=0 AND active=0 AND artist_id='".$row_reg_audio_art_cou['profile_id']."'");
							if(mysql_num_rows($sql_art_au_cou)>0)
							{
								$row_reg_audio_art_cou["table_name"] = 'general_artist_audio';
								$ans_reg_audio_art[] = $row_reg_audio_art_cou;
							}
						}
					}
					
					if(mysql_num_rows($sql_reg_gallery_art_cou)>0)
					{
						while($row_reg_gallery_art_cou = mysql_fetch_assoc($sql_reg_gallery_art_cou))
						{
							$sql_get_pro_gal = mysql_query("SELECT * FROM general_artist_gallery WHERE gallery_id='".$row_reg_gallery_art_cou['gallery_id']."'");
							$ans_get_pro_gal = mysql_fetch_assoc($sql_get_pro_gal);
							
							$sql_art_ga_cou = mysql_query("SELECT * FROM general_artist WHERE country_id='".$country_search."' AND status=0 AND active=0 AND artist_id='".$ans_get_pro_gal['profile_id']."'");
							if(mysql_num_rows($sql_art_ga_cou)>0)
							{
								$row_reg_gallery_art_cou["table_name"] = 'general_artist_gallery_list';
								$ans_reg_gallery_art[] = $row_reg_gallery_art_cou;
							}
						}
					}
					
					if(mysql_num_rows($sql_reg_video_com_cou)>0)
					{
						while($row_reg_video_com_cou = mysql_fetch_assoc($sql_reg_video_com_cou))
						{
							$sql_com_vi_cou = mysql_query("SELECT * FROM general_community WHERE country_id='".$country_search."' AND status=0 AND active=0 AND community_id='".$row_reg_video_com_cou['profile_id']."'");
							if(mysql_num_rows($sql_com_vi_cou)>0)
							{
								$row_reg_video_com_cou["table_name"] = 'general_community_video';
								$ans_reg_video_com[] = $row_reg_video_com_cou;
							}
						}
					}
					
					if(mysql_num_rows($sql_reg_audio_com_cou)>0)
					{
						while($row_reg_audio_com_cou = mysql_fetch_assoc($sql_reg_audio_com_cou))
						{
							$sql_com_au_cou = mysql_query("SELECT * FROM general_community WHERE country_id='".$country_search."' AND status=0 AND active=0 AND community_id='".$row_reg_audio_com_cou['profile_id']."'");
							if(mysql_num_rows($sql_com_au_cou)>0)
							{
								$row_reg_audio_com_cou["table_name"] = 'general_community_audio';
								$ans_reg_audio_com[] = $row_reg_audio_com_cou;
							}
						}
					}
					
					if(mysql_num_rows($sql_reg_gallery_com_cou)>0)
					{
						while($row_reg_gallery_com_cou = mysql_fetch_assoc($sql_reg_gallery_com_cou))
						{
							$sql_get_pro_gal = mysql_query("SELECT * FROM general_artist_gallery WHERE gallery_id='".$row_reg_gallery_com_cou['gallery_id']."'");
							$ans_get_pro_gal = mysql_fetch_assoc($sql_get_pro_gal);
							
							$sql_com_ga_cou = mysql_query("SELECT * FROM general_community WHERE country_id='".$country_search."' AND status=0 AND active=0 AND community_id='".$ans_get_pro_gal['profile_id']."'");
							if(mysql_num_rows($sql_com_ga_cou)>0)
							{
								$row_reg_gallery_com_cou["table_name"] = 'general_community_gallery_list';
								$ans_reg_gallery_com[] = $row_reg_gallery_com_cou;
							}
						}
					} */
					
					$sql_med = mysql_query("SELECT * FROM general_media WHERE delete_status=0 AND media_status=0");
					if(mysql_num_rows($sql_med)>0)
					{
						while($row_mede = mysql_fetch_assoc($sql_med))
						{
							if($row_mede['from_info']!="")
							{
								$exp_from = explode('|',$row_mede['from_info']);
								if($exp_from[0]=='general_artist')
								{
									$sql_get_tables = mysql_query("SELECT * FROM $exp_from[0] WHERE artist_id='".$exp_from[1]."' AND country_id='".$country_search."' AND status=0 AND active=0");
								}
								elseif($exp_from[0]=='general_community')
								{
									$sql_get_tables = mysql_query("SELECT * FROM $exp_from[0] WHERE community_id='".$exp_from[1]."' AND country_id='".$country_search."' AND status=0 AND active=0");
								}
								elseif($exp_from[0]=='community_event' || $exp_from[0]=='artist_event' || $exp_from[0]=='artist_project' || $exp_from[0]=='community_project')
								{
									$sql_get_tables = mysql_query("SELECT * FROM $exp_from[0] WHERE id='".$exp_from[1]."' AND country_id='".$country_search."' AND del_status=0 AND active=0");
								}
								
								if(isset($sql_get_tables) && !empty($sql_get_tables))
								{
									if(mysql_num_rows($sql_get_tables)>0)
									{
										$sql_get_med = mysql_query("SELECT * FROM general_media WHERE id='".$row_mede['id']."' AND delete_status=0 AND media_status=0");
										$ans_media[] = mysql_fetch_assoc($sql_get_med);
									}
								}
							}
							elseif($row_mede['creator_info']!="")
							{
								$exp_from = explode('|',$row_mede['creator_info']);
								if($exp_from[0]=='general_artist')
								{
									$sql_get_tables = mysql_query("SELECT * FROM $exp_from[0] WHERE artist_id='".$exp_from[1]."' AND country_id='".$country_search."' AND status=0 AND active=0");
								}
								elseif($exp_from[0]=='general_community')
								{
									$sql_get_tables = mysql_query("SELECT * FROM $exp_from[0] WHERE community_id='".$exp_from[1]."' AND country_id='".$country_search."' AND status=0 AND active=0");
								}
								elseif($exp_from[0]=='community_event' || $exp_from[0]=='artist_event' || $exp_from[0]=='artist_project' || $exp_from[0]=='community_project')
								{
									$sql_get_tables = mysql_query("SELECT * FROM $exp_from[0] WHERE id='".$exp_from[1]."' AND country_id='".$country_search."' AND del_status=0 AND active=0");
								}
								
								if(isset($sql_get_tables) && !empty($sql_get_tables))
								{
									if(mysql_num_rows($sql_get_tables)>0)
									{
										$sql_get_med = mysql_query("SELECT * FROM general_media WHERE id='".$row_mede['id']."' AND delete_status=0 AND media_status=0");
										$ans_media[] = mysql_fetch_assoc($sql_get_med);
									}
								}
							}
						}
					}
				}
				elseif($state_search!=0 && $city_search=="")
				{
					$sql_art = mysql_query("SELECT * FROM general_artist WHERE country_id='".$country_search."' AND state_id='".$state_search."' AND status=0 AND active=0 AND image_name!='' AND profile_url!=''");
					$sql_com = mysql_query("SELECT * FROM general_community WHERE country_id='".$country_search."' AND state_id='".$state_search."' AND status=0 AND active=0 AND image_name!='' AND profile_url!=''");
					$sql_artp = mysql_query("SELECT * FROM artist_project WHERE country_id='".$country_search."' AND state_id='".$state_search."' AND del_status=0 AND active=0 AND image_name!='' AND profile_url!=''");
					$sql_comp = mysql_query("SELECT * FROM community_project WHERE country_id='".$country_search."' AND state_id='".$state_search."' AND del_status=0 AND active=0 AND image_name!='' AND profile_url!=''");
					$sql_arte = mysql_query("SELECT * FROM artist_event WHERE country_id='".$country_search."' AND state_id='".$state_search."' AND image_name!='' AND profile_url!=''");
					$sql_come = mysql_query("SELECT * FROM community_event WHERE country_id='".$country_search."' AND state_id='".$state_search."' AND image_name!='' AND profile_url!=''");
					
					$sql_reg_video_art_cou = mysql_query("SELECT * FROM general_artist_video");
					$sql_reg_audio_art_cou = mysql_query("SELECT * FROM general_artist_audio");
					$sql_reg_gallery_art_cou = mysql_query("SELECT * FROM general_artist_gallery_list");
					
					$sql_reg_video_com_cou = mysql_query("SELECT * FROM general_community_video");
					$sql_reg_audio_com_cou = mysql_query("SELECT * FROM general_community_audio");
					$sql_reg_gallery_com_cou = mysql_query("SELECT * FROM general_community_gallery_list");
					
					/* if(mysql_num_rows($sql_reg_video_art_cou)>0)
					{
						while($row_reg_video_art = mysql_fetch_assoc($sql_reg_video_art_cou))
						{
							$sql_art_vi_cou = mysql_query("SELECT * FROM general_artist WHERE country_id='".$country_search."' AND state_id='".$state_search."' AND status=0 AND active=0 AND artist_id='".$row_reg_video_art['profile_id']."'");
							if(mysql_num_rows($sql_art_vi_cou)>0)
							{
								$row_reg_video_art["table_name"] = 'general_artist_video';
								$ans_reg_video_art[] = $row_reg_video_art;
							}
						}
					}
					
					if(mysql_num_rows($sql_reg_audio_art_cou)>0)
					{
						while($row_reg_audio_art_cou = mysql_fetch_assoc($sql_reg_audio_art_cou))
						{
							$sql_art_au_cou = mysql_query("SELECT * FROM general_artist WHERE country_id='".$country_search."' AND state_id='".$state_search."' AND status=0 AND active=0 AND artist_id='".$row_reg_audio_art_cou['profile_id']."'");
							if(mysql_num_rows($sql_art_au_cou)>0)
							{
								$row_reg_audio_art_cou["table_name"] = 'general_artist_audio';
								$ans_reg_audio_art[] = $row_reg_audio_art_cou;
							}
						}
					}
					
					if(mysql_num_rows($sql_reg_gallery_art_cou)>0)
					{
						while($row_reg_gallery_art_cou = mysql_fetch_assoc($sql_reg_gallery_art_cou))
						{
							$sql_get_pro_gal = mysql_query("SELECT * FROM general_artist_gallery WHERE gallery_id='".$row_reg_gallery_art_cou['gallery_id']."'");
							$ans_get_pro_gal = mysql_fetch_assoc($sql_get_pro_gal);
							
							$sql_art_ga_cou = mysql_query("SELECT * FROM general_artist WHERE country_id='".$country_search."' AND state_id='".$state_search."' AND status=0 AND active=0 AND artist_id='".$ans_get_pro_gal['profile_id']."'");
							if(mysql_num_rows($sql_art_ga_cou)>0)
							{
								$row_reg_gallery_art_cou["table_name"] = 'general_artist_gallery_list';
								$ans_reg_gallery_art[] = $row_reg_gallery_art_cou;
							}
						}
					}
					
					if(mysql_num_rows($sql_reg_video_com_cou)>0)
					{
						while($row_reg_video_com_cou = mysql_fetch_assoc($sql_reg_video_com_cou))
						{
							$sql_com_vi_cou = mysql_query("SELECT * FROM general_community WHERE country_id='".$country_search."' AND state_id='".$state_search."' AND status=0 AND active=0 AND community_id='".$row_reg_video_com_cou['profile_id']."'");
							if(mysql_num_rows($sql_com_vi_cou)>0)
							{
								$row_reg_video_com_cou["table_name"] = 'general_community_video';
								$ans_reg_video_com[] = $row_reg_video_com_cou;
							}
						}
					}
					
					if(mysql_num_rows($sql_reg_audio_com_cou)>0)
					{
						while($row_reg_audio_com_cou = mysql_fetch_assoc($sql_reg_audio_com_cou))
						{
							$sql_com_au_cou = mysql_query("SELECT * FROM general_community WHERE country_id='".$country_search."' AND state_id='".$state_search."' AND status=0 AND active=0 AND community_id='".$row_reg_audio_com_cou['profile_id']."'");
							if(mysql_num_rows($sql_com_au_cou)>0)
							{
								$row_reg_audio_com_cou["table_name"] = 'general_community_audio';
								$ans_reg_audio_com[] = $row_reg_audio_com_cou;
							}
						}
					}
					
					if(mysql_num_rows($sql_reg_gallery_com_cou)>0)
					{
						while($row_reg_gallery_com_cou = mysql_fetch_assoc($sql_reg_gallery_com_cou))
						{
							$sql_get_pro_gal = mysql_query("SELECT * FROM general_artist_gallery WHERE gallery_id='".$row_reg_gallery_com_cou['gallery_id']."'");
							$ans_get_pro_gal = mysql_fetch_assoc($sql_get_pro_gal);
							
							$sql_com_ga_cou = mysql_query("SELECT * FROM general_community WHERE country_id='".$country_search."' AND state_id='".$state_search."' AND status=0 AND active=0 AND community_id='".$ans_get_pro_gal['profile_id']."'");
							if(mysql_num_rows($sql_com_ga_cou)>0)
							{
								$row_reg_gallery_com_cou["table_name"] = 'general_community_gallery_list';
								$ans_reg_gallery_com[] = $row_reg_gallery_com_cou;
							}
						}
					} */
					
					
					$sql_med = mysql_query("SELECT * FROM general_media WHERE delete_status=0 AND media_status=0");
					if(mysql_num_rows($sql_med)>0)
					{
						while($row_mede = mysql_fetch_assoc($sql_med))
						{
							if($row_mede['from_info']!="")
							{
								$exp_from = explode('|',$row_mede['from_info']);
								if($exp_from[0]=='general_artist')
								{
									$sql_get_tables = mysql_query("SELECT * FROM $exp_from[0] WHERE artist_id='".$exp_from[1]."' AND country_id='".$country_search."' AND state_id='".$state_search."' AND status=0 AND active=0");
								}
								elseif($exp_from[0]=='general_community')
								{
									$sql_get_tables = mysql_query("SELECT * FROM $exp_from[0] WHERE community_id='".$exp_from[1]."' AND country_id='".$country_search."' AND state_id='".$state_search."' AND status=0 AND active=0");
								}
								elseif($exp_from[0]=='community_event' || $exp_from[0]=='artist_event' || $exp_from[0]=='artist_project' || $exp_from[0]=='community_project')
								{
									$sql_get_tables = mysql_query("SELECT * FROM $exp_from[0] WHERE id='".$exp_from[1]."' AND country_id='".$country_search."' AND state_id='".$state_search."' AND del_status=0 AND active=0");
								}
								
								if(isset($sql_get_tables) && !empty($sql_get_tables))
								{
									if(mysql_num_rows($sql_get_tables)>0)
									{
										$sql_get_med = mysql_query("SELECT * FROM general_media WHERE id='".$row_mede['id']."' AND delete_status=0 AND media_status=0");
										$ans_media[] = mysql_fetch_assoc($sql_get_med);
									}
								}								
							}
							elseif($row_mede['creator_info']!="")
							{
								$exp_from = explode('|',$row_mede['creator_info']);
								if($exp_from[0]=='general_artist')
								{
									$sql_get_tables = mysql_query("SELECT * FROM $exp_from[0] WHERE artist_id='".$exp_from[1]."' AND country_id='".$country_search."' AND state_id='".$state_search."' AND status=0 AND active=0");
								}
								elseif($exp_from[0]=='general_community')
								{
									$sql_get_tables = mysql_query("SELECT * FROM $exp_from[0] WHERE community_id='".$exp_from[1]."' AND country_id='".$country_search."' AND state_id='".$state_search."' AND status=0 AND active=0");
								}
								elseif($exp_from[0]=='community_event' || $exp_from[0]=='artist_event' || $exp_from[0]=='artist_project' || $exp_from[0]=='community_project')
								{
									$sql_get_tables = mysql_query("SELECT * FROM $exp_from[0] WHERE id='".$exp_from[1]."' AND country_id='".$country_search."' AND state_id='".$state_search."' AND del_status=0 AND active=0");
								}
								
								if(isset($sql_get_tables) && !empty($sql_get_tables))
								{
									if(mysql_num_rows($sql_get_tables)>0)
									{
										$sql_get_med = mysql_query("SELECT * FROM general_media WHERE id='".$row_mede['id']."' AND delete_status=0 AND media_status=0");
										$ans_media[] = mysql_fetch_assoc($sql_get_med);
									}
								}
							}
						}
					}
				}
				elseif($state_search==0 && $city_search!="")
				{
					$sql_art = mysql_query("SELECT * FROM general_artist WHERE country_id='".$country_search."' AND city='".$city_search."' AND status=0 AND active=0 AND image_name!='' AND profile_url!=''");
					$sql_com = mysql_query("SELECT * FROM general_community WHERE country_id='".$country_search."' AND city='".$city_search."' AND status=0 AND active=0 AND image_name!='' AND profile_url!=''");
					$sql_artp = mysql_query("SELECT * FROM artist_project WHERE country_id='".$country_search."' AND city='".$city_search."' AND del_status=0 AND active=0 AND image_name!='' AND profile_url!=''");
					$sql_comp = mysql_query("SELECT * FROM community_project WHERE country_id='".$country_search."' AND city='".$city_search."' AND del_status=0 AND active=0 AND image_name!='' AND profile_url!=''");
					$sql_arte = mysql_query("SELECT * FROM artist_event WHERE country_id='".$country_search."' AND city='".$city_search."' AND image_name!='' AND profile_url!=''");
					$sql_come = mysql_query("SELECT * FROM community_event WHERE country_id='".$country_search."' AND city='".$city_search."' AND image_name!='' AND profile_url!=''");
					
					$sql_reg_video_art_cou = mysql_query("SELECT * FROM general_artist_video");
					$sql_reg_audio_art_cou = mysql_query("SELECT * FROM general_artist_audio");
					$sql_reg_gallery_art_cou = mysql_query("SELECT * FROM general_artist_gallery_list");
					
					$sql_reg_video_com_cou = mysql_query("SELECT * FROM general_community_video");
					$sql_reg_audio_com_cou = mysql_query("SELECT * FROM general_community_audio");
					$sql_reg_gallery_com_cou = mysql_query("SELECT * FROM general_community_gallery_list");
					
					/* if(mysql_num_rows($sql_reg_video_art_cou)>0)
					{
						while($row_reg_video_art = mysql_fetch_assoc($sql_reg_video_art_cou))
						{
							$sql_art_vi_cou = mysql_query("SELECT * FROM general_artist WHERE country_id='".$country_search."' AND city='".$city_search."' AND status=0 AND active=0 AND artist_id='".$row_reg_video_art['profile_id']."'");
							if(mysql_num_rows($sql_art_vi_cou)>0)
							{
								$row_reg_video_art["table_name"] = 'general_artist_video';
								$ans_reg_video_art[] = $row_reg_video_art;
							}
						}
					}
					
					if(mysql_num_rows($sql_reg_audio_art_cou)>0)
					{
						while($row_reg_audio_art_cou = mysql_fetch_assoc($sql_reg_audio_art_cou))
						{
							$sql_art_au_cou = mysql_query("SELECT * FROM general_artist WHERE country_id='".$country_search."' AND city='".$city_search."' AND status=0 AND active=0 AND artist_id='".$row_reg_audio_art_cou['profile_id']."'");
							if(mysql_num_rows($sql_art_au_cou)>0)
							{
								$row_reg_audio_art_cou["table_name"] = 'general_artist_audio';
								$ans_reg_audio_art[] = $row_reg_audio_art_cou;
							}
						}
					}
					
					if(mysql_num_rows($sql_reg_gallery_art_cou)>0)
					{
						while($row_reg_gallery_art_cou = mysql_fetch_assoc($sql_reg_gallery_art_cou))
						{
							$sql_get_pro_gal = mysql_query("SELECT * FROM general_artist_gallery WHERE gallery_id='".$row_reg_gallery_art_cou['gallery_id']."'");
							$ans_get_pro_gal = mysql_fetch_assoc($sql_get_pro_gal);
							
							$sql_art_ga_cou = mysql_query("SELECT * FROM general_artist WHERE country_id='".$country_search."' AND city='".$city_search."' AND status=0 AND active=0 AND artist_id='".$ans_get_pro_gal['profile_id']."'");
							if(mysql_num_rows($sql_art_ga_cou)>0)
							{
								$row_reg_gallery_art_cou["table_name"] = 'general_artist_gallery_list';
								$ans_reg_gallery_art[] = $row_reg_gallery_art_cou;
							}
						}
					}
					
					if(mysql_num_rows($sql_reg_video_com_cou)>0)
					{
						while($row_reg_video_com_cou = mysql_fetch_assoc($sql_reg_video_com_cou))
						{
							$sql_com_vi_cou = mysql_query("SELECT * FROM general_community WHERE country_id='".$country_search."' AND city='".$city_search."' AND status=0 AND active=0 AND community_id='".$row_reg_video_com_cou['profile_id']."'");
							if(mysql_num_rows($sql_com_vi_cou)>0)
							{
								$row_reg_video_com_cou["table_name"] = 'general_community_video';
								$ans_reg_video_com[] = $row_reg_video_com_cou;
							}
						}
					}
					
					if(mysql_num_rows($sql_reg_audio_com_cou)>0)
					{
						while($row_reg_audio_com_cou = mysql_fetch_assoc($sql_reg_audio_com_cou))
						{
							$sql_com_au_cou = mysql_query("SELECT * FROM general_community WHERE country_id='".$country_search."' AND city='".$city_search."' AND status=0 AND active=0 AND community_id='".$row_reg_audio_com_cou['profile_id']."'");
							if(mysql_num_rows($sql_com_au_cou)>0)
							{
								$row_reg_audio_com_cou["table_name"] = 'general_community_audio';
								$ans_reg_audio_com[] = $row_reg_audio_com_cou;
							}
						}
					}
					
					if(mysql_num_rows($sql_reg_gallery_com_cou)>0)
					{
						while($row_reg_gallery_com_cou = mysql_fetch_assoc($sql_reg_gallery_com_cou))
						{
							$sql_get_pro_gal = mysql_query("SELECT * FROM general_artist_gallery WHERE gallery_id='".$row_reg_gallery_com_cou['gallery_id']."'");
							$ans_get_pro_gal = mysql_fetch_assoc($sql_get_pro_gal);
							
							$sql_com_ga_cou = mysql_query("SELECT * FROM general_community WHERE country_id='".$country_search."' AND city='".$city_search."' AND status=0 AND active=0 AND community_id='".$ans_get_pro_gal['profile_id']."'");
							if(mysql_num_rows($sql_com_ga_cou)>0)
							{
								$row_reg_gallery_com_cou["table_name"] = 'general_community_gallery_list';
								$ans_reg_gallery_com[] = $row_reg_gallery_com_cou;
							}
						}
					} */
					
					
					$sql_med = mysql_query("SELECT * FROM general_media WHERE delete_status=0 AND media_status=0");
					if(mysql_num_rows($sql_med)>0)
					{
						while($row_mede = mysql_fetch_assoc($sql_med))
						{
							if($row_mede['from_info']!="")
							{
								$exp_from = explode('|',$row_mede['from_info']);
								if($exp_from[0]=='general_artist')
								{
									$sql_get_tables = mysql_query("SELECT * FROM $exp_from[0] WHERE artist_id='".$exp_from[1]."' AND country_id='".$country_search."' AND city='".$city_search."' AND status=0 AND active=0");
								}
								elseif($exp_from[0]=='general_community')
								{
									$sql_get_tables = mysql_query("SELECT * FROM $exp_from[0] WHERE community_id='".$exp_from[1]."' AND country_id='".$country_search."' AND city='".$city_search."' AND status=0 AND active=0");
								}
								elseif($exp_from[0]=='community_event' || $exp_from[0]=='artist_event' || $exp_from[0]=='artist_project' || $exp_from[0]=='community_project')
								{
									$sql_get_tables = mysql_query("SELECT * FROM $exp_from[0] WHERE id='".$exp_from[1]."' AND country_id='".$country_search."' AND city='".$city_search."' AND del_status=0 AND active=0");
								}
								
								
								if(isset($sql_get_tables) && !empty($sql_get_tables))
								{
									if(mysql_num_rows($sql_get_tables)>0)
									{
										$sql_get_med = mysql_query("SELECT * FROM general_media WHERE id='".$row_mede['id']."'");
										$ans_media[] = mysql_fetch_assoc($sql_get_med);
									}
								}
							}
							elseif($row_mede['creator_info']!="")
							{
								$exp_from = explode('|',$row_mede['creator_info']);
								if($exp_from[0]=='general_artist')
								{
									$sql_get_tables = mysql_query("SELECT * FROM $exp_from[0] WHERE artist_id='".$exp_from[1]."' AND country_id='".$country_search."' AND city='".$city_search."' AND status=0 AND active=0");
								}
								elseif($exp_from[0]=='general_community')
								{
									$sql_get_tables = mysql_query("SELECT * FROM $exp_from[0] WHERE community_id='".$exp_from[1]."' AND country_id='".$country_search."' AND city='".$city_search."' AND status=0 AND active=0");
								}
								elseif($exp_from[0]=='community_event' || $exp_from[0]=='artist_event' || $exp_from[0]=='artist_project' || $exp_from[0]=='community_project')
								{
									$sql_get_tables = mysql_query("SELECT * FROM $exp_from[0] WHERE id='".$exp_from[1]."' AND country_id='".$country_search."' AND city='".$city_search."' AND del_status=0 AND active=0");
								}
								
								if(isset($sql_get_tables) && !empty($sql_get_tables))
								{
									if(mysql_num_rows($sql_get_tables)>0)
									{
										$sql_get_med = mysql_query("SELECT * FROM general_media WHERE id='".$row_mede['id']."'");
										$ans_media[] = mysql_fetch_assoc($sql_get_med);
									}
								}
							}
						}
					}
				}
				elseif($state_search!=0 && $city_search!="")
				{
					$sql_art = mysql_query("SELECT * FROM general_artist WHERE country_id='".$country_search."' AND state_id='".$state_search."' AND city='".$city_search."' AND status=0 AND active=0 AND image_name!='' AND profile_url!=''");
					$sql_com = mysql_query("SELECT * FROM general_community WHERE country_id='".$country_search."' AND state_id='".$state_search."' AND city='".$city_search."' AND status=0 AND active=0 AND image_name!='' AND profile_url!=''");
					$sql_artp = mysql_query("SELECT * FROM artist_project WHERE country_id='".$country_search."' AND state_id='".$state_search."' AND city='".$city_search."' AND del_status=0 AND active=0 AND image_name!='' AND profile_url!=''");
					$sql_comp = mysql_query("SELECT * FROM community_project WHERE country_id='".$country_search."' AND state_id='".$state_search."' AND city='".$city_search."' AND del_status=0 AND active=0 AND image_name!='' AND profile_url!=''");
					$sql_arte = mysql_query("SELECT * FROM artist_event WHERE country_id='".$country_search."' AND state_id='".$state_search."' AND city='".$city_search."' AND image_name!='' AND profile_url!=''");
					$sql_come = mysql_query("SELECT * FROM community_event WHERE country_id='".$country_search."' AND state_id='".$state_search."' AND city='".$city_search."' AND image_name!='' AND profile_url!=''");
					
					$sql_reg_video_art_cou = mysql_query("SELECT * FROM general_artist_video");
					$sql_reg_audio_art_cou = mysql_query("SELECT * FROM general_artist_audio");
					$sql_reg_gallery_art_cou = mysql_query("SELECT * FROM general_artist_gallery_list");
					
					$sql_reg_video_com_cou = mysql_query("SELECT * FROM general_community_video");
					$sql_reg_audio_com_cou = mysql_query("SELECT * FROM general_community_audio");
					$sql_reg_gallery_com_cou = mysql_query("SELECT * FROM general_community_gallery_list");
					
					/* if(mysql_num_rows($sql_reg_video_art_cou)>0)
					{
						while($row_reg_video_art = mysql_fetch_assoc($sql_reg_video_art_cou))
						{
							$sql_art_vi_cou = mysql_query("SELECT * FROM general_artist WHERE country_id='".$country_search."' AND state_id='".$state_search."' AND city='".$city_search."' AND status=0 AND active=0 AND artist_id='".$row_reg_video_art['profile_id']."'");
							if(mysql_num_rows($sql_art_vi_cou)>0)
							{
								$row_reg_video_art["table_name"] = 'general_artist_video';
								$ans_reg_video_art[] = $row_reg_video_art;
							}
						}
					}
					
					if(mysql_num_rows($sql_reg_audio_art_cou)>0)
					{
						while($row_reg_audio_art_cou = mysql_fetch_assoc($sql_reg_audio_art_cou))
						{
							$sql_art_au_cou = mysql_query("SELECT * FROM general_artist WHERE country_id='".$country_search."' AND state_id='".$state_search."' AND city='".$city_search."' AND status=0 AND active=0 AND artist_id='".$row_reg_audio_art_cou['profile_id']."'");
							if(mysql_num_rows($sql_art_au_cou)>0)
							{
								$row_reg_audio_art_cou["table_name"] = 'general_artist_audio';
								$ans_reg_audio_art[] = $row_reg_audio_art_cou;
							}
						}
					}
					
					if(mysql_num_rows($sql_reg_gallery_art_cou)>0)
					{
						while($row_reg_gallery_art_cou = mysql_fetch_assoc($sql_reg_gallery_art_cou))
						{
							$sql_get_pro_gal = mysql_query("SELECT * FROM general_artist_gallery WHERE gallery_id='".$row_reg_gallery_art_cou['gallery_id']."'");
							$ans_get_pro_gal = mysql_fetch_assoc($sql_get_pro_gal);
							
							$sql_art_ga_cou = mysql_query("SELECT * FROM general_artist WHERE country_id='".$country_search."' AND state_id='".$state_search."' AND city='".$city_search."' AND status=0 AND active=0 AND artist_id='".$ans_get_pro_gal['profile_id']."'");
							if(mysql_num_rows($sql_art_ga_cou)>0)
							{
								$row_reg_gallery_art_cou["table_name"] = 'general_artist_gallery_list';
								$ans_reg_gallery_art[] = $row_reg_gallery_art_cou;
							}
						}
					}
					
					if(mysql_num_rows($sql_reg_video_com_cou)>0)
					{
						while($row_reg_video_com_cou = mysql_fetch_assoc($sql_reg_video_com_cou))
						{
							$sql_com_vi_cou = mysql_query("SELECT * FROM general_community WHERE country_id='".$country_search."' AND state_id='".$state_search."' AND city='".$city_search."' AND status=0 AND active=0 AND community_id='".$row_reg_video_com_cou['profile_id']."'");
							if(mysql_num_rows($sql_com_vi_cou)>0)
							{
								$row_reg_video_com_cou["table_name"] = 'general_community_video';
								$ans_reg_video_com[] = $row_reg_video_com_cou;
							}
						}
					}
					
					if(mysql_num_rows($sql_reg_audio_com_cou)>0)
					{
						while($row_reg_audio_com_cou = mysql_fetch_assoc($sql_reg_audio_com_cou))
						{
							$sql_com_au_cou = mysql_query("SELECT * FROM general_community WHERE country_id='".$country_search."' AND state_id='".$state_search."' AND city='".$city_search."' AND status=0 AND active=0 AND community_id='".$row_reg_audio_com_cou['profile_id']."'");
							if(mysql_num_rows($sql_com_au_cou)>0)
							{
								$row_reg_audio_com_cou["table_name"] = 'general_community_audio';
								$ans_reg_audio_com[] = $row_reg_audio_com_cou;
							}
						}
					}
					
					if(mysql_num_rows($sql_reg_gallery_com_cou)>0)
					{
						while($row_reg_gallery_com_cou = mysql_fetch_assoc($sql_reg_gallery_com_cou))
						{
							$sql_get_pro_gal = mysql_query("SELECT * FROM general_artist_gallery WHERE gallery_id='".$row_reg_gallery_com_cou['gallery_id']."'");
							$ans_get_pro_gal = mysql_fetch_assoc($sql_get_pro_gal);
							
							$sql_com_ga_cou = mysql_query("SELECT * FROM general_community WHERE country_id='".$country_search."' AND state_id='".$state_search."' AND city='".$city_search."' AND status=0 AND active=0 AND community_id='".$ans_get_pro_gal['profile_id']."'");
							if(mysql_num_rows($sql_com_ga_cou)>0)
							{
								$row_reg_gallery_com_cou["table_name"] = 'general_community_gallery_list';
								$ans_reg_gallery_com[] = $row_reg_gallery_com_cou;
							}
						}
					} */
					
					$sql_med = mysql_query("SELECT * FROM general_media WHERE delete_status=0 AND media_status=0");
					if(mysql_num_rows($sql_med)>0)
					{
						while($row_mede = mysql_fetch_assoc($sql_med))
						{
							if($row_mede['from_info']!="")
							{
								$exp_from = explode('|',$row_mede['from_info']);
								if($exp_from[0]=='general_artist')
								{
									$sql_get_tables = mysql_query("SELECT * FROM $exp_from[0] WHERE artist_id='".$exp_from[1]."' AND country_id='".$country_search."' AND state_id='".$state_search."' AND city='".$city_search."' AND status=0 AND active=0");
								}
								elseif($exp_from[0]=='general_community')
								{
									$sql_get_tables = mysql_query("SELECT * FROM $exp_from[0] WHERE community_id='".$exp_from[1]."' AND country_id='".$country_search."' AND state_id='".$state_search."' AND city='".$city_search."' AND status=0 AND active=0");
								}
								elseif($exp_from[0]=='community_event' || $exp_from[0]=='artist_event' || $exp_from[0]=='artist_project' || $exp_from[0]=='community_project')
								{
									$sql_get_tables = mysql_query("SELECT * FROM $exp_from[0] WHERE id='".$exp_from[1]."' AND country_id='".$country_search."' AND state_id='".$state_search."' AND city='".$city_search."' AND del_status=0 AND active=0");
								}
								
								
								if(isset($sql_get_tables) && !empty($sql_get_tables))
								{
									if(mysql_num_rows($sql_get_tables)>0)
									{
										$sql_get_med = mysql_query("SELECT * FROM general_media WHERE id='".$row_mede['id']."' AND delete_status=0 AND media_status=0");
										$ans_media[] = mysql_fetch_assoc($sql_get_med);
									}
								}
							}
							elseif($row_mede['creator_info']!="")
							{
								$exp_from = explode('|',$row_mede['creator_info']);
								if($exp_from[0]=='general_artist')
								{
									$sql_get_tables = mysql_query("SELECT * FROM $exp_from[0] WHERE artist_id='".$exp_from[1]."' AND country_id='".$country_search."' AND state_id='".$state_search."' AND city='".$city_search."' AND status=0 AND active=0");
								}
								elseif($exp_from[0]=='general_community')
								{
									$sql_get_tables = mysql_query("SELECT * FROM $exp_from[0] WHERE community_id='".$exp_from[1]."' AND country_id='".$country_search."' AND state_id='".$state_search."' AND city='".$city_search."' AND status=0 AND active=0");
								}
								elseif($exp_from[0]=='community_event' || $exp_from[0]=='artist_event' || $exp_from[0]=='artist_project' || $exp_from[0]=='community_project')
								{
									$sql_get_tables = mysql_query("SELECT * FROM $exp_from[0] WHERE id='".$exp_from[1]."' AND country_id='".$country_search."' AND state_id='".$state_search."' AND city='".$city_search."' AND del_status=0 AND active=0");
								}
								
								if(isset($sql_get_tables) && !empty($sql_get_tables))
								{
									if(mysql_num_rows($sql_get_tables)>0)
									{
										$sql_get_med = mysql_query("SELECT * FROM general_media WHERE id='".$row_mede['id']."' AND delete_status=0 AND media_status=0");
										$ans_media[] = mysql_fetch_assoc($sql_get_med);
									}
								}
							}
						}
					}
				}
				
				
				if(mysql_num_rows($sql_art)>0)
				{
					while($row_artc = mysql_fetch_assoc($sql_art))
					{
						$ans_artc[] = $row_artc;
					}
				}
				
				if(mysql_num_rows($sql_com)>0)
				{
					while($row_comc = mysql_fetch_assoc($sql_com))
					{
						$ans_comc[] = $row_comc;
					}
				}
				
				if(mysql_num_rows($sql_artp)>0)
				{
					while($rowartpc = mysql_fetch_assoc($sql_artp))
					{
						$rowartpc['table_name'] = 'artist_project';
						$ans_artpc[] = $rowartpc;
					}
				}
				
				if(mysql_num_rows($sql_comp)>0)
				{
					while($rowcompc = mysql_fetch_assoc($sql_comp))
					{
						$rowcompc['table_name'] = 'community_project';
						$ans_compc[] = $rowcompc;
					}
				}
				
				if(mysql_num_rows($sql_arte)>0)
				{
					while($rowartec = mysql_fetch_assoc($sql_arte))
					{
						$rowartec['table_name'] = 'artist_event';
						$ans_artea[] = $rowartec;
					}
				}
				
				if(mysql_num_rows($sql_come)>0)
				{
					while($rowcomec = mysql_fetch_assoc($sql_come))
					{
						$rowcomec['table_name'] = 'commuity_event';
						$ans_comec[] = $rowcomec;
					}
				}
				
				$main_ansc = array_merge($ans_artc,$ans_comc,$ans_artpc,$ans_compc,$ans_artea,$ans_comec,$ans_media,$ans_reg_video_art,$ans_reg_audio_art,$ans_reg_gallery_art,$ans_reg_video_com,$ans_reg_audio_com,$ans_reg_gallery_com);
				return $main_ansc;
			}
			
			//**///////**////////////////Profile Search with Country and Page type and City Search//////////////////////////
			if($profile_type_search!="select" && $match_string=="Search" && ($country_search!=0 || $page_type_search!=0 || $city_search!="") && $alpha=="")
			{
				// echo "f7";
				// $re = "f7";
				// return $re;
				if($country_search!=0 && $page_type_search==0)
				{
					if($state_search!=0 && $city_search!="")
					{
						if($profile_type_search=='artist' || $profile_type_search=='community')
						{
							$sql = mysql_query("SELECT * FROM general_$profile_type_search WHERE country_id='".$country_search."' AND state_id='".$state_search."' AND city='".$city_search."' AND status=0 AND active=0 AND image_name!='' AND profile_url!=''");
							$profile = array();
							if(mysql_num_rows($sql)>0)
							{
								while($row_profile = mysql_fetch_assoc($sql))
								{
									$profile[] = $row_profile;
								}
							}
							return $profile;
						}
						elseif($profile_type_search=='project')
						{
							$profile_type_search_art = 'artist_project';
							$profile_type_search_com = 'community_project';
							$sql_profile_art = "SELECT * FROM $profile_type_search_art WHERE country_id='".$country_search."' AND state_id='".$state_search."' AND city='".$city_search."' AND del_status=0 AND active=0 AND image_name!='' AND profile_url!=''";
							$sql_profile_com = "SELECT * FROM $profile_type_search_com WHERE country_id='".$country_search."' AND state_id='".$state_search."' AND city='".$city_search."' AND del_status=0 AND active=0 AND image_name!='' AND profile_url!=''";
							$run_profile_art = mysql_query($sql_profile_art);
							$run_profile_com = mysql_query($sql_profile_com);
							$ans_profile_art = array();
							$ans_profile_com = array();
							
							if(mysql_num_rows($run_profile_art)>0)
							{
								while($row_art = mysql_fetch_assoc($run_profile_art))
								{
									$row_art['table_name'] = 'artist_project';
									$ans_profile_art[] = $row_art;
								}
							}
							
							if(mysql_num_rows($run_profile_com)>0)
							{
								while($row_com = mysql_fetch_assoc($run_profile_com))
								{
									$row_com['table_name'] = 'community_project';
									$ans_profile_com[] = $row_com;
								}
							}
							
							$main_ans = array_merge($ans_profile_art,$ans_profile_com);
							return $main_ans;
						}
						elseif($profile_type_search=='event')
						{
							$profile_type_search_art = 'artist_event';
							$profile_type_search_com = 'community_event';
							$sql_profile_art = "SELECT * FROM $profile_type_search_art WHERE country_id='".$country_search."' AND state_id='".$state_search."' AND city='".$city_search."' AND image_name!='' AND profile_url!=''";
							$sql_profile_com = "SELECT * FROM $profile_type_search_com WHERE country_id='".$country_search."' AND state_id='".$state_search."' AND city='".$city_search."' AND image_name!='' AND profile_url!=''";
							$run_profile_art = mysql_query($sql_profile_art);
							$run_profile_com = mysql_query($sql_profile_com);
							$ans_profile_art = array();
							$ans_profile_com = array();
							
							if(mysql_num_rows($run_profile_art)>0)
							{
								while($row_art = mysql_fetch_assoc($run_profile_art))
								{
									$row_art['table_name'] = 'artist_event';
									$ans_profile_art[] = $row_art;
								}
							}
							
							if(mysql_num_rows($run_profile_com)>0)
							{
								while($row_com = mysql_fetch_assoc($run_profile_com))
								{
									$row_com['table_name'] = 'community_event';
									$ans_profile_com[] = $row_com;
								}
							}
							
							$main_ans = array_merge($ans_profile_art,$ans_profile_com);
							return $main_ans;
						}
						elseif($profile_type_search=='media')
						{
							$ans_media = array();
							$sql_med = mysql_query("SELECT * FROM general_media WHERE delete_status=0 AND media_status=0");
							if(mysql_num_rows($sql_med)>0)
							{
								while($row_mede = mysql_fetch_assoc($sql_med))
								{
									if($row_mede['from_info']!="")
									{
										$exp_from = explode('|',$row_mede['from_info']);
										if($exp_from[0]=='general_artist')
										{
											$sql_get_tables = mysql_query("SELECT * FROM $exp_from[0] WHERE artist_id='".$exp_from[1]."' AND country_id='".$country_search."' AND state_id='".$state_search."' AND city='".$city_search."' AND status=0 AND active=0");
										}
										elseif($exp_from[0]=='general_community')
										{
											$sql_get_tables = mysql_query("SELECT * FROM $exp_from[0] WHERE community_id='".$exp_from[1]."' AND country_id='".$country_search."' AND state_id='".$state_search."' AND city='".$city_search."' AND status=0 AND active=0");
										}
										elseif($exp_from[0]=='community_event' || $exp_from[0]=='artist_event' || $exp_from[0]=='artist_project' || $exp_from[0]=='community_project')
										{
											$sql_get_tables = mysql_query("SELECT * FROM $exp_from[0] WHERE id='".$exp_from[1]."' AND country_id='".$country_search."' AND state_id='".$state_search."' AND city='".$city_search."' AND del_status=0 AND active=0");
										}
										
										if(isset($sql_get_tables) && !empty($sql_get_tables))
										{
											if(mysql_num_rows($sql_get_tables)>0)
											{
												$sql_get_med = mysql_query("SELECT * FROM general_media WHERE id='".$row_mede['id']."' AND delete_status=0 AND media_status=0");
												$ans_media[] = mysql_fetch_assoc($sql_get_med);
											}
										}
									}
									elseif($row_mede['creator_info']!="")
									{
										$exp_from = explode('|',$row_mede['creator_info']);
										if($exp_from[0]=='general_artist')
										{
											$sql_get_tables = mysql_query("SELECT * FROM $exp_from[0] WHERE artist_id='".$exp_from[1]."' AND country_id='".$country_search."' AND state_id='".$state_search."' AND city='".$city_search."' AND status=0 AND active=0");
										}
										elseif($exp_from[0]=='general_community')
										{
											$sql_get_tables = mysql_query("SELECT * FROM $exp_from[0] WHERE community_id='".$exp_from[1]."' AND country_id='".$country_search."' AND state_id='".$state_search."' AND city='".$city_search."' AND status=0 AND active=0");
										}
										elseif($exp_from[0]=='community_event' || $exp_from[0]=='artist_event' || $exp_from[0]=='artist_project' || $exp_from[0]=='community_project')
										{
											$sql_get_tables = mysql_query("SELECT * FROM $exp_from[0] WHERE id='".$exp_from[1]."' AND country_id='".$country_search."' AND state_id='".$state_search."' AND city='".$city_search."' AND del_status=0 AND active=0");
										}
										
										if(isset($sql_get_tables) && !empty($sql_get_tables))
										{
											if(mysql_num_rows($sql_get_tables)>0)
											{
												$sql_get_med = mysql_query("SELECT * FROM general_media WHERE id='".$row_mede['id']."' AND delete_status=0 AND media_status=0");
												$ans_media[] = mysql_fetch_assoc($sql_get_med);
											}
										}
									}
								}
							}
							return $ans_media;
						}
					}
					elseif($state_search==0 && $city_search!="")
					{
						if($profile_type_search=='artist' || $profile_type_search=='community')
						{
							//$sql1 = "SELECT * FROM general_$profile_type_search WHERE country_id='".$country_search."'";
							$sql = mysql_query("SELECT * FROM general_$profile_type_search WHERE country_id='".$country_search."' AND city='".$city_search."' AND status=0 AND active=0 AND image_name!='' AND profile_url!=''");
							$profile = array();
							if(mysql_num_rows($sql)>0)
							{
								while($row_profile = mysql_fetch_assoc($sql))
								{
									$profile[] = $row_profile;
								}
							}
							return $profile;
						}
						elseif($profile_type_search=='project')
						{
							$profile_type_search_art = 'artist_project';
							$profile_type_search_com = 'community_project';
							$sql_profile_art = "SELECT * FROM $profile_type_search_art WHERE country_id='".$country_search."' AND city='".$city_search."' AND del_status=0 AND active=0 AND image_name!='' AND profile_url!=''";
							$sql_profile_com = "SELECT * FROM $profile_type_search_com WHERE country_id='".$country_search."' AND city='".$city_search."' AND del_status=0 AND active=0 AND image_name!='' AND profile_url!=''";
							$run_profile_art = mysql_query($sql_profile_art);
							$run_profile_com = mysql_query($sql_profile_com);
							$ans_profile_art = array();
							$ans_profile_com = array();
							
							if(mysql_num_rows($run_profile_art)>0)
							{
								while($row_art = mysql_fetch_assoc($run_profile_art))
								{
									$row_art['table_name'] = 'artist_project';
									$ans_profile_art[] = $row_art;
								}
							}
							
							if(mysql_num_rows($run_profile_com)>0)
							{
								while($row_com = mysql_fetch_assoc($run_profile_com))
								{
									$row_com['table_name'] = 'community_project';
									$ans_profile_com[] = $row_com;
								}
							}
							
							$main_ans = array_merge($ans_profile_art,$ans_profile_com);
							return $main_ans;
						}
						elseif($profile_type_search=='event')
						{
							$profile_type_search_art = 'artist_event';
							$profile_type_search_com = 'community_event';
							$sql_profile_art = "SELECT * FROM $profile_type_search_art WHERE country_id='".$country_search."' AND city='".$city_search."' AND image_name!='' AND profile_url!=''";
							$sql_profile_com = "SELECT * FROM $profile_type_search_com WHERE country_id='".$country_search."' AND city='".$city_search."' AND image_name!='' AND profile_url!=''";
							$run_profile_art = mysql_query($sql_profile_art);
							$run_profile_com = mysql_query($sql_profile_com);
							$ans_profile_art = array();
							$ans_profile_com = array();
							
							if(mysql_num_rows($run_profile_art)>0)
							{
								while($row_art = mysql_fetch_assoc($run_profile_art))
								{
									$row_art['table_name'] = 'artist_event';
									$ans_profile_art[] = $row_art;
								}
							}
							
							if(mysql_num_rows($run_profile_com)>0)
							{
								while($row_com = mysql_fetch_assoc($run_profile_com))
								{
									$row_com['table_name'] = 'community_event';
									$ans_profile_com[] = $row_com;
								}
							}
							
							$main_ans = array_merge($ans_profile_art,$ans_profile_com);
							return $main_ans;
						}
						elseif($profile_type_search=='media')
						{
							$ans_media = array();
							$sql_med = mysql_query("SELECT * FROM general_media WHERE delete_status=0 AND media_status=0");
							if(mysql_num_rows($sql_med)>0)
							{
								while($row_mede = mysql_fetch_assoc($sql_med))
								{
									if($row_mede['from_info']!="")
									{
										$exp_from = explode('|',$row_mede['from_info']);
										if($exp_from[0]=='general_artist')
										{
											$sql_get_tables = mysql_query("SELECT * FROM $exp_from[0] WHERE artist_id='".$exp_from[1]."' AND country_id='".$country_search."' AND city='".$city_search."' AND status=0 AND active=0");
										}
										elseif($exp_from[0]=='general_community')
										{
											$sql_get_tables = mysql_query("SELECT * FROM $exp_from[0] WHERE community_id='".$exp_from[1]."' AND country_id='".$country_search."' AND city='".$city_search."' AND status=0 AND active=0");
										}
										elseif($exp_from[0]=='community_event' || $exp_from[0]=='artist_event' || $exp_from[0]=='artist_project' || $exp_from[0]=='community_project')
										{
											$sql_get_tables = mysql_query("SELECT * FROM $exp_from[0] WHERE id='".$exp_from[1]."' AND country_id='".$country_search."' AND city='".$city_search."' AND del_status=0 AND active=0");
										}
										
										
										if(isset($sql_get_tables) && !empty($sql_get_tables))
										{
											if(mysql_num_rows($sql_get_tables)>0)
											{
												$sql_get_med = mysql_query("SELECT * FROM general_media WHERE id='".$row_mede['id']."' AND delete_status=0 AND media_status=0");
												$ans_media[] = mysql_fetch_assoc($sql_get_med);
											}
										}
									}
									elseif($row_mede['creator_info']!="")
									{
										$exp_from = explode('|',$row_mede['creator_info']);
										if($exp_from[0]=='general_artist')
										{
											$sql_get_tables = mysql_query("SELECT * FROM $exp_from[0] WHERE artist_id='".$exp_from[1]."' AND country_id='".$country_search."' AND city='".$city_search."' AND status=0 AND active=0");
										}
										elseif($exp_from[0]=='general_community')
										{
											$sql_get_tables = mysql_query("SELECT * FROM $exp_from[0] WHERE community_id='".$exp_from[1]."' AND country_id='".$country_search."' AND city='".$city_search."' AND status=0 AND active=0");
										}
										elseif($exp_from[0]=='community_event' || $exp_from[0]=='artist_event' || $exp_from[0]=='artist_project' || $exp_from[0]=='community_project')
										{
											$sql_get_tables = mysql_query("SELECT * FROM $exp_from[0] WHERE id='".$exp_from[1]."' AND country_id='".$country_search."' AND city='".$city_search."' AND del_status=0 AND active=0");
										}
										
										if(isset($sql_get_tables) && !empty($sql_get_tables))
										{
											if(mysql_num_rows($sql_get_tables)>0)
											{
												$sql_get_med = mysql_query("SELECT * FROM general_media WHERE id='".$row_mede['id']."' AND delete_status=0 AND media_status=0");
												$ans_media[] = mysql_fetch_assoc($sql_get_med);
											}
										}
									}
								}
							}
							return $ans_media;
						}
					}
					elseif($state_search!=0 && $city_search=="")
					{
						if($profile_type_search=='artist' || $profile_type_search=='community')
						{
							//$sql1 = "SELECT * FROM general_$profile_type_search WHERE country_id='".$country_search."'";
							$sql = mysql_query("SELECT * FROM general_$profile_type_search WHERE country_id='".$country_search."' AND state_id='".$state_search."' AND status=0 AND active=0 AND image_name!='' AND profile_url!=''");
							$profile = array();
							if(mysql_num_rows($sql)>0)
							{
								while($row_profile = mysql_fetch_assoc($sql))
								{
									$profile[] = $row_profile;
								}
							}
							return $profile;
						}
						elseif($profile_type_search=='project')
						{
							$profile_type_search_art = 'artist_project';
							$profile_type_search_com = 'community_project';
							$sql_profile_art = "SELECT * FROM $profile_type_search_art WHERE country_id='".$country_search."' AND state_id='".$state_search."' AND del_status=0 AND active=0 AND image_name!='' AND profile_url!=''";
							$sql_profile_com = "SELECT * FROM $profile_type_search_com WHERE country_id='".$country_search."' AND state_id='".$state_search."' AND del_status=0 AND active=0 AND image_name!='' AND profile_url!=''";
							$run_profile_art = mysql_query($sql_profile_art);
							$run_profile_com = mysql_query($sql_profile_com);
							$ans_profile_art = array();
							$ans_profile_com = array();
							
							if(mysql_num_rows($run_profile_art)>0)
							{
								while($row_art = mysql_fetch_assoc($run_profile_art))
								{
									$row_art['table_name'] = 'artist_project';
									$ans_profile_art[] = $row_art;
								}
							}
							
							if(mysql_num_rows($run_profile_com)>0)
							{
								while($row_com = mysql_fetch_assoc($run_profile_com))
								{
									$row_com['table_name'] = 'community_project';
									$ans_profile_com[] = $row_com;
								}
							}
							
							$main_ans = array_merge($ans_profile_art,$ans_profile_com);
							return $main_ans;
						}
						elseif($profile_type_search=='event')
						{
							$profile_type_search_art = 'artist_event';
							$profile_type_search_com = 'community_event';
							$sql_profile_art = "SELECT * FROM $profile_type_search_art WHERE country_id='".$country_search."' AND state_id='".$state_search."' AND image_name!='' AND profile_url!=''";
							$sql_profile_com = "SELECT * FROM $profile_type_search_com WHERE country_id='".$country_search."' AND state_id='".$state_search."' AND image_name!='' AND profile_url!=''";
							$run_profile_art = mysql_query($sql_profile_art);
							$run_profile_com = mysql_query($sql_profile_com);
							$ans_profile_art = array();
							$ans_profile_com = array();
							
							if(mysql_num_rows($run_profile_art)>0)
							{
								while($row_art = mysql_fetch_assoc($run_profile_art))
								{
									$row_art['table_name'] = 'artist_event';
									$ans_profile_art[] = $row_art;
								}
							}
							
							if(mysql_num_rows($run_profile_com)>0)
							{
								while($row_com = mysql_fetch_assoc($run_profile_com))
								{
									$row_com['table_name'] = 'community_event';
									$ans_profile_com[] = $row_com;
								}
							}
							
							$main_ans = array_merge($ans_profile_art,$ans_profile_com);
							return $main_ans;
						}
						elseif($profile_type_search=='media')
						{
							$ans_media = array();
							$sql_med = mysql_query("SELECT * FROM general_media WHERE delete_status=0 AND media_status=0");
							if(mysql_num_rows($sql_med)>0)
							{
								while($row_mede = mysql_fetch_assoc($sql_med))
								{
									if($row_mede['from_info']!="")
									{
										$exp_from = explode('|',$row_mede['from_info']);
										if($exp_from[0]=='general_artist')
										{
											$sql_get_tables = mysql_query("SELECT * FROM $exp_from[0] WHERE artist_id='".$exp_from[1]."' AND country_id='".$country_search."' AND state_id='".$state_search."' AND status=0 AND active=0");
										}
										elseif($exp_from[0]=='general_community')
										{
											$sql_get_tables = mysql_query("SELECT * FROM $exp_from[0] WHERE community_id='".$exp_from[1]."' AND country_id='".$country_search."' AND state_id='".$state_search."' AND status=0 AND active=0");
										}
										elseif($exp_from[0]=='community_event' || $exp_from[0]=='artist_event' || $exp_from[0]=='artist_project' || $exp_from[0]=='community_project')
										{
											$sql_get_tables = mysql_query("SELECT * FROM $exp_from[0] WHERE id='".$exp_from[1]."' AND country_id='".$country_search."' AND state_id='".$state_search."' AND del_status=0 AND active=0");
										}
										
										if(isset($sql_get_tables) && !empty($sql_get_tables))
										{
											if(mysql_num_rows($sql_get_tables)>0)
											{
												$sql_get_med = mysql_query("SELECT * FROM general_media WHERE id='".$row_mede['id']."' AND delete_status=0 AND media_status=0");
												$ans_media[] = mysql_fetch_assoc($sql_get_med);
											}
										}
									}
									elseif($row_mede['creator_info']!="")
									{
										$exp_from = explode('|',$row_mede['creator_info']);
										if($exp_from[0]=='general_artist')
										{
											$sql_get_tables = mysql_query("SELECT * FROM $exp_from[0] WHERE artist_id='".$exp_from[1]."' AND country_id='".$country_search."' AND state_id='".$state_search."' AND status=0 AND active=0");
										}
										elseif($exp_from[0]=='general_community')
										{
											$sql_get_tables = mysql_query("SELECT * FROM $exp_from[0] WHERE community_id='".$exp_from[1]."' AND country_id='".$country_search."' AND state_id='".$state_search."' AND status=0 AND active=0");
										}
										elseif($exp_from[0]=='community_event' || $exp_from[0]=='artist_event' || $exp_from[0]=='artist_project' || $exp_from[0]=='community_project')
										{
											$sql_get_tables = mysql_query("SELECT * FROM $exp_from[0] WHERE id='".$exp_from[1]."' AND country_id='".$country_search."' AND state_id='".$state_search."' AND del_status=0 AND active=0");
										}
										
										if(isset($sql_get_tables) && !empty($sql_get_tables))
										{
											if(mysql_num_rows($sql_get_tables)>0)
											{
												$sql_get_med = mysql_query("SELECT * FROM general_media WHERE id='".$row_mede['id']."' AND delete_status=0 AND media_status=0");
												$ans_media[] = mysql_fetch_assoc($sql_get_med);
											}
										}
									}
								}
							}
							return $ans_media;
						}
					}
					else
					{
						if($profile_type_search=='artist' || $profile_type_search=='community')
						{
							//$sql1 = "SELECT * FROM general_$profile_type_search WHERE country_id='".$country_search."'";
							$sql = mysql_query("SELECT * FROM general_$profile_type_search WHERE country_id='".$country_search."' AND status=0 AND active=0 AND image_name!='' AND profile_url!=''");
							$profile = array();
							if(mysql_num_rows($sql)>0)
							{
								while($row_profile = mysql_fetch_assoc($sql))
								{
									$profile[] = $row_profile;
								}
							}
							return $profile;
						}
						elseif($profile_type_search=='project')
						{
							$profile_type_search_art = 'artist_project';
							$profile_type_search_com = 'community_project';
							$sql_profile_art = "SELECT * FROM $profile_type_search_art WHERE country_id='".$country_search."' AND del_status=0 AND active=0 AND image_name!='' AND profile_url!=''";
							$sql_profile_com = "SELECT * FROM $profile_type_search_com WHERE country_id='".$country_search."' AND del_status=0 AND active=0 AND image_name!='' AND profile_url!=''";
							$run_profile_art = mysql_query($sql_profile_art);
							$run_profile_com = mysql_query($sql_profile_com);
							$ans_profile_art = array();
							$ans_profile_com = array();
							
							if(mysql_num_rows($run_profile_art)>0)
							{
								while($row_art = mysql_fetch_assoc($run_profile_art))
								{
									$row_art['table_name'] = 'artist_project';
									$ans_profile_art[] = $row_art;
								}
							}
							
							if(mysql_num_rows($run_profile_com)>0)
							{
								while($row_com = mysql_fetch_assoc($run_profile_com))
								{
									$row_com['table_name'] = 'community_project';
									$ans_profile_com[] = $row_com;
								}
							}
							
							$main_ans = array_merge($ans_profile_art,$ans_profile_com);
							return $main_ans;
						}
						elseif($profile_type_search=='event')
						{
							$profile_type_search_art = 'artist_event';
							$profile_type_search_com = 'community_event';
							$sql_profile_art = "SELECT * FROM $profile_type_search_art WHERE country_id='".$country_search."' AND image_name!='' AND profile_url!=''";
							$sql_profile_com = "SELECT * FROM $profile_type_search_com WHERE country_id='".$country_search."' AND image_name!='' AND profile_url!=''";
							$run_profile_art = mysql_query($sql_profile_art);
							$run_profile_com = mysql_query($sql_profile_com);
							$ans_profile_art = array();
							$ans_profile_com = array();
							
							if(mysql_num_rows($run_profile_art)>0)
							{
								while($row_art = mysql_fetch_assoc($run_profile_art))
								{
									$row_art['table_name'] = 'artist_event';
									$ans_profile_art[] = $row_art;
								}
							}
							
							if(mysql_num_rows($run_profile_com)>0)
							{
								while($row_com = mysql_fetch_assoc($run_profile_com))
								{
									$row_com['table_name'] = 'community_event';
									$ans_profile_com[] = $row_com;
								}
							}
							
							$main_ans = array_merge($ans_profile_art,$ans_profile_com);
							return $main_ans;
						}
						elseif($profile_type_search=='media')
						{
							$ans_media = array();
							$sql_med = mysql_query("SELECT * FROM general_media WHERE delete_status=0 AND media_status=0");
							if(mysql_num_rows($sql_med)>0)
							{
								while($row_mede = mysql_fetch_assoc($sql_med))
								{
									if($row_mede['from_info']!="")
									{
										$exp_from = explode('|',$row_mede['from_info']);
										if($exp_from[0]=='general_artist')
										{
											$sql_get_tables = mysql_query("SELECT * FROM $exp_from[0] WHERE artist_id='".$exp_from[1]."' AND country_id='".$country_search."' AND status=0 AND active=0");
										}
										elseif($exp_from[0]=='general_community')
										{
											$sql_get_tables = mysql_query("SELECT * FROM $exp_from[0] WHERE community_id='".$exp_from[1]."' AND country_id='".$country_search."' AND status=0 AND active=0");
										}
										elseif($exp_from[0]=='community_event' || $exp_from[0]=='artist_event' || $exp_from[0]=='artist_project' || $exp_from[0]=='community_project')
										{
											$sql_get_tables = mysql_query("SELECT * FROM $exp_from[0] WHERE id='".$exp_from[1]."' AND country_id='".$country_search."' AND del_status=0 AND active=0");
										}
										
										if(isset($sql_get_tables) && !empty($sql_get_tables))
										{
											if(mysql_num_rows($sql_get_tables)>0)
											{
												$sql_get_med = mysql_query("SELECT * FROM general_media WHERE id='".$row_mede['id']."' AND delete_status=0 AND media_status=0");
												$ans_media[] = mysql_fetch_assoc($sql_get_med);
											}
										}
									}
									elseif($row_mede['creator_info']!="")
									{
										$exp_from = explode('|',$row_mede['creator_info']);
										if($exp_from[0]=='general_artist')
										{
											$sql_get_tables = mysql_query("SELECT * FROM $exp_from[0] WHERE artist_id='".$exp_from[1]."' AND country_id='".$country_search."' AND status=0 AND active=0");
										}
										elseif($exp_from[0]=='general_community')
										{
											$sql_get_tables = mysql_query("SELECT * FROM $exp_from[0] WHERE community_id='".$exp_from[1]."' AND country_id='".$country_search."' AND status=0 AND active=0");
										}
										elseif($exp_from[0]=='community_event' || $exp_from[0]=='artist_event' || $exp_from[0]=='artist_project' || $exp_from[0]=='community_project')
										{
											$sql_get_tables = mysql_query("SELECT * FROM $exp_from[0] WHERE id='".$exp_from[1]."' AND country_id='".$country_search."' AND del_status=0 AND active=0");
										}
										
										if(isset($sql_get_tables) && !empty($sql_get_tables))
										{
											if(mysql_num_rows($sql_get_tables)>0)
											{
												$sql_get_med = mysql_query("SELECT * FROM general_media WHERE id='".$row_mede['id']."'");
												$ans_media[] = mysql_fetch_assoc($sql_get_med);
											}
										}
									}
								}
							}
							return $ans_media;
						}
					}
				}
				elseif($country_search==0 && $page_type_search!=0 && $city_search=="")
				{
					if($sub_type_search!=0 && $meta_type_search!=0)
					{
						$ans_art_org = array();
						$ans_com_org = array();
						$ans_arte_org = array();
						$ans_artp_org = array();
						$ans_come_org = array();
						$ans_comp_org = array();
						$ans_med_org = array();
							
						if($profile_type_search=='artist' || $profile_type_search=='community')
						{
							if($profile_type_search=='artist')
							{
								$sql_all_art = mysql_query("SELECT * FROM general_artist WHERE status=0 AND active=0 AND image_name!='' AND profile_url!=''");
								if(mysql_num_rows($sql_all_art)>0)
								{
									//$all_art_ans = array();
									$i = 0;
									while($all_art_ans = mysql_fetch_assoc($sql_all_art))
									{
										//$sql_page_type_1 = "SELECT MAX( general_artist_profiles_id ) FROM general_artist_profiles WHERE type_id ='".$page_type_search."' AND artist_id='".$all_art_ans['artist_id']."'";
										
										$sql_page_typea = mysql_query("SELECT MAX( general_artist_profiles_id ) FROM general_artist_profiles WHERE type_id ='".$page_type_search."' AND subtype_id='".$sub_type_search."' AND metatype_id='".$meta_type_search."' AND artist_id='".$all_art_ans['artist_id']."'");
										$run_page_typea = mysql_fetch_assoc($sql_page_typea);
										if($run_page_typea['MAX( general_artist_profiles_id )']!=NULL)
										{
											//echo "hii";
											$sql_artt = mysql_query("SELECT * FROM general_artist WHERE artist_id='".$all_art_ans['artist_id']."'");
											
											while($row_art = mysql_fetch_assoc($sql_artt))
											{
												$ans_art_org[$i] = $row_art;
											}
											$i = $i + 1;
										}
									}
								}
							}
							
							if($profile_type_search=='community')
							{
								$sql_all_com = mysql_query("SELECT * FROM general_community WHERE status=0 AND active=0 AND image_name!='' AND profile_url!=''");
								if(mysql_num_rows($sql_all_com)>0)
								{
									//$all_art_ans = array();
									$j = 0;
									while($all_com_ans = mysql_fetch_assoc($sql_all_com))
									{
										//$sql_page_typec_1 = "SELECT MAX( general_community_profiles_id ) FROM general_community_profiles WHERE type_id ='".$page_type_search."' AND community_id='".$all_com_ans['community_id']."'";
										
										$sql_page_typec = mysql_query("SELECT MAX( general_community_profiles_id ) FROM general_community_profiles WHERE type_id ='".$page_type_search."' AND subtype_id='".$sub_type_search."' AND metatype_id='".$meta_type_search."' AND community_id='".$all_com_ans['community_id']."'");
										$run_page_typec = mysql_fetch_assoc($sql_page_typec);
										
										if($run_page_typec['MAX( general_community_profiles_id )']!=NULL)
										{
											//echo "hii";
											$sql_comt = mysql_query("SELECT * FROM general_community WHERE community_id='".$all_com_ans['community_id']."'");
											
											while($row_comt = mysql_fetch_assoc($sql_comt))
											{
												$ans_com_org[$j] = $row_comt;
											}
											$j = $j + 1;
										}
									}
								}
							}
						}
						
						if($profile_type_search=='event')
						{
							$sql_all_ae = mysql_query("SELECT * FROM artist_event WHERE del_status=0 AND active=0 AND image_name!='' AND profile_url!=''");
							if(mysql_num_rows($sql_all_ae)>0)
							{
								//$all_art_ans = array();
								$k = 0;
								while($all_ae_ans = mysql_fetch_assoc($sql_all_ae))
								{
									//$sql_page_typec_1 = "SELECT MAX( general_community_profiles_id ) FROM general_community_profiles WHERE type_id ='".$page_type_search."' AND community_id='".$all_com_ans['community_id']."'";
									
									$sql_page_typeae = mysql_query("SELECT MAX( id ) FROM artist_event_profiles WHERE type_id ='".$page_type_search."' AND subtype_id='".$sub_type_search."' AND metatype_id='".$meta_type_search."' AND artist_event_id='".$all_ae_ans['id']."'");
									$run_page_typeae = mysql_fetch_assoc($sql_page_typeae);
									
									if($run_page_typeae['MAX( id )']!=NULL)
									{
										//echo "hii";
										$sql_aet = mysql_query("SELECT * FROM artist_event WHERE id='".$all_ae_ans['id']."'");
										
										while($row_aet = mysql_fetch_assoc($sql_aet))
										{
											$row_aet['table_name'] = 'artist_event';
											$ans_arte_org[$k] = $row_aet;
										}
										$k = $k + 1;
									}
								}
							}
							
							$sql_all_ce = mysql_query("SELECT * FROM community_event WHERE del_status=0 AND active=0 AND image_name!='' AND profile_url!=''");
							if(mysql_num_rows($sql_all_ce)>0)
							{
								//$all_art_ans = array();
								$l = 0;
								while($all_ce_ans = mysql_fetch_assoc($sql_all_ce))
								{
									//$sql_page_typec_1 = "SELECT MAX( general_community_profiles_id ) FROM general_community_profiles WHERE type_id ='".$page_type_search."' AND community_id='".$all_com_ans['community_id']."'";
									
									$sql_page_typece = mysql_query("SELECT MAX( id ) FROM community_event_profiles WHERE type_id ='".$page_type_search."' AND subtype_id='".$sub_type_search."' AND metatype_id='".$meta_type_search."' AND community_event_id='".$all_ce_ans['id']."'");
									$run_page_typece = mysql_fetch_assoc($sql_page_typece);
									
									if($run_page_typece['MAX( id )']!=NULL)
									{
										//echo "hii";
										$sql_cet = mysql_query("SELECT * FROM community_event WHERE id='".$all_ce_ans['id']."'");
										
										while($row_cet = mysql_fetch_assoc($sql_cet))
										{
											$row_cet['table_name'] = 'community_event';
											$ans_come_org[$l] = $row_cet;
										}
										$l = $l + 1;
									}
								}
							}
						}
						
						if($profile_type_search=='project')
						{
							$sql_all_ap = mysql_query("SELECT * FROM artist_project WHERE del_status=0 AND active=0 AND image_name!='' AND profile_url!=''");
							if(mysql_num_rows($sql_all_ap)>0)
							{
								//$all_art_ans = array();
								$m = 0;
								while($all_ap_ans = mysql_fetch_assoc($sql_all_ap))
								{
									//$sql_page_typec_1 = "SELECT MAX( general_community_profiles_id ) FROM general_community_profiles WHERE type_id ='".$page_type_search."' AND community_id='".$all_com_ans['community_id']."'";
									
									$sql_page_typeap = mysql_query("SELECT MAX( id ) FROM artist_project_profiles WHERE type_id ='".$page_type_search."' AND subtype_id='".$sub_type_search."' AND metatype_id='".$meta_type_search."' AND artist_project_id='".$all_ap_ans['id']."'");
									$run_page_typeap = mysql_fetch_assoc($sql_page_typeap);
									
									if($run_page_typeap['MAX( id )']!=NULL)
									{
										//echo "hii";
										$sql_apt = mysql_query("SELECT * FROM artist_project WHERE id='".$all_ap_ans['id']."'");
										
										while($row_apt = mysql_fetch_assoc($sql_apt))
										{
											$row_apt['table_name'] = 'artist_project';
											$ans_artp_org[$m] = $row_apt;
										}
										$m = $m + 1;
									}
								}
							}
							
							$sql_all_cp = mysql_query("SELECT * FROM community_project WHERE del_status=0 AND active=0 AND image_name!='' AND profile_url!=''");
							if(mysql_num_rows($sql_all_cp)>0)
							{
								//$all_art_ans = array();
								$n = 0;
								while($all_cp_ans = mysql_fetch_assoc($sql_all_cp))
								{
									//$sql_page_typec_1 = "SELECT MAX( general_community_profiles_id ) FROM general_community_profiles WHERE type_id ='".$page_type_search."' AND community_id='".$all_com_ans['community_id']."'";
									
									$sql_page_typecp = mysql_query("SELECT MAX( id ) FROM community_project_profiles WHERE type_id ='".$page_type_search."' AND subtype_id='".$sub_type_search."' AND metatype_id='".$meta_type_search."' AND community_project_id='".$all_cp_ans['id']."'");
									$run_page_typecp = mysql_fetch_assoc($sql_page_typecp);
									
									if($run_page_typecp['MAX( id )']!=NULL)
									{
										//echo "hii";
										$sql_cpt = mysql_query("SELECT * FROM community_project WHERE id='".$all_cp_ans['id']."'");
										
										while($row_cpt = mysql_fetch_assoc($sql_cpt))
										{
											$row_cpt['table_name'] = 'community_project';
											$ans_comp_org[$n] = $row_cpt;
										}
										$n = $n + 1;
									}
								}
							}
						}
						
						if($profile_type_search=='media')
						{
							$sql_all_me = mysql_query("SELECT * FROM general_media WHERE delete_status=0 AND media_status=0");
							if(mysql_num_rows($sql_all_me)>0)
							{
								//$all_art_ans = array();
								$mj = 0;
								while($all_me_ans = mysql_fetch_assoc($sql_all_me))
								{
									//$sql_page_typec_1 = "SELECT MAX( general_community_profiles_id ) FROM general_community_profiles WHERE type_id ='".$page_type_search."' AND community_id='".$all_com_ans['community_id']."'";
									
									$sql_page_typeme = mysql_query("SELECT MAX( id ) FROM media_profiles_type WHERE type_id ='".$page_type_search."' AND subtype_id='".$sub_type_search."' AND metatype_id='".$meta_type_search."' AND media_id='".$all_me_ans['id']."'");
									$run_page_typeme = mysql_fetch_assoc($sql_page_typeme);
									
									if($run_page_typeme['MAX( id )']!=NULL)
									{
										//echo "hii";
										$sql_met = mysql_query("SELECT * FROM general_media WHERE id='".$all_me_ans['id']."'");
										
										while($row_mnet = mysql_fetch_assoc($sql_met))
										{
											$ans_med_org[$mj] = $row_mnet;
										}
										$mj = $mj + 1;
									}
								}
							}
						}
						$main_ans = array_merge($ans_art_org,$ans_com_org,$ans_arte_org,$ans_artp_org,$ans_come_org,$ans_comp_org,$ans_med_org);
						return $main_ans;
					}
					elseif($sub_type_search!=0 && $meta_type_search==0)
					{
						$ans_art_org = array();
						$ans_com_org = array();
						$ans_arte_org = array();
						$ans_artp_org = array();
						$ans_come_org = array();
						$ans_comp_org = array();
						$ans_med_org = array();
						
						if($profile_type_search=='artist' || $profile_type_search=='community')
						{
							if($profile_type_search=='artist')
							{
								$sql_all_art = mysql_query("SELECT * FROM general_artist WHERE status=0 AND active=0 AND image_name!='' AND profile_url!=''");
								if(mysql_num_rows($sql_all_art)>0)
								{
									//$all_art_ans = array();
									$i = 0;
									while($all_art_ans = mysql_fetch_assoc($sql_all_art))
									{
										//$sql_page_type_1 = "SELECT MAX( general_artist_profiles_id ) FROM general_artist_profiles WHERE type_id ='".$page_type_search."' AND artist_id='".$all_art_ans['artist_id']."'";
										
										$sql_page_typea = mysql_query("SELECT MAX( general_artist_profiles_id ) FROM general_artist_profiles WHERE type_id ='".$page_type_search."' AND subtype_id='".$sub_type_search."' AND artist_id='".$all_art_ans['artist_id']."'");
										$run_page_typea = mysql_fetch_assoc($sql_page_typea);
										if($run_page_typea['MAX( general_artist_profiles_id )']!=NULL)
										{
											//echo "hii";
											$sql_artt = mysql_query("SELECT * FROM general_artist WHERE artist_id='".$all_art_ans['artist_id']."'");
											
											while($row_art = mysql_fetch_assoc($sql_artt))
											{
												$ans_art_org[$i] = $row_art;
											}
											$i = $i + 1;
										}
									}
								}
							}
							
							if($profile_type_search=='community')
							{
								$sql_all_com = mysql_query("SELECT * FROM general_community WHERE status=0 AND active=0 AND image_name!='' AND profile_url!=''");
								if(mysql_num_rows($sql_all_com)>0)
								{
									//$all_art_ans = array();
									$j = 0;
									while($all_com_ans = mysql_fetch_assoc($sql_all_com))
									{
										//$sql_page_typec_1 = "SELECT MAX( general_community_profiles_id ) FROM general_community_profiles WHERE type_id ='".$page_type_search."' AND community_id='".$all_com_ans['community_id']."'";
										
										$sql_page_typec = mysql_query("SELECT MAX( general_community_profiles_id ) FROM general_community_profiles WHERE type_id ='".$page_type_search."' AND subtype_id='".$sub_type_search."' AND community_id='".$all_com_ans['community_id']."'");
										$run_page_typec = mysql_fetch_assoc($sql_page_typec);
										
										if($run_page_typec['MAX( general_community_profiles_id )']!=NULL)
										{
											//echo "hii";
											$sql_comt = mysql_query("SELECT * FROM general_community WHERE community_id='".$all_com_ans['community_id']."'");
											
											while($row_comt = mysql_fetch_assoc($sql_comt))
											{
												$ans_com_org[$j] = $row_comt;
											}
											$j = $j + 1;
										}
									}
								}
							}
						}
						
						if($profile_type_search=='event')
						{
							$sql_all_ae = mysql_query("SELECT * FROM artist_event WHERE del_status=0 AND active=0 AND image_name!='' AND profile_url!=''");
							if(mysql_num_rows($sql_all_ae)>0)
							{
								//$all_art_ans = array();
								$k = 0;
								while($all_ae_ans = mysql_fetch_assoc($sql_all_ae))
								{
									//$sql_page_typec_1 = "SELECT MAX( general_community_profiles_id ) FROM general_community_profiles WHERE type_id ='".$page_type_search."' AND community_id='".$all_com_ans['community_id']."'";
									
									$sql_page_typeae = mysql_query("SELECT MAX( id ) FROM artist_event_profiles WHERE type_id ='".$page_type_search."' AND subtype_id='".$sub_type_search."' AND artist_event_id='".$all_ae_ans['id']."'");
									$run_page_typeae = mysql_fetch_assoc($sql_page_typeae);
									
									if($run_page_typeae['MAX( id )']!=NULL)
									{
										//echo "hii";
										$sql_aet = mysql_query("SELECT * FROM artist_event WHERE id='".$all_ae_ans['id']."'");
										
										while($row_aet = mysql_fetch_assoc($sql_aet))
										{
											$row_aet['table_name'] = 'artist_event';
											$ans_arte_org[$k] = $row_aet;
										}
										$k = $k + 1;
									}
								}
							}
							
							$sql_all_ce = mysql_query("SELECT * FROM community_event WHERE del_status=0 AND active=0 AND image_name!='' AND profile_url!=''");
							if(mysql_num_rows($sql_all_ce)>0)
							{
								//$all_art_ans = array();
								$l = 0;
								while($all_ce_ans = mysql_fetch_assoc($sql_all_ce))
								{
									//$sql_page_typec_1 = "SELECT MAX( general_community_profiles_id ) FROM general_community_profiles WHERE type_id ='".$page_type_search."' AND community_id='".$all_com_ans['community_id']."'";
									
									$sql_page_typece = mysql_query("SELECT MAX( id ) FROM community_event_profiles WHERE type_id ='".$page_type_search."' AND subtype_id='".$sub_type_search."' AND community_event_id='".$all_ce_ans['id']."'");
									$run_page_typece = mysql_fetch_assoc($sql_page_typece);
									
									if($run_page_typece['MAX( id )']!=NULL)
									{
										//echo "hii";
										$sql_cet = mysql_query("SELECT * FROM community_event WHERE id='".$all_ce_ans['id']."'");
										
										while($row_cet = mysql_fetch_assoc($sql_cet))
										{
											$row_cet['table_name'] = 'community_event';
											$ans_come_org[$l] = $row_cet;
										}
										$l = $l + 1;
									}
								}
							}
						}
						
						if($profile_type_search=='project')
						{
							$sql_all_ap = mysql_query("SELECT * FROM artist_project WHERE del_status=0 AND active=0 AND image_name!='' AND profile_url!=''");
							if(mysql_num_rows($sql_all_ap)>0)
							{
								//$all_art_ans = array();
								$m = 0;
								while($all_ap_ans = mysql_fetch_assoc($sql_all_ap))
								{
									//$sql_page_typec_1 = "SELECT MAX( general_community_profiles_id ) FROM general_community_profiles WHERE type_id ='".$page_type_search."' AND community_id='".$all_com_ans['community_id']."'";
									
									$sql_page_typeap = mysql_query("SELECT MAX( id ) FROM artist_project_profiles WHERE type_id ='".$page_type_search."' AND subtype_id='".$sub_type_search."' AND artist_project_id='".$all_ap_ans['id']."'");
									$run_page_typeap = mysql_fetch_assoc($sql_page_typeap);
									
									if($run_page_typeap['MAX( id )']!=NULL)
									{
										//echo "hii";
										$sql_apt = mysql_query("SELECT * FROM artist_project WHERE id='".$all_ap_ans['id']."'");
										
										while($row_apt = mysql_fetch_assoc($sql_apt))
										{
											$row_apt['table_name'] = 'artist_project';
											$ans_artp_org[$m] = $row_apt;
										}
										$m = $m + 1;
									}
								}
							}
							
							$sql_all_cp = mysql_query("SELECT * FROM community_project WHERE del_status=0 AND active=0 AND image_name!='' AND profile_url!=''");
							if(mysql_num_rows($sql_all_cp)>0)
							{
								//$all_art_ans = array();
								$n = 0;
								while($all_cp_ans = mysql_fetch_assoc($sql_all_cp))
								{
									//$sql_page_typec_1 = "SELECT MAX( general_community_profiles_id ) FROM general_community_profiles WHERE type_id ='".$page_type_search."' AND community_id='".$all_com_ans['community_id']."'";
									
									$sql_page_typecp = mysql_query("SELECT MAX( id ) FROM community_project_profiles WHERE type_id ='".$page_type_search."' AND subtype_id='".$sub_type_search."' AND community_project_id='".$all_cp_ans['id']."'");
									$run_page_typecp = mysql_fetch_assoc($sql_page_typecp);
									
									if($run_page_typecp['MAX( id )']!=NULL)
									{
										//echo "hii";
										$sql_cpt = mysql_query("SELECT * FROM community_project WHERE id='".$all_cp_ans['id']."'");
										
										while($row_cpt = mysql_fetch_assoc($sql_cpt))
										{
											$row_cpt['table_name'] = 'community_project';
											$ans_comp_org[$n] = $row_cpt;
										}
										$n = $n + 1;
									}
								}
							}
						}
						
						if($profile_type_search=='media')
						{
							$sql_all_me = mysql_query("SELECT * FROM general_media WHERE delete_status=0 AND media_status=0");
							if(mysql_num_rows($sql_all_me)>0)
							{
								//$all_art_ans = array();
								$mj = 0;
								while($all_me_ans = mysql_fetch_assoc($sql_all_me))
								{
									//$sql_page_typec_1 = "SELECT MAX( general_community_profiles_id ) FROM general_community_profiles WHERE type_id ='".$page_type_search."' AND community_id='".$all_com_ans['community_id']."'";
									
									$sql_page_typeme = mysql_query("SELECT MAX( id ) FROM media_profiles_type WHERE type_id ='".$page_type_search."' AND subtype_id='".$sub_type_search."' AND media_id='".$all_me_ans['id']."'");
									$run_page_typeme = mysql_fetch_assoc($sql_page_typeme);
									
									if($run_page_typeme['MAX( id )']!=NULL)
									{
										//echo "hii";
										$sql_met = mysql_query("SELECT * FROM general_media WHERE id='".$all_me_ans['id']."'");
										
										while($row_mnet = mysql_fetch_assoc($sql_met))
										{
											$ans_med_org[$mj] = $row_mnet;
										}
										$mj = $mj + 1;
									}
								}
							}
						}
						
						$main_ans = array_merge($ans_art_org,$ans_com_org,$ans_arte_org,$ans_artp_org,$ans_come_org,$ans_comp_org,$ans_med_org);
						return $main_ans;
					}
					else
					{
						$ans_art_org = array();
						$ans_com_org = array();
						$ans_arte_org = array();
						$ans_artp_org = array();
						$ans_come_org = array();
						$ans_comp_org = array();
						$ans_med_org = array();
						
						if($profile_type_search=='artist' || $profile_type_search=='community')
						{
							if($profile_type_search=='artist')
							{
								$sql_all_art = mysql_query("SELECT * FROM general_artist WHERE status=0 AND active=0 AND image_name!='' AND profile_url!=''");
								if(mysql_num_rows($sql_all_art)>0)
								{
									//$all_art_ans = array();
									$i = 0;
									while($all_art_ans = mysql_fetch_assoc($sql_all_art))
									{
										//$sql_page_type_1 = "SELECT MAX( general_artist_profiles_id ) FROM general_artist_profiles WHERE type_id ='".$page_type_search."' AND artist_id='".$all_art_ans['artist_id']."'";
										
										$sql_page_typea = mysql_query("SELECT MAX( general_artist_profiles_id ) FROM general_artist_profiles WHERE type_id ='".$page_type_search."' AND artist_id='".$all_art_ans['artist_id']."'");
										$run_page_typea = mysql_fetch_assoc($sql_page_typea);
										if($run_page_typea['MAX( general_artist_profiles_id )']!=NULL)
										{
											//echo "hii";
											$sql_artt = mysql_query("SELECT * FROM general_artist WHERE artist_id='".$all_art_ans['artist_id']."'");
											
											while($row_art = mysql_fetch_assoc($sql_artt))
											{
												$ans_art_org[$i] = $row_art;
											}
											$i = $i + 1;
										}
									}
								}
							}
							
							if($profile_type_search=='community')
							{
								$sql_all_com = mysql_query("SELECT * FROM general_community WHERE status=0 AND active=0 AND image_name!='' AND profile_url!=''");
								if(mysql_num_rows($sql_all_com)>0)
								{
									//$all_art_ans = array();
									$j = 0;
									while($all_com_ans = mysql_fetch_assoc($sql_all_com))
									{
										//$sql_page_typec_1 = "SELECT MAX( general_community_profiles_id ) FROM general_community_profiles WHERE type_id ='".$page_type_search."' AND community_id='".$all_com_ans['community_id']."'";
										
										$sql_page_typec = mysql_query("SELECT MAX( general_community_profiles_id ) FROM general_community_profiles WHERE type_id ='".$page_type_search."' AND community_id='".$all_com_ans['community_id']."'");
										$run_page_typec = mysql_fetch_assoc($sql_page_typec);
										
										if($run_page_typec['MAX( general_community_profiles_id )']!=NULL)
										{
											//echo "hii";
											$sql_comt = mysql_query("SELECT * FROM general_community WHERE community_id='".$all_com_ans['community_id']."'");
											
											while($row_comt = mysql_fetch_assoc($sql_comt))
											{
												$ans_com_org[$j] = $row_comt;
											}
											$j = $j + 1;
										}
									}
								}
							}
						}
						
						if($profile_type_search=='event')
						{
							$sql_all_ae = mysql_query("SELECT * FROM artist_event WHERE del_status=0 AND active=0 AND image_name!='' AND profile_url!=''");
							if(mysql_num_rows($sql_all_ae)>0)
							{
								//$all_art_ans = array();
								$k = 0;
								while($all_ae_ans = mysql_fetch_assoc($sql_all_ae))
								{
									//$sql_page_typec_1 = "SELECT MAX( general_community_profiles_id ) FROM general_community_profiles WHERE type_id ='".$page_type_search."' AND community_id='".$all_com_ans['community_id']."'";
									
									$sql_page_typeae = mysql_query("SELECT MAX( id ) FROM artist_event_profiles WHERE type_id ='".$page_type_search."' AND artist_event_id='".$all_ae_ans['id']."'");
									$run_page_typeae = mysql_fetch_assoc($sql_page_typeae);
									
									if($run_page_typeae['MAX( id )']!=NULL)
									{
										//echo "hii";
										$sql_aet = mysql_query("SELECT * FROM artist_event WHERE id='".$all_ae_ans['id']."'");
										
										while($row_aet = mysql_fetch_assoc($sql_aet))
										{
											$row_aet['table_name'] = 'artist_event';
											$ans_arte_org[$k] = $row_aet;
										}
										$k = $k + 1;
									}
								}
							}
							
							$sql_all_ce = mysql_query("SELECT * FROM community_event WHERE del_status=0 AND active=0 AND image_name!='' AND profile_url!=''");
							if(mysql_num_rows($sql_all_ce)>0)
							{
								//$all_art_ans = array();
								$l = 0;
								while($all_ce_ans = mysql_fetch_assoc($sql_all_ce))
								{
									//$sql_page_typec_1 = "SELECT MAX( general_community_profiles_id ) FROM general_community_profiles WHERE type_id ='".$page_type_search."' AND community_id='".$all_com_ans['community_id']."'";
									
									$sql_page_typece = mysql_query("SELECT MAX( id ) FROM community_event_profiles WHERE type_id ='".$page_type_search."' AND community_event_id='".$all_ce_ans['id']."'");
									$run_page_typece = mysql_fetch_assoc($sql_page_typece);
									
									if($run_page_typece['MAX( id )']!=NULL)
									{
										//echo "hii";
										$sql_cet = mysql_query("SELECT * FROM community_event WHERE id='".$all_ce_ans['id']."'");
										
										while($row_cet = mysql_fetch_assoc($sql_cet))
										{
											$row_cet['table_name'] = 'community_event';
											$ans_come_org[$l] = $row_cet;
										}
										$l = $l + 1;
									}
								}
							}
						}
						
						if($profile_type_search=='project')
						{
							$sql_all_ap = mysql_query("SELECT * FROM artist_project WHERE del_status=0 AND active=0 AND image_name!='' AND profile_url!=''");
							if(mysql_num_rows($sql_all_ap)>0)
							{
								//$all_art_ans = array();
								$m = 0;
								while($all_ap_ans = mysql_fetch_assoc($sql_all_ap))
								{
									//$sql_page_typec_1 = "SELECT MAX( general_community_profiles_id ) FROM general_community_profiles WHERE type_id ='".$page_type_search."' AND community_id='".$all_com_ans['community_id']."'";
									
									$sql_page_typeap = mysql_query("SELECT MAX( id ) FROM artist_project_profiles WHERE type_id ='".$page_type_search."' AND artist_project_id='".$all_ap_ans['id']."'");
									$run_page_typeap = mysql_fetch_assoc($sql_page_typeap);
									
									if($run_page_typeap['MAX( id )']!=NULL)
									{
										//echo "hii";
										$sql_apt = mysql_query("SELECT * FROM artist_project WHERE id='".$all_ap_ans['id']."'");
										
										while($row_apt = mysql_fetch_assoc($sql_apt))
										{
											$row_apt['table_name'] = 'artist_project';
											$ans_artp_org[$m] = $row_apt;
										}
										$m = $m + 1;
									}
								}
							}
							
							$sql_all_cp = mysql_query("SELECT * FROM community_project WHERE del_status=0 AND active=0 AND image_name!='' AND profile_url!=''");
							if(mysql_num_rows($sql_all_cp)>0)
							{
								//$all_art_ans = array();
								$n = 0;
								while($all_cp_ans = mysql_fetch_assoc($sql_all_cp))
								{
									//$sql_page_typec_1 = "SELECT MAX( general_community_profiles_id ) FROM general_community_profiles WHERE type_id ='".$page_type_search."' AND community_id='".$all_com_ans['community_id']."'";
									
									$sql_page_typecp = mysql_query("SELECT MAX( id ) FROM community_project_profiles WHERE type_id ='".$page_type_search."' AND community_project_id='".$all_cp_ans['id']."'");
									$run_page_typecp = mysql_fetch_assoc($sql_page_typecp);
									
									if($run_page_typecp['MAX( id )']!=NULL)
									{
										//echo "hii";
										$sql_cpt = mysql_query("SELECT * FROM community_project WHERE id='".$all_cp_ans['id']."'");
										
										while($row_cpt = mysql_fetch_assoc($sql_cpt))
										{
											$row_cpt['table_name'] = 'community_project';
											$ans_comp_org[$n] = $row_cpt;
										}
										$n = $n + 1;
									}
								}
							}
						}
						
						if($profile_type_search=='media')
						{
							$sql_all_me = mysql_query("SELECT * FROM general_media WHERE delete_status=0 AND media_status=0 AND media_type='".$page_type_search."'");
							if(mysql_num_rows($sql_all_me)>0)
							{
								//$all_art_ans = array();
								$mj = 0;
								while($all_me_ans = mysql_fetch_assoc($sql_all_me))
								{
									//$sql_page_typec_1 = "SELECT MAX( general_community_profiles_id ) FROM general_community_profiles WHERE type_id ='".$page_type_search."' AND community_id='".$all_com_ans['community_id']."'";
									
									//$sql_page_typeme = mysql_query("SELECT MAX( id ) FROM media_profiles_type WHERE type_id ='".$page_type_search."' AND media_id='".$all_me_ans['id']."'");
									//$run_page_typeme = mysql_fetch_assoc($sql_page_typeme);
									
									/* if($run_page_typeme['MAX( id )']!=NULL)
									{
										//echo "hii";
										$sql_met = mysql_query("SELECT * FROM general_media WHERE id='".$all_me_ans['id']."'");
										
										while($row_mnet = mysql_fetch_assoc($sql_met))
										{ */
											$ans_med_org[$mj] = $all_me_ans;
										//}
										$mj = $mj + 1;
									//}
								}
							}
						}
						
						$main_ans = array_merge($ans_art_org,$ans_com_org,$ans_arte_org,$ans_artp_org,$ans_come_org,$ans_comp_org,$ans_med_org);
						return $main_ans;
					}
				}
				elseif($country_search!=0 && $page_type_search!=0)
				{
					$ans_art_org = array();
					$ans_com_org = array();
					$ans_arte_org = array();
					$ans_artp_org = array();
					$ans_come_org = array();
					$ans_comp_org = array();
					$ans_media = array();
						
					if($profile_type_search=='artist' || $profile_type_search=='community')
					{
						if($profile_type_search=='artist')
						{
							if($state_search!=0 && $city_search=="")
							{
								$sql_all_art = mysql_query("SELECT * FROM general_artist WHERE status=0 AND active=0 AND country_id='".$country_search."' AND state_id='".$state_search."' AND image_name!='' AND profile_url!=''");
							}
							elseif($state_search!=0 && $city_search!="")
							{
								$sql_all_art = mysql_query("SELECT * FROM general_artist WHERE status=0 AND active=0 AND country_id='".$country_search."' AND state_id='".$state_search."' AND city='".$city_search."' AND image_name!='' AND profile_url!=''");
							}
							elseif($state_search==0 && $city_search!="")
							{
								$sql_all_art = mysql_query("SELECT * FROM general_artist WHERE status=0 AND active=0 AND country_id='".$country_search."' AND city='".$city_search."' AND image_name!='' AND profile_url!=''");
							}
							else
							{
								$sql_all_art = mysql_query("SELECT * FROM general_artist WHERE status=0 AND active=0 AND country_id='".$country_search."' AND image_name!='' AND profile_url!=''");
							}
							if(mysql_num_rows($sql_all_art)>0)
							{
								//$all_art_ans = array();
								$i = 0;
								while($all_art_ans = mysql_fetch_assoc($sql_all_art))
								{
									//$sql_page_type_1 = "SELECT MAX( general_artist_profiles_id ) FROM general_artist_profiles WHERE type_id ='".$page_type_search."' AND artist_id='".$all_art_ans['artist_id']."'";
									
									if($sub_type_search!=0 && $meta_type_search==0)
									{
										$sql_page_typea = mysql_query("SELECT MAX( general_artist_profiles_id ) FROM general_artist_profiles WHERE type_id ='".$page_type_search."' AND subtype_id='".$sub_type_search."' AND artist_id='".$all_art_ans['artist_id']."'");
									}
									elseif($sub_type_search!=0 && $meta_type_search!=0)
									{
										$sql_page_typea = mysql_query("SELECT MAX( general_artist_profiles_id ) FROM general_artist_profiles WHERE type_id ='".$page_type_search."' AND subtype_id='".$sub_type_search."' AND metatype_id='".$meta_type_search."' AND artist_id='".$all_art_ans['artist_id']."'");
									}
									else
									{
										$sql_page_typea = mysql_query("SELECT MAX( general_artist_profiles_id ) FROM general_artist_profiles WHERE type_id ='".$page_type_search."' AND artist_id='".$all_art_ans['artist_id']."'");
									}
									$run_page_typea = mysql_fetch_assoc($sql_page_typea);
									if($run_page_typea['MAX( general_artist_profiles_id )']!=NULL)
									{
										//echo "hii";
										$sql_artt = mysql_query("SELECT * FROM general_artist WHERE artist_id='".$all_art_ans['artist_id']."'");
											
										while($row_art = mysql_fetch_assoc($sql_artt))
										{
											$ans_art_org[$i] = $row_art;
										}
										$i = $i + 1;
									}
								}
							}
						}
						
						if($profile_type_search=='community')
						{
							if($state_search!=0 && $city_search=="")
							{
								$sql_all_com = mysql_query("SELECT * FROM general_community WHERE status=0 AND active=0 AND country_id='".$country_search."' AND state_id='".$state_search."' AND image_name!='' AND profile_url!=''");
							}
							elseif($state_search!=0 && $city_search!="")
							{
								$sql_all_com = mysql_query("SELECT * FROM general_community WHERE status=0 AND active=0 AND country_id='".$country_search."' AND state_id='".$state_search."' AND city='".$city_search."' AND image_name!='' AND profile_url!=''");
							}
							elseif($state_search==0 && $city_search!="")
							{
								$sql_all_com = mysql_query("SELECT * FROM general_community WHERE status=0 AND active=0 AND country_id='".$country_search."' AND city='".$city_search."' AND image_name!='' AND profile_url!=''");
							}
							else
							{
								$sql_all_com = mysql_query("SELECT * FROM general_community WHERE status=0 AND active=0 AND country_id='".$country_search."' AND image_name!='' AND profile_url!=''");
							}
							if(mysql_num_rows($sql_all_com)>0)
							{
								//$all_art_ans = array();
								$j = 0;
								while($all_com_ans = mysql_fetch_assoc($sql_all_com))
								{
									//$sql_page_typec_1 = "SELECT MAX( general_community_profiles_id ) FROM general_community_profiles WHERE type_id ='".$page_type_search."' AND community_id='".$all_com_ans['community_id']."'";
									if($sub_type_search!=0 && $meta_type_search==0)
									{
										$sql_page_typec = mysql_query("SELECT MAX( general_community_profiles_id ) FROM general_community_profiles WHERE type_id ='".$page_type_search."' AND subtype_id='".$sub_type_search."' AND community_id='".$all_com_ans['community_id']."'");
									}
									elseif($sub_type_search!=0 && $meta_type_search!=0)
									{
										$sql_page_typec = mysql_query("SELECT MAX( general_community_profiles_id ) FROM general_community_profiles WHERE type_id ='".$page_type_search."' AND subtype_id='".$sub_type_search."' AND metatype_id='".$meta_type_search."' AND community_id='".$all_com_ans['community_id']."'");
									}
									else
									{
										$sql_page_typec = mysql_query("SELECT MAX( general_community_profiles_id ) FROM general_community_profiles WHERE type_id ='".$page_type_search."' AND community_id='".$all_com_ans['community_id']."'");
									}
									$run_page_typec = mysql_fetch_assoc($sql_page_typec);
									
									if($run_page_typec['MAX( general_community_profiles_id )']!=NULL)
									{
										//echo "hii";
										$sql_comt = mysql_query("SELECT * FROM general_community WHERE community_id='".$all_com_ans['community_id']."'");
										
										while($row_comt = mysql_fetch_assoc($sql_comt))
										{
											$ans_com_org[$j] = $row_comt;
										}
										$j = $j + 1;
									}
								}
							}
						}
					}
					
					if($profile_type_search=='event')
					{
						if($state_search!=0 && $city_search=="")
						{
							$sql_all_ae = mysql_query("SELECT * FROM artist_event AND country_id='".$country_search."' AND state_id='".$state_search."' AND del_status=0 AND active=0 AND image_name!='' AND profile_url!=''");
						}
						elseif($state_search!=0 && $city_search!="")
						{
							$sql_all_ae = mysql_query("SELECT * FROM artist_event AND country_id='".$country_search."' AND state_id='".$state_search."' AND city='".$city_search."' AND del_status=0 AND active=0 AND image_name!='' AND profile_url!=''");
						}
						elseif($state_search==0 && $city_search!="")
						{
							$sql_all_ae = mysql_query("SELECT * FROM artist_event AND country_id='".$country_search."' AND city='".$city_search."' AND del_status=0 AND active=0 AND image_name!='' AND profile_url!=''");
						}
						else
						{
							$sql_all_ae = mysql_query("SELECT * FROM artist_event AND country_id='".$country_search."' AND del_status=0 AND active=0 AND image_name!='' AND profile_url!=''");
						}
						
						if(isset($sql_all_ae) && !empty($sql_all_ae))
						{
							if(mysql_num_rows($sql_all_ae)>0)
							{
								//$all_art_ans = array();
								$k = 0;
								while($all_ae_ans = mysql_fetch_assoc($sql_all_ae))
								{
									//$sql_page_typec_1 = "SELECT MAX( general_community_profiles_id ) FROM general_community_profiles WHERE type_id ='".$page_type_search."' AND community_id='".$all_com_ans['community_id']."'";
									if($sub_type_search!=0 && $meta_type_search==0)
									{
										$sql_page_typeae = mysql_query("SELECT MAX( id ) FROM artist_event_profiles WHERE type_id ='".$page_type_search."' AND subtype_id='".$sub_type_search."' AND artist_event_id='".$all_ae_ans['id']."'");
									}
									elseif($sub_type_search!=0 && $meta_type_search!=0)
									{
										$sql_page_typeae = mysql_query("SELECT MAX( id ) FROM artist_event_profiles WHERE type_id ='".$page_type_search."' AND subtype_id='".$sub_type_search."' AND metatype_id='".$meta_type_search."' AND artist_event_id='".$all_ae_ans['id']."'");
									}
									else
									{
										$sql_page_typeae = mysql_query("SELECT MAX( id ) FROM artist_event_profiles WHERE type_id ='".$page_type_search."' AND artist_event_id='".$all_ae_ans['id']."'");
									}
									$run_page_typeae = mysql_fetch_assoc($sql_page_typeae);
									
									if($run_page_typeae['MAX( id )']!=NULL)
									{
										//echo "hii";
										$sql_aet = mysql_query("SELECT * FROM artist_event WHERE id='".$all_ae_ans['id']."'");
										
										while($row_aet = mysql_fetch_assoc($sql_aet))
										{
											$row_aet['table_name'] = 'artist_event';
											$ans_arte_org[$k] = $row_aet;
										}
										$k = $k + 1;
									}
								}
							}
						}
						if($state_search!=0 && $city_search=="")
						{
							$sql_all_ce = mysql_query("SELECT * FROM community_event AND country_id='".$country_search."' AND state_id='".$state_search."' AND image_name!='' AND del_status=0 AND active=0 AND profile_url!=''");
						}
						elseif($state_search!=0 && $city_search!="")
						{
							$sql_all_ce = mysql_query("SELECT * FROM community_event AND country_id='".$country_search."' AND state_id='".$state_search."' AND city='".$city_search."' AND del_status=0 AND active=0 AND image_name!='' AND profile_url!=''");
						}
						elseif($state_search==0 && $city_search!="")
						{	
							$sql_all_ce = mysql_query("SELECT * FROM community_event AND country_id='".$country_search."' AND del_status=0 AND active=0 AND city='".$city_search."' AND image_name!='' AND profile_url!=''");
						}
						else
						{
							$sql_all_ce = mysql_query("SELECT * FROM community_event AND country_id='".$country_search."' AND del_status=0 AND active=0 AND image_name!='' AND profile_url!=''");
						}
						
						if(isset($sql_all_ce) && !empty($sql_all_ce))
						{
							if(mysql_num_rows($sql_all_ce)>0)
							{
								//$all_art_ans = array();
								$l = 0;
								while($all_ce_ans = mysql_fetch_assoc($sql_all_ce))
								{
									//$sql_page_typec_1 = "SELECT MAX( general_community_profiles_id ) FROM general_community_profiles WHERE type_id ='".$page_type_search."' AND community_id='".$all_com_ans['community_id']."'";
									
									if($sub_type_search!=0 && $meta_type_search==0)
									{
										$sql_page_typece = mysql_query("SELECT MAX( id ) FROM community_event_profiles WHERE type_id ='".$page_type_search."' AND subtype_id='".$sub_type_search."' AND community_event_id='".$all_ce_ans['id']."'");
									}
									elseif($sub_type_search!=0 && $meta_type_search!=0)
									{
										$sql_page_typece = mysql_query("SELECT MAX( id ) FROM community_event_profiles WHERE type_id ='".$page_type_search."' AND subtype_id='".$sub_type_search."' AND metatype_id='".$meta_type_search."' AND community_event_id='".$all_ce_ans['id']."'");
									}
									else
									{
										$sql_page_typece = mysql_query("SELECT MAX( id ) FROM community_event_profiles WHERE type_id ='".$page_type_search."' AND community_event_id='".$all_ce_ans['id']."'");
									}
									$run_page_typece = mysql_fetch_assoc($sql_page_typece);
									
									if($run_page_typece['MAX( id )']!=NULL)
									{
										//echo "hii";
										$sql_cet = mysql_query("SELECT * FROM community_event WHERE id='".$all_ce_ans['id']."'");
										
										while($row_cet = mysql_fetch_assoc($sql_cet))
										{
											$row_cet['table_name'] = 'community_event';
											$ans_come_org[$l] = $row_cet;
										}
										$l = $l + 1;
									}
								}
							}
						}
					}
					
					if($profile_type_search=='project')
					{
						if($state_search!=0 && $city_search=="")
						{
							$sql_all_ap = mysql_query("SELECT * FROM artist_project AND country_id='".$country_search."' AND state_id='".$state_search."' AND del_status=0 AND active=0 AND image_name!='' AND profile_url!=''");
						}
						elseif($state_search!=0 && $city_search!="")
						{
							$sql_all_ap = mysql_query("SELECT * FROM artist_project AND country_id='".$country_search."' AND state_id='".$state_search."' AND city='".$city_search."' AND del_status=0 AND active=0 AND image_name!='' AND profile_url!=''");
						}
						elseif($state_search==0 && $city_search!="")
						{
							$sql_all_ap = mysql_query("SELECT * FROM artist_project AND country_id='".$country_search."' AND city='".$city_search."' AND del_status=0 AND active=0 AND image_name!='' AND profile_url!=''");
						}
						else
						{
							$sql_all_ap = mysql_query("SELECT * FROM artist_project AND country_id='".$country_search."' AND del_status=0 AND active=0 AND image_name!='' AND profile_url!=''");
						}
						if(mysql_num_rows($sql_all_ap)>0)
						{
							//$all_art_ans = array();
							$m = 0;
							while($all_ap_ans = mysql_fetch_assoc($sql_all_ap))
							{
								//$sql_page_typec_1 = "SELECT MAX( general_community_profiles_id ) FROM general_community_profiles WHERE type_id ='".$page_type_search."' AND community_id='".$all_com_ans['community_id']."'";
								
								if($sub_type_search!=0 && $meta_type_search==0)
								{
									$sql_page_typeap = mysql_query("SELECT MAX( id ) FROM artist_project_profiles WHERE type_id ='".$page_type_search."' AND subtype_id='".$sub_type_search."' AND artist_project_id='".$all_ap_ans['id']."'");
								}
								elseif($sub_type_search!=0 && $meta_type_search!=0)
								{
									$sql_page_typeap = mysql_query("SELECT MAX( id ) FROM artist_project_profiles WHERE type_id ='".$page_type_search."' AND subtype_id='".$sub_type_search."' AND metatype_id='".$meta_type_search."' AND artist_project_id='".$all_ap_ans['id']."'");
								}
								else
								{
									$sql_page_typeap = mysql_query("SELECT MAX( id ) FROM artist_project_profiles WHERE type_id ='".$page_type_search."' AND artist_project_id='".$all_ap_ans['id']."'");
								}
								$run_page_typeap = mysql_fetch_assoc($sql_page_typeap);
								
								if($run_page_typeap['MAX( id )']!=NULL)
								{
									//echo "hii";
									$sql_apt = mysql_query("SELECT * FROM artist_project WHERE id='".$all_ap_ans['id']."'");
									
									while($row_apt = mysql_fetch_assoc($sql_apt))
									{
										$row_apt['table_name'] = 'artist_project';
										$ans_artp_org[$m] = $row_apt;
									}
									$m = $m + 1;
								}
							}
						}
						
						if($state_search!=0 && $city_search=="")
						{
							$sql_all_cp = mysql_query("SELECT * FROM community_project AND country_id='".$country_search."' AND state_id='".$state_search."' AND del_status=0 AND active=0 AND image_name!='' AND profile_url!=''");
						}
						elseif($state_search!=0 && $city_search!="")
						{
							$sql_all_cp = mysql_query("SELECT * FROM community_project AND country_id='".$country_search."' AND state_id='".$state_search."' AND city='".$city_search."' AND del_status=0 AND active=0 AND image_name!='' AND profile_url!=''");
						}
						elseif($state_search==0 && $city_search!="")
						{
							$sql_all_cp = mysql_query("SELECT * FROM community_project AND country_id='".$country_search."' AND city='".$city_search."' AND del_status=0 AND active=0 AND image_name!='' AND profile_url!=''");
						}
						else
						{
							$sql_all_cp = mysql_query("SELECT * FROM community_project AND country_id='".$country_search."' AND del_status=0 AND active=0 AND image_name!='' AND profile_url!=''");
						}
						if(mysql_num_rows($sql_all_cp)>0)
						{
							//$all_art_ans = array();
							$n = 0;
							while($all_cp_ans = mysql_fetch_assoc($sql_all_cp))
							{
								//$sql_page_typec_1 = "SELECT MAX( general_community_profiles_id ) FROM general_community_profiles WHERE type_id ='".$page_type_search."' AND community_id='".$all_com_ans['community_id']."'";
								
								if($sub_type_search!=0 && $meta_type_search==0)
								{
									$sql_page_typecp = mysql_query("SELECT MAX( id ) FROM community_project_profiles WHERE type_id ='".$page_type_search."' AND subtype_id='".$sub_type_search."' AND community_project_id='".$all_cp_ans['id']."'");
								}
								elseif($sub_type_search!=0 && $meta_type_search!=0)
								{
									$sql_page_typecp = mysql_query("SELECT MAX( id ) FROM community_project_profiles WHERE type_id ='".$page_type_search."' AND subtype_id='".$sub_type_search."' AND metatype_id='".$meta_type_search."' AND community_project_id='".$all_cp_ans['id']."'");
								}
								else
								{
									$sql_page_typecp = mysql_query("SELECT MAX( id ) FROM community_project_profiles WHERE type_id ='".$page_type_search."' AND community_project_id='".$all_cp_ans['id']."'");
								}
								$run_page_typecp = mysql_fetch_assoc($sql_page_typecp);
								
								if($run_page_typecp['MAX( id )']!=NULL)
								{
									//echo "hii";
									$sql_cpt = mysql_query("SELECT * FROM community_project WHERE id='".$all_cp_ans['id']."'");
									
									while($row_cpt = mysql_fetch_assoc($sql_cpt))
									{
										$row_cpt['table_name'] = 'community_project';
										$ans_comp_org[$n] = $row_cpt;
									}
									$n = $n + 1;
								}
							}
						}
					}
					
					if($page_type_search=='media')
					{
						if($state_search!=0 && $city_search=="")
						{
							$sql_med = mysql_query("SELECT * FROM general_media WHERE delete_status=0 AND media_status=0");
							if(mysql_num_rows($sql_med)>0)
							{
								$mj = 0;
								while($row_mede = mysql_fetch_assoc($sql_med))
								{
									if($row_mede['from_info']!="")
									{
										$exp_from = explode('|',$row_mede['from_info']);
										if($exp_from[0]=='general_artist')
										{
											$sql_get_tables = mysql_query("SELECT * FROM $exp_from[0] WHERE artist_id='".$exp_from[1]."' AND country_id='".$country_search."' AND state_id='".$state_search."' AND status=0 AND active=0");
										}
										elseif($exp_from[0]=='general_community')
										{
											$sql_get_tables = mysql_query("SELECT * FROM $exp_from[0] WHERE community_id='".$exp_from[1]."' AND country_id='".$country_search."' AND state_id='".$state_search."' AND status=0 AND active=0");
										}
										elseif($exp_from[0]=='community_event' || $exp_from[0]=='artist_event' || $exp_from[0]=='artist_project' || $exp_from[0]=='community_project')
										{
											$sql_get_tables = mysql_query("SELECT * FROM $exp_from[0] WHERE id='".$exp_from[1]."' AND country_id='".$country_search."' AND state_id='".$state_search."' AND del_status=0 AND active=0");
										}
										
										if(isset($sql_get_tables) && !empty($sql_get_tables))
										{
											if(mysql_num_rows($sql_get_tables)>0)
											{
												$sql_get_med = mysql_query("SELECT * FROM general_media WHERE id='".$row_mede['id']."' AND delete_status=0 AND media_status=0");
												
												while($row_csc = mysql_fetch_assoc($sql_get_med))
												{
													if($sub_type_search!=0 && $meta_type_search==0)
													{
														$sql_page_typeme = mysql_query("SELECT MAX( id ) FROM media_profiles_type WHERE type_id ='".$page_type_search."' AND subtype_id='".$sub_type_search."' AND media_id='".$row_csc['id']."'");
													}
													elseif($sub_type_search!=0 && $meta_type_search!=0)
													{
														$sql_page_typeme = mysql_query("SELECT MAX( id ) FROM media_profiles_type WHERE type_id ='".$page_type_search."' AND subtype_id='".$sub_type_search."' AND metatype_id='".$meta_type_search."' AND media_id='".$row_csc['id']."'");
													}
													else
													{
														$sql_page_typeme = mysql_query("SELECT MAX( id ) FROM media_profiles_type WHERE type_id ='".$page_type_search."' AND media_id='".$row_csc['id']."'");
													}
													
													$run_page_typeme = mysql_fetch_assoc($sql_page_typeme);
													
													if($run_page_typeme['MAX( id )']!=NULL)
													{
														//echo "hii";
														$sql_met = mysql_query("SELECT * FROM general_media WHERE id='".$row_csc['id']."'");
														
														while($row_mnet = mysql_fetch_assoc($sql_met))
														{
															$ans_media[$mj] = $row_mnet;
														}
													}
												}
											}
										}
									}
									elseif($row_mede['creator_info']!="")
									{
										$exp_from = explode('|',$row_mede['creator_info']);
										if($exp_from[0]=='general_artist')
										{
											$sql_get_tables = mysql_query("SELECT * FROM $exp_from[0] WHERE artist_id='".$exp_from[1]."' AND country_id='".$country_search."' AND state_id='".$state_search."' AND status=0 AND active=0");
										}
										elseif($exp_from[0]=='general_community')
										{
											$sql_get_tables = mysql_query("SELECT * FROM $exp_from[0] WHERE community_id='".$exp_from[1]."' AND country_id='".$country_search."' AND state_id='".$state_search."' AND status=0 AND active=0");
										}
										elseif($exp_from[0]=='community_event' || $exp_from[0]=='artist_event' || $exp_from[0]=='artist_project' || $exp_from[0]=='community_project')
										{
											$sql_get_tables = mysql_query("SELECT * FROM $exp_from[0] WHERE id='".$exp_from[1]."' AND country_id='".$country_search."' AND state_id='".$state_search."' AND del_status=0 AND active=0");
										}
										
										if(isset($sql_get_tables) && !empty($sql_get_tables))
										{
											if(mysql_num_rows($sql_get_tables)>0)
											{
												$sql_get_med = mysql_query("SELECT * FROM general_media WHERE id='".$row_mede['id']."' AND delete_status=0 AND media_status=0");
												
												while($row_csc = mysql_fetch_assoc($sql_get_med))
												{
													if($sub_type_search!=0 && $meta_type_search==0)
													{
														$sql_page_typeme = mysql_query("SELECT MAX( id ) FROM media_profiles_type WHERE type_id ='".$page_type_search."' AND subtype_id='".$sub_type_search."' AND media_id='".$row_csc['id']."'");
													}
													elseif($sub_type_search!=0 && $meta_type_search!=0)
													{
														$sql_page_typeme = mysql_query("SELECT MAX( id ) FROM media_profiles_type WHERE type_id ='".$page_type_search."' AND subtype_id='".$sub_type_search."' AND metatype_id='".$meta_type_search."' AND media_id='".$row_csc['id']."'");
													}
													else
													{
														$sql_page_typeme = mysql_query("SELECT MAX( id ) FROM media_profiles_type WHERE type_id ='".$page_type_search."' AND media_id='".$row_csc['id']."'");
													}
													
													$run_page_typeme = mysql_fetch_assoc($sql_page_typeme);
													
													if($run_page_typeme['MAX( id )']!=NULL)
													{
														//echo "hii";
														$sql_met = mysql_query("SELECT * FROM general_media WHERE id='".$row_csc['id']."' AND delete_status=0 AND media_status=0");
														
														while($row_mnet = mysql_fetch_assoc($sql_met))
														{
															$ans_media[$mj] = $row_mnet;
														}
													}
												}
											}
										}
									}
									$mj = $mj + 1;
								}
							}
						}
						elseif($state_search!=0 && $city_search!="")
						{
							$sql_med = mysql_query("SELECT * FROM general_media WHERE delete_status=0 AND media_status=0");
							if(mysql_num_rows($sql_med)>0)
							{
								$mj_o = 0;
								while($row_mede = mysql_fetch_assoc($sql_med))
								{
									if($row_mede['from_info']!="")
									{
										$exp_from = explode('|',$row_mede['from_info']);
										if($exp_from[0]=='general_artist')
										{
											$sql_get_tables = mysql_query("SELECT * FROM $exp_from[0] WHERE artist_id='".$exp_from[1]."' AND country_id='".$country_search."' AND state_id='".$state_search."' AND city='".$city_search."' AND status=0 AND active=0");
										}
										elseif($exp_from[0]=='general_community')
										{
											$sql_get_tables = mysql_query("SELECT * FROM $exp_from[0] WHERE community_id='".$exp_from[1]."' AND country_id='".$country_search."' AND state_id='".$state_search."' AND city='".$city_search."' AND status=0 AND active=0");
										}
										elseif($exp_from[0]=='community_event' || $exp_from[0]=='artist_event' || $exp_from[0]=='artist_project' || $exp_from[0]=='community_project')
										{
											$sql_get_tables = mysql_query("SELECT * FROM $exp_from[0] WHERE id='".$exp_from[1]."' AND country_id='".$country_search."' AND state_id='".$state_search."' AND city='".$city_search."' AND del_status=0 AND active=0");
										}
										
										if(isset($sql_get_tables) && !empty($sql_get_tables))
										{
											if(mysql_num_rows($sql_get_tables)>0)
											{
												$sql_get_med = mysql_query("SELECT * FROM general_media WHERE id='".$row_mede['id']."' AND delete_status=0 AND media_status=0");
												
												while($row_csc = mysql_fetch_assoc($sql_get_med))
												{
													if($sub_type_search!=0 && $meta_type_search==0)
													{
														$sql_page_typeme = mysql_query("SELECT MAX( id ) FROM media_profiles_type WHERE type_id ='".$page_type_search."' AND subtype_id='".$sub_type_search."' AND media_id='".$row_csc['id']."'");
													}
													elseif($sub_type_search!=0 && $meta_type_search!=0)
													{
														$sql_page_typeme = mysql_query("SELECT MAX( id ) FROM media_profiles_type WHERE type_id ='".$page_type_search."' AND subtype_id='".$sub_type_search."' AND metatype_id='".$meta_type_search."' AND media_id='".$row_csc['id']."'");
													}
													else
													{
														$sql_page_typeme = mysql_query("SELECT MAX( id ) FROM media_profiles_type WHERE type_id ='".$page_type_search."' AND media_id='".$row_csc['id']."'");
													}
													
													$run_page_typeme = mysql_fetch_assoc($sql_page_typeme);
													
													if($run_page_typeme['MAX( id )']!=NULL)
													{
														//echo "hii";
														$sql_met = mysql_query("SELECT * FROM general_media WHERE id='".$row_csc['id']."' AND delete_status=0 AND media_status=0");
														
														while($row_mnet = mysql_fetch_assoc($sql_met))
														{
															$ans_media[$mj_o] = $row_mnet;
														}
													}
												}
											}
										}
									}
									elseif($row_mede['creator_info']!="")
									{
										$exp_from = explode('|',$row_mede['creator_info']);
										if($exp_from[0]=='general_artist')
										{
											$sql_get_tables = mysql_query("SELECT * FROM $exp_from[0] WHERE artist_id='".$exp_from[1]."' AND country_id='".$country_search."' AND state_id='".$state_search."' AND city='".$city_search."' AND status=0 AND active=0");
										}
										elseif($exp_from[0]=='general_community')
										{
											$sql_get_tables = mysql_query("SELECT * FROM $exp_from[0] WHERE community_id='".$exp_from[1]."' AND country_id='".$country_search."' AND state_id='".$state_search."' AND city='".$city_search."' AND status=0 AND active=0");
										}
										elseif($exp_from[0]=='community_event' || $exp_from[0]=='artist_event' || $exp_from[0]=='artist_project' || $exp_from[0]=='community_project')
										{
											$sql_get_tables = mysql_query("SELECT * FROM $exp_from[0] WHERE id='".$exp_from[1]."' AND country_id='".$country_search."' AND state_id='".$state_search."' AND city='".$city_search."' AND del_status=0 AND active=0");
										}
										
										if(isset($sql_get_tables) && !empty($sql_get_tables))
										{
											if(mysql_num_rows($sql_get_tables)>0)
											{
												$sql_get_med = mysql_query("SELECT * FROM general_media WHERE id='".$row_mede['id']."' AND delete_status=0 AND media_status=0");
												
												while($row_csc = mysql_fetch_assoc($sql_get_med))
												{
													if($sub_type_search!=0 && $meta_type_search==0)
													{
														$sql_page_typeme = mysql_query("SELECT MAX( id ) FROM media_profiles_type WHERE type_id ='".$page_type_search."' AND subtype_id='".$sub_type_search."' AND media_id='".$row_csc['id']."'");
													}
													elseif($sub_type_search!=0 && $meta_type_search!=0)
													{
														$sql_page_typeme = mysql_query("SELECT MAX( id ) FROM media_profiles_type WHERE type_id ='".$page_type_search."' AND subtype_id='".$sub_type_search."' AND metatype_id='".$meta_type_search."' AND media_id='".$row_csc['id']."'");
													}
													else
													{
														$sql_page_typeme = mysql_query("SELECT MAX( id ) FROM media_profiles_type WHERE type_id ='".$page_type_search."' AND media_id='".$row_csc['id']."'");
													}
													
													$run_page_typeme = mysql_fetch_assoc($sql_page_typeme);
													
													if($run_page_typeme['MAX( id )']!=NULL)
													{
														//echo "hii";
														$sql_met = mysql_query("SELECT * FROM general_media WHERE id='".$row_csc['id']."' AND delete_status=0 AND media_status=0");
														
														while($row_mnet = mysql_fetch_assoc($sql_met))
														{
															$ans_media[$mj_o] = $row_mnet;
														}
													}
												}
											}
										}
									}
									$mj_o = $mj_o + 1;
								}
							}
						}
						elseif($state_search==0 && $city_search!="")
						{
							$sql_med = mysql_query("SELECT * FROM general_media WHERE delete_status=0 AND media_status=0");
							if(mysql_num_rows($sql_med)>0)
							{
								$mj_p = 0;
								while($row_mede = mysql_fetch_assoc($sql_med))
								{
									if($row_mede['from_info']!="")
									{
										$exp_from = explode('|',$row_mede['from_info']);
										if($exp_from[0]=='general_artist')
										{
											$sql_get_tables = mysql_query("SELECT * FROM $exp_from[0] WHERE artist_id='".$exp_from[1]."' AND country_id='".$country_search."' AND city='".$city_search."' AND status=0 AND active=0");
										}
										elseif($exp_from[0]=='general_community')
										{
											$sql_get_tables = mysql_query("SELECT * FROM $exp_from[0] WHERE community_id='".$exp_from[1]."' AND country_id='".$country_search."' AND city='".$city_search."' AND status=0 AND active=0");
										}
										elseif($exp_from[0]=='community_event' || $exp_from[0]=='artist_event' || $exp_from[0]=='artist_project' || $exp_from[0]=='community_project')
										{
											$sql_get_tables = mysql_query("SELECT * FROM $exp_from[0] WHERE id='".$exp_from[1]."' AND country_id='".$country_search."' AND city='".$city_search."' AND del_status=0 AND active=0");
										}
										
										if(isset($sql_get_tables) && !empty($sql_get_tables))
										{
											if(mysql_num_rows($sql_get_tables)>0)
											{
												$sql_get_med = mysql_query("SELECT * FROM general_media WHERE id='".$row_mede['id']."' AND delete_status=0 AND media_status=0");
												
												while($row_csc = mysql_fetch_assoc($sql_get_med))
												{
													if($sub_type_search!=0 && $meta_type_search==0)
													{
														$sql_page_typeme = mysql_query("SELECT MAX( id ) FROM media_profiles_type WHERE type_id ='".$page_type_search."' AND subtype_id='".$sub_type_search."' AND media_id='".$row_csc['id']."'");
													}
													elseif($sub_type_search!=0 && $meta_type_search!=0)
													{
														$sql_page_typeme = mysql_query("SELECT MAX( id ) FROM media_profiles_type WHERE type_id ='".$page_type_search."' AND subtype_id='".$sub_type_search."' AND metatype_id='".$meta_type_search."' AND media_id='".$row_csc['id']."'");
													}
													else
													{
														$sql_page_typeme = mysql_query("SELECT MAX( id ) FROM media_profiles_type WHERE type_id ='".$page_type_search."' AND media_id='".$row_csc['id']."'");
													}
													
													$run_page_typeme = mysql_fetch_assoc($sql_page_typeme);
													
													if($run_page_typeme['MAX( id )']!=NULL)
													{
														//echo "hii";
														$sql_met = mysql_query("SELECT * FROM general_media WHERE id='".$row_csc['id']."' AND delete_status=0 AND media_status=0");
														
														while($row_mnet = mysql_fetch_assoc($sql_met))
														{
															$ans_media[$mj_p] = $row_mnet;
														}
													}
												}
											}
										}
									}
									elseif($row_mede['creator_info']!="")
									{
										$exp_from = explode('|',$row_mede['creator_info']);
										if($exp_from[0]=='general_artist')
										{
											$sql_get_tables = mysql_query("SELECT * FROM $exp_from[0] WHERE artist_id='".$exp_from[1]."' AND country_id='".$country_search."' AND city='".$city_search."' AND status=0 AND active=0");
										}
										elseif($exp_from[0]=='general_community')
										{
											$sql_get_tables = mysql_query("SELECT * FROM $exp_from[0] WHERE community_id='".$exp_from[1]."' AND country_id='".$country_search."' AND city='".$city_search."' AND status=0 AND active=0");
										}
										elseif($exp_from[0]=='community_event' || $exp_from[0]=='artist_event' || $exp_from[0]=='artist_project' || $exp_from[0]=='community_project')
										{
											$sql_get_tables = mysql_query("SELECT * FROM $exp_from[0] WHERE id='".$exp_from[1]."' AND country_id='".$country_search."' AND city='".$city_search."' AND del_status=0 AND active=0");
										}
										
										if(isset($sql_get_tables) && !empty($sql_get_tables))
										{
											if(mysql_num_rows($sql_get_tables)>0)
											{
												$sql_get_med = mysql_query("SELECT * FROM general_media WHERE id='".$row_mede['id']."' AND delete_status=0 AND media_status=0");
												
												while($row_csc = mysql_fetch_assoc($sql_get_med))
												{
													if($sub_type_search!=0 && $meta_type_search==0)
													{
														$sql_page_typeme = mysql_query("SELECT MAX( id ) FROM media_profiles_type WHERE type_id ='".$page_type_search."' AND subtype_id='".$sub_type_search."' AND media_id='".$row_csc['id']."'");
													}
													elseif($sub_type_search!=0 && $meta_type_search!=0)
													{
														$sql_page_typeme = mysql_query("SELECT MAX( id ) FROM media_profiles_type WHERE type_id ='".$page_type_search."' AND subtype_id='".$sub_type_search."' AND metatype_id='".$meta_type_search."' AND media_id='".$row_csc['id']."'");
													}
													else
													{
														$sql_page_typeme = mysql_query("SELECT MAX( id ) FROM media_profiles_type WHERE type_id ='".$page_type_search."' AND media_id='".$row_csc['id']."'");
													}
													
													$run_page_typeme = mysql_fetch_assoc($sql_page_typeme);
													
													if($run_page_typeme['MAX( id )']!=NULL)
													{
														//echo "hii";
														$sql_met = mysql_query("SELECT * FROM general_media WHERE id='".$row_csc['id']."' AND delete_status=0 AND media_status=0");
														
														while($row_mnet = mysql_fetch_assoc($sql_met))
														{
															$ans_media[$mj_p] = $row_mnet;
														}
													}
												}
											}
										}
									}
									$mj_p = $mj_p + 1;
								}
							}
						}
						else
						{
							$sql_med = mysql_query("SELECT * FROM general_media WHERE delete_status=0 AND media_status=0");
							if(mysql_num_rows($sql_med)>0)
							{
								$mj_c = 0;
								while($row_mede = mysql_fetch_assoc($sql_med))
								{
									if($row_mede['from_info']!="")
									{
										$exp_from = explode('|',$row_mede['from_info']);
										if($exp_from[0]=='general_artist')
										{
											$sql_get_tables = mysql_query("SELECT * FROM $exp_from[0] WHERE artist_id='".$exp_from[1]."' AND country_id='".$country_search."' AND status=0 AND active=0");
										}
										elseif($exp_from[0]=='general_community')
										{
											$sql_get_tables = mysql_query("SELECT * FROM $exp_from[0] WHERE community_id='".$exp_from[1]."' AND country_id='".$country_search."' AND status=0 AND active=0");
										}
										elseif($exp_from[0]=='community_event' || $exp_from[0]=='artist_event' || $exp_from[0]=='artist_project' || $exp_from[0]=='community_project')
										{
											$sql_get_tables = mysql_query("SELECT * FROM $exp_from[0] WHERE id='".$exp_from[1]."' AND country_id='".$country_search."' AND del_status=0 AND active=0");
										}
										
										if(isset($sql_get_tables) && !empty($sql_get_tables))
										{
											if(mysql_num_rows($sql_get_tables)>0)
											{
												$sql_get_med = mysql_query("SELECT * FROM general_media WHERE id='".$row_mede['id']."' AND delete_status=0 AND media_status=0");
												
												while($row_csc = mysql_fetch_assoc($sql_get_med))
												{
													if($sub_type_search!=0 && $meta_type_search==0)
													{
														$sql_page_typeme = mysql_query("SELECT MAX( id ) FROM media_profiles_type WHERE type_id ='".$page_type_search."' AND subtype_id='".$sub_type_search."' AND media_id='".$row_csc['id']."'");
													}
													elseif($sub_type_search!=0 && $meta_type_search!=0)
													{
														$sql_page_typeme = mysql_query("SELECT MAX( id ) FROM media_profiles_type WHERE type_id ='".$page_type_search."' AND subtype_id='".$sub_type_search."' AND metatype_id='".$meta_type_search."' AND media_id='".$row_csc['id']."'");
													}
													else
													{
														$sql_page_typeme = mysql_query("SELECT MAX( id ) FROM media_profiles_type WHERE type_id ='".$page_type_search."' AND media_id='".$row_csc['id']."'");
													}
													
													$run_page_typeme = mysql_fetch_assoc($sql_page_typeme);
													
													if($run_page_typeme['MAX( id )']!=NULL)
													{
														//echo "hii";
														$sql_met = mysql_query("SELECT * FROM general_media WHERE id='".$row_csc['id']."' AND delete_status=0 AND media_status=0");
														
														while($row_mnet = mysql_fetch_assoc($sql_met))
														{
															$ans_media[$mj_c] = $row_mnet;
														}
													}
												}
											}
										}
									}
									elseif($row_mede['creator_info']!="")
									{
										$exp_from = explode('|',$row_mede['creator_info']);
										if($exp_from[0]=='general_artist')
										{
											$sql_get_tables = mysql_query("SELECT * FROM $exp_from[0] WHERE artist_id='".$exp_from[1]."' AND country_id='".$country_search."' AND status=0 AND active=0");
										}
										elseif($exp_from[0]=='general_community')
										{
											$sql_get_tables = mysql_query("SELECT * FROM $exp_from[0] WHERE community_id='".$exp_from[1]."' AND country_id='".$country_search."' AND status=0 AND active=0");
										}
										elseif($exp_from[0]=='community_event' || $exp_from[0]=='artist_event' || $exp_from[0]=='artist_project' || $exp_from[0]=='community_project')
										{
											$sql_get_tables = mysql_query("SELECT * FROM $exp_from[0] WHERE id='".$exp_from[1]."' AND country_id='".$country_search."' AND del_status=0 AND active=0");
										}
										
										if(isset($sql_get_tables) && !empty($sql_get_tables))
										{
											if(mysql_num_rows($sql_get_tables)>0)
											{
												$sql_get_med = mysql_query("SELECT * FROM general_media WHERE id='".$row_mede['id']."' AND delete_status=0 AND media_status=0");
												
												while($row_csc = mysql_fetch_assoc($sql_get_med))
												{
													if($sub_type_search!=0 && $meta_type_search==0)
													{
														$sql_page_typeme = mysql_query("SELECT MAX( id ) FROM media_profiles_type WHERE type_id ='".$page_type_search."' AND subtype_id='".$sub_type_search."' AND media_id='".$row_csc['id']."'");
													}
													elseif($sub_type_search!=0 && $meta_type_search!=0)
													{
														$sql_page_typeme = mysql_query("SELECT MAX( id ) FROM media_profiles_type WHERE type_id ='".$page_type_search."' AND subtype_id='".$sub_type_search."' AND metatype_id='".$meta_type_search."' AND media_id='".$row_csc['id']."'");
													}
													else
													{
														$sql_page_typeme = mysql_query("SELECT MAX( id ) FROM media_profiles_type WHERE type_id ='".$page_type_search."' AND media_id='".$row_csc['id']."'");
													}
													
													$run_page_typeme = mysql_fetch_assoc($sql_page_typeme);
													
													if($run_page_typeme['MAX( id )']!=NULL)
													{
														//echo "hii";
														$sql_met = mysql_query("SELECT * FROM general_media WHERE id='".$row_csc['id']."' AND delete_status=0 AND media_status=0");
														
														while($row_mnet = mysql_fetch_assoc($sql_met))
														{
															$ans_media[$mj_c] = $row_mnet;
														}
													}
												}
											}
										}
									}
									$mj_c = $mj_c + 1;
								}
							}
						}
					}
					
					$main_ans = array_merge($ans_art_org,$ans_com_org,$ans_arte_org,$ans_artp_org,$ans_come_org,$ans_comp_org,$ans_media);
					return $main_ans;
				}
				elseif($city_search!="" && $country_search==0)
				{
					$ans_art_ci = array(); 
					$ans_com_ci = array();
					$ans_arte_ci = array();
					$ans_come_ci = array();
					$ans_artp_ci = array();
					$ans_comp_ci = array();
					$ans_med_ci = array();
					
					if($page_type_search==0)
					{
						if($profile_type_search=='artist')
						{
							$sql_art_ci = mysql_query("SELECT * FROM general_artist WHERE city='".$city_search."' AND status=0 AND active=0 AND image_name!='' AND profile_url!=''");
						}
						elseif($profile_type_search=='community')
						{
							$sql_com_ci = mysql_query("SELECT * FROM general_community WHERE city='".$city_search."' AND status=0 AND active=0 AND image_name!='' AND profile_url!=''");
						}
						elseif($profile_type_search=='event')
						{
							$sql_arte_ci = mysql_query("SELECT * FROM artist_event WHERE city='".$city_search."' AND del_status=0 AND active=0 AND image_name!='' AND profile_url!=''");
							$sql_come_ci = mysql_query("SELECT * FROM community_event WHERE city='".$city_search."' AND del_status=0 AND active=0 AND image_name!='' AND profile_url!=''");
						}
						elseif($profile_type_search=='project')
						{
							$sql_artp_ci = mysql_query("SELECT * FROM artist_project WHERE city='".$city_search."' AND del_status=0 AND active=0 AND image_name!='' AND profile_url!=''");
							$sql_comp_ci = mysql_query("SELECT * FROM community_project WHERE city='".$city_search."' AND del_status=0 AND active=0 AND image_name!='' AND profile_url!=''");
						}
						elseif($profile_type_search=='media')
						{
							$sql_med_ci = mysql_query("SELECT * FROM general_media WHERE delete_status=0 AND media_status=0");
							if(mysql_num_rows($sql_med_ci)>0)
							{
								while($row_mede = mysql_fetch_assoc($sql_med_ci))
								{
									if($row_mede['from_info']!="")
									{
										$exp_from = explode('|',$row_mede['from_info']);
										if($exp_from[0]=='general_artist')
										{
											$sql_get_tables = mysql_query("SELECT * FROM $exp_from[0] WHERE artist_id='".$exp_from[1]."' AND city='".$city_search."' AND status=0 AND active=0");
										}
										elseif($exp_from[0]=='general_community')
										{
											$sql_get_tables = mysql_query("SELECT * FROM $exp_from[0] WHERE community_id='".$exp_from[1]."' AND city='".$city_search."' AND status=0 AND active=0");
										}
										elseif($exp_from[0]=='community_event' || $exp_from[0]=='artist_event' || $exp_from[0]=='artist_project' || $exp_from[0]=='community_project')
										{
											$sql_get_tables = mysql_query("SELECT * FROM $exp_from[0] WHERE id='".$exp_from[1]."' AND city='".$city_search."' AND del_status=0 AND active=0");
										}
										
										if(isset($sql_get_tables) && !empty($sql_get_tables))
										{
											if(mysql_num_rows($sql_get_tables)>0)
											{
												$sql_get_med = mysql_query("SELECT * FROM general_media WHERE id='".$row_mede['id']."' AND delete_status=0 AND media_status=0");
												$ans_med_ci[] = mysql_fetch_assoc($sql_get_med);
											}
										}
									}
									elseif($row_mede['creator_info']!="")
									{
										$exp_from = explode('|',$row_mede['creator_info']);
										if($exp_from[0]=='general_artist')
										{
											$sql_get_tables = mysql_query("SELECT * FROM $exp_from[0] WHERE artist_id='".$exp_from[1]."' city='".$city_search."' AND status=0 AND active=0");
										}
										elseif($exp_from[0]=='general_community')
										{
											$sql_get_tables = mysql_query("SELECT * FROM $exp_from[0] WHERE community_id='".$exp_from[1]."' AND city='".$city_search."' AND status=0 AND active=0");
										}
										elseif($exp_from[0]=='community_event' || $exp_from[0]=='artist_event' || $exp_from[0]=='artist_project' || $exp_from[0]=='community_project')
										{
											$sql_get_tables = mysql_query("SELECT * FROM $exp_from[0] WHERE id='".$exp_from[1]."' AND country_id='".$country_search."' AND city='".$city_search."' AND del_status=0 AND active=0");
										}
										
										
										if(isset($sql_get_tables) && !empty($sql_get_tables))
										{
											if(mysql_num_rows($sql_get_tables)>0)
											{
												$sql_get_med = mysql_query("SELECT * FROM general_media WHERE id='".$row_mede['id']."' AND delete_status=0 AND media_status=0");
												$ans_med_ci[] = mysql_fetch_assoc($sql_get_med);
											}
										}
									}
								}
							}
						}
						
						if(isset($sql_art_ci))
						{
							if(mysql_num_rows($sql_art_ci)>0)
							{
								while($row_art_ci = mysql_fetch_assoc($sql_art_ci))
								{
									$ans_art_ci[] = $row_art_ci;
								}
							}
						}
						
						if(isset($sql_com_ci))
						{
							if(mysql_num_rows($sql_com_ci)>0)
							{
								while($row_com_ci = mysql_fetch_assoc($sql_com_ci))
								{
									$ans_com_ci[] = $row_com_ci;
								}
							}
						}
						
						if(isset($sql_arte_ci))
						{
							if(mysql_num_rows($sql_arte_ci)>0)
							{
								while($row_arte_ci = mysql_fetch_assoc($sql_arte_ci))
								{
									$row_arte_ci['table_name'] = 'artist_event';
									$ans_arte_ci[] = $row_arte_ci;
								}
							}
						}
						
						if(isset($sql_come_ci))
						{
							if(mysql_num_rows($sql_come_ci)>0)
							{
								while($row_come_ci = mysql_fetch_assoc($sql_come_ci))
								{
									$row_come_ci['table_name'] = 'community_event';
									$ans_come_ci[] = $row_come_ci;
								}
							}
						}
						
						if(isset($sql_artp_ci))
						{
							if(mysql_num_rows($sql_artp_ci)>0)
							{
								while($row_artp_ci = mysql_fetch_assoc($sql_artp_ci))
								{
									$row_artp_ci['table_name'] = 'artist_project';
									$ans_artp_ci[] = $row_artp_ci;
								}
							}
						}
						
						if(isset($sql_comp_ci))
						{
							if(mysql_num_rows($sql_comp_ci)>0)
							{
								while($row_comp_ci = mysql_fetch_assoc($sql_comp_ci))
								{
									$row_comp_ci['table_name'] = 'community_project';
									$ans_comp_ci[] = $row_comp_ci;
								}
							}
						}
						
						$main_city = array_merge($ans_art_ci,$ans_com_ci,$ans_arte_ci,$ans_come_ci,$ans_artp_ci,$ans_comp_ci,$ans_med_ci);
						return $main_city;
					}
					elseif($page_type_search!=0)
					{
						if($profile_type_search=='artist')
						{
							$sql_art_ci = mysql_query("SELECT * FROM general_artist WHERE city='".$city_search."' AND status=0 AND active=0 AND image_name!='' AND profile_url!=''");
						}
						elseif($profile_type_search=='community')
						{
							$sql_com_ci = mysql_query("SELECT * FROM general_community WHERE city='".$city_search."' AND status=0 AND active=0 AND image_name!='' AND profile_url!=''");
						}
						elseif($profile_type_search=='event')
						{
							$sql_arte_ci = mysql_query("SELECT * FROM artist_event WHERE city='".$city_search."' AND del_status=0 AND active=0 AND image_name!='' AND profile_url!=''");
							$sql_come_ci = mysql_query("SELECT * FROM community_event WHERE city='".$city_search."' AND del_status=0 AND active=0 AND image_name!='' AND profile_url!=''");
						}
						elseif($profile_type_search=='project')
						{
							$sql_artp_ci = mysql_query("SELECT * FROM artist_project WHERE city='".$city_search."' AND del_status=0 AND active=0 AND image_name!='' AND profile_url!=''");
							$sql_comp_ci = mysql_query("SELECT * FROM community_project WHERE city='".$city_search."' AND del_status=0 AND active=0 AND image_name!='' AND profile_url!=''");
						}
						elseif($profile_type_search=='media')
						{
							$sql_med = mysql_query("SELECT * FROM general_media WHERE delete_status=0 AND media_status=0");
							if(mysql_num_rows($sql_med)>0)
							{
								$mj_p_s = 0;
								while($row_mede = mysql_fetch_assoc($sql_med))
								{
									if($row_mede['from_info']!="")
									{
										$exp_from = explode('|',$row_mede['from_info']);
										if($exp_from[0]=='general_artist')
										{
											$sql_get_tables = mysql_query("SELECT * FROM $exp_from[0] WHERE artist_id='".$exp_from[1]."' AND country_id='".$country_search."' AND city='".$city_search."' AND status=0 AND active=0");
										}
										elseif($exp_from[0]=='general_community')
										{
											$sql_get_tables = mysql_query("SELECT * FROM $exp_from[0] WHERE community_id='".$exp_from[1]."' AND country_id='".$country_search."' AND city='".$city_search."' AND status=0 AND active=0");
										}
										elseif($exp_from[0]=='community_event' || $exp_from[0]=='artist_event' || $exp_from[0]=='artist_project' || $exp_from[0]=='community_project')
										{
											$sql_get_tables = mysql_query("SELECT * FROM $exp_from[0] WHERE id='".$exp_from[1]."' AND country_id='".$country_search."' AND city='".$city_search."' AND del_status=0 AND active=0");
										}
										
										if(isset($sql_get_tables) && !empty($sql_get_tables))
										{
											if(mysql_num_rows($sql_get_tables)>0)
											{
												$sql_get_med = mysql_query("SELECT * FROM general_media WHERE id='".$row_mede['id']."' AND delete_status=0 AND media_status=0");
												
												while($row_csc = mysql_fetch_assoc($sql_get_med))
												{
													if($sub_type_search!=0 && $meta_type_search==0)
													{
														$sql_page_typeme = mysql_query("SELECT MAX( id ) FROM media_profiles_type WHERE type_id ='".$page_type_search."' AND subtype_id='".$sub_type_search."' AND media_id='".$row_csc['id']."'");
													}
													elseif($sub_type_search!=0 && $meta_type_search!=0)
													{
														$sql_page_typeme = mysql_query("SELECT MAX( id ) FROM media_profiles_type WHERE type_id ='".$page_type_search."' AND subtype_id='".$sub_type_search."' AND metatype_id='".$meta_type_search."' AND media_id='".$row_csc['id']."'");
													}
													else
													{
														$sql_page_typeme = mysql_query("SELECT MAX( id ) FROM media_profiles_type WHERE type_id ='".$page_type_search."' AND media_id='".$row_csc['id']."'");
													}
													
													$run_page_typeme = mysql_fetch_assoc($sql_page_typeme);
													
													if($run_page_typeme['MAX( id )']!=NULL)
													{
														//echo "hii";
														$sql_met = mysql_query("SELECT * FROM general_media WHERE id='".$row_csc['id']."' AND delete_status=0 AND media_status=0");
														
														while($row_mnet = mysql_fetch_assoc($sql_met))
														{
															$ans_med_ci[$mj_p_s] = $row_mnet;
														}
													}
												}
											}
										}
									}
									elseif($row_mede['creator_info']!="")
									{
										$exp_from = explode('|',$row_mede['creator_info']);
										if($exp_from[0]=='general_artist')
										{
											$sql_get_tables = mysql_query("SELECT * FROM $exp_from[0] WHERE artist_id='".$exp_from[1]."' AND country_id='".$country_search."' AND city='".$city_search."' AND status=0 AND active=0");
										}
										elseif($exp_from[0]=='general_community')
										{
											$sql_get_tables = mysql_query("SELECT * FROM $exp_from[0] WHERE community_id='".$exp_from[1]."' AND country_id='".$country_search."' AND city='".$city_search."' AND status=0 AND active=0");
										}
										elseif($exp_from[0]=='community_event' || $exp_from[0]=='artist_event' || $exp_from[0]=='artist_project' || $exp_from[0]=='community_project')
										{
											$sql_get_tables = mysql_query("SELECT * FROM $exp_from[0] WHERE id='".$exp_from[1]."' AND country_id='".$country_search."' AND city='".$city_search."' AND del_status=0 AND active=0");
										}
										
										if(isset($sql_get_tables) && !empty($sql_get_tables))
										{
											if(mysql_num_rows($sql_get_tables)>0)
											{
												$sql_get_med = mysql_query("SELECT * FROM general_media WHERE id='".$row_mede['id']."' AND delete_status=0 AND media_status=0");
												
												while($row_csc = mysql_fetch_assoc($sql_get_med))
												{
													if($sub_type_search!=0 && $meta_type_search==0)
													{
														$sql_page_typeme = mysql_query("SELECT MAX( id ) FROM media_profiles_type WHERE type_id ='".$page_type_search."' AND subtype_id='".$sub_type_search."' AND media_id='".$row_csc['id']."'");
													}
													elseif($sub_type_search!=0 && $meta_type_search!=0)
													{
														$sql_page_typeme = mysql_query("SELECT MAX( id ) FROM media_profiles_type WHERE type_id ='".$page_type_search."' AND subtype_id='".$sub_type_search."' AND metatype_id='".$meta_type_search."' AND media_id='".$row_csc['id']."'");
													}
													else
													{
														$sql_page_typeme = mysql_query("SELECT MAX( id ) FROM media_profiles_type WHERE type_id ='".$page_type_search."' AND media_id='".$row_csc['id']."'");
													}
													
													$run_page_typeme = mysql_fetch_assoc($sql_page_typeme);
													
													if($run_page_typeme['MAX( id )']!=NULL)
													{
														//echo "hii";
														$sql_met = mysql_query("SELECT * FROM general_media WHERE id='".$row_csc['id']."' AND delete_status=0 AND media_status=0");
														
														while($row_mnet = mysql_fetch_assoc($sql_met))
														{
															$ans_med_ci[$mj_p_s] = $row_mnet;
														}
													}
												}
											}
										}
									}
									$mj_p_s = $mj_p_s + 1;
								}
							}
						}
						
						if(isset($sql_art_ci))
						{
							if(mysql_num_rows($sql_art_ci)>0)
							{
								$n = 0;
								while($row_art_ci = mysql_fetch_assoc($sql_art_ci))
								{
									$ans_art_cic = $row_art_ci;
									
									if($sub_type_search!=0 && $meta_type_search==0)
									{
										$sql_page_typecp = mysql_query("SELECT MAX( general_artist_profiles_id ) FROM general_artist_profiles WHERE type_id ='".$page_type_search."' AND subtype_id='".$sub_type_search."' AND artist_id='".$ans_art_cic['artist_id']."'");
									}
									elseif($sub_type_search!=0 && $meta_type_search!=0)
									{
										$sql_page_typecp = mysql_query("SELECT MAX( general_artist_profiles_id ) FROM general_artist_profiles WHERE type_id ='".$page_type_search."' AND subtype_id='".$sub_type_search."' AND metatype_id='".$meta_type_search."' AND artist_id='".$ans_art_cic['artist_id']."'");
									}
									else
									{
										$sql_page_typecp = mysql_query("SELECT MAX( general_artist_profiles_id ) FROM general_artist_profiles WHERE type_id ='".$page_type_search."' AND artist_id='".$ans_art_cic['artist_id']."'");
									}
									$run_page_typecpac = mysql_fetch_assoc($sql_page_typecp);
									
									if($run_page_typecpac['MAX( general_artist_profiles_id )']!=NULL)
									{
										//echo "hii";
										$sql_cptacs = mysql_query("SELECT * FROM general_artist WHERE artist_id='".$ans_art_cic['artist_id']."'");
										
										while($row_cptacs = mysql_fetch_assoc($sql_cptacs))
										{
											$ans_art_ci[$n] = $row_cptacs;
										}
										$n = $n + 1;
									}
								}
							}	
						}
						
						if(isset($sql_com_ci))
						{
							if(mysql_num_rows($sql_com_ci)>0)
							{
								$t = 0;
								while($row_com_ci = mysql_fetch_assoc($sql_com_ci))
								{
									$ans_com_cic = $row_com_ci;
									
									if($sub_type_search!=0 && $meta_type_search==0)
									{
										$sql_page_typecp = mysql_query("SELECT MAX( general_community_profiles_id ) FROM general_community_profiles WHERE type_id ='".$page_type_search."' AND subtype_id='".$sub_type_search."' AND community_id='".$ans_com_cic['community_id']."'");
									}
									elseif($sub_type_search!=0 && $meta_type_search!=0)
									{
										$sql_page_typecp = mysql_query("SELECT MAX( general_community_profiles_id ) FROM general_community_profiles WHERE type_id ='".$page_type_search."' AND subtype_id='".$sub_type_search."' AND metatype_id='".$meta_type_search."' AND community_id='".$ans_com_cic['community_id']."'");
									}
									else
									{
										$sql_page_typecp = mysql_query("SELECT MAX( general_community_profiles_id ) FROM general_community_profiles WHERE type_id ='".$page_type_search."' AND community_id='".$ans_com_cic['community_id']."'");
									}
									$run_page_typecpcc = mysql_fetch_assoc($sql_page_typecp);
									
									if($run_page_typecpcc['MAX( general_community_profiles_id )']!=NULL)
									{
										//echo "hii";
										$sql_cptccs = mysql_query("SELECT * FROM general_community WHERE community_id='".$ans_com_cic['community_id']."'");
										
										while($row_cptccs = mysql_fetch_assoc($sql_cptccs))
										{
											$ans_com_ci[$t] = $row_cptccs;
										}
										$t = $t + 1;
									}
								}
							}
						}
						
						if(isset($sql_arte_ci))
						{
							if(mysql_num_rows($sql_arte_ci)>0)
							{
								$r = 0;
								while($row_arte_ci = mysql_fetch_assoc($sql_arte_ci))
								{
									$ans_arte_cic = $row_arte_ci;
									
									if($sub_type_search!=0 && $meta_type_search==0)
									{
										$sql_page_typecp = mysql_query("SELECT MAX( id ) FROM artist_event_profiles WHERE type_id ='".$page_type_search."' AND subtype_id='".$sub_type_search."' AND artist_event_id='".$ans_arte_cic['id']."'");
									}
									elseif($sub_type_search!=0 && $meta_type_search!=0)
									{
										$sql_page_typecp = mysql_query("SELECT MAX( id ) FROM artist_event_profiles WHERE type_id ='".$page_type_search."' AND subtype_id='".$sub_type_search."' AND metatype_id='".$meta_type_search."' AND artist_event_id='".$ans_arte_cic['id']."'");
									}
									else
									{
										$sql_page_typecp = mysql_query("SELECT MAX( id ) FROM artist_event_profiles WHERE type_id ='".$page_type_search."' AND artist_event_id='".$ans_arte_cic['id']."'");
									}
									$run_page_typecpaes = mysql_fetch_assoc($sql_page_typecp);
									
									if($run_page_typecpaes['MAX( id )']!=NULL)
									{
										//echo "hii";
										$sql_cptaes = mysql_query("SELECT * FROM artist_event WHERE id='".$ans_arte_cic['id']."'");
										
										while($row_cptaes = mysql_fetch_assoc($sql_cptaes))
										{
											$row_cptaes['table_name'] = 'artist_event';
											$ans_arte_ci[$r] = $row_cptaes;
										}
										$r = $r + 1;
									}
								}
							}
						}
						
						if(isset($sql_come_ci))
						{
							if(mysql_num_rows($sql_come_ci)>0)
							{
								$v = 0;
								while($row_come_ci = mysql_fetch_assoc($sql_come_ci))
								{
									$ans_come_cic = $row_come_ci;
									
									if($sub_type_search!=0 && $meta_type_search==0)
									{
										$sql_page_typecp = mysql_query("SELECT MAX( id ) FROM community_event_profiles WHERE type_id ='".$page_type_search."' AND subtype_id='".$sub_type_search."' AND community_event_id='".$ans_come_cic['id']."'");
									}
									elseif($sub_type_search!=0 && $meta_type_search!=0)
									{
										$sql_page_typecp = mysql_query("SELECT MAX( id ) FROM community_event_profiles WHERE type_id ='".$page_type_search."' AND subtype_id='".$sub_type_search."' AND metatype_id='".$meta_type_search."' AND community_event_id='".$ans_come_cic['id']."'");
									}
									else
									{
										$sql_page_typecp = mysql_query("SELECT MAX( id ) FROM community_event_profiles WHERE type_id ='".$page_type_search."' AND community_event_id='".$ans_come_cic['id']."'");
									}
									$run_page_typecpaes = mysql_fetch_assoc($sql_page_typecp);
									
									if($run_page_typecpaes['MAX( id )']!=NULL)
									{
										//echo "hii";
										$sql_cptaes = mysql_query("SELECT * FROM community_event WHERE id='".$ans_come_cic['id']."'");
										
										while($row_cptaes = mysql_fetch_assoc($sql_cptaes))
										{
											$row_cptaes['table_name'] = 'community_event';
											$ans_come_ci[$v] = $row_cptaes;
										}
										$v = $v + 1;
									}
								}
							}
						}
						
						if(isset($sql_artp_ci))
						{
							if(mysql_num_rows($sql_artp_ci)>0)
							{
								$z = 0;
								while($row_artp_ci = mysql_fetch_assoc($sql_artp_ci))
								{
									$ans_artp_cic = $row_artp_ci;
									
									if($sub_type_search!=0 && $meta_type_search==0)
									{
										$sql_page_typecp = mysql_query("SELECT MAX( id ) FROM artist_project_profiles WHERE type_id ='".$page_type_search."' AND subtype_id='".$sub_type_search."' AND artist_project_id='".$ans_artp_cic['id']."'");
									}
									elseif($sub_type_search!=0 && $meta_type_search!=0)
									{
										$sql_page_typecp = mysql_query("SELECT MAX( id ) FROM artist_project_profiles WHERE type_id ='".$page_type_search."' AND subtype_id='".$sub_type_search."' AND metatype_id='".$meta_type_search."' AND artist_project_id='".$ans_artp_cic['id']."'");
									}
									else
									{
										$sql_page_typecp = mysql_query("SELECT MAX( id ) FROM artist_project_profiles WHERE type_id ='".$page_type_search."' AND artist_project_id='".$ans_artp_cic['id']."'");
									}
									$run_page_typeccss = mysql_fetch_assoc($sql_page_typecp);
									
									if($run_page_typeccss['MAX( id )']!=NULL)
									{
										//echo "hii";
										$sql_cptitcs = mysql_query("SELECT * FROM artist_project WHERE id='".$ans_artp_cic['id']."' AND del_status=0 AND active=0");
										
										while($row_cpttrs = mysql_fetch_assoc($sql_cptitcs))
										{
											$row_cpttrs['table_name'] = 'artist_project';
											$ans_artp_ci[$z] = $row_cpttrs;
										}
										$z = $z + 1;
									}
								}
							}
						}
						
						if(isset($sql_comp_ci))
						{
							if(mysql_num_rows($sql_comp_ci)>0)
							{
								$b = 0;
								while($row_comp_ci = mysql_fetch_assoc($sql_comp_ci))
								{
									$ans_comp_cic = $row_comp_ci;
									
									if($sub_type_search!=0 && $meta_type_search==0)
									{
										$sql_page_typecp = mysql_query("SELECT MAX( id ) FROM community_project_profiles WHERE type_id ='".$page_type_search."' AND subtype_id='".$sub_type_search."' AND community_project_id='".$ans_comp_cic['id']."'");
									}
									elseif($sub_type_search!=0 && $meta_type_search!=0)
									{
										$sql_page_typecp = mysql_query("SELECT MAX( id ) FROM community_project_profiles WHERE type_id ='".$page_type_search."' AND subtype_id='".$sub_type_search."' AND metatype_id='".$meta_type_search."' AND community_project_id='".$ans_comp_cic['id']."'");
									}
									else
									{
										$sql_page_typecp = mysql_query("SELECT MAX( id ) FROM community_project_profiles WHERE type_id ='".$page_type_search."' AND community_project_id='".$ans_comp_cic['id']."'");
									}
									$run_page_typeccss = mysql_fetch_assoc($sql_page_typecp);
									
									if($run_page_typeccss['MAX( id )']!=NULL)
									{
										//echo "hii";
										$sql_cptietcs = mysql_query("SELECT * FROM community_project WHERE id='".$ans_comp_cic['id']."' AND del_status=0 AND active=0");
										
										while($row_cptetrs = mysql_fetch_assoc($sql_cptietcs))
										{
											$row_cptetrs['table_name'] = 'community_project';
											$ans_comp_ci[$b] = $row_cptetrs;
										}
										$b = $b + 1;
									}
								}
							}
						}
						
						$main_city = array_merge($ans_art_ci,$ans_com_ci,$ans_arte_ci,$ans_come_ci,$ans_artp_ci,$ans_comp_ci,$ans_med_ci);
						return $main_city;
					}
				}
			}
			
			////////////////////////////////////Only Match String GET Search////////////////////////////////////
			if(($profile_type_search=="select" || $profile_type_search==NULL) && ($page_type_search==0 || $page_type_search==NULL) && ($sub_type_search==0 || $sub_type_search==NULL) && ($meta_type_search==0 || $meta_type_search==NULL) && ($country_search==0 || $country_search==NULL) && ($state_search==0 || $state_search==NULL) && ($city_search=="" || $city_search==NULL) && $match_string!="Search" && $match_table!="" && $match_id!="" && $alpha=="")
			{
				// echo "f6";
				// $re = "f6";
				// return $re;
				$match_user = array();
				$match_art_user = array();
				$match_med_user = array();
				$match_com_user = array();
				$match_artp_user = array();
				$match_come_user = array();
				$match_comp_user = array();
				$match_arte_user = array();
				
				if($match_table=="general_artist")
				{
					$sql_string = mysql_query("SELECT * FROM $match_table WHERE artist_id='".$match_id."' AND status=0 AND active=0 AND image_name!='' AND profile_url!=''");
				}
				
				if($match_table=="general_community")
				{
					$sql_string = mysql_query("SELECT * FROM $match_table WHERE community_id='".$match_id."' AND status=0 AND active=0 AND image_name!='' AND profile_url!=''");
				}
				
				if($match_table=="artist_event" || $match_table=="artist_project" || $match_table=="community_event" || $match_table=="community_project")
				{
					$sql_string = mysql_query("SELECT * FROM $match_table WHERE id='".$match_id."' AND del_status=0 AND active=0 AND image_name!='' AND profile_url!=''");
				}
				
				if($match_table=="general_media")
				{
					$sql_string = mysql_query("SELECT * FROM $match_table WHERE id='".$match_id."' AND delete_status=0 AND media_status=0");
				}
				
				if($match_table=="type")
				{
					$sql_metyps = mysql_query("SELECT * FROM $match_table WHERE type_id='".$match_id."' AND `delete`='0'");
					if($sql_metyps!="")
					{
						if(mysql_num_rows($sql_metyps)>0)
						{
							$ans_metyps = mysql_fetch_assoc($sql_metyps);
							
							$sql_al_tnaam = mysql_query("SELECT * FROM $match_table WHERE name='".$ans_metyps['name']."' AND `delete`=0");
							while($ans_al_tnams = mysql_fetch_assoc($sql_al_tnaam))
							{
								$ids .= $ans_al_tnams['type_id'].',';
							}
							
							$ids = trim($ids,',');
							
							$sql_art = mysql_query("SELECT DISTINCT gae.artist_id,ga.* as art FROM general_artist_profiles gae LEFT JOIN general_artist ga ON gae.artist_id=ga.artist_id WHERE gae.type_id IN ($ids) AND ga.artist_id!=0 AND ga.del_status=0 AND ga.status=0 AND ga.active=0");
							if($sql_art!="")
							{
								while($row_art = mysql_fetch_assoc($sql_art))
								{
									$row_art["table_name"] = 'general_artist';
									$match_art_user[] = $row_art;
								}
							}
							
							$sql_com = mysql_query("SELECT DISTINCT gae.community_id,ga.* as art FROM general_community_profiles gae LEFT JOIN general_community ga ON gae.community_id=ga.community_id WHERE gae.type_id IN ($ids) AND ga.community_id!=0 AND ga.del_status=0 AND ga.status=0 AND ga.active=0");
							if($sql_com!="")
							{
								while($row_com = mysql_fetch_assoc($sql_com))
								{
									$row_com["table_name"] = 'general_community';
									$match_com_user[] = $row_com;
								}
							}
							
							$sql_artp = mysql_query("SELECT DISTINCT gae.artist_project_id,ga.* as art FROM artist_project_profiles gae LEFT JOIN artist_project ga ON gae.artist_project_id=ga.id WHERE gae.type_id IN ($ids) AND ga.id!=0 AND ga.del_status=0 AND ga.active=0");
							if($sql_artp!="")
							{
								while($row_artp = mysql_fetch_assoc($sql_artp))
								{
									$row_artp["table_name"] = 'artist_project';
									$match_artp_user[] = $row_artp;
								}
							}
							
							$sql_comp = mysql_query("SELECT DISTINCT gae.community_project_id,ga.* as art FROM community_project_profiles gae LEFT JOIN community_project ga ON gae.community_project_id=ga.id WHERE gae.type_id IN ($ids) AND ga.id!=0 AND ga.del_status=0 AND ga.active=0");
							if($sql_comp!="")
							{
								while($row_comp = mysql_fetch_assoc($sql_comp))
								{
									$row_comp["table_name"] = 'community_project';
									$match_comp_user[] = $row_comp;
								}
							}
							
							$sql_come = mysql_query("SELECT DISTINCT gae.community_event_id,ga.* as art FROM community_event_profiles gae LEFT JOIN community_event ga ON gae.community_event_id=ga.id WHERE gae.type_id IN ($ids) AND ga.id!=0 AND ga.del_status=0 AND ga.active=0");
							if($sql_come!="")
							{
								while($row_come = mysql_fetch_assoc($sql_come))
								{
									$row_come["table_name"] = 'artist_event';
									$match_come_user[] = $row_come;
								}
							}
							
							$sql_arte = mysql_query("SELECT DISTINCT gae.artist_event_id,ga.* as art FROM artist_event_profiles gae LEFT JOIN artist_event ga ON gae.artist_event_id=ga.id WHERE gae.type_id IN ($ids) AND ga.id!=0 AND ga.del_status=0 AND ga.active=0");
							if($sql_arte!="")
							{
								while($row_arte = mysql_fetch_assoc($sql_arte))
								{
									$row_arte["table_name"] = 'artist_event';
									$match_arte_user[] = $row_arte;
								}
							}
							
							$sql_med = mysql_query("SELECT DISTINCT gae.media_id,ga.* as art FROM media_profiles_type gae LEFT JOIN general_media ga ON gae.media_id=ga.id WHERE gae.type_id IN ($ids) AND ga.id!=0 AND ga.delete_status=0 AND ga.media_status=0");
							if($sql_med!="")
							{
								while($row_med = mysql_fetch_assoc($sql_med))
								{
									$row_med["table_name"] = 'general_media';
									$match_med_user[] = $row_med;
								}
							}
							
							$match_user = array_merge($match_art_user,$match_med_user,$match_com_user,$match_artp_user,$match_comp_user,$match_come_user,$match_arte_user);
						}
					}
				}
				
				if($match_table=="subtype")
				{
					$sql_metyps = mysql_query("SELECT * FROM $match_table WHERE subtype_id='".$match_id."' AND `delete`='0'");
					if($sql_metyps!="")
					{
						if(mysql_num_rows($sql_metyps)>0)
						{
							$ans_metyps = mysql_fetch_assoc($sql_metyps);
							
							$sql_al_tnaam = mysql_query("SELECT * FROM $match_table WHERE name='".$ans_metyps['name']."' AND `delete`=0");
							while($ans_al_tnams = mysql_fetch_assoc($sql_al_tnaam))
							{
								$ids .= $ans_al_tnams['subtype_id'].',';
							}
							
							$ids = trim($ids,',');
							
							$sql_art = mysql_query("SELECT DISTINCT gae.artist_id,ga.* as art FROM general_artist_profiles gae LEFT JOIN general_artist ga ON gae.artist_id=ga.artist_id WHERE gae.subtype_id IN ($ids) AND ga.artist_id!=0 AND ga.del_status=0 AND ga.status=0 AND ga.active=0");
							if($sql_art!="")
							{
								while($row_art = mysql_fetch_assoc($sql_art))
								{
									$row_art["table_name"] = 'general_artist';
									$match_art_user[] = $row_art;
								}
							}
							
							$sql_com = mysql_query("SELECT DISTINCT gae.community_id,ga.* as art FROM general_community_profiles gae LEFT JOIN general_community ga ON gae.community_id=ga.community_id WHERE gae.subtype_id IN ($ids) AND ga.community_id!=0 AND ga.del_status=0 AND ga.status=0 AND ga.active=0");
							if($sql_com!="")
							{
								while($row_com = mysql_fetch_assoc($sql_com))
								{
									$row_com["table_name"] = 'general_community';
									$match_com_user[] = $row_com;
								}
							}
							
							$sql_artp = mysql_query("SELECT DISTINCT gae.artist_project_id,ga.* as art FROM artist_project_profiles gae LEFT JOIN artist_project ga ON gae.artist_project_id=ga.id WHERE gae.subtype_id IN ($ids) AND ga.id!=0 AND ga.del_status=0 AND ga.active=0");
							if($sql_artp!="")
							{
								while($row_artp = mysql_fetch_assoc($sql_artp))
								{
									$row_artp["table_name"] = 'artist_project';
									$match_artp_user[] = $row_artp;
								}
							}
							
							$sql_comp = mysql_query("SELECT DISTINCT gae.community_project_id,ga.* as art FROM community_project_profiles gae LEFT JOIN community_project ga ON gae.community_project_id=ga.id WHERE gae.subtype_id IN ($ids) AND ga.id!=0 AND ga.del_status=0 AND ga.active=0");
							if($sql_comp!="")
							{
								while($row_comp = mysql_fetch_assoc($sql_comp))
								{
									$row_comp["table_name"] = 'community_project';
									$match_comp_user[] = $row_comp;
								}
							}
							
							$sql_come = mysql_query("SELECT DISTINCT gae.community_event_id,ga.* as art FROM community_event_profiles gae LEFT JOIN community_event ga ON gae.community_event_id=ga.id WHERE gae.subtype_id IN ($ids) AND ga.id!=0 AND ga.del_status=0 AND ga.active=0");
							if($sql_come!="")
							{
								while($row_come = mysql_fetch_assoc($sql_come))
								{
									$row_come["table_name"] = 'artist_event';
									$match_come_user[] = $row_come;
								}
							}
							
							$sql_arte = mysql_query("SELECT DISTINCT gae.artist_event_id,ga.* as art FROM artist_event_profiles gae LEFT JOIN artist_event ga ON gae.artist_event_id=ga.id WHERE gae.subtype_id IN ($ids) AND ga.id!=0 AND ga.del_status=0 AND ga.active=0");
							if($sql_arte!="")
							{
								while($row_arte = mysql_fetch_assoc($sql_arte))
								{
									$row_arte["table_name"] = 'artist_event';
									$match_arte_user[] = $row_arte;
								}
							}
							
							$sql_med = mysql_query("SELECT DISTINCT gae.media_id,ga.* as art FROM media_profiles_type gae LEFT JOIN general_media ga ON gae.media_id=ga.id WHERE gae.subtype_id IN ($ids) AND ga.id!=0 AND ga.delete_status=0 AND ga.media_status=0");
							if($sql_med!="")
							{
								while($row_med = mysql_fetch_assoc($sql_med))
								{
									$row_med["table_name"] = 'general_media';
									$match_med_user[] = $row_med;
								}
							}
							
							$match_user = array_merge($match_art_user,$match_med_user,$match_com_user,$match_artp_user,$match_comp_user,$match_come_user,$match_arte_user);
						}
					}
				}
				
				if($match_table=="meta_type")
				{
					$sql_metyps = mysql_query("SELECT * FROM $match_table WHERE meta_id='".$match_id."' AND `delete`='0'");
					if($sql_metyps!="")
					{
						if(mysql_num_rows($sql_metyps)>0)
						{
							$ans_metyps = mysql_fetch_assoc($sql_metyps);
							
							$sql_al_tnaam = mysql_query("SELECT * FROM $match_table WHERE name='".$ans_metyps['name']."' AND `delete`=0");
							while($ans_al_tnams = mysql_fetch_assoc($sql_al_tnaam))
							{
								$ids .= $ans_al_tnams['meta_id'].',';
							}
							
							$ids = trim($ids,',');
							
							$sql_art = mysql_query("SELECT DISTINCT gae.artist_id,ga.* as art FROM general_artist_profiles gae LEFT JOIN general_artist ga ON gae.artist_id=ga.artist_id WHERE gae.metatype_id IN ($ids) AND ga.artist_id!=0 AND ga.del_status=0 AND ga.status=0 AND ga.active=0");
							if($sql_art!="")
							{
								while($row_art = mysql_fetch_assoc($sql_art))
								{
									$row_art["table_name"] = 'general_artist';
									$match_art_user[] = $row_art;
								}
							}
							
							$sql_com = mysql_query("SELECT DISTINCT gae.community_id,ga.* as art FROM general_community_profiles gae LEFT JOIN general_community ga ON gae.community_id=ga.community_id WHERE gae.metatype_id IN ($ids) AND ga.community_id!=0 AND ga.del_status=0 AND ga.status=0 AND ga.active=0");
							if($sql_com!="")
							{
								while($row_com = mysql_fetch_assoc($sql_com))
								{
									$row_com["table_name"] = 'general_community';
									$match_com_user[] = $row_com;
								}
							}
							
							$sql_artp = mysql_query("SELECT DISTINCT gae.artist_project_id,ga.* as art FROM artist_project_profiles gae LEFT JOIN artist_project ga ON gae.artist_project_id=ga.id WHERE gae.metatype_id IN ($ids) AND ga.id!=0 AND ga.del_status=0 AND ga.active=0");
							if($sql_artp!="")
							{
								while($row_artp = mysql_fetch_assoc($sql_artp))
								{
									$row_artp["table_name"] = 'artist_project';
									$match_artp_user[] = $row_artp;
								}
							}
							
							$sql_comp = mysql_query("SELECT DISTINCT gae.community_project_id,ga.* as art FROM community_project_profiles gae LEFT JOIN community_project ga ON gae.community_project_id=ga.id WHERE gae.metatype_id IN ($ids) AND ga.id!=0 AND ga.del_status=0 AND ga.active=0");
							if($sql_comp!="")
							{
								while($row_comp = mysql_fetch_assoc($sql_comp))
								{
									$row_comp["table_name"] = 'community_project';
									$match_comp_user[] = $row_comp;
								}
							}
							
							$sql_come = mysql_query("SELECT DISTINCT gae.community_event_id,ga.* as art FROM community_event_profiles gae LEFT JOIN community_event ga ON gae.community_event_id=ga.id WHERE gae.metatype_id IN ($ids) AND ga.id!=0 AND ga.del_status=0 AND ga.active=0");
							if($sql_come!="")
							{
								while($row_come = mysql_fetch_assoc($sql_come))
								{
									$row_come["table_name"] = 'artist_event';
									$match_come_user[] = $row_come;
								}
							}
							
							$sql_arte = mysql_query("SELECT DISTINCT gae.artist_event_id,ga.* as art FROM artist_event_profiles gae LEFT JOIN artist_event ga ON gae.artist_event_id=ga.id WHERE gae.metatype_id IN ($ids) AND ga.id!=0 AND ga.del_status=0 AND ga.active=0");
							if($sql_arte!="")
							{
								while($row_arte = mysql_fetch_assoc($sql_arte))
								{
									$row_arte["table_name"] = 'artist_event';
									$match_arte_user[] = $row_arte;
								}
							}
							
							$sql_med = mysql_query("SELECT DISTINCT gae.media_id,ga.* as art FROM media_profiles_type gae LEFT JOIN general_media ga ON gae.media_id=ga.id WHERE gae.metatype_id IN ($ids) AND ga.id!=0 AND ga.delete_status=0 AND ga.media_status=0");
							if($sql_med!="")
							{
								while($row_med = mysql_fetch_assoc($sql_med))
								{
									$row_med["table_name"] = 'general_media';
									$match_med_user[] = $row_med;
								}
							}
							
							$match_user = array_merge($match_art_user,$match_med_user,$match_com_user,$match_artp_user,$match_comp_user,$match_come_user,$match_arte_user);
						}
					}
				}
				
				/* if($match_table=="general_artist_gallery_list" || $match_table=="general_community_gallery_list")
				{
					$sql_string = mysql_query("SELECT * FROM $match_table WHERE gallery_id='".$match_id."'");
				}
				
				if($match_table=="general_artist_audio" || $match_table=="general_community_audio")
				{
					$sql_string = mysql_query("SELECT * FROM $match_table WHERE audio_id='".$match_id."'");
				}
				
				if($match_table=="general_artist_video" || $match_table=="general_community_video")
				{
					$sql_string = mysql_query("SELECT * FROM $match_table WHERE video_id='".$match_id."'");
				} */
				
				if($match_table!="type" && $match_table!="subtype" && $match_table!="meta_type")
				{
					if(isset($sql_string) && !empty($sql_string))
					{
						if(mysql_num_rows($sql_string)>0)
						{
							while($row_user = mysql_fetch_assoc($sql_string))
							{
								if($match_table=="general_artist_video")
								{
									$row_user["table_name"] = "general_artist_video";
								}
								elseif($match_table=="general_artist_audio")
								{
									$row_user["table_name"] = "general_artist_audio";
								}
								elseif($match_table=="general_artist_gallery_list")
								{
									$row_user["table_name"] = "general_artist_gallery_list";
								}
								elseif($match_table=="general_community_video")
								{
									$row_user["table_name"] = "general_community_video";
								}
								elseif($match_table=="general_community_audio")
								{
									$row_user["table_name"] = "general_community_audio";
								}
								elseif($match_table=="general_community_gallery_list")
								{
									$row_user["table_name"] = "general_community_gallery_list";
								}
								elseif($match_table=="artist_event")
								{
									$row_user["table_name"] = "artist_event";
								}
								elseif($match_table=="community_event")
								{
									$row_user["table_name"] = "community_event";
								}
								elseif($match_table=="community_project")
								{
									$row_user["table_name"] = "community_project";
								}
								elseif($match_table=="artist_project")
								{
									$row_user["table_name"] = "artist_project";
								}
								 
								$match_user[] = $row_user;
							}
						}
					}
				}
				
				return $match_user;
			}
			
			/////////Match String GET Search with Profile and Country and Page type and City search///////////////////
			if($match_string!="Search" && ($profile_type_search!="select" || $country_search!=0 || $page_type_search!=0 || $city_search!="") && $alpha=="")
			{
				// echo "f5";
				// $re = "f5";
				// return $re;
				$match_user = array();
				
				if($profile_type_search!="select")
				{
					if($profile_type_search=='artist')
					{
						if($match_table=="general_artist")
						{
							if($country_search!=0 && $page_type_search==0)
							{
								if($state_search!=0 && $city_search!="")
								{
									$sql_string = mysql_query("SELECT * FROM $match_table WHERE artist_id='".$match_id."' AND country_id='".$country_search."' AND state_id='".$state_search."' AND city='".$city_search."' AND status=0 AND active=0 AND image_name!='' AND profile_url!=''");
								}
								elseif($state_search!=0 && $city_search=="")
								{
									$sql_string = mysql_query("SELECT * FROM $match_table WHERE artist_id='".$match_id."' AND country_id='".$country_search."' AND state_id='".$state_search."' AND  status=0 AND active=0 AND image_name!='' AND profile_url!=''");
								}
								elseif($state_search==0 && $city_search!="")
								{
									$sql_string = mysql_query("SELECT * FROM $match_table WHERE artist_id='".$match_id."' AND country_id='".$country_search."' AND city='".$city_search."' AND status=0 AND active=0 AND image_name!='' AND profile_url!=''");
								}
								else
								{
									$sql_string = mysql_query("SELECT * FROM $match_table WHERE artist_id='".$match_id."' AND country_id='".$country_search."' AND status=0 AND active=0 AND image_name!='' AND profile_url!=''");
								}
							}
							elseif($country_search==0 && $page_type_search!=0 && $city_search=="")
							{
								$sql_all_art = mysql_query("SELECT * FROM $match_table WHERE artist_id='".$match_id."' AND status=0 AND active=0 AND image_name!='' AND profile_url!=''");
								if($sub_type_search!=0 && $meta_type_search!=0)
								{
									if(mysql_num_rows($sql_all_art)>0)
									{
										$sql_page_typea = mysql_query("SELECT MAX( general_artist_profiles_id ) FROM general_artist_profiles WHERE type_id ='".$page_type_search."' AND subtype_id='".$sub_type_search."' AND metatype_id='".$meta_type_search."' AND artist_id='".$match_id."'");
										$run_page_typea = mysql_fetch_assoc($sql_page_typea);
										if($run_page_typea['MAX( general_artist_profiles_id )']!=NULL)
										{
											//echo "hii";
											$sql_string = mysql_query("SELECT * FROM $match_table WHERE artist_id='".$match_id."'");
										}
									}
								}
								elseif($sub_type_search!=0 && $meta_type_search==0)
								{
									if(mysql_num_rows($sql_all_art)>0)
									{
										$sql_page_typea = mysql_query("SELECT MAX( general_artist_profiles_id ) FROM general_artist_profiles WHERE type_id ='".$page_type_search."' AND subtype_id='".$sub_type_search."' AND artist_id='".$match_id."'");
										$run_page_typea = mysql_fetch_assoc($sql_page_typea);
										if($run_page_typea['MAX( general_artist_profiles_id )']!=NULL)
										{
											//echo "hii";
											$sql_string = mysql_query("SELECT * FROM $match_table WHERE artist_id='".$match_id."'");
										}
									}
								}
								else
								{
									if(mysql_num_rows($sql_all_art)>0)
									{
										$sql_page_typea = mysql_query("SELECT MAX( general_artist_profiles_id ) FROM general_artist_profiles WHERE type_id ='".$page_type_search."' AND artist_id='".$match_id."'");
										$run_page_typea = mysql_fetch_assoc($sql_page_typea);
										if($run_page_typea['MAX( general_artist_profiles_id )']!=NULL)
										{
											$sql_string = mysql_query("SELECT * FROM $match_table WHERE artist_id='".$match_id."'");
										}
									}
								}
							}
							elseif($country_search!=0 && $page_type_search!=0)
							{
								if($state_search!=0 && $city_search=="")
								{
									$sql_all_art = mysql_query("SELECT * FROM $match_table WHERE artist_id='".$match_id."' AND country_id='".$country_search."' AND state_id='".$state_search."' AND status=0 AND active=0 AND image_name!='' AND profile_url!=''");
								}
								elseif($state_search!=0 && $city_search!="")
								{
									$sql_all_art = mysql_query("SELECT * FROM $match_table WHERE artist_id='".$match_id."' AND country_id='".$country_search."' AND state_id='".$state_search."' AND city='".$city_search."' AND status=0 AND active=0 AND image_name!='' AND profile_url!=''");
								}
								elseif($state_search==0 && $city_search!="")
								{
									$sql_all_art = mysql_query("SELECT * FROM $match_table WHERE artist_id='".$match_id."' AND country_id='".$country_search."' AND city='".$city_search."' AND status=0 AND active=0 AND image_name!='' AND profile_url!=''");
								}
								else
								{
									$sql_all_art = mysql_query("SELECT * FROM $match_table WHERE artist_id='".$match_id."' AND country_id='".$country_search."' AND status=0 AND active=0 AND image_name!='' AND profile_url!=''");
								}
								if(mysql_num_rows($sql_all_art)>0)
								{	
									if($sub_type_search!=0 && $meta_type_search==0)
									{
										$sql_page_typea = mysql_query("SELECT MAX( general_artist_profiles_id ) FROM general_artist_profiles WHERE type_id ='".$page_type_search."' AND subtype_id='".$sub_type_search."' AND artist_id='".$match_id."'");
									}
									elseif($sub_type_search!=0 && $meta_type_search!=0)
									{
										$sql_page_typea = mysql_query("SELECT MAX( general_artist_profiles_id ) FROM general_artist_profiles WHERE type_id ='".$page_type_search."' AND subtype_id='".$sub_type_search."' AND metatype_id='".$meta_type_search."' AND artist_id='".$match_id."'");
									}
									else
									{
										$sql_page_typea = mysql_query("SELECT MAX( general_artist_profiles_id ) FROM general_artist_profiles WHERE type_id ='".$page_type_search."' AND artist_id='".$match_id."'");
									}
									$run_page_typea = mysql_fetch_assoc($sql_page_typea);
									if($run_page_typea['MAX( general_artist_profiles_id )']!=NULL)
									{
										//echo "hii";
										$sql_string = mysql_query("SELECT * FROM $match_table WHERE artist_id='".$match_id."'");
									}
								}
							}
							elseif($city_search!="" && $country_search==0)
							{
								if($page_type_search!=0)
								{
									$sql_all_art = mysql_query("SELECT * FROM $match_table WHERE artist_id='".$match_id."' AND city='".$city_search."' AND status=0 AND active=0 AND image_name!='' AND profile_url!=''");
									
									if(mysql_num_rows($sql_all_art)>0)
									{	
										if($sub_type_search!=0 && $meta_type_search==0)
										{
											$sql_page_typea = mysql_query("SELECT MAX( general_artist_profiles_id ) FROM general_artist_profiles WHERE type_id ='".$page_type_search."' AND subtype_id='".$sub_type_search."' AND artist_id='".$match_id."'");
										}
										elseif($sub_type_search!=0 && $meta_type_search!=0)
										{
											$sql_page_typea = mysql_query("SELECT MAX( general_artist_profiles_id ) FROM general_artist_profiles WHERE type_id ='".$page_type_search."' AND subtype_id='".$sub_type_search."' AND metatype_id='".$meta_type_search."' AND artist_id='".$match_id."'");
										}
										else
										{
											$sql_page_typea = mysql_query("SELECT MAX( general_artist_profiles_id ) FROM general_artist_profiles WHERE type_id ='".$page_type_search."' AND artist_id='".$match_id."'");
										}
										$run_page_typea = mysql_fetch_assoc($sql_page_typea);
										if($run_page_typea['MAX( general_artist_profiles_id )']!=NULL)
										{
											//echo "hii";
											$sql_string = mysql_query("SELECT * FROM $match_table WHERE artist_id='".$match_id."'");
										}
									}
								}
								elseif($page_type_search==0)
								{
									$sql_string = mysql_query("SELECT * FROM $match_table WHERE artist_id='".$match_id."' AND city='".$city_search."' AND status=0 AND active=0 AND image_name!='' AND profile_url!=''");
								}
							}
							elseif($page_type_search==0 && $city_search=="" && $country_search==0)
							{
								$sql_string = mysql_query("SELECT * FROM $match_table WHERE artist_id='".$match_id."' AND status=0 AND active=0 AND image_name!='' AND profile_url!=''");
							}
						}
					}
					
					if($profile_type_search=='community')
					{
						if($match_table=="general_community")
						{
							if($country_search!=0 && $page_type_search==0)
							{
								if($state_search!=0 && $city_search!="")
								{
									$sql_string = mysql_query("SELECT * FROM $match_table WHERE community_id='".$match_id."' AND country_id='".$country_search."' AND state_id='".$state_search."' AND city='".$city_search."' AND status=0 AND active=0 AND image_name!='' AND profile_url!=''");
								}
								elseif($state_search!=0 && $city_search=="")
								{
									$sql_string = mysql_query("SELECT * FROM $match_table WHERE community_id='".$match_id."' AND country_id='".$country_search."' AND state_id='".$state_search."' AND  status=0 AND active=0 AND image_name!='' AND profile_url!=''");
								}
								elseif($state_search==0 && $city_search!="")
								{
									$sql_string = mysql_query("SELECT * FROM $match_table WHERE community_id='".$match_id."' AND country_id='".$country_search."' AND city='".$city_search."' AND status=0 AND active=0 AND image_name!='' AND profile_url!=''");
								}
								else
								{
									$sql_string = mysql_query("SELECT * FROM $match_table WHERE community_id='".$match_id."' AND country_id='".$country_search."' AND status=0 AND active=0 AND image_name!='' AND profile_url!=''");
								}
							}
							elseif($country_search==0 && $page_type_search!=0 && $city_search=="")
							{
								$sql_all_art = mysql_query("SELECT * FROM $match_table WHERE community_id='".$match_id."' AND status=0 AND active=0 AND image_name!='' AND profile_url!=''");
								if($sub_type_search!=0 && $meta_type_search!=0)
								{
									if(mysql_num_rows($sql_all_art)>0)
									{
										$sql_page_typea = mysql_query("SELECT MAX( general_community_profiles_id ) FROM general_community_profiles WHERE type_id ='".$page_type_search."' AND subtype_id='".$sub_type_search."' AND metatype_id='".$meta_type_search."' AND community_id='".$match_id."'");
										$run_page_typea = mysql_fetch_assoc($sql_page_typea);
										if($run_page_typea['MAX( general_community_profiles_id )']!=NULL)
										{
											//echo "hii";
											$sql_string = mysql_query("SELECT * FROM $match_table WHERE community_id='".$match_id."'");
										}
									}
								}
								elseif($sub_type_search!=0 && $meta_type_search==0)
								{
									if(mysql_num_rows($sql_all_art)>0)
									{
										$sql_page_typea = mysql_query("SELECT MAX( general_community_profiles_id ) FROM general_community_profiles WHERE type_id ='".$page_type_search."' AND subtype_id='".$sub_type_search."' AND community_id='".$match_id."'");
										$run_page_typea = mysql_fetch_assoc($sql_page_typea);
										if($run_page_typea['MAX( general_community_profiles_id )']!=NULL)
										{
											//echo "hii";
											$sql_string = mysql_query("SELECT * FROM $match_table WHERE community_id='".$match_id."'");
										}
									}
								}
								else
								{
									if(mysql_num_rows($sql_all_art)>0)
									{
										$sql_page_typea = mysql_query("SELECT MAX( general_community_profiles_id ) FROM general_community_profiles WHERE type_id ='".$page_type_search."' AND community_id='".$match_id."'");
										$run_page_typea = mysql_fetch_assoc($sql_page_typea);
										if($run_page_typea['MAX( general_community_profiles_id )']!=NULL)
										{
											$sql_string = mysql_query("SELECT * FROM $match_table WHERE community_id='".$match_id."'");
										}
									}
								}
							}
							elseif($country_search!=0 && $page_type_search!=0)
							{
								if($state_search!=0 && $city_search=="")
								{
									$sql_all_art = mysql_query("SELECT * FROM $match_table WHERE community_id='".$match_id."' AND country_id='".$country_search."' AND state_id='".$state_search."' AND  status=0 AND active=0 AND image_name!='' AND profile_url!=''");
								}
								elseif($state_search!=0 && $city_search!="")
								{
									$sql_all_art = mysql_query("SELECT * FROM $match_table WHERE community_id='".$match_id."' AND country_id='".$country_search."' AND state_id='".$state_search."' AND city='".$city_search."' AND status=0 AND active=0 AND image_name!='' AND profile_url!=''");
								}
								elseif($state_search==0 && $city_search!="")
								{
									$sql_all_art = mysql_query("SELECT * FROM $match_table WHERE community_id='".$match_id."' AND country_id='".$country_search."' AND city='".$city_search."' AND status=0 AND active=0 AND image_name!='' AND profile_url!=''");
								}
								else
								{
									$sql_all_art = mysql_query("SELECT * FROM $match_table WHERE community_id='".$match_id."' AND country_id='".$country_search."' AND status=0 AND active=0 AND image_name!='' AND profile_url!=''");
								}
								if(mysql_num_rows($sql_all_art)>0)
								{	
									if($sub_type_search!=0 && $meta_type_search==0)
									{
										$sql_page_typea = mysql_query("SELECT MAX( general_community_profiles_id ) FROM general_community_profiles WHERE type_id ='".$page_type_search."' AND subtype_id='".$sub_type_search."' AND community_id='".$match_id."'");
									}
									elseif($sub_type_search!=0 && $meta_type_search!=0)
									{
										$sql_page_typea = mysql_query("SELECT MAX( general_community_profiles_id ) FROM general_community_profiles WHERE type_id ='".$page_type_search."' AND subtype_id='".$sub_type_search."' AND metatype_id='".$meta_type_search."' AND community_id='".$match_id."'");
									}
									else
									{
										$sql_page_typea = mysql_query("SELECT MAX( general_community_profiles_id ) FROM general_community_profiles WHERE type_id ='".$page_type_search."' AND community_id='".$match_id."'");
									}
									$run_page_typea = mysql_fetch_assoc($sql_page_typea);
									if($run_page_typea['MAX( general_community_profiles_id )']!=NULL)
									{
										//echo "hii";
										$sql_string = mysql_query("SELECT * FROM $match_table WHERE community_id='".$match_id."'");
									}
								}
							}
							elseif($city_search!="" && $country_search==0)
							{
								if($page_type_search!=0)
								{
									$sql_all_art = mysql_query("SELECT * FROM $match_table WHERE community_id='".$match_id."' AND city='".$city_search."' AND status=0 AND active=0 AND image_name!='' AND profile_url!=''");
									
									if(mysql_num_rows($sql_all_art)>0)
									{	
										if($sub_type_search!=0 && $meta_type_search==0)
										{
											$sql_page_typea = mysql_query("SELECT MAX( general_community_profiles_id ) FROM general_community_profiles WHERE type_id ='".$page_type_search."' AND subtype_id='".$sub_type_search."' AND community_id='".$match_id."'");
										}
										elseif($sub_type_search!=0 && $meta_type_search!=0)
										{
											$sql_page_typea = mysql_query("SELECT MAX( general_community_profiles_id ) FROM general_community_profiles WHERE type_id ='".$page_type_search."' AND subtype_id='".$sub_type_search."' AND metatype_id='".$meta_type_search."' AND community_id='".$match_id."'");
										}
										else
										{
											$sql_page_typea = mysql_query("SELECT MAX( general_community_profiles_id ) FROM general_community_profiles WHERE type_id ='".$page_type_search."' AND community_id='".$match_id."'");
										}
										$run_page_typea = mysql_fetch_assoc($sql_page_typea);
										if($run_page_typea['MAX( general_community_profiles_id )']!=NULL)
										{
											//echo "hii";
											$sql_string = mysql_query("SELECT * FROM $match_table WHERE community_id='".$match_id."'");
										}
									}
								}
								elseif($page_type_search==0)
								{
									$sql_string = mysql_query("SELECT * FROM $match_table WHERE community_id='".$match_id."' AND city='".$city_search."' AND status=0 AND active=0");
								}
							}
							elseif($page_type_search==0 && $city_search=="" && $country_search==0)
							{
								$sql_string = mysql_query("SELECT * FROM $match_table WHERE community_id='".$match_id."' AND status=0 AND active=0 AND image_name!='' AND profile_url!=''");
							}
						}
					}
					
					if($profile_type_search=='event')
					{
						if($match_table=="artist_event" ||  $match_table=="community_event")
						{
							if($match_table=="artist_event")
							{
								$profiles_table = 'artist_event_profiles';
								$profiles_id = 'artist_event_id';
							}
							if($match_table=="community_event")
							{
								$profiles_table = 'community_event_profiles';
								$profiles_id = 'community_event_id';
							}
							if($country_search!=0 && $page_type_search==0)
							{
								if($state_search!=0 && $city_search!="")
								{
									$sql_string = mysql_query("SELECT * FROM $match_table WHERE id='".$match_id."' AND country_id='".$country_search."' AND state_id='".$state_search."' AND city='".$city_search."' AND image_name!='' AND profile_url!=''");
								}
								elseif($state_search!=0 && $city_search=="")
								{
									$sql_string = mysql_query("SELECT * FROM $match_table WHERE id='".$match_id."' AND country_id='".$country_search."' AND state_id='".$state_search."' AND image_name!='' AND profile_url!=''");
								}
								elseif($state_search==0 && $city_search!="")
								{
									$sql_string = mysql_query("SELECT * FROM $match_table WHERE id='".$match_id."' AND country_id='".$country_search."' AND city='".$city_search."' AND image_name!='' AND profile_url!=''");
								}
								else
								{
									$sql_string = mysql_query("SELECT * FROM $match_table WHERE id='".$match_id."' AND country_id='".$country_search."' AND image_name!='' AND profile_url!=''");
								}
							}
							elseif($country_search==0 && $page_type_search!=0 && $city_search=="")
							{
								$sql_all_art = mysql_query("SELECT * FROM $match_table WHERE id='".$match_id."' AND image_name!='' AND profile_url!=''");
								if($sub_type_search!=0 && $meta_type_search!=0)
								{
									if(mysql_num_rows($sql_all_art)>0)
									{
										$sql_page_typea = mysql_query("SELECT MAX( id ) FROM $profiles_table WHERE type_id ='".$page_type_search."' AND subtype_id='".$sub_type_search."' AND metatype_id='".$meta_type_search."' AND $profiles_id='".$match_id."'");
										$run_page_typea = mysql_fetch_assoc($sql_page_typea);
										if($run_page_typea['MAX( id )']!=NULL)
										{
											//echo "hii";
											$sql_string = mysql_query("SELECT * FROM $match_table WHERE id='".$match_id."'");
										}
									}
								}
								elseif($sub_type_search!=0 && $meta_type_search==0)
								{
									if(mysql_num_rows($sql_all_art)>0)
									{
										$sql_page_typea = mysql_query("SELECT MAX( id ) FROM $profiles_table WHERE type_id ='".$page_type_search."' AND subtype_id='".$sub_type_search."' AND $profiles_id='".$match_id."'");
										$run_page_typea = mysql_fetch_assoc($sql_page_typea);
										if($run_page_typea['MAX( id )']!=NULL)
										{
											//echo "hii";
											$sql_string = mysql_query("SELECT * FROM $match_table WHERE id='".$match_id."'");
										}
									}
								}
								else
								{
									if(mysql_num_rows($sql_all_art)>0)
									{
										$sql_page_typea = mysql_query("SELECT MAX( id ) FROM $profiles_table WHERE type_id ='".$page_type_search."' AND $profiles_id='".$match_id."'");
										$run_page_typea = mysql_fetch_assoc($sql_page_typea);
										if($run_page_typea['MAX( id )']!=NULL)
										{
											$sql_string = mysql_query("SELECT * FROM $match_table WHERE id='".$match_id."'");
										}
									}
								}
							}
							elseif($country_search!=0 && $page_type_search!=0)
							{
								if($state_search!=0 && $city_search=="")
								{
									$sql_all_art = mysql_query("SELECT * FROM $match_table WHERE id='".$match_id."' AND country_id='".$country_search."' AND state_id='".$state_search."' AND image_name!='' AND profile_url!=''");
								}
								elseif($state_search!=0 && $city_search!="")
								{
									$sql_all_art = mysql_query("SELECT * FROM $match_table WHERE id='".$match_id."' AND country_id='".$country_search."' AND state_id='".$state_search."' AND city='".$city_search."' AND image_name!='' AND profile_url!=''");
								}
								elseif($state_search==0 && $city_search!="")
								{
									$sql_all_art = mysql_query("SELECT * FROM $match_table WHERE id='".$match_id."' AND country_id='".$country_search."' AND city='".$city_search."' AND image_name!='' AND profile_url!=''");
								}
								else
								{
									$sql_all_art = mysql_query("SELECT * FROM $match_table WHERE id='".$match_id."' AND country_id='".$country_search."' AND image_name!='' AND profile_url!=''");
								}
								if(mysql_num_rows($sql_all_art)>0)
								{	
									if($sub_type_search!=0 && $meta_type_search==0)
									{
										$sql_page_typea = mysql_query("SELECT MAX( id ) FROM $profiles_table WHERE type_id ='".$page_type_search."' AND subtype_id='".$sub_type_search."' AND $profiles_id='".$match_id."'");
									}
									elseif($sub_type_search!=0 && $meta_type_search!=0)
									{
										$sql_page_typea = mysql_query("SELECT MAX( id ) FROM $profiles_table WHERE type_id ='".$page_type_search."' AND subtype_id='".$sub_type_search."' AND metatype_id='".$meta_type_search."' AND $profiles_id='".$match_id."'");
									}
									else
									{
										$sql_page_typea = mysql_query("SELECT MAX( id ) FROM $profiles_table WHERE type_id ='".$page_type_search."' AND $profiles_id='".$match_id."'");
									}
									$run_page_typea = mysql_fetch_assoc($sql_page_typea);
									if($run_page_typea['MAX( id )']!=NULL)
									{
										//echo "hii";
										$sql_string = mysql_query("SELECT * FROM $match_table WHERE id='".$match_id."'");
									}
								}
							}
							elseif($city_search!="" && $country_search==0)
							{
								if($page_type_search!=0)
								{
									$sql_all_art = mysql_query("SELECT * FROM $match_table WHERE id='".$match_id."' AND city='".$city_search."' AND image_name!='' AND profile_url!=''");
									
									if(mysql_num_rows($sql_all_art)>0)
									{	
										if($sub_type_search!=0 && $meta_type_search==0)
										{
											$sql_page_typea = mysql_query("SELECT MAX( id ) FROM $profiles_table WHERE type_id ='".$page_type_search."' AND subtype_id='".$sub_type_search."' AND $profiles_id='".$match_id."'");
										}
										elseif($sub_type_search!=0 && $meta_type_search!=0)
										{
											$sql_page_typea = mysql_query("SELECT MAX( id ) FROM $profiles_table WHERE type_id ='".$page_type_search."' AND subtype_id='".$sub_type_search."' AND metatype_id='".$meta_type_search."' AND $profiles_id='".$match_id."'");
										}
										else
										{
											$sql_page_typea = mysql_query("SELECT MAX( id ) FROM $profiles_table WHERE type_id ='".$page_type_search."' AND $profiles_id='".$match_id."'");
										}
										$run_page_typea = mysql_fetch_assoc($sql_page_typea);
										if($run_page_typea['MAX( id )']!=NULL)
										{
											//echo "hii";
											$sql_string = mysql_query("SELECT * FROM $match_table WHERE id='".$match_id."'");
										}
									}
								}
								elseif($page_type_search==0)
								{
									$sql_string = mysql_query("SELECT * FROM $match_table WHERE id='".$match_id."' AND city='".$city_search."' AND image_name!='' AND profile_url!=''");
								}
							}
							elseif($page_type_search==0 && $city_search=="" && $country_search==0)
							{
								$sql_string = mysql_query("SELECT * FROM $match_table WHERE id='".$match_id."' AND image_name!='' AND profile_url!=''");
							}
						}
					}
					
					if($profile_type_search=='project')
					{
						if($match_table=="artist_project" || $match_table=="community_project")
						{
							if($match_table=="artist_project")
							{
								$profiles_table = 'artist_project_profiles';
								$profiles_id = 'artist_project_id';
							}
							if($match_table=="community_project")
							{
								$profiles_table = 'community_project_profiles';
								$profiles_id = 'community_project_id';
							}
							if($country_search!=0 && $page_type_search==0)
							{
								if($state_search!=0 && $city_search!="")
								{
									$sql_string = mysql_query("SELECT * FROM $match_table WHERE id='".$match_id."' AND country_id='".$country_search."' AND state_id='".$state_search."' AND city='".$city_search."' AND del_status=0 AND active=0 AND image_name!='' AND profile_url!=''");
								}
								elseif($state_search!=0 && $city_search=="")
								{
									$sql_string = mysql_query("SELECT * FROM $match_table WHERE id='".$match_id."' AND country_id='".$country_search."' AND state_id='".$state_search."' AND del_status=0 AND active=0 AND image_name!='' AND profile_url!=''");
								}
								elseif($state_search==0 && $city_search!="")
								{
									$sql_string = mysql_query("SELECT * FROM $match_table WHERE id='".$match_id."' AND country_id='".$country_search."' AND city='".$city_search."' AND del_status=0 AND active=0 AND image_name!='' AND profile_url!=''");
								}
								else
								{
									$sql_string = mysql_query("SELECT * FROM $match_table WHERE id='".$match_id."' AND country_id='".$country_search."' AND del_status=0 AND active=0 AND image_name!='' AND profile_url!=''");
								}
							}
							elseif($country_search==0 && $page_type_search!=0 && $city_search=="")
							{
								$sql_all_art = mysql_query("SELECT * FROM $match_table WHERE id='".$match_id."' AND del_status=0 AND active=0 AND image_name!='' AND profile_url!=''");
								if($sub_type_search!=0 && $meta_type_search!=0)
								{
									if(mysql_num_rows($sql_all_art)>0)
									{
										$sql_page_typea = mysql_query("SELECT MAX( id ) FROM $profiles_table WHERE type_id ='".$page_type_search."' AND subtype_id='".$sub_type_search."' AND metatype_id='".$meta_type_search."' AND $profiles_id='".$match_id."'");
										$run_page_typea = mysql_fetch_assoc($sql_page_typea);
										if($run_page_typea['MAX( id )']!=NULL)
										{
											//echo "hii";
											$sql_string = mysql_query("SELECT * FROM $match_table WHERE id='".$match_id."'");
										}
									}
								}
								elseif($sub_type_search!=0 && $meta_type_search==0)
								{
									if(mysql_num_rows($sql_all_art)>0)
									{
										$sql_page_typea = mysql_query("SELECT MAX( id ) FROM $profiles_table WHERE type_id ='".$page_type_search."' AND subtype_id='".$sub_type_search."' AND $profiles_id='".$match_id."'");
										$run_page_typea = mysql_fetch_assoc($sql_page_typea);
										if($run_page_typea['MAX( id )']!=NULL)
										{
											//echo "hii";
											$sql_string = mysql_query("SELECT * FROM $match_table WHERE id='".$match_id."'");
										}
									}
								}
								else
								{
									if(mysql_num_rows($sql_all_art)>0)
									{
										$sql_page_typea = mysql_query("SELECT MAX( id ) FROM $profiles_table WHERE type_id ='".$page_type_search."' AND $profiles_id='".$match_id."'");
										$run_page_typea = mysql_fetch_assoc($sql_page_typea);
										if($run_page_typea['MAX( id )']!=NULL)
										{
											$sql_string = mysql_query("SELECT * FROM $match_table WHERE id='".$match_id."'");
										}
									}
								}
							}
							elseif($country_search!=0 && $page_type_search!=0)
							{
								if($state_search!=0 && $city_search=="")
								{
									$sql_all_art = mysql_query("SELECT * FROM $match_table WHERE id='".$match_id."' AND country_id='".$country_search."' AND state_id='".$state_search."' AND del_status=0 AND active=0 AND image_name!='' AND profile_url!=''");
								}
								elseif($state_search!=0 && $city_search!="")
								{
									$sql_all_art = mysql_query("SELECT * FROM $match_table WHERE id='".$match_id."' AND country_id='".$country_search."' AND state_id='".$state_search."' AND city='".$city_search."' AND del_status=0 AND active=0 AND image_name!='' AND profile_url!=''");
								}
								elseif($state_search==0 && $city_search!="")
								{
									$sql_all_art = mysql_query("SELECT * FROM $match_table WHERE id='".$match_id."' AND country_id='".$country_search."' AND city='".$city_search."' AND del_status=0 AND active=0 AND image_name!='' AND profile_url!=''");
								}
								else
								{
									$sql_all_art = mysql_query("SELECT * FROM $match_table WHERE id='".$match_id."' AND country_id='".$country_search."' AND image_name!='' AND profile_url!=''");
								}
								if(mysql_num_rows($sql_all_art)>0)
								{	
									if($sub_type_search!=0 && $meta_type_search==0)
									{
										$sql_page_typea = mysql_query("SELECT MAX( id ) FROM $profiles_table WHERE type_id ='".$page_type_search."' AND subtype_id='".$sub_type_search."' AND $profiles_id='".$match_id."'");
									}
									elseif($sub_type_search!=0 && $meta_type_search!=0)
									{
										$sql_page_typea = mysql_query("SELECT MAX( id ) FROM $profiles_table WHERE type_id ='".$page_type_search."' AND subtype_id='".$sub_type_search."' AND metatype_id='".$meta_type_search."' AND $profiles_id='".$match_id."'");
									}
									else
									{
										$sql_page_typea = mysql_query("SELECT MAX( id ) FROM $profiles_table WHERE type_id ='".$page_type_search."' AND $profiles_id='".$match_id."'");
									}
									$run_page_typea = mysql_fetch_assoc($sql_page_typea);
									if($run_page_typea['MAX( id )']!=NULL)
									{
										//echo "hii";
										$sql_string = mysql_query("SELECT * FROM $match_table WHERE id='".$match_id."'");
									}
								}
							}
							elseif($city_search!="" && $country_search==0)
							{
								if($page_type_search!=0)
								{
									$sql_all_art = mysql_query("SELECT * FROM $match_table WHERE id='".$match_id."' AND city='".$city_search."' AND del_status=0 AND active=0 AND image_name!='' AND profile_url!=''");
									
									if(mysql_num_rows($sql_all_art)>0)
									{	
										if($sub_type_search!=0 && $meta_type_search==0)
										{
											$sql_page_typea = mysql_query("SELECT MAX( id ) FROM $profiles_table WHERE type_id ='".$page_type_search."' AND subtype_id='".$sub_type_search."' AND $profiles_id='".$match_id."'");
										}
										elseif($sub_type_search!=0 && $meta_type_search!=0)
										{
											$sql_page_typea = mysql_query("SELECT MAX( id ) FROM $profiles_table WHERE type_id ='".$page_type_search."' AND subtype_id='".$sub_type_search."' AND metatype_id='".$meta_type_search."' AND $profiles_id='".$match_id."'");
										}
										else
										{
											$sql_page_typea = mysql_query("SELECT MAX( id ) FROM $profiles_table WHERE type_id ='".$page_type_search."' AND $profiles_id='".$match_id."'");
										}
										$run_page_typea = mysql_fetch_assoc($sql_page_typea);
										if($run_page_typea['MAX( id )']!=NULL)
										{
											//echo "hii";
											$sql_string = mysql_query("SELECT * FROM $match_table WHERE id='".$match_id."'");
										}
									}
								}
								elseif($page_type_search==0)
								{
									$sql_string = mysql_query("SELECT * FROM $match_table WHERE id='".$match_id."' AND city='".$city_search."' AND del_status=0 AND active=0 AND image_name!='' AND profile_url!=''");
								}
							}
							elseif($page_type_search==0 && $city_search=="" && $country_search==0)
							{
								$sql_string = mysql_query("SELECT * FROM $match_table WHERE id='".$match_id."' AND del_status=0 AND active=0 AND image_name!='' AND profile_url!=''");
							}
						}
					}
					
					if($profile_type_search=='media')
					{
						if($match_table=="general_media")
						{
							if($country_search!=0 && $page_type_search==0)
							{
								$ans_media = array();
								$sql_med = mysql_query("SELECT * FROM $match_table WHERE id='".$match_id."' AND delete_status=0 AND media_status=0");
								if(mysql_num_rows($sql_med)>0)
								{
									$row_mede = mysql_fetch_assoc($sql_med);
									//while($row_mede = mysql_fetch_assoc($sql_med))
									//{
										if($row_mede['from_info']!="")
										{
											$exp_from = explode('|',$row_mede['from_info']);
											if($exp_from[0]=='general_artist')
											{
												if($state_search!=0 && $city_search=="")
												{
													$sql_get_tables = mysql_query("SELECT * FROM $exp_from[0] WHERE artist_id='".$exp_from[1]."' AND country_id='".$country_search."' AND state_id='".$state_search."' AND status=0 AND active=0");
												}
												elseif($state_search!=0 && $city_search!="")
												{
													$sql_get_tables = mysql_query("SELECT * FROM $exp_from[0] WHERE artist_id='".$exp_from[1]."' AND country_id='".$country_search."' AND state_id='".$state_search."' AND city='".$city_search."' AND status=0 AND active=0");
												}
												elseif($state_search==0 && $city_search!="")
												{
													$sql_get_tables = mysql_query("SELECT * FROM $exp_from[0] WHERE artist_id='".$exp_from[1]."' AND country_id='".$country_search."' AND city='".$city_search."' AND status=0 AND active=0");
												}
												else
												{
													$sql_get_tables = mysql_query("SELECT * FROM $exp_from[0] WHERE artist_id='".$exp_from[1]."' AND country_id='".$country_search."' AND status=0 AND active=0");
												}
											}
											elseif($exp_from[0]=='general_community')
											{
												if($state_search!=0 && $city_search=="")
												{
													$sql_get_tables = mysql_query("SELECT * FROM $exp_from[0] WHERE community_id='".$exp_from[1]."' AND country_id='".$country_search."' AND state_id='".$state_search."' AND status=0 AND active=0");
												}
												elseif($state_search!=0 && $city_search!="")
												{
													$sql_get_tables = mysql_query("SELECT * FROM $exp_from[0] WHERE community_id='".$exp_from[1]."' AND country_id='".$country_search."' AND state_id='".$state_search."' AND city='".$city_search."' AND status=0 AND active=0");
												}
												elseif($state_search==0 && $city_search!="")
												{
													$sql_get_tables = mysql_query("SELECT * FROM $exp_from[0] WHERE community_id='".$exp_from[1]."' AND country_id='".$country_search."'  AND city='".$city_search."' AND status=0 AND active=0");
												}
												else
												{
													$sql_get_tables = mysql_query("SELECT * FROM $exp_from[0] WHERE community_id='".$exp_from[1]."' AND country_id='".$country_search."'  AND status=0 AND active=0");
												}
											}
											elseif($exp_from[0]=='community_event' || $exp_from[0]=='artist_event' || $exp_from[0]=='artist_project' || $exp_from[0]=='community_project')
											{
												if($state_search!=0 && $city_search=="")
												{
													$sql_get_tables = mysql_query("SELECT * FROM $exp_from[0] WHERE id='".$exp_from[1]."' AND country_id='".$country_search."' AND state_id='".$state_search."'");
												}
												elseif($state_search!=0 && $city_search!="")
												{
													$sql_get_tables = mysql_query("SELECT * FROM $exp_from[0] WHERE id='".$exp_from[1]."' AND country_id='".$country_search."' AND state_id='".$state_search."' AND city='".$city_search."'");
												}
												elseif($state_search==0 && $city_search!="")
												{
													$sql_get_tables = mysql_query("SELECT * FROM $exp_from[0] WHERE id='".$exp_from[1]."' AND country_id='".$country_search."' AND city='".$city_search."'");
												}
												else
												{
													$sql_get_tables = mysql_query("SELECT * FROM $exp_from[0] WHERE id='".$exp_from[1]."' AND country_id='".$country_search."'");
												}
											}
										}
										elseif($row_mede['creator_info']!="")
										{
											$exp_from = explode('|',$row_mede['creator_info']);
											if($exp_from[0]=='general_artist')
											{
												if($state_search!=0 && $city_search=="")
												{
													$sql_get_tables = mysql_query("SELECT * FROM $exp_from[0] WHERE artist_id='".$exp_from[1]."' AND country_id='".$country_search."' AND state_id='".$state_search."' AND status=0 AND active=0");
												}
												elseif($state_search!=0 && $city_search!="")
												{
													$sql_get_tables = mysql_query("SELECT * FROM $exp_from[0] WHERE artist_id='".$exp_from[1]."' AND country_id='".$country_search."' AND state_id='".$state_search."' AND city='".$city_search."' AND status=0 AND active=0");
												}
												elseif($state_search==0 && $city_search!="")
												{
													$sql_get_tables = mysql_query("SELECT * FROM $exp_from[0] WHERE artist_id='".$exp_from[1]."' AND country_id='".$country_search."' AND city='".$city_search."' AND status=0 AND active=0");
												}
												else
												{
													$sql_get_tables = mysql_query("SELECT * FROM $exp_from[0] WHERE artist_id='".$exp_from[1]."' AND country_id='".$country_search."' AND status=0 AND active=0");
												}
											}
											elseif($exp_from[0]=='general_community')
											{
												if($state_search!=0 && $city_search=="")
												{
													$sql_get_tables = mysql_query("SELECT * FROM $exp_from[0] WHERE community_id='".$exp_from[1]."' AND country_id='".$country_search."' AND state_id='".$state_search."' AND status=0 AND active=0");
												}
												elseif($state_search!=0 && $city_search!="")
												{
													$sql_get_tables = mysql_query("SELECT * FROM $exp_from[0] WHERE community_id='".$exp_from[1]."' AND country_id='".$country_search."' AND state_id='".$state_search."' AND city='".$city_search."' AND status=0 AND active=0");
												}
												elseif($state_search==0 && $city_search!="")
												{
													$sql_get_tables = mysql_query("SELECT * FROM $exp_from[0] WHERE community_id='".$exp_from[1]."' AND country_id='".$country_search."'  AND city='".$city_search."' AND status=0 AND active=0");
												}
												else
												{
													$sql_get_tables = mysql_query("SELECT * FROM $exp_from[0] WHERE community_id='".$exp_from[1]."' AND country_id='".$country_search."'  AND status=0 AND active=0");
												}
												
											}
											elseif($exp_from[0]=='community_event' || $exp_from[0]=='artist_event' || $exp_from[0]=='artist_project' || $exp_from[0]=='community_project')
											{
												if($state_search!=0 && $city_search=="")
												{
													$sql_get_tables = mysql_query("SELECT * FROM $exp_from[0] WHERE id='".$exp_from[1]."' AND country_id='".$country_search."' AND state_id='".$state_search."'");
												}
												elseif($state_search!=0 && $city_search!="")
												{
													$sql_get_tables = mysql_query("SELECT * FROM $exp_from[0] WHERE id='".$exp_from[1]."' AND country_id='".$country_search."' AND state_id='".$state_search."' AND city='".$city_search."'");
												}
												elseif($state_search==0 && $city_search!="")
												{
													$sql_get_tables = mysql_query("SELECT * FROM $exp_from[0] WHERE id='".$exp_from[1]."' AND country_id='".$country_search."' AND city='".$city_search."'");
												}
												else
												{
													$sql_get_tables = mysql_query("SELECT * FROM $exp_from[0] WHERE id='".$exp_from[1]."' AND country_id='".$country_search."'");
												}
											}
										}
										
										if(isset($sql_get_tables) && !empty($sql_get_tables))
										{
											if(mysql_num_rows($sql_get_tables)>0)
											{
												$sql_string = mysql_query("SELECT * FROM general_media WHERE id='".$match_id."' AND delete_status=0 AND media_status=0");
												//$ans_media[] = mysql_fetch_assoc($sql_get_med);
											}
										}
								}
							}
							elseif($country_search==0 && $page_type_search!=0 && $city_search=="")
							{
								$sql_all_art = mysql_query("SELECT * FROM $match_table WHERE _id='".$match_id."'");
								if($sub_type_search!=0 && $meta_type_search!=0)
								{
									if(mysql_num_rows($sql_all_art)>0)
									{
										$sql_page_typea = mysql_query("SELECT MAX( _id ) FROM media_profiles_type WHERE type_id ='".$page_type_search."' AND subtype_id='".$sub_type_search."' AND metatype_id='".$meta_type_search."' AND media_id='".$match_id."'");
										$run_page_typea = mysql_fetch_assoc($sql_page_typea);
										if($run_page_typea['MAX( id )']!=NULL)
										{
											$sql_string = mysql_query("SELECT * FROM $match_table WHERE id='".$match_id."'");
										}
									}
								}
								elseif($sub_type_search!=0 && $meta_type_search==0)
								{
									if(mysql_num_rows($sql_all_art)>0)
									{
										$sql_page_typea = mysql_query("SELECT MAX( id ) FROM media_profiles_type WHERE type_id ='".$page_type_search."' AND subtype_id='".$sub_type_search."' AND media_id='".$match_id."'");
										$run_page_typea = mysql_fetch_assoc($sql_page_typea);
										if($run_page_typea['MAX( id )']!=NULL)
										{
											$sql_string = mysql_query("SELECT * FROM $match_table WHERE id='".$match_id."'");
										}
									}
								}
								else
								{
									if(mysql_num_rows($sql_all_art)>0)
									{
										$sql_page_typea = mysql_query("SELECT MAX( id ) FROM media_profiles_type WHERE type_id ='".$page_type_search."' AND media_id='".$match_id."'");
										$run_page_typea = mysql_fetch_assoc($sql_page_typea);
										if($run_page_typea['MAX( id )']!=NULL)
										{
											$sql_string = mysql_query("SELECT * FROM $match_table WHERE id='".$match_id."'");
										}
									}
								}
							}
							elseif($country_search!=0 && $page_type_search!=0)
							{
								if($state_search!=0 && $city_search=="")
								{
									$sql_med = mysql_query("SELECT * FROM $match_table WHERE id='".$match_id."'");
									if(mysql_num_rows($sql_med)>0)
									{
										$row_mede = mysql_fetch_assoc($sql_med);
										
										if($row_mede['from_info']!="")
										{
											$exp_from = explode('|',$row_mede['from_info']);
											if($exp_from[0]=='general_artist')
											{
												$sql_get_tables = mysql_query("SELECT * FROM $exp_from[0] WHERE artist_id='".$exp_from[1]."' AND country_id='".$country_search."' AND state_id='".$state_search."' AND status=0 AND active=0");
											}
											elseif($exp_from[0]=='general_community')
											{
												$sql_get_tables = mysql_query("SELECT * FROM $exp_from[0] WHERE community_id='".$exp_from[1]."' AND country_id='".$country_search."' AND state_id='".$state_search."' AND status=0 AND active=0");
											}
											elseif($exp_from[0]=='community_event' || $exp_from[0]=='artist_event' || $exp_from[0]=='artist_project' || $exp_from[0]=='community_project')
											{
												$sql_get_tables = mysql_query("SELECT * FROM $exp_from[0] WHERE id='".$exp_from[1]."' AND country_id='".$country_search."' AND state_id='".$state_search."'");
											}
											
											if(isset($sql_get_tables) && !empty($sql_get_tables))
											{
												if(mysql_num_rows($sql_get_tables)>0)
												{
													$sql_get_med = mysql_query("SELECT * FROM general_media WHERE id='".$match_id."' AND delete_status=0 AND media_status=0");
													$row_csc = mysql_fetch_assoc($sql_get_med);
													
													if($sub_type_search!=0 && $meta_type_search==0)
													{
														$sql_page_typeme = mysql_query("SELECT MAX( id ) FROM media_profiles_type WHERE type_id ='".$page_type_search."' AND subtype_id='".$sub_type_search."' AND media_id='".$row_csc['id']."'");
													}
													elseif($sub_type_search!=0 && $meta_type_search!=0)
													{
														$sql_page_typeme = mysql_query("SELECT MAX( id ) FROM media_profiles_type WHERE type_id ='".$page_type_search."' AND subtype_id='".$sub_type_search."' AND metatype_id='".$meta_type_search."' AND media_id='".$row_csc['id']."'");
													}
													else
													{
														$sql_page_typeme = mysql_query("SELECT MAX( id ) FROM media_profiles_type WHERE type_id ='".$page_type_search."' AND media_id='".$row_csc['id']."'");
													}
															
													$run_page_typeme = mysql_fetch_assoc($sql_page_typeme);
															
													if($run_page_typeme['MAX( id )']!=NULL)
													{
														$sql_string = mysql_query("SELECT * FROM general_media WHERE id='".$match_id."' AND delete_status=0 AND media_status=0");
													}
												}
											}
										}
										elseif($row_mede['creator_info']!="")
										{
											$exp_from = explode('|',$row_mede['creator_info']);
											if($exp_from[0]=='general_artist')
											{
												$sql_get_tables = mysql_query("SELECT * FROM $exp_from[0] WHERE artist_id='".$exp_from[1]."' AND country_id='".$country_search."' AND state_id='".$state_search."' AND status=0 AND active=0");
											}
											elseif($exp_from[0]=='general_community')
											{
												$sql_get_tables = mysql_query("SELECT * FROM $exp_from[0] WHERE community_id='".$exp_from[1]."' AND country_id='".$country_search."' AND state_id='".$state_search."' AND status=0 AND active=0");
											}
											elseif($exp_from[0]=='community_event' || $exp_from[0]=='artist_event' || $exp_from[0]=='artist_project' || $exp_from[0]=='community_project')
											{
												$sql_get_tables = mysql_query("SELECT * FROM $exp_from[0] WHERE id='".$exp_from[1]."' AND country_id='".$country_search."' AND state_id='".$state_search."'");
											}
											
											if(isset($sql_get_tables) && !empty($sql_get_tables))
											{
												if(mysql_num_rows($sql_get_tables)>0)
												{
													$sql_get_med = mysql_query("SELECT * FROM general_media WHERE id='".$match_id."' AND delete_status=0 AND media_status=0");
													$row_csc = mysql_fetch_assoc($sql_get_med);
												
													if($sub_type_search!=0 && $meta_type_search==0)
													{
														$sql_page_typeme = mysql_query("SELECT MAX( id ) FROM media_profiles_type WHERE type_id ='".$page_type_search."' AND subtype_id='".$sub_type_search."' AND media_id='".$row_csc['id']."'");
													}
													elseif($sub_type_search!=0 && $meta_type_search!=0)
													{
														$sql_page_typeme = mysql_query("SELECT MAX( id ) FROM media_profiles_type WHERE type_id ='".$page_type_search."' AND subtype_id='".$sub_type_search."' AND metatype_id='".$meta_type_search."' AND media_id='".$row_csc['id']."'");
													}
													else
													{
														$sql_page_typeme = mysql_query("SELECT MAX( id ) FROM media_profiles_type WHERE type_id ='".$page_type_search."' AND media_id='".$row_csc['id']."'");
													}
													
													$run_page_typeme = mysql_fetch_assoc($sql_page_typeme);
													
													if($run_page_typeme['MAX( id )']!=NULL)
													{
														//echo "hii";
														$sql_string = mysql_query("SELECT * FROM general_media WHERE id='".$match_id."' AND delete_status=0 AND media_status=0");
													}
												}
											}
										}
									}
								}
								elseif($state_search!=0 && $city_search!="")
								{
									$sql_med = mysql_query("SELECT * FROM $match_table WHERE id='".$match_id."'");
									if(mysql_num_rows($sql_med)>0)
									{
										$row_mede = mysql_fetch_assoc($sql_med);
										
										if($row_mede['from_info']!="")
										{
											$exp_from = explode('|',$row_mede['from_info']);
											if($exp_from[0]=='general_artist')
											{
												$sql_get_tables = mysql_query("SELECT * FROM $exp_from[0] WHERE artist_id='".$exp_from[1]."' AND country_id='".$country_search."' AND state_id='".$state_search."' AND city='".$city_search."' AND status=0 AND active=0");
											}
											elseif($exp_from[0]=='general_community')
											{
												$sql_get_tables = mysql_query("SELECT * FROM $exp_from[0] WHERE community_id='".$exp_from[1]."' AND country_id='".$country_search."' AND state_id='".$state_search."' AND city='".$city_search."' AND status=0 AND active=0");
											}
											elseif($exp_from[0]=='community_event' || $exp_from[0]=='artist_event' || $exp_from[0]=='artist_project' || $exp_from[0]=='community_project')
											{
												$sql_get_tables = mysql_query("SELECT * FROM $exp_from[0] WHERE id='".$exp_from[1]."' AND country_id='".$country_search."' AND state_id='".$state_search."' AND city='".$city_search."'");
											}
										
											if(isset($sql_get_tables) && !empty($sql_get_tables))
											{
												if(mysql_num_rows($sql_get_tables)>0)
												{
													$sql_get_med = mysql_query("SELECT * FROM general_media WHERE id='".$match_id."' AND delete_status=0 AND media_status=0");
													
													$row_csc = mysql_fetch_assoc($sql_get_med);
											
													if($sub_type_search!=0 && $meta_type_search==0)
													{
														$sql_page_typeme = mysql_query("SELECT MAX( id ) FROM media_profiles_type WHERE type_id ='".$page_type_search."' AND subtype_id='".$sub_type_search."' AND media_id='".$row_csc['id']."'");
													}
													elseif($sub_type_search!=0 && $meta_type_search!=0)
													{
														$sql_page_typeme = mysql_query("SELECT MAX( id ) FROM media_profiles_type WHERE type_id ='".$page_type_search."' AND subtype_id='".$sub_type_search."' AND metatype_id='".$meta_type_search."' AND media_id='".$row_csc['id']."'");
													}
													else
													{
														$sql_page_typeme = mysql_query("SELECT MAX( id ) FROM media_profiles_type WHERE type_id ='".$page_type_search."' AND media_id='".$row_csc['id']."'");
													}
															
													$run_page_typeme = mysql_fetch_assoc($sql_page_typeme);
															
													if($run_page_typeme['MAX( id )']!=NULL)
													{
														//echo "hii";
														$sql_string = mysql_query("SELECT * FROM general_media WHERE id='".$match_id."' AND delete_status=0 AND media_status=0");
													}
												}
											}
										}
										elseif($row_mede['creator_info']!="")
										{
											$exp_from = explode('|',$row_mede['creator_info']);
											if($exp_from[0]=='general_artist')
											{
												$sql_get_tables = mysql_query("SELECT * FROM $exp_from[0] WHERE artist_id='".$exp_from[1]."' AND country_id='".$country_search."' AND state_id='".$state_search."' AND city='".$city_search."' AND status=0 AND active=0");
											}
											elseif($exp_from[0]=='general_community')
											{
												$sql_get_tables = mysql_query("SELECT * FROM $exp_from[0] WHERE community_id='".$exp_from[1]."' AND country_id='".$country_search."' AND state_id='".$state_search."' AND city='".$city_search."' AND status=0 AND active=0");
											}
											elseif($exp_from[0]=='community_event' || $exp_from[0]=='artist_event' || $exp_from[0]=='artist_project' || $exp_from[0]=='community_project')
											{
												$sql_get_tables = mysql_query("SELECT * FROM $exp_from[0] WHERE id='".$exp_from[1]."' AND country_id='".$country_search."' AND state_id='".$state_search."' AND city='".$city_search."'");
											}
											
											if(isset($sql_get_tables) && !empty($sql_get_tables))
											{
												if(mysql_num_rows($sql_get_tables)>0)
												{
													$sql_get_med = mysql_query("SELECT * FROM general_media WHERE id='".$match_id."' AND delete_status=0 AND media_status=0");
													$row_csc = mysql_fetch_assoc($sql_get_med);
													
													if($sub_type_search!=0 && $meta_type_search==0)
													{
														$sql_page_typeme = mysql_query("SELECT MAX( id ) FROM media_profiles_type WHERE type_id ='".$page_type_search."' AND subtype_id='".$sub_type_search."' AND media_id='".$row_csc['id']."'");
													}
													elseif($sub_type_search!=0 && $meta_type_search!=0)
													{
														$sql_page_typeme = mysql_query("SELECT MAX( id ) FROM media_profiles_type WHERE type_id ='".$page_type_search."' AND subtype_id='".$sub_type_search."' AND metatype_id='".$meta_type_search."' AND media_id='".$row_csc['id']."'");
													}
													else
													{
														$sql_page_typeme = mysql_query("SELECT MAX( id ) FROM media_profiles_type WHERE type_id ='".$page_type_search."' AND media_id='".$row_csc['id']."'");
													}
													
													$run_page_typeme = mysql_fetch_assoc($sql_page_typeme);
															
													if($run_page_typeme['MAX( id )']!=NULL)
													{
														//echo "hii";
														$sql_string = mysql_query("SELECT * FROM general_media WHERE id='".$match_id."' AND delete_status=0 AND media_status=0");
													}
												}
											}
										}
									}
								}
								elseif($state_search==0 && $city_search!="")
								{
									$sql_med = mysql_query("SELECT * FROM $match_table WHERE id='".$match_id."'");
									if(mysql_num_rows($sql_med)>0)
									{
										$row_mede = mysql_fetch_assoc($sql_med);
										if($row_mede['from_info']!="")
										{
											$exp_from = explode('|',$row_mede['from_info']);
											if($exp_from[0]=='general_artist')
											{
												$sql_get_tables = mysql_query("SELECT * FROM $exp_from[0] WHERE artist_id='".$exp_from[1]."' AND country_id='".$country_search."' AND city='".$city_search."' AND status=0 AND active=0");
											}
											elseif($exp_from[0]=='general_community')
											{
												$sql_get_tables = mysql_query("SELECT * FROM $exp_from[0] WHERE community_id='".$exp_from[1]."' AND country_id='".$country_search."' AND city='".$city_search."' AND status=0 AND active=0");
											}
											elseif($exp_from[0]=='community_event' || $exp_from[0]=='artist_event' || $exp_from[0]=='artist_project' || $exp_from[0]=='community_project')
											{
												$sql_get_tables = mysql_query("SELECT * FROM $exp_from[0] WHERE id='".$exp_from[1]."' AND country_id='".$country_search."' AND city='".$city_search."'");
											}
											
											if(isset($sql_get_tables) && !empty($sql_get_tables))
											{
												if(mysql_num_rows($sql_get_tables)>0)
												{
													$sql_get_med = mysql_query("SELECT * FROM general_media WHERE id='".$match_id."' AND delete_status=0 AND media_status=0");
													$row_csc = mysql_fetch_assoc($sql_get_med);
													
													if($sub_type_search!=0 && $meta_type_search==0)
													{
														$sql_page_typeme = mysql_query("SELECT MAX( id ) FROM media_profiles_type WHERE type_id ='".$page_type_search."' AND subtype_id='".$sub_type_search."' AND media_id='".$row_csc['id']."'");
													}
													elseif($sub_type_search!=0 && $meta_type_search!=0)
													{
														$sql_page_typeme = mysql_query("SELECT MAX( id ) FROM media_profiles_type WHERE type_id ='".$page_type_search."' AND subtype_id='".$sub_type_search."' AND metatype_id='".$meta_type_search."' AND media_id='".$row_csc['id']."'");
													}
													else
													{
														$sql_page_typeme = mysql_query("SELECT MAX( id ) FROM media_profiles_type WHERE type_id ='".$page_type_search."' AND media_id='".$row_csc['id']."'");
													}
													
													$run_page_typeme = mysql_fetch_assoc($sql_page_typeme);
															
													if($run_page_typeme['MAX( id )']!=NULL)
													{
														//echo "hii";
														$sql_string = mysql_query("SELECT * FROM general_media WHERE id='".$match_id."' AND delete_status=0 AND media_status=0");
													}
												}
											}
										}
										elseif($row_mede['creator_info']!="")
										{
											$exp_from = explode('|',$row_mede['creator_info']);
											if($exp_from[0]=='general_artist')
											{
												$sql_get_tables = mysql_query("SELECT * FROM $exp_from[0] WHERE artist_id='".$exp_from[1]."' AND country_id='".$country_search."' AND city='".$city_search."' AND status=0 AND active=0");
											}
											elseif($exp_from[0]=='general_community')
											{
												$sql_get_tables = mysql_query("SELECT * FROM $exp_from[0] WHERE community_id='".$exp_from[1]."' AND country_id='".$country_search."' AND city='".$city_search."' AND status=0 AND active=0");
											}
											elseif($exp_from[0]=='community_event' || $exp_from[0]=='artist_event' || $exp_from[0]=='artist_project' || $exp_from[0]=='community_project')
											{
												$sql_get_tables = mysql_query("SELECT * FROM $exp_from[0] WHERE id='".$exp_from[1]."' AND country_id='".$country_search."' AND city='".$city_search."'");
											}
											
											if(isset($sql_get_tables) && !empty($sql_get_tables))
											{
												if(mysql_num_rows($sql_get_tables)>0)
												{
													$sql_get_med = mysql_query("SELECT * FROM general_media WHERE id='".$match_id."' AND delete_status=0 AND media_status=0");
													$row_csc = mysql_fetch_assoc($sql_get_med);
													
													if($sub_type_search!=0 && $meta_type_search==0)
													{
														$sql_page_typeme = mysql_query("SELECT MAX( id ) FROM media_profiles_type WHERE type_id ='".$page_type_search."' AND subtype_id='".$sub_type_search."' AND media_id='".$row_csc['id']."'");
													}
													elseif($sub_type_search!=0 && $meta_type_search!=0)
													{
														$sql_page_typeme = mysql_query("SELECT MAX( id ) FROM media_profiles_type WHERE type_id ='".$page_type_search."' AND subtype_id='".$sub_type_search."' AND metatype_id='".$meta_type_search."' AND media_id='".$row_csc['id']."'");
													}
													else
													{
														$sql_page_typeme = mysql_query("SELECT MAX( id ) FROM media_profiles_type WHERE type_id ='".$page_type_search."' AND media_id='".$row_csc['id']."'");
													}
															
													$run_page_typeme = mysql_fetch_assoc($sql_page_typeme);
															
													if($run_page_typeme['MAX( id )']!=NULL)
													{
														//echo "hii";
														$sql_string = mysql_query("SELECT * FROM general_media WHERE id='".$match_id."' AND delete_status=0 AND media_status=0");
													}
												}
											}
										}
									}
								}
								else
								{
									$sql_med = mysql_query("SELECT * FROM $match_table WHERE id='".$match_id."'");
									if(mysql_num_rows($sql_med)>0)
									{
										$row_mede = mysql_fetch_assoc($sql_med);
											
										if($row_mede['from_info']!="")
										{
											$exp_from = explode('|',$row_mede['from_info']);
											if($exp_from[0]=='general_artist')
											{
												$sql_get_tables = mysql_query("SELECT * FROM $exp_from[0] WHERE artist_id='".$exp_from[1]."' AND country_id='".$country_search."' AND status=0 AND active=0");
											}
											elseif($exp_from[0]=='general_community')
											{
												$sql_get_tables = mysql_query("SELECT * FROM $exp_from[0] WHERE community_id='".$exp_from[1]."' AND country_id='".$country_search."' AND status=0 AND active=0");
											}
											elseif($exp_from[0]=='community_event' || $exp_from[0]=='artist_event' || $exp_from[0]=='artist_project' || $exp_from[0]=='community_project')
											{
												$sql_get_tables = mysql_query("SELECT * FROM $exp_from[0] WHERE id='".$exp_from[1]."' AND country_id='".$country_search."'");
											}
											
											if(isset($sql_get_tables) && !empty($sql_get_tables))
											{
												if(mysql_num_rows($sql_get_tables)>0)
												{
													$sql_get_med = mysql_query("SELECT * FROM general_media WHERE id='".$match_id."' AND delete_status=0 AND media_status=0");
													$row_csc = mysql_fetch_assoc($sql_get_med);
													
													if($sub_type_search!=0 && $meta_type_search==0)
													{
														$sql_page_typeme = mysql_query("SELECT MAX( id ) FROM media_profiles_type WHERE type_id ='".$page_type_search."' AND subtype_id='".$sub_type_search."' AND media_id='".$row_csc['id']."'");
													}
													elseif($sub_type_search!=0 && $meta_type_search!=0)
													{
														$sql_page_typeme = mysql_query("SELECT MAX( id ) FROM media_profiles_type WHERE type_id ='".$page_type_search."' AND subtype_id='".$sub_type_search."' AND metatype_id='".$meta_type_search."' AND media_id='".$row_csc['id']."'");
													}
													else
													{
														$sql_page_typeme = mysql_query("SELECT MAX( id ) FROM media_profiles_type WHERE type_id ='".$page_type_search."' AND media_id='".$row_csc['id']."'");
													}
															
													$run_page_typeme = mysql_fetch_assoc($sql_page_typeme);
															
													if($run_page_typeme['MAX( id )']!=NULL)
													{
														//echo "hii";
														$sql_string = mysql_query("SELECT * FROM general_media WHERE id='".$match_id."' AND delete_status=0 AND media_status=0");
													}
												}
											}
										}
										elseif($row_mede['creator_info']!="")
										{
											$exp_from = explode('|',$row_mede['creator_info']);
											if($exp_from[0]=='general_artist')
											{
												$sql_get_tables = mysql_query("SELECT * FROM $exp_from[0] WHERE artist_id='".$exp_from[1]."' AND country_id='".$country_search."' AND status=0 AND active=0");
											}
											elseif($exp_from[0]=='general_community')
											{
												$sql_get_tables = mysql_query("SELECT * FROM $exp_from[0] WHERE community_id='".$exp_from[1]."' AND country_id='".$country_search."' AND status=0 AND active=0");
											}
											elseif($exp_from[0]=='community_event' || $exp_from[0]=='artist_event' || $exp_from[0]=='artist_project' || $exp_from[0]=='community_project')
											{
												$sql_get_tables = mysql_query("SELECT * FROM $exp_from[0] WHERE id='".$exp_from[1]."' AND country_id='".$country_search."'");
											}
											
											if(isset($sql_get_tables) && !empty($sql_get_tables))
											{
												if(mysql_num_rows($sql_get_tables)>0)
												{
													$sql_get_med = mysql_query("SELECT * FROM general_media WHERE id='".$match_id."' AND delete_status=0 AND media_status=0");
													$row_csc = mysql_fetch_assoc($sql_get_med);
												
													if($sub_type_search!=0 && $meta_type_search==0)
													{
														$sql_page_typeme = mysql_query("SELECT MAX( id ) FROM media_profiles_type WHERE type_id ='".$page_type_search."' AND subtype_id='".$sub_type_search."' AND media_id='".$row_csc['id']."'");
													}
													elseif($sub_type_search!=0 && $meta_type_search!=0)
													{
														$sql_page_typeme = mysql_query("SELECT MAX( id ) FROM media_profiles_type WHERE type_id ='".$page_type_search."' AND subtype_id='".$sub_type_search."' AND metatype_id='".$meta_type_search."' AND media_id='".$row_csc['id']."'");
													}
													else
													{
														$sql_page_typeme = mysql_query("SELECT MAX( id ) FROM media_profiles_type WHERE type_id ='".$page_type_search."' AND media_id='".$row_csc['id']."'");
													}
															
													$run_page_typeme = mysql_fetch_assoc($sql_page_typeme);
															
													if($run_page_typeme['MAX( id )']!=NULL)
													{
														//echo "hii";
														$sql_string = mysql_query("SELECT * FROM general_media WHERE id='".$match_id."'");
													}
												}
											}
										}
									}
								}
							}
							elseif($city_search!="" && $country_search==0)
							{
								if($page_type_search!=0)
								{
									$sql_med_ci = mysql_query("SELECT * FROM $match_table WHERE id='".$match_id."'");
									
									if(mysql_num_rows($sql_med_ci)>0)
									{
										$row_mede = mysql_fetch_assoc($sql_med_ci);
										if($row_mede['from_info']!="")
										{
											$exp_from = explode('|',$row_mede['from_info']);
											if($exp_from[0]=='general_artist')
											{
												$sql_get_tables = mysql_query("SELECT * FROM $exp_from[0] WHERE artist_id='".$exp_from[1]."' AND city='".$city_search."' AND status=0 AND active=0");
											}
											elseif($exp_from[0]=='general_community')
											{
												$sql_get_tables = mysql_query("SELECT * FROM $exp_from[0] WHERE community_id='".$exp_from[1]."' AND city='".$city_search."' AND status=0 AND active=0");
											}
											elseif($exp_from[0]=='community_event' || $exp_from[0]=='artist_event' || $exp_from[0]=='artist_project' || $exp_from[0]=='community_project')
											{
												$sql_get_tables = mysql_query("SELECT * FROM $exp_from[0] WHERE id='".$exp_from[1]."' AND city='".$city_search."'");
											}
										}
										elseif($row_mede['creator_info']!="")
										{
											$exp_from = explode('|',$row_mede['creator_info']);
											if($exp_from[0]=='general_artist')
											{
												$sql_get_tables = mysql_query("SELECT * FROM $exp_from[0] WHERE artist_id='".$exp_from[1]."' city='".$city_search."' AND status=0 AND active=0");
											}
											elseif($exp_from[0]=='general_community')
											{
												$sql_get_tables = mysql_query("SELECT * FROM $exp_from[0] WHERE community_id='".$exp_from[1]."' AND city='".$city_search."' AND status=0 AND active=0");
											}
											elseif($exp_from[0]=='community_event' || $exp_from[0]=='artist_event' || $exp_from[0]=='artist_project' || $exp_from[0]=='community_project')
											{
												$sql_get_tables = mysql_query("SELECT * FROM $exp_from[0] WHERE id='".$exp_from[1]."' AND city='".$city_search."'");
											}
											
											if(isset($sql_get_tables) && !empty($sql_get_tables))
											{
												if(mysql_num_rows($sql_get_tables)>0)
												{
													//$sql_string = mysql_query("SELECT * FROM general_media WHERE id='".$match_id."'");
													//if(mysql_num_rows($sql_get_tables)>0)
													//{
														$sql_get_med = mysql_query("SELECT * FROM general_media WHERE id='".$match_id."' AND delete_status=0 AND media_status=0");
														$row_csc = mysql_fetch_assoc($sql_get_med);
														
														if($sub_type_search!=0 && $meta_type_search==0)
														{
															$sql_page_typeme = mysql_query("SELECT MAX( id ) FROM media_profiles_type WHERE type_id ='".$page_type_search."' AND subtype_id='".$sub_type_search."' AND media_id='".$row_csc['id']."'");
														}
														elseif($sub_type_search!=0 && $meta_type_search!=0)
														{
															$sql_page_typeme = mysql_query("SELECT MAX( id ) FROM media_profiles_type WHERE type_id ='".$page_type_search."' AND subtype_id='".$sub_type_search."' AND metatype_id='".$meta_type_search."' AND media_id='".$row_csc['id']."'");
														}
														else
														{
															$sql_page_typeme = mysql_query("SELECT MAX( id ) FROM media_profiles_type WHERE type_id ='".$page_type_search."' AND media_id='".$row_csc['id']."'");
														}
																
														$run_page_typeme = mysql_fetch_assoc($sql_page_typeme);
																	
														if($run_page_typeme['MAX( id )']!=NULL)
														{
															//echo "hii";
															$sql_string = mysql_query("SELECT * FROM general_media WHERE id='".$match_id."'");
														}
													//}
												}
											}
										}
									}
								}
								elseif($page_type_search==0)
								{
									$sql_med_ci = mysql_query("SELECT * FROM $match_table WHERE id='".$match_id."'");
									
									if(mysql_num_rows($sql_med_ci)>0)
									{
										if($row_mede['from_info']!="")
										{
											$exp_from = explode('|',$row_mede['from_info']);
											if($exp_from[0]=='general_artist')
											{
												$sql_get_tables = mysql_query("SELECT * FROM $exp_from[0] WHERE artist_id='".$exp_from[1]."' AND city='".$city_search."' AND status=0 AND active=0");
											}
											elseif($exp_from[0]=='general_community')
											{
												$sql_get_tables = mysql_query("SELECT * FROM $exp_from[0] WHERE community_id='".$exp_from[1]."' AND city='".$city_search."' AND status=0 AND active=0");
											}
											elseif($exp_from[0]=='community_event' || $exp_from[0]=='artist_event' || $exp_from[0]=='artist_project' || $exp_from[0]=='community_project')
											{
												$sql_get_tables = mysql_query("SELECT * FROM $exp_from[0] WHERE id='".$exp_from[1]."' AND city='".$city_search."'");
											}
										}
										elseif($row_mede['creator_info']!="")
										{
											$exp_from = explode('|',$row_mede['creator_info']);
											if($exp_from[0]=='general_artist')
											{
												$sql_get_tables = mysql_query("SELECT * FROM $exp_from[0] WHERE artist_id='".$exp_from[1]."' city='".$city_search."' AND status=0 AND active=0");
											}
											elseif($exp_from[0]=='general_community')
											{
												$sql_get_tables = mysql_query("SELECT * FROM $exp_from[0] WHERE community_id='".$exp_from[1]."' AND city='".$city_search."' AND status=0 AND active=0");
											}
											elseif($exp_from[0]=='community_event' || $exp_from[0]=='artist_event' || $exp_from[0]=='artist_project' || $exp_from[0]=='community_project')
											{
												$sql_get_tables = mysql_query("SELECT * FROM $exp_from[0] WHERE id='".$exp_from[1]."' AND city='".$city_search."'");
											}
										}
										
										if(isset($sql_get_tables) && !empty($sql_get_tables))
										{
											if(mysql_num_rows($sql_get_tables)>0)
											{
												$sql_string = mysql_query("SELECT * FROM $match_table WHERE id='".$match_id."'");
											}
										}
									}
								}
							}
							elseif($page_type_search==0 && $city_search=="" && $country_search==0)
							{
								$sql_string = mysql_query("SELECT * FROM $match_table WHERE id='".$match_id."'");
							}
						}
					}
				}
				elseif($profile_type_search=="select" && $country_search!=0)
				{
					if($match_table=="general_artist")
					{
						if($page_type_search==0)
						{
							if($state_search!=0 && $city_search!="")
							{
								$sql_string = mysql_query("SELECT * FROM $match_table WHERE artist_id='".$match_id."' AND country_id='".$country_search."' AND state_id='".$state_search."' AND city='".$city_search."' AND status=0 AND active=0 AND image_name!='' AND profile_url!=''");
							}
							elseif($state_search!=0 && $city_search=="")
							{
								$sql_string = mysql_query("SELECT * FROM $match_table WHERE artist_id='".$match_id."' AND country_id='".$country_search."' AND state_id='".$state_search."' AND status=0 AND active=0 AND image_name!='' AND profile_url!=''");
							}
							elseif($state_search==0 && $city_search!="")
							{
								$sql_string = mysql_query("SELECT * FROM $match_table WHERE artist_id='".$match_id."' AND country_id='".$country_search."' AND city='".$city_search."' AND status=0 AND active=0 AND image_name!='' AND profile_url!=''");
							}
							else
							{
								$sql_string = mysql_query("SELECT * FROM $match_table WHERE artist_id='".$match_id."' AND country_id='".$country_search."' AND status=0 AND active=0 AND image_name!='' AND profile_url!=''");
							}
						}
						elseif($page_type_search!=0)
						{
							if($state_search!=0 && $city_search=="")
							{
								$sql_all_art = mysql_query("SELECT * FROM $match_table WHERE artist_id='".$match_id."' AND country_id='".$country_search."' AND state_id='".$state_search."' AND status=0 AND active=0 AND image_name!='' AND profile_url!=''");
							}
							elseif($state_search!=0 && $city_search!="")
							{
								$sql_all_art = mysql_query("SELECT * FROM $match_table WHERE artist_id='".$match_id."' AND country_id='".$country_search."' AND state_id='".$state_search."' AND city='".$city_search."' AND status=0 AND active=0 AND image_name!='' AND profile_url!=''");
							}
							elseif($state_search==0 && $city_search!="")
							{
								$sql_all_art = mysql_query("SELECT * FROM $match_table WHERE artist_id='".$match_id."' AND country_id='".$country_search."' AND city='".$city_search."' AND status=0 AND active=0 AND image_name!='' AND profile_url!=''");
							}
							else
							{
								$sql_all_art = mysql_query("SELECT * FROM $match_table WHERE artist_id='".$match_id."' AND country_id='".$country_search."' AND status=0 AND active=0 AND image_name!='' AND profile_url!=''");
							}
							if(mysql_num_rows($sql_all_art)>0)
							{	
								if($sub_type_search!=0 && $meta_type_search==0)
								{
									$sql_page_typea = mysql_query("SELECT MAX( general_artist_profiles_id ) FROM general_artist_profiles WHERE type_id ='".$page_type_search."' AND subtype_id='".$sub_type_search."' AND artist_id='".$match_id."'");
								}
								elseif($sub_type_search!=0 && $meta_type_search!=0)
								{
									$sql_page_typea = mysql_query("SELECT MAX( general_artist_profiles_id ) FROM general_artist_profiles WHERE type_id ='".$page_type_search."' AND subtype_id='".$sub_type_search."' AND metatype_id='".$meta_type_search."' AND artist_id='".$match_id."'");
								}
								else
								{
									$sql_page_typea = mysql_query("SELECT MAX( general_artist_profiles_id ) FROM general_artist_profiles WHERE type_id ='".$page_type_search."' AND artist_id='".$match_id."'");
								}
								$run_page_typea = mysql_fetch_assoc($sql_page_typea);
								if($run_page_typea['MAX( general_artist_profiles_id )']!=NULL)
								{
									$sql_string = mysql_query("SELECT * FROM $match_table WHERE artist_id='".$match_id."'");
								}
							}
						}
					}
					if($match_table=="general_community")
					{
						if($page_type_search==0)
						{
							if($state_search!=0 && $city_search!="")
							{
								$sql_string = mysql_query("SELECT * FROM $match_table WHERE community_id='".$match_id."' AND country_id='".$country_search."' AND state_id='".$state_search."' AND city='".$city_search."' AND status=0 AND active=0 AND image_name!='' AND profile_url!=''");
							}
							elseif($state_search!=0 && $city_search=="")
							{
								$sql_string = mysql_query("SELECT * FROM $match_table WHERE community_id='".$match_id."' AND country_id='".$country_search."' AND state_id='".$state_search."' AND status=0 AND active=0 AND image_name!='' AND profile_url!=''");
							}
							elseif($state_search==0 && $city_search!="")
							{
								$sql_string = mysql_query("SELECT * FROM $match_table WHERE community_id='".$match_id."' AND country_id='".$country_search."' AND city='".$city_search."' AND status=0 AND active=0 AND image_name!='' AND profile_url!=''");
							}
							else
							{
								$sql_string = mysql_query("SELECT * FROM $match_table WHERE community_id='".$match_id."' AND country_id='".$country_search."' AND status=0 AND active=0 AND image_name!='' AND profile_url!=''");
							}
						}
						elseif($page_type_search!=0)
						{
							if($state_search!=0 && $city_search=="")
							{
								$sql_all_art = mysql_query("SELECT * FROM $match_table WHERE community_id='".$match_id."' AND country_id='".$country_search."' AND state_id='".$state_search."' AND status=0 AND active=0 AND image_name!='' AND profile_url!=''");
							}
							elseif($state_search!=0 && $city_search!="")
							{
								$sql_all_art = mysql_query("SELECT * FROM $match_table WHERE community_id='".$match_id."' AND country_id='".$country_search."' AND state_id='".$state_search."' AND city='".$city_search."' AND status=0 AND active=0 AND image_name!='' AND profile_url!=''");
							}
							elseif($state_search==0 && $city_search!="")
							{
								$sql_all_art = mysql_query("SELECT * FROM $match_table WHERE community_id='".$match_id."' AND country_id='".$country_search."' AND city='".$city_search."' AND status=0 AND active=0 AND image_name!='' AND profile_url!=''");
							}
							else
							{
								$sql_all_art = mysql_query("SELECT * FROM $match_table WHERE community_id='".$match_id."' AND country_id='".$country_search."' AND status=0 AND active=0 AND image_name!='' AND profile_url!=''");
							}
							if(mysql_num_rows($sql_all_art)>0)
							{	
								if($sub_type_search!=0 && $meta_type_search==0)
								{
									$sql_page_typea = mysql_query("SELECT MAX( general_community_profiles_id ) FROM general_community_profiles WHERE type_id ='".$page_type_search."' AND subtype_id='".$sub_type_search."' AND community_id='".$match_id."'");
								}
								elseif($sub_type_search!=0 && $meta_type_search!=0)
								{
									$sql_page_typea = mysql_query("SELECT MAX( general_community_profiles_id ) FROM general_community_profiles WHERE type_id ='".$page_type_search."' AND subtype_id='".$sub_type_search."' AND metatype_id='".$meta_type_search."' AND community_id='".$match_id."'");
								}
								else
								{
									$sql_page_typea = mysql_query("SELECT MAX( general_community_profiles_id ) FROM general_community_profiles WHERE type_id ='".$page_type_search."' AND community_id='".$match_id."'");
								}
								$run_page_typea = mysql_fetch_assoc($sql_page_typea);
								if($run_page_typea['MAX( general_community_profiles_id )']!=NULL)
								{
									//echo "hii";
									$sql_string = mysql_query("SELECT * FROM $match_table WHERE community_id='".$match_id."'");
								}
							}
						}
					}
					if($match_table=="artist_project" || $match_table=="community_project" || $match_table=="artist_event" || $match_table=="community_event")
					{
						if($match_table=="artist_project")
						{
							$profiles_table = 'artist_project_profiles';
							$profiles_id = 'artist_project_id';
						}
						if($match_table=="community_project")
						{
							$profiles_table = 'community_project_profiles';
							$profiles_id = 'community_project_id';
						}
						if($match_table=="artist_event")
						{
							$profiles_table = 'artist_event_profiles';
							$profiles_id = 'artist_event_id';
						}
						if($match_table=="community_event")
						{
							$profiles_table = 'community_event_profiles';
							$profiles_id = 'community_event_id';
						}
						
						if($page_type_search==0)
						{
							if($state_search!=0 && $city_search!="")
							{
								$sql_string = mysql_query("SELECT * FROM $match_table WHERE id='".$match_id."' AND country_id='".$country_search."' AND state_id='".$state_search."' AND city='".$city_search."' AND del_status=0 AND active=0 AND image_name!='' AND profile_url!=''");
							}
							elseif($state_search!=0 && $city_search=="")
							{
								$sql_string = mysql_query("SELECT * FROM $match_table WHERE id='".$match_id."' AND country_id='".$country_search."' AND state_id='".$state_search."' AND del_status=0 AND active=0 AND image_name!='' AND profile_url!=''");
							}
							elseif($state_search==0 && $city_search!="")
							{
								$sql_string = mysql_query("SELECT * FROM $match_table WHERE id='".$match_id."' AND country_id='".$country_search."' AND city='".$city_search."' AND del_status=0 AND active=0 AND image_name!='' AND profile_url!=''");
							}
							else
							{
								$sql_string = mysql_query("SELECT * FROM $match_table WHERE id='".$match_id."' AND country_id='".$country_search."' AND del_status=0 AND active=0 AND image_name!='' AND profile_url!=''");
							}
						}
						elseif($page_type_search!=0)
						{
							if($state_search!=0 && $city_search=="")
							{
								$sql_all_art = mysql_query("SELECT * FROM $match_table WHERE id='".$match_id."' AND country_id='".$country_search."' AND state_id='".$state_search."' AND del_status=0 AND active=0 AND image_name!='' AND profile_url!=''");
							}
							elseif($state_search!=0 && $city_search!="")
							{
								$sql_all_art = mysql_query("SELECT * FROM $match_table WHERE id='".$match_id."' AND country_id='".$country_search."' AND state_id='".$state_search."' AND city='".$city_search."' AND del_status=0 AND active=0 AND image_name!='' AND profile_url!=''");
							}
							elseif($state_search==0 && $city_search!="")
							{
								$sql_all_art = mysql_query("SELECT * FROM $match_table WHERE id='".$match_id."' AND country_id='".$country_search."' AND city='".$city_search."' AND del_status=0 AND active=0 AND image_name!='' AND profile_url!=''");
							}
							else
							{
								$sql_all_art = mysql_query("SELECT * FROM $match_table WHERE id='".$match_id."' AND country_id='".$country_search."' AND del_status=0 AND active=0 AND image_name!='' AND profile_url!=''");
							}
							if(mysql_num_rows($sql_all_art)>0)
							{	
								if($sub_type_search!=0 && $meta_type_search==0)
								{
									$sql_page_typea = mysql_query("SELECT MAX( id ) FROM $profiles_table WHERE type_id ='".$page_type_search."' AND subtype_id='".$sub_type_search."' AND $profiles_id='".$match_id."'");
								}
								elseif($sub_type_search!=0 && $meta_type_search!=0)
								{
									$sql_page_typea = mysql_query("SELECT MAX( id ) FROM $profiles_table WHERE type_id ='".$page_type_search."' AND subtype_id='".$sub_type_search."' AND metatype_id='".$meta_type_search."' AND $profiles_id='".$match_id."'");
								}
								else
								{
									$sql_page_typea = mysql_query("SELECT MAX( id ) FROM $profiles_table WHERE type_id ='".$page_type_search."' AND $profiles_id='".$match_id."'");
								}
								$run_page_typea = mysql_fetch_assoc($sql_page_typea);
								if($run_page_typea['MAX( id )']!=NULL)
								{
									//echo "hii";
									$sql_string = mysql_query("SELECT * FROM $match_table WHERE id='".$match_id."'");
								}
							}
						}
					}
					if($match_table=="general_media")
					{
						if($page_type_search==0)
						{
							$ans_media = array();
							$sql_med = mysql_query("SELECT * FROM $match_table WHERE id='".$match_id."' AND delete_status=0 AND media_status=0");
							if(mysql_num_rows($sql_med)>0)
							{
								$row_mede = mysql_fetch_assoc($sql_med);
								//while($row_mede = mysql_fetch_assoc($sql_med))
								//{
									if($row_mede['from_info']!="")
									{
										$exp_from = explode('|',$row_mede['from_info']);
										if($exp_from[0]=='general_artist')
										{
											if($state_search!=0 && $city_search=="")
											{
												$sql_get_tables = mysql_query("SELECT * FROM $exp_from[0] WHERE artist_id='".$exp_from[1]."' AND country_id='".$country_search."' AND state_id='".$state_search."' AND status=0 AND active=0");
											}
											elseif($state_search!=0 && $city_search!="")
											{
												$sql_get_tables = mysql_query("SELECT * FROM $exp_from[0] WHERE artist_id='".$exp_from[1]."' AND country_id='".$country_search."' AND state_id='".$state_search."' AND city='".$city_search."' AND status=0 AND active=0");
											}
											elseif($state_search==0 && $city_search!="")
											{
												$sql_get_tables = mysql_query("SELECT * FROM $exp_from[0] WHERE artist_id='".$exp_from[1]."' AND country_id='".$country_search."' AND city='".$city_search."' AND status=0 AND active=0");
											}
											else
											{
												$sql_get_tables = mysql_query("SELECT * FROM $exp_from[0] WHERE artist_id='".$exp_from[1]."' AND country_id='".$country_search."' AND status=0 AND active=0");
											}
										}
										elseif($exp_from[0]=='general_community')
										{
											if($state_search!=0 && $city_search=="")
											{
												$sql_get_tables = mysql_query("SELECT * FROM $exp_from[0] WHERE community_id='".$exp_from[1]."' AND country_id='".$country_search."' AND state_id='".$state_search."' AND status=0 AND active=0");
											}
											elseif($state_search!=0 && $city_search!="")
											{
												$sql_get_tables = mysql_query("SELECT * FROM $exp_from[0] WHERE community_id='".$exp_from[1]."' AND country_id='".$country_search."' AND state_id='".$state_search."' AND city='".$city_search."' AND status=0 AND active=0");
											}
											elseif($state_search==0 && $city_search!="")
											{
												$sql_get_tables = mysql_query("SELECT * FROM $exp_from[0] WHERE community_id='".$exp_from[1]."' AND country_id='".$country_search."'  AND city='".$city_search."' AND status=0 AND active=0");
											}
											else
											{
												$sql_get_tables = mysql_query("SELECT * FROM $exp_from[0] WHERE community_id='".$exp_from[1]."' AND country_id='".$country_search."'  AND status=0 AND active=0");
											}
										}
										elseif($exp_from[0]=='community_event' || $exp_from[0]=='artist_event' || $exp_from[0]=='artist_project' || $exp_from[0]=='community_project')
										{
											if($state_search!=0 && $city_search=="")
											{
												$sql_get_tables = mysql_query("SELECT * FROM $exp_from[0] WHERE id='".$exp_from[1]."' AND country_id='".$country_search."' AND state_id='".$state_search."'");
											}
											elseif($state_search!=0 && $city_search!="")
											{
												$sql_get_tables = mysql_query("SELECT * FROM $exp_from[0] WHERE id='".$exp_from[1]."' AND country_id='".$country_search."' AND state_id='".$state_search."' AND city='".$city_search."'");
											}
											elseif($state_search==0 && $city_search!="")
											{
												$sql_get_tables = mysql_query("SELECT * FROM $exp_from[0] WHERE id='".$exp_from[1]."' AND country_id='".$country_search."' AND city='".$city_search."'");
											}
											else
											{
												$sql_get_tables = mysql_query("SELECT * FROM $exp_from[0] WHERE id='".$exp_from[1]."' AND country_id='".$country_search."'");
											}
										}
									}
									elseif($row_mede['creator_info']!="")
									{
										$exp_from = explode('|',$row_mede['creator_info']);
										if($exp_from[0]=='general_artist')
										{
											if($state_search!=0 && $city_search=="")
											{
												$sql_get_tables = mysql_query("SELECT * FROM $exp_from[0] WHERE artist_id='".$exp_from[1]."' AND country_id='".$country_search."' AND state_id='".$state_search."' AND status=0 AND active=0");
											}
											elseif($state_search!=0 && $city_search!="")
											{
												$sql_get_tables = mysql_query("SELECT * FROM $exp_from[0] WHERE artist_id='".$exp_from[1]."' AND country_id='".$country_search."' AND state_id='".$state_search."' AND city='".$city_search."' AND status=0 AND active=0");
											}
											elseif($state_search==0 && $city_search!="")
											{
												$sql_get_tables = mysql_query("SELECT * FROM $exp_from[0] WHERE artist_id='".$exp_from[1]."' AND country_id='".$country_search."' AND city='".$city_search."' AND status=0 AND active=0");
											}
											else
											{
												$sql_get_tables = mysql_query("SELECT * FROM $exp_from[0] WHERE artist_id='".$exp_from[1]."' AND country_id='".$country_search."' AND status=0 AND active=0");
											}
										}
										elseif($exp_from[0]=='general_community')
										{
											if($state_search!=0 && $city_search=="")
											{
												$sql_get_tables = mysql_query("SELECT * FROM $exp_from[0] WHERE community_id='".$exp_from[1]."' AND country_id='".$country_search."' AND state_id='".$state_search."' AND status=0 AND active=0");
											}
											elseif($state_search!=0 && $city_search!="")
											{
												$sql_get_tables = mysql_query("SELECT * FROM $exp_from[0] WHERE community_id='".$exp_from[1]."' AND country_id='".$country_search."' AND state_id='".$state_search."' AND city='".$city_search."' AND status=0 AND active=0");
											}
											elseif($state_search==0 && $city_search!="")
											{
												$sql_get_tables = mysql_query("SELECT * FROM $exp_from[0] WHERE community_id='".$exp_from[1]."' AND country_id='".$country_search."'  AND city='".$city_search."' AND status=0 AND active=0");
											}
											else
											{
												$sql_get_tables = mysql_query("SELECT * FROM $exp_from[0] WHERE community_id='".$exp_from[1]."' AND country_id='".$country_search."'  AND status=0 AND active=0");
											}
											
										}
										elseif($exp_from[0]=='community_event' || $exp_from[0]=='artist_event' || $exp_from[0]=='artist_project' || $exp_from[0]=='community_project')
										{
											if($state_search!=0 && $city_search=="")
											{
												$sql_get_tables = mysql_query("SELECT * FROM $exp_from[0] WHERE id='".$exp_from[1]."' AND country_id='".$country_search."' AND state_id='".$state_search."'");
											}
											elseif($state_search!=0 && $city_search!="")
											{
												$sql_get_tables = mysql_query("SELECT * FROM $exp_from[0] WHERE id='".$exp_from[1]."' AND country_id='".$country_search."' AND state_id='".$state_search."' AND city='".$city_search."'");
											}
											elseif($state_search==0 && $city_search!="")
											{
												$sql_get_tables = mysql_query("SELECT * FROM $exp_from[0] WHERE id='".$exp_from[1]."' AND country_id='".$country_search."' AND city='".$city_search."'");
											}
											else
											{
												$sql_get_tables = mysql_query("SELECT * FROM $exp_from[0] WHERE id='".$exp_from[1]."' AND country_id='".$country_search."'");
											}
										}
									}
									
									if(isset($sql_get_tables) && !empty($sql_get_tables))
									{
										if(mysql_num_rows($sql_get_tables)>0)
										{
											$sql_string = mysql_query("SELECT * FROM general_media WHERE id='".$match_id."' AND delete_status=0 AND media_status=0");
											//$ans_media[] = mysql_fetch_assoc($sql_get_med);
										}
									}
								//}
							}
						}
						elseif($page_type_search!=0)
						{
							if($state_search!=0 && $city_search=="")
							{
								$sql_med = mysql_query("SELECT * FROM $match_table WHERE id='".$match_id."'");
								if(mysql_num_rows($sql_med)>0)
								{
									$row_mede = mysql_fetch_assoc($sql_med);
									
									if($row_mede['from_info']!="")
									{
										$exp_from = explode('|',$row_mede['from_info']);
										if($exp_from[0]=='general_artist')
										{
											$sql_get_tables = mysql_query("SELECT * FROM $exp_from[0] WHERE artist_id='".$exp_from[1]."' AND country_id='".$country_search."' AND state_id='".$state_search."' AND status=0 AND active=0");
										}
										elseif($exp_from[0]=='general_community')
										{
											$sql_get_tables = mysql_query("SELECT * FROM $exp_from[0] WHERE community_id='".$exp_from[1]."' AND country_id='".$country_search."' AND state_id='".$state_search."' AND status=0 AND active=0");
										}
										elseif($exp_from[0]=='community_event' || $exp_from[0]=='artist_event' || $exp_from[0]=='artist_project' || $exp_from[0]=='community_project')
										{
											$sql_get_tables = mysql_query("SELECT * FROM $exp_from[0] WHERE id='".$exp_from[1]."' AND country_id='".$country_search."' AND state_id='".$state_search."'");
										}
										
										if(isset($sql_get_tables) && !empty($sql_get_tables))
										{
											if(mysql_num_rows($sql_get_tables)>0)
											{
												$sql_get_med = mysql_query("SELECT * FROM general_media WHERE id='".$match_id."' AND delete_status=0 AND media_status=0");
												$row_csc = mysql_fetch_assoc($sql_get_med);
												
												if($sub_type_search!=0 && $meta_type_search==0)
												{
													$sql_page_typeme = mysql_query("SELECT MAX( id ) FROM media_profiles_type WHERE type_id ='".$page_type_search."' AND subtype_id='".$sub_type_search."' AND media_id='".$row_csc['id']."'");
												}
												elseif($sub_type_search!=0 && $meta_type_search!=0)
												{
													$sql_page_typeme = mysql_query("SELECT MAX( id ) FROM media_profiles_type WHERE type_id ='".$page_type_search."' AND subtype_id='".$sub_type_search."' AND metatype_id='".$meta_type_search."' AND media_id='".$row_csc['id']."'");
												}
												else
												{
													$sql_page_typeme = mysql_query("SELECT MAX( id ) FROM media_profiles_type WHERE type_id ='".$page_type_search."' AND media_id='".$row_csc['id']."'");
												}
														
												$run_page_typeme = mysql_fetch_assoc($sql_page_typeme);
														
												if($run_page_typeme['MAX( id )']!=NULL)
												{
													$sql_string = mysql_query("SELECT * FROM general_media WHERE id='".$match_id."'");
												}
											}
										}
									}
									elseif($row_mede['creator_info']!="")
									{
										$exp_from = explode('|',$row_mede['creator_info']);
										if($exp_from[0]=='general_artist')
										{
											$sql_get_tables = mysql_query("SELECT * FROM $exp_from[0] WHERE artist_id='".$exp_from[1]."' AND country_id='".$country_search."' AND state_id='".$state_search."' AND status=0 AND active=0");
										}
										elseif($exp_from[0]=='general_community')
										{
											$sql_get_tables = mysql_query("SELECT * FROM $exp_from[0] WHERE community_id='".$exp_from[1]."' AND country_id='".$country_search."' AND state_id='".$state_search."' AND status=0 AND active=0");
										}
										elseif($exp_from[0]=='community_event' || $exp_from[0]=='artist_event' || $exp_from[0]=='artist_project' || $exp_from[0]=='community_project')
										{
											$sql_get_tables = mysql_query("SELECT * FROM $exp_from[0] WHERE id='".$exp_from[1]."' AND country_id='".$country_search."' AND state_id='".$state_search."'");
										}
										
										if(isset($sql_get_tables) && !empty($sql_get_tables))
										{
											if(mysql_num_rows($sql_get_tables)>0)
											{
												$sql_get_med = mysql_query("SELECT * FROM general_media WHERE id='".$match_id."' AND delete_status=0 AND media_status=0");
												$row_csc = mysql_fetch_assoc($sql_get_med);
											
												if($sub_type_search!=0 && $meta_type_search==0)
												{
													$sql_page_typeme = mysql_query("SELECT MAX( id ) FROM media_profiles_type WHERE type_id ='".$page_type_search."' AND subtype_id='".$sub_type_search."' AND media_id='".$row_csc['id']."'");
												}
												elseif($sub_type_search!=0 && $meta_type_search!=0)
												{
													$sql_page_typeme = mysql_query("SELECT MAX( id ) FROM media_profiles_type WHERE type_id ='".$page_type_search."' AND subtype_id='".$sub_type_search."' AND metatype_id='".$meta_type_search."' AND media_id='".$row_csc['id']."'");
												}
												else
												{
													$sql_page_typeme = mysql_query("SELECT MAX( id ) FROM media_profiles_type WHERE type_id ='".$page_type_search."' AND media_id='".$row_csc['id']."'");
												}
												
												$run_page_typeme = mysql_fetch_assoc($sql_page_typeme);
												
												if($run_page_typeme['MAX( id )']!=NULL)
												{
													//echo "hii";
													$sql_string = mysql_query("SELECT * FROM general_media WHERE id='".$match_id."' AND delete_status=0 AND media_status=0");
												}
											}
										}
									}
								}
							}
							elseif($state_search!=0 && $city_search!="")
							{
								$sql_med = mysql_query("SELECT * FROM $match_table WHERE id='".$match_id."'");
								if(mysql_num_rows($sql_med)>0)
								{
									$row_mede = mysql_fetch_assoc($sql_med);
									
									if($row_mede['from_info']!="")
									{
										$exp_from = explode('|',$row_mede['from_info']);
										if($exp_from[0]=='general_artist')
										{
											$sql_get_tables = mysql_query("SELECT * FROM $exp_from[0] WHERE artist_id='".$exp_from[1]."' AND country_id='".$country_search."' AND state_id='".$state_search."' AND city='".$city_search."' AND status=0 AND active=0");
										}
										elseif($exp_from[0]=='general_community')
										{
											$sql_get_tables = mysql_query("SELECT * FROM $exp_from[0] WHERE community_id='".$exp_from[1]."' AND country_id='".$country_search."' AND state_id='".$state_search."' AND city='".$city_search."' AND status=0 AND active=0");
										}
										elseif($exp_from[0]=='community_event' || $exp_from[0]=='artist_event' || $exp_from[0]=='artist_project' || $exp_from[0]=='community_project')
										{
											$sql_get_tables = mysql_query("SELECT * FROM $exp_from[0] WHERE id='".$exp_from[1]."' AND country_id='".$country_search."' AND state_id='".$state_search."' AND city='".$city_search."'");
										}
									
										
										if(isset($sql_get_tables) && !empty($sql_get_tables))
										{
											if(mysql_num_rows($sql_get_tables)>0)
											{
												$sql_get_med = mysql_query("SELECT * FROM general_media WHERE id='".$match_id."' AND delete_status=0 AND media_status=0");
												
												$row_csc = mysql_fetch_assoc($sql_get_med);
										
												if($sub_type_search!=0 && $meta_type_search==0)
												{
													$sql_page_typeme = mysql_query("SELECT MAX( id ) FROM media_profiles_type WHERE type_id ='".$page_type_search."' AND subtype_id='".$sub_type_search."' AND media_id='".$row_csc['id']."'");
												}
												elseif($sub_type_search!=0 && $meta_type_search!=0)
												{
													$sql_page_typeme = mysql_query("SELECT MAX( id ) FROM media_profiles_type WHERE type_id ='".$page_type_search."' AND subtype_id='".$sub_type_search."' AND metatype_id='".$meta_type_search."' AND media_id='".$row_csc['id']."'");
												}
												else
												{
													$sql_page_typeme = mysql_query("SELECT MAX( id ) FROM media_profiles_type WHERE type_id ='".$page_type_search."' AND media_id='".$row_csc['id']."'");
												}
														
												$run_page_typeme = mysql_fetch_assoc($sql_page_typeme);
														
												if($run_page_typeme['MAX( id )']!=NULL)
												{
													//echo "hii";
													$sql_string = mysql_query("SELECT * FROM general_media WHERE id='".$match_id."' AND delete_status=0 AND media_status=0");
												}
											}
										}
									}
									elseif($row_mede['creator_info']!="")
									{
										$exp_from = explode('|',$row_mede['creator_info']);
										if($exp_from[0]=='general_artist')
										{
											$sql_get_tables = mysql_query("SELECT * FROM $exp_from[0] WHERE artist_id='".$exp_from[1]."' AND country_id='".$country_search."' AND state_id='".$state_search."' AND city='".$city_search."' AND status=0 AND active=0");
										}
										elseif($exp_from[0]=='general_community')
										{
											$sql_get_tables = mysql_query("SELECT * FROM $exp_from[0] WHERE community_id='".$exp_from[1]."' AND country_id='".$country_search."' AND state_id='".$state_search."' AND city='".$city_search."' AND status=0 AND active=0");
										}
										elseif($exp_from[0]=='community_event' || $exp_from[0]=='artist_event' || $exp_from[0]=='artist_project' || $exp_from[0]=='community_project')
										{
											$sql_get_tables = mysql_query("SELECT * FROM $exp_from[0] WHERE id='".$exp_from[1]."' AND country_id='".$country_search."' AND state_id='".$state_search."' AND city='".$city_search."'");
										}
											
										if(isset($sql_get_tables) && !empty($sql_get_tables))
										{
											if(mysql_num_rows($sql_get_tables)>0)
											{
												$sql_get_med = mysql_query("SELECT * FROM general_media WHERE id='".$match_id."' AND delete_status=0 AND media_status=0");
												$row_csc = mysql_fetch_assoc($sql_get_med);
												
												if($sub_type_search!=0 && $meta_type_search==0)
												{
													$sql_page_typeme = mysql_query("SELECT MAX( id ) FROM media_profiles_type WHERE type_id ='".$page_type_search."' AND subtype_id='".$sub_type_search."' AND media_id='".$row_csc['id']."'");
												}
												elseif($sub_type_search!=0 && $meta_type_search!=0)
												{
													$sql_page_typeme = mysql_query("SELECT MAX( id ) FROM media_profiles_type WHERE type_id ='".$page_type_search."' AND subtype_id='".$sub_type_search."' AND metatype_id='".$meta_type_search."' AND media_id='".$row_csc['id']."'");
												}
												else
												{
													$sql_page_typeme = mysql_query("SELECT MAX( id ) FROM media_profiles_type WHERE type_id ='".$page_type_search."' AND media_id='".$row_csc['id']."'");
												}
												
												$run_page_typeme = mysql_fetch_assoc($sql_page_typeme);
														
												if($run_page_typeme['MAX( id )']!=NULL)
												{
													//echo "hii";
													$sql_string = mysql_query("SELECT * FROM general_media WHERE id='".$match_id."' AND delete_status=0 AND media_status=0");
												}
											}
										}
									}
								}
							}
							elseif($state_search==0 && $city_search!="")
							{
								$sql_med = mysql_query("SELECT * FROM $match_table WHERE id='".$match_id."'");
								if(mysql_num_rows($sql_med)>0)
								{
									$row_mede = mysql_fetch_assoc($sql_med);
									if($row_mede['from_info']!="")
									{
										$exp_from = explode('|',$row_mede['from_info']);
										if($exp_from[0]=='general_artist')
										{
											$sql_get_tables = mysql_query("SELECT * FROM $exp_from[0] WHERE artist_id='".$exp_from[1]."' AND country_id='".$country_search."' AND city='".$city_search."' AND status=0 AND active=0");
										}
										elseif($exp_from[0]=='general_community')
										{
											$sql_get_tables = mysql_query("SELECT * FROM $exp_from[0] WHERE community_id='".$exp_from[1]."' AND country_id='".$country_search."' AND city='".$city_search."' AND status=0 AND active=0");
										}
										elseif($exp_from[0]=='community_event' || $exp_from[0]=='artist_event' || $exp_from[0]=='artist_project' || $exp_from[0]=='community_project')
										{
											$sql_get_tables = mysql_query("SELECT * FROM $exp_from[0] WHERE id='".$exp_from[1]."' AND country_id='".$country_search."' AND city='".$city_search."'");
										}
										
										
										if(isset($sql_get_tables) && !empty($sql_get_tables))
										{
											if(mysql_num_rows($sql_get_tables)>0)
											{
												$sql_get_med = mysql_query("SELECT * FROM general_media WHERE id='".$match_id."' AND delete_status=0 AND media_status=0");
												$row_csc = mysql_fetch_assoc($sql_get_med);
												
												if($sub_type_search!=0 && $meta_type_search==0)
												{
													$sql_page_typeme = mysql_query("SELECT MAX( id ) FROM media_profiles_type WHERE type_id ='".$page_type_search."' AND subtype_id='".$sub_type_search."' AND media_id='".$row_csc['id']."'");
												}
												elseif($sub_type_search!=0 && $meta_type_search!=0)
												{
													$sql_page_typeme = mysql_query("SELECT MAX( id ) FROM media_profiles_type WHERE type_id ='".$page_type_search."' AND subtype_id='".$sub_type_search."' AND metatype_id='".$meta_type_search."' AND media_id='".$row_csc['id']."'");
												}
												else
												{
													$sql_page_typeme = mysql_query("SELECT MAX( id ) FROM media_profiles_type WHERE type_id ='".$page_type_search."' AND media_id='".$row_csc['id']."'");
												}
												
												$run_page_typeme = mysql_fetch_assoc($sql_page_typeme);
														
												if($run_page_typeme['MAX( id )']!=NULL)
												{
													//echo "hii";
													$sql_string = mysql_query("SELECT * FROM general_media WHERE id='".$match_id."' AND delete_status=0 AND media_status=0");
												}
											}
										}
									}
									elseif($row_mede['creator_info']!="")
									{
										$exp_from = explode('|',$row_mede['creator_info']);
										if($exp_from[0]=='general_artist')
										{
											$sql_get_tables = mysql_query("SELECT * FROM $exp_from[0] WHERE artist_id='".$exp_from[1]."' AND country_id='".$country_search."' AND city='".$city_search."' AND status=0 AND active=0");
										}
										elseif($exp_from[0]=='general_community')
										{
											$sql_get_tables = mysql_query("SELECT * FROM $exp_from[0] WHERE community_id='".$exp_from[1]."' AND country_id='".$country_search."' AND city='".$city_search."' AND status=0 AND active=0");
										}
										elseif($exp_from[0]=='community_event' || $exp_from[0]=='artist_event' || $exp_from[0]=='artist_project' || $exp_from[0]=='community_project')
										{
											$sql_get_tables = mysql_query("SELECT * FROM $exp_from[0] WHERE id='".$exp_from[1]."' AND country_id='".$country_search."' AND city='".$city_search."'");
										}
											
										if(isset($sql_get_tables) && !empty($sql_get_tables))
										{
											if(mysql_num_rows($sql_get_tables)>0)
											{
												$sql_get_med = mysql_query("SELECT * FROM general_media WHERE id='".$match_id."' AND delete_status=0 AND media_status=0");
												$row_csc = mysql_fetch_assoc($sql_get_med);
												
												if($sub_type_search!=0 && $meta_type_search==0)
												{
													$sql_page_typeme = mysql_query("SELECT MAX( id ) FROM media_profiles_type WHERE type_id ='".$page_type_search."' AND subtype_id='".$sub_type_search."' AND media_id='".$row_csc['id']."'");
												}
												elseif($sub_type_search!=0 && $meta_type_search!=0)
												{
													$sql_page_typeme = mysql_query("SELECT MAX( id ) FROM media_profiles_type WHERE type_id ='".$page_type_search."' AND subtype_id='".$sub_type_search."' AND metatype_id='".$meta_type_search."' AND media_id='".$row_csc['id']."'");
												}
												else
												{
													$sql_page_typeme = mysql_query("SELECT MAX( id ) FROM media_profiles_type WHERE type_id ='".$page_type_search."' AND media_id='".$row_csc['id']."'");
												}
														
												$run_page_typeme = mysql_fetch_assoc($sql_page_typeme);
														
												if($run_page_typeme['MAX( id )']!=NULL)
												{
													//echo "hii";
													$sql_string = mysql_query("SELECT * FROM general_media WHERE id='".$match_id."' AND delete_status=0 AND media_status=0");
												}
											}
										}
									}
								}
							}
							else
							{
								$sql_med = mysql_query("SELECT * FROM $match_table WHERE id='".$match_id."'");
								if(mysql_num_rows($sql_med)>0)
								{
									$row_mede = mysql_fetch_assoc($sql_med);
										
									if($row_mede['from_info']!="")
									{
										$exp_from = explode('|',$row_mede['from_info']);
										if($exp_from[0]=='general_artist')
										{
											$sql_get_tables = mysql_query("SELECT * FROM $exp_from[0] WHERE artist_id='".$exp_from[1]."' AND country_id='".$country_search."' AND status=0 AND active=0");
										}
										elseif($exp_from[0]=='general_community')
										{
											$sql_get_tables = mysql_query("SELECT * FROM $exp_from[0] WHERE community_id='".$exp_from[1]."' AND country_id='".$country_search."' AND status=0 AND active=0");
										}
										elseif($exp_from[0]=='community_event' || $exp_from[0]=='artist_event' || $exp_from[0]=='artist_project' || $exp_from[0]=='community_project')
										{
											$sql_get_tables = mysql_query("SELECT * FROM $exp_from[0] WHERE id='".$exp_from[1]."' AND country_id='".$country_search."'");
										}
										
										
										if(isset($sql_get_tables) && !empty($sql_get_tables))
										{
											if(mysql_num_rows($sql_get_tables)>0)
											{
												$sql_get_med = mysql_query("SELECT * FROM general_media WHERE id='".$match_id."' AND delete_status=0 AND media_status=0");
												$row_csc = mysql_fetch_assoc($sql_get_med);
												
												if($sub_type_search!=0 && $meta_type_search==0)
												{
													$sql_page_typeme = mysql_query("SELECT MAX( id ) FROM media_profiles_type WHERE type_id ='".$page_type_search."' AND subtype_id='".$sub_type_search."' AND media_id='".$row_csc['id']."'");
												}
												elseif($sub_type_search!=0 && $meta_type_search!=0)
												{
													$sql_page_typeme = mysql_query("SELECT MAX( id ) FROM media_profiles_type WHERE type_id ='".$page_type_search."' AND subtype_id='".$sub_type_search."' AND metatype_id='".$meta_type_search."' AND media_id='".$row_csc['id']."'");
												}
												else
												{
													$sql_page_typeme = mysql_query("SELECT MAX( id ) FROM media_profiles_type WHERE type_id ='".$page_type_search."' AND media_id='".$row_csc['id']."'");
												}
														
												$run_page_typeme = mysql_fetch_assoc($sql_page_typeme);
														
												if($run_page_typeme['MAX( id )']!=NULL)
												{
													//echo "hii";
													$sql_string = mysql_query("SELECT * FROM general_media WHERE id='".$match_id."' AND delete_status=0 AND media_status=0");
												}
											}
										}
									}
									elseif($row_mede['creator_info']!="")
									{
										$exp_from = explode('|',$row_mede['creator_info']);
										if($exp_from[0]=='general_artist')
										{
											$sql_get_tables = mysql_query("SELECT * FROM $exp_from[0] WHERE artist_id='".$exp_from[1]."' AND country_id='".$country_search."' AND status=0 AND active=0");
										}
										elseif($exp_from[0]=='general_community')
										{
											$sql_get_tables = mysql_query("SELECT * FROM $exp_from[0] WHERE community_id='".$exp_from[1]."' AND country_id='".$country_search."' AND status=0 AND active=0");
										}
										elseif($exp_from[0]=='community_event' || $exp_from[0]=='artist_event' || $exp_from[0]=='artist_project' || $exp_from[0]=='community_project')
										{
											$sql_get_tables = mysql_query("SELECT * FROM $exp_from[0] WHERE id='".$exp_from[1]."' AND country_id='".$country_search."'");
										}
										
										if(isset($sql_get_tables) && !empty($sql_get_tables))
										{
											if(mysql_num_rows($sql_get_tables)>0)
											{
												$sql_get_med = mysql_query("SELECT * FROM general_media WHERE id='".$match_id."' AND delete_status=0 AND media_status=0");
												$row_csc = mysql_fetch_assoc($sql_get_med);
											
												if($sub_type_search!=0 && $meta_type_search==0)
												{
													$sql_page_typeme = mysql_query("SELECT MAX( id ) FROM media_profiles_type WHERE type_id ='".$page_type_search."' AND subtype_id='".$sub_type_search."' AND media_id='".$row_csc['id']."'");
												}
												elseif($sub_type_search!=0 && $meta_type_search!=0)
												{
													$sql_page_typeme = mysql_query("SELECT MAX( id ) FROM media_profiles_type WHERE type_id ='".$page_type_search."' AND subtype_id='".$sub_type_search."' AND metatype_id='".$meta_type_search."' AND media_id='".$row_csc['id']."'");
												}
												else
												{
													$sql_page_typeme = mysql_query("SELECT MAX( id ) FROM media_profiles_type WHERE type_id ='".$page_type_search."' AND media_id='".$row_csc['id']."'");
												}
														
												$run_page_typeme = mysql_fetch_assoc($sql_page_typeme);
														
												if($run_page_typeme['MAX( id )']!=NULL)
												{
													//echo "hii";
													$sql_string = mysql_query("SELECT * FROM general_media WHERE id='".$match_id."' AND delete_status=0 AND media_status=0");
												}
											}
										}
									}
								}
							}
						}
					}
				}
				elseif($profile_type_search=="select" && $country_search==0 && $page_type_search!=0)
				{
					if($match_table=="general_artist")
					{
						if($city_search!="")
						{
							$sql_all_art = mysql_query("SELECT * FROM $match_table WHERE artist_id='".$match_id."' AND city='".$city_search."' AND status=0 AND active=0 AND image_name!='' AND profile_url!=''");
						}
						else
						{
							$sql_all_art = mysql_query("SELECT * FROM $match_table WHERE artist_id='".$match_id."' AND status=0 AND active=0 AND image_name!='' AND profile_url!=''");
						}
						
						if(mysql_num_rows($sql_all_art)>0)
						{	
							if($sub_type_search!=0 && $meta_type_search==0)
							{
								$sql_page_typea = mysql_query("SELECT MAX( general_artist_profiles_id ) FROM general_artist_profiles WHERE type_id ='".$page_type_search."' AND subtype_id='".$sub_type_search."' AND artist_id='".$match_id."'");
							}
							elseif($sub_type_search!=0 && $meta_type_search!=0)
							{
								$sql_page_typea = mysql_query("SELECT MAX( general_artist_profiles_id ) FROM general_artist_profiles WHERE type_id ='".$page_type_search."' AND subtype_id='".$sub_type_search."' AND metatype_id='".$meta_type_search."' AND artist_id='".$match_id."'");
							}
							else
							{
								$sql_page_typea = mysql_query("SELECT MAX( general_artist_profiles_id ) FROM general_artist_profiles WHERE type_id ='".$page_type_search."' AND artist_id='".$match_id."'");
							}
							$run_page_typea = mysql_fetch_assoc($sql_page_typea);
							if($run_page_typea['MAX( general_artist_profiles_id )']!=NULL)
							{
								$sql_string = mysql_query("SELECT * FROM $match_table WHERE artist_id='".$match_id."'");
							}
						}
					}
					if($match_table=="general_community")
					{
						if($city_search!="")
						{
							$sql_all_art = mysql_query("SELECT * FROM $match_table WHERE community_id='".$match_id."' AND city='".$city_search."' AND status=0 AND active=0 AND image_name!='' AND profile_url!=''");
						}
						else
						{
							$sql_all_art = mysql_query("SELECT * FROM $match_table WHERE community_id='".$match_id."' AND status=0 AND active=0 AND image_name!='' AND profile_url!=''");
						}
		
						if(mysql_num_rows($sql_all_art)>0)
						{	
							if($sub_type_search!=0 && $meta_type_search==0)
							{
								$sql_page_typea = mysql_query("SELECT MAX( general_community_profiles_id ) FROM general_community_profiles WHERE type_id ='".$page_type_search."' AND subtype_id='".$sub_type_search."' AND community_id='".$match_id."'");
							}
							elseif($sub_type_search!=0 && $meta_type_search!=0)
							{
								$sql_page_typea = mysql_query("SELECT MAX( general_community_profiles_id ) FROM general_community_profiles WHERE type_id ='".$page_type_search."' AND subtype_id='".$sub_type_search."' AND metatype_id='".$meta_type_search."' AND community_id='".$match_id."'");
							}
							else
							{
								$sql_page_typea = mysql_query("SELECT MAX( general_community_profiles_id ) FROM general_community_profiles WHERE type_id ='".$page_type_search."' AND community_id='".$match_id."'");
							}
							$run_page_typea = mysql_fetch_assoc($sql_page_typea);
							if($run_page_typea['MAX( general_community_profiles_id )']!=NULL)
							{
								//echo "hii";
								$sql_string = mysql_query("SELECT * FROM $match_table WHERE community_id='".$match_id."'");
							}
						}
					}
					if($match_table=="artist_event" || $match_table=="artist_project" || $match_table=="community_event" || $match_table=="community_project")
					{
						if($match_table=="artist_project")
						{
							$profiles_table = 'artist_project_profiles';
							$profiles_id = 'artist_project_id';
						}
						if($match_table=="community_project")
						{
							$profiles_table = 'community_project_profiles';
							$profiles_id = 'community_project_id';
						}
						if($match_table=="artist_event")
						{
							$profiles_table = 'artist_event_profiles';
							$profiles_id = 'artist_event_id';
						}
						if($match_table=="community_event")
						{
							$profiles_table = 'community_event_profiles';
							$profiles_id = 'community_event_id';
						}
						
						if($city_search!="")
						{
							$sql_all_art = mysql_query("SELECT * FROM $match_table WHERE id='".$match_id."' AND city='".$city_search."' AND del_status=0 AND active=0 AND image_name!='' AND profile_url!=''");
						}
						else
						{
							$sql_all_art = mysql_query("SELECT * FROM $match_table WHERE id='".$match_id."' AND del_status=0 AND active=0 AND image_name!='' AND profile_url!=''");
						}
						
						if(mysql_num_rows($sql_all_art)>0)
						{	
							if($sub_type_search!=0 && $meta_type_search==0)
							{
								$sql_page_typea = mysql_query("SELECT MAX( id ) FROM $profiles_table WHERE type_id ='".$page_type_search."' AND subtype_id='".$sub_type_search."' AND $profiles_id='".$match_id."'");
							}
							elseif($sub_type_search!=0 && $meta_type_search!=0)
							{
								$sql_page_typea = mysql_query("SELECT MAX( id ) FROM $profiles_table WHERE type_id ='".$page_type_search."' AND subtype_id='".$sub_type_search."' AND metatype_id='".$meta_type_search."' AND $profiles_id='".$match_id."'");
							}
							else
							{
								$sql_page_typea = mysql_query("SELECT MAX( id ) FROM $profiles_table WHERE type_id ='".$page_type_search."' AND $profiles_id='".$match_id."'");
							}
							$run_page_typea = mysql_fetch_assoc($sql_page_typea);
							if($run_page_typea['MAX( id )']!=NULL)
							{
								//echo "hii";
								$sql_string = mysql_query("SELECT * FROM $match_table WHERE id='".$match_id."'");
							}
						}	
					}
					if($match_table=="general_media")
					{
						$sql_med_ci = mysql_query("SELECT * FROM $match_table WHERE id='".$match_id."' AND delete_status=0 AND media_status=0");
						if($city_search!="")
						{
							if(mysql_num_rows($sql_med_ci)>0)
							{
								if($row_mede['from_info']!="")
								{
									$exp_from = explode('|',$row_mede['from_info']);
									if($exp_from[0]=='general_artist')
									{
										$sql_get_tables = mysql_query("SELECT * FROM $exp_from[0] WHERE artist_id='".$exp_from[1]."' AND city='".$city_search."' AND status=0 AND active=0");
									}
									elseif($exp_from[0]=='general_community')
									{
										$sql_get_tables = mysql_query("SELECT * FROM $exp_from[0] WHERE community_id='".$exp_from[1]."' AND city='".$city_search."' AND status=0 AND active=0");
									}
									elseif($exp_from[0]=='community_event' || $exp_from[0]=='artist_event' || $exp_from[0]=='artist_project' || $exp_from[0]=='community_project')
									{
										$sql_get_tables = mysql_query("SELECT * FROM $exp_from[0] WHERE id='".$exp_from[1]."' AND city='".$city_search."'");
									}
								}
								elseif($row_mede['creator_info']!="")
								{
									$exp_from = explode('|',$row_mede['creator_info']);
									if($exp_from[0]=='general_artist')
									{
										$sql_get_tables = mysql_query("SELECT * FROM $exp_from[0] WHERE artist_id='".$exp_from[1]."' city='".$city_search."' AND status=0 AND active=0");
									}
									elseif($exp_from[0]=='general_community')
									{
										$sql_get_tables = mysql_query("SELECT * FROM $exp_from[0] WHERE community_id='".$exp_from[1]."' AND city='".$city_search."' AND status=0 AND active=0");
									}
									elseif($exp_from[0]=='community_event' || $exp_from[0]=='artist_event' || $exp_from[0]=='artist_project' || $exp_from[0]=='community_project')
									{
										$sql_get_tables = mysql_query("SELECT * FROM $exp_from[0] WHERE id='".$exp_from[1]."' AND city='".$city_search."'");
									}
									
									if(isset($sql_get_tables) && !empty($sql_get_tables))
									{
										if(mysql_num_rows($sql_get_tables)>0)
										{
											//$sql_string = mysql_query("SELECT * FROM general_media WHERE id='".$match_id."'");
											//if(mysql_num_rows($sql_get_tables)>0)
											//{
												$sql_get_med = mysql_query("SELECT * FROM general_media WHERE id='".$match_id."' AND delete_status=0 AND media_status=0");
												$row_csc = mysql_fetch_assoc($sql_get_med);
												
												if($sub_type_search!=0 && $meta_type_search==0)
												{
													$sql_page_typeme = mysql_query("SELECT MAX( id ) FROM media_profiles_type WHERE type_id ='".$page_type_search."' AND subtype_id='".$sub_type_search."' AND media_id='".$row_csc['id']."'");
												}
												elseif($sub_type_search!=0 && $meta_type_search!=0)
												{
													$sql_page_typeme = mysql_query("SELECT MAX( id ) FROM media_profiles_type WHERE type_id ='".$page_type_search."' AND subtype_id='".$sub_type_search."' AND metatype_id='".$meta_type_search."' AND media_id='".$row_csc['id']."'");
												}
												else
												{
													$sql_page_typeme = mysql_query("SELECT MAX( id ) FROM media_profiles_type WHERE type_id ='".$page_type_search."' AND media_id='".$row_csc['id']."'");
												}
														
												$run_page_typeme = mysql_fetch_assoc($sql_page_typeme);
															
												if($run_page_typeme['MAX( id )']!=NULL)
												{
													//echo "hii";
													$sql_string = mysql_query("SELECT * FROM general_media WHERE id='".$match_id."' AND delete_status=0 AND media_status=0");
												}
											//}
										}
									}
								}
							}
						}
						else
						{
							$sql_get_tables = mysql_query("SELECT * FROM $match_table WHERE id='".$match_id."'");
							$row_csc = mysql_fetch_assoc($sql_get_med);
							
							if($sub_type_search!=0 && $meta_type_search==0)
							{
								$sql_page_typeme = mysql_query("SELECT MAX( id ) FROM media_profiles_type WHERE type_id ='".$page_type_search."' AND subtype_id='".$sub_type_search."' AND media_id='".$row_csc['id']."'");
							}
							elseif($sub_type_search!=0 && $meta_type_search!=0)
							{
								$sql_page_typeme = mysql_query("SELECT MAX( id ) FROM media_profiles_type WHERE type_id ='".$page_type_search."' AND subtype_id='".$sub_type_search."' AND metatype_id='".$meta_type_search."' AND media_id='".$row_csc['id']."'");
							}
							else
							{
								$sql_page_typeme = mysql_query("SELECT MAX( id ) FROM media_profiles_type WHERE type_id ='".$page_type_search."' AND media_id='".$row_csc['id']."'");
							}
									
							$run_page_typeme = mysql_fetch_assoc($sql_page_typeme);
										
							if($run_page_typeme['MAX( id )']!=NULL)
							{
								$sql_string = mysql_query("SELECT * FROM general_media WHERE id='".$match_id."' AND delete_status=0 AND media_status=0");
							}
						}
					}
				}
				elseif($profile_type_search=="select" && $country_search==0 && $page_type_search==0 && $city_search!="")
				{
					if($match_table=="general_artist")
					{
						$sql_string = mysql_query("SELECT * FROM $match_table WHERE artist_id='".$match_id."' AND city='".$city_search."' AND image_name!='' AND profile_url!=''");
					}
					
					if($match_table=="general_community")
					{
						$sql_string = mysql_query("SELECT * FROM $match_table WHERE community_id='".$match_id."' AND city='".$city_search."' AND image_name!='' AND profile_url!=''");
					}
					
					if($match_table=="artist_event" || $match_table=="community_event" || $match_table=="artist_project" || $match_table=="community_project")
					{
						$sql_string = mysql_query("SELECT * FROM $match_table WHERE id='".$match_id."' AND city='".$city_search."' AND del_status=0 AND active=0 AND image_name!='' AND profile_url!=''");
					}
					
					if($match_table=="general_media")
					{
						$sql_med_ci = mysql_query("SELECT * FROM $match_table WHERE id='".$match_id."' AND delete_status=0 AND media_status=0");
						if(mysql_num_rows($sql_med_ci)>0)
						{
							if($row_mede['from_info']!="")
							{
								$exp_from = explode('|',$row_mede['from_info']);
								if($exp_from[0]=='general_artist')
								{
									$sql_get_tables = mysql_query("SELECT * FROM $exp_from[0] WHERE artist_id='".$exp_from[1]."' AND city='".$city_search."' AND status=0 AND active=0");
								}
								elseif($exp_from[0]=='general_community')
								{
									$sql_get_tables = mysql_query("SELECT * FROM $exp_from[0] WHERE community_id='".$exp_from[1]."' AND city='".$city_search."' AND status=0 AND active=0");
								}
								elseif($exp_from[0]=='community_event' || $exp_from[0]=='artist_event' || $exp_from[0]=='artist_project' || $exp_from[0]=='community_project')
								{
									$sql_get_tables = mysql_query("SELECT * FROM $exp_from[0] WHERE id='".$exp_from[1]."' AND city='".$city_search."'");
								}
							
								if(isset($sql_get_tables) && !empty($sql_get_tables))
								{
									if(mysql_num_rows($sql_get_tables)>0)
									{
										$sql_string = mysql_query("SELECT * FROM general_media WHERE id='".$match_id."' AND delete_status=0 AND media_status=0");
									}
								}
							}
							elseif($row_mede['creator_info']!="")
							{
								$exp_from = explode('|',$row_mede['creator_info']);
								if($exp_from[0]=='general_artist')
								{
									$sql_get_tables = mysql_query("SELECT * FROM $exp_from[0] WHERE artist_id='".$exp_from[1]."' city='".$city_search."' AND status=0 AND active=0");
								}
								elseif($exp_from[0]=='general_community')
								{
									$sql_get_tables = mysql_query("SELECT * FROM $exp_from[0] WHERE community_id='".$exp_from[1]."' AND city='".$city_search."' AND status=0 AND active=0");
								}
								elseif($exp_from[0]=='community_event' || $exp_from[0]=='artist_event' || $exp_from[0]=='artist_project' || $exp_from[0]=='community_project')
								{
									$sql_get_tables = mysql_query("SELECT * FROM $exp_from[0] WHERE id='".$exp_from[1]."' AND country_id='".$country_search."' AND city='".$city_search."'");
								}
								
								if(isset($sql_get_tables) && !empty($sql_get_tables))
								{
									if(mysql_num_rows($sql_get_tables)>0)
									{
										$sql_string = mysql_query("SELECT * FROM general_media WHERE id='".$match_id."'");
									}
								}
							}
						}
					}
				}
				
				if(isset($sql_string))
				{
					if(mysql_num_rows($sql_string)>0)
					{
						while($row_user = mysql_fetch_assoc($sql_string))
						{
							$match_user[] = $row_user;
						}
					}
				}
				
				return $match_user;
			}
			
			////////////////////////////////////Only Page type Search////////////////////////////////////
			if($profile_type_search=="select" && $page_type_search!=0 && $country_search==0 && $state_search==0 && $city_search=="" && $match_string=="Search" && $match_table=="" && $match_id=="" && $alpha=="")
			{
				// echo "f4";
				// $re = "f4";
				// return $re;
				if($sub_type_search!=0 && $meta_type_search!=0)
				{
					$ans_art_org = array();
					$ans_com_org = array();
					$ans_arte_org = array();
					$ans_artp_org = array();
					$ans_come_org = array();
					$ans_comp_org = array();
					$ans_med_org = array();
					
					$sql_all_art = mysql_query("SELECT * FROM general_artist WHERE status=0 AND active=0 AND image_name!='' AND profile_url!=''");
					if(mysql_num_rows($sql_all_art)>0)
					{
						//$all_art_ans = array();
						$i = 0;
						while($all_art_ans = mysql_fetch_assoc($sql_all_art))
						{
							//$sql_page_type_1 = "SELECT MAX( general_artist_profiles_id ) FROM general_artist_profiles WHERE type_id ='".$page_type_search."' AND artist_id='".$all_art_ans['artist_id']."'";
							
							$sql_page_typea = mysql_query("SELECT MAX( general_artist_profiles_id ) FROM general_artist_profiles WHERE type_id ='".$page_type_search."' AND subtype_id='".$sub_type_search."' AND metatype_id='".$meta_type_search."' AND artist_id='".$all_art_ans['artist_id']."'");
							$run_page_typea = mysql_fetch_assoc($sql_page_typea);
							if($run_page_typea['MAX( general_artist_profiles_id )']!=NULL)
							{
								//echo "hii";
								$sql_artt = mysql_query("SELECT * FROM general_artist WHERE artist_id='".$all_art_ans['artist_id']."'");
								
								while($row_art = mysql_fetch_assoc($sql_artt))
								{
									$ans_art_org[$i] = $row_art;
								}
								$i = $i + 1;
							}
						}
					}
					
					$sql_all_com = mysql_query("SELECT * FROM general_community WHERE status=0 AND active=0 AND image_name!='' AND profile_url!=''");
					if(mysql_num_rows($sql_all_com)>0)
					{
						//$all_art_ans = array();
						$j = 0;
						while($all_com_ans = mysql_fetch_assoc($sql_all_com))
						{
							//$sql_page_typec_1 = "SELECT MAX( general_community_profiles_id ) FROM general_community_profiles WHERE type_id ='".$page_type_search."' AND community_id='".$all_com_ans['community_id']."'";
							
							$sql_page_typec = mysql_query("SELECT MAX( general_community_profiles_id ) FROM general_community_profiles WHERE type_id ='".$page_type_search."' AND subtype_id='".$sub_type_search."' AND metatype_id='".$meta_type_search."' AND community_id='".$all_com_ans['community_id']."'");
							$run_page_typec = mysql_fetch_assoc($sql_page_typec);
							
							if($run_page_typec['MAX( general_community_profiles_id )']!=NULL)
							{
								//echo "hii";
								$sql_comt = mysql_query("SELECT * FROM general_community WHERE community_id='".$all_com_ans['community_id']."'");
								
								while($row_comt = mysql_fetch_assoc($sql_comt))
								{
									$ans_com_org[$j] = $row_comt;
								}
								$j = $j + 1;
							}
						}
					}
					
					$sql_all_ae = mysql_query("SELECT * FROM artist_event WHERE del_status=0 AND active=0 AND image_name!='' AND profile_url!=''");
					if(mysql_num_rows($sql_all_ae)>0)
					{
						//$all_art_ans = array();
						$k = 0;
						while($all_ae_ans = mysql_fetch_assoc($sql_all_ae))
						{
							//$sql_page_typec_1 = "SELECT MAX( general_community_profiles_id ) FROM general_community_profiles WHERE type_id ='".$page_type_search."' AND community_id='".$all_com_ans['community_id']."'";
							
							$sql_page_typeae = mysql_query("SELECT MAX( id ) FROM artist_event_profiles WHERE type_id ='".$page_type_search."' AND subtype_id='".$sub_type_search."' AND metatype_id='".$meta_type_search."' AND artist_event_id='".$all_ae_ans['id']."'");
							$run_page_typeae = mysql_fetch_assoc($sql_page_typeae);
							
							if($run_page_typeae['MAX( id )']!=NULL)
							{
								//echo "hii";
								$sql_aet = mysql_query("SELECT * FROM artist_event WHERE id='".$all_ae_ans['id']."'");
								
								while($row_aet = mysql_fetch_assoc($sql_aet))
								{
									$row_aet['table_name'] = 'artist_event';
									$ans_arte_org[$k] = $row_aet;
								}
								$k = $k + 1;
							}
						}
					}
					
					$sql_all_ce = mysql_query("SELECT * FROM community_event WHERE del_status=0 AND active=0 AND image_name!='' AND profile_url!=''");
					if(mysql_num_rows($sql_all_ce)>0)
					{
						//$all_art_ans = array();
						$l = 0;
						while($all_ce_ans = mysql_fetch_assoc($sql_all_ce))
						{
							//$sql_page_typec_1 = "SELECT MAX( general_community_profiles_id ) FROM general_community_profiles WHERE type_id ='".$page_type_search."' AND community_id='".$all_com_ans['community_id']."'";
							
							$sql_page_typece = mysql_query("SELECT MAX( id ) FROM community_event_profiles WHERE type_id ='".$page_type_search."' AND subtype_id='".$sub_type_search."' AND metatype_id='".$meta_type_search."' AND community_event_id='".$all_ce_ans['id']."'");
							$run_page_typece = mysql_fetch_assoc($sql_page_typece);
							
							if($run_page_typece['MAX( id )']!=NULL)
							{
								//echo "hii";
								$sql_cet = mysql_query("SELECT * FROM community_event WHERE id='".$all_ce_ans['id']."'");
								
								while($row_cet = mysql_fetch_assoc($sql_cet))
								{
									$row_cet['table_name'] = 'community_event';
									$ans_come_org[$l] = $row_cet;
								}
								$l = $l + 1;
							}
						}
					}
					
					$sql_all_ap = mysql_query("SELECT * FROM artist_project WHERE del_status=0 AND active=0 AND image_name!='' AND profile_url!=''");
					if(mysql_num_rows($sql_all_ap)>0)
					{
						//$all_art_ans = array();
						$m = 0;
						while($all_ap_ans = mysql_fetch_assoc($sql_all_ap))
						{
							//$sql_page_typec_1 = "SELECT MAX( general_community_profiles_id ) FROM general_community_profiles WHERE type_id ='".$page_type_search."' AND community_id='".$all_com_ans['community_id']."'";
							
							$sql_page_typeap = mysql_query("SELECT MAX( id ) FROM artist_project_profiles WHERE type_id ='".$page_type_search."' AND subtype_id='".$sub_type_search."' AND metatype_id='".$meta_type_search."' AND artist_project_id='".$all_ap_ans['id']."'");
							$run_page_typeap = mysql_fetch_assoc($sql_page_typeap);
							
							if($run_page_typeap['MAX( id )']!=NULL)
							{
								//echo "hii";
								$sql_apt = mysql_query("SELECT * FROM artist_project WHERE id='".$all_ap_ans['id']."'");
								
								while($row_apt = mysql_fetch_assoc($sql_apt))
								{
									$row_apt['table_name'] = 'artist_project';
									$ans_artp_org[$m] = $row_apt;
								}
								$m = $m + 1;
							}
						}
					}
					
					$sql_all_cp = mysql_query("SELECT * FROM community_project WHERE del_status=0 AND active=0 AND image_name!='' AND profile_url!=''");
					if(mysql_num_rows($sql_all_cp)>0)
					{
						//$all_art_ans = array();
						$n = 0;
						while($all_cp_ans = mysql_fetch_assoc($sql_all_cp))
						{
							//$sql_page_typec_1 = "SELECT MAX( general_community_profiles_id ) FROM general_community_profiles WHERE type_id ='".$page_type_search."' AND community_id='".$all_com_ans['community_id']."'";
							
							$sql_page_typecp = mysql_query("SELECT MAX( id ) FROM community_project_profiles WHERE type_id ='".$page_type_search."' AND subtype_id='".$sub_type_search."' AND metatype_id='".$meta_type_search."' AND community_project_id='".$all_cp_ans['id']."'");
							$run_page_typecp = mysql_fetch_assoc($sql_page_typecp);
							
							if($run_page_typecp['MAX( id )']!=NULL)
							{
								//echo "hii";
								$sql_cpt = mysql_query("SELECT * FROM community_project WHERE id='".$all_cp_ans['id']."'");
								
								while($row_cpt = mysql_fetch_assoc($sql_cpt))
								{
									$row_cpt['table_name'] = 'community_project';
									$ans_comp_org[$n] = $row_cpt;
								}
								$n = $n + 1;
							}
						}
					}
					
					$sql_all_me = mysql_query("SELECT * FROM general_media WHERE delete_status=0 AND media_status=0");
					if(mysql_num_rows($sql_all_me)>0)
					{
						//$all_art_ans = array();
						$g = 0;
						while($all_me_ans = mysql_fetch_assoc($sql_all_me))
						{
							//$sql_page_typec_1 = "SELECT MAX( general_community_profiles_id ) FROM general_community_profiles WHERE type_id ='".$page_type_search."' AND community_id='".$all_com_ans['community_id']."'";
							
							$sql_page_typeme = mysql_query("SELECT MAX( id ) FROM media_profiles_type WHERE type_id ='".$page_type_search."' AND subtype_id='".$sub_type_search."' AND metatype_id='".$meta_type_search."' AND media_id='".$all_me_ans['id']."'");
							$run_page_typeme = mysql_fetch_assoc($sql_page_typeme);
							
							if($run_page_typeme['MAX( id )']!=NULL)
							{
								//echo "hii";
								$sql_med_t = mysql_query("SELECT * FROM general_media WHERE id='".$all_me_ans['id']."' AND delete_status=0 AND media_status=0");
								
								while($row_met = mysql_fetch_assoc($sql_med_t))
								{
									$ans_med_org[$g] = $row_met;
								}
								$g = $g + 1;
							}
						}
					}
					
					$main_ans = array_merge($ans_art_org,$ans_com_org,$ans_arte_org,$ans_artp_org,$ans_come_org,$ans_comp_org,$ans_med_org);
					return $main_ans;
				}
				/*elseif($sub_type_search==0 && $meta_type_search!=0)
				{
					$ans_art_org = array();
					$ans_com_org = array();
					$ans_arte_org = array();
					$ans_artp_org = array();
					$ans_come_org = array();
					$ans_comp_org = array();
					
					$sql_all_art = mysql_query("SELECT * FROM general_artist WHERE status=0");
					if(mysql_num_rows($sql_all_art)>0)
					{
						//$all_art_ans = array();
						$i = 0;
						while($all_art_ans = mysql_fetch_assoc($sql_all_art))
						{
							//$sql_page_type_1 = "SELECT MAX( general_artist_profiles_id ) FROM general_artist_profiles WHERE type_id ='".$page_type_search."' AND artist_id='".$all_art_ans['artist_id']."'";
							
							$sql_page_typea = mysql_query("SELECT MAX( general_artist_profiles_id ) FROM general_artist_profiles WHERE type_id ='".$page_type_search."' AND artist_id='".$all_art_ans['artist_id']."'");
							$run_page_typea = mysql_fetch_assoc($sql_page_typea);
							if($run_page_typea['MAX( general_artist_profiles_id )']!=NULL)
							{
								//echo "hii";
								$sql_artt = mysql_query("SELECT * FROM general_artist WHERE artist_id='".$all_art_ans['artist_id']."'");
								
								while($row_art = mysql_fetch_assoc($sql_artt))
								{
									$ans_art_org[$i] = $row_art;
								}
								$i = $i + 1;
							}
						}
					}
					
					$sql_all_com = mysql_query("SELECT * FROM general_community WHERE status=0");
					if(mysql_num_rows($sql_all_com)>0)
					{
						//$all_art_ans = array();
						$j = 0;
						while($all_com_ans = mysql_fetch_assoc($sql_all_com))
						{
							//$sql_page_typec_1 = "SELECT MAX( general_community_profiles_id ) FROM general_community_profiles WHERE type_id ='".$page_type_search."' AND community_id='".$all_com_ans['community_id']."'";
							
							$sql_page_typec = mysql_query("SELECT MAX( general_community_profiles_id ) FROM general_community_profiles WHERE type_id ='".$page_type_search."' AND community_id='".$all_com_ans['community_id']."'");
							$run_page_typec = mysql_fetch_assoc($sql_page_typec);
							
							if($run_page_typec['MAX( general_community_profiles_id )']!=NULL)
							{
								//echo "hii";
								$sql_comt = mysql_query("SELECT * FROM general_community WHERE community_id='".$all_com_ans['community_id']."'");
								
								while($row_comt = mysql_fetch_assoc($sql_comt))
								{
									$ans_com_org[$j] = $row_comt;
								}
								$j = $j + 1;
							}
						}
					}
					
					$sql_all_ae = mysql_query("SELECT * FROM artist_event");
					if(mysql_num_rows($sql_all_ae)>0)
					{
						//$all_art_ans = array();
						$k = 0;
						while($all_ae_ans = mysql_fetch_assoc($sql_all_ae))
						{
							//$sql_page_typec_1 = "SELECT MAX( general_community_profiles_id ) FROM general_community_profiles WHERE type_id ='".$page_type_search."' AND community_id='".$all_com_ans['community_id']."'";
							
							$sql_page_typeae = mysql_query("SELECT MAX( id ) FROM artist_event_profiles WHERE type_id ='".$page_type_search."' AND artist_event_id='".$all_ae_ans['id']."'");
							$run_page_typeae = mysql_fetch_assoc($sql_page_typeae);
							
							if($run_page_typeae['MAX( id )']!=NULL)
							{
								//echo "hii";
								$sql_aet = mysql_query("SELECT * FROM artist_event WHERE id='".$all_ae_ans['id']."'");
								
								while($row_aet = mysql_fetch_assoc($sql_aet))
								{
									$ans_arte_org[$k] = $row_aet;
								}
								$k = $k + 1;
							}
						}
					}
					
					$sql_all_ce = mysql_query("SELECT * FROM community_event");
					if(mysql_num_rows($sql_all_ce)>0)
					{
						//$all_art_ans = array();
						$l = 0;
						while($all_ce_ans = mysql_fetch_assoc($sql_all_ce))
						{
							//$sql_page_typec_1 = "SELECT MAX( general_community_profiles_id ) FROM general_community_profiles WHERE type_id ='".$page_type_search."' AND community_id='".$all_com_ans['community_id']."'";
							
							$sql_page_typece = mysql_query("SELECT MAX( id ) FROM community_event_profiles WHERE type_id ='".$page_type_search."' AND community_event_id='".$all_ce_ans['id']."'");
							$run_page_typece = mysql_fetch_assoc($sql_page_typece);
							
							if($run_page_typece['MAX( id )']!=NULL)
							{
								//echo "hii";
								$sql_cet = mysql_query("SELECT * FROM community_event WHERE id='".$all_ce_ans['id']."'");
								
								while($row_cet = mysql_fetch_assoc($sql_cet))
								{
									$ans_come_org[$l] = $row_cet;
								}
								$l = $l + 1;
							}
						}
					}
					
					$sql_all_ap = mysql_query("SELECT * FROM artist_project");
					if(mysql_num_rows($sql_all_ap)>0)
					{
						//$all_art_ans = array();
						$m = 0;
						while($all_ap_ans = mysql_fetch_assoc($sql_all_ap))
						{
							//$sql_page_typec_1 = "SELECT MAX( general_community_profiles_id ) FROM general_community_profiles WHERE type_id ='".$page_type_search."' AND community_id='".$all_com_ans['community_id']."'";
							
							$sql_page_typeap = mysql_query("SELECT MAX( id ) FROM artist_project_profiles WHERE type_id ='".$page_type_search."' AND artist_project_id='".$all_ap_ans['id']."'");
							$run_page_typeap = mysql_fetch_assoc($sql_page_typeap);
							
							if($run_page_typeap['MAX( id )']!=NULL)
							{
								//echo "hii";
								$sql_apt = mysql_query("SELECT * FROM artist_project WHERE id='".$all_ap_ans['id']."'");
								
								while($row_apt = mysql_fetch_assoc($sql_apt))
								{
									$ans_artp_org[$m] = $row_apt;
								}
								$m = $m + 1;
							}
						}
					}
					
					$sql_all_cp = mysql_query("SELECT * FROM community_project");
					if(mysql_num_rows($sql_all_cp)>0)
					{
						//$all_art_ans = array();
						$n = 0;
						while($all_cp_ans = mysql_fetch_assoc($sql_all_cp))
						{
							//$sql_page_typec_1 = "SELECT MAX( general_community_profiles_id ) FROM general_community_profiles WHERE type_id ='".$page_type_search."' AND community_id='".$all_com_ans['community_id']."'";
							
							$sql_page_typecp = mysql_query("SELECT MAX( id ) FROM community_project_profiles WHERE type_id ='".$page_type_search."' AND community_project_id='".$all_cp_ans['id']."'");
							$run_page_typecp = mysql_fetch_assoc($sql_page_typecp);
							
							if($run_page_typecp['MAX( id )']!=NULL)
							{
								//echo "hii";
								$sql_cpt = mysql_query("SELECT * FROM community_project WHERE id='".$all_cp_ans['id']."'");
								
								while($row_cpt = mysql_fetch_assoc($sql_cpt))
								{
									$ans_comp_org[$n] = $row_cpt;
								}
								$n = $n + 1;
							}
						}
					}
					$va = "2";
					$main_ans = array_merge($ans_art_org,$ans_com_org,$ans_arte_org,$ans_artp_org,$ans_come_org,$ans_comp_org);
					return $va;
				}*/
				elseif($sub_type_search!=0 && $meta_type_search==0)
				{
					$ans_art_org = array();
					$ans_com_org = array();
					$ans_arte_org = array();
					$ans_artp_org = array();
					$ans_come_org = array();
					$ans_comp_org = array();
					$ans_med_org = array();
					
					$sql_all_art = mysql_query("SELECT * FROM general_artist WHERE status=0 AND active=0 AND image_name!='' AND profile_url!=''");
					if(mysql_num_rows($sql_all_art)>0)
					{
						//$all_art_ans = array();
						$i = 0;
						while($all_art_ans = mysql_fetch_assoc($sql_all_art))
						{
							//$sql_page_type_1 = "SELECT MAX( general_artist_profiles_id ) FROM general_artist_profiles WHERE type_id ='".$page_type_search."' AND artist_id='".$all_art_ans['artist_id']."'";
							
							$sql_page_typea = mysql_query("SELECT MAX( general_artist_profiles_id ) FROM general_artist_profiles WHERE type_id ='".$page_type_search."' AND subtype_id='".$sub_type_search."' AND artist_id='".$all_art_ans['artist_id']."'");
							$run_page_typea = mysql_fetch_assoc($sql_page_typea);
							if($run_page_typea['MAX( general_artist_profiles_id )']!=NULL)
							{
								//echo "hii";
								$sql_artt = mysql_query("SELECT * FROM general_artist WHERE artist_id='".$all_art_ans['artist_id']."'");
								
								while($row_art = mysql_fetch_assoc($sql_artt))
								{
									$ans_art_org[$i] = $row_art;
								}
								$i = $i + 1;
							}
						}
					}
					
					$sql_all_com = mysql_query("SELECT * FROM general_community WHERE status=0 AND active=0 AND image_name!='' AND profile_url!=''");
					if(mysql_num_rows($sql_all_com)>0)
					{
						//$all_art_ans = array();
						$j = 0;
						while($all_com_ans = mysql_fetch_assoc($sql_all_com))
						{
							//$sql_page_typec_1 = "SELECT MAX( general_community_profiles_id ) FROM general_community_profiles WHERE type_id ='".$page_type_search."' AND community_id='".$all_com_ans['community_id']."'";
							
							$sql_page_typec = mysql_query("SELECT MAX( general_community_profiles_id ) FROM general_community_profiles WHERE type_id ='".$page_type_search."' AND subtype_id='".$sub_type_search."' AND community_id='".$all_com_ans['community_id']."'");
							$run_page_typec = mysql_fetch_assoc($sql_page_typec);
							
							if($run_page_typec['MAX( general_community_profiles_id )']!=NULL)
							{
								//echo "hii";
								$sql_comt = mysql_query("SELECT * FROM general_community WHERE community_id='".$all_com_ans['community_id']."'");
								
								while($row_comt = mysql_fetch_assoc($sql_comt))
								{
									$ans_com_org[$j] = $row_comt;
								}
								$j = $j + 1;
							}
						}
					}
					
					$sql_all_ae = mysql_query("SELECT * FROM artist_event WHERE del_status=0 AND active=0 AND image_name!='' AND profile_url!=''");
					if(mysql_num_rows($sql_all_ae)>0)
					{
						//$all_art_ans = array();
						$k = 0;
						while($all_ae_ans = mysql_fetch_assoc($sql_all_ae))
						{
							//$sql_page_typec_1 = "SELECT MAX( general_community_profiles_id ) FROM general_community_profiles WHERE type_id ='".$page_type_search."' AND community_id='".$all_com_ans['community_id']."'";
							
							$sql_page_typeae = mysql_query("SELECT MAX( id ) FROM artist_event_profiles WHERE type_id ='".$page_type_search."' AND subtype_id='".$sub_type_search."' AND artist_event_id='".$all_ae_ans['id']."'");
							$run_page_typeae = mysql_fetch_assoc($sql_page_typeae);
							
							if($run_page_typeae['MAX( id )']!=NULL)
							{
								//echo "hii";
								$sql_aet = mysql_query("SELECT * FROM artist_event WHERE id='".$all_ae_ans['id']."'");
								
								while($row_aet = mysql_fetch_assoc($sql_aet))
								{
									$row_aet['table_name'] = 'artist_event';
									$ans_arte_org[$k] = $row_aet;
								}
								$k = $k + 1;
							}
						}
					}
					
					$sql_all_ce = mysql_query("SELECT * FROM community_event WHERE del_status=0 AND active=0 AND image_name!='' AND profile_url!=''");
					if(mysql_num_rows($sql_all_ce)>0)
					{
						//$all_art_ans = array();
						$l = 0;
						while($all_ce_ans = mysql_fetch_assoc($sql_all_ce))
						{
							//$sql_page_typec_1 = "SELECT MAX( general_community_profiles_id ) FROM general_community_profiles WHERE type_id ='".$page_type_search."' AND community_id='".$all_com_ans['community_id']."'";
							
							$sql_page_typece = mysql_query("SELECT MAX( id ) FROM community_event_profiles WHERE type_id ='".$page_type_search."' AND subtype_id='".$sub_type_search."' AND community_event_id='".$all_ce_ans['id']."'");
							$run_page_typece = mysql_fetch_assoc($sql_page_typece);
							
							if($run_page_typece['MAX( id )']!=NULL)
							{
								//echo "hii";
								$sql_cet = mysql_query("SELECT * FROM community_event WHERE id='".$all_ce_ans['id']."'");
								
								while($row_cet = mysql_fetch_assoc($sql_cet))
								{
									$row_cet['table_name'] = 'community_event';
									$ans_come_org[$l] = $row_cet;
								}
								$l = $l + 1;
							}
						}
					}
					
					$sql_all_ap = mysql_query("SELECT * FROM artist_project WHERE del_status=0 AND active=0 AND image_name!='' AND profile_url!=''");
					if(mysql_num_rows($sql_all_ap)>0)
					{
						//$all_art_ans = array();
						$m = 0;
						while($all_ap_ans = mysql_fetch_assoc($sql_all_ap))
						{
							//$sql_page_typec_1 = "SELECT MAX( general_community_profiles_id ) FROM general_community_profiles WHERE type_id ='".$page_type_search."' AND community_id='".$all_com_ans['community_id']."'";
							
							$sql_page_typeap = mysql_query("SELECT MAX( id ) FROM artist_project_profiles WHERE type_id ='".$page_type_search."' AND subtype_id='".$sub_type_search."' AND artist_project_id='".$all_ap_ans['id']."'");
							$run_page_typeap = mysql_fetch_assoc($sql_page_typeap);
							
							if($run_page_typeap['MAX( id )']!=NULL)
							{
								//echo "hii";
								$sql_apt = mysql_query("SELECT * FROM artist_project WHERE id='".$all_ap_ans['id']."'");
								
								while($row_apt = mysql_fetch_assoc($sql_apt))
								{
									$row_apt['table_name'] = 'artist_project';
									$ans_artp_org[$m] = $row_apt;
								}
								$m = $m + 1;
							}
						}
					}
					
					$sql_all_cp = mysql_query("SELECT * FROM community_project WHERE del_status=0 AND active=0 AND image_name!='' AND profile_url!=''");
					if(mysql_num_rows($sql_all_cp)>0)
					{
						//$all_art_ans = array();
						$n = 0;
						while($all_cp_ans = mysql_fetch_assoc($sql_all_cp))
						{
							//$sql_page_typec_1 = "SELECT MAX( general_community_profiles_id ) FROM general_community_profiles WHERE type_id ='".$page_type_search."' AND community_id='".$all_com_ans['community_id']."'";
							
							$sql_page_typecp = mysql_query("SELECT MAX( id ) FROM community_project_profiles WHERE type_id ='".$page_type_search."' AND subtype_id='".$sub_type_search."' AND community_project_id='".$all_cp_ans['id']."'");
							$run_page_typecp = mysql_fetch_assoc($sql_page_typecp);
							
							if($run_page_typecp['MAX( id )']!=NULL)
							{
								//echo "hii";
								$sql_cpt = mysql_query("SELECT * FROM community_project WHERE id='".$all_cp_ans['id']."'");
								
								while($row_cpt = mysql_fetch_assoc($sql_cpt))
								{
									$row_cpt['table_name'] = 'community_project';
									$ans_comp_org[$n] = $row_cpt;
								}
								$n = $n + 1;
							}
						}
					}
					
					$sql_all_me = mysql_query("SELECT * FROM general_media WHERE delete_status=0 AND media_status=0");
					if(mysql_num_rows($sql_all_me)>0)
					{
						//$all_art_ans = array();
						$g = 0;
						while($all_me_ans = mysql_fetch_assoc($sql_all_me))
						{
							//$sql_page_typec_1 = "SELECT MAX( general_community_profiles_id ) FROM general_community_profiles WHERE type_id ='".$page_type_search."' AND community_id='".$all_com_ans['community_id']."'";
							
							$sql_page_typeme = mysql_query("SELECT MAX( id ) FROM media_profiles_type WHERE type_id ='".$page_type_search."' AND subtype_id='".$sub_type_search."' AND media_id='".$all_me_ans['id']."'");
							$run_page_typeme = mysql_fetch_assoc($sql_page_typeme);
							
							if($run_page_typeme['MAX( id )']!=NULL)
							{
								//echo "hii";
								$sql_med_t = mysql_query("SELECT * FROM general_media WHERE id='".$all_me_ans['id']."' AND delete_status=0 AND media_status=0");
								
								while($row_met = mysql_fetch_assoc($sql_med_t))
								{
									$ans_med_org[$g] = $row_met;
								}
								$g = $g + 1;
							}
						}
					}
					
					//$va = "3";
					$main_ans = array_merge($ans_art_org,$ans_com_org,$ans_arte_org,$ans_artp_org,$ans_come_org,$ans_comp_org,$ans_med_org);
					return $main_ans;
				}
				else
				{
					$ans_art_org = array();
					$ans_com_org = array();
					$ans_arte_org = array();
					$ans_artp_org = array();
					$ans_come_org = array();
					$ans_comp_org = array();
					$ans_med_org = array();
					
					$sql_all_art = mysql_query("SELECT * FROM general_artist WHERE status=0 AND active=0 AND image_name!='' AND profile_url!=''");
					if(mysql_num_rows($sql_all_art)>0)
					{
						//$all_art_ans = array();
						$i = 0;
						while($all_art_ans = mysql_fetch_assoc($sql_all_art))
						{
							//$sql_page_type_1 = "SELECT MAX( general_artist_profiles_id ) FROM general_artist_profiles WHERE type_id ='".$page_type_search."' AND artist_id='".$all_art_ans['artist_id']."'";
							
							$sql_page_typea = mysql_query("SELECT MAX( general_artist_profiles_id ) FROM general_artist_profiles WHERE type_id ='".$page_type_search."' AND artist_id='".$all_art_ans['artist_id']."'");
							$run_page_typea = mysql_fetch_assoc($sql_page_typea);
							if($run_page_typea['MAX( general_artist_profiles_id )']!=NULL)
							{
								//echo "hii";
								$sql_artt = mysql_query("SELECT * FROM general_artist WHERE artist_id='".$all_art_ans['artist_id']."'");
								
								while($row_art = mysql_fetch_assoc($sql_artt))
								{
									$ans_art_org[$i] = $row_art;
								}
								$i = $i + 1;
							}
						}
					}
					
					$sql_all_com = mysql_query("SELECT * FROM general_community WHERE status=0 AND active=0 AND image_name!='' AND profile_url!=''");
					if(mysql_num_rows($sql_all_com)>0)
					{
						//$all_art_ans = array();
						$j = 0;
						while($all_com_ans = mysql_fetch_assoc($sql_all_com))
						{
							//$sql_page_typec_1 = "SELECT MAX( general_community_profiles_id ) FROM general_community_profiles WHERE type_id ='".$page_type_search."' AND community_id='".$all_com_ans['community_id']."'";
							
							$sql_page_typec = mysql_query("SELECT MAX( general_community_profiles_id ) FROM general_community_profiles WHERE type_id ='".$page_type_search."' AND community_id='".$all_com_ans['community_id']."'");
							$run_page_typec = mysql_fetch_assoc($sql_page_typec);
							
							if($run_page_typec['MAX( general_community_profiles_id )']!=NULL)
							{
								//echo "hii";
								$sql_comt = mysql_query("SELECT * FROM general_community WHERE community_id='".$all_com_ans['community_id']."'");
								
								while($row_comt = mysql_fetch_assoc($sql_comt))
								{
									$ans_com_org[$j] = $row_comt;
								}
								$j = $j + 1;
							}
						}
					}
					
					$sql_all_ae = mysql_query("SELECT * FROM artist_event WHERE del_status=0 AND active=0 AND image_name!='' AND profile_url!=''");
					if(mysql_num_rows($sql_all_ae)>0)
					{
						//$all_art_ans = array();
						$k = 0;
						while($all_ae_ans = mysql_fetch_assoc($sql_all_ae))
						{
							//$sql_page_typec_1 = "SELECT MAX( general_community_profiles_id ) FROM general_community_profiles WHERE type_id ='".$page_type_search."' AND community_id='".$all_com_ans['community_id']."'";
							
							$sql_page_typeae = mysql_query("SELECT MAX( id ) FROM artist_event_profiles WHERE type_id ='".$page_type_search."' AND artist_event_id='".$all_ae_ans['id']."'");
							$run_page_typeae = mysql_fetch_assoc($sql_page_typeae);
							
							if($run_page_typeae['MAX( id )']!=NULL)
							{
								//echo "hii";
								$sql_aet = mysql_query("SELECT * FROM artist_event WHERE id='".$all_ae_ans['id']."'");
								
								while($row_aet = mysql_fetch_assoc($sql_aet))
								{
									$row_aet['table_name'] = 'artist_event';
									$ans_arte_org[$k] = $row_aet;
								}
								$k = $k + 1;
							}
						}
					}
					
					$sql_all_ce = mysql_query("SELECT * FROM community_event WHERE del_status=0 AND active=0 AND image_name!='' AND profile_url!=''");
					if(mysql_num_rows($sql_all_ce)>0)
					{
						//$all_art_ans = array();
						$l = 0;
						while($all_ce_ans = mysql_fetch_assoc($sql_all_ce))
						{
							//$sql_page_typec_1 = "SELECT MAX( general_community_profiles_id ) FROM general_community_profiles WHERE type_id ='".$page_type_search."' AND community_id='".$all_com_ans['community_id']."'";
							
							$sql_page_typece = mysql_query("SELECT MAX( id ) FROM community_event_profiles WHERE type_id ='".$page_type_search."' AND community_event_id='".$all_ce_ans['id']."'");
							$run_page_typece = mysql_fetch_assoc($sql_page_typece);
							
							if($run_page_typece['MAX( id )']!=NULL)
							{
								//echo "hii";
								$sql_cet = mysql_query("SELECT * FROM community_event WHERE id='".$all_ce_ans['id']."'");
								
								while($row_cet = mysql_fetch_assoc($sql_cet))
								{
									$row_cet['table_name'] = 'community_event';
									$ans_come_org[$l] = $row_cet;
								}
								$l = $l + 1;
							}
						}
					}
					
					$sql_all_ap = mysql_query("SELECT * FROM artist_project WHERE del_status=0 AND active=0 AND image_name!='' AND profile_url!=''");
					if(mysql_num_rows($sql_all_ap)>0)
					{
						//$all_art_ans = array();
						$m = 0;
						while($all_ap_ans = mysql_fetch_assoc($sql_all_ap))
						{
							//$sql_page_typec_1 = "SELECT MAX( general_community_profiles_id ) FROM general_community_profiles WHERE type_id ='".$page_type_search."' AND community_id='".$all_com_ans['community_id']."'";
							
							$sql_page_typeap = mysql_query("SELECT MAX( id ) FROM artist_project_profiles WHERE type_id ='".$page_type_search."' AND artist_project_id='".$all_ap_ans['id']."'");
							$run_page_typeap = mysql_fetch_assoc($sql_page_typeap);
							
							if($run_page_typeap['MAX( id )']!=NULL)
							{
								//echo "hii";
								$sql_apt = mysql_query("SELECT * FROM artist_project WHERE id='".$all_ap_ans['id']."'");
								
								while($row_apt = mysql_fetch_assoc($sql_apt))
								{
									$row_apt['table_name'] = 'artist_project';
									$ans_artp_org[$m] = $row_apt;
								}
								$m = $m + 1;
							}
						}
					}
					
					$sql_all_cp = mysql_query("SELECT * FROM community_project WHERE del_status=0 AND active=0 AND image_name!='' AND profile_url!=''");
					if(mysql_num_rows($sql_all_cp)>0)
					{
						//$all_art_ans = array();
						$n = 0;
						while($all_cp_ans = mysql_fetch_assoc($sql_all_cp))
						{
							//$sql_page_typec_1 = "SELECT MAX( general_community_profiles_id ) FROM general_community_profiles WHERE type_id ='".$page_type_search."' AND community_id='".$all_com_ans['community_id']."'";
							
							$sql_page_typecp = mysql_query("SELECT MAX( id ) FROM community_project_profiles WHERE type_id ='".$page_type_search."' AND community_project_id='".$all_cp_ans['id']."'");
							$run_page_typecp = mysql_fetch_assoc($sql_page_typecp);
							
							if($run_page_typecp['MAX( id )']!=NULL)
							{
								//echo "hii";
								$sql_cpt = mysql_query("SELECT * FROM community_project WHERE id='".$all_cp_ans['id']."'");
								
								while($row_cpt = mysql_fetch_assoc($sql_cpt))
								{
									$row_cpt['table_name'] = 'community_project';
									$ans_comp_org[$n] = $row_cpt;
								}
								$n = $n + 1;
							}
						}
					}
					
					$sql_all_me = mysql_query("SELECT * FROM general_media WHERE delete_status=0 AND media_status=0");
					if(mysql_num_rows($sql_all_me)>0)
					{
						//$all_art_ans = array();
						$g = 0;
						while($all_me_ans = mysql_fetch_assoc($sql_all_me))
						{
							//$sql_page_typec_1 = "SELECT MAX( general_community_profiles_id ) FROM general_community_profiles WHERE type_id ='".$page_type_search."' AND community_id='".$all_com_ans['community_id']."'";
							
							$sql_page_typeme = mysql_query("SELECT MAX( id ) FROM media_profiles_type WHERE type_id ='".$page_type_search."' AND media_id='".$all_me_ans['id']."'");
							$run_page_typeme = mysql_fetch_assoc($sql_page_typeme);
							
							if($run_page_typeme['MAX( id )']!=NULL)
							{
								//echo "hii";
								$sql_med_t = mysql_query("SELECT * FROM general_media WHERE id='".$all_me_ans['id']."' AND delete_status=0 AND media_status=0");
								
								while($row_met = mysql_fetch_assoc($sql_med_t))
								{
									$ans_med_org[$g] = $row_met;
								}
								$g = $g + 1;
							}
						}
					}
					
					$main_ans = array_merge($ans_art_org,$ans_com_org,$ans_arte_org,$ans_artp_org,$ans_come_org,$ans_comp_org,$ans_med_org);
					return $main_ans;
				}
			}
			
			////////////////////////////////////Page type Search with country////////////////////////////////////
			if($profile_type_search=="select" && $page_type_search!=0 && $country_search!=0 && $match_string=="Search" && $match_table=="" && $match_id=="" && $alpha=="")
			{
				// echo "f3";
				// $re = "f3";
				// return $re;
				$ans_artcps = array();
				$ans_comcps = array();
				$ans_artpcps = array();
				$ans_compcps = array();
				$ans_artecps = array();
				$ans_comecps = array();
				
				if($state_search==0 && $city_search=="")
				{
					$sql_art = mysql_query("SELECT * FROM general_artist WHERE country_id='".$country_search."' AND status=0 AND active=0 AND image_name!='' AND profile_url!=''");
					$sql_com = mysql_query("SELECT * FROM general_community WHERE country_id='".$country_search."' AND status=0 AND active=0 AND image_name!='' AND profile_url!=''");
					$sql_artp = mysql_query("SELECT * FROM artist_project WHERE country_id='".$country_search."' AND del_status=0 AND active=0 AND image_name!='' AND profile_url!=''");
					$sql_comp = mysql_query("SELECT * FROM community_project WHERE country_id='".$country_search."' AND del_status=0 AND active=0 AND image_name!='' AND profile_url!=''");
					$sql_arte = mysql_query("SELECT * FROM artist_event WHERE country_id='".$country_search."' AND image_name!='' AND profile_url!='' AND del_status=0 AND active=0");
					$sql_come = mysql_query("SELECT * FROM community_event WHERE country_id='".$country_search."' AND image_name!='' AND profile_url!='' AND del_status=0 AND active=0");
				}
				elseif($state_search!=0 && $city_search=="")
				{
					$sql_art = mysql_query("SELECT * FROM general_artist WHERE country_id='".$country_search."' AND state_id='".$state_search."' AND status=0 AND active=0 AND image_name!='' AND profile_url!=''");
					$sql_com = mysql_query("SELECT * FROM general_community WHERE country_id='".$country_search."' AND state_id='".$state_search."' AND status=0 AND active=0 AND image_name!='' AND profile_url!=''");
					$sql_artp = mysql_query("SELECT * FROM artist_project WHERE country_id='".$country_search."' AND state_id='".$state_search."' AND del_status=0 AND active=0 AND image_name!='' AND profile_url!=''");
					$sql_comp = mysql_query("SELECT * FROM community_project WHERE country_id='".$country_search."' AND state_id='".$state_search."' AND del_status=0 AND active=0 AND image_name!='' AND profile_url!=''");
					$sql_arte = mysql_query("SELECT * FROM artist_event WHERE country_id='".$country_search."' AND state_id='".$state_search."' AND image_name!='' AND profile_url!='' AND del_status=0 AND active=0");
					$sql_come = mysql_query("SELECT * FROM community_event WHERE country_id='".$country_search."' AND state_id='".$state_search."' AND image_name!='' AND profile_url!='' AND del_status=0 AND active=0");
				}
				elseif($state_search==0 && $city_search!="")
				{
					$sql_art = mysql_query("SELECT * FROM general_artist WHERE country_id='".$country_search."' AND city='".$city_search."' AND status=0 AND active=0 AND image_name!='' AND profile_url!=''");
					$sql_com = mysql_query("SELECT * FROM general_community WHERE country_id='".$country_search."' AND city='".$city_search."' AND status=0 AND active=0 AND image_name!='' AND profile_url!=''");
					$sql_artp = mysql_query("SELECT * FROM artist_project WHERE country_id='".$country_search."' AND city='".$city_search."' AND del_status=0 AND active=0 AND image_name!='' AND profile_url!=''");
					$sql_comp = mysql_query("SELECT * FROM community_project WHERE country_id='".$country_search."' AND city='".$city_search."' AND del_status=0 AND active=0 AND image_name!='' AND profile_url!=''");
					$sql_arte = mysql_query("SELECT * FROM artist_event WHERE country_id='".$country_search."' AND city='".$city_search."' AND image_name!='' AND profile_url!='' AND del_status=0 AND active=0");
					$sql_come = mysql_query("SELECT * FROM community_event WHERE country_id='".$country_search."' AND city='".$city_search."' AND image_name!='' AND profile_url!='' AND del_status=0 AND active=0");
				}
				elseif($state_search!=0 && $city_search!="")
				{
					$sql_art = mysql_query("SELECT * FROM general_artist WHERE country_id='".$country_search."' AND state_id='".$state_search."' AND city='".$city_search."' AND status=0 AND active=0 AND image_name!='' AND profile_url!=''");
					$sql_com = mysql_query("SELECT * FROM general_community WHERE country_id='".$country_search."' AND state_id='".$state_search."' AND city='".$city_search."' AND status=0 AND active=0 AND image_name!='' AND profile_url!=''");
					$sql_artp = mysql_query("SELECT * FROM artist_project WHERE country_id='".$country_search."' AND state_id='".$state_search."' AND city='".$city_search."' AND del_status=0 AND active=0 AND image_name!='' AND profile_url!=''");
					$sql_comp = mysql_query("SELECT * FROM community_project WHERE country_id='".$country_search."' AND state_id='".$state_search."' AND city='".$city_search."' AND del_status=0 AND active=0 AND image_name!='' AND profile_url!=''");
					$sql_arte = mysql_query("SELECT * FROM artist_event WHERE country_id='".$country_search."' AND state_id='".$state_search."' AND city='".$city_search."' AND image_name!='' AND profile_url!='' AND del_status=0 AND active=0");
					$sql_come = mysql_query("SELECT * FROM community_event WHERE country_id='".$country_search."' AND state_id='".$state_search."' AND city='".$city_search."' AND image_name!='' AND profile_url!='' AND del_status=0 AND active=0");
				}
				
				if(mysql_num_rows($sql_art)>0)
				{
					while($row_artc = mysql_fetch_assoc($sql_art))
					{
						$ans_artc = $row_artc;
						//var_dump($ans_artc);
						
						if($sub_type_search!=0 && $meta_type_search==0)
						{
							$sql_page_typea = mysql_query("SELECT MAX( general_artist_profiles_id ) FROM general_artist_profiles WHERE type_id ='".$page_type_search."' AND subtype_id='".$sub_type_search."' AND artist_id='".$ans_artc['artist_id']."'");
						}
						elseif($sub_type_search!=0 && $meta_type_search!=0)
						{
							$sql_page_typea = mysql_query("SELECT MAX( general_artist_profiles_id ) FROM general_artist_profiles WHERE type_id ='".$page_type_search."' AND subtype_id='".$sub_type_search."' AND metatype_id='".$meta_type_search."' AND artist_id='".$ans_artc['artist_id']."'");
						}
						else
						{
							$sql_page_typea = mysql_query("SELECT MAX( general_artist_profiles_id ) FROM general_artist_profiles WHERE type_id ='".$page_type_search."' AND artist_id='".$ans_artc['artist_id']."'");
						}
						$run_page_typea = mysql_fetch_assoc($sql_page_typea);
						if($run_page_typea['MAX( general_artist_profiles_id )']!=NULL)
						{
							//echo "hii";
							$sql_string = mysql_query("SELECT * FROM general_artist WHERE artist_id='".$ans_artc['artist_id']."'");
							if(mysql_num_rows($sql_string)>0)
							{
								while($row_artcps = mysql_fetch_assoc($sql_string))
								{
									$ans_artcps[] = $row_artcps;
								}
							}
						}	
					}
				}
				
				if(mysql_num_rows($sql_com)>0)
				{
					while($row_comc = mysql_fetch_assoc($sql_com))
					{
						$ans_comc = $row_comc;
						
						if($sub_type_search!=0 && $meta_type_search==0)
						{
							$sql_page_typea = mysql_query("SELECT MAX( general_community_profiles_id ) FROM general_community_profiles WHERE type_id ='".$page_type_search."' AND subtype_id='".$sub_type_search."' AND community_id='".$ans_comc['community_id']."'");
						}
						elseif($sub_type_search!=0 && $meta_type_search!=0)
						{
							$sql_page_typea = mysql_query("SELECT MAX( general_community_profiles_id ) FROM general_community_profiles WHERE type_id ='".$page_type_search."' AND subtype_id='".$sub_type_search."' AND metatype_id='".$meta_type_search."' AND community_id='".$ans_comc['community_id']."'");
						}
						else
						{
							$sql_page_typea = mysql_query("SELECT MAX( general_community_profiles_id ) FROM general_community_profiles WHERE type_id ='".$page_type_search."' AND community_id='".$ans_comc['community_id']."'");
						}
						$run_page_typea = mysql_fetch_assoc($sql_page_typea);
						if($run_page_typea['MAX( general_community_profiles_id )']!=NULL)
						{
							//echo "hii";
							$sql_stringc = mysql_query("SELECT * FROM general_community WHERE community_id='".$ans_comc['community_id']."'");
							if(mysql_num_rows($sql_stringc)>0)
							{
								while($row_comcps = mysql_fetch_assoc($sql_stringc))
								{
									$ans_comcps[] = $row_comcps;
								}
							}
						}
					}
				}
				
				if(mysql_num_rows($sql_artp)>0)
				{
					while($rowartpc = mysql_fetch_assoc($sql_artp))
					{
						$ans_artpc = $rowartpc;
						
						if($sub_type_search!=0 && $meta_type_search==0)
						{
							$sql_page_typea = mysql_query("SELECT MAX( id ) FROM artist_project_profiles WHERE type_id ='".$page_type_search."' AND subtype_id='".$sub_type_search."' AND artist_project_id='".$ans_artpc['id']."'");
						}
						elseif($sub_type_search!=0 && $meta_type_search!=0)
						{
							$sql_page_typea = mysql_query("SELECT MAX( id ) FROM artist_project_profiles WHERE type_id ='".$page_type_search."' AND subtype_id='".$sub_type_search."' AND metatype_id='".$meta_type_search."' AND artist_project_id='".$ans_artpc['id']."'");
						}
						else
						{
							$sql_page_typea = mysql_query("SELECT MAX( id ) FROM artist_project_profiles WHERE type_id ='".$page_type_search."' AND artist_project_id='".$ans_artpc['id']."'");
						}
						$run_page_typea = mysql_fetch_assoc($sql_page_typea);
						if($run_page_typea['MAX( id )']!=NULL)
						{
							//echo "hii";
							$sql_stringap = mysql_query("SELECT * FROM artist_project WHERE id='".$ans_artpc['id']."'");
							if(mysql_num_rows($sql_stringap)>0)
							{
								while($row_artpcps = mysql_fetch_assoc($sql_stringap))
								{
									$row_artpcps['table_name'] = 'artist_project';
									$ans_artpcps[] = $row_artpcps;
								}
							}
						}
					}
				}
				
				if(mysql_num_rows($sql_comp)>0)
				{
					while($rowcompc = mysql_fetch_assoc($sql_comp))
					{
						$ans_compc = $rowcompc;
						
						if($sub_type_search!=0 && $meta_type_search==0)
						{
							$sql_page_typea = mysql_query("SELECT MAX( id ) FROM community_project_profiles WHERE type_id ='".$page_type_search."' AND subtype_id='".$sub_type_search."' AND community_project_id='".$ans_compc['id']."'");
						}
						elseif($sub_type_search!=0 && $meta_type_search!=0)
						{
							$sql_page_typea = mysql_query("SELECT MAX( id ) FROM community_project_profiles WHERE type_id ='".$page_type_search."' AND subtype_id='".$sub_type_search."' AND metatype_id='".$meta_type_search."' AND community_project_id='".$ans_compc['id']."'");
						}
						else
						{
							$sql_page_typea = mysql_query("SELECT MAX( id ) FROM community_project_profiles WHERE type_id ='".$page_type_search."' AND community_project_id='".$ans_compc['id']."'");
						}
						$run_page_typea = mysql_fetch_assoc($sql_page_typea);
						if($run_page_typea['MAX( id )']!=NULL)
						{
							//echo "hii";
							$sql_stringcp = mysql_query("SELECT * FROM community_project WHERE id='".$ans_compc['id']."'");
							if(mysql_num_rows($sql_stringcp)>0)
							{
								while($row_compcps = mysql_fetch_assoc($sql_stringcp))
								{
									$row_compcps['table_name'] = 'community_project';
									$ans_compcps[] = $row_compcps;
								}
							}
						}	
					}
				}
				
				if(mysql_num_rows($sql_arte)>0)
				{
					while($rowartec = mysql_fetch_assoc($sql_arte))
					{
						$ans_artea = $rowartec;
						
						if($sub_type_search!=0 && $meta_type_search==0)
						{
							$sql_page_typea = mysql_query("SELECT MAX( id ) FROM artist_event_profiles WHERE type_id ='".$page_type_search."' AND subtype_id='".$sub_type_search."' AND artist_event_id='".$ans_artea['id']."'");
						}
						elseif($sub_type_search!=0 && $meta_type_search!=0)
						{
							$sql_page_typea = mysql_query("SELECT MAX( id ) FROM artist_event_profiles WHERE type_id ='".$page_type_search."' AND subtype_id='".$sub_type_search."' AND metatype_id='".$meta_type_search."' AND artist_event_id='".$ans_artea['id']."'");
						}
						else
						{
							$sql_page_typea = mysql_query("SELECT MAX( id ) FROM artist_event_profiles WHERE type_id ='".$page_type_search."' AND artist_event_id='".$ans_artea['id']."'");
						}
						$run_page_typea = mysql_fetch_assoc($sql_page_typea);
						if($run_page_typea['MAX( id )']!=NULL)
						{
							//echo "hii";
							$sql_stringae = mysql_query("SELECT * FROM artist_event WHERE id='".$ans_artea['id']."'");
							if(mysql_num_rows($sql_stringae)>0)
							{
								while($row_artecps = mysql_fetch_assoc($sql_stringae))
								{
									$row_artecps['table_name'] = 'artist_event';
									$ans_artecps[] = $row_artecps;
								}
							}
						}
					}
				}
				
				if(mysql_num_rows($sql_come)>0)
				{
					while($rowcomec = mysql_fetch_assoc($sql_come))
					{
						$ans_comec = $rowcomec;
						
						if($sub_type_search!=0 && $meta_type_search==0)
						{
							$sql_page_typea = mysql_query("SELECT MAX( id ) FROM community_event_profiles WHERE type_id ='".$page_type_search."' AND subtype_id='".$sub_type_search."' AND community_event_id='".$ans_comec['id']."'");
						}
						elseif($sub_type_search!=0 && $meta_type_search!=0)
						{
							$sql_page_typea = mysql_query("SELECT MAX( id ) FROM community_event_profiles WHERE type_id ='".$page_type_search."' AND subtype_id='".$sub_type_search."' AND metatype_id='".$meta_type_search."' AND community_event_id='".$ans_comec['id']."'");
						}
						else
						{
							$sql_page_typea = mysql_query("SELECT MAX( id ) FROM community_event_profiles WHERE type_id ='".$page_type_search."' AND community_event_id='".$ans_comec['id']."'");
						}
						$run_page_typea = mysql_fetch_assoc($sql_page_typea);
						if($run_page_typea['MAX( id )']!=NULL)
						{
							//echo "hii";
							$sql_stringce = mysql_query("SELECT * FROM community_event WHERE id='".$ans_comec['id']."'");
							if(mysql_num_rows($sql_stringce)>0)
							{
								while($row_comecps = mysql_fetch_assoc($sql_stringce))
								{
									$row_comecps['table_name'] = 'community_event';
									$ans_comecps[] = $row_comecps;
								}
							}
						}
					}
				} 
				
				$main_ansc = array_merge($ans_artcps,$ans_comcps,$ans_artpcps,$ans_compcps,$ans_artecps,$ans_comecps);
				return $main_ansc;
			}
			
			/////////////////////////////////////Only city search///////////////////////////////////////
			if($profile_type_search=="select" && $page_type_search==0 && $sub_type_search==0 && $meta_type_search==0 && $country_search==0 && $state_search==0 && $city_search!="" && $match_string=="Search" && $match_table=="" && $match_id=="" && $alpha=="")
			{
				// echo "f2";
				 //$re = "f2";
				 //return $re;
				$ans_art_ci = array(); 
				$ans_com_ci = array();
				$ans_arte_ci = array();
				$ans_come_ci = array();
				$ans_artp_ci = array();
				$ans_comp_ci = array();
				$ans_med_ci = array();
				
				$ans_reg_video_art_ci = array();
				$ans_reg_audio_art_ci = array();
				$ans_reg_gallery_art_ci = array();
				
				$ans_reg_video_com_ci = array();
				$ans_reg_audio_com_ci = array();
				$ans_reg_gallery_com_ci = array();
				
				$sql_art_ci = mysql_query("SELECT * FROM general_artist WHERE city='".$city_search."' AND status=0 AND active=0 AND image_name!='' AND profile_url!=''");
				$sql_com_ci = mysql_query("SELECT * FROM general_community WHERE city='".$city_search."' AND status=0 AND active=0 AND image_name!='' AND profile_url!=''");
				$sql_arte_ci = mysql_query("SELECT * FROM artist_event WHERE city='".$city_search."' AND image_name!='' AND profile_url!='' AND del_status=0 AND active=0");
				$sql_come_ci = mysql_query("SELECT * FROM community_event WHERE city='".$city_search."' AND image_name!='' AND profile_url!='' AND del_status=0 AND active=0");
				$sql_artp_ci = mysql_query("SELECT * FROM artist_project WHERE city='".$city_search."' AND del_status=0 AND active=0 AND image_name!='' AND profile_url!=''");
				$sql_comp_ci = mysql_query("SELECT * FROM community_project WHERE city='".$city_search."' AND del_status=0 AND active=0 AND image_name!='' AND profile_url!=''");
				
				$sql_reg_video_art_ci = mysql_query("SELECT * FROM general_artist_video");
				$sql_reg_audio_art_ci = mysql_query("SELECT * FROM general_artist_audio");
				$sql_reg_gallery_art_ci = mysql_query("SELECT * FROM general_artist_gallery_list");
					
				$sql_reg_video_com_ci = mysql_query("SELECT * FROM general_community_video");
				$sql_reg_audio_com_ci = mysql_query("SELECT * FROM general_community_audio");
				$sql_reg_gallery_com_ci = mysql_query("SELECT * FROM general_community_gallery_list");
				
				/* if(mysql_num_rows($sql_reg_video_art_ci)>0)
				{
					while($row_reg_video_art = mysql_fetch_assoc($sql_reg_video_art_ci))
					{
						$sql_art_vi_cou = mysql_query("SELECT * FROM general_artist WHERE city='".$city_search."' AND status=0 AND active=0 AND artist_id='".$row_reg_video_art['profile_id']."'");
						if(mysql_num_rows($sql_art_vi_cou)>0)
						{
							$row_reg_video_art["table_name"] = 'general_artist_video';
							$ans_reg_video_art_ci[] = $row_reg_video_art;
						}
					}
				}
				
				if(mysql_num_rows($sql_reg_audio_art_ci)>0)
				{
					while($row_reg_audio_art_cou = mysql_fetch_assoc($sql_reg_audio_art_ci))
					{
						$sql_art_au_cou = mysql_query("SELECT * FROM general_artist WHERE city='".$city_search."' AND status=0 AND active=0 AND artist_id='".$row_reg_audio_art_cou['profile_id']."'");
						if(mysql_num_rows($sql_art_au_cou)>0)
						{
							$row_reg_audio_art_cou["table_name"] = 'general_artist_audio';
							$ans_reg_audio_art_ci[] = $row_reg_audio_art_cou;
						}
					}
				}
				
				if(mysql_num_rows($sql_reg_gallery_art_ci)>0)
				{
					while($row_reg_gallery_art_cou = mysql_fetch_assoc($sql_reg_gallery_art_ci))
					{
						$sql_get_pro_gal = mysql_query("SELECT * FROM general_artist_gallery WHERE gallery_id='".$row_reg_gallery_art_cou['gallery_id']."'");
						$ans_get_pro_gal = mysql_fetch_assoc($sql_get_pro_gal);
						
						$sql_art_ga_cou = mysql_query("SELECT * FROM general_artist WHERE city='".$city_search."' AND status=0 AND active=0 AND artist_id='".$ans_get_pro_gal['profile_id']."'");
						if(mysql_num_rows($sql_art_ga_cou)>0)
						{
							$row_reg_gallery_art_cou["table_name"] = 'general_artist_gallery_list';
							$ans_reg_gallery_art_ci[] = $row_reg_gallery_art_cou;
						}
					}
				}
				
				if(mysql_num_rows($sql_reg_video_com_ci)>0)
				{
					while($row_reg_video_com_cou = mysql_fetch_assoc($sql_reg_video_com_ci))
					{
						$sql_com_vi_cou = mysql_query("SELECT * FROM general_community WHERE city='".$city_search."' AND status=0 AND active=0 AND community_id='".$row_reg_video_com_cou['profile_id']."'");
						if(mysql_num_rows($sql_com_vi_cou)>0)
						{
							$row_reg_video_com_cou["table_name"] = 'general_community_video';
							$ans_reg_video_com_ci[] = $row_reg_video_com_cou;
						}
					}
				}
				
				if(mysql_num_rows($sql_reg_audio_com_ci)>0)
				{
					while($row_reg_audio_com_cou = mysql_fetch_assoc($sql_reg_audio_com_ci))
					{
						$sql_com_au_cou = mysql_query("SELECT * FROM general_community WHERE city='".$city_search."' AND status=0 AND active=0 AND community_id='".$row_reg_audio_com_cou['profile_id']."'");
						if(mysql_num_rows($sql_com_au_cou)>0)
						{
							$row_reg_audio_com_cou["table_name"] = 'general_community_audio';
							$ans_reg_audio_com_ci[] = $row_reg_audio_com_cou;
						}
					}
				}
				
				if(mysql_num_rows($sql_reg_gallery_com_ci)>0)
				{
					while($row_reg_gallery_com_cou = mysql_fetch_assoc($sql_reg_gallery_com_ci))
					{
						$sql_get_pro_gal = mysql_query("SELECT * FROM general_artist_gallery WHERE gallery_id='".$row_reg_gallery_com_cou['gallery_id']."'");
						$ans_get_pro_gal = mysql_fetch_assoc($sql_get_pro_gal);
						
						$sql_com_ga_cou = mysql_query("SELECT * FROM general_community WHERE city='".$city_search."' AND status=0 AND active=0 AND community_id='".$ans_get_pro_gal['profile_id']."'");
						if(mysql_num_rows($sql_com_ga_cou)>0)
						{
							$row_reg_gallery_com_cou["table_name"] = 'general_community_gallery_list';
							$ans_reg_gallery_com_ci[] = $row_reg_gallery_com_cou;
						}
					}
				} */
				
				$sql_med_ci = mysql_query("SELECT * FROM general_media WHERE delete_status=0 AND media_status=0");
				if(mysql_num_rows($sql_med_ci)>0)
				{
					while($row_mede = mysql_fetch_assoc($sql_med_ci))
					{
						if($row_mede['from_info']!="")
						{
							$exp_from = explode('|',$row_mede['from_info']);
							if($exp_from[0]=='general_artist')
							{
								$sql_get_tables = mysql_query("SELECT * FROM $exp_from[0] WHERE artist_id='".$exp_from[1]."' AND city='".$city_search."' AND status=0 AND active=0");
							}
							elseif($exp_from[0]=='general_community')
							{
								$sql_get_tables = mysql_query("SELECT * FROM $exp_from[0] WHERE community_id='".$exp_from[1]."' AND city='".$city_search."' AND status=0 AND active=0");
							}
							elseif($exp_from[0]=='community_event' || $exp_from[0]=='artist_event' || $exp_from[0]=='artist_project' || $exp_from[0]=='community_project')
							{
								$sql_get_tables = mysql_query("SELECT * FROM $exp_from[0] WHERE id='".$exp_from[1]."' AND city='".$city_search."'");
							}
							
							if(isset($sql_get_tables) && !empty($sql_get_tables))
							{
								if(mysql_num_rows($sql_get_tables)>0)
								{
									$sql_get_med = mysql_query("SELECT * FROM general_media WHERE id='".$row_mede['id']."' AND delete_status=0 AND media_status=0");
									$ans_med_ci[] = mysql_fetch_assoc($sql_get_med);
								}
							}
						}
						elseif($row_mede['creator_info']!="")
						{
							$exp_from = explode('|',$row_mede['creator_info']);
							if($exp_from[0]=='general_artist')
							{
								$sql_get_tables = mysql_query("SELECT * FROM $exp_from[0] WHERE artist_id='".$exp_from[1]."' city='".$city_search."' AND status=0 AND active=0");
							}
							elseif($exp_from[0]=='general_community')
							{
								$sql_get_tables = mysql_query("SELECT * FROM $exp_from[0] WHERE community_id='".$exp_from[1]."' AND city='".$city_search."' AND status=0 AND active=0");
							}
							elseif($exp_from[0]=='community_event' || $exp_from[0]=='artist_event' || $exp_from[0]=='artist_project' || $exp_from[0]=='community_project')
							{
								$sql_get_tables = mysql_query("SELECT * FROM $exp_from[0] WHERE id='".$exp_from[1]."' AND city='".$city_search."'");
							}
							
							if(isset($sql_get_tables) && !empty($sql_get_tables))
							{
								if(isset($sql_get_tables) && mysql_num_rows($sql_get_tables)>0)
								{
									$sql_get_med = mysql_query("SELECT * FROM general_media WHERE id='".$row_mede['id']."' AND delete_status=0 AND media_status=0");
									$ans_med_ci[] = mysql_fetch_assoc($sql_get_med);
								}
							}
						}
					}
				}
				
				if(mysql_num_rows($sql_art_ci)>0)
				{
					while($row_art_ci = mysql_fetch_assoc($sql_art_ci))
					{
						$ans_art_ci[] = $row_art_ci;
					}
				}
				
				if(mysql_num_rows($sql_com_ci)>0)
				{
					while($row_com_ci = mysql_fetch_assoc($sql_com_ci))
					{
						$ans_com_ci[] = $row_com_ci;
					}
				}
				
				if(mysql_num_rows($sql_arte_ci)>0)
				{
					while($row_arte_ci = mysql_fetch_assoc($sql_arte_ci))
					{
						$row_arte_ci['table_name'] = 'artist_event';
						$ans_arte_ci[] = $row_arte_ci;
					}
				}
				
				if(mysql_num_rows($sql_come_ci)>0)
				{
					while($row_come_ci = mysql_fetch_assoc($sql_come_ci))
					{
						$row_come_ci['table_name'] = 'community_event';
						$ans_come_ci[] = $row_come_ci;
					}
				}
				
				if(mysql_num_rows($sql_artp_ci)>0)
				{
					while($row_artp_ci = mysql_fetch_assoc($sql_artp_ci))
					{
						$row_artp_ci['table_name'] = 'artist_project';
						$ans_artp_ci[] = $row_artp_ci;
					}
				}
				
				if(mysql_num_rows($sql_comp_ci)>0)
				{
					while($row_comp_ci = mysql_fetch_assoc($sql_comp_ci))
					{
						$row_comp_ci['table_name'] = 'community_project';
						$ans_comp_ci[] = $row_comp_ci;
					}
				}
				
				if(mysql_num_rows($sql_med_ci)>0)
				{
					while($row_med_ci = mysql_fetch_assoc($sql_med_ci))
					{
						$ans_med_ci[] = $row_med_ci;
					}
				}
				
				$main_city = array_merge($ans_art_ci,$ans_com_ci,$ans_arte_ci,$ans_come_ci,$ans_artp_ci,$ans_comp_ci,$ans_med_ci,$ans_reg_video_art_ci,$ans_reg_audio_art_ci,$ans_reg_gallery_art_ci,$ans_reg_video_com_ci,$ans_reg_audio_com_ci,$ans_reg_gallery_com_ci);
				return $main_city;
			}
			
			////////////////////////////////////Page type Search with city////////////////////////////////////
			if($profile_type_search=="select" && $page_type_search!=0 && $city_search!="" && $country_search==0 && $match_string=="Search" && $match_table=="" && $match_id=="" && $state_search==0 && $alpha=="")
			{
				// echo "f1";
				// $re = "f1";
				// return $re;
				$ans_artcps = array();
				$ans_comcps = array();
				$ans_artpcps = array();
				$ans_compcps = array();
				$ans_artecps = array();
				$ans_comecps = array();
				
				$sql_art = mysql_query("SELECT * FROM general_artist WHERE city='".$city_search."' AND status=0 AND active=0 AND image_name!='' AND profile_url!=''");
				$sql_com = mysql_query("SELECT * FROM general_community WHERE city='".$city_search."' AND status=0 AND active=0 AND image_name!='' AND profile_url!=''");
				$sql_artp = mysql_query("SELECT * FROM artist_project WHERE city='".$city_search."' AND del_status=0 AND active=0 AND image_name!='' AND profile_url!=''");
				$sql_comp = mysql_query("SELECT * FROM community_project WHERE city='".$city_search."' AND del_status=0 AND active=0 AND image_name!='' AND profile_url!=''");
				$sql_arte = mysql_query("SELECT * FROM artist_event WHERE city='".$city_search."' AND image_name!='' AND profile_url!='' AND del_status=0 AND active=0");
				$sql_come = mysql_query("SELECT * FROM community_event WHERE city='".$city_search."' AND image_name!='' AND profile_url!='' AND del_status=0 AND active=0");
				
				
				if(mysql_num_rows($sql_art)>0)
				{
					while($row_artc = mysql_fetch_assoc($sql_art))
					{
						$ans_artc = $row_artc;
						//var_dump($ans_artc);
						
						if($sub_type_search!=0 && $meta_type_search==0)
						{
							$sql_page_typea = mysql_query("SELECT MAX( general_artist_profiles_id ) FROM general_artist_profiles WHERE type_id ='".$page_type_search."' AND subtype_id='".$sub_type_search."' AND artist_id='".$ans_artc['artist_id']."'");
						}
						elseif($sub_type_search!=0 && $meta_type_search!=0)
						{
							$sql_page_typea = mysql_query("SELECT MAX( general_artist_profiles_id ) FROM general_artist_profiles WHERE type_id ='".$page_type_search."' AND subtype_id='".$sub_type_search."' AND metatype_id='".$meta_type_search."' AND artist_id='".$ans_artc['artist_id']."'");
						}
						else
						{
							$sql_page_typea = mysql_query("SELECT MAX( general_artist_profiles_id ) FROM general_artist_profiles WHERE type_id ='".$page_type_search."' AND artist_id='".$ans_artc['artist_id']."'");
						}
						$run_page_typea = mysql_fetch_assoc($sql_page_typea);
						if($run_page_typea['MAX( general_artist_profiles_id )']!=NULL)
						{
							//echo "hii";
							$sql_string = mysql_query("SELECT * FROM general_artist WHERE artist_id='".$ans_artc['artist_id']."'");
							if(mysql_num_rows($sql_string)>0)
							{
								while($row_artcps = mysql_fetch_assoc($sql_string))
								{
									$ans_artcps[] = $row_artcps;
								}
							}
						}	
					}
				}
				
				if(mysql_num_rows($sql_com)>0)
				{
					while($row_comc = mysql_fetch_assoc($sql_com))
					{
						$ans_comc = $row_comc;
						
						if($sub_type_search!=0 && $meta_type_search==0)
						{
							$sql_page_typea = mysql_query("SELECT MAX( general_community_profiles_id ) FROM general_community_profiles WHERE type_id ='".$page_type_search."' AND subtype_id='".$sub_type_search."' AND community_id='".$ans_comc['community_id']."'");
						}
						elseif($sub_type_search!=0 && $meta_type_search!=0)
						{
							$sql_page_typea = mysql_query("SELECT MAX( general_community_profiles_id ) FROM general_community_profiles WHERE type_id ='".$page_type_search."' AND subtype_id='".$sub_type_search."' AND metatype_id='".$meta_type_search."' AND community_id='".$ans_comc['community_id']."'");
						}
						else
						{
							$sql_page_typea = mysql_query("SELECT MAX( general_community_profiles_id ) FROM general_community_profiles WHERE type_id ='".$page_type_search."' AND community_id='".$ans_comc['community_id']."'");
						}
						$run_page_typea = mysql_fetch_assoc($sql_page_typea);
						if($run_page_typea['MAX( general_community_profiles_id )']!=NULL)
						{
							//echo "hii";
							$sql_stringc = mysql_query("SELECT * FROM general_community WHERE community_id='".$ans_comc['community_id']."'");
							if(mysql_num_rows($sql_stringc)>0)
							{
								while($row_comcps = mysql_fetch_assoc($sql_stringc))
								{
									$ans_comcps[] = $row_comcps;
								}
							}
						}
					}
				}
				
				if(mysql_num_rows($sql_artp)>0)
				{
					while($rowartpc = mysql_fetch_assoc($sql_artp))
					{
						$ans_artpc = $rowartpc;
						
						if($sub_type_search!=0 && $meta_type_search==0)
						{
							$sql_page_typea = mysql_query("SELECT MAX( id ) FROM artist_project_profiles WHERE type_id ='".$page_type_search."' AND subtype_id='".$sub_type_search."' AND artist_project_id='".$ans_artpc['id']."'");
						}
						elseif($sub_type_search!=0 && $meta_type_search!=0)
						{
							$sql_page_typea = mysql_query("SELECT MAX( id ) FROM artist_project_profiles WHERE type_id ='".$page_type_search."' AND subtype_id='".$sub_type_search."' AND metatype_id='".$meta_type_search."' AND artist_project_id='".$ans_artpc['id']."'");
						}
						else
						{
							$sql_page_typea = mysql_query("SELECT MAX( id ) FROM artist_project_profiles WHERE type_id ='".$page_type_search."' AND artist_project_id='".$ans_artpc['id']."'");
						}
						$run_page_typea = mysql_fetch_assoc($sql_page_typea);
						if($run_page_typea['MAX( id )']!=NULL)
						{
							//echo "hii";
							$sql_stringap = mysql_query("SELECT * FROM artist_project WHERE id='".$ans_artpc['id']."'");
							if(mysql_num_rows($sql_stringap)>0)
							{
								while($row_artpcps = mysql_fetch_assoc($sql_stringap))
								{
									$row_artpcps['table_name'] = 'artist_project';
									$ans_artpcps[] = $row_artpcps;
								}
							}
						}
					}
				}
				
				if(mysql_num_rows($sql_comp)>0)
				{
					while($rowcompc = mysql_fetch_assoc($sql_comp))
					{
						$ans_compc = $rowcompc;
						
						if($sub_type_search!=0 && $meta_type_search==0)
						{
							$sql_page_typea = mysql_query("SELECT MAX( id ) FROM community_project_profiles WHERE type_id ='".$page_type_search."' AND subtype_id='".$sub_type_search."' AND community_project_id='".$ans_compc['id']."'");
						}
						elseif($sub_type_search!=0 && $meta_type_search!=0)
						{
							$sql_page_typea = mysql_query("SELECT MAX( id ) FROM community_project_profiles WHERE type_id ='".$page_type_search."' AND subtype_id='".$sub_type_search."' AND metatype_id='".$meta_type_search."' AND community_project_id='".$ans_compc['id']."'");
						}
						else
						{
							$sql_page_typea = mysql_query("SELECT MAX( id ) FROM community_project_profiles WHERE type_id ='".$page_type_search."' AND community_project_id='".$ans_compc['id']."'");
						}
						$run_page_typea = mysql_fetch_assoc($sql_page_typea);
						if($run_page_typea['MAX( id )']!=NULL)
						{
							//echo "hii";
							$sql_stringcp = mysql_query("SELECT * FROM community_project WHERE id='".$ans_compc['id']."'");
							if(mysql_num_rows($sql_stringcp)>0)
							{
								while($row_compcps = mysql_fetch_assoc($sql_stringcp))
								{
									$row_compcps['table_name'] = 'community_project';
									$ans_compcps[] = $row_compcps;
								}
							}
						}	
					}
				}
				
				if(mysql_num_rows($sql_arte)>0)
				{
					while($rowartec = mysql_fetch_assoc($sql_arte))
					{
						$ans_artea = $rowartec;
						
						if($sub_type_search!=0 && $meta_type_search==0)
						{
							$sql_page_typea = mysql_query("SELECT MAX( id ) FROM artist_event_profiles WHERE type_id ='".$page_type_search."' AND subtype_id='".$sub_type_search."' AND artist_event_id='".$ans_artea['id']."'");
						}
						elseif($sub_type_search!=0 && $meta_type_search!=0)
						{
							$sql_page_typea = mysql_query("SELECT MAX( id ) FROM artist_event_profiles WHERE type_id ='".$page_type_search."' AND subtype_id='".$sub_type_search."' AND metatype_id='".$meta_type_search."' AND artist_event_id='".$ans_artea['id']."'");
						}
						else
						{
							$sql_page_typea = mysql_query("SELECT MAX( id ) FROM artist_event_profiles WHERE type_id ='".$page_type_search."' AND artist_event_id='".$ans_artea['id']."'");
						}
						$run_page_typea = mysql_fetch_assoc($sql_page_typea);
						if($run_page_typea['MAX( id )']!=NULL)
						{
							//echo "hii";
							$sql_stringae = mysql_query("SELECT * FROM artist_event WHERE id='".$ans_artea['id']."'");
							if(mysql_num_rows($sql_stringae)>0)
							{
								while($row_artecps = mysql_fetch_assoc($sql_stringae))
								{
									$row_artecps['table_name'] = 'artist_event';
									$ans_artecps[] = $row_artecps;
								}
							}
						}
					}
				}
				
				if(mysql_num_rows($sql_come)>0)
				{
					while($rowcomec = mysql_fetch_assoc($sql_come))
					{
						$ans_comec = $rowcomec;
						
						if($sub_type_search!=0 && $meta_type_search==0)
						{
							$sql_page_typea = mysql_query("SELECT MAX( id ) FROM community_event_profiles WHERE type_id ='".$page_type_search."' AND subtype_id='".$sub_type_search."' AND community_event_id='".$ans_comec['id']."'");
						}
						elseif($sub_type_search!=0 && $meta_type_search!=0)
						{
							$sql_page_typea = mysql_query("SELECT MAX( id ) FROM community_event_profiles WHERE type_id ='".$page_type_search."' AND subtype_id='".$sub_type_search."' AND metatype_id='".$meta_type_search."' AND community_event_id='".$ans_comec['id']."'");
						}
						else
						{
							$sql_page_typea = mysql_query("SELECT MAX( id ) FROM community_event_profiles WHERE type_id ='".$page_type_search."' AND community_event_id='".$ans_comec['id']."'");
						}
						$run_page_typea = mysql_fetch_assoc($sql_page_typea);
						if($run_page_typea['MAX( id )']!=NULL)
						{
							//echo "hii";
							$sql_stringce = mysql_query("SELECT * FROM community_event WHERE id='".$ans_comec['id']."'");
							if(mysql_num_rows($sql_stringce)>0)
							{
								while($row_comecps = mysql_fetch_assoc($sql_stringce))
								{
									$row_comecps['table_name'] = 'community_event';
									$ans_comecps[] = $row_comecps;
								}
							}
						}
					}
				} 
				
				$main_ansc = array_merge($ans_artcps,$ans_comcps,$ans_artpcps,$ans_compcps,$ans_artecps,$ans_comecps);
				return $main_ansc;
			}
			
			//var_dump("hii");
			////////////////////////////////Only Alpha Search ////////////////////////////////
			if(($profile_type_search=="select" || $profile_type_search==NULL) && ($page_type_search==0 || $page_type_search==NULL) && ($sub_type_search==0 || $sub_type_search==NULL) && ($meta_type_search==0 || $meta_type_search==NULL) && ($country_search==0 || $country_search==NULL) && ($state_search==0 || $state_search==NULL) && ($city_search=="" || $city_search==NULL) && $match_string=="Search" && $match_table=="" && $match_id=="" && $alpha!="")
			{
				//var_dump("hii");
				$sql_art = "SELECT * FROM general_artist WHERE name LIKE '%$alpha%' AND status=0 AND active=0";
				$sql_comm = "SELECT * FROM general_community WHERE name LIKE '%$alpha%' AND status=0 AND active=0";
				$sql_arte = "SELECT * FROM artist_event WHERE title LIKE '%$alpha%'";
				$sql_artp = "SELECT * FROM artist_project WHERE title LIKE '%$alpha%' AND del_status=0 AND active=0";
				$sql_come = "SELECT * FROM community_event WHERE title LIKE '%$alpha%'";
				$sql_comp = "SELECT * FROM community_project WHERE title LIKE '%$alpha%' AND del_status=0 AND active=0";
				$sql_med = "SELECT * FROM general_media WHERE title LIKE '%$alpha%' AND delete_status=0 AND media_status=0";
				
				$sql_reg_video_art = "SELECT * FROM general_artist_video WHERE video_name LIKE '%$alpha%'";
				$sql_reg_audio_art = "SELECT * FROM general_artist_audio WHERE audio_name LIKE '%$alpha%'";
				$sql_reg_gallery_art = "SELECT * FROM general_artist_gallery_list WHERE gallery_title LIKE '%$alpha%'";
						
				$sql_reg_video_com = "SELECT * FROM general_community_video WHERE video_name LIKE '%$alpha%'";
				$sql_reg_audio_com = "SELECT * FROM general_community_audio WHERE audio_name LIKE '%$alpha%'";
				$sql_reg_gallery_com = "SELECT * FROM general_community_gallery_list WHERE gallery_title LIKE '%$alpha%'";
				
				$run_art = mysql_query($sql_art); 
				$run_comm = mysql_query($sql_comm);
				$run_arte = mysql_query($sql_arte);
				$run_artp = mysql_query($sql_artp);
				$run_come = mysql_query($sql_come);
				$run_comp = mysql_query($sql_comp);
				$run_med = mysql_query($sql_med);
				
				$run_reg_video_art = mysql_query($sql_reg_video_art);
				$run_reg_audio_art = mysql_query($sql_reg_audio_art);
				$run_reg_gallery_art = mysql_query($sql_reg_gallery_art);
				$run_reg_video_com = mysql_query($sql_reg_video_com);
				$run_reg_audio_com = mysql_query($sql_reg_audio_com);
				$run_reg_gallery_com = mysql_query($sql_reg_gallery_com);
				
				$ans_art = array();
				$ans_comm = array();
				$ans_arte = array();
				$ans_artp = array();
				$ans_come = array();
				$ans_comp = array();
				$ans_med = array();
				
				$ans_reg_video_art = array();
				$ans_reg_audio_art = array();
				$ans_reg_gallery_art = array();
				$ans_reg_video_com = array();
				$ans_reg_audio_com = array();
				$ans_reg_gallery_com = array();
			
				if(mysql_num_rows($run_art)>0)
				{
					while($row_art = mysql_fetch_assoc($run_art))
					{
						$row_art["table_name"] = 'general_artist';
						$ans_art[] = $row_art;
						
					}
				}
				
				if(mysql_num_rows($run_comm)>0)
				{
					while($row_comm = mysql_fetch_assoc($run_comm))
					{
						$row_comm["table_name"] = 'general_community';
						$ans_comm[] = $row_comm;
					}
				}
				
				if(mysql_num_rows($run_arte)>0)
				{
					while($row_arte = mysql_fetch_assoc($run_arte))
					{
						$row_arte["table_name"] = 'artist_event';
						$ans_arte[] = $row_arte;
					}
				}
				
				if(mysql_num_rows($run_artp)>0)
				{
					while($row_artp = mysql_fetch_assoc($run_artp))
					{
						$row_artp["table_name"] = 'artist_project';
						$ans_artp[] = $row_artp;
					}
				}
				
				if(mysql_num_rows($run_come)>0)
				{
					while($row_come = mysql_fetch_assoc($run_come))
					{
						$row_come["table_name"] = 'community_event';
						$ans_come[] = $row_come;
					}
				}
				
				if(mysql_num_rows($run_comp)>0)
				{
					while($row_comp = mysql_fetch_assoc($run_comp))
					{
						$row_comp["table_name"] = 'community_project';
						$ans_comp[] = $row_comp;
					}
				}
				
				if(mysql_num_rows($run_med)>0)
				{
					while($row_med = mysql_fetch_assoc($run_med))
					{
						$row_med["table_name"] = 'general_media';
						$ans_med[] = $row_med;
					}
				}
				
				/* if(mysql_num_rows($run_reg_video_art)>0)
				{
					while($v_arow_med = mysql_fetch_assoc($run_reg_video_art))
					{
						$v_arow_med["table_name"] = 'general_artist_video';
						$ans_reg_video_art[] = $v_arow_med;
					}
				}
				
				if(mysql_num_rows($run_reg_audio_art)>0)
				{
					while($a_arow_med = mysql_fetch_assoc($run_reg_audio_art))
					{
						$a_arow_med["table_name"] = 'general_artist_audio';
						$ans_reg_audio_art[] = $a_arow_med;
					}
				}
				
				if(mysql_num_rows($run_reg_gallery_art)>0)
				{
					while($g_arow_med = mysql_fetch_assoc($run_reg_gallery_art))
					{
						$g_arow_med["table_name"] = 'general_artist_gallery_list';
						$ans_reg_gallery_art[] = $g_arow_med;
					}
				}
				
				if(mysql_num_rows($run_reg_video_com)>0)
				{
					while($v_crow_med = mysql_fetch_assoc($run_reg_video_com))
					{
						$v_crow_med["table_name"] = 'general_community_video';
						$ans_reg_video_com[] = $v_crow_med;
					}
				}
				
				if(mysql_num_rows($run_reg_audio_com)>0)
				{
					while($a_crow_med = mysql_fetch_assoc($run_reg_audio_com))
					{
						$a_crow_med["table_name"] = 'general_community_audio';
						$ans_reg_audio_com[] = $a_crow_med;
					}
				}
				
				if(mysql_num_rows($run_reg_gallery_com)>0)
				{
					while($g_crow_med = mysql_fetch_assoc($run_reg_gallery_com))
					{
						$g_crow_med["table_name"] = 'general_community_gallery_list';
						$ans_reg_gallery_com[] = $g_crow_med;
					}
				} */
					
					$main_match = array_merge($ans_art,$ans_comm,$ans_arte,$ans_artp,$ans_come,$ans_comp,$ans_med,$ans_reg_video_art,$ans_reg_audio_art,$ans_reg_gallery_art,$ans_reg_video_com,$ans_reg_audio_com,$ans_reg_gallery_com);
					return $main_match;
			}
			
			//////////////////////////////// Media Search ////////////////////////////////////////////////
			// if($profile_type_search!="" && $profile_type_search=='media' && ($page_type_search==0))
			// {
				
			// }


			//////////////////////// FOR TAB NAME :- RECENT AND POPULAR /////////////////////
			if($tab_name!="")
			{
				if($tab_name=='recent' || $tab_name=='popular')
				{
					/******** This was added by azhar for making different recent tabs and popular tab************/
					if($tab_name=='recent')
					{
						$ans_art = array();
						$ans_comm = array();
						$ans_arte = array();
						$ans_artp = array();
						$ans_come = array();
						$ans_comp = array();
						$ans_med = array();
						
						$run = mysql_query("SELECT * FROM date_creation ORDER BY date DESC");
						if(mysql_num_rows($run)>0)
						{
							while($row = mysql_fetch_assoc($run))
							{
								if($row['profile_media_type']=='artist_project')
								{
									$sql_artp = "SELECT * FROM artist_project WHERE id='".$row['profile_media_id']."' AND del_status=0 AND active=0 AND image_name!='' AND profile_url!=''";
									$run_artp = mysql_query($sql_artp);
									if(mysql_num_rows($run_artp)>0)
									{
										while($row_artp = mysql_fetch_assoc($run_artp))
										{
											$row_artp['table_name'] = 'artist_project';
											$ans_artp[] = $row_artp;
										}
									}
								}
								elseif($row['profile_media_type']=='community_project')
								{
									$sql_comp = "SELECT * FROM community_project WHERE id='".$row['profile_media_id']."' AND del_status=0 AND active=0 AND image_name!='' AND profile_url!=''";
									$run_comp = mysql_query($sql_comp);
									if(mysql_num_rows($run_comp)>0)
									{
										while($row_comp = mysql_fetch_assoc($run_comp))
										{
											$row_comp['table_name'] = 'community_project';
											$ans_comp[] = $row_comp;
										}
									}
								}
								elseif($row['profile_media_type']=='artist_event')
								{
									$sql_arte = "SELECT * FROM artist_event WHERE id='".$row['profile_media_id']."' AND del_status=0 AND active=0 AND image_name!='' AND profile_url!=''";
									$run_arte = mysql_query($sql_arte);
									if(mysql_num_rows($run_arte)>0)
									{
										while($row_arte = mysql_fetch_assoc($run_arte))
										{
											$row_arte['table_name'] = 'artist_event';
											$ans_arte[] = $row_arte;
										}
									}
								}
								elseif($row['profile_media_type']=='community_event')
								{
									$sql_come = "SELECT * FROM community_event WHERE id='".$row['profile_media_id']."' AND del_status=0 AND active=0 AND image_name!='' AND profile_url!=''";
									$run_come = mysql_query($sql_come);
									if(mysql_num_rows($run_come)>0)
									{
										while($row_come = mysql_fetch_assoc($run_come))
										{
											$row_come['table_name'] = 'community_event';
											$ans_come[] = $row_come;
										}
									}
								}
								elseif($row['profile_media_type']=='general_artist')
								{
									$sql_art = "SELECT * FROM general_artist WHERE artist_id='".$row['profile_media_id']."' AND status=0 AND active=0 AND del_status=0 AND image_name!='' AND profile_url!=''";
									$run_art = mysql_query($sql_art); 
									if(mysql_num_rows($run_art)>0)
									{
										while($row_art = mysql_fetch_assoc($run_art))
										{
											$row_art['table_name'] = 'general_artist';
											$ans_art[] = $row_art;
										}
									}
								}
								elseif($row['profile_media_type']=='general_community')
								{
									$sql_comm = "SELECT * FROM general_community WHERE community_id='".$row['profile_media_id']."' AND status=0 AND active=0 AND del_status=0 AND image_name!='' AND profile_url!=''";
									$run_comm = mysql_query($sql_comm);
									if(mysql_num_rows($run_comm)>0)
									{
										while($row_comm = mysql_fetch_assoc($run_comm))
										{
											$row_comm['table_name'] = 'general_community';
											$ans_comm[] = $row_comm;
										}
									}
								}
								elseif($row['profile_media_type']=='general_media')
								{
									$sql_med = "SELECT * FROM general_media WHERE id='".$row['profile_media_id']."' AND delete_status=0 AND media_status=0";
									$run_med = mysql_query($sql_med);
									if(mysql_num_rows($run_med)>0)
									{
										while($row_med = mysql_fetch_assoc($run_med))
										{
											$row_med['table_name'] = 'general_media';
											$ans_med[] = $row_med;
										}
									}
								}
							}
						}
						$main_count_match = array_merge($ans_art,$ans_comm,$ans_arte,$ans_artp,$ans_come,$ans_comp,$ans_med);
						return $main_count_match;
					}
					if($tab_name=='popular')
					{
						$sql_art = "SELECT * FROM general_artist WHERE status=0 AND active=0 AND del_status=0 AND image_name!='' AND profile_url!=''";
						$sql_comm = "SELECT * FROM general_community WHERE status=0 AND active=0 AND del_status=0 AND image_name!='' AND profile_url!=''";
						$sql_arte = "SELECT * FROM artist_event WHERE del_status=0 AND active=0 AND image_name!='' AND profile_url!=''";
						$sql_artp = "SELECT * FROM artist_project WHERE del_status=0 AND active=0 AND image_name!='' AND profile_url!=''";
						$sql_come = "SELECT * FROM community_event WHERE del_status=0 AND active=0 AND image_name!='' AND profile_url!=''";
						$sql_comp = "SELECT * FROM community_project WHERE del_status=0 AND active=0 AND image_name!='' AND profile_url!=''";
						$sql_med = "SELECT * FROM general_media WHERE delete_status=0 AND media_status=0";
						
						$run_art = mysql_query($sql_art); 
						$run_comm = mysql_query($sql_comm);
						$run_arte = mysql_query($sql_arte);
						$run_artp = mysql_query($sql_artp);
						$run_come = mysql_query($sql_come);
						$run_comp = mysql_query($sql_comp);
						$run_med = mysql_query($sql_med);
						
						$ans_art = array();
						$ans_comm = array();
						$ans_arte = array();
						$ans_artp = array();
						$ans_come = array();
						$ans_comp = array();
						$ans_med = array();
						
						if(mysql_num_rows($run_art)>0)
						{
							while($row_art = mysql_fetch_assoc($run_art))
							{
								$row_art['table_name'] = 'general_artist';
								$ans_art[] = $row_art;
							}
						}
						
						if(mysql_num_rows($run_comm)>0)
						{
							while($row_comm = mysql_fetch_assoc($run_comm))
							{
								$row_comm['table_name'] = 'general_community';
								$ans_comm[] = $row_comm;
							}
						}
						
						if(mysql_num_rows($run_arte)>0)
						{
							while($row_arte = mysql_fetch_assoc($run_arte))
							{
								$row_arte['table_name'] = 'artist_event';
								$ans_arte[] = $row_arte;
							}
						}
						
						if(mysql_num_rows($run_artp)>0)
						{
							while($row_artp = mysql_fetch_assoc($run_artp))
							{
								$row_artp['table_name'] = 'artist_project';
								$ans_artp[] = $row_artp;
							}
						}
						
						if(mysql_num_rows($run_come)>0)
						{
							while($row_come = mysql_fetch_assoc($run_come))
							{
								$row_come['table_name'] = 'community_event';
								$ans_come[] = $row_come;
							}
						}
						
						if(mysql_num_rows($run_comp)>0)
						{
							while($row_comp = mysql_fetch_assoc($run_comp))
							{
								$row_comp['table_name'] = 'community_project';
								$ans_comp[] = $row_comp;
							}
						}
						
						if(mysql_num_rows($run_med)>0)
						{
							while($row_med = mysql_fetch_assoc($run_med))
							{
								$row_med['table_name'] = 'general_media';
								$ans_med[] = $row_med;
							}
						}
						
						$main_count_match = array_merge($ans_art,$ans_comm,$ans_arte,$ans_artp,$ans_come,$ans_comp,$ans_med);
						return $main_count_match;
					}
				}
			}
		}
		
		function get_audio_img($audio_id)
		{
			$user_keys_data = new user_keys();
			$s3 = new S3("","");
			$image_name ="";
			$sql_media_img = mysql_query("SELECT * FROM general_media WHERE id='".$audio_id."' AND delete_status=0 AND media_status=0");
			if(mysql_num_rows($sql_media_img)>0)
			{
				$res_media_img = mysql_fetch_assoc($sql_media_img);
				if($res_media_img['from_info']!="")
				{
					$exp_from_info = explode("|",$res_media_img['from_info']);
					$get_img = mysql_query("select * from ".$exp_from_info[0]." where id=".$exp_from_info[1]."");
					if(mysql_num_rows($get_img)>0)
					{
						$res_img = mysql_fetch_assoc($get_img);
						$image_name = $res_img['image_name'];
						
					}
				}
				if($res_media_img['from_info'] =="" && $res_media_img['creator_info']!="")
				{
					$exp_creator_info = explode("|",$res_media_img['creator_info']);
					if($exp_creator_info[0]=="general_artist"){$id_name ="artist_id";
						$get_general_user_id = $user_keys_data -> get_general_user_idby_artistid($exp_creator_info[1]);
						$image_name = $s3->getNewURI_otheruser("artjcropprofile",$get_general_user_id);
					}
					if($exp_creator_info[0]=="general_community"){$id_name ="community_id";
						$get_general_user_id = $user_keys_data -> get_general_user_idby_communityid($exp_creator_info[1]);
						$image_name = $s3->getNewURI_otheruser("comjcropprofile",$get_general_user_id);
					}
					if($exp_creator_info[0]=="community_project" || $exp_creator_info[0]=="artist_project"){$id_name ="id";
					$image_name ="";
					}
					
					$get_img = mysql_query("select * from ".$exp_creator_info[0]." where ".$id_name."=".$exp_creator_info[1]."");
					if(mysql_num_rows($get_img)>0)
					{
						$res_img = mysql_fetch_assoc($get_img);
						$image_name = $image_name .$res_img['image_name'];
					}
				}
				if($res_media_img['from_info'] =="" && $res_media_img['creator_info'] =="")
				{
					$get_gen_det = mysql_query("select * from general_user where general_user_id='".$res_media_img['general_user_id']."'");
					if(mysql_num_rows($get_gen_det)>0){
						$res_gen_det = mysql_fetch_assoc($get_gen_det);
						if($res_gen_det['artist_id']!="" && $res_gen_det['artist_id']!=0){
							$get_art_info = mysql_query("select * from general_artist where artist_id='".$res_gen_det['artist_id']."'");
							if(mysql_num_rows($get_art_info)>0){
								$res_art_info = mysql_fetch_assoc($get_art_info);
								$get_general_user_id = $user_keys_data -> get_general_user_idby_artistid($res_art_info['artist_id']);
								$image_name = $s3->getNewURI_otheruser("artjcropprofile",$get_general_user_id);
								$image_name = $image_name .$res_art_info['image_name'];
							}
						}
						if($res_gen_det['community_id']!="" && $res_gen_det['community_id']!=0){
							$get_com_info = mysql_query("select * from general_community where community_id='".$res_gen_det['community_id']."'");
							if(mysql_num_rows($get_com_info)>0){
								$res_com_info = mysql_fetch_assoc($get_com_info);
								$get_general_user_id = $user_keys_data -> get_general_user_idby_communityid($res_com_info['community_id']);
								$image_name = $s3->getNewURI_otheruser("comjcropprofile",$get_general_user_id);
								$image_name = $image_name .$res_com_info['image_name'];
							}
						}
					}
				}
			}
			return($image_name);
		}
		
		/*
		Mithesh Code
		function get_audio_img($audio_id)
		{
			$sql_media_img = mysql_query("SELECT * FROM media_songs WHERE media_id='".$audio_id."'");
			if(mysql_num_rows($sql_media_img)>0)
			{
				//var_dump("1");
				$ans_img = mysql_fetch_assoc($sql_media_img);
				if($ans_img['gallery_id']!=0 && $ans_img['gallery_at']=='general_media')
				{
					//var_dump("2");
					$sql_img_main = mysql_query("SELECT * FROM media_images WHERE media_id='".$ans_img['gallery_id']."'");
					if(mysql_num_rows($sql_img_main)>0)
					{
						$ans_img_main = mysql_fetch_assoc($sql_img_main);
						if($ans_img_main['cover_pic']!="")
						{
							//var_dump("3");
							$ans_img_main['table_name'] = 'general_media';
							return $ans_img_main;
						}
						else
						{
							$sql_min_img = mysql_query("SELECT MIN(id) FROM media_images WHERE media_id='".$ans_img['gallery_id']."'");
							$ans_min_img = mysql_fetch_assoc($sql_min_img);
							$sql_min_main = mysql_query("SELECT * FROM media_images WHERE id='".$ans_min_img['MIN(id)']."'");
							$ans_min_main = mysql_fetch_assoc($sql_min_main);
							$ans_min_main['table_name'] = 'general_media';
							return $ans_min_main;
						}
					}
					else
					{
						return 1;
					}
				}
				elseif($ans_img['gallery_id']!=0 && $ans_img['gallery_at']=='general_artist')
				{
					$sql_img_main = mysql_query("SELECT * FROM general_artist_gallery WHERE gallery_id='".$ans_img['gallery_id']."'");
					if(mysql_num_rows($sql_img_main)>0)
					{
						$ans_img_main = mysql_fetch_assoc($sql_img_main);
						if($ans_img_main['cover_pic']!="")
						{
							//var_dump("3");
							$ans_img_main['table_name'] = 'general_artist';
							return $ans_img_main;
						}
						else
						{
							$sql_min_img = mysql_query("SELECT MIN(img_id) FROM general_artist_gallery WHERE gallery_id='".$ans_img['gallery_id']."'");
							$ans_min_img = mysql_fetch_assoc($sql_min_img);
							$sql_min_main = mysql_query("SELECT * FROM general_artist_gallery WHERE img_id='".$ans_min_img['MIN(img_id)']."'");
							$ans_min_main = mysql_fetch_assoc($sql_min_main);
							$ans_min_main['table_name'] = 'general_artist';
							return $ans_min_main;
						}
					}
					else
					{
						return 1;
					}
				}
				elseif($ans_img['gallery_id']!=0 && $ans_img['gallery_at']=='general_community')
				{
					$sql_img_main = mysql_query("SELECT * FROM general_community_gallery WHERE gallery_id='".$ans_img['gallery_id']."'");
					if(mysql_num_rows($sql_img_main)>0)
					{
						$ans_img_main = mysql_fetch_assoc($sql_img_main);
						if($ans_img_main['cover_pic']!="")
						{
							//var_dump("3");
							$ans_img_main['table_name'] = 'general_community';
							return $ans_img_main;
						}
						else
						{
							$sql_min_img = mysql_query("SELECT MIN(img_id) FROM general_community_gallery WHERE gallery_id='".$ans_img['gallery_id']."'");
							$ans_min_img = mysql_fetch_assoc($sql_min_img);
							$sql_min_main = mysql_query("SELECT * FROM general_community_gallery WHERE img_id='".$ans_min_img['MIN(img_id)']."'");
							$ans_min_main = mysql_fetch_assoc($sql_min_main);
							$ans_min_main['table_name'] = 'general_community';
							return $ans_min_main;
						}
					}
					else
					{
						return 1;
					}
				}
				else
				{
					
					return 1;
				}
			}
			else
			{
				
				return 0;
			}
		}*/
		
		function get_gal_img($get_med)
		{
			$sql_get_img = mysql_query("SELECT * FROM media_images WHERE media_id='".$get_med."'");
			if(mysql_num_rows($sql_get_img)>0)
			{
				$ans_get_img1 = mysql_fetch_assoc($sql_get_img);
				if($ans_get_img1['profile_image']!="")
				{
					return $ans_get_img1['profile_image'];
				}
			}
			/*$sql_get_img = mysql_query("SELECT MIN(id) FROM media_images WHERE media_id='".$get_med."'");
			if(mysql_num_rows($sql_get_img)>0)
			{
				$ans_get_img1 = mysql_fetch_assoc($sql_get_img);
				$sql_imgs = mysql_query("SELECT * FROM media_images WHERE id='".$ans_get_img1['MIN(id)']."'");
				if(mysql_num_rows($sql_imgs)>0)
				{
					$ans_get_img = mysql_fetch_assoc($sql_imgs);
					if($ans_get_img['cover_pic']!="")
					{
						return $ans_get_img['cover_pic'];
					}
					else
					{
						return $ans_get_img['image_name'];
					}
				}
			}*/
			
		}
		
		function get_channel_img($get_med)
		{
			$sql_get_img = mysql_query("SELECT * FROM media_channel WHERE media_id='".$get_med."'");
			if(mysql_num_rows($sql_get_img)>0)
			{
				$ans_get_img1 = mysql_fetch_assoc($sql_get_img);
				if($ans_get_img1['profile_image']!="")
				{
					return $ans_get_img1['profile_image'];
				}
			}
		}
		
		function get_gal_img_reg($tables,$get_med)
		{
			if($tables=='general_artist_gallery_list')
			{
				$sql_get_img = mysql_query("SELECT MIN(img_id) FROM general_artist_gallery WHERE gallery_id='".$get_med."'");
				if(mysql_num_rows($sql_get_img)>0)
				{
					$ans_get_img1 = mysql_fetch_assoc($sql_get_img); 
					$sql_imgs = mysql_query("SELECT * FROM general_artist_gallery WHERE img_id='".$ans_get_img1['MIN(img_id)']."'");
					if(mysql_num_rows($sql_imgs)>0)
					{
						$ans_get_img = mysql_fetch_assoc($sql_imgs);
						if($ans_get_img['cover_pic']!="")
						{
							return $ans_get_img['cover_pic'];
						}
						else
						{
							return $ans_get_img['image_name'];
						}
					}
				}
			}
			elseif($tables=='general_community_gallery_list')
			{
				$sql_get_img = mysql_query("SELECT MIN(img_id) FROM general_community_gallery WHERE gallery_id='".$get_med."'");
				if(mysql_num_rows($sql_get_img)>0)
				{
					$ans_get_img1 = mysql_fetch_assoc($sql_get_img); 
					$sql_imgs = mysql_query("SELECT * FROM general_community_gallery WHERE img_id='".$ans_get_img1['MIN(img_id)']."'");
					if(mysql_num_rows($sql_imgs)>0)
					{
						$ans_get_img = mysql_fetch_assoc($sql_imgs);
						if($ans_get_img['cover_pic']!="")
						{
							return $ans_get_img['cover_pic'];
						}
						else
						{
							return $ans_get_img['image_name'];
						}
					}
				}
			}
		}
		
		function get_audio_video_reg_img($table_id,$table_name)
		{
			$exp_name_table = explode('_',$table_name);
			if($exp_name_table[1]=='artist')
			{
				$sql_get_art_img = mysql_query("SELECT * FROM general_artist WHERE artist_id='".$table_id."'");
				if(mysql_num_rows($sql_get_art_img)>0)
				{
					$ans_get_art_img = mysql_fetch_assoc($sql_get_art_img);
					if($ans_get_art_img['image_name']!="")
					{
						$image_names = $ans_get_art_img['image_name'];
						$exp_image_names = explode('_',$image_names);
						if($exp_image_names[0]=='thumbnail')
						{
							return $image_names;
						}
						else
						{
							return 0;
						}
					}
					else
					{
						return 0;
					}
				}
				else
				{
					return 0;
				}
			}
			elseif($exp_name_table[1]=='community')
			{
				$sql_get_com_img = mysql_query("SELECT * FROM general_community WHERE community_id='".$table_id."'");
				if(mysql_num_rows($sql_get_com_img)>0)
				{
					$ans_get_com_img = mysql_fetch_assoc($sql_get_com_img);
					if($ans_get_com_img['image_name']!="")
					{
						$image_names = $ans_get_com_img['image_name'];
						$exp_image_names = explode('_',$image_names);
						if($exp_image_names[0]=='thumbnail')
						{
							return $image_names;
						}
						else
						{
							return 0;
						}
					}
					else
					{
						return 0;
					}
				}
				else
				{
					return 0;
				}
			}
		}
		
		function get_gal_reg_img($profile_id,$table_name,$get_med)
		{
			$exp_name_table = explode('_',$table_name);
			if($exp_name_table[1]=='artist')
			{
				$sql_get_img_art = mysql_query("SELECT MIN(gallery_id) FROM general_artist_gallery WHERE gallery_id='".$get_med."'");
				$ans_get_img = mysql_fetch_assoc($sql_get_img_art); 
				if($ans_get_img['cover_pic']!="")
				{
					return $ans_get_img['cover_pic'];
				}
				else
				{
					return $ans_get_img['image_name'];
				}
			}
			elseif($exp_name_table[1]=='community')
			{
				$sql_get_img_com = mysql_query("SELECT MIN(gallery_id) FROM general_community_gallery WHERE gallery_id='".$get_med."'");
				$ans_get_img = mysql_fetch_assoc($sql_get_img_com); 
				if($ans_get_img['cover_pic']!="")
				{
					return $ans_get_img['cover_pic'];
				}
				else
				{
					return $ans_get_img['image_name'];
				}
			}
		}
		
		function get_title($tables,$table_ids)
		{
			if($tables=='type')
			{
				$sql = mysql_query("SELECT * FROM type WHERE type_id='".$table_ids."'");
			}
			
			if($tables=='subtype')
			{
				$sql = mysql_query("SELECT * FROM subtype WHERE subtype_id='".$table_ids."'");
			}
			
			if($tables=='meta_type')
			{
				$sql = mysql_query("SELECT * FROM meta_type WHERE meta_id='".$table_ids."'");
			}
			
			if($tables=='country')
			{
				$sql = mysql_query("SELECT * FROM country WHERE country_id='".$table_ids."'");
			}
			
			if($tables=='state')
			{
				$sql = mysql_query("SELECT * FROM state WHERE state_id='".$table_ids."'");
			}
			
			return $sql;
		}
		
		function get_media_images($media_id)
		{
			$query = mysql_query("select * from media_images where media_id ='".$media_id."'");
			if(mysql_num_rows($query)>0)
			{
				while($res = mysql_fetch_assoc($query))
				{
					$new_array[] = $res;
				}
				return($new_array);
			}
		}
		function get_registration_gallery($table_name,$id)
		{
			if($table_name == "general_artist_gallery_list"){
				$query = mysql_query("select * from general_artist_gallery where gallery_id='".$id."'");
				if(mysql_num_rows($query)>0)
				{
					while($res = mysql_fetch_assoc($query)){
						$new_art_gallery[] = $res;
					}
					return($new_art_gallery);
				}
			}
			if($table_name == "general_community_gallery_list"){
				$query = mysql_query("select * from general_community_gallery where gallery_id='".$id."'");
				if(mysql_num_rows($query)>0)
				{
					while($res = mysql_fetch_assoc($query)){
						$new_com_gallery[] = $res;
					}
					return($new_com_gallery);
				}
			}
			
		}
		/******function for playing featured media starts******/
		function get_register_featured_media_gallery($media_id,$table_name)
		{
			if($table_name == "general_artist_gallery_list"){ $tbl_name = "general_artist_gallery";}
			if($table_name == "general_community_gallery_list"){ $tbl_name = "general_community_gallery";}
			$query = mysql_query("select * from ".$tbl_name." where gallery_id='".$media_id."'");
			if(mysql_num_rows($query)>0)
			{
				while($res = mysql_fetch_assoc($query))
				{
					$new_array[] = $res;
				}
				return($new_array);
			}
		}
		function get_featured_gallery_images($media_id)
		{
			$query = mysql_query("select * from media_images where media_id='".$media_id."'");
			if(mysql_num_rows($query)>0)
			{
				while($res = mysql_fetch_assoc($query))
				{
					$new_array[] = $res;
				}
				return($new_array);
			}
		}
		/******function for playing featured media ends******/
		
		/***********Type display in search field in header*********/
		function type_display($table,$type_id)
		{
			if($table=='community_event')
			{
				$sql = mysql_query("SELECT MIN(id) FROM  community_event_profiles WHERE community_event_id='".$type_id."'  AND type_id!=0");
				if(mysql_num_rows($sql)>0)
				{
					$ans = mysql_fetch_assoc($sql);
					
					$sql6 = mysql_query("select * from community_event_profiles where id='".$ans['MIN(id)']."'");
					$res6 = mysql_fetch_assoc($sql6);
					
					$sql_type = mysql_query("SELECT * FROM type WHERE type_id='".$res6['type_id']."'");
					$ans_type = mysql_fetch_assoc($sql_type);
					
					return $ans_type;
				}
			}
			
			if($table=='artist_event')
			{
				$sql = mysql_query("SELECT MIN(id) FROM  artist_event_profiles WHERE artist_event_id='".$type_id."'  AND type_id!=0");
				if(mysql_num_rows($sql)>0)
				{
					$ans = mysql_fetch_assoc($sql);
					
					$sql6 = mysql_query("select * from artist_event_profiles where id='".$ans['MIN(id)']."'");
					$res6 = mysql_fetch_assoc($sql6);
					
					$sql_type = mysql_query("SELECT * FROM type WHERE type_id='".$res6['type_id']."'");
					$ans_type = mysql_fetch_assoc($sql_type);
					
					return $ans_type;
				}
			}
			
			if($table=='artist_project')
			{
				$sql = mysql_query("SELECT MIN(id) FROM  artist_project_profiles WHERE artist_project_id='".$type_id."'  AND type_id!=0");
				if(mysql_num_rows($sql)>0)
				{
					$ans = mysql_fetch_assoc($sql);
					
					$sql6 = mysql_query("select * from artist_project_profiles where id='".$ans['MIN(id)']."'");
					$res6 = mysql_fetch_assoc($sql6);
					
					$sql_type = mysql_query("SELECT * FROM type WHERE type_id='".$res6['type_id']."'");
					$ans_type = mysql_fetch_assoc($sql_type);
					
					return $ans_type;
				}
			}
			
			if($table=='community_project')
			{
				$sql = mysql_query("SELECT MIN(id) FROM  community_project_profiles WHERE community_project_id='".$type_id."'  AND type_id!=0");
				if(mysql_num_rows($sql)>0)
				{
					$ans = mysql_fetch_assoc($sql);
					
					$sql6 = mysql_query("select * from community_project_profiles where id='".$ans['MIN(id)']."'");
					$res6 = mysql_fetch_assoc($sql6);
					
					$sql_type = mysql_query("SELECT * FROM type WHERE type_id='".$res6['type_id']."'");
					$ans_type = mysql_fetch_assoc($sql_type);
					
					return $ans_type;
				}
			}
		}
		
		function getvideoimage($tables,$media_id)
		{
			/* if($tables=='general_artist_video' || $tables=='general_community_video')
			{
				$sql = mysql_query("select * from $tables where media_id ='".$media_id."'");
			} */
			$videoId = array();
			if($tables=='media_video')
			{
				$sql = mysql_query("select * from media_video where media_id ='".$media_id."'");
			}
			if(mysql_num_rows($sql)>0)
			{
				$res = mysql_fetch_assoc($sql);
				preg_match("#(?<=v=)[a-zA-Z0-9-]+(?=&)|(?<=v\/)[^&\n]+(?=\?)|(?<=v=)[^&\n]+|(?<=youtu.be/)[^&\n]+#", $res['videolink'], $matches);
				
				if($matches!=NULL)
				{
					$videoId[] = $matches[0];
					$videoId[] = 'youtube';
				}
				else
				{
					//preg_match("#(?<=v=)[a-zA-Z0-9-]+(?=&)|(?<=v\/)[^&\n]+(?=\?)|(?<=v=)[^&\n]+|(?<=vimeo.com/)[^&\n]+#", $res['videolink'], $matches);
					$result = preg_match('/(\d+)/', $res['videolink'], $matches);
					$videoId[] = $matches[0];
					$videoId[] = 'vimeo';
				}	
			}
			
			return($videoId);
		}
		function get_all_media_selected_channel($media_id)
		{
			$get_chan_media = mysql_query("select * from media_channel where media_id='".$media_id."'");
			if(mysql_num_rows($get_chan_media)>0)
			{
				$res_chan_media = mysql_fetch_assoc($get_chan_media);
				return($res_chan_media);
			}
		}
		/*function get_audio_img($audio_id)
		{
			$image_name ="";
			$sql_media_img = mysql_query("SELECT * FROM general_media WHERE id='".$audio_id."' AND delete_status=0 AND media_status=0");
			if(mysql_num_rows($sql_media_img)>0)
			{
				$res_media_img = mysql_fetch_assoc($sql_media_img);
				if($res_media_img['from_info']!="")
				{
					$exp_from_info = explode("|",$res_media_img['from_info']);
					$get_img = mysql_query("select * from ".$exp_from_info[0]." where id=".$exp_from_info[1]."");
					if(mysql_num_rows($get_img)>0)
					{
						$res_img = mysql_fetch_assoc($get_img);
						$image_name = $res_img['image_name'];
						
					}
				}
				if($res_media_img['from_info'] =="" && $res_media_img['creator_info']!="")
				{
					$exp_creator_info = explode("|",$res_media_img['creator_info']);
					if($exp_creator_info[0]=="general_artist"){$id_name ="artist_id";
					$image_name ="http://artjcropprofile.s3.amazonaws.com/";
					}
					if($exp_creator_info[0]=="general_community"){$id_name ="community_id";
					$image_name ="http://comjcropprofile.s3.amazonaws.com/";
					}
					if($exp_creator_info[0]=="community_project" || $exp_creator_info[0]=="artist_project"){$id_name ="id";
					$image_name ="";
					}
					
					$get_img = mysql_query("select * from ".$exp_creator_info[0]." where ".$id_name."=".$exp_creator_info[1]."");
					if(mysql_num_rows($get_img)>0)
					{
						$res_img = mysql_fetch_assoc($get_img);
						$image_name = $image_name .$res_img['image_name'];
					}
				}
				if($res_media_img['from_info'] =="" && $res_media_img['creator_info'] =="")
				{
					$get_gen_det = mysql_query("select * from general_user where general_user_id='".$res_media_img['general_user_id']."'");
					if(mysql_num_rows($get_gen_det)>0){
						$res_gen_det = mysql_fetch_assoc($get_gen_det);
						if($res_gen_det['artist_id']!="" && $res_gen_det['artist_id']!=0){
							$get_art_info = mysql_query("select * from general_artist where artist_id='".$res_gen_det['artist_id']."'");
							if(mysql_num_rows($get_art_info)>0){
								$image_name ="http://artjcropprofile.s3.amazonaws.com/";
								$res_art_info = mysql_fetch_assoc($get_art_info);
								$image_name = $image_name .$res_art_info['image_name'];
							}
						}
						if($res_gen_det['community_id']!="" && $res_gen_det['community_id']!=0){
							$get_com_info = mysql_query("select * from general_community where community_id='".$res_gen_det['community_id']."'");
							if(mysql_num_rows($get_com_info)>0){
								$image_name ="http://comjcropprofile.s3.amazonaws.com/";
								$res_com_info = mysql_fetch_assoc($get_com_info);
								$image_name = $image_name .$res_com_info['image_name'];
							}
						}
					}
				}
			}
			return($image_name);
		}*/
		
		function get_fetatured_media_info($id)
		{
			$sql = mysql_query("select * from general_media where id='".$id."'");
			if(mysql_num_rows($sql)>0){
				$res = mysql_fetch_assoc($sql);
				return($res);
			}
		}
		
		function get_gallery_info($id){
			$query = mysql_query("select * from media_images where media_id='".$id."'");
			if(mysql_num_rows($query)>0){
			$res = mysql_fetch_assoc($query);
			}
			return($res);
		}
		
		function types_of_search($type,$id)
		{
			if($type=='artist')
			{
				$ans_type_f = "";
				$get_profile_type = "";
				
				$sql_type_f = mysql_query("SELECT MIN(general_artist_profiles_id) FROM general_artist_profiles WHERE artist_id='".$id."' AND subtype_id!=0");
				if($sql_type_f!="")
				{
					if(mysql_num_rows($sql_type_f)>0)
					{
						$ans_type_f = mysql_fetch_assoc($sql_type_f);
					}
					if($ans_type_f['MIN(general_artist_profiles_id)']=="")
					{
						$sql_type_f = mysql_query("SELECT MIN(general_artist_profiles_id) FROM general_artist_profiles WHERE artist_id='".$id."'");
						if($sql_type_f!="")
						{
							if(mysql_num_rows($sql_type_f)>0)
							{
								$ans_type_f = mysql_fetch_assoc($sql_type_f);
							}
						}
					}
				}
				else
				{
					$sql_type_f = mysql_query("SELECT MIN(general_artist_profiles_id) FROM general_artist_profiles WHERE artist_id='".$id."'");
					if($sql_type_f!="")
					{
						if(mysql_num_rows($sql_type_f)>0)
						{
							$ans_type_f = mysql_fetch_assoc($sql_type_f);
						}
					}
				}
				
				if($ans_type_f!="")
				{
					$get_profile_type = mysql_query("SELECT ty.name as type_name,sty.name as subtype_name FROM `general_artist_profiles` gap LEFT JOIN type ty on gap.type_id=ty.type_id LEFT JOIN subtype sty ON gap.subtype_id = sty.subtype_id where artist_id='".$id."' AND general_artist_profiles_id='".$ans_type_f['MIN(general_artist_profiles_id)']."'");
				}
				
				return ($get_profile_type);
			}
			elseif($type=='community')
			{
				$ans_type_f = "";
				$get_profile_type = "";
				
				$sql_type_f = mysql_query("SELECT MIN(general_community_profiles_id) FROM general_community_profiles WHERE community_id='".$id."' AND subtype_id!=0");
				if($sql_type_f!="")
				{
					if(mysql_num_rows($sql_type_f)>0)
					{
						$ans_type_f = mysql_fetch_assoc($sql_type_f);
					}
					if($ans_type_f['MIN(general_community_profiles_id)']=="")
					{
						$sql_type_f = mysql_query("SELECT MIN(general_community_profiles_id) FROM general_community_profiles WHERE community_id='".$id."'");
						if($sql_type_f!="")
						{
							if(mysql_num_rows($sql_type_f)>0)
							{
								$ans_type_f = mysql_fetch_assoc($sql_type_f);
							}
						}
					}
				}
				else
				{
					$sql_type_f = mysql_query("SELECT MIN(general_community_profiles_id) FROM general_community_profiles WHERE community_id='".$id."'");
					if($sql_type_f!="")
					{
						if(mysql_num_rows($sql_type_f)>0)
						{
							$ans_type_f = mysql_fetch_assoc($sql_type_f);
						}
					}
				}
				
				if($ans_type_f!="")
				{
					$get_profile_type = mysql_query("SELECT ty.name as type_name,sty.name as subtype_name FROM `general_community_profiles` gap LEFT JOIN type ty on gap.type_id=ty.type_id LEFT JOIN subtype sty ON gap.subtype_id = sty.subtype_id where community_id='".$id."' AND general_community_profiles_id='".$ans_type_f['MIN(general_community_profiles_id)']."'");
				}
				
				return ($get_profile_type);
			}
			elseif($type=='artist_project')
			{
				$ans_type_f = "";
				$get_profile_type = "";
				
				$sql_type_f = mysql_query("SELECT MIN(id) FROM artist_project_profiles WHERE artist_project_id='".$id."' AND subtype_id!=0");
				if($sql_type_f!="")
				{
					if(mysql_num_rows($sql_type_f)>0)
					{
						$ans_type_f = mysql_fetch_assoc($sql_type_f);
					}
					if($ans_type_f['MIN(id)']=="")
					{
						$sql_type_f = mysql_query("SELECT MIN(id) FROM artist_project_profiles WHERE artist_project_id='".$id."'");
						if($sql_type_f!="")
						{
							if(mysql_num_rows($sql_type_f)>0)
							{
								$ans_type_f = mysql_fetch_assoc($sql_type_f);
							}
						}
					}
				}
				else
				{
					$sql_type_f = mysql_query("SELECT MIN(id) FROM artist_project_profiles WHERE artist_project_id='".$id."'");
					if($sql_type_f!="")
					{
						if(mysql_num_rows($sql_type_f)>0)
						{
							$ans_type_f = mysql_fetch_assoc($sql_type_f);
						}
					}
				}
				
				$get_profile_type = mysql_query("SELECT ty.name as type_name,sty.name as subtype_name FROM `artist_project_profiles` gap LEFT JOIN type ty on gap.type_id=ty.type_id LEFT JOIN subtype sty ON gap.subtype_id = sty.subtype_id where artist_project_id='".$id."' AND id='".$ans_type_f['MIN(id)']."'");
				
				return ($get_profile_type);
			}
			elseif($type=='community_project')
			{
				$ans_type_f = "";
				$get_profile_type = "";
				
				$sql_type_f = mysql_query("SELECT MIN(id) FROM community_project_profiles WHERE community_project_id='".$id."' AND subtype_id!=0");
				if($sql_type_f!="")
				{
					if(mysql_num_rows($sql_type_f)>0)
					{
						$ans_type_f = mysql_fetch_assoc($sql_type_f);
					}
					if($ans_type_f['MIN(id)']=="")
					{
						$sql_type_f = mysql_query("SELECT MIN(id) FROM community_project_profiles WHERE community_project_id='".$id."'");
						if($sql_type_f!="")
						{
							if(mysql_num_rows($sql_type_f)>0)
							{
								$ans_type_f = mysql_fetch_assoc($sql_type_f);
							}
						}
					}
				}
				else
				{
					$sql_type_f = mysql_query("SELECT MIN(id) FROM community_project_profiles WHERE community_project_id='".$id."'");
					if($sql_type_f!="")
					{
						if(mysql_num_rows($sql_type_f)>0)
						{
							$ans_type_f = mysql_fetch_assoc($sql_type_f);
						}
					}
				}
				
				$get_profile_type = mysql_query("SELECT ty.name as type_name,sty.name as subtype_name FROM `community_project_profiles` gap LEFT JOIN type ty on gap.type_id=ty.type_id LEFT JOIN subtype sty ON gap.subtype_id = sty.subtype_id where community_project_id='".$id."' AND id='".$ans_type_f['MIN(id)']."'");
				
				return ($get_profile_type);
			}
			elseif($type=='artist_event')
			{
				$ans_type_f = "";
				$get_profile_type = "";
				
				$sql_type_f = mysql_query("SELECT MIN(id) FROM artist_event_profiles WHERE artist_event_id='".$id."' AND subtype_id!=0");
				if($sql_type_f!="")
				{
					if(mysql_num_rows($sql_type_f)>0)
					{
						$ans_type_f = mysql_fetch_assoc($sql_type_f);
					}
					if($ans_type_f['MIN(id)']=="")
					{
						$sql_type_f = mysql_query("SELECT MIN(id) FROM artist_event_profiles WHERE artist_event_id='".$id."'");
						if($sql_type_f!="")
						{
							if(mysql_num_rows($sql_type_f)>0)
							{
								$ans_type_f = mysql_fetch_assoc($sql_type_f);
							}
						}
					}
				}
				else
				{
					$sql_type_f = mysql_query("SELECT MIN(id) FROM artist_event_profiles WHERE artist_event_id='".$id."'");
					if($sql_type_f!="")
					{
						if(mysql_num_rows($sql_type_f)>0)
						{
							$ans_type_f = mysql_fetch_assoc($sql_type_f);
						}
					}
				}
				
				$get_profile_type = mysql_query("SELECT *,MAX(id),ty.name as type_name,sty.name as subtype_name FROM `artist_event_profiles` gap LEFT JOIN type ty on gap.type_id=ty.type_id LEFT JOIN subtype sty ON gap.subtype_id = sty.subtype_id where artist_event_id='".$id."' AND id='".$ans_type_f['MIN(id)']."'");
				
				return ($get_profile_type);
			}
			elseif($type=='community_event')
			{
				$ans_type_f = "";
				$get_profile_type = "";
				
				$sql_type_f = mysql_query("SELECT MIN(id) FROM community_event_profiles WHERE community_event_id='".$id."' AND subtype_id!=0");
				if($sql_type_f!="")
				{
					if(mysql_num_rows($sql_type_f)>0)
					{
						$ans_type_f = mysql_fetch_assoc($sql_type_f);
					}
					if($ans_type_f['MIN(id)']=="")
					{
						$sql_type_f = mysql_query("SELECT MIN(id) FROM community_event_profiles WHERE community_event_id='".$id."'");
						if($sql_type_f!="")
						{
							if(mysql_num_rows($sql_type_f)>0)
							{
								$ans_type_f = mysql_fetch_assoc($sql_type_f);
							}
						}
					}
				}
				else
				{
					$sql_type_f = mysql_query("SELECT MIN(id) FROM community_event_profiles WHERE community_event_id='".$id."'");
					if($sql_type_f!="")
					{
						if(mysql_num_rows($sql_type_f)>0)
						{
							$ans_type_f = mysql_fetch_assoc($sql_type_f);
						}
					}
				}
				
				$get_profile_type = mysql_query("SELECT *,MAX(id),ty.name as type_name,sty.name as subtype_name FROM `community_event_profiles` gap LEFT JOIN type ty on gap.type_id=ty.type_id LEFT JOIN subtype sty ON gap.subtype_id = sty.subtype_id where community_event_id='".$id."' AND id='".$ans_type_f['MIN(id)']."'");
				
				return ($get_profile_type);
			}
			elseif($type=='media')
			{
				$ans_type_f = "";
				$get_profile_type = "";
				
				$sql_type_f = mysql_query("SELECT MIN(id) FROM media_profiles_type WHERE media_id='".$id."' AND subtype_id!=0 AND metatype_id!=0");
				if($sql_type_f!="")
				{
					if(mysql_num_rows($sql_type_f)>0)
					{
						$ans_type_f = mysql_fetch_assoc($sql_type_f);
					}
					if($ans_type_f['MIN(id)']=="")
					{
						$sql_type_f = mysql_query("SELECT MIN(id) FROM media_profiles_type WHERE media_id='".$id."' AND subtype_id!=0");
						if($sql_type_f!="")
						{
							if(mysql_num_rows($sql_type_f)>0)
							{
								$ans_type_f = mysql_fetch_assoc($sql_type_f);
							}
							if($ans_type_f['MIN(id)']=="")
							{
								$sql_type_f = mysql_query("SELECT MIN(id) FROM media_profiles_type WHERE media_id='".$id."'");
								if($sql_type_f!="")
								{
									if(mysql_num_rows($sql_type_f)>0)
									{
										$ans_type_f = mysql_fetch_assoc($sql_type_f);
									}
								}
							}
						}
						else
						{
							$sql_type_f = mysql_query("SELECT MIN(id) FROM media_profiles_type WHERE media_id='".$id."'");
							if($sql_type_f!="")
							{
								if(mysql_num_rows($sql_type_f)>0)
								{
									$ans_type_f = mysql_fetch_assoc($sql_type_f);
								}
							}
						}
					}
				}
				else
				{
					$sql_type_f = mysql_query("SELECT MIN(id) FROM media_profiles_type WHERE media_id='".$id."' AND subtype_id!=0");
					if($sql_type_f!="")
					{
						if(mysql_num_rows($sql_type_f)>0)
						{
							$ans_type_f = mysql_fetch_assoc($sql_type_f);
						}
						if($ans_type_f['MIN(id)']=="")
						{
							$sql_type_f = mysql_query("SELECT MIN(id) FROM media_profiles_type WHERE media_id='".$id."'");
							if($sql_type_f!="")
							{
								if(mysql_num_rows($sql_type_f)>0)
								{
									$ans_type_f = mysql_fetch_assoc($sql_type_f);
								}
							}
						}
					}
					else
					{
						$sql_type_f = mysql_query("SELECT MIN(id) FROM media_profiles_type WHERE media_id='".$id."'");
						if($sql_type_f!="")
						{
							if(mysql_num_rows($sql_type_f)>0)
							{
								$ans_type_f = mysql_fetch_assoc($sql_type_f);
							}
						}
					}
				}
				
				$get_profile_type = mysql_query("SELECT ty.name as type_name,sty.name as subtype_name,mty.name AS metatype_name FROM `media_profiles_type` gap LEFT JOIN type ty on gap.type_id=ty.type_id LEFT JOIN subtype sty ON gap.subtype_id = sty.subtype_id LEFT JOIN meta_type mty ON gap.metatype_id = mty.meta_id where gap.media_id='".$id."' AND id='".$ans_type_f['MIN(id)']."'");
				
				return ($get_profile_type);
			}
		}
		
		function get_recents($id,$type)
		{
			$sql = mysql_query("SELECT * FROM date_creation WHERE profile_media_id='".$id."' AND profile_media_type='".$type."'");
			if($sql!=NULL)
			{
				if(mysql_num_rows($sql)>0)
				{
					$row = mysql_fetch_assoc($sql);
					return $row;
				}
			}
		}
		
		function get_creator_by($id)
		{
			$ans_capt_by = "";
			$creat_name = array();
			
			$sql = mysql_query("SELECT * FROM general_media WHERE id='".$id."'");
			if($sql!="")
			{
				if(mysql_num_rows($sql)>0)
				{
					$ans_by = mysql_fetch_assoc($sql);
					if($ans_by['creator_info']!="")
					{
						$cret_by = explode('|',$ans_by['creator_info']);
						if($cret_by[0]=='general_artist')
						{
							$sql_capt_by = mysql_query("SELECT * FROM general_artist WHERE artist_id='".$cret_by[1]."'");
							$ans_capt_by = mysql_fetch_assoc($sql_capt_by);
							
							$creat_name[] = $ans_capt_by['name'];
							$creat_name[] = $ans_capt_by['profile_url'];
						}
						elseif($cret_by[0]=='general_community')
						{
							$sql_capt_by = mysql_query("SELECT * FROM general_community WHERE community_id='".$cret_by[1]."'");
							$ans_capt_by = mysql_fetch_assoc($sql_capt_by);
							
							$creat_name[] = $ans_capt_by['name'];
							$creat_name[] = $ans_capt_by['profile_url'];
						}
						elseif($cret_by[0]=='artist_project' || $cret_by[0]=='community_project')
						{
							$sql_capt_by = mysql_query("SELECT * FROM $cret_by[0] WHERE id='".$cret_by[1]."'");
							$ans_capt_by = mysql_fetch_assoc($sql_capt_by);
							
							$creat_name[] = $ans_capt_by['title'];
							$creat_name[] = $ans_capt_by['profile_url'];
						}					
					}
				}
			}
			
			if($ans_capt_by!="")
			{
				return $creat_name;
			}
			else
			{
				return $ans_capt_by;
			}
		}
		
		function type_chk_sugg($id)
		{
			$vals = 0;
			$sql_type_chk_art = mysql_query("SELECT * FROM `general_artist_profiles` gap LEFT JOIN `general_artist` ga ON ga.artist_id = gap.artist_id WHERE ga.status = 0 AND ga.del_status=0 AND ga.active = 0 AND gap.type_id = '".$id."'");
			
			$sql_type_chk_com = mysql_query("SELECT * FROM `general_community_profiles` gap LEFT JOIN `general_community` ga ON ga.community_id = gap.community_id WHERE ga.status = 0 AND ga.del_status=0 AND ga.active = 0 AND gap.type_id = '".$id."'");
			
			$sql_type_chk_are = mysql_query("SELECT * FROM `artist_event_profiles` gap LEFT JOIN `artist_event` ga ON ga.id = gap.artist_event_id WHERE ga.del_status = 0 AND ga.active = 0 AND gap.type_id = '".$id."'");
			
			$sql_type_chk_coe = mysql_query("SELECT * FROM `community_event_profiles` gap LEFT JOIN `community_event` ga ON ga.id = gap.community_event_id WHERE ga.del_status = 0 AND ga.active = 0 AND gap.type_id = '".$id."'");
			
			$sql_type_chk_cop = mysql_query("SELECT * FROM `community_project_profiles` gap LEFT JOIN `community_project` ga ON ga.id = gap.community_project_id WHERE ga.del_status = 0 AND ga.active = 0 AND gap.type_id = '".$id."'");
			
			$sql_type_chk_arp = mysql_query("SELECT * FROM `artist_project_profiles` gap LEFT JOIN `artist_project` ga ON ga.id = gap.artist_project_id WHERE ga.del_status = 0 AND ga.active = 0 AND gap.type_id = '".$id."'");
			
			$sql_type_chk_med = mysql_query("SELECT * FROM `media_profiles_type` gap LEFT JOIN `general_media` ga ON ga.id = gap.media_id WHERE ga.media_status = 0 AND ga.delete_status = 0 AND gap.type_id = '".$id."'");
			
			if($sql_type_chk_art!="")
			{
				if(mysql_num_rows($sql_type_chk_art)>0)
				{
					$vals = 1;					
				}
				elseif($sql_type_chk_com!="")
				{
					if(mysql_num_rows($sql_type_chk_com)>0)
					{
						$vals = 1;					
					}
					elseif($sql_type_chk_are!="")
					{
						if(mysql_num_rows($sql_type_chk_are)>0)
						{
							$vals = 1;					
						}
						elseif($sql_type_chk_coe!="")
						{
							if(mysql_num_rows($sql_type_chk_coe)>0)
							{
								$vals = 1;					
							}
							elseif($sql_type_chk_cop!="")
							{
								if(mysql_num_rows($sql_type_chk_cop)>0)
								{
									$vals = 1;					
								}	
								elseif($sql_type_chk_arp!="")
								{
									if(mysql_num_rows($sql_type_chk_arp)>0)
									{
										$vals = 1;					
									}
									elseif($sql_type_chk_med!="")
									{
										if(mysql_num_rows($sql_type_chk_med)>0)
										{
											$vals = 1;					
										}			
									}
								}
								elseif($sql_type_chk_med!="")
								{
									if(mysql_num_rows($sql_type_chk_med)>0)
									{
										$vals = 1;					
									}			
								}
							}
							elseif($sql_type_chk_arp!="")
							{
								if(mysql_num_rows($sql_type_chk_arp)>0)
								{
									$vals = 1;					
								}
								elseif($sql_type_chk_med!="")
								{
									if(mysql_num_rows($sql_type_chk_med)>0)
									{
										$vals = 1;					
									}			
								}
							}
							elseif($sql_type_chk_med!="")
							{
								if(mysql_num_rows($sql_type_chk_med)>0)
								{
									$vals = 1;					
								}			
							}
						}
						elseif($sql_type_chk_cop!="")
						{
							if(mysql_num_rows($sql_type_chk_cop)>0)
							{
								$vals = 1;					
							}	
							elseif($sql_type_chk_arp!="")
							{
								if(mysql_num_rows($sql_type_chk_arp)>0)
								{
									$vals = 1;					
								}
								elseif($sql_type_chk_med!="")
								{
									if(mysql_num_rows($sql_type_chk_med)>0)
									{
										$vals = 1;					
									}			
								}
							}
							elseif($sql_type_chk_med!="")
							{
								if(mysql_num_rows($sql_type_chk_med)>0)
								{
									$vals = 1;					
								}			
							}
						}
						elseif($sql_type_chk_arp!="")
						{
							if(mysql_num_rows($sql_type_chk_arp)>0)
							{
								$vals = 1;					
							}
							elseif($sql_type_chk_med!="")
							{
								if(mysql_num_rows($sql_type_chk_med)>0)
								{
									$vals = 1;					
								}			
							}
						}
						elseif($sql_type_chk_med!="")
						{
							if(mysql_num_rows($sql_type_chk_med)>0)
							{
								$vals = 1;					
							}			
						}
					}
					elseif($sql_type_chk_coe!="")
					{
						if(mysql_num_rows($sql_type_chk_coe)>0)
						{
							$vals = 1;					
						}
						elseif($sql_type_chk_cop!="")
						{
							if(mysql_num_rows($sql_type_chk_cop)>0)
							{
								$vals = 1;					
							}	
							elseif($sql_type_chk_arp!="")
							{
								if(mysql_num_rows($sql_type_chk_arp)>0)
								{
									$vals = 1;					
								}
								elseif($sql_type_chk_med!="")
								{
									if(mysql_num_rows($sql_type_chk_med)>0)
									{
										$vals = 1;					
									}			
								}
							}
							elseif($sql_type_chk_med!="")
							{
								if(mysql_num_rows($sql_type_chk_med)>0)
								{
									$vals = 1;					
								}			
							}
						}
						elseif($sql_type_chk_arp!="")
						{
							if(mysql_num_rows($sql_type_chk_arp)>0)
							{
								$vals = 1;					
							}
							elseif($sql_type_chk_med!="")
							{
								if(mysql_num_rows($sql_type_chk_med)>0)
								{
									$vals = 1;					
								}			
							}
						}
						elseif($sql_type_chk_med!="")
						{
							if(mysql_num_rows($sql_type_chk_med)>0)
							{
								$vals = 1;					
							}			
						}
					}
					elseif($sql_type_chk_cop!="")
					{
						if(mysql_num_rows($sql_type_chk_cop)>0)
						{
							$vals = 1;					
						}	
						elseif($sql_type_chk_arp!="")
						{
							if(mysql_num_rows($sql_type_chk_arp)>0)
							{
								$vals = 1;					
							}
							elseif($sql_type_chk_med!="")
							{
								if(mysql_num_rows($sql_type_chk_med)>0)
								{
									$vals = 1;					
								}			
							}
						}
						elseif($sql_type_chk_med!="")
						{
							if(mysql_num_rows($sql_type_chk_med)>0)
							{
								$vals = 1;					
							}			
						}
					}
					elseif($sql_type_chk_arp!="")
					{
						if(mysql_num_rows($sql_type_chk_arp)>0)
						{
							$vals = 1;					
						}
						elseif($sql_type_chk_med!="")
						{
							if(mysql_num_rows($sql_type_chk_med)>0)
							{
								$vals = 1;					
							}			
						}
					}
					elseif($sql_type_chk_med!="")
					{
						if(mysql_num_rows($sql_type_chk_med)>0)
						{
							$vals = 1;					
						}			
					}
				}
				elseif($sql_type_chk_are!="")
				{
					if(mysql_num_rows($sql_type_chk_are)>0)
					{
						$vals = 1;					
					}
					elseif($sql_type_chk_coe!="")
					{
						if(mysql_num_rows($sql_type_chk_coe)>0)
						{
							$vals = 1;					
						}
						elseif($sql_type_chk_cop!="")
						{
							if(mysql_num_rows($sql_type_chk_cop)>0)
							{
								$vals = 1;					
							}	
							elseif($sql_type_chk_arp!="")
							{
								if(mysql_num_rows($sql_type_chk_arp)>0)
								{
									$vals = 1;					
								}
								elseif($sql_type_chk_med!="")
								{
									if(mysql_num_rows($sql_type_chk_med)>0)
									{
										$vals = 1;					
									}			
								}
							}
							elseif($sql_type_chk_med!="")
							{
								if(mysql_num_rows($sql_type_chk_med)>0)
								{
									$vals = 1;					
								}			
							}
						}
						elseif($sql_type_chk_arp!="")
						{
							if(mysql_num_rows($sql_type_chk_arp)>0)
							{
								$vals = 1;					
							}
							elseif($sql_type_chk_med!="")
							{
								if(mysql_num_rows($sql_type_chk_med)>0)
								{
									$vals = 1;					
								}			
							}
						}
						elseif($sql_type_chk_med!="")
						{
							if(mysql_num_rows($sql_type_chk_med)>0)
							{
								$vals = 1;					
							}			
						}
					}
					elseif($sql_type_chk_cop!="")
					{
						if(mysql_num_rows($sql_type_chk_cop)>0)
						{
							$vals = 1;					
						}	
						elseif($sql_type_chk_arp!="")
						{
							if(mysql_num_rows($sql_type_chk_arp)>0)
							{
								$vals = 1;					
							}
							elseif($sql_type_chk_med!="")
							{
								if(mysql_num_rows($sql_type_chk_med)>0)
								{
									$vals = 1;					
								}			
							}
						}
						elseif($sql_type_chk_med!="")
						{
							if(mysql_num_rows($sql_type_chk_med)>0)
							{
								$vals = 1;					
							}			
						}
					}
					elseif($sql_type_chk_arp!="")
					{
						if(mysql_num_rows($sql_type_chk_arp)>0)
						{
							$vals = 1;					
						}
						elseif($sql_type_chk_med!="")
						{
							if(mysql_num_rows($sql_type_chk_med)>0)
							{
								$vals = 1;					
							}			
						}
					}
					elseif($sql_type_chk_med!="")
					{
						if(mysql_num_rows($sql_type_chk_med)>0)
						{
							$vals = 1;					
						}			
					}
				}
				elseif($sql_type_chk_coe!="")
				{
					if(mysql_num_rows($sql_type_chk_coe)>0)
					{
						$vals = 1;					
					}
					elseif($sql_type_chk_cop!="")
					{
						if(mysql_num_rows($sql_type_chk_cop)>0)
						{
							$vals = 1;					
						}	
						elseif($sql_type_chk_arp!="")
						{
							if(mysql_num_rows($sql_type_chk_arp)>0)
							{
								$vals = 1;					
							}
							elseif($sql_type_chk_med!="")
							{
								if(mysql_num_rows($sql_type_chk_med)>0)
								{
									$vals = 1;					
								}			
							}
						}
						elseif($sql_type_chk_med!="")
						{
							if(mysql_num_rows($sql_type_chk_med)>0)
							{
								$vals = 1;					
							}			
						}
					}
					elseif($sql_type_chk_arp!="")
					{
						if(mysql_num_rows($sql_type_chk_arp)>0)
						{
							$vals = 1;					
						}
						elseif($sql_type_chk_med!="")
						{
							if(mysql_num_rows($sql_type_chk_med)>0)
							{
								$vals = 1;					
							}			
						}
					}
					elseif($sql_type_chk_med!="")
					{
						if(mysql_num_rows($sql_type_chk_med)>0)
						{
							$vals = 1;					
						}			
					}
				}
				elseif($sql_type_chk_cop!="")
				{
					if(mysql_num_rows($sql_type_chk_cop)>0)
					{
						$vals = 1;					
					}	
					elseif($sql_type_chk_arp!="")
					{
						if(mysql_num_rows($sql_type_chk_arp)>0)
						{
							$vals = 1;					
						}
						elseif($sql_type_chk_med!="")
						{
							if(mysql_num_rows($sql_type_chk_med)>0)
							{
								$vals = 1;					
							}			
						}
					}
					elseif($sql_type_chk_med!="")
					{
						if(mysql_num_rows($sql_type_chk_med)>0)
						{
							$vals = 1;					
						}			
					}
				}
				elseif($sql_type_chk_arp!="")
				{
					if(mysql_num_rows($sql_type_chk_arp)>0)
					{
						$vals = 1;					
					}
					elseif($sql_type_chk_med!="")
					{
						if(mysql_num_rows($sql_type_chk_med)>0)
						{
							$vals = 1;					
						}			
					}
				}
				elseif($sql_type_chk_med!="")
				{
					if(mysql_num_rows($sql_type_chk_med)>0)
					{
						$vals = 1;					
					}			
				}
			}
			elseif($sql_type_chk_com!="")
			{
				if(mysql_num_rows($sql_type_chk_com)>0)
				{
					$vals = 1;					
				}
				elseif($sql_type_chk_are!="")
				{
					if(mysql_num_rows($sql_type_chk_are)>0)
					{
						$vals = 1;					
					}
					elseif($sql_type_chk_coe!="")
					{
						if(mysql_num_rows($sql_type_chk_coe)>0)
						{
							$vals = 1;					
						}
						elseif($sql_type_chk_cop!="")
						{
							if(mysql_num_rows($sql_type_chk_cop)>0)
							{
								$vals = 1;					
							}	
							elseif($sql_type_chk_arp!="")
							{
								if(mysql_num_rows($sql_type_chk_arp)>0)
								{
									$vals = 1;					
								}
								elseif($sql_type_chk_med!="")
								{
									if(mysql_num_rows($sql_type_chk_med)>0)
									{
										$vals = 1;					
									}			
								}
							}
							elseif($sql_type_chk_med!="")
							{
								if(mysql_num_rows($sql_type_chk_med)>0)
								{
									$vals = 1;					
								}			
							}
						}
						elseif($sql_type_chk_arp!="")
						{
							if(mysql_num_rows($sql_type_chk_arp)>0)
							{
								$vals = 1;					
							}
							elseif($sql_type_chk_med!="")
							{
								if(mysql_num_rows($sql_type_chk_med)>0)
								{
									$vals = 1;					
								}			
							}
						}
						elseif($sql_type_chk_med!="")
						{
							if(mysql_num_rows($sql_type_chk_med)>0)
							{
								$vals = 1;					
							}			
						}
					}
					elseif($sql_type_chk_cop!="")
					{
						if(mysql_num_rows($sql_type_chk_cop)>0)
						{
							$vals = 1;					
						}	
						elseif($sql_type_chk_arp!="")
						{
							if(mysql_num_rows($sql_type_chk_arp)>0)
							{
								$vals = 1;					
							}
							elseif($sql_type_chk_med!="")
							{
								if(mysql_num_rows($sql_type_chk_med)>0)
								{
									$vals = 1;					
								}			
							}
						}
						elseif($sql_type_chk_med!="")
						{
							if(mysql_num_rows($sql_type_chk_med)>0)
							{
								$vals = 1;					
							}			
						}
					}
					elseif($sql_type_chk_arp!="")
					{
						if(mysql_num_rows($sql_type_chk_arp)>0)
						{
							$vals = 1;					
						}
						elseif($sql_type_chk_med!="")
						{
							if(mysql_num_rows($sql_type_chk_med)>0)
							{
								$vals = 1;					
							}			
						}
					}
					elseif($sql_type_chk_med!="")
					{
						if(mysql_num_rows($sql_type_chk_med)>0)
						{
							$vals = 1;					
						}			
					}
				}
				elseif($sql_type_chk_coe!="")
				{
					if(mysql_num_rows($sql_type_chk_coe)>0)
					{
						$vals = 1;					
					}
					elseif($sql_type_chk_cop!="")
					{
						if(mysql_num_rows($sql_type_chk_cop)>0)
						{
							$vals = 1;					
						}	
						elseif($sql_type_chk_arp!="")
						{
							if(mysql_num_rows($sql_type_chk_arp)>0)
							{
								$vals = 1;					
							}
							elseif($sql_type_chk_med!="")
							{
								if(mysql_num_rows($sql_type_chk_med)>0)
								{
									$vals = 1;					
								}			
							}
						}
						elseif($sql_type_chk_med!="")
						{
							if(mysql_num_rows($sql_type_chk_med)>0)
							{
								$vals = 1;					
							}			
						}
					}
					elseif($sql_type_chk_arp!="")
					{
						if(mysql_num_rows($sql_type_chk_arp)>0)
						{
							$vals = 1;					
						}
						elseif($sql_type_chk_med!="")
						{
							if(mysql_num_rows($sql_type_chk_med)>0)
							{
								$vals = 1;					
							}			
						}
					}
					elseif($sql_type_chk_med!="")
					{
						if(mysql_num_rows($sql_type_chk_med)>0)
						{
							$vals = 1;					
						}			
					}
				}
				elseif($sql_type_chk_cop!="")
				{
					if(mysql_num_rows($sql_type_chk_cop)>0)
					{
						$vals = 1;					
					}	
					elseif($sql_type_chk_arp!="")
					{
						if(mysql_num_rows($sql_type_chk_arp)>0)
						{
							$vals = 1;					
						}
						elseif($sql_type_chk_med!="")
						{
							if(mysql_num_rows($sql_type_chk_med)>0)
							{
								$vals = 1;					
							}			
						}
					}
					elseif($sql_type_chk_med!="")
					{
						if(mysql_num_rows($sql_type_chk_med)>0)
						{
							$vals = 1;					
						}			
					}
				}
				elseif($sql_type_chk_arp!="")
				{
					if(mysql_num_rows($sql_type_chk_arp)>0)
					{
						$vals = 1;					
					}
					elseif($sql_type_chk_med!="")
					{
						if(mysql_num_rows($sql_type_chk_med)>0)
						{
							$vals = 1;					
						}			
					}
				}
				elseif($sql_type_chk_med!="")
				{
					if(mysql_num_rows($sql_type_chk_med)>0)
					{
						$vals = 1;					
					}			
				}
			}
			elseif($sql_type_chk_are!="")
			{
				if(mysql_num_rows($sql_type_chk_are)>0)
				{
					$vals = 1;					
				}
				elseif($sql_type_chk_coe!="")
				{
					if(mysql_num_rows($sql_type_chk_coe)>0)
					{
						$vals = 1;					
					}
					elseif($sql_type_chk_cop!="")
					{
						if(mysql_num_rows($sql_type_chk_cop)>0)
						{
							$vals = 1;					
						}	
						elseif($sql_type_chk_arp!="")
						{
							if(mysql_num_rows($sql_type_chk_arp)>0)
							{
								$vals = 1;					
							}
							elseif($sql_type_chk_med!="")
							{
								if(mysql_num_rows($sql_type_chk_med)>0)
								{
									$vals = 1;					
								}			
							}
						}
						elseif($sql_type_chk_med!="")
						{
							if(mysql_num_rows($sql_type_chk_med)>0)
							{
								$vals = 1;					
							}			
						}
					}
					elseif($sql_type_chk_arp!="")
					{
						if(mysql_num_rows($sql_type_chk_arp)>0)
						{
							$vals = 1;					
						}
						elseif($sql_type_chk_med!="")
						{
							if(mysql_num_rows($sql_type_chk_med)>0)
							{
								$vals = 1;					
							}			
						}
					}
					elseif($sql_type_chk_med!="")
					{
						if(mysql_num_rows($sql_type_chk_med)>0)
						{
							$vals = 1;					
						}			
					}
				}
				elseif($sql_type_chk_cop!="")
				{
					if(mysql_num_rows($sql_type_chk_cop)>0)
					{
						$vals = 1;					
					}	
					elseif($sql_type_chk_arp!="")
					{
						if(mysql_num_rows($sql_type_chk_arp)>0)
						{
							$vals = 1;					
						}
						elseif($sql_type_chk_med!="")
						{
							if(mysql_num_rows($sql_type_chk_med)>0)
							{
								$vals = 1;					
							}			
						}
					}
					elseif($sql_type_chk_med!="")
					{
						if(mysql_num_rows($sql_type_chk_med)>0)
						{
							$vals = 1;					
						}			
					}
				}
				elseif($sql_type_chk_arp!="")
				{
					if(mysql_num_rows($sql_type_chk_arp)>0)
					{
						$vals = 1;					
					}
					elseif($sql_type_chk_med!="")
					{
						if(mysql_num_rows($sql_type_chk_med)>0)
						{
							$vals = 1;					
						}			
					}
				}
				elseif($sql_type_chk_med!="")
				{
					if(mysql_num_rows($sql_type_chk_med)>0)
					{
						$vals = 1;					
					}			
				}
			}
			elseif($sql_type_chk_coe!="")
			{
				if(mysql_num_rows($sql_type_chk_coe)>0)
				{
					$vals = 1;					
				}
				elseif($sql_type_chk_cop!="")
				{
					if(mysql_num_rows($sql_type_chk_cop)>0)
					{
						$vals = 1;					
					}	
					elseif($sql_type_chk_arp!="")
					{
						if(mysql_num_rows($sql_type_chk_arp)>0)
						{
							$vals = 1;					
						}
						elseif($sql_type_chk_med!="")
						{
							if(mysql_num_rows($sql_type_chk_med)>0)
							{
								$vals = 1;					
							}			
						}
					}
					elseif($sql_type_chk_med!="")
					{
						if(mysql_num_rows($sql_type_chk_med)>0)
						{
							$vals = 1;					
						}			
					}
				}
				elseif($sql_type_chk_arp!="")
				{
					if(mysql_num_rows($sql_type_chk_arp)>0)
					{
						$vals = 1;					
					}
					elseif($sql_type_chk_med!="")
					{
						if(mysql_num_rows($sql_type_chk_med)>0)
						{
							$vals = 1;					
						}			
					}
				}
				elseif($sql_type_chk_med!="")
				{
					if(mysql_num_rows($sql_type_chk_med)>0)
					{
						$vals = 1;					
					}			
				}
			}
			elseif($sql_type_chk_cop!="")
			{
				if(mysql_num_rows($sql_type_chk_cop)>0)
				{
					$vals = 1;					
				}	
				elseif($sql_type_chk_arp!="")
				{
					if(mysql_num_rows($sql_type_chk_arp)>0)
					{
						$vals = 1;					
					}
					elseif($sql_type_chk_med!="")
					{
						if(mysql_num_rows($sql_type_chk_med)>0)
						{
							$vals = 1;					
						}			
					}
				}
				elseif($sql_type_chk_med!="")
				{
					if(mysql_num_rows($sql_type_chk_med)>0)
					{
						$vals = 1;					
					}			
				}
			}
			elseif($sql_type_chk_arp!="")
			{
				if(mysql_num_rows($sql_type_chk_arp)>0)
				{
					$vals = 1;					
				}
				elseif($sql_type_chk_med!="")
				{
					if(mysql_num_rows($sql_type_chk_med)>0)
					{
						$vals = 1;					
					}			
				}
			}
			elseif($sql_type_chk_med!="")
			{
				if(mysql_num_rows($sql_type_chk_med)>0)
				{
					$vals = 1;					
				}			
			}
			
			return $vals;
		}
		
		function subtype_chk_sugg($id)
		{
			$vals = 0;
			$sql_type_chk_art = mysql_query("SELECT * FROM `general_artist_profiles` gap LEFT JOIN `general_artist` ga ON ga.artist_id = gap.artist_id WHERE ga.status = 0 AND ga.del_status=0 AND ga.active = 0 AND gap.subtype_id = '".$id."'");
			
			$sql_type_chk_com = mysql_query("SELECT * FROM `general_community_profiles` gap LEFT JOIN `general_community` ga ON ga.community_id = gap.community_id WHERE ga.status = 0 AND ga.del_status=0 AND ga.active = 0 AND gap.subtype_id = '".$id."'");
			
			$sql_type_chk_are = mysql_query("SELECT * FROM `artist_event_profiles` gap LEFT JOIN `artist_event` ga ON ga.id = gap.artist_event_id WHERE ga.del_status = 0 AND ga.active = 0 AND gap.subtype_id = '".$id."'");
			
			$sql_type_chk_coe = mysql_query("SELECT * FROM `community_event_profiles` gap LEFT JOIN `community_event` ga ON ga.id = gap.community_event_id WHERE ga.del_status = 0 AND ga.active = 0 AND gap.subtype_id = '".$id."'");
			
			$sql_type_chk_cop = mysql_query("SELECT * FROM `community_project_profiles` gap LEFT JOIN `community_project` ga ON ga.id = gap.community_project_id WHERE ga.del_status = 0 AND ga.active = 0 AND gap.subtype_id = '".$id."'");
			
			$sql_type_chk_arp = mysql_query("SELECT * FROM `artist_project_profiles` gap LEFT JOIN `artist_project` ga ON ga.id = gap.artist_project_id WHERE ga.del_status = 0 AND ga.active = 0 AND gap.subtype_id = '".$id."'");
			
			$sql_type_chk_med = mysql_query("SELECT * FROM `media_profiles_type` gap LEFT JOIN `general_media` ga ON ga.id = gap.media_id WHERE ga.media_status = 0 AND ga.delete_status = 0 AND gap.subtype_id = '".$id."'");
			
			if($sql_type_chk_art!="")
			{
				if(mysql_num_rows($sql_type_chk_art)>0)
				{
					$vals = 1;					
				}
				elseif($sql_type_chk_com!="")
				{
					if(mysql_num_rows($sql_type_chk_com)>0)
					{
						$vals = 1;					
					}
					elseif($sql_type_chk_are!="")
					{
						if(mysql_num_rows($sql_type_chk_are)>0)
						{
							$vals = 1;					
						}
						elseif($sql_type_chk_coe!="")
						{
							if(mysql_num_rows($sql_type_chk_coe)>0)
							{
								$vals = 1;					
							}
							elseif($sql_type_chk_cop!="")
							{
								if(mysql_num_rows($sql_type_chk_cop)>0)
								{
									$vals = 1;					
								}	
								elseif($sql_type_chk_arp!="")
								{
									if(mysql_num_rows($sql_type_chk_arp)>0)
									{
										$vals = 1;					
									}
									elseif($sql_type_chk_med!="")
									{
										if(mysql_num_rows($sql_type_chk_med)>0)
										{
											$vals = 1;					
										}			
									}
								}
								elseif($sql_type_chk_med!="")
								{
									if(mysql_num_rows($sql_type_chk_med)>0)
									{
										$vals = 1;					
									}			
								}
							}
							elseif($sql_type_chk_arp!="")
							{
								if(mysql_num_rows($sql_type_chk_arp)>0)
								{
									$vals = 1;					
								}
								elseif($sql_type_chk_med!="")
								{
									if(mysql_num_rows($sql_type_chk_med)>0)
									{
										$vals = 1;					
									}			
								}
							}
							elseif($sql_type_chk_med!="")
							{
								if(mysql_num_rows($sql_type_chk_med)>0)
								{
									$vals = 1;					
								}			
							}
						}
						elseif($sql_type_chk_cop!="")
						{
							if(mysql_num_rows($sql_type_chk_cop)>0)
							{
								$vals = 1;					
							}	
							elseif($sql_type_chk_arp!="")
							{
								if(mysql_num_rows($sql_type_chk_arp)>0)
								{
									$vals = 1;					
								}
								elseif($sql_type_chk_med!="")
								{
									if(mysql_num_rows($sql_type_chk_med)>0)
									{
										$vals = 1;					
									}			
								}
							}
							elseif($sql_type_chk_med!="")
							{
								if(mysql_num_rows($sql_type_chk_med)>0)
								{
									$vals = 1;					
								}			
							}
						}
						elseif($sql_type_chk_arp!="")
						{
							if(mysql_num_rows($sql_type_chk_arp)>0)
							{
								$vals = 1;					
							}
							elseif($sql_type_chk_med!="")
							{
								if(mysql_num_rows($sql_type_chk_med)>0)
								{
									$vals = 1;					
								}			
							}
						}
						elseif($sql_type_chk_med!="")
						{
							if(mysql_num_rows($sql_type_chk_med)>0)
							{
								$vals = 1;					
							}			
						}
					}
					elseif($sql_type_chk_coe!="")
					{
						if(mysql_num_rows($sql_type_chk_coe)>0)
						{
							$vals = 1;					
						}
						elseif($sql_type_chk_cop!="")
						{
							if(mysql_num_rows($sql_type_chk_cop)>0)
							{
								$vals = 1;					
							}	
							elseif($sql_type_chk_arp!="")
							{
								if(mysql_num_rows($sql_type_chk_arp)>0)
								{
									$vals = 1;					
								}
								elseif($sql_type_chk_med!="")
								{
									if(mysql_num_rows($sql_type_chk_med)>0)
									{
										$vals = 1;					
									}			
								}
							}
							elseif($sql_type_chk_med!="")
							{
								if(mysql_num_rows($sql_type_chk_med)>0)
								{
									$vals = 1;					
								}			
							}
						}
						elseif($sql_type_chk_arp!="")
						{
							if(mysql_num_rows($sql_type_chk_arp)>0)
							{
								$vals = 1;					
							}
							elseif($sql_type_chk_med!="")
							{
								if(mysql_num_rows($sql_type_chk_med)>0)
								{
									$vals = 1;					
								}			
							}
						}
						elseif($sql_type_chk_med!="")
						{
							if(mysql_num_rows($sql_type_chk_med)>0)
							{
								$vals = 1;					
							}			
						}
					}
					elseif($sql_type_chk_cop!="")
					{
						if(mysql_num_rows($sql_type_chk_cop)>0)
						{
							$vals = 1;					
						}	
						elseif($sql_type_chk_arp!="")
						{
							if(mysql_num_rows($sql_type_chk_arp)>0)
							{
								$vals = 1;					
							}
							elseif($sql_type_chk_med!="")
							{
								if(mysql_num_rows($sql_type_chk_med)>0)
								{
									$vals = 1;					
								}			
							}
						}
						elseif($sql_type_chk_med!="")
						{
							if(mysql_num_rows($sql_type_chk_med)>0)
							{
								$vals = 1;					
							}			
						}
					}
					elseif($sql_type_chk_arp!="")
					{
						if(mysql_num_rows($sql_type_chk_arp)>0)
						{
							$vals = 1;					
						}
						elseif($sql_type_chk_med!="")
						{
							if(mysql_num_rows($sql_type_chk_med)>0)
							{
								$vals = 1;					
							}			
						}
					}
					elseif($sql_type_chk_med!="")
					{
						if(mysql_num_rows($sql_type_chk_med)>0)
						{
							$vals = 1;					
						}			
					}
				}
				elseif($sql_type_chk_are!="")
				{
					if(mysql_num_rows($sql_type_chk_are)>0)
					{
						$vals = 1;					
					}
					elseif($sql_type_chk_coe!="")
					{
						if(mysql_num_rows($sql_type_chk_coe)>0)
						{
							$vals = 1;					
						}
						elseif($sql_type_chk_cop!="")
						{
							if(mysql_num_rows($sql_type_chk_cop)>0)
							{
								$vals = 1;					
							}	
							elseif($sql_type_chk_arp!="")
							{
								if(mysql_num_rows($sql_type_chk_arp)>0)
								{
									$vals = 1;					
								}
								elseif($sql_type_chk_med!="")
								{
									if(mysql_num_rows($sql_type_chk_med)>0)
									{
										$vals = 1;					
									}			
								}
							}
							elseif($sql_type_chk_med!="")
							{
								if(mysql_num_rows($sql_type_chk_med)>0)
								{
									$vals = 1;					
								}			
							}
						}
						elseif($sql_type_chk_arp!="")
						{
							if(mysql_num_rows($sql_type_chk_arp)>0)
							{
								$vals = 1;					
							}
							elseif($sql_type_chk_med!="")
							{
								if(mysql_num_rows($sql_type_chk_med)>0)
								{
									$vals = 1;					
								}			
							}
						}
						elseif($sql_type_chk_med!="")
						{
							if(mysql_num_rows($sql_type_chk_med)>0)
							{
								$vals = 1;					
							}			
						}
					}
					elseif($sql_type_chk_cop!="")
					{
						if(mysql_num_rows($sql_type_chk_cop)>0)
						{
							$vals = 1;					
						}	
						elseif($sql_type_chk_arp!="")
						{
							if(mysql_num_rows($sql_type_chk_arp)>0)
							{
								$vals = 1;					
							}
							elseif($sql_type_chk_med!="")
							{
								if(mysql_num_rows($sql_type_chk_med)>0)
								{
									$vals = 1;					
								}			
							}
						}
						elseif($sql_type_chk_med!="")
						{
							if(mysql_num_rows($sql_type_chk_med)>0)
							{
								$vals = 1;					
							}			
						}
					}
					elseif($sql_type_chk_arp!="")
					{
						if(mysql_num_rows($sql_type_chk_arp)>0)
						{
							$vals = 1;					
						}
						elseif($sql_type_chk_med!="")
						{
							if(mysql_num_rows($sql_type_chk_med)>0)
							{
								$vals = 1;					
							}			
						}
					}
					elseif($sql_type_chk_med!="")
					{
						if(mysql_num_rows($sql_type_chk_med)>0)
						{
							$vals = 1;					
						}			
					}
				}
				elseif($sql_type_chk_coe!="")
				{
					if(mysql_num_rows($sql_type_chk_coe)>0)
					{
						$vals = 1;					
					}
					elseif($sql_type_chk_cop!="")
					{
						if(mysql_num_rows($sql_type_chk_cop)>0)
						{
							$vals = 1;					
						}	
						elseif($sql_type_chk_arp!="")
						{
							if(mysql_num_rows($sql_type_chk_arp)>0)
							{
								$vals = 1;					
							}
							elseif($sql_type_chk_med!="")
							{
								if(mysql_num_rows($sql_type_chk_med)>0)
								{
									$vals = 1;					
								}			
							}
						}
						elseif($sql_type_chk_med!="")
						{
							if(mysql_num_rows($sql_type_chk_med)>0)
							{
								$vals = 1;					
							}			
						}
					}
					elseif($sql_type_chk_arp!="")
					{
						if(mysql_num_rows($sql_type_chk_arp)>0)
						{
							$vals = 1;					
						}
						elseif($sql_type_chk_med!="")
						{
							if(mysql_num_rows($sql_type_chk_med)>0)
							{
								$vals = 1;					
							}			
						}
					}
					elseif($sql_type_chk_med!="")
					{
						if(mysql_num_rows($sql_type_chk_med)>0)
						{
							$vals = 1;					
						}			
					}
				}
				elseif($sql_type_chk_cop!="")
				{
					if(mysql_num_rows($sql_type_chk_cop)>0)
					{
						$vals = 1;					
					}	
					elseif($sql_type_chk_arp!="")
					{
						if(mysql_num_rows($sql_type_chk_arp)>0)
						{
							$vals = 1;					
						}
						elseif($sql_type_chk_med!="")
						{
							if(mysql_num_rows($sql_type_chk_med)>0)
							{
								$vals = 1;					
							}			
						}
					}
					elseif($sql_type_chk_med!="")
					{
						if(mysql_num_rows($sql_type_chk_med)>0)
						{
							$vals = 1;					
						}			
					}
				}
				elseif($sql_type_chk_arp!="")
				{
					if(mysql_num_rows($sql_type_chk_arp)>0)
					{
						$vals = 1;					
					}
					elseif($sql_type_chk_med!="")
					{
						if(mysql_num_rows($sql_type_chk_med)>0)
						{
							$vals = 1;					
						}			
					}
				}
				elseif($sql_type_chk_med!="")
				{
					if(mysql_num_rows($sql_type_chk_med)>0)
					{
						$vals = 1;					
					}			
				}
			}
			elseif($sql_type_chk_com!="")
			{
				if(mysql_num_rows($sql_type_chk_com)>0)
				{
					$vals = 1;					
				}
				elseif($sql_type_chk_are!="")
				{
					if(mysql_num_rows($sql_type_chk_are)>0)
					{
						$vals = 1;					
					}
					elseif($sql_type_chk_coe!="")
					{
						if(mysql_num_rows($sql_type_chk_coe)>0)
						{
							$vals = 1;					
						}
						elseif($sql_type_chk_cop!="")
						{
							if(mysql_num_rows($sql_type_chk_cop)>0)
							{
								$vals = 1;					
							}	
							elseif($sql_type_chk_arp!="")
							{
								if(mysql_num_rows($sql_type_chk_arp)>0)
								{
									$vals = 1;					
								}
								elseif($sql_type_chk_med!="")
								{
									if(mysql_num_rows($sql_type_chk_med)>0)
									{
										$vals = 1;					
									}			
								}
							}
							elseif($sql_type_chk_med!="")
							{
								if(mysql_num_rows($sql_type_chk_med)>0)
								{
									$vals = 1;					
								}			
							}
						}
						elseif($sql_type_chk_arp!="")
						{
							if(mysql_num_rows($sql_type_chk_arp)>0)
							{
								$vals = 1;					
							}
							elseif($sql_type_chk_med!="")
							{
								if(mysql_num_rows($sql_type_chk_med)>0)
								{
									$vals = 1;					
								}			
							}
						}
						elseif($sql_type_chk_med!="")
						{
							if(mysql_num_rows($sql_type_chk_med)>0)
							{
								$vals = 1;					
							}			
						}
					}
					elseif($sql_type_chk_cop!="")
					{
						if(mysql_num_rows($sql_type_chk_cop)>0)
						{
							$vals = 1;					
						}	
						elseif($sql_type_chk_arp!="")
						{
							if(mysql_num_rows($sql_type_chk_arp)>0)
							{
								$vals = 1;					
							}
							elseif($sql_type_chk_med!="")
							{
								if(mysql_num_rows($sql_type_chk_med)>0)
								{
									$vals = 1;					
								}			
							}
						}
						elseif($sql_type_chk_med!="")
						{
							if(mysql_num_rows($sql_type_chk_med)>0)
							{
								$vals = 1;					
							}			
						}
					}
					elseif($sql_type_chk_arp!="")
					{
						if(mysql_num_rows($sql_type_chk_arp)>0)
						{
							$vals = 1;					
						}
						elseif($sql_type_chk_med!="")
						{
							if(mysql_num_rows($sql_type_chk_med)>0)
							{
								$vals = 1;					
							}			
						}
					}
					elseif($sql_type_chk_med!="")
					{
						if(mysql_num_rows($sql_type_chk_med)>0)
						{
							$vals = 1;					
						}			
					}
				}
				elseif($sql_type_chk_coe!="")
				{
					if(mysql_num_rows($sql_type_chk_coe)>0)
					{
						$vals = 1;					
					}
					elseif($sql_type_chk_cop!="")
					{
						if(mysql_num_rows($sql_type_chk_cop)>0)
						{
							$vals = 1;					
						}	
						elseif($sql_type_chk_arp!="")
						{
							if(mysql_num_rows($sql_type_chk_arp)>0)
							{
								$vals = 1;					
							}
							elseif($sql_type_chk_med!="")
							{
								if(mysql_num_rows($sql_type_chk_med)>0)
								{
									$vals = 1;					
								}			
							}
						}
						elseif($sql_type_chk_med!="")
						{
							if(mysql_num_rows($sql_type_chk_med)>0)
							{
								$vals = 1;					
							}			
						}
					}
					elseif($sql_type_chk_arp!="")
					{
						if(mysql_num_rows($sql_type_chk_arp)>0)
						{
							$vals = 1;					
						}
						elseif($sql_type_chk_med!="")
						{
							if(mysql_num_rows($sql_type_chk_med)>0)
							{
								$vals = 1;					
							}			
						}
					}
					elseif($sql_type_chk_med!="")
					{
						if(mysql_num_rows($sql_type_chk_med)>0)
						{
							$vals = 1;					
						}			
					}
				}
				elseif($sql_type_chk_cop!="")
				{
					if(mysql_num_rows($sql_type_chk_cop)>0)
					{
						$vals = 1;					
					}	
					elseif($sql_type_chk_arp!="")
					{
						if(mysql_num_rows($sql_type_chk_arp)>0)
						{
							$vals = 1;					
						}
						elseif($sql_type_chk_med!="")
						{
							if(mysql_num_rows($sql_type_chk_med)>0)
							{
								$vals = 1;					
							}			
						}
					}
					elseif($sql_type_chk_med!="")
					{
						if(mysql_num_rows($sql_type_chk_med)>0)
						{
							$vals = 1;					
						}			
					}
				}
				elseif($sql_type_chk_arp!="")
				{
					if(mysql_num_rows($sql_type_chk_arp)>0)
					{
						$vals = 1;					
					}
					elseif($sql_type_chk_med!="")
					{
						if(mysql_num_rows($sql_type_chk_med)>0)
						{
							$vals = 1;					
						}			
					}
				}
				elseif($sql_type_chk_med!="")
				{
					if(mysql_num_rows($sql_type_chk_med)>0)
					{
						$vals = 1;					
					}			
				}
			}
			elseif($sql_type_chk_are!="")
			{
				if(mysql_num_rows($sql_type_chk_are)>0)
				{
					$vals = 1;					
				}
				elseif($sql_type_chk_coe!="")
				{
					if(mysql_num_rows($sql_type_chk_coe)>0)
					{
						$vals = 1;					
					}
					elseif($sql_type_chk_cop!="")
					{
						if(mysql_num_rows($sql_type_chk_cop)>0)
						{
							$vals = 1;					
						}	
						elseif($sql_type_chk_arp!="")
						{
							if(mysql_num_rows($sql_type_chk_arp)>0)
							{
								$vals = 1;					
							}
							elseif($sql_type_chk_med!="")
							{
								if(mysql_num_rows($sql_type_chk_med)>0)
								{
									$vals = 1;					
								}			
							}
						}
						elseif($sql_type_chk_med!="")
						{
							if(mysql_num_rows($sql_type_chk_med)>0)
							{
								$vals = 1;					
							}			
						}
					}
					elseif($sql_type_chk_arp!="")
					{
						if(mysql_num_rows($sql_type_chk_arp)>0)
						{
							$vals = 1;					
						}
						elseif($sql_type_chk_med!="")
						{
							if(mysql_num_rows($sql_type_chk_med)>0)
							{
								$vals = 1;					
							}			
						}
					}
					elseif($sql_type_chk_med!="")
					{
						if(mysql_num_rows($sql_type_chk_med)>0)
						{
							$vals = 1;					
						}			
					}
				}
				elseif($sql_type_chk_cop!="")
				{
					if(mysql_num_rows($sql_type_chk_cop)>0)
					{
						$vals = 1;					
					}	
					elseif($sql_type_chk_arp!="")
					{
						if(mysql_num_rows($sql_type_chk_arp)>0)
						{
							$vals = 1;					
						}
						elseif($sql_type_chk_med!="")
						{
							if(mysql_num_rows($sql_type_chk_med)>0)
							{
								$vals = 1;					
							}			
						}
					}
					elseif($sql_type_chk_med!="")
					{
						if(mysql_num_rows($sql_type_chk_med)>0)
						{
							$vals = 1;					
						}			
					}
				}
				elseif($sql_type_chk_arp!="")
				{
					if(mysql_num_rows($sql_type_chk_arp)>0)
					{
						$vals = 1;					
					}
					elseif($sql_type_chk_med!="")
					{
						if(mysql_num_rows($sql_type_chk_med)>0)
						{
							$vals = 1;					
						}			
					}
				}
				elseif($sql_type_chk_med!="")
				{
					if(mysql_num_rows($sql_type_chk_med)>0)
					{
						$vals = 1;					
					}			
				}
			}
			elseif($sql_type_chk_coe!="")
			{
				if(mysql_num_rows($sql_type_chk_coe)>0)
				{
					$vals = 1;					
				}
				elseif($sql_type_chk_cop!="")
				{
					if(mysql_num_rows($sql_type_chk_cop)>0)
					{
						$vals = 1;					
					}	
					elseif($sql_type_chk_arp!="")
					{
						if(mysql_num_rows($sql_type_chk_arp)>0)
						{
							$vals = 1;					
						}
						elseif($sql_type_chk_med!="")
						{
							if(mysql_num_rows($sql_type_chk_med)>0)
							{
								$vals = 1;					
							}			
						}
					}
					elseif($sql_type_chk_med!="")
					{
						if(mysql_num_rows($sql_type_chk_med)>0)
						{
							$vals = 1;					
						}			
					}
				}
				elseif($sql_type_chk_arp!="")
				{
					if(mysql_num_rows($sql_type_chk_arp)>0)
					{
						$vals = 1;					
					}
					elseif($sql_type_chk_med!="")
					{
						if(mysql_num_rows($sql_type_chk_med)>0)
						{
							$vals = 1;					
						}			
					}
				}
				elseif($sql_type_chk_med!="")
				{
					if(mysql_num_rows($sql_type_chk_med)>0)
					{
						$vals = 1;					
					}			
				}
			}
			elseif($sql_type_chk_cop!="")
			{
				if(mysql_num_rows($sql_type_chk_cop)>0)
				{
					$vals = 1;					
				}	
				elseif($sql_type_chk_arp!="")
				{
					if(mysql_num_rows($sql_type_chk_arp)>0)
					{
						$vals = 1;					
					}
					elseif($sql_type_chk_med!="")
					{
						if(mysql_num_rows($sql_type_chk_med)>0)
						{
							$vals = 1;					
						}			
					}
				}
				elseif($sql_type_chk_med!="")
				{
					if(mysql_num_rows($sql_type_chk_med)>0)
					{
						$vals = 1;					
					}			
				}
			}
			elseif($sql_type_chk_arp!="")
			{
				if(mysql_num_rows($sql_type_chk_arp)>0)
				{
					$vals = 1;					
				}
				elseif($sql_type_chk_med!="")
				{
					if(mysql_num_rows($sql_type_chk_med)>0)
					{
						$vals = 1;					
					}			
				}
			}
			elseif($sql_type_chk_med!="")
			{
				if(mysql_num_rows($sql_type_chk_med)>0)
				{
					$vals = 1;					
				}			
			}
			
			return $vals;
		}
		
		function metatype_chk_sugg($id)
		{
			$vals = 0;
			$sql_type_chk_art = mysql_query("SELECT * FROM `general_artist_profiles` gap LEFT JOIN `general_artist` ga ON ga.artist_id = gap.artist_id WHERE ga.status = 0 AND ga.del_status=0 AND ga.active = 0 AND gap.metatype_id = '".$id."'");
			
			$sql_type_chk_com = mysql_query("SELECT * FROM `general_community_profiles` gap LEFT JOIN `general_community` ga ON ga.community_id = gap.community_id WHERE ga.status = 0 AND ga.del_status=0 AND ga.active = 0 AND gap.metatype_id = '".$id."'");
			
			$sql_type_chk_are = mysql_query("SELECT * FROM `artist_event_profiles` gap LEFT JOIN `artist_event` ga ON ga.id = gap.artist_event_id WHERE ga.del_status = 0 AND ga.active = 0 AND gap.metatype_id = '".$id."'");
			
			$sql_type_chk_coe = mysql_query("SELECT * FROM `community_event_profiles` gap LEFT JOIN `community_event` ga ON ga.id = gap.community_event_id WHERE ga.del_status = 0 AND ga.active = 0 AND gap.metatype_id = '".$id."'");
			
			$sql_type_chk_cop = mysql_query("SELECT * FROM `community_project_profiles` gap LEFT JOIN `community_project` ga ON ga.id = gap.community_project_id WHERE ga.del_status = 0 AND ga.active = 0 AND gap.metatype_id = '".$id."'");
			
			$sql_type_chk_arp = mysql_query("SELECT * FROM `artist_project_profiles` gap LEFT JOIN `artist_project` ga ON ga.id = gap.artist_project_id WHERE ga.del_status = 0 AND ga.active = 0 AND gap.metatype_id = '".$id."'");
			
			$sql_type_chk_med = mysql_query("SELECT * FROM `media_profiles_type` gap LEFT JOIN `general_media` ga ON ga.id = gap.media_id WHERE ga.media_status = 0 AND ga.delete_status = 0 AND gap.metatype_id = '".$id."'");
			
			if($sql_type_chk_art!="")
			{
				if(mysql_num_rows($sql_type_chk_art)>0)
				{
					$vals = 1;					
				}
				elseif($sql_type_chk_com!="")
				{
					if(mysql_num_rows($sql_type_chk_com)>0)
					{
						$vals = 1;					
					}
					elseif($sql_type_chk_are!="")
					{
						if(mysql_num_rows($sql_type_chk_are)>0)
						{
							$vals = 1;					
						}
						elseif($sql_type_chk_coe!="")
						{
							if(mysql_num_rows($sql_type_chk_coe)>0)
							{
								$vals = 1;					
							}
							elseif($sql_type_chk_cop!="")
							{
								if(mysql_num_rows($sql_type_chk_cop)>0)
								{
									$vals = 1;					
								}	
								elseif($sql_type_chk_arp!="")
								{
									if(mysql_num_rows($sql_type_chk_arp)>0)
									{
										$vals = 1;					
									}
									elseif($sql_type_chk_med!="")
									{
										if(mysql_num_rows($sql_type_chk_med)>0)
										{
											$vals = 1;					
										}			
									}
								}
								elseif($sql_type_chk_med!="")
								{
									if(mysql_num_rows($sql_type_chk_med)>0)
									{
										$vals = 1;					
									}			
								}
							}
							elseif($sql_type_chk_arp!="")
							{
								if(mysql_num_rows($sql_type_chk_arp)>0)
								{
									$vals = 1;					
								}
								elseif($sql_type_chk_med!="")
								{
									if(mysql_num_rows($sql_type_chk_med)>0)
									{
										$vals = 1;					
									}			
								}
							}
							elseif($sql_type_chk_med!="")
							{
								if(mysql_num_rows($sql_type_chk_med)>0)
								{
									$vals = 1;					
								}			
							}
						}
						elseif($sql_type_chk_cop!="")
						{
							if(mysql_num_rows($sql_type_chk_cop)>0)
							{
								$vals = 1;					
							}	
							elseif($sql_type_chk_arp!="")
							{
								if(mysql_num_rows($sql_type_chk_arp)>0)
								{
									$vals = 1;					
								}
								elseif($sql_type_chk_med!="")
								{
									if(mysql_num_rows($sql_type_chk_med)>0)
									{
										$vals = 1;					
									}			
								}
							}
							elseif($sql_type_chk_med!="")
							{
								if(mysql_num_rows($sql_type_chk_med)>0)
								{
									$vals = 1;					
								}			
							}
						}
						elseif($sql_type_chk_arp!="")
						{
							if(mysql_num_rows($sql_type_chk_arp)>0)
							{
								$vals = 1;					
							}
							elseif($sql_type_chk_med!="")
							{
								if(mysql_num_rows($sql_type_chk_med)>0)
								{
									$vals = 1;					
								}			
							}
						}
						elseif($sql_type_chk_med!="")
						{
							if(mysql_num_rows($sql_type_chk_med)>0)
							{
								$vals = 1;					
							}			
						}
					}
					elseif($sql_type_chk_coe!="")
					{
						if(mysql_num_rows($sql_type_chk_coe)>0)
						{
							$vals = 1;					
						}
						elseif($sql_type_chk_cop!="")
						{
							if(mysql_num_rows($sql_type_chk_cop)>0)
							{
								$vals = 1;					
							}	
							elseif($sql_type_chk_arp!="")
							{
								if(mysql_num_rows($sql_type_chk_arp)>0)
								{
									$vals = 1;					
								}
								elseif($sql_type_chk_med!="")
								{
									if(mysql_num_rows($sql_type_chk_med)>0)
									{
										$vals = 1;					
									}			
								}
							}
							elseif($sql_type_chk_med!="")
							{
								if(mysql_num_rows($sql_type_chk_med)>0)
								{
									$vals = 1;					
								}			
							}
						}
						elseif($sql_type_chk_arp!="")
						{
							if(mysql_num_rows($sql_type_chk_arp)>0)
							{
								$vals = 1;					
							}
							elseif($sql_type_chk_med!="")
							{
								if(mysql_num_rows($sql_type_chk_med)>0)
								{
									$vals = 1;					
								}			
							}
						}
						elseif($sql_type_chk_med!="")
						{
							if(mysql_num_rows($sql_type_chk_med)>0)
							{
								$vals = 1;					
							}			
						}
					}
					elseif($sql_type_chk_cop!="")
					{
						if(mysql_num_rows($sql_type_chk_cop)>0)
						{
							$vals = 1;					
						}	
						elseif($sql_type_chk_arp!="")
						{
							if(mysql_num_rows($sql_type_chk_arp)>0)
							{
								$vals = 1;					
							}
							elseif($sql_type_chk_med!="")
							{
								if(mysql_num_rows($sql_type_chk_med)>0)
								{
									$vals = 1;					
								}			
							}
						}
						elseif($sql_type_chk_med!="")
						{
							if(mysql_num_rows($sql_type_chk_med)>0)
							{
								$vals = 1;					
							}			
						}
					}
					elseif($sql_type_chk_arp!="")
					{
						if(mysql_num_rows($sql_type_chk_arp)>0)
						{
							$vals = 1;					
						}
						elseif($sql_type_chk_med!="")
						{
							if(mysql_num_rows($sql_type_chk_med)>0)
							{
								$vals = 1;					
							}			
						}
					}
					elseif($sql_type_chk_med!="")
					{
						if(mysql_num_rows($sql_type_chk_med)>0)
						{
							$vals = 1;					
						}			
					}
				}
				elseif($sql_type_chk_are!="")
				{
					if(mysql_num_rows($sql_type_chk_are)>0)
					{
						$vals = 1;					
					}
					elseif($sql_type_chk_coe!="")
					{
						if(mysql_num_rows($sql_type_chk_coe)>0)
						{
							$vals = 1;					
						}
						elseif($sql_type_chk_cop!="")
						{
							if(mysql_num_rows($sql_type_chk_cop)>0)
							{
								$vals = 1;					
							}	
							elseif($sql_type_chk_arp!="")
							{
								if(mysql_num_rows($sql_type_chk_arp)>0)
								{
									$vals = 1;					
								}
								elseif($sql_type_chk_med!="")
								{
									if(mysql_num_rows($sql_type_chk_med)>0)
									{
										$vals = 1;					
									}			
								}
							}
							elseif($sql_type_chk_med!="")
							{
								if(mysql_num_rows($sql_type_chk_med)>0)
								{
									$vals = 1;					
								}			
							}
						}
						elseif($sql_type_chk_arp!="")
						{
							if(mysql_num_rows($sql_type_chk_arp)>0)
							{
								$vals = 1;					
							}
							elseif($sql_type_chk_med!="")
							{
								if(mysql_num_rows($sql_type_chk_med)>0)
								{
									$vals = 1;					
								}			
							}
						}
						elseif($sql_type_chk_med!="")
						{
							if(mysql_num_rows($sql_type_chk_med)>0)
							{
								$vals = 1;					
							}			
						}
					}
					elseif($sql_type_chk_cop!="")
					{
						if(mysql_num_rows($sql_type_chk_cop)>0)
						{
							$vals = 1;					
						}	
						elseif($sql_type_chk_arp!="")
						{
							if(mysql_num_rows($sql_type_chk_arp)>0)
							{
								$vals = 1;					
							}
							elseif($sql_type_chk_med!="")
							{
								if(mysql_num_rows($sql_type_chk_med)>0)
								{
									$vals = 1;					
								}			
							}
						}
						elseif($sql_type_chk_med!="")
						{
							if(mysql_num_rows($sql_type_chk_med)>0)
							{
								$vals = 1;					
							}			
						}
					}
					elseif($sql_type_chk_arp!="")
					{
						if(mysql_num_rows($sql_type_chk_arp)>0)
						{
							$vals = 1;					
						}
						elseif($sql_type_chk_med!="")
						{
							if(mysql_num_rows($sql_type_chk_med)>0)
							{
								$vals = 1;					
							}			
						}
					}
					elseif($sql_type_chk_med!="")
					{
						if(mysql_num_rows($sql_type_chk_med)>0)
						{
							$vals = 1;					
						}			
					}
				}
				elseif($sql_type_chk_coe!="")
				{
					if(mysql_num_rows($sql_type_chk_coe)>0)
					{
						$vals = 1;					
					}
					elseif($sql_type_chk_cop!="")
					{
						if(mysql_num_rows($sql_type_chk_cop)>0)
						{
							$vals = 1;					
						}	
						elseif($sql_type_chk_arp!="")
						{
							if(mysql_num_rows($sql_type_chk_arp)>0)
							{
								$vals = 1;					
							}
							elseif($sql_type_chk_med!="")
							{
								if(mysql_num_rows($sql_type_chk_med)>0)
								{
									$vals = 1;					
								}			
							}
						}
						elseif($sql_type_chk_med!="")
						{
							if(mysql_num_rows($sql_type_chk_med)>0)
							{
								$vals = 1;					
							}			
						}
					}
					elseif($sql_type_chk_arp!="")
					{
						if(mysql_num_rows($sql_type_chk_arp)>0)
						{
							$vals = 1;					
						}
						elseif($sql_type_chk_med!="")
						{
							if(mysql_num_rows($sql_type_chk_med)>0)
							{
								$vals = 1;					
							}			
						}
					}
					elseif($sql_type_chk_med!="")
					{
						if(mysql_num_rows($sql_type_chk_med)>0)
						{
							$vals = 1;					
						}			
					}
				}
				elseif($sql_type_chk_cop!="")
				{
					if(mysql_num_rows($sql_type_chk_cop)>0)
					{
						$vals = 1;					
					}	
					elseif($sql_type_chk_arp!="")
					{
						if(mysql_num_rows($sql_type_chk_arp)>0)
						{
							$vals = 1;					
						}
						elseif($sql_type_chk_med!="")
						{
							if(mysql_num_rows($sql_type_chk_med)>0)
							{
								$vals = 1;					
							}			
						}
					}
					elseif($sql_type_chk_med!="")
					{
						if(mysql_num_rows($sql_type_chk_med)>0)
						{
							$vals = 1;					
						}			
					}
				}
				elseif($sql_type_chk_arp!="")
				{
					if(mysql_num_rows($sql_type_chk_arp)>0)
					{
						$vals = 1;					
					}
					elseif($sql_type_chk_med!="")
					{
						if(mysql_num_rows($sql_type_chk_med)>0)
						{
							$vals = 1;					
						}			
					}
				}
				elseif($sql_type_chk_med!="")
				{
					if(mysql_num_rows($sql_type_chk_med)>0)
					{
						$vals = 1;					
					}			
				}
			}
			elseif($sql_type_chk_com!="")
			{
				if(mysql_num_rows($sql_type_chk_com)>0)
				{
					$vals = 1;					
				}
				elseif($sql_type_chk_are!="")
				{
					if(mysql_num_rows($sql_type_chk_are)>0)
					{
						$vals = 1;					
					}
					elseif($sql_type_chk_coe!="")
					{
						if(mysql_num_rows($sql_type_chk_coe)>0)
						{
							$vals = 1;					
						}
						elseif($sql_type_chk_cop!="")
						{
							if(mysql_num_rows($sql_type_chk_cop)>0)
							{
								$vals = 1;					
							}	
							elseif($sql_type_chk_arp!="")
							{
								if(mysql_num_rows($sql_type_chk_arp)>0)
								{
									$vals = 1;					
								}
								elseif($sql_type_chk_med!="")
								{
									if(mysql_num_rows($sql_type_chk_med)>0)
									{
										$vals = 1;					
									}			
								}
							}
							elseif($sql_type_chk_med!="")
							{
								if(mysql_num_rows($sql_type_chk_med)>0)
								{
									$vals = 1;					
								}			
							}
						}
						elseif($sql_type_chk_arp!="")
						{
							if(mysql_num_rows($sql_type_chk_arp)>0)
							{
								$vals = 1;					
							}
							elseif($sql_type_chk_med!="")
							{
								if(mysql_num_rows($sql_type_chk_med)>0)
								{
									$vals = 1;					
								}			
							}
						}
						elseif($sql_type_chk_med!="")
						{
							if(mysql_num_rows($sql_type_chk_med)>0)
							{
								$vals = 1;					
							}			
						}
					}
					elseif($sql_type_chk_cop!="")
					{
						if(mysql_num_rows($sql_type_chk_cop)>0)
						{
							$vals = 1;					
						}	
						elseif($sql_type_chk_arp!="")
						{
							if(mysql_num_rows($sql_type_chk_arp)>0)
							{
								$vals = 1;					
							}
							elseif($sql_type_chk_med!="")
							{
								if(mysql_num_rows($sql_type_chk_med)>0)
								{
									$vals = 1;					
								}			
							}
						}
						elseif($sql_type_chk_med!="")
						{
							if(mysql_num_rows($sql_type_chk_med)>0)
							{
								$vals = 1;					
							}			
						}
					}
					elseif($sql_type_chk_arp!="")
					{
						if(mysql_num_rows($sql_type_chk_arp)>0)
						{
							$vals = 1;					
						}
						elseif($sql_type_chk_med!="")
						{
							if(mysql_num_rows($sql_type_chk_med)>0)
							{
								$vals = 1;					
							}			
						}
					}
					elseif($sql_type_chk_med!="")
					{
						if(mysql_num_rows($sql_type_chk_med)>0)
						{
							$vals = 1;					
						}			
					}
				}
				elseif($sql_type_chk_coe!="")
				{
					if(mysql_num_rows($sql_type_chk_coe)>0)
					{
						$vals = 1;					
					}
					elseif($sql_type_chk_cop!="")
					{
						if(mysql_num_rows($sql_type_chk_cop)>0)
						{
							$vals = 1;					
						}	
						elseif($sql_type_chk_arp!="")
						{
							if(mysql_num_rows($sql_type_chk_arp)>0)
							{
								$vals = 1;					
							}
							elseif($sql_type_chk_med!="")
							{
								if(mysql_num_rows($sql_type_chk_med)>0)
								{
									$vals = 1;					
								}			
							}
						}
						elseif($sql_type_chk_med!="")
						{
							if(mysql_num_rows($sql_type_chk_med)>0)
							{
								$vals = 1;					
							}			
						}
					}
					elseif($sql_type_chk_arp!="")
					{
						if(mysql_num_rows($sql_type_chk_arp)>0)
						{
							$vals = 1;					
						}
						elseif($sql_type_chk_med!="")
						{
							if(mysql_num_rows($sql_type_chk_med)>0)
							{
								$vals = 1;					
							}			
						}
					}
					elseif($sql_type_chk_med!="")
					{
						if(mysql_num_rows($sql_type_chk_med)>0)
						{
							$vals = 1;					
						}			
					}
				}
				elseif($sql_type_chk_cop!="")
				{
					if(mysql_num_rows($sql_type_chk_cop)>0)
					{
						$vals = 1;					
					}	
					elseif($sql_type_chk_arp!="")
					{
						if(mysql_num_rows($sql_type_chk_arp)>0)
						{
							$vals = 1;					
						}
						elseif($sql_type_chk_med!="")
						{
							if(mysql_num_rows($sql_type_chk_med)>0)
							{
								$vals = 1;					
							}			
						}
					}
					elseif($sql_type_chk_med!="")
					{
						if(mysql_num_rows($sql_type_chk_med)>0)
						{
							$vals = 1;					
						}			
					}
				}
				elseif($sql_type_chk_arp!="")
				{
					if(mysql_num_rows($sql_type_chk_arp)>0)
					{
						$vals = 1;					
					}
					elseif($sql_type_chk_med!="")
					{
						if(mysql_num_rows($sql_type_chk_med)>0)
						{
							$vals = 1;					
						}			
					}
				}
				elseif($sql_type_chk_med!="")
				{
					if(mysql_num_rows($sql_type_chk_med)>0)
					{
						$vals = 1;					
					}			
				}
			}
			elseif($sql_type_chk_are!="")
			{
				if(mysql_num_rows($sql_type_chk_are)>0)
				{
					$vals = 1;					
				}
				elseif($sql_type_chk_coe!="")
				{
					if(mysql_num_rows($sql_type_chk_coe)>0)
					{
						$vals = 1;					
					}
					elseif($sql_type_chk_cop!="")
					{
						if(mysql_num_rows($sql_type_chk_cop)>0)
						{
							$vals = 1;					
						}	
						elseif($sql_type_chk_arp!="")
						{
							if(mysql_num_rows($sql_type_chk_arp)>0)
							{
								$vals = 1;					
							}
							elseif($sql_type_chk_med!="")
							{
								if(mysql_num_rows($sql_type_chk_med)>0)
								{
									$vals = 1;					
								}			
							}
						}
						elseif($sql_type_chk_med!="")
						{
							if(mysql_num_rows($sql_type_chk_med)>0)
							{
								$vals = 1;					
							}			
						}
					}
					elseif($sql_type_chk_arp!="")
					{
						if(mysql_num_rows($sql_type_chk_arp)>0)
						{
							$vals = 1;					
						}
						elseif($sql_type_chk_med!="")
						{
							if(mysql_num_rows($sql_type_chk_med)>0)
							{
								$vals = 1;					
							}			
						}
					}
					elseif($sql_type_chk_med!="")
					{
						if(mysql_num_rows($sql_type_chk_med)>0)
						{
							$vals = 1;					
						}			
					}
				}
				elseif($sql_type_chk_cop!="")
				{
					if(mysql_num_rows($sql_type_chk_cop)>0)
					{
						$vals = 1;					
					}	
					elseif($sql_type_chk_arp!="")
					{
						if(mysql_num_rows($sql_type_chk_arp)>0)
						{
							$vals = 1;					
						}
						elseif($sql_type_chk_med!="")
						{
							if(mysql_num_rows($sql_type_chk_med)>0)
							{
								$vals = 1;					
							}			
						}
					}
					elseif($sql_type_chk_med!="")
					{
						if(mysql_num_rows($sql_type_chk_med)>0)
						{
							$vals = 1;					
						}			
					}
				}
				elseif($sql_type_chk_arp!="")
				{
					if(mysql_num_rows($sql_type_chk_arp)>0)
					{
						$vals = 1;					
					}
					elseif($sql_type_chk_med!="")
					{
						if(mysql_num_rows($sql_type_chk_med)>0)
						{
							$vals = 1;					
						}			
					}
				}
				elseif($sql_type_chk_med!="")
				{
					if(mysql_num_rows($sql_type_chk_med)>0)
					{
						$vals = 1;					
					}			
				}
			}
			elseif($sql_type_chk_coe!="")
			{
				if(mysql_num_rows($sql_type_chk_coe)>0)
				{
					$vals = 1;					
				}
				elseif($sql_type_chk_cop!="")
				{
					if(mysql_num_rows($sql_type_chk_cop)>0)
					{
						$vals = 1;					
					}	
					elseif($sql_type_chk_arp!="")
					{
						if(mysql_num_rows($sql_type_chk_arp)>0)
						{
							$vals = 1;					
						}
						elseif($sql_type_chk_med!="")
						{
							if(mysql_num_rows($sql_type_chk_med)>0)
							{
								$vals = 1;					
							}			
						}
					}
					elseif($sql_type_chk_med!="")
					{
						if(mysql_num_rows($sql_type_chk_med)>0)
						{
							$vals = 1;					
						}			
					}
				}
				elseif($sql_type_chk_arp!="")
				{
					if(mysql_num_rows($sql_type_chk_arp)>0)
					{
						$vals = 1;					
					}
					elseif($sql_type_chk_med!="")
					{
						if(mysql_num_rows($sql_type_chk_med)>0)
						{
							$vals = 1;					
						}			
					}
				}
				elseif($sql_type_chk_med!="")
				{
					if(mysql_num_rows($sql_type_chk_med)>0)
					{
						$vals = 1;					
					}			
				}
			}
			elseif($sql_type_chk_cop!="")
			{
				if(mysql_num_rows($sql_type_chk_cop)>0)
				{
					$vals = 1;					
				}	
				elseif($sql_type_chk_arp!="")
				{
					if(mysql_num_rows($sql_type_chk_arp)>0)
					{
						$vals = 1;					
					}
					elseif($sql_type_chk_med!="")
					{
						if(mysql_num_rows($sql_type_chk_med)>0)
						{
							$vals = 1;					
						}			
					}
				}
				elseif($sql_type_chk_med!="")
				{
					if(mysql_num_rows($sql_type_chk_med)>0)
					{
						$vals = 1;					
					}			
				}
			}
			elseif($sql_type_chk_arp!="")
			{
				if(mysql_num_rows($sql_type_chk_arp)>0)
				{
					$vals = 1;					
				}
				elseif($sql_type_chk_med!="")
				{
					if(mysql_num_rows($sql_type_chk_med)>0)
					{
						$vals = 1;					
					}			
				}
			}
			elseif($sql_type_chk_med!="")
			{
				if(mysql_num_rows($sql_type_chk_med)>0)
				{
					$vals = 1;					
				}			
			}
			
			return $vals;
		}
		
		function get_profile_name($id)
		{
			$sql = mysql_query("SELECT * FROM profile_types WHERE id='".$id."'");
			$ans = mysql_fetch_assoc($sql);
			return $ans;
		}
		
		function get_type_name($id)
		{
			$sql = mysql_query("SELECT * FROM type WHERE type_id='".$id."'");
			$ans = mysql_fetch_assoc($sql);
			return $ans;
		}
		
		function get_subtype_name($id)
		{
			$sql = mysql_query("SELECT * FROM subtype WHERE subtype_id='".$id."'");
			$ans = mysql_fetch_assoc($sql);
			return $ans;
		}
	}		
?>