<?php
	session_start();
	ob_start();
	include_once('commons/db.php');
	include_once('loggedin_includes.php');
	include_once('classes/ProfileType.php');
	include_once('classes/Commontabs.php');
	include_once('classes/CountryState.php');
	include_once('classes/SuggestSearch.php');
	include('recaptchalib.php');
	include_once('classes/Media.php');
	$search_criteria = new SuggestSearch();
	
	//var_dump($_GET);
	//die;

$objMedia=new Media();	
if($_REQUEST['username'])
{
	$username=$_REQUEST['username'];
	$userpass=$_REQUEST['userpass'];
	$flag=$objMedia->checkUserLogin($username,$userpass);
	if($flag !="")
	{
		$_SESSION['userInfo'] = $username;
		$_SESSION['gen_user_id'] = $flag['general_user_id'];
		$_SESSION['login_email']=$username;
		$_SESSION['login_id']=$flag['general_user_id'];
		$_SESSION['name']=$flag['fname'].$flag['lname'];
		$_SESSION['artist_id']=$flag['artist_id'];
		$_SESSION['community_id']=$flag['community_id'];
		$_SESSION['member_id']=$flag['member_id'];
		$_SESSION['team_id']=$flag['team_id'];
		$logedinFlag=TRUE;
	}	
}
if(isset($_SESSION['userInfo']))
{
	$logedinFlag = TRUE;
}

if(isset($_SESSION['login_email']))
{
	$_SESSION['gen_user_id'] = $_SESSION['login_id'];
	$logedinFlag = TRUE;
}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Purify Art</title>
<!--<link rel="shortcut icon" href="" />-->
<link rel="stylesheet" type="text/css" media="screen" href="includes/purify.css" />
<script src="http://jqueryjs.googlecode.com/files/jquery-1.2.6.min.js" type="text/javascript"></script>
<script src="includes/popup.js" type="text/javascript"></script>

</head>
<body>
<div id="outerContainer">
<?php 
	$newtab=new Commontabs();
	include_once("includes/header.php");
?>
<script ype="text/javascript">
function newfunction(id)
{
	var image = document.getElementById("play_video_image"+id);
	image.src="/images/profile/play_hover.png";
}function newfunctionout(id)
{
	var image = document.getElementById("play_video_image"+id);
	image.src="/images/profile/play_youtube.png";
}
</script>
<script type="text/javascript">
$(document).ready(function(){
	
	
	///////////////////////////////////////////State changes Starts///////////////////////////////////
	<?php
		if(isset($_POST) && !empty($_POST) && $_POST['stateSelect']!='' && $_GET['q_match']=='Search')
		{
	?>
			//alert($("#countrySelect_artist").val());
			$.post("state_search.php", { country_id:$("#countrySelect").val(),state_ids:<?php echo $_POST['stateSelect']; ?> },
				function(data)
				{
					//alert("Data Loaded: " + data);											
					$("#stateSelect").html(data);
				}
			);
	<?php
		}
		else
		{
	?>
			$("#countrySelect").change(
			function ()
			{
				//alert($("#countrySelect_artist").val());
				$.post("state_search.php", { country_id:$("#countrySelect").val() },
					function(data)
					{
					//alert("Data Loaded: " + data);											
						$("#stateSelect").html(data);
					}
				);
			});
	<?php
		}
	?>
	
	$("#countrySelect").change(
		function ()
		{
			//alert($("#countrySelect_artist").val());
			$.post("state_search.php", { country_id:$("#countrySelect").val() },
				function(data)
				{
				//alert("Data Loaded: " + data);											
					$("#stateSelect").html(data);
				}
			);
		});
	///////////////////////////////////////////State changes Ends///////////////////////////////////
		
		
	//////////////////////////////////////////Sub-Type Changes Starts///////////////////////////
	<?php
		if(isset($_POST) && !empty($_POST) && isset($_POST['select-category-artist-subtype']) && $_POST['select-category-artist-subtype']!='' && $_GET['q_match']=='Search')
		{
	?>
			//alert($("#type-select").val());
			$.post("search_registration_block_subtype.php", { type_id:<?php echo $_POST['select-category-artist']; ?>,subtype_ids:<?php echo $_POST['select-category-artist-subtype']; ?> },
				function(data)
				{
					//alert("Data Loaded: " + data);
					//$("#metatype-select").html("<option value='0' selected='selected'>Select Metatype</option>");
					//$("#subtype-select").html(data);
					if(data == " " || data == null || data == "")
					{
						document.getElementById("subtype-select").disabled=true;
					}else{
						document.getElementById("subtype-select").disabled=false;
						$("#subtype-select").html(data);
					}
				}
			);
	<?php
		}
		else
		{
	?>
			$("#type-select").change(
				function ()
				{
					//alert($("#type-select").val());
					$.post("search_registration_block_subtype.php", { type_id:$("#type-select").val() },
					function(data)
					{
						//alert("Data Loaded: " + data);
						//$("#metatype-select").html("<option value='0' selected='selected'>Select Metatype</option>");
						//$("#subtype-select").html(data);
						if(data == " " || data == null || data == "" || $("#type-select").val() ==0)
						{
							document.getElementById("subtype-select").disabled=true;
							$("#subtype-select").html("");
						}else{
							document.getElementById("subtype-select").disabled=false;
							$("#subtype-select").html(data);
						}
					}
					);
				}
			);

	<?php
		}
	?>
		
	$("#type-select").change(
		function ()
		{
			//alert($("#type-select").val());
			$.post("search_registration_block_subtype.php", { type_id:$("#type-select").val() },
			function(data)
			{
				//alert("Data Loaded: " + data);
				//$("#metatype-select").html("<option value='0' selected='selected'>Select Metatype</option>");
				//$("#subtype-select").html(data);
				
				if(data == " " || data == null || data == "" || $("#type-select").val() ==0)
				{
					document.getElementById("subtype-select").disabled=true;
					$("#subtype-select").html("");
				}else{
					document.getElementById("subtype-select").disabled=false;
					$("#subtype-select").html(data);
				}
			}
			);
		}
	);
		
	//////////////////////////////////////////Sub-Type Changes Ends///////////////////////////
	
	
	//////////////////////////////////////////Meta-Type Changes Starts///////////////////////////
	<?php
		if(isset($_POST) && !empty($_POST) && isset($_POST['select-category-artist-metatype']) && $_POST['select-category-artist-metatype']!='' && $_GET['q_match']=='Search')
		{
	?>
			//alert($("#subtype-select").val());
			$.post("search_registration_block_metatype.php", { subtype_id:<?php echo $_POST['select-category-artist-subtype']; ?>,metatypes_id:<?php echo $_POST['select-category-artist-metatype']; ?> },
				function(data)
				{
					//alert("Data Loaded: " + data);
					//$("#metatype-select").html(data);
					if(data == " " || data == null || data == "")
					{
						document.getElementById("metatype-select").disabled=true;
					}else{
						document.getElementById("metatype-select").disabled=false;
						$("#metatype-select").html(data);
					}
				});
	<?php
		}
		else
		{
	?>
			$("#subtype-select").change(
				function ()
				{
					//alert($("#subtype-select").val());
					$.post("search_registration_block_metatype.php", { subtype_id:$("#subtype-select").val() },
					function(data)
					{
						//alert("Data Loaded: " + data);
						//$("#metatype-select").html(data);
						if(data == " " || data == null || data == "" || data == "Select Metatype" || $("#subtype-select").val() ==0)
						{
							document.getElementById("metatype-select").disabled=true;
							$("#metatype-select").html("");
						}else{
							document.getElementById("metatype-select").disabled=false;
							$("#metatype-select").html(data);
						}
					}
					);
				}	
			);
	<?php
		}
	?>
	
	$("#subtype-select").change(
				function ()
				{
					//alert($("#subtype-select").val());
					$.post("search_registration_block_metatype.php", { subtype_id:$("#subtype-select").val() },
					function(data)
					{
						//alert("Data Loaded: " + data);
						//$("#metatype-select").html(data);
						if(data == " " || data == null || data == "" || data == "Select Metatype" || $("#subtype-select").val() ==0)
						{
							document.getElementById("metatype-select").disabled=true;
							$("#metatype-select").html("");
						}else{
							document.getElementById("metatype-select").disabled=false;
							$("#metatype-select").html(data);
						}
					}
					);
				}	
			);
	//////////////////////////////////////////Meta-Type Changes Ends///////////////////////////
	
	////////////////////////////////////////Type Changes Starts/////////////////////////////////////////////
	<?php	
		if(isset($_POST) && !empty($_POST) && isset($_POST['select-category-artist']) && $_POST['select-category-artist']!='' && $_GET['q_match']=='Search')
		{
	?>
			/*$.ajax({
			type: "POST",
			url: 'search_type.php',
			data: { "profile_type":$("#profile_type_search").val(), "type_ids":<?php echo $_POST['select-category-artist']; ?> },
			success: function(data){
				$("#type-select").html(data);
			}
		});*/
	
	
			$.post("search_type.php", { profile_type:$("#profile_type_search").val(),type_ids:<?php echo $_POST['select-category-artist']; ?> },
				function(data)
				{											
					$("#type-select").html(data);
					document.getElementById("type-select").disabled=false;
				}
			);
	<?php
		}
		else
		{
	?>
			$("#profile_type_search").change(function ()
			{
				$.post("search_type.php", { profile_type:$("#profile_type_search").val() },
					function(data)
					{		
						document.getElementById("type-select").disabled=false;
						$("#type-select").html(data);
					}
				);
			});
	<?php
		}
	?>
	
		$("#profile_type_search").change(function ()
		{
			$.post("search_type.php", { profile_type:$("#profile_type_search").val() },
				function(data)
				{
					document.getElementById("type-select").disabled=false;
					document.getElementById("subtype-select").disabled=true;
					document.getElementById("metatype-select").disabled=true;
					//$("#subtype-select").html("<option value='0' selected='selected'>Select Subtype</option>");
					//$("#metatype-select").html("<option value='0' selected='selected'>Select Metatype</option>");
					$("#type-select").html(data);
					$("#subtype-select").html("");
					$("#metatype-select").html("");
				}
			);
		});
	////////////////////////////////////////Type Changes Ends/////////////////////////////////////////////
	
	});
/*function down_song_pop(id,name,creator)
{
	var val = confirm("would you like to donate :"+name+" ("+creator+")");
	if(val== true){
		var Dvideolink = document.getElementById("download_song"+id);
		Dvideolink.click();
	}
	else{
		window.location = "zipes_1.php?download_meds="+id;
	}
}
function down_video_pop(id,name,creator)
{
	var val = confirm("would you like to donate :"+name+" ("+creator+")");
	if(val== true){
		var Dvideolink = document.getElementById("download_video"+id);
		Dvideolink.click();
	}
	else{
		window.location = "zipes_1.php?download_meds="+id;
	}
}*/
function down_song_pop(id)
{
	var Dvideolink = document.getElementById("download_song"+id);
	Dvideolink.click();
}
function down_video_pop(id)
{
	var Dvideolink = document.getElementById("download_video"+id);
	Dvideolink.click();
}
</script>
				<!--<p><a href="../logout.php">Logout</a> </p>
			</div>
		</div>
	</div> 
</div>-->
  <div id="toplevelNav"></div>
  <div>
  		<div id="mediaTab">
            <!--<h1>Advance Search</h1>-->
                <form method="POST" name="serach_form" id="search_form">
					<div id="searchContent">
						<div class="fieldCont">
							<div title="Select the type of categories of users, profiles, and media you would like to search for." class="fieldTitle">Profile Type </div>
							<?php
								if(isset($_POST) && !empty($_POST) && $_POST['profile_type_search']!='select' && $_POST['profile_type_search']!='' && $_GET['q_match']=='Search')
								{
									if($_POST['profile_type_search'] == 'artist')
									{
							?>
										<select title="Select the type of categories of users, profiles, and media you would like to search for." class="dropdown" id="profile_type_search" name="profile_type_search">
											<option value="select">Select</option>  
											<option value="artist" selected>Artist</option>
											<option value="community">Community</option>
											<option value="event">Events</option>
											<option value="media">Media</option>
											<option value="project">Project</option>
											<!--<option value="services">Services</option>-->
										</select>
							<?php
									}
									elseif($_POST['profile_type_search'] == 'community')
									{
							?>
										<select title="Select the type of categories of users, profiles, and media you would like to search for." class="dropdown" id="profile_type_search" name="profile_type_search">
											<option value="select">Select</option>  
											<option value="artist">Artist</option>
											<option value="community" selected>Community</option>
											<option value="event">Events</option>
											<option value="media">Media</option>
											<option value="project">Project</option>
											<!--<option value="services">Services</option>-->
										</select>
							<?php
									}
									elseif($_POST['profile_type_search'] == 'event')
									{
							?>
										<select title="Select the type of categories of users, profiles, and media you would like to search for." class="dropdown" id="profile_type_search" name="profile_type_search">
											<option value="select">Select</option>  
											<option value="artist">Artist</option>
											<option value="community">Community</option>
											<option value="event" selected>Events</option>
											<option value="media">Media</option>
											<option value="project">Project</option>
											<!--<option value="services">Services</option>-->
										</select>
							<?php
									}
									elseif($_POST['profile_type_search'] == 'media')
									{
							?>
										<select title="Select the type of categories of users, profiles, and media you would like to search for." class="dropdown" id="profile_type_search" name="profile_type_search">
											<option value="select">Select</option>  
											<option value="artist">Artist</option>
											<option value="community">Community</option>
											<option value="event">Events</option>
											<option value="media" selected>Media</option>
											<option value="project">Project</option>
											<!--<option value="services">Services</option>-->
										</select>
							<?php
									}
									elseif($_POST['profile_type_search'] == 'project')
									{
							?>
										<select title="Select the type of categories of users, profiles, and media you would like to search for." class="dropdown" id="profile_type_search" name="profile_type_search">
											<option value="select">Select</option>  
											<option value="artist">Artist</option>
											<option value="community">Community</option>
											<option value="event">Events</option>
											<option value="media">Media</option>
											<option value="project" selected>Project</option>
											<!--<option value="services">Services</option>-->
										</select>
							<?php
									}
									elseif($_POST['profile_type_search'] == 'services')
									{
							?>
										<select title="Select the type of categories of users, profiles, and media you would like to search for." class="dropdown" id="profile_type_search" name="profile_type_search">
											<option value="select">Select</option>  
											<option value="artist">Artist</option>
											<option value="community">Community</option>
											<option value="event">Events</option>
											<option value="media">Media</option>
											<option value="project">Project</option>
											<!--<option value="services" selected>Services</option>-->
										</select>
							<?php
									}
								}
								else
								{
							?>
									<select title="Select the type of categories of users, profiles, and media you would like to search for." class="dropdown" id="profile_type_search" name="profile_type_search">
										<option value="select">Select</option>  
										<option value="artist">Artist</option>
										<option value="community">Community</option>
										<option value="event">Events</option>
										<option value="media">Media</option>
										<option value="project">Project</option>
										<!--<option value="services">Services</option>-->
									</select>
							<?php
								}
							?>
						</div>
					</div>
					<div id="searchContent">
						<div class="fieldCont">
							<div title="Select the type of categories of users, profiles, and media you would like to search for." class="fieldTitle">Sub Type</div>
							<?php
								if(isset($_POST) && !empty($_POST) && isset($_POST['select-category-artist']) && $_POST['select-category-artist']!='' && $_GET['q_match']=='Search')
								{
							?>
									<select title="Select the type of categories of users, profiles, and media you would like to search for." name="select-category-artist" class="dropdown" id="type-select"  disabled>
										<!--<option value="0">Select Type</option>-->
									</select>
							<?php
								}
								else
								{
							?>
									<select title="Select the type of categories of users, profiles, and media you would like to search for." name="select-category-artist" class="dropdown" id="type-select" disabled>
										<!--<option value="0" selected="selected">Select Type</option>-->
									</select>
							<?php
								}
							?>
						</div>
						<div class="fieldCont">
							<div title="Select the type of categories of users, profiles, and media you would like to search for." class="fieldTitle"></div>
							<?php
								if(isset($_POST) && !empty($_POST) && isset($_POST['select-category-artist-subtype']) && $_POST['select-category-artist-subtype']!='' && $_GET['q_match']=='Search')
								{
							?>
									<select title="Select the type of categories of users, profiles, and media you would like to search for." name="select-category-artist-subtype" class="dropdown" id="subtype-select" disabled>
										<!--<option value="0">Select Subtype</option>-->
									</select>
							<?php
								}
								else
								{
							?>
									<select title="Select the type of categories of users, profiles, and media you would like to search for." name="select-category-artist-subtype" class="dropdown" id="subtype-select" disabled>
										<!--<option value="0" selected="selected">Select Subtype</option>-->
									</select>
							<?php
								}
							?>
						</div>
						<div class="fieldCont">
							<div title="Select the type of categories of users, profiles, and media you would like to search for." class="fieldTitle"></div>
								<select title="Select the type of categories of users, profiles, and media you would like to search for." name="select-category-artist-metatype" class="dropdown" id="metatype-select" disabled>
									<!--<option value="0" selected="selected">Select Metatype</option>-->
								</select>
						</div>
					</div>
					<div id="searchContent">
						<div class="fieldCont">
							<div title="Select the location you would like to search for users, profiles, and media in." class="fieldTitle">Country</div>
							<?php
								if(isset($_POST) && !empty($_POST) && $_GET['q_match']=='Search')
								{
							?>
									<select title="Select the location you would like to search for users, profiles, and media in." id='countrySelect' name='countrySelect'  class="dropdown">
											<option value="0">Select Country</option>					

									  <?php include('country_search.php'); ?>
									</select>
							<?php
								}
								else
								{
							?>
									<select title="Select the location you would like to search for users, profiles, and media in." id='countrySelect' name='countrySelect'  class="dropdown">
											<option value="0" selected="selected">Select Country</option>					

									  <?php include('country_search.php'); ?>
									</select>
							<?php
								}
							?>
						</div>
						<div class="fieldCont">
							<div title="Select the location you would like to search for users, profiles, and media in." class="fieldTitle">State/Province</div>
							<select title="Select the location you would like to search for users, profiles, and media in." id='stateSelect' name='stateSelect' class="dropdown">
							</select>
						</div>
						<div class="fieldCont">
							<div title="Select the location you would like to search for users, profiles, and media in." class="fieldTitle">City</div>
							<?php
								if(isset($_POST) && !empty($_POST) && $_POST['city_search']!='' && $_GET['q_match']=='Search')
								{
							?>
									<input title="Select the location you would like to search for users, profiles, and media in." id="city_search" name="city_search" type="text" class="fieldText" value="<?php echo $_POST['city_search']; ?>" />
							<?php
								}
								else
								{
							?>
									<input title="Select the location you would like to search for users, profiles, and media in." id="city_search" name="city_search" type="text" class="fieldTex