<?php
	session_start();
	ob_start();
	include_once('loggedin_includes.php');
	include_once('classes/ProfileType.php');
	include_once('classes/Commontabs.php');
	include_once('classes/AddContact.php');
	include_once('classes/GeneralInfo.php');
	include_once('classes/Mails.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Purify Art</title>
<link rel="stylesheet" type="text/css" media="screen" href="includes/purify.css" />
<script src="http://jqueryjs.googlecode.com/files/jquery-1.2.6.min.js" type="text/javascript"></script>
<script src="includes/popup.js" type="text/javascript"></script>
<script src='http://ajax.googleapis.com/ajax/libs/jquery/1.6.1/jquery.min.js'></script>
<script src="includes/organictabs-jquery.js"></script>
<script>
	$(function() {

		$("#personalTab").organicTabs();
	});
</script>

<script>
var timeout	= 500;
var closetimer	= 0;
var ddmenuitem	= 0;

// open hidden layer
function mopen(id)
{	
	// cancel close timer
	mcancelclosetime();

	// close old layer
	if(ddmenuitem) ddmenuitem.style.visibility = 'hidden';

	// get new layer and show it
	ddmenuitem = document.getElementById(id);
	ddmenuitem.style.visibility = 'visible';

}
// close showed layer
function mclose()
{
	if(ddmenuitem) ddmenuitem.style.visibility = 'hidden';
}

// go close timer
function mclosetime()
{
	closetimer = window.setTimeout(mclose, timeout);
}

// cancel close timer
function mcancelclosetime()
{
	if(closetimer)
	{
		window.clearTimeout(closetimer);
		closetimer = null;
	}
}

// close layer when click-out
document.onclick = mclose; 
</script>
<script>
var ContentHeight = 700;
var TimeToSlide = 250.0;

var openAccordion = '';

function runAccordion(index)
{
  var nID = "Accordion" + index + "Content";
  if(openAccordion == nID)
    nID = '';
    
  setTimeout("animate(" + new Date().getTime() + "," + TimeToSlide + ",'" + openAccordion + "','" + nID + "')", 33);
  
  openAccordion = nID;
}

function animate(lastTick, timeLeft, closingId, openingId)
{  
  var curTick = new Date().getTime();
  var elapsedTicks = curTick - lastTick;
  
  var opening = (openingId == '') ? null : document.getElementById(openingId);
  var closing = (closingId == '') ? null : document.getElementById(closingId);
 
  if(timeLeft <= elapsedTicks)
  {
    if(opening != null)
      opening.style.height = ContentHeight + 'px';
    
    if(closing != null)
    {
      closing.style.display = 'none';
      closing.style.height = '0px';
    }
    return;
  }
 
  timeLeft -= elapsedTicks;
  var newClosedHeight = Math.round((timeLeft/TimeToSlide) * ContentHeight);

  if(opening != null)
  {
    if(opening.style.display != 'block')
      opening.style.display = 'block';
    opening.style.height = (ContentHeight - newClosedHeight) + 'px';
  }
  
  if(closing != null)
    closing.style.height = newClosedHeight + 'px';

  setTimeout("animate(" + curTick + "," + timeLeft +",'" + closingId + "','" + openingId + "')", 33);
}</script>
</head>
<body>
<div id="outerContainer">
 <?php 
$newtab=new Commontabs();
include("header.php");
?>
        <p>
        <a href="../index.php">Logout</a></p>
		</div>
	</div>
  </div>
 </div>
  
  <div id="toplevelNav"></div>
  <div>
      <div id="profileTabs">
          <ul>
           <?php
		   $check=new AddContact();
		   
		   $new_artist=$newtab->artist_status();
		   $new_community=$newtab->community_status();
		   $new_team=$newtab->team_status();
		   
		   
		   
		  // var_dump($newres1);
		  //echo $res1['artist_id'];
		  if(($newres1['artist_id']==0 && $new_artist['status']==1) && $newres1['member_id']==0 && ($newres1['community_id']==0 && $new_community['status']==1) && ($newres1['team_id']==0 && $new_team['status']==1))
		  {		  
		  ?>
            <li class="active">PERSONAL</li>		
            <li><a href="profileedit_promotions.php">SUPPORT</a></li>
		   <?php
		   }
		   elseif(($newres1['artist_id']!=0 && $new_artist['status']==0) && ($newres1['community_id']!=0 && $new_community['status']==0) && ($newres1['team_id']!=0 && $new_team['status']==0))
		   {
		   ?>
		   
		   <li class="active">PERSONAL</li>
		   <li><a href="profileedit_media.php">Media</a></li>
		   <li><a href="profileedit_artist.php">ARTIST</a></li>
		   <!--<li><a href="profileedit_community.php">COMMUNITY</a></li>-->
		   <li><a href="profileedit_community.php">COMMUNITY</a></li>
		   <li><a href="profileedit_team.php">TEAM</a></li>
           <li><a href="profileedit_promotions.php">SUPPORT</a></li>
		   
		   <?php
		   }
		   elseif(($newres1['artist_id']!=0 && $new_artist['status']==0) && ($newres1['community_id']!=0 && $new_community['status']==0))
		   {
		   ?>
		   <li class="active">PERSONAL</li>	
		   <li><a href="profileedit_media.php">Media</a></li>		   
		   <li><a href="profileedit_artist.php">ARTIST</a></li>
		   <!--<li><a href="profileedit_community.php">COMMUNITY</a></li>-->
		   <li><a href="profileedit_community.php">COMMUNITY</a></li>
           <li><a href="profileedit_promotions.php">SUPPORT</a></li>
		   <?php
		   }
		   elseif(($newres1['artist_id']!=0 && $new_artist['status']==0) && ($newres1['team_id']!=0 && $new_team['status']==0))
		   {
		   ?>
			<li class="active">PERSONAL</li>	
			<li><a href="profileedit_media.php">Media</a></li>
		    <li><a href="profileedit_artist.php">ARTIST</a></li>
			<li><a href="profileedit_team.php">TEAM</a></li>
            <li><a href="profileedit_promotions.php">SUPPORT</a></li>
		   <?php
		   }
		   elseif(($newres1['community_id']!=0 && $new_community['status']==0) && ($newres1['team_id']!=0 && $new_team['status']==0))
		   {
		   ?>
		   <li class="active">PERSONAL</li>
		   <li><a href="profileedit_media.php">Media</a></li>
		   <!--<li><a href="profileedit_community.php">COMMUNITY</a></li>-->
		   <li><a href="profileedit_community.php">COMMUNITY</a></li>
		   <li><a href="profileedit_team.php">TEAM</a></li>
           <li><a href="profileedit_promotions.php">SUPPORT</a></li>
		   <?php
		   }
		   elseif($newres1['artist_id']!=0 && $new_artist['status']==0)
		   {?>
		    <li class="active">PERSONAL</li>
			<li><a href="profileedit_media.php">Media</a></li>			
			<li><a href="profileedit_artist.php">ARTIST</a></li>
            <li><a href="profileedit_promotions.php">SUPPORT</a></li>
		   <?php
		   }
		   elseif($newres1['member_id']!=0)
		   {
		   ?>
		   <?php
		   }
		   elseif($newres1['community_id']!=0 && $new_community['status']==0)
		   {
		   ?>
		    <li class="active">PERSONAL</li>	
			<li><a href="profileedit_media.php">Media</a></li>
			<!--<li>COMMUNITY</a></li>-->
			<li><a href="profileedit_community.php">COMMUNITY</a></li>
            <li><a href="profileedit_promotions.php">SUPPORT</a></li>
		   <?php
		   }
		   elseif($newres1['team_id']!=0 && $new_team['status']==0)
		   {
		   ?>
		    <li class="active">PERSONAL</li>
			<li><a href="profileedit_media.php">Media</a></li>
			<li><a href="profileedit_team.php">TEAM</a></li>
            <li><a href="profileedit_promotions.php">SUPPORT</a></li>
`		   <?php
		   }
		   else
		   {
		   ?>
			 <li class="active">PERSONAL</li>		
            <li><a href="profileedit_promotions.php">SUPPORT</a></li>
		   <?php
		   }
			?>
          </ul>
      </div>

<script type="text/javascript">
$("document").ready(function(){
var imagename = "<?php echo $newres1['image_name']; ?>";
if(imagename=="")
{
	imagename="Noimage.png";
	$('#loggedin').css({
	backgroundImage : 'url(/uploads/profile_pic/'+ imagename +')',
	backgroundSize :'50px',
	backgroundRepeat: 'no-repeat',
	backgroundPosition: 'right top'
	});
}
else
{
	$('#loggedin').css({
	backgroundImage : 'url(general_jcrop/croppedFiles/thumb/'+ imagename +')',
//backgroundSize :'50px'
	backgroundRepeat: 'no-repeat',
	backgroundPosition: 'right top'
	});
}

});
</script> 
      
          <div id="personalTab">
            <div id="subNavigation" class="tabs">
              <ul class="nav">
                
                <li><a href="profileedit.php#addressbook">Addressbook</a></li>
                <li><a href="profileedit.php#events">Events</a></li>
                <li><a href="profileedit.php#friends">Friends &amp; Fans</a></li>
                <li><a href="profileedit.php#general">General Info </a></li>
                <li><a href="profileedit.php#mail">Mail</a></li>        
                <li><a href="profileedit.php#mymemberships">My Memberships</a></li>
                <li><a href="profileedit.php#profile" class="current">News Feed</a></li>
                <li><a href="profileedit.php#permissions">Permissions</a></li>
                <li><a href="profileedit.php#points">Points</a></li>                
                <li><a href="profileedit.php#registration">Registration</a></li>
                <li><a href="profileedit.php#statistics">Statistics</a></li>
                <li><a href="profileedit.php#subscrriptions">Subscriptions</a></li>
                <li><a href="profileedit.php#services">Sales &amp; Distribution</a></li>
                <li><a href="profileedit.php#transactions">Transactions</a></li>
                
              </ul>
            </div>
            <div class="list-wrap">

                
                
                
                <div class="subTabs" id="mail">
                	<h1>Sent Mails</h1>
                    <div id="mediaContent">
                    	<div class="topLinks" style="border-bottom:1px dotted #999;">
                            <div class="links" style="float:left;">
                                <ul>
                                    <li><a href="compose.php">Compose</a> |</li>
                                    <li><a href="profileedit.php#mail">Inbox</a> |</li>
                                    <li><a href="drafts.php">Drafts</a> |</li>
                                    <li><a href="">Sent Mails</a></li>
                                </ul>
                            </div>
                        </div>
                         <div class="titleCont">
                            <div class="blkA">Sent To</div>
                            <div class="blkB">Subject</div>
                            <div class="blkM">Date</div>
                           
                        </div>
						<?php
							$send_data_mail = new Mails();
							$send = $send_data_mail->sendmails($newres1['general_user_id']);
							$send_run = mysql_query($send);
							
							if(mysql_num_rows($send_run)>0)
							{
								$send_ans = array();
								while($row = mysql_fetch_assoc($send_run))
								{
									$send_ans[] = $row;
								}
								for($s=0;$s<count($send_ans);$s++)
								{
									$sql_get_name = mysql_query("SELECT * FROM general_user WHERE email='".$send_ans[$s]['to']."'");
									
									//$count_conv_sql = "SELECT count(*) FROM `mails` WHERE `to`='".$send_ans[$s]['to']."'";
									$count_conv_sql = mysql_query("SELECT count(*) FROM `mails` WHERE `to`='".$send_ans[$s]['to']."' AND general_user_id=$newres1[general_user_id] AND delete_status=1");
									if(mysql_num_rows($count_conv_sql)>0)
									{
										while($row_count = mysql_fetch_assoc($count_conv_sql))
										{
											$count_ans = $row_count;
										}
											//echo $count_ans['count(*)'];
									}

										if($s==1 && $send_ans[$s]['to']==$send_ans[$s-1]['to'])
										{
											continue;
										}
										else
										{
											if(isset($count_ans['count(*)']) && $count_ans['count(*)']>1)
											{
										?>
											<div class="tableCont">
												<div class="blkA"><a href="mail.php?view_mail_sent=<?php echo $send_ans[$s]['id']; ?>&count=<?php echo $count_ans['count(*)']; ?>"><?php if(mysql_num_rows($sql_get_name)>0) { $name_ans = mysql_fetch_assoc($sql_get_name); echo $name_ans['fname']; } else { echo $send_ans[$s]['to']; } ?>(<?php echo $count_ans['count(*)']; ?>)</a></div>
												<div class="blkB"><a href="mail.php?view_mail_sent=<?php echo $send_ans[$s]['id']; ?>&count=<?php echo $count_ans['count(*)']; ?>"><?php echo $send_ans[$s]['subject']; ?></a></div>
												<div class="blkM"><a href="mail.php?view_mail_sent=<?php echo $send_ans[$s]['id']; ?>&count=<?php echo $count_ans['count(*)']; ?>"><?php echo $send_ans[$s]['date']; ?></a></div>
												<div class="blkD"><a href="javascript:confirmDelete('sent-mails.php?delete=<?php echo $send_ans[$s]['id']; ?>')">Delete</a></div>
											</div>
										<?php
											}
											else
											{
										?>
											<div class="tableCont">
												<div class="blkA"><a href="mail.php?view_mail_sent=<?php echo $send_ans[$s]['id']; ?>"><?php if(mysql_num_rows($sql_get_name)>0) { $name_ans = mysql_fetch_assoc($sql_get_name); echo $name_ans['fname']; } else { echo $send_ans[$s]['to']; } ?></a></div>
												<div class="blkB"><a href="mail.php?view_mail_sent=<?php echo $send_ans[$s]['id']; ?>"><?php echo $send_ans[$s]['subject']; ?></a></div>
												<div class="blkM"><a href="mail.php?view_mail_sent=<?php echo $send_ans[$s]['id']; ?>"><?php echo $send_ans[$s]['date']; ?></a></div>
												<div class="blkD"><a href="javascript:confirmDelete('sent-mails.php?delete=<?php echo $send_ans[$s]['id']; ?>')">Delete</a></div>
											</div>
										<?php
											}
										}
								    }
							    }
						?> 
                        
                    </div>
      			</div>                
               </div>
          </div>
   </div>
</div>
<?php
include_once("footer.php")
?>
  <!-- end of main container -->
</div> 

<div id="popupContact">
		<a id="popupContactClose">x</a>
		<p id="contactArea">
			Only registered users can view media. 
		</p>
        <h1>Register <a href="index.php">Here</a>, It's Free!</h1><br />
        <p>Already Registered ? Login below.</p>
        <div class="formCont">
            <div class="formFieldCont">
        	<div class="fieldTitle">Username</div>
            <input type="text" class="textfield" />
        </div>
       		<div class="formFieldCont">
        	<div class="fieldTitle">Password</div>
            <input type="password" class="textfield" />
        </div>
        	<div class="formFieldCont">
        	<div class="fieldTitle"></div>
            <input type="image" class="login" src="images/profile/login.png" width="47" height="23" />
        </div>
        </div>
	</div>
<div id="backgroundPopup"></div>
</body>
</html>
<script>
function confirmDelete(delUrl) 
{
	if (confirm("Are you sure you want to delete?")) 
	{
		alert("Mail is deleted successfully.");
		document.location = delUrl;
	}
}
</script>
<?php
	if(isset($_GET['delete']))
	{
		$delete = $send_data_mail->delete_mail($_GET['delete']);
		header("Location: sent-mails.php");
	}
?>