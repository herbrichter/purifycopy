<?php
include("commons/db.php");

class GetFanClubMember
{
	function get_price_info($id,$type)
	{
		$sql=mysql_query("Select * from ".$type."_membership where ".$type."_id=$id");
		$res = mysql_fetch_assoc($sql);
		return($res);
	}
	
	function become_a_fan($email,$related_id,$related_type,$purchase_date,$expiry_date)
	{
		$sql1="Insert into fan_club_membership (email,related_id,related_type,purchase_date,expiry_date)
		values ('".$email."',$related_id,'".$related_type."','".$purchase_date."','".$expiry_date."')";
		mysql_query($sql1);
	}
	
	function become_a_subscrb($email,$related_id,$related_type,$purchase_date,$expiry_date)
	{
		if($related_type=='artist_project')
		{
			$sql_part = mysql_query("SELECT * FROM artist_project WHERE id='".$related_id."'");
			$ans_part = mysql_fetch_assoc($sql_part);
			$related_nid = $ans_part['artist_id']."_".$related_id;
		}
		elseif($related_type=='community_project')
		{
			$sql_part = mysql_query("SELECT * FROM community_project WHERE id='".$related_id."'");
			$ans_part = mysql_fetch_assoc($sql_part);
			$related_nid = $ans_part['community_id']."_".$related_id;
		}
		else
		{
			$related_nid = $related_id;
		}
			
		$chk_sub_a = mysql_query("SELECT * FROM email_subscription WHERE subscribe_to_id='".$_SESSION['login_id']."' AND delete_status=1 AND related_type='".$related_type."' AND related_id='".$related_nid."' AND email='".$email."'");

		if(mysql_num_rows($chk_sub_a)<=0)
		{			
			if($related_id!=0 && $related_id!="")
			{
				$chk_vals = $this->chk_registered($email);
				if($chk_vals=="0")
				{
					$fname = '';
					$lname = '';
				}
				else
				{
					$sql_gen_dts = mysql_query("SELECT * FROM general_user WHERE email='".$email."'");
					$ans_gen_dts = mysql_fetch_assoc($sql_gen_dts);
					$fname = $ans_gen_dts['fname'];
					$lname = $ans_gen_dts['lname'];
				}
				//$infos_art = $this->found_a_general($id['artist_id']);
				//if(!empty($infos_art) && $infos_art!="")
				//{
					$sql_sub = mysql_query("INSERT INTO `email_subscription`(`purchase_date`, `expiry_date`, `name`, `email`, `related_id`, `related_type`, `news_feed`, `email_update`, `subscribe_to_id`) VALUES ('".$purchase_date."','".$expiry_date."','".$fname." ".$lname."','".$email."','".$related_nid."','".$related_type."','1','1','".$_SESSION['login_id']."')");
				//}
			}
		}
	}
	
	function get_general_user_id($id,$type)
	{
		$sql3=mysql_query("select * from general_user where ".$type."_id='".$id."'");
		$res3 = mysql_fetch_assoc($sql3);
		return($res3);
	}
	
	function Get_loggedin_Info()
	{
		$get_info = mysql_query("select * from general_user where email='".$_SESSION['login_email']."'");
		$res_info = mysql_fetch_assoc($get_info);
		return($res_info);
	}
	function Get_Artist_Info($id)
	{
		$get_art = mysql_query("select * from general_artist where artist_id='".$id."'");
		if(mysql_num_rows($get_art)>0){
			$res_art = mysql_fetch_assoc($get_art);
			if($res_art['type']=="fan_club_yes" && $res_art['price']!=0){
				return ($res_art);
			}else{
				return ("no");
			}
		}
	}
	function Get_community_Info($id)
	{
		$get_art = mysql_query("select * from general_community where community_id='".$id."'");
		if(mysql_num_rows($get_art)>0){
			$res_art = mysql_fetch_assoc($get_art);
			if($res_art['type']=="fan_club_yes" && $res_art['price']!=0){
				return ($res_art);
			}else{
				return ("no");
			}
		}
	}
	
	function find_member($id,$type)
	{
		$get_id=$this->get_general_user_id($id,$type);
		$sql2=mysql_query("select count(*) from purify_membership where general_user_id='".$get_id['general_user_id']."'");
		$res2 = mysql_fetch_assoc($sql2);
		return($res2);
	}
	
	function same_user_check($chk_email)
	{
		$sql3=mysql_query("select * from general_user where email='".$chk_email."'");
		$res3 = mysql_fetch_assoc($sql3);
		if($res3['email']==$chk_email)
		{
			return 0;
		}
		else
		{
			return 1;
		}
	}
	
	function chk_registered($email_reg)
	{
		$sql_reg = mysql_query("SELECT * FROM `general_user` WHERE `email`='".$email_reg."'");
		if(mysql_num_rows($sql_reg)>0)
		{
			return 1;
		}
		else
		{
			return 0;
		}
	}
	function get_all_artist_project($art_id)
	{		
		$artist_project = array();
		$get_art_pro = mysql_query("select * from artist_project where artist_id='".$art_id."' AND del_status=0 AND active=0");
		if(mysql_num_rows($get_art_pro)>0){
			while($res_pro = mysql_fetch_assoc($get_art_pro))
			{
				if($res_pro['type']=="fan_club" && $res_pro['price']!=0){
					$artist_project[] = $res_pro;
				}else{
					$artist_project[]="";
				}
			}
		}
		return($artist_project);
	}
	function get_all_community_project($com_id)
	{
		$community_project = array();
		$get_art_pro = mysql_query("select * from community_project where community_id='".$com_id."' AND del_status=0 AND active=0");
		if(mysql_num_rows($get_art_pro)>0){
			while($res_pro = mysql_fetch_assoc($get_art_pro))
			{
				if($res_pro['type']=="fan_club" && $res_pro['price']!=0){
					$community_project[] = $res_pro;
				}else{
					$community_project[]="";
				}
			}
		}
		return($community_project);
	}
	
	function art_subs()
	{
		$id = $this->Get_loggedin_Info();
		if($id['artist_id']!=0)
		{
			$sql_mail_artist = mysql_query("SELECT * FROM email_subscription WHERE email!='".$_SESSION['login_email']."' AND related_type='artist' AND related_id='".$id['artist_id']."' AND delete_status=1 AND email_update=1");
			//$sql_mail_artist = "SELECT * FROM add_address_book WHERE email!='".$email_login."' AND category='artist' AND `general_user_id`='".$id['general_user_id']."'";
			if($sql_mail_artist!="")
			{
				if(mysql_num_rows($sql_mail_artist)>0)
				{
					return 1;
				}
			}	
		}
	}
	
	function com_subs()
	{
		$id = $this->Get_loggedin_Info();
		if($id['community_id']!=0)
		{
			$sql_mail_artist = mysql_query("SELECT * FROM email_subscription WHERE email!='".$_SESSION['login_email']."' AND related_type='community' AND related_id='".$id['community_id']."' AND delete_status=1 AND email_update=1");
			//$sql_mail_artist = "SELECT * FROM add_address_book WHERE email!='".$email_login."' AND category='artist' AND `general_user_id`='".$id['general_user_id']."'";
			if($sql_mail_artist!="")
			{
				if(mysql_num_rows($sql_mail_artist)>0)
				{
					return 1;
				}
			}	
		}
	}
	
	function Get_Artist_Info_e($id)
	{
		$get_art = mysql_query("select * from general_artist where artist_id='".$id."'");
		if(mysql_num_rows($get_art)>0){
			$res_art = mysql_fetch_assoc($get_art);
			//if($res_art['type']=="fan_club_yes" && $res_art['price']!=0){
				return ($res_art);
			//}else{
			//	return ("no");
			//}
		}
	}
	
	function Get_community_Info_e($id)
	{
		$get_art = mysql_query("select * from general_community where community_id='".$id."'");
		if(mysql_num_rows($get_art)>0){
			$res_art = mysql_fetch_assoc($get_art);
			//if($res_art['type']=="fan_club_yes" && $res_art['price']!=0){
				return ($res_art);
			//}else{
			//	return ("no");
			//}
		}
	}
	function get_profile_title($name,$id)
	{
		if($name == "artist"){ $tbl_name = "general_artist";$id_name = "artist_id";}
		if($name == "community"){ $tbl_name = "general_community";$id_name = "community_id";}
		if($name == "artist_project"){ $tbl_name = "artist_project";$id_name = "id";}
		if($name == "community_project"){ $tbl_name = "community_project";$id_name = "id";}
		$query = mysql_query("select * from ".$tbl_name." where ".$id_name."='".$id."'");
		if(mysql_num_rows($query)>0){
		$res = mysql_fetch_assoc($query);
		}
		return($res);
	}
}
?>