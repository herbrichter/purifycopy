<?php
include_once('commons/db.php');

class AddArtistProject
{
	function selgeneral()
	{
		$sql=mysql_query("select * from general_user where email='".$_SESSION['login_email']."' AND delete_status=0 AND status=0 AND active=0");
		$id=mysql_fetch_assoc($sql);
		return($id);
	}
	
	function get_users_taggeds($emails)
	{
		$sql=mysql_query("select * from general_user where email='".$emails."' AND delete_status=0 AND status=0 AND active=0");
		$id=mysql_fetch_assoc($sql);
		return($id);
	}
	
	function get_state_tag($id)
	{
		$newsql1=mysql_query("select * from state where state_id='".$id."'");
		return ($newsql1);
	}
	
	function sel_general_aid($ids)
	{
		$sql="select * from general_user where artist_id='".$ids."' AND delete_status=0";
		$row=mysql_query($sql);
		$id=mysql_fetch_assoc($row);
		return($id);
	}
	
	function sel_general_cid($ids)
	{
		$sql="select * from general_user where community_id='".$ids."' AND delete_status=0";
		$row=mysql_query($sql);
		$id=mysql_fetch_assoc($row);
		return($id);
	}
	
	function allArtists($artist_id)
	{
		$sql1=mysql_query("select * from general_artist_profiles where artist_id='".$artist_id."'");
		return ($sql1);
	}
	
	function Type($type_id)
	{
		$sql2=mysql_query("select * from type where type_id='".$type_id."'");
		return ($sql2);
	}
	
	function typeProfile($profile_id)
	{
		$sql3=mysql_query("select * from type where profile_id=46");
		return($sql3);
	}
	
	function artistProject($id)
	{
		$sql4=mysql_query("select * from artist_project where id='".$id."' AND del_status=0");
		return ($sql4);
	}
	
	function getCountry($country_id)
	{
		$newsql=mysql_query("select * from country where country_id='".$country_id."'");
		return($newsql);
	}
	
	function getState($country_id)
	{
		$newsql1=mysql_query("select * from state where country_id='".$country_id."'");
		return ($newsql1);
	}
	
	function SubType($type_id)
	{
		$sql6=mysql_query("select * from subtype where type_id='".$type_id."'");
		return ($sql6);
	}
	
	function updateProject($artist_id,$title,$creator,$btagstr
	,$featured_media,$description,$date,$email,$homepage,$profileurl,$selectCountry,$selectState
	,$editCity,$tagged_project,$tag_gallery,$tag_song,$tag_video,$tag_channel,$tag_events_up,$tag_events_rec,$profile_image,$listing_image,$update,$featured_media,$featured_media_gallery,$tag_user_email,$price,$type,$fan_song,$fan_gallery,$fan_video,$fan_channel,$sale_song,$sale_gallery,$me_table,$create_info,$free_club_song,$free_club_gallery,$free_club_video,$free_club_channel,$ask_tip,$fb_share,$twit_share,$gplus_share,$tubm_share,$stbu_share,$pin_share,$you_share,$vimeo_share,$sdcl_share,$ints_share)
	{
		$me_table = 'general_media';
		$sql= "UPDATE artist_project SET title='".$title."', creator='".$creator."', featured_media='".$featured_media."',
			 description='".$description."', homepage='".$homepage."', country_id='".$selectCountry."',state_id='".$selectState."', city='".$editCity."', users_tagged='".$btagstr."',
			 tagged_projects='".$tagged_project."', tagged_galleries='".$tag_gallery."', tagged_songs='".$tag_song."', email='".$email."',
			 tagged_videos='".$tag_video."',tagged_channel='".$tag_channel."',taggedupcomingevents='".$tag_events_up."',taggedrecordedevents='".$tag_events_rec."', date='".$date."', image_name='".$profile_image."'
			 , listing_image_name='".$listing_image."', profile_url='".$profileurl."', featured_media='".$featured_media."', media_id='".$featured_media_gallery."'
			 , tagged_user_email = '".$tag_user_email."'
			 , price = '".$price."', type = '".$type."', fan_video = '".$fan_video."', fan_song = '".$fan_song."', fan_gallery = '".$fan_gallery."', fan_channel = '".$fan_channel."'
			 , sale_song = '".$sale_song."', sale_gallery = '".$sale_gallery."', featured_media_table='".$me_table."', creators_info = '".$create_info."', free_club_song='".$free_club_song."', free_club_gallery='".$free_club_gallery."',  free_club_video='".$free_club_video."',  free_club_channel='".$free_club_channel."',  ask_tip='$ask_tip', fb_share='".$fb_share."', twit_share='".$twit_share."', gplus_share='".$gplus_share."', tubm_share='".$tubm_share."', stbu_share='".$stbu_share."', pin_share='".$pin_share."', you_share='".$you_share."', vimeo_share='".$vimeo_share."', sdcl_share='".$sdcl_share."', ints_share='".$ints_share."'
			 where id='".$update."'";
			 mysql_query($sql);
	}
	
	function insertProject($artist_id,$title,$creator,$btagstr,$featured_media,$description,$date,$email,$homepage,$profileurl,$selectCountry,$selectState,$editCity,$tagged_project,$tag_gallery,$tag_song,$tag_video,$tag_channel,$tag_events_up,$tag_events_rec,$profile_image,$listing_image,$featured_media,$featured_media_gallery,$tag_user_email,$price,$type,$fan_song,$fan_gallery,$fan_video,$fan_channel,$sale_song,$sale_gallery,$me_table,$create_info,$free_club_song,$free_club_gallery,$free_club_video,$free_club_channel,$ask_tip,$fb_share,$twit_share,$gplus_share,$tubm_share,$stbu_share,$pin_share,$you_share,$vimeo_share,$sdcl_share,$ints_share)
	{
		$me_table = 'general_media';
		$get_det = mysql_query("select * from general_artist where artist_id='".$artist_id."'");
		$res_det = mysql_fetch_assoc($get_det);
		
		$sql="INSERT INTO artist_project(artist_id, title, creator,
	 description, homepage, country_id,state_id, city, users_tagged, tagged_projects,
	tagged_galleries, tagged_songs, tagged_videos, tagged_channel, taggedupcomingevents, taggedrecordedevents, date, image_name,
	 listing_image_name, email, profile_url, featured_media, media_id, tagged_user_email, artist_view_selected, community_view_selected,price,type,fan_song,fan_gallery,fan_video,fan_channel,sale_song,sale_gallery,featured_media_table,creators_info,free_club_song,free_club_gallery,free_club_video,free_club_channel,ask_tip,fb_share,twit_share,gplus_share,tubm_share,stbu_share,pin_share,you_share,vimeo_share,sdcl_share,ints_share)
	 VALUES ('".$artist_id."','".$title."','".$creator."',
	 '".$description."','".$homepage."','".$selectCountry."','".$selectState."','".$editCity."'
	 ,'".$btagstr."','".$tagged_project."','".$tag_gallery."',
	 '".$tag_song."','".$tag_video."','".$tag_channel."','".$tag_events_up."','".$tag_events_rec."','".$date."','".$profile_image."','".$listing_image."','".$email."','".$profileurl."','".$featured_media."','".$featured_media_gallery."','".$tag_user_email."','".$res_det['artist_view_selected']."','".$res_det['community_view_selected']."',
	 '".$price."','".$type."','".$fan_song."','".$fan_gallery."','".$fan_video."','".$fan_channel."','".$sale_song."','".$sale_gallery."','".$me_table."','".$create_info."','".$free_club_song."','".$free_club_gallery."','".$free_club_video."','".$free_club_channel."','$ask_tip','".$fb_share."','".$twit_share."','".$gplus_share."','".$tubm_share."','".$stbu_share."','".$pin_share."','".$you_share."','".$vimeo_share."','".$sdcl_share."','".$ints_share."')";
		mysql_query($sql);
		$id=mysql_insert_id();
		return($id);
	}
	function general_artist_profiles($r,$insert)
	{
		//$getid=$this->insert_community_event();
		$query="insert into `artist_project_profiles` (`artist_project_id`, `type_id`, `subtype_id`, `metatype_id`) values ('$insert','$r[0]','$r[1]','$r[2]')";
		$res=mysql_query($query);
		return ($res);
	}
	function general_artist_profiles1($q,$insert)
	{
		//$getid=$this->insert_community_event();
		$query="insert into `artist_project_profiles` (`artist_project_id`, `type_id`, `subtype_id`, `metatype_id`) values ('$insert','$q[0]','$q[1]','0')";
		$res=mysql_query($query);
		return ($res);
	}
	function general_artist_profiles2($p,$insert)
	{
		//$getid=$this->insert_community_event();
		$query="insert into `artist_project_profiles` (`artist_project_id`, `type_id`, `subtype_id`, `metatype_id`) values ('$insert','$p[0]','0','0')";
		$res=mysql_query($query);
		return ($res);
	}
	function selartist_project_profile($id)
	{
		$getmeta=mysql_query("select * from artist_project_profiles where artist_project_id='".$id."' ORDER BY id ASC");
		return($getmeta);
	}
	function DeleteArtistProject($proId)
	{
		$query = mysql_query("update artist_project set del_status =1,active=1 where id='".$proId."'");
		$get_project = mysql_query("select * from artist_project where id='".$proId."'");
		if(mysql_num_rows($get_project)>0)
		{
			$res_project = mysql_fetch_assoc($get_project);
			$exp_user = explode(",",$res_project['tagged_user_email']);
			for($exp_count=0;$exp_count<count($exp_user);$exp_count++)
			{
				$get_gen = mysql_query("select * from general_user where email ='".$exp_user[$exp_count]."'");
				if(mysql_num_rows($get_gen)>0)
				{
					$all_id="";
					$res_gen = mysql_fetch_assoc($get_gen);
					$exp_id = explode(",",$res_gen['taggedartistprojects']);
					for($exp_count_id=0;$exp_count_id<count($exp_id);$exp_count_id++)
					{
						if($exp_id[$exp_count_id]==""){continue;}
						else{
							if($exp_id[$exp_count_id] != $proId)
							{
								$all_id = $all_id .",". $exp_id[$exp_count_id];
							}
						}
					}
					mysql_query("update general_user set taggedartistprojects='".$all_id."' where email='".$exp_user[$exp_count]."'");
					$get_tag_project = mysql_query("select * from artist_project where artist_id='".$res_gen['artist_id']."'");
					if(mysql_num_rows($get_tag_project)>0)
					{
						while($get_tag = mysql_fetch_assoc($get_tag_project))
						{
							$all_tag_id = "";
							$tag_pro = explode(",",$get_tag['tagged_projects']);
							for($tag_count=0;$tag_count<count($tag_pro);$tag_count++)
							{
								if($tag_pro[$tag_count]==""){continue;}
								else{
									if($tag_pro[$tag_count]!= $proId)
									{
										$all_tag_id = $all_tag_id .",".$tag_pro[$tag_count];
									}
								}
								
							}
							mysql_query("update artist_project set tagged_projects = '".$all_tag_id."' where id='".$get_tag['id']."'");
						}
					}
					$get_gen_art = mysql_query("select * from general_artist where artist_id='".$res_gen['artist_id']."'");
					if(mysql_num_rows($get_gen_art)>0)
					{
						while($res_gen_art = mysql_fetch_assoc($get_gen_art))
						{
							$all_tag_gen_id ="";
							$exp_gen_tag = explode(",",$res_gen_art['taggedprojects']);
							for($count_gen_tag=0;$count_gen_tag<count($exp_gen_tag);$count_gen_tag++)
							{
								if($exp_gen_tag[$count_gen_tag]==""){continue;}
								else{
									if($exp_gen_tag[$count_gen_tag]!= $proId)
									{
										$all_tag_gen_id = $all_tag_gen_id .",". $exp_gen_tag[$count_gen_tag];
									}
								}
							}
							mysql_query("update general_artist set taggedprojects ='".$all_tag_gen_id."' where artist_id ='".$res_gen['artist_id']."'");
						}
					}
					$art_event = mysql_query("select * from artist_event where artist_id = '".$res_gen['artist_id']."'");
					if(mysql_num_rows($art_event)>0)
					{
						while($get_artist_event = mysql_fetch_assoc($art_event))
						{
							$all_event_pro_id ="";
							$exp_event_pro = explode(",",$get_artist_event['tagged_projects']);
							for($count_event_pro=0;$count_event_pro<count($exp_event_pro);$count_event_pro++)
							{
								if($exp_event_pro[$count_event_pro]==""){continue;}
								else{
									if($exp_event_pro[$count_event_pro] != $proId)
									{
										$all_event_pro_id = $all_event_pro_id .",". $exp_event_pro[$count_event_pro];
									}
								}
							}
							mysql_query("update artist_event set tagged_projects  ='".$all_event_pro_id."' where id='".$get_artist_event['id']."'");
						}
					}
				}
			}
		}
		//$sql2="Delete from artist_project_profiles where artist_project_id=$proId";
		//mysql_query($sql2);
	}
	function delete_artist_project_relations($creator_info,$pro_id)
	{
		$get_pro_det = mysql_query("select * from artist_project where id='".$pro_id."'");
		if(mysql_num_rows($get_pro_det)>0)
		{
			$res_pro_det = mysql_fetch_assoc($get_pro_det);
			$get_all_mediaid ="";
			$get_all_pro_media = mysql_query("select * from general_media where creator_info = '".$creator_info."' OR from_info = '".$creator_info."'");
			if(mysql_num_rows($get_all_pro_media)>0){
				while($res_all_pro_media = mysql_fetch_assoc($get_all_pro_media))
				{
					$get_all_mediaid = $get_all_mediaid .",". $res_all_pro_media['id'];
				}
			}
			$exp_tag_email = explode(",",$res_pro_det['tagged_user_email']);
			for($count_tag=0;$count_tag<=count($exp_tag_email);$count_tag++)
			{
				if($exp_tag_email[$count_tag]!="")
				{
					$get_tag_det = mysql_query("select * from general_user where email='".$exp_tag_email[$count_tag]."'");
					if(mysql_num_rows($get_tag_det)>0)
					{
						$res_tag_det = mysql_fetch_assoc($get_tag_det);
						if($res_tag_det['artist_id']>0)
						{
							$get_tag_art_det = mysql_query("select * from general_artist where artist_id='".$res_tag_det['artist_id']."'");
							if(mysql_num_rows($get_tag_art_det)>0)
							{
								$res_tag_art_det = mysql_fetch_assoc($get_tag_art_det);
								$exp_tag_song = explode(",",$res_tag_art_det['taggedsongs']);
								$exp_tag_video = explode(",",$res_tag_art_det['taggedvideos']);
								$exp_tag_gallery = explode(",",$res_tag_art_det['taggedgalleries']);
								if($get_all_mediaid!="")
								{
									$exp_all_mediaid = explode(",",$get_all_mediaid);
									for($count_media=0;$count_media<=count($exp_all_mediaid);$count_media++)
									{
										if($exp_all_mediaid[$count_media]==""){continue;}
										else{
											for($count_tag=0;$count_tag<=count($exp_tag_song);$count_tag++)
											{
												if($exp_tag_song[$count_tag]==""){continue;}
												else{
													if($exp_tag_song[$count_tag] == $exp_all_mediaid[$count_media])
													{
														$exp_tag_song[$count_tag]="";
													}
												}
											}
										}
									}
									for($count_media=0;$count_media<=count($exp_all_mediaid);$count_media++)
									{
										if($exp_all_mediaid[$count_media]==""){continue;}
										else{
											for($count_tag_video=0;$count_tag_video<=count($exp_tag_video);$count_tag_video++)
											{
												if($exp_tag_video[$count_tag_video]==""){continue;}
												else{
													if($exp_tag_video[$count_tag_video] == $exp_all_mediaid[$count_media])
													{
														$exp_tag_video[$count_tag_video]="";
													}
												}
											}
										}
									}
									for($count_media=0;$count_media<=count($exp_all_mediaid);$count_media++)
									{
										if($exp_all_mediaid[$count_media]==""){continue;}
										else{
											for($count_tag_gal=0;$count_tag_gal<=count($exp_tag_gallery);$count_tag_gal++)
											{
												if($exp_tag_gallery[$count_tag_gal]==""){continue;}
												else{
													if($exp_tag_gallery[$count_tag_gal] == $exp_all_mediaid[$count_media])
													{
														$exp_tag_gallery[$count_tag_gal]="";
													}
												}
											}
										}
									}
									$new_song_id="";
									for($count_song_id=0;$count_song_id<=count($exp_tag_song);$count_song_id++)
									{
										if($exp_tag_song[$count_song_id]!=""){
										$new_song_id = $new_song_id .",". $exp_tag_song[$count_song_id];}
									}
									$new_video_id="";
									for($count_video_id=0;$count_video_id<=count($exp_tag_video);$count_video_id++)
									{
										if($exp_tag_video[$count_video_id]!=""){
										$new_video_id = $new_video_id .",". $exp_tag_video[$count_video_id];}
									}
									$new_gallery_id="";
									for($count_gallery_id=0;$count_gallery_id<=count($exp_tag_gallery);$count_gallery_id++)
									{
										if($exp_tag_gallery[$count_gallery_id]!=""){
										$new_gallery_id = $new_gallery_id .",". $exp_tag_gallery[$count_gallery_id];}
									}
									mysql_query("update general_artist set taggedsongs='".$new_song_id."', taggedvideos='".$new_video_id."', taggedgalleries='".$new_gallery_id."' where artist_id='".$res_tag_art_det['artist_id']."'");
								}
							}
							$get_tag_artpro_det = mysql_query("select * from artist_project where artist_id='".$res_tag_det['artist_id']."'");
							if(mysql_num_rows($get_tag_artpro_det)>0)
							{
								while($res_tag_artpro_det = mysql_fetch_assoc($get_tag_artpro_det))
								{
									$exp_tag_song = explode(",",$res_tag_artpro_det['tagged_songs']);
									$exp_tag_video = explode(",",$res_tag_artpro_det['tagged_videos']);
									$exp_tag_gallery = explode(",",$res_tag_artpro_det['tagged_galleries']);
									if($get_all_mediaid!="")
									{
										$exp_all_mediaid = explode(",",$get_all_mediaid);
										for($count_media=0;$count_media<=count($exp_all_mediaid);$count_media++)
										{
											if($exp_all_mediaid[$count_media]==""){continue;}
											else{
												for($count_tag=0;$count_tag<=count($exp_tag_song);$count_tag++)
												{
													if($exp_tag_song[$count_tag]==""){continue;}
													else{
														if($exp_tag_song[$count_tag] == $exp_all_mediaid[$count_media])
														{
															$exp_tag_song[$count_tag]="";
														}
													}
												}
											}
										}
										for($count_media=0;$count_media<=count($exp_all_mediaid);$count_media++)
										{
											if($exp_all_mediaid[$count_media]==""){continue;}
											else{
												for($count_tag_video=0;$count_tag_video<=count($exp_tag_video);$count_tag_video++)
												{
													if($exp_tag_video[$count_tag_video]==""){continue;}
													else{
														if($exp_tag_video[$count_tag_video] == $exp_all_mediaid[$count_media])
														{
															$exp_tag_video[$count_tag_video]="";
														}
													}
												}
											}
										}
										for($count_media=0;$count_media<=count($exp_all_mediaid);$count_media++)
										{
											if($exp_all_mediaid[$count_media]==""){continue;}
											else{
												for($count_tag_gal=0;$count_tag_gal<=count($exp_tag_gallery);$count_tag_gal++)
												{
													if($exp_tag_gallery[$count_tag_gal]==""){continue;}
													else{
														if($exp_tag_gallery[$count_tag_gal] == $exp_all_mediaid[$count_media])
														{
															$exp_tag_gallery[$count_tag_gal]="";
														}
													}
												}
											}
										}
										$new_song_id="";
										for($count_song_id=0;$count_song_id<=count($exp_tag_song);$count_song_id++)
										{
											if($exp_tag_song[$count_song_id]!=""){
											$new_song_id = $new_song_id .",". $exp_tag_song[$count_song_id];}
										}
										$new_video_id="";
										for($count_video_id=0;$count_video_id<=count($exp_tag_video);$count_video_id++)
										{
											if($exp_tag_video[$count_video_id]!=""){
											$new_video_id = $new_video_id .",". $exp_tag_video[$count_video_id];}
										}
										$new_gallery_id="";
										for($count_gallery_id=0;$count_gallery_id<=count($exp_tag_gallery);$count_gallery_id++)
										{
											if($exp_tag_gallery[$count_gallery_id]!=""){
											$new_gallery_id = $new_gallery_id .",". $exp_tag_gallery[$count_gallery_id];}
										}
										mysql_query("update artist_project set tagged_songs='".$new_song_id."', tagged_videos='".$new_video_id."', tagged_galleries='".$new_gallery_id."' where id='".$res_tag_artpro_det['id']."'");
									}
								}
							}
							$get_tag_arteve_det = mysql_query("select * from artist_event where artist_id='".$res_tag_det['artist_id']."'");
							if(mysql_num_rows($get_tag_arteve_det)>0)
							{
								while($res_tag_arteve_det = mysql_fetch_assoc($get_tag_arteve_det))
								{
									$exp_tag_song = explode(",",$res_tag_arteve_det['tagged_songs']);
									$exp_tag_video = explode(",",$res_tag_arteve_det['tagged_videos']);
									$exp_tag_gallery = explode(",",$res_tag_arteve_det['tagged_galleries']);
									if($get_all_mediaid!="")
									{
										$exp_all_mediaid = explode(",",$get_all_mediaid);
										for($count_media=0;$count_media<=count($exp_all_mediaid);$count_media++)
										{
											if($exp_all_mediaid[$count_media]==""){continue;}
											else{
												for($count_tag=0;$count_tag<=count($exp_tag_song);$count_tag++)
												{
													if($exp_tag_song[$count_tag]==""){continue;}
													else{
														if($exp_tag_song[$count_tag] == $exp_all_mediaid[$count_media])
														{
															$exp_tag_song[$count_tag]="";
														}
													}
												}
											}
										}
										for($count_media=0;$count_media<=count($exp_all_mediaid);$count_media++)
										{
											if($exp_all_mediaid[$count_media]==""){continue;}
											else{
												for($count_tag_video=0;$count_tag_video<=count($exp_tag_video);$count_tag_video++)
												{
													if($exp_tag_video[$count_tag_video]==""){continue;}
													else{
														if($exp_tag_video[$count_tag_video] == $exp_all_mediaid[$count_media])
														{
															$exp_tag_video[$count_tag_video]="";
														}
													}
												}
											}
										}
										for($count_media=0;$count_media<=count($exp_all_mediaid);$count_media++)
										{
											if($exp_all_mediaid[$count_media]==""){continue;}
											else{
												for($count_tag_gal=0;$count_tag_gal<=count($exp_tag_gallery);$count_tag_gal++)
												{
													if($exp_tag_gallery[$count_tag_gal]==""){continue;}
													else{
														if($exp_tag_gallery[$count_tag_gal] == $exp_all_mediaid[$count_media])
														{
															$exp_tag_gallery[$count_tag_gal]="";
														}
													}
												}
											}
										}
										$new_song_id="";
										for($count_song_id=0;$count_song_id<=count($exp_tag_song);$count_song_id++)
										{
											if($exp_tag_song[$count_song_id]!=""){
											$new_song_id = $new_song_id .",". $exp_tag_song[$count_song_id];}
										}
										$new_video_id="";
										for($count_video_id=0;$count_video_id<=count($exp_tag_video);$count_video_id++)
										{
											if($exp_tag_video[$count_video_id]!=""){
											$new_video_id = $new_video_id .",". $exp_tag_video[$count_video_id];}
										}
										$new_gallery_id="";
										for($count_gallery_id=0;$count_gallery_id<=count($exp_tag_gallery);$count_gallery_id++)
										{
											if($exp_tag_gallery[$count_gallery_id]!=""){
											$new_gallery_id = $new_gallery_id .",". $exp_tag_gallery[$count_gallery_id];}
										}
										mysql_query("update artist_event set tagged_songs='".$new_song_id."', tagged_videos='".$new_video_id."', tagged_galleries='".$new_gallery_id."' where id='".$res_tag_arteve_det['id']."'");
									}
								}
							}
						}
						
						if($res_tag_det['community_id']>0)
						{
							$get_tag_com_det = mysql_query("select * from general_community where community_id='".$res_tag_det['community_id']."'");
							if(mysql_num_rows($get_tag_com_det)>0)
							{
								$res_tag_com_det = mysql_fetch_assoc($get_tag_com_det);
								$exp_tag_song = explode(",",$res_tag_com_det['taggedsongs']);
								$exp_tag_video = explode(",",$res_tag_com_det['taggedvideos']);
								$exp_tag_gallery = explode(",",$res_tag_com_det['taggedgalleries']);
								if($get_all_mediaid!="")
								{
									$exp_all_mediaid = explode(",",$get_all_mediaid);
									for($count_media=0;$count_media<=count($exp_all_mediaid);$count_media++)
									{
										if($exp_all_mediaid[$count_media]==""){continue;}
										else{
											for($count_tag=0;$count_tag<=count($exp_tag_song);$count_tag++)
											{
												if($exp_tag_song[$count_tag]==""){continue;}
												else{
													if($exp_tag_song[$count_tag] == $exp_all_mediaid[$count_media])
													{
														$exp_tag_song[$count_tag]="";
													}
												}
											}
										}
									}
									for($count_media=0;$count_media<=count($exp_all_mediaid);$count_media++)
									{
										if($exp_all_mediaid[$count_media]==""){continue;}
										else{
											for($count_tag_video=0;$count_tag_video<=count($exp_tag_video);$count_tag_video++)
											{
												if($exp_tag_video[$count_tag_video]==""){continue;}
												else{
													if($exp_tag_video[$count_tag_video] == $exp_all_mediaid[$count_media])
													{
														$exp_tag_video[$count_tag_video]="";
													}
												}
											}
										}
									}
									for($count_media=0;$count_media<=count($exp_all_mediaid);$count_media++)
									{
										if($exp_all_mediaid[$count_media]==""){continue;}
										else{
											for($count_tag_gal=0;$count_tag_gal<=count($exp_tag_gallery);$count_tag_gal++)
											{
												if($exp_tag_gallery[$count_tag_gal]==""){continue;}
												else{
													if($exp_tag_gallery[$count_tag_gal] == $exp_all_mediaid[$count_media])
													{
														$exp_tag_gallery[$count_tag_gal]="";
													}
												}
											}
										}
									}
									$new_song_id="";
									for($count_song_id=0;$count_song_id<=count($exp_tag_song);$count_song_id++)
									{
										if($exp_tag_song[$count_song_id]!=""){
										$new_song_id = $new_song_id .",". $exp_tag_song[$count_song_id];}
									}
									$new_video_id="";
									for($count_video_id=0;$count_video_id<=count($exp_tag_video);$count_video_id++)
									{
										if($exp_tag_video[$count_video_id]!=""){
										$new_video_id = $new_video_id .",". $exp_tag_video[$count_video_id];}
									}
									$new_gallery_id="";
									for($count_gallery_id=0;$count_gallery_id<=count($exp_tag_gallery);$count_gallery_id++)
									{
										if($exp_tag_gallery[$count_gallery_id]!=""){
										$new_gallery_id = $new_gallery_id .",". $exp_tag_gallery[$count_gallery_id];}
									}
									mysql_query("update general_community set taggedsongs='".$new_song_id."', taggedvideos='".$new_video_id."', taggedgalleries='".$new_gallery_id."' where community_id='".$res_tag_com_det['community_id']."'");
								}
							}
							$get_tag_compro_det = mysql_query("select * from community_project where community_id='".$res_tag_det['community_id']."'");
							if(mysql_num_rows($get_tag_compro_det)>0)
							{
								while($res_tag_compro_det = mysql_fetch_assoc($get_tag_compro_det))
								{
									$exp_tag_song = explode(",",$res_tag_compro_det['tagged_songs']);
									$exp_tag_video = explode(",",$res_tag_compro_det['tagged_videos']);
									$exp_tag_gallery = explode(",",$res_tag_compro_det['tagged_galleries']);
									if($get_all_mediaid!="")
									{
										$exp_all_mediaid = explode(",",$get_all_mediaid);
										for($count_media=0;$count_media<=count($exp_all_mediaid);$count_media++)
										{
											if($exp_all_mediaid[$count_media]==""){continue;}
											else{
												for($count_tag=0;$count_tag<=count($exp_tag_song);$count_tag++)
												{
													if($exp_tag_song[$count_tag]==""){continue;}
													else{
														if($exp_tag_song[$count_tag] == $exp_all_mediaid[$count_media])
														{
															$exp_tag_song[$count_tag]="";
														}
													}
												}
											}
										}
										for($count_media=0;$count_media<=count($exp_all_mediaid);$count_media++)
										{
											if($exp_all_mediaid[$count_media]==""){continue;}
											else{
												for($count_tag_video=0;$count_tag_video<=count($exp_tag_video);$count_tag_video++)
												{
													if($exp_tag_video[$count_tag_video]==""){continue;}
													else{
														if($exp_tag_video[$count_tag_video] == $exp_all_mediaid[$count_media])
														{
															$exp_tag_video[$count_tag_video]="";
														}
													}
												}
											}
										}
										for($count_media=0;$count_media<=count($exp_all_mediaid);$count_media++)
										{
											if($exp_all_mediaid[$count_media]==""){continue;}
											else{
												for($count_tag_gal=0;$count_tag_gal<=count($exp_tag_gallery);$count_tag_gal++)
												{
													if($exp_tag_gallery[$count_tag_gal]==""){continue;}
													else{
														if($exp_tag_gallery[$count_tag_gal] == $exp_all_mediaid[$count_media])
														{
															$exp_tag_gallery[$count_tag_gal]="";
														}
													}
												}
											}
										}
										$new_song_id="";
										for($count_song_id=0;$count_song_id<=count($exp_tag_song);$count_song_id++)
										{
											if($exp_tag_song[$count_song_id]!=""){
											$new_song_id = $new_song_id .",". $exp_tag_song[$count_song_id];}
										}
										$new_video_id="";
										for($count_video_id=0;$count_video_id<=count($exp_tag_video);$count_video_id++)
										{
											if($exp_tag_video[$count_video_id]!=""){
											$new_video_id = $new_video_id .",". $exp_tag_video[$count_video_id];}
										}
										$new_gallery_id="";
										for($count_gallery_id=0;$count_gallery_id<=count($exp_tag_gallery);$count_gallery_id++)
										{
											if($exp_tag_gallery[$count_gallery_id]!=""){
											$new_gallery_id = $new_gallery_id .",". $exp_tag_gallery[$count_gallery_id];}
										}
										mysql_query("update community_project set tagged_songs='".$new_song_id."', tagged_videos='".$new_video_id."', tagged_galleries='".$new_gallery_id."' where id='".$res_tag_compro_det['id']."'");
									}
								}
							}
							$get_tag_comeve_det = mysql_query("select * from community_event where community_id='".$res_tag_det['community_id']."'");
							if(mysql_num_rows($get_tag_comeve_det)>0)
							{
								while($res_tag_comeve_det = mysql_fetch_assoc($get_tag_comeve_det))
								{
									$exp_tag_song = explode(",",$res_tag_comeve_det['tagged_songs']);
									$exp_tag_video = explode(",",$res_tag_comeve_det['tagged_videos']);
									$exp_tag_gallery = explode(",",$res_tag_comeve_det['tagged_galleries']);
									if($get_all_mediaid!="")
									{
										$exp_all_mediaid = explode(",",$get_all_mediaid);
										for($count_media=0;$count_media<=count($exp_all_mediaid);$count_media++)
										{
											if($exp_all_mediaid[$count_media]==""){continue;}
											else{
												for($count_tag=0;$count_tag<=count($exp_tag_song);$count_tag++)
												{
													if($exp_tag_song[$count_tag]==""){continue;}
													else{
														if($exp_tag_song[$count_tag] == $exp_all_mediaid[$count_media])
														{
															$exp_tag_song[$count_tag]="";
														}
													}
												}
											}
										}
										for($count_media=0;$count_media<=count($exp_all_mediaid);$count_media++)
										{
											if($exp_all_mediaid[$count_media]==""){continue;}
											else{
												for($count_tag_video=0;$count_tag_video<=count($exp_tag_video);$count_tag_video++)
												{
													if($exp_tag_video[$count_tag_video]==""){continue;}
													else{
														if($exp_tag_video[$count_tag_video] == $exp_all_mediaid[$count_media])
														{
															$exp_tag_video[$count_tag_video]="";
														}
													}
												}
											}
										}
										for($count_media=0;$count_media<=count($exp_all_mediaid);$count_media++)
										{
											if($exp_all_mediaid[$count_media]==""){continue;}
											else{
												for($count_tag_gal=0;$count_tag_gal<=count($exp_tag_gallery);$count_tag_gal++)
												{
													if($exp_tag_gallery[$count_tag_gal]==""){continue;}
													else{
														if($exp_tag_gallery[$count_tag_gal] == $exp_all_mediaid[$count_media])
														{
															$exp_tag_gallery[$count_tag_gal]="";
														}
													}
												}
											}
										}
										$new_song_id="";
										for($count_song_id=0;$count_song_id<=count($exp_tag_song);$count_song_id++)
										{
											if($exp_tag_song[$count_song_id]!=""){
											$new_song_id = $new_song_id .",". $exp_tag_song[$count_song_id];}
										}
										$new_video_id="";
										for($count_video_id=0;$count_video_id<=count($exp_tag_video);$count_video_id++)
										{
											if($exp_tag_video[$count_video_id]!=""){
											$new_video_id = $new_video_id .",". $exp_tag_video[$count_video_id];}
										}
										$new_gallery_id="";
										for($count_gallery_id=0;$count_gallery_id<=count($exp_tag_gallery);$count_gallery_id++)
										{
											if($exp_tag_gallery[$count_gallery_id]!=""){
											$new_gallery_id = $new_gallery_id .",". $exp_tag_gallery[$count_gallery_id];}
										}
										mysql_query("update community_event set tagged_songs='".$new_song_id."', tagged_videos='".$new_video_id."', tagged_galleries='".$new_gallery_id."' where id='".$res_tag_comeve_det['id']."'");
									}
								}
							}
						}
					}
				}
			}
			$exp_creator = explode("|",$res_pro_det['creator_info']);
			if($exp_creator[0]=="general_artist"){
				$get_creator_in_det = mysql_query("select * from general_user where artist_id='".$exp_creator[1]."'");
			}if($exp_creator[0]=="general_community"){
				$get_creator_in_det = mysql_query("select * from general_user where community_id='".$exp_creator[1]."'");
			}if($exp_creator[0]=="artist_project"){
				$get_prodet = mysql_query("select * from artist_project where id='".$exp_creator[1]."'");
				$res_prodet = mysql_fetch_assoc($get_prodet);
				$get_creator_in_det = mysql_query("select * from general_user where artist_id='".$res_prodet['artist_id']."'");
			}if($exp_creator[0]=="community_project"){
				$get_prodet = mysql_query("select * from community_project where id='".$exp_creator[1]."'");
				$res_prodet = mysql_fetch_assoc($get_prodet);
				$get_creator_in_det = mysql_query("select * from general_user where community_id='".$res_prodet['community_id']."'");
			}
			if(mysql_num_rows($get_creator_in_det)>0)
			{
				$res_creator_in_det = mysql_fetch_assoc($get_creator_in_det);
				if($res_creator_in_det['artist_id']>0)
				{
					$get_tag_art_det = mysql_query("select * from general_artist where artist_id='".$res_creator_in_det['artist_id']."'");
					if(mysql_num_rows($get_tag_art_det)>0)
					{
						$res_tag_art_det = mysql_fetch_assoc($get_tag_art_det);
						$exp_tag_song = explode(",",$res_tag_art_det['taggedsongs']);
						$exp_tag_video = explode(",",$res_tag_art_det['taggedvideos']);
						$exp_tag_gallery = explode(",",$res_tag_art_det['taggedgalleries']);
						if($get_all_mediaid!="")
						{
							$exp_all_mediaid = explode(",",$get_all_mediaid);
							for($count_media=0;$count_media<=count($exp_all_mediaid);$count_media++)
							{
								if($exp_all_mediaid[$count_media]==""){continue;}
								else{
									for($count_tag=0;$count_tag<=count($exp_tag_song);$count_tag++)
									{
										if($exp_tag_song[$count_tag]==""){continue;}
										else{
											if($exp_tag_song[$count_tag] == $exp_all_mediaid[$count_media])
											{
												$exp_tag_song[$count_tag]="";
											}
										}
									}
								}
							}
							for($count_media=0;$count_media<=count($exp_all_mediaid);$count_media++)
							{
								if($exp_all_mediaid[$count_media]==""){continue;}
								else{
									for($count_tag_video=0;$count_tag_video<=count($exp_tag_video);$count_tag_video++)
									{
										if($exp_tag_video[$count_tag_video]==""){continue;}
										else{
											if($exp_tag_video[$count_tag_video] == $exp_all_mediaid[$count_media])
											{
												$exp_tag_video[$count_tag_video]="";
											}
										}
									}
								}
							}
							for($count_media=0;$count_media<=count($exp_all_mediaid);$count_media++)
							{
								if($exp_all_mediaid[$count_media]==""){continue;}
								else{
									for($count_tag_gal=0;$count_tag_gal<=count($exp_tag_gallery);$count_tag_gal++)
									{
										if($exp_tag_gallery[$count_tag_gal]==""){continue;}
										else{
											if($exp_tag_gallery[$count_tag_gal] == $exp_all_mediaid[$count_media])
											{
												$exp_tag_gallery[$count_tag_gal]="";
											}
										}
									}
								}
							}
							$new_song_id="";
							for($count_song_id=0;$count_song_id<=count($exp_tag_song);$count_song_id++)
							{
								if($exp_tag_song[$count_song_id]!=""){
								$new_song_id = $new_song_id .",". $exp_tag_song[$count_song_id];}
							}
							$new_video_id="";
							for($count_video_id=0;$count_video_id<=count($exp_tag_video);$count_video_id++)
							{
								if($exp_tag_video[$count_video_id]!=""){
								$new_video_id = $new_video_id .",". $exp_tag_video[$count_video_id];}
							}
							$new_gallery_id="";
							for($count_gallery_id=0;$count_gallery_id<=count($exp_tag_gallery);$count_gallery_id++)
							{
								if($exp_tag_gallery[$count_gallery_id]!=""){
								$new_gallery_id = $new_gallery_id .",". $exp_tag_gallery[$count_gallery_id];}
							}
							mysql_query("update general_artist set taggedsongs='".$new_song_id."', taggedvideos='".$new_video_id."', taggedgalleries='".$new_gallery_id."' where artist_id='".$res_tag_art_det['artist_id']."'");
						}
					}
					$get_tag_artpro_det = mysql_query("select * from artist_project where artist_id='".$res_creator_in_det['artist_id']."'");
					if(mysql_num_rows($get_tag_artpro_det)>0)
					{
						while($res_tag_artpro_det = mysql_fetch_assoc($get_tag_artpro_det))
						{
							$exp_tag_song = explode(",",$res_tag_artpro_det['tagged_songs']);
							$exp_tag_video = explode(",",$res_tag_artpro_det['tagged_videos']);
							$exp_tag_gallery = explode(",",$res_tag_artpro_det['tagged_galleries']);
							if($get_all_mediaid!="")
							{
								$exp_all_mediaid = explode(",",$get_all_mediaid);
								for($count_media=0;$count_media<=count($exp_all_mediaid);$count_media++)
								{
									if($exp_all_mediaid[$count_media]==""){continue;}
									else{
										for($count_tag=0;$count_tag<=count($exp_tag_song);$count_tag++)
										{
											if($exp_tag_song[$count_tag]==""){continue;}
											else{
												if($exp_tag_song[$count_tag] == $exp_all_mediaid[$count_media])
												{
													$exp_tag_song[$count_tag]="";
												}
											}
										}
									}
								}
								for($count_media=0;$count_media<=count($exp_all_mediaid);$count_media++)
								{
									if($exp_all_mediaid[$count_media]==""){continue;}
									else{
										for($count_tag_video=0;$count_tag_video<=count($exp_tag_video);$count_tag_video++)
										{
											if($exp_tag_video[$count_tag_video]==""){continue;}
											else{
												if($exp_tag_video[$count_tag_video] == $exp_all_mediaid[$count_media])
												{
													$exp_tag_video[$count_tag_video]="";
												}
											}
										}
									}
								}
								for($count_media=0;$count_media<=count($exp_all_mediaid);$count_media++)
								{
									if($exp_all_mediaid[$count_media]==""){continue;}
									else{
										for($count_tag_gal=0;$count_tag_gal<=count($exp_tag_gallery);$count_tag_gal++)
										{
											if($exp_tag_gallery[$count_tag_gal]==""){continue;}
											else{
												if($exp_tag_gallery[$count_tag_gal] == $exp_all_mediaid[$count_media])
												{
													$exp_tag_gallery[$count_tag_gal]="";
												}
											}
										}
									}
								}
								$new_song_id="";
								for($count_song_id=0;$count_song_id<=count($exp_tag_song);$count_song_id++)
								{
									if($exp_tag_song[$count_song_id]!=""){
									$new_song_id = $new_song_id .",". $exp_tag_song[$count_song_id];}
								}
								$new_video_id="";
								for($count_video_id=0;$count_video_id<=count($exp_tag_video);$count_video_id++)
								{
									if($exp_tag_video[$count_video_id]!=""){
									$new_video_id = $new_video_id .",". $exp_tag_video[$count_video_id];}
								}
								$new_gallery_id="";
								for($count_gallery_id=0;$count_gallery_id<=count($exp_tag_gallery);$count_gallery_id++)
								{
									if($exp_tag_gallery[$count_gallery_id]!=""){
									$new_gallery_id = $new_gallery_id .",". $exp_tag_gallery[$count_gallery_id];}
								}
								mysql_query("update artist_project set tagged_songs='".$new_song_id."', tagged_videos='".$new_video_id."', tagged_galleries='".$new_gallery_id."' where id='".$res_tag_artpro_det['id']."'");
							}
						}
					}
					$get_tag_arteve_det = mysql_query("select * from artist_event where artist_id='".$res_creator_in_det['artist_id']."'");
					if(mysql_num_rows($get_tag_arteve_det)>0)
					{
						while($res_tag_arteve_det = mysql_fetch_assoc($get_tag_arteve_det))
						{
							$exp_tag_song = explode(",",$res_tag_arteve_det['tagged_songs']);
							$exp_tag_video = explode(",",$res_tag_arteve_det['tagged_videos']);
							$exp_tag_gallery = explode(",",$res_tag_arteve_det['tagged_galleries']);
							if($get_all_mediaid!="")
							{
								$exp_all_mediaid = explode(",",$get_all_mediaid);
								for($count_media=0;$count_media<=count($exp_all_mediaid);$count_media++)
								{
									if($exp_all_mediaid[$count_media]==""){continue;}
									else{
										for($count_tag=0;$count_tag<=count($exp_tag_song);$count_tag++)
										{
											if($exp_tag_song[$count_tag]==""){continue;}
											else{
												if($exp_tag_song[$count_tag] == $exp_all_mediaid[$count_media])
												{
													$exp_tag_song[$count_tag]="";
												}
											}
										}
									}
								}
								for($count_media=0;$count_media<=count($exp_all_mediaid);$count_media++)
								{
									if($exp_all_mediaid[$count_media]==""){continue;}
									else{
										for($count_tag_video=0;$count_tag_video<=count($exp_tag_video);$count_tag_video++)
										{
											if($exp_tag_video[$count_tag_video]==""){continue;}
											else{
												if($exp_tag_video[$count_tag_video] == $exp_all_mediaid[$count_media])
												{
													$exp_tag_video[$count_tag_video]="";
												}
											}
										}
									}
								}
								for($count_media=0;$count_media<=count($exp_all_mediaid);$count_media++)
								{
									if($exp_all_mediaid[$count_media]==""){continue;}
									else{
										for($count_tag_gal=0;$count_tag_gal<=count($exp_tag_gallery);$count_tag_gal++)
										{
											if($exp_tag_gallery[$count_tag_gal]==""){continue;}
											else{
												if($exp_tag_gallery[$count_tag_gal] == $exp_all_mediaid[$count_media])
												{
													$exp_tag_gallery[$count_tag_gal]="";
												}
											}
										}
									}
								}
								$new_song_id="";
								for($count_song_id=0;$count_song_id<=count($exp_tag_song);$count_song_id++)
								{
									if($exp_tag_song[$count_song_id]!=""){
									$new_song_id = $new_song_id .",". $exp_tag_song[$count_song_id];}
								}
								$new_video_id="";
								for($count_video_id=0;$count_video_id<=count($exp_tag_video);$count_video_id++)
								{
									if($exp_tag_video[$count_video_id]!=""){
									$new_video_id = $new_video_id .",". $exp_tag_video[$count_video_id];}
								}
								$new_gallery_id="";
								for($count_gallery_id=0;$count_gallery_id<=count($exp_tag_gallery);$count_gallery_id++)
								{
									if($exp_tag_gallery[$count_gallery_id]!=""){
									$new_gallery_id = $new_gallery_id .",". $exp_tag_gallery[$count_gallery_id];}
								}
								mysql_query("update artist_event set tagged_songs='".$new_song_id."', tagged_videos='".$new_video_id."', tagged_galleries='".$new_gallery_id."' where id='".$res_tag_arteve_det['id']."'");
							}
						}
					}
				}
				
				if($res_tag_det['community_id']>0)
				{
					$get_tag_com_det = mysql_query("select * from general_community where community_id='".$res_creator_in_det['community_id']."'");
					if(mysql_num_rows($get_tag_com_det)>0)
					{
						$res_tag_com_det = mysql_fetch_assoc($get_tag_com_det);
						$exp_tag_song = explode(",",$res_tag_com_det['taggedsongs']);
						$exp_tag_video = explode(",",$res_tag_com_det['taggedvideos']);
						$exp_tag_gallery = explode(",",$res_tag_com_det['taggedgalleries']);
						if($get_all_mediaid!="")
						{
							$exp_all_mediaid = explode(",",$get_all_mediaid);
							for($count_media=0;$count_media<=count($exp_all_mediaid);$count_media++)
							{
								if($exp_all_mediaid[$count_media]==""){continue;}
								else{
									for($count_tag=0;$count_tag<=count($exp_tag_song);$count_tag++)
									{
										if($exp_tag_song[$count_tag]==""){continue;}
										else{
											if($exp_tag_song[$count_tag] == $exp_all_mediaid[$count_media])
											{
												$exp_tag_song[$count_tag]="";
											}
										}
									}
								}
							}
							for($count_media=0;$count_media<=count($exp_all_mediaid);$count_media++)
							{
								if($exp_all_mediaid[$count_media]==""){continue;}
								else{
									for($count_tag_video=0;$count_tag_video<=count($exp_tag_video);$count_tag_video++)
									{
										if($exp_tag_video[$count_tag_video]==""){continue;}
										else{
											if($exp_tag_video[$count_tag_video] == $exp_all_mediaid[$count_media])
											{
												$exp_tag_video[$count_tag_video]="";
											}
										}
									}
								}
							}
							for($count_media=0;$count_media<=count($exp_all_mediaid);$count_media++)
							{
								if($exp_all_mediaid[$count_media]==""){continue;}
								else{
									for($count_tag_gal=0;$count_tag_gal<=count($exp_tag_gallery);$count_tag_gal++)
									{
										if($exp_tag_gallery[$count_tag_gal]==""){continue;}
										else{
											if($exp_tag_gallery[$count_tag_gal] == $exp_all_mediaid[$count_media])
											{
												$exp_tag_gallery[$count_tag_gal]="";
											}
										}
									}
								}
							}
							$new_song_id="";
							for($count_song_id=0;$count_song_id<=count($exp_tag_song);$count_song_id++)
							{
								if($exp_tag_song[$count_song_id]!=""){
								$new_song_id = $new_song_id .",". $exp_tag_song[$count_song_id];}
							}
							$new_video_id="";
							for($count_video_id=0;$count_video_id<=count($exp_tag_video);$count_video_id++)
							{
								if($exp_tag_video[$count_video_id]!=""){
								$new_video_id = $new_video_id .",". $exp_tag_video[$count_video_id];}
							}
							$new_gallery_id="";
							for($count_gallery_id=0;$count_gallery_id<=count($exp_tag_gallery);$count_gallery_id++)
							{
								if($exp_tag_gallery[$count_gallery_id]!=""){
								$new_gallery_id = $new_gallery_id .",". $exp_tag_gallery[$count_gallery_id];}
							}
							mysql_query("update general_community set taggedsongs='".$new_song_id."', taggedvideos='".$new_video_id."', taggedgalleries='".$new_gallery_id."' where community_id='".$res_tag_com_det['community_id']."'");
						}
					}
					$get_tag_compro_det = mysql_query("select * from community_project where community_id='".$res_creator_in_det['community_id']."'");
					if(mysql_num_rows($get_tag_compro_det)>0)
					{
						while($res_tag_compro_det = mysql_fetch_assoc($get_tag_compro_det))
						{
							$exp_tag_song = explode(",",$res_tag_compro_det['tagged_songs']);
							$exp_tag_video = explode(",",$res_tag_compro_det['tagged_videos']);
							$exp_tag_gallery = explode(",",$res_tag_compro_det['tagged_galleries']);
							if($get_all_mediaid!="")
							{
								$exp_all_mediaid = explode(",",$get_all_mediaid);
								for($count_media=0;$count_media<=count($exp_all_mediaid);$count_media++)
								{
									if($exp_all_mediaid[$count_media]==""){continue;}
									else{
										for($count_tag=0;$count_tag<=count($exp_tag_song);$count_tag++)
										{
											if($exp_tag_song[$count_tag]==""){continue;}
											else{
												if($exp_tag_song[$count_tag] == $exp_all_mediaid[$count_media])
												{
													$exp_tag_song[$count_tag]="";
												}
											}
										}
									}
								}
								for($count_media=0;$count_media<=count($exp_all_mediaid);$count_media++)
								{
									if($exp_all_mediaid[$count_media]==""){continue;}
									else{
										for($count_tag_video=0;$count_tag_video<=count($exp_tag_video);$count_tag_video++)
										{
											if($exp_tag_video[$count_tag_video]==""){continue;}
											else{
												if($exp_tag_video[$count_tag_video] == $exp_all_mediaid[$count_media])
												{
													$exp_tag_video[$count_tag_video]="";
												}
											}
										}
									}
								}
								for($count_media=0;$count_media<=count($exp_all_mediaid);$count_media++)
								{
									if($exp_all_mediaid[$count_media]==""){continue;}
									else{
										for($count_tag_gal=0;$count_tag_gal<=count($exp_tag_gallery);$count_tag_gal++)
										{
											if($exp_tag_gallery[$count_tag_gal]==""){continue;}
											else{
												if($exp_tag_gallery[$count_tag_gal] == $exp_all_mediaid[$count_media])
												{
													$exp_tag_gallery[$count_tag_gal]="";
												}
											}
										}
									}
								}
								$new_song_id="";
								for($count_song_id=0;$count_song_id<=count($exp_tag_song);$count_song_id++)
								{
									if($exp_tag_song[$count_song_id]!=""){
									$new_song_id = $new_song_id .",". $exp_tag_song[$count_song_id];}
								}
								$new_video_id="";
								for($count_video_id=0;$count_video_id<=count($exp_tag_video);$count_video_id++)
								{
									if($exp_tag_video[$count_video_id]!=""){
									$new_video_id = $new_video_id .",". $exp_tag_video[$count_video_id];}
								}
								$new_gallery_id="";
								for($count_gallery_id=0;$count_gallery_id<=count($exp_tag_gallery);$count_gallery_id++)
								{
									if($exp_tag_gallery[$count_gallery_id]!=""){
									$new_gallery_id = $new_gallery_id .",". $exp_tag_gallery[$count_gallery_id];}
								}
								mysql_query("update community_project set tagged_songs='".$new_song_id."', tagged_videos='".$new_video_id."', tagged_galleries='".$new_gallery_id."' where id='".$res_tag_compro_det['id']."'");
							}
						}
					}
					$get_tag_comeve_det = mysql_query("select * from community_event where community_id='".$res_creator_in_det['community_id']."'");
					if(mysql_num_rows($get_tag_comeve_det)>0)
					{
						while($res_tag_comeve_det = mysql_fetch_assoc($get_tag_comeve_det))
						{
							$exp_tag_song = explode(",",$res_tag_comeve_det['tagged_songs']);
							$exp_tag_video = explode(",",$res_tag_comeve_det['tagged_videos']);
							$exp_tag_gallery = explode(",",$res_tag_comeve_det['tagged_galleries']);
							if($get_all_mediaid!="")
							{
								$exp_all_mediaid = explode(",",$get_all_mediaid);
								for($count_media=0;$count_media<=count($exp_all_mediaid);$count_media++)
								{
									if($exp_all_mediaid[$count_media]==""){continue;}
									else{
										for($count_tag=0;$count_tag<=count($exp_tag_song);$count_tag++)
										{
											if($exp_tag_song[$count_tag]==""){continue;}
											else{
												if($exp_tag_song[$count_tag] == $exp_all_mediaid[$count_media])
												{
													$exp_tag_song[$count_tag]="";
												}
											}
										}
									}
								}
								for($count_media=0;$count_media<=count($exp_all_mediaid);$count_media++)
								{
									if($exp_all_mediaid[$count_media]==""){continue;}
									else{
										for($count_tag_video=0;$count_tag_video<=count($exp_tag_video);$count_tag_video++)
										{
											if($exp_tag_video[$count_tag_video]==""){continue;}
											else{
												if($exp_tag_video[$count_tag_video] == $exp_all_mediaid[$count_media])
												{
													$exp_tag_video[$count_tag_video]="";
												}
											}
										}
									}
								}
								for($count_media=0;$count_media<=count($exp_all_mediaid);$count_media++)
								{
									if($exp_all_mediaid[$count_media]==""){continue;}
									else{
										for($count_tag_gal=0;$count_tag_gal<=count($exp_tag_gallery);$count_tag_gal++)
										{
											if($exp_tag_gallery[$count_tag_gal]==""){continue;}
											else{
												if($exp_tag_gallery[$count_tag_gal] == $exp_all_mediaid[$count_media])
												{
													$exp_tag_gallery[$count_tag_gal]="";
												}
											}
										}
									}
								}
								$new_song_id="";
								for($count_song_id=0;$count_song_id<=count($exp_tag_song);$count_song_id++)
								{
									if($exp_tag_song[$count_song_id]!=""){
									$new_song_id = $new_song_id .",". $exp_tag_song[$count_song_id];}
								}
								$new_video_id="";
								for($count_video_id=0;$count_video_id<=count($exp_tag_video);$count_video_id++)
								{
									if($exp_tag_video[$count_video_id]!=""){
									$new_video_id = $new_video_id .",". $exp_tag_video[$count_video_id];}
								}
								$new_gallery_id="";
								for($count_gallery_id=0;$count_gallery_id<=count($exp_tag_gallery);$count_gallery_id++)
								{
									if($exp_tag_gallery[$count_gallery_id]!=""){
									$new_gallery_id = $new_gallery_id .",". $exp_tag_gallery[$count_gallery_id];}
								}
								mysql_query("update community_event set tagged_songs='".$new_song_id."', tagged_videos='".$new_video_id."', tagged_galleries='".$new_gallery_id."' where id='".$res_tag_comeve_det['id']."'");
							}
						}
					}
				}
			}
		}
		$query = mysql_query("select * from general_media where creator_info = '".$creator_info."' OR from_info = '".$creator_info."'");
		if(mysql_num_rows($query)>0)
		{
			while($res_query = mysql_fetch_assoc($query))
			{
				if($res_query['creator_info']==$creator_info){
					mysql_query("update general_media set creator='', creator_info='' where id='".$res_query['id']."'");
				}if($res_query['from_info']==$creator_info){
					mysql_query("update general_media set from='', from_info='' where id='".$res_query['id']."'");
				}
			}
		}
	}
	/*********Functions For Tagged Channel Video Song Gallery*******/
	
	function get_artist_channel_id()
	{
		$new_id=$this->selgeneral();
		$sql5=mysql_query("select * from general_media where general_user_id='".$new_id['general_user_id']."' and media_type=116 AND delete_status=0");
		return($sql5);
	}
	function get_artist_channel($id)
	{
		$sql6=mysql_query("select * from media_channel where media_id='".$id."' ");
		return($sql6);
	}
	function get_artist_video_id()
	{
		$new_id=$this->selgeneral();
		$sql7=mysql_query("select * from general_media where general_user_id='".$new_id['general_user_id']."' and media_type=115 AND delete_status=0");
		return($sql7);
	}
	function get_artist_video($id)
	{
		$sql8=mysql_query("select * from media_video where media_id='".$id."' ");
		return($sql8);
	}
	function get_artist_song_id()
	{
		$new_id=$this->selgeneral();
		$sql9=mysql_query("select * from general_media where general_user_id='".$new_id['general_user_id']."' and media_type=114 AND delete_status=0");
		return($sql9);
	}
	function get_artist_song($id)
	{
		$sql10=mysql_query("select * from media_songs where media_id='".$id."' ");
		return($sql10);
	}
	function get_artist_gallery_id()
	{
		$new_id=$this->selgeneral();
		$gallery=mysql_query("select * from general_media where general_user_id='".$new_id['general_user_id']."' and media_type=113 AND delete_status=0");
		return($gallery);
	}
	
	function get_artist_song_at_register()
	{
		$new_id=$this->selgeneral();
		$sql11=mysql_query("select * from general_artist_audio where profile_id='".$new_id['artist_id']."'");
		return($sql11);
	}
	function get_artist_video_at_register()
	{
		$new_id=$this->selgeneral();
		$sql12=mysql_query("select * from general_artist_video where profile_id='".$new_id['artist_id']."'");
		return($sql12);
	}
	function get_artist_gallery_at_register()
	{
		$new_id=$this->selgeneral();
		$sql14=mysql_query("select * from general_artist_gallery where profile_id='".$new_id['artist_id']."'");
		$sql13 = mysql_fetch_assoc($sql14);
		return($sql13);
	}
	function get_artist_gallery_name_at_register()
	{
		$new_id=$this->get_artist_gallery_at_register();
		$sql13=mysql_query("select * from general_artist_gallery_list where gallery_id='".$new_id['gallery_id']."'");
		return($sql13);
	}

	/*********Functions For Tagged Channel Video Song Gallery Ends Here*******/
	
	function update_general_user_project($email,$id)
	{
		$res=$this->selgeneral();
		$sel_chan=mysql_query("select taggedartistprojects from general_user where email='$email'");
		$sel_res=mysql_fetch_assoc($sel_chan);
		$exp_res=explode(',',$sel_res['taggedartistprojects']);
		$match = 0;
		for($i=0;$i<count($exp_res);$i++)
		{
			if($exp_res[$i] ==""){continue;}
			else{
				if($exp_res[$i] !=$id){
					$new_tag = $new_tag .','.$exp_res[$i];
				}
				else{
				$match = 1;
				}
			}
			
			/*if()
			{
				$sel_res1=$sel_res['taggedartistprojects'];
				$sql2="update general_user set taggedartistprojects='$sel_res1' where email='$email'";
				$res2=mysql_query($sql2);
				continue;
			}
			if($id!=$exp_res[$i])
			{
				$sel_res1=$sel_res['taggedartistprojects'].','.$id;
				$sql2="update general_user set taggedartistprojects='$sel_res1' where email='$email'";
				$res2=mysql_query($sql2);
				continue;
			}*/
		}
		if($match == 0)
		{
			$new_tag = $new_tag .','. $id;
			mysql_query("update general_user set taggedartistprojects='".$new_tag."' where email='".$email."'");
		}
		//mysql_query("update general_user set taggedartistprojects='".$new_tag."' where email='".$email."'");
	}
	function update_general_user_project_remove($email,$id)
	{
		$res=$this->selgeneral();
		$sel_chan=mysql_query("select * from general_user where email='$email'");
		$sel_res=mysql_fetch_assoc($sel_chan);
		$exp_res=explode(',',$sel_res['taggedartistprojects']);
		for($i=0;$i<count($exp_res);$i++)
		{
			if($exp_res[$i]==""){continue;}
			else{
				if($exp_res[$i] != $id){
					$new_array = $new_array .','. $exp_res[$i];
				}
			}
		}
		//var_dump($new_array);
		mysql_query("update general_user set taggedartistprojects ='".$new_array."' where email='".$email."'");
		
		
		/*Code For General_artist*/
		$get_art_data = mysql_query("select * from general_artist where artist_id = '".$sel_res['artist_id']."'");
		if(mysql_num_rows($get_art_data)>0)
		{
			$res_art_data = mysql_fetch_assoc($get_art_data);
			$exp_video_id = explode(",",$res_art_data['taggedprojects']);
			for($count1=0;$count1<count($exp_video_id);$count1++)
			{
				if($exp_video_id[$count1]==""){continue;}
				else{
					if($exp_video_id[$count1] != $id){
						$art_video1 = $art_video1 .",".$exp_video_id[$count1];
					}
				}
			}
			mysql_query("update general_artist set taggedprojects ='".$art_video1."' where artist_id = '".$sel_res['artist_id']."'");
		}
		/*Code For artist_event*/
		$get_art_data1 = mysql_query("select * from artist_event where artist_id = '".$sel_res['artist_id']."'");
		if(mysql_num_rows($get_art_data1)>0)
		{
			//$res_art_data = mysql_fetch_assoc($get_art_data1);
			while($res_art_data1 = mysql_fetch_assoc($get_art_data1))
			{
				$exp_video_id1 = explode(",",$res_art_data1['tagged_projects']);
				for($count4=0;$count4<count($exp_video_id1);$count4++)
				{
					if($exp_video_id1[$count4]==""){continue;}
					else{
						if($exp_video_id1[$count4] != $id){
							$art_video2 = $art_video2 .",".$exp_video_id1[$count4];
						}
					}
				}
				mysql_query("update artist_event set tagged_projects ='".$art_video2."' where id = '".$res_art_data1['id']."'");
			}
		}
		/*Code For artist_project*/
		$get_art_data2 = mysql_query("select * from artist_project where artist_id = '".$sel_res['artist_id']."'");
		if(mysql_num_rows($get_art_data2)>0)
		{
			//$res_art_data = mysql_fetch_assoc($get_art_data1);
			while($res_art_data2 = mysql_fetch_assoc($get_art_data2))
			{
				$exp_video_id2 = explode(",",$res_art_data2['tagged_projects']);
				for($count7=0;$count7<count($exp_video_id2);$count7++)
				{
					if($exp_video_id2[$count7]==""){continue;}
					else{
						if($exp_video_id2[$count7] != $id){
							$art_video3 = $art_video3 .",".$exp_video_id2[$count7];
						}
					}
				}
				mysql_query("update artist_project set tagged_projects ='".$art_video3."' where id = '".$res_art_data2['id']."'");
			}
		}
	}
	
	function getEventUpcoming()
	{
		$date=date("Y-m-d");
		$new_id=$this->selgeneral();
		$getproject=mysql_query("select * from artist_event where artist_id='".$new_id['artist_id']."' AND date>='".$date."' AND del_status=0 AND active=0");
		return ($getproject);
	}
	
	function getEventRecorded()
	{
		$date=date("Y-m-d");
		$new_id=$this->selgeneral();
		$getproject=mysql_query("select * from artist_event where artist_id='".$new_id['artist_id']."' AND date<'".$date."' AND del_status=0 AND active=0");
		return ($getproject);
	}
	
	function getComEventUpcoming()
	{
		$date=date("Y-m-d");
		$new_id=$this->selgeneral();
		$getproject=mysql_query("select * from community_event where community_id='".$new_id['community_id']."' AND date>='".$date."' AND del_status=0 AND active=0");
		return ($getproject);
	}
	
	function getComEventRecorded()
	{
		$date=date("Y-m-d");
		$new_id=$this->selgeneral();
		$getproject=mysql_query("select * from community_event where community_id='".$new_id['community_id']."' AND date<'".$date."' AND del_status=0 AND active=0");
		return ($getproject);
	}
	
	function get_event_tagged_by_other_user($id)
	{
		$date=date("Y-m-d");
		$sql14=mysql_query("select * from artist_event where id='".$id."' AND date>='".$date."' AND del_status=0 AND active=0");
		$res14=mysql_fetch_assoc($sql14);
		return($res14);
		//var_dump($res14);
	}
	
	function get_cevent_tagged_by_other_user($id)
	{
		$date=date("Y-m-d");
		$sql14=mysql_query("select * from community_event where id='".$id."' AND date>='".$date."' AND del_status=0 AND active=0");
		$res14=mysql_fetch_assoc($sql14);
		return($res14);
		//var_dump($res14);
	}
	
	function get_event_tagged_by_other_userrec($id)
	{
		$date=date("Y-m-d");
		$sql14=mysql_query("select * from artist_event where id='".$id."' AND date<'".$date."' AND del_status=0 AND active=0");
		$res14=mysql_fetch_assoc($sql14);
		return($res14);
		//var_dump($res14);
	}
	
	function get_cevent_tagged_by_other_userrec($id)
	{
		$date=date("Y-m-d");
		$sql14=mysql_query("select * from community_event where id='".$id."' AND date<'".$date."' AND del_status=0 AND active=0");
		$res14=mysql_fetch_assoc($sql14);
		return($res14);
		//var_dump($res14);
	}
	
	function selectUpcomingEvents()
	{
		$new_id=$this->selgeneral();
		$update_event = mysql_query("SELECT taggedupcomingevents FROM artist_event WHERE artist_id='".$new_id['artist_id']."'");
		return ($update_event);
	}
	
	/***********************Get member Id***************************/
	function Get_comm_member_Id()
	{
		$row=$this->selgeneral();
		$get_member=mysql_query("select * from community_membership where community_id='".$row['community_id']."'");
		$member_res=mysql_fetch_assoc($get_member);
		return($member_res);
	}
	function Get_artist_member_Id()
	{
		$row=$this->selgeneral();
		$get_art_member=mysql_query("select * from artist_membership where artist_id='".$row['artist_id']."'");
		$member_art_res=mysql_fetch_assoc($get_art_member);
		return($member_art_res);
	}
	/***********************Get member Id***************************/
	
	function Get_selected_tagg($id)
	{
		$sql1=mysql_query("select * from artist_project where id='".$id."' AND del_status=0 AND active=0");
		$newrow=mysql_fetch_assoc($sql1);
		return($newrow);
	}
	
	/********Functions for project*********/
	function get_all_projects()
	{
		$get_id=$this->selgeneral();
		$get_pro = mysql_query("SELECT * FROM artist_project WHERE artist_id='".$get_id['artist_id']."' AND del_status=0 AND active=0");
		return ($get_pro);
	}
	
	function get_all_cprojects()
	{
		$get_id=$this->selgeneral();
		$get_pro = mysql_query("SELECT * FROM community_project WHERE community_id='".$get_id['community_id']."' AND del_status=0 AND active=0");
		return ($get_pro);
	}
	/********Functions for project ends here*********/
	
	function get_all_Artists()
	{
		$sql_all_artist = "SELECT * FROM general_artist WHERE status=0 AND del_status=0 AND active=0";
		$run_all_artist = mysql_query($sql_all_artist);
		return ($run_all_artist);
	}
	
	function get_fanclub_media()
	{
		$get_gen = $this->selgeneral();
		$get_fan = mysql_query("select * from general_media where (sharing_preference = 3 OR sharing_preference = 4) AND general_user_id='".$get_gen['general_user_id']."' AND delete_status=0" );
		return($get_fan);
	}
	function get_forsale_media()
	{
		$get_gen = $this->selgeneral();
		$get_sale = mysql_query("select * from general_media where (sharing_preference = 5 OR sharing_preference = 2) AND general_user_id='".$get_gen['general_user_id']."' AND delete_status=0");
		return($get_sale);
	}
	function find_creator_or_not($id)
	{
		$find = mysql_query("select * from artist_project where id='".$id."' AND del_status=0");
		$res = mysql_fetch_assoc($find);
		$get_gen_info = mysql_query("select * from general_user where email='".$_SESSION['login_email']."' AND delete_status=0");
		$res_gen_info = mysql_fetch_assoc($get_gen_info);
		if($res_gen_info['artist_id'] == $res['artist_id'])
		{
			return ("creator");
		}
		else
		{
			return ("other");
		}
	}
	function Delete_tagged_Project($id)
	{
		$query = mysql_query("select * from artist_project where id ='".$id."' AND del_status=0");
		if(mysql_num_rows($query)>0)
		{
			$res = mysql_fetch_assoc($query);
			$sql="SELECT * FROM general_user GU LEFT JOIN state S ON GU.state_id=S.state_id WHERE GU.email='".$_SESSION['login_email']."' AND GU.delete_status=0";
			$ans = mysql_query($sql);
			$row = mysql_fetch_array($ans);
			$tag_user_name = str_replace("'","`",$row["fname"])." ".str_replace("'","`",$row["lname"])." (".str_replace("'","`",$row["city"]).".".str_replace("'","`",$row["state_name"]).")";
			$exp_tag_email = explode(",",$res['tagged_user_email']);
			$exp_tag_user_name = explode(",",$res['users_tagged']);
			for($exp_email_count = 0;$exp_email_count<count($exp_tag_email);$exp_email_count++)
			{
				 if($exp_tag_email[$exp_email_count]==""){continue;}
				 else{
					if($exp_tag_email[$exp_email_count]!=$_SESSION['login_email'])
					{
						$all_tag_email = $all_tag_email.",".$exp_tag_email[$exp_email_count];
					}
				 }
			}
			for($exp_user_count = 0;$exp_user_count<count($exp_tag_user_name);$exp_user_count++)
			{
				 if($exp_tag_user_name[$exp_user_count]==""){continue;}
				 else{
					if($exp_tag_user_name[$exp_user_count] != $tag_user_name)
					{
						$all_tag_user = $all_tag_user.",".$exp_tag_user_name[$exp_user_count];
					}
				 }
			}
			mysql_query("update artist_project set tagged_user_email ='".$all_tag_email."',users_tagged='".$all_tag_user."' where id ='".$id."'");
		}
	}
	function Get_purify_member()
	{
		$date= 	date('Y-m-d');
		$row=$this->selgeneral();
		$get_purify_member=mysql_query("select * from purify_membership where general_user_id='".$row['general_user_id']."' AND (expiry_date>'".$date."' OR lifetime=1)");
		$member_purify_res=mysql_fetch_assoc($get_purify_member);
		return($member_purify_res);
	}
	
	function get_project_create()
	{
		$id = $this->selgeneral();
		$var_type_1 = 'general_artist';
		$var_type_2 = 'general_community';
		$var_type_3 = 'artist_project';
		$var_type_4 = 'community_project';
		
		$artm = array();
		$arta = array();
		$comm = array();
		$coma = array();
		
		$art_p_ao = array();
		$com_p_ao = array();
		$art_p_co = array();
		$com_p_co = array();
		
		$artme = array();
		$artae = array();
		$comme = array();
		$comae = array();
		
		$art_e_ao = array();
		$com_e_ao = array();
		$art_e_co = array();
		$com_e_co = array();
		
		$pal_1 = array();
		$pal_2 = array();
		
		if($id['artist_id']!=0 && !empty($id['artist_id']))
		{
			$chk_type = $var_type_1.'|'.$id['artist_id'];
			
			$sql_get_allp = mysql_query("SELECT * FROM artist_project WHERE artist_id='".$id['artist_id']."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_get_allp)>0)
			{
				while($rowal = mysql_fetch_assoc($sql_get_allp))
				{
					$pal_1[] = $rowal;
				}
			}
			
			if(!empty($pal_1) && count($pal_1))
			{
				for($pl=0;$pl<count($pal_1);$pl++)
				{
					$chk_typep_1 = 'artist_project|'.$pal_1[$pl]['id'];
					$sql = mysql_query("SELECT * FROM artist_project WHERE creators_info='".$chk_typep_1."' AND del_status=0 AND active=0");
					if(mysql_num_rows($sql)>0)
					{
						while($row_po = mysql_fetch_assoc($sql))
						{
							if($row_po['artist_id']!=$id['artist_id'])
							{
								$art_p_ao[] = $row_po;
							}
						}
					}
					
					$sql = mysql_query("SELECT * FROM community_project WHERE creators_info='".$chk_typep_1."' AND del_status=0 AND active=0");
					if(mysql_num_rows($sql)>0)
					{
						while($row_poo = mysql_fetch_assoc($sql))
						{
							if($row_poo['community_id']!=$id['community_id'])
							{
								$com_p_ao[] = $row_poo;
							}
						}
					}
				}
			}
			
			if(!empty($pal_1) && count($pal_1))
			{
				for($pl=0;$pl<count($pal_1);$pl++)
				{
					$chk_typep_1 = 'artist_project|'.$pal_1[$pl]['id'];
					$sql = mysql_query("SELECT * FROM artist_event WHERE creators_info='".$chk_typep_1."' AND del_status=0 AND active=0");
					if(mysql_num_rows($sql)>0)
					{
						while($row_po = mysql_fetch_assoc($sql))
						{
							if($row_po['artist_id']!=$id['artist_id'])
							{
								$art_e_ao[] = $row_po;
							}
						}
					}
					
					$sql = mysql_query("SELECT * FROM community_event WHERE creators_info='".$chk_typep_1."' AND del_status=0 AND active=0");
					if(mysql_num_rows($sql)>0)
					{
						while($row_poo = mysql_fetch_assoc($sql))
						{
							if($row_poo['community_id']!=$id['community_id'])
							{
								$com_e_ao[] = $row_poo;
							}
						}
					}
				}
			}
			
			$sql_pro_arta = mysql_query("SELECT * FROM artist_project WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_pro_arta)>0)
			{
				while($rowaa = mysql_fetch_assoc($sql_pro_arta))
				{
					if($rowaa['artist_id']!=$id['artist_id'])
					{
						$arta[] = $rowaa;
					}
				}
			}
			
			$sql_pro_arta = mysql_query("SELECT * FROM artist_event WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_pro_arta)>0)
			{
				while($rowaa = mysql_fetch_assoc($sql_pro_arta))
				{
					if($rowaa['artist_id']!=$id['artist_id'])
					{
						$artae[] = $rowaa;
					}
				}
			}
			
			$sql_pro_coma = mysql_query("SELECT * FROM community_project WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_pro_coma)>0)
			{
				while($rowam = mysql_fetch_assoc($sql_pro_coma))
				{
					if($rowam['community_id']!=$id['community_id'])
					{
						$coma[] = $rowam;
					}
				}
			}
			
			$sql_pro_coma = mysql_query("SELECT * FROM community_event WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_pro_coma)>0)
			{
				while($rowam = mysql_fetch_assoc($sql_pro_coma))
				{
					if($rowam['community_id']!=$id['community_id'])
					{
						$comae[] = $rowam;
					}
				}
			}
		}
		
		if($id['community_id']!=0 && !empty($id['community_id']))
		{
			$chk_type = $var_type_2.'|'.$id['community_id'];
			$sql_pro_artm = mysql_query("SELECT * FROM artist_project WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_pro_artm)>0)
			{
				while($rowcm = mysql_fetch_assoc($sql_pro_artm))
				{
					if($rowcm['artist_id']!=$id['artist_id'])
					{
						$artm[] = $rowcm;
					}
				}
			}
			
			$chk_type = $var_type_2.'|'.$id['community_id'];
			$sql_pro_artm = mysql_query("SELECT * FROM artist_event WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_pro_artm)>0)
			{
				while($rowcm = mysql_fetch_assoc($sql_pro_artm))
				{
					if($rowcm['artist_id']!=$id['artist_id'])
					{
						$artme[] = $rowcm;
					}
				}
			}
			
			$sql_pro_comm = mysql_query("SELECT * FROM community_project WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_pro_comm)>0)
			{
				while($rowca = mysql_fetch_assoc($sql_pro_comm))
				{
					if($rowca['community_id']!=$id['community_id'])
					{
						$comm[] = $rowca;
					}
				}
			}
			
			$sql_pro_comm = mysql_query("SELECT * FROM community_event WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_pro_comm)>0)
			{
				while($rowca = mysql_fetch_assoc($sql_pro_comm))
				{
					if($rowca['community_id']!=$id['community_id'])
					{
						$comme[] = $rowca;
					}
				}
			}
			
			$sql_get_allp = mysql_query("SELECT * FROM community_project WHERE community_id='".$id['community_id']."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_get_allp)>0)
			{
				while($rowal = mysql_fetch_assoc($sql_get_allp))
				{
					$pal_2[] = $rowal;
				}
			}
			
			if(!empty($pal_2) && count($pal_2))
			{
				for($pl2=0;$pl2<count($pal_2);$pl2++)
				{
					$chk_typep_2 = 'community_project|'.$pal_2[$pl2]['id'];
					$sql = mysql_query("SELECT * FROM artist_project WHERE creators_info='".$chk_typep_2."' AND del_status=0 AND active=0");
					if(mysql_num_rows($sql)>0)
					{
						while($row_po = mysql_fetch_assoc($sql))
						{
							if($row_po['artist_id']!=$id['artist_id'])
							{
								$art_p_co[] = $row_po;
							}
						}
					}
					
					$sql = mysql_query("SELECT * FROM community_project WHERE creators_info='".$chk_typep_2."' AND del_status=0 AND active=0");
					if(mysql_num_rows($sql)>0)
					{
						while($row_poo = mysql_fetch_assoc($sql))
						{
							if($row_poo['community_id']!=$id['community_id'])
							{
								$com_p_co[] = $row_poo;
							}
						}
					}
				}
			}
			
			if(!empty($pal_2) && count($pal_2))
			{
				for($pl2=0;$pl2<count($pal_2);$pl2++)
				{
					$chk_typep_2 = 'community_project|'.$pal_2[$pl2]['id'];
					$sql = mysql_query("SELECT * FROM artist_event WHERE creators_info='".$chk_typep_2."' AND del_status=0 AND active=0");
					if(mysql_num_rows($sql)>0)
					{
						while($row_po = mysql_fetch_assoc($sql))
						{
							if($row_po['artist_id']!=$id['artist_id'])
							{
								$art_e_co[] = $row_po;
							}
						}
					}
					
					$sql = mysql_query("SELECT * FROM community_event WHERE creators_info='".$chk_typep_2."' AND del_status=0 AND active=0");
					if(mysql_num_rows($sql)>0)
					{
						while($row_poo = mysql_fetch_assoc($sql))
						{
							if($row_poo['community_id']!=$id['community_id'])
							{
								$com_e_co[] = $row_poo;
							}
						}
					}
				}
			}
		}
		
		$totals = array();
		$totals = array_merge($artm,$arta,$comm,$coma,$art_p_ao,$com_p_ao,$art_p_co,$com_p_co,$artme,$artae,$comme,$comae,$art_e_ao,$com_e_ao,$art_e_co,$com_e_co);
		return $totals;
	}
	
	/************Functions for displaying tagged media fro projects**********************/
	
	function get_artist_project_tag($email)
	{
		$query = mysql_query("SELECT * FROM artist_project WHERE del_status=0 AND active=0 AND tagged_user_email LIKE '%".$email."%'");
		return($query);
	}
	
	function get_community_project_tag($email)
	{
		$query = mysql_query("SELECT * FROM community_project WHERE del_status=0 AND active=0 AND tagged_user_email LIKE '%".$email."%'");
		return($query);
	}
	
	function get_artist_event_tag($email)
	{
		$query = mysql_query("SELECT * FROM artist_event WHERE del_status=0 AND active=0 AND tagged_user_email LIKE '%".$email."%'");
		return($query);
	}
	
	function get_community_event_tag($email)
	{
		$query = mysql_query("SELECT * FROM community_event WHERE del_status=0 AND active=0 AND tagged_user_email LIKE '%".$email."%'");
		return($query);
	}
	
	function get_tag_media_info($media_id)
	{
		$sql= mysql_query("SELECT general_media. * , type.name, type.type_id FROM general_media,type WHERE general_media.media_type = type.type_id AND general_media.id ='".$media_id."' AND general_media.delete_status=0");
		if(mysql_num_rows($sql)>0)
		{
			$row = mysql_fetch_assoc($sql);
			return($row);
		}
	}
	/************Functions for displaying tagged media fro projects**********************/
	
	function get_sel_songs($id)
	{
		$query = mysql_query("select * from general_community where media_id='".$id."'");
		$res = mysql_fetch_assoc($query);
		return($res);
	}
	
	function get_media_reg_info($med_id,$types)
	{
		if($types!='')
		{
			$typ_ss = explode('~',$types);
			$id = $this->selgeneral();
			$chk_reg_med = '';
			if($typ_ss[0]=='artist' && $typ_ss[1]!='')
			{
				$sql_audr = mysql_query("SELECT * FROM general_artist_audio WHERE audio_id='".$med_id."' AND profile_id='".$typ_ss[1]."'");
				$sql_vidr = mysql_query("SELECT * FROM general_artist_video WHERE video_id='".$med_id."' AND profile_id='".$typ_ss[1]."'");
				$sql_galr = mysql_query("SELECT * FROM general_artist_gallery WHERE gallery_id='".$med_id."' AND profile_id='".$typ_ss[1]."'");
				
				if(mysql_num_rows($sql_audr)>0)
				{
					$chk_reg_med = 'general_artist_audio';
				}
				elseif(mysql_num_rows($sql_vidr)>0)
				{
					$chk_reg_med = 'general_artist_video';
				}
				elseif(mysql_num_rows($sql_galr)>0)
				{
					$chk_reg_med = 'general_artist_gallery';
				}
				
				if($chk_reg_med!="")
				{
					return $chk_reg_med;
				}
			}
			elseif($typ_ss[0]=='community' && $typ_ss[1]!='')
			{
				$sql_audr = mysql_query("SELECT * FROM general_community_audio WHERE audio_id='".$med_id."' AND profile_id='".$typ_ss[1]."'");
				$sql_vidr = mysql_query("SELECT * FROM general_community_video WHERE video_id='".$med_id."' AND profile_id='".$typ_ss[1]."'");
				$sql_galr = mysql_query("SELECT * FROM general_community_gallery WHERE gallery_id='".$med_id."' AND profile_id='".$typ_ss[1]."'");
				
				if(mysql_num_rows($sql_audr)>0)
				{
					$chk_reg_med = 'general_community_audio';
				}
				elseif(mysql_num_rows($sql_vidr)>0)
				{
					$chk_reg_med = 'general_community_video';
				}
				elseif(mysql_num_rows($sql_galr)>0)
				{
					$chk_reg_med = 'general_community_gallery';
				}
				
				if($chk_reg_med!="")
				{
					return $chk_reg_med;
				}
			}
		}
	}
	
/*	function delete_tagged_user_media($email,$gall_id,$song_id,$video_id){
		$dummy_gals = array();
		$dummy_song = array();
		$dummy_video = array();
		$sql= mysql_query("select * from general_user where email='".$email."'");
		if(mysql_num_rows($sql)>0){
			$res_gen = mysql_fetch_assoc($sql);
			if($res_gen['artist_id']>0 || $res_gen['artist_id']!=""){
				$art_det = mysql_query("select * from general_artist where artist_id = '".$res_gen['artist_id']."'");
				if(mysql_num_rows($art_det)>0){
					$res_art = mysql_fetch_assoc($art_det);
					$get_gal = explode(",",$res_art['taggedgalleries']);
					for($count_gal=0;$count_gal<=count($get_gal);$count_gal++){
						if(in_array($get_gal[$count_gal],$dummy_gals))
						{
						}
						else
						{
							$dummy_gals[] = $get_gal[$count_gal];
						}
					}
					
					$newgal = explode(",",$gall_id);
					for($count_gal=0;$count_gal<=count($dummy_gals);$count_gal++){
						if($dummy_gals[$count_gal]!=""){
							for($new_count=0;$new_count<=count($newgal);$new_count++){
								if($newgal[$new_count]!=""){
									if($newgal[$new_count]==$get_gal[$count_gal]){
										$get_gal[$count_gal]="";
									}
								}
							}
						}
					}
					$all_gal="";
					for($new_countxyz=0;$new_countxyz<=count($get_gal);$new_countxyz++)
					{
						if($get_gal[$new_countxyz]!=""){
						$all_gal = $all_gal .",".$get_gal[$new_countxyz];
						}
					}
					mysql_query("update general_artist set taggedgalleries='".$all_gal."' where artist_id = '".$res_gen['artist_id']."'");

					$get_song = explode(",",$res_art['taggedsongs']);
					for($count_song=0;$count_song<=count($get_song);$count_song++){
						if(in_array($get_song[$count_song],$dummy_song))
						{
						}
						else
						{
							$dummy_song[] = $get_song[$count_song];
						}
					}
					$newsong = explode(",",$song_id);
					for($count_song=0;$count_song<=count($dummy_song);$count_song++){
						if($dummy_song[$count_song]!=""){
							for($new_count_song=0;$new_count_song<=count($newsong);$new_count_song++){
								if($newsong[$new_count_song]!=""){
									if($newsong[$new_count_song]==$get_song[$count_song]){
										$get_song[$count_song]="";
									}
								}
							}
						}
					}
					$all_song ="";
					for($new_countxyz=0;$new_countxyz<=count($get_song);$new_countxyz++)
					{
						if($get_song[$new_countxyz]!=""){
						$all_song = $all_song .",".$get_song[$new_countxyz];
						}
					}
					mysql_query("update general_artist set taggedsongs='".$all_song."' where artist_id = '".$res_gen['artist_id']."'");
					
					$get_video = explode(",",$res_art['taggedvideos']);
					for($count_vid=0;$count_vid<=count($get_video);$count_vid++){
						if(in_array($get_video[$count_vid],$dummy_video))
						{
						}
						else
						{
							$dummy_video[] = $get_video[$count_vid];
						}
					}
					$newvideo = explode(",",$video_id);
					for($count_vid=0;$count_vid<=count($dummy_video);$count_vid++){
						if($dummy_video[$count_vid]!=""){
							for($new_count_video=0;$new_count_video<=count($newvideo);$new_count_video++){
								if($newvideo[$new_count_video]!=""){
									if($newvideo[$new_count_video]==$get_video[$count_vid]){
										$get_video[$count_vid]="";
									}
								}
							}
						}
					}
					$all_videos ="";
					for($new_countxyz=0;$new_countxyz<=count($get_video);$new_countxyz++)
					{
						if($get_video[$new_countxyz]!=""){
						$all_videos = $all_videos .",".$get_video[$new_countxyz];
						}
					}
					mysql_query("update general_artist set taggedvideos='".$all_videos."' where artist_id = '".$res_gen['artist_id']."'");
					
					/***code for artist project***//*
					$art_pro = mysql_query("select * from artist_project where artist_id ='".$res_gen['artist_id']."'");
					if(mysql_num_rows($art_pro)>0){
						while($row_art_pro = mysql_fetch_assoc($art_pro)){
							$get_gal_artpro = explode(",",$row_art_pro['tagged_galleries']);
							$dummy_gals = array();
							for($count_gal=0;$count_gal<=count($get_gal_artpro);$count_gal++){
								if(in_array($get_gal_artpro[$count_gal],$dummy_gals))
								{
								}
								else
								{
									$dummy_gals[] = $get_gal_artpro[$count_gal];
								}
							}
							$newgal = explode(",",$gall_id);
							for($count_gal=0;$count_gal<=count($dummy_gals);$count_gal++){
								if($dummy_gals[$count_gal]!=""){
									for($new_count=0;$new_count<=count($newgal);$new_count++){
										if($newgal[$new_count]!=""){
											if($newgal[$new_count]==$get_gal_artpro[$count_gal]){
												$get_gal_artpro[$count_gal]="";
											}
										}
									}
								}
							}
							$all_gal ="";
							for($new_count_xyz=0;$new_count_xyz<=count($get_gal_artpro);$new_count_xyz++)
							{
								if($get_gal_artpro[$new_count_xyz]!=""){
								$all_gal = $all_gal .",".$get_gal_artpro[$new_count_xyz];
								}
							}
							mysql_query("update artist_project set tagged_galleries='".$all_gal."' where id = '".$row_art_pro['id']."'");
							
							$get_song_artpro = explode(",",$row_art_pro['tagged_songs']);
							$dummy_songs = array();
							for($count_song=0;$count_song<=count($get_song_artpro);$count_song++){
								if(in_array($get_song_artpro[$count_song],$dummy_songs))
								{
								}
								else
								{
									$dummy_songs[] = $get_song_artpro[$count_song];
								}
							}
							$newsong = explode(",",$song_id);
							for($count_song=0;$count_song<=count($dummy_songs);$count_song++){
								if($dummy_songs[$count_song]!=""){
									for($new_count_song=0;$new_count_song<=count($newsong);$new_count_song++){
										if($newsong[$new_count_song]!=""){
											if($newsong[$new_count_song]==$get_song_artpro[$count_song]){
												$get_song_artpro[$count_song]="";
											}
										}
									}
								}
							}
							$all_song ="";
							for($new_count_xyz=0;$new_count_xyz<=count($get_song_artpro);$new_count_xyz++)
							{
								if($get_song_artpro[$new_count_xyz]!=""){
								$all_song = $all_song .",".$get_song_artpro[$new_count_xyz];
								}
							}
							mysql_query("update artist_project set tagged_songs='".$all_song."' where id = '".$row_art_pro['id']."'");
							
							$get_video_artpro = explode(",",$row_art_pro['tagged_videos']);
							$dummy_video = array();
							for($count_vid=0;$count_vid<=count($get_video_artpro);$count_vid++){
								if(in_array($get_video_artpro[$count_vid],$dummy_video))
								{
								}
								else
								{
									$dummy_video[] = $get_video_artpro[$count_vid];
								}
							}
							$newvideo = explode(",",$video_id);
							for($count_vid=0;$count_vid<=count($dummy_video);$count_vid++){
								if($dummy_video[$count_vid]!=""){
									for($new_count_video=0;$new_count_video<=count($newvideo);$new_count_video++){
										if($newvideo[$new_count_video]!=""){
											if($newvideo[$new_count_video]==$get_video_artpro[$count_vid]){
												$get_video_artpro[$count_vid]="";
											}
										}
									}
								}
							}
							$all_videos="";
							for($new_count_xyz=0;$new_count_xyz<=count($get_video_artpro);$new_count_xyz++)
							{
								if($get_video_artpro[$new_count_xyz]!=""){
								$all_videos = $all_videos .",".$get_video_artpro[$new_count_xyz];
								}
							}
							mysql_query("update artist_project set tagged_videos='".$all_videos."' where id = '".$row_art_pro['id']."'");
						}
					}
					
					/***code for artist project ends here***/
					/***code for artist Event***//*
					$art_event = mysql_query("select * from artist_event where artist_id ='".$res_gen['artist_id']."'");
					if(mysql_num_rows($art_event)>0){
						while($row_art_eve = mysql_fetch_assoc($art_event)){
							$get_gal_arteve = explode(",",$row_art_eve['tagged_galleries']);
							$dummy_gal = array();
							for($count_gal=0;$count_gal<=count($get_gal_arteve);$count_gal++){
								if(in_array($get_gal_arteve[$count_gal],$dummy_gal))
								{
								}
								else
								{
									$dummy_gal[] = $get_gal_arteve[$count_gal];
								}
							}
							$newgal = explode(",",$gall_id);
							for($count_gal=0;$count_gal<=count($dummy_gal);$count_gal++){
								if($dummy_gal[$count_gal]!=""){
									for($new_count=0;$new_count<=count($newgal);$new_count++){
										if($newgal[$new_count]!=""){
											if($newgal[$new_count]==$get_gal_arteve[$count_gal]){
												$get_gal_arteve[$count_gal]="";
											}
										}
									}
								}
							}
							$all_gal ="";
							for($new_count_xyz=0;$new_count_xyz<=count($get_gal_arteve);$new_count_xyz++)
							{
								if($get_gal_arteve[$new_count_xyz]!=""){
								$all_gal = $all_gal .",".$get_gal_arteve[$new_count_xyz];
								}
							}
							mysql_query("update artist_event set tagged_galleries='".$all_gal."' where id = '".$row_art_eve['id']."'");
							
							$get_song_arteve = explode(",",$row_art_eve['tagged_songs']);
							$dummy_song = array();
							for($count_song=0;$count_song<=count($get_song_arteve);$count_song++){
								if(in_array($get_song_arteve[$count_song],$dummy_song))
								{
								}
								else
								{
									$dummy_song[] = $get_song_arteve[$count_song];
								}
							}
							$newsong = explode(",",$song_id);
							for($count_song=0;$count_song<=count($dummy_song);$count_song++){
								if($dummy_song[$count_song]!=""){
									for($new_count_song=0;$new_count_song<=count($newsong);$new_count_song++){
										if($newsong[$new_count_song]!=""){
											if($newsong[$new_count_song]==$get_song_arteve[$count_song]){
												$get_song_arteve[$count_song]="";
											}
										}
									}
								}
							}
							$all_song ="";
							for($new_count_xyz=0;$new_count_xyz<=count($get_song_arteve);$new_count_xyz++)
							{
								if($get_song_arteve[$new_count_xyz]!=""){
								$all_song = $all_song .",".$get_song_arteve[$new_count_xyz];
								}
							}
							mysql_query("update artist_event set tagged_songs='".$all_song."' where id = '".$row_art_eve['id']."'");
							
							$get_video_arteve = explode(",",$row_art_eve['tagged_videos']);
							$dummy_video = array();
							for($count_vid=0;$count_vid<=count($get_video_arteve);$count_vid++){
								if(in_array($get_video_arteve[$count_vid],$dummy_video))
								{
								}
								else
								{
									$dummy_video[] = $get_video_arteve[$count_vid];
								}
							}
							$newvideo = explode(",",$video_id);
							for($count_vid=0;$count_vid<=count($dummy_video);$count_vid++){
								if($dummy_video[$count_vid]!=""){
									for($new_count_video=0;$new_count_video<=count($newvideo);$new_count_video++){
										if($newvideo[$new_count_video]!=""){
											if($newvideo[$new_count_video]==$get_video_arteve[$count_vid]){
												$get_video_arteve[$count_vid]="";
											}
										}
									}
								}
							}
							$all_videos ="";
							for($new_count_xyz=0;$new_count_xyz<=count($get_video_arteve);$new_count_xyz++)
							{
								if($get_video_arteve[$new_count_xyz]!=""){
								$all_videos = $all_videos .",".$get_video_arteve[$new_count_xyz];
								}
							}
							mysql_query("update artist_event set tagged_videos='".$all_videos."' where id = '".$row_art_eve['id']."'");
						}
					}
					
					/***code for artist Event ends here***//*
				}
			}
			/***Code for community***//*
			if($res_gen['community_id']>0 || $res_gen['community_id']!=""){
				$com_det = mysql_query("select * from general_community where community_id = '".$res_gen['community_id']."'");
				if(mysql_num_rows($com_det)>0){
					$res_com = mysql_fetch_assoc($com_det);
					$get_com_gal = explode(",",$res_com['taggedgalleries']);
					$dummy_gal = array();
					for($count_gal=0;$count_gal<=count($get_com_gal);$count_gal++){
						if(in_array($get_com_gal[$count_gal],$dummy_gal))
						{
						}
						else
						{
							$dummy_gal[] = $get_com_gal[$count_gal];
						}
					}
					$newgal = explode(",",$gall_id);
					for($count_gal=0;$count_gal<=count($dummy_gal);$count_gal++){
						if($dummy_gal[$count_gal]!=""){
							for($new_count=0;$new_count<=count($newgal);$new_count++){
								if($newgal[$new_count]!=""){
									if($newgal[$new_count]==$get_com_gal[$count_gal]){
										$get_com_gal[$count_gal]="";
									}
								}
							}
						}
					}
					$all_gal ="";
					for($new_count_xyz=0;$new_count_xyz<=count($get_com_gal);$new_count_xyz++)
					{
						if($get_com_gal[$new_count_xyz]!=""){
						$all_gal = $all_gal .",".$get_com_gal[$new_count_xyz];
						}
					}
					mysql_query("update general_community set taggedgalleries='".$all_gal."' where community_id = '".$res_gen['community_id']."'");
					
					$get_com_song = explode(",",$res_com['taggedsongs']);
					$dummy_song = array();
					for($count_song=0;$count_song<=count($get_com_song);$count_song++){
						if(in_array($get_com_song[$count_song],$dummy_song))
						{
						}
						else
						{
							$dummy_song[] = $get_com_song[$count_song];
						}
					}
					$newsong = explode(",",$song_id);
					for($count_song=0;$count_song<=count($dummy_song);$count_song++){
						if($dummy_song[$count_song]!=""){
							for($new_count_song=0;$new_count_song<=count($newsong);$new_count_song++){
								if($newsong[$new_count_song]!=""){
									if($newsong[$new_count_song]==$get_com_song[$count_song]){
										$get_com_song[$count_song]="";
									}
								}
							}
						}
					}
					$all_song="";
					for($new_count_xyz=0;$new_count_xyz<=count($get_com_song);$new_count_xyz++)
					{
						if($get_com_song[$new_count_xyz]!=""){
						$all_song = $all_song .",".$get_com_song[$new_count_xyz];
						}
					}
					mysql_query("update general_community set taggedsongs='".$all_song."' where community_id = '".$res_gen['community_id']."'");
					
					$get_com_video = explode(",",$res_com['taggedvideos']);
					$dummy_video = array();
					for($count_vid=0;$count_vid<=count($get_com_video);$count_vid++){
						if(in_array($get_com_video[$count_vid],$dummy_video))
						{
						}
						else
						{
							$dummy_video[] = $get_com_video[$count_vid];
						}
					}
					$newvideo = explode(",",$video_id);
					for($count_vid=0;$count_vid<=count($dummy_video);$count_vid++){
						if($dummy_video[$count_vid]!=""){
							for($new_count_video=0;$new_count_video<=count($newvideo);$new_count_video++){
								if($newvideo[$new_count_video]!=""){
									if($newvideo[$new_count_video]==$get_com_video[$count_vid]){
										$get_com_video[$count_vid]="";
									}
								}
							}
						}
					}
					$all_videos="";
					for($new_count_xyz=0;$new_count_xyz<=count($get_com_video);$new_count_xyz++)
					{
						if($get_com_video[$new_count_xyz]!=""){
						$all_videos = $all_videos .",".$get_com_video[$new_count_xyz];
						}
					}
					mysql_query("update general_community set taggedvideos='".$all_videos."' where community_id = '".$res_gen['community_id']."'");
					
					/***code for community project***//*
					$com_pro = mysql_query("select * from community_project where community_id ='".$res_gen['community_id']."'");
					if(mysql_num_rows($com_pro)>0){
						while($row_com_pro = mysql_fetch_assoc($com_pro)){
							$get_gal_compro = explode(",",$row_com_pro['tagged_galleries']);
							$dummy_gal = array();
							for($count_gal=0;$count_gal<=count($get_gal_compro);$count_gal++){
								if(in_array($get_gal_compro[$count_gal],$dummy_gal))
								{
								}
								else
								{
									$dummy_gal[] = $get_gal_compro[$count_gal];
								}
							}
							$newgal = explode(",",$gall_id);
							for($count_gal=0;$count_gal<=count($dummy_gal);$count_gal++){
								if($dummy_gal[$count_gal]!=""){
									for($new_count=0;$new_count<=count($newgal);$new_count++){
										if($newgal[$new_count]!=""){
											if($newgal[$new_count]==$get_gal_compro[$count_gal]){
												$get_gal_compro[$count_gal]="";
											}
										}
									}
								}
							}
							$all_gal ="";
							for($new_count_xyz=0;$new_count_xyz<=count($get_gal_compro);$new_count_xyz++)
							{
								if($get_gal_compro[$new_count_xyz]!=""){
								$all_gal = $all_gal .",".$get_gal_compro[$new_count_xyz];
								}
							}
							mysql_query("update community_project set tagged_galleries='".$all_gal."' where id = '".$row_com_pro['id']."'");
							
							$get_song_compro = explode(",",$row_com_pro['tagged_songs']);
							$dummy_song = array();
							for($count_song=0;$count_song<=count($get_song_compro);$count_song++){
								if(in_array($get_song_compro[$count_song],$dummy_song))
								{
								}
								else
								{
									$dummy_song[] = $get_song_compro[$count_song];
								}
							}
							$newsong = explode(",",$song_id);
							for($count_song=0;$count_song<=count($dummy_song);$count_song++){
								if($dummy_song[$count_song]!=""){
									for($new_count_song=0;$new_count_song<=count($newsong);$new_count_song++){
										if($newsong[$new_count_song]!=""){
											if($newsong[$new_count_song]==$get_song_compro[$count_song]){
												$get_song_compro[$count_song]="";
											}
										}
									}
								}
							}
							$all_song="";
							for($new_count_xyz=0;$new_count_xyz<=count($get_song_compro);$new_count_xyz++)
							{
								if($get_song_compro[$new_count_xyz]!=""){
								$all_song = $all_song .",".$get_song_compro[$new_count_xyz];
								}
							}
							mysql_query("update community_project set tagged_songs='".$all_song."' where id = '".$row_com_pro['id']."'");
							
							$get_video_compro = explode(",",$row_com_pro['tagged_videos']);
							$dummy_video = array();
							for($count_vid=0;$count_vid<=count($get_video_compro);$count_vid++){
								if(in_array($get_video_compro[$count_vid],$dummy_video))
								{
								}
								else
								{
									$dummy_video[] = $get_video_compro[$count_vid];
								}
							}
							$newvideo = explode(",",$video_id);
							for($count_vid=0;$count_vid<=count($dummy_video);$count_vid++){
								if($dummy_video[$count_vid]!=""){
									for($new_count_video=0;$new_count_video<=count($newvideo);$new_count_video++){
										if($newvideo[$new_count_video]!=""){
											if($newvideo[$new_count_video]==$get_video_compro[$count_vid]){
												$get_video_compro[$count_vid]="";
											}
										}
									}
								}
							}
							$all_videos="";
							for($new_count_xyz=0;$new_count_xyz<=count($get_video_compro);$new_count_xyz++)
							{
								if($get_video_compro[$new_count_xyz]!=""){
								$all_videos = $all_videos .",".$get_video_compro[$new_count_xyz];
								}
							}
							mysql_query("update community_project set tagged_videos='".$all_videos."' where id = '".$row_com_pro['id']."'");
						}
					}
					
					/***code for community project ends here***/
					/***code for community Event***//*
					$com_event = mysql_query("select * from community_event where community_id ='".$res_gen['community_id']."'");
					if(mysql_num_rows($com_event)>0){
						while($row_com_eve = mysql_fetch_assoc($com_event)){
							$get_gal_comeve = explode(",",$row_com_eve['tagged_galleries']);
							$dummy_gal = array();
							for($count_gal=0;$count_gal<=count($get_gal_comeve);$count_gal++){
								if(in_array($get_gal_comeve[$count_gal],$dummy_gal))
								{
								}
								else
								{
									$dummy_gal[] = $get_gal_comeve[$count_gal];
								}
							}
							$newgal = explode(",",$gall_id);
							for($count_gal=0;$count_gal<=count($dummy_gal);$count_gal++){
								if($dummy_gal[$count_gal]!=""){
									for($new_count=0;$new_count<=count($newgal);$new_count++){
										if($newgal[$new_count]!=""){
											if($newgal[$new_count]==$get_gal_comeve[$count_gal]){
												$get_gal_comeve[$count_gal]="";
											}
										}
									}
								}
							}
							$all_gal ="";
							for($new_count_xyz=0;$new_count_xyz<=count($get_gal_comeve);$new_count_xyz++)
							{
								if($get_gal_comeve[$new_count_xyz]!=""){
								$all_gal = $all_gal .",".$get_gal_comeve[$new_count_xyz];
								}
							}
							mysql_query("update community_event set tagged_galleries='".$all_gal."' where id = '".$row_com_eve['id']."'");
							
							$get_song_comeve = explode(",",$row_com_eve['tagged_songs']);
							$dummy_song = array();
							for($count_song=0;$count_song<=count($get_song_comeve);$count_song++){
								if(in_array($get_song_comeve[$count_song],$dummy_song))
								{
								}
								else
								{
									$dummy_song[] = $get_song_comeve[$count_song];
								}
							}
							$newsong = explode(",",$song_id);
							for($count_song=0;$count_song<=count($dummy_song);$count_song++){
								if($dummy_song[$count_song]!=""){
									for($new_count_song=0;$new_count_song<=count($newsong);$new_count_song++){
										if($newsong[$new_count_song]!=""){
											if($newsong[$new_count_song]==$get_song_comeve[$count_song]){
												$get_song_comeve[$count_song]="";
											}
										}
									}
								}
							}
							$all_song="";
							for($new_count_xyz=0;$new_count_xyz<=count($get_song_comeve);$new_count_xyz++)
							{
								if($get_song_comeve[$new_count_xyz]!=""){
								$all_song = $all_song .",".$get_song_comeve[$new_count_xyz];
								}
							}
							mysql_query("update community_event set tagged_songs='".$all_song."' where id = '".$row_com_eve['id']."'");
							
							$get_video_comeve = explode(",",$row_com_eve['tagged_videos']);
							$dummy_video = array();
							for($count_vid=0;$count_vid<=count($get_video_comeve);$count_vid++){
								if(in_array($get_video_comeve[$count_vid],$dummy_video))
								{
								}
								else
								{
									$dummy_video[] = $get_video_comeve[$count_vid];
								}
							}
							$newvideo = explode(",",$video_id);
							for($count_vid=0;$count_vid<=count($dummy_video);$count_vid++){
								if($dummy_video[$count_vid]!=""){
									for($new_count_video=0;$new_count_video<=count($newvideo);$new_count_video++){
										if($newvideo[$new_count_video]!=""){
											if($newvideo[$new_count_video]==$get_video_comeve[$count_vid]){
												$get_video_comeve[$count_vid]="";
											}
										}
									}
								}
							}
							$all_videos="";
							for($new_count_xyz=0;$new_count_xyz<=count($get_video_comeve);$new_count_xyz++)
							{
								if($get_video_comeve[$new_count_xyz]!=""){
								$all_videos = $all_videos .",".$get_video_comeve[$new_count_xyz];
								}
							}
							mysql_query("update community_event set tagged_videos='".$all_videos."' where id = '".$row_com_eve['id']."'");
						}
					}
					
					/***code for community Event ends here***//*
				}
			}
			/***Code for community ends here***//*
			
		}
	}
	
	
	
	function Add_remove_tagged_user_media($email,$gall_id,$song_id,$video_id,$new_gall_id,$new_song_id,$new_video_id){
		$sql= mysql_query("select * from general_user where email='".$email."'");
		if(mysql_num_rows($sql)>0){
			$res_gen = mysql_fetch_assoc($sql);
			if($res_gen['artist_id']>0 || $res_gen['artist_id']!=""){
				$art_det = mysql_query("select * from general_artist where artist_id = '".$res_gen['artist_id']."'");
				if(mysql_num_rows($art_det)>0){
					$res_art = mysql_fetch_assoc($art_det);
					$get_gal = explode(",",$res_art['taggedgalleries']);
					$dummy_gal = array();
					for($count_gal=0;$count_gal<=count($get_gal);$count_gal++){
						if(in_array($get_gal[$count_gal],$dummy_gal))
						{
						}
						else
						{
							$dummy_gal[] = $get_gal[$count_gal];
						}
					}
					$newgal = explode(",",$gall_id);
					for($count_gal=0;$count_gal<=count($dummy_gal);$count_gal++){
						if($dummy_gal[$count_gal]!=""){
							for($new_count=0;$new_count<=count($newgal);$new_count++){
								if($newgal[$new_count]!=""){
									if($newgal[$new_count]==$get_gal[$count_gal]){
										$get_gal[$count_gal]="";
									}
								}
							}
						}
					}
					$all_gal="";
					for($new_count_xyz=0;$new_count_xyz<=count($get_gal);$new_count_xyz++)
					{
						if($get_gal[$new_count_xyz]!=""){
						$all_gal = $all_gal .",".$get_gal[$new_count_xyz];
						}
					}
					$all_gal = $all_gal .",". $new_gall_id;
					mysql_query("update general_artist set taggedgalleries='".$all_gal."' where artist_id = '".$res_gen['artist_id']."'");
					
					$get_song = explode(",",$res_art['taggedsongs']);
					$dummy_song = array();
					for($count_song=0;$count_song<=count($get_song);$count_song++){
						if(in_array($get_song[$count_song],$dummy_song))
						{
						}
						else
						{
							$dummy_song[] = $get_song[$count_song];
						}
					}
					$newsong = explode(",",$song_id);
					for($count_song=0;$count_song<=count($dummy_song);$count_song++){
						if($dummy_song[$count_song]!=""){
							for($new_count_song=0;$new_count_song<=count($newsong);$new_count_song++){
								if($newsong[$new_count_song]!=""){
									if($newsong[$new_count_song]==$get_song[$count_song]){
										$get_song[$count_song]="";
									}
								}
							}
						}
					}
					$all_song ="";
					for($new_count_xyz=0;$new_count_xyz<=count($get_song);$new_count_xyz++)
					{
						if($get_song[$new_count_xyz]!=""){
						$all_song = $all_song .",".$get_song[$new_count_xyz];
						}
					}
					$all_song = $all_song .",". $new_song_id;
					mysql_query("update general_artist set taggedsongs='".$all_song."' where artist_id = '".$res_gen['artist_id']."'");
					
					$get_video = explode(",",$res_art['taggedvideos']);
					$dummy_video = array();
					for($count_vid=0;$count_vid<=count($get_video);$count_vid++){
						if(in_array($get_video[$count_vid],$dummy_video))
						{
						}
						else
						{
							$dummy_video[] = $get_video[$count_vid];
						}
					}
					$newvideo = explode(",",$video_id);
					for($count_vid=0;$count_vid<=count($dummy_video);$count_vid++){
						if($dummy_video[$count_vid]!=""){
							for($new_count_video=0;$new_count_video<=count($newvideo);$new_count_video++){
								if($newvideo[$new_count_video]!=""){
									if($newvideo[$new_count_video]==$get_video[$count_vid]){
										$get_video[$count_vid]="";
									}
								}
							}
						}
					}
					$all_videos ="";
					for($new_count_xyz=0;$new_count_xyz<=count($get_video);$new_count_xyz++)
					{
						if($get_video[$new_count_xyz]!=""){
						$all_videos = $all_videos .",".$get_video[$new_count_xyz];
						}
					}
					$all_videos = $all_videos .",". $new_video_id;
					mysql_query("update general_artist set taggedvideos='".$all_videos."' where artist_id = '".$res_gen['artist_id']."'");
					
					/***code for artist project***//*
					$art_pro = mysql_query("select * from artist_project where artist_id ='".$res_gen['artist_id']."'");
					if(mysql_num_rows($art_pro)>0){
						while($row_art_pro = mysql_fetch_assoc($art_pro)){
							$get_gal_artpro = explode(",",$row_art_pro['tagged_galleries']);
							$dummy_gal = array();
							for($count_gal=0;$count_gal<=count($get_gal_artpro);$count_gal++){
								if(in_array($get_gal_artpro[$count_gal],$dummy_gal))
								{
								}
								else
								{
									$dummy_gal[] = $get_gal_artpro[$count_gal];
								}
							}
							$newgal = explode(",",$gall_id);
							for($count_gal=0;$count_gal<=count($dummy_gal);$count_gal++){
								if($dummy_gal[$count_gal]!=""){
									for($new_count=0;$new_count<=count($newgal);$new_count++){
										if($newgal[$new_count]!=""){
											if($newgal[$new_count]==$get_gal_artpro[$count_gal]){
												$get_gal_artpro[$count_gal]="";
											}
										}
									}
								}
							}
							$all_gal ="";
							for($new_count_xyz=0;$new_count_xyz<=count($get_gal_artpro);$new_count_xyz++)
							{
								if($get_gal_artpro[$new_count_xyz]!=""){
								$all_gal = $all_gal .",".$get_gal_artpro[$new_count_xyz];
								}
							}
							$all_gal = $all_gal .",". $new_gall_id;
							mysql_query("update artist_project set tagged_galleries='".$all_gal."' where id = '".$row_art_pro['id']."'");
							
							$get_song_artpro = explode(",",$row_art_pro['tagged_songs']);
							$dummy_song = array();
							for($count_song=0;$count_song<=count($get_song_artpro);$count_song++){
								if(in_array($get_song_artpro[$count_song],$dummy_song))
								{
								}
								else
								{
									$dummy_song[] = $get_song_artpro[$count_song];
								}
							}
							$newsong = explode(",",$song_id);
							for($count_song=0;$count_song<=count($dummy_song);$count_song++){
								if($dummy_song[$count_song]!=""){
									for($new_count_song=0;$new_count_song<=count($newsong);$new_count_song++){
										if($newsong[$new_count_song]!=""){
											if($newsong[$new_count_song]==$get_song_artpro[$count_song]){
												$get_song_artpro[$count_song]="";
											}
										}
									}
								}
							}
							$all_song ="";
							for($new_count_xyz=0;$new_count_xyz<=count($get_song_artpro);$new_count_xyz++)
							{
								if($get_song_artpro[$new_count_xyz]!=""){
								$all_song = $all_song .",".$get_song_artpro[$new_count_xyz];
								}
							}
							$all_song = $all_song .",". $new_song_id;
							mysql_query("update artist_project set tagged_songs='".$all_song."' where id = '".$row_art_pro['id']."'");
							
							$get_video_artpro = explode(",",$row_art_pro['tagged_videos']);
							$dummy_video = array();
							for($count_vid=0;$count_vid<=count($get_video_artpro);$count_vid++){
								if(in_array($get_video_artpro[$count_vid],$dummy_video))
								{
								}
								else
								{
									$dummy_video[] = $get_video_artpro[$count_vid];
								}
							}
							$newvideo = explode(",",$video_id);
							for($count_vid=0;$count_vid<=count($dummy_video);$count_vid++){
								if($dummy_video[$count_vid]!=""){
									for($new_count_video=0;$new_count_video<=count($newvideo);$new_count_video++){
										if($newvideo[$new_count_video]!=""){
											if($newvideo[$new_count_video]==$get_video_artpro[$count_vid]){
												$get_video_artpro[$count_vid]="";
											}
										}
									}
								}
							}
							$all_videos="";
							for($new_count_xyz=0;$new_count_xyz<=count($get_video_artpro);$new_count_xyz++)
							{
								if($get_video_artpro[$new_count_xyz]!=""){
								$all_videos = $all_videos .",".$get_video_artpro[$new_count_xyz];
								}
							}
							$all_videos = $all_videos .",". $new_video_id;
							mysql_query("update artist_project set tagged_videos='".$all_videos."' where id = '".$row_art_pro['id']."'");
						}
					}
					
					/***code for artist project ends here***/
					/***code for artist Event***//*
					$art_event = mysql_query("select * from artist_event where artist_id ='".$res_gen['artist_id']."'");
					if(mysql_num_rows($art_event)>0){
						while($row_art_eve = mysql_fetch_assoc($art_event)){
							$get_gal_arteve = explode(",",$row_art_eve['tagged_galleries']);
							$dummy_gal = array();
							for($count_gal=0;$count_gal<=count($get_gal_arteve);$count_gal++){
								if(in_array($get_gal_arteve[$count_gal],$dummy_gal))
								{
								}
								else
								{
									$dummy_gal[] = $get_gal_arteve[$count_gal];
								}
							}
							$newgal = explode(",",$gall_id);
							for($count_gal=0;$count_gal<=count($dummy_gal);$count_gal++){
								if($dummy_gal[$count_gal]!=""){
									for($new_count=0;$new_count<=count($newgal);$new_count++){
										if($newgal[$new_count]!=""){
											if($newgal[$new_count]==$get_gal_arteve[$count_gal]){
												$get_gal_arteve[$count_gal]="";
											}
										}
									}
								}
							}
							$all_gal ="";
							for($new_count_xyz=0;$new_count_xyz<=count($get_gal_arteve);$new_count_xyz++)
							{
								if($get_gal_arteve[$new_count_xyz]!=""){
								$all_gal = $all_gal .",".$get_gal_arteve[$new_count_xyz];
								}
							}
							$all_gal = $all_gal .",". $new_gall_id;
							mysql_query("update artist_event set tagged_galleries='".$all_gal."' where id = '".$row_art_eve['id']."'");
							
							$get_song_arteve = explode(",",$row_art_eve['tagged_songs']);
							$dummy_song = array();
							for($count_song=0;$count_song<=count($get_song_arteve);$count_song++){
								if(in_array($get_song_arteve[$count_song],$dummy_song))
								{
								}
								else
								{
									$dummy_song[] = $get_song_arteve[$count_song];
								}
							}
							$newsong = explode(",",$song_id);
							for($count_song=0;$count_song<=count($dummy_song);$count_song++){
								if($dummy_song[$count_song]!=""){
									for($new_count_song=0;$new_count_song<=count($newsong);$new_count_song++){
										if($newsong[$new_count_song]!=""){
											if($newsong[$new_count_song]==$get_song_arteve[$count_song]){
												$get_song_arteve[$count_song]="";
											}
										}
									}
								}
							}
							$all_song ="";
							for($new_count_xyz=0;$new_count_xyz<=count($get_song_arteve);$new_count_xyz++)
							{
								if($get_song_arteve[$new_count_xyz]!=""){
								$all_song = $all_song .",".$get_song_arteve[$new_count_xyz];
								}
							}
							$all_song = $all_song .",". $new_song_id;
							mysql_query("update artist_event set tagged_songs='".$all_song."' where id = '".$row_art_eve['id']."'");
							
							$get_video_arteve = explode(",",$row_art_eve['tagged_videos']);
							$dummy_video = array();
							for($count_vid=0;$count_vid<=count($get_video_arteve);$count_vid++){
								if(in_array($get_video_arteve[$count_vid],$dummy_video))
								{
								}
								else
								{
									$dummy_video[] = $get_video_arteve[$count_vid];
								}
							}
							$newvideo = explode(",",$video_id);
							for($count_vid=0;$count_vid<=count($dummy_video);$count_vid++){
								if($dummy_video[$count_vid]!=""){
									for($new_count_video=0;$new_count_video<=count($newvideo);$new_count_video++){
										if($newvideo[$new_count_video]!=""){
											if($newvideo[$new_count_video]==$get_video_arteve[$count_vid]){
												$get_video_arteve[$count_vid]="";
											}
										}
									}
								}
							}
							$all_videos ="";
							for($new_count_xyz=0;$new_count_xyz<=count($get_video_arteve);$new_count_xyz++)
							{
								if($get_video_arteve[$new_count_xyz]!=""){
								$all_videos = $all_videos .",".$get_video_arteve[$new_count_xyz];
								}
							}
							$all_videos = $all_videos .",". $new_video_id;
							mysql_query("update artist_event set tagged_videos='".$all_videos."' where id = '".$row_art_eve['id']."'");
						}
					}
					
					/***code for artist Event ends here***//*
				}
			}
			/***Code for community***//*
			if($res_gen['community_id']>0 || $res_gen['community_id']!=""){
				$com_det = mysql_query("select * from general_community where community_id = '".$res_gen['community_id']."'");
				if(mysql_num_rows($com_det)>0){
					$res_com = mysql_fetch_assoc($com_det);
					$get_com_gal = explode(",",$res_com['taggedgalleries']);
					$dummy_gal = array();
					for($count_gal=0;$count_gal<=count($get_com_gal);$count_gal++){
						if(in_array($get_com_gal[$count_gal],$dummy_gal))
						{
						}
						else
						{
							$dummy_gal[] = $get_com_gal[$count_gal];
						}
					}
					$newgal = explode(",",$gall_id);
					for($count_gal=0;$count_gal<=count($dummy_gal);$count_gal++){
						if($dummy_gal[$count_gal]!=""){
							for($new_count=0;$new_count<=count($newgal);$new_count++){
								if($newgal[$new_count]!=""){
									if($newgal[$new_count]==$get_com_gal[$count_gal]){
										$get_com_gal[$count_gal]="";
									}
								}
							}
						}
					}
					$all_gal ="";
					for($new_count_xyz=0;$new_count_xyz<=count($get_com_gal);$new_count_xyz++)
					{
						if($get_com_gal[$new_count_xyz]!=""){
						$all_gal = $all_gal .",".$get_com_gal[$new_count_xyz];
						}
					}
					$all_gal = $all_gal .",". $new_gall_id;
					mysql_query("update general_community set taggedgalleries='".$all_gal."' where community_id = '".$res_gen['community_id']."'");
					
					$get_com_song = explode(",",$res_com['taggedsongs']);
					$dummy_song = array();
					for($count_song=0;$count_song<=count($get_com_song);$count_song++){
						if(in_array($get_com_song[$count_song],$dummy_song))
						{
						}
						else
						{
							$dummy_song[] = $get_com_song[$count_song];
						}
					}
					$newsong = explode(",",$song_id);
					for($count_song=0;$count_song<=count($dummy_song);$count_song++){
						if($dummy_song[$count_song]!=""){
							for($new_count_song=0;$new_count_song<=count($newsong);$new_count_song++){
								if($newsong[$new_count_song]!=""){
									if($newsong[$new_count_song]==$get_com_song[$count_song]){
										$get_com_song[$count_song]="";
									}
								}
							}
						}
					}
					$all_song="";
					for($new_count_xyz=0;$new_count_xyz<=count($get_com_song);$new_count_xyz++)
					{
						if($get_com_song[$new_count_xyz]!=""){
						$all_song = $all_song .",".$get_com_song[$new_count_xyz];
						}
					}
					$all_song = $all_song .",". $new_song_id;
					mysql_query("update general_community set taggedsongs='".$all_song."' where community_id = '".$res_gen['community_id']."'");
					
					$get_com_video = explode(",",$res_com['taggedvideos']);
					$dummy_video = array();
					for($count_vid=0;$count_vid<=count($get_com_video);$count_vid++){
						if(in_array($get_com_video[$count_vid],$dummy_video))
						{
						}
						else
						{
							$dummy_video[] = $get_com_video[$count_vid];
						}
					}
					$newvideo = explode(",",$video_id);
					for($count_vid=0;$count_vid<=count($dummy_video);$count_vid++){
						if($dummy_video[$count_vid]!=""){
							for($new_count_video=0;$new_count_video<=count($newvideo);$new_count_video++){
								if($newvideo[$new_count_video]!=""){
									if($newvideo[$new_count_video]==$get_com_video[$count_vid]){
										$get_com_video[$count_vid]="";
									}
								}
							}
						}
					}
					$all_videos="";
					for($new_count_xyz=0;$new_count_xyz<=count($get_com_video);$new_count_xyz++)
					{
						if($get_com_video[$new_count_xyz]!=""){
						$all_videos = $all_videos .",".$get_com_video[$new_count_xyz];
						}
					}
					$all_videos = $all_videos .",". $new_video_id;
					mysql_query("update general_community set taggedvideos='".$all_videos."' where community_id = '".$res_gen['community_id']."'");
					
					/***code for community project***//*
					$com_pro = mysql_query("select * from community_project where community_id ='".$res_gen['community_id']."'");
					if(mysql_num_rows($com_pro)>0){
						while($row_com_pro = mysql_fetch_assoc($com_pro)){
							$get_gal_compro = explode(",",$row_com_pro['tagged_galleries']);
							$dummy_gal = array();
							for($count_gal=0;$count_gal<=count($get_gal_compro);$count_gal++){
								if(in_array($get_gal_compro[$count_gal],$dummy_gal))
								{
								}
								else
								{
									$dummy_gal[] = $get_gal_compro[$count_gal];
								}
							}
							$newgal = explode(",",$gall_id);
							for($count_gal=0;$count_gal<=count($dummy_gal);$count_gal++){
								if($dummy_gal[$count_gal]!=""){
									for($new_count=0;$new_count<=count($newgal);$new_count++){
										if($newgal[$new_count]!=""){
											if($newgal[$new_count]==$get_gal_compro[$count_gal]){
												$get_gal_compro[$count_gal]="";
											}
										}
									}
								}
							}
							$all_gal ="";
							for($new_count_xyz=0;$new_count_xyz<=count($get_gal_compro);$new_count_xyz++)
							{
								if($get_gal_compro[$new_count_xyz]!=""){
								$all_gal = $all_gal .",".$get_gal_compro[$new_count_xyz];
								}
							}
							$all_gal = $all_gal .",". $new_gall_id;
							mysql_query("update community_project set tagged_galleries='".$all_gal."' where id = '".$row_com_pro['id']."'");
							
							$get_song_compro = explode(",",$row_com_pro['tagged_songs']);
							$dummy_song = array();
							for($count_song=0;$count_song<=count($get_song_compro);$count_song++){
								if(in_array($get_song_compro[$count_song],$dummy_song))
								{
								}
								else
								{
									$dummy_song[] = $get_song_compro[$count_song];
								}
							}
							$newsong = explode(",",$song_id);
							for($count_song=0;$count_song<=count($dummy_song);$count_song++){
								if($dummy_song[$count_song]!=""){
									for($new_count_song=0;$new_count_song<=count($newsong);$new_count_song++){
										if($newsong[$new_count_song]!=""){
											if($newsong[$new_count_song]==$get_song_compro[$count_song]){
												$get_song_compro[$count_song]="";
											}
										}
									}
								}
							}
							$all_song="";
							for($new_count_xyz=0;$new_count_xyz<=count($get_song_compro);$new_count_xyz++)
							{
								if($get_song_compro[$new_count_xyz]!=""){
								$all_song = $all_song .",".$get_song_compro[$new_count_xyz];
								}
							}
							$all_song = $all_song .",". $new_song_id;
							mysql_query("update community_project set tagged_songs='".$all_song."' where id = '".$row_com_pro['id']."'");
							
							$get_video_compro = explode(",",$row_com_pro['tagged_videos']);
							$dummy_video = array();
							for($count_vid=0;$count_vid<=count($get_video_compro);$count_vid++){
								if(in_array($get_video_compro[$count_vid],$dummy_video))
								{
								}
								else
								{
									$dummy_video[] = $get_video_compro[$count_vid];
								}
							}
							$newvideo = explode(",",$video_id);
							for($count_vid=0;$count_vid<=count($dummy_video);$count_vid++){
								if($dummy_video[$count_vid]!=""){
									for($new_count_video=0;$new_count_video<=count($newvideo);$new_count_video++){
										if($newvideo[$new_count_video]!=""){
											if($newvideo[$new_count_video]==$get_video_compro[$count_vid]){
												$get_video_compro[$count_vid]="";
											}
										}
									}
								}
							}
							$all_videos="";
							for($new_count_xyz=0;$new_count_xyz<=count($get_video_compro);$new_count_xyz++)
							{
								if($get_video_compro[$new_count_xyz]!=""){
								$all_videos = $all_videos .",".$get_video_compro[$new_count_xyz];
								}
							}
							$all_videos = $all_videos .",". $new_video_id;
							mysql_query("update community_project set tagged_videos='".$all_videos."' where id = '".$row_com_pro['id']."'");
						}
					}
					
					/***code for community project ends here***/
					/***code for community Event***//*
					$com_event = mysql_query("select * from community_event where community_id ='".$res_gen['community_id']."'");
					if(mysql_num_rows($com_event)>0){
						while($row_com_eve = mysql_fetch_assoc($com_event)){
							$get_gal_comeve = explode(",",$row_com_eve['tagged_galleries']);
							$dummy_gal = array();
							for($count_gal=0;$count_gal<=count($get_gal_comeve);$count_gal++){
								if(in_array($get_gal_comeve[$count_gal],$dummy_gal))
								{
								}
								else
								{
									$dummy_gal[] = $get_gal_comeve[$count_gal];
								}
							}
							$newgal = explode(",",$gall_id);
							for($count_gal=0;$count_gal<=count($dummy_gal);$count_gal++){
								if($dummy_gal[$count_gal]!=""){
									for($new_count=0;$new_count<=count($newgal);$new_count++){
										if($newgal[$new_count]!=""){
											if($newgal[$new_count]==$get_gal_comeve[$count_gal]){
												$get_gal_comeve[$count_gal]="";
											}
										}
									}
								}
							}
							$all_gal ="";
							for($new_count_xyz=0;$new_count_xyz<=count($get_gal_comeve);$new_count_xyz++)
							{
								if($get_gal_comeve[$new_count_xyz]!=""){
								$all_gal = $all_gal .",".$get_gal_comeve[$new_count_xyz];
								}
							}
							$all_gal = $all_gal .",". $new_gall_id;
							mysql_query("update community_event set tagged_galleries='".$all_gal."' where id = '".$row_com_eve['id']."'");
							
							$get_song_comeve = explode(",",$row_com_eve['tagged_songs']);
							$dummy_song = array();
							for($count_song=0;$count_song<=count($get_song_comeve);$count_song++){
								if(in_array($get_song_comeve[$count_song],$dummy_song))
								{
								}
								else
								{
									$dummy_song[] = $get_song_comeve[$count_song];
								}
							}
							$newsong = explode(",",$song_id);
							for($count_song=0;$count_song<=count($dummy_song);$count_song++){
								if($dummy_song[$count_song]!=""){
									for($new_count_song=0;$new_count_song<=count($newsong);$new_count_song++){
										if($newsong[$new_count_song]!=""){
											if($newsong[$new_count_song]==$get_song_comeve[$count_song]){
												$get_song_comeve[$count_song]="";
											}
										}
									}
								}
							}
							$all_song="";
							for($new_count_xyz=0;$new_count_xyz<=count($get_song_comeve);$new_count_xyz++)
							{
								if($get_song_comeve[$new_count_xyz]!=""){
								$all_song = $all_song .",".$get_song_comeve[$new_count_xyz];
								}
							}
							$all_song = $all_song .",". $new_song_id;
							mysql_query("update community_event set tagged_songs='".$all_song."' where id = '".$row_com_eve['id']."'");
							
							$get_video_comeve = explode(",",$row_com_eve['tagged_videos']);
							$dummy_video = array();
							for($count_vid=0;$count_vid<=count($get_video_comeve);$count_vid++){
								if(in_array($get_video_comeve[$count_vid],$dummy_video))
								{
								}
								else
								{
									$dummy_video[] = $get_video_comeve[$count_vid];
								}
							}
							$newvideo = explode(",",$video_id);
							for($count_vid=0;$count_vid<=count($dummy_video);$count_vid++){
								if($dummy_video[$count_vid]!=""){
									for($new_count_video=0;$new_count_video<=count($newvideo);$new_count_video++){
										if($newvideo[$new_count_video]!=""){
											if($newvideo[$new_count_video]==$get_video_comeve[$count_vid]){
												$get_video_comeve[$count_vid]="";
											}
										}
									}
								}
							}
							$all_videos="";
							for($new_count_xyz=0;$new_count_xyz<=count($get_video_comeve);$new_count_xyz++)
							{
								if($get_video_comeve[$new_count_xyz]!=""){
								$all_videos = $all_videos .",".$get_video_comeve[$new_count_xyz];
								}
							}
							$all_videos = $all_videos .",". $new_video_id;
							mysql_query("update community_event set tagged_videos='".$all_videos."' where id = '".$row_com_eve['id']."'");
						}
					}
					
					/***code for community Event ends here***//*
				}
			}
			/***Code for community ends here***//*
			
		}
	}
	
	
	
	function Add_tagged_user_media($email,$new_gall_id,$new_song_id,$new_video_id){
		$sql= mysql_query("select * from general_user where email='".$email."'");
		if(mysql_num_rows($sql)>0){
			$res_gen = mysql_fetch_assoc($sql);
			if($res_gen['artist_id']>0 || $res_gen['artist_id']!=""){
				$art_det = mysql_query("select * from general_artist where artist_id = '".$res_gen['artist_id']."'");
				if(mysql_num_rows($art_det)>0){
					$res_art = mysql_fetch_assoc($art_det);
					$get_gal = $res_art['taggedgalleries'];
					$all_gal = $get_gal .",". $new_gall_id;
					mysql_query("update general_artist set taggedgalleries='".$all_gal."' where artist_id = '".$res_gen['artist_id']."'");
					
					$get_song = $res_art['taggedsongs'];
					$all_song = $get_song .",". $new_song_id;
					mysql_query("update general_artist set taggedsongs='".$all_song."' where artist_id = '".$res_gen['artist_id']."'");
					
					$get_video = $res_art['taggedvideos'];
					$all_videos = $get_video .",". $new_video_id;
					mysql_query("update general_artist set taggedvideos='".$all_videos."' where artist_id = '".$res_gen['artist_id']."'");
					
					/***code for artist project***//*
					$art_pro = mysql_query("select * from artist_project where artist_id ='".$res_gen['artist_id']."'");
					if(mysql_num_rows($art_pro)>0){
						while($row_art_pro = mysql_fetch_assoc($art_pro)){
							$get_gal_artpro = $row_art_pro['tagged_galleries'];
							$all_gal = $get_gal_artpro .",". $new_gall_id;
							mysql_query("update artist_project set tagged_galleries='".$all_gal."' where id = '".$row_art_pro['id']."'");
							
							$get_song_artpro = $row_art_pro['tagged_songs'];
							$all_song = $get_song_artpro .",". $new_song_id;
							mysql_query("update artist_project set tagged_songs='".$all_song."' where id = '".$row_art_pro['id']."'");
							
							$get_video_artpro = $row_art_pro['tagged_videos'];
							$all_videos = $get_video_artpro .",". $new_video_id;
							mysql_query("update artist_project set tagged_videos='".$all_videos."' where id = '".$row_art_pro['id']."'");
						}
					}
					
					/***code for artist project ends here***/
					/***code for artist Event***//*
					$art_event = mysql_query("select * from artist_event where artist_id ='".$res_gen['artist_id']."'");
					if(mysql_num_rows($art_event)>0){
						while($row_art_eve = mysql_fetch_assoc($art_event)){
							$get_gal_arteve = $row_art_eve['tagged_galleries'];
							$all_gal = $get_gal_arteve .",". $new_gall_id;
							mysql_query("update artist_event set tagged_galleries='".$all_gal."' where id = '".$row_art_eve['id']."'");
							
							$get_song_arteve = $row_art_eve['tagged_songs'];
							$all_song = $get_song_arteve .",". $new_song_id;
							mysql_query("update artist_event set tagged_songs='".$all_song."' where id = '".$row_art_eve['id']."'");
							
							$get_video_arteve = $row_art_eve['tagged_videos'];
							$all_videos = $get_video_arteve .",". $new_video_id;
							mysql_query("update artist_event set tagged_videos='".$all_videos."' where id = '".$row_art_eve['id']."'");
						}
					}
					
					/***code for artist Event ends here***//*
				}
			}
			/***Code for community***//*
			if($res_gen['community_id']>0 || $res_gen['community_id']!=""){
				$com_det = mysql_query("select * from general_community where community_id = '".$res_gen['community_id']."'");
				if(mysql_num_rows($com_det)>0){
					$res_com = mysql_fetch_assoc($com_det);
					$get_com_gal = $res_com['taggedgalleries'];
					$all_gal = $get_com_gal .",". $new_gall_id;
					mysql_query("update general_community set taggedgalleries='".$all_gal."' where community_id = '".$res_gen['community_id']."'");
					
					$get_com_song = $res_com['taggedsongs'];
					$all_song = $get_com_song .",". $new_song_id;
					mysql_query("update general_community set taggedsongs='".$all_song."' where community_id = '".$res_gen['community_id']."'");
					
					$get_com_video = $res_com['taggedvideos'];
					$all_videos = $get_com_video .",". $new_video_id;
					mysql_query("update general_community set taggedvideos='".$all_videos."' where community_id = '".$res_gen['community_id']."'");
					
					/***code for community project***//*
					$com_pro = mysql_query("select * from community_project where community_id ='".$res_gen['community_id']."'");
					if(mysql_num_rows($com_pro)>0){
						while($row_com_pro = mysql_fetch_assoc($com_pro)){
							$get_gal_compro = $row_com_pro['tagged_galleries'];
							$all_gal = $get_gal_compro .",". $new_gall_id;
							mysql_query("update community_project set tagged_galleries='".$all_gal."' where id = '".$row_com_pro['id']."'");
							
							$get_song_compro = $row_com_pro['tagged_songs'];
							$all_song = $get_song_compro .",". $new_song_id;
							mysql_query("update community_project set tagged_songs='".$all_song."' where id = '".$row_com_pro['id']."'");
							
							$get_video_compro = $row_com_pro['tagged_videos'];
							$all_videos = $get_video_compro .",". $new_video_id;
							mysql_query("update community_project set tagged_videos='".$all_videos."' where id = '".$row_com_pro['id']."'");
						}
					}
					
					/***code for community project ends here***/
					/***code for community Event***//*
					$com_event = mysql_query("select * from community_event where community_id ='".$res_gen['community_id']."'");
					if(mysql_num_rows($com_event)>0){
						while($row_com_eve = mysql_fetch_assoc($com_event)){
							$get_gal_comeve = $row_com_eve['tagged_galleries'];
							$all_gal = $get_gal_comeve .",". $new_gall_id;
							mysql_query("update community_event set tagged_galleries='".$all_gal."' where id = '".$row_com_eve['id']."'");
							
							$get_song_comeve = $row_com_eve['tagged_songs'];
							$all_song = $get_song_comeve .",". $new_song_id;
							mysql_query("update community_event set tagged_songs='".$all_song."' where id = '".$row_com_eve['id']."'");
							
							$get_video_comeve = $row_com_eve['tagged_videos'];
							$all_videos = $get_video_comeve .",". $new_video_id;
							mysql_query("update community_event set tagged_videos='".$all_videos."' where id = '".$row_com_eve['id']."'");
						}
					}
					
					/***code for community Event ends here***//*
				}
			}
			/***Code for community ends here***/
			/*
		}
	}
	
	function Add_creator_user_media($new_gall_id,$new_song_id,$new_video_id,$id_name,$id){
		$sql= mysql_query("select * from general_user where ".$id_name."='".$id."'");
		if(mysql_num_rows($sql)>0){
			$res_gen = mysql_fetch_assoc($sql);
			if($res_gen['artist_id']>0 || $res_gen['artist_id']!=""){
				$art_det = mysql_query("select * from general_artist where artist_id = '".$res_gen['artist_id']."'");
				if(mysql_num_rows($art_det)>0){
					$res_art = mysql_fetch_assoc($art_det);
					$get_gal = $res_art['taggedgalleries'];
					$all_gal = $get_gal .",". $new_gall_id;
					mysql_query("update general_artist set taggedgalleries='".$all_gal."' where artist_id = '".$res_gen['artist_id']."'");
					
					$get_song = $res_art['taggedsongs'];
					$all_song = $get_song .",". $new_song_id;
					mysql_query("update general_artist set taggedsongs='".$all_song."' where artist_id = '".$res_gen['artist_id']."'");
					
					$get_video = $res_art['taggedvideos'];
					$all_videos = $get_video .",". $new_video_id;
					mysql_query("update general_artist set taggedvideos='".$all_videos."' where artist_id = '".$res_gen['artist_id']."'");
					
					/***code for artist project***//*
					$art_pro = mysql_query("select * from artist_project where artist_id ='".$res_gen['artist_id']."'");
					if(mysql_num_rows($art_pro)>0){
						while($row_art_pro = mysql_fetch_assoc($art_pro)){
							$get_gal_artpro = $row_art_pro['tagged_galleries'];
							$all_gal = $get_gal_artpro .",". $new_gall_id;
							mysql_query("update artist_project set tagged_galleries='".$all_gal."' where id = '".$row_art_pro['id']."'");
							
							$get_song_artpro = $row_art_pro['tagged_songs'];
							$all_song = $get_song_artpro .",". $new_song_id;
							mysql_query("update artist_project set tagged_songs='".$all_song."' where id = '".$row_art_pro['id']."'");
							
							$get_video_artpro = $row_art_pro['tagged_videos'];
							$all_videos = $get_video_artpro .",". $new_video_id;
							mysql_query("update artist_project set tagged_videos='".$all_videos."' where id = '".$row_art_pro['id']."'");
						}
					}
					
					/***code for artist project ends here***/
					/***code for artist Event***//*
					$art_event = mysql_query("select * from artist_event where artist_id ='".$res_gen['artist_id']."'");
					if(mysql_num_rows($art_event)>0){
						while($row_art_eve = mysql_fetch_assoc($art_event)){
							$get_gal_arteve = $row_art_eve['tagged_galleries'];
							$all_gal = $get_gal_arteve .",". $new_gall_id;
							mysql_query("update artist_event set tagged_galleries='".$all_gal."' where id = '".$row_art_eve['id']."'");
							
							$get_song_arteve = $row_art_eve['tagged_songs'];
							$all_song = $get_song_arteve .",". $new_song_id;
							mysql_query("update artist_event set tagged_songs='".$all_song."' where id = '".$row_art_eve['id']."'");
							
							$get_video_arteve = $row_art_eve['tagged_videos'];
							$all_videos = $get_video_arteve .",". $new_video_id;
							mysql_query("update artist_event set tagged_videos='".$all_videos."' where id = '".$row_art_eve['id']."'");
						}
					}
					
					/***code for artist Event ends here***//*
				}
			}
			/***Code for community***//*
			if($res_gen['community_id']>0 || $res_gen['community_id']!=""){
				$com_det = mysql_query("select * from general_community where community_id = '".$res_gen['community_id']."'");
				if(mysql_num_rows($com_det)>0){
					$res_com = mysql_fetch_assoc($com_det);
					$get_com_gal = $res_com['taggedgalleries'];
					$all_gal = $get_com_gal .",". $new_gall_id;
					mysql_query("update general_community set taggedgalleries='".$all_gal."' where community_id = '".$res_gen['community_id']."'");
					
					$get_com_song = $res_com['taggedsongs'];
					$all_song = $get_com_song .",". $new_song_id;
					mysql_query("update general_community set taggedsongs='".$all_song."' where community_id = '".$res_gen['community_id']."'");
					
					$get_com_video = $res_com['taggedvideos'];
					$all_videos = $get_com_video .",". $new_video_id;
					mysql_query("update general_community set taggedvideos='".$all_videos."' where community_id = '".$res_gen['community_id']."'");
					
					/***code for community project***//*
					$com_pro = mysql_query("select * from community_project where community_id ='".$res_gen['community_id']."'");
					if(mysql_num_rows($com_pro)>0){
						while($row_com_pro = mysql_fetch_assoc($com_pro)){
							$get_gal_compro = $row_com_pro['tagged_galleries'];
							$all_gal = $get_gal_compro .",". $new_gall_id;
							mysql_query("update community_project set tagged_galleries='".$all_gal."' where id = '".$row_com_pro['id']."'");
							
							$get_song_compro = $row_com_pro['tagged_songs'];
							$all_song = $get_song_compro .",". $new_song_id;
							mysql_query("update community_project set tagged_songs='".$all_song."' where id = '".$row_com_pro['id']."'");
							
							$get_video_compro = $row_com_pro['tagged_videos'];
							$all_videos = $get_video_compro .",". $new_video_id;
							mysql_query("update community_project set tagged_videos='".$all_videos."' where id = '".$row_com_pro['id']."'");
						}
					}
					
					/***code for community project ends here***/
					/***code for community Event***//*
					$com_event = mysql_query("select * from community_event where community_id ='".$res_gen['community_id']."'");
					if(mysql_num_rows($com_event)>0){
						while($row_com_eve = mysql_fetch_assoc($com_event)){
							$get_gal_comeve = $row_com_eve['tagged_galleries'];
							$all_gal = $get_gal_comeve .",". $new_gall_id;
							mysql_query("update community_event set tagged_galleries='".$all_gal."' where id = '".$row_com_eve['id']."'");
							
							$get_song_comeve = $row_com_eve['tagged_songs'];
							$all_song = $get_song_comeve .",". $new_song_id;
							mysql_query("update community_event set tagged_songs='".$all_song."' where id = '".$row_com_eve['id']."'");
							
							$get_video_comeve = $row_com_eve['tagged_videos'];
							$all_videos = $get_video_comeve .",". $new_video_id;
							mysql_query("update community_event set tagged_videos='".$all_videos."' where id = '".$row_com_eve['id']."'");
						}
					}
					
					/***code for community Event ends here***//*
				}
			}
			/***Code for community ends here***//*
			
		}
	}
	
	
	function delete_creator_user_media($gall_id,$song_id,$video_id,$id_name,$id){
		$dummy_gals = array();
		$dummy_song = array();
		$dummy_video = array();
		$sql= mysql_query("select * from general_user where ".$id_name."='".$id."'");
		if(mysql_num_rows($sql)>0){
			$res_gen = mysql_fetch_assoc($sql);
			if($res_gen['artist_id']>0 || $res_gen['artist_id']!=""){
				$art_det = mysql_query("select * from general_artist where artist_id = '".$res_gen['artist_id']."'");
				if(mysql_num_rows($art_det)>0){
					$res_art = mysql_fetch_assoc($art_det);
					$get_gal = explode(",",$res_art['taggedgalleries']);
					for($count_gal=0;$count_gal<=count($get_gal);$count_gal++){
						if(in_array($get_gal[$count_gal],$dummy_gals))
						{
						}
						else
						{
							$dummy_gals[] = $get_gal[$count_gal];
						}
					}
					
					$newgal = explode(",",$gall_id);
					for($count_gal=0;$count_gal<=count($dummy_gals);$count_gal++){
						if($dummy_gals[$count_gal]!=""){
							for($new_count=0;$new_count<=count($newgal);$new_count++){
								if($newgal[$new_count]!=""){
									if($newgal[$new_count]==$get_gal[$count_gal]){
										$get_gal[$count_gal]="";
									}
								}
							}
						}
					}
					$all_gal="";
					for($new_countxyz=0;$new_countxyz<=count($get_gal);$new_countxyz++)
					{
						if($get_gal[$new_countxyz]!=""){
						$all_gal = $all_gal .",".$get_gal[$new_countxyz];
						}
					}
					mysql_query("update general_artist set taggedgalleries='".$all_gal."' where artist_id = '".$res_gen['artist_id']."'");

					$get_song = explode(",",$res_art['taggedsongs']);
					for($count_song=0;$count_song<=count($get_song);$count_song++){
						if(in_array($get_song[$count_song],$dummy_song))
						{
						}
						else
						{
							$dummy_song[] = $get_song[$count_song];
						}
					}
					$newsong = explode(",",$song_id);
					for($count_song=0;$count_song<=count($dummy_song);$count_song++){
						if($dummy_song[$count_song]!=""){
							for($new_count_song=0;$new_count_song<=count($newsong);$new_count_song++){
								if($newsong[$new_count_song]!=""){
									if($newsong[$new_count_song]==$get_song[$count_song]){
										$get_song[$count_song]="";
									}
								}
							}
						}
					}
					$all_song ="";
					for($new_countxyz=0;$new_countxyz<=count($get_song);$new_countxyz++)
					{
						if($get_song[$new_countxyz]!=""){
						$all_song = $all_song .",".$get_song[$new_countxyz];
						}
					}
					mysql_query("update general_artist set taggedsongs='".$all_song."' where artist_id = '".$res_gen['artist_id']."'");
					
					$get_video = explode(",",$res_art['taggedvideos']);
					for($count_vid=0;$count_vid<=count($get_video);$count_vid++){
						if(in_array($get_video[$count_vid],$dummy_video))
						{
						}
						else
						{
							$dummy_video[] = $get_video[$count_vid];
						}
					}
					$newvideo = explode(",",$video_id);
					for($count_vid=0;$count_vid<=count($dummy_video);$count_vid++){
						if($dummy_video[$count_vid]!=""){
							for($new_count_video=0;$new_count_video<=count($newvideo);$new_count_video++){
								if($newvideo[$new_count_video]!=""){
									if($newvideo[$new_count_video]==$get_video[$count_vid]){
										$get_video[$count_vid]="";
									}
								}
							}
						}
					}
					$all_videos ="";
					for($new_countxyz=0;$new_countxyz<=count($get_video);$new_countxyz++)
					{
						if($get_video[$new_countxyz]!=""){
						$all_videos = $all_videos .",".$get_video[$new_countxyz];
						}
					}
					mysql_query("update general_artist set taggedvideos='".$all_videos."' where artist_id = '".$res_gen['artist_id']."'");
					
					/***code for artist project***//*
					$art_pro = mysql_query("select * from artist_project where artist_id ='".$res_gen['artist_id']."'");
					if(mysql_num_rows($art_pro)>0){
						while($row_art_pro = mysql_fetch_assoc($art_pro)){
							$get_gal_artpro = explode(",",$row_art_pro['tagged_galleries']);
							$dummy_gals = array();
							for($count_gal=0;$count_gal<=count($get_gal_artpro);$count_gal++){
								if(in_array($get_gal_artpro[$count_gal],$dummy_gals))
								{
								}
								else
								{
									$dummy_gals[] = $get_gal_artpro[$count_gal];
								}
							}
							$newgal = explode(",",$gall_id);
							for($count_gal=0;$count_gal<=count($dummy_gals);$count_gal++){
								if($dummy_gals[$count_gal]!=""){
									for($new_count=0;$new_count<=count($newgal);$new_count++){
										if($newgal[$new_count]!=""){
											if($newgal[$new_count]==$get_gal_artpro[$count_gal]){
												$get_gal_artpro[$count_gal]="";
											}
										}
									}
								}
							}
							$all_gal ="";
							for($new_count_xyz=0;$new_count_xyz<=count($get_gal_artpro);$new_count_xyz++)
							{
								if($get_gal_artpro[$new_count_xyz]!=""){
								$all_gal = $all_gal .",".$get_gal_artpro[$new_count_xyz];
								}
							}
							mysql_query("update artist_project set tagged_galleries='".$all_gal."' where id = '".$row_art_pro['id']."'");
							
							$get_song_artpro = explode(",",$row_art_pro['tagged_songs']);
							$dummy_songs = array();
							for($count_song=0;$count_song<=count($get_song_artpro);$count_song++){
								if(in_array($get_song_artpro[$count_song],$dummy_songs))
								{
								}
								else
								{
									$dummy_songs[] = $get_song_artpro[$count_song];
								}
							}
							$newsong = explode(",",$song_id);
							for($count_song=0;$count_song<=count($dummy_songs);$count_song++){
								if($dummy_songs[$count_song]!=""){
									for($new_count_song=0;$new_count_song<=count($newsong);$new_count_song++){
										if($newsong[$new_count_song]!=""){
											if($newsong[$new_count_song]==$get_song_artpro[$count_song]){
												$get_song_artpro[$count_song]="";
											}
										}
									}
								}
							}
							$all_song ="";
							for($new_count_xyz=0;$new_count_xyz<=count($get_song_artpro);$new_count_xyz++)
							{
								if($get_song_artpro[$new_count_xyz]!=""){
								$all_song = $all_song .",".$get_song_artpro[$new_count_xyz];
								}
							}
							mysql_query("update artist_project set tagged_songs='".$all_song."' where id = '".$row_art_pro['id']."'");
							
							$get_video_artpro = explode(",",$row_art_pro['tagged_videos']);
							$dummy_video = array();
							for($count_vid=0;$count_vid<=count($get_video_artpro);$count_vid++){
								if(in_array($get_video_artpro[$count_vid],$dummy_video))
								{
								}
								else
								{
									$dummy_video[] = $get_video_artpro[$count_vid];
								}
							}
							$newvideo = explode(",",$video_id);
							for($count_vid=0;$count_vid<=count($dummy_video);$count_vid++){
								if($dummy_video[$count_vid]!=""){
									for($new_count_video=0;$new_count_video<=count($newvideo);$new_count_video++){
										if($newvideo[$new_count_video]!=""){
											if($newvideo[$new_count_video]==$get_video_artpro[$count_vid]){
												$get_video_artpro[$count_vid]="";
											}
										}
									}
								}
							}
							$all_videos="";
							for($new_count_xyz=0;$new_count_xyz<=count($get_video_artpro);$new_count_xyz++)
							{
								if($get_video_artpro[$new_count_xyz]!=""){
								$all_videos = $all_videos .",".$get_video_artpro[$new_count_xyz];
								}
							}
							mysql_query("update artist_project set tagged_videos='".$all_videos."' where id = '".$row_art_pro['id']."'");
						}
					}
					
					/***code for artist project ends here***/
					/***code for artist Event***//*
					$art_event = mysql_query("select * from artist_event where artist_id ='".$res_gen['artist_id']."'");
					if(mysql_num_rows($art_event)>0){
						while($row_art_eve = mysql_fetch_assoc($art_event)){
							$get_gal_arteve = explode(",",$row_art_eve['tagged_galleries']);
							$dummy_gal = array();
							for($count_gal=0;$count_gal<=count($get_gal_arteve);$count_gal++){
								if(in_array($get_gal_arteve[$count_gal],$dummy_gal))
								{
								}
								else
								{
									$dummy_gal[] = $get_gal_arteve[$count_gal];
								}
							}
							$newgal = explode(",",$gall_id);
							for($count_gal=0;$count_gal<=count($dummy_gal);$count_gal++){
								if($dummy_gal[$count_gal]!=""){
									for($new_count=0;$new_count<=count($newgal);$new_count++){
										if($newgal[$new_count]!=""){
											if($newgal[$new_count]==$get_gal_arteve[$count_gal]){
												$get_gal_arteve[$count_gal]="";
											}
										}
									}
								}
							}
							$all_gal ="";
							for($new_count_xyz=0;$new_count_xyz<=count($get_gal_arteve);$new_count_xyz++)
							{
								if($get_gal_arteve[$new_count_xyz]!=""){
								$all_gal = $all_gal .",".$get_gal_arteve[$new_count_xyz];
								}
							}
							mysql_query("update artist_event set tagged_galleries='".$all_gal."' where id = '".$row_art_eve['id']."'");
							
							$get_song_arteve = explode(",",$row_art_eve['tagged_songs']);
							$dummy_song = array();
							for($count_song=0;$count_song<=count($get_song_arteve);$count_song++){
								if(in_array($get_song_arteve[$count_song],$dummy_song))
								{
								}
								else
								{
									$dummy_song[] = $get_song_arteve[$count_song];
								}
							}
							$newsong = explode(",",$song_id);
							for($count_song=0;$count_song<=count($dummy_song);$count_song++){
								if($dummy_song[$count_song]!=""){
									for($new_count_song=0;$new_count_song<=count($newsong);$new_count_song++){
										if($newsong[$new_count_song]!=""){
											if($newsong[$new_count_song]==$get_song_arteve[$count_song]){
												$get_song_arteve[$count_song]="";
											}
										}
									}
								}
							}
							$all_song ="";
							for($new_count_xyz=0;$new_count_xyz<=count($get_song_arteve);$new_count_xyz++)
							{
								if($get_song_arteve[$new_count_xyz]!=""){
								$all_song = $all_song .",".$get_song_arteve[$new_count_xyz];
								}
							}
							mysql_query("update artist_event set tagged_songs='".$all_song."' where id = '".$row_art_eve['id']."'");
							
							$get_video_arteve = explode(",",$row_art_eve['tagged_videos']);
							$dummy_video = array();
							for($count_vid=0;$count_vid<=count($get_video_arteve);$count_vid++){
								if(in_array($get_video_arteve[$count_vid],$dummy_video))
								{
								}
								else
								{
									$dummy_video[] = $get_video_arteve[$count_vid];
								}
							}
							$newvideo = explode(",",$video_id);
							for($count_vid=0;$count_vid<=count($dummy_video);$count_vid++){
								if($dummy_video[$count_vid]!=""){
									for($new_count_video=0;$new_count_video<=count($newvideo);$new_count_video++){
										if($newvideo[$new_count_video]!=""){
											if($newvideo[$new_count_video]==$get_video_arteve[$count_vid]){
												$get_video_arteve[$count_vid]="";
											}
										}
									}
								}
							}
							$all_videos ="";
							for($new_count_xyz=0;$new_count_xyz<=count($get_video_arteve);$new_count_xyz++)
							{
								if($get_video_arteve[$new_count_xyz]!=""){
								$all_videos = $all_videos .",".$get_video_arteve[$new_count_xyz];
								}
							}
							mysql_query("update artist_event set tagged_videos='".$all_videos."' where id = '".$row_art_eve['id']."'");
						}
					}
					
					/***code for artist Event ends here***//*
				}
			}
			/***Code for community***//*
			if($res_gen['community_id']>0 || $res_gen['community_id']!=""){
				$com_det = mysql_query("select * from general_community where community_id = '".$res_gen['community_id']."'");
				if(mysql_num_rows($com_det)>0){
					$res_com = mysql_fetch_assoc($com_det);
					$get_com_gal = explode(",",$res_com['taggedgalleries']);
					$dummy_gal = array();
					for($count_gal=0;$count_gal<=count($get_com_gal);$count_gal++){
						if(in_array($get_com_gal[$count_gal],$dummy_gal))
						{
						}
						else
						{
							$dummy_gal[] = $get_com_gal[$count_gal];
						}
					}
					$newgal = explode(",",$gall_id);
					for($count_gal=0;$count_gal<=count($dummy_gal);$count_gal++){
						if($dummy_gal[$count_gal]!=""){
							for($new_count=0;$new_count<=count($newgal);$new_count++){
								if($newgal[$new_count]!=""){
									if($newgal[$new_count]==$get_com_gal[$count_gal]){
										$get_com_gal[$count_gal]="";
									}
								}
							}
						}
					}
					$all_gal ="";
					for($new_count_xyz=0;$new_count_xyz<=count($get_com_gal);$new_count_xyz++)
					{
						if($get_com_gal[$new_count_xyz]!=""){
						$all_gal = $all_gal .",".$get_com_gal[$new_count_xyz];
						}
					}
					mysql_query("update general_community set taggedgalleries='".$all_gal."' where community_id = '".$res_gen['community_id']."'");
					
					$get_com_song = explode(",",$res_com['taggedsongs']);
					$dummy_song = array();
					for($count_song=0;$count_song<=count($get_com_song);$count_song++){
						if(in_array($get_com_song[$count_song],$dummy_song))
						{
						}
						else
						{
							$dummy_song[] = $get_com_song[$count_song];
						}
					}
					$newsong = explode(",",$song_id);
					for($count_song=0;$count_song<=count($dummy_song);$count_song++){
						if($dummy_song[$count_song]!=""){
							for($new_count_song=0;$new_count_song<=count($newsong);$new_count_song++){
								if($newsong[$new_count_song]!=""){
									if($newsong[$new_count_song]==$get_com_song[$count_song]){
										$get_com_song[$count_song]="";
									}
								}
							}
						}
					}
					$all_song="";
					for($new_count_xyz=0;$new_count_xyz<=count($get_com_song);$new_count_xyz++)
					{
						if($get_com_song[$new_count_xyz]!=""){
						$all_song = $all_song .",".$get_com_song[$new_count_xyz];
						}
					}
					mysql_query("update general_community set taggedsongs='".$all_song."' where community_id = '".$res_gen['community_id']."'");
					
					$get_com_video = explode(",",$res_com['taggedvideos']);
					$dummy_video = array();
					for($count_vid=0;$count_vid<=count($get_com_video);$count_vid++){
						if(in_array($get_com_video[$count_vid],$dummy_video))
						{
						}
						else
						{
							$dummy_video[] = $get_com_video[$count_vid];
						}
					}
					$newvideo = explode(",",$video_id);
					for($count_vid=0;$count_vid<=count($dummy_video);$count_vid++){
						if($dummy_video[$count_vid]!=""){
							for($new_count_video=0;$new_count_video<=count($newvideo);$new_count_video++){
								if($newvideo[$new_count_video]!=""){
									if($newvideo[$new_count_video]==$get_com_video[$count_vid]){
										$get_com_video[$count_vid]="";
									}
								}
							}
						}
					}
					$all_videos="";
					for($new_count_xyz=0;$new_count_xyz<=count($get_com_video);$new_count_xyz++)
					{
						if($get_com_video[$new_count_xyz]!=""){
						$all_videos = $all_videos .",".$get_com_video[$new_count_xyz];
						}
					}
					mysql_query("update general_community set taggedvideos='".$all_videos."' where community_id = '".$res_gen['community_id']."'");
					
					/***code for community project***//*
					$com_pro = mysql_query("select * from community_project where community_id ='".$res_gen['community_id']."'");
					if(mysql_num_rows($com_pro)>0){
						while($row_com_pro = mysql_fetch_assoc($com_pro)){
							$get_gal_compro = explode(",",$row_com_pro['tagged_galleries']);
							$dummy_gal = array();
							for($count_gal=0;$count_gal<=count($get_gal_compro);$count_gal++){
								if(in_array($get_gal_compro[$count_gal],$dummy_gal))
								{
								}
								else
								{
									$dummy_gal[] = $get_gal_compro[$count_gal];
								}
							}
							$newgal = explode(",",$gall_id);
							for($count_gal=0;$count_gal<=count($dummy_gal);$count_gal++){
								if($dummy_gal[$count_gal]!=""){
									for($new_count=0;$new_count<=count($newgal);$new_count++){
										if($newgal[$new_count]!=""){
											if($newgal[$new_count]==$get_gal_compro[$count_gal]){
												$get_gal_compro[$count_gal]="";
											}
										}
									}
								}
							}
							$all_gal ="";
							for($new_count_xyz=0;$new_count_xyz<=count($get_gal_compro);$new_count_xyz++)
							{
								if($get_gal_compro[$new_count_xyz]!=""){
								$all_gal = $all_gal .",".$get_gal_compro[$new_count_xyz];
								}
							}
							mysql_query("update community_project set tagged_galleries='".$all_gal."' where id = '".$row_com_pro['id']."'");
							
							$get_song_compro = explode(",",$row_com_pro['tagged_songs']);
							$dummy_song = array();
							for($count_song=0;$count_song<=count($get_song_compro);$count_song++){
								if(in_array($get_song_compro[$count_song],$dummy_song))
								{
								}
								else
								{
									$dummy_song[] = $get_song_compro[$count_song];
								}
							}
							$newsong = explode(",",$song_id);
							for($count_song=0;$count_song<=count($dummy_song);$count_song++){
								if($dummy_song[$count_song]!=""){
									for($new_count_song=0;$new_count_song<=count($newsong);$new_count_song++){
										if($newsong[$new_count_song]!=""){
											if($newsong[$new_count_song]==$get_song_compro[$count_song]){
												$get_song_compro[$count_song]="";
											}
										}
									}
								}
							}
							$all_song="";
							for($new_count_xyz=0;$new_count_xyz<=count($get_song_compro);$new_count_xyz++)
							{
								if($get_song_compro[$new_count_xyz]!=""){
								$all_song = $all_song .",".$get_song_compro[$new_count_xyz];
								}
							}
							mysql_query("update community_project set tagged_songs='".$all_song."' where id = '".$row_com_pro['id']."'");
							
							$get_video_compro = explode(",",$row_com_pro['tagged_videos']);
							$dummy_video = array();
							for($count_vid=0;$count_vid<=count($get_video_compro);$count_vid++){
								if(in_array($get_video_compro[$count_vid],$dummy_video))
								{
								}
								else
								{
									$dummy_video[] = $get_video_compro[$count_vid];
								}
							}
							$newvideo = explode(",",$video_id);
							for($count_vid=0;$count_vid<=count($dummy_video);$count_vid++){
								if($dummy_video[$count_vid]!=""){
									for($new_count_video=0;$new_count_video<=count($newvideo);$new_count_video++){
										if($newvideo[$new_count_video]!=""){
											if($newvideo[$new_count_video]==$get_video_compro[$count_vid]){
												$get_video_compro[$count_vid]="";
											}
										}
									}
								}
							}
							$all_videos="";
							for($new_count_xyz=0;$new_count_xyz<=count($get_video_compro);$new_count_xyz++)
							{
								if($get_video_compro[$new_count_xyz]!=""){
								$all_videos = $all_videos .",".$get_video_compro[$new_count_xyz];
								}
							}
							mysql_query("update community_project set tagged_videos='".$all_videos."' where id = '".$row_com_pro['id']."'");
						}
					}
					
					/***code for community project ends here***/
					/***code for community Event***//*
					$com_event = mysql_query("select * from community_event where community_id ='".$res_gen['community_id']."'");
					if(mysql_num_rows($com_event)>0){
						while($row_com_eve = mysql_fetch_assoc($com_event)){
							$get_gal_comeve = explode(",",$row_com_eve['tagged_galleries']);
							$dummy_gal = array();
							for($count_gal=0;$count_gal<=count($get_gal_comeve);$count_gal++){
								if(in_array($get_gal_comeve[$count_gal],$dummy_gal))
								{
								}
								else
								{
									$dummy_gal[] = $get_gal_comeve[$count_gal];
								}
							}
							$newgal = explode(",",$gall_id);
							for($count_gal=0;$count_gal<=count($dummy_gal);$count_gal++){
								if($dummy_gal[$count_gal]!=""){
									for($new_count=0;$new_count<=count($newgal);$new_count++){
										if($newgal[$new_count]!=""){
											if($newgal[$new_count]==$get_gal_comeve[$count_gal]){
												$get_gal_comeve[$count_gal]="";
											}
										}
									}
								}
							}
							$all_gal ="";
							for($new_count_xyz=0;$new_count_xyz<=count($get_gal_comeve);$new_count_xyz++)
							{
								if($get_gal_comeve[$new_count_xyz]!=""){
								$all_gal = $all_gal .",".$get_gal_comeve[$new_count_xyz];
								}
							}
							mysql_query("update community_event set tagged_galleries='".$all_gal."' where id = '".$row_com_eve['id']."'");
							
							$get_song_comeve = explode(",",$row_com_eve['tagged_songs']);
							$dummy_song = array();
							for($count_song=0;$count_song<=count($get_song_comeve);$count_song++){
								if(in_array($get_song_comeve[$count_song],$dummy_song))
								{
								}
								else
								{
									$dummy_song[] = $get_song_comeve[$count_song];
								}
							}
							$newsong = explode(",",$song_id);
							for($count_song=0;$count_song<=count($dummy_song);$count_song++){
								if($dummy_song[$count_song]!=""){
									for($new_count_song=0;$new_count_song<=count($newsong);$new_count_song++){
										if($newsong[$new_count_song]!=""){
											if($newsong[$new_count_song]==$get_song_comeve[$count_song]){
												$get_song_comeve[$count_song]="";
											}
										}
									}
								}
							}
							$all_song="";
							for($new_count_xyz=0;$new_count_xyz<=count($get_song_comeve);$new_count_xyz++)
							{
								if($get_song_comeve[$new_count_xyz]!=""){
								$all_song = $all_song .",".$get_song_comeve[$new_count_xyz];
								}
							}
							mysql_query("update community_event set tagged_songs='".$all_song."' where id = '".$row_com_eve['id']."'");
							
							$get_video_comeve = explode(",",$row_com_eve['tagged_videos']);
							$dummy_video = array();
							for($count_vid=0;$count_vid<=count($get_video_comeve);$count_vid++){
								if(in_array($get_video_comeve[$count_vid],$dummy_video))
								{
								}
								else
								{
									$dummy_video[] = $get_video_comeve[$count_vid];
								}
							}
							$newvideo = explode(",",$video_id);
							for($count_vid=0;$count_vid<=count($dummy_video);$count_vid++){
								if($dummy_video[$count_vid]!=""){
									for($new_count_video=0;$new_count_video<=count($newvideo);$new_count_video++){
										if($newvideo[$new_count_video]!=""){
											if($newvideo[$new_count_video]==$get_video_comeve[$count_vid]){
												$get_video_comeve[$count_vid]="";
											}
										}
									}
								}
							}
							$all_videos="";
							for($new_count_xyz=0;$new_count_xyz<=count($get_video_comeve);$new_count_xyz++)
							{
								if($get_video_comeve[$new_count_xyz]!=""){
								$all_videos = $all_videos .",".$get_video_comeve[$new_count_xyz];
								}
							}
							mysql_query("update community_event set tagged_videos='".$all_videos."' where id = '".$row_com_eve['id']."'");
						}
					}
					
					/***code for community Event ends here***//*
				}
			}
			/***Code for community ends here***//*
			
		}
	}
	
	function Add_remove_creator_user_media($gall_id,$song_id,$video_id,$new_gall_id,$new_song_id,$new_video_id,$id_name,$id){
		$sql= mysql_query("select * from general_user where ".$id_name."='".$id."'");
		if(mysql_num_rows($sql)>0){
			$res_gen = mysql_fetch_assoc($sql);
			if($res_gen['artist_id']>0 || $res_gen['artist_id']!=""){
				$art_det = mysql_query("select * from general_artist where artist_id = '".$res_gen['artist_id']."'");
				if(mysql_num_rows($art_det)>0){
					$res_art = mysql_fetch_assoc($art_det);
					$get_gal = explode(",",$res_art['taggedgalleries']);
					$dummy_gal = array();
					for($count_gal=0;$count_gal<=count($get_gal);$count_gal++){
						if(in_array($get_gal[$count_gal],$dummy_gal))
						{
						}
						else
						{
							$dummy_gal[] = $get_gal[$count_gal];
						}
					}
					$newgal = explode(",",$gall_id);
					for($count_gal=0;$count_gal<=count($dummy_gal);$count_gal++){
						if($dummy_gal[$count_gal]!=""){
							for($new_count=0;$new_count<=count($newgal);$new_count++){
								if($newgal[$new_count]!=""){
									if($newgal[$new_count]==$get_gal[$count_gal]){
										$get_gal[$count_gal]="";
									}
								}
							}
						}
					}
					$all_gal="";
					for($new_count_xyz=0;$new_count_xyz<=count($get_gal);$new_count_xyz++)
					{
						if($get_gal[$new_count_xyz]!=""){
						$all_gal = $all_gal .",".$get_gal[$new_count_xyz];
						}
					}
					$all_gal = $all_gal .",". $new_gall_id;
					mysql_query("update general_artist set taggedgalleries='".$all_gal."' where artist_id = '".$res_gen['artist_id']."'");
					
					$get_song = explode(",",$res_art['taggedsongs']);
					$dummy_song = array();
					for($count_song=0;$count_song<=count($get_song);$count_song++){
						if(in_array($get_song[$count_song],$dummy_song))
						{
						}
						else
						{
							$dummy_song[] = $get_song[$count_song];
						}
					}
					$newsong = explode(",",$song_id);
					for($count_song=0;$count_song<=count($dummy_song);$count_song++){
						if($dummy_song[$count_song]!=""){
							for($new_count_song=0;$new_count_song<=count($newsong);$new_count_song++){
								if($newsong[$new_count_song]!=""){
									if($newsong[$new_count_song]==$get_song[$count_song]){
										$get_song[$count_song]="";
									}
								}
							}
						}
					}
					$all_song ="";
					for($new_count_xyz=0;$new_count_xyz<=count($get_song);$new_count_xyz++)
					{
						if($get_song[$new_count_xyz]!=""){
						$all_song = $all_song .",".$get_song[$new_count_xyz];
						}
					}
					$all_song = $all_song .",". $new_song_id;
					mysql_query("update general_artist set taggedsongs='".$all_song."' where artist_id = '".$res_gen['artist_id']."'");
					
					$get_video = explode(",",$res_art['taggedvideos']);
					$dummy_video = array();
					for($count_vid=0;$count_vid<=count($get_video);$count_vid++){
						if(in_array($get_video[$count_vid],$dummy_video))
						{
						}
						else
						{
							$dummy_video[] = $get_video[$count_vid];
						}
					}
					$newvideo = explode(",",$video_id);
					for($count_vid=0;$count_vid<=count($dummy_video);$count_vid++){
						if($dummy_video[$count_vid]!=""){
							for($new_count_video=0;$new_count_video<=count($newvideo);$new_count_video++){
								if($newvideo[$new_count_video]!=""){
									if($newvideo[$new_count_video]==$get_video[$count_vid]){
										$get_video[$count_vid]="";
									}
								}
							}
						}
					}
					$all_videos ="";
					for($new_count_xyz=0;$new_count_xyz<=count($get_video);$new_count_xyz++)
					{
						if($get_video[$new_count_xyz]!=""){
						$all_videos = $all_videos .",".$get_video[$new_count_xyz];
						}
					}
					$all_videos = $all_videos .",". $new_video_id;
					mysql_query("update general_artist set taggedvideos='".$all_videos."' where artist_id = '".$res_gen['artist_id']."'");
					
					/***code for artist project***//*
					$art_pro = mysql_query("select * from artist_project where artist_id ='".$res_gen['artist_id']."'");
					if(mysql_num_rows($art_pro)>0){
						while($row_art_pro = mysql_fetch_assoc($art_pro)){
							$get_gal_artpro = explode(",",$row_art_pro['tagged_galleries']);
							$dummy_gal = array();
							for($count_gal=0;$count_gal<=count($get_gal_artpro);$count_gal++){
								if(in_array($get_gal_artpro[$count_gal],$dummy_gal))
								{
								}
								else
								{
									$dummy_gal[] = $get_gal_artpro[$count_gal];
								}
							}
							$newgal = explode(",",$gall_id);
							for($count_gal=0;$count_gal<=count($dummy_gal);$count_gal++){
								if($dummy_gal[$count_gal]!=""){
									for($new_count=0;$new_count<=count($newgal);$new_count++){
										if($newgal[$new_count]!=""){
											if($newgal[$new_count]==$get_gal_artpro[$count_gal]){
												$get_gal_artpro[$count_gal]="";
											}
										}
									}
								}
							}
							$all_gal ="";
							for($new_count_xyz=0;$new_count_xyz<=count($get_gal_artpro);$new_count_xyz++)
							{
								if($get_gal_artpro[$new_count_xyz]!=""){
								$all_gal = $all_gal .",".$get_gal_artpro[$new_count_xyz];
								}
							}
							$all_gal = $all_gal .",". $new_gall_id;
							mysql_query("update artist_project set tagged_galleries='".$all_gal."' where id = '".$row_art_pro['id']."'");
							
							$get_song_artpro = explode(",",$row_art_pro['tagged_songs']);
							$dummy_song = array();
							for($count_song=0;$count_song<=count($get_song_artpro);$count_song++){
								if(in_array($get_song_artpro[$count_song],$dummy_song))
								{
								}
								else
								{
									$dummy_song[] = $get_song_artpro[$count_song];
								}
							}
							$newsong = explode(",",$song_id);
							for($count_song=0;$count_song<=count($dummy_song);$count_song++){
								if($dummy_song[$count_song]!=""){
									for($new_count_song=0;$new_count_song<=count($newsong);$new_count_song++){
										if($newsong[$new_count_song]!=""){
											if($newsong[$new_count_song]==$get_song_artpro[$count_song]){
												$get_song_artpro[$count_song]="";
											}
										}
									}
								}
							}
							$all_song ="";
							for($new_count_xyz=0;$new_count_xyz<=count($get_song_artpro);$new_count_xyz++)
							{
								if($get_song_artpro[$new_count_xyz]!=""){
								$all_song = $all_song .",".$get_song_artpro[$new_count_xyz];
								}
							}
							$all_song = $all_song .",". $new_song_id;
							mysql_query("update artist_project set tagged_songs='".$all_song."' where id = '".$row_art_pro['id']."'");
							
							$get_video_artpro = explode(",",$row_art_pro['tagged_videos']);
							$dummy_video = array();
							for($count_vid=0;$count_vid<=count($get_video_artpro);$count_vid++){
								if(in_array($get_video_artpro[$count_vid],$dummy_video))
								{
								}
								else
								{
									$dummy_video[] = $get_video_artpro[$count_vid];
								}
							}
							$newvideo = explode(",",$video_id);
							for($count_vid=0;$count_vid<=count($dummy_video);$count_vid++){
								if($dummy_video[$count_vid]!=""){
									for($new_count_video=0;$new_count_video<=count($newvideo);$new_count_video++){
										if($newvideo[$new_count_video]!=""){
											if($newvideo[$new_count_video]==$get_video_artpro[$count_vid]){
												$get_video_artpro[$count_vid]="";
											}
										}
									}
								}
							}
							$all_videos="";
							for($new_count_xyz=0;$new_count_xyz<=count($get_video_artpro);$new_count_xyz++)
							{
								if($get_video_artpro[$new_count_xyz]!=""){
								$all_videos = $all_videos .",".$get_video_artpro[$new_count_xyz];
								}
							}
							$all_videos = $all_videos .",". $new_video_id;
							mysql_query("update artist_project set tagged_videos='".$all_videos."' where id = '".$row_art_pro['id']."'");
						}
					}
					
					/***code for artist project ends here***/
					/***code for artist Event***//*
					$art_event = mysql_query("select * from artist_event where artist_id ='".$res_gen['artist_id']."'");
					if(mysql_num_rows($art_event)>0){
						while($row_art_eve = mysql_fetch_assoc($art_event)){
							$get_gal_arteve = explode(",",$row_art_eve['tagged_galleries']);
							$dummy_gal = array();
							for($count_gal=0;$count_gal<=count($get_gal_arteve);$count_gal++){
								if(in_array($get_gal_arteve[$count_gal],$dummy_gal))
								{
								}
								else
								{
									$dummy_gal[] = $get_gal_arteve[$count_gal];
								}
							}
							$newgal = explode(",",$gall_id);
							for($count_gal=0;$count_gal<=count($dummy_gal);$count_gal++){
								if($dummy_gal[$count_gal]!=""){
									for($new_count=0;$new_count<=count($newgal);$new_count++){
										if($newgal[$new_count]!=""){
											if($newgal[$new_count]==$get_gal_arteve[$count_gal]){
												$get_gal_arteve[$count_gal]="";
											}
										}
									}
								}
							}
							$all_gal ="";
							for($new_count_xyz=0;$new_count_xyz<=count($get_gal_arteve);$new_count_xyz++)
							{
								if($get_gal_arteve[$new_count_xyz]!=""){
								$all_gal = $all_gal .",".$get_gal_arteve[$new_count_xyz];
								}
							}
							$all_gal = $all_gal .",". $new_gall_id;
							mysql_query("update artist_event set tagged_galleries='".$all_gal."' where id = '".$row_art_eve['id']."'");
							
							$get_song_arteve = explode(",",$row_art_eve['tagged_songs']);
							$dummy_song = array();
							for($count_song=0;$count_song<=count($get_song_arteve);$count_song++){
								if(in_array($get_song_arteve[$count_song],$dummy_song))
								{
								}
								else
								{
									$dummy_song[] = $get_song_arteve[$count_song];
								}
							}
							$newsong = explode(",",$song_id);
							for($count_song=0;$count_song<=count($dummy_song);$count_song++){
								if($dummy_song[$count_song]!=""){
									for($new_count_song=0;$new_count_song<=count($newsong);$new_count_song++){
										if($newsong[$new_count_song]!=""){
											if($newsong[$new_count_song]==$get_song_arteve[$count_song]){
												$get_song_arteve[$count_song]="";
											}
										}
									}
								}
							}
							$all_song ="";
							for($new_count_xyz=0;$new_count_xyz<=count($get_song_arteve);$new_count_xyz++)
							{
								if($get_song_arteve[$new_count_xyz]!=""){
								$all_song = $all_song .",".$get_song_arteve[$new_count_xyz];
								}
							}
							$all_song = $all_song .",". $new_song_id;
							mysql_query("update artist_event set tagged_songs='".$all_song."' where id = '".$row_art_eve['id']."'");
							
							$get_video_arteve = explode(",",$row_art_eve['tagged_videos']);
							$dummy_video = array();
							for($count_vid=0;$count_vid<=count($get_video_arteve);$count_vid++){
								if(in_array($get_video_arteve[$count_vid],$dummy_video))
								{
								}
								else
								{
									$dummy_video[] = $get_video_arteve[$count_vid];
								}
							}
							$newvideo = explode(",",$video_id);
							for($count_vid=0;$count_vid<=count($dummy_video);$count_vid++){
								if($dummy_video[$count_vid]!=""){
									for($new_count_video=0;$new_count_video<=count($newvideo);$new_count_video++){
										if($newvideo[$new_count_video]!=""){
											if($newvideo[$new_count_video]==$get_video_arteve[$count_vid]){
												$get_video_arteve[$count_vid]="";
											}
										}
									}
								}
							}
							$all_videos ="";
							for($new_count_xyz=0;$new_count_xyz<=count($get_video_arteve);$new_count_xyz++)
							{
								if($get_video_arteve[$new_count_xyz]!=""){
								$all_videos = $all_videos .",".$get_video_arteve[$new_count_xyz];
								}
							}
							$all_videos = $all_videos .",". $new_video_id;
							mysql_query("update artist_event set tagged_videos='".$all_videos."' where id = '".$row_art_eve['id']."'");
						}
					}
					
					/***code for artist Event ends here***//*
				}
			}
			/***Code for community***//*
			if($res_gen['community_id']>0 || $res_gen['community_id']!=""){
				$com_det = mysql_query("select * from general_community where community_id = '".$res_gen['community_id']."'");
				if(mysql_num_rows($com_det)>0){
					$res_com = mysql_fetch_assoc($com_det);
					$get_com_gal = explode(",",$res_com['taggedgalleries']);
					$dummy_gal = array();
					for($count_gal=0;$count_gal<=count($get_com_gal);$count_gal++){
						if(in_array($get_com_gal[$count_gal],$dummy_gal))
						{
						}
						else
						{
							$dummy_gal[] = $get_com_gal[$count_gal];
						}
					}
					$newgal = explode(",",$gall_id);
					for($count_gal=0;$count_gal<=count($dummy_gal);$count_gal++){
						if($dummy_gal[$count_gal]!=""){
							for($new_count=0;$new_count<=count($newgal);$new_count++){
								if($newgal[$new_count]!=""){
									if($newgal[$new_count]==$get_com_gal[$count_gal]){
										$get_com_gal[$count_gal]="";
									}
								}
							}
						}
					}
					$all_gal ="";
					for($new_count_xyz=0;$new_count_xyz<=count($get_com_gal);$new_count_xyz++)
					{
						if($get_com_gal[$new_count_xyz]!=""){
						$all_gal = $all_gal .",".$get_com_gal[$new_count_xyz];
						}
					}
					$all_gal = $all_gal .",". $new_gall_id;
					mysql_query("update general_community set taggedgalleries='".$all_gal."' where community_id = '".$res_gen['community_id']."'");
					
					$get_com_song = explode(",",$res_com['taggedsongs']);
					$dummy_song = array();
					for($count_song=0;$count_song<=count($get_com_song);$count_song++){
						if(in_array($get_com_song[$count_song],$dummy_song))
						{
						}
						else
						{
							$dummy_song[] = $get_com_song[$count_song];
						}
					}
					$newsong = explode(",",$song_id);
					for($count_song=0;$count_song<=count($dummy_song);$count_song++){
						if($dummy_song[$count_song]!=""){
							for($new_count_song=0;$new_count_song<=count($newsong);$new_count_song++){
								if($newsong[$new_count_song]!=""){
									if($newsong[$new_count_song]==$get_com_song[$count_song]){
										$get_com_song[$count_song]="";
									}
								}
							}
						}
					}
					$all_song="";
					for($new_count_xyz=0;$new_count_xyz<=count($get_com_song);$new_count_xyz++)
					{
						if($get_com_song[$new_count_xyz]!=""){
						$all_song = $all_song .",".$get_com_song[$new_count_xyz];
						}
					}
					$all_song = $all_song .",". $new_song_id;
					mysql_query("update general_community set taggedsongs='".$all_song."' where community_id = '".$res_gen['community_id']."'");
					
					$get_com_video = explode(",",$res_com['taggedvideos']);
					$dummy_video = array();
					for($count_vid=0;$count_vid<=count($get_com_video);$count_vid++){
						if(in_array($get_com_video[$count_vid],$dummy_video))
						{
						}
						else
						{
							$dummy_video[] = $get_com_video[$count_vid];
						}
					}
					$newvideo = explode(",",$video_id);
					for($count_vid=0;$count_vid<=count($dummy_video);$count_vid++){
						if($dummy_video[$count_vid]!=""){
							for($new_count_video=0;$new_count_video<=count($newvideo);$new_count_video++){
								if($newvideo[$new_count_video]!=""){
									if($newvideo[$new_count_video]==$get_com_video[$count_vid]){
										$get_com_video[$count_vid]="";
									}
								}
							}
						}
					}
					$all_videos="";
					for($new_count_xyz=0;$new_count_xyz<=count($get_com_video);$new_count_xyz++)
					{
						if($get_com_video[$new_count_xyz]!=""){
						$all_videos = $all_videos .",".$get_com_video[$new_count_xyz];
						}
					}
					$all_videos = $all_videos .",". $new_video_id;
					mysql_query("update general_community set taggedvideos='".$all_videos."' where community_id = '".$res_gen['community_id']."'");
					
					/***code for community project***//*
					$com_pro = mysql_query("select * from community_project where community_id ='".$res_gen['community_id']."'");
					if(mysql_num_rows($com_pro)>0){
						while($row_com_pro = mysql_fetch_assoc($com_pro)){
							$get_gal_compro = explode(",",$row_com_pro['tagged_galleries']);
							$dummy_gal = array();
							for($count_gal=0;$count_gal<=count($get_gal_compro);$count_gal++){
								if(in_array($get_gal_compro[$count_gal],$dummy_gal))
								{
								}
								else
								{
									$dummy_gal[] = $get_gal_compro[$count_gal];
								}
							}
							$newgal = explode(",",$gall_id);
							for($count_gal=0;$count_gal<=count($dummy_gal);$count_gal++){
								if($dummy_gal[$count_gal]!=""){
									for($new_count=0;$new_count<=count($newgal);$new_count++){
										if($newgal[$new_count]!=""){
											if($newgal[$new_count]==$get_gal_compro[$count_gal]){
												$get_gal_compro[$count_gal]="";
											}
										}
									}
								}
							}
							$all_gal ="";
							for($new_count_xyz=0;$new_count_xyz<=count($get_gal_compro);$new_count_xyz++)
							{
								if($get_gal_compro[$new_count_xyz]!=""){
								$all_gal = $all_gal .",".$get_gal_compro[$new_count_xyz];
								}
							}
							$all_gal = $all_gal .",". $new_gall_id;
							mysql_query("update community_project set tagged_galleries='".$all_gal."' where id = '".$row_com_pro['id']."'");
							
							$get_song_compro = explode(",",$row_com_pro['tagged_songs']);
							$dummy_song = array();
							for($count_song=0;$count_song<=count($get_song_compro);$count_song++){
								if(in_array($get_song_compro[$count_song],$dummy_song))
								{
								}
								else
								{
									$dummy_song[] = $get_song_compro[$count_song];
								}
							}
							$newsong = explode(",",$song_id);
							for($count_song=0;$count_song<=count($dummy_song);$count_song++){
								if($dummy_song[$count_song]!=""){
									for($new_count_song=0;$new_count_song<=count($newsong);$new_count_song++){
										if($newsong[$new_count_song]!=""){
											if($newsong[$new_count_song]==$get_song_compro[$count_song]){
												$get_song_compro[$count_song]="";
											}
										}
									}
								}
							}
							$all_song="";
							for($new_count_xyz=0;$new_count_xyz<=count($get_song_compro);$new_count_xyz++)
							{
								if($get_song_compro[$new_count_xyz]!=""){
								$all_song = $all_song .",".$get_song_compro[$new_count_xyz];
								}
							}
							$all_song = $all_song .",". $new_song_id;
							mysql_query("update community_project set tagged_songs='".$all_song."' where id = '".$row_com_pro['id']."'");
							
							$get_video_compro = explode(",",$row_com_pro['tagged_videos']);
							$dummy_video = array();
							for($count_vid=0;$count_vid<=count($get_video_compro);$count_vid++){
								if(in_array($get_video_compro[$count_vid],$dummy_video))
								{
								}
								else
								{
									$dummy_video[] = $get_video_compro[$count_vid];
								}
							}
							$newvideo = explode(",",$video_id);
							for($count_vid=0;$count_vid<=count($dummy_video);$count_vid++){
								if($dummy_video[$count_vid]!=""){
									for($new_count_video=0;$new_count_video<=count($newvideo);$new_count_video++){
										if($newvideo[$new_count_video]!=""){
											if($newvideo[$new_count_video]==$get_video_compro[$count_vid]){
												$get_video_compro[$count_vid]="";
											}
										}
									}
								}
							}
							$all_videos="";
							for($new_count_xyz=0;$new_count_xyz<=count($get_video_compro);$new_count_xyz++)
							{
								if($get_video_compro[$new_count_xyz]!=""){
								$all_videos = $all_videos .",".$get_video_compro[$new_count_xyz];
								}
							}
							$all_videos = $all_videos .",". $new_video_id;
							mysql_query("update community_project set tagged_videos='".$all_videos."' where id = '".$row_com_pro['id']."'");
						}
					}
					
					/***code for community project ends here***/
					/***code for community Event***//*
					$com_event = mysql_query("select * from community_event where community_id ='".$res_gen['community_id']."'");
					if(mysql_num_rows($com_event)>0){
						while($row_com_eve = mysql_fetch_assoc($com_event)){
							$get_gal_comeve = explode(",",$row_com_eve['tagged_galleries']);
							$dummy_gal = array();
							for($count_gal=0;$count_gal<=count($get_gal_comeve);$count_gal++){
								if(in_array($get_gal_comeve[$count_gal],$dummy_gal))
								{
								}
								else
								{
									$dummy_gal[] = $get_gal_comeve[$count_gal];
								}
							}
							$newgal = explode(",",$gall_id);
							for($count_gal=0;$count_gal<=count($dummy_gal);$count_gal++){
								if($dummy_gal[$count_gal]!=""){
									for($new_count=0;$new_count<=count($newgal);$new_count++){
										if($newgal[$new_count]!=""){
											if($newgal[$new_count]==$get_gal_comeve[$count_gal]){
												$get_gal_comeve[$count_gal]="";
											}
										}
									}
								}
							}
							$all_gal ="";
							for($new_count_xyz=0;$new_count_xyz<=count($get_gal_comeve);$new_count_xyz++)
							{
								if($get_gal_comeve[$new_count_xyz]!=""){
								$all_gal = $all_gal .",".$get_gal_comeve[$new_count_xyz];
								}
							}
							$all_gal = $all_gal .",". $new_gall_id;
							mysql_query("update community_event set tagged_galleries='".$all_gal."' where id = '".$row_com_eve['id']."'");
							
							$get_song_comeve = explode(",",$row_com_eve['tagged_songs']);
							$dummy_song = array();
							for($count_song=0;$count_song<=count($get_song_comeve);$count_song++){
								if(in_array($get_song_comeve[$count_song],$dummy_song))
								{
								}
								else
								{
									$dummy_song[] = $get_song_comeve[$count_song];
								}
							}
							$newsong = explode(",",$song_id);
							for($count_song=0;$count_song<=count($dummy_song);$count_song++){
								if($dummy_song[$count_song]!=""){
									for($new_count_song=0;$new_count_song<=count($newsong);$new_count_song++){
										if($newsong[$new_count_song]!=""){
											if($newsong[$new_count_song]==$get_song_comeve[$count_song]){
												$get_song_comeve[$count_song]="";
											}
										}
									}
								}
							}
							$all_song="";
							for($new_count_xyz=0;$new_count_xyz<=count($get_song_comeve);$new_count_xyz++)
							{
								if($get_song_comeve[$new_count_xyz]!=""){
								$all_song = $all_song .",".$get_song_comeve[$new_count_xyz];
								}
							}
							$all_song = $all_song .",". $new_song_id;
							mysql_query("update community_event set tagged_songs='".$all_song."' where id = '".$row_com_eve['id']."'");
							
							$get_video_comeve = explode(",",$row_com_eve['tagged_videos']);
							$dummy_video = array();
							for($count_vid=0;$count_vid<=count($get_video_comeve);$count_vid++){
								if(in_array($get_video_comeve[$count_vid],$dummy_video))
								{
								}
								else
								{
									$dummy_video[] = $get_video_comeve[$count_vid];
								}
							}
							$newvideo = explode(",",$video_id);
							for($count_vid=0;$count_vid<=count($dummy_video);$count_vid++){
								if($dummy_video[$count_vid]!=""){
									for($new_count_video=0;$new_count_video<=count($newvideo);$new_count_video++){
										if($newvideo[$new_count_video]!=""){
											if($newvideo[$new_count_video]==$get_video_comeve[$count_vid]){
												$get_video_comeve[$count_vid]="";
											}
										}
									}
								}
							}
							$all_videos="";
							for($new_count_xyz=0;$new_count_xyz<=count($get_video_comeve);$new_count_xyz++)
							{
								if($get_video_comeve[$new_count_xyz]!=""){
								$all_videos = $all_videos .",".$get_video_comeve[$new_count_xyz];
								}
							}
							$all_videos = $all_videos .",". $new_video_id;
							mysql_query("update community_event set tagged_videos='".$all_videos."' where id = '".$row_com_eve['id']."'");
						}
					}
					
					/***code for community Event ends here***//*
				}
			}
			/***Code for community ends here***//*
			
		}
	}*/
	
	function get_creator_id($table_name,$id)
	{
		$sql = mysql_query("select * from ".$table_name." where id ='".$id."'");
		if(mysql_num_rows($sql)>0){
			$res = mysql_fetch_assoc($sql);
		}
		return($res);
	}
	
	function get_media_create($type)
	{
		$id = $this->selgeneral();
	  
		$var_type_1 = 'general_artist';
		$var_type_2 = 'general_community';
		$var_type_3 = 'artist_project';
		$var_type_4 = 'community_project';
	  
		$pal_1 = array();
		$pal_2 = array();
		$pal_3 = array();
		$pal_4 = array();
	  
		$final_c_f = array();
	  
		/* if($id['artist_id']!=0 && !empty($id['artist_id']))
		{
			$chk_type_a = $var_type_1.'|'.$id['artist_id'];

			$sql = mysql_query("SELECT * FROM general_media WHERE creator_info='".$chk_type_a."' AND delete_status=0 AND media_status=0 AND media_type='".$type."' AND general_user_id!='".$id['general_user_id']."'");
			if(mysql_num_rows($sql)>0)
			{
				while($row_a = mysql_fetch_assoc($sql))
				{
					$final_c_f[] = $row_a;
				}
			}
		}
	  
		if($id['community_id']!=0 && !empty($id['community_id']))
		{
			$chk_type_c = $var_type_2.'|'.$id['community_id'];
	   
			$sql = mysql_query("SELECT * FROM general_media WHERE creator_info='".$chk_type_c."' AND delete_status=0 AND media_status=0 AND media_type='".$type."' AND general_user_id!='".$id['general_user_id']."'");
			if(mysql_num_rows($sql)>0)
			{
				while($row_c = mysql_fetch_assoc($sql))
				{
					$final_c_f[] = $row_c;
				}    
			}
		} */
	  
		if($id['artist_id']!=0 && !empty($id['artist_id']))
		{   
			$sql_get_allp = mysql_query("SELECT * FROM artist_project WHERE artist_id='".$id['artist_id']."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_get_allp)>0)
			{
				while($rowal = mysql_fetch_assoc($sql_get_allp))
				{
					$pal_1[] = $rowal;
				}
			}
	   
			$sql_get_alle = mysql_query("SELECT * FROM artist_event WHERE artist_id='".$id['artist_id']."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_get_alle)>0)
			{
				while($rowael = mysql_fetch_assoc($sql_get_alle))
				{
					$pal_3[] = $rowael;
				}
			}
		   
			if(!empty($pal_1) && count($pal_1))
			{
				for($pl=0;$pl<count($pal_1);$pl++)
				{
					$chk_typep_1 = 'artist_project|'.$pal_1[$pl]['id'];
			 
					$sql_as = mysql_query("SELECT * FROM general_media WHERE creator_info='".$chk_typep_1."' AND delete_status=0 AND media_status=0 AND media_type='".$type."' AND general_user_id!='".$id['general_user_id']."'");
					if(mysql_num_rows($sql_as)>0)
					{
						while($row_ap = mysql_fetch_assoc($sql_as))
						{
							$final_c_f[] = $row_ap;
						}
					}
				}
			}
			
			if(!empty($pal_1) && count($pal_1))
			{
				for($pl=0;$pl<count($pal_1);$pl++)
				{
					$chk_typep_1 = 'artist_project|'.$pal_1[$pl]['id'];
			 
					$sql_as = mysql_query("SELECT * FROM general_media WHERE from_info='".$chk_typep_1."' AND delete_status=0 AND media_status=0 AND media_type='".$type."' AND general_user_id!='".$id['general_user_id']."'");
					if(mysql_num_rows($sql_as)>0)
					{
						while($row_ap = mysql_fetch_assoc($sql_as))
						{
							$final_c_f[] = $row_ap;
						}
					}
				}
			}
			
			/* if(!empty($pal_1) && count($pal_1))
			{
				for($pl=0;$pl<count($pal_1);$pl++)
				{
					$chk_typep_1 = 'artist_project|'.$pal_1[$pl]['id'];
			 
					$sql_as = mysql_query("SELECT * FROM general_media WHERE from_info='".$chk_typep_1."' AND delete_status=0 AND media_status=0 AND media_type='".$type."' AND general_user_id!='".$id['general_user_id']."'");
					if(mysql_num_rows($sql_as)>0)
					{
						while($row_ap = mysql_fetch_assoc($sql_as))
						{
							$final_c_f[] = $row_ap;
						}
					}
				}
			} */
		   
			/* if(!empty($pal_3) && count($pal_3))
			{
				for($pl=0;$pl<count($pal_3);$pl++)
				{
					$chk_typep_1 = 'artist_event|'.$pal_3[$pl]['id'];
			 
					$sql_as = mysql_query("SELECT * FROM general_media WHERE from_info='".$chk_typep_1."' AND delete_status=0 AND media_status=0 AND media_type='".$type."' AND general_user_id!='".$id['general_user_id']."'");
					if(mysql_num_rows($sql_as)>0)
					{
						while($row_ae = mysql_fetch_assoc($sql_as))
						{
							$final_c_f[] = $row_ae;
						}
					}
				}
			} */
		}
	  
		/* if($id['community_id']!=0 && !empty($id['community_id']))
		{   
			$sql_get_allp = mysql_query("SELECT * FROM community_project WHERE community_id='".$id['community_id']."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_get_allp)>0)
			{
				while($rowal = mysql_fetch_assoc($sql_get_allp))
				{
					$pal_2[] = $rowal;
				}
			}
		   
			$sql_get_allew = mysql_query("SELECT * FROM community_event WHERE community_id='".$id['community_id']."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_get_allew)>0)
			{
				while($rowawl = mysql_fetch_assoc($sql_get_allew))
				{
					$pal_4[] = $rowawl;
				}
			}
		   
			if(!empty($pal_2) && count($pal_2))
			{
				for($pl=0;$pl<count($pal_1);$pl++)
				{
					$chk_typep_1 = 'community_project|'.$pal_1[$pl]['id'];
			 
					$sql_as = mysql_query("SELECT * FROM general_media WHERE creator_info='".$chk_typep_1."' AND delete_status=0 AND media_status=0 AND media_type='".$type."' AND general_user_id!='".$id['general_user_id']."'");
					if(mysql_num_rows($sql_as)>0)
					{
						while($row_cp = mysql_fetch_assoc($sql_as))
						{
							$final_c_f[] = $row_cp;
						}
					}
				}
			}
		   
			if(!empty($pal_4) && count($pal_4))
			{
				for($pl=0;$pl<count($pal_4);$pl++)
				{
					$chk_typep_1 = 'community_event|'.$pal_4[$pl]['id'];
			 
					$sql_as = mysql_query("SELECT * FROM general_media WHERE from_info='".$chk_typep_1."' AND delete_status=0 AND media_status=0 AND media_type='".$type."' AND general_user_id!='".$id['general_user_id']."'");
					if(mysql_num_rows($sql_as)>0)
					{
						while($row_ce = mysql_fetch_assoc($sql_as))
						{
							$final_c_f[] = $row_ce;
						}
					}
				}
			}
		} */
		//$final_c_f = array_merge($final_c_f1,$final_c_f2);
		return $final_c_f;
	}
	
	function ups_creators($ty_tab,$ty_id,$pe_id)
	{
		if($ty_tab=='general_artist')
		{
			$up_taggedprojects = "";
			$sql = mysql_query("SELECT * FROM general_artist WHERE artist_id='".$ty_id."'");
			if(mysql_num_rows($sql)>0)
			{
				$row = mysql_fetch_assoc($sql);
				if($row['taggedprojects']!="" && !empty($row['taggedprojects']))
				{
					$up_taggedprojects = $row['taggedprojects'].','.$pe_id.'~art';
				}
				else
				{
					$up_taggedprojects = $pe_id.'~art';
				}
				
				$rem_up = mysql_query("SELECT * FROM general_artist WHERE taggedprojects LIKE '%".$pe_id."~art%' AND artist_id='".$ty_id."'");
				if(mysql_num_rows($rem_up)==0)
				{
					mysql_query("UPDATE general_artist SET taggedprojects='".$up_taggedprojects."' WHERE artist_id='".$ty_id."'");
				}
			}
		}
		
		if($ty_tab=='general_community')
		{
			$up_taggedprojects = "";
			$sql = mysql_query("SELECT * FROM general_community WHERE community_id='".$ty_id."'");
			if(mysql_num_rows($sql)>0)
			{
				$row = mysql_fetch_assoc($sql);
				if($row['taggedprojects']!="" && !empty($row['taggedprojects']))
				{
					$up_taggedprojects = $row['taggedprojects'].','.$pe_id.'~art';
				}
				else
				{
					$up_taggedprojects = $pe_id.'~art';
				}
				
				$rem_up = mysql_query("SELECT * FROM general_community WHERE taggedprojects LIKE '%".$pe_id."~art%' AND community_id='".$ty_id."'");
				if(mysql_num_rows($rem_up)==0)
				{
					mysql_query("UPDATE general_community SET taggedprojects='".$up_taggedprojects."' WHERE community_id='".$ty_id."'");
				}
			}
		}
		
		if($ty_tab=='artist_project')
		{
			$up_taggedprojects = "";
			$sql = mysql_query("SELECT * FROM artist_project WHERE id='".$ty_id."'");
			if(mysql_num_rows($sql)>0)
			{
				$row = mysql_fetch_assoc($sql);
				if($row['tagged_projects']!="" && !empty($row['tagged_projects']))
				{
					$up_taggedprojects = $row['tagged_projects'].','.$pe_id.'~art';
				}
				else
				{
					$up_taggedprojects = $pe_id.'~art';
				}
				
				$rem_up = mysql_query("SELECT * FROM artist_project WHERE tagged_projects LIKE '%".$pe_id."~art%' AND id='".$ty_id."'");
				if(mysql_num_rows($rem_up)==0)
				{
					mysql_query("UPDATE artist_project SET tagged_projects='".$up_taggedprojects."' WHERE id='".$ty_id."'");
				}
			}
		}
		
		if($ty_tab=='community_project')
		{
			$up_taggedprojects = "";
			$sql = mysql_query("SELECT * FROM community_project WHERE id='".$ty_id."'");
			if(mysql_num_rows($sql)>0)
			{
				$row = mysql_fetch_assoc($sql);
				if($row['tagged_projects']!="" && !empty($row['tagged_projects']))
				{
					$up_taggedprojects = $row['tagged_projects'].','.$pe_id.'~art';
				}
				else
				{
					$up_taggedprojects = $pe_id.'~art';
				}
				
				$rem_up = mysql_query("SELECT * FROM community_project WHERE tagged_projects LIKE '%".$pe_id."~art%' AND id='".$ty_id."'");
				if(mysql_num_rows($rem_up)==0)
				{
					mysql_query("UPDATE community_project SET tagged_projects='".$up_taggedprojects."' WHERE id='".$ty_id."'");
				}
			}
		}
		// || $ty_tab=='general_community'c
	}
	
	/* function only_creator_this($id)
	{
		$new_id = $this->selgeneral();
		$final_pros = array();
		
		if(isset($new_id['artist_id']) && !empty($new_id['artist_id']) && $new_id['artist_id']!=0)
		{
			$mcth = 'artist_project|'.$id;
			
			$sql_apro = mysql_query("SELECT * FROM artist_project WHERE creators_info='".$mcth."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_apro)>0)
			{
				while($ans_apro = mysql_fetch_assoc($sql_apro))
				{				
					$ans_apro['id'] = $ans_apro['id'].'~art';
					$final_pros[] = $ans_apro;
				}	
			}
			
			$sql_cpro = mysql_query("SELECT * FROM community_project WHERE creators_info='".$mcth."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_cpro)>0)
			{
				while($ans_cpro = mysql_fetch_assoc($sql_cpro))
				{				
					$ans_cpro['id'] = $ans_cpro['id'].'~art';
					$final_pros[] = $ans_cpro;
				}
			}
		}
		
		return $final_pros;
	} */
	
	function only_creator_this()
	{
		$id = $this->selgeneral();
		$var_type_1 = 'general_artist';
		$var_type_2 = 'general_community';
		$var_type_3 = 'artist_project';
		$var_type_4 = 'community_project';
		
		$artm = array();
		$arta = array();
		$comm = array();
		$coma = array();
		
		$artme = array();
		$artae = array();
		$comme = array();
		$comae = array();
		
		$art_p_ao = array();
		$com_p_ao = array();
		$art_p_co = array();
		$com_p_co = array();
		
		$art_e_ao = array();
		$com_e_ao = array();
		$art_e_co = array();
		$com_e_co = array();
		
		$pal_1 = array();
		$pal_2 = array();
		
		if($id['artist_id']!=0 && !empty($id['artist_id']))
		{
			$chk_type = $var_type_1.'|'.$id['artist_id'];
			
			$sql_get_allp = mysql_query("SELECT * FROM artist_project WHERE artist_id='".$id['artist_id']."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_get_allp)>0)
			{
				while($rowal = mysql_fetch_assoc($sql_get_allp))
				{	
					$rowal['whos_project'] = "only_creator";
					$pal_1[] = $rowal;
				}
			}
			
			if(!empty($pal_1) && count($pal_1))
			{
				for($pl=0;$pl<count($pal_1);$pl++)
				{
					$chk_typep_1 = 'artist_project|'.$pal_1[$pl]['id'];
					$sql = mysql_query("SELECT * FROM artist_project WHERE creators_info='".$chk_typep_1."' AND del_status=0 AND active=0");
					if(mysql_num_rows($sql)>0)
					{
						while($row_po = mysql_fetch_assoc($sql))
						{
							if($row_po['artist_id']!=$id['artist_id'])
							{
								$row_po['id'] = $row_po['id'].'~art';
								$row_po['whos_project'] = "only_creator";
								$art_p_ao[] = $row_po;
							}
						}
					}
					
					$sql = mysql_query("SELECT * FROM community_project WHERE creators_info='".$chk_typep_1."' AND del_status=0 AND active=0");
					if(mysql_num_rows($sql)>0)
					{
						while($row_poo = mysql_fetch_assoc($sql))
						{
							if($row_poo['community_id']!=$id['community_id'])
							{
								$row_poo['id'] = $row_poo['id'].'~com';
								$row_poo['whos_project'] = "only_creator";
								$com_p_ao[] = $row_poo;
							}
						}
					}
				}
			}
			
			/* if(!empty($pal_1) && count($pal_1))
			{
				for($pl=0;$pl<count($pal_1);$pl++)
				{
					$chk_typep_1 = 'artist_project|'.$pal_1[$pl]['id'];
					$sql = mysql_query("SELECT * FROM artist_event WHERE creators_info='".$chk_typep_1."' AND del_status=0 AND active=0");
					if(mysql_num_rows($sql)>0)
					{
						while($row_po = mysql_fetch_assoc($sql))
						{
							if($row_po['artist_id']!=$id['artist_id'])
							{
								$art_e_ao[] = $row_po;
							}
						}
					}
					
					$sql = mysql_query("SELECT * FROM community_event WHERE creators_info='".$chk_typep_1."' AND del_status=0 AND active=0");
					if(mysql_num_rows($sql)>0)
					{
						while($row_poo = mysql_fetch_assoc($sql))
						{
							if($row_poo['community_id']!=$id['community_id'])
							{
								$com_e_ao[] = $row_poo;
							}
						}
					}
				}
			} */
			
			$sql_pro_arta = mysql_query("SELECT * FROM artist_project WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_pro_arta)>0)
			{
				while($rowaa = mysql_fetch_assoc($sql_pro_arta))
				{
					if($rowaa['artist_id']!=$id['artist_id'])
					{
						$rowaa['id'] = $rowaa['id'].'~art';
						$rowaa['whos_project'] = "only_creator";
						$arta[] = $rowaa;
					}
				}
			}
			
			/* $sql_pro_arta = mysql_query("SELECT * FROM artist_event WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_pro_arta)>0)
			{
				while($rowaa = mysql_fetch_assoc($sql_pro_arta))
				{
					if($rowaa['artist_id']!=$id['artist_id'])
					{
						$artae[] = $rowaa;
					}
				}
			} */
			
			$sql_pro_coma = mysql_query("SELECT * FROM community_project WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_pro_coma)>0)
			{
				while($rowam = mysql_fetch_assoc($sql_pro_coma))
				{
					if($rowam['community_id']!=$id['community_id'])
					{
						$rowam['id'] = $rowam['id'].'~com';
						$rowam['whos_project'] = "only_creator";
						$coma[] = $rowam;
					}
				}
			}
			
			/* $sql_pro_coma = mysql_query("SELECT * FROM community_event WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_pro_coma)>0)
			{
				while($rowam = mysql_fetch_assoc($sql_pro_coma))
				{
					if($rowam['community_id']!=$id['community_id'])
					{
						$comae[] = $rowam;
					}
				}
			} */
		}
		
		if($id['community_id']!=0 && !empty($id['community_id']))
		{
			$chk_type = $var_type_2.'|'.$id['community_id'];
			$sql_pro_artm = mysql_query("SELECT * FROM artist_project WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_pro_artm)>0)
			{
				while($rowcm = mysql_fetch_assoc($sql_pro_artm))
				{
					if($rowcm['artist_id']!=$id['artist_id'])
					{
						$rowcm['id'] = $rowcm['id'].'~art';
						$rowcm['whos_project'] = "only_creator";
						$artm[] = $rowcm;
					}
				}
			}
			
			/* $chk_type = $var_type_2.'|'.$id['community_id'];
			$sql_pro_artm = mysql_query("SELECT * FROM artist_event WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_pro_artm)>0)
			{
				while($rowcm = mysql_fetch_assoc($sql_pro_artm))
				{
					if($rowcm['artist_id']!=$id['artist_id'])
					{
						$artme[] = $rowcm;
					}
				}
			} */
			
			$sql_pro_comm = mysql_query("SELECT * FROM community_project WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_pro_comm)>0)
			{
				while($rowca = mysql_fetch_assoc($sql_pro_comm))
				{
					if($rowca['community_id']!=$id['community_id'])
					{
						$rowca['id'] = $rowca['id'].'~com';
						$rowca['whos_project'] = "only_creator";
						$comm[] = $rowca;
					}
				}
			}
			
			/* $sql_pro_comm = mysql_query("SELECT * FROM community_event WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_pro_comm)>0)
			{
				while($rowca = mysql_fetch_assoc($sql_pro_comm))
				{
					if($rowca['community_id']!=$id['community_id'])
					{
						$comme[] = $rowca;
					}
				}
			} */
			
			$sql_get_allp = mysql_query("SELECT * FROM community_project WHERE community_id='".$id['community_id']."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_get_allp)>0)
			{
				while($rowal = mysql_fetch_assoc($sql_get_allp))
				{
					$rowal['whos_project'] = "only_creator";
					$pal_2[] = $rowal;
				}
			}
			
			if(!empty($pal_2) && count($pal_2))
			{
				for($pl2=0;$pl2<count($pal_2);$pl2++)
				{
					$chk_typep_2 = 'community_project|'.$pal_2[$pl2]['id'];
					$sql = mysql_query("SELECT * FROM artist_project WHERE creators_info='".$chk_typep_2."' AND del_status=0 AND active=0");
					if(mysql_num_rows($sql)>0)
					{
						while($row_po = mysql_fetch_assoc($sql))
						{
							if($row_po['artist_id']!=$id['artist_id'])
							{
								$row_po['id'] = $row_po['id'].'~art';
								$row_po['whos_project'] = "only_creator";
								$art_p_co[] = $row_po;
							}
						}
					}
					
					$sql = mysql_query("SELECT * FROM community_project WHERE creators_info='".$chk_typep_2."' AND del_status=0 AND active=0");
					if(mysql_num_rows($sql)>0)
					{
						while($row_poo = mysql_fetch_assoc($sql))
						{
							if($row_poo['community_id']!=$id['community_id'])
							{
								$row_poo['id'] = $row_poo['id'].'~com';
								$row_poo['whos_project'] = "only_creator";
								$com_p_co[] = $row_poo;
							}
						}
					}
				}
			}
			
			/* if(!empty($pal_2) && count($pal_2))
			{
				for($pl2=0;$pl2<count($pal_2);$pl2++)
				{
					$chk_typep_2 = 'community_project|'.$pal_2[$pl2]['id'];
					$sql = mysql_query("SELECT * FROM artist_event WHERE creators_info='".$chk_typep_2."' AND del_status=0 AND active=0");
					if(mysql_num_rows($sql)>0)
					{
						while($row_po = mysql_fetch_assoc($sql))
						{
							if($row_po['artist_id']!=$id['artist_id'])
							{
								$art_e_co[] = $row_po;
							}
						}
					}
					
					$sql = mysql_query("SELECT * FROM community_event WHERE creators_info='".$chk_typep_2."' AND del_status=0 AND active=0");
					if(mysql_num_rows($sql)>0)
					{
						while($row_poo = mysql_fetch_assoc($sql))
						{
							if($row_poo['community_id']!=$id['community_id'])
							{
								$com_e_co[] = $row_poo;
							}
						}
					}
				}
			} */
		}
		
		$totals = array();
		$totals = array_merge($artm,$arta,$comm,$coma,$art_p_ao,$com_p_ao,$art_p_co,$com_p_co,$artme,$artae,$comme,$comae,$art_e_ao,$com_e_ao,$art_e_co,$com_e_co);
		return $totals;
	}
	
	/* function only_creator_upevent($id)
	{
		$date = date("Y-m-d");
		
		$new_id = $this->selgeneral();
		$final_pros = array();
		
		if(isset($new_id['artist_id']) && !empty($new_id['artist_id']) && $new_id['artist_id']!=0)
		{
			$mcth = 'artist_project|'.$id;
			
			$sql_apro = mysql_query("SELECT * FROM artist_event WHERE creators_info='".$mcth."' AND del_status=0 AND active=0 AND date>='".$date."'");
			if(mysql_num_rows($sql_apro)>0)
			{
				while($ans_apro = mysql_fetch_assoc($sql_apro))
				{				
					$ans_apro['id'] = $ans_apro['id'].'~art';
					$final_pros[] = $ans_apro;
				}	
			}
			
			$sql_cpro = mysql_query("SELECT * FROM community_event WHERE creators_info='".$mcth."' AND del_status=0 AND active=0 AND date>='".$date."'");
			if(mysql_num_rows($sql_cpro)>0)
			{
				while($ans_cpro = mysql_fetch_assoc($sql_cpro))
				{				
					$ans_cpro['id'] = $ans_cpro['id'].'~com';
					$final_pros[] = $ans_cpro;
				}
			}
		}
		
		return $final_pros;
	}
	
	function only_creator_recevent($id)
	{
		$date = date("Y-m-d");
		
		$new_id = $this->selgeneral();
		$final_pros = array();
		
		if(isset($new_id['artist_id']) && !empty($new_id['artist_id']) && $new_id['artist_id']!=0)
		{
			$mcth = 'artist_project|'.$id;
			
			$sql_apro = mysql_query("SELECT * FROM artist_event WHERE creators_info='".$mcth."' AND del_status=0 AND active=0 AND date<'".$date."'");
			if(mysql_num_rows($sql_apro)>0)
			{
				while($ans_apro = mysql_fetch_assoc($sql_apro))
				{				
					$ans_apro['id'] = $ans_apro['id'].'~art';
					$final_pros[] = $ans_apro;
				}	
			}
			
			$sql_cpro = mysql_query("SELECT * FROM community_event WHERE creators_info='".$mcth."' AND del_status=0 AND active=0 AND date<'".$date."'");
			if(mysql_num_rows($sql_cpro)>0)
			{
				while($ans_cpro = mysql_fetch_assoc($sql_cpro))
				{				
					$ans_cpro['id'] = $ans_cpro['id'].'~com';
					$final_pros[] = $ans_cpro;
				}
			}
		}
		
		return $final_pros;
	} */
	
	function only_creator_upevent()
	{
		$id = $this->selgeneral();
		$date = date("Y-m-d");
		
		$var_type_1 = 'general_artist';
		$var_type_2 = 'general_community';
		$var_type_3 = 'artist_project';
		$var_type_4 = 'community_project';
		
		$artm = array();
		$arta = array();
		$comm = array();
		$coma = array();
		
		$artme = array();
		$artae = array();
		$comme = array();
		$comae = array();
		
		$art_p_ao = array();
		$com_p_ao = array();
		$art_p_co = array();
		$com_p_co = array();
		
		$art_e_ao = array();
		$com_e_ao = array();
		$art_e_co = array();
		$com_e_co = array();
		
		$pal_1 = array();
		$pal_2 = array();
		
		if($id['artist_id']!=0 && !empty($id['artist_id']))
		{
			$chk_type = $var_type_1.'|'.$id['artist_id'];
			
			$sql_get_allp = mysql_query("SELECT * FROM artist_project WHERE artist_id='".$id['artist_id']."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_get_allp)>0)
			{
				while($rowal = mysql_fetch_assoc($sql_get_allp))
				{
					$pal_1[] = $rowal;
				}
			}
			
			
			
			 if(!empty($pal_1) && count($pal_1))
			{
				for($pl=0;$pl<count($pal_1);$pl++)
				{
					$chk_typep_1 = 'artist_project|'.$pal_1[$pl]['id'];
					
					$sql = mysql_query("SELECT * FROM artist_event WHERE creators_info='".$chk_typep_1."' AND del_status=0 AND active=0 AND date>='".$date."'");
					if(mysql_num_rows($sql)>0)
					{
						while($row_po = mysql_fetch_assoc($sql))
						{
							if($row_po['artist_id']!=$id['artist_id'])
							{
								$row_po['id'] = $row_po['id'].'~art';
								$art_e_ao[] = $row_po;
							}
						}
					}
					
					$sql = mysql_query("SELECT * FROM community_event WHERE creators_info='".$chk_typep_1."' AND del_status=0 AND active=0 AND date>='".$date."'");
					if(mysql_num_rows($sql)>0)
					{
						while($row_poo = mysql_fetch_assoc($sql))
						{
							if($row_poo['community_id']!=$id['community_id'])
							{
								$row_poo['id'] = $row_poo['id'].'~com';
								$com_e_ao[] = $row_poo;
							}
						}
					}
				}
			} 

			/*$sql_pro_arta = mysql_query("SELECT * FROM artist_project WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_pro_arta)>0)
			{
				while($rowaa = mysql_fetch_assoc($sql_pro_arta))
				{
					if($rowaa['artist_id']!=$id['artist_id'])
					{
						$rowaa['id'] = $rowaa['id'].'~art';
						$arta[] = $rowaa;
					}
				}
			}*/
			
			$sql_pro_arta = mysql_query("SELECT * FROM artist_event WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0 AND date>='".$date."'");
			if(mysql_num_rows($sql_pro_arta)>0)
			{
				while($rowaa = mysql_fetch_assoc($sql_pro_arta))
				{
					if($rowaa['artist_id']!=$id['artist_id'])
					{
						$rowaa['id'] = $rowaa['id'].'~art';
						$artae[] = $rowaa;
					}
				}
			} 
			
			/*$sql_pro_coma = mysql_query("SELECT * FROM community_project WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_pro_coma)>0)
			{
				while($rowam = mysql_fetch_assoc($sql_pro_coma))
				{
					if($rowam['community_id']!=$id['community_id'])
					{
						$rowam['id'] = $rowam['id'].'~com';
						$coma[] = $rowam;
					}
				}
			}*/
			
			$sql_pro_coma = mysql_query("SELECT * FROM community_event WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0 AND date>='".$date."'");
			if(mysql_num_rows($sql_pro_coma)>0)
			{
				while($rowam = mysql_fetch_assoc($sql_pro_coma))
				{
					if($rowam['community_id']!=$id['community_id'])
					{
						$rowam['id'] = $rowam['id'].'~com';
						$comae[] = $rowam;
					}
				}
			} 
		}
		
		if($id['community_id']!=0 && !empty($id['community_id']))
		{
			/*$chk_type = $var_type_2.'|'.$id['community_id'];
			$sql_pro_artm = mysql_query("SELECT * FROM artist_project WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_pro_artm)>0)
			{
				while($rowcm = mysql_fetch_assoc($sql_pro_artm))
				{
					if($rowcm['artist_id']!=$id['artist_id'])
					{
						$rowcm['id'] = $rowcm['id'].'~art';
						$artm[] = $rowcm;
					}
				}
			}*/
			
			 $chk_type = $var_type_2.'|'.$id['community_id'];
			$sql_pro_artm = mysql_query("SELECT * FROM artist_event WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0 AND date>='".$date."'");
			if(mysql_num_rows($sql_pro_artm)>0)
			{
				while($rowcm = mysql_fetch_assoc($sql_pro_artm))
				{
					if($rowcm['artist_id']!=$id['artist_id'])
					{
						$rowcm['id'] = $rowcm['id'].'~art';
						$artme[] = $rowcm;
					}
				}
			} 
			
			/*$sql_pro_comm = mysql_query("SELECT * FROM community_project WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_pro_comm)>0)
			{
				while($rowca = mysql_fetch_assoc($sql_pro_comm))
				{
					if($rowca['community_id']!=$id['community_id'])
					{
						$rowca['id'] = $rowca['id'].'~com';
						$comm[] = $rowca;
					}
				}
			}*/
			
			 $sql_pro_comm = mysql_query("SELECT * FROM community_event WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0 AND date>='".$date."'");
			if(mysql_num_rows($sql_pro_comm)>0)
			{
				while($rowca = mysql_fetch_assoc($sql_pro_comm))
				{
					if($rowca['community_id']!=$id['community_id'])
					{
						$rowca['id'] = $rowca['id'].'~com';
						$comme[] = $rowca;
					}
				}
			} 
			
			$sql_get_allp = mysql_query("SELECT * FROM community_project WHERE community_id='".$id['community_id']."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_get_allp)>0)
			{
				while($rowal = mysql_fetch_assoc($sql_get_allp))
				{
					$pal_2[] = $rowal;
				}
			}
			
			/*if(!empty($pal_2) && count($pal_2))
			{
				for($pl2=0;$pl2<count($pal_2);$pl2++)
				{
					$chk_typep_2 = 'community_project|'.$pal_2[$pl2]['id'];
					$sql = mysql_query("SELECT * FROM artist_project WHERE creators_info='".$chk_typep_2."' AND del_status=0 AND active=0");
					if(mysql_num_rows($sql)>0)
					{
						while($row_po = mysql_fetch_assoc($sql))
						{
							if($row_po['artist_id']!=$id['artist_id'])
							{
								$row_po['id'] = $row_po['id'].'~art';
								$art_p_co[] = $row_po;
							}
						}
					}
					
					$sql = mysql_query("SELECT * FROM community_project WHERE creators_info='".$chk_typep_2."' AND del_status=0 AND active=0");
					if(mysql_num_rows($sql)>0)
					{
						while($row_poo = mysql_fetch_assoc($sql))
						{
							if($row_poo['community_id']!=$id['community_id'])
							{
								$row_poo['id'] = $row_poo['id'].'~com';
								$com_p_co[] = $row_poo;
							}
						}
					}
				}
			}*/
			
			 if(!empty($pal_2) && count($pal_2))
			{
				for($pl2=0;$pl2<count($pal_2);$pl2++)
				{
					$chk_typep_2 = 'community_project|'.$pal_2[$pl2]['id'];
					$sql = mysql_query("SELECT * FROM artist_event WHERE creators_info='".$chk_typep_2."' AND del_status=0 AND active=0 AND date>='".$date."'");
					if(mysql_num_rows($sql)>0)
					{
						while($row_po = mysql_fetch_assoc($sql))
						{
							if($row_po['artist_id']!=$id['artist_id'])
							{
								$row_po['id'] = $row_po['id'].'~art';
								$art_e_co[] = $row_po;
							}
						}
					}
					
					$sql = mysql_query("SELECT * FROM community_event WHERE creators_info='".$chk_typep_2."' AND del_status=0 AND active=0 AND date>='".$date."'");
					if(mysql_num_rows($sql)>0)
					{
						while($row_poo = mysql_fetch_assoc($sql))
						{
							if($row_poo['community_id']!=$id['community_id'])
							{
								$row_poo['id'] = $row_poo['id'].'~com';
								$com_e_co[] = $row_poo;
							}
						}
					}
				}
			} 
		}
		
		/* var_dump($artm);
		var_dump($arta);
		var_dump($comm);
		var_dump($coma);
		var_dump($art_p_ao);
		var_dump($com_p_ao);
		var_dump($art_p_co);
		var_dump($com_p_co); */
		
		$totals = array();
		$totals = array_merge($artm,$arta,$comm,$coma,$art_p_ao,$com_p_ao,$art_p_co,$com_p_co,$artme,$artae,$comme,$comae,$art_e_ao,$com_e_ao,$art_e_co,$com_e_co);
		return $totals;
	}
	
	function only_creator_recevent()
	{
		$id = $this->selgeneral();
		$date = date("Y-m-d");
		
		$var_type_1 = 'general_artist';
		$var_type_2 = 'general_community';
		$var_type_3 = 'artist_project';
		$var_type_4 = 'community_project';
		
		$artm = array();
		$arta = array();
		$comm = array();
		$coma = array();
		
		$artme = array();
		$artae = array();
		$comme = array();
		$comae = array();
		
		$art_p_ao = array();
		$com_p_ao = array();
		$art_p_co = array();
		$com_p_co = array();
		
		$art_e_ao = array();
		$com_e_ao = array();
		$art_e_co = array();
		$com_e_co = array();
		
		$pal_1 = array();
		$pal_2 = array();
		
		if($id['artist_id']!=0 && !empty($id['artist_id']))
		{
			$chk_type = $var_type_1.'|'.$id['artist_id'];
			
			$sql_get_allp = mysql_query("SELECT * FROM artist_project WHERE artist_id='".$id['artist_id']."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_get_allp)>0)
			{
				while($rowal = mysql_fetch_assoc($sql_get_allp))
				{
					$pal_1[] = $rowal;
				}
			}
			
			
			
			 if(!empty($pal_1) && count($pal_1))
			{
				for($pl=0;$pl<count($pal_1);$pl++)
				{
					$chk_typep_1 = 'artist_project|'.$pal_1[$pl]['id'];
					
					$sql = mysql_query("SELECT * FROM artist_event WHERE creators_info='".$chk_typep_1."' AND del_status=0 AND active=0 AND date<'".$date."'");
					if(mysql_num_rows($sql)>0)
					{
						while($row_po = mysql_fetch_assoc($sql))
						{
							if($row_po['artist_id']!=$id['artist_id'])
							{
								$row_po['id'] = $row_po['id'].'~art';
								$art_e_ao[] = $row_po;
							}
						}
					}
					
					$sql = mysql_query("SELECT * FROM community_event WHERE creators_info='".$chk_typep_1."' AND del_status=0 AND active=0 AND date<'".$date."'");
					if(mysql_num_rows($sql)>0)
					{
						while($row_poo = mysql_fetch_assoc($sql))
						{
							if($row_poo['community_id']!=$id['community_id'])
							{
								$row_poo['id'] = $row_poo['id'].'~com';
								$com_e_ao[] = $row_poo;
							}
						}
					}
				}
			} 

			/*$sql_pro_arta = mysql_query("SELECT * FROM artist_project WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_pro_arta)>0)
			{
				while($rowaa = mysql_fetch_assoc($sql_pro_arta))
				{
					if($rowaa['artist_id']!=$id['artist_id'])
					{
						$rowaa['id'] = $rowaa['id'].'~art';
						$arta[] = $rowaa;
					}
				}
			}*/
			
			$sql_pro_arta = mysql_query("SELECT * FROM artist_event WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0 AND date<'".$date."'");
			if(mysql_num_rows($sql_pro_arta)>0)
			{
				while($rowaa = mysql_fetch_assoc($sql_pro_arta))
				{
					if($rowaa['artist_id']!=$id['artist_id'])
					{
						$rowaa['id'] = $rowaa['id'].'~art';
						$artae[] = $rowaa;
					}
				}
			} 
			
			/*$sql_pro_coma = mysql_query("SELECT * FROM community_project WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_pro_coma)>0)
			{
				while($rowam = mysql_fetch_assoc($sql_pro_coma))
				{
					if($rowam['community_id']!=$id['community_id'])
					{
						$rowam['id'] = $rowam['id'].'~com';
						$coma[] = $rowam;
					}
				}
			}*/
			
			$sql_pro_coma = mysql_query("SELECT * FROM community_event WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0 AND date<'".$date."'");
			if(mysql_num_rows($sql_pro_coma)>0)
			{
				while($rowam = mysql_fetch_assoc($sql_pro_coma))
				{
					if($rowam['community_id']!=$id['community_id'])
					{
						$rowam['id'] = $rowam['id'].'~com';
						$comae[] = $rowam;
					}
				}
			} 
		}
		
		if($id['community_id']!=0 && !empty($id['community_id']))
		{
			/*$chk_type = $var_type_2.'|'.$id['community_id'];
			$sql_pro_artm = mysql_query("SELECT * FROM artist_project WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_pro_artm)>0)
			{
				while($rowcm = mysql_fetch_assoc($sql_pro_artm))
				{
					if($rowcm['artist_id']!=$id['artist_id'])
					{
						$rowcm['id'] = $rowcm['id'].'~art';
						$artm[] = $rowcm;
					}
				}
			}*/
			
			 $chk_type = $var_type_2.'|'.$id['community_id'];
			$sql_pro_artm = mysql_query("SELECT * FROM artist_event WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0 AND date<'".$date."'");
			if(mysql_num_rows($sql_pro_artm)>0)
			{
				while($rowcm = mysql_fetch_assoc($sql_pro_artm))
				{
					if($rowcm['artist_id']!=$id['artist_id'])
					{
						$rowcm['id'] = $rowcm['id'].'~art';
						$artme[] = $rowcm;
					}
				}
			} 
			
			/*$sql_pro_comm = mysql_query("SELECT * FROM community_project WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_pro_comm)>0)
			{
				while($rowca = mysql_fetch_assoc($sql_pro_comm))
				{
					if($rowca['community_id']!=$id['community_id'])
					{
						$rowca['id'] = $rowca['id'].'~com';
						$comm[] = $rowca;
					}
				}
			}*/
			
			 $sql_pro_comm = mysql_query("SELECT * FROM community_event WHERE creators_info='".$chk_type."' AND del_status=0 AND active=0 AND date<'".$date."'");
			if(mysql_num_rows($sql_pro_comm)>0)
			{
				while($rowca = mysql_fetch_assoc($sql_pro_comm))
				{
					if($rowca['community_id']!=$id['community_id'])
					{
						$rowca['id'] = $rowca['id'].'~com';
						$comme[] = $rowca;
					}
				}
			} 
			
			$sql_get_allp = mysql_query("SELECT * FROM community_project WHERE community_id='".$id['community_id']."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_get_allp)>0)
			{
				while($rowal = mysql_fetch_assoc($sql_get_allp))
				{
					$pal_2[] = $rowal;
				}
			}
			
			/*if(!empty($pal_2) && count($pal_2))
			{
				for($pl2=0;$pl2<count($pal_2);$pl2++)
				{
					$chk_typep_2 = 'community_project|'.$pal_2[$pl2]['id'];
					$sql = mysql_query("SELECT * FROM artist_project WHERE creators_info='".$chk_typep_2."' AND del_status=0 AND active=0");
					if(mysql_num_rows($sql)>0)
					{
						while($row_po = mysql_fetch_assoc($sql))
						{
							if($row_po['artist_id']!=$id['artist_id'])
							{
								$row_po['id'] = $row_po['id'].'~art';
								$art_p_co[] = $row_po;
							}
						}
					}
					
					$sql = mysql_query("SELECT * FROM community_project WHERE creators_info='".$chk_typep_2."' AND del_status=0 AND active=0");
					if(mysql_num_rows($sql)>0)
					{
						while($row_poo = mysql_fetch_assoc($sql))
						{
							if($row_poo['community_id']!=$id['community_id'])
							{
								$row_poo['id'] = $row_poo['id'].'~com';
								$com_p_co[] = $row_poo;
							}
						}
					}
				}
			}*/
			
			 if(!empty($pal_2) && count($pal_2))
			{
				for($pl2=0;$pl2<count($pal_2);$pl2++)
				{
					$chk_typep_2 = 'community_project|'.$pal_2[$pl2]['id'];
					$sql = mysql_query("SELECT * FROM artist_event WHERE creators_info='".$chk_typep_2."' AND del_status=0 AND active=0 AND date<'".$date."'");
					if(mysql_num_rows($sql)>0)
					{
						while($row_po = mysql_fetch_assoc($sql))
						{
							if($row_po['artist_id']!=$id['artist_id'])
							{
								$row_po['id'] = $row_po['id'].'~art';
								$art_e_co[] = $row_po;
							}
						}
					}
					
					$sql = mysql_query("SELECT * FROM community_event WHERE creators_info='".$chk_typep_2."' AND del_status=0 AND active=0 AND date<'".$date."'");
					if(mysql_num_rows($sql)>0)
					{
						while($row_poo = mysql_fetch_assoc($sql))
						{
							if($row_poo['community_id']!=$id['community_id'])
							{
								$row_poo['id'] = $row_poo['id'].'~com';
								$com_e_co[] = $row_poo;
							}
						}
					}
				}
			} 
		}
		
		/* var_dump($artm);
		var_dump($arta);
		var_dump($comm);
		var_dump($coma);
		var_dump($art_p_ao);
		var_dump($com_p_ao);
		var_dump($art_p_co);
		var_dump($com_p_co); */
		
		$totals = array();
		$totals = array_merge($artm,$arta,$comm,$coma,$art_p_ao,$com_p_ao,$art_p_co,$com_p_co,$artme,$artae,$comme,$comae,$art_e_ao,$com_e_ao,$art_e_co,$com_e_co);
		return $totals;
	}
	
	/*function get_creator_heis(){
		$get_det = $this->selgeneral();
		$get_media_art_pro = array();
		$get_media_art_eve = array();
		$get_media_com_pro = array();
		$get_media_com_eve = array();
		$get_media_artist_project1 = array();
		$get_media_artist_project2 = array();
		$get_media_artist_project3 = array();
		$get_media_artist_project4 = array();
		$get_media_community_project1 = array();
		$get_media_community_project2 = array();
		$get_media_community_project3 = array();
		$get_media_community_project4 = array();
		//if($get_det['artist_id'] !="" && $get_det['artist_id']!=0){
			$creator_info = "general_community|".$get_det['community_id'];
			$query = mysql_query("select * from artist_project where creators_info ='".$creator_info."'");
			if(mysql_num_rows($query)>0){
				while($row = mysql_fetch_assoc($query)){
					$new_creator = "artist_project|".$row['id'];
					$find_cretor_med = mysql_query("select * from general_media where creator_info='".$new_creator."' OR from_info='".$new_creator."'");
					if(mysql_num_rows($find_cretor_med)>0){
						while($res_new_det = mysql_fetch_assoc($find_cretor_med)){
							$get_media_art_pro = $res_new_det;
						}
					}
				}
			}
			$query1 = mysql_query("select * from artist_event where creators_info ='".$creator_info."'");
			if(mysql_num_rows($query1)>0){
				while($row = mysql_fetch_assoc($query1)){
					$new_creator = "artist_event|".$row['id'];
					$find_cretor_med = mysql_query("select * from general_media where creator_info='".$new_creator."' OR from_info='".$new_creator."'");
					if(mysql_num_rows($find_cretor_med)>0){
						while($res_new_det = mysql_fetch_assoc($find_cretor_med)){
							$get_media_art_eve = $res_new_det;
						}
					}
				}
			}
			$query = mysql_query("select * from community_project where creators_info ='".$creator_info."'");
			if(mysql_num_rows($query)>0){
				while($row = mysql_fetch_assoc($query)){
					$new_creator = "community_project|".$row['id'];
					$find_cretor_med = mysql_query("select * from general_media where creator_info='".$new_creator."' OR from_info='".$new_creator."'");
					if(mysql_num_rows($find_cretor_med)>0){
						while($res_new_det = mysql_fetch_assoc($find_cretor_med)){
							$get_media_com_pro = $res_new_det;
						}
					}
				}
			}
			$query1 = mysql_query("select * from community_event where creators_info ='".$creator_info."'");
			if(mysql_num_rows($query1)>0){
				while($row = mysql_fetch_assoc($query1)){
					$new_creator = "community_event|".$row['id'];
					$find_cretor_med = mysql_query("select * from general_media where creator_info='".$new_creator."' OR from_info='".$new_creator."'");
					if(mysql_num_rows($find_cretor_med)>0){
						while($res_new_det = mysql_fetch_assoc($find_cretor_med)){
							$get_media_com_eve = $res_new_det;
						}
					}
				}
			}
			
			$creator_info = $get_det['artist_id'];
			$query = mysql_query("select * from artist_project where creators_info ='".$creator_info."'");
			if(mysql_num_rows($query)>0){
				while($row = mysql_fetch_assoc($query)){
					$new_creator = "artist_project|".$row['id'];
					$find_cretor_med = mysql_query("select * from general_media where creator_info='".$new_creator."' OR from_info='".$new_creator."'");
					if(mysql_num_rows($find_cretor_med)>0){
						while($res_new_det = mysql_fetch_assoc($find_cretor_med)){
							$get_media_art_pro = $res_new_det;
						}
					}
				}
			}
			$query1 = mysql_query("select * from artist_event where creators_info ='".$creator_info."'");
			if(mysql_num_rows($query1)>0){
				while($row = mysql_fetch_assoc($query1)){
					$new_creator = "artist_event|".$row['id'];
					$find_cretor_med = mysql_query("select * from general_media where creator_info='".$new_creator."' OR from_info='".$new_creator."'");
					if(mysql_num_rows($find_cretor_med)>0){
						while($res_new_det = mysql_fetch_assoc($find_cretor_med)){
							$get_media_art_eve = $res_new_det;
						}
					}
				}
			}
			$query = mysql_query("select * from community_project where creators_info ='".$creator_info."'");
			if(mysql_num_rows($query)>0){
				while($row = mysql_fetch_assoc($query)){
					$new_creator = "community_project|".$row['id'];
					$find_cretor_med = mysql_query("select * from general_media where creator_info='".$new_creator."' OR from_info='".$new_creator."'");
					if(mysql_num_rows($find_cretor_med)>0){
						while($res_new_det = mysql_fetch_assoc($find_cretor_med)){
							$get_media_com_pro = $res_new_det;
						}
					}
				}
			}
			$query1 = mysql_query("select * from community_event where creators_info ='".$creator_info."'");
			if(mysql_num_rows($query1)>0){
				while($row = mysql_fetch_assoc($query1)){
					$new_creator = "community_event|".$row['id'];
					$find_cretor_med = mysql_query("select * from general_media where creator_info='".$new_creator."' OR from_info='".$new_creator."'");
					if(mysql_num_rows($find_cretor_med)>0){
						while($res_new_det = mysql_fetch_assoc($find_cretor_med)){
							$get_media_com_eve = $res_new_det;
						}
					}
				}
			}
			/**get community projects**//*
			$get_compro = mysql_query("select * from community_project where community_id = '".$get_det['community_id']."'");
			if(mysql_num_rows($get_compro)>0){
				while($res_com_pro = mysql_fetch_assoc($get_compro))
				{
					$creator_info = "community_project|".$res_com_pro['id'];
					$query = mysql_query("select * from artist_project where creators_info ='".$creator_info."'");
					if(mysql_num_rows($query)>0){
						while($row = mysql_fetch_assoc($query)){
							$new_creator = "artist_project|".$row['id'];
							$find_cretor_med = mysql_query("select * from general_media where creator_info='".$new_creator."' OR from_info='".$new_creator."'");
							if(mysql_num_rows($find_cretor_med)>0){
								while($res_new_det = mysql_fetch_assoc($find_cretor_med)){
									$get_media_community_project1 = $res_new_det;
								}
							}
						}
					}
					$query = mysql_query("select * from artist_event where creators_info ='".$creator_info."'");
					if(mysql_num_rows($query)>0){
						while($row = mysql_fetch_assoc($query)){
							$new_creator = "artist_event|".$row['id'];
							$find_cretor_med = mysql_query("select * from general_media where creator_info='".$new_creator."' OR from_info='".$new_creator."'");
							if(mysql_num_rows($find_cretor_med)>0){
								while($res_new_det = mysql_fetch_assoc($find_cretor_med)){
									$get_media_community_project2 = $res_new_det;
								}
							}
						}
					}
					$query = mysql_query("select * from community_project where creators_info ='".$creator_info."'");
					if(mysql_num_rows($query)>0){
						while($row = mysql_fetch_assoc($query)){
							$new_creator = "community_project|".$row['id'];
							$find_cretor_med = mysql_query("select * from general_media where creator_info='".$new_creator."' OR from_info='".$new_creator."'");
							if(mysql_num_rows($find_cretor_med)>0){
								while($res_new_det = mysql_fetch_assoc($find_cretor_med)){
									$get_media_community_project3 = $res_new_det;
								}
							}
						}
					}
					$query = mysql_query("select * from community_event where creators_info ='".$creator_info."'");
					if(mysql_num_rows($query)>0){
						while($row = mysql_fetch_assoc($query)){
							$new_creator = "community_event|".$row['id'];
							$find_cretor_med = mysql_query("select * from general_media where creator_info='".$new_creator."' OR from_info='".$new_creator."'");
							if(mysql_num_rows($find_cretor_med)>0){
								while($res_new_det = mysql_fetch_assoc($find_cretor_med)){
									$get_media_community_project4 = $res_new_det;
								}
							}
						}
					}
				}
			}
			
			/**get community projects ends here**/
			
			/**get artist projects**//*
			$get_compro = mysql_query("select * from artist_project where artist_id = '".$get_det['artist_id']."'");
			if(mysql_num_rows($get_compro)>0){
				while($res_com_pro = mysql_fetch_assoc($get_compro))
				{
					$creator_info = "artist_project|".$res_com_pro['id'];
					$query = mysql_query("select * from artist_project where creators_info ='".$creator_info."'");
					if(mysql_num_rows($query)>0){
						while($row = mysql_fetch_assoc($query)){
							$new_creator = "artist_project|".$row['id'];
							$find_cretor_med = mysql_query("select * from general_media where creator_info='".$new_creator."' OR from_info='".$new_creator."'");
							if(mysql_num_rows($find_cretor_med)>0){
								while($res_new_det = mysql_fetch_assoc($find_cretor_med)){
									$get_media_artist_project1 = $res_new_det;
								}
							}
						}
					}
					$query = mysql_query("select * from artist_event where creators_info ='".$creator_info."'");
					if(mysql_num_rows($query)>0){
						while($row = mysql_fetch_assoc($query)){
							$new_creator = "artist_event|".$row['id'];
							$find_cretor_med = mysql_query("select * from general_media where creator_info='".$new_creator."' OR from_info='".$new_creator."'");
							if(mysql_num_rows($find_cretor_med)>0){
								while($res_new_det = mysql_fetch_assoc($find_cretor_med)){
									$get_media_artist_project2 = $res_new_det;
								}
							}
						}
					}
					$query = mysql_query("select * from community_project where creators_info ='".$creator_info."'");
					if(mysql_num_rows($query)>0){
						while($row = mysql_fetch_assoc($query)){
							$new_creator = "community_project|".$row['id'];
							$find_cretor_med = mysql_query("select * from general_media where creator_info='".$new_creator."' OR from_info='".$new_creator."'");
							if(mysql_num_rows($find_cretor_med)>0){
								while($res_new_det = mysql_fetch_assoc($find_cretor_med)){
									$get_media_artist_project3 = $res_new_det;
								}
							}
						}
					}
					$query = mysql_query("select * from community_event where creators_info ='".$creator_info."'");
					if(mysql_num_rows($query)>0){
						while($row = mysql_fetch_assoc($query)){
							$new_creator = "community_event|".$row['id'];
							$find_cretor_med = mysql_query("select * from general_media where creator_info='".$new_creator."' OR from_info='".$new_creator."'");
							if(mysql_num_rows($find_cretor_med)>0){
								while($res_new_det = mysql_fetch_assoc($find_cretor_med)){
									$get_media_artist_project4 = $res_new_det;
								}
							}
						}
					}
				}
			}
			
			/**get artist projects ends here**//*

		$get_media = array_merge($get_media_art_pro,$get_media_art_eve,$get_media_com_eve,$get_media_com_pro,$get_media_artist_project1,$get_media_artist_project2,$get_media_artist_project3,$get_media_artist_project4,$get_media_community_project1,$get_media_community_project2,$get_media_community_project3,$get_media_community_project4);
		return($get_media);
	}*/
	
	/*function get_tag_where_heis($email){
		$get_det = $this->selgeneral();
		$get_media_artist_project1 = array();
		$get_media_artist_event1 = array();
		$get_media_community_project1 = array();
		$get_media_community_event1 = array();
			
			/**get community projects**//*
			$get_all_tag_det = mysql_query("select * from community_project where tagged_user_email LIKE '%".$email."%'");
			if(mysql_num_rows($get_all_tag_det)>0){
				while($row_res = mysql_fetch_assoc($get_all_tag_det)){
					$creator_info = "community_project|".$row_res['id'];
					$find_cretor_med = mysql_query("select * from general_media where creator_info='".$creator_info."' OR from_info='".$creator_info."'");
					if(mysql_num_rows($find_cretor_med)>0){
						while($res_new_det = mysql_fetch_assoc($find_cretor_med)){
							$get_media_community_project1 = $res_new_det;
						}
					}
				}
			}
			
			$get_all_tag_det_com_eve = mysql_query("select * from community_event where tagged_user_email LIKE '%".$email."%'");
			if(mysql_num_rows($get_all_tag_det_com_eve)>0){
				while($row_res_com_eve = mysql_fetch_assoc($get_all_tag_det_com_eve)){
					$creator_info = "community_event|".$row_res_com_eve['id'];
					$find_cretor_med = mysql_query("select * from general_media where creator_info='".$creator_info."' OR from_info='".$creator_info."'");
					if(mysql_num_rows($find_cretor_med)>0){
						while($res_new_det = mysql_fetch_assoc($find_cretor_med)){
							$get_media_community_event1 = $res_new_det;
						}
					}
				}
			}
			
			$get_all_tag_det_art_pro = mysql_query("select * from artist_project where tagged_user_email LIKE '%".$email."%'");
			if(mysql_num_rows($get_all_tag_det_art_pro)>0){
				while($row_res_art_pro = mysql_fetch_assoc($get_all_tag_det_art_pro)){
					$creator_info = "artist_project|".$row_res_art_pro['id'];
					$find_cretor_med = mysql_query("select * from general_media where creator_info='".$creator_info."' OR from_info='".$creator_info."'");
					if(mysql_num_rows($find_cretor_med)>0){
						while($res_new_det = mysql_fetch_assoc($find_cretor_med)){
							$get_media_artist_project1 = $res_new_det;
						}
					}
				}
			}
			
			$get_all_tag_det_art_eve = mysql_query("select * from artist_event where tagged_user_email LIKE '%".$email."%'");
			if(mysql_num_rows($get_all_tag_det_art_eve)>0){
				while($row_res_art_eve = mysql_fetch_assoc($get_all_tag_det_art_eve)){
					$creator_info = "artist_event|".$row_res_art_eve['id'];
					$find_cretor_med = mysql_query("select * from general_media where creator_info='".$creator_info."' OR from_info='".$creator_info."'");
					if(mysql_num_rows($find_cretor_med)>0){
						while($res_new_det = mysql_fetch_assoc($find_cretor_med)){
							$get_media_artist_event1 = $res_new_det;
						}
					}
				}
			}

		$get_media = array_merge($get_media_artist_project1,$get_media_artist_event1,$get_media_community_project1,$get_media_community_event1);
		return($get_media);
	}*/
	
	function get_media_where_creator_or_from()
	{
		$get_art_media1 = array();
		$get_art_media2 = array();
		$get_art_media3 = array();
		$get_art_media4 = array();
		$get_art_media5 = array();
		$get_art_media6 = array();
		
		$get_gen_det = $this->selgeneral();
		if($get_gen_det['artist_id']>0){
			$creator_info = "general_artist|".$get_gen_det['artist_id'];
			$query = mysql_query("select * from general_media where creator_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
			if(mysql_num_rows($query)>0){
				while($row_art = mysql_fetch_assoc($query)){
					if($get_gen_det['general_user_id']!= $row_art['general_user_id']){
					$get_art_media1[] = $row_art;
					}
				}
			}
			$get_creator = mysql_query("select * from artist_project where artist_id='".$get_gen_det['artist_id']."' AND del_status=0 AND active=0");
			if(mysql_num_rows($get_creator)>0)
			{
				while($row_art_pro = mysql_fetch_assoc($get_creator))
				{
					$creator_info = "artist_project|".$row_art_pro["id"];
					$query1 = mysql_query("select * from general_media where creator_info = '".$creator_info."' OR from_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
					if(mysql_num_rows($query1)>0){
						while($row_art_media = mysql_fetch_assoc($query1)){
							if($get_gen_det['general_user_id']!= $row_art_media['general_user_id']){
							$get_art_media2[] = $row_art_media;
							}
						}
					}
				}
			}
			$get_creator = mysql_query("select * from artist_event where artist_id='".$get_gen_det['artist_id']."' AND del_status=0 AND active=0");
			if(mysql_num_rows($get_creator)>0)
			{
				while($row_art_eve = mysql_fetch_assoc($get_creator))
				{
					$creator_info = "artist_event|".$row_art_eve["id"];
					$query1 = mysql_query("select * from general_media where creator_info = '".$creator_info."' OR from_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
					if(mysql_num_rows($query1)>0){
						while($row_art_media = mysql_fetch_assoc($query1)){
							if($get_gen_det['general_user_id']!= $row_art_media['general_user_id']){
							$get_art_media3[] = $row_art_media;
							}
						}
					}
				}
			}
		}
		if($get_gen_det['community_id']>0){
			$creator_info = "general_community|".$get_gen_det['community_id'];
			$query = mysql_query("select * from general_media where creator_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
			if(mysql_num_rows($query)>0){
				while($row_com = mysql_fetch_assoc($query)){
					//echo "azher4";
					if($get_gen_det['general_user_id']!= $row_com['general_user_id']){
					$get_art_media4[] = $row_com;
					}
				}
			}
			$get_creator = mysql_query("select * from community_project where community_id='".$get_gen_det['community_id']."' AND del_status=0 AND active=0");
			if(mysql_num_rows($get_creator)>0)
			{
				while($row_com_pro = mysql_fetch_assoc($get_creator))
				{
					$creator_info = "community_project|".$row_com_pro["id"];
					$query1 = mysql_query("select * from general_media where creator_info = '".$creator_info."' OR from_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
					if(mysql_num_rows($query1)>0){
						while($row_com_media = mysql_fetch_assoc($query1)){
							//echo "azher5";
							if($get_gen_det['general_user_id']!= $row_com_media['general_user_id']){
							$get_art_media5[] = $row_com_media;
							}
						}
					}
				}
			}
			$get_creator = mysql_query("select * from community_event where community_id='".$get_gen_det['community_id']."' AND del_status=0 AND active=0");
			if(mysql_num_rows($get_creator)>0)
			{
				while($row_com_eve = mysql_fetch_assoc($get_creator))
				{
					$creator_info = "community_event|".$row_com_eve["id"];
					$query1 = mysql_query("select * from general_media where creator_info = '".$creator_info."' OR from_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
					if(mysql_num_rows($query1)>0){
						while($row_com_media = mysql_fetch_assoc($query1)){
							if($get_gen_det['general_user_id']!= $row_com_media['general_user_id']){
							$get_art_media6[] = $row_com_media;
							}
						}
					}
				}
			}
		}
		
		$all_media = array_merge($get_art_media1,$get_art_media2,$get_art_media3,$get_art_media4,$get_art_media5,$get_art_media6);
		return($all_media);
	}
	
	function get_creator_heis()
	{
		$id = $this->selgeneral();
        $var_type_1 = 'general_artist';
        $var_type_2 = 'general_community';
        $var_type_3 = 'artist_project';
        $var_type_4 = 'community_project';
		$res_media1 = array();
		$res_media2 = array();
		$res_media3 = array();
		$res_media4 = array();
		$res_media5 = array();
		$res_media6 = array();
		$res_media7 = array();
		$res_media8 = array();
		$res_media9 = array();
		$res_media10 = array();
		$res_media11 = array();
		$res_media12 = array();
		$res_media13 = array();
		$res_media14 = array();
		$res_media15 = array();
		$res_media16 = array();
        if($id['artist_id']!=0 && !empty($id['artist_id']))
        {
            $chk_type = $var_type_1.'|'.$id['artist_id'];
			//echo "select * from artist_project where creators_info='".$chk_type."'";
			$sql_get_pro = mysql_query("select * from artist_project where creators_info='".$chk_type."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_get_pro)>0)
			{
				while($row = mysql_fetch_assoc($sql_get_pro))
				{
					$creator_info = "artist_project|".$row['id'];
					//echo "select * from general_media where creator_info = '".$creator_info."'";
					$get_media = mysql_query("select * from general_media where creator_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
					if(mysql_num_rows($get_media)>0)
					{
						while($res = mysql_fetch_assoc($get_media))
						{
							$res_media1[] = $res;
						}
					}
				}
			}
			$sql_get_pro1 = mysql_query("select * from community_project where creators_info='".$chk_type."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_get_pro1)>0)
			{
				while($row1 = mysql_fetch_assoc($sql_get_pro1))
				{
					$creator_info = "community_project|".$row1['id'];
					$get_media = mysql_query("select * from general_media where creator_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
					if(mysql_num_rows($get_media)>0)
					{
						while($res = mysql_fetch_assoc($get_media))
						{
							$res_media2[] = $res;
						}
					}
				}
			}
			/*$sql_get_pro2 = mysql_query("select * from artist_event where creators_info='".$chk_type."'");
			if(mysql_num_rows($sql_get_pro2)>0)
			{
				while($row = mysql_fetch_assoc($sql_get_pro2))
				{
					$creator_info = "artist_event|".$row['id'];
					$get_media = mysql_query("select * from general_media where creator_info = '".$creator_info."'");
					if(mysql_num_rows($get_media)>0)
					{
						while($res = mysql_fetch_assoc($get_media))
						{
							$res_media3[] = $res;
						}
					}
				}
			}
			$sql_get_pro3 = mysql_query("select * from community_event where creators_info='".$chk_type."'");
			if(mysql_num_rows($sql_get_pro3)>0)
			{
				while($row1 = mysql_fetch_assoc($sql_get_pro3))
				{
					$creator_info = "community_event|".$row1['id'];
					$get_media = mysql_query("select * from general_media where creator_info = '".$creator_info."'");
					if(mysql_num_rows($get_media)>0)
					{
						while($res = mysql_fetch_assoc($get_media))
						{
							$res_media4[] = $res;
						}
					}
				}
			}*/
			
			$find_project = mysql_query("select * from artist_project where artist_id='".$id['artist_id']."' AND del_status=0 AND active=0");
			if(mysql_num_rows($find_project)>0)
			{
				while($get_all_pro = mysql_fetch_assoc($find_project))
				{
					$new_creator_info = "artist_project|".$get_all_pro['id'];
					$sql_get_pro = mysql_query("select * from artist_project where creators_info='".$new_creator_info."' AND del_status=0 AND active=0");
					if(mysql_num_rows($sql_get_pro)>0)
					{
						while($row = mysql_fetch_assoc($sql_get_pro))
						{
							$creator_info = "artist_project|".$row['id'];
							$get_media = mysql_query("select * from general_media where creator_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
							if(mysql_num_rows($get_media)>0)
							{
								while($res = mysql_fetch_assoc($get_media))
								{
									$res_media5[] = $res;
								}
							}
						}
					}
					$sql_get_pro1 = mysql_query("select * from community_project where creators_info='".$new_creator_info."' AND del_status=0 AND active=0");
					if(mysql_num_rows($sql_get_pro1)>0)
					{
						while($row1 = mysql_fetch_assoc($sql_get_pro1))
						{
							$creator_info = "community_project|".$row1['id'];
							$get_media = mysql_query("select * from general_media where creator_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
							if(mysql_num_rows($get_media)>0)
							{
								while($res = mysql_fetch_assoc($get_media))
								{
									$res_media6[] = $res;
								}
							}
						}
					}
					/*$sql_get_pro2 = mysql_query("select * from artist_event where creators_info='".$new_creator_info."'");
					if(mysql_num_rows($sql_get_pro2)>0)
					{
						while($row = mysql_fetch_assoc($sql_get_pro2))
						{
							$creator_info = "artist_event|".$row['id'];
							$get_media = mysql_query("select * from general_media where creator_info = '".$creator_info."'");
							if(mysql_num_rows($get_media)>0)
							{
								while($res = mysql_fetch_assoc($get_media))
								{
									$res_media7[] = $res;
								}
							}
						}
					}
					$sql_get_pro3 = mysql_query("select * from community_event where creators_info='".$new_creator_info."'");
					if(mysql_num_rows($sql_get_pro3)>0)
					{
						while($row1 = mysql_fetch_assoc($sql_get_pro3))
						{
							$creator_info = "community_event|".$row1['id'];
							$get_media = mysql_query("select * from general_media where creator_info = '".$creator_info."'");
							if(mysql_num_rows($get_media)>0)
							{
								while($res = mysql_fetch_assoc($get_media))
								{
									$res_media8[] = $res;
								}
							}
						}
					}*/
				}
			}
		   
		}
		if($id['community_id']!=0 && !empty($id['community_id']))
        {
            $chk_type = $var_type_2.'|'.$id['community_id'];
			$sql_get_pro = mysql_query("select * from artist_project where creators_info='".$chk_type."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_get_pro)>0)
			{
				while($row = mysql_fetch_assoc($sql_get_pro))
				{
					$creator_info = "artist_project|".$row['id'];
					$get_media = mysql_query("select * from general_media where creator_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
					if(mysql_num_rows($get_media)>0)
					{
						while($res = mysql_fetch_assoc($get_media))
						{
							$res_media9[] = $res;
						}
					}
				}
			}
			$sql_get_pro1 = mysql_query("select * from community_project where creators_info='".$chk_type."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_get_pro1)>0)
			{
				while($row1 = mysql_fetch_assoc($sql_get_pro1))
				{
					$creator_info = "community_project|".$row1['id'];
					$get_media = mysql_query("select * from general_media where creator_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
					if(mysql_num_rows($get_media)>0)
					{
						while($res = mysql_fetch_assoc($get_media))
						{
							$res_media10[] = $res;
						}
					}
				}
			}
			/*$sql_get_pro2 = mysql_query("select * from artist_event where creators_info='".$chk_type."'");
			if(mysql_num_rows($sql_get_pro2)>0)
			{
				while($row = mysql_fetch_assoc($sql_get_pro2))
				{
					$creator_info = "artist_event|".$row['id'];
					$get_media = mysql_query("select * from general_media where creator_info = '".$creator_info."'");
					if(mysql_num_rows($get_media)>0)
					{
						while($res = mysql_fetch_assoc($get_media))
						{
							$res_media11[] = $res;
						}
					}
				}
			}
			$sql_get_pro3 = mysql_query("select * from community_event where creators_info='".$chk_type."'");
			if(mysql_num_rows($sql_get_pro3)>0)
			{
				while($row1 = mysql_fetch_assoc($sql_get_pro3))
				{
					$creator_info = "community_event|".$row1['id'];
					$get_media = mysql_query("select * from general_media where creator_info = '".$creator_info."'");
					if(mysql_num_rows($get_media)>0)
					{
						while($res = mysql_fetch_assoc($get_media))
						{
							$res_media12[] = $res;
						}
					}
				}
			}*/
			$find_project = mysql_query("select * from community_project where community_id='".$id['community_id']."' AND del_status=0 AND active=0");
			if(mysql_num_rows($find_project)>0)
			{
				while($get_all_pro = mysql_fetch_assoc($find_project))
				{
					$new_creator_info = "community_project|".$get_all_pro['id'];
					$sql_get_pro = mysql_query("select * from artist_project where creators_info='".$new_creator_info."' AND del_status=0 AND active=0");
					if(mysql_num_rows($sql_get_pro)>0)
					{
						while($row = mysql_fetch_assoc($sql_get_pro))
						{
							$creator_info = "artist_project|".$row['id'];
							$get_media = mysql_query("select * from general_media where creator_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
							if(mysql_num_rows($get_media)>0)
							{
								while($res = mysql_fetch_assoc($get_media))
								{
									$res_media13[] = $res;
								}
							}
						}
					}
					$sql_get_pro1 = mysql_query("select * from community_project where creators_info='".$new_creator_info."' AND del_status=0 AND active=0");
					if(mysql_num_rows($sql_get_pro1)>0)
					{
						while($row1 = mysql_fetch_assoc($sql_get_pro1))
						{
							$creator_info = "community_project|".$row1['id'];
							$get_media = mysql_query("select * from general_media where creator_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
							if(mysql_num_rows($get_media)>0)
							{
								while($res = mysql_fetch_assoc($get_media))
								{
									$res_media14[] = $res;
								}
							}
						}
					}
					/*$sql_get_pro2 = mysql_query("select * from artist_event where creators_info='".$new_creator_info."'");
					if(mysql_num_rows($sql_get_pro2)>0)
					{
						while($row = mysql_fetch_assoc($sql_get_pro2))
						{
							$creator_info = "artist_event|".$row['id'];
							$get_media = mysql_query("select * from general_media where creator_info = '".$creator_info."'");
							if(mysql_num_rows($get_media)>0)
							{
								while($res = mysql_fetch_assoc($get_media))
								{
									$res_media15[] = $res;
								}
							}
						}
					}
					$sql_get_pro3 = mysql_query("select * from community_event where creators_info='".$new_creator_info."'");
					if(mysql_num_rows($sql_get_pro3)>0)
					{
						while($row1 = mysql_fetch_assoc($sql_get_pro3))
						{
							$creator_info = "community_event|".$row1['id'];
							$get_media = mysql_query("select * from general_media where creator_info = '".$creator_info."'");
							if(mysql_num_rows($get_media)>0)
							{
								while($res = mysql_fetch_assoc($get_media))
								{
									$res_media16[] = $res;
								}
							}
						}
					}*/
				}
			}
		   
		}
		   
		$get_all_media = array_merge($res_media1,$res_media2,$res_media3,$res_media4,$res_media5,$res_media6,$res_media7,$res_media8,$res_media9,$res_media10,$res_media11,$res_media12,$res_media13,$res_media14,$res_media15,$res_media16);
		//var_dump($get_all_media);
		return($get_all_media);
	}
	function get_from_heis()
	{
		$id = $this->selgeneral();
        $var_type_1 = 'general_artist';
        $var_type_2 = 'general_community';
        $var_type_3 = 'artist_project';
        $var_type_4 = 'community_project';
		$res_media1 = array();
		$res_media2 = array();
		$res_media3 = array();
		$res_media4 = array();
		$res_media5 = array();
		$res_media6 = array();
		$res_media7 = array();
		$res_media8 = array();
		$res_media9 = array();
		$res_media10 = array();
		$res_media11 = array();
		$res_media12 = array();
		$res_media13 = array();
		$res_media14 = array();
		$res_media15 = array();
		$res_media16 = array();
        if($id['artist_id']!=0 && !empty($id['artist_id']))
        {
			$chk_type = $var_type_1.'|'.$id['artist_id'];
			$sql_get_pro = mysql_query("select * from artist_project where creators_info='".$chk_type."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_get_pro)>0)
			{
				while($row = mysql_fetch_assoc($sql_get_pro))
				{
					$creator_info = "artist_project|".$row['id'];
					$get_media = mysql_query("select * from general_media where from_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
					if(mysql_num_rows($get_media)>0)
					{
						while($res = mysql_fetch_assoc($get_media))
						{
							$res_media1[] = $res;
						}
					}
				}
			}
			$sql_get_pro1 = mysql_query("select * from community_project where creators_info='".$chk_type."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_get_pro1)>0)
			{
				while($row1 = mysql_fetch_assoc($sql_get_pro1))
				{
					$creator_info = "community_project|".$row1['id'];
					$get_media = mysql_query("select * from general_media where from_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
					if(mysql_num_rows($get_media)>0)
					{
						while($res = mysql_fetch_assoc($get_media))
						{
							$res_media2[] = $res;
						}
					}
				}
			}
			$sql_get_pro1 = mysql_query("select * from community_event where creators_info='".$chk_type."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_get_pro1)>0)
			{
				while($row1 = mysql_fetch_assoc($sql_get_pro1))
				{
					$creator_info = "community_event|".$row1['id'];
					$get_media = mysql_query("select * from general_media where from_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
					if(mysql_num_rows($get_media)>0)
					{
						while($res = mysql_fetch_assoc($get_media))
						{
							$res_media3[] = $res;
						}
					}
				}
			}
			$sql_get_pro1 = mysql_query("select * from artist_event where creators_info='".$chk_type."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_get_pro1)>0)
			{
				while($row1 = mysql_fetch_assoc($sql_get_pro1))
				{
					$creator_info = "artist_event|".$row1['id'];
					$get_media = mysql_query("select * from general_media where from_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
					if(mysql_num_rows($get_media)>0)
					{
						while($res = mysql_fetch_assoc($get_media))
						{
							$res_media4[] = $res;
						}
					}
				}
			}
			
			
			$find_project = mysql_query("select * from artist_project where artist_id='".$id['artist_id']."' AND del_status=0 AND active=0");
			if(mysql_num_rows($find_project)>0)
			{
				while($get_all_pro = mysql_fetch_assoc($find_project))
				{
					$new_creator_info = "artist_project|".$get_all_pro['id'];
					//echo "select * from artist_project where creators_info='".$new_creator_info."'";
					$sql_get_pro = mysql_query("select * from artist_project where creators_info='".$new_creator_info."' AND del_status=0 AND active=0");
					if(mysql_num_rows($sql_get_pro)>0)
					{
						while($row = mysql_fetch_assoc($sql_get_pro))
						{
							$creator_info = "artist_project|".$row['id'];
							$get_media = mysql_query("select * from general_media where from_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
							if(mysql_num_rows($get_media)>0)
							{
								while($res = mysql_fetch_assoc($get_media))
								{
									if($id['general_user_id'] != $res['general_user_id'])
									{
										$res_media5[] = $res;	}
								}
							}
						}
					}
					$sql_get_pro1 = mysql_query("select * from community_project where creators_info='".$new_creator_info."' AND del_status=0 AND active=0");
					if(mysql_num_rows($sql_get_pro1)>0)
					{
						while($row1 = mysql_fetch_assoc($sql_get_pro1))
						{
							$creator_info = "community_project|".$row1['id'];
							$get_media = mysql_query("select * from general_media where from_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
							if(mysql_num_rows($get_media)>0)
							{
								while($res = mysql_fetch_assoc($get_media))
								{
									if($id['general_user_id'] != $res['general_user_id'])
									{
										$res_media6[] = $res;}
								}
							}
						}
					}
					$sql_get_pro2 = mysql_query("select * from artist_event where creators_info='".$new_creator_info."' AND del_status=0 AND active=0");
					if(mysql_num_rows($sql_get_pro2)>0)
					{
						while($row = mysql_fetch_assoc($sql_get_pro2))
						{
							$creator_info = "artist_event|".$row['id'];
							$get_media = mysql_query("select * from general_media where from_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
							if(mysql_num_rows($get_media)>0)
							{
								while($res = mysql_fetch_assoc($get_media))
								{
									if($id['general_user_id'] != $res['general_user_id'])
									{
										$res_media7[] = $res;}
								}
							}
						}
					}
					$sql_get_pro3 = mysql_query("select * from community_event where creators_info='".$new_creator_info."' AND del_status=0 AND active=0");
					if(mysql_num_rows($sql_get_pro3)>0)
					{
						while($row1 = mysql_fetch_assoc($sql_get_pro3))
						{
							$creator_info = "community_event|".$row1['id'];
							$get_media = mysql_query("select * from general_media where from_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
							if(mysql_num_rows($get_media)>0)
							{
								while($res = mysql_fetch_assoc($get_media))
								{
									if($id['general_user_id'] != $res['general_user_id'])
									{
										$res_media8[] = $res;}
								}
							}
						}
					}
				}
			}
			$find_project = mysql_query("select * from artist_event where artist_id='".$id['artist_id']."' AND del_status=0 AND active=0");
			if(mysql_num_rows($find_project)>0)
			{
				while($get_all_pro = mysql_fetch_assoc($find_project))
				{
					$new_creator_info = "artist_event|".$get_all_pro['id'];
					//echo "select * from artist_project where creators_info='".$new_creator_info."'";
					$sql_get_pro = mysql_query("select * from artist_project where creators_info='".$new_creator_info."' AND del_status=0 AND active=0");
					if(mysql_num_rows($sql_get_pro)>0)
					{
						while($row = mysql_fetch_assoc($sql_get_pro))
						{
							$creator_info = "artist_project|".$row['id'];
							$get_media = mysql_query("select * from general_media where from_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
							if(mysql_num_rows($get_media)>0)
							{
								while($res = mysql_fetch_assoc($get_media))
								{
									if($id['general_user_id'] != $res['general_user_id'])
									{
										$res_media5[] = $res;	}
								}
							}
						}
					}
					$sql_get_pro1 = mysql_query("select * from community_project where creators_info='".$new_creator_info."' AND del_status=0 AND active=0");
					if(mysql_num_rows($sql_get_pro1)>0)
					{
						while($row1 = mysql_fetch_assoc($sql_get_pro1))
						{
							$creator_info = "community_project|".$row1['id'];
							$get_media = mysql_query("select * from general_media where from_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
							if(mysql_num_rows($get_media)>0)
							{
								while($res = mysql_fetch_assoc($get_media))
								{
									if($id['general_user_id'] != $res['general_user_id'])
									{
										$res_media6[] = $res;}
								}
							}
						}
					}
					$sql_get_pro2 = mysql_query("select * from artist_event where creators_info='".$new_creator_info."' AND del_status=0 AND active=0");
					if(mysql_num_rows($sql_get_pro2)>0)
					{
						while($row = mysql_fetch_assoc($sql_get_pro2))
						{
							$creator_info = "artist_event|".$row['id'];
							$get_media = mysql_query("select * from general_media where from_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
							if(mysql_num_rows($get_media)>0)
							{
								while($res = mysql_fetch_assoc($get_media))
								{
									if($id['general_user_id'] != $res['general_user_id'])
									{
										$res_media7[] = $res;}
								}
							}
						}
					}
					$sql_get_pro3 = mysql_query("select * from community_event where creators_info='".$new_creator_info."' AND del_status=0 AND active=0");
					if(mysql_num_rows($sql_get_pro3)>0)
					{
						while($row1 = mysql_fetch_assoc($sql_get_pro3))
						{
							$creator_info = "community_event|".$row1['id'];
							$get_media = mysql_query("select * from general_media where from_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
							if(mysql_num_rows($get_media)>0)
							{
								while($res = mysql_fetch_assoc($get_media))
								{
									if($id['general_user_id'] != $res['general_user_id'])
									{
										$res_media8[] = $res;}
								}
							}
						}
					}
				}
			}
		   
		}
		if($id['community_id']!=0 && !empty($id['community_id']))
        {
		
			$chk_type = $var_type_2.'|'.$id['community_id'];
			$sql_get_pro = mysql_query("select * from artist_project where creators_info='".$chk_type."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_get_pro)>0)
			{
				while($row = mysql_fetch_assoc($sql_get_pro))
				{
					$creator_info = "artist_project|".$row['id'];
					$get_media = mysql_query("select * from general_media where from_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
					if(mysql_num_rows($get_media)>0)
					{
						while($res = mysql_fetch_assoc($get_media))
						{
							$res_media9[] = $res;
						}
					}
				}
			}
			$sql_get_pro1 = mysql_query("select * from community_project where creators_info='".$chk_type."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_get_pro1)>0)
			{
				while($row1 = mysql_fetch_assoc($sql_get_pro1))
				{
					$creator_info = "community_project|".$row1['id'];
					$get_media = mysql_query("select * from general_media where from_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
					if(mysql_num_rows($get_media)>0)
					{
						while($res = mysql_fetch_assoc($get_media))
						{
							$res_media10[] = $res;
						}
					}
				}
			}
			$sql_get_pro1 = mysql_query("select * from community_event where creators_info='".$chk_type."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_get_pro1)>0)
			{
				while($row1 = mysql_fetch_assoc($sql_get_pro1))
				{
					$creator_info = "community_event|".$row1['id'];
					$get_media = mysql_query("select * from general_media where from_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
					if(mysql_num_rows($get_media)>0)
					{
						while($res = mysql_fetch_assoc($get_media))
						{
							$res_media11[] = $res;
						}
					}
				}
			}
			$sql_get_pro1 = mysql_query("select * from artist_event where creators_info='".$chk_type."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_get_pro1)>0)
			{
				while($row1 = mysql_fetch_assoc($sql_get_pro1))
				{
					$creator_info = "artist_event|".$row1['id'];
					$get_media = mysql_query("select * from general_media where from_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
					if(mysql_num_rows($get_media)>0)
					{
						while($res = mysql_fetch_assoc($get_media))
						{
							$res_media12[] = $res;
						}
					}
				}
			}
		
			$find_project = mysql_query("select * from community_project where community_id='".$id['community_id']."' AND del_status=0 AND active=0");
			if(mysql_num_rows($find_project)>0)
			{
				while($get_all_pro = mysql_fetch_assoc($find_project))
				{
					$new_creator_info = "community_project|".$get_all_pro['id'];
					$sql_get_pro = mysql_query("select * from artist_project where creators_info='".$new_creator_info."' AND del_status=0 AND active=0");
					if(mysql_num_rows($sql_get_pro)>0)
					{
						while($row = mysql_fetch_assoc($sql_get_pro))
						{
							$creator_info = "artist_project|".$row['id'];
							$get_media = mysql_query("select * from general_media where from_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
							if(mysql_num_rows($get_media)>0)
							{
								while($res = mysql_fetch_assoc($get_media))
								{
									if($id['general_user_id'] != $res['general_user_id'])
									{
										$res_media13[] = $res;}
								}
							}
						}
					}
					$sql_get_pro1 = mysql_query("select * from community_project where creators_info='".$new_creator_info."' AND del_status=0 AND active=0");
					if(mysql_num_rows($sql_get_pro1)>0)
					{
						while($row1 = mysql_fetch_assoc($sql_get_pro1))
						{
							$creator_info = "community_project|".$row1['id'];
							$get_media = mysql_query("select * from general_media where from_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
							if(mysql_num_rows($get_media)>0)
							{
								while($res = mysql_fetch_assoc($get_media))
								{
									if($id['general_user_id'] != $res['general_user_id'])
									{
										$res_media14[] = $res;}
								}
							}
						}
					}
					$sql_get_pro2 = mysql_query("select * from artist_event where creators_info='".$new_creator_info."' AND del_status=0 AND active=0");
					if(mysql_num_rows($sql_get_pro2)>0)
					{
						while($row = mysql_fetch_assoc($sql_get_pro2))
						{
							$creator_info = "artist_event|".$row['id'];
							$get_media = mysql_query("select * from general_media where from_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
							if(mysql_num_rows($get_media)>0)
							{
								while($res = mysql_fetch_assoc($get_media))
								{
									if($id['general_user_id'] != $res['general_user_id'])
									{
										$res_media15[] = $res;}
								}
							}
						}
					}
					$sql_get_pro3 = mysql_query("select * from community_event where creators_info='".$new_creator_info."'");
					if(mysql_num_rows($sql_get_pro3)>0)
					{
						while($row1 = mysql_fetch_assoc($sql_get_pro3))
						{
							$creator_info = "community_event|".$row1['id'];
							$get_media = mysql_query("select * from general_media where from_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
							if(mysql_num_rows($get_media)>0)
							{
								while($res = mysql_fetch_assoc($get_media))
								{
									if($id['general_user_id'] != $res['general_user_id'])
									{
										$res_media16[] = $res;}
								}
							}
						}
					}
				}
			}
			$find_project = mysql_query("select * from community_event where community_id='".$id['community_id']."' AND del_status=0 AND active=0");
			if(mysql_num_rows($find_project)>0)
			{
				while($get_all_pro = mysql_fetch_assoc($find_project))
				{
					$new_creator_info = "community_event|".$get_all_pro['id'];
					$sql_get_pro = mysql_query("select * from artist_project where creators_info='".$new_creator_info."' AND del_status=0 AND active=0");
					if(mysql_num_rows($sql_get_pro)>0)
					{
						while($row = mysql_fetch_assoc($sql_get_pro))
						{
							$creator_info = "artist_project|".$row['id'];
							$get_media = mysql_query("select * from general_media where from_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
							if(mysql_num_rows($get_media)>0)
							{
								while($res = mysql_fetch_assoc($get_media))
								{
									if($id['general_user_id'] != $res['general_user_id'])
									{
										$res_media13[] = $res;}
								}
							}
						}
					}
					$sql_get_pro1 = mysql_query("select * from community_project where creators_info='".$new_creator_info."' AND del_status=0 AND active=0");
					if(mysql_num_rows($sql_get_pro1)>0)
					{
						while($row1 = mysql_fetch_assoc($sql_get_pro1))
						{
							$creator_info = "community_project|".$row1['id'];
							$get_media = mysql_query("select * from general_media where from_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
							if(mysql_num_rows($get_media)>0)
							{
								while($res = mysql_fetch_assoc($get_media))
								{
									if($id['general_user_id'] != $res['general_user_id'])
									{
										$res_media14[] = $res;}
								}
							}
						}
					}
					$sql_get_pro2 = mysql_query("select * from artist_event where creators_info='".$new_creator_info."' AND del_status=0 AND active=0");
					if(mysql_num_rows($sql_get_pro2)>0)
					{
						while($row = mysql_fetch_assoc($sql_get_pro2))
						{
							$creator_info = "artist_event|".$row['id'];
							$get_media = mysql_query("select * from general_media where from_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
							if(mysql_num_rows($get_media)>0)
							{
								while($res = mysql_fetch_assoc($get_media))
								{
									if($id['general_user_id'] != $res['general_user_id'])
									{
										$res_media15[] = $res;}
								}
							}
						}
					}
					$sql_get_pro3 = mysql_query("select * from community_event where creators_info='".$new_creator_info."' AND del_status=0 AND active=0");
					if(mysql_num_rows($sql_get_pro3)>0)
					{
						while($row1 = mysql_fetch_assoc($sql_get_pro3))
						{
							$creator_info = "community_event|".$row1['id'];
							$get_media = mysql_query("select * from general_media where from_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
							if(mysql_num_rows($get_media)>0)
							{
								while($res = mysql_fetch_assoc($get_media))
								{
									if($id['general_user_id'] != $res['general_user_id'])
									{
										$res_media16[] = $res;}
								}
							}
						}
					}
				}
			}
		   
		}
		   
		$get_all_media = array_merge($res_media1,$res_media2,$res_media3,$res_media4,$res_media5,$res_media6,$res_media7,$res_media8,$res_media9,$res_media10,$res_media11,$res_media12,$res_media13,$res_media14,$res_media15,$res_media16);
		return($get_all_media);
	}
	/*
	old function
	function get_from_heis()
	{
		$id = $this->selgeneral();
        $var_type_1 = 'general_artist';
        $var_type_2 = 'general_community';
        $var_type_3 = 'artist_project';
        $var_type_4 = 'community_project';
		$res_media1 = array();
		$res_media2 = array();
		$res_media3 = array();
		$res_media4 = array();
		$res_media5 = array();
		$res_media6 = array();
		$res_media7 = array();
		$res_media8 = array();
		$res_media9 = array();
		$res_media10 = array();
		$res_media11 = array();
		$res_media12 = array();
		$res_media13 = array();
		$res_media14 = array();
		$res_media15 = array();
		$res_media16 = array();
        if($id['artist_id']!=0 && !empty($id['artist_id']))
        {
			$find_project = mysql_query("select * from artist_project where artist_id='".$id['artist_id']."'");
			if(mysql_num_rows($find_project)>0)
			{
				while($get_all_pro = mysql_fetch_assoc($find_project))
				{
					$new_creator_info = "artist_project|".$get_all_pro['id'];
					$sql_get_pro = mysql_query("select * from artist_project where creators_info='".$new_creator_info."'");
					if(mysql_num_rows($sql_get_pro)>0)
					{
						while($row = mysql_fetch_assoc($sql_get_pro))
						{
							$creator_info = "artist_project|".$row['id'];
							$get_media = mysql_query("select * from general_media where from_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
							if(mysql_num_rows($get_media)>0)
							{
								while($res = mysql_fetch_assoc($get_media))
								{
									$res_media5[] = $res;
								}
							}
						}
					}
					$sql_get_pro1 = mysql_query("select * from community_project where creators_info='".$new_creator_info."'");
					if(mysql_num_rows($sql_get_pro1)>0)
					{
						while($row1 = mysql_fetch_assoc($sql_get_pro1))
						{
							$creator_info = "community_project|".$row1['id'];
							$get_media = mysql_query("select * from general_media where from_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
							if(mysql_num_rows($get_media)>0)
							{
								while($res = mysql_fetch_assoc($get_media))
								{
									$res_media6[] = $res;
								}
							}
						}
					}
					$sql_get_pro2 = mysql_query("select * from artist_event where creators_info='".$new_creator_info."'");
					if(mysql_num_rows($sql_get_pro2)>0)
					{
						while($row = mysql_fetch_assoc($sql_get_pro2))
						{
							$creator_info = "artist_event|".$row['id'];
							$get_media = mysql_query("select * from general_media where from_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
							if(mysql_num_rows($get_media)>0)
							{
								while($res = mysql_fetch_assoc($get_media))
								{
									$res_media7[] = $res;
								}
							}
						}
					}
					$sql_get_pro3 = mysql_query("select * from community_event where creators_info='".$new_creator_info."'");
					if(mysql_num_rows($sql_get_pro3)>0)
					{
						while($row1 = mysql_fetch_assoc($sql_get_pro3))
						{
							$creator_info = "community_event|".$row1['id'];
							$get_media = mysql_query("select * from general_media where from_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
							if(mysql_num_rows($get_media)>0)
							{
								while($res = mysql_fetch_assoc($get_media))
								{
									$res_media8[] = $res;
								}
							}
						}
					}
				}
			}
		   
		}
		if($id['community_id']!=0 && !empty($id['community_id']))
        {
			$find_project = mysql_query("select * from community_project where community_id='".$id['community_id']."'");
			if(mysql_num_rows($find_project)>0)
			{
				while($get_all_pro = mysql_fetch_assoc($find_project))
				{
					$new_creator_info = "community_project|".$get_all_pro['id'];
					$sql_get_pro = mysql_query("select * from artist_project where creators_info='".$new_creator_info."'");
					if(mysql_num_rows($sql_get_pro)>0)
					{
						while($row = mysql_fetch_assoc($sql_get_pro))
						{
							$creator_info = "artist_project|".$row['id'];
							$get_media = mysql_query("select * from general_media where from_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
							if(mysql_num_rows($get_media)>0)
							{
								while($res = mysql_fetch_assoc($get_media))
								{
									$res_media13[] = $res;
								}
							}
						}
					}
					$sql_get_pro1 = mysql_query("select * from community_project where creators_info='".$new_creator_info."'");
					if(mysql_num_rows($sql_get_pro1)>0)
					{
						while($row1 = mysql_fetch_assoc($sql_get_pro1))
						{
							$creator_info = "community_project|".$row1['id'];
							$get_media = mysql_query("select * from general_media where from_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
							if(mysql_num_rows($get_media)>0)
							{
								while($res = mysql_fetch_assoc($get_media))
								{
									$res_media14[] = $res;
								}
							}
						}
					}
					$sql_get_pro2 = mysql_query("select * from artist_event where creators_info='".$new_creator_info."'");
					if(mysql_num_rows($sql_get_pro2)>0)
					{
						while($row = mysql_fetch_assoc($sql_get_pro2))
						{
							$creator_info = "artist_event|".$row['id'];
							$get_media = mysql_query("select * from general_media where from_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
							if(mysql_num_rows($get_media)>0)
							{
								while($res = mysql_fetch_assoc($get_media))
								{
									$res_media15[] = $res;
								}
							}
						}
					}
					$sql_get_pro3 = mysql_query("select * from community_event where creators_info='".$new_creator_info."'");
					if(mysql_num_rows($sql_get_pro3)>0)
					{
						while($row1 = mysql_fetch_assoc($sql_get_pro3))
						{
							$creator_info = "community_event|".$row1['id'];
							$get_media = mysql_query("select * from general_media where from_info = '".$creator_info."' AND delete_status=0 AND media_status=0");
							if(mysql_num_rows($get_media)>0)
							{
								while($res = mysql_fetch_assoc($get_media))
								{
									$res_media16[] = $res;
								}
							}
						}
					}
				}
			}
		   
		}
		   
		$get_all_media = array_merge($res_media5,$res_media6,$res_media7,$res_media8,$res_media13,$res_media14,$res_media15,$res_media16);
		return($get_all_media);
	}*/
	function get_the_user_tagin_project($email)
	{
		$id = $this->selgeneral();
        $var_type_1 = 'general_artist';
        $var_type_2 = 'general_community';
        $var_type_3 = 'artist_project';
        $var_type_4 = 'community_project';
		$res_media1 = array();
		$res_media2 = array();
		$res_media3 = array();
		$res_media4 = array();

		$get_all_art_project = mysql_query("SELECT * FROM artist_project WHERE tagged_user_email LIKE '%".$email."%' AND del_status=0 AND active=0");
		if(mysql_num_rows($get_all_art_project)>0)
		{
			while($res_data = mysql_fetch_assoc($get_all_art_project))
			{
				$creator_info = "artist_project|".$res_data['id'];
				$get_media = mysql_query("select * from general_media where creator_info ='".$creator_info."' OR from_info ='".$creator_info."' AND delete_status=0 AND media_status=0");
				if(mysql_num_rows($get_media)>0)
				{
					while($res_media_data = mysql_fetch_assoc($get_media))
					{
						$res_media1[] = $res_media_data;
					}
				}
			}
		}
		$get_all_art_event = mysql_query("SELECT * FROM artist_event WHERE tagged_user_email LIKE '%".$email."%' AND del_status=0 AND active=0");
		if(mysql_num_rows($get_all_art_event)>0)
		{
			while($res_data = mysql_fetch_assoc($get_all_art_event))
			{
				$creator_info = "artist_event|".$res_data['id'];
				$get_media = mysql_query("select * from general_media where creator_info ='".$creator_info."' OR from_info ='".$creator_info."' AND delete_status=0 AND media_status=0");
				if(mysql_num_rows($get_media)>0)
				{
					while($res_media_data = mysql_fetch_assoc($get_media))
					{
						$res_media2[] = $res_media_data;
					}
				}
			}
		}
		$get_all_com_event = mysql_query("SELECT * FROM community_event WHERE tagged_user_email LIKE '%".$email."%' AND del_status=0 AND active=0");
		if(mysql_num_rows($get_all_com_event)>0)
		{
			while($res_data = mysql_fetch_assoc($get_all_com_event))
			{
				$creator_info = "community_event|".$res_data['id'];
				$get_media = mysql_query("select * from general_media where creator_info ='".$creator_info."' OR from_info ='".$creator_info."' AND delete_status=0 AND media_status=0");
				if(mysql_num_rows($get_media)>0)
				{
					while($res_media_data = mysql_fetch_assoc($get_media))
					{
						$res_media3[] = $res_media_data;
					}
				}
			}
		}
		$get_all_com_project = mysql_query("SELECT * FROM community_project WHERE tagged_user_email LIKE '%".$email."%' AND del_status=0 AND active=0");
		if(mysql_num_rows($get_all_com_project)>0)
		{
			while($res_data = mysql_fetch_assoc($get_all_com_project))
			{
				$creator_info = "community_project|".$res_data['id'];
				$get_media = mysql_query("select * from general_media where creator_info ='".$creator_info."' OR from_info ='".$creator_info."'  AND delete_status=0 AND media_status=0");
				if(mysql_num_rows($get_media)>0)
				{
					while($res_media_data = mysql_fetch_assoc($get_media))
					{
						$res_media4[] = $res_media_data;
					}
				}
			}
		}
	   
		$get_all_media = array_merge($res_media1,$res_media2,$res_media3,$res_media4);
		//var_dump($get_all_media);
		return($get_all_media);
	}
	function get_all_media_where_creator($creator_info,$from_info,$email)
	{
		$query = mysql_query("select * from general_media where creator_info='".$creator_info."' OR from_info='".$from_info."'");
		if(mysql_num_rows($query)>0)
		{
			while($res_media = mysql_fetch_assoc($query))
			{
				$res_all_media_id = $res_all_media_id .",".$res_media['id'];
			}
			$get_gen_det = mysql_query("select * from general_user where email='".$email."'");
			if(mysql_num_rows($get_gen_det)>0)
			{
				$res_remove_user_data = mysql_fetch_assoc($get_gen_det);
			}
			$exp_user_data = explode(",",$res_all_media_id);
			$exp_removing_user_data = explode(",",$res_remove_user_data['deleted_media_id']);
			for($count_user=0;$count_user<=count($exp_user_data);$count_user++)
			{
				if($exp_user_data[$count_user]==""){continue;}
				else{
					for($count_rem_user=0;$count_rem_user<=count($exp_removing_user_data);$count_rem_user++)
					{
						if($exp_removing_user_data[$count_rem_user]==""){continue;}
						else{
							if($exp_removing_user_data[$count_rem_user]==$exp_user_data[$count_user])
							{
								$exp_removing_user_data[$count_rem_user]="";
							}
						}
					}
				}
			}
			for($count_res=0;$count_res<=count($exp_removing_user_data);$count_res++)
			{
				if($exp_removing_user_data[$count_res]!="")
				{
					$new_id = $new_id .",".$exp_removing_user_data[$count_res];
				}
			}
			mysql_query("update general_user set deleted_media_id='".$new_id."' where email='".$email."'");
		}
	}
	
	function get_all_media_where_creator_or_from($creator_info,$from_info,$pre_creator_info)
	{
		$get_all_login_media = mysql_query("select * from general_media where creator_info='".$creator_info."' OR from_info='".$from_info."'");
		if(mysql_num_rows($get_all_login_media)>0)
		{
			while($res_all_media = mysql_fetch_assoc($get_all_login_media))
			{
				$get_all_media_id = $get_all_media_id .",". $res_all_media['id'];
			}

			$exp_pre_creator_info = explode("|",$pre_creator_info);
			if($exp_pre_creator_info[0]=="general_artist"){
				$get_general_det = mysql_query("select * general_user where artist_id=".$exp_pre_creator_info[1]."");
			}if($exp_pre_creator_info[0]=="general_community"){
				$get_general_det = mysql_query("select * from general_user where community_id=".$exp_pre_creator_info[1]."");
			}if($exp_pre_creator_info[0]=="community_project" || $exp_pre_creator_info[0]=="artist_project"){
				$get_pro_det = mysql_query("select * from ".$exp_pre_creator_info[0]." where id=".$exp_pre_creator_info[1]."");
				$res_pro_det = mysql_fetch_assoc($get_pro_det);
				if($exp_pre_creator_info[0]=="community_project"){
					$get_general_det = mysql_query("select * from general_user where community_id=".$res_pro_det['community_id']."");
				}if($exp_pre_creator_info[0]=="artist_project"){
					$get_general_det = mysql_query("select * from general_user where artist_id=".$res_pro_det['artist_id']."");
				}
			}
			if(mysql_num_rows($get_general_det)>0)
			{
				$res_general_det = mysql_fetch_assoc($get_general_det);
				$exp_res_media = explode(",",$get_all_media_id);
				$exp_deleted_cre = explode(",",$res_general_det['deleted_media_id']);
				//$exp_deleted_from = explode(",",$res_det['deleted_media_by_from_from']);
				for($count_media=0;$count_media<=count($exp_res_media);$count_media++)
				{
					if($exp_res_media[$count_media]==""){continue;}else{
						for($count_del_media_cre=0;$count_del_media_cre<=count($exp_deleted_cre);$count_del_media_cre++)
						{
							if($exp_deleted_cre[$count_del_media_cre]==""){continue;}else{
								if($exp_deleted_cre[$count_del_media_cre] == $exp_res_media[$count_media])
								{
									$exp_deleted_cre[$count_del_media_cre]="";
								}
							}
						}
					}
				}
				/*for($count_media=0;$count_media<=count($exp_res_media);$count_media++)
				{
					if($exp_res_media[$count_media]==""){continue;}else{
						for($count_del_media_from=0;$count_del_media_from<=count($exp_deleted_from);$count_del_media_from++)
						{
							if($exp_deleted_from[$count_del_media_from]==""){continue;}else{
								if($exp_deleted_from[$count_del_media_from] == $exp_res_media[$count_media])
								{
									$exp_deleted_from[$count_del_media_from]="";
								}
							}
						}
					}
				}*/
				$new_deletd_id_cre="";
				for($res_del_media=0;$res_del_media<=count($exp_deleted_cre);$res_del_media++)
				{
					if($exp_deleted_cre[$res_del_media]!="")
					{
						$new_deletd_id_cre = $new_deletd_id_cre .",".$exp_deleted_cre[$res_del_media];
					}
				}
				mysql_query("update general_user set deleted_media_id='".$new_deletd_id_cre."' where email='".$res_general_det['email']."'");
				/*$new_deletd_id_from="";
				for($res_del_media=0;$res_del_media<=count($exp_deleted_from);$res_del_media++)
				{
					if($exp_deleted_from[$res_del_media]!="")
					{
						$new_deletd_id_from = $new_deletd_id_from .",".$exp_deleted_from[$res_del_media];
					}
				}
				if($exp_pre_creator_info[0]=="general_artist")
				{
					mysql_query("update ".$exp_pre_creator_info[0]." set deleted_media_by_creator_creator='".$new_deletd_id_cre."', deleted_media_by_from_from='".$new_deletd_id_from."' where artist_id=".$exp_pre_creator_info[1]."");
				}
				else if($exp_pre_creator_info[0]=="general_community")
				{
					mysql_query("update ".$exp_pre_creator_info[0]." set deleted_media_by_creator_creator='".$new_deletd_id_cre."', deleted_media_by_from_from='".$new_deletd_id_from."' where community_id=".$exp_pre_creator_info[1]."");
				}
				else{
					mysql_query("update ".$exp_pre_creator_info[0]." set deleted_media_by_creator_creator='".$new_deletd_id_cre."', deleted_media_by_from_from='".$new_deletd_id_from."' where id=".$exp_pre_creator_info[1]."");
				}*/
			}
		}
	}
	
	function get_all_del_media_id()
	{
		$all_deleted_id = "";
		$sql = mysql_query("select * from general_user where email='".$_SESSION['login_email']."'");
		$res = mysql_fetch_assoc($sql);
		$all_deleted_id = $res['deleted_media_id'];
		return($all_deleted_id);
	}
	
	/*
	OLD ONE
	function get_all_del_media_id()
	{
		$all_deleted_id = "";
		$sql = mysql_query("select * from general_user where email='".$_SESSION['login_email']."'");
		$res = mysql_fetch_assoc($sql);
		$all_deleted_id = $res['deleted_media_by_tag_user'];
		if($res['artist_id'] >0)
		{
			$get_art = mysql_query("select * from general_artist where artist_id='".$res['artist_id']."'");
			if(mysql_num_rows($get_art)>0)
			{
				$res_art = mysql_fetch_assoc($get_art);
				if($res_art['deleted_media_by_creator_creator'] !="" || $res_art['deleted_media_by_from_from']!=""){
				$all_deleted_id = $all_deleted_id ."," . $res_art['deleted_media_by_creator_creator'].",".$res_art['deleted_media_by_from_from'];}
			}
			$get_art_pro = mysql_query("select * from artist_project where artist_id='".$res['artist_id']."'");
			if(mysql_num_rows($get_art_pro)>0)
			{
				while($res_art_pro = mysql_fetch_assoc($get_art_pro))
				{
					if($res_art_pro['deleted_media_by_creator_creator'] !="" || $res_art_pro['deleted_media_by_from_from'] !=""){
					$all_deleted_id = $all_deleted_id ."," . $res_art_pro['deleted_media_by_creator_creator'].",".$res_art_pro['deleted_media_by_from_from'];}
				}
			}
			$get_art_eve = mysql_query("select * from artist_event where artist_id='".$res['artist_id']."'");
			if(mysql_num_rows($get_art_eve)>0)
			{
				while($res_art_eve = mysql_fetch_assoc($get_art_eve))
				{
					if($res_art_eve['deleted_media_by_creator_creator'] !="" || $res_art_eve['deleted_media_by_from_from']!=""){
					$all_deleted_id = $all_deleted_id ."," . $res_art_eve['deleted_media_by_creator_creator'].",".$res_art_eve['deleted_media_by_from_from'];}
				}
			}
		}
		if($res['community_id'] >0)
		{
			$get_com = mysql_query("select * from general_community where community_id='".$res['community_id']."'");
			if(mysql_num_rows($get_com)>0)
			{
				$res_com = mysql_fetch_assoc($get_com);
				if($res_com['deleted_media_by_creator_creator'] !="" || $res_com['deleted_media_by_from_from'] !=""){
				$all_deleted_id = $all_deleted_id ."," . $res_com['deleted_media_by_creator_creator'].",".$res_com['deleted_media_by_from_from'];}
			}
			$get_com_pro = mysql_query("select * from community_project where community_id='".$res['community_id']."'");
			if(mysql_num_rows($get_com_pro)>0)
			{
				while($res_com_pro = mysql_fetch_assoc($get_com_pro))
				{
					if($res_com_pro['deleted_media_by_creator_creator'] !="" || $res_com_pro['deleted_media_by_from_from'] !=""){
					$all_deleted_id = $all_deleted_id ."," . $res_com_pro['deleted_media_by_creator_creator'].",".$res_com_pro['deleted_media_by_from_from'];}
				}
			}
			$get_com_eve = mysql_query("select * from community_event where community_id='".$res['community_id']."'");
			if(mysql_num_rows($get_com_eve)>0)
			{
				while($res_com_eve = mysql_fetch_assoc($get_com_eve))
				{
					if($res_com_eve['deleted_media_by_creator_creator'] !="" || $res_com_eve['deleted_media_by_from_from'] !=""){
					$all_deleted_id = $all_deleted_id ."," . $res_com_eve['deleted_media_by_creator_creator'].",".$res_com_eve['deleted_media_by_from_from'];}
				}
			}
		}
		return($all_deleted_id);
	}*/
	
	function delete_where_project_is_creator($creator_info,$pro_id)
	{
		/*get deleting project media*/
		$get_media = mysql_query("select * from general_media where creator_info='".$creator_info."' OR from_info='".$creator_info."'");
		if(mysql_num_rows($get_media)>0)
		{
			while($res_media = mysql_fetch_assoc($get_media))
			{
				$all_media_id = $all_media_id .",". $res_media['id'];
			}
		}
		$exp_all_media_id = explode(",",$all_media_id);
		/*get login user detail*/
		$get_gen_det = mysql_query("select * from general_user where email='".$_SESSION['login_email']."'");
		{
			if(mysql_num_rows($get_gen_det)>0)
			{
				$res_gen_det = mysql_fetch_assoc($get_gen_det);
				$exp_deleted_id = explode(",",$res_gen_det['deleted_media_id']);
				for($count_deleted=0;$count_deleted<=count($exp_deleted_id);$count_deleted++)
				{
					if($exp_deleted_id[$count_deleted]==""){
					continue;
					}else{
						for($count_media=0;$count_media<=count($exp_all_media_id);$count_media++)
						{
							if($exp_all_media_id[$count_media]==""){ continue; }else{
								if($exp_all_media_id[$count_media]==$exp_deleted_id[$count_deleted]){
									$exp_deleted_id[$count_deleted] ="";
								}
							}
						}
					}
				}
				$new_deleted_id="";
				for($count_new=0;$count_new<=count($exp_deleted_id);$count_new++)
				{
					if($exp_deleted_id[$count_new]!="")
					{
						$new_deleted_id = $new_deleted_id .",". $exp_deleted_id[$count_new];
					}
				}
				mysql_query("update general_user set deleted_media_id='".$new_deleted_id."' where email='".$_SESSION['login_email']."'");
				if($res_gen_det['artist_id']>0)
				{
					$get_art_det = mysql_query("select * from general_artist where artist_id='".$res_gen_det['artist_id']."'");
					if(mysql_num_rows($get_art_det)>0)
					{
						$res_art_det = mysql_fetch_assoc($get_art_det);
						$exp_song_id = explode(",",$res_art_det['taggedsongs']);
						$exp_video_id = explode(",",$res_art_det['taggedvideos']);
						$exp_gallery_id = explode(",",$res_art_det['taggedgalleries']);
						//$exp_deleted_cre_id = explode(",",$res_art_det['deleted_media_by_creator_creator']);
						//$exp_deleted_fro_id = explode(",",$res_art_det['deleted_media_by_from_from']);
						for($count_song=0;$count_song<=count($exp_song_id);$count_song++){
							if($exp_song_id[$count_song]!=""){
								for($count_media=0;$count_media<=count($exp_all_media_id);$count_media++){
									if($exp_all_media_id[$count_media]!=""){
										if($exp_all_media_id[$count_media] == $exp_song_id[$count_song]){
											$exp_song_id[$count_song] ="";
										}
									}
								}
							}
						}
						$new_song_id ="";
						for($count_new_song=0;$count_new_song<=count($exp_song_id);$count_new_song++){
							if($exp_song_id[$count_new_song]!=""){
								$new_song_id = $new_song_id .",".$exp_song_id[$count_new_song];
							}
						}
						for($count_video=0;$count_video<=count($exp_video_id);$count_video++){
							if($exp_video_id[$count_video]!=""){
								for($count_media=0;$count_media<=count($exp_all_media_id);$count_media++){
									if($exp_all_media_id[$count_media]!=""){
										if($exp_all_media_id[$count_media] == $exp_video_id[$count_video]){
											$exp_video_id[$count_video] ="";
										}
									}
								}
							}
						}
						$new_video_id ="";
						for($count_new_video=0;$count_new_video<=count($exp_video_id);$count_new_video++){
							if($exp_video_id[$count_new_video]!=""){
								$new_video_id = $new_video_id .",".$exp_video_id[$count_new_video];
							}
						}
						for($count_gal=0;$count_gal<=count($exp_gallery_id);$count_gal++){
							if($exp_gallery_id[$count_gal]!=""){
								for($count_media=0;$count_media<=count($exp_all_media_id);$count_media++){
									if($exp_all_media_id[$count_media]!=""){
										if($exp_all_media_id[$count_media] == $exp_gallery_id[$count_gal]){
											$exp_gallery_id[$count_gal] ="";
										}
									}
								}
							}
						}
						$new_gal_id ="";
						for($count_new_gal=0;$count_new_gal<=count($exp_gallery_id);$count_new_gal++){
							if($exp_gallery_id[$count_new_gal]!=""){
								$new_gal_id = $new_gal_id .",".$exp_gallery_id[$count_new_gal];
							}
						}
						/*for($count_del_cre=0;$count_del_cre<=count($exp_deleted_cre_id);$count_del_cre++){
							if($exp_deleted_cre_id[$count_del_cre]!=""){
								for($count_media=0;$count_media<=count($exp_all_media_id);$count_media++){
									if($exp_all_media_id[$count_media]!=""){
										if($exp_all_media_id[$count_media] == $exp_deleted_cre_id[$count_del_cre]){
											$exp_deleted_cre_id[$count_del_cre] ="";
										}
									}
								}
							}
						}
						$new_del_cre_id ="";
						for($count_new_del_cre=0;$count_new_del_cre<=count($exp_deleted_cre_id);$count_new_del_cre++){
							if($exp_deleted_cre_id[$count_new_del_cre]!=""){
								$new_del_cre_id = $new_del_cre_id .",".$exp_deleted_cre_id[$count_new_del_cre];
							}
						}
						for($count_del_fro=0;$count_del_fro<=count($exp_deleted_fro_id);$count_del_fro++){
							if($exp_deleted_fro_id[$count_del_fro]!=""){
								for($count_media=0;$count_media<=count($exp_all_media_id);$count_media++){
									if($exp_all_media_id[$count_media]!=""){
										if($exp_all_media_id[$count_media] == $exp_deleted_fro_id[$count_del_fro]){
											$exp_deleted_fro_id[$count_del_fro] ="";
										}
									}
								}
							}
						}
						$new_del_fro_id ="";
						for($count_new_del_fro=0;$count_new_del_fro<=count($exp_deleted_fro_id);$count_new_del_fro++){
							if($exp_deleted_cre_id[$count_new_del_fro]!=""){
								$new_del_fro_id = $new_del_fro_id .",".$exp_deleted_cre_id[$count_new_del_fro];
							}
						}
						mysql_query("update general_artist set taggedsongs ='".$new_song_id."', taggedvideos ='".$new_video_id."', taggedgalleries ='".$new_gal_id."', deleted_media_by_creator_creator ='".$new_del_cre_id."', deleted_media_by_from_from='".$new_del_fro_id."' where artist_id='".$res_art_det['artist_id']."'");
						*/
						mysql_query("update general_artist set taggedsongs ='".$new_song_id."', taggedvideos ='".$new_video_id."', taggedgalleries ='".$new_gal_id."'  where artist_id='".$res_art_det['artist_id']."'");
					}
					$get_artpro_det = mysql_query("select * from artist_project where artist_id='".$res_gen_det['artist_id']."'");
					if(mysql_num_rows($get_artpro_det)>0)
					{
						while($res_artpro_det = mysql_fetch_assoc($get_artpro_det))
						{
							$exp_song_id = explode(",",$res_artpro_det['tagged_songs']);
							$exp_video_id = explode(",",$res_artpro_det['tagged_videos']);
							$exp_gallery_id = explode(",",$res_artpro_det['tagged_galleries']);
							//$exp_deleted_cre_id = explode(",",$res_artpro_det['deleted_media_by_creator_creator']);
							//$exp_deleted_fro_id = explode(",",$res_artpro_det['deleted_media_by_from_from']);
							for($count_song=0;$count_song<=count($exp_song_id);$count_song++){
								if($exp_song_id[$count_song]!=""){
									for($count_media=0;$count_media<=count($exp_all_media_id);$count_media++){
										if($exp_all_media_id[$count_media]!=""){
											if($exp_all_media_id[$count_media] == $exp_song_id[$count_song]){
												$exp_song_id[$count_song] ="";
											}
										}
									}
								}
							}
							$new_song_id ="";
							for($count_new_song=0;$count_new_song<=count($exp_song_id);$count_new_song++){
								if($exp_song_id[$count_new_song]!=""){
									$new_song_id = $new_song_id .",".$exp_song_id[$count_new_song];
								}
							}
							for($count_video=0;$count_video<=count($exp_video_id);$count_video++){
								if($exp_video_id[$count_video]!=""){
									for($count_media=0;$count_media<=count($exp_all_media_id);$count_media++){
										if($exp_all_media_id[$count_media]!=""){
											if($exp_all_media_id[$count_media] == $exp_video_id[$count_video]){
												$exp_video_id[$count_video] ="";
											}
										}
									}
								}
							}
							$new_video_id ="";
							for($count_new_video=0;$count_new_video<=count($exp_video_id);$count_new_video++){
								if($exp_video_id[$count_new_video]!=""){
									$new_video_id = $new_video_id .",".$exp_video_id[$count_new_video];
								}
							}
							for($count_gal=0;$count_gal<=count($exp_gallery_id);$count_gal++){
								if($exp_gallery_id[$count_gal]!=""){
									for($count_media=0;$count_media<=count($exp_all_media_id);$count_media++){
										if($exp_all_media_id[$count_media]!=""){
											if($exp_all_media_id[$count_media] == $exp_gallery_id[$count_gal]){
												$exp_gallery_id[$count_gal] ="";
											}
										}
									}
								}
							}
							$new_gal_id ="";
							for($count_new_gal=0;$count_new_gal<=count($exp_gallery_id);$count_new_gal++){
								if($exp_gallery_id[$count_new_gal]!=""){
									$new_gal_id = $new_gal_id .",".$exp_gallery_id[$count_new_gal];
								}
							}
							/*for($count_del_cre=0;$count_del_cre<=count($exp_deleted_cre_id);$count_del_cre++){
								if($exp_deleted_cre_id[$count_del_cre]!=""){
									for($count_media=0;$count_media<=count($exp_all_media_id);$count_media++){
										if($exp_all_media_id[$count_media]!=""){
											if($exp_all_media_id[$count_media] == $exp_deleted_cre_id[$count_del_cre]){
												$exp_deleted_cre_id[$count_del_cre] ="";
											}
										}
									}
								}
							}
							$new_del_cre_id ="";
							for($count_new_del_cre=0;$count_new_del_cre<=count($exp_deleted_cre_id);$count_new_del_cre++){
								if($exp_deleted_cre_id[$count_new_del_cre]!=""){
									$new_del_cre_id = $new_del_cre_id .",".$exp_deleted_cre_id[$count_new_del_cre];
								}
							}
							for($count_del_fro=0;$count_del_fro<=count($exp_deleted_fro_id);$count_del_fro++){
								if($exp_deleted_fro_id[$count_del_fro]!=""){
									for($count_media=0;$count_media<=count($exp_all_media_id);$count_media++){
										if($exp_all_media_id[$count_media]!=""){
											if($exp_all_media_id[$count_media] == $exp_deleted_fro_id[$count_del_fro]){
												$exp_deleted_fro_id[$count_del_fro] ="";
											}
										}
									}
								}
							}
							$new_del_fro_id ="";
							for($count_new_del_fro=0;$count_new_del_fro<=count($exp_deleted_fro_id);$count_new_del_fro++){
								if($exp_deleted_cre_id[$count_new_del_fro]!=""){
									$new_del_fro_id = $new_del_fro_id .",".$exp_deleted_cre_id[$count_new_del_fro];
								}
							}
							mysql_query("update artist_project set tagged_songs ='".$new_song_id."', tagged_videos ='".$new_video_id."', tagged_galleries ='".$new_gal_id."', deleted_media_by_creator_creator ='".$new_del_cre_id."', deleted_media_by_from_from='".$new_del_fro_id."' where id='".$res_artpro_det['id']."'");
							*/
							
							mysql_query("update artist_project set tagged_songs ='".$new_song_id."', tagged_videos ='".$new_video_id."', tagged_galleries ='".$new_gal_id."' where id='".$res_artpro_det['id']."'");
						}
					}
					$get_arteve_det = mysql_query("select * from artist_event where artist_id='".$res_gen_det['artist_id']."'");
					if(mysql_num_rows($get_arteve_det)>0)
					{
						while($res_arteve_det = mysql_fetch_assoc($get_arteve_det))
						{
							$exp_song_id = explode(",",$res_arteve_det['tagged_songs']);
							$exp_video_id = explode(",",$res_arteve_det['tagged_videos']);
							$exp_gallery_id = explode(",",$res_arteve_det['tagged_galleries']);
							//$exp_deleted_cre_id = explode(",",$res_arteve_det['deleted_media_by_creator_creator']);
							//$exp_deleted_fro_id = explode(",",$res_arteve_det['deleted_media_by_from_from']);
							for($count_song=0;$count_song<=count($exp_song_id);$count_song++){
								if($exp_song_id[$count_song]!=""){
									for($count_media=0;$count_media<=count($exp_all_media_id);$count_media++){
										if($exp_all_media_id[$count_media]!=""){
											if($exp_all_media_id[$count_media] == $exp_song_id[$count_song]){
												$exp_song_id[$count_song] ="";
											}
										}
									}
								}
							}
							$new_song_id ="";
							for($count_new_song=0;$count_new_song<=count($exp_song_id);$count_new_song++){
								if($exp_song_id[$count_new_song]!=""){
									$new_song_id = $new_song_id .",".$exp_song_id[$count_new_song];
								}
							}
							for($count_video=0;$count_video<=count($exp_video_id);$count_video++){
								if($exp_video_id[$count_video]!=""){
									for($count_media=0;$count_media<=count($exp_all_media_id);$count_media++){
										if($exp_all_media_id[$count_media]!=""){
											if($exp_all_media_id[$count_media] == $exp_video_id[$count_video]){
												$exp_video_id[$count_video] ="";
											}
										}
									}
								}
							}
							$new_video_id ="";
							for($count_new_video=0;$count_new_video<=count($exp_video_id);$count_new_video++){
								if($exp_video_id[$count_new_video]!=""){
									$new_video_id = $new_video_id .",".$exp_video_id[$count_new_video];
								}
							}
							for($count_gal=0;$count_gal<=count($exp_gallery_id);$count_gal++){
								if($exp_gallery_id[$count_gal]!=""){
									for($count_media=0;$count_media<=count($exp_all_media_id);$count_media++){
										if($exp_all_media_id[$count_media]!=""){
											if($exp_all_media_id[$count_media] == $exp_gallery_id[$count_gal]){
												$exp_gallery_id[$count_gal] ="";
											}
										}
									}
								}
							}
							$new_gal_id ="";
							for($count_new_gal=0;$count_new_gal<=count($exp_gallery_id);$count_new_gal++){
								if($exp_gallery_id[$count_new_gal]!=""){
									$new_gal_id = $new_gal_id .",".$exp_gallery_id[$count_new_gal];
								}
							}
							
							/*for($count_del_cre=0;$count_del_cre<=count($exp_deleted_cre_id);$count_del_cre++){
								if($exp_deleted_cre_id[$count_del_cre]!=""){
									for($count_media=0;$count_media<=count($exp_all_media_id);$count_media++){
										if($exp_all_media_id[$count_media]!=""){
											if($exp_all_media_id[$count_media] == $exp_deleted_cre_id[$count_del_cre]){
												$exp_deleted_cre_id[$count_del_cre] ="";
											}
										}
									}
								}
							}
							$new_del_cre_id ="";
							for($count_new_del_cre=0;$count_new_del_cre<=count($exp_deleted_cre_id);$count_new_del_cre++){
								if($exp_deleted_cre_id[$count_new_del_cre]!=""){
									$new_del_cre_id = $new_del_cre_id .",".$exp_deleted_cre_id[$count_new_del_cre];
								}
							}
							for($count_del_fro=0;$count_del_fro<=count($exp_deleted_fro_id);$count_del_fro++){
								if($exp_deleted_fro_id[$count_del_fro]!=""){
									for($count_media=0;$count_media<=count($exp_all_media_id);$count_media++){
										if($exp_all_media_id[$count_media]!=""){
											if($exp_all_media_id[$count_media] == $exp_deleted_fro_id[$count_del_fro]){
												$exp_deleted_fro_id[$count_del_fro] ="";
											}
										}
									}
								}
							}
							$new_del_fro_id ="";
							for($count_new_del_fro=0;$count_new_del_fro<=count($exp_deleted_fro_id);$count_new_del_fro++){
								if($exp_deleted_cre_id[$count_new_del_fro]!=""){
									$new_del_fro_id = $new_del_fro_id .",".$exp_deleted_cre_id[$count_new_del_fro];
								}
							}
							mysql_query("update artist_event set tagged_songs ='".$new_song_id."', tagged_videos ='".$new_video_id."', tagged_galleries ='".$new_gal_id."', deleted_media_by_creator_creator ='".$new_del_cre_id."', deleted_media_by_from_from='".$new_del_fro_id."' where id='".$res_arteve_det['id']."'");
							*/
							
							mysql_query("update artist_event set tagged_songs ='".$new_song_id."', tagged_videos ='".$new_video_id."', tagged_galleries ='".$new_gal_id."'  where id='".$res_arteve_det['id']."'");
						}
					}
				}
				
				if($res_gen_det['community_id']>0)
				{
					$get_com_det = mysql_query("select * from general_community where community_id='".$res_gen_det['community_id']."'");
					if(mysql_num_rows($get_com_det)>0)
					{
						$res_com_det = mysql_fetch_assoc($get_com_det);
						$exp_song_id = explode(",",$res_com_det['taggedsongs']);
						$exp_video_id = explode(",",$res_com_det['taggedvideos']);
						$exp_gallery_id = explode(",",$res_com_det['taggedgalleries']);
						//$exp_deleted_cre_id = explode(",",$res_com_det['deleted_media_by_creator_creator']);
						//$exp_deleted_fro_id = explode(",",$res_com_det['deleted_media_by_from_from']);
						for($count_song=0;$count_song<=count($exp_song_id);$count_song++){
							if($exp_song_id[$count_song]!=""){
								for($count_media=0;$count_media<=count($exp_all_media_id);$count_media++){
									if($exp_all_media_id[$count_media]!=""){
										if($exp_all_media_id[$count_media] == $exp_song_id[$count_song]){
											$exp_song_id[$count_song] ="";
										}
									}
								}
							}
						}
						$new_song_id ="";
						for($count_new_song=0;$count_new_song<=count($exp_song_id);$count_new_song++){
							if($exp_song_id[$count_new_song]!=""){
								$new_song_id = $new_song_id .",".$exp_song_id[$count_new_song];
							}
						}
						for($count_video=0;$count_video<=count($exp_video_id);$count_video++){
							if($exp_video_id[$count_video]!=""){
								for($count_media=0;$count_media<=count($exp_all_media_id);$count_media++){
									if($exp_all_media_id[$count_media]!=""){
										if($exp_all_media_id[$count_media] == $exp_video_id[$count_video]){
											$exp_video_id[$count_video] ="";
										}
									}
								}
							}
						}
						$new_video_id ="";
						for($count_new_video=0;$count_new_video<=count($exp_video_id);$count_new_video++){
							if($exp_video_id[$count_new_video]!=""){
								$new_video_id = $new_video_id .",".$exp_video_id[$count_new_video];
							}
						}
						for($count_gal=0;$count_gal<=count($exp_gallery_id);$count_gal++){
							if($exp_gallery_id[$count_gal]!=""){
								for($count_media=0;$count_media<=count($exp_all_media_id);$count_media++){
									if($exp_all_media_id[$count_media]!=""){
										if($exp_all_media_id[$count_media] == $exp_gallery_id[$count_gal]){
											$exp_gallery_id[$count_gal] ="";
										}
									}
								}
							}
						}
						$new_gal_id ="";
						for($count_new_gal=0;$count_new_gal<=count($exp_gallery_id);$count_new_gal++){
							if($exp_gallery_id[$count_new_gal]!=""){
								$new_gal_id = $new_gal_id .",".$exp_gallery_id[$count_new_gal];
							}
						}
						
						
						/*for($count_del_cre=0;$count_del_cre<=count($exp_deleted_cre_id);$count_del_cre++){
							if($exp_deleted_cre_id[$count_del_cre]!=""){
								for($count_media=0;$count_media<=count($exp_all_media_id);$count_media++){
									if($exp_all_media_id[$count_media]!=""){
										if($exp_all_media_id[$count_media] == $exp_deleted_cre_id[$count_del_cre]){
											$exp_deleted_cre_id[$count_del_cre] ="";
										}
									}
								}
							}
						}
						$new_del_cre_id ="";
						for($count_new_del_cre=0;$count_new_del_cre<=count($exp_deleted_cre_id);$count_new_del_cre++){
							if($exp_deleted_cre_id[$count_new_del_cre]!=""){
								$new_del_cre_id = $new_del_cre_id .",".$exp_deleted_cre_id[$count_new_del_cre];
							}
						}
						for($count_del_fro=0;$count_del_fro<=count($exp_deleted_fro_id);$count_del_fro++){
							if($exp_deleted_fro_id[$count_del_fro]!=""){
								for($count_media=0;$count_media<=count($exp_all_media_id);$count_media++){
									if($exp_all_media_id[$count_media]!=""){
										if($exp_all_media_id[$count_media] == $exp_deleted_fro_id[$count_del_fro]){
											$exp_deleted_fro_id[$count_del_fro] ="";
										}
									}
								}
							}
						}
						$new_del_fro_id ="";
						for($count_new_del_fro=0;$count_new_del_fro<=count($exp_deleted_fro_id);$count_new_del_fro++){
							if($exp_deleted_cre_id[$count_new_del_fro]!=""){
								$new_del_fro_id = $new_del_fro_id .",".$exp_deleted_cre_id[$count_new_del_fro];
							}
						}
						mysql_query("update general_community set taggedsongs ='".$new_song_id."', taggedvideos ='".$new_video_id."', taggedgalleries ='".$new_gal_id."', deleted_media_by_creator_creator ='".$new_del_cre_id."', deleted_media_by_from_from='".$new_del_fro_id."' where community_id='".$res_com_det['community_id']."'");
						*/
						
						mysql_query("update general_community set taggedsongs ='".$new_song_id."', taggedvideos ='".$new_video_id."', taggedgalleries ='".$new_gal_id."' where community_id='".$res_com_det['community_id']."'");
					}
					$get_compro_det = mysql_query("select * from community_project where community_id='".$res_gen_det['community_id']."'");
					if(mysql_num_rows($get_compro_det)>0)
					{
						while($res_compro_det = mysql_fetch_assoc($get_compro_det))
						{
							$exp_song_id = explode(",",$res_compro_det['tagged_songs']);
							$exp_video_id = explode(",",$res_compro_det['tagged_videos']);
							$exp_gallery_id = explode(",",$res_compro_det['tagged_galleries']);
							//$exp_deleted_cre_id = explode(",",$res_compro_det['deleted_media_by_creator_creator']);
							//$exp_deleted_fro_id = explode(",",$res_compro_det['deleted_media_by_from_from']);
							for($count_song=0;$count_song<=count($exp_song_id);$count_song++){
								if($exp_song_id[$count_song]!=""){
									for($count_media=0;$count_media<=count($exp_all_media_id);$count_media++){
										if($exp_all_media_id[$count_media]!=""){
											if($exp_all_media_id[$count_media] == $exp_song_id[$count_song]){
												$exp_song_id[$count_song] ="";
											}
										}
									}
								}
							}
							$new_song_id ="";
							for($count_new_song=0;$count_new_song<=count($exp_song_id);$count_new_song++){
								if($exp_song_id[$count_new_song]!=""){
									$new_song_id = $new_song_id .",".$exp_song_id[$count_new_song];
								}
							}
							for($count_video=0;$count_video<=count($exp_video_id);$count_video++){
								if($exp_video_id[$count_video]!=""){
									for($count_media=0;$count_media<=count($exp_all_media_id);$count_media++){
										if($exp_all_media_id[$count_media]!=""){
											if($exp_all_media_id[$count_media] == $exp_video_id[$count_video]){
												$exp_video_id[$count_video] ="";
											}
										}
									}
								}
							}
							$new_video_id ="";
							for($count_new_video=0;$count_new_video<=count($exp_video_id);$count_new_video++){
								if($exp_video_id[$count_new_video]!=""){
									$new_video_id = $new_video_id .",".$exp_video_id[$count_new_video];
								}
							}
							for($count_gal=0;$count_gal<=count($exp_gallery_id);$count_gal++){
								if($exp_gallery_id[$count_gal]!=""){
									for($count_media=0;$count_media<=count($exp_all_media_id);$count_media++){
										if($exp_all_media_id[$count_media]!=""){
											if($exp_all_media_id[$count_media] == $exp_gallery_id[$count_gal]){
												$exp_gallery_id[$count_gal] ="";
											}
										}
									}
								}
							}
							$new_gal_id ="";
							for($count_new_gal=0;$count_new_gal<=count($exp_gallery_id);$count_new_gal++){
								if($exp_gallery_id[$count_new_gal]!=""){
									$new_gal_id = $new_gal_id .",".$exp_gallery_id[$count_new_gal];
								}
							}
							
							
							/*for($count_del_cre=0;$count_del_cre<=count($exp_deleted_cre_id);$count_del_cre++){
								if($exp_deleted_cre_id[$count_del_cre]!=""){
									for($count_media=0;$count_media<=count($exp_all_media_id);$count_media++){
										if($exp_all_media_id[$count_media]!=""){
											if($exp_all_media_id[$count_media] == $exp_deleted_cre_id[$count_del_cre]){
												$exp_deleted_cre_id[$count_del_cre] ="";
											}
										}
									}
								}
							}
							$new_del_cre_id ="";
							for($count_new_del_cre=0;$count_new_del_cre<=count($exp_deleted_cre_id);$count_new_del_cre++){
								if($exp_deleted_cre_id[$count_new_del_cre]!=""){
									$new_del_cre_id = $new_del_cre_id .",".$exp_deleted_cre_id[$count_new_del_cre];
								}
							}
							for($count_del_fro=0;$count_del_fro<=count($exp_deleted_fro_id);$count_del_fro++){
								if($exp_deleted_fro_id[$count_del_fro]!=""){
									for($count_media=0;$count_media<=count($exp_all_media_id);$count_media++){
										if($exp_all_media_id[$count_media]!=""){
											if($exp_all_media_id[$count_media] == $exp_deleted_fro_id[$count_del_fro]){
												$exp_deleted_fro_id[$count_del_fro] ="";
											}
										}
									}
								}
							}
							$new_del_fro_id ="";
							for($count_new_del_fro=0;$count_new_del_fro<=count($exp_deleted_fro_id);$count_new_del_fro++){
								if($exp_deleted_cre_id[$count_new_del_fro]!=""){
									$new_del_fro_id = $new_del_fro_id .",".$exp_deleted_cre_id[$count_new_del_fro];
								}
							}
							mysql_query("update community_project set tagged_songs ='".$new_song_id."', tagged_videos ='".$new_video_id."', tagged_galleries ='".$new_gal_id."', deleted_media_by_creator_creator ='".$new_del_cre_id."', deleted_media_by_from_from='".$new_del_fro_id."' where id='".$res_compro_det['id']."'");
							*/
							
							mysql_query("update community_project set tagged_songs ='".$new_song_id."', tagged_videos ='".$new_video_id."', tagged_galleries ='".$new_gal_id."' where id='".$res_compro_det['id']."'");
						}
					}
					$get_comeve_det = mysql_query("select * from community_event where community_id='".$res_gen_det['community_id']."'");
					if(mysql_num_rows($get_comeve_det)>0)
					{
						while($res_comeve_det = mysql_fetch_assoc($get_comeve_det))
						{
							$exp_song_id = explode(",",$res_comeve_det['tagged_songs']);
							$exp_video_id = explode(",",$res_comeve_det['tagged_videos']);
							$exp_gallery_id = explode(",",$res_comeve_det['tagged_galleries']);
							//$exp_deleted_cre_id = explode(",",$res_comeve_det['deleted_media_by_creator_creator']);
							//$exp_deleted_fro_id = explode(",",$res_comeve_det['deleted_media_by_from_from']);
							for($count_song=0;$count_song<=count($exp_song_id);$count_song++){
								if($exp_song_id[$count_song]!=""){
									for($count_media=0;$count_media<=count($exp_all_media_id);$count_media++){
										if($exp_all_media_id[$count_media]!=""){
											if($exp_all_media_id[$count_media] == $exp_song_id[$count_song]){
												$exp_song_id[$count_song] ="";
											}
										}
									}
								}
							}
							$new_song_id ="";
							for($count_new_song=0;$count_new_song<=count($exp_song_id);$count_new_song++){
								if($exp_song_id[$count_new_song]!=""){
									$new_song_id = $new_song_id .",".$exp_song_id[$count_new_song];
								}
							}
							for($count_video=0;$count_video<=count($exp_video_id);$count_video++){
								if($exp_video_id[$count_video]!=""){
									for($count_media=0;$count_media<=count($exp_all_media_id);$count_media++){
										if($exp_all_media_id[$count_media]!=""){
											if($exp_all_media_id[$count_media] == $exp_video_id[$count_video]){
												$exp_video_id[$count_video] ="";
											}
										}
									}
								}
							}
							$new_video_id ="";
							for($count_new_video=0;$count_new_video<=count($exp_video_id);$count_new_video++){
								if($exp_video_id[$count_new_video]!=""){
									$new_video_id = $new_video_id .",".$exp_video_id[$count_new_video];
								}
							}
							for($count_gal=0;$count_gal<=count($exp_gallery_id);$count_gal++){
								if($exp_gallery_id[$count_gal]!=""){
									for($count_media=0;$count_media<=count($exp_all_media_id);$count_media++){
										if($exp_all_media_id[$count_media]!=""){
											if($exp_all_media_id[$count_media] == $exp_gallery_id[$count_gal]){
												$exp_gallery_id[$count_gal] ="";
											}
										}
									}
								}
							}
							$new_gal_id ="";
							for($count_new_gal=0;$count_new_gal<=count($exp_gallery_id);$count_new_gal++){
								if($exp_gallery_id[$count_new_gal]!=""){
									$new_gal_id = $new_gal_id .",".$exp_gallery_id[$count_new_gal];
								}
							}
							
							/*
							for($count_del_cre=0;$count_del_cre<=count($exp_deleted_cre_id);$count_del_cre++){
								if($exp_deleted_cre_id[$count_del_cre]!=""){
									for($count_media=0;$count_media<=count($exp_all_media_id);$count_media++){
										if($exp_all_media_id[$count_media]!=""){
											if($exp_all_media_id[$count_media] == $exp_deleted_cre_id[$count_del_cre]){
												$exp_deleted_cre_id[$count_del_cre] ="";
											}
										}
									}
								}
							}
							$new_del_cre_id ="";
							for($count_new_del_cre=0;$count_new_del_cre<=count($exp_deleted_cre_id);$count_new_del_cre++){
								if($exp_deleted_cre_id[$count_new_del_cre]!=""){
									$new_del_cre_id = $new_del_cre_id .",".$exp_deleted_cre_id[$count_new_del_cre];
								}
							}
							for($count_del_fro=0;$count_del_fro<=count($exp_deleted_fro_id);$count_del_fro++){
								if($exp_deleted_fro_id[$count_del_fro]!=""){
									for($count_media=0;$count_media<=count($exp_all_media_id);$count_media++){
										if($exp_all_media_id[$count_media]!=""){
											if($exp_all_media_id[$count_media] == $exp_deleted_fro_id[$count_del_fro]){
												$exp_deleted_fro_id[$count_del_fro] ="";
											}
										}
									}
								}
							}
							$new_del_fro_id ="";
							for($count_new_del_fro=0;$count_new_del_fro<=count($exp_deleted_fro_id);$count_new_del_fro++){
								if($exp_deleted_cre_id[$count_new_del_fro]!=""){
									$new_del_fro_id = $new_del_fro_id .",".$exp_deleted_cre_id[$count_new_del_fro];
								}
							}
							mysql_query("update community_event set tagged_songs ='".$new_song_id."', tagged_videos ='".$new_video_id."', tagged_galleries ='".$new_gal_id."', deleted_media_by_creator_creator ='".$new_del_cre_id."', deleted_media_by_from_from='".$new_del_fro_id."' where id='".$res_comeve_det['id']."'");
							*/
							
							mysql_query("update community_event set tagged_songs ='".$new_song_id."', tagged_videos ='".$new_video_id."', tagged_galleries ='".$new_gal_id."'  where id='".$res_comeve_det['id']."'");
						}
					}
				}
				/*Delete the project*/
				$update_artist_det = mysql_query("select * from general_artist where artist_id='".$res_gen_det['artist_id']."'");
				if(mysql_num_rows($update_artist_det)>0)
				{
					$res_update_artist_det = mysql_fetch_assoc($update_artist_det);
					$all_del_id = $res_update_artist_det['deleted_project_id'] .",". $pro_id;
					mysql_query("update general_artist set deleted_project_id='".$all_del_id."' where artist_id='".$res_gen_det['artist_id']."'");
				}
				$update_community_det = mysql_query("select * from general_community where community_id='".$res_gen_det['community_id']."'");
				if(mysql_num_rows($update_community_det)>0)
				{
					$res_update_community_det = mysql_fetch_assoc($update_community_det);
					$all_del_id = $res_update_community_det['deleted_project_id'] .",". $pro_id;
					mysql_query("update general_community set deleted_project_id='".$all_del_id."' where community_id='".$res_gen_det['community_id']."'");
				}
			}
		}
		/*Delete the project*/
		//mysql_query("update artist_project set creator='', creators_info='' where id='".$pro_id."'");
	}
	
	function delete_where_user_tagin_project($creator_info,$pro_id)
	{
		/*get deleting project media*/
		$get_media = mysql_query("select * from general_media where creator_info='".$creator_info."' OR from_info='".$creator_info."'");
		if(mysql_num_rows($get_media)>0)
		{
			while($res_media = mysql_fetch_assoc($get_media))
			{
				$all_media_id = $all_media_id .",". $res_media['id'];
			}
		}
		$exp_all_media_id = explode(",",$all_media_id);
		/*get login user detail*/
		$get_gen_det = mysql_query("select * from general_user where email='".$_SESSION['login_email']."'");
		{
			if(mysql_num_rows($get_gen_det)>0)
			{
				$res_gen_det = mysql_fetch_assoc($get_gen_det);
				$exp_deleted_id = explode(",",$res_gen_det['deleted_media_id']);
				for($count_deleted=0;$count_deleted<=count($exp_deleted_id);$count_deleted++)
				{
					if($exp_deleted_id[$count_deleted]==""){
					continue;
					}else{
						for($count_media=0;$count_media<=count($exp_all_media_id);$count_media++)
						{
							if($exp_all_media_id[$count_media]==""){ continue; }else{
								if($exp_all_media_id[$count_media]==$exp_deleted_id[$count_deleted]){
									$exp_deleted_id[$count_deleted] ="";
								}
							}
						}
					}
				}
				$new_deleted_id="";
				for($count_new=0;$count_new<=count($exp_deleted_id);$count_new++)
				{
					if($exp_deleted_id[$count_new]!="")
					{
						$new_deleted_id = $new_deleted_id .",". $exp_deleted_id[$count_new];
					}
				}
				$exp_deleting_pro_id = explode(",",$res_gen_det['taggedartistprojects']);
				for($count_id=0;$count_id<=count($exp_deleting_pro_id);$count_id++)
				{
					if($exp_deleting_pro_id[$count_id]!="")
					{
						if($exp_deleting_pro_id[$count_id] !=$pro_id)
						{
							$all_new_id = $all_new_id .",". $exp_deleting_pro_id[$count_id];
						}
					}
				}
				mysql_query("update general_user set deleted_media_id='".$new_deleted_id."', taggedartistprojects ='".$all_new_id."' where email='".$_SESSION['login_email']."'");
				if($res_gen_det['artist_id']>0)
				{
					$get_art_det = mysql_query("select * from general_artist where artist_id='".$res_gen_det['artist_id']."'");
					if(mysql_num_rows($get_art_det)>0)
					{
						$res_art_det = mysql_fetch_assoc($get_art_det);
						$exp_song_id = explode(",",$res_art_det['taggedsongs']);
						$exp_video_id = explode(",",$res_art_det['taggedvideos']);
						$exp_gallery_id = explode(",",$res_art_det['taggedgalleries']);
						//$exp_deleted_cre_id = explode(",",$res_art_det['deleted_media_by_creator_creator']);
						//$exp_deleted_fro_id = explode(",",$res_art_det['deleted_media_by_from_from']);
						for($count_song=0;$count_song<=count($exp_song_id);$count_song++){
							if($exp_song_id[$count_song]!=""){
								for($count_media=0;$count_media<=count($exp_all_media_id);$count_media++){
									if($exp_all_media_id[$count_media]!=""){
										if($exp_all_media_id[$count_media] == $exp_song_id[$count_song]){
											$exp_song_id[$count_song] ="";
										}
									}
								}
							}
						}
						$new_song_id ="";
						for($count_new_song=0;$count_new_song<=count($exp_song_id);$count_new_song++){
							if($exp_song_id[$count_new_song]!=""){
								$new_song_id = $new_song_id .",".$exp_song_id[$count_new_song];
							}
						}
						for($count_video=0;$count_video<=count($exp_video_id);$count_video++){
							if($exp_video_id[$count_video]!=""){
								for($count_media=0;$count_media<=count($exp_all_media_id);$count_media++){
									if($exp_all_media_id[$count_media]!=""){
										if($exp_all_media_id[$count_media] == $exp_video_id[$count_video]){
											$exp_video_id[$count_video] ="";
										}
									}
								}
							}
						}
						$new_video_id ="";
						for($count_new_video=0;$count_new_video<=count($exp_video_id);$count_new_video++){
							if($exp_video_id[$count_new_video]!=""){
								$new_video_id = $new_video_id .",".$exp_video_id[$count_new_video];
							}
						}
						for($count_gal=0;$count_gal<=count($exp_gallery_id);$count_gal++){
							if($exp_gallery_id[$count_gal]!=""){
								for($count_media=0;$count_media<=count($exp_all_media_id);$count_media++){
									if($exp_all_media_id[$count_media]!=""){
										if($exp_all_media_id[$count_media] == $exp_gallery_id[$count_gal]){
											$exp_gallery_id[$count_gal] ="";
										}
									}
								}
							}
						}
						$new_gal_id ="";
						for($count_new_gal=0;$count_new_gal<=count($exp_gallery_id);$count_new_gal++){
							if($exp_gallery_id[$count_new_gal]!=""){
								$new_gal_id = $new_gal_id .",".$exp_gallery_id[$count_new_gal];
							}
						}
						
						/*
						for($count_del_cre=0;$count_del_cre<=count($exp_deleted_cre_id);$count_del_cre++){
							if($exp_deleted_cre_id[$count_del_cre]!=""){
								for($count_media=0;$count_media<=count($exp_all_media_id);$count_media++){
									if($exp_all_media_id[$count_media]!=""){
										if($exp_all_media_id[$count_media] == $exp_deleted_cre_id[$count_del_cre]){
											$exp_deleted_cre_id[$count_del_cre] ="";
										}
									}
								}
							}
						}
						$new_del_cre_id ="";
						for($count_new_del_cre=0;$count_new_del_cre<=count($exp_deleted_cre_id);$count_new_del_cre++){
							if($exp_deleted_cre_id[$count_new_del_cre]!=""){
								$new_del_cre_id = $new_del_cre_id .",".$exp_deleted_cre_id[$count_new_del_cre];
							}
						}
						for($count_del_fro=0;$count_del_fro<=count($exp_deleted_fro_id);$count_del_fro++){
							if($exp_deleted_fro_id[$count_del_fro]!=""){
								for($count_media=0;$count_media<=count($exp_all_media_id);$count_media++){
									if($exp_all_media_id[$count_media]!=""){
										if($exp_all_media_id[$count_media] == $exp_deleted_fro_id[$count_del_fro]){
											$exp_deleted_fro_id[$count_del_fro] ="";
										}
									}
								}
							}
						}
						$new_del_fro_id ="";
						for($count_new_del_fro=0;$count_new_del_fro<=count($exp_deleted_fro_id);$count_new_del_fro++){
							if($exp_deleted_cre_id[$count_new_del_fro]!=""){
								$new_del_fro_id = $new_del_fro_id .",".$exp_deleted_cre_id[$count_new_del_fro];
							}
						}
						mysql_query("update general_artist set taggedsongs ='".$new_song_id."', taggedvideos ='".$new_video_id."', taggedgalleries ='".$new_gal_id."', deleted_media_by_creator_creator ='".$new_del_cre_id."', deleted_media_by_from_from='".$new_del_fro_id."' where artist_id='".$res_art_det['artist_id']."'");
						*/
						mysql_query("update general_artist set taggedsongs ='".$new_song_id."', taggedvideos ='".$new_video_id."', taggedgalleries ='".$new_gal_id."'  where artist_id='".$res_art_det['artist_id']."'");
					}
					$get_artpro_det = mysql_query("select * from artist_project where artist_id='".$res_gen_det['artist_id']."'");
					if(mysql_num_rows($get_artpro_det)>0)
					{
						while($res_artpro_det = mysql_fetch_assoc($get_artpro_det))
						{
							$exp_song_id = explode(",",$res_artpro_det['tagged_songs']);
							$exp_video_id = explode(",",$res_artpro_det['tagged_videos']);
							$exp_gallery_id = explode(",",$res_artpro_det['tagged_galleries']);
							//$exp_deleted_cre_id = explode(",",$res_artpro_det['deleted_media_by_creator_creator']);
							//$exp_deleted_fro_id = explode(",",$res_artpro_det['deleted_media_by_from_from']);
							for($count_song=0;$count_song<=count($exp_song_id);$count_song++){
								if($exp_song_id[$count_song]!=""){
									for($count_media=0;$count_media<=count($exp_all_media_id);$count_media++){
										if($exp_all_media_id[$count_media]!=""){
											if($exp_all_media_id[$count_media] == $exp_song_id[$count_song]){
												$exp_song_id[$count_song] ="";
											}
										}
									}
								}
							}
							$new_song_id ="";
							for($count_new_song=0;$count_new_song<=count($exp_song_id);$count_new_song++){
								if($exp_song_id[$count_new_song]!=""){
									$new_song_id = $new_song_id .",".$exp_song_id[$count_new_song];
								}
							}
							for($count_video=0;$count_video<=count($exp_video_id);$count_video++){
								if($exp_video_id[$count_video]!=""){
									for($count_media=0;$count_media<=count($exp_all_media_id);$count_media++){
										if($exp_all_media_id[$count_media]!=""){
											if($exp_all_media_id[$count_media] == $exp_video_id[$count_video]){
												$exp_video_id[$count_video] ="";
											}
										}
									}
								}
							}
							$new_video_id ="";
							for($count_new_video=0;$count_new_video<=count($exp_video_id);$count_new_video++){
								if($exp_video_id[$count_new_video]!=""){
									$new_video_id = $new_video_id .",".$exp_video_id[$count_new_video];
								}
							}
							for($count_gal=0;$count_gal<=count($exp_gallery_id);$count_gal++){
								if($exp_gallery_id[$count_gal]!=""){
									for($count_media=0;$count_media<=count($exp_all_media_id);$count_media++){
										if($exp_all_media_id[$count_media]!=""){
											if($exp_all_media_id[$count_media] == $exp_gallery_id[$count_gal]){
												$exp_gallery_id[$count_gal] ="";
											}
										}
									}
								}
							}
							$new_gal_id ="";
							for($count_new_gal=0;$count_new_gal<=count($exp_gallery_id);$count_new_gal++){
								if($exp_gallery_id[$count_new_gal]!=""){
									$new_gal_id = $new_gal_id .",".$exp_gallery_id[$count_new_gal];
								}
							}
							
							/*
							for($count_del_cre=0;$count_del_cre<=count($exp_deleted_cre_id);$count_del_cre++){
								if($exp_deleted_cre_id[$count_del_cre]!=""){
									for($count_media=0;$count_media<=count($exp_all_media_id);$count_media++){
										if($exp_all_media_id[$count_media]!=""){
											if($exp_all_media_id[$count_media] == $exp_deleted_cre_id[$count_del_cre]){
												$exp_deleted_cre_id[$count_del_cre] ="";
											}
										}
									}
								}
							}
							$new_del_cre_id ="";
							for($count_new_del_cre=0;$count_new_del_cre<=count($exp_deleted_cre_id);$count_new_del_cre++){
								if($exp_deleted_cre_id[$count_new_del_cre]!=""){
									$new_del_cre_id = $new_del_cre_id .",".$exp_deleted_cre_id[$count_new_del_cre];
								}
							}
							for($count_del_fro=0;$count_del_fro<=count($exp_deleted_fro_id);$count_del_fro++){
								if($exp_deleted_fro_id[$count_del_fro]!=""){
									for($count_media=0;$count_media<=count($exp_all_media_id);$count_media++){
										if($exp_all_media_id[$count_media]!=""){
											if($exp_all_media_id[$count_media] == $exp_deleted_fro_id[$count_del_fro]){
												$exp_deleted_fro_id[$count_del_fro] ="";
											}
										}
									}
								}
							}
							$new_del_fro_id ="";
							for($count_new_del_fro=0;$count_new_del_fro<=count($exp_deleted_fro_id);$count_new_del_fro++){
								if($exp_deleted_cre_id[$count_new_del_fro]!=""){
									$new_del_fro_id = $new_del_fro_id .",".$exp_deleted_cre_id[$count_new_del_fro];
								}
							}
							mysql_query("update artist_project set tagged_songs ='".$new_song_id."', tagged_videos ='".$new_video_id."', tagged_galleries ='".$new_gal_id."', deleted_media_by_creator_creator ='".$new_del_cre_id."', deleted_media_by_from_from='".$new_del_fro_id."' where id='".$res_artpro_det['id']."'");
							*/
							mysql_query("update artist_project set tagged_songs ='".$new_song_id."', tagged_videos ='".$new_video_id."', tagged_galleries ='".$new_gal_id."' where id='".$res_artpro_det['id']."'");
						}
					}
					$get_arteve_det = mysql_query("select * from artist_event where artist_id='".$res_gen_det['artist_id']."'");
					if(mysql_num_rows($get_arteve_det)>0)
					{
						while($res_arteve_det = mysql_fetch_assoc($get_arteve_det))
						{
							$exp_song_id = explode(",",$res_arteve_det['tagged_songs']);
							$exp_video_id = explode(",",$res_arteve_det['tagged_videos']);
							$exp_gallery_id = explode(",",$res_arteve_det['tagged_galleries']);
							//$exp_deleted_cre_id = explode(",",$res_arteve_det['deleted_media_by_creator_creator']);
							//$exp_deleted_fro_id = explode(",",$res_arteve_det['deleted_media_by_from_from']);
							for($count_song=0;$count_song<=count($exp_song_id);$count_song++){
								if($exp_song_id[$count_song]!=""){
									for($count_media=0;$count_media<=count($exp_all_media_id);$count_media++){
										if($exp_all_media_id[$count_media]!=""){
											if($exp_all_media_id[$count_media] == $exp_song_id[$count_song]){
												$exp_song_id[$count_song] ="";
											}
										}
									}
								}
							}
							$new_song_id ="";
							for($count_new_song=0;$count_new_song<=count($exp_song_id);$count_new_song++){
								if($exp_song_id[$count_new_song]!=""){
									$new_song_id = $new_song_id .",".$exp_song_id[$count_new_song];
								}
							}
							for($count_video=0;$count_video<=count($exp_video_id);$count_video++){
								if($exp_video_id[$count_video]!=""){
									for($count_media=0;$count_media<=count($exp_all_media_id);$count_media++){
										if($exp_all_media_id[$count_media]!=""){
											if($exp_all_media_id[$count_media] == $exp_video_id[$count_video]){
												$exp_video_id[$count_video] ="";
											}
										}
									}
								}
							}
							$new_video_id ="";
							for($count_new_video=0;$count_new_video<=count($exp_video_id);$count_new_video++){
								if($exp_video_id[$count_new_video]!=""){
									$new_video_id = $new_video_id .",".$exp_video_id[$count_new_video];
								}
							}
							for($count_gal=0;$count_gal<=count($exp_gallery_id);$count_gal++){
								if($exp_gallery_id[$count_gal]!=""){
									for($count_media=0;$count_media<=count($exp_all_media_id);$count_media++){
										if($exp_all_media_id[$count_media]!=""){
											if($exp_all_media_id[$count_media] == $exp_gallery_id[$count_gal]){
												$exp_gallery_id[$count_gal] ="";
											}
										}
									}
								}
							}
							$new_gal_id ="";
							for($count_new_gal=0;$count_new_gal<=count($exp_gallery_id);$count_new_gal++){
								if($exp_gallery_id[$count_new_gal]!=""){
									$new_gal_id = $new_gal_id .",".$exp_gallery_id[$count_new_gal];
								}
							}
							
							/*
							for($count_del_cre=0;$count_del_cre<=count($exp_deleted_cre_id);$count_del_cre++){
								if($exp_deleted_cre_id[$count_del_cre]!=""){
									for($count_media=0;$count_media<=count($exp_all_media_id);$count_media++){
										if($exp_all_media_id[$count_media]!=""){
											if($exp_all_media_id[$count_media] == $exp_deleted_cre_id[$count_del_cre]){
												$exp_deleted_cre_id[$count_del_cre] ="";
											}
										}
									}
								}
							}
							$new_del_cre_id ="";
							for($count_new_del_cre=0;$count_new_del_cre<=count($exp_deleted_cre_id);$count_new_del_cre++){
								if($exp_deleted_cre_id[$count_new_del_cre]!=""){
									$new_del_cre_id = $new_del_cre_id .",".$exp_deleted_cre_id[$count_new_del_cre];
								}
							}
							for($count_del_fro=0;$count_del_fro<=count($exp_deleted_fro_id);$count_del_fro++){
								if($exp_deleted_fro_id[$count_del_fro]!=""){
									for($count_media=0;$count_media<=count($exp_all_media_id);$count_media++){
										if($exp_all_media_id[$count_media]!=""){
											if($exp_all_media_id[$count_media] == $exp_deleted_fro_id[$count_del_fro]){
												$exp_deleted_fro_id[$count_del_fro] ="";
											}
										}
									}
								}
							}
							$new_del_fro_id ="";
							for($count_new_del_fro=0;$count_new_del_fro<=count($exp_deleted_fro_id);$count_new_del_fro++){
								if($exp_deleted_cre_id[$count_new_del_fro]!=""){
									$new_del_fro_id = $new_del_fro_id .",".$exp_deleted_cre_id[$count_new_del_fro];
								}
							}
							mysql_query("update artist_event set tagged_songs ='".$new_song_id."', tagged_videos ='".$new_video_id."', tagged_galleries ='".$new_gal_id."', deleted_media_by_creator_creator ='".$new_del_cre_id."', deleted_media_by_from_from='".$new_del_fro_id."' where id='".$res_arteve_det['id']."'");
							*/
							mysql_query("update artist_event set tagged_songs ='".$new_song_id."', tagged_videos ='".$new_video_id."', tagged_galleries ='".$new_gal_id."' where id='".$res_arteve_det['id']."'");
						}
					}
				}
				
				if($res_gen_det['community_id']>0)
				{
					$get_com_det = mysql_query("select * from general_community where community_id='".$res_gen_det['community_id']."'");
					if(mysql_num_rows($get_com_det)>0)
					{
						$res_com_det = mysql_fetch_assoc($get_com_det);
						$exp_song_id = explode(",",$res_com_det['taggedsongs']);
						$exp_video_id = explode(",",$res_com_det['taggedvideos']);
						$exp_gallery_id = explode(",",$res_com_det['taggedgalleries']);
						//$exp_deleted_cre_id = explode(",",$res_com_det['deleted_media_by_creator_creator']);
						//$exp_deleted_fro_id = explode(",",$res_com_det['deleted_media_by_from_from']);
						for($count_song=0;$count_song<=count($exp_song_id);$count_song++){
							if($exp_song_id[$count_song]!=""){
								for($count_media=0;$count_media<=count($exp_all_media_id);$count_media++){
									if($exp_all_media_id[$count_media]!=""){
										if($exp_all_media_id[$count_media] == $exp_song_id[$count_song]){
											$exp_song_id[$count_song] ="";
										}
									}
								}
							}
						}
						$new_song_id ="";
						for($count_new_song=0;$count_new_song<=count($exp_song_id);$count_new_song++){
							if($exp_song_id[$count_new_song]!=""){
								$new_song_id = $new_song_id .",".$exp_song_id[$count_new_song];
							}
						}
						for($count_video=0;$count_video<=count($exp_video_id);$count_video++){
							if($exp_video_id[$count_video]!=""){
								for($count_media=0;$count_media<=count($exp_all_media_id);$count_media++){
									if($exp_all_media_id[$count_media]!=""){
										if($exp_all_media_id[$count_media] == $exp_video_id[$count_video]){
											$exp_video_id[$count_video] ="";
										}
									}
								}
							}
						}
						$new_video_id ="";
						for($count_new_video=0;$count_new_video<=count($exp_video_id);$count_new_video++){
							if($exp_video_id[$count_new_video]!=""){
								$new_video_id = $new_video_id .",".$exp_video_id[$count_new_video];
							}
						}
						for($count_gal=0;$count_gal<=count($exp_gallery_id);$count_gal++){
							if($exp_gallery_id[$count_gal]!=""){
								for($count_media=0;$count_media<=count($exp_all_media_id);$count_media++){
									if($exp_all_media_id[$count_media]!=""){
										if($exp_all_media_id[$count_media] == $exp_gallery_id[$count_gal]){
											$exp_gallery_id[$count_gal] ="";
										}
									}
								}
							}
						}
						$new_gal_id ="";
						for($count_new_gal=0;$count_new_gal<=count($exp_gallery_id);$count_new_gal++){
							if($exp_gallery_id[$count_new_gal]!=""){
								$new_gal_id = $new_gal_id .",".$exp_gallery_id[$count_new_gal];
							}
						}
						
						/*
						for($count_del_cre=0;$count_del_cre<=count($exp_deleted_cre_id);$count_del_cre++){
							if($exp_deleted_cre_id[$count_del_cre]!=""){
								for($count_media=0;$count_media<=count($exp_all_media_id);$count_media++){
									if($exp_all_media_id[$count_media]!=""){
										if($exp_all_media_id[$count_media] == $exp_deleted_cre_id[$count_del_cre]){
											$exp_deleted_cre_id[$count_del_cre] ="";
										}
									}
								}
							}
						}
						$new_del_cre_id ="";
						for($count_new_del_cre=0;$count_new_del_cre<=count($exp_deleted_cre_id);$count_new_del_cre++){
							if($exp_deleted_cre_id[$count_new_del_cre]!=""){
								$new_del_cre_id = $new_del_cre_id .",".$exp_deleted_cre_id[$count_new_del_cre];
							}
						}
						for($count_del_fro=0;$count_del_fro<=count($exp_deleted_fro_id);$count_del_fro++){
							if($exp_deleted_fro_id[$count_del_fro]!=""){
								for($count_media=0;$count_media<=count($exp_all_media_id);$count_media++){
									if($exp_all_media_id[$count_media]!=""){
										if($exp_all_media_id[$count_media] == $exp_deleted_fro_id[$count_del_fro]){
											$exp_deleted_fro_id[$count_del_fro] ="";
										}
									}
								}
							}
						}
						$new_del_fro_id ="";
						for($count_new_del_fro=0;$count_new_del_fro<=count($exp_deleted_fro_id);$count_new_del_fro++){
							if($exp_deleted_cre_id[$count_new_del_fro]!=""){
								$new_del_fro_id = $new_del_fro_id .",".$exp_deleted_cre_id[$count_new_del_fro];
							}
						}
						
						mysql_query("update general_community set taggedsongs ='".$new_song_id."', taggedvideos ='".$new_video_id."', taggedgalleries ='".$new_gal_id."', deleted_media_by_creator_creator ='".$new_del_cre_id."', deleted_media_by_from_from='".$new_del_fro_id."' where community_id='".$res_com_det['community_id']."'");
						*/
						mysql_query("update general_community set taggedsongs ='".$new_song_id."', taggedvideos ='".$new_video_id."', taggedgalleries ='".$new_gal_id."' where community_id='".$res_com_det['community_id']."'");
					}
					$get_compro_det = mysql_query("select * from community_project where community_id='".$res_gen_det['community_id']."'");
					if(mysql_num_rows($get_compro_det)>0)
					{
						while($res_compro_det = mysql_fetch_assoc($get_compro_det))
						{
							$exp_song_id = explode(",",$res_compro_det['tagged_songs']);
							$exp_video_id = explode(",",$res_compro_det['tagged_videos']);
							$exp_gallery_id = explode(",",$res_compro_det['tagged_galleries']);
							//$exp_deleted_cre_id = explode(",",$res_compro_det['deleted_media_by_creator_creator']);
							//$exp_deleted_fro_id = explode(",",$res_compro_det['deleted_media_by_from_from']);
							for($count_song=0;$count_song<=count($exp_song_id);$count_song++){
								if($exp_song_id[$count_song]!=""){
									for($count_media=0;$count_media<=count($exp_all_media_id);$count_media++){
										if($exp_all_media_id[$count_media]!=""){
											if($exp_all_media_id[$count_media] == $exp_song_id[$count_song]){
												$exp_song_id[$count_song] ="";
											}
										}
									}
								}
							}
							$new_song_id ="";
							for($count_new_song=0;$count_new_song<=count($exp_song_id);$count_new_song++){
								if($exp_song_id[$count_new_song]!=""){
									$new_song_id = $new_song_id .",".$exp_song_id[$count_new_song];
								}
							}
							for($count_video=0;$count_video<=count($exp_video_id);$count_video++){
								if($exp_video_id[$count_video]!=""){
									for($count_media=0;$count_media<=count($exp_all_media_id);$count_media++){
										if($exp_all_media_id[$count_media]!=""){
											if($exp_all_media_id[$count_media] == $exp_video_id[$count_video]){
												$exp_video_id[$count_video] ="";
											}
										}
									}
								}
							}
							$new_video_id ="";
							for($count_new_video=0;$count_new_video<=count($exp_video_id);$count_new_video++){
								if($exp_video_id[$count_new_video]!=""){
									$new_video_id = $new_video_id .",".$exp_video_id[$count_new_video];
								}
							}
							for($count_gal=0;$count_gal<=count($exp_gallery_id);$count_gal++){
								if($exp_gallery_id[$count_gal]!=""){
									for($count_media=0;$count_media<=count($exp_all_media_id);$count_media++){
										if($exp_all_media_id[$count_media]!=""){
											if($exp_all_media_id[$count_media] == $exp_gallery_id[$count_gal]){
												$exp_gallery_id[$count_gal] ="";
											}
										}
									}
								}
							}
							$new_gal_id ="";
							for($count_new_gal=0;$count_new_gal<=count($exp_gallery_id);$count_new_gal++){
								if($exp_gallery_id[$count_new_gal]!=""){
									$new_gal_id = $new_gal_id .",".$exp_gallery_id[$count_new_gal];
								}
							}
							/*
							for($count_del_cre=0;$count_del_cre<=count($exp_deleted_cre_id);$count_del_cre++){
								if($exp_deleted_cre_id[$count_del_cre]!=""){
									for($count_media=0;$count_media<=count($exp_all_media_id);$count_media++){
										if($exp_all_media_id[$count_media]!=""){
											if($exp_all_media_id[$count_media] == $exp_deleted_cre_id[$count_del_cre]){
												$exp_deleted_cre_id[$count_del_cre] ="";
											}
										}
									}
								}
							}
							$new_del_cre_id ="";
							for($count_new_del_cre=0;$count_new_del_cre<=count($exp_deleted_cre_id);$count_new_del_cre++){
								if($exp_deleted_cre_id[$count_new_del_cre]!=""){
									$new_del_cre_id = $new_del_cre_id .",".$exp_deleted_cre_id[$count_new_del_cre];
								}
							}
							for($count_del_fro=0;$count_del_fro<=count($exp_deleted_fro_id);$count_del_fro++){
								if($exp_deleted_fro_id[$count_del_fro]!=""){
									for($count_media=0;$count_media<=count($exp_all_media_id);$count_media++){
										if($exp_all_media_id[$count_media]!=""){
											if($exp_all_media_id[$count_media] == $exp_deleted_fro_id[$count_del_fro]){
												$exp_deleted_fro_id[$count_del_fro] ="";
											}
										}
									}
								}
							}
							$new_del_fro_id ="";
							for($count_new_del_fro=0;$count_new_del_fro<=count($exp_deleted_fro_id);$count_new_del_fro++){
								if($exp_deleted_cre_id[$count_new_del_fro]!=""){
									$new_del_fro_id = $new_del_fro_id .",".$exp_deleted_cre_id[$count_new_del_fro];
								}
							}
							mysql_query("update community_project set tagged_songs ='".$new_song_id."', tagged_videos ='".$new_video_id."', tagged_galleries ='".$new_gal_id."', deleted_media_by_creator_creator ='".$new_del_cre_id."', deleted_media_by_from_from='".$new_del_fro_id."' where id='".$res_compro_det['id']."'");
							*/
							mysql_query("update community_project set tagged_songs ='".$new_song_id."', tagged_videos ='".$new_video_id."', tagged_galleries ='".$new_gal_id."' where id='".$res_compro_det['id']."'");
						}
					}
					$get_comeve_det = mysql_query("select * from community_event where community_id='".$res_gen_det['community_id']."'");
					if(mysql_num_rows($get_comeve_det)>0)
					{
						while($res_comeve_det = mysql_fetch_assoc($get_comeve_det))
						{
							$exp_song_id = explode(",",$res_comeve_det['tagged_songs']);
							$exp_video_id = explode(",",$res_comeve_det['tagged_videos']);
							$exp_gallery_id = explode(",",$res_comeve_det['tagged_galleries']);
							//$exp_deleted_cre_id = explode(",",$res_comeve_det['deleted_media_by_creator_creator']);
							//$exp_deleted_fro_id = explode(",",$res_comeve_det['deleted_media_by_from_from']);
							for($count_song=0;$count_song<=count($exp_song_id);$count_song++){
								if($exp_song_id[$count_song]!=""){
									for($count_media=0;$count_media<=count($exp_all_media_id);$count_media++){
										if($exp_all_media_id[$count_media]!=""){
											if($exp_all_media_id[$count_media] == $exp_song_id[$count_song]){
												$exp_song_id[$count_song] ="";
											}
										}
									}
								}
							}
							$new_song_id ="";
							for($count_new_song=0;$count_new_song<=count($exp_song_id);$count_new_song++){
								if($exp_song_id[$count_new_song]!=""){
									$new_song_id = $new_song_id .",".$exp_song_id[$count_new_song];
								}
							}
							for($count_video=0;$count_video<=count($exp_video_id);$count_video++){
								if($exp_video_id[$count_video]!=""){
									for($count_media=0;$count_media<=count($exp_all_media_id);$count_media++){
										if($exp_all_media_id[$count_media]!=""){
											if($exp_all_media_id[$count_media] == $exp_video_id[$count_video]){
												$exp_video_id[$count_video] ="";
											}
										}
									}
								}
							}
							$new_video_id ="";
							for($count_new_video=0;$count_new_video<=count($exp_video_id);$count_new_video++){
								if($exp_video_id[$count_new_video]!=""){
									$new_video_id = $new_video_id .",".$exp_video_id[$count_new_video];
								}
							}
							for($count_gal=0;$count_gal<=count($exp_gallery_id);$count_gal++){
								if($exp_gallery_id[$count_gal]!=""){
									for($count_media=0;$count_media<=count($exp_all_media_id);$count_media++){
										if($exp_all_media_id[$count_media]!=""){
											if($exp_all_media_id[$count_media] == $exp_gallery_id[$count_gal]){
												$exp_gallery_id[$count_gal] ="";
											}
										}
									}
								}
							}
							$new_gal_id ="";
							for($count_new_gal=0;$count_new_gal<=count($exp_gallery_id);$count_new_gal++){
								if($exp_gallery_id[$count_new_gal]!=""){
									$new_gal_id = $new_gal_id .",".$exp_gallery_id[$count_new_gal];
								}
							}
							/*
							for($count_del_cre=0;$count_del_cre<=count($exp_deleted_cre_id);$count_del_cre++){
								if($exp_deleted_cre_id[$count_del_cre]!=""){
									for($count_media=0;$count_media<=count($exp_all_media_id);$count_media++){
										if($exp_all_media_id[$count_media]!=""){
											if($exp_all_media_id[$count_media] == $exp_deleted_cre_id[$count_del_cre]){
												$exp_deleted_cre_id[$count_del_cre] ="";
											}
										}
									}
								}
							}
							$new_del_cre_id ="";
							for($count_new_del_cre=0;$count_new_del_cre<=count($exp_deleted_cre_id);$count_new_del_cre++){
								if($exp_deleted_cre_id[$count_new_del_cre]!=""){
									$new_del_cre_id = $new_del_cre_id .",".$exp_deleted_cre_id[$count_new_del_cre];
								}
							}
							for($count_del_fro=0;$count_del_fro<=count($exp_deleted_fro_id);$count_del_fro++){
								if($exp_deleted_fro_id[$count_del_fro]!=""){
									for($count_media=0;$count_media<=count($exp_all_media_id);$count_media++){
										if($exp_all_media_id[$count_media]!=""){
											if($exp_all_media_id[$count_media] == $exp_deleted_fro_id[$count_del_fro]){
												$exp_deleted_fro_id[$count_del_fro] ="";
											}
										}
									}
								}
							}
							$new_del_fro_id ="";
							for($count_new_del_fro=0;$count_new_del_fro<=count($exp_deleted_fro_id);$count_new_del_fro++){
								if($exp_deleted_cre_id[$count_new_del_fro]!=""){
									$new_del_fro_id = $new_del_fro_id .",".$exp_deleted_cre_id[$count_new_del_fro];
								}
							}
							mysql_query("update community_event set tagged_songs ='".$new_song_id."', tagged_videos ='".$new_video_id."', tagged_galleries ='".$new_gal_id."', deleted_media_by_creator_creator ='".$new_del_cre_id."', deleted_media_by_from_from='".$new_del_fro_id."' where id='".$res_comeve_det['id']."'");
							*/
							mysql_query("update community_event set tagged_songs ='".$new_song_id."', tagged_videos ='".$new_video_id."', tagged_galleries ='".$new_gal_id."' where id='".$res_comeve_det['id']."'");
						}
					}
				}
				/*Delete the project*/
				$update_artist_det = mysql_query("select * from general_artist where artist_id='".$res_gen_det['artist_id']."'");
				if(mysql_num_rows($update_artist_det)>0)
				{
					$res_update_artist_det = mysql_fetch_assoc($update_artist_det);
					$all_del_id = $res_update_artist_det['deleted_project_id'] .",". $pro_id;
					mysql_query("update general_artist set deleted_project_id='".$all_del_id."' where artist_id='".$res_gen_det['artist_id']."'");
				}
				$update_community_det = mysql_query("select * from general_community where community_id='".$res_gen_det['community_id']."'");
				if(mysql_num_rows($update_community_det)>0)
				{
					$res_update_community_det = mysql_fetch_assoc($update_community_det);
					$all_del_id = $res_update_community_det['deleted_project_id'] .",". $pro_id;
					mysql_query("update general_community set deleted_project_id='".$all_del_id."' where community_id='".$res_gen_det['community_id']."'");
				}
			}
		}
		/*Delete the project
		
		$get_del_pro_det = mysql_query("select * from artist_project where id='".$pro_id."'");
		if(mysql_num_rows($get_del_pro_det)>0)
		{
			$new_email = "";
			$new_user = "";
			$res_del_pro_det = mysql_fetch_assoc($get_del_pro_det);
			$exp_tag_users = explode(",",$res_del_pro_det['users_tagged']);
			$exp_tag_user_email = explode(",",$res_del_pro_det['tagged_user_email']);
			for($count_email=0;$count_email<=count($exp_tag_user_email);$count_email++)
			{
				if($exp_tag_user_email[$count_email]!="")
				{
					if($exp_tag_user_email[$count_email] != $_SESSION['login_email']){
						$new_email = $new_email .",". $exp_tag_user_email[$count_email];
						$new_user = $new_user .",". $exp_tag_users[$count_email];
					}
				}
			}
			mysql_query("update artist_project set users_tagged ='".$new_user."', tagged_user_email ='".$new_email."' where id='".$pro_id."'");
		}*/
		
		
	}
	
	function only_creator2pr_this()
	{
		$id = $this->selgeneral();
		$var_type_1 = 'general_artist';
		$var_type_2 = 'general_community';
		$var_type_3 = 'artist_project';
		$var_type_4 = 'community_project';
		
		$art_p_ao = array();
		$art_c_ao = array();
		
		$com_p_ao = array();
		$com_c_ao = array();
		
		$com_a_ao = array();
		$art_a_ao = array();
		
		$art_ap_ao = array();
		$com_cp_ao = array();
		
		$pal_1 = array();
		$pal_2 = array();
		
		if($id['artist_id']!=0 && !empty($id['artist_id']))
		{
			$chk_type = $var_type_1.'|'.$id['artist_id'];
			
			$sql_get_allp = mysql_query("SELECT * FROM artist_project WHERE artist_id='".$id['artist_id']."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_get_allp)>0)
			{
				while($rowal = mysql_fetch_assoc($sql_get_allp))
				{
					//$rowal['whos_project'] = "only_creator";
					$pal_1[] = $rowal;
				}
			}
			
			if(!empty($pal_1) && count($pal_1))
			{
				for($pl=0;$pl<count($pal_1);$pl++)
				{
					$chk_typep_1 = 'artist_project|'.$pal_1[$pl]['id'];
					
					$sql = mysql_query("SELECT * FROM artist_project WHERE (creators_info='".$chk_typep_1."' OR creators_info='".$chk_type."') AND del_status=0 AND active=0");
					if(mysql_num_rows($sql)>0)
					{
						while($row_po = mysql_fetch_assoc($sql))
						{
							//if($row_po['artist_id']!=$id['artist_id'])
							//{
								
								$sql_art2 = mysql_query("SELECT * FROM artist_project WHERE creators_info='artist_project|".$row_po['id']."' AND del_status=0 AND active=0");
								if(mysql_num_rows($sql_art2)>0)
								{
									while($row2_po = mysql_fetch_assoc($sql_art2))
									{
										$row2_po['id'] = $row2_po['id'].'~art';
										//$row_po['whos_project'] = "only_creator";
										$art_p_ao[] = $row2_po;
									}
								}
								
								
								$sql_art3 = mysql_query("SELECT * FROM community_project WHERE creators_info='artist_project|".$row_po['id']."' AND del_status=0 AND active=0");
								if(mysql_num_rows($sql_art3)>0)
								{
									while($row3_po = mysql_fetch_assoc($sql_art3))
									{
										$row3_po['id'] = $row3_po['id'].'~com';
										//$row_po['whos_project'] = "only_creator";
										$art_c_ao[] = $row3_po;
									}
								}
							//}
						}
					}
					
					
					$sql = mysql_query("SELECT * FROM community_project WHERE (creators_info='".$chk_typep_1."'  OR creators_info='".$chk_type."') AND del_status=0 AND active=0");
					if(mysql_num_rows($sql)>0)
					{
						while($row_poo = mysql_fetch_assoc($sql))
						{
							//if($row_poo['community_id']!=$id['community_id'])
							//{		
								
								$sql_art2 = mysql_query("SELECT * FROM artist_project WHERE creators_info='community_project|".$row_poo['id']."' AND del_status=0 AND active=0");
								if(mysql_num_rows($sql_art2)>0)
								{
									while($row2_po = mysql_fetch_assoc($sql_art2))
									{
										$row2_po['id'] = $row2_po['id'].'~art';
										//$row_poo['whos_project'] = "only_creator";
										$com_p_ao[] = $row2_po;
									}
								}
								
								
								$sql_art3 = mysql_query("SELECT * FROM community_project WHERE creators_info='community_project|".$row_poo['id']."' AND del_status=0 AND active=0");
								if(mysql_num_rows($sql_art3)>0)
								{
									while($row3_po = mysql_fetch_assoc($sql_art3))
									{
										$row3_po['id'] = $row3_po['id'].'~com';
										//$row_poo['whos_project'] = "only_creator";
										$com_c_ao[] = $row3_po;
									}
								}
							//}
						}
					}
				}
			}
		}
		
		if($id['community_id']!=0 && !empty($id['community_id']))
		{
			$chk_type = $var_type_2.'|'.$id['community_id'];
			$sql_get_allp = mysql_query("SELECT * FROM community_project WHERE community_id='".$id['community_id']."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_get_allp)>0)
			{
				while($rowal = mysql_fetch_assoc($sql_get_allp))
				{
					//$rowal['whos_project'] = "only_creator";
					$pal_2[] = $rowal;
				}
			}
			
			if(!empty($pal_2) && count($pal_2))
			{
				for($pl2=0;$pl2<count($pal_2);$pl2++)
				{
					$chk_typep_2 = 'community_project|'.$pal_2[$pl2]['id'];
					
					
					$sql = mysql_query("SELECT * FROM artist_project WHERE (creators_info='".$chk_typep_2."' OR creators_info='".$chk_type."') AND del_status=0 AND active=0");
					if(mysql_num_rows($sql)>0)
					{
						while($row_po = mysql_fetch_assoc($sql))
						{
							//if($row_po['artist_id']!=$id['artist_id'])
							//{
								
								$sql_art2 = mysql_query("SELECT * FROM artist_project WHERE creators_info='artist_project|".$row_po['id']."' AND del_status=0 AND active=0");
								if(mysql_num_rows($sql_art2)>0)
								{
									while($row2_po = mysql_fetch_assoc($sql_art2))
									{
										$row2_po['id'] = $row2_po['id'].'~art';
										//$row_po['whos_project'] = "only_creator";
										$com_a_ao[] = $row2_po;
									}
								}
								
								
								$sql_art3 = mysql_query("SELECT * FROM community_project WHERE creators_info='artist_project|".$row_po['id']."' AND del_status=0 AND active=0");
								if(mysql_num_rows($sql_art3)>0)
								{
									while($row3_po = mysql_fetch_assoc($sql_art3))
									{
										$row3_po['id'] = $row3_po['id'].'~com';
										//$row_po['whos_project'] = "only_creator";
										$art_a_ao[] = $row3_po;
									}
								}
							//}
						}
					}
					
					
					$sql = mysql_query("SELECT * FROM community_project WHERE (creators_info='".$chk_typep_2."' OR creators_info='".$chk_type."') AND del_status=0 AND active=0");
					if(mysql_num_rows($sql)>0)
					{
						while($row_poo = mysql_fetch_assoc($sql))
						{
							//if($row_poo['community_id']!=$id['community_id'])
							//{
								
								$sql_art2 = mysql_query("SELECT * FROM artist_project WHERE creators_info='community_project|".$row_poo['id']."' AND del_status=0 AND active=0");
								if(mysql_num_rows($sql_art2)>0)
								{
									while($row2_po = mysql_fetch_assoc($sql_art2))
									{
										$row2_po['id'] = $row2_po['id'].'~art';
										//$row_poo['whos_project'] = "only_creator";
										$art_ap_ao[] = $row2_po;
									}
								}
								
								
								$sql_art3 = mysql_query("SELECT * FROM community_project WHERE creators_info='community_project|".$row_poo['id']."' AND del_status=0 AND active=0");
								if(mysql_num_rows($sql_art3)>0)
								{
									while($row3_po = mysql_fetch_assoc($sql_art3))
									{
										$row3_po['id'] = $row3_po['id'].'~com';
										//$row_poo['whos_project'] = "only_creator";
										$com_cp_ao[] = $row3_po;
									}
								}
							//}
						}
					}
				}
			}
		}		
		
		$totals = array();
		$totals = array_merge($art_p_ao,$art_c_ao,$com_p_ao,$com_c_ao,$com_a_ao,$art_a_ao,$art_ap_ao,$com_cp_ao);
		return $totals;
	}
	
	function only_creator2ev_upevent()
	{
		$id = $this->selgeneral();
		$date = date("Y-m-d");
		
		$var_type_1 = 'general_artist';
		$var_type_2 = 'general_community';
		$var_type_3 = 'artist_project';
		$var_type_4 = 'community_project';
		
		$art_p_ao = array();
		$art_c_ao = array();
		
		$com_p_ao = array();
		$com_c_ao = array();
		
		$com_a_ao = array();
		$art_a_ao = array();
		
		$art_ap_ao = array();
		$com_cp_ao = array();
		
		$pal_1 = array();
		$pal_2 = array();
		
		if($id['artist_id']!=0 && !empty($id['artist_id']))
		{
			$chk_type = $var_type_1.'|'.$id['artist_id'];
			
			$sql_get_allp = mysql_query("SELECT * FROM artist_project WHERE artist_id='".$id['artist_id']."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_get_allp)>0)
			{
				while($rowal = mysql_fetch_assoc($sql_get_allp))
				{
					//$rowal['whos_project'] = "only_creator";
					$pal_1[] = $rowal;
				}
			}
			
			if(!empty($pal_1) && count($pal_1))
			{
				for($pl=0;$pl<count($pal_1);$pl++)
				{
					$chk_typep_1 = 'artist_project|'.$pal_1[$pl]['id'];
					
					$sql = mysql_query("SELECT * FROM artist_project WHERE (creators_info='".$chk_typep_1."' OR creators_info='".$chk_type."') AND del_status=0 AND active=0");
					if(mysql_num_rows($sql)>0)
					{
						while($row_po = mysql_fetch_assoc($sql))
						{
							//if($row_po['artist_id']!=$id['artist_id'])
							//{
								
								$sql_art2 = mysql_query("SELECT * FROM artist_event WHERE creators_info='artist_project|".$row_po['id']."' AND del_status=0 AND active=0 AND date>='".$date."'");
								if(mysql_num_rows($sql_art2)>0)
								{
									while($row2_po = mysql_fetch_assoc($sql_art2))
									{
										$row2_po['id'] = $row2_po['id'].'~art';
										//$row_po['whos_project'] = "only_creator";
										$art_p_ao[] = $row2_po;
									}
								}
								
								
								$sql_art3 = mysql_query("SELECT * FROM community_event WHERE creators_info='artist_project|".$row_po['id']."' AND del_status=0 AND active=0 AND date>='".$date."'");
								if(mysql_num_rows($sql_art3)>0)
								{
									while($row3_po = mysql_fetch_assoc($sql_art3))
									{
										$row3_po['id'] = $row3_po['id'].'~com';
										//$row_po['whos_project'] = "only_creator";
										$art_c_ao[] = $row3_po;
									}
								}
							//}
						}
					}
					
					
					$sql = mysql_query("SELECT * FROM community_project WHERE (creators_info='".$chk_typep_1."' OR creators_info='".$chk_type."') AND del_status=0 AND active=0");
					if(mysql_num_rows($sql)>0)
					{
						while($row_poo = mysql_fetch_assoc($sql))
						{
							//if($row_poo['community_id']!=$id['community_id'])
							//{		
								
								$sql_art2 = mysql_query("SELECT * FROM artist_event WHERE creators_info='community_project|".$row_poo['id']."' AND del_status=0 AND active=0 AND date>='".$date."'");
								if(mysql_num_rows($sql_art2)>0)
								{
									while($row2_po = mysql_fetch_assoc($sql_art2))
									{
										$row2_po['id'] = $row2_po['id'].'~art';
										//$row_poo['whos_project'] = "only_creator";
										$com_p_ao[] = $row2_po;
									}
								}
								
								
								$sql_art3 = mysql_query("SELECT * FROM community_event WHERE creators_info='community_project|".$row_poo['id']."' AND del_status=0 AND active=0 AND date>='".$date."'");
								if(mysql_num_rows($sql_art3)>0)
								{
									while($row3_po = mysql_fetch_assoc($sql_art3))
									{
										$row3_po['id'] = $row3_po['id'].'~com';
										//$row_poo['whos_project'] = "only_creator";
										$com_c_ao[] = $row3_po;
									}
								}
							//}
						}
					}
				}
			}
		}
		
		if($id['community_id']!=0 && !empty($id['community_id']))
		{
			$chk_type = $var_type_2.'|'.$id['community_id'];
			
			$sql_get_allp = mysql_query("SELECT * FROM community_project WHERE community_id='".$id['community_id']."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_get_allp)>0)
			{
				while($rowal = mysql_fetch_assoc($sql_get_allp))
				{
					//$rowal['whos_project'] = "only_creator";
					$pal_2[] = $rowal;
				}
			}
			
			if(!empty($pal_2) && count($pal_2))
			{
				for($pl2=0;$pl2<count($pal_2);$pl2++)
				{
					$chk_typep_2 = 'community_project|'.$pal_2[$pl2]['id'];
					
					
					$sql = mysql_query("SELECT * FROM artist_project WHERE (creators_info='".$chk_typep_2."' OR creators_info='".$chk_type."') AND del_status=0 AND active=0");
					if(mysql_num_rows($sql)>0)
					{
						while($row_po = mysql_fetch_assoc($sql))
						{
							//if($row_po['artist_id']!=$id['artist_id'])
							//{
								
								$sql_art2 = mysql_query("SELECT * FROM artist_event WHERE creators_info='artist_project|".$row_po['id']."' AND del_status=0 AND active=0 AND date>='".$date."'");
								if(mysql_num_rows($sql_art2)>0)
								{
									while($row2_po = mysql_fetch_assoc($sql_art2))
									{
										$row2_po['id'] = $row2_po['id'].'~art';
										//$row_po['whos_project'] = "only_creator";
										$com_a_ao[] = $row2_po;
									}
								}
								
								
								$sql_art3 = mysql_query("SELECT * FROM community_event WHERE creators_info='artist_project|".$row_po['id']."' AND del_status=0 AND active=0 AND date>='".$date."'");
								if(mysql_num_rows($sql_art3)>0)
								{
									while($row3_po = mysql_fetch_assoc($sql_art3))
									{
										$row3_po['id'] = $row3_po['id'].'~com';
										//$row_po['whos_project'] = "only_creator";
										$art_a_ao[] = $row3_po;
									}
								}
							//}
						}
					}
					
					
					$sql = mysql_query("SELECT * FROM community_project WHERE (creators_info='".$chk_typep_2."' OR creators_info='".$chk_type."') AND del_status=0 AND active=0");
					if(mysql_num_rows($sql)>0)
					{
						while($row_poo = mysql_fetch_assoc($sql))
						{
							//if($row_poo['community_id']!=$id['community_id'])
							//{
								
								$sql_art2 = mysql_query("SELECT * FROM artist_event WHERE creators_info='community_project|".$row_poo['id']."' AND del_status=0 AND active=0 AND date>='".$date."'");
								if(mysql_num_rows($sql_art2)>0)
								{
									while($row2_po = mysql_fetch_assoc($sql_art2))
									{
										$row2_po['id'] = $row2_po['id'].'~art';
										//$row_poo['whos_project'] = "only_creator";
										$art_ap_ao[] = $row2_po;
									}
								}
								
								
								$sql_art3 = mysql_query("SELECT * FROM community_event WHERE creators_info='community_project|".$row_poo['id']."' AND del_status=0 AND active=0 AND date>='".$date."'");
								if(mysql_num_rows($sql_art3)>0)
								{
									while($row3_po = mysql_fetch_assoc($sql_art3))
									{
										$row3_po['id'] = $row3_po['id'].'~com';
										//$row_poo['whos_project'] = "only_creator";
										$com_cp_ao[] = $row3_po;
									}
								}
							//}
						}
					}
				}
			}
		}		
		
		$totals = array();
		$totals = array_merge($art_p_ao,$art_c_ao,$com_p_ao,$com_c_ao,$com_a_ao,$art_a_ao,$art_ap_ao,$com_cp_ao);
		return $totals;
	}
	
	function only_creator2_recevent()
	{
		$id = $this->selgeneral();
		$date = date("Y-m-d");
		
		$var_type_1 = 'general_artist';
		$var_type_2 = 'general_community';
		$var_type_3 = 'artist_project';
		$var_type_4 = 'community_project';
		
		$art_p_ao = array();
		$art_c_ao = array();
		
		$com_p_ao = array();
		$com_c_ao = array();
		
		$com_a_ao = array();
		$art_a_ao = array();
		
		$art_ap_ao = array();
		$com_cp_ao = array();
		
		$pal_1 = array();
		$pal_2 = array();
		
		if($id['artist_id']!=0 && !empty($id['artist_id']))
		{
			$chk_type = $var_type_1.'|'.$id['artist_id'];
			
			$sql_get_allp = mysql_query("SELECT * FROM artist_project WHERE artist_id='".$id['artist_id']."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_get_allp)>0)
			{
				while($rowal = mysql_fetch_assoc($sql_get_allp))
				{
					//$rowal['whos_project'] = "only_creator";
					$pal_1[] = $rowal;
				}
			}
			
			if(!empty($pal_1) && count($pal_1))
			{
				for($pl=0;$pl<count($pal_1);$pl++)
				{
					$chk_typep_1 = 'artist_project|'.$pal_1[$pl]['id'];
					
					$sql = mysql_query("SELECT * FROM artist_project WHERE (creators_info='".$chk_typep_1."' OR creators_info='".$chk_type."') AND del_status=0 AND active=0");
					if(mysql_num_rows($sql)>0)
					{
						while($row_po = mysql_fetch_assoc($sql))
						{
							//if($row_po['artist_id']!=$id['artist_id'])
							//{
								
								$sql_art2 = mysql_query("SELECT * FROM artist_event WHERE creators_info='artist_project|".$row_po['id']."' AND del_status=0 AND active=0 AND date<'".$date."'");
								if(mysql_num_rows($sql_art2)>0)
								{
									while($row2_po = mysql_fetch_assoc($sql_art2))
									{
										$row2_po['id'] = $row2_po['id'].'~art';
										//$row_po['whos_project'] = "only_creator";
										$art_p_ao[] = $row2_po;
									}
								}
								
								
								$sql_art3 = mysql_query("SELECT * FROM community_event WHERE creators_info='artist_project|".$row_po['id']."' AND del_status=0 AND active=0 AND date<'".$date."'");
								if(mysql_num_rows($sql_art3)>0)
								{
									while($row3_po = mysql_fetch_assoc($sql_art3))
									{
										$row3_po['id'] = $row3_po['id'].'~com';
										//$row_po['whos_project'] = "only_creator";
										$art_c_ao[] = $row3_po;
									}
								}
							//}
						}
					}
					
					
					$sql = mysql_query("SELECT * FROM community_project WHERE (creators_info='".$chk_typep_1."' OR creators_info='".$chk_type."') AND del_status=0 AND active=0");
					if(mysql_num_rows($sql)>0)
					{
						while($row_poo = mysql_fetch_assoc($sql))
						{
							//if($row_poo['community_id']!=$id['community_id'])
							//{		
								
								$sql_art2 = mysql_query("SELECT * FROM artist_event WHERE creators_info='community_project|".$row_poo['id']."' AND del_status=0 AND active=0 AND date<'".$date."'");
								if(mysql_num_rows($sql_art2)>0)
								{
									while($row2_po = mysql_fetch_assoc($sql_art2))
									{
										$row2_po['id'] = $row2_po['id'].'~art';
										//$row_poo['whos_project'] = "only_creator";
										$com_p_ao[] = $row2_po;
									}
								}
								
								
								$sql_art3 = mysql_query("SELECT * FROM community_event WHERE creators_info='community_project|".$row_poo['id']."' AND del_status=0 AND active=0 AND date<'".$date."'");
								if(mysql_num_rows($sql_art3)>0)
								{
									while($row3_po = mysql_fetch_assoc($sql_art3))
									{
										$row3_po['id'] = $row3_po['id'].'~com';
										//$row_poo['whos_project'] = "only_creator";
										$com_c_ao[] = $row3_po;
									}
								}
							//}
						}
					}
				}
			}
		}
		
		if($id['community_id']!=0 && !empty($id['community_id']))
		{
			$chk_type = $var_type_2.'|'.$id['community_id'];
			
			$sql_get_allp = mysql_query("SELECT * FROM community_project WHERE community_id='".$id['community_id']."' AND del_status=0 AND active=0");
			if(mysql_num_rows($sql_get_allp)>0)
			{
				while($rowal = mysql_fetch_assoc($sql_get_allp))
				{
					//$rowal['whos_project'] = "only_creator";
					$pal_2[] = $rowal;
				}
			}
			
			if(!empty($pal_2) && count($pal_2))
			{
				for($pl2=0;$pl2<count($pal_2);$pl2++)
				{
					$chk_typep_2 = 'community_project|'.$pal_2[$pl2]['id'];
					
					
					$sql = mysql_query("SELECT * FROM artist_project WHERE (creators_info='".$chk_typep_2."' OR creators_info='".$chk_type."') AND del_status=0 AND active=0");
					if(mysql_num_rows($sql)>0)
					{
						while($row_po = mysql_fetch_assoc($sql))
						{
							//if($row_po['artist_id']!=$id['artist_id'])
							//{
								
								$sql_art2 = mysql_query("SELECT * FROM artist_event WHERE creators_info='artist_project|".$row_po['id']."' AND del_status=0 AND active=0 AND date<'".$date."'");
								if(mysql_num_rows($sql_art2)>0)
								{
									while($row2_po = mysql_fetch_assoc($sql_art2))
									{
										$row2_po['id'] = $row2_po['id'].'~art';
										//$row_po['whos_project'] = "only_creator";
										$com_a_ao[] = $row2_po;
									}
								}
								
								
								$sql_art3 = mysql_query("SELECT * FROM community_event WHERE creators_info='artist_project|".$row_po['id']."' AND del_status=0 AND active=0 AND date<'".$date."'");
								if(mysql_num_rows($sql_art3)>0)
								{
									while($row3_po = mysql_fetch_assoc($sql_art3))
									{
										$row3_po['id'] = $row3_po['id'].'~com';
										//$row_po['whos_project'] = "only_creator";
										$art_a_ao[] = $row3_po;
									}
								}
							//}
						}
					}
					
					
					$sql = mysql_query("SELECT * FROM community_project WHERE (creators_info='".$chk_typep_2."' OR creators_info='".$chk_type."') AND del_status=0 AND active=0");
					if(mysql_num_rows($sql)>0)
					{
						while($row_poo = mysql_fetch_assoc($sql))
						{
							//if($row_poo['community_id']!=$id['community_id'])
							//{
								
								$sql_art2 = mysql_query("SELECT * FROM artist_event WHERE creators_info='community_project|".$row_poo['id']."' AND del_status=0 AND active=0 AND date<'".$date."'");
								if(mysql_num_rows($sql_art2)>0)
								{
									while($row2_po = mysql_fetch_assoc($sql_art2))
									{
										$row2_po['id'] = $row2_po['id'].'~art';
										//$row_poo['whos_project'] = "only_creator";
										$art_ap_ao[] = $row2_po;
									}
								}
								
								
								$sql_art3 = mysql_query("SELECT * FROM community_event WHERE creators_info='community_project|".$row_poo['id']."' AND del_status=0 AND active=0 AND date<'".$date."'");
								if(mysql_num_rows($sql_art3)>0)
								{
									while($row3_po = mysql_fetch_assoc($sql_art3))
									{
										$row3_po['id'] = $row3_po['id'].'~com';
										//$row_poo['whos_project'] = "only_creator";
										$com_cp_ao[] = $row3_po;
									}
								}
							//}
						}
					}
				}
			}
		}		
		
		$totals = array();
		$totals = array_merge($art_p_ao,$art_c_ao,$com_p_ao,$com_c_ao,$com_a_ao,$art_a_ao,$art_ap_ao,$com_cp_ao);
		return $totals;
	}
	
	function delete_creator_creator($pro_id)
	{
		$gen_det = $this ->selgeneral();
		$get_art_det = mysql_query("select * from general_artist where artist_id='".$gen_det['artist_id']."'");
		$get_com_det = mysql_query("select * from general_community where community_id='".$gen_det['community_id']."'");
		if(mysql_num_rows($get_art_det)>0)
		{
			$res_art_det = mysql_fetch_assoc($get_art_det);
			$art_del_id = $res_art_det['deleted_project_id'] .",".$pro_id;
			$tag_art = explode(",",$res_art_det['taggedprojects']);
			$all_art_pro_id ="";
			for($count_pro=0;$count_pro<=count($tag_art);$count_pro++)
			{
				if($tag_art[$count_pro]!="")
				{
					if($tag_art[$count_pro]!=$pro_id)
					{
						$all_art_pro_id = $all_art_pro_id .",".$tag_art[$count_pro];
					}
				}
			}
			mysql_query("update general_artist set deleted_project_id ='".$art_del_id."',taggedprojects='".$all_art_pro_id."' where artist_id='".$gen_det['artist_id']."'");
			$get_art_pro = mysql_query("select * from artist_project where artist_id ='".$gen_det['artist_id']."'");
			if(mysql_num_rows($get_art_pro)>0)
			{
				while($res_art_pro = mysql_fetch_assoc($get_art_pro))
				{
					$tag_art_pro = explode(",",$res_art_pro['tagged_projects']);
					$all_art_project_id ="";
					for($count_pro=0;$count_pro<=count($tag_art_pro);$count_pro++)
					{
						if($tag_art_pro[$count_pro]!="")
						{
							if($tag_art_pro[$count_pro]!=$pro_id)
							{
								$all_art_project_id = $all_art_project_id .",".$tag_art_pro[$count_pro];
							}
						}
					}
					mysql_query("update artist_project set tagged_projects='".$all_art_project_id."' where id='".$res_art_pro['id']."'");
				}
			}
			$get_art_event = mysql_query("select * from artist_event where artist_id ='".$gen_det['artist_id']."'");
			if(mysql_num_rows($get_art_event)>0)
			{
				while($res_art_event = mysql_fetch_assoc($get_art_event))
				{
					$tag_art_pro = explode(",",$res_art_event['tagged_projects']);
					$all_art_event_id ="";
					for($count_pro=0;$count_pro<=count($tag_art_pro);$count_pro++)
					{
						if($tag_art_pro[$count_pro]!="")
						{
							if($tag_art_pro[$count_pro]!=$pro_id)
							{
								$all_art_event_id = $all_art_event_id .",".$tag_art_pro[$count_pro];
							}
						}
					}
					mysql_query("update artist_event set tagged_projects='".$all_art_event_id."' where id='".$res_art_event['id']."'");
				}
			}
		}
		if(mysql_num_rows($get_com_det)>0)
		{
			$res_com_det = mysql_fetch_assoc($get_com_det);
			$com_del_id = $res_com_det['deleted_project_id'] .",".$pro_id;
			$tag_com = explode(",",$res_com_det['taggedprojects']);
			$all_com_pro_id ="";
			for($count_pro=0;$count_pro<=count($tag_com);$count_pro++)
			{
				if($tag_com[$count_pro]!="")
				{
					if($tag_com[$count_pro]!=$pro_id)
					{
						$all_com_pro_id = $all_com_pro_id .",".$tag_com[$count_pro];
					}
				}
			}
			echo "update general_community set deleted_project_id ='".$com_del_id."',taggedprojects='".$all_com_pro_id."' where community_id='".$gen_det['community_id']."'";
			mysql_query("update general_community set deleted_project_id ='".$com_del_id."',taggedprojects='".$all_com_pro_id."' where community_id='".$gen_det['community_id']."'");
			
			$get_com_pro = mysql_query("select * from community_project where community_id ='".$gen_det['community_id']."'");
			if(mysql_num_rows($get_com_pro)>0)
			{
				while($res_com_pro = mysql_fetch_assoc($get_com_pro))
				{
					$tag_com_pro = explode(",",$res_com_pro['tagged_projects']);
					$all_com_project_id ="";
					for($count_pro=0;$count_pro<=count($tag_com_pro);$count_pro++)
					{
						if($tag_com_pro[$count_pro]!="")
						{
							if($tag_com_pro[$count_pro]!=$pro_id)
							{
								$all_com_project_id = $all_com_project_id .",".$tag_com_pro[$count_pro];
							}
						}
					}
					mysql_query("update community_project set tagged_projects='".$all_com_project_id."' where id='".$res_com_pro['id']."'");
				}
			}
			
			$get_com_event = mysql_query("select * from community_event where community_id ='".$gen_det['community_id']."'");
			if(mysql_num_rows($get_com_event)>0)
			{
				while($res_com_event = mysql_fetch_assoc($get_com_event))
				{
					$tag_com_pro = explode(",",$res_com_event['tagged_projects']);
					$all_com_event_id ="";
					for($count_pro=0;$count_pro<=count($tag_com_pro);$count_pro++)
					{
						if($tag_com_pro[$count_pro]!="")
						{
							if($tag_com_pro[$count_pro]!=$pro_id)
							{
								$all_com_event_id = $all_com_event_id .",".$tag_com_pro[$count_pro];
							}
						}
					}
					mysql_query("update community_event set tagged_projects='".$all_com_event_id."' where id='".$res_com_event['id']."'");
				}
			}
		}
	}
	
	function remove_deleted_id($info,$creator_info)
	{
		$all_art_pro_id ="";
		$all_com_pro_id ="";
		$all_art_eve_id ="";
		$all_com_eve_id ="";
		$mown = explode("|",$creator_info);
		$all_id="";
		$get_art_creator = mysql_query("select * from artist_project where creators_info='".$creator_info."'");
		if(mysql_num_rows($get_art_creator)>0){
			while($res_art_creator = mysql_fetch_assoc($get_art_creator)){
				$all_art_pro_id = $all_art_pro_id .",". $res_art_creator['id']."~art";
			}
		}
		$get_com_creator = mysql_query("select * from community_project where creators_info='".$creator_info."'");
		if(mysql_num_rows($get_com_creator)>0){
			while($res_com_creator = mysql_fetch_assoc($get_com_creator)){
				$all_com_pro_id = $all_com_pro_id .",". $res_com_creator['id']."~com";
			}
		}
		$get_art_creator_eve = mysql_query("select * from artist_event where creators_info='".$creator_info."'");
		if(mysql_num_rows($get_art_creator_eve)>0){
			while($res_art_creator_eve = mysql_fetch_assoc($get_art_creator_eve)){
				$all_art_eve_id = $all_art_eve_id .",". $res_art_creator_eve['id']."~art";
			}
		}
		$get_com_creator_eve = mysql_query("select * from community_event where creators_info='".$creator_info."'");
		if(mysql_num_rows($get_com_creator_eve)>0){
			while($res_com_creator_eve = mysql_fetch_assoc($get_com_creator_eve)){
				$all_com_eve_id = $all_com_eve_id .",". $res_com_creator_eve['id']."~com";
			}
		}
		$all_id = $all_art_pro_id .$all_com_pro_id. $all_art_eve_id .$all_com_eve_id.",".$mown[1]."~art";
		$exp_all_id = explode(",",$all_id);
		
		$exp_info = explode("|",$info);
		if($exp_info[0] == "general_artist"){ 
			$get_det = mysql_query("select * from general_user where artist_id='".$exp_info[1]."'");
		}if($exp_info[0] == "general_community"){ 
			$get_det = mysql_query("select * from general_user where community_id='".$exp_info[1]."'");
		}if($exp_info[0] == "artist_project"){
			$get_pro = mysql_query("select * from ".$exp_info[0]." where id='".$exp_info[1]."'");
			$res_pro = mysql_fetch_assoc($get_pro);
			$get_det = mysql_query("select * from general_user where artist_id='".$res_pro['artist_id']."'");
		}if($exp_info[0] == "community_project"){
			$get_pro = mysql_query("select * from ".$exp_info[0]." where id='".$exp_info[1]."'");
			$res_pro = mysql_fetch_assoc($get_pro);
			$get_det = mysql_query("select * from general_user where community_id='".$res_pro['community_id']."'");
		}
		if(mysql_num_rows($get_det)>0)
		{
			$res_det = mysql_fetch_assoc($get_det);
			$get_art_det = mysql_query("select * from general_artist where artist_id='".$res_det['artist_id']."'");
			if(mysql_num_rows($get_art_det)>0)
			{
				$res_art_det = mysql_fetch_assoc($get_art_det);
				$exp_deleted_id = explode(",",$res_art_det['deleted_project_id']);
				$new_id ="";
				for($count_all=0;$count_all<=count($exp_all_id);$count_all++){
					if($exp_all_id[$count_all]!=""){
						for($count_del=0;$count_del<=count($exp_deleted_id);$count_del++){
							if($exp_deleted_id[$count_del]!=""){
								if($exp_all_id[$count_all] == $exp_deleted_id[$count_del]){
									$exp_deleted_id[$count_del]="";
								}
							}
						}
					}
				}
				$exp_deleted_eve_id = explode(",",$res_art_det['deleted_event_id']);
				$new_eve_id ="";
				for($count_all=0;$count_all<=count($exp_all_id);$count_all++){
					if($exp_all_id[$count_all]!=""){
						for($count_del=0;$count_del<=count($exp_deleted_eve_id);$count_del++){
							if($exp_deleted_eve_id[$count_del]!=""){
								if($exp_all_id[$count_all] == $exp_deleted_eve_id[$count_del]){
									$exp_deleted_eve_id[$count_del]="";
								}
							}
						}
					}
				}
				for($count_new_id=0;$count_new_id<=count($exp_deleted_id);$count_new_id++){
					if($exp_deleted_id[$count_new_id]!=""){
						$new_id = $new_id .",".$exp_deleted_id[$count_new_id];
					}
				}
				for($count_new_id=0;$count_new_id<=count($exp_deleted_eve_id);$count_new_id++){
					if($exp_deleted_eve_id[$count_new_id]!=""){
						$new_eve_id = $new_eve_id .",".$exp_deleted_eve_id[$count_new_id];
					}
				}
				mysql_query("update general_artist set deleted_project_id ='".$new_id."',deleted_event_id ='".$new_eve_id."' where artist_id = '".$res_art_det['artist_id']."'");
			}
			$get_com_det = mysql_query("select * from general_community where community_id='".$res_det['community_id']."'");
			if(mysql_num_rows($get_com_det)>0)
			{
				$res_com_det = mysql_fetch_assoc($get_com_det);
				$exp_deleted_id = explode(",",$res_com_det['deleted_project_id']);
				$new_id ="";
				for($count_all=0;$count_all<=count($exp_all_id);$count_all++){
					if($exp_all_id[$count_all]!=""){
						for($count_del=0;$count_del<=count($exp_deleted_id);$count_del++){
							if($exp_deleted_id[$count_del]!=""){
								if($exp_all_id[$count_all] == $exp_deleted_id[$count_del]){
									$exp_deleted_id[$count_del] ="";
								}
							}
						}
					}
				}
				$exp_deleted_eve_id = explode(",",$res_com_det['deleted_event_id']);
				$new_eve_id ="";
				for($count_all=0;$count_all<=count($exp_all_id);$count_all++){
					if($exp_all_id[$count_all]!=""){
						for($count_del=0;$count_del<=count($exp_deleted_eve_id);$count_del++){
							if($exp_deleted_eve_id[$count_del]!=""){
								if($exp_all_id[$count_all] == $exp_deleted_eve_id[$count_del]){
									$exp_deleted_eve_id[$count_del]="";
								}
							}
						}
					}
				}
				for($count_new_id=0;$count_new_id<=count($exp_deleted_id);$count_new_id++){
					if($exp_deleted_id[$count_new_id]!=""){
						$new_id = $new_id .",".$exp_deleted_id[$count_new_id];
					}
				}
				for($count_new_id=0;$count_new_id<=count($exp_deleted_eve_id);$count_new_id++){
					if($exp_deleted_eve_id[$count_new_id]!=""){
						$new_eve_id = $new_eve_id .",".$exp_deleted_eve_id[$count_new_id];
					}
				}
				mysql_query("update general_community set deleted_project_id ='".$new_id."',deleted_event_id ='".$new_eve_id."' where community_id = '".$res_com_det['community_id']."'");
			}
		}
	}
	
	
	function remove_deleted_id_tagged($email,$creator_info)
	{
	
		$all_art_pro_id ="";
		$all_com_pro_id ="";
		$all_art_eve_id ="";
		$all_com_eve_id ="";
		$mown = explode("|",$creator_info);
		$all_id="";
		$get_art_creator = mysql_query("select * from artist_project where creators_info='".$creator_info."'");
		if(mysql_num_rows($get_art_creator)>0){
			while($res_art_creator = mysql_fetch_assoc($get_art_creator)){
				$all_art_pro_id = $all_art_pro_id .",". $res_art_creator['id']."~art";
			}
		}
		$get_com_creator = mysql_query("select * from community_project where creators_info='".$creator_info."'");
		if(mysql_num_rows($get_com_creator)>0){
			while($res_com_creator = mysql_fetch_assoc($get_com_creator)){
				$all_com_pro_id = $all_com_pro_id .",". $res_com_creator['id']."~com";
			}
		}
		$get_art_creator_eve = mysql_query("select * from artist_event where creators_info='".$creator_info."'");
		if(mysql_num_rows($get_art_creator_eve)>0){
			while($res_art_creator_eve = mysql_fetch_assoc($get_art_creator_eve)){
				$all_art_eve_id = $all_art_eve_id .",". $res_art_creator_eve['id']."~art";
			}
		}
		$get_com_creator_eve = mysql_query("select * from community_event where creators_info='".$creator_info."'");
		if(mysql_num_rows($get_com_creator_eve)>0){
			while($res_com_creator_eve = mysql_fetch_assoc($get_com_creator_eve)){
				$all_com_eve_id = $all_com_eve_id .",". $res_com_creator_eve['id']."~com";
			}
		}
		$all_id = $all_art_pro_id .$all_com_pro_id . $all_com_eve_id . $all_art_eve_id .",".$mown[1]."~art";
		$exp_all_id = explode(",",$all_id);
		
		
		$get_gen_det = mysql_query("select * from general_user where email='".$email."'");
		if(mysql_num_rows($get_gen_det)>0)
		{
			$res_det = mysql_fetch_assoc($get_gen_det);
			$get_art_det = mysql_query("select * from general_artist where artist_id='".$res_det['artist_id']."'");
			if(mysql_num_rows($get_art_det)>0)
			{
				$res_art_det = mysql_fetch_assoc($get_art_det);
				$exp_deleted_id = explode(",",$res_art_det['deleted_project_id']);
				$new_id ="";
				for($count_all=0;$count_all<=count($exp_all_id);$count_all++){
					if($exp_all_id[$count_all]!=""){
						for($count_del=0;$count_del<=count($exp_deleted_id);$count_del++){
							if($exp_deleted_id[$count_del]!=""){
								if($exp_all_id[$count_all] == $exp_deleted_id[$count_del]){
									$exp_deleted_id[$count_del]="";
								}
							}
						}
					}
				}
				$exp_deleted_eve_id = explode(",",$res_art_det['deleted_event_id']);
				$new_eve_id ="";
				for($count_all=0;$count_all<=count($exp_all_id);$count_all++){
					if($exp_all_id[$count_all]!=""){
						for($count_del=0;$count_del<=count($exp_deleted_eve_id);$count_del++){
							if($exp_deleted_eve_id[$count_del]!=""){
								if($exp_all_id[$count_all] == $exp_deleted_eve_id[$count_del]){
									$exp_deleted_eve_id[$count_del]="";
								}
							}
						}
					}
				}
				for($count_new_id=0;$count_new_id<=count($exp_deleted_id);$count_new_id++){
					if($exp_deleted_id[$count_new_id]!=""){
						$new_id = $new_id .",".$exp_deleted_id[$count_new_id];
					}
				}
				for($count_new_id=0;$count_new_id<=count($exp_deleted_eve_id);$count_new_id++){
					if($exp_deleted_eve_id[$count_new_id]!=""){
						$new_eve_id = $new_eve_id .",".$exp_deleted_eve_id[$count_new_id];
					}
				}
				mysql_query("update general_artist set deleted_project_id ='".$new_id."',deleted_event_id ='".$new_eve_id."' where artist_id = '".$res_art_det['artist_id']."'");
			}
			$get_com_det = mysql_query("select * from general_community where community_id='".$res_det['community_id']."'");
			if(mysql_num_rows($get_com_det)>0)
			{
				$res_com_det = mysql_fetch_assoc($get_com_det);
				$exp_deleted_id = explode(",",$res_com_det['deleted_project_id']);
				$new_id ="";
				for($count_all=0;$count_all<=count($exp_all_id);$count_all++){
					if($exp_all_id[$count_all]!=""){
						for($count_del=0;$count_del<=count($exp_deleted_id);$count_del++){
							if($exp_deleted_id[$count_del]!=""){
								if($exp_all_id[$count_all] == $exp_deleted_id[$count_del]){
									$exp_deleted_id[$count_del] ="";
								}
							}
						}
					}
				}
				$exp_deleted_eve_id = explode(",",$res_com_det['deleted_event_id']);
				$new_eve_id ="";
				for($count_all=0;$count_all<=count($exp_all_id);$count_all++){
					if($exp_all_id[$count_all]!=""){
						for($count_del=0;$count_del<=count($exp_deleted_eve_id);$count_del++){
							if($exp_deleted_eve_id[$count_del]!=""){
								if($exp_all_id[$count_all] == $exp_deleted_eve_id[$count_del]){
									$exp_deleted_eve_id[$count_del] ="";
								}
							}
						}
					}
				}
				for($count_new_id=0;$count_new_id<=count($exp_deleted_id);$count_new_id++){
					if($exp_deleted_id[$count_new_id]!=""){
						$new_id = $new_id .",".$exp_deleted_id[$count_new_id];
					}
				}
				for($count_new_id=0;$count_new_id<=count($exp_deleted_eve_id);$count_new_id++){
					if($exp_deleted_eve_id[$count_new_id]!=""){
						$new_eve_id = $new_eve_id .",".$exp_deleted_eve_id[$count_new_id];
					}
				}
				mysql_query("update general_community set deleted_project_id ='".$new_id."',deleted_event_id ='".$new_eve_id."' where community_id = '".$res_com_det['community_id']."'");
			}
		}
	}
	function get_deleted_project_id()
	{
		$get_gen_det = $this->selgeneral();
		$res_art_id ="";
		$get_art_id = mysql_query("select * from general_artist where artist_id='".$get_gen_det['artist_id']."'");
		if(mysql_num_rows($get_art_id)>0)
		{
			$res_art_id = mysql_fetch_assoc($get_art_id);
		}
		return($res_art_id);
	}
	
	function create_artist_pro_member($frequency,$cost,$description,$mem_id,$mem_type)
	{
		$get_art_mem = mysql_query("select * from project_membership where member_id='".$mem_id."' AND member_type='".$mem_type."'");
		if(mysql_num_rows($get_art_mem)>0){
			$art_member=mysql_query("update project_membership set frequency='".$frequency."',cost='".$cost."',description='".$description."' where member_id='".$mem_id."' AND member_type='".$mem_type."'");
		}else{
			$art_member=mysql_query("insert into project_membership (member_id,member_type,frequency,cost,description)
			values('".$mem_id."','".$mem_type."','".$frequency."','".$cost."','".$description."')");
		}
	}
	function get_fan_detail($mem_id,$mem_type)
	{
		$sql= mysql_query("select * from project_membership where member_id ='".$mem_id."' AND member_type='".$mem_type."'");
		if(mysql_num_rows($sql)>0){
			$res = mysql_fetch_assoc($sql);
		}
		return($res);
	}
	function delete_project_member($mem_id,$mem_type)
	{
		$sql= mysql_query("delete from project_membership where member_id ='".$mem_id."' AND member_type='".$mem_type."'");
	}
	
	function update_meds($id,$title)
	{
		$sql = mysql_query("UPDATE general_media SET creator='".$title."' WHERE creator_info='artist_project|".$id."'");
		$sql = mysql_query("UPDATE general_media SET `from`='".$title."' WHERE from_info='artist_project|".$id."'");
		$sql = mysql_query("UPDATE artist_event SET creator='".$title."' WHERE creators_info='artist_project|".$id."'");
		$sql = mysql_query("UPDATE community_event SET creator='".$title."' WHERE creators_info='artist_project|".$id."'");
		$sql = mysql_query("UPDATE artist_project SET creator='".$title."' WHERE creators_info='artist_project|".$id."'");
		$sql = mysql_query("UPDATE community_project SET creator='".$title."' WHERE creators_info='artist_project|".$id."'");
	}
	
	function date_creations($id)
	{
		$date = date("Y-m-d h:i:s");
		$sql = mysql_query("SELECT * FROM date_creation WHERE profile_media_id='".$id."' AND profile_media_type='artist_project'");
		if(mysql_num_rows($sql)<=0)
		{
			$sql_insert = mysql_query("INSERT INTO date_creation (date, profile_media_id, profile_media_type) VALUES ('".$date."','".$id."','artist_project')");
		}
	}
}

?>