<?php
	session_start();
	ob_start();
	include_once('commons/db.php');
	include_once('loggedin_includes.php');
	include_once('classes/ProfileType.php');
	include_once('classes/Commontabs.php');
	include_once('classes/AddContact.php');
	include_once('classes/GeneralInfo.php');
	include_once('classes/CountryState.php');
	//include('recaptchalib.php');
	include_once('classes/ProfileEvent.php');
	include_once('classes/Addfriend.php');
	include_once('classes/GetAllFans.php');
	include_once('classes/NewSuggestion.php');
	include_once('classes/Mails.php');
	include_once('classes/CreatePurifyMember.php');
	include_once('classes/SubscriptionListings.php');
	include_once('classes/PersonalWallDisplayFeeds.php');
	include_once('classes/AddPermission.php');
	include_once('classes/ProfileeditCommunity.php');
	include_once('classes/GetSalesInfo.php');
	include_once('classes/DisplayStatistics.php');
	include_once('classes/Get_Transactions.php');
	include_once("classes/DeleteMembership.php");
	require_once('vimeo-vimeo-php-lib/vimeo.php');
	require_once('classes/Social_details.php');
	require_once("src/MCAPI.class.php");
	$ex_uid1 ="";
    if (!class_exists('\user_keys'))
    {require_once($_SERVER["DOCUMENT_ROOT"].'/classes/user_keys.php');} 
    
    $domainname=$_SERVER['SERVER_NAME'];
	
	$delete_obj_pros = new DeleteMembership();
	$sql_feeds = new PersonalWallDisplayFeeds();
	$newgeneral = new ProfileeditCommunity();
	$trans = new Get_Transactions();
	$all_stats = new DisplayStatistics();
	$member_obj = new CreatePurifyMember();
	$social_obj = new SocialDetails();   
    
    // Get user and password from user_keys table
    $uk = new \user_keys();
    $sgk = $uk->get_UserKeys("Vimeo");
    $key = $sgk['clientid'];
    $secret = $sgk['clientsecret']; 
	$vimeo = new phpVimeo($key, $secret);
    
    // add user name to bucketname in s3 url
    $root = realpath($_SERVER["DOCUMENT_ROOT"]);
    if (!class_exists('S3')) require_once ($root.'/S3.php');    
    $s3 = new S3();
    $generalproimageURI=$s3->getNewURI("generalproimage");
    
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en" /><head>
	<meta https-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Purify Art: Personal Profile</title>
	<script src="includes/jquery.js"></script>

<?php
	$newtab=new Commontabs();

	include_once("header.php");

	$sub_listings = new SubscriptionListings();	
	$get_sales_info = new GetSalesInfo();	
	$get_permission = new AddPermission();	
	$mails_object = new Mails();
	if(isset($_GET['mail_delete']))
	{
		$count_inbox_del = 0;
		$delete = $mails_object->delete_mail($_GET['mail_delete'],$count_inbox_del);
		header("Location: profileedit.php#mail");
	}
	if(isset($_GET['delete_sub_stat_art']))
	{
		$delete = $sub_listings->deleteSubscription($_GET['delete_sub_stat_art']);
		header("Location: profileedit.php#subscrriptions");
	}
?>
<link href="/galleryfiles/gallery.css" rel="stylesheet"/>
<script src="/galleryfiles/jquery.easing.1.3.js"></script>
<script src="/includes/functionsmedia.js" type="text/javascript"></script>
<script src="/includes/functionsURL.js" type="text/javascript"></script>
<script src="/includes/mediaelement-and-player.min.js"></script>
<link rel="stylesheet" href="/includes/mediaelementplayer.min.css" />

<link rel="stylesheet" href="../lightbox/prettyPhoto.css" type="text/css" media="screen" title="prettyPhoto main stylesheet" charset="utf-8" />
<script src="../lightbox/jquery.prettyPhoto.js" type="text/javascript" charset="utf-8"></script>
<script src="../javascripts/popup.js" type="text/javascript" charset="utf-8"></script>


<div id="light-gal" style="z-index:99999;" class="gallery-wrapper">
	<div id="bg">
		<a href="#" class="nextImageBtn" title="next"></a>
		<a href="#" class="prevImageBtn" title="previous"></a>
		<!--<img width="1680" src=".././uploads/gallery/1335074394-7032884797_475ce129ca.jpg" height="1050" alt="" title="" id="bgimg" />-->
	</div>
	<div id="preloader"><img src=".././galleryfiles/ajax-loader_dark.gif" width="32" height="32" /></div>
	<div id="img_title"></div>
	<div id="toolbar">
		<a href="#" title="Pause" id="pauseImg" onClick="setimage('pause')"><img src=".././galleryfiles/pause.png" width="50" height="50"  /></a>
		<a href="#" title="Maximize" id="maximizeImg" onClick="ImageViewMode('full');return false"><img src=".././galleryfiles/toolbar_fs_icon.png" width="50" height="50"  /></a>
		<script type="text/javascript">
		<!--[if IE]>
		$("#maximizeImg").css({"display":"none"});
		<![endif]-->
		</script>
		<a href="#" title="Close" id="closeGal" onClick="CloseGallery();return false"><img src=".././galleryfiles/close.png" width="50" height="50"  /></a>
	</div>
	<div id="thumbnails_wrapper">
	<div id="outer_container">
	<div class="thumbScroller">
		<div class="container" id="gallery-container">
			
		</div>
	</div>
	</div>
	</div>
</div>
<div id="outerContainer">
				<!--<p><a href="../logout.php">Logout</a> </p>
			</div>
		</div>
	</div> 
</div>-->
<!--<link rel="stylesheet" type="text/css" media="screen" href="includes/purify.css" />
	<script src='http://ajax.googleapis.com/ajax/libs/jquery/1.6.1/jquery.min.js'></script>-->
	
	<!--<link rel="stylesheet" href="includes/jquery.ui.all.css" />
	<script src="includes/jquery.ui.core.js"></script>
	<script src="includes/jquery.ui.widget.js"></script>
	<script src="includes/jquery.ui.datepicker.js"></script>-->
	<script>
			$(function() 
			{
				$("#editbday").datepicker({
				dateFormat: 'yy-mm-dd',
				changeMonth: true,
				changeYear: true
				});
			});
		</script>
	<script type="text/javascript">
		$(function() {

			$("#personalTab").organicTabs();
		});
		
		function changeForm()
		{
			document.membership_type.submit();
		}
	</script>
	<script type="text/javascript">
	var $_returnvalue ="";
	var $_returnclicktext ="";
	var $_returnsong_id ="";
	function confirm_renew_member(text,name)
	{
		var a =confirm("Are you sure you want to "+text+" "+name+" membership?");
		if(a == false){
			return false;
		}
	}
	function confirm_renew_my_member(text,name,id)
	{
		var a =confirm("Are you sure you want to "+text+" "+name+" membership?");
		if(a == false){
			return false;
		}else{
			$("#"+id).click();
		}
	}
	function confirmdelete()
	{
		var x;
		var name=confirm("Are You Sure You want To Delete?");
		if (name==false)
		{
			return false;
		}
	}
	function download_directly_song(id,t,c,f){
		var val = confirm("Are you sure you want to download "+t+" by "+c+" from "+f+"?");
		if(val==true){
		window.location = "zipes_1.php?download_meds="+id;
		}
	}

	function ask_to_donate(name,creator,song_id,clicktext)
	{
		$("#get_donationpop").attr("href", "asktodonate.php?name="+name+"&creator="+creator+"&clicktext="+clicktext+"&song_id="+song_id);
		$("#get_donationpop").click();
	}
	function down_video_pop(id)
	{
		var Dvideolink = document.getElementById("download_video"+id);
		Dvideolink.click();
	}

	function download_directly_song(id,t,c,f){
		var val = confirm("Are you sure you want to download "+t+" by "+c+" from "+f+"?");
		if(val==true){
		window.location = "zipes_1.php?download_meds="+id;
		}
	}
	function down_song_pop(id)
	{
		var Dvideolink = document.getElementById("download_song"+id);
		Dvideolink.click();

	}
	function down_song_pop_project(id)
	{
		var Dvideolink = document.getElementById("download_songproject"+id);
		Dvideolink.click();
	}
	function down_video_pop_project(id)
	{
		var Dvideolink = document.getElementById("download_videoproject"+id);
		Dvideolink.click();
	}
	function confirm_delposts()
	{
		var x;
		var name=confirm("Are You Sure You want To Delete?");
		if (name==false)
		{
			return false;
		}
	}
	$(document).ready(function() {
	$("#get_donationpop").fancybox({
		'width'				: '35%',
		'height'			: '25%',
		'transitionIn'		: 'none',
		'transitionOut'		: 'none',
		'type'				: 'iframe',
		'onClosed'			: function(){
								ask_to_donate_result();	
							}
	});
});


function ask_to_donate_result()
{
	if($_returnvalue == 1){
		$_returnvalue ="";
		var Dvideolink = document.getElementById($_returnclicktext);
		setTimeout(function(){
		Dvideolink.click();
		}
		,2000);
	}
	else if($_returnvalue == 2){
		setTimeout(function(){
		$_returnvalue ="";
		window.location = "zipes_1.php?download_meds="+$_returnsong_id;
		}
		,1000);
	}
}
	$("document").ready(function(){
		$("#pass_sub").click(function()
		{
			var pw1 = $("#new_pass").val();
			var pw2 = $("#new_conf_pass").val();
			var pw3 = $("#old_pass").val();
			var invalid = " ";
			if(pw3!='' && pw1!="" && pw2!="")
			{
				var ajax_mess = 'oldie='+ pw3 +'&newie='+ pw1 +'&newiec='+ pw2;
				$.ajax({
					type: 'POST',
					url : 'Change_password.php',
					data: ajax_mess,
					success: function(data)  
					{
						if(data!="")
						{
							$("#error_pass").html(data);
						}
						else
						{
							$("#error_pass").html('');
							alert("Your Password is changed.");
							window.location = "profileedit.php";
						}
					}
				});
			}
		});
	});
	
	function validate_passesform()
	{
		var invalid = " "; // Invalid character is a space
		var minLength = 6; // Minimum length
		var pw1 = $("#new_pass").val();
		var pw2 = $("#new_conf_pass").val();
		var pw3 = $("#old_pass").val();
		
		if(pw3=='')
		{
			alert("Please enter your old password.");
			$("#old_pass").focus();
			return false;
		}
		
		if($("#old_pass").val().indexOf(invalid) > -1)
		{
			alert("Please enter valid password.");
			$("#old_pass").focus();
			return false;
		}
		
		// check for minimum length
		if ($("#new_pass").val().length < minLength)
		{
			alert('Your password must be at least ' + minLength + ' characters long. Try again.');
			$("#new_pass").focus();
			return false;
		}
		// check for a value in both fields.
		if (pw1 == '' || pw2 == '')
		{
			alert('Please Re-enter your password twice ');
			$("#new_conf_pass").focus();
			return false;
		}
		// check for spaces
		if ($("#new_pass").val().indexOf(invalid) > -1)
		{
			alert("Sorry, spaces are not allowed.");
			$("#new_pass").focus();
			return false;
		}
		
		if (pw1 != pw2)
		{
			alert ("You did not enter the same new password twice. Please re-enter your password.");
			$("#new_conf_pass").focus();
			return false;
		}
	}
	
	function confirm_delete(y)
	{
		var x;
		var name=confirm("Are You Sure You want To Delete  "+y+" ?");
		if (name==false)
		{
			return false;
		}
	}
	function confirm_creditcard_delete(s)
	{
		var card = confirm("Are You Sure You want To Remove  "+s+" Credit Card?");
		if (card==false)
		{
			return false;
		}
	}
	function confirm_member_delete(s)
	{
		var member = confirm("Are You Sure You want To delete  "+s+" Member?");
		if (member==false)
		{
			return false;
		}
	}
	function runscript()
	{
		$("#searchlink").click();
	}
	function find_firends(){
		$("#searchlink").click();
	}
	</script>
	<script type="text/javascript">
	
	function handleSelection2(choice)
	{
		document.getElementById('mymember').style.display="block";
		document.getElementById('purimember').style.display="none";
	}
	function handleSelection(choice) {
	$(".list-wrap").css({"height":""});
	//document.getElementById('select').disabled=true;
	if(choice=='total')
	{
		document.getElementById(choice).style.display="block";
		document.getElementById('salesLog').style.display="none";
		document.getElementById('payments').style.display="none";
	}
	else if(choice=='payments')
	{
		document.getElementById(choice).style.display="block";
		document.getElementById('total').style.display="none";
		document.getElementById('salesLog').style.display="none";
	}
	else
	{
		if(document.getElementById(choice)!=null){ document.getElementById(choice).style.display="block"; }
		if(document.getElementById('payments')!=null){ document.getElementById('payments').style.display="none";}
		if(document.getElementById('total')!=null){ document.getElementById('total').style.display="none";}
	}
	
	if(choice=='upcoming')
		{
		  document.getElementById(choice).style.display="block";
		  document.getElementById('published').style.display="none";
		  document.getElementById('allevents').style.display="none";
		}
	if(choice=='published')
		{
		  document.getElementById(choice).style.display="block";
		  document.getElementById('upcoming').style.display="none";
		  document.getElementById('allevents').style.display="none";
		}
	if(choice=='allevents')
		{
		  document.getElementById(choice).style.display="block";
		  document.getElementById('upcoming').style.display="none";
		  document.getElementById('published').style.display="none";
		}

	if(choice=='artist')
		{
		  document.getElementById(choice).style.display="block";
		  document.getElementById('select').style.display="none";
		}
		else
		{
			if(document.getElementById(choice)!=null){ document.getElementById(choice).style.display="block"; }
			if(document.getElementById('artist')!=null){ document.getElementById('artist').style.display="none"; }
		}

	if(choice=='community')
		{
		  document.getElementById(choice).style.display="block";
		  document.getElementById('select').style.display="none";
		}
		else
		{
			if(document.getElementById(choice)!=null){ document.getElementById(choice).style.display="block"; }
			if(document.getElementById('community')!=null){ document.getElementById('community').style.display="none"; }
		}

	if(choice=='artistFans')
		{
		  document.getElementById(choice).style.display="block";
		  document.getElementById('select').style.display="none";
		}
		else
		{
			if(document.getElementById(choice)!=null){ document.getElementById(choice).style.display="block"; }
			if(document.getElementById('artistFans')!=null){ document.getElementById('artistFans').style.display="none"; }
		}

	if(choice=='communityFans')
		{
		  document.getElementById(choice).style.display="block";
		  document.getElementById('select').style.display="none";
		}
		else
		{
			if(document.getElementById(choice)!=null){ document.getElementById(choice).style.display="block"; }
			if(document.getElementById('communityFans')!=null){ document.getElementById('communityFans').style.display="none"; }
		}
		
		if(choice=='purimember')
		{
		  document.getElementById(choice).style.display="block";
		 // document.getElementById('purimember').style.display="none";
		}
		else
		{
			if(document.getElementById(choice)!=null){ document.getElementById(choice).style.display="block"; }
			if(document.getElementById('purimember')!=null){ document.getElementById('purimember').style.display="none"; }
		}	
		
	if(choice=='mymember')
		{
		  document.getElementById(choice).style.display="block";
		  document.getElementById('purimember').style.display="none";
		}
		else
		{
			if(document.getElementById(choice)!=null){ document.getElementById(choice).style.display="block"; }
			if(document.getElementById('mymember')!=null){ document.getElementById('mymember').style.display="none"; }
		}		

	if(choice=='artmember')
		{
			document.getElementById(choice).style.display="block";
			if(document.getElementById("total_artist")!=null){
				document.getElementById("total_artist").style.display="block";
			}
			document.getElementById('purimember').style.display="none";
		}
		else
		{
			
			document.getElementById(choice).style.display="block";
			if(document.getElementById("total_artist")!= null){
				document.getElementById("total_artist").style.display="none";
			}
			//document.getElementById(choice).style.display="none";
			//document.getElementById('artmember').style.display="none";
			if(document.getElementById('artmember')!=null){ document.getElementById('artmember').style.display="none"; }
		}		

	if(choice=='commember')
		{
			document.getElementById(choice).style.display="block";
			if(document.getElementById("total_community")!= null){
				document.getElementById("total_community").style.display="block";
			}
			document.getElementById('purimember').style.display="none";
		}
		else
		{
			if(document.getElementById("total_community")!= null){
				document.getElementById("total_community").style.display="none";
			}
			//document.getElementById(choice).style.display="block";
			//document.getElementById('commember').style.display="none";
			if(document.getElementById(choice)!=null){ document.getElementById(choice).style.display="block"; }
			if(document.getElementById('commember')!=null){ document.getElementById('commember').style.display="none"; }
		}
	if(choice=='comprojectmember')
	{
		var text_value1 = $('#viewmembership option:selected').attr('name');
		var text_value = $('#viewmembership').val();
		var ajax_mess = 'id='+ text_value1 +'&type='+ text_value;
		$.ajax({
			type: 'POST',
			url : 'get_project_memberlist.php',
			data: ajax_mess,
			success: function(data)  
			{
				if(data!="")
				{
					$("#comprojectmember").html(data);
					//$("#comprojectmember").html(data);
				}
			}
		});
		document.getElementById(choice).style.display="block";
		document.getElementById('purimember').style.display="none";
		
	}
	else
	{
		if(document.getElementById(choice)!=null){ document.getElementById(choice).style.display="block"; }
		if(document.getElementById('comprojectmember')!=null){ document.getElementById('comprojectmember').style.display="none"; }
	}
	if(choice=='artprojectmember')
	{
		var text_value1 = $('#viewmembership option:selected').attr('name');
		var text_value = $('#viewmembership').val();
		var ajax_mess = 'id='+ text_value1 +'&type='+ text_value;
		$.ajax({
			type: 'POST',
			url : 'get_project_memberlist.php',
			data: ajax_mess,
			success: function(data)  
			{
				if(data!="")
				{
					//$("#artprojectmember").innerhtml(data);
					$("#artprojectmember").html(data);
				}
			}
		});
		document.getElementById(choice).style.display="block";
		document.getElementById('purimember').style.display="none";
		
	}
	else
	{
		if(document.getElementById(choice)!=null){ document.getElementById(choice).style.display="block"; }
		if(document.getElementById('artprojectmember')!=null){ document.getElementById('artprojectmember').style.display="none"; }
	}
	if(choice=='mycal')
		{
		  document.getElementById(choice).style.display="block";
		  document.getElementById('mylist').style.display="none";
		}
		else
		{
			if(document.getElementById(choice)!=null){ document.getElementById(choice).style.display="block"; }
			if(document.getElementById('mycal')!=null){ document.getElementById('mycal').style.display="none"; }
		}	
	
	
	
	if(choice=='subPutify')
	{
		if(document.getElementById(choice)!=null){ document.getElementById(choice).style.display="block"; }
		if(document.getElementById('all_putify')!=null){ document.getElementById('all_putify').style.display="none"; }
		if(document.getElementById('subArtist')!=null){ document.getElementById('subArtist').style.display="none"; }
		if(document.getElementById('subCommunity')!=null){ document.getElementById('subCommunity').style.display="none"; }
		if(document.getElementById('sub_Projects')!=null){ document.getElementById('sub_Projects').style.display="none"; }
		if(document.getElementById('sub_Events')!=null){ document.getElementById('sub_Events').style.display="none"; }
	}
	/*else
	{
	  document.getElementById(choice).style.display="block";
	  document.getElementById('subPutify').style.display="none";
	}*/
		
	if(choice=='subArtist')
	{
		if(document.getElementById(choice)!=null){ document.getElementById(choice).style.display="block"; }
		if(document.getElementById('all_putify')!=null){ document.getElementById('all_putify').style.display="none"; }
		if(document.getElementById('subPutify')!=null){ document.getElementById('subPutify').style.display="none"; }
		if(document.getElementById('subCommunity')!=null){ document.getElementById('subCommunity').style.display="none"; }
		if(document.getElementById('sub_Projects')!=null){ document.getElementById('sub_Projects').style.display="none"; }
		if(document.getElementById('sub_Events')!=null){ document.getElementById('sub_Events').style.display="none"; }
	}
		/*else
		{
		  document.getElementById(choice).style.display="block";
		  document.getElementById('subArtist').style.display="none";
		}*/

	if(choice=='subCommunity')
	{	
		if(document.getElementById(choice)!=null){ document.getElementById(choice).style.display="block"; }
		if(document.getElementById('all_putify')!=null){ document.getElementById('all_putify').style.display="none"; }
		if(document.getElementById('subArtist')!=null){ document.getElementById('subArtist').style.display="none"; }
		if(document.getElementById('subPutify')!=null){ document.getElementById('subPutify').style.display="none"; }
		if(document.getElementById('sub_Projects')!=null){ document.getElementById('sub_Projects').style.display="none"; }
		if(document.getElementById('sub_Events')!=null){ document.getElementById('sub_Events').style.display="none"; }
	}
		/*else
		{
		  document.getElementById(choice).style.display="block";
		  document.getElementById('subCommunity').style.display="none";
		}*/
		
	if(choice=='sub_Projects')
	{
		if(document.getElementById(choice)!=null){ document.getElementById(choice).style.display="block"; }
		if(document.getElementById('subCommunity')!=null){ document.getElementById('subCommunity').style.display="none"; }
		if(document.getElementById('all_putify')!=null){ document.getElementById('all_putify').style.display="none"; }
		if(document.getElementById('subArtist')!=null){ document.getElementById('subArtist').style.display="none"; }
		if(document.getElementById('subPutify')!=null){ document.getElementById('subPutify').style.display="none"; }
		if(document.getElementById('sub_Events')!=null){ document.getElementById('sub_Events').style.display="none"; }
	}
		/*else
		{
		  document.getElementById(choice).style.display="block";
		  document.getElementById('sub_Projects').style.display="none";
		}*/
		
	if(choice=='sub_Events')
	{
		if(document.getElementById(choice)!=null){ document.getElementById(choice).style.display="block"; }
		if(document.getElementById('sub_Projects')!=null){ document.getElementById('sub_Projects').style.display="none"; }
		if(document.getElementById('subCommunity')!=null){ document.getElementById('subCommunity').style.display="none"; }
		if(document.getElementById('all_putify')!=null){ document.getElementById('all_putify').style.display="none"; }
		if(document.getElementById('subArtist')!=null){ document.getElementById('subArtist').style.display="none"; }
		if(document.getElementById('subPutify')!=null){ document.getElementById('subPutify').style.display="none"; }
	}
		/*else
		{
		  document.getElementById(choice).style.display="block";
		  document.getElementById('sub_Events').style.display="none";
		}*/
		
		
		
		
	if(choice=='events_feeds')
		{
		  document.getElementById(choice).style.display="block";
		  document.getElementById('all_news_feeds').style.display="none";
		}
		else
		{
			if(document.getElementById(choice)!=null){ document.getElementById(choice).style.display="block"; }
			if(document.getElementById('events_feeds')!=null){ document.getElementById('events_feeds').style.display="none"; }
		}
		
	if(choice=='project_feeds')
		{
		  document.getElementById(choice).style.display="block";
		  document.getElementById('all_news_feeds').style.display="none";
		}
		else
		{
			if(document.getElementById(choice)!=null){ document.getElementById(choice).style.display="block"; }
			if(document.getElementById('project_feeds')!=null){ document.getElementById('project_feeds').style.display="none"; }
		}

	if(choice=='music_feeds')
		{
		  document.getElementById(choice).style.display="block";
		  document.getElementById('all_news_feeds').style.display="none";
		}
		else
		{
			if(document.getElementById(choice)!=null){ document.getElementById(choice).style.display="block"; }
			if(document.getElementById('music_feeds')!=null){ document.getElementById('music_feeds').style.display="none"; }
		}

	if(choice=='photo_feeds')
		{
		  document.getElementById(choice).style.display="block";
		  document.getElementById('all_news_feeds').style.display="none";
		}
		else
		{
			if(document.getElementById(choice)!=null){ document.getElementById(choice).style.display="block"; }
			if(document.getElementById('photo_feeds')!=null){ document.getElementById('photo_feeds').style.display="none"; }
		}
		
	if(choice=='video_feeds')
		{
		  document.getElementById(choice).style.display="block";
		  document.getElementById('all_news_feeds').style.display="none";
		}
		else
		{
			if(document.getElementById(choice)!=null){ document.getElementById(choice).style.display="block"; }
			if(document.getElementById('video_feeds')!=null){ document.getElementById('video_feeds').style.display="none"; }
		}
	if(choice=='events_feeds_p')
		{
		  document.getElementById(choice).style.display="block";
		  document.getElementById('all_news_feeds_p').style.display="none";
		}
		else
		{
			if(document.getElementById(choice)!=null){ document.getElementById(choice).style.display="block"; }
			if(document.getElementById('events_feeds_p')!=null){ document.getElementById('events_feeds_p').style.display="none"; }
		}
		
	if(choice=='project_feeds_p')
		{
		  document.getElementById(choice).style.display="block";
		  document.getElementById('all_news_feeds_p').style.display="none";
		}
		else
		{
			if(document.getElementById(choice)!=null){ document.getElementById(choice).style.display="block"; }
			if(document.getElementById('project_feeds_p')!=null){ document.getElementById('project_feeds_p').style.display="none"; }
		}

	if(choice=='music_feeds_p')
		{
		  document.getElementById(choice).style.display="block";
		  document.getElementById('all_news_feeds_p').style.display="none";
		}
		else
		{
			if(document.getElementById(choice)!=null){ document.getElementById(choice).style.display="block"; }
			if(document.getElementById('music_feeds_p')!=null){ document.getElementById('music_feeds_p').style.display="none"; }
		}

	if(choice=='photo_feeds_p')
		{
		  document.getElementById(choice).style.display="block";
		  document.getElementById('all_news_feeds_p').style.display="none";
		}
		else
		{
			if(document.getElementById(choice)!=null){ document.getElementById(choice).style.display="block"; }
			if(document.getElementById('photo_feeds_p')!=null){ document.getElementById('photo_feeds_p').style.display="none"; }
		}
		
	if(choice=='video_feeds_p')
		{
		  document.getElementById(choice).style.display="block";
		  document.getElementById('all_news_feeds_p').style.display="none";
		}
		else
		{
			if(document.getElementById(choice)!=null){ document.getElementById(choice).style.display="block"; }
			if(document.getElementById('video_feeds_p')!=null){ document.getElementById('video_feeds_p').style.display="none"; }
		}
		
	if(choice=='statmedia')
		{
		  document.getElementById(choice).style.display="block";
		  document.getElementById('statpersonal').style.display="none";
		}
		else
		{
			if(document.getElementById(choice)!=null){ document.getElementById(choice).style.display="block"; }
			if(document.getElementById('statmedia')!=null){ document.getElementById('statmedia').style.display="none"; }
		}

	if(choice=='statartist')
		{
		  document.getElementById(choice).style.display="block";
		  document.getElementById('statpersonal').style.display="none";
		}
		else
		{
			if(document.getElementById(choice)!=null){ document.getElementById(choice).style.display="block"; }
			if(document.getElementById('statartist')!=null){ document.getElementById('statartist').style.display="none"; }
		}		

	if(choice=='statcommunity')
		{
		  document.getElementById(choice).style.display="block";
		  document.getElementById('statpersonal').style.display="none";
		}
		else
		{
			if(document.getElementById(choice)!=null){ document.getElementById(choice).style.display="block"; }
			if(document.getElementById('statcommunity')!=null){ document.getElementById('statcommunity').style.display="none"; }
		}
	
	if(choice=='awaiting_response')
		{
			document.getElementById(choice).style.display="block";
			document.getElementById('all_friends_fans').style.display="block";
		}
		else
		{
			if(document.getElementById(choice)!=null){ document.getElementById(choice).style.display="block"; }
			if(document.getElementById('awaiting_response')!=null){ document.getElementById('awaiting_response').style.display="none"; }
		}
		
	if(choice=='artist_fansa')
		{
			document.getElementById(choice).style.display="block";
			document.getElementById('all_friends_fans').style.display="none";
		}
		else
		{
			if(document.getElementById(choice)!=null){ document.getElementById(choice).style.display="block"; } 
			if(document.getElementById('artist_fansa')!=null){ document.getElementById('artist_fansa').style.display="none"; }
		}
		
	if(choice=='artist_friendsa')
		{
			document.getElementById(choice).style.display="block";
			document.getElementById('all_friends_fans').style.display="none";
		}
		else
		{
			if(document.getElementById(choice)!=null){ document.getElementById(choice).style.display="block"; }
			if(document.getElementById('artist_friendsa')!=null){ document.getElementById('artist_friendsa').style.display="none"; }
		}
		
	if(choice=='community_fansa')
		{
			document.getElementById(choice).style.display="block";
			document.getElementById('all_friends_fans').style.display="none";
		}
		else
		{
			if(document.getElementById(choice)!=null){ document.getElementById(choice).style.display="block"; }
			if(document.getElementById('community_fansa')!=null){ document.getElementById('community_fansa').style.display="none"; }
		}
		
	if(choice=='community_friendsa')
		{
			document.getElementById(choice).style.display="block";
			document.getElementById('all_friends_fans').style.display="none";
		}
		else
		{
			if(document.getElementById(choice)!=null){ document.getElementById(choice).style.display="block"; }
			if(document.getElementById('community_friendsa')!=null){ document.getElementById('community_friendsa').style.display="none"; }
		}
	}
	
	//if($("#viewmembership").val()=='mymember')
	//{
		//$('#viewmembership').change();
	//}
	
	$("document").ready(function(){
	$("#countrySelect").change(function (){
			$.post("State.php", { country_id:$("#countrySelect").val() },
				function(data)
				{
				//alert("Data Loaded: " + data);											
					$("#stateSelect").html(data);
				});
		});
	});	
	$("document").ready(function(){
	$("#countrySelectmember").change(function (){
			$.post("State.php", { country_id:$("#countrySelectmember").val() },
				function(data)
				{
				//alert("Data Loaded: " + data);											
					$("#stateSelectmember").html(data);
				});
		});
	});		
	/*window.onload = function(){ 
			$.post("State.php", { country_id:$("#countrySelect").val() },
				function(data)
				{
						//alert("Data Loaded: " + data);											
						$("#stateSelect").html(data);
				}
			);
		}*/
		
	function selectChange()
	{
		var select2 = document.getElementById("select").value;
		window.location = "profileedit.php?sel_val="+document.getElementById("select").value +"#addressbook";
	}

	function validate()
	{
		var fname = document.getElementById("editfname");
		if(fname.value=="" || fname.value==null)
		{
			alert("Please enter First Name.")
			return false;
		}
	}

	var ContentHeight = 250;
	var TimeToSlide = 250.0;
	var openAccordion = '';

	function runAccordion(index)
	{
	  var nID = "Accordion" + index + "Content";
	  if(openAccordion == nID)
		nID = '';
		
	  setTimeout("animate(" + new Date().getTime() + "," + TimeToSlide + ",'" + openAccordion + "','" + nID + "')", 33);
	  
	  openAccordion = nID;
	}

	function animate(lastTick, timeLeft, closingId, openingId)
	{  
	  var curTick = new Date().getTime();
	  var elapsedTicks = curTick - lastTick;
	  
	  var opening = (openingId == '') ? null : document.getElementById(openingId);
	  var closing = (closingId == '') ? null : document.getElementById(closingId);
	 
	  if(timeLeft <= elapsedTicks)
	  {
		if(opening != null)
		  opening.style.height = ContentHeight + 'px';
		
		if(closing != null)
		{
		  closing.style.display = 'none';
		  closing.style.height = '0px';
		}
		return;
	  }
	 
	  timeLeft -= elapsedTicks;
	  var newClosedHeight = Math.round((timeLeft/TimeToSlide) * ContentHeight);

	  if(opening != null)
	  {
		if(opening.style.display != 'block')
		  opening.style.display = 'block';
		opening.style.height = (ContentHeight - newClosedHeight) + 'px';
	  }
	  
	  if(closing != null)
		closing.style.height = newClosedHeight + 'px';

	  setTimeout("animate(" + curTick + "," + timeLeft +",'" + closingId + "','" + openingId + "')", 33);
	}
	
	var ContentHeight_new = 115;
	var TimeToSlide = 250.0;
	var openAccordion = '';

	function runAccordion_new(index)
	{
	  var nID = "Accordion_new" + index + "Content";
	  if(openAccordion == nID)
		nID = '';
		
	  setTimeout("animate_new(" + new Date().getTime() + "," + TimeToSlide + ",'" + openAccordion + "','" + nID + "')", 33);
	  
	  openAccordion = nID;
	}

	function animate_new(lastTick, timeLeft, closingId, openingId)
	{  
	  var curTick = new Date().getTime();
	  var elapsedTicks = curTick - lastTick;
	  
	  var opening = (openingId == '') ? null : document.getElementById(openingId);
	  var closing = (closingId == '') ? null : document.getElementById(closingId);
	 
	  if(timeLeft <= elapsedTicks)
	  {
		if(opening != null)
		  opening.style.height = ContentHeight_new + 'px';
		
		if(closing != null)
		{
		  closing.style.display = 'none';
		  closing.style.height = '0px';
		}
		return;
	  }
	 
	  timeLeft -= elapsedTicks;
	  var newClosedHeight = Math.round((timeLeft/TimeToSlide) * ContentHeight_new);

	  if(opening != null)
	  {
		if(opening.style.display != 'block')
		  opening.style.display = 'block';
		opening.style.height = (ContentHeight_new - newClosedHeight) + 'px';
	  }
	  
	  if(closing != null)
		closing.style.height = newClosedHeight + 'px';

	  setTimeout("animate_new(" + curTick + "," + timeLeft +",'" + closingId + "','" + openingId + "')", 33);
	}
	
	function confirmdelete(name)
	{
		var con_del = confirm("Are You Sure You Want To UnSubscribe From  "+ name +".");
		if(con_del == false)
		{
			return false;
		}
	}
	
	function confirmdelete_addr(name,email)
	{
		var con_del = confirm("Are you sure you want to delete the contact, "+ name +" ("+email+").");
		if(con_del == false)
		{
			return false;
		}
		else if(con_del == true)
		{
			loader_body.style.display = 'block';
			all_wrap_personal.style.display = 'none';
		}
	}
	</script>
	<script type="text/javascript">
		var timeout	= 500;
		var closetimer	= 0;
		var ddmenuitem	= 0;

		// open hidden layer
		function mopen(id)
		{	
			// cancel close timer
			mcancelclosetime();

			// close old layer
			if(ddmenuitem) ddmenuitem.style.visibility = 'hidden';

			// get new layer and show it
			ddmenuitem = document.getElementById(id);
			ddmenuitem.style.visibility = 'visible';

		}
		// close showed layer
		function mclose()
		{
			if(ddmenuitem) ddmenuitem.style.visibility = 'hidden';
		}

		// go close timer
		function mclosetime()
		{
			closetimer = window.setTimeout(mclose, timeout);
		}

		// cancel close timer
		function mcancelclosetime()
		{
			if(closetimer)
			{
				window.clearTimeout(closetimer);
				closetimer = null;
			}
		}

		// close layer when click-out
		document.onclick = mclose; 
	</script>
	<script type="text/javascript">
		function link_add()
		{
			window.location=("add_contact.php");
		}
		
		function on_values()
		{
			var dataString_news = 'newsletter=on';
				
			$.ajax({
				type: 'POST',
				url : 'SubscriptionAjaxRadio.php',
				data: dataString_news,
				success: function(data) 
				{
					alert("Your settings are saved.");
				}
			});
		}
		
		function off_values()
		{
			var dataString_news = 'newsletter=off';
				
			$.ajax({
				type: 'POST',
				url : 'SubscriptionAjaxRadio.php',
				data: dataString_news,
				success: function(data) 
				{
					alert("Your settings are saved.");
				}
			});
		}
		
		function on_feeds(id)
		{
			var dataString_news = 'feeds=on&ids='+id;
				
			$.ajax({
				type: 'POST',
				url : 'SubscriptionAjaxRadio.php',
				data: dataString_news,
				success: function(data) 
				{
					alert("Your settings are saved.");
				}
			});
		}
		
		function off_feeds(id)
		{
			var dataString_news = 'feeds=off&ids='+id;
				
			$.ajax({
				type: 'POST',
				url : 'SubscriptionAjaxRadio.php',
				data: dataString_news,
				success: function(data) 
				{
					alert("Your settings are saved.");
				}
			});
		}
		
		/* function a_on_feeds(id)
		{
			var dataString_news = 'feeds=on&ids='+id;
				
			$.ajax({
				type: 'POST',
				url : 'SubscriptionAjaxRadio.php',
				data: dataString_news,
				success: function(data) 
				{
					alert("Your settings are saved.");
				}
			});
		}
		
		function a_off_feeds(id)
		{
			var dataString_news = 'feeds=off&ids='+id;
				
			$.ajax({
				type: 'POST',
				url : 'SubscriptionAjaxRadio.php',
				data: dataString_news,
				success: function(data) 
				{
					alert("Your settings are saved.");
				}
			});
		} */
		
		function on_emails(id,from,to,type)
		{
			var dataString_news = 'emails=on&ids='+id+'&from='+from+'&to='+to+'&type='+type;
				
			$.ajax({
				type: 'POST',
				url : 'SubscriptionAjaxRadio.php',
				data: dataString_news,
				success: function(data) 
				{
					alert("Your settings are saved.");
				}
			});
		}
		
		function off_emails(id,from,to,type)
		{
			var dataString_news = 'emails=off&ids='+id+'&from='+from+'&to='+to+'&type='+type;
				
			$.ajax({
				type: 'POST',
				url : 'SubscriptionAjaxRadio.php',
				data: dataString_news,
				success: function(data) 
				{
					alert("Your settings are saved.");
				}
			});
		}
		
		/* function a_on_emails(id,from,to,type)
		{
			var dataString_news = 'emails=on&ids='+id+'&from='+from+'&to='+to+'&type='+type;
				
			$.ajax({
				type: 'POST',
				url : 'SubscriptionAjaxRadio.php',
				data: dataString_news,
				success: function(data) 
				{
					alert("Your settings are saved.");
				}
			});
		}
		
		function a_off_emails(id,from,to,type)
		{
			var dataString_news = 'emails=off&ids='+id+'&from='+from+'&to='+to+'&type='+type;
				
			$.ajax({
				type: 'POST',
				url : 'SubscriptionAjaxRadio.php',
				data: dataString_news,
				success: function(data) 
				{
					alert("Your settings are saved.");
				}
			});
		} */
		
		/* $(document).ready(function()
		{			
			$('div#subscrriptions div#mediaContent div#subArtist input.chk').click(function()
			{
				//alert("hii");
				var id = $(this).attr("id");
				//alert(id);
				
				var dataString_art = 'art_chk_radio='+ id;
				
				$.ajax({
					type: 'POST',
					url : 'SubscriptionAjaxRadio.php',
					data: dataString_art,
					success: function(data) 
					{
						alert("Your settings are saved.");
					}
				});
			});
			
			$('div#subscrriptions div#mediaContent div#subCommunity input.chk').click(function()
			{
				//alert("hii");
				var id = $(this).attr("id");
				//alert(id);
				
				var dataString_art = 'art_chk_radio='+ id;
				
				$.ajax({
					type: 'POST',
					url : 'SubscriptionAjaxRadio.php',
					data: dataString_art,
					success: function(data) 
					{
						alert("Your settings are saved.");
					}
				});
			});
			
			$('div#subscrriptions div#mediaContent div#sub_Events input.chk').click(function()
			{
				//alert("hii");
				var id = $(this).attr("id");
				//alert(id);
				
				var dataString_art = 'art_chk_radio='+ id;
				
				$.ajax({
					type: 'POST',
					url : 'SubscriptionAjaxRadio.php',
					data: dataString_art,
					success: function(data) 
					{
						alert("Your settings are saved.");
					}
				});
			});
			
			$('div#subscrriptions div#mediaContent div#sub_Projects input.chk').click(function()
			{
				//alert("hii");
				var id = $(this).attr("id");
				//alert(id);
				
				var dataString_art = 'art_chk_radio='+ id;
				
				$.ajax({
					type: 'POST',
					url : 'SubscriptionAjaxRadio.php',
					data: dataString_art,
					success: function(data) 
					{
						alert("Your settings are saved.");
					}
				});
			});
		}); */
	</script>
	<script>
		$(document).ready(function(){
		  $("#toggle_div").click(function(){
		  var gets = document.getElementById("toggle_div");
			if(gets.value=='Add Post ( + )')
			{
				document.getElementById("toggle_div").value = 'Add Post ( - )';
			}
			else if(gets.value=='Add Post ( - )')
			{
				document.getElementById("toggle_div").value = 'Add Post ( + )';
			}
			$("#toogles").toggle("slow");
		  });
		});
		
		$(document).ready(function(){
		  $("#toggle_my_div").click(function(){
		  var gets = document.getElementById("toggle_my_div");
			if(gets.value=='My Posts ( + )')
			{
				document.getElementById("toggle_my_div").value = 'My Posts ( - )';
			}
			else if(gets.value=='My Posts ( - )')
			{
				document.getElementById("toggle_my_div").value = 'My Posts ( + )';
			}
			$("#toogles_my").toggle("slow");
		  });
		});
		function confirm_subscription_delete(x)
		{
			var a = confirm("Are you sure you want to unsubscribe from "+x+".");
			if(a==false){
			return(false);
			}
		}
	</script>
	<?php
	if(isset($_GET['yes']) && $_GET['yes']=="in")
	{
	?>
		<script type="text/javascript">
		$("document").ready(function(){
		alert("User Contact has been inserted.");
		});
		</script>
	<?php
	}
	if(isset($_GET['yes']) && $_GET['yes']=="up")
	{
	?>
		<script type="text/javascript">
		$("document").ready(function(){
		alert("User Contact has been updated.");
		});
		</script>
	<?php
	}
	?>
<!--<script type="text/javascript" src="./fancybox/jquery.fancybox-1.3.4.pack.js"></script>
<link rel="stylesheet" type="text/css" href="./fancybox/jquery.fancybox-1.3.4.css" media="screen" />-->
<script type="text/javascript">
//!window.jQuery && document.write('<script src="./fancybox/jquery-1.4.3.min.js"><\/script>');
	$(document).ready(function() {
				$("#popup_s3_error").fancybox({
				'width'				: '75%',
				'height'			: '70%',
				'autoScale'			: true,
				'transitionIn'		: 'none',
				'transitionOut'		: 'none',
				'type'				: 'iframe'
			});
		});
		
		$(document).ready(function() {
			$("#become_member_fancy_box").fancybox({
				helpers : {
					media : {},
					buttons : {},
					title : null
				}
			});
		});
	$(document).ready(function() {
		$("#req_pay_new").fancybox({
			helpers : {
				media : {},
				buttons : {},
				title : null
			},
			afterClose: function() { 
				$("#all_wrap_personal").css({"display":"none"});
				$("#loader_body").css({"display":"block"});
				parent.location.reload(true); 
			} 
		});
		/*$("#req_pay_new").fancybox({
			'width'				: '55%',
			'height'			: '65%',
			'autoScale'			: true,
			'transitionIn'		: 'none',
			'transitionOut'		: 'none',
			'type'				: 'iframe',
			'onClosed': function() {   
				 parent.location.reload(true); 
				}
		});*/
	});
</script>
<div id="toplevelNav"></div>
<div id="loader_body" style="display: block; text-align: center;">
	<img src="images/ajax_loader_large.gif" style="height: 40px; position: relative; width: 40px;">
</div>
<div id="all_wrap_personal" style="display:none;">
      <div id="profileTabs">
          <ul>
           <?php
		   $fan_var = 0;
		   $member_var = 0;
		   $check=new AddContact();
		   
		   $new_artist=$newtab->artist_status();
		   $new_community=$newtab->community_status();
		   $new_team=$newtab->team_status();
		   $sql_fan_chk = mysql_query("SELECT * FROM fan_club_membership WHERE email='".$_SESSION['login_email']."' AND accept=1");
			if($sql_fan_chk!="" && $sql_fan_chk!=NULL)
			{
				if(mysql_num_rows($sql_fan_chk)>0)
				{
					while($row_fan_chk = mysql_fetch_assoc($sql_fan_chk))
					{
						$fan_var = $fan_var + 1;
					}
				}
			}
		   $gen_det = mysql_query("SELECT * FROM general_user WHERE email='".$_SESSION['login_email']."'");
			if($gen_det!="" && $gen_det!=NULL)
			{
				if(mysql_num_rows($gen_det)>0){
					$res_gen_det = mysql_fetch_assoc($gen_det);
				}
			}
		   $date = date("Y-m-d");
			$sql_member_chk = mysql_query("SELECT * FROM purify_membership WHERE general_user_id='".$res_gen_det['general_user_id']."' AND (expiry_date>'".$date."' OR lifetime=1)");
			if($sql_member_chk!="" && $sql_member_chk!=NULL)
			{
				if(mysql_num_rows($sql_member_chk)>0)
				{
					$member_var = $member_var + 1;
				}
			}
			// echo $newres1['artist_id'];
			// echo "<br/>";
			// echo $new_artist['status'];
			// echo "<br/>";
			// echo $newres1['member_id'];
			// echo "<br/>";
			// echo $newres1['community_id'];
			// echo "<br/>";
			// echo $new_community['status'];
			// echo "<br/>";
			// echo $newres1['team_id'];
			// echo "<br/>";
			// echo $new_team['status'];
			// echo "<br/>";
			// echo $fan_var;
			
		  if(($newres1['artist_id']==0 && $new_artist['status']==1) && $newres1['member_id']==0 && ($newres1['community_id']==0 && $new_community['status']==1) && ($newres1['team_id']==0 && $new_team['status']==1) && $fan_var==0 && $avail_meds_tabs_chk==0)
		  {
		  ?>
            <li class="active">HOME</li>		
            <!--<li><a href="profileedit_support.php">SUPPORT</a></li>-->
		   <?php
		   }
		   elseif(($newres1['artist_id']==0 && ($new_artist['status']!=0 ||  $new_artist['status']=="")) && ($newres1['community_id']==0 && ($new_community['status']!=0 || $new_community['status']=="")) && ($newres1['team_id']==0 && ($new_team['status']!=0 || $new_team['status']=="")) && $fan_var!=0)
		   {
		   ?>
			<li class="active">HOME</li>
			<li><a href="profileedit_media.php">Media</a></li>
            <!--<li><a href="profileedit_support.php">SUPPORT</a></li>-->
		   <?php
		   }
		    elseif(($newres1['artist_id']==0 && ($new_artist['status']!=0 ||  $new_artist['status']=="")) && ($newres1['community_id']==0 && ($new_community['status']!=0 || $new_community['status']=="")) && ($newres1['team_id']==0 && ($new_team['status']!=0 || $new_team['status']=="")) && $member_var!=0)
		   {
		   ?>
			<li class="active">HOME</li>
			<li><a href="profileedit_media.php">Media</a></li>
            <!--<li><a href="profileedit_support.php">SUPPORT</a></li>-->
		   <?php
		   }
		   elseif(($newres1['artist_id']==0 && ($new_artist['status']!=0 ||  $new_artist['status']=="")) && ($newres1['community_id']==0 && ($new_community['status']!=0 || $new_community['status']=="")) && ($newres1['team_id']==0 && ($new_team['status']!=0 || $new_team['status']=="")) && $avail_meds_tabs_chk!=0)
		   {
		   ?>
			<li class="active">HOME</li>
			<li><a href="profileedit_media.php">Media</a></li>
            <!--<li><a href="profileedit_support.php">SUPPORT</a></li>-->
		   <?php
		   }
		   elseif(($newres1['artist_id']!=0 && $new_artist['status']==0 && $new_artist['active']==0) && ($newres1['community_id']!=0 && $new_community['status']==0 && $new_community['active']==0) && ($newres1['team_id']!=0 && $new_team['status']==0))
		   {
		   ?>
		   
		   <li class="active">HOME</li>
		   <li><a href="profileedit_media.php">Media</a></li>
		   <li><a href="profileedit_artist.php">ARTIST</a></li>
		   <!--<li><a href="profileedit_community.php">COMPANY</a></li>-->
		   <li><a href="profileedit_community.php">COMPANY</a></li>
		   <li><a href="profileedit_team.php">TEAM</a></li>
           <!--<li><a href="profileedit_support.php">SUPPORT</a></li>-->
		   
		   <?php
		   }
		   elseif(($newres1['artist_id']!=0 && $new_artist['status']==0 && $new_artist['active']==0) && ($newres1['community_id']!=0 && $new_community['status']==0 && $new_community['active']==0))
		   {
		   ?>
		   <li class="active">HOME</li>	
		   <li><a href="profileedit_media.php">Media</a></li>		   
		   <li><a href="profileedit_artist.php">ARTIST</a></li>
		   <!--<li><a href="profileedit_community.php">COMPANY</a></li>-->
		   <li><a href="profileedit_community.php">COMPANY</a></li>
           <!--<li><a href="profileedit_support.php">SUPPORT</a></li>-->
		   <?php
		   }
		   elseif(($newres1['artist_id']!=0 && $new_artist['status']==0 && $new_artist['active']==0) && ($newres1['team_id']!=0 && $new_team['status']==0))
		   {
		   ?>
			<li class="active">HOME</li>	
			<li><a href="profileedit_media.php">Media</a></li>
		    <li><a href="profileedit_artist.php">ARTIST</a></li>
			<li><a href="profileedit_team.php">TEAM</a></li>
            <!--<li><a href="profileedit_support.php">SUPPORT</a></li>-->
		   <?php
		   }
		   elseif(($newres1['community_id']!=0 && $new_community['status']==0 && $new_community['active']==0) && ($newres1['team_id']!=0 && $new_team['status']==0))
		   {
		   ?>
		   <li class="active">HOME</li>
		   <li><a href="profileedit_media.php">Media</a></li>
		   <!--<li><a href="profileedit_community.php">COMPANY</a></li>-->
		   <li><a href="profileedit_community.php">COMPANY</a></li>
		   <li><a href="profileedit_team.php">TEAM</a></li>
           <!--<li><a href="profileedit_support.php">SUPPORT</a></li>-->
		   <?php
		   }
		   elseif($newres1['artist_id']!=0 && $new_artist['status']==0 && $new_artist['active']==0)
		   {?>
		    <li class="active">HOME</li>
			<li><a href="profileedit_media.php">Media</a></li>			
			<li><a href="profileedit_artist.php">ARTIST</a></li>
            <!--<li><a href="profileedit_support.php">SUPPORT</a></li>-->
		   <?php
		   }
		   elseif($newres1['member_id']!=0)
		   {
		   ?>
		   <?php
		   }
		   elseif($newres1['community_id']!=0 && $new_community['status']==0 && $new_community['active']==0)
		   {
		   ?>
		    <li class="active">HOME</li>	
			<li><a href="profileedit_media.php">Media</a></li>
			<!--<li>COMPANY</a></li>-->
			<li><a href="profileedit_community.php">COMPANY</a></li>
            <!--<li><a href="profileedit_support.php">SUPPORT</a></li>-->
		   <?php
		   }
		   elseif($newres1['team_id']!=0 && $new_team['status']==0)
		   {
		   ?>
		    <li class="active">HOME</li>
			<li><a href="profileedit_media.php">Media</a></li>
			<li><a href="profileedit_team.php">TEAM</a></li>
            <!--<li><a href="profileedit_support.php">SUPPORT</a></li>-->
`		   <?php
		   }
		   else
		   {
		   ?>
			 <li class="active">HOME</li>		
            <!--<li><a href="profileedit_support.php">SUPPORT</a></li>-->
		   <?php
		   }
			?>
          </ul>
      </div>
	<script type="text/javascript" src="/galleryfiles/jqgal.js"></script>
	<div id="personalTab">
		<div id="subNavigation" class="tabs">
			<ul class="nav">
			<!--<li><a href="#testpage" ></a></li>
			<li><a href="#profile"  ></a></li>-->
			
			<?php
				if(($newres1['artist_id']!=0 && $new_artist['status']==0 && $new_artist['active']==0) || ($newres1['community_id']!=0 && $new_community['status']==0 && $new_community['active']==0))
				{
			?>
					<li><a href="profileedit.php" >Addressbook</a></li>
			<?php
				}
				$check_become_mem = 0;
				$purify_member_list = $member_obj->select_purify_member();
				if($purify_member_list!="" && $purify_member_list!=NULL)
				{
					if(mysql_num_rows($purify_member_list)>0)
					{
						while($row = mysql_fetch_assoc($purify_member_list))
						{
							if($row['lifetime']==1)
							{
								$check_become_mem = 1;
							}
						}
					}
				}
			?>
			<li><a href="<?php if($check_become_mem==1){ echo "mynew_fancy.php"; }else { echo "#Become_a_member"; }?>" class="<?php if($check_become_mem==1){ echo "fancybox fancybox.ajax"; }?>" <?php if($check_become_mem==1){ ?> id="become_member_fancy_box"<?php } ?>>Become A Member </a></li>
			<li><a href="profileedit.php#events">Events</a></li>
			<?php
				if(($newres1['artist_id']!=0 && $new_artist['status']==0 && $new_artist['active']==0) || ($newres1['community_id']!=0 && $new_community['status']==0 && $new_community['active']==0))
				{
			?>
					<li><a href="profileedit.php#friends">Friends</a></li>
			<?php
				}
			?>
			<li><a href="profileedit.php#general">General Info </a></li>
			<?php
				if(($newres1['artist_id']!=0 && $new_artist['status']==0 && $new_artist['active']==0) || ($newres1['community_id']!=0 && $new_community['status']==0 && $new_community['active']==0))
				{
			?>
					<li><a href="compose.php">Mail</a></li>
			<?php
				}
			?>
			<li><a href="profileedit.php#mymemberships">My Memberships</a></li>
			<!--<li><a href="#profile">News Feeds</a></li>-->
			<?php
				if(($newres1['artist_id']!=0 && $new_artist['status']==0 && $new_artist['active']==0) || ($newres1['community_id']!=0 && $new_community['status']==0 && $new_community['active']==0))
				{
			?>
					<li><a href="profileedit.php#permissions">Permissions</a></li>
			<?php
				}
			?>
			<!--<li><a href="#points">Points</a></li>-->
			<li><a href="profileedit.php#registration">Registration</a></li>
			<li><a href="profileedit.php#social_connection">Social Connect</a></li>
			<li><a href="profileedit.php#statistics">Statistics</a></li>
			<!--<li><a href="#services">Services</a></li>-->
			<li><a href="profileedit.php#subscrriptions">Subscriptions</a></li>
			<!--<li><a href="#sales_distribution">Sales Profile</a></li>-->
			<li><a href="profileedit.php#chng_pass">Change Password</a></li>
			<li style="display:none;"><a href="#addcontact">new</a></li>
			<li style="display:none;"><a href="#addmailchimp">new</a></li>
			<li style="display:none;"><a href="#addressbook">back</a></li>
			<li style="display:none;"><a href="#addsalecards">cards</a></li>
			<?php
				if((($newres1['artist_id']!=0 && $new_artist['status']==0 && $new_artist['active']==0) || ($newres1['community_id']!=0 && $new_community['status']==0 && $new_community['active']==0)) && $member_var!=0)
				{
			?>
					<li><a href="profileedit.php#transactions">Transactions</a></li>
			<?php
				}
			?>
			<li style="display:none;"><a href="#welcome"></a></li>
		  </ul>
		</div>
		<div id="list_height_incr" class="list-wrap" style="margin-bottom: 30px; height: 697px;">
<div class="subTabs" id="profile">
				<?php
					if(($newres1['artist_id']!=0 && $new_artist['status']==0 && $new_artist['active']==0) || ($newres1['community_id']!=0 && $new_community['status']==0 && $new_community['active']==0))
					{
				?>
						<input type="button" id="toggle_div" value="Add Post ( + )" class="register" style="background-color: #000000; border: 0 none; color: #FFFFFF; font-family: 'futura-pt',sans-serif; font-size: 0.825em; font-weight: bold; padding: 6px 10px;text-decoration: none; border-radius:5px; -webkit-border-radius:5px; -moz-border-radius:5px; -ms-border-radius:5px; -o-border-radius:5px;"/>
						
						<input type="button" id="toggle_my_div" value="My Posts ( + )" class="register" style="background-color: #000000; border: 0 none; color: #FFFFFF; font-family: 'futura-pt',sans-serif; font-size: 0.825em; font-weight: bold; padding: 6px 10px;text-decoration: none; border-radius:5px; -webkit-border-radius:5px; -moz-border-radius:5px; -ms-border-radius:5px; -o-border-radius:5px;"/>
				<?php
					}
				?>
					<div id="toogles" style="display:none;">
						<h1>News Post</h1>
						<div id="actualContent">
							<form method="POST" action="insertpost.php" onSubmit="return validate_post_first()" id="news_first_form">
								<div class="fieldCont">
									<div class="fieldTitle" title="Select a topic to post about.">Topic</div>
									<select title="Select a topic to post about." class="dropdown" id="post_list_first" name="post_list_first">
										<option value="images_post">Images</option>
										<option value="songs_post">Songs</option>
										<option value="videos_post">Videos</option>
										<option value="events_post">Events</option>
										<option value="projects_post">Projects</option>
									</select>
								</div>
								
								<div id="loader_first" style="display:none;">
									<img src="images/ajax_loader_large.gif" style="height: 25px; position: relative; right: 285px; top: 38px; width: 25px;">
								</div>
								
								<div class="fieldCont" id="fieldCont_media_first">
								</div>
								
								<div class="fieldCont">
									<textarea class="jquery_ckeditor" id="description_post_first" name="description_post_first"></textarea>
								</div>
								
								<div class="fieldContainer">
									<div class="rightSide">
										<a href="#newsfeeds" onmouseover="mopen('m_post_first')" onmouseout="mclosetime()">
										<input id="allow_to_first" name="allow_to_first" type="button" value="All" style="border-radius:5px; -webkit-border-radius:5px; -moz-border-radius:5px; -ms-border-radius:5px; -o-border-radius:5px; background:black; border: medium none;color: white;font-family: futura-pt,sans-serif;font-size: 0.825em;font-weight: bold;padding: 8px 18px;" /></a>
										<div style="visibility:hidden; border: 1px solid #777777; width: 150px; text-decoration:none; height:56px;" id="m_post_first" class="popUp" onmouseover="mcancelclosetime()" onmouseout="mclosetime()">
											<ul style="list-style: none outside none;">
												<li><a id="1_post" style="cursor:pointer;" onclick="allowpost_first('1_post')">Fans</a></li>
												<li><a id="2_post" style="cursor:pointer;" onclick="allowpost_first('2_post')">Friends</a></li>
												<li><a id="3_post" style="cursor:pointer;" onclick="allowpost_first('3_post')">Subscribers</a></li>
											</ul>
										</div>
									</div>
								</div>		
								
								<div class="fieldCont">
									<div class="fieldTitle"></div>
									<input type="hidden" id="allowh_post_first" name="allowh_post_first" value="All" />
									<input type="submit" value="Post" class="register" style="position:relative;float:right;left:112px;border-radius:5px; -webkit-border-radius:5px; -moz-border-radius:5px; -ms-border-radius:5px; -o-border-radius:5px;"/>
								</div>
							</form>
						</div>
					</div>
					
					<div id="toogles_my" style="display:none;">
						<h1>My Posts</h1>
						<div id="actualContent">
	<?php				
					$chk_sql = $sql_feeds->get_log_info($_SESSION['login_email']);
								
					if($chk_sql!="" && $chk_sql!=NULL)
					{
					$get_loged = $sql_feeds->myposts($_SESSION['login_id']);
					if($get_loged!="" && $get_loged!=NULL)
					{
					if(mysql_num_rows($get_loged)>0)
					{
						while($mydata = mysql_fetch_assoc($get_loged))
						{
							$gens = $sql_feeds->get_log_ids($mydata['general_user_id']);
		?>														
							<div class="storyCont">
								<div class="leftSide">
									<div class="avatar">
		<?php
										if($mydata['profile_type']=="" && $mydata['media']=="")
										{																					
		?>
											<img src="<?php echo $s3->getNewURI_otheruser("generalproimage",$mydata['general_user_id']).$gens['image_name'];
										
										?>" />
										<?php
										}
										else
										{
											$art = 0;
														
											if($gens['artist_id']!=0)
											{
												$sql_art = mysql_query("SELECT * FROM general_artist WHERE artist_id='".$gens['artist_id']."' AND status=0 AND active=0 AND del_status=0");
												if($sql_art!="" && $sql_art!=NULL)
												{
													if(mysql_num_rows($sql_art)>0)
													{
														$ros = mysql_fetch_assoc($sql_art);
														$art = 1;
													}
												}
											}
														
											$com = 0;
											if($gens['community_id']!=0)
											{
												$sql_com = mysql_query("SELECT * FROM general_community WHERE community_id='".$gens['community_id']."' AND status=0 AND active=0 AND del_status=0");
												if($sql_com!="" && $sql_com!=NULL)
												{
													if(mysql_num_rows($sql_com)>0)
													{
														$ros_com = mysql_fetch_assoc($sql_com);
														$com = 1;
													}
												}
											}
											
														
											if($art==1)
											{
												$get_news_general_user_id = $uk -> get_general_user_idby_artistid($ros['artist_id']);
			?>
												<img width="50" height="50" src="<?php
												if($ros['image_name']!="" && !empty($ros['image_name']))
												{
													echo $s3->getNewURI_otheruser("artjcropprofile",$get_news_general_user_id).$ros['image_name']; 
												}
												else
												{
													echo "http://artjcropprofile.s3.amazonaws.com/Noimage.png";
												}
												?>" />
			<?php
											}
											elseif($com==1)
											{
												$get_news_general_user_id = $uk -> get_general_user_idby_communityid($ros_com['community_id']);
			?>
												<img width="50" height="50" src="<?php
														
												if($ros_com['image_name']!="" && !empty($ros_com['image_name']))
												{
													echo $s3->getNewURI_otheruser("comjcropprofile",$get_news_general_user_id).$ros_com['image_name']; 
												}
												else
												{
													echo "http://comjcropprofile.s3.amazonaws.com/Noimage.png";
												}
												?>" />
			<?php
											}
										}
		?>
									</div>
		<?php						//echo "table=".$reg_table_media_type;
									//echo $ans_audio_feeds[$audio_fed_lo]['id'];
		?>
									<div class="storyContainer">
										<div class="storyTitle">
		<?php
										if($mydata['profile_type']=="" && $mydata['media']=="")
										{
		?>
											<span class="userName"><a href=""><?php echo $gens['fname'].' '.$gens['lname']; ?></a></span>
		<?php
										}
										else
										{
		?>
											<span class="userName"><a href=""><?php 
											if($art==1)
											{
												if($ros['profile_url']!="")
												{
													echo "<a href=".$ros['profile_url'].">".$ros['name']."</a>";
												}
												else
												{
													echo $ros['name'];
												}
											}
											elseif($com==1)
											{
												if($ros_com['profile_url']!="")
												{
													echo "<a href=".$ros['profile_url'].">".$ros_com['name']."</a>";
												}
												else
												{
													echo $ros_com['name'];
												}
											} ?></a></span> <?php 
										}
		?>
										</div>
		<?php
										if($mydata['profile_type']!="" && $mydata['media']!="")
										{
											if($mydata['profile_type']=='songs_post' || $mydata['profile_type']=='videos_post' || $mydata['profile_type']=='images_post')
											{
												$exp_meds = explode('~',$mydata['media']);
												
												if($exp_meds[1]=='reg_med')
												{
												
													$sql_meds = $sql_feeds->get_mediaSelected($exp_meds[0]);
												}
												elseif(($exp_meds[1]=='reg_art' || $exp_meds[1]=='reg_com') && $mydata['profile_type']=='images_post')
												{
													if($exp_meds[1]=='reg_art')
													{
														$reg_table_type = 'general_artist_gallery_list';
													}
													elseif($exp_meds[1]=='reg_com')
													{
														$reg_table_type = 'general_community_gallery_list';
													}
													
													$sql_meds = $sql_feeds->get_galleryData($reg_table_type,$exp_meds[0]);
												}
												elseif(($exp_meds[1]=='reg_art' || $exp_meds[1]=='reg_com') && $mydata=='videos_post')
												{
													if($exp_meds[1]=='reg_art')
													{
														$reg_table_type = 'general_artist_video';
													}
													elseif($exp_meds[1]=='reg_com')
													{
														$reg_table_type = 'general_community_video';
													}
																	
													$sql_meds = $sql_feeds->get_MediaReg($reg_table_type,$exp_meds[0]);
												}
												elseif(($exp_meds[1]=='reg_art' || $exp_meds[1]=='reg_com') && $mydata=='songs_post')
												{
													if($exp_meds[1]=='reg_art')
													{
														$reg_table_type = 'general_artist_audio';
													}
													elseif($exp_meds[1]=='reg_com')
													{
														$reg_table_type = 'general_community_audio';
													}
																	
													$sql_meds = $sql_feeds->get_MediaReg($reg_table_type,$exp_meds[0]);
												}	
											}
											elseif($mydata['profile_type']=='projects_post' || $mydata['profile_type']=='events_post')
											{
												$exp_meds = explode('~',$mydata['media']);
												$sql_meds = $sql_feeds->featured_pro_eve($exp_meds[1],$exp_meds[0]);
											}
		?>
											<div class="activityCont">
												<div class="sideL">
													<div class="storyImage">
		<?php
														if( (isset($sql_meds['media_type']) && $sql_meds['media_type']==115) || (isset($ans_all_posts[$po_all]['profile_type']) && $ans_all_posts[$po_all]['profile_type']=='videos_post') )
														{
															if(isset($sql_meds['media_type']) && $sql_meds['media_type']==115)
															{
																$tables = 'media_video';
																$get_video_image = $sql_feeds->getvideoimage($tables,$sql_meds['id']);
																
																if($get_video_image[1]=='youtube')
																{
																	$vid_pros_edit_img = "http://img.youtube.com/vi/".$get_video_image[0]."/default.jpg";
																}
																else
																{
																	if(!empty($get_video_image[0]) && $get_video_image[0]!=null){
																		$video_vim = $vimeo->call('vimeo.videos.getInfo', array('video_id' => $get_video_image[0]));
																		$vid_pros_edit_img = $video_vim->video[0]->thumbnails->thumbnail[1]->_content;
																	}else{
																		$vid_pros_edit_img ="";
																	}
																}
																
																?>
																<a onclick="showPlayer('v','<?php echo $sql_meds['id']; ?>','playlist');" href="javascript:void(0);">
																	<img width="90" height="90" src="<?php echo $vid_pros_edit_img;  ?>" />
																</a>
																<?php
															}
															else if($ans_all_posts[$po_all]['profile_type']=='videos_post' && $exp_meds[1]=='reg_art')
															{
																$tables = 'general_artist_video';
																$get_video_image = $sql_feeds->getvideoimage($tables,$sql_meds['id']);
																if($get_video_image[1]=='youtube')
																{
																	$vid_pros_edit_img = "http://img.youtube.com/vi/".$get_video_image[0]."/default.jpg";
																}
																else
																{
																	$video_vim = $vimeo->call('vimeo.videos.getInfo', array('video_id' => $get_video_image[0]));
																	$vid_pros_edit_img = $video_vim->video[0]->thumbnails->thumbnail[1]->_content;
																}
																?>
																<a onclick="showPlayer('v','<?php echo $sql_meds['id']; ?>','playlist');" href="javascript:void(0);">
																	<img width="90" height="90" src="<?php echo $vid_pros_edit_img; ?>" />
																</a>
																<?php
															}
															elseif($ans_all_posts[$po_all]['profile_type']=='videos_post' && $exp_meds[1]=='reg_com')
															{
																$tables = 'general_community_video';
																$get_video_image = $sql_feeds->getvideoimage($tables,$sql_meds['id']);
																if($get_video_image[1]=='youtube')
																{
																	$vid_pros_edit_img = "http://img.youtube.com/vi/".$get_video_image[0]."/default.jpg";
																}
																else
																{
																	$video_vim = $vimeo->call('vimeo.videos.getInfo', array('video_id' => $get_video_image[0]));
																	$vid_pros_edit_img = $video_vim->video[0]->thumbnails->thumbnail[1]->_content;
																}
																?>
																<a onclick="showPlayer('v','<?php echo $sql_meds['id']; ?>','playlist');" href="javascript:void(0);">
																	<img width="90" height="90" src="<?php echo $vid_pros_edit_img; ?>" />
																</a>
																<?php
															}
														}
														/* elseif($sql_meds['media_type']==114)
														{
															$get_audio_image = $sql_feeds->get_song_med_img($sql_meds['id']);
															
															if($get_audio_image!="" && !empty($get_audio_image))
															{
																if($get_audio_image['gallery_at']!="")
																{
																	$get_audio__timage = $sql_feeds->get_song_med_t_img($get_audio_image['gallery_at'],$get_audio_image['gallery_id']);
																	if($get_audio_image['gallery_at']=='general_media')
																	{
														?>
																		<img src="http://medgalthumb.s3.amazonaws.com/<?php echo $get_audio__timage; ?>" width="90" height="90" />
														<?php
																	}
																}
															}
														} */
														elseif(isset($sql_meds['media_type']) && $sql_meds['media_type']==113)
														{
															$get_all_image1 = $sql_feeds->get_all_gallery_images($sql_meds['id']);
															$play_array1 = array();
															if(mysql_num_rows($get_all_image1)>0)
															{
																while($row = mysql_fetch_assoc($get_all_image1))
																{
																	$play_array1[] = $row;
																}
																$get_news_general_user_id = $uk -> get_general_user_idby_mediaid($sql_meds['id']);
																${'orgPath'.$my_po}= $s3->getNewURI_otheruser("medgallery",$get_news_general_user_id);
																${'thumbPath'.$my_po}= $s3->getNewURI_otheruser("medgalthumb",$get_news_general_user_id);
																for($l=0;$l<count($play_array1);$l++)
																{
																	$mediaSrc = $play_array1[0]['image_name'];
																	$filePath = $s3->getNewURI_otheruser("medgallery",$get_news_general_user_id).$mediaSrc;
																	
																	
																	${'galleryImagesData'.$my_po}[$galleryImagesDataIndex] = $play_array1[$l]['image_name'];
																	${'galleryImagestitle'.$my_po}[$galleryImagesDataIndex] = $play_array1[$l]['image_title'];
																	//echo ${'galleryImagesData'.$my_po}[$galleryImagesDataIndex];
																	$galleryImagesDataIndex++;
																}
																
															}
															if(isset(${'galleryImagesData'.$my_po}))
															{
																${'galleryImagesData'.$my_po} = implode(",", ${'galleryImagesData'.$my_po});
																${'galleryImagestitle'.$my_po} = implode(",", ${'galleryImagestitle'.$my_po});
																																								}
															
															$get_gal_image = $sql_feeds->gallery_meds($sql_meds['id']);
															if($get_gal_image!="" && !empty($get_gal_image))
															{
																if($get_gal_image['profile_image']!="")
																{
														?>
																<a href="javascript:void(0);" onclick="checkload('<?php echo $mediaSrc;?>','<?php echo ${'galleryImagesData'.$my_po}; ?>','<?php echo ${'thumbPath'.$my_po}; ?>','<?php echo ${'orgPath'.$my_po}; ?>','<?php echo ${'galleryImagestitle'.$my_po}; ?>','<?php echo $get_gallery['id'];?>')">
																	<img src="<?php echo $get_gal_image['profile_image']; ?>" width="90" height="90" />
																</a>	
														<?php
																}
																else
																{
														?>
																<a href="javascript:void(0);" onclick="checkload('<?php echo $mediaSrc;?>','<?php echo ${'galleryImagesData'.$my_po}; ?>','<?php echo ${'thumbPath'.$my_po}; ?>','<?php echo ${'orgPath'.$my_po}; ?>','<?php echo ${'galleryImagestitle'.$my_po}; ?>','<?php echo $get_gallery['id'];?>')">
																	<img width="90" height="90" src="http://comjcropprofile.s3.amazonaws.com/<?php echo "Noimage.png";?>" />
																</a>	
																<?php	
																}
															}
															else
															{
														?>
															<a href="javascript:void(0);" onclick="checkload('<?php echo $mediaSrc;?>','<?php echo ${'galleryImagesData'.$my_po}; ?>','<?php echo ${'thumbPath'.$my_po}; ?>','<?php echo ${'orgPath'.$my_po}; ?>','<?php echo ${'galleryImagestitle'.$my_po}; ?>','<?php echo $get_gallery['id'];?>')">
																<img width="90" height="90" src="http://comjcropprofile.s3.amazonaws.com/<?php echo "Noimage.png";?>" />
															</a>	
															<?php	
														
															}
															$my_po = $my_po + 1;
														}
														elseif($sql_meds['media_type']==116)
														{
															$get_ch_image = $sql_feeds->channel_meds($sql_meds['id']);
															if($get_ch_image!="" && !empty($get_ch_image))
															{
																if($get_ch_image['profile_image']!="")
																{
														?>
																	<img src="<?php echo $get_ch_image['profile_image']; ?>" width="90" height="90" />
														<?php
																}
																else
																{
														?>
																	<img width="90" height="90" src="http://comjcropprofile.s3.amazonaws.com/<?php echo "Noimage.png";?>" />
																<?php	
																}
															}
															else
															{
														?>
																<img width="90" height="90" src="http://comjcropprofile.s3.amazonaws.com/<?php echo "Noimage.png";?>" />
															<?php	
															}
														}
														elseif($exp_meds[1]=='reg_art' && $ans_all_posts[$po_all]['profile_type']=='images_post')
														{
															$tables = 'artist';
															$imgs = $sql_feeds->reg_gal($tables,$sql_meds['gallery_id']);
															
															if($imgs!="" && !empty($imgs))
															{
																if($imgs['image_name']!="")
																{
															?>
																	<img width="90" height="90" src="http://reggalthumb.s3.amazonaws.com/<?php echo $imgs['image_name'];?>" />
																<?php	
																}
																else
																{
															?>
																	<img width="90" height="90" src="http://comjcropprofile.s3.amazonaws.com/<?php echo "Noimage.png";?>" />
																<?php	
																}
															}
															else
															{
															?>
																<img width="90" height="90" src="http://comjcropprofile.s3.amazonaws.com/<?php echo "Noimage.png";?>" />
															<?php	
															}
														}
														elseif($exp_meds[1]=='reg_com' && $ans_all_posts[$po_all]['profile_type']=='images_post')
														{
															$tables = 'community';
															$imgs = $sql_feeds->reg_gal($tables,$sql_meds['gallery_id']);
															
															if($imgs!="" && !empty($imgs))
															{
																if($imgs['image_name']!="")
																{
															?>
																	<img width="90" height="90" src="http://reggalthumb.s3.amazonaws.com/<?php echo $imgs['image_name'];?>" />
																<?php	
																}
																else
																{
															?>
																	<img width="90" height="90" src="http://comjcropprofile.s3.amazonaws.com/<?php echo "Noimage.png";?>" />
																<?php
																}
															}
															else
															{
															?>
																<img width="90" height="90" src="http://comjcropprofile.s3.amazonaws.com/<?php echo "Noimage.png";?>" />
															<?php	
															}
														}elseif($mydata['profile_type']=='projects_post' || $mydata['profile_type']=='events_post')
														{
															if($sql_meds['profile_url']!="")
															{
														?>
															<a href="<?php echo $sql_meds['profile_url']; ?>"><img width="90" height="90" src="<?php if($sql_meds['image_name']!="") { echo $sql_meds['image_name']; } else { echo "http://comjcropprofile.s3.amazonaws.com/Noimage.png"; }?>" /></a>
														<?php
															}
															else
															{
															?>
															<img width="90" height="90" src="<?php if($sql_meds['image_name']!="") { echo $sql_meds['image_name']; } else { echo "http://comjcropprofile.s3.amazonaws.com/Noimage.png"; }?>" />
														<?php
															}
														}
		?>
													</div>
													<?php
													if(isset($sql_meds['media_type']) && $sql_meds['media_type']!=113)
													{
													?>								
													<div class="player">
													<?php 
														if(isset($sql_meds['media_type']) && $sql_meds['media_type']==115)
														{
														?>
															<img src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" onclick="showPlayer('v','<?php echo $sql_meds['id'];?>','playlist');">
															<img src="images/profile/add.gif" onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'" onclick="showPlayer('v','<?php echo $sql_meds['id'];?>','addvi');">
															<?php
															if( (isset($sql_meds['sharing_preference']) && $sql_meds['sharing_preference']==2) || (isset($sql_meds['sharing_preference']) && $sql_meds['sharing_preference']==5) ){
															?>
															<!--<a href="javascript:void(0);" onclick="download_directly_song('<?php echo $sql_meds['id'];?>','<?php echo str_replace("'","\'",$sql_meds['title']);?>','<?php echo str_replace("'","\'",$sql_meds['creator']);?>','<?php echo str_replace("'","\'",$sql_meds['from']);?>')"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>-->
														<?php
															}else{
															?>
															<!--<img style="cursor: default;" src="images/profile/download-over.gif">-->
														<?php
															}
														}/*else if($sql_meds['media_type']==113)
														{
														?>
															<img src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'"></a>
															<img src="images/profile/add.gif" onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'">
															<img style="cursor: default;" src="images/profile/download-over.gif">
														<?php
														}*/else if(isset($sql_meds['media_type']) && $sql_meds['media_type']==114)
														{
														?>
															<img src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" onclick="showPlayer('a','<?php echo $sql_meds['id'];?>','play');">
															<img src="images/profile/add.gif" onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'" onclick="showPlayer('a','<?php echo $sql_meds['id'];?>','add');">
														<?php
															if($sql_meds['sharing_preference']==2 || $sql_meds['sharing_preference']==5){
															?>
															<!--<a href="javascript:void(0);" onclick="download_directly_song('<?php echo $sql_meds['id'];?>','<?php echo str_replace("'","\'",$sql_meds['title']);?>','<?php echo str_replace("'","\'",$sql_meds['creator']);?>','<?php echo str_replace("'","\'",$sql_meds['from']);?>')"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>-->
														<?php
															}else{
															?>
															<!--<img style="cursor: default;" src="images/profile/download-over.gif">-->
														<?php
															}
														}
														else if($sql_meds['featured_media']!="" || $sql_meds['featured_media']!="nodisplay"){
															if($sql_meds['featured_media'] == "Song" && $sql_meds['media_id']!= "0") {
															?>
																<img src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" onclick="showPlayer('a','<?php echo $sql_meds['media_id'];?>','play');" />
																<img src="images/profile/add.gif" onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'" onclick="showPlayer('a','<?php echo $sql_meds['media_id'];?>','add');" />
																<?php
																	$get_media_feature_info = $sql_feeds ->get_media_details($sql_meds['media_id']);
																	if($get_media_feature_info['sharing_preference']==2 || $get_media_feature_info['sharing_preference']==5){
																	?>
																	<!--<a href="javascript:void(0);" onclick="download_directly_song('<?php echo $get_media_feature_info['id'];?>','<?php echo str_replace("'","\'",$get_media_feature_info['title']);?>','<?php echo str_replace("'","\'",$get_media_feature_info['creator']);?>','<?php echo str_replace("'","\'",$get_media_feature_info['from']);?>')"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>-->
																<?php
																	}else{
																	?>
																	<!--<img style="cursor: default;" src="images/profile/download-over.gif">-->
																<?php
																	}
																}
															if($sql_meds['featured_media'] == "Gallery" && $sql_meds['media_id']!= "0"){
																$get_all_feature_image3 = $sql_feeds->get_all_gallery_images($sql_meds['media_id']);
																if($get_all_feature_image3!="" && $get_all_feature_image3!=NULL)
																{
																	if(mysql_num_rows($get_all_feature_image3)>0)
																	{
																		$galleryImagesData3 ="";
																		$galleryImagestitle3 ="";
																		$mediaSrc3 ="";
																		$get_news_general_user_id = $uk -> get_general_user_idby_mediaid($sql_meds['media_id']);
																		while($row = mysql_fetch_assoc($get_all_feature_image3))
																		{
																			$mediaSrc3 = $row[0]['image_name'];
																			$filePath = $s3->getNewURI_otheruser("medgallery",$get_news_general_user_id).$mediaSrc3;
																			if($row['image_name']!=""){
																				$galleryImagesData3 = $galleryImagesData3 .",". $row['image_name'];
																				$galleryImagestitle3 = $galleryImagestitle3 .",".$row['image_title'];
																			}
																		}
																		$orgPath3 = $s3->getNewURI_otheruser("medgallery",$get_news_general_user_id);
																		$thumbPath3 = $s3->getNewURI_otheruser("medgalthumb",$get_news_general_user_id);
																	}
																}
															?>
																<a href="javascript:void(0);" onclick="checkload('<?php echo $mediaSrc3;?>','<?php echo $galleryImagesData3; ?>','<?php echo $thumbPath3; ?>','<?php echo $orgPath3; ?>','<?php echo $galleryImagestitle3; ?>','<?php echo $get_gallery['id'];?>')">
																<img src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'"></a>
																<img src="images/profile/add.gif" onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'">
																<?php
																$get_media_feature_info = $sql_feeds ->get_media_details($sql_meds['media_id']);
																	if($get_media_feature_info['sharing_preference']==2 || $get_media_feature_info['sharing_preference']==5){
																	?>
																	<!--<a href="javascript:void(0);" onclick="download_directly_song('<?php echo $get_media_feature_info['id'];?>','<?php echo str_replace("'","\'",$get_media_feature_info['title']);?>','<?php echo str_replace("'","\'",$get_media_feature_info['creator']);?>','<?php echo str_replace("'","\'",$get_media_feature_info['from']);?>')"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>-->
																<?php
																	}else{
																	?>
																	<!--<img style="cursor: default;" src="images/profile/download-over.gif">-->
																<?php
																	}
															}if($sql_meds['featured_media'] == "Video" && $sql_meds['media_id']!= "0"){
															?>
																<img src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" onclick="showPlayer('v','<?php echo $sql_meds['media_id'];?>','playlist');"/>
																<img src="images/profile/add.gif" onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'" onclick="showPlayer('v','<?php echo $sql_meds['media_id'];?>','addvi');"/>
																<?php
																$get_media_feature_info = $sql_feeds ->get_media_details($sql_meds['media_id']);
																	if($get_media_feature_info['sharing_preference']==2 || $get_media_feature_info['sharing_preference']==5){
																	?>
																	<!--<a href="javascript:void(0);" onclick="download_directly_song('<?php echo $get_media_feature_info['id'];?>','<?php echo str_replace("'","\'",$get_media_feature_info['title']);?>','<?php echo str_replace("'","\'",$get_media_feature_info['creator']);?>','<?php echo str_replace("'","\'",$get_media_feature_info['from']);?>')"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>-->
																<?php
																	}else{
																	?>
																	<!--<img style="cursor: default;" src="images/profile/download-over.gif">-->
																<?php
																	}
															}
														}
														else{
														?>
															<img src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'">
															<img src="images/profile/add.gif" onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'">
															<!--<img style="cursor: default;" src="images/profile/download-over.gif">-->
														<?php
														}
													?>
														
													</div>	
													<?php
													}
													?>
												</div>
												
												<div class="sideR">
													<div class="activityTitle">
														
		<?php
													if($exp_meds[1]=='reg_med' || $mydata['profile_type']=='projects_post' || $mydata['profile_type']=='events_post')
													{
														if($mydata['profile_type']=='projects_post' || $mydata['profile_type']=='events_post')
														{
															if($sql_meds['profile_url']!="")
															{
																if(isset($sql_meds['media_type']) && $sql_meds['media_type']==113){
																	$get_all_image1 = $sql_feeds->get_all_gallery_images($sql_meds['id']);
																	$play_array1 = array();
																	if($get_all_image1!="" && $get_all_image1!=NULL)
																	{
																		if(mysql_num_rows($get_all_image1)>0)
																		{
																			while($row = mysql_fetch_assoc($get_all_image1))
																			{
																				$play_array1[] = $row;
																			}
																			$get_news_general_user_id = $uk -> get_general_user_idby_mediaid($sql_meds['id']);
																			${'orgPath'.$my_po}= $s3->getNewURI_otheruser("medgallery",$get_news_general_user_id);
																			${'thumbPath'.$my_po} = $s3->getNewURI_otheruser("medgalthumb",$get_news_general_user_id);
																			for($l=0;$l<count($play_array1);$l++)
																			{
																				$mediaSrc = $play_array1[0]['image_name'];
																				$filePath = $s3->getNewURI_otheruser("medgallery",$get_news_general_user_id).$mediaSrc;
																				
																				
																				${'galleryImagesData'.$my_po}[$galleryImagesDataIndex] = $play_array1[$l]['image_name'];
																				${'galleryImagestitle'.$my_po}[$galleryImagesDataIndex] = $play_array1[$l]['image_title'];
																				//echo ${'galleryImagesData'.$my_po}[$galleryImagesDataIndex];
																				$galleryImagesDataIndex++;
																			}
																			
																		}
																	}
																	if(isset(${'galleryImagesData'.$my_po}))
																	{
																		${'galleryImagesData'.$my_po} = implode(",", ${'galleryImagesData'.$my_po});
																		${'galleryImagestitle'.$my_po} = implode(",", ${'galleryImagestitle'.$my_po});
																	}
																	echo "<a href=javascript:void(0) onclick=checkload('". $mediaSrc."','". ${'galleryImagesData'.$my_po}."','". ${'thumbPath'.$my_po}."','". ${'orgPath'.$my_po}."','". ${'galleryImagestitle'.$my_po}."','". $get_gallery['id']."')>".$sql_meds['title']."</a>";
																	$my_po = $my_po + 1;
																}else{
																	echo "<a href=".$sql_meds['profile_url'].">".$sql_meds['title']."</a>";
																}
															}
															else
															{
																echo $sql_meds['title'];
															}
														}
														elseif($exp_meds[1]=='reg_med')
														{
															if($sql_meds['media_type']==114)
															{
																$sql_me = mysql_query("SELECT * FROM media_songs WHERE media_id='".$sql_meds['id']."'");
																if($sql_me!="" && $sql_me!=NULL)
																{
																$ans_me = mysql_fetch_assoc($sql_me);
																
															$ecehos = '<a style="text-decoration: none; cursor: text;" href="javascript:void(0)"> '.$ans_me['track'].' </a>';
															$ecehos .= '<a style="text-decoration: none; cursor: text;" href="javascript:void(0)">'.$sql_meds['title'].'</a>';
															
																if($sql_meds['creator']!="" && !empty($sql_meds['creator']))
																{
																	$create_c = strpos($sql_meds['creator'],'(');
																	if(!empty($create_c) && $create_c!=Null){ $end = substr($sql_meds['creator'],0,$create_c); }
																	else{ $end = $sql_meds['creator']; }
																	$exp_cr = explode('|',$sql_meds['creator_info']);
																	
																	if($exp_cr[0]=='general_artist')
																	{
																		$sql_c_ar = mysql_query("SELECT * FROM general_artist WHERE artist_id='".$exp_cr[1]."'");
																	}
																	if($exp_cr[0]=='general_community')
																	{
																		$sql_c_ar = mysql_query("SELECT * FROM general_community WHERE community_id='".$exp_cr[1]."'");
																	}
																	if($exp_cr[0]=='artist_project' || $exp_cr[0]=='community_project' || $exp_cr[0]=='community_event' || $exp_cr[0]=='artist_event')
																	{
																		$sql_c_ar = mysql_query("SELECT * FROM $exp_cr[0] WHERE id='".$exp_cr[1]."'");
																	}
																	
																	
																	if(isset($sql_c_ar))
																	{
																		if($sql_c_ar!="" && $sql_c_ar!=NULL)
																{
																		if(mysql_num_rows($sql_c_ar)>0)
																		{
																			$ans_c_ar = mysql_fetch_assoc($sql_c_ar);
																			if($ans_c_ar['profile_url']!="" && !empty($ans_c_ar['profile_url']))
																			{
																				$ecehos .= " By  <a href='/".$ans_c_ar['profile_url']."'><b>".$end."</b></a>  ";
																			}
																			else
																			{
																				$ecehos .= " By  <b>".$end."</b>  ";
																			}
																		}
																		}
																		else
																		{
																			$ecehos .= " By  <b>".$end."</b>  ";
																		}
																	}
																	else
																	{
																		
																		$ecehos .= " By  <b>".$end."</b>  ";
																	}
																}
																if($sql_meds['from']!="" && !empty($sql_meds['from']))
																{
																	$create_f = strpos($sql_meds['from'],'(');
																	if(!empty($create_f) && $create_f!=Null){ $end_f = substr($sql_meds['from'],0,$create_f); }
																	else{ $end_f = $sql_meds['from']; }
																	
																	
																	$exp_fr = explode('|',$sql_meds['from_info']);
																	if($exp_fr[0]=='general_artist')
																	{
																		$sql_f_ar = mysql_query("SELECT * FROM general_artist WHERE artist_id='".$exp_fr[1]."'");
																	}
																	if($exp_fr[0]=='general_community')
																	{
																		$sql_f_ar = mysql_query("SELECT * FROM general_community WHERE community_id='".$exp_fr[1]."'");
																	}
																	if($exp_fr[0]=='artist_project' || $exp_fr[0]=='community_project' || $exp_fr[0]=='community_event' || $exp_fr[0]=='artist_event')
																	{
																		$sql_f_ar = mysql_query("SELECT * FROM $exp_fr[0] WHERE id='".$exp_fr[1]."'");
																	}
																	
																	if(isset($sql_f_ar))
																	{
																		if($sql_f_ar!="" && $sql_f_ar!=NULL)
																		{
																		if(mysql_num_rows($sql_f_ar)>0)
																		{
																			$ans_f_ar = mysql_fetch_assoc($sql_f_ar);
																			
																			if($ans_f_ar['profile_url']!="" && !empty($ans_f_ar['profile_url']))
																			{
																				$ecehos .= " From  <a href='/".$ans_f_ar['profile_url']."'><b>".$end_f."</b></a>";
																			}
																			else
																			{
																				$ecehos .= " From  <b>".$end_f."</b>";
																			}
																		}
																		else
																		{
																			$ecehos .= " From  <b>".$end_f."</b>";
																		}
																	}
																	else
																	{
																		$ecehos .= " From  <b>".$end_f."</b>";
																	}
																}
																else
																	{
																		$ecehos .= " From  <b>".$end_f."</b>";
																	}
																}
															}
															}
															if($sql_meds['media_type']!=114)
															{
																if($sql_meds['media_type']==113){
																	$get_all_image1 = $sql_feeds->get_all_gallery_images($sql_meds['id']);
																	$play_array1 = array();
																	if($get_all_image1!="" && $get_all_image1!=NULL)
																		{
																		if(mysql_num_rows($get_all_image1)>0)
																		{
																			while($row = mysql_fetch_assoc($get_all_image1))
																			{
																				$play_array1[] = $row;
																			}
																			$get_news_general_user_id = $uk -> get_general_user_idby_mediaid($sql_meds['id']);
																			${'orgPath'.$my_po} = $s3->getNewURI_otheruser("medgallery",$get_news_general_user_id);
																			${'thumbPath'.$my_po} = $s3->getNewURI_otheruser("medgalthumb",$get_news_general_user_id);
																			for($l=0;$l<count($play_array1);$l++)
																			{
																				$mediaSrc = $play_array1[0]['image_name'];
																				$filePath = $s3->getNewURI_otheruser("medgallery",$get_news_general_user_id).$mediaSrc;
																				
																				
																				${'galleryImagesData'.$my_po}[$galleryImagesDataIndex] = $play_array1[$l]['image_name'];
																				${'galleryImagestitle'.$my_po}[$galleryImagesDataIndex] = $play_array1[$l]['image_title'];
																				//echo ${'galleryImagesData'.$my_po}[$galleryImagesDataIndex];
																				$galleryImagesDataIndex++;
																			}
																			
																		}
																	}
																	if(isset(${'galleryImagesData'.$my_po}))
																	{
																		${'galleryImagesData'.$my_po} = implode(",", ${'galleryImagesData'.$my_po});
																		${'galleryImagestitle'.$my_po} = implode(",", ${'galleryImagestitle'.$my_po});
																	}
																	echo "<a href=javascript:void(0) onclick=checkload('". $mediaSrc."','". ${'galleryImagesData'.$my_po}."','". ${'thumbPath'.$my_po}."','". ${'orgPath'.$my_po}."','". ${'galleryImagestitle'.$my_po}."','". $get_gallery['id']."')>".$sql_meds['title']."</a>";
																	$my_po = $my_po + 1;
																}else{
																	if(isset($sql_meds['media_type']) && $sql_meds['media_type']==115){
																		//$ecehos = '<a href="">'.$sql_meds['title'].'</a>';
																		echo "<a href=javascript:void(0) onclick=showPlayer('v','".$sql_meds['id']."','playlist')>$sql_meds[title]</a>";
																	}
																}
																
															}
															echo $ecehos;
														}
													}
													elseif($exp_meds[1]=='reg_art' || $exp_meds[1]=='reg_com')
													{
														if($mydata['profile_type']=='images_post')
														{
															$get_all_image1 = $sql_feeds->get_all_gallery_images($sql_meds['id']);
															$play_array1 = array();
															if($get_all_image1!="" && $get_all_image1!=NULL)
																		{
																if(mysql_num_rows($get_all_image1)>0)
																{
																	while($row = mysql_fetch_assoc($get_all_image1))
																	{
																		$play_array1[] = $row;
																	}
																	$get_news_general_user_id = $uk -> get_general_user_idby_mediaid($sql_meds['id']);
																	${'orgPath'.$my_po} = $s3->getNewURI_otheruser("medgallery",$get_news_general_user_id);
																	${'thumbPath'.$my_po} = $s3->getNewURI_otheruser("medgalthumb",$get_news_general_user_id);
																	for($l=0;$l<count($play_array1);$l++)
																	{
																		$mediaSrc = $play_array1[0]['image_name'];
																		$filePath = $s3->getNewURI_otheruser("medgallery",$get_news_general_user_id).$mediaSrc;
																		
																		
																		${'galleryImagesData'.$my_po}[$galleryImagesDataIndex] = $play_array1[$l]['image_name'];
																		${'galleryImagestitle'.$my_po}[$galleryImagesDataIndex] = $play_array1[$l]['image_title'];
																		//echo ${'galleryImagesData'.$my_po}[$galleryImagesDataIndex];
																		$galleryImagesDataIndex++;
																	}
																	
																}
															}
															if(isset(${'galleryImagesData'.$my_po}))
															{
																${'galleryImagesData'.$my_po} = implode(",", ${'galleryImagesData'.$my_po});
																${'galleryImagestitle'.$my_po} = implode(",", ${'galleryImagestitle'.$my_po});
															}
															echo "<a href=javascript:void(0) onclick=checkload('". $mediaSrc."','". ${'galleryImagesData'.$my_po}."','". ${'thumbPath'.$my_po}."','". ${'orgPath'.$my_po}."','". ${'galleryImagestitle'.$my_po}."','". $get_gallery['id']."')>".$sql_meds['gallery_title']."</a>";
															$my_po = $my_po + 1;
														}
														elseif($mydata['profile_type']=='songs_post')
														{
															echo $sql_meds['audio_name'];
														}
														elseif($mydata['profile_type']=='videos_post')
														{
															//echo "<a href=javascript:void(0) onclick=showPlayer('v','".$sql_meds['id']."','playlist')>$sql_meds[video_name]</a>";
															echo $sql_meds['video_name'];
														}
													}
		?>
														
												</div>
												<div class="comment more">
												<?php
													echo $mydata['description'];
												?>
												</div>
											</div>
										</div>
		<?php
										}
		?>
									</div>
								</div>

								<div class="rightSide" style="width:41px;margin-right: 0;">
									<a href="edit_my_post.php?ed_post=<?php echo $mydata['id']; ?>" id="edit_my_post_fan<?php echo $mydata['id']; ?>" class="fancybox fancybox.iframe"><img style="height: 15px; position: relative; top: 16px;" src="images/black_edit.png" /></a>
									<a href="Unsubscribe_feeds.php?del_posts=<?php echo $mydata['id']; ?>"><img style="height: 15px; position: relative; top: 16px;" src="images/Black_Remove.png" onclick="return confirm_delposts();" /></a>
									<script type="text/javascript">
										$(document).ready(function() {
											$("#edit_my_post_fan<?php echo $mydata['id']; ?>").fancybox({
												autoSize : false,
												fitToView   : false, 
												height:"90%",
												type : 'iframe'
											});
										});
									</script>
								<!--	<a href="#" onmouseover="mopen('mm<?php echo $po_all;?>1')" onmouseout="mclosetime()"><img src="images/profile/arrow.png" onmouseover="this.src='images/profile/arrow-down.png'" onmouseout="this.src='images/profile/arrow.png'" /></a>
									<div id="mm<?php echo $po_all;?>1" class="popUp" onmouseover="mcancelclosetime()" onmouseout="mclosetime()">
										<ul>
											<li><a href="hideposts.php?id=<?php echo $ans_all_posts[$po_all]['id'];?>">Hide Feed</a></li>
											<li><a href="">Report spam</a></li>
											<li><div class="seperator"></div></li>
											<li><a href="">Unsubscribe from <?php echo $gens['fname'].' '.$gens['lname']; ?></a></li>
										</ul>
									</div>-->
								</div>
							</div>
		<?php	
						}
					}
					
					else{
						echo "You have not posted any feed yet.";
					}
				}
				else{
						echo "You have not posted any feed yet.";
					}
				}
			
			?>
			
			<STYLE>
.comment.more {
    color: #666;
}
a.morelink {
	text-decoration:none;
	outline: none;
	color: #666;
}
.morecontent span {
	display: none;
color: #666;
}
</STYLE>
<SCRIPT>
$(document).ready(function() {
	var showChar = 230;
	var ellipsestext = "...";
	var moretext = "more";
	var lesstext = "less";
	$('.more').each(function() {
		var content = $(this).html();

		var get_breaks = content.split("<br>");
		if(get_breaks.length>4){
			var stored_count =0,c="",h="";
			for(var o=0;o<get_breaks.length;o++){
				if(stored_count == 0 || stored_count<230){
					if(c==""){
						c = get_breaks[o];
					}else{
						c = c +"<br>"+ get_breaks[o];
					}
					if(get_breaks[o].length>58){
						stored_count = stored_count + get_breaks[o].length;
					}else{
						stored_count = stored_count +58;
					}
				}else{
					h = h +"<br>"+ get_breaks[o];
				}
			}
			var html = c + '<span class="moreelipses">'+ellipsestext+'</span>&nbsp;<span class="morecontent"><span>' + h + '</span>&nbsp;&nbsp;<a href="" class="morelink">'+moretext+'</a></span>';
			$(this).html(html);
		}else{
			if(content.length > showChar) {
				
				var c1 = content.substr(0, showChar);
				var h1 = content.substr(showChar-1, content.length - showChar);
				c1 = c1.replace("<p>","");
				c1 = c1.replace("</p>","");
				
				var html1 = c1 + '<span class="moreelipses">'+ellipsestext+'</span>&nbsp;<span class="morecontent"><span>' + h1 + '</span>&nbsp;&nbsp;<a href="" class="morelink">'+moretext+'</a></span>';

				$(this).html(html1);
			}
		}

	});

	$(".morelink").click(function(){
		if($(this).hasClass("less")) {
			$(this).removeClass("less");
			$(this).html(moretext);
		} else {
			$(this).addClass("less");
			$(this).html(lesstext);
		}
		$(this).parent().prev().toggle("slow");
		$(this).prev().toggle("slow");
		return false;
	});
});
</SCRIPT>	
			</div>
		</div>
		
		<div>
		</div>
		<!--Added files for displaying fancy box 2-->
		<script type="text/javascript" src="new_fancy_box/jquery.fancybox.js?v=2.1.4"></script>
		<link rel="stylesheet" type="text/css" href="new_fancy_box/jquery.fancybox.css?v=2.1.4" media="screen" />
		<script type="text/javascript" src="new_fancy_box/helpers/jquery.fancybox-media.js?v=1.0.5"></script>
	<!--Added files for displaying fancy box 2 Ends here-->
                	<h1 style="padding: 0 0 6px;">News Feed</h1>
					<?php
					$sql_all_feeds = $sql_feeds->all_feeds($chk_sql['email']);
					$run_all_feeds = mysql_query($sql_all_feeds);
					
					$sql_fan_feeds = $sql_feeds->fan_club($chk_sql['email']);
					$run_fan_feeds = mysql_query($sql_fan_feeds);
					
					$frds_posts_all_l = $sql_feeds->all_frds_posts($_SESSION['login_id']);
					$frds_posts_all = mysql_query($frds_posts_all_l);
					
					if(($newres1['artist_id']!=0 && $new_artist['status']==0 && $new_artist['active']==0) || ($newres1['community_id']!=0 && $new_community['status']==0 && $new_community['active']==0) || (mysql_num_rows($frds_posts_all)>0 || mysql_num_rows($run_fan_feeds)>0 || mysql_num_rows($run_all_feeds)>0))
					{
						if(mysql_num_rows($frds_posts_all)>0 || mysql_num_rows($run_fan_feeds)>0 || mysql_num_rows($run_all_feeds)>0)
						{
					?>
                    <div id="mediaContent">
                        <div class="topLinks">
                            <div class="links" style="width:665px;">
                                <select class="eventdrop" onChange="handleSelection(value)">
                                	<option value="all_news_feeds">All News</option>
									<option value="photo_feeds">Photo Feed</option>
                                    <option value="music_feeds">Music Feed</option>
									<option value="video_feeds">Video Feed</option>
                                    <option value="events_feeds">Event Feed</option>
                                    <option value="project_feeds">Project Feed</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div id="actualContent" style="height: 750px;overflow-x: hidden;overflow-y: auto;margin-bottom: 30px;">
		<?php
					$chk_sql = $sql_feeds->get_log_info($_SESSION['login_email']);
					if($chk_sql!="")
					{
		?>
                    	<div id="all_news_feeds">
							
		<?php
					$ans_all_feeds = array();
					$ans_sub_feeds = array();
					$ans_fan_club = array();
					$pro_eve_id = array();
					$art_com_id = array();
					$get_posts = array();
					$arts_ids = array();
					$coms_ids = array();
					$ans_all_posts = array();
					
					$sql_all_feeds = $sql_feeds->all_feeds($chk_sql['email']);
					$run_all_feeds = mysql_query($sql_all_feeds);
					
					$sql_fan_feeds = $sql_feeds->fan_club($chk_sql['email']);
					$run_fan_feeds = mysql_query($sql_fan_feeds);
					
					if($run_all_feeds!="" && $run_all_feeds!=NULL)
					{
						if(mysql_num_rows($run_all_feeds)>0)
						{
							while($row_all_feeds = mysql_fetch_assoc($run_all_feeds))
							{
								$ans_sub_feeds[] = $row_all_feeds;
							}
						}
					}
					
					if($run_fan_feeds!="" && $run_fan_feeds!=NULL)
					{
						if(mysql_num_rows($run_fan_feeds)>0)
						{
							while($row_fan_feeds = mysql_fetch_assoc($run_fan_feeds))
							{
								$ans_fan_club[] = $row_fan_feeds;
							}
						}
					}
					
					$ans_frds_posts = array();
					$frds_posts_all_l = $sql_feeds->all_frds_posts($_SESSION['login_id']);
					$frds_posts_all = mysql_query($frds_posts_all_l);
					if($frds_posts_all!="" && $frds_posts_all!=NULL)
					{
						if(mysql_num_rows($frds_posts_all)>0)
						{
							while($row_f = mysql_fetch_assoc($frds_posts_all))
							{
								$ans_frds_posts[] = $row_f;
							}
						}
					}
					$ans_all_feeds = array_merge($ans_sub_feeds,$ans_fan_club);
					if(($ans_all_feeds!=NULL && !empty($ans_all_feeds)) || ($ans_frds_posts!="" && !empty($ans_frds_posts)))
					{
						for($all_fed_lo=0;$all_fed_lo<count($ans_all_feeds);$all_fed_lo++)
						{
							if($ans_all_feeds[$all_fed_lo]['related_type']!='artist' && $ans_all_feeds[$all_fed_lo]['related_type']!='community')
							{
								$num_ids = explode('_',$ans_all_feeds[$all_fed_lo]['related_id']);
								if($ans_all_feeds[$all_fed_lo]['related_type']=='artist_project' || $ans_all_feeds[$all_fed_lo]['related_type']=='artist_event')
								{
									$pro_eve_id[$all_fed_lo]['related_id_art'] = $num_ids[0];
									$art_com_id[$all_fed_lo]['related_id_com'] ="";
								}
								elseif($ans_all_feeds[$all_fed_lo]['related_type']=='community_project' || $ans_all_feeds[$all_fed_lo]['related_type']=='community_event')
								{
									$art_com_id[$all_fed_lo]['related_id_com'] = $num_ids[0];
									$pro_eve_id[$all_fed_lo]['related_id_art'] = "";
								}
							}
							elseif($ans_all_feeds[$all_fed_lo]['related_type']=='artist' || $ans_all_feeds[$all_fed_lo]['related_type']=='community')
							{
								if($ans_all_feeds[$all_fed_lo]['related_type']=='artist')
								{
									$pro_eve_id[$all_fed_lo]['related_id_art'] = $ans_all_feeds[$all_fed_lo]['related_id'];
									$art_com_id[$all_fed_lo]['related_id_com'] ="";
								}
								elseif($ans_all_feeds[$all_fed_lo]['related_type']=='community')
								{
									$art_com_id[$all_fed_lo]['related_id_com'] = $ans_all_feeds[$all_fed_lo]['related_id'];
									$pro_eve_id[$all_fed_lo]['related_id_art'] = "";
								}
							}
						}
						if($pro_eve_id!=0 && count($pro_eve_id)>0)
						{
							for($arts=0;$arts<=count($pro_eve_id);$arts++)
							{
								if(isset($pro_eve_id[$arts]['related_id_art'])){
								$org_art[] = $pro_eve_id[$arts]['related_id_art']; }
							}
							$new_art = array();
							$is = 0;
							foreach ($org_art as $key => $value)
							{
								if(isset($new_art[$value]))
								{
									$new_art[$value] += 1;
								}
								else
								{
									$new_art[$value] = 1;
									
									$sqls = $sql_feeds->arts_gen($value);
									
									if($sqls!="")
									{
										$arts_ids[$is] = $sqls;
									}
								}
								$is = $is + 1;
							}
						}
						
						if($art_com_id!="" && count($art_com_id)>0)
						{
							for($coms=0;$coms<count($art_com_id);$coms++)
							{
								$org_com[] = $art_com_id[$coms]['related_id_com'];
							}
							
							$new_com = array();
							$ie_c = 0;
							foreach ($org_com as $key => $value)
							{
								if(isset($new_com[$value]))
								{
									$new_com[$value] += 1;
								}
								else
								{
									$new_com[$value] = 1;
									
									$sqls_c = $sql_feeds->coms_gen($value);
									if($sqls_c!="")
									{
										$coms_ids[$ie_c] = $sqls_c;
									}
								}
								$ie_c = $ie_c + 1;
							}
						}
						
						$ans_frds_posts = array();
						$frds_posts_all_l = $sql_feeds->all_frds_posts($_SESSION['login_id']);
						$frds_posts_all = mysql_query($frds_posts_all_l);
						if($frds_posts_all!="" && $frds_posts_all!=NULL)
						{
							if(mysql_num_rows($frds_posts_all)>0)
							{
								while($row_f = mysql_fetch_assoc($frds_posts_all))
								{
									$ans_frds_posts[] = $row_f;
								}
							}
						}						
						
						/* $ans_org_frds = array();
						if(count($ans_frds_posts)>0 && !empty($ans_frds_posts))
						{
							for($fp_o=0;$fp_o<count($ans_frds_posts);$fp_o++)
							{
								$ans_org_frds[$fp_o] = $ans_frds_posts[$fp_o]['general_user_id'];
							}
						} */

						
						$doub_chk = array_merge($arts_ids,$coms_ids,$ans_frds_posts);
						$rel_d_id = array();
						for($ch_d=0;$ch_d<count($doub_chk);$ch_d++)
						{
							//echo $doub_chk[$ch_d]['general_user_id'];
							$rel_d_id[] = $doub_chk[$ch_d]['general_user_id'];
						}
						
						
						$new_array_re = array();
						foreach ($rel_d_id as $key => $value)
						{
							if(isset($new_array_re[$value]))
								$new_array_re[$value] += 1;
							else
								$new_array_re[$value] = 1;
						}
						foreach ($new_array_re as $uid => $n)
						{
							$ex_uid1 = $ex_uid1.','.$uid;
						}
						$ex_tr = trim($ex_uid1, ",");
						$sep_ids = explode(',',$ex_tr);
						
						
						
						
						
						if($sep_ids!="" && count($sep_ids)>0)
						{
							for($se_ca=0;$se_ca<count($sep_ids);$se_ca++)
							{
								$ids = $sql_feeds->all_posts_check($sep_ids[$se_ca],$_SESSION['login_id']);
								if($ids!="" && count($ids)>0)
								{
									$posts_all = $sql_feeds->all_posts($sep_ids[$se_ca],$_SESSION['login_id']);
									if($posts_all!="" && $posts_all!=NULL)
									{
										if($posts_all!="" && mysql_num_rows($posts_all)>0)
										{
											while($rows_posts_all = mysql_fetch_assoc($posts_all))
											{
												$ans_all_posts[] = $rows_posts_all;
											}
										}
									}
								}
							}
						}
						
						$sort = array();
						foreach($ans_all_posts as $k=>$v)
						{
							$sort['date_time'][$k] = $v['date_time'];
							//$sort['from'][$k] = $end_f;
							//$sort['track'][$k] = $v['track'];
							
						}
						if(!empty($sort))
						{
							array_multisort($sort['date_time'], SORT_DESC, $ans_all_posts);
						}
										
						if($ans_all_posts!="" && count($ans_all_posts)>0)
						{
							for($po_all=0;$po_all<count($ans_all_posts);$po_all++)
							{
								if($ans_all_posts[$po_all]['profile_type']=='songs_post' || $ans_all_posts[$po_all]['profile_type']=='videos_post' || $ans_all_posts[$po_all]['profile_type']=='images_post')
								{
									$exp_meds = explode('~',$ans_all_posts[$po_all]['media']);
									if($exp_meds[1]=='reg_med')
									{
										$sql_meds = $sql_feeds->get_mediaSelected($exp_meds[0]);
									}
									elseif(($exp_meds[1]=='reg_art' || $exp_meds[1]=='reg_com') && $ans_all_posts[$po_all]['profile_type']=='images_post')
									{
										if($exp_meds[1]=='reg_art')
										{
											$reg_table_type = 'general_artist_gallery_list';
										}
										elseif($exp_meds[1]=='reg_com')
										{
											$reg_table_type = 'general_community_gallery_list';
										}
										
										$sql_meds = $sql_feeds->get_galleryData($reg_table_type,$exp_meds[0]);
									}
									elseif(($exp_meds[1]=='reg_art' || $exp_meds[1]=='reg_com') && $ans_all_posts[$po_all]['profile_type']=='videos_post')
									{
										if($exp_meds[1]=='reg_art')
										{
											$reg_table_type = 'general_artist_video';
										}
										elseif($exp_meds[1]=='reg_com')
										{
											$reg_table_type = 'general_community_video';
										}
														
										$sql_meds = $sql_feeds->get_MediaReg($reg_table_type,$exp_meds[0]);
									}
									elseif(($exp_meds[1]=='reg_art' || $exp_meds[1]=='reg_com') && $ans_all_posts[$po_all]['profile_type']=='songs_post')
									{
										if($exp_meds[1]=='reg_art')
										{
											$reg_table_type = 'general_artist_audio';
										}
										elseif($exp_meds[1]=='reg_com')
										{
											$reg_table_type = 'general_community_audio';
										}
														
										$sql_meds = $sql_feeds->get_MediaReg($reg_table_type,$exp_meds[0]);
									}
								}
								elseif($ans_all_posts[$po_all]['profile_type']=='projects_post' || $ans_all_posts[$po_all]['profile_type']=='events_post')
								{
									$exp_meds = explode('~',$ans_all_posts[$po_all]['media']);
									$sql_meds = $sql_feeds->featured_pro_eve($exp_meds[1],$exp_meds[0]);
								}
										
								if(isset($sql_meds) && $sql_meds!='0' && $sql_meds!=NULL)
								{
									$gens = $sql_feeds->get_log_ids($ans_all_posts[$po_all]['general_user_id']);
									if($ans_all_posts[$po_all]['display_to']=='All')
									{
										$hide8 = 0;
										$get_gen_id = $sql_feeds->get_general_info();
										$hide_by = $ans_all_posts[$po_all]['hidden_by'];
										$exp_hide_by = explode(",",$hide_by);
										for($exp_count=0;$exp_count<=count($exp_hide_by);$exp_count++)
										{
											if(isset($exp_hide_by[$exp_count])){
												if($exp_hide_by[$exp_count] ==$get_gen_id['general_user_id'])
												{
													$hide8 =1;
												}
											}
										}
										if($hide8 != 1)
										{
		?>
											<div class="storyCont">
												<div class="leftSide">
													<div class="avatar">
													<?php
													$art = 0;
													
													if($gens['artist_id']!=0)
													{
														$sql_art = mysql_query("SELECT * FROM general_artist WHERE artist_id='".$gens['artist_id']."' AND status=0 AND active=0 AND del_status=0");
														if($sql_art!="" && $sql_art!=NULL)
														{
															if(mysql_num_rows($sql_art)>0)
															{
																$ros = mysql_fetch_assoc($sql_art);
																$art = 1;
															}
														}
													}
													
													$com = 0;
													if($gens['community_id']!=0)
													{
														$sql_com = mysql_query("SELECT * FROM general_community WHERE community_id='".$gens['community_id']."' AND status=0 AND active=0 AND del_status=0");
														if($sql_com!="" && $sql_com!=NULL)
														{
															if(mysql_num_rows($sql_com)>0)
															{
																$ros_com = mysql_fetch_assoc($sql_com);
																$com = 1;
															}
														}
													}
													
													if($art==1)
													{
														if($ros['profile_url']!="")
														{
													?>
														<a href="/<?php echo $ros['profile_url']; ?>">
													<?php
														}
													?>
														<img width="50" height="50" src="<?php
														if($ros['image_name']!="" && !empty($ros['image_name']))
														{
															$get_news_general_user_id = $uk-> get_general_user_idby_artistid($ros['artist_id']);
															echo $s3->getNewURI_otheruser("artjcropprofile",$get_news_general_user_id) . $ros['image_name']; 
														}
														else
														{
															echo "http://artjcropprofile.s3.amazonaws.com/Noimage.png";
														}
													?>" />
													<?php
													if($ros['profile_url']!="")
														{
													?>
													</a>
													<?php
														}
													}
													elseif($com==1)
													{
														if($ros_com['profile_url']!="")
														{
													?>
														<a href="/<?php echo $ros_com['profile_url']; ?>">
													<?php
														}
													?>
														<img width="50" height="50" src="<?php
													
													
													if($ros_com['image_name']!="" && !empty($ros_com['image_name']))
													{
														$get_news_general_user_id = $uk-> get_general_user_idby_communityid($ros_com['community_id']);
														echo $s3->getNewURI_otheruser("comjcropprofile",$get_news_general_user_id) . $ros_com['image_name']; 
													}
													else
													{
														echo "http://comjcropprofile.s3.amazonaws.com/Noimage.png";
													}
													
													?>" />
													<?php
													if($ros_com['profile_url']!="")
														{
													?>
															</a>
													<?php
														}
													}
													?>
													</div>
		<?php 									
													//echo "table=".$reg_table_media_type;
													//echo $ans_audio_feeds[$audio_fed_lo]['id'];
		?>
													<div class="storyContainer">
														<div class="storyTitle">
															<span class="userName"><?php 
															if($art==1)
															{
																if($ros['profile_url']!="")
														{
													?>
														<a href="/<?php echo $ros['profile_url']; ?>">
													<?php
														}
																echo $ros['name'];
																if($ros['profile_url']!="")
														{
													?>
														</a>
													<?php
														}
															}
															elseif($com==1)
															{
																if($ros_com['profile_url']!="")
														{
													?>
														<a href="/<?php echo $ros_com['profile_url']; ?>">
													<?php
														}
																echo $ros_com['name'];
																if($ros_com['profile_url']!="")
														{
													?>
														</a>
													<?php
														}
															}
															?></span> <?php //echo $ans_all_posts[$po_all]['description']; ?>
														</div>											
		<?php
														if($ans_all_posts[$po_all]['profile_type']!="" && $ans_all_posts[$po_all]['media']!="")
														{
		?>
															<div class="activityCont">
																<div class="sideL">
																	<div class="storyImage" style="position:relative;">
																	<?php
																		if( (isset($sql_meds['media_type']) && $sql_meds['media_type']==115) || (isset($ans_all_posts[$po_all]['profile_type']) && $ans_all_posts[$po_all]['profile_type']=='videos_post'))
																		{
																			if($sql_meds['media_type']==115)
																			{
																				$tables = 'media_video';
																				$get_video_image = $sql_feeds->getvideoimage($tables,$sql_meds['id']);
																			}
																			elseif($ans_all_posts[$po_all]['profile_type']=='videos_post' && $exp_meds[1]=='reg_art')
																			{
																				$tables = 'general_artist_video';
																				$get_video_image = $sql_feeds->getvideoimage($tables,$sql_meds['id']);
																			}
																			elseif($ans_all_posts[$po_all]['profile_type']=='videos_post' && $exp_meds[1]=='reg_com')
																			{
																				$tables = 'general_community_video';
																				$get_video_image = $sql_feeds->getvideoimage($tables,$sql_meds['id']);
																			}
																			if($get_video_image[1]=='youtube')
																			{
																				$vid_pros_edit_img = "http://img.youtube.com/vi/".$get_video_image[0]."/default.jpg";
																			}
																			else
																			{
																				$video_vim = $vimeo->call('vimeo.videos.getInfo', array('video_id' => $get_video_image[0]));
																				$vid_pros_edit_img = $video_vim->video[0]->thumbnails->thumbnail[1]->_content;
																			}
																	?>
																			<a onmouseout="newfunctionout('<?php echo $sql_meds['id']; ?>','a')" onmouseover="newfunction('<?php echo $sql_meds['id']; ?>','a')" href="javascript:void(0);" onclick="showPlayer('v','<?php echo $sql_meds['id'];?>','playlist');">
																			<img src="<?php echo $vid_pros_edit_img; ?>" width="90" height="90" />
																			</a>
																			<div class="navButton">
			<div class="playMedia">
				<a href="javascript:void(0);" class="list_play"><img title="Add file to the bottom of your playlist without stopping current media." id="play_video_image<?php echo $sql_meds['id']; ?>a" onclick="showPlayer('v','<?php echo $sql_meds['id'];?>','playlist');" src="/images/play.png" onmouseout="newfunctionout('<?php echo $sql_meds['id']; ?>','a')" onmouseover="newfunction('<?php echo $sql_meds['id']; ?>','a')" style="border-radius: 6px;"></a>
			</div>
		</div>
																	<?php
																		}
																		/* elseif($sql_meds['media_type']==114)
																		{
																			$get_audio_image = $sql_feeds->get_song_med_img($sql_meds['id']);
																			
																			if($get_audio_image!="" && !empty($get_audio_image))
																			{
																				if($get_audio_image['gallery_at']!="")
																				{
																					$get_audio__timage = $sql_feeds->get_song_med_t_img($get_audio_image['gallery_at'],$get_audio_image['gallery_id']);
																					if($get_audio_image['gallery_at']=='general_media')
																					{
																		?>
																						<img src="http://medgalthumb.s3.amazonaws.com/<?php echo $get_audio__timage; ?>" width="90" height="90" />
																		<?php
																					}
																				}
																			}
																		} */
																		elseif(isset($sql_meds['media_type']) && $sql_meds['media_type']==113)
																		{
																			$get_all_image1 = $sql_feeds->get_all_gallery_images($sql_meds['id']);
																			$play_array1 = array();
																			if($get_all_image1!="" && $get_all_image1!=NULL)
																			{
																			if(mysql_num_rows($get_all_image1)>0)
																			{
																				while($row = mysql_fetch_assoc($get_all_image1))
																				{
																					$play_array1[] = $row;
																				}
																				$get_news_general_user_id = $uk -> get_general_user_idby_mediaid($sql_meds['id']);
																				${'orgPath'.$my_po}= $s3->getNewURI_otheruser("medgallery",$get_news_general_user_id); 
																				${'thumbPath'.$my_po}= $s3->getNewURI_otheruser("medgalthumb",$get_news_general_user_id);
																				for($l=0;$l<count($play_array1);$l++)
																				{
																					$mediaSrc = $play_array1[0]['image_name'];
																					$filePath = $s3->getNewURI_otheruser("medgallery",$get_news_general_user_id).$mediaSrc;
																					
																					
																					${'galleryImagesData'.$my_po}[$galleryImagesDataIndex] = $play_array1[$l]['image_name'];
																					${'galleryImagestitle'.$my_po}[$galleryImagesDataIndex] = $play_array1[$l]['image_title'];
																					//echo ${'galleryImagesData'.$my_po}[$galleryImagesDataIndex];
																					$galleryImagesDataIndex++;
																				}
																				}
																			}
																			if(isset(${'galleryImagesData'.$my_po}))
																			{
																				${'galleryImagesData'.$my_po} = implode(",", ${'galleryImagesData'.$my_po});
																				${'galleryImagestitle'.$my_po} = implode(",", ${'galleryImagestitle'.$my_po});
																			}
																			
																			$get_gal_image = $sql_feeds->gallery_meds($sql_meds['id']);
																			if($get_gal_image!="" && !empty($get_gal_image))
																			{
																				if($get_gal_image['profile_image']!="")
																				{
																		?>
																				<a onmouseout="newfunctionout('<?php echo $sql_meds['id']; ?>','a')" onmouseover="newfunction('<?php echo $sql_meds['id']; ?>','a')" href="javascript:void(0);" onclick="checkload('<?php echo $mediaSrc;?>','<?php echo ${'galleryImagesData'.$my_po}; ?>','<?php echo ${'thumbPath'.$my_po}; ?>','<?php echo ${'orgPath'.$my_po}; ?>','<?php echo ${'galleryImagestitle'.$my_po}; ?>','<?php echo $get_gallery['id'];?>')">
																					<img src="<?php echo $get_gal_image['profile_image']; ?>" width="90" height="90" />
																				</a>	
																		<?php
																				}
																				else
																				{
																		?>
																				<a onmouseout="newfunctionout('<?php echo $sql_meds['id']; ?>','a')" onmouseover="newfunction('<?php echo $sql_meds['id']; ?>','a')" href="javascript:void(0);" onclick="checkload('<?php echo $mediaSrc;?>','<?php echo ${'galleryImagesData'.$my_po}; ?>','<?php echo ${'thumbPath'.$my_po}; ?>','<?php echo ${'orgPath'.$my_po}; ?>','<?php echo ${'galleryImagestitle'.$my_po}; ?>','<?php echo $get_gallery['id'];?>')">
																					<img width="90" height="90" src="http://comjcropprofile.s3.amazonaws.com/<?php echo "Noimage.png";?>" />
																				</a>	
																				<?php	
																				}
																			}
																			else
																			{
																		?>
																			<a onmouseout="newfunctionout('<?php echo $sql_meds['id']; ?>','a')" onmouseover="newfunction('<?php echo $sql_meds['id']; ?>','a')" href="javascript:void(0);" onclick="checkload('<?php echo $mediaSrc;?>','<?php echo ${'galleryImagesData'.$my_po}; ?>','<?php echo ${'thumbPath'.$my_po}; ?>','<?php echo ${'orgPath'.$my_po}; ?>','<?php echo ${'galleryImagestitle'.$my_po}; ?>','<?php echo $get_gallery['id'];?>')">
																				<img width="90" height="90" src="http://comjcropprofile.s3.amazonaws.com/<?php echo "Noimage.png";?>" />
																			</a>	
																			<?php	
																			}
																			$my_po = $my_po + 1;
																			?>
																				<div class="navButton">
			<div class="playMedia">
				<a href="javascript:void(0);" class="list_play"><img title="Add file to the bottom of your playlist without stopping current media." id="play_video_image<?php echo $sql_meds['id']; ?>a" onclick="checkload('<?php echo $mediaSrc;?>','<?php echo ${'galleryImagesData'.$my_po}; ?>','<?php echo ${'thumbPath'.$my_po}; ?>','<?php echo ${'orgPath'.$my_po}; ?>','<?php echo ${'galleryImagestitle'.$my_po}; ?>','<?php echo $get_gallery['id'];?>')" src="/images/play.png" onmouseout="newfunctionout('<?php echo $sql_meds['id']; ?>','a')" onmouseover="newfunction('<?php echo $sql_meds['id']; ?>','a')" style="border-radius: 6px;"></a>
			</div>
			</div>
																			<?php
																		}
																		elseif(isset($sql_meds['media_type']) && $sql_meds['media_type']==116)
																		{
																			$get_ch_image = $sql_feeds->channel_meds($sql_meds['id']);
																			if($get_ch_image!="" && !empty($get_ch_image))
																			{
																				if($get_ch_image['profile_image']!="")
																				{
																		?>
																					<img src="<?php echo $get_ch_image['profile_image']; ?>" width="90" height="90" />
																		<?php
																				}
																				else
																				{
																		?>
																					<img width="90" height="90" src="http://comjcropprofile.s3.amazonaws.com/<?php echo "Noimage.png";?>" />
																				<?php
																				}
																			}
																			else
																			{
																		?>
																				<img width="90" height="90" src="http://comjcropprofile.s3.amazonaws.com/<?php echo "Noimage.png";?>" />
																			<?php	
																			}
																		}
																		elseif($exp_meds[1]=='reg_art' && $ans_all_posts[$po_all]['profile_type']=='images_post')
																		{
																			$tables = 'artist';
																			$imgs = $sql_feeds->reg_gal($tables,$sql_meds['gallery_id']);
																			
																			if($imgs!="" && !empty($imgs))
																			{
																				if($imgs['image_name']!="")
																				{
																			?>
																					<img width="90" height="90" src="http://reggalthumb.s3.amazonaws.com/<?php echo $imgs['image_name'];?>" />
																				<?php	
																				}
																				else
																				{
																			?>
																					<img width="90" height="90" src="http://comjcropprofile.s3.amazonaws.com/<?php echo "Noimage.png";?>" />
																				<?php	
																				}
																			}
																			else
																			{
																			?>
																				<img width="90" height="90" src="http://comjcropprofile.s3.amazonaws.com/<?php echo "Noimage.png";?>" />
																			<?php	
																			}
																		}
																		elseif($exp_meds[1]=='reg_com' && $ans_all_posts[$po_all]['profile_type']=='images_post')
																		{
																			$tables = 'community';
																			$imgs = $sql_feeds->reg_gal($tables,$sql_meds['gallery_id']);
																			
																			if($imgs!="" && !empty($imgs))
																			{
																				if($imgs['image_name']!="")
																				{
																			?>
																					<img width="90" height="90" src="http://reggalthumb.s3.amazonaws.com/<?php echo $imgs['image_name'];?>" />
																				<?php	
																				}
																				else
																				{
																			?>
																					<img width="90" height="90" src="http://comjcropprofile.s3.amazonaws.com/<?php echo "Noimage.png";?>" />
																				<?php
																				}
																			}
																			else
																			{
																			?>
																				<img width="90" height="90" src="http://comjcropprofile.s3.amazonaws.com/<?php echo "Noimage.png";?>" />
																			<?php	
																			}
																		}
																		elseif($ans_all_posts[$po_all]['profile_type']=='events_post' || $ans_all_posts[$po_all]['profile_type']=='projects_post')
																		{
																			if($sql_meds['profile_url']!="") {
		?>																				
																					<a href="/<?php echo $sql_meds['profile_url']; ?>">
		<?php
																		}
																		?>
																			<img width="90" height="90" src="<?php if($sql_meds['image_name']!="") { echo $sql_meds['image_name']; } else { echo "http://comjcropprofile.s3.amazonaws.com/Noimage.png"; }?>" />
																		<?php	
																		if($sql_meds['profile_url']!="") {
		?>																				
																					</a>
		<?php
																		}
																		}
																		
																	?>
																</div>
																	<?php
																	if(isset($sql_meds['media_type']) && $sql_meds['media_type']!=113){
																	?>				
																	<div class="player">
																	<?php 
																		if(isset($sql_meds['media_type']) && $sql_meds['media_type']==115)
																		{
																			include_once("findsale_video.php");
																		?>
																			<img src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" onclick="showPlayer('v','<?php echo $sql_meds['id'];?>','playlist');">
																			<img src="images/profile/add.gif" onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'" onclick="showPlayer('v','<?php echo $sql_meds['id'];?>','addvi');">
																			<?php
																			if($sql_meds['sharing_preference'] == 5 || $sql_meds['sharing_preference'] == 2)
																			{
																				$get_video_image = $sql_feeds->getvideoimage_feature($sql_meds['id']);
																				//$image_name_cart = "https://img.youtube.com/vi/".$get_video_image."/default.jpg";
																				if($get_video_image[1]=='youtube')
																			{
																				$image_name_cart = "http://img.youtube.com/vi/".$get_video_image[0]."/default.jpg";
																			}
																			else
																			{
																				$video_vim = $vimeo->call('vimeo.videos.getInfo', array('video_id' => $get_video_image[0]));
																				$image_name_cart = $video_vim->video[0]->thumbnails->thumbnail[1]->_content;
																			}
																				$free_down_rel_video="";
																				for($find_down=0;$find_down<=count($get_vids_pnew_w);$find_down++)
																				{
																					if(isset($sql_meds) && $sql_meds != null){
																						if($get_vids_pnew_w[$find_down]['id']==$sql_meds['id']){
																							$free_down_rel_video = "yes";
																						}
																						else{
																							if($free_down_rel_video =="" || $free_down_rel_video =="no"){
																								$free_down_rel_video = "no";
																							}
																						}
																					}
																				}
																				if($free_down_rel_video == "yes"){
																				 ?>
																					<!--<a href="javascript:void(0);" onclick="download_directly_song('<?php echo $sql_meds['id']; ?>','<?php echo str_replace("'","\'",$sql_meds['title']); ?>','<?php echo str_replace("'","\'",$sql_meds['creator']); ?>','<?php echo str_replace("'","\'",$sql_meds['from']); ?>')"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>-->
																			 <?php
																				}
																				else if($sql_meds['sharing_preference']==2){
																					if($sql_meds['id']!=""){
																					?>
																					<!--<a href="javascript:void(0);" onclick="ask_to_donate('<?php echo str_replace("'","\'",$sql_meds['title']);?>','<?php echo str_replace("'","\'",$sql_meds['creator']);?>','<?php echo $sql_meds['id'];?>','download_song<?php echo $sql_meds['id'];?>')"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>-->
																				<?php
																					}else{
																					?>
																					<!--<a href="javascript:void(0);" onclick="ask_to_donate('<?php echo str_replace("'","\'",$sql_meds['title']);?>','<?php echo str_replace("'","\'",$sql_meds['creator']);?>','<?php echo $sql_meds['id'];?>','download_song<?php echo $sql_meds['id'];?>')"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>-->
																				<?php
																					}
																				}
																				else{
																			?>
																				<!--<a href="javascript:void(0);" onclick="down_video_pop(<?php echo $sql_meds['id'];?>)"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>-->
																			<?php
																				}
																			}else{
																			?>
																				<!--<img src="../images/profile/download-over.gif" style="cursor:default"/>-->
																			<?php
																			}
																			?>
																			<!--<a href="addtocart.php?seller_id=<?php echo $sql_meds['general_user_id']; ?>&media_id=<?php echo $sql_meds['id']; ?>&buyer_id=<?php echo $_SESSION['gen_user_id'];?>&title=<?php echo str_replace("'","\'",$sql_meds['title']);?>&image_name=<?php echo $image_name_cart;?>" id="download_video<?php echo $sql_meds['id'];?>" style="display:none;"></a>-->
																			<script type="text/javascript">
																			$(document).ready(function() {
																					$("#download_video<?php echo $sql_meds['id'];?>").fancybox({
																					'height'			: '75%',
																					'width'				: '69%',
																					'transitionIn'		: 'none',
																					'transitionOut'		: 'none',
																					'type'				: 'iframe'
																					});
																			});
																			</script>
																			<?php
																		}/*else if($sql_meds['media_type']==113)
																		{
																			
																		?>
																			
																			<img src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'"></a>
																			<img src="images/profile/add.gif" onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'">
																			<img style="cursor: default;" src="images/profile/download-over.gif">
																		<?php
																		
																		}*/else if(isset($sql_meds['media_type']) && $sql_meds['media_type']==114)
																		{
																			include_once("findsale_song.php");
																		?>
																			<img src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" onclick="showPlayer('a','<?php echo $sql_meds['id'];?>','play');">
																			<img src="images/profile/add.gif" onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'" onclick="showPlayer('a','<?php echo $sql_meds['id'];?>','add');">
																			<?php
																			if($sql_meds['sharing_preference']==5 || $sql_meds['sharing_preference']==2)
																			{
																				$song_image_name_cart = $sql_feeds->get_audio_img($sql_meds['id']);
																				$free_down_rel ="";
																				for($find_down=0;$find_down<=count($get_songs_pnew_w);$find_down++)
																				{
																					if(isset($sql_meds) && $sql_meds != null)
																					{
																						if($get_songs_pnew_w[$find_down]['id']==$sql_meds['id']){
																							$free_down_rel = "yes";
																						}
																						else{
																							if($free_down_rel =="" || $free_down_rel =="no"){
																								$free_down_rel = "no";
																							}
																						}
																					}
																				}
																				if($free_down_rel == "yes"){
																				 ?>
																				 <!--<a href="javascript:void(0);" onclick="download_directly_song('<?php echo $sql_meds['id'];?>','<?php echo str_replace("'","\'",$sql_meds['title']);?>','<?php echo str_replace("'","\'",$sql_meds['creator']); ?>','<?php echo str_replace("'","\'",$sql_meds['from']); ?>')"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>-->
																			 <?php
																				}
																				else if($sql_meds['sharing_preference']==2){
																					if($sql_meds['id']!=""){
																					?>
																					<!--<a href="javascript:void(0);" onclick="ask_to_donate('<?php echo str_replace("'","\'",$sql_meds['title']);?>','<?php echo str_replace("'","\'",$sql_meds['creator']);?>','<?php echo $sql_meds['id'];?>','download_song<?php echo $sql_meds['id'];?>')"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>-->
																				<?php
																					}else{
																					?>
																					<!--<a href="javascript:void(0);" onclick="ask_to_donate('<?php echo str_replace("'","\'",$sql_meds['title']);?>','<?php echo str_replace("'","\'",$sql_meds['creator']);?>','<?php echo $sql_meds['id'];?>','download_song<?php echo $sql_meds['id'];?>')"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>-->
																				<?php
																					}
																				}
																				else{
																			?>
																				<!--<a href="javascript:void(0);" onclick="down_song_pop(<?php echo $sql_meds['id'];?>)"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>-->
																			<?php
																				}
																			}else{
																			?>	
																			<!--<img src="../images/profile/download-over.gif" style="cursor:default"/>-->
																			<?php
																			}
																			?>
																			<!--<a href="addtocart.php?seller_id=<?php echo $sql_meds['general_user_id']; ?>&media_id=<?php echo $sql_meds['id'] ?>&buyer_id=<?php echo $_SESSION['gen_user_id'];?>&title=<?php echo str_replace("'","\'",$sql_meds['title']); ?>&image_name=<?php echo $song_image_name_cart;?>" id="download_song<?php echo $sql_meds['id'];?>" style="display:none;"></a>-->
																				<script type="text/javascript">
																				$(document).ready(function() {
																						$("#download_song<?php echo $sql_meds['id'];?>").fancybox({
																						'width'				: '75%',
																						'height'			: '75%',
																						'transitionIn'		: 'none',
																						'transitionOut'		: 'none',
																						'type'				: 'iframe'
																						});
																				});
																				</script>
																			<?php
																		}else if($sql_meds['featured_media']!="" || $sql_meds['featured_media']!="nodisplay"){
																			if($sql_meds['featured_media'] == "Song" && $sql_meds['media_id'] != "0"){
																				include_once("findsale_song.php");
																			?>
																				<img src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" onclick="showPlayer('a','<?php echo $sql_meds['media_id'];?>','play');" />
																				<img src="images/profile/add.gif" onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'" onclick="showPlayer('a','<?php echo $sql_meds['media_id'];?>','add');" />
																			<?php
																				$get_media_info_down_gal = $sql_feeds ->get_media_details($sql_meds['media_id']);
																				if($get_media_info_down_gal['sharing_preference']==5 || $get_media_info_down_gal['sharing_preference']==2)
																				{
																					$song_image_name_cartproject = $sql_feeds->get_audio_img($get_media_info_down_gal['id']);
																					$free_down_rel ="";
																					for($find_down=0;$find_down<=count($get_songs_pnew_w);$find_down++)
																					{
																						//echo $get_songs_pnew_w[$find_down]['id'] ."==". $get_Song_title['id']."</br>";
																						if(isset($get_media_info_down_gal) && $get_media_info_down_gal != null)
																						{
																							if($get_songs_pnew_w[$find_down]['id']==$get_media_info_down_gal['id']){
																							$free_down_rel = "yes";
																							}
																							else{
																								if($free_down_rel =="" || $free_down_rel =="no"){
																									$free_down_rel = "no";
																								}
																							}
																						}
																					}
																					if($free_down_rel == "yes"){
																					 ?>
																					<!-- <a href="javascript:void(0);" onclick="download_directly_song('<?php echo $get_media_info_down_gal['id'];?>','<?php echo str_replace("'","\'",$get_media_info_down_gal['title']);?>','<?php echo str_replace("'","\'",$get_media_info_down_gal['creator']);?>','<?php echo str_replace("'","\'",$get_media_info_down_gal['from']);?>')"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>-->
																					<?php
																					}
																					else if($get_media_info_down_gal['sharing_preference']==2){
																					 ?>
																					<!--<a href="javascript:void(0);" onclick="ask_to_donate('<?php echo str_replace("'","\'",$get_media_info_down_gal['title']);?>','<?php echo str_replace("'","\'",$get_media_info_down_gal['creator']);?>','<?php echo $get_media_info_down_gal['id'];?>','download_songproject<?php echo $get_media_info_down_gal['id'];?>')"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>-->
																				 <?php
																					}
																					else{
																				?>
																					<!--<a href="javascript:void(0);" onclick="down_song_pop_project(<?php echo $get_media_info_down_gal['id'];?>)"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>-->
																				<?php
																					}
																				}else{
																				?>	
																				<!--<img src="../images/profile/download-over.gif" style="cursor:default" />-->
																				<?php
																				}
																				?>
																				<!--<a href="addtocart.php?seller_id=<?php echo $get_media_info_down_gal['general_user_id'];?>&media_id=<?php echo $get_media_info_down_gal['id'];?>&buyer_id=<?php echo $_SESSION['gen_user_id'];?>&title=<?php echo str_replace("'","\'",$get_media_info_down_gal['title']);?>&image_name=<?php echo $song_image_name_cartproject;?>" id="download_songproject<?php echo $get_media_info_down_gal['id'];?>" style="display:none;"></a>-->
																				<script type="text/javascript">
																				$(document).ready(function() {
																						$("#download_songproject<?php echo $get_media_info_down_gal['id'];?>").fancybox({
																						'width'				: '75%',
																						'height'			: '75%',
																						'transitionIn'		: 'none',
																						'transitionOut'		: 'none',
																						'type'				: 'iframe'
																						});
																				});
																				</script>
																				<?php
																			}if($sql_meds['featured_media'] == "Gallery" && $sql_meds['media_id'] != "0"){
																				$get_all_feature_image = $sql_feeds->get_all_gallery_images($sql_meds['media_id']);
																				if($get_all_feature_image!="" && $get_all_feature_image!=NULL)
																			{
																				if(mysql_num_rows($get_all_feature_image)>0)
																				{
																					$galleryImagesData ="";
																					$galleryImagestitle ="";
																					$mediaSrc ="";
																					$get_news_general_user_id = $uk -> get_general_user_idby_mediaid($sql_meds['media_id']);
																					while($row = mysql_fetch_assoc($get_all_feature_image))
																					{
																						$mediaSrc = $row[0]['image_name'];
																						$filePath = $s3->getNewURI_otheruser("medgallery",$get_news_general_user_id).$mediaSrc;
																						if($row['image_name']!=""){
																							$galleryImagesData = $galleryImagesData .",". $row['image_name'];
																							$galleryImagestitle = $galleryImagestitle .",".$row['image_title'];
																						}
																					}
																					$orgPath = $s3->getNewURI_otheruser("medgallery",$get_news_general_user_id);
																					$thumbPath = $s3->getNewURI_otheruser("medgalthumb",$get_news_general_user_id);
																				}
																				}
																			?>
																				<a href="javascript:void(0);" onclick="checkload('<?php echo $mediaSrc;?>','<?php echo $galleryImagesData; ?>','<?php echo $thumbPath; ?>','<?php echo $orgPath; ?>','<?php echo $galleryImagestitle; ?>','<?php echo $get_gallery['id'];?>')">
																				<img src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'"></a>
																				<img src="images/profile/add.gif" onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'">
																				<?php
																				$get_media_info_down_gal = $sql_feeds ->get_media_details($sql_meds['media_id']);
																				$get_gal_image_project  = $sql_feeds ->get_gallery_info($sql_meds['media_id']);
																				if($get_media_info_down_gal['sharing_preference']==2)
																				{
																				?>
																					<!--<a href="javascript:void(0);" onclick="ask_to_donate('<?php echo str_replace("'","\'",$get_media_info_down_gal['title']);?>','<?php echo str_replace("'","\'",$get_media_info_down_gal['creator']);?>','<?php echo $get_media_info_down_gal['id'];?>','download_galproject<?php echo $sql_meds['media_id'];?>')"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>-->
																				 <?php
																				}else{
																				?>
																				<!--<img src="../images/profile/download-over.gif" style="cursor:default"/>-->
																				<?php
																				}
																				?>
																				<!--<a href="addtocart.php?seller_id=<?php echo $get_media_info_down_gal['general_user_id'];?>&media_id=<?php echo $get_media_info_down_gal['id'];?>&buyer_id=<?php echo $_SESSION['gen_user_id'];?>&title=<?php echo str_replace("'","\'",$get_media_info_down_gal['title']);?>&image_name=<?php echo $get_gal_image_project['profile_image'];?>" id="download_galproject<?php echo $sql_meds['media_id'];?>" style="display:none;"></a>-->
																				<script type="text/javascript">
																				$(document).ready(function() {
																						$("#download_galproject<?php echo $sql_meds['media_id'];?>").fancybox({
																						'width'				: '75%',
																						'height'			: '75%',
																						'transitionIn'		: 'none',
																						'transitionOut'		: 'none',
																						'type'				: 'iframe'
																						});
																				});
																				</script>
																				<?php
																			}if($sql_meds['featured_media'] == "Video" && $sql_meds['media_id'] != "0"){
																				include_once("findsale_video.php");
																			?>
																				<img src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" onclick="showPlayer('v','<?php echo $sql_meds['media_id'];?>','playlist');"/>
																				<img src="images/profile/add.gif" onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'" onclick="showPlayer('v','<?php echo $sql_meds['media_id'];?>','addvi');"/>
																				<?php
																				$get_media_info_down_gal = $sql_feeds ->get_media_details($sql_meds['media_id']);
																				if($get_media_info_down_gal['sharing_preference']==5 || $get_media_info_down_gal['sharing_preference']==2)
																				{
																					$get_video_image = $sql_feeds->getvideoimage_feature($get_media_info_down_gal['id']);
																					//$image_name_cartproject = "https://img.youtube.com/vi/".$get_video_image."/default.jpg";
																					
																				if($get_video_image[1]=='youtube')
																			{
																				$image_name_cartproject = "http://img.youtube.com/vi/".$get_video_image[0]."/default.jpg";
																			}
																			else
																			{
																				$video_vim = $vimeo->call('vimeo.videos.getInfo', array('video_id' => $get_video_image[0]));
																				$image_name_cartproject = $video_vim->video[0]->thumbnails->thumbnail[1]->_content;
																			}
																			$free_down_rel_video="";
																					for($find_down=0;$find_down<=count($get_vids_pnew_w);$find_down++)
																					{
																						if(isset($get_media_info_down_gal) && $get_media_info_down_gal != null){
																							if($get_vids_pnew_w[$find_down]['id']==$get_media_info_down_gal['id']){
																								$free_down_rel_video = "yes";
																							}
																							else{
																								if($free_down_rel_video =="" || $free_down_rel_video =="no"){
																									$free_down_rel_video = "no";
																								}
																							}
																						}
																					}
																					if($free_down_rel_video == "yes"){
																					 ?>
																					 <!--<a href="javascript:void(0);" onclick="download_directly_song('<?php echo $get_media_info_down_gal['id'];?>','<?php echo str_replace("'","\'",$get_media_info_down_gal['title']);?>','<?php echo str_replace("'","\'",$get_media_info_down_gal['creator']);?>','<?php echo str_replace("'","\'",$get_media_info_down_gal['from']);?>')"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>
																					 -->
																				 <?php
																					}
																					else if($get_media_info_down_gal['sharing_preference']==2){
																					 ?>
																						<!--<a href="javascript:void(0);" onclick="ask_to_donate('<?php echo str_replace("'","\'",$get_media_info_down_gal['title']);?>','<?php echo str_replace("'","\'",$get_media_info_down_gal['creator']);?>','<?php echo $get_media_info_down_gal['id'];?>','download_videoproject<?php echo $sql_meds['media_id'];?>')"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>-->
																					<?php
																					}
																					else{
																				?>
																					<!--<a href="javascript:void(0);" onclick="down_video_pop_project(<?php echo $sql_meds['media_id'];?>)"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>-->
																				<?php
																					}
																				}
																				else{
																				?>
																				<!--<img src="../images/profile/download-over.gif" style="cursor:default"/>-->
																				<?php
																				}
																				?>
																				<!--<a href="addtocart.php?seller_id=<?php echo $get_media_info_down_gal['general_user_id'];?>&media_id=<?php echo $get_media_info_down_gal['id'];?>&buyer_id=<?php echo $_SESSION['gen_user_id'];?>&title=<?php echo str_replace("'","\'",$get_media_info_down_gal['title']);?>&image_name=<?php echo $image_name_cartproject;?>" id="download_videoproject<?php echo $sql_meds['media_id'];?>" style="display:none;"></a>-->
																				<script type="text/javascript">
																				$(document).ready(function() {
																						$("#download_videoproject<?php echo $sql_meds['media_id'];?>").fancybox({
																						'height'			: '75%',
																						'width'				: '69%',
																						'transitionIn'		: 'none',
																						'transitionOut'		: 'none',
																						'type'				: 'iframe'
																						});
																				});
																				</script>
																				<?php
																			}
																		}
																		else{
																		?>
																			<img src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'">
																			<img src="images/profile/add.gif" onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'">
																			<!--<img style="cursor: default;" src="images/profile/download-over.gif">-->
																		<?php
																		}
																	?>
																		
																	</div>	
																	<?php
																	}
																	?>
																</div>
																	
																<div class="sideR">
																	<div class="activityTitle">
																		
		<?php
																			if($exp_meds[1]=='reg_med' || $ans_all_posts[$po_all]['profile_type']=='projects_post' || $ans_all_posts[$po_all]['profile_type']=='events_post')
																			{
																				if($ans_all_posts[$po_all]['profile_type']=='projects_post' || $ans_all_posts[$po_all]['profile_type']=='events_post' || $sql_meds['media_type']!=114)
																				{
																					if($sql_meds['profile_url']!="") {
		?>																				
																					<a href="/<?php echo $sql_meds['profile_url']; ?>">
		<?php
																		}
																					if(isset($sql_meds['media_type']) && $sql_meds['media_type']==115){
																					echo "<a href=javascript:void(0) onclick=showPlayer('v','".$sql_meds['id']."','playlist')>".$sql_meds['title']."</a>";
																					}else if(isset($sql_meds['media_type']) && $sql_meds['media_type']==113){
																						$get_all_image1 = $sql_feeds->get_all_gallery_images($sql_meds['id']);
																						$play_array1 = array();
																						if($get_all_image1!="" && $get_all_image1!=NULL)
																			{
																						if(mysql_num_rows($get_all_image1)>0)
																						{
																							while($row = mysql_fetch_assoc($get_all_image1))
																							{
																								$play_array1[] = $row;
																							}
																							$get_news_general_user_id = $uk -> get_general_user_idby_mediaid($sql_meds['id']);
																							
																							${'orgPath'.$my_po}= $s3->getNewURI_otheruser("medgallery",$get_news_general_user_id);
																							${'thumbPath'.$my_po}= $s3->getNewURI_otheruser("medgalthumb",$get_news_general_user_id);
																							for($l=0;$l<count($play_array1);$l++)
																							{
																								$mediaSrc = $play_array1[0]['image_name'];
																								$filePath = $s3->getNewURI_otheruser("medgallery",$get_news_general_user_id).$mediaSrc;
																								
																								
																								${'galleryImagesData'.$my_po}[$galleryImagesDataIndex] = $play_array1[$l]['image_name'];
																								${'galleryImagestitle'.$my_po}[$galleryImagesDataIndex] = $play_array1[$l]['image_title'];
																								//echo ${'galleryImagesData'.$my_po}[$galleryImagesDataIndex];
																								$galleryImagesDataIndex++;
																							}
																							
																						}
																						}
																						if(isset(${'galleryImagesData'.$my_po}))
																						{
																							${'galleryImagesData'.$my_po} = implode(",", ${'galleryImagesData'.$my_po});
																							${'galleryImagestitle'.$my_po} = implode(",", ${'galleryImagestitle'.$my_po});
																						}
																						echo "<a href=javascript:void(0) onclick=checkload('".$mediaSrc."','". ${'galleryImagesData'.$my_po}."','". ${'thumbPath'.$my_po}."','". ${'orgPath'.$my_po}."','". ${'galleryImagestitle'.$my_po}."','". $get_gallery['id']."')>".$sql_meds['title']."</a>";
																						$my_po = $my_po + 1;
																					}else{
																					echo $sql_meds['title'];
																					}
																					if($sql_meds['profile_url']!="") {
		?>																				
																					</a>
		<?php
																		}
																				}
																				elseif($exp_meds[1]=='reg_med' && $sql_meds['media_type']==114)
																				{
																					$sql_m = mysql_query("SELECT * FROM media_songs WHERE media_id='".$sql_meds['id']."'");
																					$ans_m = mysql_fetch_assoc($sql_m);
																					$ecehos = '<a style="text-decoration: none; cursor: text;" href="javascript:void(0)"> '.$ans_m['track'].' </a>';
																					if($sql_meds['media_type']==114)
																					{
																						$ecehos .= "<a style='text-decoration: none; cursor: text;' href='javascript:void(0)'>".$sql_meds['title']."</a>";
																					}
																					else if($sql_meds['media_type']==115)
																					{
																						$ecehos .= "<a href=javascript:void(0) onclick=showPlayer('v',".$sql_meds['id'].",'addvi');>".$sql_meds['title']."</a>";
																					}
																					else if($sql_meds['media_type']==113)
																					{
																						$get_all_gal_image = $sql_feeds->get_all_gallery_images($sql_meds['id']);
																						if($get_all_gal_image!="" && $get_all_gal_image!=NULL)
																			{
																						if(mysql_num_rows($get_all_gal_image)>0)
																						{
																							$galleryImagesData ="";
																							$galleryImagestitle ="";
																							$mediaSrc ="";
																							$get_news_general_user_id = $uk -> get_general_user_idby_mediaid($sql_meds['id']);
																							while($row = mysql_fetch_assoc($get_all_gal_image))
																							{
																								$mediaSrc = $row[0]['image_name'];
																								$filePath = $s3->getNewURI_otheruser("medgallery",$get_news_general_user_id).$mediaSrc;
																								if($row['image_name']!=""){
																									$galleryImagesData = $galleryImagesData .",". $row['image_name'];
																									$galleryImagestitle = $galleryImagestitle .",".$row['image_title'];
																								}
																							}
																							$orgPath = $s3->getNewURI_otheruser("medgallery",$get_news_general_user_id);
																							$thumbPath = $s3->getNewURI_otheruser("medgalthumb",$get_news_general_user_id);
																						}
																						}
																					
																						$ecehos .= "<a href=javascript:void(0) onclick=checkload('".$mediaSrc."','".$galleryImagesData."','".$thumbPath."','".$orgPath."','". $galleryImagestitle."','".$get_gallery['id'].")>".$sql_meds['title']."</a>";
																						$galleryImagesData = "";
																						$galleryImagestitle = "";
																						$mediaSrc ="";
																					}
																					else
																					{
																						$ecehos .= '<a href="">'.$sql_meds['title'].'</a>';
																					}
																					
																	
																	if($sql_meds['creator']!="")
																	{
																		$create_c = strpos($sql_meds['creator'],'(');
																		$end = substr($sql_meds['creator'],0,$create_c);
																		$exp_cr = explode('|',$sql_meds['creator_info']);
																		
																		if($exp_cr[0]=='general_artist')
																		{
																			$sql_c_ar = mysql_query("SELECT * FROM general_artist WHERE artist_id='".$exp_cr[1]."'");
																		}
																		if($exp_cr[0]=='general_community')
																		{
																			$sql_c_ar = mysql_query("SELECT * FROM general_community WHERE community_id='".$exp_cr[1]."'");
																		}
																		if($exp_cr[0]=='artist_project' || $exp_cr[0]=='community_project' || $exp_cr[0]=='community_event' || $exp_cr[0]=='artist_event')
																		{
																			$sql_c_ar = mysql_query("SELECT * FROM $exp_cr[0] WHERE id='".$exp_cr[1]."'");
																		}
																		
																		
																		if(isset($sql_c_ar))
																		{
																			if($sql_c_ar!="" && $sql_c_ar!=NULL)
																			{
																			if(mysql_num_rows($sql_c_ar)>0)
																			{
																				$ans_c_ar = mysql_fetch_assoc($sql_c_ar);
																				if(isset($ans_c_ar['name']))
																				{
																					$end = $ans_c_ar['name'];
																				}
																				else
																				{
																					$end = $ans_c_ar['title'];
																				}
																				if($ans_c_ar['profile_url']!="" && !empty($ans_c_ar['profile_url']))
																				{
																					$ecehos .= " By  <a href='/".$ans_c_ar['profile_url']."'><b>".$end."</b></a>  ";
																				}
																				else
																				{
																					$ecehos .= " By  <b>".$end."</b>  ";
																				}
																			}
																			else
																			{
																				$ecehos .= " By  <b>".$end."</b>  ";
																			}
																			}
																			else
																			{
																				$ecehos .= " By  <b>".$end."</b>  ";
																			}
																		}
																		else
																		{
																			
																			$ecehos .= " By  <b>".$end."</b>  ";
																		}
																	}
																	if($sql_meds['from']!="")
																	{
																		$create_f = strpos($sql_meds['from'],'(');
																		$end_f = substr($sql_meds['from'],0,$create_f);
																		
																		$exp_fr = explode('|',$sql_meds['from_info']);
																		if($exp_fr[0]=='general_artist')
																		{
																			$sql_f_ar = mysql_query("SELECT * FROM general_artist WHERE artist_id='".$exp_fr[1]."'");
																		}
																		if($exp_fr[0]=='general_community')
																		{
																			$sql_f_ar = mysql_query("SELECT * FROM general_community WHERE community_id='".$exp_fr[1]."'");
																		}
																		if($exp_fr[0]=='artist_project' || $exp_fr[0]=='community_project' || $exp_fr[0]=='community_event' || $exp_fr[0]=='artist_event')
																		{
																			$sql_f_ar = mysql_query("SELECT * FROM $exp_fr[0] WHERE id='".$exp_fr[1]."'");
																		}
																		
																		if(isset($sql_f_ar))
																		{
																			if($sql_f_ar!="" && $sql_f_ar!=NULL)
																			{
																			if(mysql_num_rows($sql_f_ar)>0)
																			{
																				$ans_f_ar = mysql_fetch_assoc($sql_f_ar);
																				if(isset($ans_f_ar['name']))
																				{
																					$end_f = $ans_f_ar['name'];
																				}
																				else
																				{
																					$end_f = $ans_f_ar['title'];
																				}
																				if($ans_f_ar['profile_url']!="" && !empty($ans_f_ar['profile_url']))
																				{
																					$ecehos .= " From  <a href='/".$ans_f_ar['profile_url']."'><b>".$end_f."</b></a>";
																				}
																				else
																				{
																					$ecehos .= " From  <b>".$end_f."</b>";
																				}
																			}
																			else
																			{
																				$ecehos .= " From  <b>".$end_f."</b>";
																			}
																			}
																			else
																			{
																				$ecehos .= " From  <b>".$end_f."</b>";
																			}
																		}
																		else
																		{
																			$ecehos .= " From  <b>".$end_f."</b>";
																		}
																	}
																	echo $ecehos;
																				}
																			}
																			elseif($exp_meds[1]=='reg_art' || $exp_meds[1]=='reg_com')
																			{
																				if($ans_all_posts[$po_all]['profile_type']=='images_post')
																				{
																					echo $sql_meds['gallery_title'];
																				}
																				elseif($ans_all_posts[$po_all]['profile_type']=='songs_post')
																				{
																					echo $sql_meds['audio_name'];
																				}
																				elseif($ans_all_posts[$po_all]['profile_type']=='videos_post')
																				{
																					echo "<a href=javascript:void(0) onclick=showPlayer('v','".$sql_meds['id']."','playlist')>".$sql_meds['video_name']."</a>";
																				}
																			}
		?>
																		<div style="color:#808080;font-size:12px;">
																			<div class="comment more">
																				<?php echo $ans_all_posts[$po_all]['description']; ?>
																			</div>
																		</div>
																	</div>
																					
																	<!--<div class="activityContent">Tagged: <?php //echo $medias['tagged_users']; 
																		//$name_org = explode(')',$medias['tagged_users']);
																		//for($i_name_org=0;$i_name_org<count($name_org);$i_name_org++)
																		//{
																			//	$end_pos = strpos($name_org[$i_name_org],'(');
																			//	$org_name = substr($name_org[$i_name_org],0,$end_pos);
																			//		echo $org_name;
																		//	}
																		?></div>-->
																</div>
															</div>
		<?php
														}
		?>
													</div>
												</div>
								
												<div class="rightSide">
													<a href="#" onmouseover="mopen('mm<?php echo $po_all;?>1')" onmouseout="mclosetime()"><img src="images/profile/arrow.png" onmouseover="this.src='images/profile/arrow-down.png'" onmouseout="this.src='images/profile/arrow.png'" /></a>
											
													<div id="mm<?php echo $po_all;?>1" class="popUp" onmouseover="mcancelclosetime()" onmouseout="mclosetime()">
														<ul>
															<li><a href="hideposts.php?id=<?php echo $ans_all_posts[$po_all]['id'];?>">Hide Feed</a></li>
															<li><a href="hideposts.php?report_id=<?php echo $ans_all_posts[$po_all]['id'];?>">Report spam</a></li>
															<li><div class="seperator"></div></li>
															<li><!--<a href="Unsubscribe_feeds.php?all=1&user_ids=<?php echo $gens['general_user_id']; ?>" onclick="return confirm_subscription_delete('<?php if($art==1){ echo $ros['name']; } elseif($com==1){ echo $ros_com['name']; } ?>')">Unsubscribe from <?php if($art==1)
															{
																echo $ros['name'];
															}
															elseif($com==1)
															{
																echo $ros_com['name'];
															} ?></a>-->
															<a href="profileedit.php#subscrriptions" >Unsubscribe from <?php if($art==1)
															{
																echo $ros['name'];
															}
															elseif($com==1)
															{
																echo $ros_com['name'];
															} ?></a>
															</li>
														</ul>
													</div>
												</div>
											</div>
																
		<?php
										}
									}
									elseif($ans_all_posts[$po_all]['display_to']=='Fans')
									{
										if($gens!="")
										{
											$fans_all =	$sql_feeds->fans_cond($chk_sql['email'],$gens['community_id'],$gens['artist_id']);
											if($fans_all=='1')
											{
												$hide9 = 0;
												$get_gen_id = $sql_feeds->get_general_info();
												$hide_by = $ans_all_posts[$po_all]['hidden_by'];
												$exp_hide_by = explode(",",$hide_by);
												for($exp_count=0;$exp_count<=count($exp_hide_by);$exp_count++)
												{
													if($exp_hide_by[$exp_count] ==$get_gen_id['general_user_id'])
													{
														$hide9 =1;
													}
												}
												if($hide9 != 1)
												{
			?>
													<div class="storyCont">
														<div class="leftSide">
															<div class="avatar">
																<?php
													$art = 0;
													
													if($gens['artist_id']!=0)
													{
														$sql_art = mysql_query("SELECT * FROM general_artist WHERE artist_id='".$gens['artist_id']."' AND status=0 AND active=0 AND del_status=0");
														if($sql_art!="" && $sql_art!=NULL)
														{
															if(mysql_num_rows($sql_art)>0)
															{
																$ros = mysql_fetch_assoc($sql_art);
																$art = 1;
															}
														}
													}
													
													$com = 0;
													if($gens['community_id']!=0)
													{
														$sql_com = mysql_query("SELECT * FROM general_community WHERE community_id='".$gens['community_id']."' AND status=0 AND active=0 AND del_status=0");
														if($sql_com!="" && $sql_com!=NULL)
														{
															if(mysql_num_rows($sql_com)>0)
															{
																$ros_com = mysql_fetch_assoc($sql_com);
																$com = 1;
															}
														}
													}
													
													if($art==1)
													{
														if($ros['profile_url']!="")
														{
													?>
														<a href="/<?php echo $ros['profile_url']; ?>">
													<?php
														}
														$get_news_general_user_id = $uk -> get_general_user_idby_artistid($ros['artist_id']);
													?>
														<img width="50" height="50" src="<?php
														if($ros['image_name']!="" && !empty($ros['image_name']))
														{
															echo $s3->getNewURI_otheruser("artjcropprofile",$get_news_general_user_id) . $ros['image_name']; 
														}
														else
														{
															echo "http://artjcropprofile.s3.amazonaws.com/Noimage.png";
														}
													?>" />
													<?php
													if($ros['profile_url']!="")
														{
													?>
															</a>
													<?php
														}
													
													}
													elseif($com==1)
													{
														if($ros_com['profile_url']!="")
														{
													?>
														<a href="/<?php echo $ros_com['profile_url']; ?>">
													<?php
														}
														$get_news_general_user_id = $uk -> get_general_user_idby_communityid($ros_com['community_id']);
													?>
														<img width="50" height="50" src="<?php
													
													
													if($ros_com['image_name']!="" && !empty($ros_com['image_name']))
													{
														echo $s3->getNewURI_otheruser("comjcropprofile",$get_news_general_user_id) . $ros_com['image_name']; 
													}
													else
													{
														echo "http://comjcropprofile.s3.amazonaws.com/Noimage.png";
													}
													
													?>" />
													<?php
													if($ros_com['profile_url']!="")
														{
														?>
															</a>
														<?php
														}
													
													}
													?>
															</div>
															<?php //echo "table=".$reg_table_media_type;
																	//echo $ans_audio_feeds[$audio_fed_lo]['id'];
			?>
															<div class="storyContainer">
																<div class="storyTitle">
																	<span class="userName"><?php 
																	if($art==1)
															{
																if($ros['profile_url']!="")
														{
													?>
														<a href="/<?php echo $ros['profile_url']; ?>">
													<?php
														}
																echo $ros['name'];
																if($ros['profile_url']!="")
														{
													?>
														</a>
													<?php
														}
															}
															elseif($com==1)
															{
																if($ros_com['profile_url']!="")
														{
													?>
														<a href="/<?php echo $ros_com['profile_url']; ?>">
													<?php
														}
																echo $ros_com['name'];
																if($ros_com['profile_url']!="")
														{
													?>
														</a>
													<?php
														}
															} ?></span>  <?php //echo $ans_all_posts[$po_all]['description']; ?>
																</div>
																<div class="activityCont">
																	<div class="sideL">
																		<div class="storyImage" style="position:relative;">
																		<?php
																		if($sql_meds['media_type']==115 || $ans_all_posts[$po_all]['profile_type']=='videos_post')
																		{
																			if($sql_meds['media_type']==115)
																			{
																				$tables = 'media_video';
																				$get_video_image = $sql_feeds->getvideoimage($tables,$sql_meds['id']);
																			}
																			elseif($ans_all_posts[$po_all]['profile_type']=='videos_post' && $exp_meds[1]=='reg_art')
																			{
																				$tables = 'general_artist_video';
																				$get_video_image = $sql_feeds->getvideoimage($tables,$sql_meds['id']);
																			}
																			elseif($ans_all_posts[$po_all]['profile_type']=='videos_post' && $exp_meds[1]=='reg_com')
																			{
																				$tables = 'general_community_video';
																				$get_video_image = $sql_feeds->getvideoimage($tables,$sql_meds['id']);
																			}
																			if($get_video_image[1]=='youtube')
																			{
																				$vid_pros_edit_img = "http://img.youtube.com/vi/".$get_video_image[0]."/default.jpg";
																			}
																			else
																			{
																				$video_vim = $vimeo->call('vimeo.videos.getInfo', array('video_id' => $get_video_image[0]));
																				$vid_pros_edit_img = $video_vim->video[0]->thumbnails->thumbnail[1]->_content;
																			}
																	?>
																			<a onmouseout="newfunctionout('<?php echo $sql_meds['id']; ?>','a')" onmouseover="newfunction('<?php echo $sql_meds['id']; ?>','a')" href="javascript:void(0);" onclick="showPlayer('v','<?php echo $sql_meds['id'];?>','playlist');">
																			<img src="<?php echo $vid_pros_edit_img; ?>" width="90" height="90" />
																			</a>
																			<div class="navButton">
			<div class="playMedia">
				<a href="javascript:void(0);" class="list_play"><img title="Add file to the bottom of your playlist without stopping current media." id="play_video_image<?php echo $sql_meds['id']; ?>a" onclick="showPlayer('v','<?php echo $sql_meds['id'];?>','playlist');" src="/images/play.png" onmouseout="newfunctionout('<?php echo $sql_meds['id']; ?>','a')" onmouseover="newfunction('<?php echo $sql_meds['id']; ?>','a')" style="border-radius: 6px;"></a>
			</div>
		</div>
																	<?php
																		}
																		/* elseif($sql_meds['media_type']==114)
																		{
																			$get_audio_image = $sql_feeds->get_song_med_img($sql_meds['id']);
																			
																			if($get_audio_image!="" && !empty($get_audio_image))
																			{
																				if($get_audio_image['gallery_at']!="")
																				{
																					$get_audio__timage = $sql_feeds->get_song_med_t_img($get_audio_image['gallery_at'],$get_audio_image['gallery_id']);
																					if($get_audio_image['gallery_at']=='general_media')
																					{
																		?>
																						<img src="http://medgalthumb.s3.amazonaws.com/<?php echo $get_audio__timage; ?>" width="90" height="90" />
																		<?php
																					}
																				}
																			}
																		} */
																		elseif($sql_meds['media_type']==113)
																		{
																			$get_all_image1 = $sql_feeds->get_all_gallery_images($sql_meds['id']);
																			$play_array1 = array();
																			if($get_all_image1!="" && $get_all_image1!=NULL)
														{
																			if(mysql_num_rows($get_all_image1)>0)
																			{
																				while($row = mysql_fetch_assoc($get_all_image1))
																				{
																					$play_array1[] = $row;
																				}
																				$get_news_general_user_id = $uk -> get_general_user_idby_mediaid($sql_meds['id']);
																				${'orgPath'.$my_po}= $s3->getNewURI_otheruser("medgallery",$get_news_general_user_id);
																				${'thumbPath'.$my_po}= $s3->getNewURI_otheruser("medgalthumb",$get_news_general_user_id);
																				for($l=0;$l<count($play_array1);$l++)
																				{
																					$mediaSrc = $play_array1[0]['image_name'];
																					$filePath = $s3->getNewURI_otheruser("medgallery",$get_news_general_user_id).$mediaSrc;
																					
																					
																					${'galleryImagesData'.$my_po}[$galleryImagesDataIndex] = $play_array1[$l]['image_name'];
																					${'galleryImagestitle'.$my_po}[$galleryImagesDataIndex] = $play_array1[$l]['image_title'];
																					//echo ${'galleryImagesData'.$my_po}[$galleryImagesDataIndex];
																					$galleryImagesDataIndex++;
																				}
																				
																			}
																			}
																			if(isset(${'galleryImagesData'.$my_po}))
																			{
																				${'galleryImagesData'.$my_po} = implode(",", ${'galleryImagesData'.$my_po});
																				${'galleryImagestitle'.$my_po} = implode(",", ${'galleryImagestitle'.$my_po});
																			}
																			
																			$get_gal_image = $sql_feeds->gallery_meds($sql_meds['id']);
																			if($get_gal_image!="" && !empty($get_gal_image))
																			{
																				if($get_gal_image['profile_image']!="")
																				{
																		?>
																				<a onmouseout="newfunctionout('<?php echo $sql_meds['id']; ?>','a')" onmouseover="newfunction('<?php echo $sql_meds['id']; ?>','a')" href="javascript:void(0);" onclick="checkload('<?php echo $mediaSrc;?>','<?php echo ${'galleryImagesData'.$my_po}; ?>','<?php echo ${'thumbPath'.$my_po}; ?>','<?php echo ${'orgPath'.$my_po}; ?>','<?php echo ${'galleryImagestitle'.$my_po}; ?>','<?php echo $get_gallery['id'];?>')">
																					<img src="<?php echo $get_gal_image['profile_image']; ?>" width="90" height="90" />
																				</a>	
																		<?php
																				}
																				else
																				{
																		?>
																				<a onmouseout="newfunctionout('<?php echo $sql_meds['id']; ?>','a')" onmouseover="newfunction('<?php echo $sql_meds['id']; ?>','a')" href="javascript:void(0);" onclick="checkload('<?php echo $mediaSrc;?>','<?php echo ${'galleryImagesData'.$my_po}; ?>','<?php echo ${'thumbPath'.$my_po}; ?>','<?php echo ${'orgPath'.$my_po}; ?>','<?php echo ${'galleryImagestitle'.$my_po}; ?>','<?php echo $get_gallery['id'];?>')">
																					<img width="90" height="90" src="http://comjcropprofile.s3.amazonaws.com/<?php echo "Noimage.png";?>" />
																				</a>	
																				<?php	
																				}
																			}
																			else
																			{
																		?>
																			<a onmouseout="newfunctionout('<?php echo $sql_meds['id']; ?>','a')" onmouseover="newfunction('<?php echo $sql_meds['id']; ?>','a')" href="javascript:void(0);" onclick="checkload('<?php echo $mediaSrc;?>','<?php echo ${'galleryImagesData'.$my_po}; ?>','<?php echo ${'thumbPath'.$my_po}; ?>','<?php echo ${'orgPath'.$my_po}; ?>','<?php echo ${'galleryImagestitle'.$my_po}; ?>','<?php echo $get_gallery['id'];?>')">
																				<img width="90" height="90" src="http://comjcropprofile.s3.amazonaws.com/<?php echo "Noimage.png";?>" />
																			</a>	
																			<?php	
																		
																			}
																			$my_po = $my_po + 1;
																			?>
																				<div class="navButton">
			<div class="playMedia">
				<a href="javascript:void(0);" class="list_play"><img title="Add file to the bottom of your playlist without stopping current media." id="play_video_image<?php echo $sql_meds['id']; ?>a" onclick="checkload('<?php echo $mediaSrc;?>','<?php echo ${'galleryImagesData'.$my_po}; ?>','<?php echo ${'thumbPath'.$my_po}; ?>','<?php echo ${'orgPath'.$my_po}; ?>','<?php echo ${'galleryImagestitle'.$my_po}; ?>','<?php echo $get_gallery['id'];?>')" src="/images/play.png" onmouseout="newfunctionout('<?php echo $sql_meds['id']; ?>','a')" onmouseover="newfunction('<?php echo $sql_meds['id']; ?>','a')" style="border-radius: 6px;"></a>
			</div>
		</div>
																			<?php
																		}
																		elseif($sql_meds['media_type']==116)
																		{
																			$get_ch_image = $sql_feeds->channel_meds($sql_meds['id']);
																			if($get_ch_image!="" && !empty($get_ch_image))
																			{
																				if($get_ch_image['profile_image']!="")
																				{
																		?>
																					<img src="<?php echo $get_ch_image['profile_image']; ?>" width="90" height="90" />
																		<?php
																				}
																				else
																				{
																		?>
																					<img width="90" height="90" src="http://comjcropprofile.s3.amazonaws.com/<?php echo "Noimage.png";?>" />
																				<?php	
																				}
																			}
																			else
																			{
																		?>
																				<img width="90" height="90" src="http://comjcropprofile.s3.amazonaws.com/<?php echo "Noimage.png";?>" />
																			<?php	
																			}
																		}
																		elseif($exp_meds[1]=='reg_art' && $ans_all_posts[$po_all]['profile_type']=='images_post')
																		{
																			$tables = 'artist';
																			$imgs = $sql_feeds->reg_gal($tables,$sql_meds['gallery_id']);
																			
																			if($imgs!="" && !empty($imgs))
																			{
																				if($imgs['image_name']!="")
																				{
																			?>
																					<img width="90" height="90" src="http://reggalthumb.s3.amazonaws.com/<?php echo $imgs['image_name'];?>" />
																				<?php	
																				}
																				else
																				{
																			?>
																					<img width="90" height="90" src="http://comjcropprofile.s3.amazonaws.com/<?php echo "Noimage.png";?>" />
																				<?php	
																				}
																			}
																			else
																			{
																			?>
																				<img width="90" height="90" src="http://comjcropprofile.s3.amazonaws.com/<?php echo "Noimage.png";?>" />
																			<?php	
																			}
																		}
																		elseif($exp_meds[1]=='reg_com' && $ans_all_posts[$po_all]['profile_type']=='images_post')
																		{
																			$tables = 'community';
																			$imgs = $sql_feeds->reg_gal($tables,$sql_meds['gallery_id']);
																			
																			if($imgs!="" && !empty($imgs))
																			{
																				if($imgs['image_name']!="")
																				{
																			?>
																					<img width="90" height="90" src="http://reggalthumb.s3.amazonaws.com/<?php echo $imgs['image_name'];?>" />
																				<?php	
																				}
																				else
																				{
																			?>
																					<img width="90" height="90" src="http://comjcropprofile.s3.amazonaws.com/<?php echo "Noimage.png";?>" />
																				<?php	
																				}
																			}
																			else
																			{
																			?>
																				<img width="90" height="90" src="http://comjcropprofile.s3.amazonaws.com/<?php echo "Noimage.png";?>" />
																			<?php	
																			}
																		}
																		elseif($ans_all_posts[$po_all]['profile_type']=='events_post' || $ans_all_posts[$po_all]['profile_type']=='projects_post')
																		{
																			if($sql_meds['profile_url']!="") {
		?>																				
																					<a href="/<?php echo $sql_meds['profile_url']; ?>">
		<?php
																		}
																		?>
																			<img width="90" height="90" src="<?php if($sql_meds['image_name']!="") { echo $sql_meds['image_name']; } else { echo "http://comjcropprofile.s3.amazonaws.com/Noimage.png"; }?>" />
																		<?php	
																		if($sql_meds['profile_url']!="") {
		?>																				
																					</a>
		<?php
																		}
																		}
																		?>
																	</div>
																	<?php
																	if($sql_meds['media_type']!=113)
																	{
																	?>
																	<div class="player">
																	<?php 
																		if($sql_meds['media_type']==115)
																		{
																			include_once("findsale_video.php");
																		?>
																			<img src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" onclick="showPlayer('v','<?php echo $sql_meds['id'];?>','playlist');">
																			<img src="images/profile/add.gif" onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'" onclick="showPlayer('v','<?php echo $sql_meds['id'];?>','addvi');">
																			<?php
																			if($sql_meds['sharing_preference'] == 5 || $sql_meds['sharing_preference'] == 2)
																			{
																				$get_video_image = $sql_feeds->getvideoimage_feature($sql_meds['id']);
																				//$image_name_cart = "https://img.youtube.com/vi/".$get_video_image."/default.jpg";
																				if($get_video_image[1]=='youtube')
																			{
																				$image_name_cart = "http://img.youtube.com/vi/".$get_video_image[0]."/default.jpg";
																			}
																			else
																			{
																				$video_vim = $vimeo->call('vimeo.videos.getInfo', array('video_id' => $get_video_image[0]));
																				$image_name_cart = $video_vim->video[0]->thumbnails->thumbnail[1]->_content;
																			}
																				$free_down_rel_video="";
																				for($find_down=0;$find_down<=count($get_vids_pnew_w);$find_down++)
																				{
																					if(isset($sql_meds) && $sql_meds != null){
																						if($get_vids_pnew_w[$find_down]['id']==$sql_meds['id']){
																							$free_down_rel_video = "yes";
																						}
																						else{
																							if($free_down_rel_video =="" || $free_down_rel_video =="no"){
																								$free_down_rel_video = "no";
																							}
																						}
																					}
																				}
																				if($free_down_rel_video == "yes"){
																				 ?>
																					<!--<a href="javascript:void(0);" onclick="download_directly_song('<?php echo $sql_meds['id']; ?>','<?php echo str_replace("'","\'",$sql_meds['title']); ?>','<?php echo str_replace("'","\'",$sql_meds['creator']); ?>','<?php echo str_replace("'","\'",$sql_meds['from']); ?>')"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>-->
																			 <?php
																				}
																				else if($sql_meds['sharing_preference']==2){
																					if($sql_meds['id']!=""){
																					?>
																					<!--<a href="javascript:void(0);" onclick="ask_to_donate('<?php echo str_replace("'","\'",$sql_meds['title']);?>','<?php echo str_replace("'","\'",$sql_meds['creator']);?>','<?php echo $sql_meds['id'];?>','download_song<?php echo $sql_meds['id'];?>')"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>-->
																				<?php
																					}else{
																					?>
																					<!--<a href="javascript:void(0);" onclick="ask_to_donate('<?php echo str_replace("'","\'",$sql_meds['title']);?>','<?php echo str_replace("'","\'",$sql_meds['creator']);?>','<?php echo $sql_meds['id'];?>','download_song<?php echo $sql_meds['id'];?>')"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>-->
																				<?php
																					}
																				}
																				else{
																			?>
																				<!--<a href="javascript:void(0);" onclick="down_video_pop(<?php echo $sql_meds['id'];?>)"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>-->
																			<?php
																				}
																			}else{
																			?>
																				<!--<img src="../images/profile/download-over.gif" style="cursor:default"/>-->
																			<?php
																			}
																			?>
																			<!--<a href="addtocart.php?seller_id=<?php echo $sql_meds['general_user_id']; ?>&media_id=<?php echo $sql_meds['id']; ?>&buyer_id=<?php echo $_SESSION['gen_user_id'];?>&title=<?php echo str_replace("'","\'",$sql_meds['title']);?>&image_name=<?php echo $image_name_cart;?>" id="download_video<?php echo $sql_meds['id'];?>" style="display:none;"></a>-->
																			<script type="text/javascript">
																			$(document).ready(function() {
																					$("#download_video<?php echo $sql_meds['id'];?>").fancybox({
																					'height'			: '75%',
																					'width'				: '69%',
																					'transitionIn'		: 'none',
																					'transitionOut'		: 'none',
																					'type'				: 'iframe'
																					});
																			});
																			</script>
																			<?php
																		}/*else if($sql_meds['media_type']==113)
																		{
																		?>
																			<img src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'">
																			<img src="images/profile/add.gif" onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'">
																			<img style="cursor: default;" src="images/profile/download-over.gif">
																		<?php
																		
																		}*/else if($sql_meds['media_type']==114)
																		{
																			include_once("findsale_song.php");
																		?>
																			<img src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" onclick="showPlayer('a','<?php echo $sql_meds['id'];?>','play');">
																			<img src="images/profile/add.gif" onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'" onclick="showPlayer('a','<?php echo $sql_meds['id'];?>','add');">
																		<?php
																			if($sql_meds['sharing_preference']==5 || $sql_meds['sharing_preference']==2)
																			{
																				$song_image_name_cart = $sql_feeds->get_audio_img($sql_meds['id']);
																				$free_down_rel ="";
																				for($find_down=0;$find_down<=count($get_songs_pnew_w);$find_down++)
																				{
																					if(isset($sql_meds) && $sql_meds != null)
																					{
																						if($get_songs_pnew_w[$find_down]['id']==$sql_meds['id']){
																							$free_down_rel = "yes";
																						}
																						else{
																							if($free_down_rel =="" || $free_down_rel =="no"){
																								$free_down_rel = "no";
																							}
																						}
																					}
																				}
																				if($free_down_rel == "yes"){
																				 ?>
																				<!-- <a href="javascript:void(0);" onclick="download_directly_song('<?php echo $sql_meds['id'];?>','<?php echo str_replace("'","\'",$sql_meds['title']);?>','<?php echo str_replace("'","\'",$sql_meds['creator']); ?>','<?php echo str_replace("'","\'",$sql_meds['from']); ?>')"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>-->
																			 <?php
																				}
																				else if($sql_meds['sharing_preference']==2){
																					if($sql_meds['id']!=""){
																					?>
																					<!--<a href="javascript:void(0);" onclick="ask_to_donate('<?php echo str_replace("'","\'",$sql_meds['title']);?>','<?php echo str_replace("'","\'",$sql_meds['creator']);?>','<?php echo $sql_meds['id'];?>','download_song<?php echo $sql_meds['id'];?>')"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>-->
																				<?php
																					}else{
																					?>
																					<!--<a href="javascript:void(0);" onclick="ask_to_donate('<?php echo str_replace("'","\'",$sql_meds['title']);?>','<?php echo str_replace("'","\'",$sql_meds['creator']);?>','<?php echo $sql_meds['id'];?>','download_song<?php echo $sql_meds['id'];?>')"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>-->
																				<?php
																					}
																				}
																				else{
																			?>
																				<!--<a href="javascript:void(0);" onclick="down_song_pop(<?php echo $sql_meds['id'];?>)"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>-->
																			<?php
																				}
																			}else{
																			?>	
																			<!--<img src="../images/profile/download-over.gif" style="cursor:default"/>-->
																			<?php
																			}
																			?>
																			<!--<a href="addtocart.php?seller_id=<?php echo $sql_meds['general_user_id']; ?>&media_id=<?php echo $sql_meds['id'] ?>&buyer_id=<?php echo $_SESSION['gen_user_id'];?>&title=<?php echo str_replace("'","\'",$sql_meds['title']); ?>&image_name=<?php echo $song_image_name_cart;?>" id="download_song<?php echo $sql_meds['id'];?>" style="display:none;"></a>-->
																				<script type="text/javascript">
																				$(document).ready(function() {
																						$("#download_song<?php echo $sql_meds['id'];?>").fancybox({
																						'width'				: '75%',
																						'height'			: '75%',
																						'transitionIn'		: 'none',
																						'transitionOut'		: 'none',
																						'type'				: 'iframe'
																						});
																				});
																				</script>
																			<?php
																		}else if($sql_meds['featured_media']!="" || $sql_meds['featured_media']!="nodisplay"){
																			if($sql_meds['featured_media'] == "Song" && $sql_meds['media_id'] != "0"){
																				include_once("findsale_song.php");
																			?>
																				<img src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" onclick="showPlayer('a','<?php echo $sql_meds['media_id'];?>','play');" />
																				<img src="images/profile/add.gif" onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'" onclick="showPlayer('a','<?php echo $sql_meds['media_id'];?>','add');" />
																				<?php
																				$get_media_info_down_gal = $sql_feeds ->get_media_details($sql_meds['media_id']);
																				if($get_media_info_down_gal['sharing_preference']==5 || $get_media_info_down_gal['sharing_preference']==2)
																				{
																					$song_image_name_cartproject = $sql_feeds->get_audio_img($get_media_info_down_gal['id']);
																					$free_down_rel ="";
																					for($find_down=0;$find_down<=count($get_songs_pnew_w);$find_down++)
																					{
																						//echo $get_songs_pnew_w[$find_down]['id'] ."==". $get_Song_title['id']."</br>";
																						if(isset($get_media_info_down_gal) && $get_media_info_down_gal != null)
																						{
																							if($get_songs_pnew_w[$find_down]['id']==$get_media_info_down_gal['id']){
																							$free_down_rel = "yes";
																							}
																							else{
																								if($free_down_rel =="" || $free_down_rel =="no"){
																									$free_down_rel = "no";
																								}
																							}
																						}
																					}
																					if($free_down_rel == "yes"){
																					 ?>
																					 <!--<a href="javascript:void(0);" onclick="download_directly_song('<?php echo $get_media_info_down_gal['id'];?>','<?php echo str_replace("'","\'",$get_media_info_down_gal['title']);?>','<?php echo str_replace("'","\'",$get_media_info_down_gal['creator']);?>','<?php echo str_replace("'","\'",$get_media_info_down_gal['from']);?>')"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>-->
																					<?php
																					}
																					else if($get_media_info_down_gal['sharing_preference']==2){
																					 ?>
																					<!--<a href="javascript:void(0);" onclick="ask_to_donate('<?php echo str_replace("'","\'",$get_media_info_down_gal['title']);?>','<?php echo str_replace("'","\'",$get_media_info_down_gal['creator']);?>','<?php echo $get_media_info_down_gal['id'];?>','download_songproject<?php echo $sql_meds['media_id'];?>')"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>-->
																				 <?php
																					}
																					else{
																				?>
																					<!--<a href="javascript:void(0);" onclick="down_song_pop_project(<?php echo $sql_meds['media_id'];?>)"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>-->
																				<?php
																					}
																				}else{
																				?>	
																				<!--<img src="../images/profile/download-over.gif" style="cursor:default"/>-->
																				<?php
																				}
																				?>
																				<!--<a href="addtocart.php?seller_id=<?php echo $get_media_info_down_gal['general_user_id'];?>&media_id=<?php echo $get_media_info_down_gal['id'];?>&buyer_id=<?php echo $_SESSION['gen_user_id'];?>&title=<?php echo str_replace("'","\'",$get_media_info_down_gal['title']);?>&image_name=<?php echo $song_image_name_cartproject;?>" id="download_songproject<?php echo $sql_meds['media_id'];?>" style="display:none;"></a>-->
																				<script type="text/javascript">
																				$(document).ready(function() {
																						$("#download_songproject<?php echo $sql_meds['media_id'];?>").fancybox({
																						'width'				: '75%',
																						'height'			: '75%',
																						'transitionIn'		: 'none',
																						'transitionOut'		: 'none',
																						'type'				: 'iframe'
																						});
																				});
																				</script>
																			<?php
																			}if($sql_meds['featured_media'] == "Gallery" && $sql_meds['media_id'] != "0"){
																				$get_all_feature_image1 = $sql_feeds->get_all_gallery_images($sql_meds['media_id']);
																				if($get_all_feature_image1!="" && $get_all_feature_image1!=NULL)
														{
																				if(mysql_num_rows($get_all_feature_image1)>0)
																				{
																					$galleryImagesData1="";
																					$galleryImagestitle1 ="";
																					$mediaSrc1 ="";
																					$get_news_general_user_id = $uk -> get_general_user_idby_mediaid($sql_meds['media_id']);
																					while($row = mysql_fetch_assoc($get_all_feature_image1))
																					{
																						$mediaSrc1 = $row[0]['image_name'];
																						$filePath = $s3->getNewURI_otheruser("medgallery",$get_news_general_user_id).$mediaSrc1;
																						$galleryImagesData1 = $galleryImagesData1 .",". $row['image_name'];
																						$galleryImagestitle1 = $galleryImagestitle1 .",".$row['image_title'];
																					}
																					$orgPath1 = $s3->getNewURI_otheruser("medgallery",$get_news_general_user_id);
																					$thumbPath1 = $s3->getNewURI_otheruser("medgalthumb",$get_news_general_user_id);
																				}
																				}
																			?>
																				<a href="javascript:void(0);" onclick="checkload('<?php echo $mediaSrc1;?>','<?php echo $galleryImagesData1; ?>','<?php echo $thumbPath1; ?>','<?php echo $orgPath1; ?>','<?php echo $galleryImagestitle1; ?>','<?php echo $get_gallery['id'];?>')">
																				<img src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'"></a>
																				<img src="images/profile/add.gif" onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'">
																				<?php
																				$get_media_info_down_gal = $sql_feeds ->get_media_details($sql_meds['media_id']);
																				$get_gal_image_project  = $sql_feeds ->get_gallery_info($sql_meds['media_id']);
																				if($get_media_info_down_gal['sharing_preference']==2)
																				{
																				?>
																					<!--<a href="javascript:void(0);" onclick="ask_to_donate('<?php echo str_replace("'","\'",$get_media_info_down_gal['title']);?>','<?php echo str_replace("'","\'",$get_media_info_down_gal['creator']);?>','<?php echo $get_media_info_down_gal['id'];?>','download_galproject<?php echo $sql_meds['media_id'];?>')"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>-->
																				 <?php
																				}else{
																				?>
																				<!--<img src="../images/profile/download-over.gif" style="cursor:default"/>-->
																				<?php
																				}
																				?>
																				<!--<a href="addtocart.php?seller_id=<?php echo $get_media_info_down_gal['general_user_id'];?>&media_id=<?php echo $get_media_info_down_gal['id'];?>&buyer_id=<?php echo $_SESSION['gen_user_id'];?>&title=<?php echo str_replace("'","\'",$get_media_info_down_gal['title']);?>&image_name=<?php echo $get_gal_image_project['profile_image'];?>" id="download_galproject<?php echo $sql_meds['media_id'];?>" style="display:none;"></a>-->
																				<script type="text/javascript">
																				$(document).ready(function() {
																						$("#download_galproject<?php echo $sql_meds['media_id'];?>").fancybox({
																						'width'				: '75%',
																						'height'			: '75%',
																						'transitionIn'		: 'none',
																						'transitionOut'		: 'none',
																						'type'				: 'iframe'
																						});
																				});
																				</script>
																			<?php
																			}if($sql_meds['featured_media'] == "Video" && $sql_meds['media_id'] != "0"){
																				include_once("findsale_video.php");
																			?>
																				<img src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" onclick="showPlayer('v','<?php echo $sql_meds['media_id'];?>','playlist');"/>
																				<img src="images/profile/add.gif" onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'" onclick="showPlayer('v','<?php echo $sql_meds['media_id'];?>','addvi');"/>
																				<?php
																				$get_media_info_down_gal = $sql_feeds ->get_media_details($sql_meds['media_id']);
																				if($get_media_info_down_gal['sharing_preference']==5 || $get_media_info_down_gal['sharing_preference']==2)
																				{
																					$get_video_image = $sql_feeds->getvideoimage_feature($get_media_info_down_gal['id']);
																					//$image_name_cartproject = "https://img.youtube.com/vi/".$get_video_image."/default.jpg";
																					
																					
																				if($get_video_image[1]=='youtube')
																			{
																				$image_name_cartproject = "http://img.youtube.com/vi/".$get_video_image[0]."/default.jpg";
																			}
																			else
																			{
																				$video_vim = $vimeo->call('vimeo.videos.getInfo', array('video_id' => $get_video_image[0]));
																				$image_name_cartproject = $video_vim->video[0]->thumbnails->thumbnail[1]->_content;
																			}
																			$free_down_rel_video="";
																					for($find_down=0;$find_down<=count($get_vids_pnew_w);$find_down++)
																					{
																						if(isset($get_media_info_down_gal) && $get_media_info_down_gal != null){
																							if($get_vids_pnew_w[$find_down]['id']==$get_media_info_down_gal['id']){
																								$free_down_rel_video = "yes";
																							}
																							else{
																								if($free_down_rel_video =="" || $free_down_rel_video =="no"){
																									$free_down_rel_video = "no";
																								}
																							}
																						}
																					}
																					if($free_down_rel_video == "yes"){
																					 ?>
																					 <!--<a href="javascript:void(0);" onclick="download_directly_song('<?php echo $get_media_info_down_gal['id'];?>','<?php echo str_replace("'","\'",$get_media_info_down_gal['title']);?>','<?php echo str_replace("'","\'",$get_media_info_down_gal['creator']);?>','<?php echo str_replace("'","\'",$get_media_info_down_gal['from']);?>')"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>
																					 -->
																				 <?php
																					}
																					else if($get_media_info_down_gal['sharing_preference']==2){
																					 ?>
																						<!--<a href="javascript:void(0);" onclick="ask_to_donate('<?php echo str_replace("'","\'",$get_media_info_down_gal['title']);?>','<?php echo str_replace("'","\'",$get_media_info_down_gal['creator']);?>','<?php echo $get_media_info_down_gal['id'];?>','download_videoproject<?php echo $sql_meds['media_id'];?>')"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>-->
																					<?php
																					}
																					else{
																				?>
																					<!--<a href="javascript:void(0);" onclick="down_video_pop_project(<?php echo $sql_meds['media_id'];?>)"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>-->
																				<?php
																					}
																				}
																				else{
																				?>
																				<!--<img src="../images/profile/download-over.gif" style="cursor:default"/>-->
																				<?php
																				}
																				?>
																				<!--<a href="addtocart.php?seller_id=<?php echo $get_media_info_down_gal['general_user_id'];?>&media_id=<?php echo $get_media_info_down_gal['id'];?>&buyer_id=<?php echo $_SESSION['gen_user_id'];?>&title=<?php echo str_replace("'","\'",$get_media_info_down_gal['title']);?>&image_name=<?php echo $image_name_cartproject;?>" id="download_videoproject<?php echo $sql_meds['media_id'];?>" style="display:none;"></a>-->
																				<script type="text/javascript">
																				$(document).ready(function() {
																						$("#download_videoproject<?php echo $sql_meds['media_id'];?>").fancybox({
																						'height'			: '75%',
																						'width'				: '69%',
																						'transitionIn'		: 'none',
																						'transitionOut'		: 'none',
																						'type'				: 'iframe'
																						});
																				});
																				</script>
																			<?php
																			}
																		}
																		else{
																		?>
																			<img src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'">
																			<img src="images/profile/add.gif" onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'">
																			<!--<img style="cursor: default;" src="images/profile/download-over.gif">-->
																		<?php
																		}
																	?>
																		
																	</div>	
																	<?php
																	}
																	?>
																		<!--<div class="player">
																			<img src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'">
																			<img src="images/profile/add.gif" onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'">
																			<!--<img src="images/profile/download.gif" onmouseover="this.src='images/profile/download-over.gif'" onmouseout="this.src='images/profile/download.gif'">
																			<img style="cursor: default;" src="images/profile/download-over.gif">
																		</div>-->
																	</div>
																	
																	<div class="sideR">
																		<div class="activityTitle"><?php
																			if($exp_meds[1]=='reg_med' || $ans_all_posts[$po_all]['profile_type']=='projects_post' || $ans_all_posts[$po_all]['profile_type']=='events_post')
																			{
																				if($ans_all_posts[$po_all]['profile_type']=='projects_post' || $ans_all_posts[$po_all]['profile_type']=='events_post' || $sql_meds['media_type']!=114)
																				{
																					if($sql_meds['profile_url']!="") {
		?>																				
																					<a href="/<?php echo $sql_meds['profile_url']; ?>">
		<?php
																		}
																					if($sql_meds['media_type']==115){
																					echo "<a href=javascript:void(0) onclick=showPlayer('v','".$sql_meds['id']."','playlist')>".$sql_meds['title']."</a>";
																					}else if($sql_meds['media_type']==113){
																						$get_all_image1 = $sql_feeds->get_all_gallery_images($sql_meds['id']);
																						$play_array1 = array();
																						if($get_all_image1!="" && $get_all_image1!=NULL)
														{
																						if(mysql_num_rows($get_all_image1)>0)
																						{
																							while($row = mysql_fetch_assoc($get_all_image1))
																							{
																								$play_array1[] = $row;
																							}
																							$get_news_general_user_id = $uk -> get_general_user_idby_mediaid($sql_meds['id']);
																							${'orgPath'.$my_po}= $s3->getNewURI_otheruser("medgallery",$get_news_general_user_id);
																							${'thumbPath'.$my_po}= $s3->getNewURI_otheruser("medgalthumb",$get_news_general_user_id);
																							for($l=0;$l<count($play_array1);$l++)
																							{
																								$mediaSrc = $play_array1[0]['image_name'];
																								$filePath = $s3->getNewURI_otheruser("medgallery",$get_news_general_user_id).$mediaSrc;
																								
																								
																								${'galleryImagesData'.$my_po}[$galleryImagesDataIndex] = $play_array1[$l]['image_name'];
																								${'galleryImagestitle'.$my_po}[$galleryImagesDataIndex] = $play_array1[$l]['image_title'];
																								//echo ${'galleryImagesData'.$my_po}[$galleryImagesDataIndex];
																								$galleryImagesDataIndex++;
																							}
																							
																						}
																						}
																						if(isset(${'galleryImagesData'.$my_po}))
																						{
																							${'galleryImagesData'.$my_po} = implode(",", ${'galleryImagesData'.$my_po});
																							${'galleryImagestitle'.$my_po} = implode(",", ${'galleryImagestitle'.$my_po});
																						}
																						
																						echo "<a href=javascript:void(0) onclick=checkload('".$mediaSrc."','". ${'galleryImagesData'.$my_po}."','". ${'thumbPath'.$my_po}."','". ${'orgPath'.$my_po}."','". ${'galleryImagestitle'.$my_po}."','". $get_gallery['id']."')>".$sql_meds['title']."</a>";
																						$my_po = $my_po + 1;
																					}else{
																					echo $sql_meds['title'];
																					}
																					if($sql_meds['profile_url']!="") {
		?>																				
																					</a>
		<?php
																		}
																				}
																				elseif($exp_meds[1]=='reg_med' && $sql_meds['media_type']==114)
																				{
																					$sql_me = mysql_query("SELECT * FROM media_songs WHERE media_id='".$sql_meds['id']."'");
																					$ans_me = mysql_fetch_assoc($sql_me);
																					$ecehos = '<a style="text-decoration: none; cursor: text;" href="javascript:void(0)"> '.$ans_me['track'].' </a>';
																					if($sql_meds['media_type']==114)
																					{
																						//$ecehos .= "<a href=javascript:void(0) onclick=showPlayer('a',".$sql_meds['id'].",'add');>".$sql_meds['title']."</a>";
																						$ecehos .= "<a style='text-decoration: none; cursor: text;' href='javascript:void(0)'>".$sql_meds['title']."</a>";
																						
																					}
																					else if($sql_meds['media_type']==115)
																					{
																						$ecehos .= "<a href=javascript:void(0) onclick=showPlayer('v',".$sql_meds['id'].",'addvi');>".$sql_meds['title']."</a>";
																					}
																					else if($sql_meds['media_type']==113)
																					{
																						$get_all_gal_image = $sql_feeds->get_all_gallery_images($sql_meds['id']);
																						if($get_all_gal_image!="" && $get_all_gal_image!=NULL)
														{
																						if(mysql_num_rows($get_all_gal_image)>0)
																						{
																							$galleryImagesData ="";
																							$galleryImagestitle ="";
																							$mediaSrc ="";
																							$get_news_general_user_id = $uk -> get_general_user_idby_mediaid($sql_meds['id']);
																							while($row = mysql_fetch_assoc($get_all_gal_image))
																							{
																								$mediaSrc = $row[0]['image_name'];
																								$filePath = $s3->getNewURI_otheruser("medgallery",$get_news_general_user_id).$mediaSrc;
																								if($row['image_name']!=""){
																									$galleryImagesData = $galleryImagesData .",". $row['image_name'];
																									$galleryImagestitle = $galleryImagestitle .",".$row['image_title'];
																								}
																							}
																							$orgPath = $s3->getNewURI_otheruser("medgallery",$get_news_general_user_id);
																							$thumbPath = $s3->getNewURI_otheruser("medgalthumb",$get_news_general_user_id);
																						}
																						}
																						$ecehos .= "<a href=javascript:void(0) onclick=checkload('".$mediaSrc."','".$galleryImagesData."','".$thumbPath."','".$orgPath."','". $galleryImagestitle."','".$get_gallery['id'].")>".$sql_meds['title']."</a>";
																						$galleryImagesData ="";
																						$galleryImagestitle ="";
																						$mediaSrc ="";
																					}
																					else
																					{
																						$ecehos .= '<a href="">'.$sql_meds['title'].'</a>';
																					}
																					//$ecehos .= '<a href="">'.$sql_meds['title'].'</a>';
																	
																	if($sql_meds['creator']!="")
																	{
																		$create_c = strpos($sql_meds['creator'],'(');
																		$end = substr($sql_meds['creator'],0,$create_c);
																		$exp_cr = explode('|',$sql_meds['creator_info']);
																		
																		if($exp_cr[0]=='general_artist')
																		{
																			$sql_c_ar = mysql_query("SELECT * FROM general_artist WHERE artist_id='".$exp_cr[1]."'");
																		}
																		if($exp_cr[0]=='general_community')
																		{
																			$sql_c_ar = mysql_query("SELECT * FROM general_community WHERE community_id='".$exp_cr[1]."'");
																		}
																		if($exp_cr[0]=='artist_project' || $exp_cr[0]=='community_project' || $exp_cr[0]=='community_event' || $exp_cr[0]=='artist_event')
																		{
																			$sql_c_ar = mysql_query("SELECT * FROM $exp_cr[0] WHERE id='".$exp_cr[1]."'");
																		}
																		
																		
																		if(isset($sql_c_ar))
																		{
																			if($sql_c_ar!="" && $sql_c_ar!=NULL)
														{
																			if(mysql_num_rows($sql_c_ar)>0)
																			{
																				$ans_c_ar = mysql_fetch_assoc($sql_c_ar);
																				if(isset($ans_c_ar['name']))
																				{
																					$end = $ans_c_ar['name'];
																				}
																				else
																				{
																					$end = $ans_c_ar['title'];
																				}
																				if($ans_c_ar['profile_url']!="" && !empty($ans_c_ar['profile_url']))
																				{
																					$ecehos .= " By  <a href='/".$ans_c_ar['profile_url']."'><b>".$end."</b></a>  ";
																				}
																				else
																				{
																					$ecehos .= " By  <b>".$end."</b>  ";
																				}
																			}
																			else
																			{
																				$ecehos .= " By  <b>".$end."</b>  ";
																			}
																			}
																			else
																			{
																				$ecehos .= " By  <b>".$end."</b>  ";
																			}
																		}
																		else
																		{
																			
																			$ecehos .= " By  <b>".$end."</b>  ";
																		}
																	}
																	if($sql_meds['from']!="")
																	{
																		$create_f = strpos($sql_meds['from'],'(');
																		$end_f = substr($sql_meds['from'],0,$create_f);
																		
																		$exp_fr = explode('|',$sql_meds['from_info']);
																		if($exp_fr[0]=='general_artist')
																		{
																			$sql_f_ar = mysql_query("SELECT * FROM general_artist WHERE artist_id='".$exp_fr[1]."'");
																		}
																		if($exp_fr[0]=='general_community')
																		{
																			$sql_f_ar = mysql_query("SELECT * FROM general_community WHERE community_id='".$exp_fr[1]."'");
																		}
																		if($exp_fr[0]=='artist_project' || $exp_fr[0]=='community_project' || $exp_fr[0]=='community_event' || $exp_fr[0]=='artist_event')
																		{
																			$sql_f_ar = mysql_query("SELECT * FROM $exp_fr[0] WHERE id='".$exp_fr[1]."'");
																		}
																		
																		if(isset($sql_f_ar))
																		{
																			if($sql_f_ar!="" && $sql_f_ar!=NULL)
														{
																			if(mysql_num_rows($sql_f_ar)>0)
																			{
																				$ans_f_ar = mysql_fetch_assoc($sql_f_ar);
																				if(isset($ans_f_ar['name']))
																				{
																					$end_f = $ans_f_ar['name'];
																				}
																				else
																				{
																					$end_f = $ans_f_ar['title'];
																				}
																				if($ans_f_ar['profile_url']!="" && !empty($ans_f_ar['profile_url']))
																				{
																					$ecehos .= " From  <a href='/".$ans_f_ar['profile_url']."'><b>".$end_f."</b></a>";
																				}
																				else
																				{
																					$ecehos .= " From  <b>".$end_f."</b>";
																				}
																			}
																			else
																			{
																				$ecehos .= " From  <b>".$end_f."</b>";
																			}
																			}
																			else
																			{
																				$ecehos .= " From  <b>".$end_f."</b>";
																			}
																		}
																		else
																		{
																			$ecehos .= " From  <b>".$end_f."</b>";
																		}
																	}
																	echo $ecehos;
																				}
																			}
																			elseif($exp_meds[1]=='reg_art' || $exp_meds[1]=='reg_com')
																			{
																				if($ans_all_posts[$po_all]['profile_type']=='images_post')
																				{
																					echo $sql_meds['gallery_title'];
																				}
																				elseif($ans_all_posts[$po_all]['profile_type']=='songs_post')
																				{
																					echo $sql_meds['audio_name'];
																				}
																				elseif($ans_all_posts[$po_all]['profile_type']=='videos_post')
																				{
																					echo "<a href=javascript:void(0) onclick=showPlayer('v','".$sql_meds['id']."','playlist')>".$sql_meds['video_name']."</a>";
																				}
																			}
		?>
																			<div style="color:#808080;font-size:12px;">
																				<div class="comment more">
																					<?php echo $ans_all_posts[$po_all]['description']; ?>
																				</div>
																			</div>
																		</div>
																		<!--<div class="activityContent">Tagged: <?php //echo $medias['tagged_users']; 
																			//$name_org = explode(')',$medias['tagged_users']);
																			//for($i_name_org=0;$i_name_org<count($name_org);$i_name_org++)
																			//{
																				//	$end_pos = strpos($name_org[$i_name_org],'(');
																				//	$org_name = substr($name_org[$i_name_org],0,$end_pos);
																				//		echo $org_name;
																		//	}
																		?></div>-->
																	</div>
																</div>
															</div>
														</div>
														
														<div class="rightSide">
															<a href="#" onmouseover="mopen('mm<?php echo $po_all;?>2')" onmouseout="mclosetime()"><img src="images/profile/arrow.png" onmouseover="this.src='images/profile/arrow-down.png'" onmouseout="this.src='images/profile/arrow.png'" /></a>
															<div id="mm<?php echo $po_all;?>2" class="popUp" onmouseover="mcancelclosetime()" onmouseout="mclosetime()">
																<ul>
																	<li><a href="hideposts.php?id=<?php echo $ans_all_posts[$po_all]['id'];?>">Hide Feed</a></li>
																	<li><a href="hideposts.php?report_id=<?php echo $ans_all_posts[$po_all]['id'];?>">Report spam</a></li>
																	<li><div class="seperator"></div></li>
																	<li><a href="">Unsubscribe from <?php echo $gens['fname'].' '.$gens['lname']; ?></a></li>
																</ul>
														</div>
													</div>
												</div>
		<?php
											}
										}
									}
								}
								elseif($ans_all_posts[$po_all]['display_to']=='Subscribers')
								{
									if($gens!="")
									{
										$subscribers_all =	$sql_feeds->subscribers_cond($chk_sql['email'],$gens['community_id'],$gens['artist_id']);
										if($subscribers_all=='1')
										{
											$hide10 = 0;
											$get_gen_id = $sql_feeds->get_general_info();
											$hide_by = $ans_all_posts[$po_all]['hidden_by'];
											$exp_hide_by = explode(",",$hide_by);
											for($exp_count=0;$exp_count<=count($exp_hide_by);$exp_count++)
											{
												if($exp_hide_by[$exp_count] ==$get_gen_id['general_user_id'])
												{
													$hide10 =1;
												}
											}
											if($hide10 != 1)
											{
		?>
												<div class="storyCont">
													<div class="leftSide">
														<div class="avatar">
															<?php
													$art = 0;
													
													if($gens['artist_id']!=0)
													{
														$sql_art = mysql_query("SELECT * FROM general_artist WHERE artist_id='".$gens['artist_id']."' AND status=0 AND active=0 AND del_status=0");
														if($sql_art!="" && $sql_art!=NULL)
														{
															if(mysql_num_rows($sql_art)>0)
															{
																$ros = mysql_fetch_assoc($sql_art);
																$art = 1;
															}
														}
													}
													
													
													$com = 0;
													if($gens['community_id']!=0)
													{
														$sql_com = mysql_query("SELECT * FROM general_community WHERE community_id='".$gens['community_id']."' AND status=0 AND active=0 AND del_status=0");
														if($sql_com!="" && $sql_com!=NULL)
														{
															if(mysql_num_rows($sql_com)>0)
															{
																$ros_com = mysql_fetch_assoc($sql_com);
																$com = 1;
															}
														}
													}
													
													if($art==1)
													{
														if($ros['profile_url']!="")
														{
													?>
														<a href="/<?php echo $ros['profile_url']; ?>">
													<?php
														}
														$get_news_general_user_id = $uk -> get_general_user_idby_artistid($ros['artist_id']);
													?>
														<img width="50" height="50" src="<?php
														if($ros['image_name']!="" && !empty($ros['image_name']))
														{
															echo $s3->getNewURI_otheruser("artjcropprofile",$get_news_general_user_id) . $ros['image_name']; 
														}
														else
														{
															echo "http://artjcropprofile.s3.amazonaws.com/Noimage.png";
														}
													?>" />
													<?php
													if($ros['profile_url']!="")
														{
													?>
														</a>
													<?php
														}
													}
													elseif($com==1)
													{
														if($ros_com['profile_url']!="")
														{
													?>
														<a href="/<?php echo $ros_com['profile_url']; ?>">
													<?php
														}
														$get_news_general_user_id = $uk -> get_general_user_idby_communityid($ros_com['community_id']);
													?>
														<img width="50" height="50" src="<?php
													
													
													if($ros_com['image_name']!="" && !empty($ros_com['image_name']))
													{
														echo $s3->getNewURI_otheruser("comjcropprofile",$get_news_general_user_id).$ros_com['image_name']; 
													}
													else
													{
														echo "http://comjcropprofile.s3.amazonaws.com/Noimage.png";
													}
													
													?>" />
													<?php
														if($ros_com['profile_url']!="")
														{
													?>
														</a>
													<?php
														}
													}
													?>
														</div>
														<?php //echo "table=".$reg_table_media_type;
																//echo $ans_audio_feeds[$audio_fed_lo]['id'];
		?>
														<div class="storyContainer">
															<div class="storyTitle">
																<span class="userName"><?php if($art==1)
															{
																if($ros['profile_url']!="")
														{
													?>
														<a href="/<?php echo $ros['profile_url']; ?>">
													<?php
														}
																echo $ros['name'];
																if($ros['profile_url']!="")
														{
													?>
														</a>
													<?php
														}
															}
															elseif($com==1)
															{
																if($ros_com['profile_url']!="")
														{
													?>
														<a href="/<?php echo $ros_com['profile_url']; ?>">
													<?php
														}
																echo $ros_com['name'];
																if($ros_com['profile_url']!="")
														{
													?>
														</a>
													<?php
														}
															}?></span>  <?php //echo $ans_all_posts[$po_all]['description']; ?>
														</div>
														<div class="activityCont">
															<div class="sideL">
																<div class="storyImage" style="position:relative;"><?php
																
																if($sql_meds['media_type']==115 || $ans_all_posts[$po_all]['profile_type']=='videos_post')
																		{
																			if($sql_meds['media_type']==115)
																			{
																				$tables = 'media_video';
																				$get_video_image = $sql_feeds->getvideoimage($tables,$sql_meds['id']);
																			}
																			elseif($ans_all_posts[$po_all]['profile_type']=='videos_post' && $exp_meds[1]=='reg_art')
																			{
																				$tables = 'general_artist_video';
																				$get_video_image = $sql_feeds->getvideoimage($tables,$sql_meds['id']);
																			}
																			elseif($ans_all_posts[$po_all]['profile_type']=='videos_post' && $exp_meds[1]=='reg_com')
																			{
																				$tables = 'general_community_video';
																				$get_video_image = $sql_feeds->getvideoimage($tables,$sql_meds['id']);
																			}
																			if($get_video_image[1]=='youtube')
																			{
																				$vid_pros_edit_img = "http://img.youtube.com/vi/".$get_video_image[0]."/default.jpg";
																			}
																			else
																			{
																				$video_vim = $vimeo->call('vimeo.videos.getInfo', array('video_id' => $get_video_image[0]));
																				$vid_pros_edit_img = $video_vim->video[0]->thumbnails->thumbnail[1]->_content;
																			}
																	?>
																		<a onmouseout="newfunctionout('<?php echo $sql_meds['id']; ?>','a')" onmouseover="newfunction('<?php echo $sql_meds['id']; ?>','a')" href="javascript:void(0);" onclick="showPlayer('v','<?php echo $sql_meds['id'];?>','playlist');">
																			<img src="<?php echo $vid_pros_edit_img; ?>" width="90" height="90" />
																		</a>
																		<div class="navButton">
			<div class="playMedia">
				<a href="javascript:void(0);" class="list_play"><img title="Add file to the bottom of your playlist without stopping current media." id="play_video_image<?php echo $sql_meds['id']; ?>a" onclick="showPlayer('v','<?php echo $sql_meds['id'];?>','playlist');" src="/images/play.png" onmouseout="newfunctionout('<?php echo $sql_meds['id']; ?>','a')" onmouseover="newfunction('<?php echo $sql_meds['id']; ?>','a')" style="border-radius: 6px;"></a>
			</div>
		</div>
																	<?php
																		}
																		/* elseif($sql_meds['media_type']==114)
																		{
																			$get_audio_image = $sql_feeds->get_song_med_img($sql_meds['id']);
																			
																			if($get_audio_image!="" && !empty($get_audio_image))
																			{
																				if($get_audio_image['gallery_at']!="")
																				{
																					$get_audio__timage = $sql_feeds->get_song_med_t_img($get_audio_image['gallery_at'],$get_audio_image['gallery_id']);
																					if($get_audio_image['gallery_at']=='general_media')
																					{
																		?>
																						<img src="http://medgalthumb.s3.amazonaws.com/<?php echo $get_audio__timage; ?>" width="90" height="90" />
																		<?php
																					}
																				}
																			}
																		} */
																		elseif($sql_meds['media_type']==113)
																		{
																			$get_all_image1 = $sql_feeds->get_all_gallery_images($sql_meds['id']);
																			$play_array1 = array();
																			if($get_all_image1!="" && $get_all_image1!=NULL)
																			{
																				if(mysql_num_rows($get_all_image1)>0)
																				{
																					while($row = mysql_fetch_assoc($get_all_image1))
																					{
																						$play_array1[] = $row;
																					}
																					$get_news_general_user_id = $uk -> get_general_user_idby_mediaid($sql_meds['id']);
																					${'orgPath'.$my_po}= $s3->getNewURI_otheruser("medgallery",$get_news_general_user_id);
																					${'thumbPath'.$my_po}= $s3->getNewURI_otheruser("medgalthumb",$get_news_general_user_id);
																					for($l=0;$l<count($play_array1);$l++)
																					{
																						$mediaSrc = $play_array1[0]['image_name'];
																						$filePath = $s3->getNewURI_otheruser("medgallery",$get_news_general_user_id).$mediaSrc;
																						
																						
																						${'galleryImagesData'.$my_po}[$galleryImagesDataIndex] = $play_array1[$l]['image_name'];
																						${'galleryImagestitle'.$my_po}[$galleryImagesDataIndex] = $play_array1[$l]['image_title'];
																						//echo ${'galleryImagesData'.$my_po}[$galleryImagesDataIndex];
																						$galleryImagesDataIndex++;
																					}
																					
																				}
																			}
																			if(isset(${'galleryImagesData'.$my_po}))
																			{
																				${'galleryImagesData'.$my_po} = implode(",", ${'galleryImagesData'.$my_po});
																				${'galleryImagestitle'.$my_po} = implode(",", ${'galleryImagestitle'.$my_po});
																			}
																			
																			$get_gal_image = $sql_feeds->gallery_meds($sql_meds['id']);
																			if($get_gal_image!="" && !empty($get_gal_image))
																			{
																				if($get_gal_image['profile_image']!="")
																				{
																		?>
																				<a onmouseout="newfunctionout('<?php echo $sql_meds['id']; ?>','a')" onmouseover="newfunction('<?php echo $sql_meds['id']; ?>','a')" href="javascript:void(0);" onclick="checkload('<?php echo $mediaSrc;?>','<?php echo ${'galleryImagesData'.$my_po}; ?>','<?php echo ${'thumbPath'.$my_po}; ?>','<?php echo ${'orgPath'.$my_po}; ?>','<?php echo ${'galleryImagestitle'.$my_po}; ?>','<?php echo $get_gallery['id'];?>')">
																					<img src="<?php echo $get_gal_image['profile_image']; ?>" width="90" height="90" />
																				</a>	
																		<?php
																				}
																				else
																				{
																		?>
																				<a onmouseout="newfunctionout('<?php echo $sql_meds['id']; ?>','a')" onmouseover="newfunction('<?php echo $sql_meds['id']; ?>','a')" href="javascript:void(0);" onclick="checkload('<?php echo $mediaSrc;?>','<?php echo ${'galleryImagesData'.$my_po}; ?>','<?php echo ${'thumbPath'.$my_po}; ?>','<?php echo ${'orgPath'.$my_po}; ?>','<?php echo ${'galleryImagestitle'.$my_po}; ?>','<?php echo $get_gallery['id'];?>')">
																					<img width="90" height="90" src="http://comjcropprofile.s3.amazonaws.com/<?php echo "Noimage.png";?>" />
																				</a>	
																				<?php	
																				}
																			}
																			else
																			{
																		?>
																			<a onmouseout="newfunctionout('<?php echo $sql_meds['id']; ?>','a')" onmouseover="newfunction('<?php echo $sql_meds['id']; ?>','a')" href="javascript:void(0);" onclick="checkload('<?php echo $mediaSrc;?>','<?php echo ${'galleryImagesData'.$my_po}; ?>','<?php echo ${'thumbPath'.$my_po}; ?>','<?php echo ${'orgPath'.$my_po}; ?>','<?php echo ${'galleryImagestitle'.$my_po}; ?>','<?php echo $get_gallery['id'];?>')">
																				<img width="90" height="90" src="http://comjcropprofile.s3.amazonaws.com/<?php echo "Noimage.png";?>" />
																			</a>	
																			<?php	
																		
																			}
																			$my_po = $my_po + 1;
																			?>
																			<div class="navButton">
			<div class="playMedia">
				<a href="javascript:void(0);" class="list_play"><img title="Add file to the bottom of your playlist without stopping current media." id="play_video_image<?php echo $sql_meds['id']; ?>a" onclick="checkload('<?php echo $mediaSrc;?>','<?php echo ${'galleryImagesData'.$my_po}; ?>','<?php echo ${'thumbPath'.$my_po}; ?>','<?php echo ${'orgPath'.$my_po}; ?>','<?php echo ${'galleryImagestitle'.$my_po}; ?>','<?php echo $get_gallery['id'];?>')" src="/images/play.png" onmouseout="newfunctionout('<?php echo $sql_meds['id']; ?>','a')" onmouseover="newfunction('<?php echo $sql_meds['id']; ?>','a')" style="border-radius: 6px;"></a>
			</div>
		</div>
																			<?php
																		}
																		elseif($sql_meds['media_type']==116)
																		{
																			$get_ch_image = $sql_feeds->channel_meds($sql_meds['id']);
																			if($get_ch_image!="" && !empty($get_ch_image))
																			{
																				if($get_ch_image['profile_image']!="")
																				{
																		?>
																					<img src="<?php echo $get_ch_image['profile_image']; ?>" width="90" height="90" />
																		<?php
																				}
																				else
																				{
																		?>
																					<img width="90" height="90" src="http://comjcropprofile.s3.amazonaws.com/<?php echo "Noimage.png";?>" />
																				<?php	
																				}
																			}
																			else
																			{
																		?>
																				<img width="90" height="90" src="http://comjcropprofile.s3.amazonaws.com/<?php echo "Noimage.png";?>" />
																			<?php	
																			}
																		}
																		elseif($exp_meds[1]=='reg_art' && $ans_all_posts[$po_all]['profile_type']=='images_post')
																		{
																			$tables = 'artist';
																			$imgs = $sql_feeds->reg_gal($tables,$sql_meds['gallery_id']);
																			
																			if($imgs!="" && !empty($imgs))
																			{
																				if($imgs['image_name']!="")
																				{
																			?>
																					<img width="90" height="90" src="http://reggalthumb.s3.amazonaws.com/<?php echo $imgs['image_name'];?>" />
																				<?php	
																				}
																				else
																				{
																			?>
																					<img width="90" height="90" src="http://comjcropprofile.s3.amazonaws.com/<?php echo "Noimage.png";?>" />
																				<?php	
																				}
																			}
																			else
																			{
																			?>
																				<img width="90" height="90" src="http://comjcropprofile.s3.amazonaws.com/<?php echo "Noimage.png";?>" />
																			<?php	
																			}
																		}
																		elseif($exp_meds[1]=='reg_com' && $ans_all_posts[$po_all]['profile_type']=='images_post')
																		{
																			$tables = 'community';
																			$imgs = $sql_feeds->reg_gal($tables,$sql_meds['gallery_id']);
																			
																			if($imgs!="" && !empty($imgs))
																			{
																				if($imgs['image_name']!="")
																				{
																			?>
																					<img width="90" height="90" src="http://reggalthumb.s3.amazonaws.com/<?php echo $imgs['image_name'];?>" />
																				<?php	
																				}
																				else
																				{
																			?>
																					<img width="90" height="90" src="http://comjcropprofile.s3.amazonaws.com/<?php echo "Noimage.png";?>" />
																				<?php	
																				}
																			}
																			else
																			{
																			?>
																				<img width="90" height="90" src="http://comjcropprofile.s3.amazonaws.com/<?php echo "Noimage.png";?>" />
																			<?php	
																			}
																		}
																		elseif($ans_all_posts[$po_all]['profile_type']=='events_post' || $ans_all_posts[$po_all]['profile_type']=='projects_post')
																		{
																			if($sql_meds['profile_url']!="") {
		?>																				
																					<a href="/<?php echo $sql_meds['profile_url']; ?>">
		<?php
																		}
																		?>
																			<img width="90" height="90" src="<?php if($sql_meds['image_name']!="") { echo $sql_meds['image_name']; } else { echo "http://comjcropprofile.s3.amazonaws.com/Noimage.png"; }?>" />
																		<?php
if($sql_meds['profile_url']!="") {
		?>																				
																					</a>
		<?php
																		}																		
																		}
																
																?></div>
																<?php 
																if($sql_meds['media_type']!=113)
																{
																?>
																<div class="player">
																	<?php 
																		if($sql_meds['media_type']==115)
																		{
																			include_once("findsale_video.php");
																		?>
																			<img src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" onclick="showPlayer('v','<?php echo $sql_meds['id'];?>','playlist');">
																			<img src="images/profile/add.gif" onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'" onclick="showPlayer('v','<?php echo $sql_meds['id'];?>','addvi');">
																		<?php
																			if($sql_meds['sharing_preference'] == 5 || $sql_meds['sharing_preference'] == 2)
																			{
																				$get_video_image = $sql_feeds->getvideoimage_feature($sql_meds['id']);
																				//$image_name_cart = "https://img.youtube.com/vi/".$get_video_image."/default.jpg";
																				if($get_video_image[1]=='youtube')
																			{
																				$image_name_cart = "http://img.youtube.com/vi/".$get_video_image[0]."/default.jpg";
																			}
																			else
																			{
																				$video_vim = $vimeo->call('vimeo.videos.getInfo', array('video_id' => $get_video_image[0]));
																				$image_name_cart = $video_vim->video[0]->thumbnails->thumbnail[1]->_content;
																			}
																				$free_down_rel_video="";
																				for($find_down=0;$find_down<=count($get_vids_pnew_w);$find_down++)
																				{
																					if(isset($sql_meds) && $sql_meds != null){
																						if($get_vids_pnew_w[$find_down]['id']==$sql_meds['id']){
																							$free_down_rel_video = "yes";
																						}
																						else{
																							if($free_down_rel_video =="" || $free_down_rel_video =="no"){
																								$free_down_rel_video = "no";
																							}
																						}
																					}
																				}
																				if($free_down_rel_video == "yes"){
																				 ?>
																					<!--<a href="javascript:void(0);" onclick="download_directly_song('<?php echo $sql_meds['id']; ?>','<?php echo str_replace("'","\'",$sql_meds['title']); ?>','<?php echo str_replace("'","\'",$sql_meds['creator']); ?>','<?php echo str_replace("'","\'",$sql_meds['from']); ?>')"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>-->
																			 <?php
																				}
																				else if($sql_meds['sharing_preference']==2){
																					if($sql_meds['id']!=""){
																					?>
																					<!--<a href="javascript:void(0);" onclick="ask_to_donate('<?php echo str_replace("'","\'",$sql_meds['title']);?>','<?php echo str_replace("'","\'",$sql_meds['creator']);?>','<?php echo $sql_meds['id'];?>','download_song<?php echo $sql_meds['id'];?>')"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>-->
																				<?php
																					}else{
																					?>
																					<!--<a href="javascript:void(0);" onclick="ask_to_donate('<?php echo str_replace("'","\'",$sql_meds['title']);?>','<?php echo str_replace("'","\'",$sql_meds['creator']);?>','<?php echo $sql_meds['id'];?>','download_song<?php echo $sql_meds['id'];?>')"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>-->
																				<?php
																					}
																				}
																				else{
																			?>
																				<!--<a href="javascript:void(0);" onclick="down_video_pop(<?php echo $sql_meds['id'];?>)"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>-->
																			<?php
																				}
																			}else{
																			?>
																				<!--<img src="../images/profile/download-over.gif" style="cursor:default"/>-->
																			<?php
																			}
																			?>
																			<!--<a href="addtocart.php?seller_id=<?php echo $sql_meds['general_user_id']; ?>&media_id=<?php echo $sql_meds['id']; ?>&buyer_id=<?php echo $_SESSION['gen_user_id'];?>&title=<?php echo str_replace("'","\'",$sql_meds['title']);?>&image_name=<?php echo $image_name_cart;?>" id="download_video<?php echo $sql_meds['id'];?>" style="display:none;"></a>-->
																			<script type="text/javascript">
																			$(document).ready(function() {
																					$("#download_video<?php echo $sql_meds['id'];?>").fancybox({
																					'height'			: '75%',
																					'width'				: '69%',
																					'transitionIn'		: 'none',
																					'transitionOut'		: 'none',
																					'type'				: 'iframe'
																					});
																			});
																			</script>
																			<?php
																		}/*else if($sql_meds['media_type']==113)
																		{
																		?>
																			<img src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'"></a>
																			<img src="images/profile/add.gif" onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'">
																			<img style="cursor: default;" src="images/profile/download-over.gif">
																		<?php
																		
																		}*/else if($sql_meds['media_type']==114)
																		{
																			include_once("findsale_song.php");
																		?>
																			<img src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" onclick="showPlayer('a','<?php echo $sql_meds['id'];?>','play');">
																			<img src="images/profile/add.gif" onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'" onclick="showPlayer('a','<?php echo $sql_meds['id'];?>','add');">
																		<?php
																			if($sql_meds['sharing_preference']==5 || $sql_meds['sharing_preference']==2)
																			{
																				$song_image_name_cart = $sql_feeds->get_audio_img($sql_meds['id']);
																				$free_down_rel ="";
																				for($find_down=0;$find_down<=count($get_songs_pnew_w);$find_down++)
																				{
																					if(isset($sql_meds) && $sql_meds != null)
																					{
																						if($get_songs_pnew_w[$find_down]['id']==$sql_meds['id']){
																							$free_down_rel = "yes";
																						}
																						else{
																							if($free_down_rel =="" || $free_down_rel =="no"){
																								$free_down_rel = "no";
																							}
																						}
																					}
																				}
																				if($free_down_rel == "yes"){
																				 ?>
																				<!-- <a href="javascript:void(0);" onclick="download_directly_song('<?php echo $sql_meds['id'];?>','<?php echo str_replace("'","\'",$sql_meds['title']);?>','<?php echo str_replace("'","\'",$sql_meds['creator']); ?>','<?php echo str_replace("'","\'",$sql_meds['from']); ?>')"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>-->
																			 <?php
																				}
																				else if($sql_meds['sharing_preference']==2){
																					if($sql_meds['id']!=""){
																					?>
																					<!--<a href="javascript:void(0);" onclick="ask_to_donate('<?php echo str_replace("'","\'",$sql_meds['title']);?>','<?php echo str_replace("'","\'",$sql_meds['creator']);?>','<?php echo $sql_meds['id'];?>','download_song<?php echo $sql_meds['id'];?>')"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>-->
																				<?php
																					}else{
																					?>
																					<!--<a href="javascript:void(0);" onclick="ask_to_donate('<?php echo str_replace("'","\'",$sql_meds['title']);?>','<?php echo str_replace("'","\'",$sql_meds['creator']);?>','<?php echo $sql_meds['id'];?>','download_song<?php echo $sql_meds['id'];?>')"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>-->
																				<?php
																					}
																				}
																				else{
																			?>
																				<!--<a href="javascript:void(0);" onclick="down_song_pop(<?php echo $sql_meds['id'];?>)"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>-->
																			<?php
																				}
																			}else{
																			?>	
																			<!--<img src="../images/profile/download-over.gif" style="cursor:default"/>-->
																			<?php
																			}
																			?>
																			<!--<a href="addtocart.php?seller_id=<?php echo $sql_meds['general_user_id']; ?>&media_id=<?php echo $sql_meds['id'] ?>&buyer_id=<?php echo $_SESSION['gen_user_id'];?>&title=<?php echo str_replace("'","\'",$sql_meds['title']); ?>&image_name=<?php echo $song_image_name_cart;?>" id="download_song<?php echo $sql_meds['id'];?>" style="display:none;"></a>-->
																				<script type="text/javascript">
																				$(document).ready(function() {
																						$("#download_song<?php echo $sql_meds['id'];?>").fancybox({
																						'width'				: '75%',
																						'height'			: '75%',
																						'transitionIn'		: 'none',
																						'transitionOut'		: 'none',
																						'type'				: 'iframe'
																						});
																				});
																				</script>
																			<?php
																		}else if($sql_meds['featured_media']!="" || $sql_meds['featured_media']!="nodisplay"){
																			if($sql_meds['featured_media'] == "Song" && $sql_meds['media_id'] != "0"){
																				include_once("findsale_song.php");
																			?>
																				<img src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" onclick="showPlayer('a','<?php echo $sql_meds['media_id'];?>','play');" />
																				<img src="images/profile/add.gif" onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'" onclick="showPlayer('a','<?php echo $sql_meds['media_id'];?>','add');" />
																				<?php
																				$get_media_info_down_gal = $sql_feeds ->get_media_details($sql_meds['media_id']);
																				if($get_media_info_down_gal['sharing_preference']==5 || $get_media_info_down_gal['sharing_preference']==2)
																				{
																					$song_image_name_cartproject = $sql_feeds->get_audio_img($get_media_info_down_gal['id']);
																					$free_down_rel ="";
																					for($find_down=0;$find_down<=count($get_songs_pnew_w);$find_down++)
																					{
																						//echo $get_songs_pnew_w[$find_down]['id'] ."==". $get_Song_title['id']."</br>";
																						if(isset($get_media_info_down_gal) && $get_media_info_down_gal != null)
																						{
																							if($get_songs_pnew_w[$find_down]['id']==$get_media_info_down_gal['id']){
																							$free_down_rel = "yes";
																							}
																							else{
																								if($free_down_rel =="" || $free_down_rel =="no"){
																									$free_down_rel = "no";
																								}
																							}
																						}
																					}
																					if($free_down_rel == "yes"){
																					 ?>
																					<!-- <a href="javascript:void(0);" onclick="download_directly_song('<?php echo $get_media_info_down_gal['id'];?>','<?php echo str_replace("'","\'",$get_media_info_down_gal['title']);?>','<?php echo str_replace("'","\'",$get_media_info_down_gal['creator']);?>','<?php echo str_replace("'","\'",$get_media_info_down_gal['from']);?>')"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>-->
																					<?php
																					}
																					else if($get_media_info_down_gal['sharing_preference']==2){
																					 ?>
																					<!--<a href="javascript:void(0);" onclick="ask_to_donate('<?php echo str_replace("'","\'",$get_media_info_down_gal['title']);?>','<?php echo str_replace("'","\'",$get_media_info_down_gal['creator']);?>','<?php echo $get_media_info_down_gal['id'];?>','download_songproject<?php echo $sql_meds['media_id'];?>')"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>-->
																				 <?php
																					}
																					else{
																				?>
																					<!--<a href="javascript:void(0);" onclick="down_song_pop_project(<?php echo $sql_meds['media_id'];?>)"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>-->
																				<?php
																					}
																				}else{
																				?>	
																				<!--<img src="../images/profile/download-over.gif" style="cursor:default"/>-->
																				<?php
																				}
																				?>
																				<!--<a href="addtocart.php?seller_id=<?php echo $get_media_info_down_gal['general_user_id'];?>&media_id=<?php echo $get_media_info_down_gal['id'];?>&buyer_id=<?php echo $_SESSION['gen_user_id'];?>&title=<?php echo str_replace("'","\'",$get_media_info_down_gal['title']);?>&image_name=<?php echo $song_image_name_cartproject;?>" id="download_songproject<?php echo $sql_meds['media_id'];?>" style="display:none;"></a>-->
																				<script type="text/javascript">
																				$(document).ready(function() {
																						$("#download_songproject<?php echo $sql_meds['media_id'];?>").fancybox({
																						'width'				: '75%',
																						'height'			: '75%',
																						'transitionIn'		: 'none',
																						'transitionOut'		: 'none',
																						'type'				: 'iframe'
																						});
																				});
																				</script>
																			<?php
																			}if($sql_meds['featured_media'] == "Gallery" && $sql_meds['media_id'] != "0"){
																				$get_all_feature_image2 = $sql_feeds->get_all_gallery_images($sql_meds['media_id']);
																				if($get_all_feature_image2!="" && $get_all_feature_image2!=NULL)
																			{
																					if(mysql_num_rows($get_all_feature_image2)>0)
																					{
																						$galleryImagesData2 ="";
																						$galleryImagestitle2 ="";
																						$mediaSrc2 ="";
																						$get_news_general_user_id = $uk -> get_general_user_idby_mediaid($sql_meds['media_id']);
																						while($row = mysql_fetch_assoc($get_all_feature_image2))
																						{
																							$mediaSrc2 = $row[0]['image_name'];
																							$filePath = $s3->getNewURI_otheruser("medgallery",$get_news_general_user_id).$mediaSrc2;
																							$galleryImagesData2 = $galleryImagesData2 .",". $row['image_name'];
																							$galleryImagestitle2 = $galleryImagestitle2 .",".$row['image_title'];
																						}
																						$orgPath2 = $s3->getNewURI_otheruser("medgallery",$get_news_general_user_id);
																						$thumbPath2 = $s3->getNewURI_otheruser("medgalthumb",$get_news_general_user_id);
																					}
																				}
																			?>
																				<a href="javascript:void(0);" onclick="checkload('<?php echo $mediaSrc2;?>','<?php echo $galleryImagesData2; ?>','<?php echo $thumbPath2; ?>','<?php echo $orgPath2; ?>','<?php echo $galleryImagestitle2; ?>','<?php echo $get_gallery['id'];?>')">
																				<img src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'"></a>
																				<img src="images/profile/add.gif" onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'">
																				<?php
																				$get_media_info_down_gal = $sql_feeds ->get_media_details($sql_meds['media_id']);
																				$get_gal_image_project  = $sql_feeds ->get_gallery_info($sql_meds['media_id']);
																				if($get_media_info_down_gal['sharing_preference']==2)
																				{
																				?>
																					<!--<a href="javascript:void(0);" onclick="ask_to_donate('<?php echo str_replace("'","\'",$get_media_info_down_gal['title']);?>','<?php echo str_replace("'","\'",$get_media_info_down_gal['creator']);?>','<?php echo $get_media_info_down_gal['id'];?>','download_galproject<?php echo $sql_meds['media_id'];?>')"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>-->
																				 <?php
																				}else{
																				?>
																				<!--<img src="../images/profile/download-over.gif" style="cursor:default"/>-->
																				<?php
																				}
																				?>
																				<!--<a href="addtocart.php?seller_id=<?php echo $get_media_info_down_gal['general_user_id'];?>&media_id=<?php echo $get_media_info_down_gal['id'];?>&buyer_id=<?php echo $_SESSION['gen_user_id'];?>&title=<?php echo str_replace("'","\'",$get_media_info_down_gal['title']);?>&image_name=<?php echo $get_gal_image_project['profile_image'];?>" id="download_galproject<?php echo $sql_meds['media_id'];?>" style="display:none;"></a>-->
																				<script type="text/javascript">
																				$(document).ready(function() {
																						$("#download_galproject<?php echo $sql_meds['media_id'];?>").fancybox({
																						'width'				: '75%',
																						'height'			: '75%',
																						'transitionIn'		: 'none',
																						'transitionOut'		: 'none',
																						'type'				: 'iframe'
																						});
																				});
																				</script>
																			<?php
																			}if($sql_meds['featured_media'] == "Video" && $sql_meds['media_id'] != "0"){
																				include_once("findsale_video.php");
																			?>
																				<img src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" onclick="showPlayer('v','<?php echo $sql_meds['media_id'];?>','playlist');"/>
																				<img src="images/profile/add.gif" onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'" onclick="showPlayer('v','<?php echo $sql_meds['media_id'];?>','addvi');"/>
																				<?php
																				$get_media_info_down_gal = $sql_feeds ->get_media_details($sql_meds['media_id']);
																				if($get_media_info_down_gal['sharing_preference']==5 || $get_media_info_down_gal['sharing_preference']==2)
																				{
																					$get_video_image = $sql_feeds->getvideoimage_feature($get_media_info_down_gal['id']);
																					//$image_name_cartproject = "https://img.youtube.com/vi/".$get_video_image."/default.jpg";
																					
																					if($get_video_image[1]=='youtube')
																			{
																				$image_name_cartproject = "http://img.youtube.com/vi/".$get_video_image[0]."/default.jpg";
																			}
																			else
																			{
																				$video_vim = $vimeo->call('vimeo.videos.getInfo', array('video_id' => $get_video_image[0]));
																				$image_name_cartproject = $video_vim->video[0]->thumbnails->thumbnail[1]->_content;
																			}
																					$free_down_rel_video="";
																					for($find_down=0;$find_down<=count($get_vids_pnew_w);$find_down++)
																					{
																						if(isset($get_media_info_down_gal) && $get_media_info_down_gal != null){
																							if($get_vids_pnew_w[$find_down]['id']==$get_media_info_down_gal['id']){
																								$free_down_rel_video = "yes";
																							}
																							else{
																								if($free_down_rel_video =="" || $free_down_rel_video =="no"){
																									$free_down_rel_video = "no";
																								}
																							}
																						}
																					}
																					if($free_down_rel_video == "yes"){
																					 ?>
																					 <!--<a href="javascript:void(0);" onclick="download_directly_song('<?php echo $get_media_info_down_gal['id'];?>','<?php echo str_replace("'","\'",$get_media_info_down_gal['title']);?>','<?php echo str_replace("'","\'",$get_media_info_down_gal['creator']);?>','<?php echo str_replace("'","\'",$get_media_info_down_gal['from']);?>')"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>
																					 -->
																				 <?php
																					}
																					else if($get_media_info_down_gal['sharing_preference']==2){
																					 ?>
																						<!--<a href="javascript:void(0);" onclick="ask_to_donate('<?php echo str_replace("'","\'",$get_media_info_down_gal['title']);?>','<?php echo str_replace("'","\'",$get_media_info_down_gal['creator']);?>','<?php echo $get_media_info_down_gal['id'];?>','download_videoproject<?php echo $sql_meds['media_id'];?>')"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>-->
																					<?php
																					}
																					else{
																				?>
																					<!--<a href="javascript:void(0);" onclick="down_video_pop_project(<?php echo $sql_meds['media_id'];?>)"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>-->
																				<?php
																					}
																				}
																				else{
																				?>
																				<!--<img src="../images/profile/download-over.gif" style="cursor:default"/>-->
																				<?php
																				}
																				?>
																				<!--<a href="addtocart.php?seller_id=<?php echo $get_media_info_down_gal['general_user_id'];?>&media_id=<?php echo $get_media_info_down_gal['id'];?>&buyer_id=<?php echo $_SESSION['gen_user_id'];?>&title=<?php echo str_replace("'","\'",$get_media_info_down_gal['title']);?>&image_name=<?php echo $image_name_cartproject;?>" id="download_videoproject<?php echo $sql_meds['media_id'];?>" style="display:none;"></a>-->
																				<script type="text/javascript">
																				$(document).ready(function() {
																						$("#download_videoproject<?php echo $sql_meds['media_id'];?>").fancybox({
																						'height'			: '75%',
																						'width'				: '69%',
																						'transitionIn'		: 'none',
																						'transitionOut'		: 'none',
																						'type'				: 'iframe'
																						});
																				});
																				</script>
																			<?php
																			}
																		}
																		else{
																		?>
																			<img src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'">
																			<img src="images/profile/add.gif" onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'">
																			<!--<img style="cursor: default;" src="images/profile/download-over.gif">-->
																		<?php
																		}
																	?>
																		
																</div>	
																<?php
																}
																?>
															</div>
															
															<div class="sideR">
																<div class="activityTitle">
		<?php
																	if($exp_meds[1]=='reg_med' || $ans_all_posts[$po_all]['profile_type']=='projects_post' || $ans_all_posts[$po_all]['profile_type']=='events_post')
																			{
																				if($ans_all_posts[$po_all]['profile_type']=='projects_post' || $ans_all_posts[$po_all]['profile_type']=='events_post' || $sql_meds['media_type']!=114)
																				{
																					if($sql_meds['profile_url']!="") {
		?>																				
																					<a href="/<?php echo $sql_meds['profile_url']; ?>">
		<?php
																		}
																					if($sql_meds['media_type']==115){
																					echo "<a href=javascript:void(0) onclick=showPlayer('v','".$sql_meds['id']."','playlist')>".$sql_meds['title']."</a>";
																					}else if($sql_meds['media_type']==113){
																						$get_all_image1 = $sql_feeds->get_all_gallery_images($sql_meds['id']);
																						$play_array1 = array();
																						if($get_all_image1!="" && $get_all_image1!=NULL)
																			{
																						if(mysql_num_rows($get_all_image1)>0)
																						{
																							while($row = mysql_fetch_assoc($get_all_image1))
																							{
																								$play_array1[] = $row;
																							}
																							$get_news_general_user_id = $uk -> get_general_user_idby_mediaid($sql_meds['id']);
																							${'orgPath'.$my_po}= $s3->getNewURI_otheruser("medgallery",$get_news_general_user_id);
																							${'thumbPath'.$my_po}= $s3->getNewURI_otheruser("medgalthumb",$get_news_general_user_id);
																							for($l=0;$l<count($play_array1);$l++)
																							{
																								$mediaSrc = $play_array1[0]['image_name'];
																								$filePath = $s3->getNewURI_otheruser("medgallery",$get_news_general_user_id).$mediaSrc;
																								
																								
																								${'galleryImagesData'.$my_po}[$galleryImagesDataIndex] = $play_array1[$l]['image_name'];
																								${'galleryImagestitle'.$my_po}[$galleryImagesDataIndex] = $play_array1[$l]['image_title'];
																								//echo ${'galleryImagesData'.$my_po}[$galleryImagesDataIndex];
																								$galleryImagesDataIndex++;
																							}
																							
																						}
																						}
																						if(isset(${'galleryImagesData'.$my_po}))
																						{
																							${'galleryImagesData'.$my_po} = implode(",", ${'galleryImagesData'.$my_po});
																							${'galleryImagestitle'.$my_po} = implode(",", ${'galleryImagestitle'.$my_po});
																						}
																						echo "<a href=javascript:void(0) onclick=checkload('".$mediaSrc."','". ${'galleryImagesData'.$my_po}."','". ${'thumbPath'.$my_po}."','". ${'orgPath'.$my_po}."','". ${'galleryImagestitle'.$my_po}."','". $get_gallery['id']."')>".$sql_meds['title']."</a>";
																						$my_po = $my_po + 1;
																					}else{
																					echo $sql_meds['title'];
																					}
																					if($sql_meds['profile_url']!="") {
		?>																				
																					</a>
		<?php
																		}
																				}
																				elseif($exp_meds[1]=='reg_med' && $sql_meds['media_type']==114)
																				{
																					$sql_me = mysql_query("SELECT * FROM media_songs WHERE media_id='".$sql_meds['id']."'");
																					$ans_me = mysql_fetch_assoc($sql_me);
																					$ecehos = '<a style="text-decoration: none; cursor: text;" href="javascript:void(0)"> '.$ans_me['track'].' </a>';
																					//$ecehos .= '<a href="">'.$sql_meds['title'].'</a>';
																					if($sql_meds['media_type']==114)
																					{
																						//$ecehos .= "<a href=javascript:void(0) onclick=showPlayer('a',".$sql_meds['id'].",'add');>".$sql_meds['title']."</a>";
																						$ecehos .= "<a style='text-decoration: none; cursor: text;' href='javascript:void(0)'>".$sql_meds['title']."</a>";
																						
																					}
																					else if($sql_meds['media_type']==115)
																					{
																						$ecehos .= "<a href=javascript:void(0) onclick=showPlayer('v',".$sql_meds['id'].",'addvi');>".$sql_meds['title']."</a>";
																					}
																					else if($sql_meds['media_type']==113)
																					{
																						$get_all_gal_image = $sql_feeds->get_all_gallery_images($sql_meds['id']);
																						if($get_all_gal_image!="" && $get_all_gal_image!=NULL)
																			{
																						if(mysql_num_rows($get_all_gal_image)>0)
																						{
																							$galleryImagesData ="";
																							$galleryImagestitle ="";
																							$mediaSrc ="";
																							$get_news_general_user_id = $uk -> get_general_user_idby_mediaid($sql_meds['id']);
																							while($row = mysql_fetch_assoc($get_all_gal_image))
																							{
																								$mediaSrc = $row[0]['image_name'];
																								$filePath = $s3->getNewURI_otheruser("medgallery",$get_news_general_user_id).$mediaSrc;
																								if($row['image_name']!=""){
																									$galleryImagesData = $galleryImagesData .",". $row['image_name'];
																									$galleryImagestitle = $galleryImagestitle .",".$row['image_title'];
																								}
																							}
																							$orgPath = $s3->getNewURI_otheruser("medgallery",$get_news_general_user_id);
																							$thumbPath = $s3->getNewURI_otheruser("medgalthumb",$get_news_general_user_id);
																						}
																						}
																					
																						$ecehos .= "<a href=javascript:void(0) onclick=checkload('".$mediaSrc."','".$galleryImagesData."','".$thumbPath."','".$orgPath."','". $galleryImagestitle."','".$get_gallery['id'].")>".$sql_meds['title']."</a>";
																						$galleryImagesData = "";
																						$galleryImagestitle = "";
																						$mediaSrc ="";
																					}
																					else
																					{
																						$ecehos .= '<a href="">'.$sql_meds['title'].'</a>';
																					}
																	
																	if($sql_meds['creator']!="")
																	{
																		$create_c = strpos($sql_meds['creator'],'(');
																		$end = substr($sql_meds['creator'],0,$create_c);
																		$exp_cr = explode('|',$sql_meds['creator_info']);
																		
																		if($exp_cr[0]=='general_artist')
																		{
																			$sql_c_ar = mysql_query("SELECT * FROM general_artist WHERE artist_id='".$exp_cr[1]."'");
																		}
																		if($exp_cr[0]=='general_community')
																		{
																			$sql_c_ar = mysql_query("SELECT * FROM general_community WHERE community_id='".$exp_cr[1]."'");
																		}
																		if($exp_cr[0]=='artist_project' || $exp_cr[0]=='community_project' || $exp_cr[0]=='community_event' || $exp_cr[0]=='artist_event')
																		{
																			$sql_c_ar = mysql_query("SELECT * FROM $exp_cr[0] WHERE id='".$exp_cr[1]."'");
																		}
																		
																		
																		if(isset($sql_c_ar))
																		{
																			if($sql_c_ar!="" && $sql_c_ar!=NULL)
																			{
																			if(mysql_num_rows($sql_c_ar)>0)
																			{
																				$ans_c_ar = mysql_fetch_assoc($sql_c_ar);
																				if(isset($ans_c_ar['name']))
																				{
																					$end = $ans_c_ar['name'];
																				}
																				else
																				{
																					$end = $ans_c_ar['title'];
																				}
																				if($ans_c_ar['profile_url']!="" && !empty($ans_c_ar['profile_url']))
																				{
																					$ecehos .= " By  <a href='/".$ans_c_ar['profile_url']."'><b>".$end."</b></a>  ";
																				}
																				else
																				{
																					$ecehos .= " By  <b>".$end."</b>  ";
																				}
																			}
																			else
																			{
																				$ecehos .= " By  <b>".$end."</b>  ";
																			}
																			}
																			else
																			{
																				$ecehos .= " By  <b>".$end."</b>  ";
																			}
																		}
																		else
																		{
																			
																			$ecehos .= " By  <b>".$end."</b>  ";
																		}
																	}
																	if($sql_meds['from']!="")
																	{
																		$create_f = strpos($sql_meds['from'],'(');
																		$end_f = substr($sql_meds['from'],0,$create_f);
																		
																		$exp_fr = explode('|',$sql_meds['from_info']);
																		if($exp_fr[0]=='general_artist')
																		{
																			$sql_f_ar = mysql_query("SELECT * FROM general_artist WHERE artist_id='".$exp_fr[1]."'");
																		}
																		if($exp_fr[0]=='general_community')
																		{
																			$sql_f_ar = mysql_query("SELECT * FROM general_community WHERE community_id='".$exp_fr[1]."'");
																		}
																		if($exp_fr[0]=='artist_project' || $exp_fr[0]=='community_project' || $exp_fr[0]=='community_event' || $exp_fr[0]=='artist_event')
																		{
																			$sql_f_ar = mysql_query("SELECT * FROM $exp_fr[0] WHERE id='".$exp_fr[1]."'");
																		}
																		
																		if(isset($sql_f_ar))
																		{
																			if($sql_f_ar!="" && $sql_f_ar!=NULL)
																			{
																			if(mysql_num_rows($sql_f_ar)>0)
																			{
																				$ans_f_ar = mysql_fetch_assoc($sql_f_ar);
																				if(isset($ans_f_ar['name']))
																				{
																					$end_f = $ans_f_ar['name'];
																				}
																				else
																				{
																					$end_f = $ans_f_ar['title'];
																				}
																				if($ans_f_ar['profile_url']!="" && !empty($ans_f_ar['profile_url']))
																				{
																					$ecehos .= " From  <a href='/".$ans_f_ar['profile_url']."'><b>".$end_f."</b></a>";
																				}
																				else
																				{
																					$ecehos .= " From  <b>".$end_f."</b>";
																				}
																			}
																			else
																			{
																				$ecehos .= " From  <b>".$end_f."</b>";
																			}
																			}
																			else
																			{
																				$ecehos .= " From  <b>".$end_f."</b>";
																			}
																		}
																		else
																		{
																			$ecehos .= " From  <b>".$end_f."</b>";
																		}
																	}
																	echo $ecehos;
																				}
																			}
																			elseif($exp_meds[1]=='reg_art' || $exp_meds[1]=='reg_com')
																			{
																				if($ans_all_posts[$po_all]['profile_type']=='images_post')
																				{
																					echo $sql_meds['gallery_title'];
																				}
																				elseif($ans_all_posts[$po_all]['profile_type']=='songs_post')
																				{
																					echo $sql_meds['audio_name'];
																				}
																				elseif($ans_all_posts[$po_all]['profile_type']=='videos_post')
																				{
																					echo "<a href=javascript:void(0) onclick=showPlayer('v','".$sql_meds['id']."','playlist')>".$sql_meds['video_name']."</a>";
																				}
																			}
		?>
																	<div style="color:#808080;font-size:12px;">
																		<div class="comment more">
																			<?php echo $ans_all_posts[$po_all]['description']; ?>
																		</div>
																	</div>
																</div>
																
																<!--<div class="activityContent">Tagged: <?php //echo $medias['tagged_users']; 
																//$name_org = explode(')',$medias['tagged_users']);
																//for($i_name_org=0;$i_name_org<count($name_org);$i_name_org++)
																//{
																	//	$end_pos = strpos($name_org[$i_name_org],'(');
																	//	$org_name = substr($name_org[$i_name_org],0,$end_pos);
																	//		echo $org_name;
																//	}
																?></div>-->
															</div>
														</div>
													</div>
												</div>
										
												<div class="rightSide">
													<a href="#" onmouseover="mopen('mm<?php echo $po_all;?>3')" onmouseout="mclosetime()"><img src="images/profile/arrow.png" onmouseover="this.src='images/profile/arrow-down.png'" onmouseout="this.src='images/profile/arrow.png'" /></a>
													<div id="mm<?php echo $po_all;?>3" class="popUp" onmouseover="mcancelclosetime()" onmouseout="mclosetime()">
														<ul>
															<li><a href="hideposts.php?id=<?php echo $ans_all_posts[$po_all]['id'];?>">Hide Feed</a></li>
															<li><a href="hideposts.php?report_id=<?php echo $ans_all_posts[$po_all]['id'];?>">Report spam</a></li>
															<li><div class="seperator"></div></li>
															<li><!--<a href="Unsubscribe_feeds.php?sub=1&user_ids=<?php echo $gens['general_user_id']; ?>" onclick="return confirm_subscription_delete('<?php if($art==1){ echo $ros['name']; } elseif($com==1){ echo $ros_com['name']; } ?>')">Unsubscribe from <?php if($art==1)
															{
																echo $ros['name'];
															}
															elseif($com==1)
															{
																echo $ros_com['name'];
															} ?></a>-->
															<a href="profileedit.php#subscrriptions" >Unsubscribe from <?php if($art==1)
															{
																echo $ros['name'];
															}
															elseif($com==1)
															{
																echo $ros_com['name'];
															} ?></a>
															</li>
														</ul>
													</div>
												</div>
											</div>
			<?php
										}
									}
								}
							}
							elseif($ans_all_posts[$po_all]['display_to']=='Friends')
							{
								if($gens!="")
								{
									$friends_c = $sql_feeds->friends_cond($ans_all_posts[$po_all]['general_user_id'],$_SESSION['login_id']);
														
									if($friends_c=='1')
									{
										$hide11 = 0;
										$get_gen_id = $sql_feeds->get_general_info();
										$hide_by = $ans_all_posts[$po_all]['hidden_by'];
										$exp_hide_by = explode(",",$hide_by);
										for($exp_count=0;$exp_count<=count($exp_hide_by);$exp_count++)
										{
											if($exp_hide_by[$exp_count] ==$get_gen_id['general_user_id'])
											{
												$hide11 =1;
											}
										}
										if($hide11 != 1)
										{
			?>
											<div class="storyCont">
												<div class="leftSide">
													<div class="avatar">
														<?php
													$art = 0;
													
													if($gens['artist_id']!=0)
													{
														$sql_art = mysql_query("SELECT * FROM general_artist WHERE artist_id='".$gens['artist_id']."' AND status=0 AND active=0 AND del_status=0");
														if($sql_art!="" && $sql_art!=NULL)
														{
															if(mysql_num_rows($sql_art)>0)
															{
																$ros = mysql_fetch_assoc($sql_art);
																$art = 1;
															}
														}
													}
													
													$com = 0;
													if($gens['community_id']!=0)
													{
														$sql_com = mysql_query("SELECT * FROM general_community WHERE community_id='".$gens['community_id']."' AND status=0 AND active=0 AND del_status=0");
														if($sql_com!="" && $sql_com!=NULL)
														{
															if(mysql_num_rows($sql_com)>0)
															{
																$ros_com = mysql_fetch_assoc($sql_com);
																$com = 1;
															}
														}
													}
													
													if($art==1)
													{
														if($ros['profile_url']!="")
														{
													?>
														<a href="/<?php echo $ros['profile_url']; ?>">
													<?php
														}
														$get_news_general_user_id = $uk -> get_general_user_idby_artistid($ros['artist_id']);
													?>
														<img width="50" height="50" src="<?php
														if($ros['image_name']!="" && !empty($ros['image_name']))
														{
															echo $s3->getNewURI_otheruser("artjcropprofile",$get_news_general_user_id).$ros['image_name']; 
														}
														else
														{
															echo "http://artjcropprofile.s3.amazonaws.com/Noimage.png";
														}
													?>" />
													<?php
														if($ros['profile_url']!="")
														{
													?>
														</a>
													<?php
														}
													}
													elseif($com==1)
													{
														if($ros_com['profile_url']!="")
														{
													?>
														<a href="/<?php echo $ros_com['profile_url']; ?>">
													<?php
														}
														$get_news_general_user_id = $uk -> get_general_user_idby_communityid($ros_com['community_id']);
													?>
														<img width="50" height="50" src="<?php
													
													
													if($ros_com['image_name']!="" && !empty($ros_com['image_name']))
													{
														echo $s3->getNewURI_otheruser("comjcropprofile",$get_news_general_user_id).$ros_com['image_name']; 
													}
													else
													{
														echo "http://comjcropprofile.s3.amazonaws.com/Noimage.png";
													}
													
													?>" />
													<?php
														if($ros_com['profile_url']!="")
														{
													?>
														</a>
													<?php
														}
													}
													?>
													</div>
													<?php //echo "table=".$reg_table_media_type;
														//echo $ans_audio_feeds[$audio_fed_lo]['id'];
													?>
													<div class="storyContainer">
														<div class="storyTitle">
															<span class="userName"><?php if($art==1)
															{
																if($ros['profile_url']!="")
														{
													?>
														<a href="/<?php echo $ros['profile_url']; ?>">
													<?php
														}
																echo $ros['name'];
																if($ros['profile_url']!="")
														{
													?>
														</a>
													<?php
														}
															}
															elseif($com==1)
															{
																if($ros_com['profile_url']!="")
														{
													?>
														<a href="/<?php echo $ros_com['profile_url']; ?>">
													<?php
														}
																echo $ros_com['name'];
																if($ros_com['profile_url']!="")
														{
													?>
														</a>
													<?php
														}
															} ?></span>  <?php //echo $ans_all_posts[$po_all]['description']; ?>
														</div>
														<div class="activityCont">
															<div class="sideL">
																<div class="storyImage" style="position:relative;"><?php
																
																if($sql_meds['media_type']==115 || $ans_all_posts[$po_all]['profile_type']=='videos_post')
																		{
																			if($sql_meds['media_type']==115)
																			{
																				$tables = 'media_video';
																				$get_video_image = $sql_feeds->getvideoimage($tables,$sql_meds['id']);
																			}
																			elseif($ans_all_posts[$po_all]['profile_type']=='videos_post' && $exp_meds[1]=='reg_art')
																			{
																				$tables = 'general_artist_video';
																				$get_video_image = $sql_feeds->getvideoimage($tables,$sql_meds['id']);
																			}
																			elseif($ans_all_posts[$po_all]['profile_type']=='videos_post' && $exp_meds[1]=='reg_com')
																			{
																				$tables = 'general_community_video';
																				$get_video_image = $sql_feeds->getvideoimage($tables,$sql_meds['id']);
																			}
																			if($get_video_image[1]=='youtube')
																			{
																				$vid_pros_edit_img = "http://img.youtube.com/vi/".$get_video_image[0]."/default.jpg";
																			}
																			else
																			{
																				$video_vim = $vimeo->call('vimeo.videos.getInfo', array('video_id' => $get_video_image[0]));
																				$vid_pros_edit_img = $video_vim->video[0]->thumbnails->thumbnail[1]->_content;
																			}
																	?>
																		<a onmouseout="newfunctionout('<?php echo $sql_meds['id']; ?>','a')" onmouseover="newfunction('<?php echo $sql_meds['id']; ?>','a')" href="javascript:void(0);" onclick="showPlayer('v','<?php echo $sql_meds['id'];?>','playlist');">
																			<img src="<?php echo $vid_pros_edit_img; ?>" width="90" height="90" />
																		</a>
																		<div class="navButton">
			<div class="playMedia">
				<a href="javascript:void(0);" class="list_play"><img title="Add file to the bottom of your playlist without stopping current media." id="play_video_image<?php echo $sql_meds['id']; ?>a" onclick="showPlayer('v','<?php echo $sql_meds['id'];?>','playlist');" src="/images/play.png" onmouseout="newfunctionout('<?php echo $sql_meds['id']; ?>','a')" onmouseover="newfunction('<?php echo $sql_meds['id']; ?>','a')" style="border-radius: 6px;"></a>
			</div>
		</div>
																	<?php
																		}
																		/* elseif($sql_meds['media_type']==114)
																		{
																			$get_audio_image = $sql_feeds->get_song_med_img($sql_meds['id']);
																			
																			if($get_audio_image!="" && !empty($get_audio_image))
																			{
																				if($get_audio_image['gallery_at']!="")
																				{
																					$get_audio__timage = $sql_feeds->get_song_med_t_img($get_audio_image['gallery_at'],$get_audio_image['gallery_id']);
																					if($get_audio_image['gallery_at']=='general_media')
																					{
																		?>
																						<img src="http://medgalthumb.s3.amazonaws.com/<?php echo $get_audio__timage; ?>" width="90" height="90" />
																		<?php
																					}
																				}
																			}
																		} */
																		elseif($sql_meds['media_type']==113)
																		{
																			$get_all_image1 = $sql_feeds->get_all_gallery_images($sql_meds['id']);
																			$play_array1 = array();
																			if($get_all_image1!="" && $get_all_image1!=NULL)
														{
																				if(mysql_num_rows($get_all_image1)>0)
																				{
																					while($row = mysql_fetch_assoc($get_all_image1))
																					{
																						$play_array1[] = $row;
																					}
																					$get_news_general_user_id = $uk -> get_general_user_idby_mediaid($sql_meds['id']);
																					${'orgPath'.$my_po}= $s3->getNewURI_otheruser("medgallery",$get_news_general_user_id);
																					${'thumbPath'.$my_po}= $s3->getNewURI_otheruser("medgalthumb",$get_news_general_user_id);
																					for($l=0;$l<count($play_array1);$l++)
																					{
																						$mediaSrc = $play_array1[0]['image_name'];
																						$filePath = $s3->getNewURI_otheruser("medgallery",$get_news_general_user_id).$mediaSrc;
																						
																						
																						${'galleryImagesData'.$my_po}[$galleryImagesDataIndex] = $play_array1[$l]['image_name'];
																						${'galleryImagestitle'.$my_po}[$galleryImagesDataIndex] = $play_array1[$l]['image_title'];
																						//echo ${'galleryImagesData'.$my_po}[$galleryImagesDataIndex];
																						$galleryImagesDataIndex++;
																					}
																					
																				}
																			}
																			if(isset(${'galleryImagesData'.$my_po}))
																			{
																				${'galleryImagesData'.$my_po} = implode(",", ${'galleryImagesData'.$my_po});
																				${'galleryImagestitle'.$my_po} = implode(",", ${'galleryImagestitle'.$my_po});
																			}
																			
																			$get_gal_image = $sql_feeds->gallery_meds($sql_meds['id']);
																			if($get_gal_image!="" && !empty($get_gal_image))
																			{
																				if($get_gal_image['profile_image']!="")
																				{
																		?>
																				<a onmouseout="newfunctionout('<?php echo $sql_meds['id']; ?>','a')" onmouseover="newfunction('<?php echo $sql_meds['id']; ?>','a')" href="javascript:void(0);" onclick="checkload('<?php echo $mediaSrc;?>','<?php echo ${'galleryImagesData'.$my_po}; ?>','<?php echo ${'thumbPath'.$my_po}; ?>','<?php echo ${'orgPath'.$my_po}; ?>','<?php echo ${'galleryImagestitle'.$my_po}; ?>','<?php echo $get_gallery['id'];?>')">
																					<img src="<?php echo $get_gal_image['profile_image']; ?>" width="90" height="90" />
																				</a>	
																		<?php
																				}
																				else
																				{
																		?>
																				<a onmouseout="newfunctionout('<?php echo $sql_meds['id']; ?>','a')" onmouseover="newfunction('<?php echo $sql_meds['id']; ?>','a')" href="javascript:void(0);" onclick="checkload('<?php echo $mediaSrc;?>','<?php echo ${'galleryImagesData'.$my_po}; ?>','<?php echo ${'thumbPath'.$my_po}; ?>','<?php echo ${'orgPath'.$my_po}; ?>','<?php echo ${'galleryImagestitle'.$my_po}; ?>','<?php echo $get_gallery['id'];?>')">
																					<img width="90" height="90" src="http://comjcropprofile.s3.amazonaws.com/<?php echo "Noimage.png";?>" />
																				</a>	
																				<?php	
																				}
																			}
																			else
																			{
																		?>
																			<a onmouseout="newfunctionout('<?php echo $sql_meds['id']; ?>','a')" onmouseover="newfunction('<?php echo $sql_meds['id']; ?>','a')" href="javascript:void(0);" onclick="checkload('<?php echo $mediaSrc;?>','<?php echo ${'galleryImagesData'.$my_po}; ?>','<?php echo ${'thumbPath'.$my_po}; ?>','<?php echo ${'orgPath'.$my_po}; ?>','<?php echo ${'galleryImagestitle'.$my_po}; ?>','<?php echo $get_gallery['id'];?>')">
																				<img width="90" height="90" src="http://comjcropprofile.s3.amazonaws.com/<?php echo "Noimage.png";?>" />
																			</a>	
																			<?php	
																		
																			}
																			$my_po = $my_po + 1;
																			?>
																				<div class="navButton">
			<div class="playMedia">
				<a href="javascript:void(0);" class="list_play"><img title="Add file to the bottom of your playlist without stopping current media." id="play_video_image<?php echo $sql_meds['id']; ?>a" onclick="checkload('<?php echo $mediaSrc;?>','<?php echo ${'galleryImagesData'.$my_po}; ?>','<?php echo ${'thumbPath'.$my_po}; ?>','<?php echo ${'orgPath'.$my_po}; ?>','<?php echo ${'galleryImagestitle'.$my_po}; ?>','<?php echo $get_gallery['id'];?>')" src="/images/play.png" onmouseout="newfunctionout('<?php echo $sql_meds['id']; ?>','a')" onmouseover="newfunction('<?php echo $sql_meds['id']; ?>','a')" style="border-radius: 6px;"></a>
			</div>
		</div>
																			<?php
																		}
																		elseif($sql_meds['media_type']==116)
																		{
																			$get_ch_image = $sql_feeds->channel_meds($sql_meds['id']);
																			if($get_ch_image!="" && !empty($get_ch_image))
																			{
																				if($get_ch_image['profile_image']!="")
																				{
																		?>
																					<img src="<?php echo $get_ch_image['profile_image']; ?>" width="90" height="90" />
																		<?php
																				}
																				else
																				{
																		?>
																					<img width="90" height="90" src="http://comjcropprofile.s3.amazonaws.com/<?php echo "Noimage.png";?>" />
																				<?php	
																				}
																			}
																			else
																			{
																		?>
																				<img width="90" height="90" src="http://comjcropprofile.s3.amazonaws.com/<?php echo "Noimage.png";?>" />
																			<?php	
																			}
																		}
																		elseif($exp_meds[1]=='reg_art' && $ans_all_posts[$po_all]['profile_type']=='images_post')
																		{
																			$tables = 'artist';
																			$imgs = $sql_feeds->reg_gal($tables,$sql_meds['gallery_id']);
																			
																			if($imgs!="" && !empty($imgs))
																			{
																				if($imgs['image_name']!="")
																				{
																			?>
																					<img width="90" height="90" src="http://reggalthumb.s3.amazonaws.com/<?php echo $imgs['image_name'];?>" />
																				<?php	
																				}
																				else
																				{
																			?>
																					<img width="90" height="90" src="http://comjcropprofile.s3.amazonaws.com/<?php echo "Noimage.png";?>" />
																				<?php	
																				}
																			}
																			else
																			{
																			?>
																				<img width="90" height="90" src="http://comjcropprofile.s3.amazonaws.com/<?php echo "Noimage.png";?>" />
																			<?php	
																			}
																		}
																		elseif($exp_meds[1]=='reg_com' && $ans_all_posts[$po_all]['profile_type']=='images_post')
																		{
																			$tables = 'community';
																			$imgs = $sql_feeds->reg_gal($tables,$sql_meds['gallery_id']);
																			
																			if($imgs!="" && !empty($imgs))
																			{
																				if($imgs['image_name']!="")
																				{
																			?>
																					<img width="90" height="90" src="http://reggalthumb.s3.amazonaws.com/<?php echo $imgs['image_name'];?>" />
																				<?php	
																				}
																				else
																				{
																			?>
																					<img width="90" height="90" src="http://comjcropprofile.s3.amazonaws.com/<?php echo "Noimage.png";?>" />
																				<?php	
																				}
																			}
																			else
																			{
																			?>
																				<img width="90" height="90" src="http://comjcropprofile.s3.amazonaws.com/<?php echo "Noimage.png";?>" />
																			<?php	
																			}
																		}
																		elseif($ans_all_posts[$po_all]['profile_type']=='events_post' || $ans_all_posts[$po_all]['profile_type']=='projects_post')
																		{
																			if($sql_meds['profile_url']!="") {
		?>																				
																					<a href="/<?php echo $sql_meds['profile_url']; ?>">
		<?php
																		}
																		?>
																			<img width="90" height="90" src="<?php if($sql_meds['image_name']!="") { echo $sql_meds['image_name']; } else { echo "http://comjcropprofile.s3.amazonaws.com/Noimage.png"; }?>" />
																		<?php
if($sql_meds['profile_url']!="") {
		?>																				
																					</a>
		<?php
																		}																		
																		}
																
																?></div>
																<?php
																if($sql_meds['media_type']!=113)
																{
																?>
																<div class="player">
																	<?php 
																		if($sql_meds['media_type']==115)
																		{
																			include_once("findsale_video.php");
																		?>
																			<img src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" onclick="showPlayer('v','<?php echo $sql_meds['id'];?>','playlist');">
																			<img src="images/profile/add.gif" onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'" onclick="showPlayer('v','<?php echo $sql_meds['id'];?>','addvi');">
																		<?php
																			if($sql_meds['sharing_preference'] == 5 || $sql_meds['sharing_preference'] == 2)
																			{
																				$get_video_image = $sql_feeds->getvideoimage_feature($sql_meds['id']);
																				//$image_name_cart = "https://img.youtube.com/vi/".$get_video_image."/default.jpg";
																				if($get_video_image[1]=='youtube')
																			{
																				$image_name_cart = "http://img.youtube.com/vi/".$get_video_image[0]."/default.jpg";
																			}
																			else
																			{
																				$video_vim = $vimeo->call('vimeo.videos.getInfo', array('video_id' => $get_video_image[0]));
																				$image_name_cart = $video_vim->video[0]->thumbnails->thumbnail[1]->_content;
																			}
																				$free_down_rel_video="";
																				for($find_down=0;$find_down<=count($get_vids_pnew_w);$find_down++)
																				{
																					if(isset($sql_meds) && $sql_meds != null){
																						if($get_vids_pnew_w[$find_down]['id']==$sql_meds['id']){
																							$free_down_rel_video = "yes";
																						}
																						else{
																							if($free_down_rel_video =="" || $free_down_rel_video =="no"){
																								$free_down_rel_video = "no";
																							}
																						}
																					}
																				}
																				if($free_down_rel_video == "yes"){
																				 ?>
																					<!--<a href="javascript:void(0);" onclick="download_directly_song('<?php echo $sql_meds['id']; ?>','<?php echo str_replace("'","\'",$sql_meds['title']); ?>','<?php echo str_replace("'","\'",$sql_meds['creator']); ?>','<?php echo str_replace("'","\'",$sql_meds['from']); ?>')"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>-->
																			 <?php
																				}
																				else if($sql_meds['sharing_preference']==2){
																					if($sql_meds['id']!=""){
																					?>
																					<!--<a href="javascript:void(0);" onclick="ask_to_donate('<?php echo str_replace("'","\'",$sql_meds['title']);?>','<?php echo str_replace("'","\'",$sql_meds['creator']);?>','<?php echo $sql_meds['id'];?>','download_song<?php echo $sql_meds['id'];?>')"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>-->
																				<?php
																					}else{
																					?>
																					<!--<a href="javascript:void(0);" onclick="ask_to_donate('<?php echo str_replace("'","\'",$sql_meds['title']);?>','<?php echo str_replace("'","\'",$sql_meds['creator']);?>','<?php echo $sql_meds['id'];?>','download_song<?php echo $sql_meds['id'];?>')"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>-->
																				<?php
																					}
																				}
																				else{
																			?>
																				<!--<a href="javascript:void(0);" onclick="down_video_pop(<?php echo $sql_meds['id'];?>)"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>-->
																			<?php
																				}
																			}else{
																			?>
																				<!--<img src="../images/profile/download-over.gif" style="cursor:default"/>-->
																			<?php
																			}
																			?>
																			<!--<a href="addtocart.php?seller_id=<?php echo $sql_meds['general_user_id']; ?>&media_id=<?php echo $sql_meds['id']; ?>&buyer_id=<?php echo $_SESSION['gen_user_id'];?>&title=<?php echo str_replace("'","\'",$sql_meds['title']);?>&image_name=<?php echo $image_name_cart;?>" id="download_video<?php echo $sql_meds['id'];?>" style="display:none;"></a>-->
																			<script type="text/javascript">
																			$(document).ready(function() {
																					$("#download_video<?php echo $sql_meds['id'];?>").fancybox({
																					'height'			: '75%',
																					'width'				: '69%',
																					'transitionIn'		: 'none',
																					'transitionOut'		: 'none',
																					'type'				: 'iframe'
																					});
																			});
																			</script>
																			<?php
																		}/*else if($sql_meds['media_type']==113)
																		{
																		?>
																			<img src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'"></a>
																			<img src="images/profile/add.gif" onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'">
																			<img style="cursor: default;" src="images/profile/download-over.gif">
																		<?php
																		}*/else if($sql_meds['media_type']==114)
																		{
																			include_once("findsale_song.php");
																		?>
																			<img src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" onclick="showPlayer('a','<?php echo $sql_meds['id'];?>','play');">
																			<img src="images/profile/add.gif" onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'" onclick="showPlayer('a','<?php echo $sql_meds['id'];?>','add');">
																		<?php
																			if($sql_meds['sharing_preference']==5 || $sql_meds['sharing_preference']==2)
																			{
																				$song_image_name_cart = $sql_feeds->get_audio_img($sql_meds['id']);
																				$free_down_rel ="";
																				for($find_down=0;$find_down<=count($get_songs_pnew_w);$find_down++)
																				{
																					if(isset($sql_meds) && $sql_meds != null)
																					{
																						if($get_songs_pnew_w[$find_down]['id']==$sql_meds['id']){
																							$free_down_rel = "yes";
																						}
																						else{
																							if($free_down_rel =="" || $free_down_rel =="no"){
																								$free_down_rel = "no";
																							}
																						}
																					}
																				}
																				if($free_down_rel == "yes"){
																				 ?>
																				<!-- <a href="javascript:void(0);" onclick="download_directly_song('<?php echo $sql_meds['id'];?>','<?php echo str_replace("'","\'",$sql_meds['title']);?>','<?php echo str_replace("'","\'",$sql_meds['creator']); ?>','<?php echo str_replace("'","\'",$sql_meds['from']); ?>')"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>-->
																			 <?php
																				}
																				else if($sql_meds['sharing_preference']==2){
																					if($sql_meds['id']!=""){
																					?>
																					<!--<a href="javascript:void(0);" onclick="ask_to_donate('<?php echo str_replace("'","\'",$sql_meds['title']);?>','<?php echo str_replace("'","\'",$sql_meds['creator']);?>','<?php echo $sql_meds['id'];?>','download_song<?php echo $sql_meds['id'];?>')"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>-->
																				<?php
																					}else{
																					?>
																					<!--<a href="javascript:void(0);" onclick="ask_to_donate('<?php echo str_replace("'","\'",$sql_meds['title']);?>','<?php echo str_replace("'","\'",$sql_meds['creator']);?>','<?php echo $sql_meds['id'];?>','download_song<?php echo $sql_meds['id'];?>')"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>-->
																				<?php
																					}
																				}
																				else{
																			?>
																				<!--<a href="javascript:void(0);" onclick="down_song_pop(<?php echo $sql_meds['id'];?>)"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>-->
																			<?php
																				}
																			}else{
																			?>	
																			<!--<img src="../images/profile/download-over.gif" style="cursor:default"/>-->
																			<?php
																			}
																			?>
																			<!--<a href="addtocart.php?seller_id=<?php echo $sql_meds['general_user_id']; ?>&media_id=<?php echo $sql_meds['id'] ?>&buyer_id=<?php echo $_SESSION['gen_user_id'];?>&title=<?php echo str_replace("'","\'",$sql_meds['title']); ?>&image_name=<?php echo $song_image_name_cart;?>" id="download_song<?php echo $sql_meds['id'];?>" style="display:none;"></a>-->
																				<script type="text/javascript">
																				$(document).ready(function() {
																						$("#download_song<?php echo $sql_meds['id'];?>").fancybox({
																						'width'				: '75%',
																						'height'			: '75%',
																						'transitionIn'		: 'none',
																						'transitionOut'		: 'none',
																						'type'				: 'iframe'
																						});
																				});
																				</script>
																			<?php
																		}
																		else if($sql_meds['featured_media']!="" || $sql_meds['featured_media']!="nodisplay"){
																			if($sql_meds['featured_media'] == "Song" && $sql_meds['media_id'] != "0"){
																				include_once("findsale_song.php");
																			?>
																				<img src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" onclick="showPlayer('a','<?php echo $sql_meds['media_id'];?>','play');" />
																				<img src="images/profile/add.gif" onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'" onclick="showPlayer('a','<?php echo $sql_meds['media_id'];?>','add');" />
																				<?php
																				$get_media_info_down_gal = $sql_feeds ->get_media_details($sql_meds['media_id']);
																				if($get_media_info_down_gal['sharing_preference']==5 || $get_media_info_down_gal['sharing_preference']==2)
																				{
																					$song_image_name_cartproject = $sql_feeds->get_audio_img($get_media_info_down_gal['id']);
																					$free_down_rel ="";
																					for($find_down=0;$find_down<=count($get_songs_pnew_w);$find_down++)
																					{
																						//echo $get_songs_pnew_w[$find_down]['id'] ."==". $get_Song_title['id']."</br>";
																						if(isset($get_media_info_down_gal) && $get_media_info_down_gal != null)
																						{
																							if($get_songs_pnew_w[$find_down]['id']==$get_media_info_down_gal['id']){
																							$free_down_rel = "yes";
																							}
																							else{
																								if($free_down_rel =="" || $free_down_rel =="no"){
																									$free_down_rel = "no";
																								}
																							}
																						}
																					}
																					if($free_down_rel == "yes"){
																					 ?>
																				<!--	 <a href="javascript:void(0);" onclick="download_directly_song('<?php echo $get_media_info_down_gal['id'];?>','<?php echo str_replace("'","\'",$get_media_info_down_gal['title']);?>','<?php echo str_replace("'","\'",$get_media_info_down_gal['creator']);?>','<?php echo str_replace("'","\'",$get_media_info_down_gal['from']);?>')"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>-->
																					<?php
																					}
																					else if($get_media_info_down_gal['sharing_preference']==2){
																					 ?>
																					<!--<a href="javascript:void(0);" onclick="ask_to_donate('<?php echo str_replace("'","\'",$get_media_info_down_gal['title']);?>','<?php echo str_replace("'","\'",$get_media_info_down_gal['creator']);?>','<?php echo $get_media_info_down_gal['id'];?>','download_songproject<?php echo $sql_meds['media_id'];?>')"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>-->
																				 <?php
																					}
																					else{
																				?>
																					<!--<a href="javascript:void(0);" onclick="down_song_pop_project(<?php echo $sql_meds['media_id'];?>)"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>-->
																				<?php
																					}
																				}else{
																				?>	
																				<!--<img src="../images/profile/download-over.gif" style="cursor:default"/>-->
																				<?php
																				}
																				?>
																				<!--<a href="addtocart.php?seller_id=<?php echo $get_media_info_down_gal['general_user_id'];?>&media_id=<?php echo $get_media_info_down_gal['id'];?>&buyer_id=<?php echo $_SESSION['gen_user_id'];?>&title=<?php echo str_replace("'","\'",$get_media_info_down_gal['title']);?>&image_name=<?php echo $song_image_name_cartproject;?>" id="download_songproject<?php echo $sql_meds['media_id'];?>" style="display:none;"></a>-->
																				<script type="text/javascript">
																				$(document).ready(function() {
																						$("#download_songproject<?php echo $sql_meds['media_id'];?>").fancybox({
																						'width'				: '75%',
																						'height'			: '75%',
																						'transitionIn'		: 'none',
																						'transitionOut'		: 'none',
																						'type'				: 'iframe'
																						});
																				});
																				</script>
																			<?php
																			}if($sql_meds['featured_media'] == "Gallery" && $sql_meds['media_id'] != "0"){
																				$get_all_feature_image3 = $sql_feeds->get_all_gallery_images($sql_meds['media_id']);
																				if($get_all_feature_image3!="" && $get_all_feature_image3!=NULL)
																				{
																					if(mysql_num_rows($get_all_feature_image3)>0)
																					{
																						$galleryImagesData3 ="";
																						$galleryImagestitle3 ="";
																						$mediaSrc3 ="";
																						$get_news_general_user_id = $uk -> get_general_user_idby_mediaid($sql_meds['media_id']);
																						while($row = mysql_fetch_assoc($get_all_feature_image3))
																						{
																							$mediaSrc3 = $row[0]['image_name'];
																							$filePath = $s3->getNewURI_otheruser("medgallery",$get_news_general_user_id).$mediaSrc3;
																							$galleryImagesData3 = $galleryImagesData3 .",". $row['image_name'];
																							$galleryImagestitle3 = $galleryImagestitle3 .",".$row['image_title'];
																						}
																						$orgPath3 = $s3->getNewURI_otheruser("medgallery",$get_news_general_user_id);
																						$thumbPath3 = $s3->getNewURI_otheruser("medgalthumb",$get_news_general_user_id);
																					}
																				}
																			?>
																				<a href="javascript:void(0);" onclick="checkload('<?php echo $mediaSrc3;?>','<?php echo $galleryImagesData3; ?>','<?php echo $thumbPath3; ?>','<?php echo $orgPath3; ?>','<?php echo $galleryImagestitle3; ?>','<?php echo $get_gallery['id'];?>')">
																				<img src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'"></a>
																				<img src="images/profile/add.gif" onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'">
																				<?php
																				$get_media_info_down_gal = $sql_feeds ->get_media_details($sql_meds['media_id']);
																				$get_gal_image_project  = $sql_feeds ->get_gallery_info($sql_meds['media_id']);
																				if($get_media_info_down_gal['sharing_preference']==2)
																				{
																				?>
																					<!--<a href="javascript:void(0);" onclick="ask_to_donate('<?php echo str_replace("'","\'",$get_media_info_down_gal['title']);?>','<?php echo str_replace("'","\'",$get_media_info_down_gal['creator']);?>','<?php echo $get_media_info_down_gal['id'];?>','download_galproject<?php echo $sql_meds['media_id'];?>')"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>-->
																				 <?php
																				}else{
																				?>
																				<!--<img src="../images/profile/download-over.gif" style="cursor:default" />-->
																				<?php
																				}
																				?>
																				<!--<a href="addtocart.php?seller_id=<?php echo $get_media_info_down_gal['general_user_id'];?>&media_id=<?php echo $get_media_info_down_gal['id'];?>&buyer_id=<?php echo $_SESSION['gen_user_id'];?>&title=<?php echo str_replace("'","\'",$get_media_info_down_gal['title']);?>&image_name=<?php echo $get_gal_image_project['profile_image'];?>" id="download_galproject<?php echo $sql_meds['media_id'];?>" style="display:none;"></a>-->
																				<script type="text/javascript">
																				$(document).ready(function() {
																						$("#download_galproject<?php echo $sql_meds['media_id'];?>").fancybox({
																						'width'				: '75%',
																						'height'			: '75%',
																						'transitionIn'		: 'none',
																						'transitionOut'		: 'none',
																						'type'				: 'iframe'
																						});
																				});
																				</script>
																			<?php
																			}if($sql_meds['featured_media'] == "Video" && $sql_meds['media_id'] != "0"){
																				include_once("findsale_video.php");
																			?>
																				<img src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" onclick="showPlayer('v','<?php echo $sql_meds['media_id'];?>','playlist');"/>
																				<img src="images/profile/add.gif" onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'" onclick="showPlayer('v','<?php echo $sql_meds['media_id'];?>','addvi');"/>
																				<?php
																				$get_media_info_down_gal = $sql_feeds ->get_media_details($sql_meds['media_id']);
																				if($get_media_info_down_gal['sharing_preference']==5 || $get_media_info_down_gal['sharing_preference']==2)
																				{
																					$get_video_image = $sql_feeds->getvideoimage_feature($get_media_info_down_gal['id']);
																					//$image_name_cartproject = "https://img.youtube.com/vi/".$get_video_image."/default.jpg";
																					
																					if($get_video_image[1]=='youtube')
																			{
																				$image_name_cartproject = "http://img.youtube.com/vi/".$get_video_image[0]."/default.jpg";
																			}
																			else
																			{
																				$video_vim = $vimeo->call('vimeo.videos.getInfo', array('video_id' => $get_video_image[0]));
																				$image_name_cartproject = $video_vim->video[0]->thumbnails->thumbnail[1]->_content;
																			}
																					$free_down_rel_video="";
																					for($find_down=0;$find_down<=count($get_vids_pnew_w);$find_down++)
																					{
																						if(isset($get_media_info_down_gal) && $get_media_info_down_gal != null){
																							if($get_vids_pnew_w[$find_down]['id']==$get_media_info_down_gal['id']){
																								$free_down_rel_video = "yes";
																							}
																							else{
																								if($free_down_rel_video =="" || $free_down_rel_video =="no"){
																									$free_down_rel_video = "no";
																								}
																							}
																						}
																					}
																					if($free_down_rel_video == "yes"){
																					 ?>
																					 <!--<a href="javascript:void(0);" onclick="download_directly_song('<?php echo $get_media_info_down_gal['id'];?>','<?php echo str_replace("'","\'",$get_media_info_down_gal['title']);?>','<?php echo str_replace("'","\'",$get_media_info_down_gal['creator']);?>','<?php echo str_replace("'","\'",$get_media_info_down_gal['from']);?>')"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>
																					 -->
																				 <?php
																					}
																					else if($get_media_info_down_gal['sharing_preference']==2){
																					 ?>
																						<!--<a href="javascript:void(0);" onclick="ask_to_donate('<?php echo str_replace("'","\'",$get_media_info_down_gal['title']);?>','<?php echo str_replace("'","\'",$get_media_info_down_gal['creator']);?>','<?php echo $get_media_info_down_gal['id'];?>','download_videoproject<?php echo $sql_meds['media_id'];?>')"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>-->
																					<?php
																					}
																					else{
																				?>
																					<!--<a href="javascript:void(0);" onclick="down_video_pop_project(<?php echo $sql_meds['media_id'];?>)"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>-->
																				<?php
																					}
																				}
																				else{
																				?>
																				<!--<img src="../images/profile/download-over.gif" style="cursor:default"/>-->
																				<?php
																				}
																				?>
																				<!--<a href="addtocart.php?seller_id=<?php echo $get_media_info_down_gal['general_user_id'];?>&media_id=<?php echo $get_media_info_down_gal['id'];?>&buyer_id=<?php echo $_SESSION['gen_user_id'];?>&title=<?php echo str_replace("'","\'",$get_media_info_down_gal['title']);?>&image_name=<?php echo $image_name_cartproject;?>" id="download_videoproject<?php echo $sql_meds['media_id'];?>" style="display:none;"></a>-->
																				<script type="text/javascript">
																				$(document).ready(function() {
																						$("#download_videoproject<?php echo $sql_meds['media_id'];?>").fancybox({
																						'height'			: '75%',
																						'width'				: '69%',
																						'transitionIn'		: 'none',
																						'transitionOut'		: 'none',
																						'type'				: 'iframe'
																						});
																				});
																				</script>
																			<?php
																			}
																		}
																		else{
																		?>
																			<img src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'">
																			<img src="images/profile/add.gif" onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'">
																			<!--<img style="cursor: default;" src="images/profile/download-over.gif">-->
																		<?php
																		}
																	?>
																		
																	</div>	
																<?php
																}
																?>
															</div>
				
															<div class="sideR">
																<div class="activityTitle"><?php
																	if($exp_meds[1]=='reg_med' || $ans_all_posts[$po_all]['profile_type']=='projects_post' || $ans_all_posts[$po_all]['profile_type']=='events_post')
																			{
																				if($ans_all_posts[$po_all]['profile_type']=='projects_post' || $ans_all_posts[$po_all]['profile_type']=='events_post' || $sql_meds['media_type']!=114)
																				{
																					if($sql_meds['profile_url']!="") {
		?>																				
																					<a href="/<?php echo $sql_meds['profile_url']; ?>">
		<?php
																		}
																					if($sql_meds['media_type']==115){
																					echo "<a href=javascript:void(0) onclick=showPlayer('v','".$sql_meds['id']."','playlist')>".$sql_meds['title']."</a>";
																					}else if($sql_meds['media_type']==113){
																						$get_all_image1 = $sql_feeds->get_all_gallery_images($sql_meds['id']);
																						$play_array1 = array();
																						if($get_all_image1!="" && $get_all_image1!=NULL)
																				{
																						if(mysql_num_rows($get_all_image1)>0)
																						{
																							while($row = mysql_fetch_assoc($get_all_image1))
																							{
																								$play_array1[] = $row;
																							}
																							$get_news_general_user_id = $uk -> get_general_user_idby_mediaid($sql_meds['id']);
																							${'orgPath'.$my_po}= $s3->getNewURI_otheruser("medgallery",$get_news_general_user_id);
																							${'thumbPath'.$my_po}= $s3->getNewURI_otheruser("medgalthumb",$get_news_general_user_id);
																							for($l=0;$l<count($play_array1);$l++)
																							{
																								$mediaSrc = $play_array1[0]['image_name'];
																								$filePath = $s3->getNewURI_otheruser("medgallery",$get_news_general_user_id).$mediaSrc;
																								
																								
																								${'galleryImagesData'.$my_po}[$galleryImagesDataIndex] = $play_array1[$l]['image_name'];
																								${'galleryImagestitle'.$my_po}[$galleryImagesDataIndex] = $play_array1[$l]['image_title'];
																								//echo ${'galleryImagesData'.$my_po}[$galleryImagesDataIndex];
																								$galleryImagesDataIndex++;
																							}
																							
																						}
																						}
																						if(isset(${'galleryImagesData'.$my_po}))
																						{
																							${'galleryImagesData'.$my_po} = implode(",", ${'galleryImagesData'.$my_po});
																							${'galleryImagestitle'.$my_po} = implode(",", ${'galleryImagestitle'.$my_po});
																						}
																						echo "<a href=javascript:void(0) onclick=checkload('".$mediaSrc."','". ${'galleryImagesData'.$my_po}."','". ${'thumbPath'.$my_po}."','". ${'orgPath'.$my_po}."','". ${'galleryImagestitle'.$my_po}."','". $get_gallery['id']."')>".$sql_meds['title']."</a>";
																						$my_po = $my_po + 1;
																					}else{
																					echo $sql_meds['title'];
																					}
																					if($sql_meds['profile_url']!="") {
		?>																				
																					</a>
		<?php
																		}
																				}
																				elseif($exp_meds[1]=='reg_med' && $sql_meds['media_type']==114)
																				{
																					$sql_me = mysql_query("SELECT * FROM media_songs WHERE media_id='".$sql_meds['id']."'");
																					$ans_me = mysql_fetch_assoc($sql_me);
																					$ecehos = '<a style="text-decoration: none; cursor: text;" href="javascript:void(0)"> '.$ans_me['track'].' </a>';
																					//$ecehos .= '<a href="">'.$sql_meds['title'].'</a>';
																					if($sql_meds['media_type']==114)
																					{
																						//$ecehos .= "<a href=javascript:void(0) onclick=showPlayer('a',".$sql_meds['id'].",'add');>".$sql_meds['title']."</a>";
																						$ecehos .= "<a style='text-decoration: none; cursor: text;' href='javascript:void(0)'>".$sql_meds['title']."</a>";
																						
																					}
																					else if($sql_meds['media_type']==115)
																					{
																						$ecehos .= "<a href=javascript:void(0) onclick=showPlayer('v',".$sql_meds['id'].",'addvi');>".$sql_meds['title']."</a>";
																					}
																					else if($sql_meds['media_type']==113)
																					{
																						$get_all_gal_image = $sql_feeds->get_all_gallery_images($sql_meds['id']);
																						if($get_all_gal_image!="" && $get_all_gal_image!=NULL)
																				{
																						if(mysql_num_rows($get_all_gal_image)>0)
																						{
																							$galleryImagesData ="";
																							$galleryImagestitle ="";
																							$mediaSrc ="";
																							$get_news_general_user_id = $uk -> get_general_user_idby_mediaid($sql_meds['id']);
																							while($row = mysql_fetch_assoc($get_all_gal_image))
																							{
																								$mediaSrc = $row[0]['image_name'];
																								$filePath = $s3->getNewURI_otheruser("medgallery",$get_news_general_user_id).$mediaSrc;
																								if($row['image_name']!=""){
																									$galleryImagesData = $galleryImagesData .",". $row['image_name'];
																									$galleryImagestitle = $galleryImagestitle .",".$row['image_title'];
																								}
																							}
																							$orgPath = $s3->getNewURI_otheruser("medgallery",$get_news_general_user_id);
																							$thumbPath = $s3->getNewURI_otheruser("medgalthumb",$get_news_general_user_id);
																						}
																					}
																					
																						$ecehos .= "<a href=javascript:void(0) onclick=checkload('".$mediaSrc."','".$galleryImagesData."','".$thumbPath."','".$orgPath."','". $galleryImagestitle."','".$get_gallery['id'].")>".$sql_meds['title']."</a>";
																						$galleryImagesData = "";
																						$galleryImagestitle = "";
																						$mediaSrc ="";
																					}
																					else
																					{
																						$ecehos .= '<a href="">'.$sql_meds['title'].'</a>';
																					}
																	
																	if($sql_meds['creator']!="")
																	{
																		$create_c = strpos($sql_meds['creator'],'(');
																		$end = substr($sql_meds['creator'],0,$create_c);
																		$exp_cr = explode('|',$sql_meds['creator_info']);
																		
																		if($exp_cr[0]=='general_artist')
																		{
																			$sql_c_ar = mysql_query("SELECT * FROM general_artist WHERE artist_id='".$exp_cr[1]."'");
																		}
																		if($exp_cr[0]=='general_community')
																		{
																			$sql_c_ar = mysql_query("SELECT * FROM general_community WHERE community_id='".$exp_cr[1]."'");
																		}
																		if($exp_cr[0]=='artist_project' || $exp_cr[0]=='community_project' || $exp_cr[0]=='community_event' || $exp_cr[0]=='artist_event')
																		{
																			$sql_c_ar = mysql_query("SELECT * FROM $exp_cr[0] WHERE id='".$exp_cr[1]."'");
																		}
																		
																		
																		if(isset($sql_c_ar))
																		{
																			if($sql_c_ar!="" && $sql_c_ar!=NULL)
																				{
																			if(mysql_num_rows($sql_c_ar)>0)
																			{
																				$ans_c_ar = mysql_fetch_assoc($sql_c_ar);
																				if(isset($ans_c_ar['name']))
																				{
																					$end = $ans_c_ar['name'];
																				}
																				else
																				{
																					$end = $ans_c_ar['title'];
																				}
																				if($ans_c_ar['profile_url']!="" && !empty($ans_c_ar['profile_url']))
																				{
																					$ecehos .= " By  <a href='/".$ans_c_ar['profile_url']."'><b>".$end."</b></a>  ";
																				}
																				else
																				{
																					$ecehos .= " By  <b>".$end."</b>  ";
																				}
																			}
																				else
																			{
																				$ecehos .= " By  <b>".$end."</b>  ";
																			}
																			}
																			else
																			{
																				$ecehos .= " By  <b>".$end."</b>  ";
																			}
																		}
																		else
																		{
																			
																			$ecehos .= " By  <b>".$end."</b>  ";
																		}
																	}
																	if($sql_meds['from']!="")
																	{
																		$create_f = strpos($sql_meds['from'],'(');
																		$end_f = substr($sql_meds['from'],0,$create_f);
																		
																		$exp_fr = explode('|',$sql_meds['from_info']);
																		if($exp_fr[0]=='general_artist')
																		{
																			$sql_f_ar = mysql_query("SELECT * FROM general_artist WHERE artist_id='".$exp_fr[1]."'");
																		}
																		if($exp_fr[0]=='general_community')
																		{
																			$sql_f_ar = mysql_query("SELECT * FROM general_community WHERE community_id='".$exp_fr[1]."'");
																		}
																		if($exp_fr[0]=='artist_project' || $exp_fr[0]=='community_project' || $exp_fr[0]=='community_event' || $exp_fr[0]=='artist_event')
																		{
																			$sql_f_ar = mysql_query("SELECT * FROM $exp_fr[0] WHERE id='".$exp_fr[1]."'");
																		}
																		
																		if(isset($sql_f_ar))
																		{
																			if($sql_f_ar!="" && $sql_f_ar!=NULL)
																				{
																			if(mysql_num_rows($sql_f_ar)>0)
																			{
																				$ans_f_ar = mysql_fetch_assoc($sql_f_ar);
																				if(isset($ans_f_ar['name']))
																				{
																					$end_f = $ans_f_ar['name'];
																				}
																				else
																				{
																					$end_f = $ans_f_ar['title'];
																				}
																				if($ans_f_ar['profile_url']!="" && !empty($ans_f_ar['profile_url']))
																				{
																					$ecehos .= " From  <a href='/".$ans_f_ar['profile_url']."'><b>".$end_f."</b></a>";
																				}
																				else
																				{
																					$ecehos .= " From  <b>".$end_f."</b>";
																				}
																			}
																			else
																			{
																				$ecehos .= " From  <b>".$end_f."</b>";
																			}
																			}
																			else
																			{
																				$ecehos .= " From  <b>".$end_f."</b>";
																			}
																		}
																		else
																		{
																			$ecehos .= " From  <b>".$end_f."</b>";
																		}
																	}
																	echo $ecehos;
																				}
																			}
																			elseif($exp_meds[1]=='reg_art' || $exp_meds[1]=='reg_com')
																			{
																				if($ans_all_posts[$po_all]['profile_type']=='images_post')
																				{
																					echo $sql_meds['gallery_title'];
																				}
																				elseif($ans_all_posts[$po_all]['profile_type']=='songs_post')
																				{
																					echo $sql_meds['audio_name'];
																				}
																				elseif($ans_all_posts[$po_all]['profile_type']=='videos_post')
																				{
																					echo "<a href=javascript:void(0) onclick=showPlayer('v','".$sql_meds['id']."','playlist')>".$sql_meds['video_name']."</a>";
																				}
																			}
		?>
																	<div style="color:#808080;font-size:12px;">
																		<div class="comment more">
																			<?php echo $ans_all_posts[$po_all]['description']; ?>
																		</div>
																	</div>
																</div>
																
																<!--<div class="activityContent">Tagged: <?php //echo $medias['tagged_users']; 
																//$name_org = explode(')',$medias['tagged_users']);
																//for($i_name_org=0;$i_name_org<count($name_org);$i_name_org++)
																//{
																	//	$end_pos = strpos($name_org[$i_name_org],'(');
																	//	$org_name = substr($name_org[$i_name_org],0,$end_pos);
																	//		echo $org_name;
																//	}
																?></div>-->
															</div>
														</div>
													</div>
												</div>
												
												<div class="rightSide">
													<a href="#" onmouseover="mopen('mm<?php echo $po_all;?>4')" onmouseout="mclosetime()"><img src="images/profile/arrow.png" onmouseover="this.src='images/profile/arrow-down.png'" onmouseout="this.src='images/profile/arrow.png'" /></a>
													<div id="mm<?php echo $po_all;?>4" class="popUp" onmouseover="mcancelclosetime()" onmouseout="mclosetime()">
														<ul>
															<li><a href="hideposts.php?id=<?php echo $ans_all_posts[$po_all]['id'];?>">Hide Feed</a></li>
															<li><a href="hideposts.php?report_id=<?php echo $ans_all_posts[$po_all]['id'];?>">Report spam</a></li>
															<li><div class="seperator"></div></li>
															<li><!--<a href="Unsubscribe_feeds.php?frd=1&user_ids=<?php echo $gens['general_user_id']; ?>" onclick="return confirm_subscription_delete('<?php if($art==1){ echo $ros['name']; } elseif($com==1){ echo $ros_com['name']; } ?>')">Unsubscribe from <?php if($art==1)
															{
																echo $ros['name'];
															}
															elseif($com==1)
															{
																echo $ros_com['name'];
															} ?></a>-->
															<a href="profileedit.php#subscrriptions" >Unsubscribe from <?php if($art==1)
															{
																echo $ros['name'];
															}
															elseif($com==1)
															{
																echo $ros_com['name'];
															} ?></a>
															</li>
														</ul>
													</div>
												</div>
											</div>
			<?php
										}
									}
								}
							}
						}
					}
				}
			}
		?>
		</div>
		
		<div id="photo_feeds" style="display:none;">
		<?php
			$ans_photo_feeds = array();
			$ans_photo_sub_feeds = array();
			$ans_fan_club_photo = array();
			$ph_pro_eve_id = array();
			$ph_art_com_id = array();
			$ph_arts_ids = array();
			$ph_coms_ids = array();
			$ans_photo_posts = array();
			
			$sql_photo_feeds = $sql_feeds->all_feeds($chk_sql['email']);
			$run_photo_feeds = mysql_query($sql_photo_feeds);
					
			$fan_sql_photo_feeds = $sql_feeds->fan_club($chk_sql['email']);
			$fan_run_photo_feeds = mysql_query($fan_sql_photo_feeds);
			
			if($run_photo_feeds!="" && $run_photo_feeds!=NULL)
			{			
				while($row_photo_feeds = mysql_fetch_assoc($run_photo_feeds))
				{
					$ans_photo_sub_feeds[] = $row_photo_feeds;
				}
			}
			
			if($fan_run_photo_feeds!="" && $fan_run_photo_feeds!=NULL)
			{			
				while($fan_row_photo_feeds = mysql_fetch_assoc($fan_run_photo_feeds))
				{
					$ans_fan_club_photo[] = $fan_row_photo_feeds;
				}
			}
			
			$ans_photo_feeds = array_merge($ans_photo_sub_feeds,$ans_fan_club_photo);
			
			$ans_frds_posts = array();
			$frds_posts_all_l = $sql_feeds->all_frds_posts($_SESSION['login_id']);
			$frds_posts_all = mysql_query($frds_posts_all_l);
			if($frds_posts_all!="" && $frds_posts_all!=NULL)
			{
				if(mysql_num_rows($frds_posts_all)>0)
				{
					while($row_f = mysql_fetch_assoc($frds_posts_all))
					{
						$ans_frds_posts[] = $row_f;
					}
				}
			}
			
			if(($ans_photo_feeds!=NULL && !empty($ans_photo_feeds)) || ($ans_frds_posts!="" && !empty($ans_frds_posts)))
			{
				for($ph_fed_lo=0;$ph_fed_lo<count($ans_photo_feeds);$ph_fed_lo++)
				{
					if($ans_photo_feeds[$ph_fed_lo]['related_type']!='artist' && $ans_photo_feeds[$ph_fed_lo]['related_type']!='community')
					{
						$num_ids = explode('_',$ans_photo_feeds[$ph_fed_lo]['related_id']);
						if($ans_photo_feeds[$ph_fed_lo]['related_type']=='artist_project' || $ans_photo_feeds[$ph_fed_lo]['related_type']=='artist_event')
						{
							$ph_pro_eve_id[$ph_fed_lo]['related_id_art'] = $num_ids[0];
						}
						elseif($ans_photo_feeds[$ph_fed_lo]['related_type']=='community_project' || $ans_photo_feeds[$ph_fed_lo]['related_type']=='community_event')
						{
							$ph_art_com_id[$ph_fed_lo]['related_id_com'] = $num_ids[0];
						}
					}
					elseif($ans_photo_feeds[$ph_fed_lo]['related_type']=='artist' || $ans_photo_feeds[$ph_fed_lo]['related_type']=='community')
					{
						if($ans_photo_feeds[$ph_fed_lo]['related_type']=='artist')
						{
							$ph_pro_eve_id[$ph_fed_lo]['related_id_art'] = $ans_photo_feeds[$ph_fed_lo]['related_id'];
						}
						elseif($ans_photo_feeds[$ph_fed_lo]['related_type']=='community')
						{
							$ph_art_com_id[$ph_fed_lo]['related_id_com'] = $ans_photo_feeds[$ph_fed_lo]['related_id'];
						}
					}
				}
		
				if($ph_pro_eve_id!=0 && count($ph_pro_eve_id)>0)
				{
					for($arts=0;$arts<count($ph_pro_eve_id);$arts++)
					{
						$org_art[] = $ph_pro_eve_id[$arts]['related_id_art'];
					}
								
					$new_art = array();
					$pho = 0;
					foreach ($org_art as $key => $value)
					{
						if(isset($new_art[$value]))
						{
							$new_art[$value] += 1;
						}
						else
						{
							$new_art[$value] = 1;									
							$sqls = $sql_feeds->arts_gen($value);
													
							if($sqls!="")
							{
								$ph_arts_ids[$pho] = $sqls;
							}
						}
						$pho = $pho + 1;
					}
				}
								
				if($ph_art_com_id!="" && count($ph_art_com_id)>0)
				{
					for($coms=0;$coms<count($ph_art_com_id);$coms++)
					{
						$org_com[] = $ph_art_com_id[$coms]['related_id_com'];
					}
											
					$new_com = array();
					$iph_c = 0;
					foreach ($org_com as $key => $value)
					{
						if(isset($new_com[$value]))
						{
							$new_com[$value] += 1;
						}
						else
						{
							$new_com[$value] = 1;
							
							$sqls_c = $sql_feeds->coms_gen($value);
							if($sqls_c!="")
							{
								$ph_coms_ids[$iph_c] = $sqls_c;
							}
						}
						$iph_c = $iph_c + 1;
					}
				}
				
				$ans_frds_posts = array();
				$frds_posts_all_l = $sql_feeds->all_frds_posts($_SESSION['login_id']);
				$frds_posts_all = mysql_query($frds_posts_all_l);
				if($frds_posts_all!="" && $frds_posts_all!=NULL)
				{
					if(mysql_num_rows($frds_posts_all)>0)
					{
						while($row_f = mysql_fetch_assoc($frds_posts_all))
						{
							$ans_frds_posts[] = $row_f;
						}
					}
				}
				
				$doub_ph_chk = array_merge($ph_arts_ids,$ph_coms_ids,$ans_frds_posts);
				
				for($chp_d=0;$chp_d<count($doub_ph_chk);$chp_d++)
				{
					$rel_d_id[] = $doub_ph_chk[$chp_d]['general_user_id'];
				}
										
				$new_array_rep_p = array();
				foreach ($rel_d_id as $key => $value)
				{
					if(isset($new_array_rep_p[$value]))
						$new_array_rep_p[$value] += 1;
					else
						$new_array_rep_p[$value] = 1;
				}
				foreach ($new_array_rep_p as $uid => $n)
				{
					$ex_uid_ph = $ex_uid_ph.','.$uid;
				}
				$ex_tr_p = trim($ex_uid_ph, ",");
				$sep_ids_p = explode(',',$ex_tr_p);
				
				if($sep_ids_p!="" && count($sep_ids_p)>0)
				{
					for($se_ca=0;$se_ca<count($sep_ids_p);$se_ca++)
					{
						$types = 'images_post';
						$ids = $sql_feeds->pmva_posts_check($sep_ids_p[$se_ca],$_SESSION['login_id'],$types);
						if($ids!="" && count($ids)>0)
						{
							$posts_photo = $sql_feeds->pmv_posts_check($sep_ids_p[$se_ca],$_SESSION['login_id'],$types);
							if($posts_photo!="" && $posts_photo!=NULL)
							{
								if($posts_photo!="" && mysql_num_rows($posts_photo)>0)
								{
									while($rows_posts_all = mysql_fetch_assoc($posts_photo))
									{
										$ans_photo_posts[] = $rows_posts_all;
									}
								}
							}
						}
					}
				}
										
				if($ans_photo_posts!="" && count($ans_photo_posts)>0)
				{
					for($hpo_all=0;$hpo_all<count($ans_photo_posts);$hpo_all++)
					{
						if($ans_photo_posts[$hpo_all]['profile_type']=='images_post')
						{
							$exp_meds = explode('~',$ans_photo_posts[$hpo_all]['media']);
							if($exp_meds[1]=='reg_med')
							{
								$sql_meds_ph = $sql_feeds->get_mediaSelected($exp_meds[0]);
							}
							elseif(($exp_meds[1]=='reg_art' || $exp_meds[1]=='reg_com') && $ans_photo_posts[$hpo_all]['profile_type']=='images_post')
							{
								if($exp_meds[1]=='reg_art')
								{
									$reg_table_type = 'general_artist_gallery_list';
								}
								elseif($exp_meds[1]=='reg_com')
								{
									$reg_table_type = 'general_community_gallery_list';
								}
														
								$sql_meds_ph = $sql_feeds->get_galleryData($reg_table_type,$exp_meds[0]);
							}
						}
							
						if(isset($sql_meds_ph) && $sql_meds_ph!='0' && $sql_meds_ph!=NULL)
						{
							$gens = $sql_feeds->get_log_ids($ans_photo_posts[$hpo_all]['general_user_id']);
							if($ans_photo_posts[$hpo_all]['display_to']=='All' && $ans_photo_posts[$hpo_all]['profile_type']=='images_post')
							{
								$hide12 = 0;
								$get_gen_id = $sql_feeds->get_general_info();
								$hide_by = $ans_photo_posts[$hpo_all]['hidden_by'];
								$exp_hide_by = explode(",",$hide_by);
								for($exp_count=0;$exp_count<=count($exp_hide_by);$exp_count++)
								{
									if($exp_hide_by[$exp_count] ==$get_gen_id['general_user_id'])
									{
										$hide12 =1;
									}
								}
								if($hide12 != 1)
								{
		?>
									<div class="storyCont">
										<div class="leftSide">
											<div class="avatar">
		<?php
												$art = 0;
													
												if($gens['artist_id']!=0)
												{
													$sql_art = mysql_query("SELECT * FROM general_artist WHERE artist_id='".$gens['artist_id']."' AND status=0 AND active=0 AND del_status=0");
													if($sql_art!="" && $sql_art!=NULL)
													{
														if(mysql_num_rows($sql_art)>0)
														{
															$ros = mysql_fetch_assoc($sql_art);
															$art = 1;
														}
													}
												}
													
												$com = 0;
												if($gens['community_id']!=0)
												{
													$sql_com = mysql_query("SELECT * FROM general_community WHERE community_id='".$gens['community_id']."' AND status=0 AND active=0 AND del_status=0");
													if($sql_com!="" && $sql_com!=NULL)
													{
														if(mysql_num_rows($sql_com)>0)
														{
															$ros_com = mysql_fetch_assoc($sql_com);
															$com = 1;
														}
													}
												}
													
												if($art==1)
												{
													$get_news_general_user_id = $uk -> get_general_user_idby_artistid($ros['artist_id']);
		?>
													<img width="50" height="50" src="<?php
													if($ros['image_name']!="" && !empty($ros['image_name']))
													{
														echo $s3->getNewURI_otheruser("artjcropprofile",$get_news_general_user_id) . $ros['image_name']; 
													}
													else
													{
														echo "http://artjcropprofile.s3.amazonaws.com/Noimage.png";
													}
													?>" />
		<?php
												}
												elseif($com==1)
												{
													$get_news_general_user_id = $uk -> get_general_user_idby_communityid($ros_com['community_id']);
		?>
													<img width="50" height="50" src="<?php
													
													
													if($ros_com['image_name']!="" && !empty($ros_com['image_name']))
													{
														echo  $s3->getNewURI_otheruser("comjcropprofile",$get_news_general_user_id) . $ros_com['image_name']; 
													}
													else
													{
														echo "http://comjcropprofile.s3.amazonaws.com/Noimage.png";
													}
													
													?>" />
													<?php
												}
		?>
											</div>
		<?php 
												//echo "table=".$reg_table_media_type;
												//echo $ans_audio_feeds[$audio_fed_lo]['id'];
		?>
											<div class="storyContainer">
												<div class="storyTitle">
													<span class="userName">
														
			<?php 
															if($art==1)
															{
																if($ros['profile_url']!="")
														{
													?>
														<a href="/<?php echo $ros['profile_url']; ?>">
													<?php
														}
																echo $ros['name'];
																if($ros['profile_url']!="")
														{
													?>
														</a>
													<?php
														}
															}
															elseif($com==1)
															{
																if($ros_com['profile_url']!="")
														{
													?>
														<a href="/<?php echo $ros_com['profile_url']; ?>">
													<?php
														}
																echo $ros_com['name'];
																if($ros_com['profile_url']!="")
														{
													?>
														</a>
													<?php
														}
															} 
		?>
														
													</span>
		<?php 
													//echo $ans_photo_posts[$hpo_all]['description'];
		?>
												</div>
											
												<div class="activityCont">
													<div class="sideL">
														<div class="storyImage" style="position:relative;"><?php
														
															$get_all_image = $sql_feeds->get_all_gallery_images($sql_meds_ph['id']);
															$play_array = array();
															if($get_all_image!="" && $get_all_image!=NULL)
															{
															if(mysql_num_rows($get_all_image)>0)
															{
																while($row = mysql_fetch_assoc($get_all_image))
																{
																	$play_array[] = $row;
																}
																$get_news_general_user_id = $uk -> get_general_user_idby_mediaid($sql_meds_ph['id']);
																${'orgPath'.$hpo_all}= $s3->getNewURI_otheruser("medgallery",$get_news_general_user_id);
																${'thumbPath'.$hpo_all}= $s3->getNewURI_otheruser("medgalthumb",$get_news_general_user_id);
																for($l=0;$l<count($play_array);$l++)
																{
																	$mediaSrc = $play_array[0]['image_name'];
																	$filePath = $s3->getNewURI_otheruser("medgallery",$get_news_general_user_id).$mediaSrc;
																	
																	
																	${'galleryImagesData'.$hpo_all}[$galleryImagesDataIndex] = $play_array[$l]['image_name'];
																	${'galleryImagestitle'.$hpo_all}[$galleryImagesDataIndex] = $play_array[$l]['image_title'];
																	//echo ${'galleryImagesData'.$hpo_all}[$galleryImagesDataIndex];
																	$galleryImagesDataIndex++;
																}
																
															}
															}
															if(isset(${'galleryImagesData'.$hpo_all}))
															{
																${'galleryImagesData'.$hpo_all} = implode(",", ${'galleryImagesData'.$hpo_all});
																${'galleryImagestitle'.$hpo_all} = implode(",", ${'galleryImagestitle'.$hpo_all});
															}
														if($sql_meds_ph['media_type']==113)
																		{
																			$get_gal_image = $sql_feeds->gallery_meds($sql_meds_ph['id']);
																			if($get_gal_image!="" && !empty($get_gal_image))
																			{
																				if($get_gal_image['profile_image']!="")
																				{
																		?>
																					<a onmouseout="newfunctionout('<?php echo $sql_meds_ph['id']; ?>','g')" onmouseover="newfunction('<?php echo $sql_meds_ph['id']; ?>','g')" href="javascript:void(0);" onclick="checkload('<?php echo $mediaSrc;?>','<?php echo ${'galleryImagesData'.$hpo_all}; ?>','<?php echo ${'thumbPath'.$hpo_all}; ?>','<?php echo ${'orgPath'.$hpo_all}; ?>','<?php echo ${'galleryImagestitle'.$hpo_all}; ?>','<?php echo $get_gallery['id'];?>')">
																					<img src="<?php echo $get_gal_image['profile_image']; ?>" width="90" height="90" />
																					</a>
																		<?php
																				}
																				else
																				{
																		?>
																				<a onmouseout="newfunctionout('<?php echo $sql_meds_ph['id']; ?>','g')" onmouseover="newfunction('<?php echo $sql_meds_ph['id']; ?>','g')" href="javascript:void(0);" onclick="checkload('<?php echo $mediaSrc;?>','<?php echo ${'galleryImagesData'.$hpo_all}; ?>','<?php echo ${'thumbPath'.$hpo_all}; ?>','<?php echo ${'orgPath'.$hpo_all}; ?>','<?php echo ${'galleryImagestitle'.$hpo_all}; ?>','<?php echo $get_gallery['id'];?>')">
																					<img width="90" height="90" src="http://comjcropprofile.s3.amazonaws.com/<?php echo "Noimage.png";?>" />
																				</a>	
																				<?php	
																				}
																			}
																			else
																			{
																		?>
																			<a onmouseout="newfunctionout('<?php echo $sql_meds_ph['id']; ?>','g')" onmouseover="newfunction('<?php echo $sql_meds_ph['id']; ?>','g')" href="javascript:void(0);" onclick="checkload('<?php echo $mediaSrc;?>','<?php echo ${'galleryImagesData'.$hpo_all}; ?>','<?php echo ${'thumbPath'.$hpo_all}; ?>','<?php echo ${'orgPath'.$hpo_all}; ?>','<?php echo ${'galleryImagestitle'.$hpo_all}; ?>','<?php echo $get_gallery['id'];?>')">
																				<img width="90" height="90" src="http://comjcropprofile.s3.amazonaws.com/<?php echo "Noimage.png";?>" />
																			</a>
																			<?php	
																		
																			}
																		}
																		elseif($exp_meds[1]=='reg_art' && $ans_all_posts[$po_all]['profile_type']=='images_post')
																		{
																			$tables = 'artist';
																			$imgs = $sql_feeds->reg_gal($tables,$sql_meds_ph['gallery_id']);
																			
																			if($imgs!="" && !empty($imgs))
																			{
																				if($imgs['image_name']!="")
																				{
																			?>
																				<a onmouseout="newfunctionout('<?php echo $sql_meds_ph['id']; ?>','g')" onmouseover="newfunction('<?php echo $sql_meds_ph['id']; ?>','g')" href="javascript:void(0);" onclick="checkload('<?php echo $mediaSrc;?>','<?php echo ${'galleryImagesData'.$hpo_all}; ?>','<?php echo ${'thumbPath'.$hpo_all}; ?>','<?php echo ${'orgPath'.$hpo_all}; ?>','<?php echo ${'galleryImagestitle'.$hpo_all}; ?>','<?php echo $get_gallery['id'];?>')">
																					<img width="90" height="90" src="http://reggalthumb.s3.amazonaws.com/<?php echo $imgs['image_name'];?>"/>
																				</a>	
																				<?php	
																				}
																				else
																				{
																			?>
																				<a onmouseout="newfunctionout('<?php echo $sql_meds_ph['id']; ?>','g')" onmouseover="newfunction('<?php echo $sql_meds_ph['id']; ?>','g')" href="javascript:void(0);" onclick="checkload('<?php echo $mediaSrc;?>','<?php echo ${'galleryImagesData'.$hpo_all}; ?>','<?php echo ${'thumbPath'.$hpo_all}; ?>','<?php echo ${'orgPath'.$hpo_all}; ?>','<?php echo ${'galleryImagestitle'.$hpo_all}; ?>','<?php echo $get_gallery['id'];?>')">
																					<img width="90" height="90" src="http://comjcropprofile.s3.amazonaws.com/<?php echo "Noimage.png";?>"/>
																				</a>	
																				<?php	
																				}
																			}
																			else
																			{
																			?>
																			<a onmouseout="newfunctionout('<?php echo $sql_meds_ph['id']; ?>','g')" onmouseover="newfunction('<?php echo $sql_meds_ph['id']; ?>','g')" href="javascript:void(0);" onclick="checkload('<?php echo $mediaSrc;?>','<?php echo ${'galleryImagesData'.$hpo_all}; ?>','<?php echo ${'thumbPath'.$hpo_all}; ?>','<?php echo ${'orgPath'.$hpo_all}; ?>','<?php echo ${'galleryImagestitle'.$hpo_all}; ?>','<?php echo $get_gallery['id'];?>')">
																				<img width="90" height="90" src="http://comjcropprofile.s3.amazonaws.com/<?php echo "Noimage.png";?>"/>
																			</a>	
																			<?php	
																			}
																		}
																		elseif($exp_meds[1]=='reg_com' && $ans_all_posts[$po_all]['profile_type']=='images_post')
																		{
																			$tables = 'community';
																			$imgs = $sql_feeds->reg_gal($tables,$sql_meds_ph['gallery_id']);
																			
																			if($imgs!="" && !empty($imgs))
																			{
																				if($imgs['image_name']!="")
																				{
																			?>
																				<a onmouseout="newfunctionout('<?php echo $sql_meds_ph['id']; ?>','g')" onmouseover="newfunction('<?php echo $sql_meds_ph['id']; ?>','g')" href="javascript:void(0);" onclick="checkload('<?php echo $mediaSrc;?>','<?php echo ${'galleryImagesData'.$hpo_all}; ?>','<?php echo ${'thumbPath'.$hpo_all}; ?>','<?php echo ${'orgPath'.$hpo_all}; ?>','<?php echo ${'galleryImagestitle'.$hpo_all}; ?>','<?php echo $get_gallery['id'];?>')">
																					<img width="90" height="90" src="http://reggalthumb.s3.amazonaws.com/"<?php echo $imgs['image_name'];?> />
																				</a>	
																			<?php
																				}
																				else
																				{
																			?>
																				<a onmouseout="newfunctionout('<?php echo $sql_meds_ph['id']; ?>','g')" onmouseover="newfunction('<?php echo $sql_meds_ph['id']; ?>','g')" href="javascript:void(0);" onclick="checkload('<?php echo $mediaSrc;?>','<?php echo ${'galleryImagesData'.$hpo_all}; ?>','<?php echo ${'thumbPath'.$hpo_all}; ?>','<?php echo ${'orgPath'.$hpo_all}; ?>','<?php echo ${'galleryImagestitle'.$hpo_all}; ?>','<?php echo $get_gallery['id'];?>')">
																					<img width="90" height="90" src="http://comjcropprofile.s3.amazonaws.com/"<?php echo "Noimage.png";?> />
																				</a>	
																			<?php
																				}
																			}
																			else
																			{
																			?>
																			<a onmouseout="newfunctionout('<?php echo $sql_meds_ph['id']; ?>','g')" onmouseover="newfunction('<?php echo $sql_meds_ph['id']; ?>','g')" href="javascript:void(0);" onclick="checkload('<?php echo $mediaSrc;?>','<?php echo ${'galleryImagesData'.$hpo_all}; ?>','<?php echo ${'thumbPath'.$hpo_all}; ?>','<?php echo ${'orgPath'.$hpo_all}; ?>','<?php echo ${'galleryImagestitle'.$hpo_all}; ?>','<?php echo $get_gallery['id'];?>')">
																				<img width="90" height="90" src="http://comjcropprofile.s3.amazonaws.com/"<?php echo "Noimage.png";?> />
																			</a>	
																			<?php
																			}
																		}
																		
																		
																		?>
																		<div class="navButton">
			<div class="playMedia">
				<a href="javascript:void(0);" class="list_play"><img title="Add file to the bottom of your playlist without stopping current media." id="play_video_image<?php echo $sql_meds_ph['id']; ?>g" onclick="checkload('<?php echo $mediaSrc;?>','<?php echo ${'galleryImagesData'.$hpo_all}; ?>','<?php echo ${'thumbPath'.$hpo_all}; ?>','<?php echo ${'orgPath'.$hpo_all}; ?>','<?php echo ${'galleryImagestitle'.$hpo_all}; ?>','<?php echo $get_gallery['id'];?>')" src="/images/play.png" onmouseout="newfunctionout('<?php echo $sql_meds_ph['id']; ?>','g')" onmouseover="newfunction('<?php echo $sql_meds_ph['id']; ?>','g')" style="border-radius: 6px;"></a>
			</div>
		</div>
																	</div>
																	
														<!--<div class="player">
															<a href="javascript:void(0);" onclick="checkload('<?php /*echo $mediaSrc;?>','<?php echo ${'galleryImagesData'.$hpo_all}; ?>','<?php echo ${'thumbPath'.$hpo_all}; ?>','<?php echo ${'orgPath'.$hpo_all}; ?>','<?php echo ${'galleryImagestitle'.$hpo_all}; ?>','<?php echo $get_gallery['id'];*/?>')">
															<img src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'"></a>
															<img src="images/profile/add.gif" onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'">
															<img style="cursor: default;" src="images/profile/download-over.gif">
														</div>-->
													</div>
										
													<div class="sideR">
														<div class="activityTitle"><?php
														if($exp_meds[1]=='reg_med')
														{
															$get_all_gal_image = $sql_feeds->get_all_gallery_images($sql_meds_ph['id']);
															if($get_all_gal_image!="" && $get_all_gal_image!=NULL)
															{
																if(mysql_num_rows($get_all_gal_image)>0)
																{
																	$galleryImagesData ="";
																	$galleryImagestitle ="";
																	$mediaSrc ="";
																	$get_news_general_user_id = $uk -> get_general_user_idby_mediaid($sql_meds_ph['id']);
																	while($row = mysql_fetch_assoc($get_all_gal_image))
																	{
																		$mediaSrc = $row[0]['image_name'];
																		$filePath = $s3->getNewURI_otheruser("medgallery",$get_news_general_user_id).$mediaSrc;
																		if($row['image_name']!=""){
																			$galleryImagesData = $galleryImagesData .",". $row['image_name'];
																			$galleryImagestitle = $galleryImagestitle .",".$row['image_title'];
																		}
																	}
																	$orgPath = $s3->getNewURI_otheruser("medgallery",$get_news_general_user_id);
																	$thumbPath  = $s3->getNewURI_otheruser("medgalthumb",$get_news_general_user_id);
																}
															}
															?>
															<a href="javascript:void(0);" onclick="checkload('<?php echo $mediaSrc; ?>','<?php echo $galleryImagesData;?>','<?php echo $thumbPath; ?>','<?php echo $orgPath;?>','<?php echo $galleryImagestitle;?>','<?php echo $get_gallery['id'];?>')"><?php echo $sql_meds_ph['title'];?></a>
															<?php
															$galleryImagesData = "";
															$galleryImagestitle = "";
															$mediaSrc ="";
														}
														elseif($exp_meds[1]=='reg_art' || $exp_meds[1]=='reg_com')
														{
															if($ans_photo_posts[$hpo_all]['profile_type']=='images_post')
															{
																$get_all_gal_image = $sql_feeds->get_all_gallery_images($sql_meds_ph['id']);
																if($get_all_gal_image!="" && $get_all_gal_image!=NULL)
															{
																	if(mysql_num_rows($get_all_gal_image)>0)
																	{
																		$galleryImagesData ="";
																		$galleryImagestitle ="";
																		$mediaSrc ="";
																		$get_news_general_user_id = $uk -> get_general_user_idby_mediaid($sql_meds_ph['id']);
																		while($row = mysql_fetch_assoc($get_all_gal_image))
																		{
																			$mediaSrc = $row[0]['image_name'];
																			$filePath = $s3->getNewURI_otheruser("medgallery",$get_news_general_user_id).$mediaSrc;
																			if($row['image_name']!=""){
																				$galleryImagesData = $galleryImagesData .",". $row['image_name'];
																				$galleryImagestitle = $galleryImagestitle .",".$row['image_title'];
																			}
																		}
																		$orgPath = $s3->getNewURI_otheruser("medgallery",$get_news_general_user_id);
																		$thumbPath = $s3->getNewURI_otheruser("medgalthumb",$get_news_general_user_id);
																	}
																}
																?>
																<a href="javascript:void(0);" onclick="checkload('<?php echo $mediaSrc; ?>','<?php echo $galleryImagesData;?>','<?php echo $thumbPath; ?>','<?php echo $orgPath;?>','<?php echo $galleryImagestitle;?>','<?php echo $get_gallery['id'];?>')"><?php echo $sql_meds_ph['gallery_title'];?></a>
																<?php
																$galleryImagesData = "";
																$galleryImagestitle = "";
																$mediaSrc ="";
															}
														}
		?>
														<div style="color:#808080;font-size:12px;">
															<div class="comment more">
																<?php echo $ans_photo_posts[$hpo_all]['description']; ?>
															</div>
														</div>
													</div>
													
													<!--<div class="activityContent">Tagged: <?php //echo $medias['tagged_users']; 
														//$name_org = explode(')',$medias['tagged_users']);
														//for($i_name_org=0;$i_name_org<count($name_org);$i_name_org++)
														//{
															//	$end_pos = strpos($name_org[$i_name_org],'(');
															//	$org_name = substr($name_org[$i_name_org],0,$end_pos);
															//		echo $org_name;
														//	}
		?>
													</div>-->
												</div>
											</div>
										</div>
									</div>
		
									<div class="rightSide">
										<a href="#" onmouseover="mopen('mm<?php echo $hpo_all;?>5')" onmouseout="mclosetime()"><img src="images/profile/arrow.png" onmouseover="this.src='images/profile/arrow-down.png'" onmouseout="this.src='images/profile/arrow.png'" /></a>
										<div id="mm<?php echo $hpo_all;?>5" class="popUp" onmouseover="mcancelclosetime()" onmouseout="mclosetime()">
											<ul>
												<li><a href="hideposts.php?id=<?php echo $ans_photo_posts[$hpo_all]['id'];?>">Hide Feed</a></li>
												<li><a href="hideposts.php?report_id=<?php echo $ans_all_posts[$po_all]['id'];?>">Report spam</a></li>
												<li><div class="seperator"></div></li>
												<li><!--<a href="Unsubscribe_feeds.php?all=1&user_ids=<?php echo $gens['general_user_id']; ?>" onclick="return confirm_subscription_delete('<?php if($art==1){ echo $ros['name']; } elseif($com==1){ echo $ros_com['name']; } ?>')">Unsubscribe from <?php if($art==1)
															{
																echo $ros['name'];
															}
															elseif($com==1)
															{
																echo $ros_com['name'];
															} ?></a>-->
															<a href="profileedit.php#subscrriptions" >Unsubscribe from <?php if($art==1)
															{
																echo $ros['name'];
															}
															elseif($com==1)
															{
																echo $ros_com['name'];
															} ?></a></li>
											</ul>
										</div>
									</div>
								</div>
		<?php
							}
						}
						elseif($ans_photo_posts[$hpo_all]['display_to']=='Fans')
						{
							if($gens!="")
							{
								$fans_all =	$sql_feeds->fans_cond($chk_sql['email'],$gens['community_id'],$gens['artist_id']);
								if($fans_all=='1')
								{
									$hide13 = 0;
									$get_gen_id = $sql_feeds->get_general_info();
									$hide_by = $ans_photo_posts[$hpo_all]['hidden_by'];
									$exp_hide_by = explode(",",$hide_by);
									for($exp_count=0;$exp_count<=count($exp_hide_by);$exp_count++)
									{
										if($exp_hide_by[$exp_count] ==$get_gen_id['general_user_id'])
										{
											$hide13 =1;
										}
									}
									
									if($hide13 != 1)
									{
		?>
										<div class="storyCont">
											<div class="leftSide">
												<div class="avatar">
		<?php
													$art = 0;
													
													if($gens['artist_id']!=0)
													{
														$sql_art = mysql_query("SELECT * FROM general_artist WHERE artist_id='".$gens['artist_id']."' AND status=0 AND active=0 AND del_status=0");
														if($sql_art!="" && $sql_art!=NULL)
														{
															if(mysql_num_rows($sql_art)>0)
															{
																$ros = mysql_fetch_assoc($sql_art);
																$art = 1;
															}
														}
													}
													
													$com = 0;
													if($gens['community_id']!=0)
													{
														$sql_com = mysql_query("SELECT * FROM general_community WHERE community_id='".$gens['community_id']."' AND status=0 AND active=0 AND del_status=0");
														if($sql_com!="" && $sql_com!=NULL)
														{
															if(mysql_num_rows($sql_com)>0)
															{
																$ros_com = mysql_fetch_assoc($sql_com);
																$com = 1;
															}
														}
													}
													
													if($art==1)
													{
														if($ros['profile_url']!="")
														{
													?>
														<a href="/<?php echo $ros['profile_url']; ?>">
													<?php
														}
														$get_news_general_user_id = $uk -> get_general_user_idby_artistid($ros['artist_id']);
		?>
														<img width="50" height="50" src="<?php
														if($ros['image_name']!="" && !empty($ros['image_name']))
														{
															echo $s3->getNewURI_otheruser("artjcropprofile",$get_news_general_user_id) . $ros['image_name']; 
														}
														else
														{
															echo "http://artjcropprofile.s3.amazonaws.com/Noimage.png";
														}
													?>" />
													<?php
														if($ros['profile_url']!="")
														{
													?>
														</a>
													<?php
														}
													}
													elseif($com==1)
													{
														if($ros_com['profile_url']!="")
														{
													?>
														<a href="/<?php echo $ros_com['profile_url']; ?>">
													<?php
														}
														$get_news_general_user_id = $uk -> get_general_user_idby_communityid($ros_com['community_id']);
													?>
														<img width="50" height="50" src="<?php
													
													
													if($ros_com['image_name']!="" && !empty($ros_com['image_name']))
													{
														echo $s3->getNewURI_otheruser("comjcropprofile",$get_news_general_user_id).$ros_com['image_name'];
													}
													else
													{
														echo "http://comjcropprofile.s3.amazonaws.com/Noimage.png";
													}
													
													?>" />
													<?php
														if($ros_com['profile_url']!="")
														{
													?>
														</a>
													<?php
														}
													}
													?>
												</div>
		<?php 
												//echo "table=".$reg_table_media_type;
												//echo $ans_audio_feeds[$audio_fed_lo]['id'];
		?>
												<div class="storyContainer">
													<div class="storyTitle">
														<span class="userName"><a href=""><?php if($art==1)
														{
															echo $ros['name'];
														}
														elseif($com==1)
														{
															echo $ros_com['name'];
														}
		?>
														</a>
														</span>
		<?php 
														//echo $ans_photo_posts[$hpo_all]['description'];
		?>
													</div>
		
													<div class="activityCont">
														<div class="sideL">
															<div class="storyImage" style="position:relative;"><?php
															$get_all_gal_image = $sql_feeds->get_all_gallery_images($sql_meds_ph['id']);
															if($get_all_gal_image!="" && $get_all_gal_image!=NULL)
														{
																if(mysql_num_rows($get_all_gal_image)>0)
																{
																	$galleryImagesData ="";
																	$galleryImagestitle ="";
																	$mediaSrc ="";
																	$get_news_general_user_id = $uk -> get_general_user_idby_mediaid($sql_meds_ph['id']);
																	while($row = mysql_fetch_assoc($get_all_gal_image))
																	{
																		$mediaSrc = $row[0]['image_name'];
																		$filePath = $s3->getNewURI_otheruser("medgallery",$get_news_general_user_id).$mediaSrc;
																		if($row['image_name']!=""){
																			$galleryImagesData = $galleryImagesData .",". $row['image_name'];
																			$galleryImagestitle = $galleryImagestitle .",".$row['image_title'];
																		}
																	}
																	$orgPath = $s3->getNewURI_otheruser("medgallery",$get_news_general_user_id);
																	$thumbPath = $s3->getNewURI_otheruser("medgalthumb",$get_news_general_user_id);
																}
															}
															if($sql_meds_ph['media_type']==113)
																		{
																			
																			$get_gal_image = $sql_feeds->gallery_meds($sql_meds_ph['id']);
																			if($get_gal_image!="" && !empty($get_gal_image))
																			{
																				if($get_gal_image['profile_image']!="")
																				{
																		?>
																				<a onmouseout="newfunctionout('<?php echo $sql_meds_ph['id']; ?>','g')" onmouseover="newfunction('<?php echo $sql_meds_ph['id']; ?>','g')" href="javascript:void(0);" onclick="checkload('<?php echo $mediaSrc; ?>','<?php echo $galleryImagesData;?>','<?php echo $thumbPath; ?>','<?php echo $orgPath;?>','<?php echo $galleryImagestitle;?>','<?php echo $get_gallery['id'];?>')">
																					<img src="<?php echo $get_gal_image['profile_image']; ?>" width="90" height="90" />
																				</a>	
																		<?php
																				$galleryImagesData = "";
																				$galleryImagestitle = "";
																				$mediaSrc ="";
																		
																				}
																				else
																				{
																		?>
																				<a onmouseout="newfunctionout('<?php echo $sql_meds_ph['id']; ?>','g')" onmouseover="newfunction('<?php echo $sql_meds_ph['id']; ?>','g')" href="javascript:void(0);" onclick="checkload('<?php echo $mediaSrc; ?>','<?php echo $galleryImagesData;?>','<?php echo $thumbPath; ?>','<?php echo $orgPath;?>','<?php echo $galleryImagestitle;?>','<?php echo $get_gallery['id'];?>')">
																					<img width="90" height="90" src="http://comjcropprofile.s3.amazonaws.com/<?php echo "Noimage.png";?>" />
																				</a>	
																				<?php	
																				$galleryImagesData = "";
																				$galleryImagestitle = "";
																				$mediaSrc ="";
																				}
																			}
																			else
																			{
																		?>
																			<a onmouseout="newfunctionout('<?php echo $sql_meds_ph['id']; ?>','g')" onmouseover="newfunction('<?php echo $sql_meds_ph['id']; ?>','g')" href="javascript:void(0);" onclick="checkload('<?php echo $mediaSrc; ?>','<?php echo $galleryImagesData;?>','<?php echo $thumbPath; ?>','<?php echo $orgPath;?>','<?php echo $galleryImagestitle;?>','<?php echo $get_gallery['id'];?>')">
																				<img width="90" height="90" src="http://comjcropprofile.s3.amazonaws.com/<?php echo "Noimage.png";?>"/>
																			</a>	
																			<?php	
																			$galleryImagesData = "";
																			$galleryImagestitle = "";
																			$mediaSrc ="";
																			}
																		}
																		elseif($exp_meds[1]=='reg_art' && $ans_all_posts[$po_all]['profile_type']=='images_post')
																		{
																			$tables = 'artist';
																			$imgs = $sql_feeds->reg_gal($tables,$sql_meds_ph['gallery_id']);
																			
																			if($imgs!="" && !empty($imgs))
																			{
																				if($imgs['image_name']!="")
																				{
																			?>
																				<a onmouseout="newfunctionout('<?php echo $sql_meds_ph['id']; ?>','g')" onmouseover="newfunction('<?php echo $sql_meds_ph['id']; ?>','g')" href="javascript:void(0);" onclick="checkload('<?php echo $mediaSrc; ?>','<?php echo $galleryImagesData;?>','<?php echo $thumbPath; ?>','<?php echo $orgPath;?>','<?php echo $galleryImagestitle;?>','<?php echo $get_gallery['id'];?>')">
																					<img width="90" height="90" src="http://reggalthumb.s3.amazonaws.com/<?php echo $imgs['image_name'];?>" />
																				</a>	
																				<?php	
																				$galleryImagesData = "";
																				$galleryImagestitle = "";
																				$mediaSrc ="";
																				}
																				else
																				{
																			?>
																				<a onmouseout="newfunctionout('<?php echo $sql_meds_ph['id']; ?>','g')" onmouseover="newfunction('<?php echo $sql_meds_ph['id']; ?>','g')" href="javascript:void(0);" onclick="checkload('<?php echo $mediaSrc; ?>','<?php echo $galleryImagesData;?>','<?php echo $thumbPath; ?>','<?php echo $orgPath;?>','<?php echo $galleryImagestitle;?>','<?php echo $get_gallery['id'];?>')">
																					<img width="90" height="90" src="http://comjcropprofile.s3.amazonaws.com/<?php echo "Noimage.png";?>" />
																				</a>	
																				<?php	
																				$galleryImagesData = "";
																				$galleryImagestitle = "";
																				$mediaSrc ="";
																				}
																			}
																			else
																			{
																			?>	
																			<a onmouseout="newfunctionout('<?php echo $sql_meds_ph['id']; ?>','g')" onmouseover="newfunction('<?php echo $sql_meds_ph['id']; ?>','g')" href="javascript:void(0);" onclick="checkload('<?php echo $mediaSrc; ?>','<?php echo $galleryImagesData;?>','<?php echo $thumbPath; ?>','<?php echo $orgPath;?>','<?php echo $galleryImagestitle;?>','<?php echo $get_gallery['id'];?>')">
																				<img width="90" height="90" src="http://comjcropprofile.s3.amazonaws.com/<?php echo "Noimage.png";?>" />
																			</a>	
																			<?php	
																			$galleryImagesData = "";
																			$galleryImagestitle = "";
																			$mediaSrc ="";
																			}
																		}
																		elseif($exp_meds[1]=='reg_com' && $ans_all_posts[$po_all]['profile_type']=='images_post')
																		{
																			$tables = 'community';
																			$imgs = $sql_feeds->reg_gal($tables,$sql_meds_ph['gallery_id']);
																			
																			if($imgs!="" && !empty($imgs))
																			{
																				if($imgs['image_name']!="")
																				{
																			?>
																				<a onmouseout="newfunctionout('<?php echo $sql_meds_ph['id']; ?>','g')" onmouseover="newfunction('<?php echo $sql_meds_ph['id']; ?>','g')" href="javascript:void(0);" onclick="checkload('<?php echo $mediaSrc; ?>','<?php echo $galleryImagesData;?>','<?php echo $thumbPath; ?>','<?php echo $orgPath;?>','<?php echo $galleryImagestitle;?>','<?php echo $get_gallery['id'];?>')">
																					<img width="90" height="90" src="http://reggalthumb.s3.amazonaws.com/<?php echo $imgs['image_name'];?>" />
																				</a>	
																				<?php	
																				$galleryImagesData = "";
																				$galleryImagestitle = "";
																				$mediaSrc ="";
																				}
																				else
																				{
																			?>
																				<a onmouseout="newfunctionout('<?php echo $sql_meds_ph['id']; ?>','g')" onmouseover="newfunction('<?php echo $sql_meds_ph['id']; ?>','g')" href="javascript:void(0);" onclick="checkload('<?php echo $mediaSrc; ?>','<?php echo $galleryImagesData;?>','<?php echo $thumbPath; ?>','<?php echo $orgPath;?>','<?php echo $galleryImagestitle;?>','<?php echo $get_gallery['id'];?>')">
																					<img width="90" height="90" src="http://comjcropprofile.s3.amazonaws.com/<?php echo "Noimage.png";?>" />
																				</a>	
																				<?php	
																				$galleryImagesData = "";
																				$galleryImagestitle = "";
																				$mediaSrc ="";
																				}
																			}
																			else
																			{
																			?>
																			<a onmouseout="newfunctionout('<?php echo $sql_meds_ph['id']; ?>','g')" onmouseover="newfunction('<?php echo $sql_meds_ph['id']; ?>','g')" href="javascript:void(0);" onclick="checkload('<?php echo $mediaSrc; ?>','<?php echo $galleryImagesData;?>','<?php echo $thumbPath; ?>','<?php echo $orgPath;?>','<?php echo $galleryImagestitle;?>','<?php echo $get_gallery['id'];?>')">
																				<img width="90" height="90" src="http://comjcropprofile.s3.amazonaws.com/<?php echo "Noimage.png";?>" />
																			</a>	
																			<?php	
																			$galleryImagesData = "";
																			$galleryImagestitle = "";
																			$mediaSrc ="";
																			}
																		}
															
															?>
															<div class="navButton">
			<div class="playMedia">
				<a href="javascript:void(0);" class="list_play"><img title="Add file to the bottom of your playlist without stopping current media." id="play_video_image<?php echo $sql_meds_ph['id']; ?>g" onclick="checkload('<?php echo $mediaSrc; ?>','<?php echo $galleryImagesData;?>','<?php echo $thumbPath; ?>','<?php echo $orgPath;?>','<?php echo $galleryImagestitle;?>','<?php echo $get_gallery['id'];?>')" src="/images/play.png" onmouseout="newfunctionout('<?php echo $sql_meds_ph['id']; ?>','g')" onmouseover="newfunction('<?php echo $sql_meds_ph['id']; ?>','g')" style="border-radius: 6px;"></a>
			</div>
															</div>
															<!--<div class="player">
																<img src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'">
																<img src="images/profile/add.gif" onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'">
																<img style="cursor: default;" src="images/profile/download-over.gif">
															</div>-->
														</div>
				
														<div class="sideR">
															<div class="activityTitle"><?php
																if($exp_meds[1]=='reg_med')
																{
																	$get_all_gal_image = $sql_feeds->get_all_gallery_images($sql_meds_ph['id']);
																	if($get_all_gal_image!="" && $get_all_gal_image!=NULL)
														{
																		if(mysql_num_rows($get_all_gal_image)>0)
																		{
																			$galleryImagesData ="";
																			$galleryImagestitle ="";
																			$mediaSrc ="";
																			$get_news_general_user_id = $uk -> get_general_user_idby_mediaid($sql_meds_ph['id']);
																			while($row = mysql_fetch_assoc($get_all_gal_image))
																			{
																				$mediaSrc = $row[0]['image_name'];
																				$filePath = $s3->getNewURI_otheruser("medgallery",$get_news_general_user_id).$mediaSrc;
																				if($row['image_name']!=""){
																					$galleryImagesData = $galleryImagesData .",". $row['image_name'];
																					$galleryImagestitle = $galleryImagestitle .",".$row['image_title'];
																				}
																			}
																			$orgPath = $s3->getNewURI_otheruser("medgallery",$get_news_general_user_id);
																			$thumbPath = $s3->getNewURI_otheruser("medgalthumb",$get_news_general_user_id);
																		}
																	}
																	?>
																	<a href="javascript:void(0);" onclick="checkload('<?php echo $mediaSrc; ?>','<?php echo $galleryImagesData;?>','<?php echo $thumbPath; ?>','<?php echo $orgPath;?>','<?php echo $galleryImagestitle;?>','<?php echo $get_gallery['id'];?>')"><?php echo $sql_meds_ph['title'];?></a>
																	<?php
																	$galleryImagesData = "";
																	$galleryImagestitle = "";
																	$mediaSrc ="";
																}
																elseif($exp_meds[1]=='reg_art' || $exp_meds[1]=='reg_com')
																{
																	if($ans_photo_posts[$hpo_all]['profile_type']=='images_post')
																	{
																		$get_all_gal_image = $sql_feeds->get_all_gallery_images($sql_meds_ph['id']);
																		if($get_all_gal_image!="" && $get_all_gal_image!=NULL)
														{
																		if(mysql_num_rows($get_all_gal_image)>0)
																		{
																			$galleryImagesData ="";
																			$galleryImagestitle ="";
																			$mediaSrc ="";
																			$get_news_general_user_id = $uk -> get_general_user_idby_mediaid($sql_meds_ph['id']);
																			while($row = mysql_fetch_assoc($get_all_gal_image))
																			{
																				$mediaSrc = $row[0]['image_name'];
																				$filePath = $s3->getNewURI_otheruser("medgallery",$get_news_general_user_id).$mediaSrc;
																				if($row['image_name']!=""){
																					$galleryImagesData = $galleryImagesData .",". $row['image_name'];
																					$galleryImagestitle = $galleryImagestitle .",".$row['image_title'];
																				}
																			}
																			$orgPath = $s3->getNewURI_otheruser("medgallery",$get_news_general_user_id);
																			$thumbPath = $s3->getNewURI_otheruser("medgalthumb",$get_news_general_user_id);
																		}
																		}
																		?>
																		<a href="javascript:void(0);" onclick="checkload('<?php echo $mediaSrc; ?>','<?php echo $galleryImagesData;?>','<?php echo $thumbPath; ?>','<?php echo $orgPath;?>','<?php echo $galleryImagestitle;?>','<?php echo $get_gallery['id'];?>')"><?php echo $sql_meds_ph['gallery_title'];?></a>
																		<?php
																		$galleryImagesData = "";
																		$galleryImagestitle = "";
																		$mediaSrc ="";
																	}
																			
																}
		?>
																<div style="color:#808080;font-size:12px;">
																	<div class="comment more">
																		<?php echo $ans_photo_posts[$hpo_all]['description']; ?>
																	</div>
																</div>
															</div>
															<!--<div class="activityContent">Tagged: <?php //echo $medias['tagged_users']; 
																//$name_org = explode(')',$medias['tagged_users']);
																//for($i_name_org=0;$i_name_org<count($name_org);$i_name_org++)
																//{
																	//	$end_pos = strpos($name_org[$i_name_org],'(');
																	//	$org_name = substr($name_org[$i_name_org],0,$end_pos);
																	//		echo $org_name;
																//	}
		?>
															</div>-->
														</div>
													</div>
												</div>
											</div>
				
											<div class="rightSide">
												<a href="#" onmouseover="mopen('mm<?php echo $hpo_all;?>6')" onmouseout="mclosetime()"><img src="images/profile/arrow.png" onmouseover="this.src='images/profile/arrow-down.png'" onmouseout="this.src='images/profile/arrow.png'" /></a>
												<div id="mm<?php echo $hpo_all;?>6" class="popUp" onmouseover="mcancelclosetime()" onmouseout="mclosetime()">
													<ul>
														<li><a href="hideposts.php?id=<?php echo $ans_photo_posts[$hpo_all]['id'];?>">Hide Feed</a></li>
														<li><a href="hideposts.php?report_id=<?php echo $ans_all_posts[$po_all]['id'];?>">Report spam</a></li>
														<li><div class="seperator"></div></li>
														<li><a href="">Unsubscribe from <?php echo $gens['fname'].' '.$gens['lname']; ?></a></li>
													</ul>
												</div>
											</div>
										</div>
		<?php
									}
								}
							}
						}
						elseif($ans_photo_posts[$hpo_all]['display_to']=='Subscribers')
						{
							if($gens!="")
							{
								$subscribers_all =	$sql_feeds->subscribers_cond($chk_sql['email'],$gens['community_id'],$gens['artist_id']);
								if($subscribers_all=='1')
								{
									$hide14 = 0;
									$get_gen_id = $sql_feeds->get_general_info();
									$hide_by = $ans_photo_posts[$hpo_all]['hidden_by'];
									$exp_hide_by = explode(",",$hide_by);
									for($exp_count=0;$exp_count<=count($exp_hide_by);$exp_count++)
									{
										if($exp_hide_by[$exp_count] ==$get_gen_id['general_user_id'])
										{
											$hide14 =1;
										}
									}
									if($hide14 != 1)
									{
		?>
										<div class="storyCont">
											<div class="leftSide">
												<div class="avatar">
		<?php
													$art = 0;
													
													if($gens['artist_id']!=0)
													{
														$sql_art = mysql_query("SELECT * FROM general_artist WHERE artist_id='".$gens['artist_id']."' AND status=0 AND active=0 AND del_status=0");
														if($sql_art!="" && $sql_art!=NULL)
														{
															if(mysql_num_rows($sql_art)>0)
															{
																$ros = mysql_fetch_assoc($sql_art);
																$art = 1;
															}
														}
													}
													
													$com = 0;
													if($gens['community_id']!=0)
													{
														$sql_com = mysql_query("SELECT * FROM general_community WHERE community_id='".$gens['community_id']."' AND status=0 AND active=0 AND del_status=0");
														if($sql_com!="" && $sql_com!=NULL)
														{
															if(mysql_num_rows($sql_com)>0)
															{
																$ros_com = mysql_fetch_assoc($sql_com);
																$com = 1;
															}
														}
													}
													
													if($art==1)
													{
														if($ros['profile_url']!="")
														{
													?>
														<a href="/<?php echo $ros['profile_url']; ?>">
													<?php
														}
														$get_news_general_user_id = $uk -> get_general_user_idby_artistid($ros['artist_id']);
													?>
														<img width="50" height="50" src="<?php
														if($ros['image_name']!="" && !empty($ros['image_name']))
														{
															echo $s3->getNewURI_otheruser("artjcropprofile",$get_news_general_user_id).$ros['image_name']; 
														}
														else
														{
															echo "http://artjcropprofile.s3.amazonaws.com/Noimage.png";
														}
													?>" />
													<?php
														if($ros['profile_url']!="")
														{
													?>
														</a>
													<?php
														}
													}
													elseif($com==1)
													{
														if($ros_com['profile_url']!="")
														{
													?>
														<a href="/<?php echo $ros_com['profile_url']; ?>">
													<?php
														}
														$get_news_general_user_id = $uk -> get_general_user_idby_communityid($ros_com['community_id']);
													?>
														<img width="50" height="50" src="<?php
													
													
													if($ros_com['image_name']!="" && !empty($ros_com['image_name']))
													{
														echo $s3->getNewURI_otheruser("comjcropprofile",$get_news_general_user_id).$ros_com['image_name']; 
													}
													else
													{
														echo "http://comjcropprofile.s3.amazonaws.com/Noimage.png";
													}
													
													?>" />
													<?php
														if($ros_com['profile_url']!="")
														{
													?>
														</a>
													<?php
														}
													}
													?>
												</div>
		<?php 
												//echo "table=".$reg_table_media_type;
												//echo $ans_audio_feeds[$audio_fed_lo]['id'];
		?>
												<div class="storyContainer">
													<div class="storyTitle">
														<span class="userName"><?php if($art==1)
														{
															if($ros['profile_url']!="")
														{
													?>
														<a href="/<?php echo $ros['profile_url']; ?>">
													<?php
														}
															echo $ros['name'];
															if($ros['profile_url']!="")
														{
													?>
														</a>
													<?php
														}
														}
														elseif($com==1)
														{
															if($ros_com['profile_url']!="")
														{
													?>
														<a href="/<?php echo $ros_com['profile_url']; ?>">
													<?php
														}
															echo $ros_com['name'];
															if($ros_com['profile_url']!="")
														{
													?>
														</a>
													<?php
														}
														} 
		?>
														
														</span> 
		<?php 
														//echo $ans_photo_posts[$hpo_all]['description'];
		?>
													</div>
		
													<div class="activityCont">
														<div class="sideL">
															<div class="storyImage" style="position:relative;"><?php
															$get_all_gal_image = $sql_feeds->get_all_gallery_images($sql_meds_ph['id']);
																if($get_all_gal_image!="" && $get_all_gal_image!=NULL)
														{
																if(mysql_num_rows($get_all_gal_image)>0)
																{
																	$galleryImagesData ="";
																	$galleryImagestitle ="";
																	$mediaSrc ="";
																	$get_news_general_user_id = $uk -> get_general_user_idby_mediaid($sql_meds_ph['id']);
																	while($row = mysql_fetch_assoc($get_all_gal_image))
																	{
																		$mediaSrc = $row[0]['image_name'];
																		$filePath = $s3->getNewURI_otheruser("medgallery",$get_news_general_user_id).$mediaSrc;
																		if($row['image_name']!=""){
																			$galleryImagesData = $galleryImagesData .",". $row['image_name'];
																			$galleryImagestitle = $galleryImagestitle .",".$row['image_title'];
																		}
																	}
																	$orgPath = $s3->getNewURI_otheruser("medgallery",$get_news_general_user_id);
																	$thumbPath = $s3->getNewURI_otheruser("medgalthumb",$get_news_general_user_id);
																}
																}
															if($sql_meds_ph['media_type']==113)
																		{
																			$get_gal_image = $sql_feeds->gallery_meds($sql_meds_ph['id']);
																			if($get_gal_image!="" && !empty($get_gal_image))
																			{
																				if($get_gal_image['profile_image']!="")
																				{
																		?>
																				<a onmouseout="newfunctionout('<?php echo $sql_meds_ph['id']; ?>','g')" onmouseover="newfunction('<?php echo $sql_meds_ph['id']; ?>','g')" href="javascript:void(0);" onclick="checkload('<?php echo $mediaSrc; ?>','<?php echo $galleryImagesData;?>','<?php echo $thumbPath; ?>','<?php echo $orgPath;?>','<?php echo $galleryImagestitle;?>','<?php echo $get_gallery['id'];?>')">
																				<img src="<?php echo $get_gal_image['profile_image']; ?>" width="90" height="90" />
																				</a>
																		<?php
																				$galleryImagesData = "";
																				$galleryImagestitle = "";
																				$mediaSrc ="";
																				}
																				else
																				{
																		?>
																				<a onmouseout="newfunctionout('<?php echo $sql_meds_ph['id']; ?>','g')" onmouseover="newfunction('<?php echo $sql_meds_ph['id']; ?>','g')" href="javascript:void(0);" onclick="checkload('<?php echo $mediaSrc; ?>','<?php echo $galleryImagesData;?>','<?php echo $thumbPath; ?>','<?php echo $orgPath;?>','<?php echo $galleryImagestitle;?>','<?php echo $get_gallery['id'];?>')">
																					<img width="90" height="90" src="http://comjcropprofile.s3.amazonaws.com/<?php echo "Noimage.png";?>" />
																				</a>	
																				<?php
																				$galleryImagesData = "";
																				$galleryImagestitle = "";
																				$mediaSrc ="";																				
																				}
																			}
																			else
																			{
																		?>
																			<a onmouseout="newfunctionout('<?php echo $sql_meds_ph['id']; ?>','g')" onmouseover="newfunction('<?php echo $sql_meds_ph['id']; ?>','g')" href="javascript:void(0);" onclick="checkload('<?php echo $mediaSrc; ?>','<?php echo $galleryImagesData;?>','<?php echo $thumbPath; ?>','<?php echo $orgPath;?>','<?php echo $galleryImagestitle;?>','<?php echo $get_gallery['id'];?>')">
																				<img width="90" height="90" src="http://comjcropprofile.s3.amazonaws.com/<?php echo "Noimage.png";?>" />
																			</a>	
																			<?php	
																			$galleryImagesData = "";
																			$galleryImagestitle = "";
																			$mediaSrc ="";	
																			}
																		}
																		elseif($exp_meds[1]=='reg_art' && $ans_all_posts[$po_all]['profile_type']=='images_post')
																		{
																			$tables = 'artist';
																			$imgs = $sql_feeds->reg_gal($tables,$sql_meds_ph['gallery_id']);
																			
																			if($imgs!="" && !empty($imgs))
																			{
																				if($imgs['image_name']!="")
																				{
																			?>
																				<a onmouseout="newfunctionout('<?php echo $sql_meds_ph['id']; ?>','g')" onmouseover="newfunction('<?php echo $sql_meds_ph['id']; ?>','g')" href="javascript:void(0);" onclick="checkload('<?php echo $mediaSrc; ?>','<?php echo $galleryImagesData;?>','<?php echo $thumbPath; ?>','<?php echo $orgPath;?>','<?php echo $galleryImagestitle;?>','<?php echo $get_gallery['id'];?>')">
																					<img width="90" height="90" src="http://reggalthumb.s3.amazonaws.com/<?php echo $imgs['image_name'];?>" />
																				</a>	
																				<?php	
																				$galleryImagesData = "";
																				$galleryImagestitle = "";
																				$mediaSrc ="";	
																				}
																				else
																				{
																			?>
																				<a onmouseout="newfunctionout('<?php echo $sql_meds_ph['id']; ?>','g')" onmouseover="newfunction('<?php echo $sql_meds_ph['id']; ?>','g')" href="javascript:void(0);" onclick="checkload('<?php echo $mediaSrc; ?>','<?php echo $galleryImagesData;?>','<?php echo $thumbPath; ?>','<?php echo $orgPath;?>','<?php echo $galleryImagestitle;?>','<?php echo $get_gallery['id'];?>')">
																					<img width="90" height="90" src="http://comjcropprofile.s3.amazonaws.com/<?php echo "Noimage.png";?>" />
																				</a>	
																				<?php	
																				$galleryImagesData = "";
																				$galleryImagestitle = "";
																				$mediaSrc ="";	
																				}
																			}
																			else
																			{
																			?>
																			<a onmouseout="newfunctionout('<?php echo $sql_meds_ph['id']; ?>','g')" onmouseover="newfunction('<?php echo $sql_meds_ph['id']; ?>','g')" href="javascript:void(0);" onclick="checkload('<?php echo $mediaSrc; ?>','<?php echo $galleryImagesData;?>','<?php echo $thumbPath; ?>','<?php echo $orgPath;?>','<?php echo $galleryImagestitle;?>','<?php echo $get_gallery['id'];?>')">
																				<img width="90" height="90" src="http://comjcropprofile.s3.amazonaws.com/<?php echo "Noimage.png";?>" />
																			</a>	
																			<?php	
																			$galleryImagesData = "";
																			$galleryImagestitle = "";
																			$mediaSrc ="";	
																			}
																		}
																		elseif($exp_meds[1]=='reg_com' && $ans_all_posts[$po_all]['profile_type']=='images_post')
																		{
																			$tables = 'community';
																			$imgs = $sql_feeds->reg_gal($tables,$sql_meds_ph['gallery_id']);
																			
																			if($imgs!="" && !empty($imgs))
																			{
																				if($imgs['image_name']!="")
																				{
																			?>
																				<a onmouseout="newfunctionout('<?php echo $sql_meds_ph['id']; ?>','g')" onmouseover="newfunction('<?php echo $sql_meds_ph['id']; ?>','g')" href="javascript:void(0);" onclick="checkload('<?php echo $mediaSrc; ?>','<?php echo $galleryImagesData;?>','<?php echo $thumbPath; ?>','<?php echo $orgPath;?>','<?php echo $galleryImagestitle;?>','<?php echo $get_gallery['id'];?>')">
																					<img width="90" height="90" src="http://reggalthumb.s3.amazonaws.com/<?php echo $imgs['image_name'];?>" />
																				</a>	
																				<?php	
																				$galleryImagesData = "";
																				$galleryImagestitle = "";
																				$mediaSrc ="";
																				}
																				else
																				{
																			?>
																				<a onmouseout="newfunctionout('<?php echo $sql_meds_ph['id']; ?>','g')" onmouseover="newfunction('<?php echo $sql_meds_ph['id']; ?>','g')" href="javascript:void(0);" onclick="checkload('<?php echo $mediaSrc; ?>','<?php echo $galleryImagesData;?>','<?php echo $thumbPath; ?>','<?php echo $orgPath;?>','<?php echo $galleryImagestitle;?>','<?php echo $get_gallery['id'];?>')">
																					<img width="90" height="90" src="http://comjcropprofile.s3.amazonaws.com/<?php echo "Noimage.png";?>" />
																				</a>	
																				<?php	
																				$galleryImagesData = "";
																				$galleryImagestitle = "";
																				$mediaSrc ="";
																				}
																			}
																			else
																			{
																			?>
																			<a onmouseout="newfunctionout('<?php echo $sql_meds_ph['id']; ?>','g')" onmouseover="newfunction('<?php echo $sql_meds_ph['id']; ?>','g')" href="javascript:void(0);" onclick="checkload('<?php echo $mediaSrc; ?>','<?php echo $galleryImagesData;?>','<?php echo $thumbPath; ?>','<?php echo $orgPath;?>','<?php echo $galleryImagestitle;?>','<?php echo $get_gallery['id'];?>')">
																				<img width="90" height="90" src="http://comjcropprofile.s3.amazonaws.com/<?php echo "Noimage.png";?>" />
																			</a>	
																			<?php
																				$galleryImagesData = "";
																				$galleryImagestitle = "";
																				$mediaSrc ="";																			
																			}
																		}
															
															?><div class="navButton">
			<div class="playMedia">
				<a href="javascript:void(0);" class="list_play"><img title="Add file to the bottom of your playlist without stopping current media." id="play_video_image<?php echo $sql_meds_ph['id']; ?>g" onclick="checkload('<?php echo $mediaSrc; ?>','<?php echo $galleryImagesData;?>','<?php echo $thumbPath; ?>','<?php echo $orgPath;?>','<?php echo $galleryImagestitle;?>','<?php echo $get_gallery['id'];?>')" src="/images/play.png" onmouseout="newfunctionout('<?php echo $sql_meds_ph['id']; ?>','g')" onmouseover="newfunction('<?php echo $sql_meds_ph['id']; ?>','g')" style="border-radius: 6px;"></a>
			</div>
															
															</div>
															<!--<div class="player">
																<img src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'">
																<img src="images/profile/add.gif" onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'">
																<img style="cursor: default;" src="images/profile/download-over.gif">
															</div>-->
														</div>
														
														<div class="sideR">
															<div class="activityTitle"><?php
															if($exp_meds[1]=='reg_med')
															{
																$get_all_gal_image = $sql_feeds->get_all_gallery_images($sql_meds_ph['id']);
																if($get_all_gal_image!="" && $get_all_gal_image!=NULL)
														{
																if(mysql_num_rows($get_all_gal_image)>0)
																{
																	$galleryImagesData ="";
																	$galleryImagestitle ="";
																	$mediaSrc ="";
																	$get_news_general_user_id = $uk -> get_general_user_idby_mediaid($sql_meds_ph['id']);
																	while($row = mysql_fetch_assoc($get_all_gal_image))
																	{
																		$mediaSrc = $row[0]['image_name'];
																		$filePath = $s3->getNewURI_otheruser("medgallery",$get_news_general_user_id).$mediaSrc;
																		if($row['image_name']!=""){
																			$galleryImagesData = $galleryImagesData .",". $row['image_name'];
																			$galleryImagestitle = $galleryImagestitle .",".$row['image_title'];
																		}
																	}
																	$orgPath = $s3->getNewURI_otheruser("medgallery",$get_news_general_user_id);
																	$thumbPath = $s3->getNewURI_otheruser("medgalthumb",$get_news_general_user_id);
																}
																}
																?>
																<a href="javascript:void(0);" onclick="checkload('<?php echo $mediaSrc; ?>','<?php echo $galleryImagesData;?>','<?php echo $thumbPath; ?>','<?php echo $orgPath;?>','<?php echo $galleryImagestitle;?>','<?php echo $get_gallery['id'];?>')"><?php echo $sql_meds_ph['title'];?></a>
																<?php
																$galleryImagesData = "";
																$galleryImagestitle = "";
																$mediaSrc ="";
															}
															elseif($exp_meds[1]=='reg_art' || $exp_meds[1]=='reg_com')
															{
																if($ans_photo_posts[$hpo_all]['profile_type']=='images_post')
																{
																	$get_all_gal_image = $sql_feeds->get_all_gallery_images($sql_meds_ph['id']);
																	if($get_all_gal_image!="" && $get_all_gal_image!=NULL)
														{
																	if(mysql_num_rows($get_all_gal_image)>0)
																	{
																		$galleryImagesData ="";
																		$galleryImagestitle ="";
																		$mediaSrc ="";
																		$get_news_general_user_id = $uk -> get_general_user_idby_mediaid($sql_meds_ph['id']);
																		while($row = mysql_fetch_assoc($get_all_gal_image))
																		{
																			$mediaSrc = $row[0]['image_name'];
																			$filePath = $s3->getNewURI_otheruser("medgallery",$get_news_general_user_id).$mediaSrc;
																			if($row['image_name']!=""){
																				$galleryImagesData = $galleryImagesData .",". $row['image_name'];
																				$galleryImagestitle = $galleryImagestitle .",".$row['image_title'];
																			}
																		}
																		$orgPath = $s3->getNewURI_otheruser("medgallery",$get_news_general_user_id);
																		$thumbPath = $s3->getNewURI_otheruser("medgalthumb",$get_news_general_user_id);
																	}
																	}
																	?>
																	<a href="javascript:void(0);" onclick="checkload('<?php echo $mediaSrc; ?>','<?php echo $galleryImagesData;?>','<?php echo $thumbPath; ?>','<?php echo $orgPath;?>','<?php echo $galleryImagestitle;?>','<?php echo $get_gallery['id'];?>')"><?php echo $sql_meds_ph['gallery_title'];?></a>
																	<?php
																	$galleryImagesData = "";
																	$galleryImagestitle = "";
																	$mediaSrc ="";
																}
															}
		?>
																<div style="color:#808080;font-size:12px;">
																	<div class="comment more">
																		<?php echo $ans_photo_posts[$hpo_all]['description']; ?>
																	</div>
																</div>
															</div>
															<!--<div class="activityContent">Tagged: <?php //echo $medias['tagged_users']; 
																//$name_org = explode(')',$medias['tagged_users']);
																//for($i_name_org=0;$i_name_org<count($name_org);$i_name_org++)
																//{
																	//	$end_pos = strpos($name_org[$i_name_org],'(');
																	//	$org_name = substr($name_org[$i_name_org],0,$end_pos);
																	//		echo $org_name;
															//	}
		?>
															</div>-->
														</div>
													</div>
												</div>
											</div>

											<div class="rightSide">
												<a href="#" onmouseover="mopen('mm<?php echo $hpo_all;?>7')" onmouseout="mclosetime()"><img src="images/profile/arrow.png" onmouseover="this.src='images/profile/arrow-down.png'" onmouseout="this.src='images/profile/arrow.png'" /></a>
												<div id="mm<?php echo $hpo_all;?>7" class="popUp" onmouseover="mcancelclosetime()" onmouseout="mclosetime()">
													<ul>
														<li><a href="hideposts.php?id=<?php echo $ans_photo_posts[$hpo_all]['id'];?>">Hide Feed</a></li>
														<li><a href="hideposts.php?report_id=<?php echo $ans_all_posts[$po_all]['id'];?>">Report spam</a></li>
														<li><div class="seperator"></div></li>
														<li><!--<a href="Unsubscribe_feeds.php?sub=1&user_ids=<?php echo $gens['general_user_id']; ?>" onclick="return confirm_subscription_delete('<?php if($art==1){ echo $ros['name']; } elseif($com==1){ echo $ros_com['name']; } ?>')">Unsubscribe from <?php if($art==1)
															{
																echo $ros['name'];
															}
															elseif($com==1)
															{
																echo $ros_com['name'];
															} ?></a>-->
															<a href="profileedit.php#subscrriptions" >Unsubscribe from <?php if($art==1)
															{
																echo $ros['name'];
															}
															elseif($com==1)
															{
																echo $ros_com['name'];
															} ?></a>
															</li>
													</ul>
												</div>
											</div>
										</div>
		<?php
									}
								}
							}
						}
						elseif($ans_photo_posts[$hpo_all]['display_to']=='Friends')
						{
							if($gens!="")
							{
								$friends_c = $sql_feeds->friends_cond($ans_photo_posts[$hpo_all]['general_user_id'],$_SESSION['login_id']);
															
								if($friends_c=='1')
								{
									$hide15 = 0;
									$get_gen_id = $sql_feeds->get_general_info();
									$hide_by = $ans_photo_posts[$hpo_all]['hidden_by'];
									$exp_hide_by = explode(",",$hide_by);
									for($exp_count=0;$exp_count<=count($exp_hide_by);$exp_count++)
									{
										if($exp_hide_by[$exp_count] ==$get_gen_id['general_user_id'])
										{
											$hide15 =1;
										}
									}
									if($hide15 != 1)
									{
		?>
										<div class="storyCont">
											<div class="leftSide">
												<div class="avatar">
		<?php
													$art = 0;
													
													if($gens['artist_id']!=0)
													{
														$sql_art = mysql_query("SELECT * FROM general_artist WHERE artist_id='".$gens['artist_id']."' AND status=0 AND active=0 AND del_status=0");
														if($sql_art!="" && $sql_art!=NULL)
														{
															if(mysql_num_rows($sql_art)>0)
															{
																$ros = mysql_fetch_assoc($sql_art);
																$art = 1;
															}
														}
													}
													
													$com = 0;
													if($gens['community_id']!=0)
													{
														$sql_com = mysql_query("SELECT * FROM general_community WHERE community_id='".$gens['community_id']."' AND status=0 AND active=0 AND del_status=0");
														if($sql_com!="" && $sql_com!=NULL)
														{
															if(mysql_num_rows($sql_com)>0)
															{
																$ros_com = mysql_fetch_assoc($sql_com);
																$com = 1;
															}
														}
													}
													
													if($art==1)
													{
														if($ros['profile_url']!="")
														{
													?>
														<a href="/<?php echo $ros['profile_url']; ?>">
													<?php
														}
														$get_news_general_user_id = $uk -> get_general_user_idby_artistid($ros['artist_id']);
													?>
														<img width="50" height="50" src="<?php
														if($ros['image_name']!="" && !empty($ros['image_name']))
														{
															echo $s3->getNewURI_otheruser("artjcropprofile",$get_news_general_user_id).$ros['image_name']; 
														}
														else
														{
															echo "http://artjcropprofile.s3.amazonaws.com/Noimage.png";
														}
													?>" />
													<?php
														if($ros['profile_url']!="")
														{
													?>
														</a>
													<?php
														}
													}
													elseif($com==1)
													{
														if($ros_com['profile_url']!="")
														{
													?>
														<a href="/<?php echo $ros_com['profile_url']; ?>">
													<?php
														}
														$get_news_general_user_id = $uk -> get_general_user_idby_communityid($ros_com['community_id']);
													?>
														<img width="50" height="50" src="<?php
													
													
													if($ros_com['image_name']!="" && !empty($ros_com['image_name']))
													{
														echo $s3->getNewURI_otheruser("comjcropprofile",$get_news_general_user_id).$ros_com['image_name']; 
													}
													else
													{
														echo "http://comjcropprofile.s3.amazonaws.com/Noimage.png";
													}
													
													?>" />
													<?php
														if($ros_com['profile_url']!="")
														{
													?>
														</a>
													<?php
														}
													}
													?>
												</div>
												<?php //echo "table=".$reg_table_media_type;
													//echo $ans_audio_feeds[$audio_fed_lo]['id'];
		?>
												<div class="storyContainer">
													<div class="storyTitle">
														<span class="userName"><?php if($art==1)
														{
															if($ros['profile_url']!="")
														{
													?>
														<a href="/<?php echo $ros['profile_url']; ?>">
													<?php
														}
															echo $ros['name'];
															if($ros['profile_url']!="")
														{
													?>
														</a>
													<?php
														}
														}
														elseif($com==1)
														{
															if($ros_com['profile_url']!="")
														{
													?>
														<a href="/<?php echo $ros_com['profile_url']; ?>">
													<?php
														}
															echo $ros_com['name'];
															if($ros_com['profile_url']!="")
														{
													?>
														</a>
													<?php
														}
														} 
		?>
														</span>
		<?php
														//echo $ans_photo_posts[$hpo_all]['description'];
		?>
													</div>
		
													<div class="activityCont">
														<div class="sideL">
															<div class="storyImage" style="position:relative;"><?php
															$get_all_gal_image = $sql_feeds->get_all_gallery_images($sql_meds_ph['id']);
															if($get_all_gal_image!="" && $get_all_gal_image!=NULL)
														{
																if(mysql_num_rows($get_all_gal_image)>0)
																{
																	$galleryImagesData ="";
																	$galleryImagestitle ="";
																	$mediaSrc ="";
																	$get_news_general_user_id = $uk -> get_general_user_idby_mediaid($sql_meds_ph['id']);
																	while($row = mysql_fetch_assoc($get_all_gal_image))
																	{
																		$mediaSrc = $row[0]['image_name'];
																		$filePath = $s3->getNewURI_otheruser("medgallery",$get_news_general_user_id).$mediaSrc;
																		if($row['image_name']!=""){
																			$galleryImagesData = $galleryImagesData .",". $row['image_name'];
																			$galleryImagestitle = $galleryImagestitle .",".$row['image_title'];
																		}
																	}
																	$orgPath = $s3->getNewURI_otheruser("medgallery",$get_news_general_user_id);
																	$thumbPath = $s3->getNewURI_otheruser("medgalthumb",$get_news_general_user_id);
																}
																}
															if($sql_meds_ph['media_type']==113)
																		{
																			$get_gal_image = $sql_feeds->gallery_meds($sql_meds_ph['id']);
																			if($get_gal_image!="" && !empty($get_gal_image))
																			{
																				if($get_gal_image['profile_image']!="")
																				{
																		?>
																				<a onmouseout="newfunctionout('<?php echo $sql_meds_ph['id']; ?>','g')" onmouseover="newfunction('<?php echo $sql_meds_ph['id']; ?>','g')" href="javascript:void(0);" onclick="checkload('<?php echo $mediaSrc; ?>','<?php echo $galleryImagesData;?>','<?php echo $thumbPath; ?>','<?php echo $orgPath;?>','<?php echo $galleryImagestitle;?>','<?php echo $get_gallery['id'];?>')">
																					<img src="<?php echo $get_gal_image['profile_image']; ?>" width="90" height="90" />
																				</a>	
																		<?php
																			$galleryImagesData = "";
																			$galleryImagestitle = "";
																			$mediaSrc ="";
																				}
																				else
																				{
																		?>
																				<a onmouseout="newfunctionout('<?php echo $sql_meds_ph['id']; ?>','g')" onmouseover="newfunction('<?php echo $sql_meds_ph['id']; ?>','g')" href="javascript:void(0);" onclick="checkload('<?php echo $mediaSrc; ?>','<?php echo $galleryImagesData;?>','<?php echo $thumbPath; ?>','<?php echo $orgPath;?>','<?php echo $galleryImagestitle;?>','<?php echo $get_gallery['id'];?>')">
																					<img width="90" height="90" src="http://comjcropprofile.s3.amazonaws.com/<?php echo "Noimage.png";?>" />
																				</a>	
																				<?php	
																				$galleryImagesData = "";
																				$galleryImagestitle = "";
																				$mediaSrc ="";
																				}
																			}
																			else
																			{
																		?>
																			<a onmouseout="newfunctionout('<?php echo $sql_meds_ph['id']; ?>','g')" onmouseover="newfunction('<?php echo $sql_meds_ph['id']; ?>','g')" href="javascript:void(0);" onclick="checkload('<?php echo $mediaSrc; ?>','<?php echo $galleryImagesData;?>','<?php echo $thumbPath; ?>','<?php echo $orgPath;?>','<?php echo $galleryImagestitle;?>','<?php echo $get_gallery['id'];?>')">
																				<img width="90" height="90" src="http://comjcropprofile.s3.amazonaws.com/<?php echo "Noimage.png";?>" />
																			</a>	
																			<?php	
																			$galleryImagesData = "";
																			$galleryImagestitle = "";
																			$mediaSrc ="";
																			}
																		}
																		elseif($exp_meds[1]=='reg_art' && $ans_all_posts[$po_all]['profile_type']=='images_post')
																		{
																			$tables = 'artist';
																			$imgs = $sql_feeds->reg_gal($tables,$sql_meds_ph['gallery_id']);
																			
																			if($imgs!="" && !empty($imgs))
																			{
																				if($imgs['image_name']!="")
																				{
																			?>
																				<a onmouseout="newfunctionout('<?php echo $sql_meds_ph['id']; ?>','g')" onmouseover="newfunction('<?php echo $sql_meds_ph['id']; ?>','g')" href="javascript:void(0);" onclick="checkload('<?php echo $mediaSrc; ?>','<?php echo $galleryImagesData;?>','<?php echo $thumbPath; ?>','<?php echo $orgPath;?>','<?php echo $galleryImagestitle;?>','<?php echo $get_gallery['id'];?>')">
																					<img width="90" height="90" src="http://reggalthumb.s3.amazonaws.com/<?php echo $imgs['image_name'];?>" />
																				</a>	
																				<?php
																				$galleryImagesData = "";
																				$galleryImagestitle = "";
																				$mediaSrc ="";																				
																				}
																				else
																				{
																			?>
																				<a onmouseout="newfunctionout('<?php echo $sql_meds_ph['id']; ?>','g')" onmouseover="newfunction('<?php echo $sql_meds_ph['id']; ?>','g')" href="javascript:void(0);" onclick="checkload('<?php echo $mediaSrc; ?>','<?php echo $galleryImagesData;?>','<?php echo $thumbPath; ?>','<?php echo $orgPath;?>','<?php echo $galleryImagestitle;?>','<?php echo $get_gallery['id'];?>')">
																					<img width="90" height="90" src="http://comjcropprofile.s3.amazonaws.com/<?php echo "Noimage.png";?>" />
																				</a>	
																				<?php	
																				$galleryImagesData = "";
																				$galleryImagestitle = "";
																				$mediaSrc ="";
																				}
																			}
																			else
																			{
																			?>
																			<a onmouseout="newfunctionout('<?php echo $sql_meds_ph['id']; ?>','g')" onmouseover="newfunction('<?php echo $sql_meds_ph['id']; ?>','g')" href="javascript:void(0);" onclick="checkload('<?php echo $mediaSrc; ?>','<?php echo $galleryImagesData;?>','<?php echo $thumbPath; ?>','<?php echo $orgPath;?>','<?php echo $galleryImagestitle;?>','<?php echo $get_gallery['id'];?>')">
																				<img width="90" height="90" src="http://comjcropprofile.s3.amazonaws.com/<?php echo "Noimage.png";?>" />
																			</a>	
																			<?php	
																			$galleryImagesData = "";
																			$galleryImagestitle = "";
																			$mediaSrc ="";
																			}
																		}
																		elseif($exp_meds[1]=='reg_com' && $ans_all_posts[$po_all]['profile_type']=='images_post')
																		{
																			$tables = 'community';
																			$imgs = $sql_feeds->reg_gal($tables,$sql_meds_ph['gallery_id']);
																			
																			if($imgs!="" && !empty($imgs))
																			{
																				if($imgs['image_name']!="")
																				{
																			?>
																				<a onmouseout="newfunctionout('<?php echo $sql_meds_ph['id']; ?>','g')" onmouseover="newfunction('<?php echo $sql_meds_ph['id']; ?>','g')" href="javascript:void(0);" onclick="checkload('<?php echo $mediaSrc; ?>','<?php echo $galleryImagesData;?>','<?php echo $thumbPath; ?>','<?php echo $orgPath;?>','<?php echo $galleryImagestitle;?>','<?php echo $get_gallery['id'];?>')">
																					<img width="90" height="90" src="http://reggalthumb.s3.amazonaws.com/<?php echo $imgs['image_name'];?>" />
																				</a>	
																				<?php	
																				$galleryImagesData = "";
																				$galleryImagestitle = "";
																				$mediaSrc ="";
																				}
																				else
																				{
																				
																			?>
																				<a onmouseout="newfunctionout('<?php echo $sql_meds_ph['id']; ?>','g')" onmouseover="newfunction('<?php echo $sql_meds_ph['id']; ?>','g')" href="javascript:void(0);" onclick="checkload('<?php echo $mediaSrc; ?>','<?php echo $galleryImagesData;?>','<?php echo $thumbPath; ?>','<?php echo $orgPath;?>','<?php echo $galleryImagestitle;?>','<?php echo $get_gallery['id'];?>')">
																					<img width="90" height="90" src="http://comjcropprofile.s3.amazonaws.com/<?php echo "Noimage.png";?>" />
																				</a>	
																				<?php	
																				$galleryImagesData = "";
																				$galleryImagestitle = "";
																				$mediaSrc ="";
																				}
																			}
																			else
																			{
																			?>
																			<a onmouseout="newfunctionout('<?php echo $sql_meds_ph['id']; ?>','g')" onmouseover="newfunction('<?php echo $sql_meds_ph['id']; ?>','g')" href="javascript:void(0);" onclick="checkload('<?php echo $mediaSrc; ?>','<?php echo $galleryImagesData;?>','<?php echo $thumbPath; ?>','<?php echo $orgPath;?>','<?php echo $galleryImagestitle;?>','<?php echo $get_gallery['id'];?>')">
																				<img width="90" height="90" src="http://comjcropprofile.s3.amazonaws.com/<?php echo "Noimage.png";?>" />
																			</a>	
																			<?php	
																			$galleryImagesData = "";
																				$galleryImagestitle = "";
																				$mediaSrc ="";
																			}
																		}
															
															?>
															<div class="navButton">
			<div class="playMedia">
				<a href="javascript:void(0);" class="list_play"><img title="Add file to the bottom of your playlist without stopping current media." id="play_video_image<?php echo $sql_meds_ph['id']; ?>g" onclick="checkload('<?php echo $mediaSrc; ?>','<?php echo $galleryImagesData;?>','<?php echo $thumbPath; ?>','<?php echo $orgPath;?>','<?php echo $galleryImagestitle;?>','<?php echo $get_gallery['id'];?>')" src="/images/play.png" onmouseout="newfunctionout('<?php echo $sql_meds_ph['id']; ?>','g')" onmouseover="newfunction('<?php echo $sql_meds_ph['id']; ?>','g')" style="border-radius: 6px;"></a>
			</div>
															</div>
															<!--<div class="player">
																<img src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'">
																<img src="images/profile/add.gif" onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'">
																<img style="cursor: default;" src="images/profile/download-over.gif">
															</div>-->
														</div>
						
														<div class="sideR">
															<div class="activityTitle"><?php
															if($exp_meds[1]=='reg_med')
															{
																$get_all_gal_image = $sql_feeds->get_all_gallery_images($sql_meds_ph['id']);
																if($get_all_gal_image!="" && $get_all_gal_image!=NULL)
														{
																	if(mysql_num_rows($get_all_gal_image)>0)
																	{
																		$galleryImagesData ="";
																		$galleryImagestitle ="";
																		$mediaSrc ="";
																		$get_news_general_user_id = $uk -> get_general_user_idby_mediaid($sql_meds_ph['id']);
																		while($row = mysql_fetch_assoc($get_all_gal_image))
																		{
																			$mediaSrc = $row[0]['image_name'];
																			$filePath = $s3->getNewURI_otheruser("medgallery",$get_news_general_user_id).$mediaSrc;
																			if($row['image_name']!=""){
																				$galleryImagesData = $galleryImagesData .",". $row['image_name'];
																				$galleryImagestitle = $galleryImagestitle .",".$row['image_title'];
																			}
																		}
																		$orgPath = $s3->getNewURI_otheruser("medgallery",$get_news_general_user_id);
																		$thumbPath = $s3->getNewURI_otheruser("medgalthumb",$get_news_general_user_id);
																	}
																}
																?>
																<a href="javascript:void(0);" onclick="checkload('<?php echo $mediaSrc; ?>','<?php echo $galleryImagesData;?>','<?php echo $thumbPath; ?>','<?php echo $orgPath;?>','<?php echo $galleryImagestitle;?>','<?php echo $get_gallery['id'];?>')"><?php echo $sql_meds_ph['title'];?></a>
																<?php
																$galleryImagesData = "";
																$galleryImagestitle = "";
																$mediaSrc ="";
															}
															elseif($exp_meds[1]=='reg_art' || $exp_meds[1]=='reg_com')
															{
																if($ans_photo_posts[$hpo_all]['profile_type']=='images_post')
																{
																	$get_all_gal_image = $sql_feeds->get_all_gallery_images($sql_meds_ph['id']);
																	if($get_all_gal_image!="" && $get_all_gal_image!=NULL)
														{
																	if(mysql_num_rows($get_all_gal_image)>0)
																	{
																		$galleryImagesData ="";
																		$galleryImagestitle ="";
																		$mediaSrc ="";
																		$get_news_general_user_id = $uk -> get_general_user_idby_mediaid($sql_meds_ph['id']);
																		while($row = mysql_fetch_assoc($get_all_gal_image))
																		{
																			$mediaSrc = $row[0]['image_name'];
																			$filePath = $s3->getNewURI_otheruser("medgallery",$get_news_general_user_id).$mediaSrc;
																			if($row['image_name']!=""){
																				$galleryImagesData = $galleryImagesData .",". $row['image_name'];
																				$galleryImagestitle = $galleryImagestitle .",".$row['image_title'];
																			}
																		}
																		$orgPath = $s3->getNewURI_otheruser("medgallery",$get_news_general_user_id);
																		$thumbPath = $s3->getNewURI_otheruser("medgalthumb",$get_news_general_user_id);
																	}
																	}
																	?>
																	<a href="javascript:void(0);" onclick="checkload('<?php echo $mediaSrc; ?>','<?php echo $galleryImagesData;?>','<?php echo $thumbPath; ?>','<?php echo $orgPath;?>','<?php echo $galleryImagestitle;?>','<?php echo $get_gallery['id'];?>')"><?php echo $sql_meds_ph['gallery_title'];?></a>
																	<?php
																	$galleryImagesData = "";
																	$galleryImagestitle = "";
																	$mediaSrc ="";
																}
															}
		?>
																<div style="color:#808080;font-size:12px;">
																	<div class="comment more">
																		<?php echo $ans_photo_posts[$hpo_all]['description']; ?>
																	</div>
																</div>
															</div>
															<!--<div class="activityContent">Tagged: <?php //echo $medias['tagged_users']; 
																//$name_org = explode(')',$medias['tagged_users']);
																//for($i_name_org=0;$i_name_org<count($name_org);$i_name_org++)
																//{
																	//	$end_pos = strpos($name_org[$i_name_org],'(');
																	//	$org_name = substr($name_org[$i_name_org],0,$end_pos);
																	//		echo $org_name;
																//	}
															?></div>-->
														</div>
													</div>
												</div>
											</div>
		
											<div class="rightSide">
												<a href="#" onmouseover="mopen('mm<?php echo $hpo_all;?>8')" onmouseout="mclosetime()"><img src="images/profile/arrow.png" onmouseover="this.src='images/profile/arrow-down.png'" onmouseout="this.src='images/profile/arrow.png'" /></a>
												<div id="mm<?php echo $hpo_all;?>8" class="popUp" onmouseover="mcancelclosetime()" onmouseout="mclosetime()">
													<ul>
														<li><a href="hideposts.php?id=<?php echo $ans_photo_posts[$hpo_all]['id'];?>">Hide Feed</a></li>
														<li><a href="hideposts.php?report_id=<?php echo $ans_all_posts[$po_all]['id'];?>">Report spam</a></li>
														<li><div class="seperator"></div></li>
														<li><!--<a href="Unsubscribe_feeds.php?frd=1&user_ids=<?php echo $gens['general_user_id']; ?>" onclick="return confirm_subscription_delete('<?php if($art==1){ echo $ros['name']; } elseif($com==1){ echo $ros_com['name']; } ?>')">Unsubscribe from <?php if($art==1)
															{
																echo $ros['name'];
															}
															elseif($com==1)
															{
																echo $ros_com['name'];
															} ?></a>-->
															<a href="profileedit.php#subscrriptions" >Unsubscribe from <?php if($art==1)
															{
																echo $ros['name'];
															}
															elseif($com==1)
															{
																echo $ros_com['name'];
															} ?></a>
															</li>
													</ul>
												</div>
											</div>
										</div>
		<?php
									}
								}
							}
						}
					}
				}
			}
		}
		?>
	</div>
							
	<div id="music_feeds" style="display:none;">
		<?php
		include_once("findsale_song.php");
		$ans_audio_feeds = array();
		$ans_audio_sub_feeds = array();
		$ans_audio_fan_club_feeds = array();
		$mu_art_com_id = array();
		$mu_pro_eve_id = array();
		$mu_arts_ids = array();
		$mu_coms_ids = array();
		$ans_music_posts = array();
		
		
		$sql_audio_feeds = $sql_feeds->all_feeds($chk_sql['email']);
		$run_audio_feeds = mysql_query($sql_audio_feeds);
		
		$fan_sql_audio_feeds = $sql_feeds->fan_club($chk_sql['email']);
		$fan_run_audio_feeds = mysql_query($fan_sql_audio_feeds);
		
		if($run_audio_feeds!="" && $run_audio_feeds!=NULL)
		{
			while($row_audio_feeds = mysql_fetch_assoc($run_audio_feeds))
			{
				$ans_audio_sub_feeds[] = $row_audio_feeds;
			}
		}
		
		if($fan_run_audio_feeds!="" && $fan_run_audio_feeds!=NULL)
		{
			while($fan_row_audio_feeds = mysql_fetch_assoc($fan_run_audio_feeds))
			{
				$ans_audio_fan_club_feeds[] = $fan_row_audio_feeds;
			}
		}
		
		$ans_audio_feeds = array_merge($ans_audio_sub_feeds,$ans_audio_fan_club_feeds);
		
		$ans_frds_posts = array();
		$frds_posts_all_l = $sql_feeds->all_frds_posts($_SESSION['login_id']);
		$frds_posts_all = mysql_query($frds_posts_all_l);
		if($frds_posts_all!="" && $frds_posts_all!=NULL)
		{
			if(mysql_num_rows($frds_posts_all)>0)
			{
				while($row_f = mysql_fetch_assoc($frds_posts_all))
				{
					$ans_frds_posts[] = $row_f;
				}
			}
		}
		
		if(($ans_audio_feeds!=NULL && !empty($ans_audio_feeds)) || ($ans_frds_posts!="" && !empty($ans_frds_posts)))
		{
			for($mu_fed_lo=0;$mu_fed_lo<count($ans_audio_feeds);$mu_fed_lo++)
			{
				if($ans_audio_feeds[$mu_fed_lo]['related_type']!='artist' && $ans_audio_feeds[$mu_fed_lo]['related_type']!='community')
				{
					$num_ids = explode('_',$ans_audio_feeds[$mu_fed_lo]['related_id']);
					if($ans_audio_feeds[$mu_fed_lo]['related_type']=='artist_project' || $ans_audio_feeds[$mu_fed_lo]['related_type']=='artist_event')
					{
						$mu_pro_eve_id[$mu_fed_lo]['related_id_art'] = $num_ids[0];
					}
					elseif($ans_audio_feeds[$mu_fed_lo]['related_type']=='community_project' || $ans_audio_feeds[$mu_fed_lo]['related_type']=='community_event')
					{
						$mu_art_com_id[$mu_fed_lo]['related_id_com'] = $num_ids[0];
					}
				}
				elseif($ans_audio_feeds[$mu_fed_lo]['related_type']=='artist' || $ans_audio_feeds[$mu_fed_lo]['related_type']=='community')
				{
					if($ans_audio_feeds[$mu_fed_lo]['related_type']=='artist')
					{
						$mu_pro_eve_id[$mu_fed_lo]['related_id_art'] = $ans_audio_feeds[$mu_fed_lo]['related_id'];
					}
					elseif($ans_audio_feeds[$mu_fed_lo]['related_type']=='community')
					{
						$mu_art_com_id[$mu_fed_lo]['related_id_com'] = $ans_audio_feeds[$mu_fed_lo]['related_id'];
					}
				}
			}
										
			if($mu_pro_eve_id!=0 && count($mu_pro_eve_id)>0)
			{
				for($arts=0;$arts<count($mu_pro_eve_id);$arts++)
				{
					$org_art[] = $mu_pro_eve_id[$arts]['related_id_art'];
				}
				
				$new_art_mu = array();
				$mu = 0;
				foreach ($org_art as $key => $value)
				{
					if(isset($new_art_mu[$value]))
					{
						$new_art_mu[$value] += 1;
					}
					else
					{
						$new_art_mu[$value] = 1;		
						$sqls = $sql_feeds->arts_gen($value);
						
						if($sqls!="")
						{
							$mu_arts_ids[$mu] = $sqls;
						}
					}
					$mu = $mu + 1;
				}
			}
			
			if($mu_art_com_id!="" && count($mu_art_com_id)>0)
			{
				for($coms=0;$coms<count($mu_art_com_id);$coms++)
				{
					$org_com[] = $mu_art_com_id[$coms]['related_id_com'];
				}
				
				$new_com_mu = array();
				$muh_c = 0;
				foreach ($org_com as $key => $value)
				{
					if(isset($new_com_mu[$value]))
					{
						$new_com_mu[$value] += 1;
					}
					else
					{
						$new_com_mu[$value] = 1;
			
						$sqls_c = $sql_feeds->coms_gen($value);
						if($sqls_c!="")
						{
							$mu_coms_ids[$muh_c] = $sqls_c;
						}
					}
					$muh_c = $muh_c + 1;
				}
			}
	
			$ans_frds_posts = array();
			$frds_posts_all_l = $sql_feeds->all_frds_posts($_SESSION['login_id']);
			$frds_posts_all = mysql_query($frds_posts_all_l);
			if($frds_posts_all!="" && $frds_posts_all!=NULL)
			{
				if(mysql_num_rows($frds_posts_all)>0)
				{
					while($row_f = mysql_fetch_assoc($frds_posts_all))
					{
						$ans_frds_posts[] = $row_f;
					}
				}
			}
			
			$doub_mu_chk = array_merge($mu_arts_ids,$mu_coms_ids,$ans_frds_posts);
			
			for($chm_d=0;$chm_d<count($doub_mu_chk);$chm_d++)
			{
				$rel_d_id[] = $doub_mu_chk[$chm_d]['general_user_id'];
			}
			
			$new_array_rep_mu = array();
			foreach ($rel_d_id as $key => $value)
			{
				if(isset($new_array_rep_mu[$value]))
					$new_array_rep_mu[$value] += 1;
				else
					$new_array_rep_mu[$value] = 1;
			}
			
			foreach ($new_array_rep_mu as $uid => $n)
			{
				$ex_uid_mu = $ex_uid_mu.','.$uid;
			}
			$ex_tr_mu = trim($ex_uid_mu, ",");
			$sep_ids_mp = explode(',',$ex_tr_mu);
			
			if($sep_ids_mp!="" && count($sep_ids_mp)>0)
			{
				for($se_ca=0;$se_ca<count($sep_ids_mp);$se_ca++)
				{
					$types = 'songs_post';
					$ids = $sql_feeds->pmva_posts_check($sep_ids_mp[$se_ca],$_SESSION['login_id'],$types);
					
					//die;
					if($ids!="" && count($ids)>0)
					{
						$posts_music = $sql_feeds->pmv_posts_check($sep_ids_mp[$se_ca],$_SESSION['login_id'],$types);
						if($posts_music!="" && $posts_music!=NULL)
						{
							if($posts_music!="" && mysql_num_rows($posts_music)>0)
							{
								while($rows_posts_all = mysql_fetch_assoc($posts_music))
								{
									$ans_music_posts[] = $rows_posts_all;
								}
							}
						}
					}
				}
			}
			
			if($ans_music_posts!="" && count($ans_music_posts)>0)
			{
				for($mpu_all=0;$mpu_all<count($ans_music_posts);$mpu_all++)
				{
					if($ans_music_posts[$mpu_all]['profile_type']=='songs_post')
					{
						$exp_meds = explode('~',$ans_music_posts[$mpu_all]['media']);
						if($exp_meds[1]=='reg_med')
						{
							$sep_ids_m = $sql_feeds->get_mediaSelected($exp_meds[0]);
						}
						elseif(($exp_meds[1]=='reg_art' || $exp_meds[1]=='reg_com') && $ans_music_posts[$mpu_all]['profile_type']=='songs_post')
						{
							if($exp_meds[1]=='reg_art')
							{
								$reg_table_type = 'general_artist_audio';
							}
							elseif($exp_meds[1]=='reg_com')
							{
								$reg_table_type = 'general_community_audio';
							}
							
							$sep_ids_m = $sql_feeds->get_MediaReg($reg_table_type,$exp_meds[0]);
						}
					}
					
					if(isset($sep_ids_m) && $sep_ids_m!='0' && $sep_ids_m!=NULL)
					{
						$gens = $sql_feeds->get_log_ids($ans_music_posts[$mpu_all]['general_user_id']);
						if($ans_music_posts[$mpu_all]['display_to']=='All' && $ans_music_posts[$mpu_all]['profile_type']=='songs_post')
						{
							$hide16 = 0;
							$get_gen_id = $sql_feeds->get_general_info();
							$hide_by = $ans_music_posts[$mpu_all]['hidden_by'];
							$exp_hide_by = explode(",",$hide_by);
							for($exp_count=0;$exp_count<=count($exp_hide_by);$exp_count++)
							{
								if($exp_hide_by[$exp_count] ==$get_gen_id['general_user_id'])
								{
									$hide16 =1;
								}
							}
							if($hide16 != 1)
							{
		?>
								<div class="storyCont">
									<div class="leftSide">
										<div class="avatar">
		<?php
											$art = 0;
										
											if($gens['artist_id']!=0)
											{
												$sql_art = mysql_query("SELECT * FROM general_artist WHERE artist_id='".$gens['artist_id']."' AND status=0 AND active=0 AND del_status=0");
												if($sql_art!="" && $sql_art!=NULL)
												{
													if(mysql_num_rows($sql_art)>0)
													{
														$ros = mysql_fetch_assoc($sql_art);
														$art = 1;
													}
												}
											}
													
											$com = 0;
											if($gens['community_id']!=0)
											{
												$sql_com = mysql_query("SELECT * FROM general_community WHERE community_id='".$gens['community_id']."' AND status=0 AND active=0 AND del_status=0");
												if($sql_com!="" && $sql_com!=NULL)
												{
													if(mysql_num_rows($sql_com)>0)
													{
														$ros_com = mysql_fetch_assoc($sql_com);
														$com = 1;
													}
												}
											}
											
											if($art==1)
											{
												$get_news_general_user_id = $uk -> get_general_user_idby_artistid($ros['artist_id']);
		?>
												<img width="50" height="50" src="<?php
												if($ros['image_name']!="" && !empty($ros['image_name']))
												{
													echo $s3->getNewURI_otheruser("artjcropprofile",$get_news_general_user_id).$ros['image_name']; 
												}
												else
												{
													echo "http://artjcropprofile.s3.amazonaws.com/Noimage.png";
												}
											?>" />
		<?php
											}
											elseif($com==1)
											{
												$get_news_general_user_id = $uk -> get_general_user_idby_communityid($ros_com['community_id']);
		?>
												<img width="50" height="50" src="<?php
											
											
											if($ros_com['image_name']!="" && !empty($ros_com['image_name']))
											{
												echo $s3->getNewURI_otheruser("comjcropprofile",$get_news_general_user_id).$ros_com['image_name']; 
											}
											else
											{
												echo "http://comjcropprofile.s3.amazonaws.com/Noimage.png";
											}
											
											?>" />
											<?php
											}
											?>
															</div>
															<?php //echo "table=".$reg_table_media_type;
															//echo $ans_audio_feeds[$audio_fed_lo]['id'];
															?>
																		<div class="storyContainer">
																			<div class="storyTitle">
																				<span class="userName"><?php if($art==1)
															{
																if($ros['profile_url']!="")
														{
													?>
														<a href="/<?php echo $ros['profile_url']; ?>">
													<?php
														}
																echo $ros['name'];
																if($ros['profile_url']!="")
														{
													?>
														</a>
													<?php
														}
															}
															elseif($com==1)
															{
																if($ros_com['profile_url']!="")
														{
													?>
														<a href="/<?php echo $ros_com['profile_url']; ?>">
													<?php
														}
																echo $ros_com['name'];
																if($ros_com['profile_url']!="")
														{
													?>
														</a>
													<?php
														}
															} ?></span> <?php //echo $ans_music_posts[$mpu_all]['description']; 
															
															?>
																			</div>
																			<div class="activityCont">
																				<div class="sideL">
																					<div class="storyImage"></div>
																					<div class="player">
																						<img src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" onclick="showPlayer('a','<?php echo $sep_ids_m['id'];?>','play');">
																						<img src="images/profile/add.gif" onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'" onclick="showPlayer('a','<?php echo $sep_ids_m['id'];?>','add');">
																						<?php
																						if($sep_ids_m['sharing_preference']==5 || $sep_ids_m['sharing_preference']==2)
																						{
																							$song_image_name_cart = $sql_feeds->get_audio_img($sep_ids_m['id']);
																							$free_down_rel ="";
																							for($find_down=0;$find_down<=count($get_songs_pnew_w);$find_down++)
																							{
																								if(isset($sep_ids_m) && $sep_ids_m != null)
																								{
																									if($get_songs_pnew_w[$find_down]['id']==$sep_ids_m['id']){
																										$free_down_rel = "yes";
																									}
																									else{
																										if($free_down_rel =="" || $free_down_rel =="no"){
																											$free_down_rel = "no";
																										}
																									}
																								}
																							}
																							if($free_down_rel == "yes"){
																							 ?>
																							 <!--<a href="javascript:void(0);" onclick="download_directly_song('<?php echo $sep_ids_m['id'];?>','<?php echo str_replace("'","\'",$sep_ids_m['title']);?>','<?php echo str_replace("'","\'",$sep_ids_m['creator']); ?>','<?php echo str_replace("'","\'",$sep_ids_m['from']); ?>')"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>-->
																						 <?php
																							}
																							else if($sep_ids_m['sharing_preference']==2){
																								if($sep_ids_m['id']!=""){
																								?>
																								<!--<a href="javascript:void(0);" onclick="ask_to_donate('<?php echo str_replace("'","\'",$sep_ids_m['title']);?>','<?php echo str_replace("'","\'",$sep_ids_m['creator']);?>','<?php echo $sep_ids_m['id'];?>','download_song<?php echo $sep_ids_m['id'];?>')"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>-->
																							<?php
																								}else{
																								?>
																								<!--<a href="javascript:void(0);" onclick="ask_to_donate('<?php echo str_replace("'","\'",$sep_ids_m['title']);?>','<?php echo str_replace("'","\'",$sep_ids_m['creator']);?>','<?php echo $sep_ids_m['id'];?>','download_song<?php echo $sep_ids_m['id'];?>')"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>-->
																							<?php
																								}
																							}
																							else{
																						?>
																							<!--<a href="javascript:void(0);" onclick="down_song_pop(<?php echo $sep_ids_m['id'];?>)"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>-->
																						<?php
																							}
																						}else{
																						?>	
																						<!--<img src="../images/profile/download-over.gif" style="cursor:default"/>-->
																						<?php
																						}
																						?>
																						<!--<a href="addtocart.php?seller_id=<?php echo $sep_ids_m['general_user_id']; ?>&media_id=<?php echo $sep_ids_m['id'] ?>&buyer_id=<?php echo $_SESSION['gen_user_id'];?>&title=<?php echo str_replace("'","\'",$sep_ids_m['title']); ?>&image_name=<?php echo $song_image_name_cart;?>" id="download_song<?php echo $sep_ids_m['id'];?>" style="display:none;"></a>-->
																						<script type="text/javascript">
																						$(document).ready(function() {
																								$("#download_song<?php echo $sep_ids_m['id'];?>").fancybox({
																								'width'				: '75%',
																								'height'			: '75%',
																								'transitionIn'		: 'none',
																								'transitionOut'		: 'none',
																								'type'				: 'iframe'
																								});
																						});
																						</script>
																					</div>
																				</div>
																				<div class="sideR">
																					<div class="activityTitle"><?php
																					if($exp_meds[1]=='reg_med')
																					{
																						$sql_me = mysql_query("SELECT * FROM media_songs WHERE media_id='".$sep_ids_m['id']."'");
																						$ans_me = mysql_fetch_assoc($sql_me);
																						$ecehos = '<a style="text-decoration: none; cursor: text;" href="javascript:void(0)" > '.$ans_me['track'].' </a>';
																						if($sep_ids_m['media_type']==114)
																						{
																							//$ecehos .= "<a href=javascript:void(0) onclick = showPlayer('a',".$sep_ids_m['id'].",'add');>".$sep_ids_m['title']."</a>";
																							
																							$ecehos .= "<a style='text-decoration: none; cursor: text;' href='javascript:void(0)'>".$sep_ids_m['title']."</a>";
																						}
																						
																	
																	if($sep_ids_m['creator']!="")
																	{
																		$create_c = strpos($sep_ids_m['creator'],'(');
																		$end = $sep_ids_m['creator'];
																		$exp_cr = explode('|',$sep_ids_m['creator_info']);
																		
																		if($exp_cr[0]=='general_artist')
																		{
																			$sql_c_ar = mysql_query("SELECT * FROM general_artist WHERE artist_id='".$exp_cr[1]."'");
																		}
																		if($exp_cr[0]=='general_community')
																		{
																			$sql_c_ar = mysql_query("SELECT * FROM general_community WHERE community_id='".$exp_cr[1]."'");
																		}
																		if($exp_cr[0]=='artist_project' || $exp_cr[0]=='community_project' || $exp_cr[0]=='community_event' || $exp_cr[0]=='artist_event')
																		{
																			$sql_c_ar = mysql_query("SELECT * FROM $exp_cr[0] WHERE id='".$exp_cr[1]."'");
																		}
																		
																		
																		if(isset($sql_c_ar))
																		{
																			if($sql_c_ar!="" && $sql_c_ar!=NULL)
												{
																				if(mysql_num_rows($sql_c_ar)>0)
																				{
																					$ans_c_ar = mysql_fetch_assoc($sql_c_ar);
																					if(isset($ans_c_ar['name']))
																				{
																					$end = $ans_c_ar['name'];
																				}
																				else
																				{
																					$end = $ans_c_ar['title'];
																				}
																					if($ans_c_ar['profile_url']!="" && !empty($ans_c_ar['profile_url']))
																					{
																						$ecehos .= " By  <a href='/".$ans_c_ar['profile_url']."'><b>".$end."</b></a>  ";
																					}
																					else
																					{
																						$ecehos .= " By  <b>".$end."</b>  ";
																					}
																				}
																				else
																			{
																				$ecehos .= " By  <b>".$end."</b>  ";
																			}
																			}
																			else
																			{
																				$ecehos .= " By  <b>".$end."</b>  ";
																			}
																		}
																		else
																		{
																			
																			$ecehos .= " By  <b>".$end."</b>  ";
																		}
																	}
																	if($sep_ids_m['from']!="")
																	{
																		$create_f = strpos($sep_ids_m['from'],'(');
																		$end_f = substr($sep_ids_m['from'],0,$create_f);
																		
																		$exp_fr = explode('|',$sep_ids_m['from_info']);
																		if($exp_fr[0]=='general_artist')
																		{
																			$sql_f_ar = mysql_query("SELECT * FROM general_artist WHERE artist_id='".$exp_fr[1]."'");
																		}
																		if($exp_fr[0]=='general_community')
																		{
																			$sql_f_ar = mysql_query("SELECT * FROM general_community WHERE community_id='".$exp_fr[1]."'");
																		}
																		if($exp_fr[0]=='artist_project' || $exp_fr[0]=='community_project' || $exp_fr[0]=='community_event' || $exp_fr[0]=='artist_event')
																		{
																			$sql_f_ar = mysql_query("SELECT * FROM $exp_fr[0] WHERE id='".$exp_fr[1]."'");
																		}
																		
																		if(isset($sql_f_ar))
																		{
																			if($sql_f_ar!="" && $sql_f_ar!=NULL)
												{
																			if(mysql_num_rows($sql_f_ar)>0)
																			{
																				$ans_f_ar = mysql_fetch_assoc($sql_f_ar);
																				if(isset($ans_f_ar['name']))
																				{
																					$end_f = $ans_f_ar['name'];
																				}
																				else
																				{
																					$end_f = $ans_f_ar['title'];
																				}
																				if($ans_f_ar['profile_url']!="" && !empty($ans_f_ar['profile_url']))
																				{
																					$ecehos .= " From  <a href='/".$ans_f_ar['profile_url']."'><b>".$end_f."</b></a>";
																				}
																				else
																				{
																					$ecehos .= " From  <b>".$end_f."</b>";
																				}
																			}
																			else
																			{
																				$ecehos .= " From  <b>".$end_f."</b>";
																			}
																			}
																			else
																			{
																				$ecehos .= " From  <b>".$end_f."</b>";
																			}
																		}
																		else
																		{
																			$ecehos .= " From  <b>".$end_f."</b>";
																		}
																	}
																						
																						echo $ecehos;
																					}
																					elseif($exp_meds[1]=='reg_art' || $exp_meds[1]=='reg_com')
																					{
																						if($ans_music_posts[$mpu_all]['profile_type']=='songs_post')
																						{
																							echo $sep_ids_m['audio_name'];
																						}
																					}
																					?>
																						<div style="color:#808080;font-size:12px;">
																							<div class="comment more">
																								<?php echo $ans_music_posts[$mpu_all]['description']; ?>
																							</div>
																						</div>
																					</div>
																					<!--<div class="activityContent">Tagged: <?php //echo $medias['tagged_users']; 
																						//$name_org = explode(')',$medias['tagged_users']);
																						//for($i_name_org=0;$i_name_org<count($name_org);$i_name_org++)
																						//{
																						//	$end_pos = strpos($name_org[$i_name_org],'(');
																						//	$org_name = substr($name_org[$i_name_org],0,$end_pos);
																					//		echo $org_name;
																					//	}
																					?></div>-->
																				</div>
																			</div>
																		</div>
																	</div>
																	<div class="rightSide">
																		<a href="#" onmouseover="mopen('mm<?php echo $mpu_all;?>9')" onmouseout="mclosetime()"><img src="images/profile/arrow.png" onmouseover="this.src='images/profile/arrow-down.png'" onmouseout="this.src='images/profile/arrow.png'" /></a>
																		<div id="mm<?php echo $mpu_all;?>9" class="popUp" onmouseover="mcancelclosetime()" onmouseout="mclosetime()">
																			<ul>
																				<li><a href="hideposts.php?id=<?php echo $ans_music_posts[$mpu_all]['id'];?>">Hide Feed</a></li>
																				<li><a href="hideposts.php?report_id=<?php echo $ans_all_posts[$po_all]['id'];?>">Report spam</a></li>
																				<li><div class="seperator"></div></li>
																				<li><!--<a href="Unsubscribe_feeds.php?all=1&user_ids=<?php echo $gens['general_user_id']; ?>" onclick="return confirm_subscription_delete('<?php if($art==1){ echo $ros['name']; } elseif($com==1){ echo $ros_com['name']; } ?>')">Unsubscribe from <?php if($art==1)
															{
																echo $ros['name'];
															}
															elseif($com==1)
															{
																echo $ros_com['name'];
															} ?></a>-->
															<a href="profileedit.php#subscrriptions" >Unsubscribe from <?php if($art==1)
															{
																echo $ros['name'];
															}
															elseif($com==1)
															{
																echo $ros_com['name'];
															} ?></a>
															</li>
																			</ul>
																		</div>
																	</div>
																</div>
			<?php
														}
													}
													elseif($ans_music_posts[$mpu_all]['display_to']=='Fans')
													{
														if($gens!="")
														{
															$fans_all =	$sql_feeds->fans_cond($chk_sql['email'],$gens['community_id'],$gens['artist_id']);
															if($fans_all=='1')
															{
																$hide17 = 0;
																$get_gen_id = $sql_feeds->get_general_info();
																$hide_by = $ans_music_posts[$mpu_all]['hidden_by'];
																$exp_hide_by = explode(",",$hide_by);
																for($exp_count=0;$exp_count<=count($exp_hide_by);$exp_count++)
																{
																	if($exp_hide_by[$exp_count] ==$get_gen_id['general_user_id'])
																	{
																		$hide17 =1;
																	}
																}
																if($hide17 != 1)
																{
			?>
																<div class="storyCont">
																	<div class="leftSide">
																		<div class="avatar">
																			<?php
													$art = 0;
													
													if($gens['artist_id']!=0)
													{
														$sql_art = mysql_query("SELECT * FROM general_artist WHERE artist_id='".$gens['artist_id']."' AND status=0 AND active=0 AND del_status=0");
														if($sql_art!="" && $sql_art!=NULL)
														{
															if(mysql_num_rows($sql_art)>0)
															{
																$ros = mysql_fetch_assoc($sql_art);
																$art = 1;
															}
														}
													}
													
													$com = 0;
													if($gens['community_id']!=0)
													{
														$sql_com = mysql_query("SELECT * FROM general_community WHERE community_id='".$gens['community_id']."' AND status=0 AND active=0 AND del_status=0");
														if($sql_com!="" && $sql_com!=NULL)
														{
															if(mysql_num_rows($sql_com)>0)
															{
																$ros_com = mysql_fetch_assoc($sql_com);
																$com = 1;
															}
														}
													}
													
													if($art==1)
													{
														if($ros['profile_url']!="")
														{
													?>
														<a href="/<?php echo $ros['profile_url']; ?>">
													<?php
														}
														$get_news_general_user_id = $uk -> get_general_user_idby_artistid($ros['artist_id']);
													?>
														<img width="50" height="50" src="<?php
														if($ros['image_name']!="" && !empty($ros['image_name']))
														{
															echo $s3->getNewURI_otheruser("artjcropprofile",$get_news_general_user_id).$ros['image_name']; 
														}
														else
														{
															echo "http://artjcropprofile.s3.amazonaws.com/Noimage.png";
														}
													?>" />
													<?php
														if($ros['profile_url']!="")
														{
													?>
														</a>
													<?php
														}
													}
													elseif($com==1)
													{
														if($ros_com['profile_url']!="")
														{
													?>
														<a href="/<?php echo $ros_com['profile_url']; ?>">
													<?php
														}
														$get_news_general_user_id = $uk -> get_general_user_idby_communityid($ros_com['community_id']);
													?>
														<img width="50" height="50" src="<?php
													
													
													if($ros_com['image_name']!="" && !empty($ros_com['image_name']))
													{
														echo $s3->getNewURI_otheruser("comjcropprofile",$get_news_general_user_id).$ros_com['image_name']; 
													}
													else
													{
														echo "http://comjcropprofile.s3.amazonaws.com/Noimage.png";
													}
													
													?>" />
													<?php
														if($ros_com['profile_url']!="")
														{
													?>
														<a href="/<?php echo $ros['profile_url']; ?>">
													<?php
														}
													}
													?>
																	</div>
																	<?php //echo "table=".$reg_table_media_type;
																	//echo $ans_audio_feeds[$audio_fed_lo]['id'];
																	?>
																		<div class="storyContainer">
																			<div class="storyTitle">
																				<span class="userName"><?php if($art==1)
															{
																if($ros['profile_url']!="")
														{
													?>
														<a href="/<?php echo $ros['profile_url']; ?>">
													<?php
														}
																echo $ros['name'];
																if($ros['profile_url']!="")
														{
													?>
														</a>
													<?php
														}
															}
															elseif($com==1)
															{
																if($ros_com['profile_url']!="")
														{
													?>
														<a href="/<?php echo $ros_com['profile_url']; ?>">
													<?php
														}
																echo $ros_com['name'];
																if($ros_com['profile_url']!="")
														{
													?>
														</a>
													<?php
														}
															} ?></span>  <?php //echo $ans_music_posts[$mpu_all]['description']; ?>
																			</div>
																			<div class="activityCont">
																				<div class="sideL">
																					<div class="storyImage"></div>
																					<div class="player">
																						<img src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" onclick="showPlayer('a','<?php echo $sep_ids_m['id'];?>','play');">
																						<img src="images/profile/add.gif" onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'" onclick="showPlayer('a','<?php echo $sep_ids_m['id'];?>','add');">
																						<?php
																						if($sep_ids_m['sharing_preference']==5 || $sep_ids_m['sharing_preference']==2)
																						{
																							$song_image_name_cart = $sql_feeds->get_audio_img($sep_ids_m['id']);
																							$free_down_rel ="";
																							for($find_down=0;$find_down<=count($get_songs_pnew_w);$find_down++)
																							{
																								if(isset($sep_ids_m) && $sep_ids_m != null)
																								{
																									if($get_songs_pnew_w[$find_down]['id']==$sep_ids_m['id']){
																										$free_down_rel = "yes";
																									}
																									else{
																										if($free_down_rel =="" || $free_down_rel =="no"){
																											$free_down_rel = "no";
																										}
																									}
																								}
																							}
																							if($free_down_rel == "yes"){
																							 ?>
																							 <!--<a href="javascript:void(0);" onclick="download_directly_song('<?php echo $sep_ids_m['id'];?>','<?php echo str_replace("'","\'",$sep_ids_m['title']);?>','<?php echo str_replace("'","\'",$sep_ids_m['creator']); ?>','<?php echo str_replace("'","\'",$sep_ids_m['from']); ?>')"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>-->
																						 <?php
																							}
																							else if($sep_ids_m['sharing_preference']==2){
																								if($sep_ids_m['id']!=""){
																								?>
																								<!--<a href="javascript:void(0);" onclick="ask_to_donate('<?php echo str_replace("'","\'",$sep_ids_m['title']);?>','<?php echo str_replace("'","\'",$sep_ids_m['creator']);?>','<?php echo $sep_ids_m['id'];?>','download_song<?php echo $sep_ids_m['id'];?>')"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>-->
																							<?php
																								}else{
																								?>
																								<!--<a href="javascript:void(0);" onclick="ask_to_donate('<?php echo str_replace("'","\'",$sep_ids_m['title']);?>','<?php echo str_replace("'","\'",$sep_ids_m['creator']);?>','<?php echo $sep_ids_m['id'];?>','download_song<?php echo $sep_ids_m['id'];?>')"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>-->
																							<?php
																								}
																							}
																							else{
																						?>
																							<!--<a href="javascript:void(0);" onclick="down_song_pop(<?php echo $sep_ids_m['id'];?>)"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>-->
																						<?php
																							}
																						}else{
																						?>	
																						<!--<img src="../images/profile/download-over.gif" style="cursor:default"/>-->
																						<?php
																						}
																						?>
																						<!--<a href="addtocart.php?seller_id=<?php echo $sep_ids_m['general_user_id']; ?>&media_id=<?php echo $sep_ids_m['id'] ?>&buyer_id=<?php echo $_SESSION['gen_user_id'];?>&title=<?php echo str_replace("'","\'",$sep_ids_m['title']); ?>&image_name=<?php echo $song_image_name_cart;?>" id="download_song<?php echo $sep_ids_m['id'];?>" style="display:none;"></a>-->
																						<script type="text/javascript">
																						$(document).ready(function() {
																								$("#download_song<?php echo $sep_ids_m['id'];?>").fancybox({
																								'width'				: '75%',
																								'height'			: '75%',
																								'transitionIn'		: 'none',
																								'transitionOut'		: 'none',
																								'type'				: 'iframe'
																								});
																						});
																						</script>
																					</div>
																				</div>
																				<div class="sideR">
																					<div class="activityTitle"><?php
																					if($exp_meds[1]=='reg_med')
																					{
																						$sql_me = mysql_query("SELECT * FROM media_songs WHERE media_id='".$sep_ids_m['id']."'");
																						$ans_me = mysql_fetch_assoc($sql_me);
																						$ecehos = '<a style="text-decoration: none; cursor: text;" href="javascript:void(0)"> '.$ans_me['track'].' </a>';
																						//$ecehos .= '<a href="">'.$sep_ids_m['title'].'</a>';
																						//$ecehos .= "<a href=javascript:void(0) onclick = showPlayer('a',".$sep_ids_m['id'].",'add');>".$sep_ids_m['title']."</a>";
																						$ecehos .= "<a style='text-decoration: none; cursor: text;' href='javascript:void(0)'>".$sep_ids_m['title']."</a>";
																	
																	if($sep_ids_m['creator']!="")
																	{
																		$create_c = strpos($sep_ids_m['creator'],'(');
																		$end = $sep_ids_m['creator'];
																		$exp_cr = explode('|',$sep_ids_m['creator_info']);
																		
																		if($exp_cr[0]=='general_artist')
																		{
																			$sql_c_ar = mysql_query("SELECT * FROM general_artist WHERE artist_id='".$exp_cr[1]."'");
																		}
																		if($exp_cr[0]=='general_community')
																		{
																			$sql_c_ar = mysql_query("SELECT * FROM general_community WHERE community_id='".$exp_cr[1]."'");
																		}
																		if($exp_cr[0]=='artist_project' || $exp_cr[0]=='community_project' || $exp_cr[0]=='community_event' || $exp_cr[0]=='artist_event')
																		{
																			$sql_c_ar = mysql_query("SELECT * FROM $exp_cr[0] WHERE id='".$exp_cr[1]."'");
																		}
																		
																		
																		if(isset($sql_c_ar))
																		{
																			if($sql_c_ar!="" && $sql_c_ar!=NULL)
														{
																			if(mysql_num_rows($sql_c_ar)>0)
																			{
																				$ans_c_ar = mysql_fetch_assoc($sql_c_ar);
																				if(isset($ans_c_ar['name']))
																				{
																					$end = $ans_c_ar['name'];
																				}
																				else
																				{
																					$end = $ans_c_ar['title'];
																				}
																				if($ans_c_ar['profile_url']!="" && !empty($ans_c_ar['profile_url']))
																				{
																					$ecehos .= " By  <a href='/".$ans_c_ar['profile_url']."'><b>".$end."</b></a>  ";
																				}
																				else
																				{
																					$ecehos .= " By  <b>".$end."</b>  ";
																				}
																			}
																			else
																			{
																				$ecehos .= " By  <b>".$end."</b>  ";
																			}
																			}
																			else
																			{
																				$ecehos .= " By  <b>".$end."</b>  ";
																			}
																		}
																		else
																		{
																			
																			$ecehos .= " By  <b>".$end."</b>  ";
																		}
																	}
																	if($sep_ids_m['from']!="")
																	{
																		$create_f = strpos($sep_ids_m['from'],'(');
																		$end_f = substr($sep_ids_m['from'],0,$create_f);
																		
																		$exp_fr = explode('|',$sep_ids_m['from_info']);
																		if($exp_fr[0]=='general_artist')
																		{
																			$sql_f_ar = mysql_query("SELECT * FROM general_artist WHERE artist_id='".$exp_fr[1]."'");
																		}
																		if($exp_fr[0]=='general_community')
																		{
																			$sql_f_ar = mysql_query("SELECT * FROM general_community WHERE community_id='".$exp_fr[1]."'");
																		}
																		if($exp_fr[0]=='artist_project' || $exp_fr[0]=='community_project' || $exp_fr[0]=='community_event' || $exp_fr[0]=='artist_event')
																		{
																			$sql_f_ar = mysql_query("SELECT * FROM $exp_fr[0] WHERE id='".$exp_fr[1]."'");
																		}
																		
																		if(isset($sql_f_ar))
																		{
																			if($sql_f_ar!="" && $sql_f_ar!=NULL)
														{
																			if(mysql_num_rows($sql_f_ar)>0)
																			{
																				$ans_f_ar = mysql_fetch_assoc($sql_f_ar);
																				if(isset($ans_f_ar['name']))
																				{
																					$end_f = $ans_f_ar['name'];
																				}
																				else
																				{
																					$end_f = $ans_f_ar['title'];
																				}
																				if($ans_f_ar['profile_url']!="" && !empty($ans_f_ar['profile_url']))
																				{
																					$ecehos .= " From  <a href='/".$ans_f_ar['profile_url']."'><b>".$end_f."</b></a>";
																				}
																				else
																				{
																					$ecehos .= " From  <b>".$end_f."</b>";
																				}
																			}
																			else
																			{
																				$ecehos .= " From  <b>".$end_f."</b>";
																			}
																			}
																			else
																			{
																				$ecehos .= " From  <b>".$end_f."</b>";
																			}
																		}
																		else
																		{
																			$ecehos .= " From  <b>".$end_f."</b>";
																		}
																	}
																						echo $ecehos;
																					}
																					elseif($exp_meds[1]=='reg_art' || $exp_meds[1]=='reg_com')
																					{
																						if($ans_music_posts[$mpu_all]['profile_type']=='songs_post')
																						{
																							echo $sep_ids_m['audio_name'];
																						}
																						
																					}?>
																						<div style="color:#808080;font-size:12px;">
																							<div class="comment more">
																								<?php echo $ans_music_posts[$mpu_all]['description']; ?>
																							</div>
																						</div>
																					</div>
																					<!--<div class="activityContent">Tagged: <?php //echo $medias['tagged_users']; 
																						//$name_org = explode(')',$medias['tagged_users']);
																						//for($i_name_org=0;$i_name_org<count($name_org);$i_name_org++)
																						//{
																						//	$end_pos = strpos($name_org[$i_name_org],'(');
																						//	$org_name = substr($name_org[$i_name_org],0,$end_pos);
																					//		echo $org_name;
																					//	}
																					?></div>-->
																				</div>
																			</div>
																		</div>
																	</div>
																	<div class="rightSide">
																		<a href="#" onmouseover="mopen('mm<?php echo $mpu_all;?>10')" onmouseout="mclosetime()"><img src="images/profile/arrow.png" onmouseover="this.src='images/profile/arrow-down.png'" onmouseout="this.src='images/profile/arrow.png'" /></a>
																		<div id="mm<?php echo $mpu_all;?>10" class="popUp" onmouseover="mcancelclosetime()" onmouseout="mclosetime()">
																			<ul>
																				<li><a href="hideposts.php?id=<?php echo $ans_music_posts[$mpu_all]['id'];?>">Hide Feed</a></li>
																				<li><a href="hideposts.php?report_id=<?php echo $ans_all_posts[$po_all]['id'];?>">Report spam</a></li>
																				<li><div class="seperator"></div></li>
																				<li><a href="">Unsubscribe from <?php echo $gens['fname'].' '.$gens['lname']; ?></a></li>
																			</ul>
																		</div>
																	</div>
																</div>
			<?php
																}		
															}
														}
													}
													elseif($ans_music_posts[$mpu_all]['display_to']=='Subscribers')
													{
														if($gens!="")
														{
															$subscribers_all =	$sql_feeds->subscribers_cond($chk_sql['email'],$gens['community_id'],$gens['artist_id']);
															if($subscribers_all=='1')
															{
																$hide18 = 0;
																$get_gen_id = $sql_feeds->get_general_info();
																$hide_by = $ans_music_posts[$mpu_all]['hidden_by'];
																$exp_hide_by = explode(",",$hide_by);
																for($exp_count=0;$exp_count<=count($exp_hide_by);$exp_count++)
																{
																	if($exp_hide_by[$exp_count] ==$get_gen_id['general_user_id'])
																	{
																		$hide18 =1;
																	}
																}
																if($hide18 != 1)
																{
			?>
																<div class="storyCont">
																	<div class="leftSide">
																		<div class="avatar">
																			<?php
													$art = 0;
													
													if($gens['artist_id']!=0)
													{
														$sql_art = mysql_query("SELECT * FROM general_artist WHERE artist_id='".$gens['artist_id']."' AND status=0 AND active=0 AND del_status=0");
														if($sql_art!="" && $sql_art!=NULL)
														{
															if(mysql_num_rows($sql_art)>0)
															{
																$ros = mysql_fetch_assoc($sql_art);
																$art = 1;
															}
														}
													}
													
													$com = 0;
													if($gens['community_id']!=0)
													{
														$sql_com = mysql_query("SELECT * FROM general_community WHERE community_id='".$gens['community_id']."' AND status=0 AND active=0 AND del_status=0");
														if($sql_com!="" && $sql_com!=NULL)
														{
															if(mysql_num_rows($sql_com)>0)
															{
																$ros_com = mysql_fetch_assoc($sql_com);
																$com = 1;
															}
														}
													}
													
													if($art==1)
													{
														if($ros['profile_url']!="")
														{
													?>
														<a href="/<?php echo $ros['profile_url']; ?>">
													<?php
														}
														$get_news_general_user_id = $uk -> get_general_user_idby_artistid($ros['artist_id']);
													?>
														<img width="50" height="50" src="<?php
														if($ros['image_name']!="" && !empty($ros['image_name']))
														{
															echo $s3->getNewURI_otheruser("artjcropprofile",$get_news_general_user_id).$ros['image_name']; 
														}
														else
														{
															echo "http://artjcropprofile.s3.amazonaws.com/Noimage.png";
														}
													?>" />
													<?php
														if($ros['profile_url']!="")
														{
													?>
														</a>
													<?php
														}
													}
													elseif($com==1)
													{
														if($ros_com['profile_url']!="")
														{
													?>
														<a href="/<?php echo $ros_com['profile_url']; ?>">
													<?php
														}
														$get_news_general_user_id = $uk -> get_general_user_idby_communityid($ros_com['community_id']);
													?>
														<img width="50" height="50" src="<?php
													
													
													if($ros_com['image_name']!="" && !empty($ros_com['image_name']))
													{
														echo $s3->getNewURI_otheruser("comjcropprofile",$get_news_general_user_id).$ros_com['image_name']; 
													}
													else
													{
														echo "http://comjcropprofile.s3.amazonaws.com/Noimage.png";
													}
													
													?>" />
													<?php
														if($ros_com['profile_url']!="")
														{
													?>
														</a>
													<?php
														}
													}
													?>
																	</div>
																	<?php //echo "table=".$reg_table_media_type;
																	//echo $ans_audio_feeds[$audio_fed_lo]['id'];
																	?>
																		<div class="storyContainer">
																			<div class="storyTitle">
																				<span class="userName"><?php if($art==1)
															{
																if($ros['profile_url']!="")
														{
													?>
														<a href="/<?php echo $ros['profile_url']; ?>">
													<?php
														}
																echo $ros['name'];
																if($ros['profile_url']!="")
														{
													?>
														</a>
													<?php
														}
															}
															elseif($com==1)
															{
																if($ros_com['profile_url']!="")
														{
													?>
														<a href="/<?php echo $ros_com['profile_url']; ?>">
													<?php
														}
																echo $ros_com['name'];
																if($ros_com['profile_url']!="")
														{
													?>
														</a>
													<?php
														}
															} ?></span>  <?php //echo $ans_music_posts[$mpu_all]['description']; ?>
																			</div>
																			<div class="activityCont">
																				<div class="sideL">
																					<div class="storyImage"></div>
																					<div class="player">
																						<img src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" onclick="showPlayer('a','<?php echo $sep_ids_m['id']; ?>','play');">
																						<img src="images/profile/add.gif" onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'" onclick="showPlayer('a','<?php echo $sep_ids_m['id']; ?>','add');">
																						<?php
																						if($sep_ids_m['sharing_preference']==5 || $sep_ids_m['sharing_preference']==2)
																						{
																							$song_image_name_cart = $sql_feeds->get_audio_img($sep_ids_m['id']);
																							$free_down_rel ="";
																							for($find_down=0;$find_down<=count($get_songs_pnew_w);$find_down++)
																							{
																								if(isset($sep_ids_m) && $sep_ids_m != null)
																								{
																									if($get_songs_pnew_w[$find_down]['id']==$sep_ids_m['id']){
																										$free_down_rel = "yes";
																									}
																									else{
																										if($free_down_rel =="" || $free_down_rel =="no"){
																											$free_down_rel = "no";
																										}
																									}
																								}
																							}
																							if($free_down_rel == "yes"){
																							 ?>
																							 <!--<a href="javascript:void(0);" onclick="download_directly_song('<?php echo $sep_ids_m['id'];?>','<?php echo str_replace("'","\'",$sep_ids_m['title']);?>','<?php echo str_replace("'","\'",$sep_ids_m['creator']); ?>','<?php echo str_replace("'","\'",$sep_ids_m['from']); ?>')"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>-->
																						 <?php
																							}
																							else if($sep_ids_m['sharing_preference']==2){
																								if($sep_ids_m['id']!=""){
																								?>
																								<!--<a href="javascript:void(0);" onclick="ask_to_donate('<?php echo str_replace("'","\'",$sep_ids_m['title']);?>','<?php echo str_replace("'","\'",$sep_ids_m['creator']);?>','<?php echo $sep_ids_m['id'];?>','download_song<?php echo $sep_ids_m['id'];?>')"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>-->
																							<?php
																								}else{
																								?>
																								<!--<a href="javascript:void(0);" onclick="ask_to_donate('<?php echo str_replace("'","\'",$sep_ids_m['title']);?>','<?php echo str_replace("'","\'",$sep_ids_m['creator']);?>','<?php echo $sep_ids_m['id'];?>','download_song<?php echo $sep_ids_m['id'];?>')"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>-->
																							<?php
																								}
																							}
																							else{
																						?>
																							<!--<a href="javascript:void(0);" onclick="down_song_pop(<?php echo $sep_ids_m['id'];?>)"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>-->
																						<?php
																							}
																						}else{
																						?>	
																						<!--<img src="../images/profile/download-over.gif" style="cursor:default"/>-->
																						<?php
																						}
																						?>
																						<!--<a href="addtocart.php?seller_id=<?php echo $sep_ids_m['general_user_id']; ?>&media_id=<?php echo $sep_ids_m['id'] ?>&buyer_id=<?php echo $_SESSION['gen_user_id'];?>&title=<?php echo str_replace("'","\'",$sep_ids_m['title']); ?>&image_name=<?php echo $song_image_name_cart;?>" id="download_song<?php echo $sep_ids_m['id'];?>" style="display:none;"></a>-->
																						<script type="text/javascript">
																						$(document).ready(function() {
																								$("#download_song<?php echo $sep_ids_m['id'];?>").fancybox({
																								'width'				: '75%',
																								'height'			: '75%',
																								'transitionIn'		: 'none',
																								'transitionOut'		: 'none',
																								'type'				: 'iframe'
																								});
																						});
																						</script>
																					</div>
																				</div>
																				<div class="sideR">
																					<div class="activityTitle"><?php
																					if($exp_meds[1]=='reg_med')
																					{
																						$sql_me = mysql_query("SELECT * FROM media_songs WHERE media_id='".$sep_ids_m['id']."'");
																						$ans_me = mysql_fetch_assoc($sql_me);
																						$ecehos = '<a style="text-decoration: none; cursor: text;" href="javascript:void(0)"> '.$ans_me['track'].' </a>';
																						//$ecehos .= '<a href="">'.$sep_ids_m['title'].'</a>';
																						//$ecehos .= "<a href=javascript:void(0) onclick = showPlayer('a',".$sep_ids_m['id'].",'add');>".$sep_ids_m['title']."</a>";
																						$ecehos .= "<a style='text-decoration: none; cursor: text;' href='javascript:void(0)'>".$sep_ids_m['title']."</a>";
																	
																	if($sep_ids_m['creator']!="")
																	{
																		$create_c = strpos($sep_ids_m['creator'],'(');
																		$end = $sep_ids_m['creator'];
																		$exp_cr = explode('|',$sep_ids_m['creator_info']);
																		
																		if($exp_cr[0]=='general_artist')
																		{
																			$sql_c_ar = mysql_query("SELECT * FROM general_artist WHERE artist_id='".$exp_cr[1]."'");
																		}
																		if($exp_cr[0]=='general_community')
																		{
																			$sql_c_ar = mysql_query("SELECT * FROM general_community WHERE community_id='".$exp_cr[1]."'");
																		}
																		if($exp_cr[0]=='artist_project' || $exp_cr[0]=='community_project' || $exp_cr[0]=='community_event' || $exp_cr[0]=='artist_event')
																		{
																			$sql_c_ar = mysql_query("SELECT * FROM $exp_cr[0] WHERE id='".$exp_cr[1]."'");
																		}
																		
																		
																		if(isset($sql_c_ar))
																		{
																			if($sql_c_ar!="" && $sql_c_ar!=NULL)
														{
																			if(mysql_num_rows($sql_c_ar)>0)
																			{
																				$ans_c_ar = mysql_fetch_assoc($sql_c_ar);
																				if(isset($ans_c_ar['name']))
																				{
																					$end = $ans_c_ar['name'];
																				}
																				else
																				{
																					$end = $ans_c_ar['title'];
																				}
																				if($ans_c_ar['profile_url']!="" && !empty($ans_c_ar['profile_url']))
																				{
																					$ecehos .= " By  <a href='/".$ans_c_ar['profile_url']."'><b>".$end."</b></a>  ";
																				}
																				else
																				{
																					$ecehos .= " By  <b>".$end."</b>  ";
																				}
																			}
																			else
																			{
																				$ecehos .= " By  <b>".$end."</b>  ";
																			}
																			}
																			else
																			{
																				$ecehos .= " By  <b>".$end."</b>  ";
																			}
																		}
																		else
																		{
																			
																			$ecehos .= " By  <b>".$end."</b>  ";
																		}
																	}
																	if($sep_ids_m['from']!="")
																	{
																		$create_f = strpos($sep_ids_m['from'],'(');
																		$end_f = substr($sep_ids_m['from'],0,$create_f);
																		
																		$exp_fr = explode('|',$sep_ids_m['from_info']);
																		if($exp_fr[0]=='general_artist')
																		{
																			$sql_f_ar = mysql_query("SELECT * FROM general_artist WHERE artist_id='".$exp_fr[1]."'");
																		}
																		if($exp_fr[0]=='general_community')
																		{
																			$sql_f_ar = mysql_query("SELECT * FROM general_community WHERE community_id='".$exp_fr[1]."'");
																		}
																		if($exp_fr[0]=='artist_project' || $exp_fr[0]=='community_project' || $exp_fr[0]=='community_event' || $exp_fr[0]=='artist_event')
																		{
																			$sql_f_ar = mysql_query("SELECT * FROM $exp_fr[0] WHERE id='".$exp_fr[1]."'");
																		}
																		
																		if(isset($sql_f_ar))
																		{
																			if($sql_f_ar!="" && $sql_f_ar!=NULL)
														{
																			if(mysql_num_rows($sql_f_ar)>0)
																			{
																				$ans_f_ar = mysql_fetch_assoc($sql_f_ar);
																				if(isset($ans_f_ar['name']))
																				{
																					$end_f = $ans_f_ar['name'];
																				}
																				else
																				{
																					$end_f = $ans_f_ar['title'];
																				}
																				if($ans_f_ar['profile_url']!="" && !empty($ans_f_ar['profile_url']))
																				{
																					$ecehos .= " From  <a href='/".$ans_f_ar['profile_url']."'><b>".$end_f."</b></a>";
																				}
																				else
																				{
																					$ecehos .= " From  <b>".$end_f."</b>";
																				}
																			}
																			else
																			{
																				$ecehos .= " From  <b>".$end_f."</b>";
																			}
																			}
																			else
																			{
																				$ecehos .= " From  <b>".$end_f."</b>";
																			}
																		}
																		else
																		{
																			$ecehos .= " From  <b>".$end_f."</b>";
																		}
																	}
																						echo $ecehos;
																					}
																					elseif($exp_meds[1]=='reg_art' || $exp_meds[1]=='reg_com')
																					{
																						if($ans_music_posts[$mpu_all]['profile_type']=='songs_post')
																						{
																							echo $sep_ids_m['audio_name'];
																						}
																					}
																					?>
																						<div style="color:#808080;font-size:12px;">
																							<div class="comment more">
																								<?php echo $ans_music_posts[$mpu_all]['description']; ?>
																							</div>
																						</div>
																					</div>
																					<!--<div class="activityContent">Tagged: <?php //echo $medias['tagged_users']; 
																						//$name_org = explode(')',$medias['tagged_users']);
																						//for($i_name_org=0;$i_name_org<count($name_org);$i_name_org++)
																						//{
																						//	$end_pos = strpos($name_org[$i_name_org],'(');
																						//	$org_name = substr($name_org[$i_name_org],0,$end_pos);
																					//		echo $org_name;
																					//	}
																					?></div>-->
																				</div>
																			</div>
																		</div>
																	</div>
																	<div class="rightSide">
																		<a href="#" onmouseover="mopen('mm<?php echo $mpu_all;?>11')" onmouseout="mclosetime()"><img src="images/profile/arrow.png" onmouseover="this.src='images/profile/arrow-down.png'" onmouseout="this.src='images/profile/arrow.png'" /></a>
																		<div id="mm<?php echo $mpu_all;?>11" class="popUp" onmouseover="mcancelclosetime()" onmouseout="mclosetime()">
																			<ul>
																				<li><a href="hideposts.php?id=<?php echo $ans_music_posts[$mpu_all]['id'];?>">Hide Feed</a></li>
																				<li><a href="hideposts.php?report_id=<?php echo $ans_all_posts[$po_all]['id'];?>">Report spam</a></li>
																				<li><div class="seperator"></div></li>
																				<li><!--<a href="Unsubscribe_feeds.php?sub=1&user_ids=<?php echo $gens['general_user_id']; ?>" onclick="return confirm_subscription_delete('<?php if($art==1){ echo $ros['name']; } elseif($com==1){ echo $ros_com['name']; } ?>')">Unsubscribe from <?php if($art==1)
															{
																echo $ros['name'];
															}
															elseif($com==1)
															{
																echo $ros_com['name'];
															} ?></a>-->
															<a href="profileedit.php#subscrriptions" >Unsubscribe from <?php if($art==1)
															{
																echo $ros['name'];
															}
															elseif($com==1)
															{
																echo $ros_com['name'];
															} ?></a>
															</li>
																			</ul>
																		</div>
																	</div>
																</div>
			<?php
																}
															}
														}
													}
													elseif($ans_music_posts[$mpu_all]['display_to']=='Friends')
													{
														if($gens!="")
														{
															$friends_c = $sql_feeds->friends_cond($ans_music_posts[$mpu_all]['general_user_id'],$_SESSION['login_id']);
															
															if($friends_c=='1')
															{
																$hide19 = 0;
																$get_gen_id = $sql_feeds->get_general_info();
																$hide_by = $ans_music_posts[$mpu_all]['hidden_by'];
																$exp_hide_by = explode(",",$hide_by);
																for($exp_count=0;$exp_count<=count($exp_hide_by);$exp_count++)
																{
																	if($exp_hide_by[$exp_count] ==$get_gen_id['general_user_id'])
																	{
																		$hide19 =1;
																	}
																}
																if($hide19 != 1)
																{
			?>
																<div class="storyCont">
																	<div class="leftSide">
																		<div class="avatar">
																			<?php
													$art = 0;
													
													if($gens['artist_id']!=0)
													{
														$sql_art = mysql_query("SELECT * FROM general_artist WHERE artist_id='".$gens['artist_id']."' AND status=0 AND active=0 AND del_status=0");
														if($sql_art!="" && $sql_art!=NULL)
														{
															if(mysql_num_rows($sql_art)>0)
															{
																$ros = mysql_fetch_assoc($sql_art);
																$art = 1;
															}
														}
													}
													
													$com = 0;
													if($gens['community_id']!=0)
													{
														$sql_com = mysql_query("SELECT * FROM general_community WHERE community_id='".$gens['community_id']."' AND status=0 AND active=0 AND del_status=0");
														if($sql_com!="" && $sql_com!=NULL)
														{
															if(mysql_num_rows($sql_com)>0)
															{
																$ros_com = mysql_fetch_assoc($sql_com);
																$com = 1;
															}
														}
													}
													
													if($art==1)
													{
														if($ros['profile_url']!="")
														{
													?>
														<a href="/<?php echo $ros['profile_url']; ?>">
													<?php
														}
														$get_news_general_user_id = $uk -> get_general_user_idby_artistid($ros['artist_id']);
													?>
														<img width="50" height="50" src="<?php
														if($ros['image_name']!="" && !empty($ros['image_name']))
														{
															echo $s3->getNewURI_otheruser("artjcropprofile",$get_news_general_user_id).$ros['image_name']; 
														}
														else
														{
															echo "http://artjcropprofile.s3.amazonaws.com/Noimage.png";
														}
													?>" />
													<?php
														if($ros['profile_url']!="")
														{
													?>
														</a>
													<?php
														}
													}
													elseif($com==1)
													{
														if($ros_com['profile_url']!="")
														{
													?>
														<a href="/<?php echo $ros_com['profile_url']; ?>">
													<?php
														}
														$get_news_general_user_id = $uk -> get_general_user_idby_communityid($ros_com['community_id']);
													?>
														<img width="50" height="50" src="<?php
													
													
													if($ros_com['image_name']!="" && !empty($ros_com['image_name']))
													{
														echo $s3->getNewURI_otheruser("comjcropprofile",$get_news_general_user_id).$ros_com['image_name']; 
													}
													else
													{
														echo "http://comjcropprofile.s3.amazonaws.com/Noimage.png";
													}
													
													?>" />
													<?php
														if($ros_com['profile_url']!="")
														{
													?>
														</a>
													<?php
														}
													}
													?>
																	</div>
																	<?php //echo "table=".$reg_table_media_type;
																	//echo $ans_audio_feeds[$audio_fed_lo]['id'];
																	?>
																		<div class="storyContainer">
																			<div class="storyTitle">
																				<span class="userName"><?php if($art==1)
															{
																if($ros['profile_url']!="")
														{
													?>
														<a href="/<?php echo $ros['profile_url']; ?>">
													<?php
														}
																echo $ros['name'];
																if($ros['profile_url']!="")
														{
													?>
														</a>
													<?php
														}
															}
															elseif($com==1)
															{
																if($ros_com['profile_url']!="")
														{
													?>
														<a href="/<?php echo $ros_com['profile_url']; ?>">
													<?php
														}
																echo $ros_com['name'];
																if($ros_com['profile_url']!="")
														{
													?>
														</a>
													<?php
														}
															} ?></span>  <?php //echo $ans_music_posts[$mpu_all]['description']; ?>
																			</div>
																			<div class="activityCont">
																				<div class="sideL">
																					<div class="storyImage"></div>
																					<div class="player">
																						<img src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" onclick="showPlayer('a','<?php echo $sep_ids_m['id']; ?>','play');">
																						<img src="images/profile/add.gif" onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'" onclick="showPlayer('a','<?php echo $sep_ids_m['id']; ?>','add');">
																						<?php
																						if($sep_ids_m['sharing_preference']==5 || $sep_ids_m['sharing_preference']==2)
																						{
																							$song_image_name_cart = $sql_feeds->get_audio_img($sep_ids_m['id']);
																							$free_down_rel ="";
																							for($find_down=0;$find_down<=count($get_songs_pnew_w);$find_down++)
																							{
																								if(isset($sep_ids_m) && $sep_ids_m != null)
																								{
																									if($get_songs_pnew_w[$find_down]['id']==$sep_ids_m['id']){
																										$free_down_rel = "yes";
																									}
																									else{
																										if($free_down_rel =="" || $free_down_rel =="no"){
																											$free_down_rel = "no";
																										}
																									}
																								}
																							}
																							if($free_down_rel == "yes"){
																							 ?>
																							 <!--<a href="javascript:void(0);" onclick="download_directly_song('<?php echo $sep_ids_m['id'];?>','<?php echo str_replace("'","\'",$sep_ids_m['title']);?>','<?php echo str_replace("'","\'",$sep_ids_m['creator']); ?>','<?php echo str_replace("'","\'",$sep_ids_m['from']); ?>')"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>-->
																						 <?php
																							}
																							else if($sep_ids_m['sharing_preference']==2){
																								if($sep_ids_m['id']!=""){
																								?>
																								<!--<a href="javascript:void(0);" onclick="ask_to_donate('<?php echo str_replace("'","\'",$sep_ids_m['title']);?>','<?php echo str_replace("'","\'",$sep_ids_m['creator']);?>','<?php echo $sep_ids_m['id'];?>','download_song<?php echo $sep_ids_m['id'];?>')"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>-->
																							<?php
																								}else{
																								?>
																								<!--<a href="javascript:void(0);" onclick="ask_to_donate('<?php echo str_replace("'","\'",$sep_ids_m['title']);?>','<?php echo str_replace("'","\'",$sep_ids_m['creator']);?>','<?php echo $sep_ids_m['id'];?>','download_song<?php echo $sep_ids_m['id'];?>')"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>-->
																							<?php
																								}
																							}
																							else{
																						?>
																							<!--<a href="javascript:void(0);" onclick="down_song_pop(<?php echo $sep_ids_m['id'];?>)"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>-->
																						<?php
																							}
																						}else{
																						?>	
																						<img src="../images/profile/download-over.gif" style="cursor:default"/>
																						<?php
																						}
																						?>
																						<!--<a href="addtocart.php?seller_id=<?php echo $sep_ids_m['general_user_id']; ?>&media_id=<?php echo $sep_ids_m['id'] ?>&buyer_id=<?php echo $_SESSION['gen_user_id'];?>&title=<?php echo str_replace("'","\'",$sep_ids_m['title']); ?>&image_name=<?php echo $song_image_name_cart;?>" id="download_song<?php echo $sep_ids_m['id'];?>" style="display:none;"></a>-->
																						<script type="text/javascript">
																						$(document).ready(function() {
																								$("#download_song<?php echo $sep_ids_m['id'];?>").fancybox({
																								'width'				: '75%',
																								'height'			: '75%',
																								'transitionIn'		: 'none',
																								'transitionOut'		: 'none',
																								'type'				: 'iframe'
																								});
																						});
																						</script>
																					</div>
																				</div>
																				<div class="sideR">
																					<div class="activityTitle"><?php
																					if($exp_meds[1]=='reg_med')
																					{
																						$sql_me = mysql_query("SELECT * FROM media_songs WHERE media_id='".$sep_ids_m['id']."'");
																						$ans_me = mysql_fetch_assoc($sql_me);
																						$ecehos = '<a style="text-decoration: none; cursor: text;" href="javascript:void(0)"> '.$ans_me['track'].' </a>';
																						//$ecehos .= "<a href=javascript:void(0) onclick = showPlayer('a',".$sep_ids_m['id'].",'add');>".$sep_ids_m['title']."</a>";
																						$ecehos .= "<a style='text-decoration: none; cursor: text;' href='javascript:void(0)'>".$sep_ids_m['title']."</a>";
																						//$ecehos .= '<a href="">'.$sep_ids_m['title'].'</a>';
																	
																	if($sep_ids_m['creator']!="")
																	{
																		$create_c = strpos($sep_ids_m['creator'],'(');
																		$end = substr($sep_ids_m['creator'],0,$create_c);
																		$exp_cr = explode('|',$sep_ids_m['creator_info']);
																		
																		if($exp_cr[0]=='general_artist')
																		{
																			$sql_c_ar = mysql_query("SELECT * FROM general_artist WHERE artist_id='".$exp_cr[1]."'");
																		}
																		if($exp_cr[0]=='general_community')
																		{
																			$sql_c_ar = mysql_query("SELECT * FROM general_community WHERE community_id='".$exp_cr[1]."'");
																		}
																		if($exp_cr[0]=='artist_project' || $exp_cr[0]=='community_project' || $exp_cr[0]=='community_event' || $exp_cr[0]=='artist_event')
																		{
																			$sql_c_ar = mysql_query("SELECT * FROM $exp_cr[0] WHERE id='".$exp_cr[1]."'");
																		}
																		
																		
																		if(isset($sql_c_ar))
																		{
																			if($sql_c_ar!="" && $sql_c_ar!=NULL)
														{
																			if(mysql_num_rows($sql_c_ar)>0)
																			{
																				$ans_c_ar = mysql_fetch_assoc($sql_c_ar);
																				if(isset($ans_c_ar['name']))
																				{
																					$end = $ans_c_ar['name'];
																				}
																				else
																				{
																					$end = $ans_c_ar['title'];
																				}
																				if($ans_c_ar['profile_url']!="" && !empty($ans_c_ar['profile_url']))
																				{
																					$ecehos .= " By  <a href='/".$ans_c_ar['profile_url']."'><b>".$end."</b></a>  ";
																				}
																				else
																				{
																					$ecehos .= " By  <b>".$end."</b>  ";
																				}
																			}
																			else
																			{
																				$ecehos .= " By  <b>".$end."</b>  ";
																			}
																			}
																			else
																			{
																				$ecehos .= " By  <b>".$end."</b>  ";
																			}
																		}
																		else
																		{
																			
																			$ecehos .= " By  <b>".$end."</b>  ";
																		}
																	}
																	if($sep_ids_m['from']!="")
																	{
																		$create_f = strpos($sep_ids_m['from'],'(');
																		$end_f = substr($sep_ids_m['from'],0,$create_f);
																		
																		$exp_fr = explode('|',$sep_ids_m['from_info']);
																		if($exp_fr[0]=='general_artist')
																		{
																			$sql_f_ar = mysql_query("SELECT * FROM general_artist WHERE artist_id='".$exp_fr[1]."'");
																		}
																		if($exp_fr[0]=='general_community')
																		{
																			$sql_f_ar = mysql_query("SELECT * FROM general_community WHERE community_id='".$exp_fr[1]."'");
																		}
																		if($exp_fr[0]=='artist_project' || $exp_fr[0]=='community_project' || $exp_fr[0]=='community_event' || $exp_fr[0]=='artist_event')
																		{
																			$sql_f_ar = mysql_query("SELECT * FROM $exp_fr[0] WHERE id='".$exp_fr[1]."'");
																		}
																		
																		if(isset($sql_f_ar))
																		{
																			if($sql_f_ar!="" && $sql_f_ar!=NULL)
														{
																			if(mysql_num_rows($sql_f_ar)>0)
																			{
																				$ans_f_ar = mysql_fetch_assoc($sql_f_ar);
																				if(isset($ans_f_ar['name']))
																				{
																					$end_f = $ans_f_ar['name'];
																				}
																				else
																				{
																					$end_f = $ans_f_ar['title'];
																				}
																				if($ans_f_ar['profile_url']!="" && !empty($ans_f_ar['profile_url']))
																				{
																					$ecehos .= " From  <a href='/".$ans_f_ar['profile_url']."'><b>".$end_f."</b></a>";
																				}
																				else
																				{
																					$ecehos .= " From  <b>".$end_f."</b>";
																				}
																			}
																			else
																			{
																				$ecehos .= " From  <b>".$end_f."</b>";
																			}
																			}
																			else
																			{
																				$ecehos .= " From  <b>".$end_f."</b>";
																			}
																		}
																		else
																		{
																			$ecehos .= " From  <b>".$end_f."</b>";
																		}
																	}
																						echo $ecehos;
																					}
																					elseif($exp_meds[1]=='reg_art' || $exp_meds[1]=='reg_com')
																					{
																						if($ans_music_posts[$mpu_all]['profile_type']=='songs_post')
																						{
																							echo $sep_ids_m['audio_name'];
																						}
																					}
																					?>
																						<div style="color:#808080;font-size:12px;">
																							<div class="comment more">
																								<?php echo $ans_music_posts[$mpu_all]['description']; ?>
																							</div>
																						</div>
																					</div>
																					<!--<div class="activityContent">Tagged: <?php //echo $medias['tagged_users']; 
																						//$name_org = explode(')',$medias['tagged_users']);
																						//for($i_name_org=0;$i_name_org<count($name_org);$i_name_org++)
																						//{
																						//	$end_pos = strpos($name_org[$i_name_org],'(');
																						//	$org_name = substr($name_org[$i_name_org],0,$end_pos);
																					//		echo $org_name;
																					//	}
																					?></div>-->
																				</div>
																			</div>
																		</div>
																	</div>
																	<div class="rightSide">
																		<a href="#" onmouseover="mopen('mm<?php echo $mpu_all;?>12')" onmouseout="mclosetime()"><img src="images/profile/arrow.png" onmouseover="this.src='images/profile/arrow-down.png'" onmouseout="this.src='images/profile/arrow.png'" /></a>
																		<div id="mm<?php echo $mpu_all;?>12" class="popUp" onmouseover="mcancelclosetime()" onmouseout="mclosetime()">
																			<ul>
																				<li><a href="hideposts.php?id=<?php echo $ans_music_posts[$mpu_all]['id'];?>">Hide Feed</a></li>
																				<li><a href="hideposts.php?report_id=<?php echo $ans_all_posts[$po_all]['id'];?>">Report spam</a></li>
																				<li><div class="seperator"></div></li>
																				<li><!--<a href="Unsubscribe_feeds.php?frd=1&user_ids=<?php echo $gens['general_user_id']; ?>" onclick="return confirm_subscription_delete('<?php if($art==1){ echo $ros['name']; } elseif($com==1){ echo $ros_com['name']; } ?>')">Unsubscribe from <?php if($art==1)
															{
																echo $ros['name'];
															}
															elseif($com==1)
															{
																echo $ros_com['name'];
															} ?></a>-->
															<a href="profileedit.php#subscrriptions" >Unsubscribe from <?php if($art==1)
															{
																echo $ros['name'];
															}
															elseif($com==1)
															{
																echo $ros_com['name'];
															} ?></a>
															</li>
																			</ul>
																		</div>
																	</div>
																</div>
			<?php
																}
															}
														}
													}
												}
											}
										}
									}
							?>
							</div>
							<div id="video_feeds" style="display:none;">
							<?php
									include_once("findsale_video.php");
									$ans_video_feeds = array();
									$ans_sub_video_feeds = array();
									$fan_ans_video_feeds = array();
									$vi_pro_eve_id = array();
									$vi_art_com_id = array();
									$vi_arts_ids = array();
									$vi_coms_ids = array();
									$ans_videos_posts = array();
									
									$sql_video_feeds = $sql_feeds->all_feeds($chk_sql['email']);
									$run_video_feeds = mysql_query($sql_video_feeds);
									
									$fan_sql_video_feeds = $sql_feeds->fan_club($chk_sql['email']);
									$fan_run_video_feeds = mysql_query($fan_sql_video_feeds);
									if($run_video_feeds!="" && $run_video_feeds!=NULL)
									{
										while($row_video_feeds = mysql_fetch_assoc($run_video_feeds))
										{
											$ans_sub_video_feeds[] = $row_video_feeds;
										}
									}
									
									if($fan_run_video_feeds!="" && $fan_run_video_feeds!=NULL)
									{
										while($fan_row_video_feeds = mysql_fetch_assoc($fan_run_video_feeds))
										{
											$fan_ans_video_feeds[] = $fan_row_video_feeds;
										}
									}
									
									$ans_video_feeds = array_merge($ans_sub_video_feeds,$fan_ans_video_feeds);
									
									$ans_frds_posts = array();
									$frds_posts_all_l = $sql_feeds->all_frds_posts($_SESSION['login_id']);
									$frds_posts_all = mysql_query($frds_posts_all_l);
									if($frds_posts_all!="" && $frds_posts_all!=NULL)
									{
										if(mysql_num_rows($frds_posts_all)>0)
										{
											while($row_f = mysql_fetch_assoc($frds_posts_all))
											{
												$ans_frds_posts[] = $row_f;
											}
										}
									}
									
									if(($ans_video_feeds!=NULL && !empty($ans_video_feeds)) || (!empty($ans_frds_posts) && $ans_frds_posts!=""))
									{
										for($vi_fed_lo=0;$vi_fed_lo<count($ans_video_feeds);$vi_fed_lo++)
										{
											if($ans_video_feeds[$vi_fed_lo]['related_type']!='artist' && $ans_video_feeds[$vi_fed_lo]['related_type']!='community')
											{
												$num_ids = explode('_',$ans_video_feeds[$vi_fed_lo]['related_id']);
												if($ans_video_feeds[$vi_fed_lo]['related_type']=='artist_project' || $ans_video_feeds[$vi_fed_lo]['related_type']=='artist_event')
												{
													$vi_pro_eve_id[$vi_fed_lo]['related_id_art'] = $num_ids[0];
												}
												elseif($ans_video_feeds[$vi_fed_lo]['related_type']=='community_project' || $ans_video_feeds[$vi_fed_lo]['related_type']=='community_event')
												{
													$vi_art_com_id[$vi_fed_lo]['related_id_com'] = $num_ids[0];
												}
											}
											elseif($ans_video_feeds[$vi_fed_lo]['related_type']=='artist' || $ans_video_feeds[$vi_fed_lo]['related_type']=='community')
											{
												if($ans_video_feeds[$vi_fed_lo]['related_type']=='artist')
												{
													$vi_pro_eve_id[$vi_fed_lo]['related_id_art'] = $ans_video_feeds[$vi_fed_lo]['related_id'];
												}
												elseif($ans_video_feeds[$vi_fed_lo]['related_type']=='community')
												{
													$vi_art_com_id[$vi_fed_lo]['related_id_com'] = $ans_video_feeds[$vi_fed_lo]['related_id'];
												}
											}
										}
										
										if($vi_pro_eve_id!=0 && count($vi_pro_eve_id)>0)
										{
											for($arts=0;$arts<count($vi_pro_eve_id);$arts++)
											{
												$org_art[] = $vi_pro_eve_id[$arts]['related_id_art'];
											}
											
											$new_art_vi = array();
											$vi = 0;
											foreach ($org_art as $key => $value)
											{
												if(isset($new_art_vi[$value]))
												{
													$new_art_vi[$value] += 1;
												}
												else
												{
													$new_art_vi[$value] = 1;
													
													$sqls = $sql_feeds->arts_gen($value);
													
													if($sqls!="")
													{
														$vi_arts_ids[$vi] = $sqls;
													}
												}
												$vi = $vi + 1;
											}
										}
										
										if($vi_art_com_id!="" && count($vi_art_com_id)>0)
										{
											for($coms=0;$coms<count($vi_art_com_id);$coms++)
											{
												$org_com[] = $vi_art_com_id[$coms]['related_id_com'];
											}
											
											$new_com_vi = array();
											$vih_c = 0;
											foreach ($org_com as $key => $value)
											{
												if(isset($new_com_vi[$value]))
												{
													$new_com_vi[$value] += 1;
												}
												else
												{
													$new_com_vi[$value] = 1;
													
													$sqls_c = $sql_feeds->coms_gen($value);
													if($sqls_c!="")
													{
														$vi_coms_ids[$vih_c] = $sqls_c;
													}
												}
												$vih_c = $vih_c + 1;
											}
										}
										
										$ans_frds_posts = array();
										$frds_posts_all_l = $sql_feeds->all_frds_posts($_SESSION['login_id']);
										$frds_posts_all = mysql_query($frds_posts_all_l);
										if($frds_posts_all!="" && $frds_posts_all!=NULL)
										{
											if(mysql_num_rows($frds_posts_all)>0)
											{
												while($row_f = mysql_fetch_assoc($frds_posts_all))
												{
													$ans_frds_posts[] = $row_f;
												}
											}
										}
										
										$doub_vi_chk = array_merge($vi_arts_ids,$vi_coms_ids,$ans_frds_posts);
										
										for($cvm_d=0;$cvm_d<count($doub_vi_chk);$cvm_d++)
										{
											$rel_d_id[] = $doub_vi_chk[$cvm_d]['general_user_id'];
										}
										
										$new_array_rep_vi = array();
										foreach ($rel_d_id as $key => $value)
										{
											if(isset($new_array_rep_vi[$value]))
												$new_array_rep_vi[$value] += 1;
											else
												$new_array_rep_vi[$value] = 1;
										}
										
										foreach ($new_array_rep_vi as $uid => $n)
										{
											$ex_uid_vi = $ex_uid_vi.','.$uid;
										}
										$ex_tr_vi = trim($ex_uid_vi, ",");
										$sep_ids_vv = explode(',',$ex_tr_vi);
										
										if($sep_ids_vv!="" && count($sep_ids_vv)>0)
										{
											for($se_ca=0;$se_ca<count($sep_ids_vv);$se_ca++)
											{
												$types = 'videos_post';
												$ids = $sql_feeds->pmva_posts_check($sep_ids_vv[$se_ca],$_SESSION['login_id'],$types);
												
												//die;
												if($ids!="" && count($ids)>0)
												{
													$posts_video = $sql_feeds->pmv_posts_check($sep_ids_vv[$se_ca],$_SESSION['login_id'],$types);
													if($posts_video!="" && $posts_video!=NULL)
													{
														if($posts_video!="" && mysql_num_rows($posts_video)>0)
														{
															while($rows_posts_all = mysql_fetch_assoc($posts_video))
															{
																$ans_videos_posts[] = $rows_posts_all;
															}
														}
													}
												}
											}
										}
										
										if($ans_videos_posts!="" && count($ans_videos_posts)>0)
										{
											for($vpi_all=0;$vpi_all<count($ans_videos_posts);$vpi_all++)
											{
												if($ans_videos_posts[$vpi_all]['profile_type']=='videos_post')
												{
													$exp_meds = explode('~',$ans_videos_posts[$vpi_all]['media']);
													if($exp_meds[1]=='reg_med')
													{
														$sep_ids_v = $sql_feeds->get_mediaSelected($exp_meds[0]);
													}
													elseif(($exp_meds[1]=='reg_art' || $exp_meds[1]=='reg_com') && $ans_videos_posts[$vpi_all]['profile_type']=='videos_post')
													{
														if($exp_meds[1]=='reg_art')
														{
															$reg_table_type = 'general_artist_video';
														}
														elseif($exp_meds[1]=='reg_com')
														{
															$reg_table_type = 'general_community_video';
														}
														
														$sep_ids_v = $sql_feeds->get_MediaReg($reg_table_type,$exp_meds[0]);
													}
												}
												
												if(isset($sep_ids_v) && $sep_ids_v!='0' && $sep_ids_v!=NULL)
												{
													$gens = $sql_feeds->get_log_ids($ans_videos_posts[$vpi_all]['general_user_id']);
													if($ans_videos_posts[$vpi_all]['display_to']=='All' && $ans_videos_posts[$vpi_all]['profile_type']=='videos_post')
													{
														$hide20 = 0;
														$get_gen_id = $sql_feeds->get_general_info();
														$hide_by = $ans_videos_posts[$vpi_all]['hidden_by'];
														$exp_hide_by = explode(",",$hide_by);
														for($exp_count=0;$exp_count<=count($exp_hide_by);$exp_count++)
														{
															if($exp_hide_by[$exp_count] ==$get_gen_id['general_user_id'])
															{
																$hide20 =1;
															}
														}
														if($hide20 != 1)
														{
			?>
														<div class="storyCont">
																	<div class="leftSide">
																		<div class="avatar">
																			<?php
													$art = 0;
													
													if($gens['artist_id']!=0)
													{
														$sql_art = mysql_query("SELECT * FROM general_artist WHERE artist_id='".$gens['artist_id']."' AND status=0 AND active=0 AND del_status=0");
														if($sql_art!="" && $sql_art!=NULL)
														{
															if(mysql_num_rows($sql_art)>0)
															{
																$ros = mysql_fetch_assoc($sql_art);
																$art = 1;
															}
														}
													}
													
													$com = 0;
													if($gens['community_id']!=0)
													{
														$sql_com = mysql_query("SELECT * FROM general_community WHERE community_id='".$gens['community_id']."' AND status=0 AND active=0 AND del_status=0");
														if($sql_com!="" && $sql_com!=NULL)
														{
															if(mysql_num_rows($sql_com)>0)
															{
																$ros_com = mysql_fetch_assoc($sql_com);
																$com = 1;
															}
														}
													}
													
													if($art==1)
													{
														if($ros['profile_url']!="")
														{
													?>
														<a href="/<?php echo $ros['profile_url']; ?>">
													<?php
														}
														$get_news_general_user_id = $uk -> get_general_user_idby_artistid($ros['artist_id']);
													?>
														<img width="50" height="50" src="<?php
														if($ros['image_name']!="" && !empty($ros['image_name']))
														{
															echo $s3->getNewURI_otheruser("artjcropprofile",$get_news_general_user_id).$ros['image_name']; 
														}
														else
														{
															echo "http://artjcropprofile.s3.amazonaws.com/Noimage.png";
														}
													?>" />
													<?php
														if($ros['profile_url']!="")
														{
													?>
														</a>
													<?php
														}
													}
													elseif($com==1)
													{
														if($ros_com['profile_url']!="")
														{
													?>
														<a href="/<?php echo $ros_com['profile_url']; ?>">
													<?php
														}
														$get_news_general_user_id = $uk -> get_general_user_idby_communityid($ros_com['community_id']);
													?>
														<img width="50" height="50" src="<?php
													
													
													if($ros_com['image_name']!="" && !empty($ros_com['image_name']))
													{
														echo $s3->getNewURI_otheruser("comjcropprofile",$get_news_general_user_id).$ros_com['image_name']; 
													}
													else
													{
														echo "http://comjcropprofile.s3.amazonaws.com/Noimage.png";
													}
													
													?>" />
													<?php
														if($ros_com['profile_url']!="")
														{
													?>
														</a>
													<?php
														}
													}
													?>
																	</div>
																	<?php //echo "table=".$reg_table_media_type;
																	//echo $ans_audio_feeds[$audio_fed_lo]['id'];
																	?>
																		<div class="storyContainer">
																			<div class="storyTitle">
																				<span class="userName"><?php if($art==1)
															{
																if($ros['profile_url']!="")
														{
													?>
														<a href="/<?php echo $ros['profile_url']; ?>">
													<?php
														}
																echo $ros['name'];
																if($ros['profile_url']!="")
														{
													?>
														</a>
													<?php
														}
															}
															elseif($com==1)
															{
																if($ros_com['profile_url']!="")
														{
													?>
														<a href="/<?php echo $ros_com['profile_url']; ?>">
													<?php
														}
																echo $ros_com['name'];
																if($ros_com['profile_url']!="")
														{
													?>
														</a>
													<?php
														}
															} ?></span> <?php// echo $ans_videos_posts[$vpi_all]['description']; ?>
																			</div>
																			<div class="activityCont">
																				<div class="sideL">
																					<div class="storyImage" style="position:relative;"><?php
																					
																					if($sep_ids_v['media_type']==115 || $ans_all_posts[$po_all]['profile_type']=='videos_post')
																		{
																			if($sep_ids_v['media_type']==115)
																			{
																				$tables = 'media_video';
																				$get_video_image = $sql_feeds->getvideoimage($tables,$sep_ids_v['id']);
																			}
																			elseif($ans_all_posts[$po_all]['profile_type']=='videos_post' && $exp_meds[1]=='reg_art')
																			{
																				$tables = 'general_artist_video';
																				$get_video_image = $sql_feeds->getvideoimage($tables,$sep_ids_v['id']);
																			}
																			elseif($ans_all_posts[$po_all]['profile_type']=='videos_post' && $exp_meds[1]=='reg_com')
																			{
																				$tables = 'general_community_video';
																				$get_video_image = $sql_feeds->getvideoimage($tables,$sep_ids_v['id']);
																			}
																			if($get_video_image[1]=='youtube')
																			{
																				$vid_pros_edit_img = "http://img.youtube.com/vi/".$get_video_image[0]."/default.jpg";
																			}
																			else
																			{
																				$video_vim = $vimeo->call('vimeo.videos.getInfo', array('video_id' => $get_video_image[0]));
																				$vid_pros_edit_img = $video_vim->video[0]->thumbnails->thumbnail[1]->_content;
																			}
																	?>
																		<a onmouseout="newfunctionout('<?php echo $sep_ids_v['id']; ?>','v')" onmouseover="newfunction('<?php echo $sep_ids_v['id']; ?>','v')" href="javascript:void(0);" onclick="showPlayer('v','<?php echo $sep_ids_v['id'];?>','playlist');">
																			<img src="<?php echo $vid_pros_edit_img; ?>" width="90" height="90" />
																		</a>
																	<?php
																		}
																	?><div class="navButton">
			<div class="playMedia">
				<a href="javascript:void(0);" class="list_play"><img title="Add file to the bottom of your playlist without stopping current media." id="play_video_image<?php echo $sep_ids_v['id']; ?>v" onclick="showPlayer('v','<?php echo $sep_ids_v['id'];?>','playlist');" src="/images/play.png" onmouseout="newfunctionout('<?php echo $sep_ids_v['id']; ?>','v')" onmouseover="newfunction('<?php echo $sep_ids_v['id']; ?>','v')" style="border-radius: 6px;"></a>
			</div>
		</div></div>
																	
																					<div class="player">
																						<img src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" onclick="showPlayer('v','<?php echo $sep_ids_v['id'];?>','playlist');">
																						<img src="images/profile/add.gif" onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'" onclick="showPlayer('v','<?php echo $sep_ids_v['id'];?>','addvi');">
																						<?php
																						if($sep_ids_v['sharing_preference'] == 5 || $sep_ids_v['sharing_preference'] == 2)
																						{
																							$get_video_image = $sql_feeds->getvideoimage_feature($sep_ids_v['id']);
																							//$image_name_cart = "https://img.youtube.com/vi/".$get_video_image."/default.jpg";
																							if($get_video_image[1]=='youtube')
																			{
																				$image_name_cart = "http://img.youtube.com/vi/".$get_video_image[0]."/default.jpg";
																			}
																			else
																			{
																				$video_vim = $vimeo->call('vimeo.videos.getInfo', array('video_id' => $get_video_image[0]));
																				$image_name_cart = $video_vim->video[0]->thumbnails->thumbnail[1]->_content;
																			}
																							$free_down_rel_video="";
																							for($find_down=0;$find_down<=count($get_vids_pnew_w);$find_down++)
																							{
																								if(isset($sep_ids_v) && $sep_ids_v != null){
																									if($get_vids_pnew_w[$find_down]['id']==$sep_ids_v['id']){
																										$free_down_rel_video = "yes";
																									}
																									else{
																										if($free_down_rel_video =="" || $free_down_rel_video =="no"){
																											$free_down_rel_video = "no";
																										}
																									}
																								}
																							}
																							if($free_down_rel_video == "yes"){
																							 ?>
																								<!--<a href="javascript:void(0);" onclick="download_directly_song('<?php echo $sep_ids_v['id']; ?>','<?php echo str_replace("'","\'",$sep_ids_v['title']); ?>','<?php echo str_replace("'","\'",$sep_ids_v['creator']); ?>','<?php echo str_replace("'","\'",$sep_ids_v['from']); ?>')"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>-->
																						 <?php
																							}
																							else if($sep_ids_v['sharing_preference']==2){
																								if($sep_ids_v['id']!=""){
																								?>
																								<!--<a href="javascript:void(0);" onclick="ask_to_donate('<?php echo str_replace("'","\'",$sep_ids_v['title']);?>','<?php echo str_replace("'","\'",$sep_ids_v['creator']);?>','<?php echo $sep_ids_v['id'];?>','download_song<?php echo $sep_ids_v['id'];?>')"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>-->
																							<?php
																								}else{
																								?>
																								<!--<a href="javascript:void(0);" onclick="ask_to_donate('<?php echo str_replace("'","\'",$sep_ids_v['title']);?>','<?php echo str_replace("'","\'",$sep_ids_v['creator']);?>','<?php echo $sep_ids_v['id'];?>','download_song<?php echo $sep_ids_v['id'];?>')"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>-->
																							<?php
																								}
																							}
																							else{
																						?>
																							<!--<a href="javascript:void(0);" onclick="down_video_pop(<?php echo $sep_ids_v['id'];?>)"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>-->
																						<?php
																							}
																						}else{
																						?>
																							<!--<img src="../images/profile/download-over.gif" style="cursor:default"/>-->
																						<?php
																						}
																						?>
																						<!--<a href="addtocart.php?seller_id=<?php echo $sep_ids_v['general_user_id']; ?>&media_id=<?php echo $sep_ids_v['id']; ?>&buyer_id=<?php echo $_SESSION['gen_user_id'];?>&title=<?php echo str_replace("'","\'",$sep_ids_v['title']);?>&image_name=<?php echo $image_name_cart;?>" id="download_video<?php echo $sep_ids_v['id'];?>" style="display:none;"></a>-->
																						<script type="text/javascript">
																						$(document).ready(function() {
																								$("#download_video<?php echo $sep_ids_v['id'];?>").fancybox({
																								'height'			: '75%',
																								'width'				: '69%',
																								'transitionIn'		: 'none',
																								'transitionOut'		: 'none',
																								'type'				: 'iframe'
																								});
																						});
																						</script>
																					</div>
																				</div>
																				<div class="sideR">
																					<div class="activityTitle"><a href=""><?php
																					if($exp_meds[1]=='reg_med')
																					{
																						?>
																						<a href="javascript:void(0)" onclick = "showPlayer('v',<?php echo $sep_ids_v['id'];?>,'addvi');"><?php echo $sep_ids_v['title'];?></a>
																						<?php
																					}
																					elseif($exp_meds[1]=='reg_art' || $exp_meds[1]=='reg_com')
																					{
																						if($ans_videos_posts[$vpi_all]['profile_type']=='videos_post')
																						{
																							?>
																							<a href="javascript:void(0)" onclick = "showPlayer('v',<?php echo $sep_ids_v['id'];?>,'addvi');"><?php echo $sep_ids_v['video_name'];?></a>
																							<?php
																						}
																					}
																					?></a>
																						<div style="color:#808080;font-size:12px;">
																							<div class="comment more">
																								<?php echo $ans_videos_posts[$vpi_all]['description']; ?>
																							</div>
																						</div>
																					</div>
																					<!--<div class="activityContent">Tagged: <?php //echo $medias['tagged_users']; 
																						//$name_org = explode(')',$medias['tagged_users']);
																						//for($i_name_org=0;$i_name_org<count($name_org);$i_name_org++)
																						//{
																						//	$end_pos = strpos($name_org[$i_name_org],'(');
																						//	$org_name = substr($name_org[$i_name_org],0,$end_pos);
																					//		echo $org_name;
																					//	}
																					?></div>-->
																				</div>
																			</div>
																		</div>
																	</div>
																	<div class="rightSide">
																		<a href="#" onmouseover="mopen('mm<?php echo $vpi_all;?>13')" onmouseout="mclosetime()"><img src="images/profile/arrow.png" onmouseover="this.src='images/profile/arrow-down.png'" onmouseout="this.src='images/profile/arrow.png'" /></a>
																		<div id="mm<?php echo $vpi_all;?>13" class="popUp" onmouseover="mcancelclosetime()" onmouseout="mclosetime()">
																			<ul>
																				<li><a href="hideposts.php?id=<?php echo $ans_videos_posts[$vpi_all]['id'];?>">Hide Feed</a></li>
																				<li><a href="hideposts.php?report_id=<?php echo $ans_all_posts[$po_all]['id'];?>">Report spam</a></li>
																				<li><div class="seperator"></div></li>
																				<li><!--<a href="Unsubscribe_feeds.php?all=1&user_ids=<?php echo $gens['general_user_id']; ?>" onclick="return confirm_subscription_delete('<?php if($art==1){ echo $ros['name']; } elseif($com==1){ echo $ros_com['name']; } ?>')">Unsubscribe from <?php if($art==1)
															{
																echo $ros['name'];
															}
															elseif($com==1)
															{
																echo $ros_com['name'];
															} ?></a>-->
															<a href="profileedit.php#subscrriptions" >Unsubscribe from <?php if($art==1)
															{
																echo $ros['name'];
															}
															elseif($com==1)
															{
																echo $ros_com['name'];
															} ?></a>
															</li>
																			</ul>
																		</div>
																	</div>
																</div>
			<?php
														}
													}
													elseif($ans_videos_posts[$vpi_all]['display_to']=='Fans')
													{
														if($gens!="")
														{
															$fans_all =	$sql_feeds->fans_cond($chk_sql['email'],$gens['community_id'],$gens['artist_id']);
															if($fans_all=='1')
															{
																$hide21 = 0;
																$get_gen_id = $sql_feeds->get_general_info();
																$hide_by = $ans_videos_posts[$vpi_all]['hidden_by'];
																$exp_hide_by = explode(",",$hide_by);
																for($exp_count=0;$exp_count<=count($exp_hide_by);$exp_count++)
																{
																	if($exp_hide_by[$exp_count] ==$get_gen_id['general_user_id'])
																	{
																		$hide21 =1;
																	}
																}
																if($hide21 != 1)
																{
			?>
																<div class="storyCont">
																	<div class="leftSide">
																		<div class="avatar">
																			<?php
													$art = 0;
													
													if($gens['artist_id']!=0)
													{
														$sql_art = mysql_query("SELECT * FROM general_artist WHERE artist_id='".$gens['artist_id']."' AND status=0 AND active=0 AND del_status=0");
														if(mysql_num_rows($sql_art)>0)
														{
															$ros = mysql_fetch_assoc($sql_art);
															$art = 1;
														}
													}
													
													$com = 0;
													if($gens['community_id']!=0)
													{
														$sql_com = mysql_query("SELECT * FROM general_community WHERE community_id='".$gens['community_id']."' AND status=0 AND active=0 AND del_status=0");
														if($sql_com!="" && $sql_com!=NULL)
														{
															if(mysql_num_rows($sql_com)>0)
															{
																$ros_com = mysql_fetch_assoc($sql_com);
																$com = 1;
															}
														}
													}
													
													if($art==1)
													{
														if($ros['profile_url']!="")
														{
													?>
														<a href="/<?php echo $ros['profile_url']; ?>">
													<?php
														}
														$get_news_general_user_id = $uk -> get_general_user_idby_artistid($ros['artist_id']);
													?>
														<img width="50" height="50" src="<?php
														if($ros['image_name']!="" && !empty($ros['image_name']))
														{
															echo $s3->getNewURI_otheruser("artjcropprofile",$get_news_general_user_id).$ros['image_name']; 
														}
														else
														{
															echo "http://artjcropprofile.s3.amazonaws.com/Noimage.png";
														}
													?>" />
													<?php
														if($ros['profile_url']!="")
														{
													?>
														</a>
													<?php
														}
													}
													elseif($com==1)
													{
														if($ros_com['profile_url']!="")
														{
													?>
														<a href="/<?php echo $ros_com['profile_url']; ?>">
													<?php
														}
														$get_news_general_user_id = $uk -> get_general_user_idby_communityid($ros_com['community_id']);
													?>
														<img width="50" height="50" src="<?php
													
													
													if($ros_com['image_name']!="" && !empty($ros_com['image_name']))
													{
														echo $s3->getNewURI_otheruser("comjcropprofile",$get_news_general_user_id).$ros_com['image_name']; 
													}
													else
													{
														echo "http://comjcropprofile.s3.amazonaws.com/Noimage.png";
													}
													
													?>" />
													<?php
														if($ros_com['profile_url']!="")
														{
													?>
														</a>
													<?php
														}
													}
													?>
																	</div>
																	<?php //echo "table=".$reg_table_media_type;
																	//echo $ans_audio_feeds[$audio_fed_lo]['id'];
																	?>
																		<div class="storyContainer">
																			<div class="storyTitle">
																				<span class="userName"><?php if($art==1)
															{
																if($ros['profile_url']!="")
														{
													?>
														<a href="/<?php echo $ros['profile_url']; ?>">
													<?php
														}
																echo $ros['name'];
																if($ros['profile_url']!="")
														{
													?>
														</a>
													<?php
														}
															}
															elseif($com==1)
															{
																if($ros_com['profile_url']!="")
														{
													?>
														<a href="/<?php echo $ros_com['profile_url']; ?>">
													<?php
														}
																echo $ros_com['name'];
																if($ros_com['profile_url']!="")
														{
													?>
														</a>
													<?php
														}
															} ?></span>  <?php //echo $ans_videos_posts[$vpi_all]['description']; ?>
																			</div>
																			<div class="activityCont">
																				<div class="sideL">
																					<div class="storyImage" style="position:relative;"><?php
																					
																					if($sep_ids_v['media_type']==115 || $ans_all_posts[$po_all]['profile_type']=='videos_post')
																		{
																			if($sep_ids_v['media_type']==115)
																			{
																				$tables = 'media_video';
																				$get_video_image = $sql_feeds->getvideoimage($tables,$sep_ids_v['id']);
																			}
																			elseif($ans_all_posts[$po_all]['profile_type']=='videos_post' && $exp_meds[1]=='reg_art')
																			{
																				$tables = 'general_artist_video';
																				$get_video_image = $sql_feeds->getvideoimage($tables,$sep_ids_v['id']);
																			}
																			elseif($ans_all_posts[$po_all]['profile_type']=='videos_post' && $exp_meds[1]=='reg_com')
																			{
																				$tables = 'general_community_video';
																				$get_video_image = $sql_feeds->getvideoimage($tables,$sep_ids_v['id']);
																			}
																			if($get_video_image[1]=='youtube')
																			{
																				$vid_pros_edit_img = "http://img.youtube.com/vi/".$get_video_image[0]."/default.jpg";
																			}
																			else
																			{
																				$video_vim = $vimeo->call('vimeo.videos.getInfo', array('video_id' => $get_video_image[0]));
																				$vid_pros_edit_img = $video_vim->video[0]->thumbnails->thumbnail[1]->_content;
																			}
																	?>
																		<a onmouseout="newfunctionout('<?php echo $sep_ids_v['id']; ?>','v')" onmouseover="newfunction('<?php echo $sep_ids_v['id']; ?>','v')" href="javascript:void(0);" onclick="showPlayer('v','<?php echo $sep_ids_v['id'];?>','playlist');">
																			<img src="<?php echo $vid_pros_edit_img; ?>" width="90" height="90" />
																		</a>
																	<?php
																		}
																					
																					?><div class="navButton">
			<div class="playMedia">
				<a href="javascript:void(0);" class="list_play"><img title="Add file to the bottom of your playlist without stopping current media." id="play_video_image<?php echo $sep_ids_v['id']; ?>v" onclick="showPlayer('v','<?php echo $sep_ids_v['id'];?>','playlist');" src="/images/play.png" onmouseout="newfunctionout('<?php echo $sep_ids_v['id']; ?>','v')" onmouseover="newfunction('<?php echo $sep_ids_v['id']; ?>','v')" style="border-radius: 6px;"></a>
			</div>
		</div></div>
																					
																					<div class="player">
																						<img src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" onclick="showPlayer('v','<?php echo $sep_ids_v['id'];?>','playlist');">
																						<img src="images/profile/add.gif" onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'" onclick="showPlayer('v','<?php echo $sep_ids_v['id'];?>','addvi');">
																						<?php
																						if($sep_ids_v['sharing_preference'] == 5 || $sep_ids_v['sharing_preference'] == 2)
																						{
																							$get_video_image = $sql_feeds->getvideoimage_feature($sep_ids_v['id']);
																							//$image_name_cart = "https://img.youtube.com/vi/".$get_video_image."/default.jpg";
																							if($get_video_image[1]=='youtube')
																			{
																				$image_name_cart = "http://img.youtube.com/vi/".$get_video_image[0]."/default.jpg";
																			}
																			else
																			{
																				$video_vim = $vimeo->call('vimeo.videos.getInfo', array('video_id' => $get_video_image[0]));
																				$image_name_cart = $video_vim->video[0]->thumbnails->thumbnail[1]->_content;
																			}
																							$free_down_rel_video="";
																							for($find_down=0;$find_down<=count($get_vids_pnew_w);$find_down++)
																							{
																								if(isset($sep_ids_v) && $sep_ids_v != null){
																									if($get_vids_pnew_w[$find_down]['id']==$sep_ids_v['id']){
																										$free_down_rel_video = "yes";
																									}
																									else{
																										if($free_down_rel_video =="" || $free_down_rel_video =="no"){
																											$free_down_rel_video = "no";
																										}
																									}
																								}
																							}
																							if($free_down_rel_video == "yes"){
																							 ?>
																								<!--<a href="javascript:void(0);" onclick="download_directly_song('<?php echo $sep_ids_v['id']; ?>','<?php echo str_replace("'","\'",$sep_ids_v['title']); ?>','<?php echo str_replace("'","\'",$sep_ids_v['creator']); ?>','<?php echo str_replace("'","\'",$sep_ids_v['from']); ?>')"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>-->
																						 <?php
																							}
																							else if($sep_ids_v['sharing_preference']==2){
																								if($sep_ids_v['id']!=""){
																								?>
																								<!--<a href="javascript:void(0);" onclick="ask_to_donate('<?php echo str_replace("'","\'",$sep_ids_v['title']);?>','<?php echo str_replace("'","\'",$sep_ids_v['creator']);?>','<?php echo $sep_ids_v['id'];?>','download_song<?php echo $sep_ids_v['id'];?>')"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>-->
																							<?php
																								}else{
																								?>
																								<!--<a href="javascript:void(0);" onclick="ask_to_donate('<?php echo str_replace("'","\'",$sep_ids_v['title']);?>','<?php echo str_replace("'","\'",$sep_ids_v['creator']);?>','<?php echo $sep_ids_v['id'];?>','download_song<?php echo $sep_ids_v['id'];?>')"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>-->
																							<?php
																								}
																							}
																							else{
																						?>
																							<!--<a href="javascript:void(0);" onclick="down_video_pop(<?php echo $sep_ids_v['id'];?>)"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>-->
																						<?php
																							}
																						}else{
																						?>
																							<!--<img src="../images/profile/download-over.gif" style="cursor:default"/>-->
																						<?php
																						}
																						?>
																						<!--<a href="addtocart.php?seller_id=<?php echo $sep_ids_v['general_user_id']; ?>&media_id=<?php echo $sep_ids_v['id']; ?>&buyer_id=<?php echo $_SESSION['gen_user_id'];?>&title=<?php echo str_replace("'","\'",$sep_ids_v['title']);?>&image_name=<?php echo $image_name_cart;?>" id="download_video<?php echo $sep_ids_v['id'];?>" style="display:none;"></a>-->
																						<script type="text/javascript">
																						$(document).ready(function() {
																								$("#download_video<?php echo $sep_ids_v['id'];?>").fancybox({
																								'height'			: '75%',
																								'width'				: '69%',
																								'transitionIn'		: 'none',
																								'transitionOut'		: 'none',
																								'type'				: 'iframe'
																								});
																						});
																						</script>
																					</div>
																				</div>
																				<div class="sideR">
																					<div class="activityTitle"><a href=""><?php
																					if($exp_meds[1]=='reg_med')
																					{
																						?>
																						<a href="javascript:void(0)" onclick = "showPlayer('v',<?php echo $sep_ids_v['id'];?>,'addvi');"><?php echo $sep_ids_v['title'];?></a>
																						<?php
																					}
																					elseif($exp_meds[1]=='reg_art' || $exp_meds[1]=='reg_com')
																					{
																						if($ans_videos_posts[$vpi_all]['profile_type']=='videos_post')
																						{
																							?>
																							<a href="javascript:void(0)" onclick = "showPlayer('v',<?php echo $sep_ids_v['id'];?>,'addvi');"><?php echo $sep_ids_v['video_name'];?></a>
																							<?php
																						}
																						
																					}?></a>
																						<div style="color:#808080;font-size:12px;">
																							<div class="comment more">
																								<?php echo $ans_videos_posts[$vpi_all]['description']; ?>
																							</div>
																						</div>
																					</div>
																					<!--<div class="activityContent">Tagged: <?php //echo $medias['tagged_users']; 
																						//$name_org = explode(')',$medias['tagged_users']);
																						//for($i_name_org=0;$i_name_org<count($name_org);$i_name_org++)
																						//{
																						//	$end_pos = strpos($name_org[$i_name_org],'(');
																						//	$org_name = substr($name_org[$i_name_org],0,$end_pos);
																					//		echo $org_name;
																					//	}
																					?></div>-->
																				</div>
																			</div>
																		</div>
																	</div>
																	<div class="rightSide">
																		<a href="#" onmouseover="mopen('mm<?php echo $vpi_all;?>14')" onmouseout="mclosetime()"><img src="images/profile/arrow.png" onmouseover="this.src='images/profile/arrow-down.png'" onmouseout="this.src='images/profile/arrow.png'" /></a>
																		<div id="mm<?php echo $vpi_all;?>14" class="popUp" onmouseover="mcancelclosetime()" onmouseout="mclosetime()">
																			<ul>
																				<li><a href="hideposts.php?id=<?php echo $ans_videos_posts[$vpi_all]['id'];?>">Hide Feed</a></li>
																				<li><a href="hideposts.php?report_id=<?php echo $ans_all_posts[$po_all]['id'];?>">Report spam</a></li>
																				<li><div class="seperator"></div></li>
																				<li><a href="">Unsubscribe from <?php echo $gens['fname'].' '.$gens['lname']; ?></a></li>
																			</ul>
																		</div>
																	</div>
																</div>
			<?php
																}
															}		
														}
													}
													elseif($ans_videos_posts[$vpi_all]['display_to']=='Subscribers')
													{
														if($gens!="")
														{
															$subscribers_all =	$sql_feeds->subscribers_cond($chk_sql['email'],$gens['community_id'],$gens['artist_id']);
															if($subscribers_all=='1')
															{
																$hide21 = 0;
																$get_gen_id = $sql_feeds->get_general_info();
																$hide_by = $ans_videos_posts[$vpi_all]['hidden_by'];
																$exp_hide_by = explode(",",$hide_by);
																for($exp_count=0;$exp_count<=count($exp_hide_by);$exp_count++)
																{
																	if($exp_hide_by[$exp_count] ==$get_gen_id['general_user_id'])
																	{
																		$hide21 =1;
																	}
																}
																if($hide21 != 1)
																{
			?>
																<div class="storyCont">
																	<div class="leftSide">
																		<div class="avatar">
																			<?php
													$art = 0;
													
													if($gens['artist_id']!=0)
													{
														$sql_art = mysql_query("SELECT * FROM general_artist WHERE artist_id='".$gens['artist_id']."' AND status=0 AND active=0 AND del_status=0");
														if($sql_art!="" && $sql_art!=NULL)
														{
															if(mysql_num_rows($sql_art)>0)
															{
																$ros = mysql_fetch_assoc($sql_art);
																$art = 1;
															}
														}
													}
													
													$com = 0;
													if($gens['community_id']!=0)
													{
														$sql_com = mysql_query("SELECT * FROM general_community WHERE community_id='".$gens['community_id']."' AND status=0 AND active=0 AND del_status=0");
														if($sql_com!="" && $sql_com!=NULL)
														{
															if(mysql_num_rows($sql_com)>0)
															{
																$ros_com = mysql_fetch_assoc($sql_com);
																$com = 1;
															}
														}
													}
													
													if($art==1)
													{
														if($ros['profile_url']!="")
														{
													?>
														<a href="/<?php echo $ros['profile_url']; ?>">
													<?php
														}
														$get_news_general_user_id = $uk -> get_general_user_idby_artistid($ros['artist_id']);
													?>
														<img width="50" height="50" src="<?php
														if($ros['image_name']!="" && !empty($ros['image_name']))
														{
															echo $s3->getNewURI_otheruser("artjcropprofile",$get_news_general_user_id).$ros['image_name']; 
														}
														else
														{
															echo "http://artjcropprofile.s3.amazonaws.com/Noimage.png";
														}
													?>" />
													<?php
														if($ros['profile_url']!="")
														{
													?>
														</a>
													<?php
														}
													}
													elseif($com==1)
													{
														if($ros_com['profile_url']!="")
														{
													?>
														<a href="/<?php echo $ros_com['profile_url']; ?>">
													<?php
														}
														$get_news_general_user_id = $uk -> get_general_user_idby_communityid($ros_com['community_id']);
													?>
														<img width="50" height="50" src="<?php
													
													
													if($ros_com['image_name']!="" && !empty($ros_com['image_name']))
													{
														echo $s3->getNewURI_otheruser("comjcropprofile",$get_news_general_user_id).$ros_com['image_name']; 
													}
													else
													{
														echo "http://comjcropprofile.s3.amazonaws.com/Noimage.png";
													}
													
													?>" />
													<?php
														if($ros_com['profile_url']!="")
														{
													?>
														</a>
													<?php
														}
													}
													?>
																	</div>
																	<?php //echo "table=".$reg_table_media_type;
																	//echo $ans_audio_feeds[$audio_fed_lo]['id'];
																	?>
																		<div class="storyContainer">
																			<div class="storyTitle">
																				<span class="userName"><?php if($art==1)
															{
																if($ros['profile_url']!="")
														{
													?>
														<a href="/<?php echo $ros['profile_url']; ?>">
													<?php
														}
																echo $ros['name'];
																if($ros['profile_url']!="")
														{
													?>
														</a>
													<?php
														}
															}
															elseif($com==1)
															{
																if($ros_com['profile_url']!="")
														{
													?>
														<a href="/<?php echo $ros_com['profile_url']; ?>">
													<?php
														}
																echo $ros_com['name'];
																if($ros_com['profile_url']!="")
														{
													?>
														</a>
													<?php
														}
															} ?></span>  <?php //echo $ans_videos_posts[$vpi_all]['description']; ?>
																			</div>
																			<div class="activityCont">
																				<div class="sideL">
																					<div class="storyImage" style="position:relative;"><?php
																					
																					if($sep_ids_v['media_type']==115 || $ans_all_posts[$po_all]['profile_type']=='videos_post')
																		{
																			if($sep_ids_v['media_type']==115)
																			{
																				$tables = 'media_video';
																				$get_video_image = $sql_feeds->getvideoimage($tables,$sep_ids_v['id']);
																			}
																			elseif($ans_all_posts[$po_all]['profile_type']=='videos_post' && $exp_meds[1]=='reg_art')
																			{
																				$tables = 'general_artist_video';
																				$get_video_image = $sql_feeds->getvideoimage($tables,$sep_ids_v['id']);
																			}
																			elseif($ans_all_posts[$po_all]['profile_type']=='videos_post' && $exp_meds[1]=='reg_com')
																			{
																				$tables = 'general_community_video';
																				$get_video_image = $sql_feeds->getvideoimage($tables,$sep_ids_v['id']);
																			}
																			if($get_video_image[1]=='youtube')
																			{
																				$vid_pros_edit_img = "http://img.youtube.com/vi/".$get_video_image[0]."/default.jpg";
																			}
																			else
																			{
																				$video_vim = $vimeo->call('vimeo.videos.getInfo', array('video_id' => $get_video_image[0]));
																				$vid_pros_edit_img = $video_vim->video[0]->thumbnails->thumbnail[1]->_content;
																			}
																	?>
																		<a onmouseout="newfunctionout('<?php echo $sep_ids_v['id']; ?>','v')" onmouseover="newfunction('<?php echo $sep_ids_v['id']; ?>','v')" href="javascript:void(0);" onclick="showPlayer('v','<?php echo $sep_ids_v['id'];?>','playlist');">
																			<img src="<?php echo $vid_pros_edit_img; ?>" width="90" height="90" />
																		</a>
																	<?php
																		}
																					
																					?><div class="navButton">
			<div class="playMedia">
				<a href="javascript:void(0);" class="list_play"><img title="Add file to the bottom of your playlist without stopping current media." id="play_video_image<?php echo $sep_ids_v['id']; ?>v" onclick="showPlayer('v','<?php echo $sep_ids_v['id'];?>','playlist');" src="/images/play.png" onmouseout="newfunctionout('<?php echo $sep_ids_v['id']; ?>','v')" onmouseover="newfunction('<?php echo $sep_ids_v['id']; ?>','v')" style="border-radius: 6px;"></a>
			</div>
		</div></div>
																					
																					<div class="player">
																						<img src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" onclick="showPlayer('v','<?php echo $sep_ids_v['id'];?>','playlist');">
																						<img src="images/profile/add.gif" onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'" onclick="showPlayer('v','<?php echo $sep_ids_v['id'];?>','addvi');">
																						<?php
																						if($sep_ids_v['sharing_preference'] == 5 || $sep_ids_v['sharing_preference'] == 2)
																						{
																							$get_video_image = $sql_feeds->getvideoimage_feature($sep_ids_v['id']);
																							//$image_name_cart = "https://img.youtube.com/vi/".$get_video_image."/default.jpg";
																							if($get_video_image[1]=='youtube')
																			{
																				$image_name_cart = "http://img.youtube.com/vi/".$get_video_image[0]."/default.jpg";
																			}
																			else
																			{
																				$video_vim = $vimeo->call('vimeo.videos.getInfo', array('video_id' => $get_video_image[0]));
																				$image_name_cart = $video_vim->video[0]->thumbnails->thumbnail[1]->_content;
																			}
																							$free_down_rel_video="";
																							for($find_down=0;$find_down<=count($get_vids_pnew_w);$find_down++)
																							{
																								if(isset($sep_ids_v) && $sep_ids_v != null){
																									if($get_vids_pnew_w[$find_down]['id']==$sep_ids_v['id']){
																										$free_down_rel_video = "yes";
																									}
																									else{
																										if($free_down_rel_video =="" || $free_down_rel_video =="no"){
																											$free_down_rel_video = "no";
																										}
																									}
																								}
																							}
																							if($free_down_rel_video == "yes"){
																							 ?>
																								<!--<a href="javascript:void(0);" onclick="download_directly_song('<?php echo $sep_ids_v['id']; ?>','<?php echo str_replace("'","\'",$sep_ids_v['title']); ?>','<?php echo str_replace("'","\'",$sep_ids_v['creator']); ?>','<?php echo str_replace("'","\'",$sep_ids_v['from']); ?>')"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>-->
																						 <?php
																							}
																							else if($sep_ids_v['sharing_preference']==2){
																								if($sep_ids_v['id']!=""){
																								?>
																								<!--<a href="javascript:void(0);" onclick="ask_to_donate('<?php echo str_replace("'","\'",$sep_ids_v['title']);?>','<?php echo str_replace("'","\'",$sep_ids_v['creator']);?>','<?php echo $sep_ids_v['id'];?>','download_song<?php echo $sep_ids_v['id'];?>')"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>-->
																							<?php
																								}else{
																								?>
																								<!--<a href="javascript:void(0);" onclick="ask_to_donate('<?php echo str_replace("'","\'",$sep_ids_v['title']);?>','<?php echo str_replace("'","\'",$sep_ids_v['creator']);?>','<?php echo $sep_ids_v['id'];?>','download_song<?php echo $sep_ids_v['id'];?>')"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>-->
																							<?php
																								}
																							}
																							else{
																						?>
																							<!--<a href="javascript:void(0);" onclick="down_video_pop(<?php echo $sep_ids_v['id'];?>)"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>-->
																						<?php
																							}
																						}else{
																						?>
																							<!--<img src="../images/profile/download-over.gif" style="cursor:default"/>-->
																						<?php
																						}
																						?>
																						<!--<a href="addtocart.php?seller_id=<?php echo $sep_ids_v['general_user_id']; ?>&media_id=<?php echo $sep_ids_v['id']; ?>&buyer_id=<?php echo $_SESSION['gen_user_id'];?>&title=<?php echo str_replace("'","\'",$sep_ids_v['title']);?>&image_name=<?php echo $image_name_cart;?>" id="download_video<?php echo $sep_ids_v['id'];?>" style="display:none;"></a>-->
																						<script type="text/javascript">
																						$(document).ready(function() {
																								$("#download_video<?php echo $sep_ids_v['id'];?>").fancybox({
																								'height'			: '75%',
																								'width'				: '69%',
																								'transitionIn'		: 'none',
																								'transitionOut'		: 'none',
																								'type'				: 'iframe'
																								});
																						});
																						</script>
																					</div>
																				</div>
																				<div class="sideR">
																					<div class="activityTitle"><a href=""><?php
																					if($exp_meds[1]=='reg_med')
																					{
																						?>
																						<a href="javascript:void(0)" onclick = "showPlayer('v',<?php echo $sep_ids_v['id'];?>,'addvi');"><?php echo $sep_ids_v['title'];?></a>
																						<?php
																					}
																					elseif($exp_meds[1]=='reg_art' || $exp_meds[1]=='reg_com')
																					{
																						if($ans_videos_posts[$vpi_all]['profile_type']=='videos_post')
																						{
																							?>
																							<a href="javascript:void(0)" onclick = "showPlayer('v',<?php echo $sep_ids_v['id'];?>,'addvi');"><?php echo $sep_ids_v['video_name'];?></a>
																							<?php
																						}
																					}
																					?></a>
																						<div style="color:#808080;font-size:12px;">
																							<div class="comment more">
																								<?php echo $ans_videos_posts[$vpi_all]['description']; ?>
																							</div>
																						</div>
																					</div>
																					<!--<div class="activityContent">Tagged: <?php //echo $medias['tagged_users']; 
																						//$name_org = explode(')',$medias['tagged_users']);
																						//for($i_name_org=0;$i_name_org<count($name_org);$i_name_org++)
																						//{
																						//	$end_pos = strpos($name_org[$i_name_org],'(');
																						//	$org_name = substr($name_org[$i_name_org],0,$end_pos);
																					//		echo $org_name;
																					//	}
																					?></div>-->
																				</div>
																			</div>
																		</div>
																	</div>
																	<div class="rightSide">
																		<a href="#" onmouseover="mopen('mm<?php echo $vpi_all;?>15')" onmouseout="mclosetime()"><img src="images/profile/arrow.png" onmouseover="this.src='images/profile/arrow-down.png'" onmouseout="this.src='images/profile/arrow.png'" /></a>
																		<div id="mm<?php echo $vpi_all;?>15" class="popUp" onmouseover="mcancelclosetime()" onmouseout="mclosetime()">
																			<ul>
																				<li><a href="hideposts.php?id=<?php echo $ans_videos_posts[$vpi_all]['id'];?>">Hide Feed</a></li>
																				<li><a href="hideposts.php?report_id=<?php echo $ans_all_posts[$po_all]['id'];?>">Report spam</a></li>
																				<li><div class="seperator"></div></li>
																				<li><!--<a href="Unsubscribe_feeds.php?sub=1&user_ids=<?php echo $gens['general_user_id']; ?>" onclick="return confirm_subscription_delete('<?php if($art==1){ echo $ros['name']; } elseif($com==1){ echo $ros_com['name']; } ?>')">Unsubscribe from <?php if($art==1)
															{
																echo $ros['name'];
															}
															elseif($com==1)
															{
																echo $ros_com['name'];
															} ?></a>-->
															<a href="profileedit.php#subscrriptions" >Unsubscribe from <?php if($art==1)
															{
																echo $ros['name'];
															}
															elseif($com==1)
															{
																echo $ros_com['name'];
															} ?></a>
															</li>
																			</ul>
																		</div>
																	</div>
																</div>
			<?php
																}
															}
														}
													}
													elseif($ans_videos_posts[$vpi_all]['display_to']=='Friends')
													{
														if($gens!="")
														{
															$friends_c = $sql_feeds->friends_cond($ans_videos_posts[$vpi_all]['general_user_id'],$_SESSION['login_id']);
															
															if($friends_c=='1')
															{
																$hide22 = 0;
																$get_gen_id = $sql_feeds->get_general_info();
																$hide_by = $ans_videos_posts[$vpi_all]['hidden_by'];
																$exp_hide_by = explode(",",$hide_by);
																for($exp_count=0;$exp_count<=count($exp_hide_by);$exp_count++)
																{
																	if($exp_hide_by[$exp_count] ==$get_gen_id['general_user_id'])
																	{
																		$hide22 =1;
																	}
																}
																if($hide22 != 1)
																{
			?>
																<div class="storyCont">
																	<div class="leftSide">
																		<div class="avatar">
																			<?php
													$art = 0;
													
													if($gens['artist_id']!=0)
													{
														$sql_art = mysql_query("SELECT * FROM general_artist WHERE artist_id='".$gens['artist_id']."' AND status=0 AND active=0 AND del_status=0");
														if($sql_art!="" && $sql_art!=NULL)
														{
															if(mysql_num_rows($sql_art)>0)
															{
																$ros = mysql_fetch_assoc($sql_art);
																$art = 1;
															}
														}
													}
													
													$com = 0;
													if($gens['community_id']!=0)
													{
														$sql_com = mysql_query("SELECT * FROM general_community WHERE community_id='".$gens['community_id']."' AND status=0 AND active=0 AND del_status=0");
														if($sql_com!="" && $sql_com!=NULL)
														{
															if(mysql_num_rows($sql_com)>0)
															{
																$ros_com = mysql_fetch_assoc($sql_com);
																$com = 1;
															}
														}
													}
													
													if($art==1)
													{
														if($ros['profile_url']!="")
														{
													?>
														<a href="/<?php echo $ros['profile_url']; ?>">
													<?php
														}
														$get_news_general_user_id = $uk -> get_general_user_idby_artistid($ros['artist_id']);
													?>
														<img width="50" height="50" src="<?php
														if($ros['image_name']!="" && !empty($ros['image_name']))
														{
															echo $s3->getNewURI_otheruser("artjcropprofile",$get_news_general_user_id).$ros['image_name']; 
														}
														else
														{
															echo "http://artjcropprofile.s3.amazonaws.com/Noimage.png";
														}
													?>" />
													<?php
														if($ros['profile_url']!="")
														{
													?>
														</a>
													<?php
														}
													}
													elseif($com==1)
													{
														if($ros_com['profile_url']!="")
														{
													?>
														<a href="/<?php echo $ros_com['profile_url']; ?>">
													<?php
														}
														$get_news_general_user_id = $uk -> get_general_user_idby_communityid($ros_com['community_id']);
													?>
														<img width="50" height="50" src="<?php
													
													
													if($ros_com['image_name']!="" && !empty($ros_com['image_name']))
													{
														echo $s3->getNewURI_otheruser("comjcropprofile",$get_news_general_user_id).$ros_com['image_name']; 
													}
													else
													{
														echo "http://comjcropprofile.s3.amazonaws.com/Noimage.png";
													}
													
													?>" />
													<?php
														if($ros_com['profile_url']!="")
														{
													?>
														</a>
													<?php
														}
													}
													?>
																	</div>
																	<?php //echo "table=".$reg_table_media_type;
																	//echo $ans_audio_feeds[$audio_fed_lo]['id'];
																	?>
																		<div class="storyContainer">
																			<div class="storyTitle">
																				<span class="userName"><?php if($art==1)
															{
																if($ros['profile_url']!="")
														{
													?>
														<a href="/<?php echo $ros['profile_url']; ?>">
													<?php
														}
																echo $ros['name'];
																if($ros['profile_url']!="")
														{
													?>
														</a>
													<?php
														}
															}
															elseif($com==1)
															{
																if($ros_com['profile_url']!="")
														{
													?>
														<a href="/<?php echo $ros_com['profile_url']; ?>">
													<?php
														}
																echo $ros_com['name'];
																if($ros_com['profile_url']!="")
														{
													?>
														</a>
													<?php
														}
															} ?></span>  <?php //echo $ans_videos_posts[$vpi_all]['description']; ?>
																			</div>
																			<div class="activityCont">
																				<div class="sideL">
																					<div class="storyImage" style="position:relative;"><?php
																					
																					if($sep_ids_v['media_type']==115 || $ans_all_posts[$po_all]['profile_type']=='videos_post')
																		{
																			if($sep_ids_v['media_type']==115)
																			{
																				$tables = 'media_video';
																				$get_video_image = $sql_feeds->getvideoimage($tables,$sep_ids_v['id']);
																			}
																			elseif($ans_all_posts[$po_all]['profile_type']=='videos_post' && $exp_meds[1]=='reg_art')
																			{
																				$tables = 'general_artist_video';
																				$get_video_image = $sql_feeds->getvideoimage($tables,$sep_ids_v['id']);
																			}
																			elseif($ans_all_posts[$po_all]['profile_type']=='videos_post' && $exp_meds[1]=='reg_com')
																			{
																				$tables = 'general_community_video';
																				$get_video_image = $sql_feeds->getvideoimage($tables,$sep_ids_v['id']);
																			}
																			if($get_video_image[1]=='youtube')
																			{
																				$vid_pros_edit_img = "http://img.youtube.com/vi/".$get_video_image[0]."/default.jpg";
																			}
																			else
																			{
																				$video_vim = $vimeo->call('vimeo.videos.getInfo', array('video_id' => $get_video_image[0]));
																				$vid_pros_edit_img = $video_vim->video[0]->thumbnails->thumbnail[1]->_content;
																			}
																	?>
																		<a onmouseout="newfunctionout('<?php echo $sep_ids_v['id']; ?>','v')" onmouseover="newfunction('<?php echo $sep_ids_v['id']; ?>','v')" href="javascript:void(0);" onclick="showPlayer('v','<?php echo $sep_ids_v['id'];?>','playlist');">
																			<img src="<?php echo $vid_pros_edit_img; ?>" width="90" height="90" />
																		</a>
																	<?php
																		}
																					
																					?>
																					<div class="navButton">
			<div class="playMedia">
				<a href="javascript:void(0);" class="list_play"><img title="Add file to the bottom of your playlist without stopping current media." id="play_video_image<?php echo $sep_ids_v['id']; ?>v" onclick="showPlayer('v','<?php echo $sep_ids_v['id'];?>','playlist');" src="/images/play.png" onmouseout="newfunctionout('<?php echo $sep_ids_v['id']; ?>','v')" onmouseover="newfunction('<?php echo $sep_ids_v['id']; ?>','v')" style="border-radius: 6px;"></a>
			</div>
		</div>
		</div>
																					</div>
																					
																					<div class="player">
																						<img src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" onclick="showPlayer('v','<?php echo $sep_ids_v['id'];?>','playlist');">
																						<img src="images/profile/add.gif" onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'" onclick="showPlayer('v','<?php echo $sep_ids_v['id'];?>','addvi');">
																						<?php
																						if($sep_ids_v['sharing_preference'] == 5 || $sep_ids_v['sharing_preference'] == 2)
																						{
																							$get_video_image = $sql_feeds->getvideoimage_feature($sep_ids_v['id']);
																							//$image_name_cart = "https://img.youtube.com/vi/".$get_video_image."/default.jpg";
																							if($get_video_image[1]=='youtube')
																			{
																				$image_name_cart = "http://img.youtube.com/vi/".$get_video_image[0]."/default.jpg";
																			}
																			else
																			{
																				$video_vim = $vimeo->call('vimeo.videos.getInfo', array('video_id' => $get_video_image[0]));
																				$image_name_cart = $video_vim->video[0]->thumbnails->thumbnail[1]->_content;
																			}
																							$free_down_rel_video="";
																							for($find_down=0;$find_down<=count($get_vids_pnew_w);$find_down++)
																							{
																								if(isset($sep_ids_v) && $sep_ids_v != null){
																									if($get_vids_pnew_w[$find_down]['id']==$sep_ids_v['id']){
																										$free_down_rel_video = "yes";
																									}
																									else{
																										if($free_down_rel_video =="" || $free_down_rel_video =="no"){
																											$free_down_rel_video = "no";
																										}
																									}
																								}
																							}
																							if($free_down_rel_video == "yes"){
																							 ?>
																								<!--<a href="javascript:void(0);" onclick="download_directly_song('<?php echo $sep_ids_v['id']; ?>','<?php echo str_replace("'","\'",$sep_ids_v['title']); ?>','<?php echo str_replace("'","\'",$sep_ids_v['creator']); ?>','<?php echo str_replace("'","\'",$sep_ids_v['from']); ?>')"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>-->
																						 <?php
																							}
																							else if($sep_ids_v['sharing_preference']==2){
																								if($sep_ids_v['id']!=""){
																								?>
																								<!--<a href="javascript:void(0);" onclick="ask_to_donate('<?php echo str_replace("'","\'",$sep_ids_v['title']);?>','<?php echo str_replace("'","\'",$sep_ids_v['creator']);?>','<?php echo $sep_ids_v['id'];?>','download_song<?php echo $sep_ids_v['id'];?>')"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>-->
																							<?php
																								}else{
																								?>
																								<!--<a href="javascript:void(0);" onclick="ask_to_donate('<?php echo str_replace("'","\'",$sep_ids_v['title']);?>','<?php echo str_replace("'","\'",$sep_ids_v['creator']);?>','<?php echo $sep_ids_v['id'];?>','download_song<?php echo $sep_ids_v['id'];?>')"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>-->
																							<?php
																								}
																							}
																							else{
																						?>
																							<!--<a href="javascript:void(0);" onclick="down_video_pop(<?php echo $sep_ids_v['id'];?>)"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>-->
																						<?php
																							}
																						}else{
																						?>
																							<!--<img src="../images/profile/download-over.gif" style="cursor:default"/>-->
																						<?php
																						}
																						?>
																						<!--<a href="addtocart.php?seller_id=<?php echo $sep_ids_v['general_user_id']; ?>&media_id=<?php echo $sep_ids_v['id']; ?>&buyer_id=<?php echo $_SESSION['gen_user_id'];?>&title=<?php echo str_replace("'","\'",$sep_ids_v['title']);?>&image_name=<?php echo $image_name_cart;?>" id="download_video<?php echo $sep_ids_v['id'];?>" style="display:none;"></a>-->
																						<script type="text/javascript">
																						$(document).ready(function() {
																								$("#download_video<?php echo $sep_ids_v['id'];?>").fancybox({
																								'height'			: '75%',
																								'width'				: '69%',
																								'transitionIn'		: 'none',
																								'transitionOut'		: 'none',
																								'type'				: 'iframe'
																								});
																						});
																						</script>
																					</div>
																				</div>
																				<div class="sideR">
																					<div class="activityTitle"><a href=""><?php
																					if($exp_meds[1]=='reg_med')
																					{
																						?>
																						<a href="javascript:void(0)" onclick = "showPlayer('v',<?php echo $sep_ids_v['id'];?>,'addvi');"><?php echo $sep_ids_v['title'];?></a>
																						<?php
																					}
																					elseif($exp_meds[1]=='reg_art' || $exp_meds[1]=='reg_com')
																					{
																						if($ans_videos_posts[$vpi_all]['profile_type']=='videos_post')
																						{
																							?>
																							<a href="javascript:void(0)" onclick = "showPlayer('v',<?php echo $sep_ids_v['id'];?>,'addvi');"><?php echo $sep_ids_v['video_name'];?></a>
																							<?php
																						}
																					}
																					?></a>
																						<div style="color:#808080;font-size:12px;">
																							<div class="comment more">
																								<?php echo $ans_videos_posts[$vpi_all]['description']; ?>
																							</div>
																						</div>
																					</div>
																					<!--<div class="activityContent">Tagged: <?php //echo $medias['tagged_users']; 
																						//$name_org = explode(')',$medias['tagged_users']);
																						//for($i_name_org=0;$i_name_org<count($name_org);$i_name_org++)
																						//{
																						//	$end_pos = strpos($name_org[$i_name_org],'(');
																						//	$org_name = substr($name_org[$i_name_org],0,$end_pos);
																					//		echo $org_name;
																					//	}
																					?></div>-->
																				</div>
																			</div>
																		</div>
																	</div>
																	<div class="rightSide">
																		<a href="#" onmouseover="mopen('mm<?php echo $vpi_all;?>16')" onmouseout="mclosetime()"><img src="images/profile/arrow.png" onmouseover="this.src='images/profile/arrow-down.png'" onmouseout="this.src='images/profile/arrow.png'" /></a>
																		<div id="mm<?php echo $vpi_all;?>16" class="popUp" onmouseover="mcancelclosetime()" onmouseout="mclosetime()">
																			<ul>
																				<li><a href="hideposts.php?id=<?php echo $ans_videos_posts[$vpi_all]['id'];?>">Hide Feed</a></li>
																				<li><a href="hideposts.php?report_id=<?php echo $ans_all_posts[$po_all]['id'];?>">Report spam</a></li>
																				<li><div class="seperator"></div></li>
																				<li><!--<a href="Unsubscribe_feeds.php?frd=1&user_ids=<?php echo $gens['general_user_id']; ?>" onclick="return confirm_subscription_delete('<?php if($art==1){ echo $ros['name']; } elseif($com==1){ echo $ros_com['name']; } ?>')">Unsubscribe from <?php if($art==1)
															{
																echo $ros['name'];
															}
															elseif($com==1)
															{
																echo $ros_com['name'];
															} ?></a>-->
															<a href="profileedit.php#subscrriptions" >Unsubscribe from <?php if($art==1)
															{
																echo $ros['name'];
															}
															elseif($com==1)
															{
																echo $ros_com['name'];
															} ?></a>
															</li>
																			</ul>
																		</div>
																	</div>
																</div>
			<?php
																}
															}
														}
													}
												}
											}
										}
									}
							?>
							</div>
							
							
							<div id="events_feeds" style="display:none;">
							<?php
									include_once("findsale_song.php");
									include_once("findsale_video.php");
									$eve_ans_all_feeds = array();
									$sub_eve_ans_all_feeds = array();
									$fan_eve_ans_all_feeds = array();
									$ve_art_com_id = array();
									$ve_pro_eve_id = array();
									$evi_arts_ids = array();
									$evi_coms_ids = array();
									$ans_events_posts = array();
									
									$eve_sql_all_feeds = $sql_feeds->all_feeds($chk_sql['email']);
									$e_run_all_feeds = mysql_query($eve_sql_all_feeds);
									
									$eve_sql_fan_feeds = $sql_feeds->fan_club($chk_sql['email']);
									$e_run_fan_feeds = mysql_query($eve_sql_fan_feeds);
									
									if($e_run_all_feeds!="" && $e_run_all_feeds!=NULL)
									{
										while($e_row_all_feeds = mysql_fetch_assoc($e_run_all_feeds))
										{
											$sub_eve_ans_all_feeds[] = $e_row_all_feeds;
										}
									}
									
									if($e_run_fan_feeds!="" && $e_run_fan_feeds!=NULL)
									{
										while($e_row_fan_feeds = mysql_fetch_assoc($e_run_fan_feeds))
										{
											$fan_eve_ans_all_feeds[] = $e_row_fan_feeds;
										}
									}
									
									$eve_ans_all_feeds = array_merge($sub_eve_ans_all_feeds,$fan_eve_ans_all_feeds);
									if($eve_ans_all_feeds!=NULL)
									{
										for($vev_fed_lo=0;$vev_fed_lo<count($eve_ans_all_feeds);$vev_fed_lo++)
										{
											if($eve_ans_all_feeds[$vev_fed_lo]['related_type']!='artist' && $eve_ans_all_feeds[$vev_fed_lo]['related_type']!='community')
											{
												$num_ids = explode('_',$eve_ans_all_feeds[$vev_fed_lo]['related_id']);
												if($eve_ans_all_feeds[$vev_fed_lo]['related_type']=='artist_project' || $eve_ans_all_feeds[$vev_fed_lo]['related_type']=='artist_event')
												{
													$ve_pro_eve_id[$vev_fed_lo]['related_id_art'] = $num_ids[0];
												}
												elseif($eve_ans_all_feeds[$vev_fed_lo]['related_type']=='community_project' || $eve_ans_all_feeds[$vev_fed_lo]['related_type']=='community_event')
												{
													$ve_art_com_id[$vev_fed_lo]['related_id_com'] = $num_ids[0];
												}
											}
											elseif($eve_ans_all_feeds[$vev_fed_lo]['related_type']=='artist' || $eve_ans_all_feeds[$vev_fed_lo]['related_type']=='community')
											{
												if($eve_ans_all_feeds[$vev_fed_lo]['related_type']=='artist')
												{
													$ve_pro_eve_id[$vev_fed_lo]['related_id_art'] = $eve_ans_all_feeds[$vev_fed_lo]['related_id'];
												}
												elseif($eve_ans_all_feeds[$vev_fed_lo]['related_type']=='community')
												{
													$ve_art_com_id[$vev_fed_lo]['related_id_com'] = $eve_ans_all_feeds[$vev_fed_lo]['related_id'];
												}
											}
										}
										
										if($ve_pro_eve_id!=0 && count($ve_pro_eve_id)>0)
										{
											for($arts=0;$arts<count($ve_pro_eve_id);$arts++)
											{
												$org_art[] = $ve_pro_eve_id[$arts]['related_id_art'];
											}
											
											$new_art_vd = array();
											$vi = 0;
											foreach ($org_art as $key => $value)
											{
												if(isset($new_art_vd[$value]))
												{
													$new_art_vd[$value] += 1;
												}
												else
												{
													$new_art_vd[$value] = 1;
													
													$sqls = $sql_feeds->arts_gen($value);
													
													if($sqls!="")
													{
														$evi_arts_ids[$vi] = $sqls;
													}
												}
												$vi = $vi + 1;
											}
										}
										
										if($ve_art_com_id!="" && count($ve_art_com_id)>0)
										{
											for($coms=0;$coms<count($ve_art_com_id);$coms++)
											{
												$org_com[] = $ve_art_com_id[$coms]['related_id_com'];
											}
											
											$new_com_ves = array();
											$vih_c = 0;
											foreach ($org_com as $key => $value)
											{
												if(isset($new_com_ves[$value]))
												{
													$new_com_ves[$value] += 1;
												}
												else
												{
													$new_com_ves[$value] = 1;
													
													$sqls_c = $sql_feeds->coms_gen($value);
													if($sqls_c!="")
													{
														$evi_coms_ids[$vih_c] = $sqls_c;
													}
												}
												$vih_c = $vih_c + 1;
											}
										}
										
										$doub_er_chk = array_merge($evi_arts_ids,$evi_coms_ids);
										
										for($cevm_d=0;$cevm_d<count($doub_er_chk);$cevm_d++)
										{
											$rel_d_id[] = $doub_er_chk[$cevm_d]['general_user_id'];
										}
										
										$new_array_rep_edv = array();
										foreach ($rel_d_id as $key => $value)
										{
											if(isset($new_array_rep_edv[$value]))
												$new_array_rep_edv[$value] += 1;
											else
												$new_array_rep_edv[$value] = 1;
										}
										
										foreach ($new_array_rep_edv as $uid => $n)
										{
											$ex_uid_devs = $ex_uid_devs.','.$uid;
										}
										$ex_tr_esi = trim($ex_uid_devs, ",");
										$sep_ids_edvp = explode(',',$ex_tr_esi);
										
										if($sep_ids_edvp!="" && count($sep_ids_edvp)>0)
										{
											for($se_ca=0;$se_ca<count($sep_ids_edvp);$se_ca++)
											{
												$types = 'events_post';
												$ids = $sql_feeds->pmva_posts_check($sep_ids_edvp[$se_ca],$_SESSION['login_id'],$types);
												
												//die;
												if($ids!="" && count($ids)>0)
												{
													$posts_event = $sql_feeds->pmv_posts_check($sep_ids_edvp[$se_ca],$_SESSION['login_id'],$types);
													if($posts_event!="" && $posts_event!=NULL)
													{
														if($posts_event!="" && mysql_num_rows($posts_event)>0)
														{
															while($rows_posts_all = mysql_fetch_assoc($posts_event))
															{
																$ans_events_posts[] = $rows_posts_all;
															}
														}
													}
												}
											}
										}
										
										if($ans_events_posts!="" && count($ans_events_posts)>0)
										{
											for($vpev_all=0;$vpev_all<count($ans_events_posts);$vpev_all++)
											{
												if($ans_events_posts[$vpev_all]['profile_type']=='events_post')
												{
													$exp_meds = explode('~',$ans_events_posts[$vpev_all]['media']);
													
													if($ans_events_posts[$vpev_all]['profile_type']=='events_post')
													{
														$exp_meds = explode('~',$ans_events_posts[$vpev_all]['media']);
														$sep_ids_edv = $sql_feeds->featured_pro_eve($exp_meds[1],$exp_meds[0]);
													}
												}
												
												if(isset($sep_ids_edv) && $sep_ids_edv!='0' && $sep_ids_edv!=NULL)
												{
													$gens = $sql_feeds->get_log_ids($ans_events_posts[$vpev_all]['general_user_id']);
													if($ans_events_posts[$vpev_all]['display_to']=='All' && $ans_events_posts[$vpev_all]['profile_type']=='events_post')
													{
														$hide = 0;
														$get_gen_id = $sql_feeds->get_general_info();
														$hide_by = $ans_events_posts[$vpev_all]['hidden_by'];
														$exp_hide_by = explode(",",$hide_by);
														for($exp_count=0;$exp_count<=count($exp_hide_by);$exp_count++)
														{
															if($exp_hide_by[$exp_count] ==$get_gen_id['general_user_id'])
															{
																$hide =1;
															}
														}
														if($hide != 1)
														{
															$art = 0;
															if(isset($sep_ids_edv['artist_id']))
															{
																$sql_art = mysql_query("SELECT * FROM general_artist WHERE artist_id='".$gens['artist_id']."' AND status=0 AND active=0 AND del_status=0");
																if($sql_art!="" && $sql_art!=NULL)
																{
																	if(mysql_num_rows($sql_art)>0)
																	{
																		$ros = mysql_fetch_assoc($sql_art);
																		$art = 1;
																	}
																}
															}
															
															$com = 0;
															if(isset($sep_ids_edv['community_id']))
															{
																$sql_com = mysql_query("SELECT * FROM general_community WHERE community_id='".$gens['community_id']."' AND status=0 AND active=0 AND del_status=0");
																if($sql_com!="" && $sql_com!=NULL)
																{
																	if(mysql_num_rows($sql_com)>0)
																	{
																		$ros_com = mysql_fetch_assoc($sql_com);
																		$com = 1;
																	}
																}
															}
			?>											
														<div class="storyCont">
																	<div class="leftSide">
																		<div class="avatar">
		<?php
													if($art==1)
													{
														if($ros['profile_url']!="")
														{
													?>
														<a href="/<?php echo $ros['profile_url']; ?>">
													<?php
														}
														$get_news_general_user_id = $uk -> get_general_user_idby_artistid($ros['artist_id']);
													?>
														<img width="50" height="50" src="<?php
														if($ros['image_name']!="" && !empty($ros['image_name']))
														{
															echo $s3->getNewURI_otheruser("artjcropprofile",$get_news_general_user_id).$ros['image_name']; 
														}
														else
														{
															echo "http://artjcropprofile.s3.amazonaws.com/Noimage.png";
														}
													?>" />
													<?php
														if($ros['profile_url']!="")
														{
													?>
														</a>
													<?php
														}
													}
													elseif($com==1)
													{
														if($ros_com['profile_url']!="")
														{
													?>
														<a href="/<?php echo $ros_com['profile_url']; ?>">
													<?php
														}
														$get_news_general_user_id = $uk -> get_general_user_idby_communityid($ros_com['community_id']);
													?>
														<img width="50" height="50" src="<?php
													
													
													if($ros_com['image_name']!="" && !empty($ros_com['image_name']))
													{
														echo $s3->getNewURI_otheruser("comjcropprofile",$get_news_general_user_id).$ros_com['image_name']; 
													}
													else
													{
														echo "http://comjcropprofile.s3.amazonaws.com/Noimage.png";
													}
													
													?>" />
													<?php
														if($ros_com['profile_url']!="")
														{
													?>
														</a>
													<?php
														}
													}
													?>
																	</div>
																	<?php //echo "table=".$reg_table_media_type;
																	//echo $ans_audio_feeds[$audio_fed_lo]['id'];
																	?>
																		<div class="storyContainer">
																			<div class="storyTitle">
																				<span class="userName"><?php if($art==1)
																					{
																						if($ros['profile_url']!="")
														{
													?>
														<a href="/<?php echo $ros['profile_url']; ?>">
													<?php
														}
																						echo $ros['name'];
																						if($ros['profile_url']!="")
														{
													?>
														</a>
													<?php
														}
																					}
																					elseif($com==1)
																					{
																						if($ros_com['profile_url']!="")
														{
													?>
														<a href="/<?php echo $ros_com['profile_url']; ?>">
													<?php
														}
																						echo $ros_com['name'];
																						if($ros_com['profile_url']!="")
														{
													?>
														</a>
													<?php
														}
																					} ?></span> <?php// echo $ans_events_posts[$vpev_all]['description']; ?>
																			</div>
																			<div class="activityCont">
																				<div class="sideL">
																					<div class="storyImage">
		<?php
																	if($sep_ids_edv['profile_url']!="") {
		?>																				
																					<a href="/<?php echo $sep_ids_edv['profile_url']; ?>">
		<?php
																		}
		?>
																			<img width="90" height="90" src="<?php if($sep_ids_edv['image_name']!="") { echo $sep_ids_edv['image_name']; } else { echo "http://comjcropprofile.s3.amazonaws.com/Noimage.png"; }?>" />
		<?php
																	if($sep_ids_edv['image_name']!="") {
		?>
																			</a>
		<?php
																	}
		?>	
			
																			</a>
																		</div>
																					<div class="player">
																					<?php
																						if($sep_ids_edv['featured_media']!="" || $sep_ids_edv['featured_media']!="nodisplay"){
																						if($sep_ids_edv['featured_media'] == "Song" && $sep_ids_edv['media_id'] != 0){
																						?>
																							<img src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" onclick="showPlayer('a','<?php echo $sep_ids_edv['media_id'];?>','play');" />
																							<img src="images/profile/add.gif" onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'" onclick="showPlayer('a','<?php echo $sep_ids_edv['media_id'];?>','add');" />
																							<?php
																							$get_media_info_down_gal = $sql_feeds ->get_media_details($sep_ids_edv['media_id']);
																							if($get_media_info_down_gal['sharing_preference']==5 || $get_media_info_down_gal['sharing_preference']==2)
																							{
																								$song_image_name_cartproject = $sql_feeds->get_audio_img($get_media_info_down_gal['id']);
																								$free_down_rel ="";
																								for($find_down=0;$find_down<=count($get_songs_pnew_w);$find_down++)
																								{
																									//echo $get_songs_pnew_w[$find_down]['id'] ."==". $get_Song_title['id']."</br>";
																									if(isset($get_media_info_down_gal) && $get_media_info_down_gal != null)
																									{
																										if($get_songs_pnew_w[$find_down]['id']==$get_media_info_down_gal['id']){
																										$free_down_rel = "yes";
																										}
																										else{
																											if($free_down_rel =="" || $free_down_rel =="no"){
																												$free_down_rel = "no";
																											}
																										}
																									}
																								}
																								if($free_down_rel == "yes"){
																								 ?>
																								 <!--<a href="javascript:void(0);" onclick="download_directly_song('<?php echo $get_media_info_down_gal['id'];?>','<?php echo str_replace("'","\'",$get_media_info_down_gal['title']);?>','<?php echo str_replace("'","\'",$get_media_info_down_gal['creator']);?>','<?php echo str_replace("'","\'",$get_media_info_down_gal['from']);?>')"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>-->
																								<?php
																								}
																								else if($get_media_info_down_gal['sharing_preference']==2){
																								 ?>
																								<!--<a href="javascript:void(0);" onclick="ask_to_donate('<?php echo str_replace("'","\'",$get_media_info_down_gal['title']);?>','<?php echo str_replace("'","\'",$get_media_info_down_gal['creator']);?>','<?php echo $get_media_info_down_gal['id'];?>','download_songproject<?php echo $get_media_info_down_gal['id'];?>')"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>-->
																							 <?php
																								}
																								else{
																							?>
																								<!--<a href="javascript:void(0);" onclick="down_song_pop_project(<?php echo $get_media_info_down_gal['id'];?>)"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>-->
																							<?php
																								}
																							}else{
																							?>	
																							<!--<img src="../images/profile/download-over.gif" style="cursor:default" />-->
																							<?php
																							}
																							?>
																							<!--<a href="addtocart.php?seller_id=<?php echo $get_media_info_down_gal['general_user_id'];?>&media_id=<?php echo $get_media_info_down_gal['id'];?>&buyer_id=<?php echo $_SESSION['gen_user_id'];?>&title=<?php echo str_replace("'","\'",$get_media_info_down_gal['title']);?>&image_name=<?php echo $song_image_name_cartproject;?>" id="download_songproject<?php echo $get_media_info_down_gal['id'];?>" style="display:none;"></a>-->
																							<script type="text/javascript">
																							$(document).ready(function() {
																									$("#download_songproject<?php echo $get_media_info_down_gal['id'];?>").fancybox({
																									'width'				: '75%',
																									'height'			: '75%',
																									'transitionIn'		: 'none',
																									'transitionOut'		: 'none',
																									'type'				: 'iframe'
																									});
																							});
																							</script>
																						<?php
																						}if($sep_ids_edv['featured_media'] == "Gallery"  && $sep_ids_edv['media_id'] != 0){
																							$get_all_feature_image3 = $sql_feeds->get_all_gallery_images($sep_ids_edv['media_id']);
																							$play_array4 = array();
																							if($get_all_feature_image3!="" && $get_all_feature_image3!=NULL)
																{
																							if(mysql_num_rows($get_all_feature_image3)>0)
																							{
																								$mediaSrc3 = "";
																								$galleryImagesData3 = "";
																								$galleryImagestitle3 = "";
																								$get_news_general_user_id = $uk -> get_general_user_idby_mediaid($sep_ids_edv['media_id']);
																								while($row = mysql_fetch_assoc($get_all_feature_image3))
																								{
																									$mediaSrc3 = $row[0]['image_name'];
																									$filePath = $s3->getNewURI_otheruser("medgallery",$get_news_general_user_id).$mediaSrc3;
																									$galleryImagesData3 = $galleryImagesData3 .",". $row['image_name'];
																									$galleryImagestitle3 = $galleryImagestitle3 .",".$row['image_title'];
																								}
																								$orgPath3 = $s3->getNewURI_otheruser("medgallery",$get_news_general_user_id);
																								$thumbPath3 = $s3->getNewURI_otheruser("medgalthumb",$get_news_general_user_id);
																							}
																							}
																						?>
																							<a href="javascript:void(0);" onclick="checkload('<?php echo $mediaSrc3;?>','<?php echo $galleryImagesData3; ?>','<?php echo $thumbPath3; ?>','<?php echo $orgPath3; ?>','<?php echo $galleryImagestitle3; ?>','<?php echo $get_gallery['id'];?>')">
																							<img src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'"></a>
																							<img src="images/profile/add.gif" onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'">
																							<?php
																							$get_media_info_down_gal = $sql_feeds ->get_media_details($sep_ids_edv['media_id']);
																							$get_gal_image_project  = $sql_feeds ->get_gallery_info($sep_ids_edv['media_id']);
																							if($get_media_info_down_gal['sharing_preference']==2)
																							{
																							?>
																								<!--<a href="javascript:void(0);" onclick="ask_to_donate('<?php echo str_replace("'","\'",$get_media_info_down_gal['title']);?>','<?php echo str_replace("'","\'",$get_media_info_down_gal['creator']);?>','<?php echo $get_media_info_down_gal['id'];?>','download_galproject<?php echo $sep_ids_edv['media_id'];?>')"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>-->
																							 <?php
																							}else{
																							?>
																							<!--<img src="../images/profile/download-over.gif" style="cursor:default"/>-->
																							<?php
																							}
																							?>
																							<!--<a href="addtocart.php?seller_id=<?php echo $get_media_info_down_gal['general_user_id'];?>&media_id=<?php echo $get_media_info_down_gal['id'];?>&buyer_id=<?php echo $_SESSION['gen_user_id'];?>&title=<?php echo str_replace("'","\'",$get_media_info_down_gal['title']);?>&image_name=<?php echo $get_gal_image_project['profile_image'];?>" id="download_galproject<?php echo $sep_ids_edv['media_id'];?>" style="display:none;"></a>-->
																							<script type="text/javascript">
																							$(document).ready(function() {
																									$("#download_galproject<?php echo $sep_ids_edv['media_id'];?>").fancybox({
																									'width'				: '75%',
																									'height'			: '75%',
																									'transitionIn'		: 'none',
																									'transitionOut'		: 'none',
																									'type'				: 'iframe'
																									});
																							});
																							</script>
																						<?php
																						}if($sep_ids_edv['featured_media'] == "Video" && $sep_ids_edv['media_id'] != 0){
																						?>
																							<img src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" onclick="showPlayer('v','<?php echo $sep_ids_edv['media_id'];?>','playlist');"/>
																							<img src="images/profile/add.gif" onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'" onclick="showPlayer('v','<?php echo $sep_ids_edv['media_id'];?>','addvi');"/>
																							<?php
																							$get_media_info_down_gal = $sql_feeds ->get_media_details($sep_ids_edv['media_id']);
																							if($get_media_info_down_gal['sharing_preference']==5 || $get_media_info_down_gal['sharing_preference']==2)
																							{
																								$get_video_image = $sql_feeds->getvideoimage_feature($get_media_info_down_gal['id']);
																								//$image_name_cartproject = "https://img.youtube.com/vi/".$get_video_image."/default.jpg";
																								if($get_video_image[1]=='youtube')
																			{
																				$image_name_cartproject = "http://img.youtube.com/vi/".$get_video_image[0]."/default.jpg";
																			}
																			else
																			{
																				$video_vim = $vimeo->call('vimeo.videos.getInfo', array('video_id' => $get_video_image[0]));
																				$image_name_cartproject = $video_vim->video[0]->thumbnails->thumbnail[1]->_content;
																			}
																								$free_down_rel_video="";
																								for($find_down=0;$find_down<=count($get_vids_pnew_w);$find_down++)
																								{
																									if(isset($get_media_info_down_gal) && $get_media_info_down_gal != null){
																										if($get_vids_pnew_w[$find_down]['id']==$get_media_info_down_gal['id']){
																											$free_down_rel_video = "yes";
																										}
																										else{
																											if($free_down_rel_video =="" || $free_down_rel_video =="no"){
																												$free_down_rel_video = "no";
																											}
																										}
																									}
																								}
																								if($free_down_rel_video == "yes"){
																								 ?>
																								 <!--<a href="javascript:void(0);" onclick="download_directly_song('<?php echo $get_media_info_down_gal['id'];?>','<?php echo str_replace("'","\'",$get_media_info_down_gal['title']);?>','<?php echo str_replace("'","\'",$get_media_info_down_gal['creator']);?>','<?php echo str_replace("'","\'",$get_media_info_down_gal['from']);?>')"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>-->
																								 
																							 <?php
																								}
																								else if($get_media_info_down_gal['sharing_preference']==2){
																								 ?>
																									<!--<a href="javascript:void(0);" onclick="ask_to_donate('<?php echo str_replace("'","\'",$get_media_info_down_gal['title']);?>','<?php echo str_replace("'","\'",$get_media_info_down_gal['creator']);?>','<?php echo $get_media_info_down_gal['id'];?>','download_videoproject<?php echo $sep_ids_edv['media_id'];?>')"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>-->
																								<?php
																								}
																								else{
																							?>
																								<!--<a href="javascript:void(0);" onclick="down_video_pop_project(<?php echo $sep_ids_edv['media_id'];?>)"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>-->
																							<?php
																								}
																							}
																							else{
																							?>
																							<!--<img src="../images/profile/download-over.gif" style="cursor:default"/>-->
																							<?php
																							}
																							?>
																							<!--<a href="addtocart.php?seller_id=<?php echo $get_media_info_down_gal['general_user_id'];?>&media_id=<?php echo $get_media_info_down_gal['id'];?>&buyer_id=<?php echo $_SESSION['gen_user_id'];?>&title=<?php echo str_replace("'","\'",$get_media_info_down_gal['title']);?>&image_name=<?php echo $image_name_cartproject;?>" id="download_videoproject<?php echo $sep_ids_edv['media_id'];?>" style="display:none;"></a>-->
																							<script type="text/javascript">
																							$(document).ready(function() {
																									$("#download_videoproject<?php echo $sep_ids_edv['media_id'];?>").fancybox({
																									'height'			: '75%',
																									'width'				: '69%',
																									'transitionIn'		: 'none',
																									'transitionOut'		: 'none',
																									'type'				: 'iframe'
																									});
																							});
																							</script>
																						<?php
																						}
																					}
																					?>
																					</div>
																				</div>
																				<div class="sideR">
																					<div class="activityTitle"><?php
																					if($ans_events_posts[$vpev_all]['profile_type']=='events_post')
																					{
																				
																	if($sep_ids_edv['profile_url']!="") {
		?>																				
																					<a href="/<?php echo $sep_ids_edv['profile_url']; ?>">
		<?php
																		}
	
																						echo $sep_ids_edv['title'];
																						
																						
																	if($sep_ids_edv['profile_url']!="") {
		?>																				
																					</a>
		<?php
																		}
																					}
																					?>
																						<div style="color:#808080;font-size:12px;">
																							<div class="comment more">
																								<?php echo $ans_events_posts[$vpev_all]['description']; ?>
																							</div>
																						</div>
																					</div>
																					<!--<div class="activityContent">Tagged: <?php //echo $medias['tagged_users']; 
																						//$name_org = explode(')',$medias['tagged_users']);
																						//for($i_name_org=0;$i_name_org<count($name_org);$i_name_org++)
																						//{
																						//	$end_pos = strpos($name_org[$i_name_org],'(');
																						//	$org_name = substr($name_org[$i_name_org],0,$end_pos);
																					//		echo $org_name;
																					//	}
																					?></div>-->
																				</div>
																			</div>
																		</div>
																	</div>
																	<div class="rightSide">
																		<a href="#" onmouseover="mopen('mm<?php echo $vpev_all;?>17')" onmouseout="mclosetime()"><img src="images/profile/arrow.png" onmouseover="this.src='images/profile/arrow-down.png'" onmouseout="this.src='images/profile/arrow.png'" /></a>
																		<div id="mm<?php echo $vpev_all;?>17" class="popUp" onmouseover="mcancelclosetime()" onmouseout="mclosetime()">
																			<ul>
																				<li><a href="hideposts.php?id=<?php echo $ans_events_posts[$vpev_all]['id'];?>">Hide Feed</a></li>
																				<li><a href="hideposts.php?report_id=<?php echo $ans_all_posts[$po_all]['id'];?>">Report spam</a></li>
																				<li><div class="seperator"></div></li>
																				<li><a href="">Unsubscribe from <?php echo $gens['fname'].' '.$gens['lname']; ?></a></li>
																			</ul>
																		</div>
																	</div>
																</div>
			<?php
														}
													}
													elseif($ans_events_posts[$vpev_all]['display_to']=='Fans')
													{
														if($gens!="")
														{
															$fans_all =	$sql_feeds->fans_cond($chk_sql['email'],$gens['community_id'],$gens['artist_id']);
															if($fans_all=='1')
															{
																$hide1 = 0;
																$get_gen_id = $sql_feeds->get_general_info();
																$hide_by = $ans_events_posts[$vpev_all]['hidden_by'];
																$exp_hide_by = explode(",",$hide_by);
																for($exp_count=0;$exp_count<=count($exp_hide_by);$exp_count++)
																{
																	if($exp_hide_by[$exp_count] ==$get_gen_id['general_user_id'])
																	{
																		$hide1 =1;
																	}
																}
																if($hide1 != 1)
																{
																	$art = 0;
																	if(isset($sep_ids_edv['artist_id']))
																	{
																		$sql_art = mysql_query("SELECT * FROM general_artist WHERE artist_id='".$gens['artist_id']."' AND status=0 AND active=0 AND del_status=0");
																		if($sql_art!="" && $sql_art!=NULL)
																		{
																			if(mysql_num_rows($sql_art)>0)
																			{
																				$ros = mysql_fetch_assoc($sql_art);
																				$art = 1;
																			}
																		}
																	}
																	
																	$com = 0;
																	if(isset($sep_ids_edv['community_id']))
																	{
																		$sql_com = mysql_query("SELECT * FROM general_community WHERE community_id='".$gens['community_id']."' AND status=0 AND active=0 AND del_status=0");
																		if($sql_com!="" && $sql_com!=NULL)
																		{
																			if(mysql_num_rows($sql_com)>0)
																			{
																				$ros_com = mysql_fetch_assoc($sql_com);
																				$com = 1;
																			}
																		}
																	}
			?>
																<div class="storyCont">
																	<div class="leftSide">
																		<div class="avatar">
																			<?php
													if($art==1)
													{
														if($ros['profile_url']!="")
														{
													?>
														<a href="/<?php echo $ros['profile_url']; ?>">
													<?php
														}
														$get_news_general_user_id = $uk -> get_general_user_idby_artistid($ros['artist_id']);
													?>
														<img width="50" height="50" src="<?php
														if($ros['image_name']!="" && !empty($ros['image_name']))
														{
															echo $s3->getNewURI_otheruser("artjcropprofile",$get_news_general_user_id).$ros['image_name']; 
														}
														else
														{
															echo "http://artjcropprofile.s3.amazonaws.com/Noimage.png";
														}
													?>" />
													<?php
														if($ros['profile_url']!="")
														{
													?>
														</a>
													<?php
														}
													}
													elseif($com==1)
													{
														if($ros_com['profile_url']!="")
														{
													?>
														<a href="/<?php echo $ros_com['profile_url']; ?>">
													<?php
														}
														$get_news_general_user_id = $uk -> get_general_user_idby_communityid($ros_com['community_id']);
													?>
														<img width="50" height="50" src="<?php
													
													
													if($ros_com['image_name']!="" && !empty($ros_com['image_name']))
													{
														echo $s3->getNewURI_otheruser("comjcropprofile",$get_news_general_user_id).$ros_com['image_name']; 
													}
													else
													{
														echo "http://comjcropprofile.s3.amazonaws.com/Noimage.png";
													}
													
													?>" />
													<?php
														if($ros_com['profile_url']!="")
														{
													?>
														</a>
													<?php
														}
													}
													?>
																	</div>
																	<?php //echo "table=".$reg_table_media_type;
																	//echo $ans_audio_feeds[$audio_fed_lo]['id'];
																	?>
																		<div class="storyContainer">
																			<div class="storyTitle">
																				<span class="userName"><?php if($art==1)
																					{
																					if($ros['profile_url']!="")
														{
													?>
														<a href="/<?php echo $ros['profile_url']; ?>">
													<?php
														}
																						echo $ros['name'];
																						if($ros['profile_url']!="")
														{
													?>
														</a>
													<?php
														}
																					}
																					elseif($com==1)
																					{
																						if($ros_com['profile_url']!="")
														{
													?>
														<a href="/<?php echo $ros_com['profile_url']; ?>">
													<?php
														}
																						echo $ros_com['name'];
																						if($ros_com['profile_url']!="")
														{
													?>
														</a>
													<?php
														}
																					} ?></span>  <?php //echo $ans_events_posts[$vpev_all]['description']; ?>
																			</div>
																			<div class="activityCont">
																				<div class="sideL">
																					<div class="storyImage">
	<?php																				if($sep_ids_edv['profile_url']!="") {
		?>																				
																					<a href="/<?php echo $sep_ids_edv['profile_url']; ?>">
		<?php
																		}?><img width="90" height="90" src="<?php if($sep_ids_edv['image_name']!="") { echo $sep_ids_edv['image_name']; } else { echo "http://comjcropprofile.s3.amazonaws.com/Noimage.png"; }?>" />
		<?php
																			if($sep_ids_edv['profile_url']!="") {
		?>																				
																					</a>
		<?php
																		}
																		?>
																		</div>
																					<div class="player">
																					<?php
																						if($sep_ids_edv['featured_media']!="" || $sep_ids_edv['featured_media']!="nodisplay"){
																						if($sep_ids_edv['featured_media'] == "Song" && $sep_ids_edv['media_id'] != 0){
																						?>
																							<img src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" onclick="showPlayer('a','<?php echo $sep_ids_edv['media_id'];?>','play');" />
																							<img src="images/profile/add.gif" onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'" onclick="showPlayer('a','<?php echo $sep_ids_edv['media_id'];?>','add');" />
																							<?php
																							$get_media_info_down_gal = $sql_feeds ->get_media_details($sep_ids_edv['media_id']);
																							if($get_media_info_down_gal['sharing_preference']==5 || $get_media_info_down_gal['sharing_preference']==2)
																							{
																								$song_image_name_cartproject = $sql_feeds->get_audio_img($get_media_info_down_gal['id']);
																								$free_down_rel ="";
																								for($find_down=0;$find_down<=count($get_songs_pnew_w);$find_down++)
																								{
																									//echo $get_songs_pnew_w[$find_down]['id'] ."==". $get_Song_title['id']."</br>";
																									if(isset($get_media_info_down_gal) && $get_media_info_down_gal != null)
																									{
																										if($get_songs_pnew_w[$find_down]['id']==$get_media_info_down_gal['id']){
																										$free_down_rel = "yes";
																										}
																										else{
																											if($free_down_rel =="" || $free_down_rel =="no"){
																												$free_down_rel = "no";
																											}
																										}
																									}
																								}
																								if($free_down_rel == "yes"){
																								 ?>
																								 <!--<a href="javascript:void(0);" onclick="download_directly_song('<?php echo $get_media_info_down_gal['id'];?>','<?php echo str_replace("'","\'",$get_media_info_down_gal['title']);?>','<?php echo str_replace("'","\'",$get_media_info_down_gal['creator']);?>','<?php echo str_replace("'","\'",$get_media_info_down_gal['from']);?>')"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>-->
																								<?php
																								}
																								else if($get_media_info_down_gal['sharing_preference']==2){
																								 ?>
																								<!--<a href="javascript:void(0);" onclick="ask_to_donate('<?php echo str_replace("'","\'",$get_media_info_down_gal['title']);?>','<?php echo str_replace("'","\'",$get_media_info_down_gal['creator']);?>','<?php echo $get_media_info_down_gal['id'];?>','download_songproject<?php echo $get_media_info_down_gal['id'];?>')"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>-->
																							 <?php
																								}
																								else{
																							?>
																								<!--<a href="javascript:void(0);" onclick="down_song_pop_project(<?php echo $get_media_info_down_gal['id'];?>)"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>-->
																							<?php
																								}
																							}else{
																							?>	
																							<!--<img src="../images/profile/download-over.gif" style="cursor:default" />-->
																							<?php
																							}
																							?>
																							<!--<a href="addtocart.php?seller_id=<?php echo $get_media_info_down_gal['general_user_id'];?>&media_id=<?php echo $get_media_info_down_gal['id'];?>&buyer_id=<?php echo $_SESSION['gen_user_id'];?>&title=<?php echo str_replace("'","\'",$get_media_info_down_gal['title']);?>&image_name=<?php echo $song_image_name_cartproject;?>" id="download_songproject<?php echo $get_media_info_down_gal['id'];?>" style="display:none;"></a>-->
																							<script type="text/javascript">
																							$(document).ready(function() {
																									$("#download_songproject<?php echo $get_media_info_down_gal['id'];?>").fancybox({
																									'width'				: '75%',
																									'height'			: '75%',
																									'transitionIn'		: 'none',
																									'transitionOut'		: 'none',
																									'type'				: 'iframe'
																									});
																							});
																							</script>
																						<?php
																						}if($sep_ids_edv['featured_media'] == "Gallery"  && $sep_ids_edv['media_id'] != 0){
																							$get_all_feature_image3 = $sql_feeds->get_all_gallery_images($sep_ids_edv['media_id']);
																							if($get_all_feature_image3!="" && $get_all_feature_image3!=NULL)
																		{
																							if(mysql_num_rows($get_all_feature_image3)>0)
																							{
																								$mediaSrc3 = "";
																								$galleryImagesData3 = "";
																								$galleryImagestitle3 = "";
																								$get_news_general_user_id = $uk -> get_general_user_idby_mediaid($sep_ids_edv['media_id']);
																								while($row = mysql_fetch_assoc($get_all_feature_image3))
																								{
																									$mediaSrc3 = $row[0]['image_name'];
																									$filePath = $s3->getNewURI_otheruser("medgallery",$get_news_general_user_id).$mediaSrc3;
																									$galleryImagesData3 = $galleryImagesData3 .",". $row['image_name'];
																									$galleryImagestitle3 = $galleryImagestitle3 .",".$row['image_title'];
																								}
																								$orgPath3 = $s3->getNewURI_otheruser("medgallery",$get_news_general_user_id);
																								$thumbPath3 = $s3->getNewURI_otheruser("medgalthumb",$get_news_general_user_id);
																							}
																							}
																						?>
																							<a href="javascript:void(0);" onclick="checkload('<?php echo $mediaSrc3;?>','<?php echo $galleryImagesData3; ?>','<?php echo $thumbPath3; ?>','<?php echo $orgPath3; ?>','<?php echo $galleryImagestitle3; ?>','<?php echo $get_gallery['id'];?>')">
																							<img src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'"></a>
																							<img src="images/profile/add.gif" onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'">
																							<?php
																							$get_media_info_down_gal = $sql_feeds ->get_media_details($sep_ids_edv['media_id']);
																							$get_gal_image_project  = $sql_feeds ->get_gallery_info($sep_ids_edv['media_id']);
																							if($get_media_info_down_gal['sharing_preference']==2)
																							{
																							?>
																								<!--<a href="javascript:void(0);" onclick="ask_to_donate('<?php echo str_replace("'","\'",$get_media_info_down_gal['title']);?>','<?php echo str_replace("'","\'",$get_media_info_down_gal['creator']);?>','<?php echo $get_media_info_down_gal['id'];?>','download_galproject<?php echo $sep_ids_edv['media_id'];?>')"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>-->
																							 <?php
																							}else{
																							?>
																							<!--<img src="../images/profile/download-over.gif" style="cursor:default"/>-->
																							<?php
																							}
																							?>
																				<!--			<a href="addtocart.php?seller_id=<?php echo $get_media_info_down_gal['general_user_id'];?>&media_id=<?php echo $get_media_info_down_gal['id'];?>&buyer_id=<?php echo $_SESSION['gen_user_id'];?>&title=<?php echo str_replace("'","\'",$get_media_info_down_gal['title']);?>&image_name=<?php echo $get_gal_image_project['profile_image'];?>" id="download_galproject<?php echo $sep_ids_edv['media_id'];?>" style="display:none;"></a>-->
																							<script type="text/javascript">
																							$(document).ready(function() {
																									$("#download_galproject<?php echo $sep_ids_edv['media_id'];?>").fancybox({
																									'width'				: '75%',
																									'height'			: '75%',
																									'transitionIn'		: 'none',
																									'transitionOut'		: 'none',
																									'type'				: 'iframe'
																									});
																							});
																							</script>
																						<?php
																						}if($sep_ids_edv['featured_media'] == "Video" && $sep_ids_edv['media_id'] != 0){
																						?>
																							<img src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" onclick="showPlayer('v','<?php echo $sep_ids_edv['media_id'];?>','playlist');"/>
																							<img src="images/profile/add.gif" onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'" onclick="showPlayer('v','<?php echo $sep_ids_edv['media_id'];?>','addvi');"/>
																							<?php
																							$get_media_info_down_gal = $sql_feeds ->get_media_details($sep_ids_edv['media_id']);
																							if($get_media_info_down_gal['sharing_preference']==5 || $get_media_info_down_gal['sharing_preference']==2)
																							{
																								$get_video_image = $sql_feeds->getvideoimage_feature($get_media_info_down_gal['id']);
																								//$image_name_cartproject = "https://img.youtube.com/vi/".$get_video_image."/default.jpg";
																								if($get_video_image[1]=='youtube')
																			{
																				$image_name_cartproject = "http://img.youtube.com/vi/".$get_video_image[0]."/default.jpg";
																			}
																			else
																			{
																				$video_vim = $vimeo->call('vimeo.videos.getInfo', array('video_id' => $get_video_image[0]));
																				$image_name_cartproject = $video_vim->video[0]->thumbnails->thumbnail[1]->_content;
																			}
																								$free_down_rel_video="";
																								for($find_down=0;$find_down<=count($get_vids_pnew_w);$find_down++)
																								{
																									if(isset($get_media_info_down_gal) && $get_media_info_down_gal != null){
																										if($get_vids_pnew_w[$find_down]['id']==$get_media_info_down_gal['id']){
																											$free_down_rel_video = "yes";
																										}
																										else{
																											if($free_down_rel_video =="" || $free_down_rel_video =="no"){
																												$free_down_rel_video = "no";
																											}
																										}
																									}
																								}
																								if($free_down_rel_video == "yes"){
																								 ?>
																								 <!--<a href="javascript:void(0);" onclick="download_directly_song('<?php echo $get_media_info_down_gal['id'];?>','<?php echo str_replace("'","\'",$get_media_info_down_gal['title']);?>','<?php echo str_replace("'","\'",$get_media_info_down_gal['creator']);?>','<?php echo str_replace("'","\'",$get_media_info_down_gal['from']);?>')"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>-->
																								 
																							 <?php
																								}
																								else if($get_media_info_down_gal['sharing_preference']==2){
																								 ?>
																									<!--<a href="javascript:void(0);" onclick="ask_to_donate('<?php echo str_replace("'","\'",$get_media_info_down_gal['title']);?>','<?php echo str_replace("'","\'",$get_media_info_down_gal['creator']);?>','<?php echo $get_media_info_down_gal['id'];?>','download_videoproject<?php echo $sep_ids_edv['media_id'];?>')"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>-->
																								<?php
																								}
																								else{
																							?>
																								<!--<a href="javascript:void(0);" onclick="down_video_pop_project(<?php echo $sep_ids_edv['media_id'];?>)"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>-->
																							<?php
																								}
																							}
																							else{
																							?>
																							<!--<img src="../images/profile/download-over.gif" style="cursor:default"/>-->
																							<?php
																							}
																							?>
																							<!--<a href="addtocart.php?seller_id=<?php echo $get_media_info_down_gal['general_user_id'];?>&media_id=<?php echo $get_media_info_down_gal['id'];?>&buyer_id=<?php echo $_SESSION['gen_user_id'];?>&title=<?php echo str_replace("'","\'",$get_media_info_down_gal['title']);?>&image_name=<?php echo $image_name_cartproject;?>" id="download_videoproject<?php echo $sep_ids_edv['media_id'];?>" style="display:none;"></a>-->
																							<script type="text/javascript">
																							$(document).ready(function() {
																									$("#download_videoproject<?php echo $sep_ids_edv['media_id'];?>").fancybox({
																									'height'			: '75%',
																									'width'				: '69%',
																									'transitionIn'		: 'none',
																									'transitionOut'		: 'none',
																									'type'				: 'iframe'
																									});
																							});
																							</script>
																						<?php
																						}
																					}
																					?>
																					</div>
																				</div>
																				<div class="sideR">
																					<div class="activityTitle"><?php
																					if($ans_events_posts[$vpev_all]['profile_type']=='events_post')
																					{
																					if($sep_ids_edv['profile_url']!="") {
		?>																				
																					<a href="/<?php echo $sep_ids_edv['profile_url']; ?>">
		<?php
																		}
																						echo $sep_ids_edv['title'];
																						if($sep_ids_edv['profile_url']!="") {
		?>																				
																					</a>
		<?php
																		}
																					}?>
																						<div style="color:#808080;font-size:12px;">
																							<div class="comment more">
																								<?php echo $ans_events_posts[$vpev_all]['description']; ?>
																							</div>
																						</div>
																					</div>
																					<!--<div class="activityContent">Tagged: <?php //echo $medias['tagged_users']; 
																						//$name_org = explode(')',$medias['tagged_users']);
																						//for($i_name_org=0;$i_name_org<count($name_org);$i_name_org++)
																						//{
																						//	$end_pos = strpos($name_org[$i_name_org],'(');
																						//	$org_name = substr($name_org[$i_name_org],0,$end_pos);
																					//		echo $org_name;
																					//	}
																					?></div>-->
																				</div>
																			</div>
																		</div>
																	</div>
																	<div class="rightSide">
																		<a href="#" onmouseover="mopen('mm<?php echo $vpev_all;?>18')" onmouseout="mclosetime()"><img src="images/profile/arrow.png" onmouseover="this.src='images/profile/arrow-down.png'" onmouseout="this.src='images/profile/arrow.png'" /></a>
																		<div id="mm<?php echo $vpev_all;?>18" class="popUp" onmouseover="mcancelclosetime()" onmouseout="mclosetime()">
																			<ul>
																				<li><a href="hideposts.php?id=<?php echo $ans_events_posts[$vpev_all]['id'];?>">Hide Feed</a></li>
																				<li><a href="hideposts.php?report_id=<?php echo $ans_all_posts[$po_all]['id'];?>">Report spam</a></li>
																				<li><div class="seperator"></div></li>
																				<li><a href="">Unsubscribe from <?php echo $gens['fname'].' '.$gens['lname']; ?></a></li>
																			</ul>
																		</div>
																	</div>
																</div>
			<?php
																}
															}
														}
													}
													elseif($ans_events_posts[$vpev_all]['display_to']=='Subscribers')
													{
														if($gens!="")
														{
															$subscribers_all =	$sql_feeds->subscribers_cond($chk_sql['email'],$gens['community_id'],$gens['artist_id']);
															if($subscribers_all=='1')
															{
																$hide2 = 0;
																$get_gen_id = $sql_feeds->get_general_info();
																$hide_by = $ans_events_posts[$vpev_all]['hidden_by'];
																$exp_hide_by = explode(",",$hide_by);
																for($exp_count=0;$exp_count<=count($exp_hide_by);$exp_count++)
																{
																	if($exp_hide_by[$exp_count] ==$get_gen_id['general_user_id'])
																	{
																		$hide2 =1;
																	}
																}
																if($hide2 != 1)
																{
																	$art = 0;
																	if(isset($sep_ids_edv['artist_id']))
																	{
																		$sql_art = mysql_query("SELECT * FROM general_artist WHERE artist_id='".$gens['artist_id']."' AND status=0 AND active=0 AND del_status=0");
																		if($sql_art!="" && $sql_art!=NULL)
																		{
																			if(mysql_num_rows($sql_art)>0)
																			{
																				$ros = mysql_fetch_assoc($sql_art);
																				$art = 1;
																			}
																		}
																	}
																	
																	$com = 0;
																	if(isset($sep_ids_edv['community_id']))
																	{
																		$sql_com = mysql_query("SELECT * FROM general_community WHERE community_id='".$gens['community_id']."' AND status=0 AND active=0 AND del_status=0");
																		if($sql_com!="" && $sql_com!=NULL)
																		{
																			if(mysql_num_rows($sql_com)>0)
																			{
																				$ros_com = mysql_fetch_assoc($sql_com);
																				$com = 1;
																			}
																		}
																	}
			?>
																<div class="storyCont">
																	<div class="leftSide">
																		<div class="avatar">
																			<?php
													if($art==1)
													{
														if($ros['profile_url']!="")
														{
													?>
														<a href="/<?php echo $ros['profile_url']; ?>">
													<?php
														}
														$get_news_general_user_id = $uk -> get_general_user_idby_artistid($ros['artist_id']);
													?>
														<img width="50" height="50" src="<?php
														if($ros['image_name']!="" && !empty($ros['image_name']))
														{
															echo $s3->getNewURI_otheruser("artjcropprofile",$get_news_general_user_id).$ros['image_name']; 
														}
														else
														{
															echo "http://artjcropprofile.s3.amazonaws.com/Noimage.png";
														}
													?>" />
													<?php
														if($ros['profile_url']!="")
														{
													?>
														</a>
													<?php
														}
													}
													elseif($com==1)
													{
														if($ros_com['profile_url']!="")
														{
													?>
														<a href="/<?php echo $ros_com['profile_url']; ?>">
													<?php
														}
														$get_news_general_user_id = $uk -> get_general_user_idby_communityid($ros_com['community_id']);
													?>
														<img width="50" height="50" src="<?php
													
													
													if($ros_com['image_name']!="" && !empty($ros_com['image_name']))
													{
														echo $s3->getNewURI_otheruser("comjcropprofile",$get_news_general_user_id).$ros_com['image_name']; 
													}
													else
													{
														echo "http://comjcropprofile.s3.amazonaws.com/Noimage.png";
													}
													
													?>" />
													<?php
														if($ros_com['profile_url']!="")
														{
													?>
														</a>
													<?php
														}
													}
													?>
																	</div>
																	<?php //echo "table=".$reg_table_media_type;
																	//echo $ans_audio_feeds[$audio_fed_lo]['id'];
																	?>
																		<div class="storyContainer">
																			<div class="storyTitle">
																				<span class="userName"><?php if($art==1)
																					{
																					if($ros['profile_url']!="")
														{
													?>
														<a href="/<?php echo $ros['profile_url']; ?>">
													<?php
														}
																						echo $ros['name'];
																						if($ros['profile_url']!="")
														{
													?>
														</a>
													<?php
														}
																					}
																					elseif($com==1)
																					{
																					if($ros_com['profile_url']!="")
														{
													?>
														<a href="/<?php echo $ros_com['profile_url']; ?>">
													<?php
														}
																						echo $ros_com['name'];
																						if($ros_com['profile_url']!="")
														{
													?>
														</a>
													<?php
														}
																					} ?></span>  <?php //echo $ans_events_posts[$vpev_all]['description']; ?>
																			</div>
																			<div class="activityCont">
																				<div class="sideL">
																					<div class="storyImage">
		<?php																			if($sep_ids_edv['profile_url']!="") {
		?>																				
																					<a href="/<?php echo $sep_ids_edv['profile_url']; ?>">
		<?php
																		}
																		?>
																					<img width="90" height="90" src="<?php if($sep_ids_edv['image_name']!="") { echo $sep_ids_edv['image_name']; } else { echo "http://comjcropprofile.s3.amazonaws.com/Noimage.png"; }?>" />
		<?php																			if($sep_ids_edv['profile_url']!="") {
		?>																				
																					</a>
		<?php
																		}
																		?>
																					</div>
																					<div class="player">
																					<?php
																						if($sep_ids_edv['featured_media']!="" || $sep_ids_edv['featured_media']!="nodisplay"){
																						if($sep_ids_edv['featured_media'] == "Song" && $sep_ids_edv['media_id'] != 0){
																						?>
																							<img src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" onclick="showPlayer('a','<?php echo $sep_ids_edv['media_id'];?>','play');" />
																							<img src="images/profile/add.gif" onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'" onclick="showPlayer('a','<?php echo $sep_ids_edv['media_id'];?>','add');" />
																							<?php
																							$get_media_info_down_gal = $sql_feeds ->get_media_details($sep_ids_edv['media_id']);
																							if($get_media_info_down_gal['sharing_preference']==5 || $get_media_info_down_gal['sharing_preference']==2)
																							{
																								$song_image_name_cartproject = $sql_feeds->get_audio_img($get_media_info_down_gal['id']);
																								$free_down_rel ="";
																								for($find_down=0;$find_down<=count($get_songs_pnew_w);$find_down++)
																								{
																									//echo $get_songs_pnew_w[$find_down]['id'] ."==". $get_Song_title['id']."</br>";
																									if(isset($get_media_info_down_gal) && $get_media_info_down_gal != null)
																									{
																										if($get_songs_pnew_w[$find_down]['id']==$get_media_info_down_gal['id']){
																										$free_down_rel = "yes";
																										}
																										else{
																											if($free_down_rel =="" || $free_down_rel =="no"){
																												$free_down_rel = "no";
																											}
																										}
																									}
																								}
																								if($free_down_rel == "yes"){
																								 ?>
																								<!--<a href="javascript:void(0);" onclick="download_directly_song('<?php echo $get_media_info_down_gal['id'];?>','<?php echo str_replace("'","\'",$get_media_info_down_gal['title']);?>','<?php echo str_replace("'","\'",$get_media_info_down_gal['creator']);?>','<?php echo str_replace("'","\'",$get_media_info_down_gal['from']);?>')"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>-->
																								<?php
																								}
																								else if($get_media_info_down_gal['sharing_preference']==2){
																								 ?>
																								<!--<a href="javascript:void(0);" onclick="ask_to_donate('<?php echo str_replace("'","\'",$get_media_info_down_gal['title']);?>','<?php echo str_replace("'","\'",$get_media_info_down_gal['creator']);?>','<?php echo $get_media_info_down_gal['id'];?>','download_songproject<?php echo $get_media_info_down_gal['id'];?>')"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>-->
																							 <?php
																								}
																								else{
																							?>
																								<!--<a href="javascript:void(0);" onclick="down_song_pop_project(<?php echo $get_media_info_down_gal['id'];?>)"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>-->
																							<?php
																								}
																							}else{
																							?>	
																							<!--<img src="../images/profile/download-over.gif" style="cursor:default" />-->
																							<?php
																							}
																							?>
																							<!--<a href="addtocart.php?seller_id=<?php echo $get_media_info_down_gal['general_user_id'];?>&media_id=<?php echo $get_media_info_down_gal['id'];?>&buyer_id=<?php echo $_SESSION['gen_user_id'];?>&title=<?php echo str_replace("'","\'",$get_media_info_down_gal['title']);?>&image_name=<?php echo $song_image_name_cartproject;?>" id="download_songproject<?php echo $get_media_info_down_gal['id'];?>" style="display:none;"></a>-->
																							<script type="text/javascript">
																							$(document).ready(function() {
																									$("#download_songproject<?php echo $get_media_info_down_gal['id'];?>").fancybox({
																									'width'				: '75%',
																									'height'			: '75%',
																									'transitionIn'		: 'none',
																									'transitionOut'		: 'none',
																									'type'				: 'iframe'
																									});
																							});
																							</script>
																						<?php
																						}if($sep_ids_edv['featured_media'] == "Gallery"  && $sep_ids_edv['media_id'] != 0){
																							$get_all_feature_image3 = $sql_feeds->get_all_gallery_images($sep_ids_edv['media_id']);
																							if($get_all_feature_image3!="" && $get_all_feature_image3!=NULL)
																		{
																							if(mysql_num_rows($get_all_feature_image3)>0)
																							{
																								$mediaSrc3 = "";
																								$galleryImagesData3 = "";
																								$galleryImagestitle3 = "";
																								$get_news_general_user_id = $uk -> get_general_user_idby_mediaid($sep_ids_edv['media_id']);
																								while($row = mysql_fetch_assoc($get_all_feature_image3))
																								{
																									$mediaSrc3 = $row[0]['image_name'];
																									$filePath = $s3->getNewURI_otheruser("medgallery",$get_news_general_user_id).$mediaSrc3;
																									$galleryImagesData3 = $galleryImagesData3 .",". $row['image_name'];
																									$galleryImagestitle3 = $galleryImagestitle3 .",".$row['image_title'];
																								}
																								$orgPath3 = $s3->getNewURI_otheruser("medgallery",$get_news_general_user_id);
																								$thumbPath3 = $s3->getNewURI_otheruser("medgalthumb",$get_news_general_user_id);
																							}
																							}
																						?>
																							<a href="javascript:void(0);" onclick="checkload('<?php echo $mediaSrc3;?>','<?php echo $galleryImagesData3; ?>','<?php echo $thumbPath3; ?>','<?php echo $orgPath3; ?>','<?php echo $galleryImagestitle3; ?>','<?php echo $get_gallery['id'];?>')">
																							<img src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'"></a>
																							<img src="images/profile/add.gif" onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'">
																							<?php
																							$get_media_info_down_gal = $sql_feeds ->get_media_details($sep_ids_edv['media_id']);
																							$get_gal_image_project  = $sql_feeds ->get_gallery_info($sep_ids_edv['media_id']);
																							if($get_media_info_down_gal['sharing_preference']==2)
																							{
																							?>
																								<!--<a href="javascript:void(0);" onclick="ask_to_donate('<?php echo str_replace("'","\'",$get_media_info_down_gal['title']);?>','<?php echo str_replace("'","\'",$get_media_info_down_gal['creator']);?>','<?php echo $get_media_info_down_gal['id'];?>','download_galproject<?php echo $sep_ids_edv['media_id'];?>')"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>-->
																							 <?php
																							}else{
																							?>
																							<!--<img src="../images/profile/download-over.gif" style="cursor:default"/>-->
																							<?php
																							}
																							?>
																							<!--<a href="addtocart.php?seller_id=<?php echo $get_media_info_down_gal['general_user_id'];?>&media_id=<?php echo $get_media_info_down_gal['id'];?>&buyer_id=<?php echo $_SESSION['gen_user_id'];?>&title=<?php echo str_replace("'","\'",$get_media_info_down_gal['title']);?>&image_name=<?php echo $get_gal_image_project['profile_image'];?>" id="download_galproject<?php echo $sep_ids_edv['media_id'];?>" style="display:none;"></a>-->
																							<script type="text/javascript">
																							$(document).ready(function() {
																									$("#download_galproject<?php echo $sep_ids_edv['media_id'];?>").fancybox({
																									'width'				: '75%',
																									'height'			: '75%',
																									'transitionIn'		: 'none',
																									'transitionOut'		: 'none',
																									'type'				: 'iframe'
																									});
																							});
																							</script>
																						<?php
																						}if($sep_ids_edv['featured_media'] == "Video" && $sep_ids_edv['media_id'] != 0){
																						?>
																							<img src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" onclick="showPlayer('v','<?php echo $sep_ids_edv['media_id'];?>','playlist');"/>
																							<img src="images/profile/add.gif" onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'" onclick="showPlayer('v','<?php echo $sep_ids_edv['media_id'];?>','addvi');"/>
																							<?php
																							$get_media_info_down_gal = $sql_feeds ->get_media_details($sep_ids_edv['media_id']);
																							if($get_media_info_down_gal['sharing_preference']==5 || $get_media_info_down_gal['sharing_preference']==2)
																							{
																								$get_video_image = $sql_feeds->getvideoimage_feature($get_media_info_down_gal['id']);
																								//$image_name_cartproject = "https://img.youtube.com/vi/".$get_video_image."/default.jpg";
																								if($get_video_image[1]=='youtube')
																			{
																				$image_name_cartproject = "http://img.youtube.com/vi/".$get_video_image[0]."/default.jpg";
																			}
																			else
																			{
																				$video_vim = $vimeo->call('vimeo.videos.getInfo', array('video_id' => $get_video_image[0]));
																				$image_name_cartproject = $video_vim->video[0]->thumbnails->thumbnail[1]->_content;
																			}
																								$free_down_rel_video="";
																								for($find_down=0;$find_down<=count($get_vids_pnew_w);$find_down++)
																								{
																									if(isset($get_media_info_down_gal) && $get_media_info_down_gal != null){
																										if($get_vids_pnew_w[$find_down]['id']==$get_media_info_down_gal['id']){
																											$free_down_rel_video = "yes";
																										}
																										else{
																											if($free_down_rel_video =="" || $free_down_rel_video =="no"){
																												$free_down_rel_video = "no";
																											}
																										}
																									}
																								}
																								if($free_down_rel_video == "yes"){
																								 ?>
																								 <!--<a href="javascript:void(0);" onclick="download_directly_song('<?php echo $get_media_info_down_gal['id'];?>','<?php echo str_replace("'","\'",$get_media_info_down_gal['title']);?>','<?php echo str_replace("'","\'",$get_media_info_down_gal['creator']);?>','<?php echo str_replace("'","\'",$get_media_info_down_gal['from']);?>')"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>-->
																								 
																							 <?php
																								}
																								else if($get_media_info_down_gal['sharing_preference']==2){
																								 ?>
																									<!--<a href="javascript:void(0);" onclick="ask_to_donate('<?php echo str_replace("'","\'",$get_media_info_down_gal['title']);?>','<?php echo str_replace("'","\'",$get_media_info_down_gal['creator']);?>','<?php echo $get_media_info_down_gal['id'];?>','download_videoproject<?php echo $sep_ids_edv['media_id'];?>')"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>-->
																								<?php
																								}
																								else{
																							?>
																								<!--<a href="javascript:void(0);" onclick="down_video_pop_project(<?php echo $sep_ids_edv['media_id'];?>)"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>-->
																							<?php
																								}
																							}
																							else{
																							?>
																							<!--<img src="../images/profile/download-over.gif" style="cursor:default"/>-->
																							<?php
																							}
																							?>
																							<!--<a href="addtocart.php?seller_id=<?php echo $get_media_info_down_gal['general_user_id'];?>&media_id=<?php echo $get_media_info_down_gal['id'];?>&buyer_id=<?php echo $_SESSION['gen_user_id'];?>&title=<?php echo str_replace("'","\'",$get_media_info_down_gal['title']);?>&image_name=<?php echo $image_name_cartproject;?>" id="download_videoproject<?php echo $sep_ids_edv['media_id'];?>" style="display:none;"></a>-->
																							<script type="text/javascript">
																							$(document).ready(function() {
																									$("#download_videoproject<?php echo $sep_ids_edv['media_id'];?>").fancybox({
																									'height'			: '75%',
																									'width'				: '69%',
																									'transitionIn'		: 'none',
																									'transitionOut'		: 'none',
																									'type'				: 'iframe'
																									});
																							});
																							</script>
																						<?php
																						}
																					}
																					?>
																					</div>
																				</div>
																				<div class="sideR">
																					<div class="activityTitle"><?php
																					if($ans_events_posts[$vpev_all]['profile_type']=='events_post')
																					{
																					if($sep_ids_edv['profile_url']!="") {
		?>																				
																					<a href="/<?php echo $sep_ids_edv['profile_url']; ?>">
		<?php
																		}
																						echo $sep_ids_edv['title'];
																						if($sep_ids_edv['profile_url']!="") {
		?>																				
																					</a>
		<?php
																		}
																					}?>
																						<div style="color:#808080;font-size:12px;">
																							<div class="comment more">
																								<?php echo $ans_events_posts[$vpev_all]['description']; ?>
																							</div>
																						</div>
																					</div>
																					<!--<div class="activityContent">Tagged: <?php //echo $medias['tagged_users']; 
																						//$name_org = explode(')',$medias['tagged_users']);
																						//for($i_name_org=0;$i_name_org<count($name_org);$i_name_org++)
																						//{
																						//	$end_pos = strpos($name_org[$i_name_org],'(');
																						//	$org_name = substr($name_org[$i_name_org],0,$end_pos);
																					//		echo $org_name;
																					//	}
																					?></div>-->
																				</div>
																			</div>
																		</div>
																	</div>
																	<div class="rightSide">
																		<a href="#" onmouseover="mopen('mm<?php echo $vpev_all;?>19')" onmouseout="mclosetime()"><img src="images/profile/arrow.png" onmouseover="this.src='images/profile/arrow-down.png'" onmouseout="this.src='images/profile/arrow.png'" /></a>
																		<div id="mm<?php echo $vpev_all;?>19" class="popUp" onmouseover="mcancelclosetime()" onmouseout="mclosetime()">
																			<ul>
																				<li><a href="hideposts.php?id=<?php echo $ans_events_posts[$vpev_all]['id'];?>">Hide Feed</a></li>
																				<li><a href="hideposts.php?report_id=<?php echo $ans_all_posts[$po_all]['id'];?>">Report spam</a></li>
																				<li><div class="seperator"></div></li>
																				<li><a href="">Unsubscribe from <?php echo $gens['fname'].' '.$gens['lname']; ?></a></li>
																			</ul>
																		</div>
																	</div>
																</div>
			<?php
																}
															}
														}
													}
													elseif($ans_events_posts[$vpev_all]['display_to']=='Friends')
													{
														if($gens!="")
														{
															$friends_c = $sql_feeds->friends_cond($ans_events_posts[$vpev_all]['general_user_id'],$_SESSION['login_id']);
															
															if($friends_c=='1')
															{
																$hide3 = 0;
																$get_gen_id = $sql_feeds->get_general_info();
																$hide_by = $ans_events_posts[$vpev_all]['hidden_by'];
																$exp_hide_by = explode(",",$hide_by);
																for($exp_count=0;$exp_count<=count($exp_hide_by);$exp_count++)
																{
																	if($exp_hide_by[$exp_count] ==$get_gen_id['general_user_id'])
																	{
																		$hide3 =1;
																	}
																}
																if($hide3 != 1)
																{
																	if(isset($sep_ids_edv['artist_id']))
															{
																$sql_art = mysql_query("SELECT * FROM general_artist WHERE artist_id='".$gens['artist_id']."' AND status=0 AND active=0 AND del_status=0");
																if($sql_art!="" && $sql_art!=NULL)
																{
																	if(mysql_num_rows($sql_art)>0)
																	{
																		$ros = mysql_fetch_assoc($sql_art);
																		$art = 1;
																	}
																}
															}
															
															$com = 0;
															if(isset($sep_ids_edv['community_id']))
															{
																$sql_com = mysql_query("SELECT * FROM general_community WHERE community_id='".$gens['community_id']."' AND status=0 AND active=0 AND del_status=0");
																if($sql_com!="" && $sql_com!=NULL)
																{
																	if(mysql_num_rows($sql_com)>0)
																	{
																		$ros_com = mysql_fetch_assoc($sql_com);
																		$com = 1;
																	}
																}
															}
			?>
																<div class="storyCont">
																	<div class="leftSide">
																		<div class="avatar">
																			<?php
													if($art==1)
													{
														if($ros['profile_url']!="")
														{
													?>
														<a href="/<?php echo $ros['profile_url']; ?>">
													<?php
														}
														$get_news_general_user_id = $uk -> get_general_user_idby_artistid($ros['artist_id']);
													?>
														<img width="50" height="50" src="<?php
														if($ros['image_name']!="" && !empty($ros['image_name']))
														{
															echo $s3->getNewURI_otheruser("artjcropprofile",$get_news_general_user_id).$ros['image_name']; 
														}
														else
														{
															echo "http://artjcropprofile.s3.amazonaws.com/Noimage.png";
														}
													?>" />
													<?php
														if($ros['profile_url']!="")
														{
													?>
														</a>
													<?php
														}
													}
													elseif($com==1)
													{
														if($ros_com['profile_url']!="")
														{
													?>
														<a href="/<?php echo $ros_com['profile_url']; ?>">
													<?php
														}
														$get_news_general_user_id = $uk -> get_general_user_idby_communityid($ros_com['community_id']);
													?>
														<img width="50" height="50" src="<?php
													
													
													if($ros_com['image_name']!="" && !empty($ros_com['image_name']))
													{
														echo $s3->getNewURI_otheruser("comjcropprofile",$get_news_general_user_id).$ros_com['image_name']; 
													}
													else
													{
														echo "http://comjcropprofile.s3.amazonaws.com/Noimage.png";
													}
													
													?>" />
													<?php
														if($ros_com['profile_url']!="")
														{
													?>
														</a>
													<?php
														}
													}
													?>
																	</div>
																	<?php //echo "table=".$reg_table_media_type;
																	//echo $ans_audio_feeds[$audio_fed_lo]['id'];
																	?>
																		<div class="storyContainer">
																			<div class="storyTitle">
																				<span class="userName"><?php if($art==1)
																					{
																					if($ros['profile_url']!="")
														{
													?>
														<a href="/<?php echo $ros['profile_url']; ?>">
													<?php
														}
																						echo $ros['name'];
																						if($ros['profile_url']!="")
														{
													?>
														</a>
													<?php
														}
																					}
																					elseif($com==1)
																					{
																					if($ros_com['profile_url']!="")
														{
													?>
														<a href="/<?php echo $ros_com['profile_url']; ?>">
													<?php
														}
																						echo $ros_com['name'];
																						if($ros_com['profile_url']!="")
														{
													?>
														</a>
													<?php
														}
																					} ?></span>  <?php //echo $ans_events_posts[$vpev_all]['description']; ?>
																			</div>
																			<div class="activityCont">
																				<div class="sideL">
																					<div class="storyImage">
		<?php																			if($sep_ids_edv['profile_url']!="") {
		?>																				
																					<a href="/<?php echo $sep_ids_edv['profile_url']; ?>">
		<?php
																		}
		?>
																					<img width="90" height="90" src="<?php if($sep_ids_edv['image_name']!="") { echo $sep_ids_edv['image_name']; } else { echo "http://comjcropprofile.s3.amazonaws.com/Noimage.png"; }?>" />
		<?php																			if($sep_ids_edv['profile_url']!="") {
		?>																				
																					</a>
		<?php
																		}
		?>
																					</div>
																					<div class="player">
																					<?php
																						if($sep_ids_edv['featured_media']!="" || $sep_ids_edv['featured_media']!="nodisplay"){
																						if($sep_ids_edv['featured_media'] == "Song" && $sep_ids_edv['media_id'] != 0){
																						?>
																							<img src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" onclick="showPlayer('a','<?php echo $sep_ids_edv['media_id'];?>','play');" />
																							<img src="images/profile/add.gif" onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'" onclick="showPlayer('a','<?php echo $sep_ids_edv['media_id'];?>','add');" />
																							<?php
																							$get_media_info_down_gal = $sql_feeds ->get_media_details($sep_ids_edv['media_id']);
																							if($get_media_info_down_gal['sharing_preference']==5 || $get_media_info_down_gal['sharing_preference']==2)
																							{
																								$song_image_name_cartproject = $sql_feeds->get_audio_img($get_media_info_down_gal['id']);
																								$free_down_rel ="";
																								for($find_down=0;$find_down<=count($get_songs_pnew_w);$find_down++)
																								{
																									//echo $get_songs_pnew_w[$find_down]['id'] ."==". $get_Song_title['id']."</br>";
																									if(isset($get_media_info_down_gal) && $get_media_info_down_gal != null)
																									{
																										if($get_songs_pnew_w[$find_down]['id']==$get_media_info_down_gal['id']){
																										$free_down_rel = "yes";
																										}
																										else{
																											if($free_down_rel =="" || $free_down_rel =="no"){
																												$free_down_rel = "no";
																											}
																										}
																									}
																								}
																								if($free_down_rel == "yes"){
																								 ?>
																								 <!--<a href="javascript:void(0);" onclick="download_directly_song('<?php echo $get_media_info_down_gal['id'];?>','<?php echo str_replace("'","\'",$get_media_info_down_gal['title']);?>','<?php echo str_replace("'","\'",$get_media_info_down_gal['creator']);?>','<?php echo str_replace("'","\'",$get_media_info_down_gal['from']);?>')"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>-->
																								<?php
																								}
																								else if($get_media_info_down_gal['sharing_preference']==2){
																								 ?>
																								<!--<a href="javascript:void(0);" onclick="ask_to_donate('<?php echo str_replace("'","\'",$get_media_info_down_gal['title']);?>','<?php echo str_replace("'","\'",$get_media_info_down_gal['creator']);?>','<?php echo $get_media_info_down_gal['id'];?>','download_songproject<?php echo $get_media_info_down_gal['id'];?>')"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>-->
																							 <?php
																								}
																								else{
																							?>
																								<!--<a href="javascript:void(0);" onclick="down_song_pop_project(<?php echo $get_media_info_down_gal['id'];?>)"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>-->
																							<?php
																								}
																							}else{
																							?>	
																							<!--<img src="../images/profile/download-over.gif" style="cursor:default" />-->
																							<?php
																							}
																							?>
																							<!--<a href="addtocart.php?seller_id=<?php echo $get_media_info_down_gal['general_user_id'];?>&media_id=<?php echo $get_media_info_down_gal['id'];?>&buyer_id=<?php echo $_SESSION['gen_user_id'];?>&title=<?php echo str_replace("'","\'",$get_media_info_down_gal['title']);?>&image_name=<?php echo $song_image_name_cartproject;?>" id="download_songproject<?php echo $get_media_info_down_gal['id'];?>" style="display:none;"></a>-->
																							<script type="text/javascript">
																							$(document).ready(function() {
																									$("#download_songproject<?php echo $get_media_info_down_gal['id'];?>").fancybox({
																									'width'				: '75%',
																									'height'			: '75%',
																									'transitionIn'		: 'none',
																									'transitionOut'		: 'none',
																									'type'				: 'iframe'
																									});
																							});
																							</script>
																						<?php
																						}if($sep_ids_edv['featured_media'] == "Gallery"  && $sep_ids_edv['media_id'] != 0){
																							$get_all_feature_image3 = $sql_feeds->get_all_gallery_images($sep_ids_edv['media_id']);
																							if($get_all_feature_image3!="" && $get_all_feature_image3!=NULL)
																{
																							if(mysql_num_rows($get_all_feature_image3)>0)
																							{
																								$mediaSrc3 = "";
																								$galleryImagesData3 = "";
																								$galleryImagestitle3 = "";
																								$get_news_general_user_id = $uk -> get_general_user_idby_mediaid($sep_ids_edv['media_id']);
																								while($row = mysql_fetch_assoc($get_all_feature_image3))
																								{
																									$mediaSrc3 = $row[0]['image_name'];
																									$filePath = $s3->getNewURI_otheruser("medgallery",$get_news_general_user_id).$mediaSrc3;
																									$galleryImagesData3 = $galleryImagesData3 .",". $row['image_name'];
																									$galleryImagestitle3 = $galleryImagestitle3 .",".$row['image_title'];
																								}
																								$orgPath3 = $s3->getNewURI_otheruser("medgallery",$get_news_general_user_id);
																								$thumbPath3 = $s3->getNewURI_otheruser("medgalthumb",$get_news_general_user_id);
																							}
																							}
																						?>
																							<a href="javascript:void(0);" onclick="checkload('<?php echo $mediaSrc3;?>','<?php echo $galleryImagesData3; ?>','<?php echo $thumbPath3; ?>','<?php echo $orgPath3; ?>','<?php echo $galleryImagestitle3; ?>','<?php echo $get_gallery['id'];?>')">
																							<img src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'"></a>
																							<img src="images/profile/add.gif" onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'">
																							<?php
																							$get_media_info_down_gal = $sql_feeds ->get_media_details($sep_ids_edv['media_id']);
																							$get_gal_image_project  = $sql_feeds ->get_gallery_info($sep_ids_edv['media_id']);
																							if($get_media_info_down_gal['sharing_preference']==2)
																							{
																							?>
																								<!--<a href="javascript:void(0);" onclick="ask_to_donate('<?php echo str_replace("'","\'",$get_media_info_down_gal['title']);?>','<?php echo str_replace("'","\'",$get_media_info_down_gal['creator']);?>','<?php echo $get_media_info_down_gal['id'];?>','download_galproject<?php echo $sep_ids_edv['media_id'];?>')"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>-->
																							 <?php
																							}else{
																							?>
																							<!--<img src="../images/profile/download-over.gif" style="cursor:default"/>-->
																							<?php
																							}
																							?>
																							<!--<a href="addtocart.php?seller_id=<?php echo $get_media_info_down_gal['general_user_id'];?>&media_id=<?php echo $get_media_info_down_gal['id'];?>&buyer_id=<?php echo $_SESSION['gen_user_id'];?>&title=<?php echo str_replace("'","\'",$get_media_info_down_gal['title']);?>&image_name=<?php echo $get_gal_image_project['profile_image'];?>" id="download_galproject<?php echo $sep_ids_edv['media_id'];?>" style="display:none;"></a>-->
																							<script type="text/javascript">
																							$(document).ready(function() {
																									$("#download_galproject<?php echo $sep_ids_edv['media_id'];?>").fancybox({
																									'width'				: '75%',
																									'height'			: '75%',
																									'transitionIn'		: 'none',
																									'transitionOut'		: 'none',
																									'type'				: 'iframe'
																									});
																							});
																							</script>
																						<?php
																						}if($sep_ids_edv['featured_media'] == "Video" && $sep_ids_edv['media_id'] != 0){
																						?>
																							<img src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" onclick="showPlayer('v','<?php echo $sep_ids_edv['media_id'];?>','playlist');"/>
																							<img src="images/profile/add.gif" onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'" onclick="showPlayer('v','<?php echo $sep_ids_edv['media_id'];?>','addvi');"/>
																							<?php
																							$get_media_info_down_gal = $sql_feeds ->get_media_details($sep_ids_edv['media_id']);
																							if($get_media_info_down_gal['sharing_preference']==5 || $get_media_info_down_gal['sharing_preference']==2)
																							{
																								$get_video_image = $sql_feeds->getvideoimage_feature($get_media_info_down_gal['id']);
																								//$image_name_cartproject = "https://img.youtube.com/vi/".$get_video_image."/default.jpg";
																								if($get_video_image[1]=='youtube')
																			{
																				$image_name_cartproject = "http://img.youtube.com/vi/".$get_video_image[0]."/default.jpg";
																			}
																			else
																			{
																				$video_vim = $vimeo->call('vimeo.videos.getInfo', array('video_id' => $get_video_image[0]));
																				$image_name_cartproject = $video_vim->video[0]->thumbnails->thumbnail[1]->_content;
																			}
																								$free_down_rel_video="";
																								for($find_down=0;$find_down<=count($get_vids_pnew_w);$find_down++)
																								{
																									if(isset($get_media_info_down_gal) && $get_media_info_down_gal != null){
																										if($get_vids_pnew_w[$find_down]['id']==$get_media_info_down_gal['id']){
																											$free_down_rel_video = "yes";
																										}
																										else{
																											if($free_down_rel_video =="" || $free_down_rel_video =="no"){
																												$free_down_rel_video = "no";
																											}
																										}
																									}
																								}
																								if($free_down_rel_video == "yes"){
																								 ?>
																								<!--<a href="javascript:void(0);" onclick="download_directly_song('<?php echo $get_media_info_down_gal['id'];?>','<?php echo str_replace("'","\'",$get_media_info_down_gal['title']);?>','<?php echo str_replace("'","\'",$get_media_info_down_gal['creator']);?>','<?php echo str_replace("'","\'",$get_media_info_down_gal['from']);?>')"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>-->
																								 
																							 <?php
																								}
																								else if($get_media_info_down_gal['sharing_preference']==2){
																								 ?>
																									<!--<a href="javascript:void(0);" onclick="ask_to_donate('<?php echo str_replace("'","\'",$get_media_info_down_gal['title']);?>','<?php echo str_replace("'","\'",$get_media_info_down_gal['creator']);?>','<?php echo $get_media_info_down_gal['id'];?>','download_videoproject<?php echo $sep_ids_edv['media_id'];?>')"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>-->
																								<?php
																								}
																								else{
																							?>
																								<!--<a href="javascript:void(0);" onclick="down_video_pop_project(<?php echo $sep_ids_edv['media_id'];?>)"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>-->
																							<?php
																								}
																							}
																							else{
																							?>
																							<!--<img src="../images/profile/download-over.gif" style="cursor:default"/>-->
																							<?php
																							}
																							?>
																							<!--<a href="addtocart.php?seller_id=<?php echo $get_media_info_down_gal['general_user_id'];?>&media_id=<?php echo $get_media_info_down_gal['id'];?>&buyer_id=<?php echo $_SESSION['gen_user_id'];?>&title=<?php echo str_replace("'","\'",$get_media_info_down_gal['title']);?>&image_name=<?php echo $image_name_cartproject;?>" id="download_videoproject<?php echo $sep_ids_edv['media_id'];?>" style="display:none;"></a>-->
																							<script type="text/javascript">
																							$(document).ready(function() {
																									$("#download_videoproject<?php echo $sep_ids_edv['media_id'];?>").fancybox({
																									'height'			: '75%',
																									'width'				: '69%',
																									'transitionIn'		: 'none',
																									'transitionOut'		: 'none',
																									'type'				: 'iframe'
																									});
																							});
																							</script>
																						<?php
																						}
																					}
																					?>
																					</div>
																				</div>
																				<div class="sideR">
																					<div class="activityTitle"><?php
																					if($ans_events_posts[$vpev_all]['profile_type']=='events_post')
																					{
																					if($sep_ids_edv['profile_url']!="") {
		?>																				
																					<a href="/<?php echo $sep_ids_edv['profile_url']; ?>">
		<?php
																		}
																						echo $sep_ids_edv['title'];
																						if($sep_ids_edv['profile_url']!="") {
		?>																				
																					</a>
		<?php
																		}
																					}?>
																						<div style="color:#808080;font-size:12px;">
																							<div class="comment more">
																								<?php echo $ans_events_posts[$vpev_all]['description']; ?>
																							</div>
																						</div>
																					</div>
																					<!--<div class="activityContent">Tagged: <?php //echo $medias['tagged_users']; 
																						//$name_org = explode(')',$medias['tagged_users']);
																						//for($i_name_org=0;$i_name_org<count($name_org);$i_name_org++)
																						//{
																						//	$end_pos = strpos($name_org[$i_name_org],'(');
																						//	$org_name = substr($name_org[$i_name_org],0,$end_pos);
																					//		echo $org_name;
																					//	}
																					?></div>-->
																				</div>
																			</div>
																		</div>
																	</div>
																	<div class="rightSide">
																		<a href="#" onmouseover="mopen('mm<?php echo $vpev_all;?>20')" onmouseout="mclosetime()"><img src="images/profile/arrow.png" onmouseover="this.src='images/profile/arrow-down.png'" onmouseout="this.src='images/profile/arrow.png'" /></a>
																		<div id="mm<?php echo $vpev_all;?>20" class="popUp" onmouseover="mcancelclosetime()" onmouseout="mclosetime()">
																			<ul>
																				<li><a href="hideposts.php?id=<?php echo $ans_events_posts[$vpev_all]['id'];?>">Hide Feed</a></li>
																				<li><a href="hideposts.php?report_id=<?php echo $ans_all_posts[$po_all]['id'];?>">Report spam</a></li>
																				<li><div class="seperator"></div></li>
																				<li><a href="">Unsubscribe from <?php echo $gens['fname'].' '.$gens['lname']; ?></a></li>
																			</ul>
																		</div>
																	</div>
																</div>
			<?php
																}
															}
														}
													}
												}
											}
										}
									}	
							?>
							</div>
							
							<div id="project_feeds" style="display:none;">
							<?php
									include_once("findsale_video.php");
									include_once("findsale_song.php");
									$pro_ans_all_feeds = array();
									$sub_pro_ans_all_feeds = array();
									$pro_ans_fan_feeds = array();
									$pvr_pro_eve_id = array();
									$vpr_art_com_id = array();
									$pri_arts_ids = array();
									$pri_coms_ids = array();
									$ans_projects_posts = array();
									
									$pro_sql_all_feeds = $sql_feeds->all_feeds($chk_sql['email']);
									$p_run_all_feeds = mysql_query($pro_sql_all_feeds);
									
									$pro_sql_fan_all_feeds = $sql_feeds->fan_club($chk_sql['email']);
									$p_run_fan_all_feeds = mysql_query($pro_sql_fan_all_feeds);
									
									if($p_run_all_feeds!="" && $p_run_all_feeds!=NULL)
									{
										while($pro_row_all_feeds = mysql_fetch_assoc($p_run_all_feeds))
										{
											$sub_pro_ans_all_feeds[] = $pro_row_all_feeds;
										}
									}
									
									if($p_run_fan_all_feeds!="" && $p_run_fan_all_feeds!=NULL)
									{
										while($pro_row_fan_all_feeds = mysql_fetch_assoc($p_run_fan_all_feeds))
										{
											$pro_ans_fan_feeds[] = $pro_row_fan_all_feeds;
										}
									}
									
									$pro_ans_all_feeds = array_merge($sub_pro_ans_all_feeds,$pro_ans_fan_feeds);
									
									if($pro_ans_all_feeds!=NULL)
									{
										for($vpr_fed_lo=0;$vpr_fed_lo<count($pro_ans_all_feeds);$vpr_fed_lo++)
										{
											if($pro_ans_all_feeds[$vpr_fed_lo]['related_type']!='artist' && $pro_ans_all_feeds[$vpr_fed_lo]['related_type']!='community')
											{
												$num_ids = explode('_',$pro_ans_all_feeds[$vpr_fed_lo]['related_id']);
												if($pro_ans_all_feeds[$vpr_fed_lo]['related_type']=='artist_project' || $pro_ans_all_feeds[$vpr_fed_lo]['related_type']=='artist_event')
												{
													$pvr_pro_eve_id[$vpr_fed_lo]['related_id_art'] = $num_ids[0];
												}
												elseif($pro_ans_all_feeds[$vpr_fed_lo]['related_type']=='community_project' || $pro_ans_all_feeds[$vpr_fed_lo]['related_type']=='community_event')
												{
													$vpr_art_com_id[$vpr_fed_lo]['related_id_com'] = $num_ids[0];
												}
											}
											elseif($pro_ans_all_feeds[$vpr_fed_lo]['related_type']=='artist' || $pro_ans_all_feeds[$vpr_fed_lo]['related_type']=='community')
											{
												if($pro_ans_all_feeds[$vpr_fed_lo]['related_type']=='artist')
												{
													$pvr_pro_eve_id[$vpr_fed_lo]['related_id_art'] = $pro_ans_all_feeds[$vpr_fed_lo]['related_id'];
												}
												elseif($pro_ans_all_feeds[$vpr_fed_lo]['related_type']=='community')
												{
													$vpr_art_com_id[$vpr_fed_lo]['related_id_com'] = $pro_ans_all_feeds[$vpr_fed_lo]['related_id'];
												}
											}
										}
										
										if($pvr_pro_eve_id!=0 && count($pvr_pro_eve_id)>0)
										{
											for($arts=0;$arts<count($pvr_pro_eve_id);$arts++)
											{
												$org_art[] = $pvr_pro_eve_id[$arts]['related_id_art'];
											}
											
											$new_art_rp = array();
											$rpi = 0;
											foreach ($org_art as $key => $value)
											{
												if(isset($new_art_rp[$value]))
												{
													$new_art_rp[$value] += 1;
												}
												else
												{
													$new_art_rp[$value] = 1;
													
													$sqls = $sql_feeds->arts_gen($value);
													
													if($sqls!="")
													{
														$pri_arts_ids[$rpi] = $sqls;
													}
												}
												$rpi = $rpi + 1;
											}
										}
										
										if($vpr_art_com_id!="" && count($vpr_art_com_id)>0)
										{
											for($coms=0;$coms<count($vpr_art_com_id);$coms++)
											{
												$org_com[] = $vpr_art_com_id[$coms]['related_id_com'];
											}
											
											$new_com_prs = array();
											$vrp_c = 0;
											foreach ($org_com as $key => $value)
											{
												if(isset($new_com_prs[$value]))
												{
													$new_com_prs[$value] += 1;
												}
												else
												{
													$new_com_prs[$value] = 1;
													
													$sqls_c = $sql_feeds->coms_gen($value);
													if($sqls_c!="")
													{
														$pri_coms_ids[$vrp_c] = $sqls_c;
													}
												}
												$vrp_c = $vrp_c + 1;
											}
										}
										
										$doub_psr_chk = array_merge($pri_arts_ids,$pri_coms_ids);
										
										for($prim_d=0;$prim_d<count($doub_psr_chk);$prim_d++)
										{
											$rel_d_id[] = $doub_psr_chk[$prim_d]['general_user_id'];
										}
										
										$new_array_rep_pir = array();
										foreach ($rel_d_id as $key => $value)
										{
											if(isset($new_array_rep_pir[$value]))
												$new_array_rep_pir[$value] += 1;
											else
												$new_array_rep_pir[$value] = 1;
										}
										
										foreach ($new_array_rep_pir as $uid => $n)
										{
											$ex_uid_prvs = $ex_uid_prvs.','.$uid;
										}
										$ex_tr_eri = trim($ex_uid_prvs, ",");
										$sep_ids_irpp = explode(',',$ex_tr_eri);

										if($sep_ids_irpp!="" && count($sep_ids_irpp)>0)
										{
											for($se_ca=0;$se_ca<count($sep_ids_irpp);$se_ca++)
											{
												$types = 'projects_post';
												$ids = $sql_feeds->pmva_posts_check($sep_ids_irpp[$se_ca],$_SESSION['login_id'],$types);
												
												//die;
												if($ids!="" && count($ids)>0)
												{
													$posts_event = $sql_feeds->pmv_posts_check($sep_ids_irpp[$se_ca],$_SESSION['login_id'],$types);
													if($posts_event!="" && $posts_event!=NULL)
													{
														if($posts_event!="" && mysql_num_rows($posts_event)>0)
														{
															while($rows_posts_all = mysql_fetch_assoc($posts_event))
															{
																$ans_projects_posts[] = $rows_posts_all;
															}
														}
													}
												}
											}
										}
										
										if($ans_projects_posts!="" && count($ans_projects_posts)>0) 
										{
											for($vppr_all=0;$vppr_all<count($ans_projects_posts);$vppr_all++)
											{
												if($ans_projects_posts[$vppr_all]['profile_type']=='projects_post')
												{
													$exp_meds = explode('~',$ans_projects_posts[$vppr_all]['media']);
													
													if($ans_projects_posts[$vppr_all]['profile_type']=='projects_post')
													{
														$exp_meds = explode('~',$ans_projects_posts[$vppr_all]['media']);
														$sep_ids_irp = $sql_feeds->featured_pro_eve($exp_meds[1],$exp_meds[0]);
													}
												}
										
												if(isset($sep_ids_irp) && $sep_ids_irp!='0' && $sep_ids_irp!=NULL)
												{
													$gens = $sql_feeds->get_log_ids($ans_projects_posts[$vppr_all]['general_user_id']);
													if($ans_projects_posts[$vppr_all]['display_to']=='All' && $ans_projects_posts[$vppr_all]['profile_type']=='projects_post')
													{
														$hide4 = 0;
														$get_gen_id = $sql_feeds->get_general_info();
														$hide_by = $ans_projects_posts[$vppr_all]['hidden_by'];
														$exp_hide_by = explode(",",$hide_by);
														for($exp_count=0;$exp_count<=count($exp_hide_by);$exp_count++)
														{
															if($exp_hide_by[$exp_count] ==$get_gen_id['general_user_id'])
															{
																$hide4 =1;
															}
														}
														if($hide4 != 1)
														{
															$art = 0;
															if(isset($sep_ids_irp['artist_id']))
															{
																$sql_art = mysql_query("SELECT * FROM general_artist WHERE artist_id='".$gens['artist_id']."' AND status=0 AND active=0 AND del_status=0");
																if($sql_art!="" && $sql_art!=NULL)
																{
																	if(mysql_num_rows($sql_art)>0)
																	{
																		$ros = mysql_fetch_assoc($sql_art);
																		$art = 1;
																	}
																}
															}
															
															$com = 0;
															if(isset($sep_ids_irp['community_id']))
															{
																$sql_com = mysql_query("SELECT * FROM general_community WHERE community_id='".$gens['community_id']."' AND status=0 AND active=0 AND del_status=0");
																if($sql_com!="" && $sql_com!=NULL)
																{
																	if(mysql_num_rows($sql_com)>0)
																	{
																		$ros_com = mysql_fetch_assoc($sql_com);
																		$com = 1;
																	}
																}
															}
			?>
														<div class="storyCont">
																	<div class="leftSide">
																		<div class="avatar">
																			<?php
													if($art==1)
													{
														if($ros['profile_url']!="")
														{
													?>
														<a href="/<?php echo $ros['profile_url']; ?>">
													<?php
														}
														$get_news_general_user_id = $uk -> get_general_user_idby_artistid($ros['artist_id']);
													?>
														<img width="50" height="50" src="<?php
														if($ros['image_name']!="" && !empty($ros['image_name']))
														{
															echo $s3->getNewURI_otheruser("artjcropprofile",$get_news_general_user_id).$ros['image_name']; 
														}
														else
														{
															echo "http://artjcropprofile.s3.amazonaws.com/Noimage.png";
														}
													?>" />
													<?php
														if($ros['profile_url']!="")
														{
													?>
														</a>
													<?php
														}
													}
													elseif($com==1)
													{
														if($ros_com['profile_url']!="")
														{
													?>
														<a href="/<?php echo $ros_com['profile_url']; ?>">
													<?php
														}
														$get_news_general_user_id = $uk -> get_general_user_idby_communityid($ros_com['community_id']);
													?>
														<img width="50" height="50" src="<?php
													
													
													if($ros_com['image_name']!="" && !empty($ros_com['image_name']))
													{
														echo $s3->getNewURI_otheruser("comjcropprofile",$get_news_general_user_id).$ros_com['image_name']; 
													}
													else
													{
														echo "http://comjcropprofile.s3.amazonaws.com/Noimage.png";
													}
													
													?>" />
													<?php
														if($ros_com['profile_url']!="")
														{
													?>
														</a>
													<?php
														}
													}
													?>
																	</div>
																	<?php //echo "table=".$reg_table_media_type;
																	//echo $ans_audio_feeds[$audio_fed_lo]['id'];
																	?>
																		<div class="storyContainer">
																			<div class="storyTitle">
																				<span class="userName"><?php if($art==1)
																					{
																					if($ros['profile_url']!="")
														{
													?>
														<a href="/<?php echo $ros['profile_url']; ?>">
													<?php
														}
																						echo $ros['name'];
																						if($ros['profile_url']!="")
														{
													?>
														</a>
													<?php
														}
																					}
																					elseif($com==1)
																					{
																					if($ros_com['profile_url']!="")
														{
													?>
														<a href="/<?php echo $ros_com['profile_url']; ?>">
													<?php
														}
																						echo $ros_com['name'];
																						if($ros_com['profile_url']!="")
														{
													?>
														</a>
													<?php
														}
																					} ?></span> <?php //echo $ans_projects_posts[$vppr_all]['description']; ?>
																			</div>
																			<div class="activityCont">
																				<div class="sideL">
																					<div class="storyImage">
		<?php																			if($sep_ids_irp['profile_url']!="") {
		?>																				
																					<a href="/<?php echo $sep_ids_irp['profile_url']; ?>">
		<?php
																		}
		?>
																					<img width="90" height="90" src="<?php if($sep_ids_irp['image_name']!="") { echo $sep_ids_irp['image_name']; } else { echo "http://comjcropprofile.s3.amazonaws.com/Noimage.png"; }?>" />
		<?php																			if($sep_ids_irp['profile_url']!="") {
		?>																				
																					</a>
		<?php
																		}
		?>
																					</div>
																					<div class="player">
																					<?php
																						if($sep_ids_irp['featured_media']!="" || $sep_ids_irp['featured_media']!="nodisplay"){
																						if($sep_ids_irp['featured_media'] == "Song" && $sep_ids_irp['media_id'] != "0"){
																						?>
																							<img src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" onclick="showPlayer('a','<?php echo $sep_ids_irp['media_id'];?>','play');" />
																							<img src="images/profile/add.gif" onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'" onclick="showPlayer('a','<?php echo $sep_ids_irp['media_id'];?>','add');" />
																							<?php
																							$get_media_info_down_gal = $sql_feeds ->get_media_details($sep_ids_irp['media_id']);
																							if($get_media_info_down_gal['sharing_preference']==5 || $get_media_info_down_gal['sharing_preference']==2)
																							{
																								$song_image_name_cartproject = $sql_feeds->get_audio_img($get_media_info_down_gal['id']);
																								$free_down_rel ="";
																								for($find_down=0;$find_down<=count($get_songs_pnew_w);$find_down++)
																								{
																									//echo $get_songs_pnew_w[$find_down]['id'] ."==". $get_Song_title['id']."</br>";
																									if(isset($get_media_info_down_gal) && $get_media_info_down_gal != null)
																									{
																										if($get_songs_pnew_w[$find_down]['id']==$get_media_info_down_gal['id']){
																										$free_down_rel = "yes";
																										}
																										else{
																											if($free_down_rel =="" || $free_down_rel =="no"){
																												$free_down_rel = "no";
																											}
																										}
																									}
																								}
																								if($free_down_rel == "yes"){
																								 ?>
																								 <!--<a href="javascript:void(0);" onclick="download_directly_song('<?php echo $get_media_info_down_gal['id'];?>','<?php echo str_replace("'","\'",$get_media_info_down_gal['title']);?>','<?php echo str_replace("'","\'",$get_media_info_down_gal['creator']);?>','<?php echo str_replace("'","\'",$get_media_info_down_gal['from']);?>')"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>-->
																								<?php
																								}
																								else if($get_media_info_down_gal['sharing_preference']==2){
																								 ?>
																								<!--<a href="javascript:void(0);" onclick="ask_to_donate('<?php echo str_replace("'","\'",$get_media_info_down_gal['title']);?>','<?php echo str_replace("'","\'",$get_media_info_down_gal['creator']);?>','<?php echo $get_media_info_down_gal['id'];?>','download_songproject<?php echo $get_media_info_down_gal['id'];?>')"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>-->
																							 <?php
																								}
																								else{
																							?>
																								<!--<a href="javascript:void(0);" onclick="down_song_pop_project(<?php echo $get_media_info_down_gal['id'];?>)"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>-->
																							<?php
																								}
																							}else{
																							?>	
																							<!--<img src="../images/profile/download-over.gif" style="cursor:default" />-->
																							<?php
																							}
																							?>
																							<!--<a href="addtocart.php?seller_id=<?php echo $get_media_info_down_gal['general_user_id'];?>&media_id=<?php echo $get_media_info_down_gal['id'];?>&buyer_id=<?php echo $_SESSION['gen_user_id'];?>&title=<?php echo str_replace("'","\'",$get_media_info_down_gal['title']);?>&image_name=<?php echo $song_image_name_cartproject;?>" id="download_songproject<?php echo $get_media_info_down_gal['id'];?>" style="display:none;"></a>-->
																							<script type="text/javascript">
																							$(document).ready(function() {
																									$("#download_songproject<?php echo $get_media_info_down_gal['id'];?>").fancybox({
																									'width'				: '75%',
																									'height'			: '75%',
																									'transitionIn'		: 'none',
																									'transitionOut'		: 'none',
																									'type'				: 'iframe'
																									});
																							});
																							</script>
																						<?php
																						}if($sep_ids_irp['featured_media'] == "Gallery"  && $sep_ids_irp['media_id'] != "0"){
																							$get_all_feature_image3 = $sql_feeds->get_all_gallery_images($sep_ids_irp['media_id']);
																							if($get_all_feature_image3!="" && $get_all_feature_image3!=NULL)
																{
																							if(mysql_num_rows($get_all_feature_image3)>0)
																							{
																								$mediaSrc3 = "";
																								$galleryImagesData3 = "";
																								$galleryImagestitle3 = "";
																								$get_news_general_user_id = $uk -> get_general_user_idby_mediaid($sep_ids_irp['media_id']);
																								while($row = mysql_fetch_assoc($get_all_feature_image3))
																								{
																									$mediaSrc3 = $row[0]['image_name'];
																									$filePath = $s3->getNewURI_otheruser("medgallery",$get_news_general_user_id).$mediaSrc3;
																									$galleryImagesData3 = $galleryImagesData3 .",". $row['image_name'];
																									$galleryImagestitle3 = $galleryImagestitle3 .",".$row['image_title'];
																								}
																								$orgPath3 = $s3->getNewURI_otheruser("medgallery",$get_news_general_user_id);
																								$thumbPath3 = $s3->getNewURI_otheruser("medgalthumb",$get_news_general_user_id);
																							}
																							}
																						?>
																							<a href="javascript:void(0);" onclick="checkload('<?php echo $mediaSrc3;?>','<?php echo $galleryImagesData3; ?>','<?php echo $thumbPath3; ?>','<?php echo $orgPath3; ?>','<?php echo $galleryImagestitle3; ?>','<?php echo $get_gallery['id'];?>')">
																							<img src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'"></a>
																							<img src="images/profile/add.gif" onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'">
																							<?php
																							$get_media_info_down_gal = $sql_feeds ->get_media_details($sep_ids_irp['media_id']);
																							$get_gal_image_project  = $sql_feeds ->get_gallery_info($sep_ids_irp['media_id']);
																							if($get_media_info_down_gal['sharing_preference']==2)
																							{
																							?>
																								<!--<a href="javascript:void(0);" onclick="ask_to_donate('<?php echo str_replace("'","\'",$get_media_info_down_gal['title']);?>','<?php echo str_replace("'","\'",$get_media_info_down_gal['creator']);?>','<?php echo $get_media_info_down_gal['id'];?>','download_galproject<?php echo $sep_ids_irp['media_id'];?>')"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>-->
																							 <?php
																							}else{
																							?>
																							<!--<img src="../images/profile/download-over.gif" style="cursor:default"/>-->
																							<?php
																							}
																							?>
																							<!--<a href="addtocart.php?seller_id=<?php echo $get_media_info_down_gal['general_user_id'];?>&media_id=<?php echo $get_media_info_down_gal['id'];?>&buyer_id=<?php echo $_SESSION['gen_user_id'];?>&title=<?php echo str_replace("'","\'",$get_media_info_down_gal['title']);?>&image_name=<?php echo $get_gal_image_project['profile_image'];?>" id="download_galproject<?php echo $sep_ids_irp['media_id'];?>" style="display:none;"></a>-->
																							<script type="text/javascript">
																							$(document).ready(function() {
																									$("#download_galproject<?php echo $sep_ids_irp['media_id'];?>").fancybox({
																									'width'				: '75%',
																									'height'			: '75%',
																									'transitionIn'		: 'none',
																									'transitionOut'		: 'none',
																									'type'				: 'iframe'
																									});
																							});
																							</script>
																						<?php
																						}if($sep_ids_irp['featured_media'] == "Video" && $sep_ids_irp['media_id'] != "0"){
																						?>
																							<img src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" onclick="showPlayer('v','<?php echo $sep_ids_irp['media_id'];?>','playlist');"/>
																							<img src="images/profile/add.gif" onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'" onclick="showPlayer('v','<?php echo $sep_ids_irp['media_id'];?>','addvi');"/>
																							<?php
																							$get_media_info_down_gal = $sql_feeds ->get_media_details($sep_ids_irp['media_id']);
																							if($get_media_info_down_gal['sharing_preference']==5 || $get_media_info_down_gal['sharing_preference']==2)
																							{
																								$get_video_image = $sql_feeds->getvideoimage_feature($get_media_info_down_gal['id']);
																								//$image_name_cartproject = "https://img.youtube.com/vi/".$get_video_image."/default.jpg";
																								if($get_video_image[1]=='youtube')
																			{
																				$image_name_cartproject = "http://img.youtube.com/vi/".$get_video_image[0]."/default.jpg";
																			}
																			else
																			{
																				$video_vim = $vimeo->call('vimeo.videos.getInfo', array('video_id' => $get_video_image[0]));
																				$image_name_cartproject = $video_vim->video[0]->thumbnails->thumbnail[1]->_content;
																			}
																								$free_down_rel_video="";
																								for($find_down=0;$find_down<=count($get_vids_pnew_w);$find_down++)
																								{
																									if(isset($get_media_info_down_gal) && $get_media_info_down_gal != null){
																										if($get_vids_pnew_w[$find_down]['id']==$get_media_info_down_gal['id']){
																											$free_down_rel_video = "yes";
																										}
																										else{
																											if($free_down_rel_video =="" || $free_down_rel_video =="no"){
																												$free_down_rel_video = "no";
																											}
																										}
																									}
																								}
																								if($free_down_rel_video == "yes"){
																								 ?>
																								<!-- <a href="javascript:void(0);" onclick="download_directly_song('<?php echo $get_media_info_down_gal['id'];?>','<?php echo str_replace("'","\'",$get_media_info_down_gal['title']);?>','<?php echo str_replace("'","\'",$get_media_info_down_gal['creator']);?>','<?php echo str_replace("'","\'",$get_media_info_down_gal['from']);?>')"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>-->
																								 
																							 <?php
																								}
																								else if($get_media_info_down_gal['sharing_preference']==2){
																								 ?>
																									<!--<a href="javascript:void(0);" onclick="ask_to_donate('<?php echo str_replace("'","\'",$get_media_info_down_gal['title']);?>','<?php echo str_replace("'","\'",$get_media_info_down_gal['creator']);?>','<?php echo $get_media_info_down_gal['id'];?>','download_videoproject<?php echo $sep_ids_irp['media_id'];?>')"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>-->
																								<?php
																								}
																								else{
																							?>
																								<!--<a href="javascript:void(0);" onclick="down_video_pop_project(<?php echo $sep_ids_irp['media_id'];?>)"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>-->
																							<?php
																								}
																							}
																							else{
																							?>
																							<!--<img src="../images/profile/download-over.gif" style="cursor:default"/>-->
																							<?php
																							}
																							?>
																							<!--<a href="addtocart.php?seller_id=<?php echo $get_media_info_down_gal['general_user_id'];?>&media_id=<?php echo $get_media_info_down_gal['id'];?>&buyer_id=<?php echo $_SESSION['gen_user_id'];?>&title=<?php echo str_replace("'","\'",$get_media_info_down_gal['title']);?>&image_name=<?php echo $image_name_cartproject;?>" id="download_videoproject<?php echo $sep_ids_irp['media_id'];?>" style="display:none;"></a>-->
																							<script type="text/javascript">
																							$(document).ready(function() {
																									$("#download_videoproject<?php echo $sep_ids_irp['media_id'];?>").fancybox({
																									'height'			: '75%',
																									'width'				: '69%',
																									'transitionIn'		: 'none',
																									'transitionOut'		: 'none',
																									'type'				: 'iframe'
																									});
																							});
																							</script>
																						<?php
																						}
																					}
																					?>
																					</div>
																				</div>
																				<div class="sideR">
																					<div class="activityTitle"><?php
																					if($ans_projects_posts[$vppr_all]['profile_type']=='projects_post')
																					{
																					if($sep_ids_irp['profile_url']!="") {
		?>																				
																					<a href="/<?php echo $sep_ids_irp['profile_url']; ?>">
		<?php
																		}
																						echo $sep_ids_irp['title'];
																						if($sep_ids_irp['profile_url']!="") {
		?>																				
																					</a>
		<?php
																		}
																					}
																					?>
																						<div style="color:#808080;font-size:12px;">
																							<div class="comment more">
																								<?php echo $ans_projects_posts[$vppr_all]['description']; ?>
																							</div>
																						</div>
																					</div>
																					<!--<div class="activityContent">Tagged: <?php //echo $medias['tagged_users']; 
																						//$name_org = explode(')',$medias['tagged_users']);
																						//for($i_name_org=0;$i_name_org<count($name_org);$i_name_org++)
																						//{
																						//	$end_pos = strpos($name_org[$i_name_org],'(');
																						//	$org_name = substr($name_org[$i_name_org],0,$end_pos);
																					//		echo $org_name;
																					//	}
																					?></div>-->
																				</div>
																			</div>
																		</div>
																	</div>
																	<div class="rightSide">
																		<a href="#" onmouseover="mopen('mm<?php echo $vppr_all;?>21')" onmouseout="mclosetime()"><img src="images/profile/arrow.png" onmouseover="this.src='images/profile/arrow-down.png'" onmouseout="this.src='images/profile/arrow.png'" /></a>
																		<div id="mm<?php echo $vppr_all;?>21" class="popUp" onmouseover="mcancelclosetime()" onmouseout="mclosetime()">
																			<ul>
																				<li><a href="hideposts.php?id=<?php echo $ans_projects_posts[$vppr_all]['id'];?>">Hide Feed</a></li>
																				<li><a href="hideposts.php?report_id=<?php echo $ans_all_posts[$po_all]['id'];?>">Report spam</a></li>
																				<li><div class="seperator"></div></li>
																				<li><a href="">Unsubscribe from <?php echo $gens['fname'].' '.$gens['lname']; ?></a></li>
																			</ul>
																		</div>
																	</div>
																</div>
			<?php
														}
													}
													elseif($ans_projects_posts[$vppr_all]['display_to']=='Fans')
													{
														if($gens!="")
														{
															$fans_all =	$sql_feeds->fans_cond($chk_sql['email'],$gens['community_id'],$gens['artist_id']);
															if($fans_all=='1')
															{
																$hide5 = 0;
																$get_gen_id = $sql_feeds->get_general_info();
																$hide_by = $ans_projects_posts[$vppr_all]['hidden_by'];
																$exp_hide_by = explode(",",$hide_by);
																for($exp_count=0;$exp_count<=count($exp_hide_by);$exp_count++)
																{
																	if($exp_hide_by[$exp_count] ==$get_gen_id['general_user_id'])
																	{
																		$hide5 =1;
																	}
																}
																if($hide5 != 1)
																{
			?>
																<div class="storyCont">
																	<div class="leftSide">
																		<div class="avatar">
																			<img src="<?php echo $generalproimageURI.$gens['image_name']; ?>" />
																	</div>
																	<?php //echo "table=".$reg_table_media_type;
																	//echo $ans_audio_feeds[$audio_fed_lo]['id'];
																	?>
																		<div class="storyContainer">
																			<div class="storyTitle">
																				<span class="userName"><a href=""><?php echo $gens['fname'].' '.$gens['lname']; ?></a></span>  <?php //echo $ans_projects_posts[$vppr_all]['description']; ?>
																			</div>
																			<div class="activityCont">
																				<div class="sideL">
																					<div class="storyImage">
			<?php																		if($sep_ids_irp['profile_url']!="") {
		?>																				
																					<a href="/<?php echo $sep_ids_irp['profile_url']; ?>">
		<?php
																		}
		?>
																					<img width="90" height="90" src="<?php if($sep_ids_irp['image_name']!="") { echo $sep_ids_irp['image_name']; } else { echo "http://comjcropprofile.s3.amazonaws.com/Noimage.png"; }?>" />
			<?php																		if($sep_ids_irp['profile_url']!="") {
		?>																				
																					</a>
		<?php
																		}
																		?></div>
																					<div class="player">
																					<?php
																						if($sep_ids_irp['featured_media']!="" || $sep_ids_irp['featured_media']!="nodisplay"){
																						if($sep_ids_irp['featured_media'] == "Song" && $sep_ids_irp['media_id'] != "0"){
																						?>
																							<img src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" onclick="showPlayer('a','<?php echo $sep_ids_irp['media_id'];?>','play');" />
																							<img src="images/profile/add.gif" onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'" onclick="showPlayer('a','<?php echo $sep_ids_irp['media_id'];?>','add');" />
																							<?php
																							$get_media_info_down_gal = $sql_feeds ->get_media_details($sep_ids_irp['media_id']);
																							if($get_media_info_down_gal['sharing_preference']==5 || $get_media_info_down_gal['sharing_preference']==2)
																							{
																								$song_image_name_cartproject = $sql_feeds->get_audio_img($get_media_info_down_gal['id']);
																								$free_down_rel ="";
																								for($find_down=0;$find_down<=count($get_songs_pnew_w);$find_down++)
																								{
																									//echo $get_songs_pnew_w[$find_down]['id'] ."==". $get_Song_title['id']."</br>";
																									if(isset($get_media_info_down_gal) && $get_media_info_down_gal != null)
																									{
																										if($get_songs_pnew_w[$find_down]['id']==$get_media_info_down_gal['id']){
																										$free_down_rel = "yes";
																										}
																										else{
																											if($free_down_rel =="" || $free_down_rel =="no"){
																												$free_down_rel = "no";
																											}
																										}
																									}
																								}
																								if($free_down_rel == "yes"){
																								 ?>
																								<!-- <a href="javascript:void(0);" onclick="download_directly_song('<?php echo $get_media_info_down_gal['id'];?>','<?php echo str_replace("'","\'",$get_media_info_down_gal['title']);?>','<?php echo str_replace("'","\'",$get_media_info_down_gal['creator']);?>','<?php echo str_replace("'","\'",$get_media_info_down_gal['from']);?>')"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>-->
																								<?php
																								}
																								else if($get_media_info_down_gal['sharing_preference']==2){
																								 ?>
																								<!--<a href="javascript:void(0);" onclick="ask_to_donate('<?php echo str_replace("'","\'",$get_media_info_down_gal['title']);?>','<?php echo str_replace("'","\'",$get_media_info_down_gal['creator']);?>','<?php echo $get_media_info_down_gal['id'];?>','download_songproject<?php echo $get_media_info_down_gal['id'];?>')"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>-->
																							 <?php
																								}
																								else{
																							?>
																								<!--<a href="javascript:void(0);" onclick="down_song_pop_project(<?php echo $get_media_info_down_gal['id'];?>)"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>-->
																							<?php
																								}
																							}else{
																							?>	
																							<!--<img src="../images/profile/download-over.gif" style="cursor:default" />-->
																							<?php
																							}
																							?>
																							<!--<a href="addtocart.php?seller_id=<?php echo $get_media_info_down_gal['general_user_id'];?>&media_id=<?php echo $get_media_info_down_gal['id'];?>&buyer_id=<?php echo $_SESSION['gen_user_id'];?>&title=<?php echo str_replace("'","\'",$get_media_info_down_gal['title']);?>&image_name=<?php echo $song_image_name_cartproject;?>" id="download_songproject<?php echo $get_media_info_down_gal['id'];?>" style="display:none;"></a>-->
																							<script type="text/javascript">
																							$(document).ready(function() {
																									$("#download_songproject<?php echo $get_media_info_down_gal['id'];?>").fancybox({
																									'width'				: '75%',
																									'height'			: '75%',
																									'transitionIn'		: 'none',
																									'transitionOut'		: 'none',
																									'type'				: 'iframe'
																									});
																							});
																							</script>
																						<?php
																						}if($sep_ids_irp['featured_media'] == "Gallery"  && $sep_ids_irp['media_id'] != "0"){
																							$get_all_feature_image3 = $sql_feeds->get_all_gallery_images($sep_ids_irp['media_id']);
																							if($get_all_feature_image3!="" && $get_all_feature_image3!=NULL)
																{
																							if(mysql_num_rows($get_all_feature_image3)>0)
																							{
																								$mediaSrc3 = "";
																								$galleryImagesData3 = "";
																								$galleryImagestitle3 = "";
																								$get_news_general_user_id = $uk -> get_general_user_idby_mediaid($sep_ids_irp['media_id']);
																								while($row = mysql_fetch_assoc($get_all_feature_image3))
																								{
																									$mediaSrc3 = $row[0]['image_name'];
																									$filePath = $s3->getNewURI_otheruser("medgallery",$get_news_general_user_id).$mediaSrc3;
																									$galleryImagesData3 = $galleryImagesData3 .",". $row['image_name'];
																									$galleryImagestitle3 = $galleryImagestitle3 .",".$row['image_title'];
																								}
																								$orgPath3 = $s3->getNewURI_otheruser("medgallery",$get_news_general_user_id);
																								$thumbPath3 = $s3->getNewURI_otheruser("medgalthumb",$get_news_general_user_id);
																							}
																							}
																						?>
																							<a href="javascript:void(0);" onclick="checkload('<?php echo $mediaSrc3;?>','<?php echo $galleryImagesData3; ?>','<?php echo $thumbPath3; ?>','<?php echo $orgPath3; ?>','<?php echo $galleryImagestitle3; ?>','<?php echo $get_gallery['id'];?>')">
																							<img src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'"></a>
																							<img src="images/profile/add.gif" onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'">
																							<?php
																							$get_media_info_down_gal = $sql_feeds ->get_media_details($sep_ids_irp['media_id']);
																							$get_gal_image_project  = $sql_feeds ->get_gallery_info($sep_ids_irp['media_id']);
																							if($get_media_info_down_gal['sharing_preference']==2)
																							{
																							?>
																								<!--<a href="javascript:void(0);" onclick="ask_to_donate('<?php echo str_replace("'","\'",$get_media_info_down_gal['title']);?>','<?php echo str_replace("'","\'",$get_media_info_down_gal['creator']);?>','<?php echo $get_media_info_down_gal['id'];?>','download_galproject<?php echo $sep_ids_irp['media_id'];?>')"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>-->
																							 <?php
																							}else{
																							?>
																							<!--<img src="../images/profile/download-over.gif" style="cursor:default"/>-->
																							<?php
																							}
																							?>
																							<!--<a href="addtocart.php?seller_id=<?php echo $get_media_info_down_gal['general_user_id'];?>&media_id=<?php echo $get_media_info_down_gal['id'];?>&buyer_id=<?php echo $_SESSION['gen_user_id'];?>&title=<?php echo str_replace("'","\'",$get_media_info_down_gal['title']);?>&image_name=<?php echo $get_gal_image_project['profile_image'];?>" id="download_galproject<?php echo $sep_ids_irp['media_id'];?>" style="display:none;"></a>-->
																							<script type="text/javascript">
																							$(document).ready(function() {
																									$("#download_galproject<?php echo $sep_ids_irp['media_id'];?>").fancybox({
																									'width'				: '75%',
																									'height'			: '75%',
																									'transitionIn'		: 'none',
																									'transitionOut'		: 'none',
																									'type'				: 'iframe'
																									});
																							});
																							</script>
																						<?php
																						}if($sep_ids_irp['featured_media'] == "Video" && $sep_ids_irp['media_id'] != "0"){
																						?>
																							<img src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" onclick="showPlayer('v','<?php echo $sep_ids_irp['media_id'];?>','playlist');"/>
																							<img src="images/profile/add.gif" onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'" onclick="showPlayer('v','<?php echo $sep_ids_irp['media_id'];?>','addvi');"/>
																							<?php
																							$get_media_info_down_gal = $sql_feeds ->get_media_details($sep_ids_irp['media_id']);
																							if($get_media_info_down_gal['sharing_preference']==5 || $get_media_info_down_gal['sharing_preference']==2)
																							{
																								$get_video_image = $sql_feeds->getvideoimage_feature($get_media_info_down_gal['id']);
																								//$image_name_cartproject = "https://img.youtube.com/vi/".$get_video_image."/default.jpg";
																								if($get_video_image[1]=='youtube')
																			{
																				$image_name_cartproject = "http://img.youtube.com/vi/".$get_video_image[0]."/default.jpg";
																			}
																			else
																			{
																				$video_vim = $vimeo->call('vimeo.videos.getInfo', array('video_id' => $get_video_image[0]));
																				$image_name_cartproject = $video_vim->video[0]->thumbnails->thumbnail[1]->_content;
																			}
																								$free_down_rel_video="";
																								for($find_down=0;$find_down<=count($get_vids_pnew_w);$find_down++)
																								{
																									if(isset($get_media_info_down_gal) && $get_media_info_down_gal != null){
																										if($get_vids_pnew_w[$find_down]['id']==$get_media_info_down_gal['id']){
																											$free_down_rel_video = "yes";
																										}
																										else{
																											if($free_down_rel_video =="" || $free_down_rel_video =="no"){
																												$free_down_rel_video = "no";
																											}
																										}
																									}
																								}
																								if($free_down_rel_video == "yes"){
																								 ?>
																								 <!--<a href="javascript:void(0);" onclick="download_directly_song('<?php echo $get_media_info_down_gal['id'];?>','<?php echo str_replace("'","\'",$get_media_info_down_gal['title']);?>','<?php echo str_replace("'","\'",$get_media_info_down_gal['creator']);?>','<?php echo str_replace("'","\'",$get_media_info_down_gal['from']);?>')"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>-->
																								 
																							 <?php
																								}
																								else if($get_media_info_down_gal['sharing_preference']==2){
																								 ?>
																									<!--<a href="javascript:void(0);" onclick="ask_to_donate('<?php echo str_replace("'","\'",$get_media_info_down_gal['title']);?>','<?php echo str_replace("'","\'",$get_media_info_down_gal['creator']);?>','<?php echo $get_media_info_down_gal['id'];?>','download_videoproject<?php echo $sep_ids_irp['media_id'];?>')"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>-->
																								<?php
																								}
																								else{
																							?>
																								<!--<a href="javascript:void(0);" onclick="down_video_pop_project(<?php echo $sep_ids_irp['media_id'];?>)"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>-->
																							<?php
																								}
																							}
																							else{
																							?>
																							<!--<img src="../images/profile/download-over.gif" style="cursor:default"/>-->
																							<?php
																							}
																							?>
																							<!--<a href="addtocart.php?seller_id=<?php echo $get_media_info_down_gal['general_user_id'];?>&media_id=<?php echo $get_media_info_down_gal['id'];?>&buyer_id=<?php echo $_SESSION['gen_user_id'];?>&title=<?php echo str_replace("'","\'",$get_media_info_down_gal['title']);?>&image_name=<?php echo $image_name_cartproject;?>" id="download_videoproject<?php echo $sep_ids_irp['media_id'];?>" style="display:none;"></a>-->
																							<script type="text/javascript">
																							$(document).ready(function() {
																									$("#download_videoproject<?php echo $sep_ids_irp['media_id'];?>").fancybox({
																									'height'			: '75%',
																									'width'				: '69%',
																									'transitionIn'		: 'none',
																									'transitionOut'		: 'none',
																									'type'				: 'iframe'
																									});
																							});
																							</script>
																						<?php
																						}
																					}
																					?>
																					</div>
																				</div>
																				<div class="sideR">
																					<div class="activityTitle"><?php
																					if($ans_projects_posts[$vppr_all]['profile_type']=='projects_post')
																					{
																					if($sep_ids_irp['profile_url']!="") {
		?>																				
																					<a href="/<?php echo $sep_ids_irp['profile_url']; ?>">
		<?php
																		}
																						echo $sep_ids_irp['title'];
																						if($sep_ids_irp['profile_url']!="") {
		?>																				
																					</a>
		<?php
																		}
																					}?>
																						<div style="color:#808080;font-size:12px;">
																							<div class="comment more">
																								<?php echo $ans_projects_posts[$vppr_all]['description']; ?>
																							</div>
																						</div>
																					</div>
																					<!--<div class="activityContent">Tagged: <?php //echo $medias['tagged_users']; 
																						//$name_org = explode(')',$medias['tagged_users']);
																						//for($i_name_org=0;$i_name_org<count($name_org);$i_name_org++)
																						//{
																						//	$end_pos = strpos($name_org[$i_name_org],'(');
																						//	$org_name = substr($name_org[$i_name_org],0,$end_pos);
																					//		echo $org_name;
																					//	}
																					?></div>-->
																				</div>
																			</div>
																		</div>
																	</div>
																	<div class="rightSide">
																		<a href="#" onmouseover="mopen('mm<?php echo $vppr_all;?>22')" onmouseout="mclosetime()"><img src="images/profile/arrow.png" onmouseover="this.src='images/profile/arrow-down.png'" onmouseout="this.src='images/profile/arrow.png'" /></a>
																		<div id="mm<?php echo $vppr_all;?>22" class="popUp" onmouseover="mcancelclosetime()" onmouseout="mclosetime()">
																			<ul>
																				<li><a href="hideposts.php?id=<?php echo $ans_projects_posts[$vppr_all]['id'];?>">Hide Feed</a></li>
																				<li><a href="hideposts.php?report_id=<?php echo $ans_all_posts[$po_all]['id'];?>">Report spam</a></li>
																				<li><div class="seperator"></div></li>
																				<li><a href="">Unsubscribe from <?php echo $gens['fname'].' '.$gens['lname']; ?></a></li>
																			</ul>
																		</div>
																	</div>
																</div>
			<?php
																}
															}
														}
													}
													elseif($ans_projects_posts[$vppr_all]['display_to']=='Subscribers')
													{
														if($gens!="")
														{
															$subscribers_all =	$sql_feeds->subscribers_cond($chk_sql['email'],$gens['community_id'],$gens['artist_id']);
															if($subscribers_all=='1')
															{
																$hide6 = 0;
																$get_gen_id = $sql_feeds->get_general_info();
																$hide_by = $ans_projects_posts[$vppr_all]['hidden_by'];
																$exp_hide_by = explode(",",$hide_by);
																for($exp_count=0;$exp_count<=count($exp_hide_by);$exp_count++)
																{
																	if($exp_hide_by[$exp_count] ==$get_gen_id['general_user_id'])
																	{
																		$hide6 =1;
																	}
																}
																if($hide6 != 1)
																{
			?>
																<div class="storyCont">
																	<div class="leftSide">
																		<div class="avatar">
																			<img src="<?php echo $generalproimageURI.$gens['image_name']; ?>" />
																	</div>
																	<?php //echo "table=".$reg_table_media_type;
																	//echo $ans_audio_feeds[$audio_fed_lo]['id'];
																	?>
																		<div class="storyContainer">
																			<div class="storyTitle">
																				<span class="userName"><a href=""><?php echo $gens['fname'].' '.$gens['lname']; ?></a></span>  <?php //echo $ans_projects_posts[$vppr_all]['description']; ?>
																			</div>
																			<div class="activityCont">
																				<div class="sideL">
																					<div class="storyImage">
		<?php																			if($sep_ids_irp['profile_url']!="") {
		?>																				
																					<a href="/<?php echo $sep_ids_irp['profile_url']; ?>">
		<?php
																		}
																		?>
																					<img width="90" height="90" src="<?php if($sep_ids_irp['image_name']!="") { echo $sep_ids_irp['image_name']; } else { echo "http://comjcropprofile.s3.amazonaws.com/Noimage.png"; }?>" />
		<?php																			if($sep_ids_irp['profile_url']!="") {
		?>																				
																					</a>
		<?php
																		}?></div>
																					<div class="player">
																					<?php
																						if($sep_ids_irp['featured_media']!="" || $sep_ids_irp['featured_media']!="nodisplay"){
																						if($sep_ids_irp['featured_media'] == "Song" && $sep_ids_irp['media_id'] != "0"){
																						?>
																							<img src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" onclick="showPlayer('a','<?php echo $sep_ids_irp['media_id'];?>','play');" />
																							<img src="images/profile/add.gif" onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'" onclick="showPlayer('a','<?php echo $sep_ids_irp['media_id'];?>','add');" />
																							<?php
																							$get_media_info_down_gal = $sql_feeds ->get_media_details($sep_ids_irp['media_id']);
																							if($get_media_info_down_gal['sharing_preference']==5 || $get_media_info_down_gal['sharing_preference']==2)
																							{
																								$song_image_name_cartproject = $sql_feeds->get_audio_img($get_media_info_down_gal['id']);
																								$free_down_rel ="";
																								for($find_down=0;$find_down<=count($get_songs_pnew_w);$find_down++)
																								{
																									//echo $get_songs_pnew_w[$find_down]['id'] ."==". $get_Song_title['id']."</br>";
																									if(isset($get_media_info_down_gal) && $get_media_info_down_gal != null)
																									{
																										if($get_songs_pnew_w[$find_down]['id']==$get_media_info_down_gal['id']){
																										$free_down_rel = "yes";
																										}
																										else{
																											if($free_down_rel =="" || $free_down_rel =="no"){
																												$free_down_rel = "no";
																											}
																										}
																									}
																								}
																								if($free_down_rel == "yes"){
																								 ?>
																								 <!--<a href="javascript:void(0);" onclick="download_directly_song('<?php echo $get_media_info_down_gal['id'];?>','<?php echo str_replace("'","\'",$get_media_info_down_gal['title']);?>','<?php echo str_replace("'","\'",$get_media_info_down_gal['creator']);?>','<?php echo str_replace("'","\'",$get_media_info_down_gal['from']);?>')"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>-->
																								<?php
																								}
																								else if($get_media_info_down_gal['sharing_preference']==2){
																								 ?>
																								<!--<a href="javascript:void(0);" onclick="ask_to_donate('<?php echo str_replace("'","\'",$get_media_info_down_gal['title']);?>','<?php echo str_replace("'","\'",$get_media_info_down_gal['creator']);?>','<?php echo $get_media_info_down_gal['id'];?>','download_songproject<?php echo $get_media_info_down_gal['id'];?>')"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>-->
																							 <?php
																								}
																								else{
																							?>
																								<!--<a href="javascript:void(0);" onclick="down_song_pop_project(<?php echo $get_media_info_down_gal['id'];?>)"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>-->
																							<?php
																								}
																							}else{
																							?>	
																							<!--<img src="../images/profile/download-over.gif" style="cursor:default" />-->
																							<?php
																							}
																							?>
																							<!--<a href="addtocart.php?seller_id=<?php echo $get_media_info_down_gal['general_user_id'];?>&media_id=<?php echo $get_media_info_down_gal['id'];?>&buyer_id=<?php echo $_SESSION['gen_user_id'];?>&title=<?php echo str_replace("'","\'",$get_media_info_down_gal['title']);?>&image_name=<?php echo $song_image_name_cartproject;?>" id="download_songproject<?php echo $get_media_info_down_gal['id'];?>" style="display:none;"></a>-->
																							<script type="text/javascript">
																							$(document).ready(function() {
																									$("#download_songproject<?php echo $get_media_info_down_gal['id'];?>").fancybox({
																									'width'				: '75%',
																									'height'			: '75%',
																									'transitionIn'		: 'none',
																									'transitionOut'		: 'none',
																									'type'				: 'iframe'
																									});
																							});
																							</script>
																						<?php
																						}if($sep_ids_irp['featured_media'] == "Gallery"  && $sep_ids_irp['media_id'] != "0"){
																							$get_all_feature_image3 = $sql_feeds->get_all_gallery_images($sep_ids_irp['media_id']);
																							if($get_all_feature_image3!="" && $get_all_feature_image3!=NULL)
																{
																							if(mysql_num_rows($get_all_feature_image3)>0)
																							{
																								$mediaSrc3 = "";
																								$galleryImagesData3 = "";
																								$galleryImagestitle3 = "";
																								$get_news_general_user_id = $uk -> get_general_user_idby_mediaid($sep_ids_irp['media_id']);
																								while($row = mysql_fetch_assoc($get_all_feature_image3))
																								{
																									$mediaSrc3 = $row[0]['image_name'];
																									$filePath = $s3->getNewURI_otheruser("medgallery",$get_news_general_user_id).$mediaSrc3;
																									$galleryImagesData3 = $galleryImagesData3 .",". $row['image_name'];
																									$galleryImagestitle3 = $galleryImagestitle3 .",".$row['image_title'];
																								}
																								$orgPath3 = $s3->getNewURI_otheruser("medgallery",$get_news_general_user_id);
																								$thumbPath3 = $s3->getNewURI_otheruser("medgalthumb",$get_news_general_user_id);
																							}
																							}
																						?>
																							<a href="javascript:void(0);" onclick="checkload('<?php echo $mediaSrc3;?>','<?php echo $galleryImagesData3; ?>','<?php echo $thumbPath3; ?>','<?php echo $orgPath3; ?>','<?php echo $galleryImagestitle3; ?>','<?php echo $get_gallery['id'];?>')">
																							<img src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'"></a>
																							<img src="images/profile/add.gif" onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'">
																							<?php
																							$get_media_info_down_gal = $sql_feeds ->get_media_details($sep_ids_irp['media_id']);
																							$get_gal_image_project  = $sql_feeds ->get_gallery_info($sep_ids_irp['media_id']);
																							if($get_media_info_down_gal['sharing_preference']==2)
																							{
																							?>
																								<!--<a href="javascript:void(0);" onclick="ask_to_donate('<?php echo str_replace("'","\'",$get_media_info_down_gal['title']);?>','<?php echo str_replace("'","\'",$get_media_info_down_gal['creator']);?>','<?php echo $get_media_info_down_gal['id'];?>','download_galproject<?php echo $sep_ids_irp['media_id'];?>')"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>-->
																							 <?php
																							}else{
																							?>
																							<!--<img src="../images/profile/download-over.gif" style="cursor:default"/>-->
																							<?php
																							}
																							?>
																							<!--<a href="addtocart.php?seller_id=<?php echo $get_media_info_down_gal['general_user_id'];?>&media_id=<?php echo $get_media_info_down_gal['id'];?>&buyer_id=<?php echo $_SESSION['gen_user_id'];?>&title=<?php echo str_replace("'","\'",$get_media_info_down_gal['title']);?>&image_name=<?php echo $get_gal_image_project['profile_image'];?>" id="download_galproject<?php echo $sep_ids_irp['media_id'];?>" style="display:none;"></a>-->
																							<script type="text/javascript">
																							$(document).ready(function() {
																									$("#download_galproject<?php echo $sep_ids_irp['media_id'];?>").fancybox({
																									'width'				: '75%',
																									'height'			: '75%',
																									'transitionIn'		: 'none',
																									'transitionOut'		: 'none',
																									'type'				: 'iframe'
																									});
																							});
																							</script>
																						<?php
																						}if($sep_ids_irp['featured_media'] == "Video" && $sep_ids_irp['media_id'] != "0"){
																						?>
																							<img src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" onclick="showPlayer('v','<?php echo $sep_ids_irp['media_id'];?>','playlist');"/>
																							<img src="images/profile/add.gif" onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'" onclick="showPlayer('v','<?php echo $sep_ids_irp['media_id'];?>','addvi');"/>
																							<img style="cursor: default;" src="images/profile/download-over.gif">
																						<?php
																						}
																					}
																					?>
																					</div>
																				</div>
																				<div class="sideR">
																					<div class="activityTitle"><?php
																					if($ans_projects_posts[$vppr_all]['profile_type']=='projects_post')
																					{
																					if($sep_ids_irp['profile_url']!="") {
		?>																				
																					<a href="/<?php echo $sep_ids_irp['profile_url']; ?>">
		<?php
																		}
																						echo $sep_ids_irp['title'];
																						if($sep_ids_irp['profile_url']!="") {
		?>																				
																					</a>
		<?php
																		}
																					}?>
																						<div style="color:#808080;font-size:12px;">	
																							<div class="comment more">
																								<?php echo $ans_projects_posts[$vppr_all]['description']; ?>
																							</div>
																						</div>
																					</div>
																					<!--<div class="activityContent">Tagged: <?php //echo $medias['tagged_users']; 
																						//$name_org = explode(')',$medias['tagged_users']);
																						//for($i_name_org=0;$i_name_org<count($name_org);$i_name_org++)
																						//{
																						//	$end_pos = strpos($name_org[$i_name_org],'(');
																						//	$org_name = substr($name_org[$i_name_org],0,$end_pos);
																					//		echo $org_name;
																					//	}
																					?></div>-->
																				</div>
																			</div>
																		</div>
																	</div>
																	<div class="rightSide">
																		<a href="#" onmouseover="mopen('mm<?php echo $vppr_all;?>23')" onmouseout="mclosetime()"><img src="images/profile/arrow.png" onmouseover="this.src='images/profile/arrow-down.png'" onmouseout="this.src='images/profile/arrow.png'" /></a>
																		<div id="mm<?php echo $vppr_all;?>23" class="popUp" onmouseover="mcancelclosetime()" onmouseout="mclosetime()">
																			<ul>
																				<li><a href="hideposts.php?id=<?php echo $ans_projects_posts[$vppr_all]['id'];?>">Hide Feed</a></li>
																				<li><a href="hideposts.php?report_id=<?php echo $ans_all_posts[$po_all]['id'];?>">Report spam</a></li>
																				<li><div class="seperator"></div></li>
																				<li><a href="">Unsubscribe from <?php echo $gens['fname'].' '.$gens['lname']; ?></a></li>
																			</ul>
																		</div>
																	</div>
																</div>
			<?php
																}
															}
														}
													}
													elseif($ans_projects_posts[$vppr_all]['display_to']=='Friends')
													{
														if($gens!="")
														{
															$friends_c = $sql_feeds->friends_cond($ans_projects_posts[$vppr_all]['general_user_id'],$_SESSION['login_id']);
															
															if($friends_c=='1')
															{	
																$hide7 = 0;
																$get_gen_id = $sql_feeds->get_general_info();
																$hide_by = $ans_projects_posts[$vppr_all]['hidden_by'];
																$exp_hide_by = explode(",",$hide_by);
																for($exp_count=0;$exp_count<=count($exp_hide_by);$exp_count++)
																{
																	if($exp_hide_by[$exp_count] ==$get_gen_id['general_user_id'])
																	{
																		$hide7 =1;
																	}
																}
																if($hide7 != 1)
																{
			?>
																<div class="storyCont">
																	<div class="leftSide">
																		<div class="avatar">
																			<img src="<?php echo $generalproimageURI.$gens['image_name']; ?>" />
																	</div>
																	<?php //echo "table=".$reg_table_media_type;
																	//echo $ans_audio_feeds[$audio_fed_lo]['id'];
																	?>
																		<div class="storyContainer">
																			<div class="storyTitle">
																				<span class="userName"><a href=""><?php echo $gens['fname'].' '.$gens['lname']; ?></a></span>  <?php //echo $ans_projects_posts[$vppr_all]['description']; ?>
																			</div>
																			<div class="activityCont">
																				<div class="sideL">
																					<div class="storyImage">
		<?php																			if($sep_ids_irp['profile_url']!="") {
		?>																				
																					<a href="/<?php echo $sep_ids_irp['profile_url']; ?>">
		<?php
																		}?>
																					<img width="90" height="90" src="<?php if($sep_ids_irp['image_name']!="") { echo $sep_ids_irp['image_name']; } else { echo "http://comjcropprofile.s3.amazonaws.com/Noimage.png"; }?>" />
		<?php																			if($sep_ids_irp['profile_url']!="") {
		?>																				
																					</a>
		<?php
																		}?>
																					</div>
																					<div class="player">
																					<?php
																						if($sep_ids_irp['featured_media']!="" || $sep_ids_irp['featured_media']!="nodisplay"){
																						if($sep_ids_irp['featured_media'] == "Song" && $sep_ids_irp['media_id'] != "0"){
																						?>
																							<img src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" onclick="showPlayer('a','<?php echo $sep_ids_irp['media_id'];?>','play');" />
																							<img src="images/profile/add.gif" onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'" onclick="showPlayer('a','<?php echo $sep_ids_irp['media_id'];?>','add');" />
																							<?php
																							$get_media_info_down_gal = $sql_feeds ->get_media_details($sep_ids_irp['media_id']);
																							if($get_media_info_down_gal['sharing_preference']==5 || $get_media_info_down_gal['sharing_preference']==2)
																							{
																								$song_image_name_cartproject = $sql_feeds->get_audio_img($get_media_info_down_gal['id']);
																								$free_down_rel ="";
																								for($find_down=0;$find_down<=count($get_songs_pnew_w);$find_down++)
																								{
																									//echo $get_songs_pnew_w[$find_down]['id'] ."==". $get_Song_title['id']."</br>";
																									if(isset($get_media_info_down_gal) && $get_media_info_down_gal != null)
																									{
																										if($get_songs_pnew_w[$find_down]['id']==$get_media_info_down_gal['id']){
																										$free_down_rel = "yes";
																										}
																										else{
																											if($free_down_rel =="" || $free_down_rel =="no"){
																												$free_down_rel = "no";
																											}
																										}
																									}
																								}
																								if($free_down_rel == "yes"){
																								 ?>
																								 <!--<a href="javascript:void(0);" onclick="download_directly_song('<?php echo $get_media_info_down_gal['id'];?>','<?php echo str_replace("'","\'",$get_media_info_down_gal['title']);?>','<?php echo str_replace("'","\'",$get_media_info_down_gal['creator']);?>','<?php echo str_replace("'","\'",$get_media_info_down_gal['from']);?>')"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>-->
																								<?php
																								}
																								else if($get_media_info_down_gal['sharing_preference']==2){
																								 ?>
																								<!--<a href="javascript:void(0);" onclick="ask_to_donate('<?php echo str_replace("'","\'",$get_media_info_down_gal['title']);?>','<?php echo str_replace("'","\'",$get_media_info_down_gal['creator']);?>','<?php echo $get_media_info_down_gal['id'];?>','download_songproject<?php echo $get_media_info_down_gal['id'];?>')"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>-->
																							 <?php
																								}
																								else{
																							?>
																								<!--<a href="javascript:void(0);" onclick="down_song_pop_project(<?php echo $get_media_info_down_gal['id'];?>)"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>-->
																							<?php
																								}
																							}else{
																							?>	
																							<!--<img src="../images/profile/download-over.gif" style="cursor:default" />-->
																							<?php
																							}
																							?>
																							<!--<a href="addtocart.php?seller_id=<?php echo $get_media_info_down_gal['general_user_id'];?>&media_id=<?php echo $get_media_info_down_gal['id'];?>&buyer_id=<?php echo $_SESSION['gen_user_id'];?>&title=<?php echo str_replace("'","\'",$get_media_info_down_gal['title']);?>&image_name=<?php echo $song_image_name_cartproject;?>" id="download_songproject<?php echo $get_media_info_down_gal['id'];?>" style="display:none;"></a>-->
																							<script type="text/javascript">
																							$(document).ready(function() {
																									$("#download_songproject<?php echo $get_media_info_down_gal['id'];?>").fancybox({
																									'width'				: '75%',
																									'height'			: '75%',
																									'transitionIn'		: 'none',
																									'transitionOut'		: 'none',
																									'type'				: 'iframe'
																									});
																							});
																							</script>
																						<?php
																						}if($sep_ids_irp['featured_media'] == "Gallery"  && $sep_ids_irp['media_id'] != "0"){
																							$get_all_feature_image3 = $sql_feeds->get_all_gallery_images($sep_ids_irp['media_id']);
																							if($get_all_feature_image3!="" && $get_all_feature_image3!=NULL)
																{
																							if(mysql_num_rows($get_all_feature_image3)>0)
																							{
																								$mediaSrc3 = "";
																								$galleryImagesData3 = "";
																								$galleryImagestitle3 = "";
																								$get_news_general_user_id = $uk -> get_general_user_idby_mediaid($sep_ids_irp['media_id']);
																								while($row = mysql_fetch_assoc($get_all_feature_image3))
																								{
																									$mediaSrc3 = $row[0]['image_name'];
																									$filePath = $s3->getNewURI_otheruser("medgallery",$get_news_general_user_id).$mediaSrc3;
																									$galleryImagesData3 = $galleryImagesData3 .",". $row['image_name'];
																									$galleryImagestitle3 = $galleryImagestitle3 .",".$row['image_title'];
																								}
																								$orgPath3 = $s3->getNewURI_otheruser("medgallery",$get_news_general_user_id);
																								$thumbPath3 = $s3->getNewURI_otheruser("medgalthumb",$get_news_general_user_id);
																							}
																							}
																						?>
																							<a href="javascript:void(0);" onclick="checkload('<?php echo $mediaSrc3;?>','<?php echo $galleryImagesData3; ?>','<?php echo $thumbPath3; ?>','<?php echo $orgPath3; ?>','<?php echo $galleryImagestitle3; ?>','<?php echo $get_gallery['id'];?>')">
																							<img src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'"></a>
																							<img src="images/profile/add.gif" onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'">
																							<?php
																							$get_media_info_down_gal = $sql_feeds ->get_media_details($sep_ids_irp['media_id']);
																							$get_gal_image_project  = $sql_feeds ->get_gallery_info($sep_ids_irp['media_id']);
																							if($get_media_info_down_gal['sharing_preference']==2)
																							{
																							?>
																								<!--<a href="javascript:void(0);" onclick="ask_to_donate('<?php echo str_replace("'","\'",$get_media_info_down_gal['title']);?>','<?php echo str_replace("'","\'",$get_media_info_down_gal['creator']);?>','<?php echo $get_media_info_down_gal['id'];?>','download_galproject<?php echo $sep_ids_irp['media_id'];?>')"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>-->
																							 <?php
																							}else{
																							?>
																							<!--<img src="../images/profile/download-over.gif" style="cursor:default"/>-->
																							<?php
																							}
																							?>
																							<!--<a href="addtocart.php?seller_id=<?php echo $get_media_info_down_gal['general_user_id'];?>&media_id=<?php echo $get_media_info_down_gal['id'];?>&buyer_id=<?php echo $_SESSION['gen_user_id'];?>&title=<?php echo str_replace("'","\'",$get_media_info_down_gal['title']);?>&image_name=<?php echo $get_gal_image_project['profile_image'];?>" id="download_galproject<?php echo $sep_ids_irp['media_id'];?>" style="display:none;"></a>-->
																							<script type="text/javascript">
																							$(document).ready(function() {
																									$("#download_galproject<?php echo $sep_ids_irp['media_id'];?>").fancybox({
																									'width'				: '75%',
																									'height'			: '75%',
																									'transitionIn'		: 'none',
																									'transitionOut'		: 'none',
																									'type'				: 'iframe'
																									});
																							});
																							</script>
																						<?php
																						}if($sep_ids_irp['featured_media'] == "Video" && $sep_ids_irp['media_id'] != "0"){
																						?>
																							<img src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" onclick="showPlayer('v','<?php echo $sep_ids_irp['media_id'];?>','playlist');"/>
																							<img src="images/profile/add.gif" onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'" onclick="showPlayer('v','<?php echo $sep_ids_irp['media_id'];?>','addvi');"/>
																							<?php
																							$get_media_info_down_gal = $sql_feeds ->get_media_details($sep_ids_irp['media_id']);
																							if($get_media_info_down_gal['sharing_preference']==5 || $get_media_info_down_gal['sharing_preference']==2)
																							{
																								$get_video_image = $sql_feeds->getvideoimage_feature($get_media_info_down_gal['id']);
																								//$image_name_cartproject = "https://img.youtube.com/vi/".$get_video_image."/default.jpg";
																								if($get_video_image[1]=='youtube')
																			{
																				$image_name_cartproject = "http://img.youtube.com/vi/".$get_video_image[0]."/default.jpg";
																			}
																			else
																			{
																				$video_vim = $vimeo->call('vimeo.videos.getInfo', array('video_id' => $get_video_image[0]));
																				$image_name_cartproject = $video_vim->video[0]->thumbnails->thumbnail[1]->_content;
																			}
																								$free_down_rel_video="";
																								for($find_down=0;$find_down<=count($get_vids_pnew_w);$find_down++)
																								{
																									if(isset($get_media_info_down_gal) && $get_media_info_down_gal != null){
																										if($get_vids_pnew_w[$find_down]['id']==$get_media_info_down_gal['id']){
																											$free_down_rel_video = "yes";
																										}
																										else{
																											if($free_down_rel_video =="" || $free_down_rel_video =="no"){
																												$free_down_rel_video = "no";
																											}
																										}
																									}
																								}
																								if($free_down_rel_video == "yes"){
																								 ?>
																								 <!--<a href="javascript:void(0);" onclick="download_directly_song('<?php echo $get_media_info_down_gal['id'];?>','<?php echo str_replace("'","\'",$get_media_info_down_gal['title']);?>','<?php echo str_replace("'","\'",$get_media_info_down_gal['creator']);?>','<?php echo str_replace("'","\'",$get_media_info_down_gal['from']);?>')"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>-->
																								 
																							 <?php
																								}
																								else if($get_media_info_down_gal['sharing_preference']==2){
																								 ?>
																									<!--<a href="javascript:void(0);" onclick="ask_to_donate('<?php echo str_replace("'","\'",$get_media_info_down_gal['title']);?>','<?php echo str_replace("'","\'",$get_media_info_down_gal['creator']);?>','<?php echo $get_media_info_down_gal['id'];?>','download_videoproject<?php echo $sep_ids_irp['media_id'];?>')"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>-->
																								<?php
																								}
																								else{
																							?>
																								<!--<a href="javascript:void(0);" onclick="down_video_pop_project(<?php echo $sep_ids_irp['media_id'];?>)"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>-->
																							<?php
																								}
																							}
																							else{
																							?>
																							<!--<img src="../images/profile/download-over.gif" style="cursor:default"/>-->
																							<?php
																							}
																							?>
																							<!--<a href="addtocart.php?seller_id=<?php echo $get_media_info_down_gal['general_user_id'];?>&media_id=<?php echo $get_media_info_down_gal['id'];?>&buyer_id=<?php echo $_SESSION['gen_user_id'];?>&title=<?php echo str_replace("'","\'",$get_media_info_down_gal['title']);?>&image_name=<?php echo $image_name_cartproject;?>" id="download_videoproject<?php echo $sep_ids_irp['media_id'];?>" style="display:none;"></a>-->
																							<script type="text/javascript">
																							$(document).ready(function() {
																									$("#download_videoproject<?php echo $sep_ids_irp['media_id'];?>").fancybox({
																									'height'			: '75%',
																									'width'				: '69%',
																									'transitionIn'		: 'none',
																									'transitionOut'		: 'none',
																									'type'				: 'iframe'
																									});
																							});
																							</script>
																						<?php
																						}
																					}
																					?>
																					</div>
																				</div>
																				<div class="sideR">
																					<div class="activityTitle"><?php
																					if(isset($ans_projects_posts[$vppr_all]['profile_type']) && $ans_projects_posts[$vppr_all]['profile_type']=='projects_post')
																					{
																					if(isset($sep_ids_irp['profile_url']) && $sep_ids_irp['profile_url']!="") {
		?>																				
																					<a href="/<?php echo $sep_ids_irp['profile_url']; ?>">
		<?php
																		}
																						echo $sep_ids_irp['title'];
																						if(isset($sep_ids_irp['profile_url']) && $sep_ids_irp['profile_url']!="") {
		?>																				
																					</a>
		<?php
																		}
																					}?>
																						<div style="color:#808080;font-size:12px;">
																							<div class="comment more">
																								<?php echo $ans_projects_posts[$vppr_all]['description']; ?>
																							</div>
																						</div>
																					</div>
																					<!--<div class="activityContent">Tagged: <?php //echo $medias['tagged_users']; 
																						//$name_org = explode(')',$medias['tagged_users']);
																						//for($i_name_org=0;$i_name_org<count($name_org);$i_name_org++)
																						//{
																						//	$end_pos = strpos($name_org[$i_name_org],'(');
																						//	$org_name = substr($name_org[$i_name_org],0,$end_pos);
																					//		echo $org_name;
																					//	}
																					?></div>-->
																				</div>
																			</div>
																		</div>
																	</div>
																	<div class="rightSide">
																		<a href="#" onmouseover="mopen('mm<?php echo $vppr_all;?>24')" onmouseout="mclosetime()"><img src="images/profile/arrow.png" onmouseover="this.src='images/profile/arrow-down.png'" onmouseout="this.src='images/profile/arrow.png'" /></a>
																		<div id="mm<?php echo $vppr_all;?>24" class="popUp" onmouseover="mcancelclosetime()" onmouseout="mclosetime()">
																			<ul>
																				<li><a href="hideposts.php?id=<?php echo $ans_projects_posts[$vppr_all]['id'];?>">Hide Feed</a></li>
																				<li><a href="hideposts.php?report_id=<?php echo $ans_all_posts[$po_all]['id'];?>">Report spam</a></li>
																				<li><div class="seperator"></div></li>
																				<li><a href="">Unsubscribe from <?php echo $gens['fname'].' '.$gens['lname']; ?></a></li>
																			</ul>
																		</div>
																	</div>
																</div>
			<?php
																}
															}
														}
													}
												}
											}
										}
									}
							?>
							</div>
		<?php
					}
		?>
                    </div>
					<?php
					}
						else
						{
							echo "<p style='line-height: 28px;'>In order to receive news feed updates you will need to be subscribed to and/or become friends with Artists or Community users. Once you find a user you would like to friend or subscribe to click on the 'Add Friend' and 'Subscribe' button on their pages.</p>";
							echo "<input type='button' onclick='find_firends()' style='background-color: #000000;border: 0 none;color: #FFFFFF;cursor: pointer;float: left;font-size: 15px;font-weight: bold;padding: 7px 0;text-align: center;text-decoration: none;width: 120px;' value='Find Users'/>";
						}
					}
					else
					{
						echo "<p style='line-height: 28px;'>In order to receive news feed updates you will need to be subscribed to Artists or Community users. Begin your subscription by clicking on the 'Subscribe' button on their pages.</p>";
						echo "<input type='button' onclick='find_firends()' style='background-color: #000000;border: 0 none;color: #FFFFFF;cursor: pointer;float: left;font-size: 15px;font-weight: bold;padding: 7px 0;text-align: center;text-decoration: none;width: 120px;' value='Find Users'/>";
					}
					?>
                </div>
			</div>
		</div>
	</div>
</div>
</div>
				
				
				<input type="hidden" id="curr_playing_playlist" name="curr_playing_playlist" >

<?php 
if(isset($_GET['con_exs']) && $_GET['con_exs']==1)
{
?>

<script>
alert("Contact Already Exist.");
</script>	

<?php
}
?>
<script type="text/javascript">
 $(document).ready(function(){
 if(location.hash == "")
 {
		$("#addressbook").show();
 }
 
 
 
 $("a").each(
    function()
    {    
        if($(this).attr("href") == location.hash  && $(this).attr("href") != "" )
        {
           $(this).click();
        }
    });
 
 $(window).hashchange(
  function()
  {
   $("a").each(
   function()
   {    
    if($(this).attr("href") == location.hash  && $(this).attr("href") != "" )
    {
     $(this).click();
    }
   });
  }
 )
 
 });
</script>
<script type="text/javascript">
function confirmDeleteMail(delUrl1) 
{
	if (confirm("Are you sure you want to delete?")) 
	{
		alert("Mail is deleted successfully.");
		document.location = delUrl1;
	}
}

function confirmDeleteSup(delUrl1) 
{
	if (confirm("Are you sure you want to delete?")) 
	{
		alert("Subscription has been deleted successfully.");
		document.location = delUrl1;
	}
}

function alreadyMember(mem_var)
{
	
	/*if(mem_var==1)
	{
		alert("You are already a Member.");
		return false;
	}
	else
	{*/
		var isChecked = $("#member_age").is(":checked");
		if($("#member_ship_type").val() == "select" || $("#payment_mode").val()== "select")
		{
			alert("You Have To Select Member Ship Type.");
			return false;
		}
		else if($("#countrySelectmember").val() ==0 || $("#countrySelectmember").val()==" ")
		{
			alert("You must select country.");
			return false;
		}
		else if($("#stateSelectmember").val() ==0 || $("#stateSelectmember").val()==" ")
		{
			alert("You must select state.");
			return false;
		}
		else if($("#editcitymember").val() =="" || $("#editcitymember").val()==" ")
		{
			alert("You must enter city.");
			return false;
		}
		
		else if(!isChecked)
		{
			alert("You must confirm that you are 18 years old to purchase a Purify Art membership.");
			return false;
		}
		else
		{
			alert("First You Have To Complete Your Payment.");
			return false;
		}
		
	//}
}
</script>



<!--files for search technique
<script type="text/javascript" src="javascripts/jquery.js"></script>
<script type="text/javascript" src="javascripts/jquery.autocomplete.js"></script>
<link rel="stylesheet" href="css/jquery.autocomplete.css" type="text/css" />-->
<!--files for search technique ends here -->
<script type="text/javascript">
function newfunction(id,typ)
{
	$("#play_video_image"+id+typ).css({"background":"none repeat scroll 0 0 #000000","border-radius":"6px 6px 6px 6px"});
}function newfunctionout(id,typ)
{
	$("#play_video_image"+id+typ).css({"background":"none repeat scroll 0 0 #333333","border-radius":"6px 6px 6px 6px"});
}
					
/*
script for suggesting names in address book name field
 $(function() {
		$( "#user_add_name" ).autocomplete({
			source: "namesuggession.php",
			minLength: 1,
			select: function( event, ui ) {
			//document.getElementById("table_name").value = ui.item.value1;
			//document.getElementById("table_id").value = ui.item.value2;			
			}
		});
	});
*/
$(function()
{
	list_post();
	$("#post_list").change(function() 
	{
		//$('.jquery_ckeditor').html = "";
		$("#loader").show();
		$("#fieldCont_media_first").hide();
		$("#fieldCont_media").hide();
		$.post("PostList.php", { type_val:$("#post_list").val() },
		function(data)
		{
			$("#fieldCont_media_first").html(data);
			$("#fieldCont_media").html(data);
			$("#loader").hide();
			$("#fieldCont_media_first").show();
			$("#fieldCont_media").show();
			
		});
	});
	$('.list-wrap').css({"height":""});
});
$(function()
{
	list_post_first();
	$("#post_list_first").change(function() 
	{
		//$('.jquery_ckeditor').html = "";
		$("#loader_first").show();
		$("#fieldCont_media_first").hide();
		$.post("PostList_First.php", { type_val:$("#post_list_first").val() },
		function(data)
		{
			$("#fieldCont_media_first").html(data);
			$("#loader_first").hide();
			$("#fieldCont_media_first").show();
		});
	});
	$('.list-wrap').css({"height":""});
});
function allowpost(ids)
{
	document.getElementById('allowh_post').value = document.getElementById(ids).innerHTML;
	document.getElementById('allow_to').value = document.getElementById('allowh_post').value;
	
	if(document.getElementById('allow_to').value=='All')
	{
		document.getElementById("1o_post").innerHTML = 'Fans';
		document.getElementById("2o_post").innerHTML = 'Friends';
		document.getElementById("3o_post").innerHTML = 'Subscribers';
		$('#allow_to').css({"background-position":"32px"});
		$('#actualContent .fieldContainer').css({"float": "right","margin-bottom": "20px","position": "relative","width":"52px","top": "299px"});
	}
	else if(document.getElementById('allow_to').value=='Fans')
	{
		document.getElementById("1o_post").innerHTML = 'All';
		document.getElementById("2o_post").innerHTML = 'Friends';
		document.getElementById("3o_post").innerHTML = 'Subscribers';
		$('#allow_to').css({"background-position":"40px"});
		$('#actualContent .fieldContainer').css({"float": "right","margin-bottom": "20px","position": "relative","width":"59px","top": "299px"});
	}
	else if(document.getElementById('allow_to').value=='Friends')
	{
		document.getElementById("1o_post").innerHTML = 'All';
		document.getElementById("2o_post").innerHTML = 'Fans';
		document.getElementById("3o_post").innerHTML = 'Subscribers';
		$('#allow_to').css({"background-position":"53px"});
		$('#actualContent .fieldContainer').css({"float": "right","margin-bottom": "20px","position": "relative","width":"72px","top": "299px"});
	}
	else if(document.getElementById('allow_to').value=='Subscribers')
	{
		document.getElementById("1o_post").innerHTML = 'All';
		document.getElementById("2o_post").innerHTML = 'Fans';
		document.getElementById("3o_post").innerHTML = 'Friends';
		$('#allow_to').css({"background-position":"78px"});
		$('#actualContent .fieldContainer').css({"float": "right","margin-bottom": "20px","position": "relative","width":"97px","top": "299px"});
	}
}
function allowpost_first(ids)
{
	document.getElementById('allowh_post_first').value = document.getElementById(ids).innerHTML;
	document.getElementById('allow_to_first').value = document.getElementById('allowh_post_first').value;
	//alert(data);
	if(document.getElementById('allow_to_first').value=='All')
	{
		document.getElementById("1_post").innerHTML = 'Fans';
		document.getElementById("2_post").innerHTML = 'Friends';
		document.getElementById("3_post").innerHTML = 'Subscribers';
		$('#allow_to_first').css({"background-position":"32px"});
		$('#actualContent .fieldContainer').css({"float": "right","margin-bottom": "20px","position": "relative","width":"52px"});
	}
	else if(document.getElementById('allow_to_first').value=='Fans')
	{
		document.getElementById("1_post").innerHTML = 'All';
		document.getElementById("2_post").innerHTML = 'Friends';
		document.getElementById("3_post").innerHTML = 'Subscribers';
		$('#allow_to_first').css({"background-position":"40px"});
		$('#actualContent .fieldContainer').css({"float": "right","margin-bottom": "20px","position": "relative","width":"59px"});
	}
	else if(document.getElementById('allow_to_first').value=='Friends')
	{
		document.getElementById("1_post").innerHTML = 'All';
		document.getElementById("2_post").innerHTML = 'Fans';
		document.getElementById("3_post").innerHTML = 'Subscribers';
		$('#allow_to_first').css({"background-position":"53px"});
		$('#actualContent .fieldContainer').css({"float": "right","margin-bottom": "20px","position": "relative","width":"72px"});
	}
	else if(document.getElementById('allow_to_first').value=='Subscribers')
	{
		document.getElementById("1_post").innerHTML = 'All';
		document.getElementById("2_post").innerHTML = 'Fans';
		document.getElementById("3_post").innerHTML = 'Friends';
		$('#allow_to_first').css({"background-position":"78px"});
		$('#actualContent .fieldContainer').css({"float": "right","margin-bottom": "20px","position": "relative","width":"97px"});
	}
}
function list_post()
{
	$("#loader").show();
	$("#fieldCont_media").hide();
	$.post("PostList.php", { type_val:'images_post' },
	function(data)
	{
		$("#fieldCont_media").html(data);
		$("#loader").hide();
		$("#fieldCont_media").show();
	});
	$('.list-wrap').css({"height":""});
}
function list_post_first()
{
	$("#loader_first").show();
	$("#fieldCont_media_first").hide();
	$.post("PostList_First.php", { type_val:'images_post' },
	function(data)
	{
		$("#fieldCont_media_first").html(data);
		$("#loader_first").hide();
		$("#fieldCont_media_first").show();
	});
	$('.list-wrap').css({"height":""});
}
</script>
<!--Script For Name Suggession Box Ends Here-->

<!--Script For Email Suggession Box Starts Here for address book email field
<script type="text/javascript">
 $(function() {
		$( "#user_add_email" ).autocomplete({
			source: "suggession.php",
			minLength: 1,
			select: function( event, ui ) {
			//document.getElementById("table_name").value = ui.item.value1;
			//document.getElementById("table_id").value = ui.item.value2;			
			}
		});
	});
</script>-->
<script type="text/javascript">
function validate_email()
{
	 var email = document.getElementById("user_add_email").value;
	 var username = document.getElementById("user_add_name").value;
	 var atpos = email.indexOf("@");
	 var dotpos = email.lastIndexOf(".");
	
	 if(atpos<1 || dotpos<atpos+2 || dotpos+2>=email.lenght || email=="")
	   {
		alert("Please enter a valid email address.");
		return false;
	 }
	 else if(email=="")
	 {
		alert("Please Enter a email address.");
		return false;
	 }
	 else if(username=="")
	 {
		alert("Please Enter Name.");
		return false;
	 }
	 
}

function validate_post()
{
	var list_post = document.getElementById("list_epm[]");
	alert(list_post.value);
	if(list_post.value=="" || list_post.value==null)
	{
		alert("Please select any Media to Post.");
		return false;
	}
}
function validate_post_first()
{
	var list_post_first = document.getElementById("list_epm_first[]").value;
	if(list_post_first=="" || list_post_first==null)
	{
		alert("Please select any Media to Post.");
		return false;
	}
}
function confirm_subscriber_delete(s)
{
	var find = confirm("Are You Sure You Want To Delete "+s+" Subscription?");
	if(find == false)
	{
		return false;
	}
}
</script>

<script type="text/javascript">
	//<![CDATA[

		$(function()
		{
			var config = {
				toolbar:
				[
					['Bold', 'Italic', '-', 'NumberedList', 'BulletedList', '-', 'Link', 'Unlink'],
					['UIColor']
				],
				enterMode : CKEDITOR.ENTER_BR
			};

			// Initialize the editor.
			// Callback function can be passed and executed after full instance creation.
			$('.jquery_ckeditor').ckeditor(config);
		});

			//]]>
	</script>
<?php include_once("displayfooter.php"); ?>
</body>
<script type="text/javascript">
$(window).bind("load", function() {
	$(".list-wrap").css({"height":""});
});

function checkload(defImg,Images,orgpath,thumbpath,title,media_id){
	var arr_sep = Images.split(',');
	var title_sep = null;
	title_sep = title.split(',');
	//alert(title_sep);
	var k;
	var data = new Array();
	for(k=0;k<arr_sep.length;k++)
	{
		//arr_sep[k] = arr_sep[k].replace("\"","");
		arr_sep[k] = arr_sep[k].replace(/"/g, '');
		arr_sep[k] = arr_sep[k].replace("[","");
		arr_sep[k] = arr_sep[k].replace("]","");
		data[k] = arr_sep[k];
	}
	
	var q;
	var data1 = new Array();
	for(q=0;q<title_sep.length;q++)
	{
		//arr_sep[k] = arr_sep[k].replace("\"","");
		title_sep[q] = title_sep[q].replace(/"/g, '');
		title_sep[q] = title_sep[q].replace("[","");
		title_sep[q] = title_sep[q].replace("]","");
		data1[q] = title_sep[q];
	}
	
	if(data.length>0){
		$("#gallery-container").html('');
	}
	//$("#bgimg").attr('src','<?php echo $orgPath;?>'+defImg);
	//alert(orgpath+defImg);
	$("#bgimg").attr('src',thumbpath+defImg);
	

	for(var i=0;i<data.length;i++){

		$("#gallery-container").append("<div class='content'><div><a href='"+thumbpath+data[i]+"'><img  src='"+orgpath+data[i]+"' title='"+data1[i]+"' alt='"+data1[i]+"' class='thumb' /></a></div></div>");
	}
	//setTimeout(function(){	loadGallery();	$("#light-gal").css('visibility','visible');	},500);
	loadGallery();
	$("#light-gal").css('visibility','visible');
	$("#projectsTab").css('display','none');
	$("#miniFooter").css('display','none');
	$("#profileTabs").css('display','none');
	$("#mediaTab").css('display','none');
	$(".accountSettings").css('z-index','1');
	

	document.getElementById("gallery-container").style.left = "0px";
	$.ajax({
			type: "POST",
			url: 'update_media.php',
			data: { "media_id":media_id },
			success: function(data){
			}
	});
}



var windowRef=false;
//}
var profileName = "Mithesh";
function showPlayer(tp,ref,act){
//alert(act);
//alert('<?php echo $ans_sql['general_user_id'] ;?>');
	//var windowRef = window.open('player/?tp='+tp+'&ref='+ref+'&source='+source);
	var pl = $("#curr_playing_playlist").val();
	$.ajax({
		type: "POST",
		url: 'player/getMediaInfo.php',
		data: { "tp":tp, "ref":ref, "source":"<?php echo $ans_sql['general_user_id']; ?>", "act":act, "pl":pl, "creator":profileName, "update":"update" },
		success: function(data){
			get_cookie(data);
			var is_chrome = navigator.userAgent.toLowerCase().indexOf('chrome') > -1;
			if(is_chrome ==true){
				WinHeight = 579;
			}else{
				WinHeight = 580;
			}
			windowRef = window.open('', 'formpopup', 'height='+WinHeight+',width=560,resizable=no,scrollbars');
			if(windowRef == null || typeof(windowRef) == "undefined") {
			   alert("The media player is a pop up player to give you uninterrupted streaming while browsing the site. To view it you must allow pop ups from Purify Art in the settings of your browser.");
			   return false;
			}
			if(windowRef.location == "about:blank" || (act!="add" && act!="addvi")){
				windowRef.location.href='combinePlayer/index.php#tp='+tp+'&ref='+ref+'&pl='+data+'&buyer_id='+'<?php echo $_SESSION['gen_user_id'];?>'+'&act=play';
				//jQuery(window).blur();
				windowRef.focus();
			}
			else{
				windowRef.location.href='combinePlayer/index.php#tp='+tp+'&ref='+ref+'&pl='+data+'&buyer_id='+'<?php echo $_SESSION['gen_user_id'];?>';
				windowRef.focus();
			}
		}
	});
}
	
function showPlayer_reg(tp,ref,act,tbl){
	var pl = $("#curr_playing_playlist").val();
	$.ajax({
		type: "POST",
		url: 'player/getMediaInfo.php',
		data: { "tp":tp, "ref":ref, "tbl":tbl, "source":"<?php echo $ans_sql['general_user_id']; ?>", "act":act, "pl":pl, "creator":profileName, "update":"update" },
		success: function(data){
		//alert(data);
			//$("#curr_playing_playlist").val(data);
			get_cookie(data);
			var is_chrome = navigator.userAgent.toLowerCase().indexOf('chrome') > -1;
			if(is_chrome ==true){
				WinHeight = 579;
			}else{
				WinHeight = 580;
			}
			windowRef = window.open('', 'formpopup', 'height='+WinHeight+',width=560,resizable=no,scrollbars');
			if(windowRef == null || typeof(windowRef) == "undefined") {
			   alert("The media player is a pop up player to give you uninterrupted streaming while browsing the site. To view it you must allow pop ups from Purify Art in the settings of your browser.");
			   return false;
			}
			if(windowRef.location == "about:blank" || (act!="add" && act!="addvi" && act!="add_reg" && act!="addvi_reg")){
				windowRef.location.href='combinePlayer/index.php#tp='+tp+'&ref='+ref+'&pl='+data+'&buyer_id='+'<?php echo $_SESSION['gen_user_id'];?>'+'&act=play';
				//jQuery(window).blur();
				windowRef.focus();
			}
			else{
				windowRef.location.href='combinePlayer/index.php#tp='+tp+'&ref='+ref+'&pl='+data+'&buyer_id='+'<?php echo $_SESSION['gen_user_id'];?>';
				//jQuery(window).blur();
				windowRef.focus();
			}
		}
	});
}
function get_cookie(data)
{
	document.cookie="filename=" + data;
	getCookieValue("filename");
}
/*function get_cookie_obj(data)
{
	document.cookie="windowobj=" + data;
	getCookieValue_for_obj("windowobj");
}*/
function getCookieValue(key)
{
	currentcookie = document.cookie;
	if (currentcookie.length > 0)
	{
		firstidx = currentcookie.indexOf(key + "=");
		if (firstidx != -1)
		{
			firstidx = firstidx + key.length + 1;
			lastidx = currentcookie.indexOf(";",firstidx);
			if (lastidx == -1)
			{
				lastidx = currentcookie.length;
			}
			getdata = unescape(currentcookie.substring(firstidx, lastidx));
			$("#curr_playing_playlist").val(unescape(currentcookie.substring(firstidx, lastidx)));
			//return unescape(currentcookie.substring(firstidx, lastidx));
		}
		
	}
	return "";
}
$("document").ready(function(){
window.setInterval("getCookieValue('filename')",500);
});

function find_s3_account(detail)
{
	if(detail == "blank"){
		//alert("");
		document.getElementById("popup_s3_error").click();
		//document.getElementById(id).click();
	}
}

function lists_views()
{
	$("#list_height_incr").css({"height":""});	
}

function chk_amts()
{
	var whole_amt = document.getElementById("whole").value;
	if(whole_amt<=0)
	{
		alert("Your balance is zero. You cannot request the payment.");
		return false;
	}
	else
	{
		$("#req_pay_new").click();
	}
}

$("#close_membership").click(
	function () {
	$("#popupContactClose_member").click();
	window.open('profileedit.php?code_chks_member=trues#mymemberships');
});

$(window).load(function() {
	$("#all_wrap_personal").css({"display":"block"});
	$("#loader_body").css({"display":"none"});
});

function show_loader(){
	$("#all_wrap_personal").css({"display":"none"});
	$("#loader_body").css({"display":"block"});
}
</script>
</html>
<style>
	#beome_not:hover { background: none repeat scroll 0 0 #000000 !important; }
	.navButton { margin: 0 0 4px; width: 142px; float: left; position: absolute; left: 33%; top: 35%; }
.playMedia { float: left; margin-left: 1px; }
.playMedia a { height: inherit !important; left: 74px !important; width: inherit !important; border-radius: 6px 6px 6px 6px !important; bottom: 138px !important; clear: both; background: none repeat scroll 0 0 #333333; float:left; }
.playMedia a:hover { background:none repeat scroll 0 0 #000000; }
.playMedia .list_play img { margin-bottom: 0px !important; }
</style>
