<?php
	include("classes/ProfileDisplay.php");
	include("classes/GetSubscriptionInfo.php");
	include("newsmtp/PHPMailer/index.php");
	include_once('sendgrid/SendGrid_loader.php');
	session_start();
    $domainname=$_SERVER['SERVER_NAME'];
?>
<link rel="stylesheet" type="text/css" media="screen" href="includes/purify.css" />
<!--<script src='https://ajax.googleapis.com/ajax/libs/jquery/1.6.1/jquery.min.js'></script>-->
<script>
	function validate_fields_sub()
	{
		var sub_email = document.getElementById("sub_email").value;
		var sub_name = document.getElementById("sub_name").value;
		var atpos = sub_email.indexOf("@");
		var dotpos = sub_email.lastIndexOf(".");
		/* if (atpos<1 || dotpos<atpos+2 || dotpos+2>=sub_email.lenght || sub_email=="" || sub_email==null)
		{
			alert("Please enter a valid email address.");
			return false;
		} */
		/* if(sub_name=="" || sub_name==null)
		{
			alert("Please enter a your name.");
			return false;
		} */
		
		$("#member_email_new").css({"display":"block"});
		var check_vid_sub = $("#member_email_new").html();
		
		if(check_vid_sub == "You have already subscribed to this user." || check_vid_sub == "You have already subscribed to this event." || check_vid_sub == "You have already subscribed to this project.")
		{
			alert(check_vid_sub);
			return false;
		}
		
		//alert(check_vid_sub);
		//return false;
		if(check_vid_sub == "Please register first to subscribe.")
		{
			alert("Please register first to subscribe.");
			return false;
		}
	}
	$("document").ready(function(){
		document.getElementById("sub_email").value = '<?php if(isset($_SESSION['login_email'])){ echo $_SESSION['login_email']; }?>';

		
	});
	$("document").ready(function(){
			//$("#sub_email").focusout(function(){
				var text_value = document.getElementById("sub_email").value;
			//alert(text_value);
				var match_db_id = document.getElementById("rel_id_sub").value;
				var match_db_text = document.getElementById("rel_type_sub").value;
				//alert(match_db_text);
				var uurls = parent.document.URL;
				var text_data = 'reg_sub='+ text_value+'&'+'match_id_sub='+ match_db_id+'&'+'match_text_sub='+ match_db_text+'&'+'match_uuls='+ uurls;
				 
				$.ajax({	 
					type: 'POST',
					url : 'SubscriptionAjaxMembership.php',
					data: text_data,
					success: function(data) 
					{
						$("#member_email_new").html(data);
						
						if(data=="Please register first to subscribe.")
						{
							$("#links").css({"display":"block"});
						}
						else if(data == "same")
						{
							$("#member_email_same").css({"display":"block"});
						}
						else
						{
							$("#links").css({"display":"none"});
						}
					}
				});
			//});
		});
		
		function link_register()
		{
			window.parent.location = "https://www.".<?php echo $domainname; ?>;
			parent.jQuery.fancybox.close();
		}
		function link_after_subscription(link)
		{
			window.parent.location = link;
			parent.jQuery.fancybox.close();
		}
</script>
<?php
$sendgrid = new SendGrid('','');

if(!isset($_GET['pre_url']) || $_GET['pre_url'] == null || $_GET['pre_url']=="") //if($_GET['pre_url'] == null || $_GET['pre_url']=="")
{
	if(isset($_SERVER["HTTP_REFERER"])){
		$url = $_SERVER["HTTP_REFERER"];
	}else{
		$url ="";
	}
	$url_sep = explode('/',$url);
	if(isset($url_sep[3])){
		$match = $url_sep[3];
	}else{
		$match = "";
	}
}else{
$match = $_GET['pre_url'];
}
//echo $match;
$display = new ProfileDisplay();
$subscription_become = new GetSubscriptionInfo();

$artist_check = $display->artistCheck($match);
$community_check = $display->communityCheck($match);

$community_event_check = $display->communityEventCheck($match);
$community_project_check = $display->communityProjectCheck($match);

$artist_event_check = $display->artistEventCheck($match);
$artist_project_check = $display->artistProjectCheck($match);


if($artist_check!=0)
{
	$get_user_Info = $artist_check;
	$get_rel_id_sub=$get_user_Info['artist_id'];
	$get_rel_type_sub="artist";
	//$get_fan_Info = $get_fan_club->get_price_info($artist_check['artist_id'],"artist");
	$find_member_sub = $subscription_become->find_member($artist_check['artist_id'],"artist");
	$url_popup = $display->profile_url_pop("artist",$artist_check['artist_id']);
	//var_dump($find_member);
}

if($community_check!=0)
{
	$get_user_Info = $community_check;
	$get_rel_id_sub = $get_user_Info['community_id'];
	$get_rel_type_sub = "community";
	//$get_fan_Info = $get_fan_club->get_price_info($community_check['community_id'],"community");
	$find_member_sub = $subscription_become->find_member($community_check['community_id'],"community");
	$url_popup = $display->profile_url_pop("community",$community_check['community_id']);
}

if($artist_event_check!=0)
{
	//$sql_rel_art = $display->eventrel_art_comm('artist',$artist_event_check['artist_id']);
	//var_dump($sql_rel_art);
	//die;
	$get_user_Info = $display->eventrel_art_comm('artist',$artist_event_check['artist_id']);
	$get_rel_id_sub = $artist_event_check['artist_id'].'_'.$artist_event_check['id'];
	$get_rel_type_sub = "artist_event";
	//$get_fan_Info = $get_fan_club->get_price_info($artist_check['artist_id'],"artist");
	$find_member_sub = $subscription_become->find_member($artist_event_check['artist_id'],"artist");
	$url_popup = $display->profileURLpop_eve_pro("artist_event",$artist_event_check['id']);
}

if($community_event_check!=0)
{
	//$sql_rel_art = $display->eventrel_art_comm('artist',$community_event_check['artist_id']);
	//var_dump($sql_rel_art);
	//die;
	$get_user_Info = $display->eventrel_art_comm('community',$community_event_check['community_id']);
	$get_rel_id_sub = $community_event_check['community_id'].'_'.$community_event_check['id'];
	$get_rel_type_sub = "community_event";
	//$get_fan_Info = $get_fan_club->get_price_info($artist_check['artist_id'],"artist");
	$find_member_sub = $subscription_become->find_member($community_event_check['community_id'],"community");
	$url_popup = $display->profileURLpop_eve_pro("community_event",$community_event_check['id']);
}

if($community_project_check!=0)
{
	//$sql_rel_art = $display->eventrel_art_comm('artist',$community_event_check['artist_id']);
	//var_dump($sql_rel_art);
	//die;
	$get_user_Info = $display->eventrel_art_comm('community',$community_project_check['community_id']);
	$get_rel_id_sub = $community_project_check['community_id'].'_'.$community_project_check['id'];
	$get_rel_type_sub = "community_project";
	//$get_fan_Info = $get_fan_club->get_price_info($artist_check['artist_id'],"artist");
	$find_member_sub = $subscription_become->find_member($community_project_check['community_id'],"community");
	$url_popup = $display->profileURLpop_eve_pro("community_project",$community_project_check['id']);
}

if($artist_project_check!=0)
{
	//$sql_rel_art = $display->eventrel_art_comm('artist',$community_event_check['artist_id']);
	//var_dump($sql_rel_art);
	//die;
	$get_user_Info = $display->eventrel_art_comm('artist',$artist_project_check['artist_id']);
	$get_rel_id_sub = $artist_project_check['artist_id'].'_'.$artist_project_check['id'];
	$get_rel_type_sub = "artist_project";
	//$get_fan_Info = $get_fan_club->get_price_info($artist_check['artist_id'],"artist");
	$find_member_sub = $subscription_become->find_member($artist_project_check['artist_id'],"artist");
	$url_popup = $display->profileURLpop_eve_pro("artist_project",$artist_project_check['id']);
}
//echo "%s &copy %s";
//var_dump($_POST);
//var_dump($find_member_sub);
//die;
if($_POST == null)
{
	//echo "hi";
	//if($find_member_sub['count(*)']>0)
	//{
?>
	<div class="fancy_header">
		Follow
	</div>
	<div class="follow_whole_content" id="box_whole_content">
<?php
		if($artist_event_check!=0 || $community_event_check!=0)
		{
		?>
			<p style="font-size:13px;">You may subscribe to updates from <?php if($artist_event_check!=0){ echo $artist_event_check['title']; $get_user_info_pro=$artist_event_check['title'];}elseif($community_event_check!=0) { echo $community_event_check['title']; $get_user_info_pro=$community_event_check['title']; } ?> using the following options: <?php //echo "Purify Cost"; ?></p>
		<?php
		}
		if($community_check!=0 || $artist_check!=0)
		{
		?>
			<p style="font-size:13px;">You may subscribe to updates from <?php echo $get_user_Info['name']; ?> using the following options: <?php //echo "Purify Cost"; ?></p>
		<?php
		}
		if($artist_project_check!=0 || $community_project_check!=0)
		{
		?>
			<p style="font-size:13px;">You may subscribe to updates from <?php if($artist_project_check!=0){ echo $artist_project_check['title']; $get_user_info_pro = $artist_project_check['title']; }elseif($community_project_check!=0) { echo $community_project_check['title']; $get_user_info_pro = $community_project_check['title']; } ?> using the following options: <?php //echo "Purify Cost"; ?></p>
		<?php
		}
		?>
		<div style="float: left;">
			<div id="actualContent" style="width:400px">
				<form action="" id="email_subscription_form" method="POST" onSubmit="return validate_fields_sub()">
					<div class="fieldCont" style="display:none;">
						<div class="fieldTitle">Email</div>
						<input type="hidden" name="sub_email" id="sub_email" class="fieldText" />
					</div>
					<div class="hintR" id="member_email_new" style="display:none;"></div><a style="display:none; font-size:smaller; text-decoration:underline;" id="links" href="#" onClick="link_register()">To Register Click Here.</a>
					<div class="hintR" style="display:none;" id="member_email_same">You Cannot Subscribe to your own profile.</div>
					<div class="fieldCont" style="display:none;">
						<div class="fieldTitle">Name</div>
						<input type="hidden" name="sub_name" id="sub_name" class="fieldText" />
						<input type="hidden" name="profile_title" id="profile_title" class="fieldText" value="<?php echo $get_user_info_pro;?>"/>
					</div>
						<input type="hidden" name="rel_id_sub" id="rel_id_sub" class="fieldText" value="<?php echo $get_rel_id_sub;?>" />
						<input type="hidden" name="rel_type_sub" id="rel_type_sub" class="fieldText" value="<?php echo $get_rel_type_sub;?>" />
						<input type="hidden" name="subscribed_to_name" id="subscribed_to_name" class="fieldText" value="<?php if($artist_event_check!=0){ echo $artist_event_check['title']; }elseif($community_event_check!=0) { echo $community_event_check['title']; }elseif($artist_project_check!=0){ echo $artist_project_check['title']; }elseif($community_project_check!=0) { echo $community_project_check['title']; }elseif($community_check!=0 || $artist_check!=0){ echo $get_user_Info['name']; }  ?>" />
					<div class="affiBlock" style="width:380px;">
                        <div class="RowCont" style="width:400px;">
                            <div class="sideL">News Feed :</div>
                            <div class="sideR" style="float:left;width:150px;">
                            	<div class="chkCont">
                                	<input checked type="radio" name="feed_chck" class="chk" id="yes_feed" value="yes"/>
                                    <div class="radioTitle">Yes</div>
                                </div>
                                <div class="chkCont">
                                    <input type="radio" name="feed_chck" class="chk" id="no_feed" value="no"/>
                                    <div class="radioTitle">No</div>
                                </div>
                            </div>
                        </div>
                        <div class="RowCont" style="width:400px;">
                            <div class="sideL">Email Update :</div>
                            <div class="sideR" style="float:left;width:150px;">
                            	<div class="chkCont">
                                	<input checked type="radio" name="email_chck" class="chk" id="yes" value="yes"/>
                                    <div class="radioTitle">Yes</div>
                                </div>
                                <div class="chkCont">
                                    <input type="radio" name="email_chck" class="chk" id="no" value="no"/>
                                    <div class="radioTitle">No</div>
                                </div>
                            </div>
                        </div>
                        <div class="RowCont" style="display:none;">
                            <div class="sideL">RSS :</div>
                            <div class="sideR"><?php echo 'www.'.$domainname.'/'.$url_popup['profile_url']; ?></div>
                        </div>
                    </div>
					<div class="fieldCont" style="width:300px;margin-bottom:0px;">
						<div class="fieldTitle" style="width:0px;"></div>
						<input type="submit" value="Follow" id="sub_button" style="color: rgb(255, 255, 255); font-family: 'futura-pt',sans-serif; font-size: 12px; border-radius:5px; font-weight: bold; padding: 6px 10px; float: left; border: 0px none; background-color: rgb(0, 0, 0);"/>
					</div>
				</form>
			</div>	
		</div>
		
	</div>
<script type="text/javascript">
$("#email_subscription_form").bind("submit",function(){
	$.ajax({
		type : "POST",
		url  : "email_subscription.php?pre_url=<?php if(isset($url_sep[3])){ echo $url_sep[3]; }?>",
		data  : $(this).serializeArray(),
		success:function(data){
			$("#box_whole_content").html(data);
		}
	});
	return false;
});
</script>
		<?php
	//}
	//else
	//{
	//	echo "<p>This Facility Is Not Present For This User.</p>";
	//}
	
}
?>

<?php
	$email_db_chk = 0;
	$news_feed_db_chk = 0;
	if(isset($_POST) && $_POST != null)
	{
		if(isset($_POST['email_chck']) && $_POST['email_chck']!="") //////////////1:-YES and 2:- NO
		{
			if($_POST['email_chck']=='yes')
			{
				$email_db_chk = 1;
			}
			if($_POST['email_chck']=='no')
			{
				$email_db_chk = 2;
			}
		}

		if(isset($_POST['feed_chck']) && $_POST['feed_chck']!="") //////////////1:-YES and 2:- NO
		{
			if($_POST['feed_chck']=='yes')
			{
				$news_feed_db_chk = 1;
			}
			if($_POST['feed_chck']=='no')
			{
				$news_feed_db_chk = 2;
			}
		}
		
		if(isset($_POST['sub_email']) && $_POST['sub_email']!="")
		{
			$sql  = mysql_query("SELECT * FROM general_user WHERE email='".$_POST['sub_email']."'");
			$ans = mysql_fetch_assoc($sql);
			
			$_POST['sub_name'] = $ans['fname'].' '.$ans['lname'];
		}
	
		$purchase_date_sub = date('Y-m-d');
		$expiry_date_sub = strtotime(date("Y-m-d", strtotime($purchase_date_sub)) . " +1 year");
		$expiry_date_sub = date('Y-m-d', $expiry_date_sub);
	
		if($_POST['rel_type_sub']!='artist' && $_POST['rel_type_sub']!='community')
		{
			$rel_id_sub_ep = explode('_',$_POST['rel_id_sub']);
			$rel_type_sub_ep = explode('_',$_POST['rel_type_sub']);
			$subscribe_to = $subscription_become->get_subscribe_to_info($rel_type_sub_ep['0'],$rel_id_sub_ep['0']);
			$sql_check_same = $subscription_become->same_user_check($_POST['sub_email'],$rel_id_sub_ep['0'],$rel_type_sub_ep['0']);///////////// 0:-Same user and 1:-diff user
		}
		else
		{
			$sql_check_same = $subscription_become->same_user_check($_POST['sub_email'],$_POST['rel_id_sub'],$_POST['rel_type_sub']);///////////// 0:-Same user and 1:-diff user
			$subscribe_to = $subscription_become->get_subscribe_to_info($_POST['rel_type_sub'],$_POST['rel_id_sub']);
		}
		
		if($sql_check_same==0)
		{
			echo "You cannot subscribe to your own profile.";
		}
		else
		{
			$subcribe_to = $subscription_become->become_subscriber($_POST['sub_name'],$_POST['sub_email'],$_POST['rel_id_sub'],$_POST['rel_type_sub'],$purchase_date_sub,$expiry_date_sub,$news_feed_db_chk,$email_db_chk,$subscribe_to['general_user_id']);
			//echo $subcribe_to;
			//die;
			
			$latest_id = mysql_insert_id();
			//echo $latest_id;
			if($_POST['rel_type_sub']=='artist_event' || $_POST['rel_type_sub']=='artist_project')
			{
				$category_contact = 'artist';
				$get_general_ids = $subscription_become->get_general_user_id($_POST['rel_id_sub'],$category_contact);
				$sql_chk_address_contact = $subscription_become->chk_addressbook_contact($get_general_ids['general_user_id'],$category_contact,$_POST['sub_email']);
			}
			elseif($_POST['rel_type_sub']=='community_event' || $_POST['rel_type_sub']=='community_project')
			{
				$category_contact = 'community';
				$get_general_ids = $subscription_become->get_general_user_id($_POST['rel_id_sub'],$category_contact);
				$sql_chk_address_contact = $subscription_become->chk_addressbook_contact($get_general_ids['general_user_id'],$category_contact,$_POST['sub_email']);
			}
			else
			{
				$get_general_ids = $subscription_become->get_general_user_id($_POST['rel_id_sub'],$_POST['rel_type_sub']);
				$sql_chk_address_contact = $subscription_become->chk_addressbook_contact($get_general_ids['general_user_id'],$_POST['rel_type_sub'],$_POST['sub_email']);
			}
			
			if($sql_chk_address_contact==1 && $email_db_chk==1)
			{
				if($_POST['rel_type_sub']=='artist_event' || $_POST['rel_type_sub']=='artist_project')
				{
					$category_contact = 'artist';
				}
				if($_POST['rel_type_sub']=='community_event' || $_POST['rel_type_sub']=='community_project')
				{
					$category_contact = 'community';
				}
				if($_POST['rel_type_sub']=='community' || $_POST['rel_type_sub']=='artist')
				{
					$category_contact = $_POST['rel_type_sub'];
				}
				$sql_addressbook_sup = $subscription_become->insert_addressbook($get_general_ids['general_user_id'],$category_contact,$_POST['sub_name'],$_POST['sub_email']);
			}
			
			echo "You have subscribed to ".$_POST['subscribed_to_name'].". The free media included with this subscription has been added to your <a href=javascript:void(0) onclick=link_after_subscription('profileedit_media.php')>media library</a>. You can manage your subscriptions <a href=javascript:void(0) onclick=link_after_subscription('profileedit.php#subscrriptions')>here</a>.";
			
			
			$chk_registered = $subscription_become->chk_registered($_POST['sub_email']);
			if($chk_registered==1)			//////// 0:-Not Registered and 1:-Registered User
			{	
				if($_POST['rel_type_sub']!='artist' && $_POST['rel_type_sub']!='community')
				{
					$rel_id_sub_ep = explode('_',$_POST['rel_id_sub']);
					$rel_type_sub_ep = explode('_',$_POST['rel_type_sub']);
					
					//$sql_type_mail = $subscription_become->get_general_user_id($rel_id_sub_ep['0'],$rel_type_sub_ep['0']);
					$sql_type_mail = $subscription_become->get_profile_detail($rel_id_sub_ep['0'],$rel_type_sub_ep['0']);
				}
				else
				{
					//$sql_type_mail = $subscription_become->get_general_user_id($_POST['rel_id_sub'],$_POST['rel_type_sub']);
					$sql_type_mail = $subscription_become->get_profile_detail($_POST['rel_id_sub'],$_POST['rel_type_sub']);
				}
				
				/////////////////////////////////MAIL BODY///////////////////////////////////////////
				if($_POST['rel_type_sub']=='artist' || $_POST['rel_type_sub']=='community')
				{
					$message = $_POST['sub_name'].","."\n \n"."\t\tyou have subscribed to ".$sql_type_mail['name']."'.\n\n\t\tYou can manage your subscriptions <a href='profileedit.php#subscrriptions'>here</a>.Profile title will link to that profile display page.";
				}
				if($_POST['rel_type_sub']=='community_event' || $_POST['rel_type_sub']=='artist_event')
				{
					$message = $_POST['sub_name'].","."\n \n"."\t\tyou have subscribed to ".$sql_type_mail['name']."'.\n\n\t\tYou can manage your subscriptions <a href='profileedit.php#subscrriptions'>here</a>.Profile title will link to that profile display page.";
				}
				if($_POST['rel_type_sub']=='community_project' || $_POST['rel_type_sub']=='artist_project')
				{
					$message = $_POST['sub_name'].","."\n \n"."\t\tyou have subscribed to ".$sql_type_mail['name']."'.\n\n\t\tYou can manage your subscriptions <a href='profileedit.php#subscrriptions'>here</a>.Profile title will link to that profile display page.";
				}
			}
			else
			{
				if($_POST['rel_type_sub']!='artist' && $_POST['rel_type_sub']!='community')
				{
					$rel_id_sub_ep = explode('_',$_POST['rel_id_sub']);
					$rel_type_sub_ep = explode('_',$_POST['rel_type_sub']);
					
					$sql_type_mail = $subscription_become->get_general_user_id($rel_id_sub_ep['0'],$rel_type_sub_ep['0']);
				}
				else
				{
					$sql_type_mail = $subscription_become->get_general_user_id($_POST['rel_id_sub'],$_POST['rel_type_sub']);
				}
				
				/////////////////////////////////MAIL BODY///////////////////////////////////////////
				/*if($_POST['rel_type_sub']=='artist' || $_POST['rel_type_sub']=='community')
				{
					$message = $_POST['sub_name'].","."\n \n"."\t\tyou have subscribed to ".$sql_type_mail['name']."'.\n\n\t\tYou can manage your subscriptions <a href='profileedit.php#subscrriptions'>here</a>.Profile title will link to that profile display page.";
				}
				if($_POST['rel_type_sub']=='artist_event' || $_POST['rel_type_sub']=='community_event')
				{
					$message = $_POST['sub_name'].","."\n \n"."\t\tyou have subscribed to ".$sql_type_mail['name'].".\n\n\t\tYou can manage your subscriptions <a href='profileedit.php#subscrriptions'>here</a>.Profile title will link to that profile display page.";
				}
				if($_POST['rel_type_sub']=='artist_project' || $_POST['rel_type_sub']=='community_project')
				{
					$message = $_POST['sub_name'].","."\n \n"."\t\tyou have subscribed to ".$sql_type_mail['name']."'.\n\n\t\tYou can manage your subscriptions <a href='profileedit.php#subscrriptions'>here</a>.Profile title will link to that profile display page.";
				}*/
			}
			
			//$from = "Purify Art < no-reply@purifyart.com>";
			//$headers = "From:" . $from;
			
			//mail($to,$subject,$message,$headers);

			$headers .= "";
			$headers .= "MIME-Version: 1.0\r\n";
			$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
			//$headers .= "From: Purify Art < no-reply@purifyart.com>";
			
			
			if(isset($sql_type_mail['artist_id']) && $sql_type_mail['artist_id']!=0)
			{
				$sql = mysql_query("SELECT * FROM general_user WHERE artist_id='".$sql_type_mail['artist_id']."'");
			}
			elseif(isset($sql_type_mail['community_id']) && $sql_type_mail['community_id']!=0)
			{
				$sql = mysql_query("SELECT * FROM general_user WHERE community_id='".$sql_type_mail['community_id']."'");
			}
			$ans = mysql_fetch_assoc($sql);
			
			$sub_to_user = $ans['email'];
			$sub_subject = "Purify Art | New Subscriber";
			
			if($_POST['rel_type_sub']=='artist' || $_POST['rel_type_sub']=='community')
			{
				$sub_message = "Your profile $get_user_Info[name] has received a new subscriber. $_POST[sub_name] has subscribed to your:";
				if($email_db_chk ==1 && $news_feed_db_chk == 0){
					$sub_message .="
					<html><body><br>Email List<br></body></html>";
				}else if($email_db_chk ==0 && $news_feed_db_chk == 1){
					$sub_message .="
					<html><body><br>News Feed<br></body></html>";
				}else if($email_db_chk ==1 && $news_feed_db_chk == 1){
					$sub_message .="
					<html><body>
					<br>
					Email List </body></html>
					<html><body>
					News Feed
					<br>
					</body></html>";
				}
				$sub_message .="
				<html><body><br>You may send out updates to your subscribers by adding a post to your <a href='profileedit.php#profile'>News Feed</a>, sending updates on your <a href='profileedit_artist.php#promotions'>Promotions page</a>, or sending out email newsletters from your <a href='profileedit.php#mail'>Mail page</a>.</body></html>";
				//$sub_message = "Hello ".$sql_type_mail['fname'].","."\n\n\t\tUser ".$_POST['sub_name']." Subscribed to your's ".ucfirst($_POST['rel_type_sub'])." profile successfully.";
			}
			else
			{
				$sub_message = "<html><body>Your profile $_POST[profile_title] has received a new subscriber. $_POST[sub_name] has subscribed to your:";
				//echo $sub_message;
				if($email_db_chk ==1 && $news_feed_db_chk == 0){
					$sub_message .="
					Email List <br>";
				}else if($email_db_chk ==0 && $news_feed_db_chk == 1){
					$sub_message .="
					<br>News Feed <br>";
				}else if($email_db_chk ==1 && $news_feed_db_chk == 1){
					$sub_message .="
					Email List
					News Feed<br>";
				}
				$sub_message .="You may send out updates to your subscribers by adding a post to your <a href='profileedit.php#profile'>News Feed</a>, sending updates on your <a href='profileedit_artist.php#promotions'>Promotions page</a>, or sending out email newsletters from your <a href='profileedit.php#mail'>Mail page</a>.</body></html>";
				//$sub_message = "Hello ".$sql_type_mail['fname'].","."\n\n\t\tUser '".$_POST['sub_name']."' Subscribed to your's ".ucfirst($rel_type_sub_ep['1']).' of '.ucfirst($rel_type_sub_ep['0'])." profile successfully.";
			}
			$headers .= "from: Purify Art < no-reply@purifyart.com>"."\r\n";
			//mail($sub_to_user,$sub_subject,$sub_message,$headers);
			
			$url = "admin/newsletter/"; 
			$myFile = $url . "other_templates.txt";
			$fh = fopen($myFile, 'r');
			$theData = fread($fh, filesize($myFile));
			fclose($fh);
			
			$data_email = explode('|~|', $theData);
			
			$ns_user_n = str_replace('{::message::}', $sub_message, $data_email['1']);
			
			$body_email = $data_email['0'] . $ns_user_n . $data_email['2'];
			
			$mail_grid = new SendGrid\Mail();
			
			$mail_grid->addTo($sub_to_user)->
					setFromName('Purify Art')->
					setFrom('no-reply@purifyart.com')->
					setSubject($sub_subject)->
					setHtml($body_email);
					
			//$sendgrid->web->send($mail_grid);
			$sendgrid -> smtp -> send($mail_grid); 
			
			/* $mail->AddAddress($sub_to_user);
			$mail->Subject = $sub_subject;
			//mail($sendto_donation,$down_subject,$messagedonation,$headers);
			$mail->Body    = $sub_message;
			if($mail->Send()){
				$sentmail = true;
			} */
		}
	}
?>
