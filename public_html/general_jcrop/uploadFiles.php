<?php
//include_once('commons/db.php');
session_start();
error_reporting(0);
if (!empty($_FILES)) {
	
		$get_id=$_GET['id'];
		$tempFile = $_FILES['Filedata']['tmp_name'];
		//$ext = substr($_FILES['Filedata']['name'], strpos($_FILES['Filedata']['name'],'.'), strlen($_FILES['Filedata']['name'])-1);     
		$get_image_ext = $_FILES['Filedata']['name'];
		$ext = pathinfo($get_image_ext, PATHINFO_EXTENSION);
		
		$fileName = $get_id.'_'.time()."_image.".$ext;
		$folderName = $_REQUEST['folder'];
		
		$tempFile = $_FILES['Filedata']['tmp_name'];
		$targetPath = $_SERVER['DOCUMENT_ROOT'] . $folderName;
		$targetPathThumb = $_SERVER['DOCUMENT_ROOT'] . $folderName;

		$targetFile =  str_replace('//','/',$targetPath) . $fileName;
		
		move_uploaded_file($tempFile,$targetFile);

		createThumbnail($fileName, $targetPath, $targetPathThumb);
}

function createThumbnail($filename, $path_to_image_directory, $path_to_thumbs_directory) {

	$final_width_of_image = 600;
	$final_height_of_image = 450;
	
	
	//$path_to_image_directory = 'images/fullsized/';
	//$path_to_thumbs_directory = 'images/thumbs/';

	if(preg_match('/[.](jpg)$/', $filename)) {
		$im = imagecreatefromjpeg($path_to_image_directory . $filename);
	} else if (preg_match('/[.](gif)$/', $filename)) {
		$im = imagecreatefromgif($path_to_image_directory . $filename);
	} else if (preg_match('/[.](png)$/', $filename)) {
		$im = imagecreatefrompng($path_to_image_directory . $filename);
	} else if (preg_match('/[.](jpeg)$/', $filename)) {
		$im = imagecreatefrompng($path_to_image_directory . $filename);
	}
	
	if(preg_match('/[.](JPG)$/', $filename)) {
		$im = imagecreatefromjpeg($path_to_image_directory . $filename);
	} else if (preg_match('/[.](GIF)$/', $filename)) {
		$im = imagecreatefromgif($path_to_image_directory . $filename);
	} else if (preg_match('/[.](PNG)$/', $filename)) {
		$im = imagecreatefrompng($path_to_image_directory . $filename);
	} else if (preg_match('/[.](JPEG)$/', $filename)) {
		$im = imagecreatefrompng($path_to_image_directory . $filename);
	}

	$ox = imagesx($im);
	$oy = imagesy($im);
	
	//$ox=1024--width
	//$oy=681--height
	if($ox < 450 && $oy < 450){ echo "invalid";
	die;
	}
	
	if($ox<$oy)
	{
		$new_ox=450;
		//original height / original width x new width = new height
		//1200 / 1600 x 400 = 300
		$ny = ($oy / $ox) * $new_ox;
		$nx = $new_ox;
	}
	if($oy<$ox)
	{
		$new_oy=450;
		//original width / original height x new height = new width
		$nx = ($ox / $oy) * $new_oy;
		//1200 / 1600 x 400 = 300
		$ny = $new_oy;
	}
	//$nx = $final_width_of_image;
	
	
	
	
	
	//we have changed this becoz of image size this is calculating image size and we are giving it directly//
	
	//$ny = floor($oy * ($final_width_of_image / $ox));
	//if($nx<450 || $ny<450){ return "invalid_azhar_'".$oy."''".$ox."'";}
	
	//we have changed this becoz of image size this is calculating image size and we are giving it directly Ends here//

	$nm = imagecreatetruecolor($nx, $ny);

	imagecopyresized($nm, $im, 0,0,0,0,$nx,$ny,$ox,$oy);

	if(!file_exists($path_to_thumbs_directory)) {
	  if(!mkdir($path_to_thumbs_directory)) {
           die("There was a problem. Please try again!");
	  }
       }

	imagejpeg($nm, $path_to_thumbs_directory . $filename);
	
	$tempFile = $nm;
	$bucket_name = "generaljcrop";
    $root = realpath($_SERVER["DOCUMENT_ROOT"]);
    if (!class_exists('S3')) require_once ($root.'/S3.php');
	if (!defined('awsAccessKey')) define('awsAccessKey', '');
	if (!defined('awsSecretKey')) define('awsSecretKey', '');
	$s3 = new S3(awsAccessKey, awsSecretKey);
	$response=$s3->putBucket($bucket_name, S3::ACL_PUBLIC_READ);
    if(!$response)
    {
        error_log('Error creating thumbnail on s3 for '.$path_to_thumbs_directory.$filename);
    }
    
	$s3->putObject(S3::inputFile($path_to_thumbs_directory.$filename),$bucket_name,$filename,S3::ACL_PUBLIC_READ);
	unlink($path_to_thumbs_directory.$filename);
	echo "http://generaljcrop.s3.amazonaws.com/".$filename;
	//return $filename;
}


?>