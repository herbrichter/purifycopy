    <!--<script src="community_community_community_community_jcrop/js/jquery.min.js" type="text/javascript"></script>-->
    <script src="general_jcrop/js/jquery.Jcrop.js" type="text/javascript"></script>
    <link rel="stylesheet" href="general_jcrop/css/jquery.Jcrop.css" type="text/css" />
    <link rel="stylesheet" href="general_jcrop/demo_files/demos.css" type="text/css" />

	<!--<script src="general_jcrop/js/popup.js" type="text/javascript"></script>
    <link rel="stylesheet" href="general_jcrop/css/popup.css" type="text/css" />-->

	<script type="text/javascript" src="uploadify/swfobject.js"></script>
	<script type="text/javascript" src="uploadify/jquery.uploadify.v2.1.4.min.js"></script>
	<link href="uploadify/uploadify.css" rel="stylesheet"/>

	<?php
		if(isset($_SESSION['login_email']))
		{
			$sql = mysql_query("select * from  general_user where email='".$_SESSION['login_email']."'");
			$res=mysql_fetch_assoc($sql);
			//var_dump($res);

			//$sql1=mysql_query("select * from  general_community where community_id = '".$res['community_id']."'");
			//$res1=mysql_fetch_assoc($sql1);
		}
	?>
	<script type="text/javascript">
	var myUploader_crop = null;
	var iMaxUploadSize = 10485760; //10MB
	var IMG_UPLOAD_PATH = "http://generaljcrop.s3.amazonaws.com/";
	// Create variables (in this scope) to hold the API and image size
	var general_jcrop_api, boundx, boundy;
	var allowResizeCropper = false;
    jQuery(function($){
		$("#cropBtn").click(function(){
			if($('#x').val()!="" && $("#target").attr('src')!=""){
				$.ajax({
					type: "POST",
					url: 'general_jcrop/cropImage.php',
					data: { "action":'crop',"resize":allowResizeCropper,"x":$('#x').val(), "y":$('#y').val(), "w":$('#w').val(), "h":$('#h').val(), "srcImg":$("#target").attr('src')},
					success: function(data){
						//alert($("#target").attr('src'));
						//disablePopup();
						//window.location.href="profileedit.php#general";
						parent.jQuery.fancybox.close();
						window.location.reload();
						if(allowResizeCropper)
							$("#resultImgThumb").attr('src',data);
						else
							$("#resultImgProfile").attr('src',data);
							var diff=data.split("/");
							//alert(diff['4']);
							window.parent.document.getElementById("pro_pic").value = diff['4'];
							//alert(a);
							//a = diff['4'];
							//alert(a);
					}
				});
			}
		});
    });


	function generateUploader(){
		if(!myUploader_crop){
			myUploader_crop = {
				uploadify : function(){
					$('#file_upload_crop').uploadify({
						'uploader'  : 'uploadify/uploadify.swf',
						'script'    : 'general_jcrop/uploadFiles.php?id=<?php echo $res['general_user_id']; ?>',
						'cancelImg' : 'uploadify/cancel.png',
						'folder'    : 'general_jcrop/croppingFiles/',
						'auto'      : true,
						'multi'		: true,
						'removeCompleted' : true,
						'wmode'		: 'transparent',
						'buttonText': 'Upload Image',
						'fileExt'     : '*.jpg;*.gif;*.png',
						'fileDesc'    : 'Image Files',
						'simUploadLimit' : 1,
						'sizeLimit'	: iMaxUploadSize, //10 MB size
						'onComplete': function(event, ID, fileObj, response, data) {
							// On File Upload Completion			
							//location = 'uploadtos3.php?uploads=complete';
							//$("#uploadedImages").val($("#uploadedImages").val() + response + "|");
							if(response=="invalid"){
								$("#errMsg").html("Invalid image size.");
							}else{
								$("#errMsg").html('');
								$("#cropperSpace").show();
								//alert(IMG_UPLOAD_PATH);
								//alert(response);
								generateCropperObj(response);
							}
							setTimeout(function(){
								$.fancybox.update();
							},1000);
						},
						'onSelect' : function (event, ID, fileObj){
						},
						'onSelectOnce' : function(event, data)
						{
							/*iTotFileSize = data.allBytesTotal;
							if(iTotFileSize >= iMaxUploadSize){
								var iOverSize = (iTotFileSize - iMaxUploadSize) / 1024 / 1024; // Size, in MB
								$("#divGalleryFileSize").show().html("Maximum 10MB allowed. Total queue size approximately: <font color='red'>" + Math.ceil(iTotFileSize/1024/1024) + " (MB)</font>");
								$('#file_upload').uploadifyClearQueue();
							}
							else
							{
							   $("#divGalleryFileSize").hide() 
							}*/
						},
						'onOpen'	: function() {
							//hide overly
						}
						/*,
						'onError'     : function (event,ID,fileObj,errorObj) {
						  alert(errorObj.type + ' Error: ' + errorObj.info);
						},
						'onProgress'  : function(event,ID,fileObj,data) {
						  var bytes = Math.round(data.bytesLoaded / 1024);
						  $('#containerResult').html(' - ' + bytes + 'KB Uploaded');
						  return false;
						}*/

						});
					}
				};
				myUploader_crop.uploadify();
		}
	}

function generateCropperObj(img){
//alert(img);
      if(general_jcrop_api){ 
		general_jcrop_api.destroy(); 
	  }
		$("#targetTD").html('<img id="target">');
		$("#target").attr('src',img);
		$("#preview").attr('src',img);

      $('#target').Jcrop({
        onChange: updatePreview,
        onSelect: updateCoords,
		allowResize: allowResizeCropper,
		allowSelect: allowResizeCropper,
        aspectRatio: 1
      },function(){
        // Use the API to get the real image size
        var bounds = this.getBounds();
        boundx = bounds[0];
        boundy = bounds[1];
        general_jcrop_api = this;
		if(!allowResizeCropper){
			general_jcrop_api.animateTo([0,0,450,450]);
		}
        // Store the API in the general_jcrop_api variable
      });

	function updateCoords(c)
	{
		$('#x').val(c.x);
		$('#y').val(c.y);
		$('#w').val(c.w);
		$('#h').val(c.h);
		updatePreview(c);
	};

      function updatePreview(c)
      {
        if (parseInt(c.w) > 0)
        {
          var rx = 100 / c.w;
          var ry = 100 / c.h;

          $('#preview').css({
            width: Math.round(rx * boundx) + 'px',
            height: Math.round(ry * boundy) + 'px',
            marginLeft: '-' + Math.round(rx * c.x) + 'px',
            marginTop: '-' + Math.round(ry * c.y) + 'px'
          });
        }
      };
}

function showCropper(resizeFlag){
	allowResizeCropper = resizeFlag;
	/*this line is commented as per our requirement*/
		
		//$("#cropperSpace").hide();
		
	/*this line is commented as per our requirement*/
	//centerPopup();	loadPopup(); 
	generateUploader(); //generateCropperObj();
}


function create(newresizeFlag)
{
	showCropper(newresizeFlag);
	var IMG_UPLOAD_PATH = "http://generaljcrop.s3.amazonaws.com/";
	//alert("<?php echo $newres['image_name']; ?>");
	var str ="<?php echo $newres['image_name']; ?>";
	var image_name = str.substr(10);
	//alert(image_name);
	var response = image_name;
	//alert(response)
	if(response=="" || response==null)
	{
		return;
	}
	else
	{
		$("#cropperSpace").show();
		allowResizeCropper="true";
		generateCropperObj(IMG_UPLOAD_PATH+response);
	}
}

/*Script For Detecting Flash*/
$("document").ready(function(){
	var flashEnabled = !!(navigator.mimeTypes["application/x-shockwave-flash"] || window.ActiveXObject && new ActiveXObject('ShockwaveFlash.ShockwaveFlash'));
	if (!flashEnabled) { 
		$("#Flashnotfound_img_gen").css({"display":"block","color":"red","float":"left","width":"300px","position":"relative","left":"40px"});
	}
});
/*Script For Detecting Flash Ends Here*/
  </script>


<!--<a href="javascript:showCropper(true);" class="hint" style="text-decoration:none;" onClick="create()"><input type="button" value="Change" /></a>-->
<a href="#popupContact_general_jcrop" class="fancybox_general_jcrop" style="text-decoration:none;" onClick="create(true)"><input class="blackhint" type="button" value="Change" style="border-radius:5px; -webkit-border-radius:5px; -moz-border-radius:5px; -ms-border-radius:5px; -o-border-radius:5px;" /></a>
<script type="text/javascript">
	$(".fancybox_general_jcrop").fancybox({
		helpers : {
			media : {},
			buttons : {},
			title : null
		},
		afterShow: function(){
			var resize = setTimeout(function(){
			$.fancybox.update();
			},1000);
		}
	});
</script>

<!--<a href="javascript:showCropper(false);" onClick="create()">Create Profile Image</a><br/><br/>-->
<!--<div style="margin:50px;">
	<img id="resultImgThumb" style="vertical-align:top;"/>
	<img id="resultImgProfile"/>
</div>-->
<!-- POPUP BOX START -->
<div id="popupContact_general_jcrop" style="display:none;">
	<div class="fancy_header" id="artist_project_header">
		General Profile Picture
	</div>
	<div class="pop_whole_image_content" >
		<div class="popup_image_preview_box">
			Preview
			<div class="popup_image_preview">
				<img src="" id="preview" alt="Preview" class="general_jcrop-preview" />
			</div>
		</div>			
		<div id="Flashnotfound_img_gen" style="display:none;">
			Either your flash player plugin is disabled or if you don't have the flash player <a href="http://get.adobe.com/flashplayer/" target="_blank">Click Here</a> to install.
		</div>
		<div class="pop_image_function_button">
			<form action="" methos="post" enctype="multipart/form-data" class="pop_image_form">
				<div class="pop_image_upload_button"><input name="theFile" type="file" id="file_upload_crop" /></div>
				<!--<div style="width:400px;float:left;color:grey;line-height:30px; position:relative; left:20px;"> Image size should be more than 600x600.</div>-->
				<div id="hinttext" class="pop_image_upload_hinttext">Image size should be more than 450x450.</div>
			</form>
			<div class="pop_image_crop_button">
				<input type="button" value="Crop Image" id="cropBtn" style=" background-color: #404040;border-radius: 5px;color: white;cursor: pointer;padding: 7px 27px;border:none;" />
				<input type="hidden" id="x" name="x" />
				<input type="hidden" id="y" name="y" />
				<input type="hidden" id="w" name="w" />
				<input type="hidden" id="h" name="h" />
			</div>
		</div>
		<div style="clear:both"></div>
        <div id="errMsg" style="color:red;text-align:center;"></div>
		<table id="cropperSpace" style="display:none;" class="big_image">
			<tr>
				<td rowspan="2" id="targetTD">
					<img src="" style="max-width:400px" id="target"/>
				</td>
			</tr>
		</table>
	</div>
</div>
<div id="backgroundPopup"></div>
<!-- POPUP BOX END -->