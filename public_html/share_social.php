<?php
	session_start();
	include_once("commons/db.php");
    $domainname=$_SERVER['SERVER_NAME'];
?>
	<link rel="stylesheet" type="text/css" media="screen" href="includes/purify.css" />
	<!--<link rel="stylesheet" type="text/css" href="new_fancy_box/jquery.fancybox.css?v=2.1.4" media="screen" />-->
	<link href="sample.css" rel="stylesheet" type="text/css" />
	<style>
		#actualContent {padding: 20px;width: 430px;background-color: #FFFFFF;border-radius:6px;}
		#actualContent .fieldCont{width:auto;}
		#contents_frames{padding: 20px;background-color: #FFFFFF;border-radius:6px;}
		#at3winfooter { display: none !important; }
		#at3winssi { display: none !important; }
		.head_setters { font-size: 18px; font-weight: bold; }
	</style>
	<!--------New editor
	<link type="text/css" rel="stylesheet" href="newedit/jquery-te-1.4.0.css">
	<script type="text/javascript" src="newedit/jquery-te-1.4.0.min.js" charset="utf-8"></script>-->
	<script type="text/javascript">var addthis_config = {"data_track_addressbar":false, "addthis_config.ui_show_promo":false};</script>
	<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-52ac1fee583ddf3c"></script>
	<div>
		<div class="fancy_header">
			Share & Follow
		</div>
		<div id="actualContent">
			<!--<div id="loaders" style="text-align:center;">
				<img src="images/ajax_loader_large.gif" style="height: 35px;">
			</div>-->
			<span class="head_setters">SHARE :- </span>
			<div class="addthis_toolbox addthis_default_style addthis_32x32_style">
				<a class="addthis_button_email" addthis:title="Check out <?php echo $_GET['subjects']; ?> on Purify Art" ></a>
				<a class="addthis_button_facebook" addthis:url="http://<?php echo $domainname; ?>/<?php echo $_GET['match']; ?>"></a>
				<a class="addthis_button_twitter" addthis:url="http://<?php echo $domainname; ?>/<?php echo $_GET['match']; ?>"></a>
				<a class="addthis_button_google_plusone_share" addthis:url="http://<?php echo $domainname; ?>/<?php echo $_GET['match']; ?>"></a>
				<a class="addthis_button_pinterest_share" addthis:url="http://<?php echo $domainname; ?>/<?php echo $_GET['match']; ?>"></a>
				<a class="addthis_button_tumblr" addthis:url="http://<?php echo $domainname; ?>/<?php echo $_GET['match']; ?>"></a>
				<a class="addthis_button_stumbleupon" addthis:url="http://<?php echo $domainname; ?>/<?php echo $_GET['match']; ?>"></a>
			</div>
		
		<?php
			if($_GET['fb']!="" || $_GET['twit_share']!="" || $_GET['gplus_share']!="" || $_GET['vimeo_share']!="" || $_GET['pin_share']!="" || $_GET['ints_share']!="" || $_GET['tubm_share']!="" || $_GET['sdcl_share']!="" || $_GET['stbu_share']!="" || $_GET['you_share']!="")
			{
		?>
				<br/><br/>
				<span class="head_setters">FOLLOW :- </span>
				<div class="addthis_toolbox addthis_32x32_style addthis_default_style">
					<?php
						if(isset($_GET['fb']))
						{
							if($_GET['fb']!="")
							{
					?>
								<a class="addthis_button_facebook_follow" addthis:userid="<?php echo $_GET['fb']; ?>"></a>
					<?php
							}
						}
						
						if(isset($_GET['twit_share']))
						{
							if($_GET['twit_share']!="")
							{
					?>
								<a class="addthis_button_twitter_follow" addthis:userid="<?php echo $_GET['twit_share']; ?>"></a>
					<?php
							}
						}
						
						if(isset($_GET['gplus_share']))
						{
							if($_GET['gplus_share']!="")
							{
					?>
								<a class="addthis_button_google_follow" addthis:userid="<?php echo $_GET['gplus_share']; ?>"></a>
					<?php
							}
						}
						
						if(isset($_GET['vimeo_share']))
						{
							if($_GET['vimeo_share']!="")
							{
					?>
								<a class="addthis_button_vimeo_follow" addthis:userid="<?php echo $_GET['vimeo_share']; ?>"></a>
					<?php
							}
						}
						
						if(isset($_GET['pin_share']))
						{
							if($_GET['pin_share']!="")
							{
					?>
								<a class="addthis_button_pinterest_follow" addthis:userid="<?php echo $_GET['pin_share']; ?>"></a>
					<?php
							}
						}
						
						if(isset($_GET['ints_share']))
						{
							if($_GET['ints_share']!="")
							{
					?>
								<a class="addthis_button_instagram_follow" addthis:userid="<?php echo $_GET['ints_share']; ?>"></a>
					<?php
							}
						}
						
						if(isset($_GET['tubm_share']))
						{
							if($_GET['tubm_share']!="")
							{
					?>
								<a class="addthis_button_tumblr_follow" addthis:userid="<?php echo $_GET['tubm_share']; ?>"></a>
					<?php
							}
						}
						
						if(isset($_GET['sdcl_share']))
						{
							if($_GET['sdcl_share']!="")
							{
					?>
								<a href="http://soundcloud.com/<?php echo $_GET['sdcl_share']; ?>"><img border="0" src="images/logo-soundcloud.png" alt="follow <?php echo $_GET['sdcl_share']; ?> on soundcloud" width="32" height="32"></a>
					<?php
							}
						}
						
						if(isset($_GET['stbu_share']))
						{
							if($_GET['stbu_share']!="")
							{
					?>
								<a href="http://stumbleupon.com/stumbler/<?php echo $_GET['stbu_share']; ?>"><img border="0" src="images/stumble_upon.png" alt="follow <?php echo $_GET['stbu_share']; ?> on soundcloud" width="32" height="32"></a>
					<?php
							}
						}
						
						if(isset($_GET['you_share']))
						{
							if($_GET['you_share']!="")
							{
					?>
								<a class="addthis_button_youtube_follow" addthis:userid="<?php echo $_GET['you_share']; ?>"></a>
					<?php
							}
						}
					?>
				</div>
		<?php
			}
		?>
		</div>
	</div>
	
	<script>
		/* $(window).load(function() {
			$("#share_buttons").css({"display":"block"});
			$("#loaders").css({"display":"none"});
			startTab();
		}); */
	</script>