﻿<?php
// Error on the following line:
error_reporting(0);

	ob_start();
	include_once('commons/session_check.php');
	include_once('classes/Newsletter.php');		
	include_once('classes/UnregUser.php');
	
	session_start();
	$username = $_SESSION['username'];
	if(!$username == '')
	{
		$login_flag=1;
	}
	
	if($login_flag) include_once('loggedin_includes.php');
	else include_once('login_includes.php');
	
	$obj=new Newsletter();	
	$row=$obj->getCurrentNewsletter();
	if($row) 
	{
		$newsletter_path=$row['upload_path'];
		$flag_current=1;
	}

	if($uemail=$_POST['uemail'])
	{
		$uobj=new UnregUser();
		if($uobj->addUser($parent,$uemail))
		{
			$uflag=1;
		}
	}
?>

<?php include_once('includes/header.php'); ?>
<title>Purify Entertainment: About</title>
<script type="text/javascript" language="javascript">
	function unreg_submit()
	{
		var frm=document.getElementById('unreg_form');
		var uemail=$('#uemail');
		var email_val = /^([a-zA-Z0-9_.-])+@(([a-zA-Z0-9-])+.)+([a-zA-Z])+$/;
		
		if(!email_val.test(uemail.val()))
		{
			alert("Error: Email id not in correct format.");
			document.unreg_form.uemail.focus();
		}
		else
		{
			frm.submit();
		}
	}
	
	function clearme(theText)
	{
		if (theText.value == theText.defaultValue) {
         theText.value = ""
     	}		 
	}

</script>

  <div id="contentContainer">
	<?php include_once('includes/subnavigation_aboutpages.php'); ?>
    <div id="actualContent">
      <div style="float:left; width:400px;"><h1>Newsletter</h1></div>
      <div style="float:left; width:100%; margin-top: 10px; padding: 5px;">
          Current newsletter: 
        </div>
        <div style="float:left; width:650px; height:300px; margin: 0px; overflow:scroll; border: 1px solid #CCCCCC;" class="hint">
          <?php if($flag_current)
					include($newsletter_path);
				else
					echo "--None--";
		  ?>
        </div>

    </div>
    
    <div class="clearMe"></div>
    
  </div>
  
<?php include_once('includes/footer.php'); ?>