<?php
$result = array();
$errorflag='no';

$errormsg=array();
$inputparams=array();
$username='nouser';
$userpass='nopass';
$listartists='leerick,maleko';
$format='json';

$data = file_get_contents("php://input");
$dataary=json_decode($data,true);

if (isset($dataary['format'])){    
    $format = strtolower($dataary['format']);
}
if(isset($dataary['listartists'])) {
    $listartists=$dataary['listartists'];
}else {
    $errormessage='missing profile';
    $errormsg[]=array('errormsg'=>$errormessage);
    $result[] = array('error'=>$errormsg);
    $inputparams[] = array('listartists'=>$listartists);
    $inputparams[] = array('format'=>$format);
    $inputparams[] = array('post'=>$dataary);
    $result[] = array('inputparams'=>$inputparams);
    $errorflag='yes';
}

if ($errorflag='no'){
    include_once("commons/db.php");
    include_once('classes/Artist.php');
    include_once('classes/Media.php');
    include_once('classes/CountryState.php');
    include_once('classes/Type.php');
    include_once('classes/SubType.php');
    include_once('classes/MetaType.php');
    include_once("classes/ViewArtistProfileURL.php");    
    include_once('classes/ProfileDisplay.php');
  
    $new_profile_class_obj = new ViewArtistProfileURL();
    $date = date('Y-m-d H:i:s');
    $display = new ProfileDisplay();    
    $foundartists=",";
    $foundupcomingevent="";
    $foundrecordedevent="";
    
    $listartists_exp = explode(',',$listartists);
    //echo '<br> exp listartists_exp <br>';
    //print_r($listartists_exp);    
    $outputupcomingeventindex=0;
    $outputrecordedeventindex=0;
    if(isset($listartists_exp) && count($listartists_exp)>0 && !empty($listartists_exp))
    {
        $tagged_profileurls="";
        for($artistindex=0;$artistindex<count($listartists_exp);$artistindex++)
        {               
            //echo '<br>getting profileurl='.$listartists_exp[$artistindex].' <br>';     
            $pos=strpos($foundartists,','.$listartists_exp[$artistindex]);
            if ($pos===false)
            {                            
                $foundartists=$foundartists.','.$listartists_exp[$artistindex];                           
                if($listartists_exp[$artistindex]!="")
                { 
                    $artistprofileurl=$listartists_exp[$artistindex]; 
                    
                    If($artistindex==0)
                    {
                        $objArtist=new Artist();
                        $objMedia=new Media();
                        $display = new ProfileDisplay();
                        $artist_check = $display->artistCheck($artistprofileurl);
    
                        //$new_profile_class_obj = new ViewArtistProfileURL();
                        $getgeneral =  $new_profile_class_obj->get_user_info($artist_check['artist_id']);
	                    $result[] = array('generaluser'=>$getgeneral);
    
                        //$get_user_delete_or_not = $new_profile_class_obj->chk_user_delete($artist_check['artist_id']);
                        //$get_common_id=$artist_check['artist_id'];
    
    
                        ////var_dump($getgeneral);
                        $getuser = $new_profile_class_obj->get_user_artist_profile($artist_check['artist_id']);
                        $result[] = array('generalartist'=>$getuser);
                        $getcountry = $new_profile_class_obj->get_user_country($artist_check['artist_id']);
                        $result[] = array('country'=>$getcountry);
                        $getstate = $new_profile_class_obj->get_user_state($artist_check['artist_id']);
                        $result[] = array('state'=>$getstate);
                        $gettype = $new_profile_class_obj->get_artist_user_type($artist_check['artist_id']);
                        $result[] = array('type'=>$gettype);
                        ////$get_type_dis = $new_profile_class_obj->get_artist_user($artist_check['artist_id']);
                        ////$result[] = array('get_type_dis'=>$get_type_dis);
	
                        $get_subtype = $new_profile_class_obj->get_artist_user_subtype($artist_check['artist_id']);
                        $result[] = array('subtype'=>$get_subtype);
                        ////$get_subtype_dis = $new_profile_class_obj->get_artist_user_subs($artist_check['artist_id']);
                        ////$result[] = array('get_subtype_dis'=>$get_subtype_dis);
	
                        ////var_dump($get_subtype);
                        $get_metatype = $new_profile_class_obj->get_artist_user_metatype($artist_check['artist_id']);
                        $result[] = array('metatype'=>$get_metatype);
                        //$src = "http://artjcropprofile.s3.amazonaws.com/";
                        //$src_list = "http://artjcropthumb.s3.amazonaws.com/";
                        //$src1 = "../";
    
                        //echo '<br> about to get all projects <br>';
                        
                    
                    
                    
                    
                    
                    
                                        //echo '<br>getting user artist for artisturl='.$listartists_exp[$artistindex].' <br>'; 
                    }                    
                    
                    
                    //echo '<br>getting user artist for artisturl='.$listartists_exp[$artistindex].' <br>'; 
                    $userartist =$new_profile_class_obj->get_user_artist_profileurl($artistprofileurl);
                    //echo '<br> userartist <br>';
                    //print_r($userartist);                                       

                    $event_exp_up = explode(',',$userartist['taggedupcomingevents']);

                    if(isset($event_exp_up) && count($event_exp_up)>0)
                    {
                        for($i=0;$i<count($event_exp_up);$i++)
                        {
                            $pos=strpos($foundupcomingevent,','.$event_exp_up[$i]);
                            if ($pos===false)
                            {                            
                                $foundupcomingevent=$foundupcomingevent.','.$event_exp_up[$i];
                                if($event_exp_up[$i]==""){continue;}
                                else
                                {
                                    $act_id = explode('~',$event_exp_up[$i]);
				
                                    if($act_id[1]=='art')
                                    {
                                        $get_upcoming_event[$outputupcomingeventindex] = mysql_fetch_assoc($new_profile_class_obj->upcomingArtistEvents($act_id[0],$date));
                                    }
                                    elseif($act_id[1]=='com')
                                    {
                                        $get_upcoming_event[$outputupcomingeventindex] = mysql_fetch_assoc($new_profile_class_obj->upcomingComEvents($act_id[0],$date));
                                    }
                                }
                                $outputupcomingeventindex++;
                            }
                        }                        
                    }
 
    
                    $event_exp_rec = explode(',',$userartist['taggedrecordedevents']);
	                //print_r($event_exp_rec);
                    if(isset($event_exp_rec) && count($event_exp_rec)>0)
                    {
                        for($i=0;$i<count($event_exp_rec);$i++)
                        {
                            //error_log('foundrecordedevent='.$foundrecordedevent.' i='.$i.' event='.$event_exp_rec[$i]);
                            $pos=strpos($foundrecordedevent,','.$event_exp_rec[$i]);
                            if ($pos===false)
                            {                            
                                $foundrecordedevent=$foundrecordedevent.','.$event_exp_rec[$i];
                                
                                if($event_exp_rec[$i]==""){continue;}
                                else
                                {
                                    $act_id = explode('~',$event_exp_rec[$i]);
				
                                    if($act_id[1]=='art')
                                    {
                                        $get_recorded_event[$outputrecordedeventindex] = mysql_fetch_assoc($new_profile_class_obj->recordedArtistEvents($act_id[0],$date));
                                    }
                                    elseif($act_id[1]=='com')
                                    {
                                        $get_recorded_event[$outputrecordedeventindex] = mysql_fetch_assoc($new_profile_class_obj->recordedComEvents($act_id[0],$date));
                                    }
                                } 
                                $outputrecordedeventindex++;
                            }
                        }                        
                    }                
                    //print_r($get_all_user_artists_for_profileurl); 
                    //echo '<br>emails in project index ='.$j.' <br>';
                } // no artists - should not have this error
            } // skipping this profileurl already processed
        } // end of get events for profileurl
    }    

    //////print_r($songimagefile);
    //////die;
        
    //// Obtain a list of columns
    //foreach ($get_artistproject as $key => $row) {
    //    $from[$key]  = $row['creator'];
    //    $track[$key] = $row['title'];
    //}

    //// Sort the data with volume descending, edition ascending
    //// Add $data as the last parameter, to sort by the common key
    //array_multisort($from, SORT_ASC, $track, SORT_ASC, $get_artistproject);
     
    if($outputupcomingeventindex>0)
    {
        $result[] = array('upcomingevents'=>$get_upcoming_event);
    }
    else
    {
        $result[] = array('upcomingevents'=>array());
    }
    
    if($outputrecordedeventindex>0)
    {
        $result[] = array('recordedevents'=>$get_recorded_event);
    }
    else
    {
        $result[] = array('recordedevents'=>array());
    }     

    
    
//print ( '<pre>' );
//print_r($exp_tagged_albums_info);
//print_r($videoimagefile);
//print_r($profileurlforvideo);
//print ( '</pre>' );    
//die;

  
    // add alluserartistsforproject to result array
  
    

} //end of if error == no

/* output in necessary format */
if($format == 'json') {
    header('Content-type: application/json');
    echo json_encode($result);
    //echo json_encode(array('artistprofile'=>$result));
    //echo array_to_json($result);
}
else {
    if ($format == 'xml'){
        header('Content-type: text/xml');
        echo '<result>';
        foreach($result as $index => $resultentry) {
            if(is_array($resultentry)) {
                foreach($resultentry as $key => $value) {
                    echo '<',$key,'>';
                    if(is_array($value)) {
                        foreach($value as $tag => $val) {
                            echo '<',$tag,'>',htmlentities($val),'</',$tag,'>';
                        }
                    }
                    echo '</',$key,'>';
                }
            }
        }
        echo '</result>';
    }
}

function array_to_json( $array ){

    if( !is_array( $array ) ){
        return false;
    }

    $associative = count( array_diff( array_keys($array), array_keys( array_keys( $array )) ));
    if( $associative ){

        $construct = array();
        foreach( $array as $key => $value ){

            // We first copy each key/value pair into a staging array,
            // formatting each key and value properly as we go.

            // Format the key:
            if( is_numeric($key) ){
                $key = "key_$key";
            }
            $key = "\"".addslashes($key)."\"";

            // Format the value:
            if( is_array( $value )){
                $value = array_to_json( $value );
            } else if( !is_numeric( $value ) || is_string( $value ) ){
                $value = "\"".addslashes($value)."\"";
            }

            // Add to staging array:
            $construct[] = "$key: $value";
        }

        // Then we collapse the staging array into the JSON form:
        $result = "{ " . implode( ", ", $construct ) . " }";

    } else { // If the array is a vector (not associative):

        $construct = array();
        foreach( $array as $value ){

            // Format the value:
            if( is_array( $value )){
                $value = array_to_json( $value );
            } else if( !is_numeric( $value ) || is_string( $value ) ){
                $value = "'".addslashes($value)."'";
            }

            // Add to staging array:
            $construct[] = $value;
        }

        // Then we collapse the staging array into the JSON form:
        $result = "[ " . implode( ", ", $construct ) . " ]";
    }
	
    return $result;
}

?>