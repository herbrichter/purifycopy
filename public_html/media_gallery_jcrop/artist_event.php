    <!--<script src="community_community_community_community_jcrop/js/jquery.min.js" type="text/javascript"></script>-->
    <script src="artist_event_jcrop/js/jquery.Jcrop.js" type="text/javascript"></script>
    <link rel="stylesheet" href="artist_event_jcrop/css/jquery.Jcrop.css" type="text/css" />
    <link rel="stylesheet" href="artist_event_jcrop/demo_files/demos.css" type="text/css" />

	<script src="artist_event_jcrop/js/popup.js" type="text/javascript"></script>
    <link rel="stylesheet" href="artist_event_jcrop/css/popup.css" type="text/css" />

	<script type="text/javascript" src="uploadify/swfobject.js"></script>
	<script type="text/javascript" src="uploadify/jquery.uploadify.v2.1.4.min.js"></script>
	<link href="uploadify/uploadify.css" rel="stylesheet"/>

	<?php
		/*if(isset($_SESSION['login_email']))
		{
			$sql = mysql_query("select * from  general_user where email='".$_SESSION['login_email']."'");
			$res=mysql_fetch_assoc($sql);
			//var_dump($res);

			$sql1=mysql_query("select * from  general_community where community_id = '".$res['community_id']."'");
			$res1=mysql_fetch_assoc($sql1);
		}*/
		$random_number = rand(1,1000000);
	?>
	<script type="text/javascript">
	var myUploader = null;
	var iMaxUploadSize = 10485760; //10MB
	var IMG_UPLOAD_PATH = "artist_event_jcrop/croppingFiles/";
	// Create variables (in this scope) to hold the API and image size
	var artist_event_jcrop_api, boundx, boundy;
	var allowResizeCropper = false;
    jQuery(function($){
		$("#cropBtn").click(function(){
			if($('#x').val()!="" && $("#target").attr('src')!=""){
				$.ajax({
					type: "POST",
					url: 'artist_event_jcrop/cropImage.php',
					data: { "action":'crop',"resize":allowResizeCropper,"x":$('#x').val(), "y":$('#y').val(), "w":$('#w').val(), "h":$('#h').val(), "srcImg":$("#target").attr('src')},
					success: function(data){
						//alert($("#target").attr('src'));
						disablePopup();
						//window.location.href="profileedit_community.php";
						//window.location.reload();
						if(allowResizeCropper)
						{
							var apro_img= new Date();
							//alert(a.getTime());
							data7 = data + "?pet=" + apro_img.getTime();
							
							$("#resultImgThumb").attr('src',data7);
							$("#resultImgThumb").show();
							var pro_pic_thum=window.parent.document.getElementById("listing_image");
							pro_pic_thum.value=data;
							$("#Noimagediv_listing").hide();
							$("#stored_listing_image").hide();
						}
						else
						{
							var acpro_img= new Date();
							//alert(a.getTime());
							data8 = data + "?vet=" + acpro_img.getTime();
							
							$("#resultImgProfile").attr('src',data8);
							$("#resultImgProfile").show();
							var pro_pic=window.parent.document.getElementById("profile_image");
							pro_pic.value=data;
							//$("#Noimagediv").style.display="none";
							$("#Noimagediv").hide();
							$("#stored_profile_image").hide();
							var diff=data.split("/");
							//alert(diff['4']);
							//window.parent.document.getElementById("pro_pic").value = diff['4'];
							//alert(a);
							//a = diff['4'];
							//alert(a);
						}	
					}
				});
			}
		});
    });


	function generateUploader(){
		if(!myUploader){
			myUploader = {
				uploadify : function(){
					$('#file_upload').uploadify({
						'uploader'  : 'uploadify/uploadify.swf',
						'script'    : 'artist_event_jcrop/uploadFiles.php?id=<?php echo $random_number; ?>',
						'cancelImg' : 'uploadify/cancel.png',
						'folder'    : 'artist_event_jcrop/croppingFiles/',
						'auto'      : true,
						'multi'		: true,
						'removeCompleted' : true,
						'wmode'		: 'transparent',
						'buttonText': 'Upload Image',
						'fileExt'     : '*.jpg;*.gif;*.png',
						'fileDesc'    : 'Image Files',
						'simUploadLimit' : 1,
						'sizeLimit'	: iMaxUploadSize, //10 MB size
						'onComplete': function(event, ID, fileObj, response, data) {
							// On File Upload Completion			
							//location = 'uploadtos3.php?uploads=complete';
							//$("#uploadedImages").val($("#uploadedImages").val() + response + "|");
							if(response=="invalid"){
								$("#errMsg").html("Invalid image size.");
							}else{
								$("#errMsg").html('');
								$("#cropperSpace").show();
								//alert(IMG_UPLOAD_PATH);
								//alert(response);
								generateCropperObj(response);
							}
						},
						'onSelect' : function (event, ID, fileObj){
						},
						'onSelectOnce' : function(event, data)
						{
							/*iTotFileSize = data.allBytesTotal;
							if(iTotFileSize >= iMaxUploadSize){
								var iOverSize = (iTotFileSize - iMaxUploadSize) / 1024 / 1024; // Size, in MB
								$("#divGalleryFileSize").show().html("Maximum 10MB allowed. Total queue size approximately: <font color='red'>" + Math.ceil(iTotFileSize/1024/1024) + " (MB)</font>");
								$('#file_upload').uploadifyClearQueue();
							}
							else
							{
							   $("#divGalleryFileSize").hide() 
							}*/
						},
						'onOpen'	: function() {
							//hide overly
						}
						/*,
						'onError'     : function (event,ID,fileObj,errorObj) {
						  alert(errorObj.type + ' Error: ' + errorObj.info);
						},
						'onProgress'  : function(event,ID,fileObj,data) {
						  var bytes = Math.round(data.bytesLoaded / 1024);
						  $('#containerResult').html(' - ' + bytes + 'KB Uploaded');
						  return false;
						}*/

						});
					}
				};
				myUploader.uploadify();
		}
	}

function generateCropperObj(img){
//alert(img);
      if(artist_event_jcrop_api){ 
		artist_event_jcrop_api.destroy(); 
	  }
		$("#targetTD").html('<img id="target">');
		$("#target").attr('src',img);
		$("#preview").attr('src',img);
		//alert($("#preview").attr('src',img));

      $('#target').Jcrop({
        onChange: updatePreview,
        onSelect: updateCoords,
		allowResize: allowResizeCropper,
		allowSelect: allowResizeCropper,
        aspectRatio: 1
      },function(){
        // Use the API to get the real image size
        var bounds = this.getBounds();
        boundx = bounds[0];
        boundy = bounds[1];
        artist_event_jcrop_api = this;
		if(!allowResizeCropper){
			artist_event_jcrop_api.animateTo([0,0,450,450]);
			artist_event_jcrop_api.setOptions({ allowSelect: false });
			artist_event_jcrop_api.setOptions({ allowResize: true });
			artist_event_jcrop_api.setOptions(true? {
            minSize: [ 450, 450 ],
            maxSize: [ 0, 0 ]
          }: {
            minSize: [ 0, 0 ],
            maxSize: [ 0, 0 ]
          });
			$("#hinttext").text("Image size should be more than 450x450.");
		}
		else if(allowResizeCropper=="350"){
			artist_event_jcrop_api.animateTo([0,0,200,175]);
			artist_event_jcrop_api.setOptions({ allowSelect: false });
			artist_event_jcrop_api.setOptions({ allowResize: true });
			artist_event_jcrop_api.setOptions(true? {
            minSize: [ 200, 175 ],
            maxSize: [ 0, 0 ]
          }: {
            minSize: [ 0, 0 ],
            maxSize: [ 0, 0 ]
          });
		  $("#hinttext").text("Image size should be more than 200x175.");
		}
        // Store the API in the artist_event_jcrop_api variable
      });

	function updateCoords(c)
	{
		$('#x').val(c.x);
		$('#y').val(c.y);
		$('#w').val(c.w);
		$('#h').val(c.h);
		updatePreview(c);
	};

      function updatePreview(c)
      {
        if (parseInt(c.w) > 0)
        {
          var rx = 100 / c.w;
          var ry = 100 / c.h;

          $('#preview').css({
            width: Math.round(rx * boundx) + 'px',
            height: Math.round(ry * boundy) + 'px',
            marginLeft: '-' + Math.round(rx * c.x) + 'px',
            marginTop: '-' + Math.round(ry * c.y) + 'px'
          });
        }
      };
}

function showCropper(resizeFlag){
	allowResizeCropper = resizeFlag;
	if(allowResizeCropper=="350" && myUploader=="null")
	{
		$("#cropperSpace").hide();
	}
	else
	{
		$("#cropperSpace").show();
	}
	centerPopup();	loadPopup(); generateUploader(); //generateCropperObj();
}

function get_profile_pic()
{
	/*var str ="<?php echo $getdetail['image_name']; ?>";
	if(str!="")
	{
		var as = str.split("/");
		var image_name = as[3].substr(10);
		var response = "http://arteventjcrop.s3.amazonaws.com/" + image_name;
	}
	else
	{
		response="";
	}
	if(response=="" || response==null)
	{*/
		var pro=document.getElementById("profile_image");
		if(pro.value!="")
		{
			response =pro.value;
			var img=response.split("/");
			var image_name = img[3].substr(10);
			var response = "http://arteventjcrop.s3.amazonaws.com/" + image_name;
			
			$("#cropperSpace").show();
			allowResizeCropper=false;
			
			generateCropperObj(response);
		}
		else
		{
			return;
		}
	/*}
	else
	{
		$("#cropperSpace").show();
		allowResizeCropper=false;
		generateCropperObj(response);
	}*/
}
function get_listing_pic()
{
	/*var str ="<?php echo $getdetail['listing_image_name']; ?>";
	if(str!="")
	{
		var as = str.split("/");
		var image_name = as[3].substr(10);
		var response = "http://arteventjcrop.s3.amazonaws.com/" + image_name;
	}
	else
	{
		response="";
	}
	if(response=="" || response==null)
	{*/
		var list_pro= document.getElementById("listing_image");
		if(list_pro.value!="")
		{
			response =list_pro.value;
			var img=response.split("/");
			var image_name = img[3].substr(10);
			var response = "http://arteventjcrop.s3.amazonaws.com/" + image_name;
			//alert(response);
			
			$("#cropperSpace").show();
			allowResizeCropper="350";
			generateCropperObj(response);
			myUploader= "Notnull";
			return;
		}
		else
		{
			var list_preview= document.getElementById("preview");
			list_preview.src=" " ;
			//alert(list_preview.src);
			myUploader="null";
			return;
		}
	/*}
	else
	{
		$("#cropperSpace").show();
		allowResizeCropper="350";
		generateCropperObj(response);
	}*/
}


$(document).ready(function(){
if(!allowResizeCropper)
{
	$("#resultImgProfile").hide();
	$("#resultImgThumb").hide();
}
});
  </script>

<div class="fieldCont">
<div class="fieldTitle">*Profile Picture</div>
<?php
if(isset($_GET['id']) && $getdetail['image_name']!="")
{
?>
<img width="100" id="stored_profile_image" src="<?php if(isset($_GET['id'])) echo $getdetail['image_name'];?>"  />
<img id="resultImgProfile"  width="100"/>
<?php
}
else
{
?>
<img id="resultImgProfile" width="100"/>
<img id="Noimagediv" src="http://regprofilepic.s3.amazonaws.com/Noimage.png" width="100"/>
<?php
}
?>


<a href="javascript:showCropper(false);" onclick="get_profile_pic()" class="hint" style="text-decoration:none;"><input type="button" value=" Change "/></a>
</div>

<div class="fieldCont">
<div class="fieldTitle">*Listing Picture</div>
<?php
if(isset($_GET['id']) && $getdetail['listing_image_name']!="")
{
?>
<img src="<?php if(isset($_GET['id'])) echo $getdetail['listing_image_name'];?>" width="100" id="stored_listing_image"/>
<img id="resultImgThumb"  width="100"/>
<?php
}
else
{
?>
<img id="resultImgThumb"  width="100"/>
<img id="Noimagediv_listing" src="http://regprofilepic.s3.amazonaws.com/Noimage.png" width="100"/>
<?php
}
?>
<a href="javascript:showCropper(350);" onclick="get_listing_pic()" class="hint" style="text-decoration:none;"><input type="button" value=" Change "/></a>
</div>


<!--<div style="margin:50px;">
	
	
</div>-->

<!-- POPUP BOX START -->
<div id="popupContact">
		<a id="popupContactClose">x</a>
		<p id="contactArea"><b>Image Cropper</b></p>
		
		<div style=" float: left; width: 100px;right:100px;">
			Preview
			<div style="width:100px;height:100px;overflow:hidden;">
				<img src="" id="preview" alt="Preview" class="artist_event_jcrop-preview" />
			</div>
		</div>
		
		<form action="" methos="post" enctype="multipart/form-data" style="width:75%; float:left; margin:19px 0 0 20px;">
			<div style="width:auto;float:left;"><input name="theFile" type="file" id="file_upload" /></div>
			<div id="hinttext" style="width:400px;float:left;color:grey;line-height:30px; position:relative; left:20px;">Image size should be more than 450x450.</div>
		</form>
		
		<div style="width:75%; float:left; margin-left:18px;">
			<input type="button" value="Crop Image" id="cropBtn" style=" background-color: #404040;border-radius: 3px 3px 3px 3px;color: white;cursor: pointer;padding: 7px 24px;border:none;margin-top:15px;"/>
			<input type="hidden" id="x" name="x" />
			<input type="hidden" id="y" name="y" />
			<input type="hidden" id="w" name="w" />
			<input type="hidden" id="h" name="h" />
		</div>
		<div style="clear:both"></div>
        <div id="errMsg" style="color:red;text-align:center;"></div>
		
		
		
		<table id="cropperSpace" style="display:none;  bottom: 100px;">
			  <tr>
			<td rowspan="2" id="targetTD">
				  <img src="" style="max-width:400px" id="target"/>				
				</td>
				
			  <!--</tr>
			  <tr>
			    <td>Result
					<div style="width:100px;height:100px;overflow:hidden;">
						<img  id="result" alt="Result" class="artist_event_jcrop-result" style="width:100px;height:100px;" />
					</div>
				</td>
		      </tr>-->
			  
			</table>
		<br>
</div>
<div id="backgroundPopup"></div>
<!-- POPUP BOX END -->