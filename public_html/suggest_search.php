<?php
header('Content-type: application/json');
include('classes/SuggestSearch.php');

$newclassobj = new SuggestSearch();
$q = strtolower($_GET["term"]);
if (!$q) return;

	$cou_gt=$newclassobj->count_suggest($q);
	$rs=$newclassobj->suggest_match($q);
	$result = array();
	for($i=0;$i<=count($rs);$i++)
	{
		if(isset($rs[$i]['title']) && $rs[$i]['title']!="")
		{
			if($rs[$i]['table_name']=='general_media' && $rs[$i]['media_type']=='113')
			{
				array_push($result, array("id"=>str_replace("'","`",$rs[$i]['title']).' (Galleries/Images)', "label"=>str_replace("'","`",$rs[$i]['title']).' (Galleries/Images)', "value" => str_replace("'","`",$rs[$i]['title']).' (Galleries/Images)', "value1" => $rs[$i]['table_name'], "value2" => $rs[$i]['id']));
			}
			elseif($rs[$i]['table_name']=='general_media' && $rs[$i]['media_type']=='114')
			{
				array_push($result, array("id"=>str_replace("'","`",$rs[$i]['title']).' (Song)', "label"=>str_replace("'","`",$rs[$i]['title']).' (Song)', "value" => str_replace("'","`",$rs[$i]['title']).' (Song)', "value1" => $rs[$i]['table_name'], "value2" => $rs[$i]['id']));
			}
			elseif($rs[$i]['table_name']=='general_media' && $rs[$i]['media_type']=='115')
			{
				array_push($result, array("id"=>str_replace("'","`",$rs[$i]['title']).' (Video)', "label"=>str_replace("'","`",$rs[$i]['title']).' (Video)', "value" => str_replace("'","`",$rs[$i]['title']).' (Video)', "value1" => $rs[$i]['table_name'], "value2" => $rs[$i]['id']));
			}
			elseif($rs[$i]['table_name']=='general_media' && $rs[$i]['media_type']=='116')
			{
				array_push($result, array("id"=>str_replace("'","`",$rs[$i]['title']).' (Playlist)', "label"=>str_replace("'","`",$rs[$i]['title']).' (Playlist)', "value" => str_replace("'","`",$rs[$i]['title']).' (Playlist)', "value1" => $rs[$i]['table_name'], "value2" => $rs[$i]['id']));
			}
			elseif($rs[$i]['table_name']=='artist_event')
			{
				$type_disp = $newclassobj->type_display($rs[$i]['table_name'],$rs[$i]['id']);
				if($type_disp!="" && !empty($type_disp))
				{
					$new_dt = date("m.d.y", strtotime($rs[$i]['date']));
					array_push($result, array("id"=>str_replace("'","`",$rs[$i]['title']).' ('.str_replace("'","`",$type_disp['name']).')', "label"=>$new_dt.' '.str_replace("'","`",$rs[$i]['title']).' ('.str_replace("'","`",$type_disp['name']).')', "value" => str_replace("'","`",$rs[$i]['title']).' ('.str_replace("'","`",$type_disp['name']).')', "value1" => $rs[$i]['table_name'], "value2" => $rs[$i]['id']));
				}
				else
				{
					$new_dt1 = date("m.d.y", strtotime($rs[$i]['date']));
					array_push($result, array("id"=>str_replace("'","`",$rs[$i]['title']), "label"=>str_replace("'","`",$rs[$i]['title']), "value" =>$new_dt1.' '.str_replace("'","`",$rs[$i]['title']), "value1" => $rs[$i]['table_name'], "value2" => $rs[$i]['id']));
				}
			}
			elseif($rs[$i]['table_name']=='community_event')
			{
				$type_disp = $newclassobj->type_display($rs[$i]['table_name'],$rs[$i]['id']);
				if($type_disp!="" && !empty($type_disp))
				{
					$new_dt2 = date("m.d.y", strtotime($rs[$i]['date']));
					array_push($result, array("id"=>str_replace("'","`",$rs[$i]['title']).' ('.str_replace("'","`",$type_disp['name']).')', "label"=>$new_dt2.' '.str_replace("'","`",$rs[$i]['title']).' ('.str_replace("'","`",$type_disp['name']).')', "value" => str_replace("'","`",$rs[$i]['title']).' ('.str_replace("'","`",$type_disp['name']).')', "value1" => $rs[$i]['table_name'], "value2" => $rs[$i]['id']));
				}
				else
				{
					$new_dt3 = date("m.d.y", strtotime($rs[$i]['date']));
					array_push($result, array("id"=>str_replace("'","`",$rs[$i]['title']), "label"=>str_replace("'","`",$rs[$i]['title']), "value" =>$new_dt3.' '.str_replace("'","`",$rs[$i]['title']), "value1" => $rs[$i]['table_name'], "value2" => $rs[$i]['id']));
				}
			}
			elseif($rs[$i]['table_name']=='artist_project')
			{
				$type_disp = $newclassobj->type_display($rs[$i]['table_name'],$rs[$i]['id']);
				if($type_disp!="" && !empty($type_disp))
				{
					array_push($result, array("id"=>str_replace("'","`",$rs[$i]['title']).' ('.str_replace("'","`",$type_disp['name']).')', "label"=>str_replace("'","`",$rs[$i]['title']).' ('.str_replace("'","`",$type_disp['name']).')', "value" => str_replace("'","`",$rs[$i]['title']).' ('.str_replace("'","`",$type_disp['name']).')', "value1" => $rs[$i]['table_name'], "value2" => $rs[$i]['id']));
				}
				else
				{
					array_push($result, array("id"=>str_replace("'","`",$rs[$i]['title']), "label"=>str_replace("'","`",$rs[$i]['title']), "value" => str_replace("'","`",$rs[$i]['title']), "value1" => $rs[$i]['table_name'], "value2" => $rs[$i]['id']));
				}
			}
			elseif($rs[$i]['table_name']=='community_project')
			{
				$type_disp = $newclassobj->type_display($rs[$i]['table_name'],$rs[$i]['id']);
				if($type_disp!="" && !empty($type_disp))
				{
					array_push($result, array("id"=>str_replace("'","`",$rs[$i]['title']).' ('.str_replace("'","`",$type_disp['name']).')', "label"=>str_replace("'","`",$rs[$i]['title']).' ('.str_replace("'","`",$type_disp['name']).')', "value" => str_replace("'","`",$rs[$i]['title']).' ('.str_replace("'","`",$type_disp['name']).')', "value1" => $rs[$i]['table_name'], "value2" => $rs[$i]['id']));
				}
				else
				{
					array_push($result, array("id"=>str_replace("'","`",$rs[$i]['title']), "label"=>str_replace("'","`",$rs[$i]['title']), "value" => str_replace("'","`",$rs[$i]['title']), "value1" => $rs[$i]['table_name'], "value2" => $rs[$i]['id']));
				}
			}
		}
		elseif(isset($rs[$i]['audio_id']) && $rs[$i]['audio_id']!="")
		{
				array_push($result, array("id"=>str_replace("'","`",$rs[$i]['audio_name']).' (Song)', "label"=>str_replace("'","`",$rs[$i]['audio_name']).' (Song)', "value" => str_replace("'","`",$rs[$i]['audio_name']).' (Song)', "value1" => $rs[$i]['table_name'], "value2" => $rs[$i]['audio_id']));
		}
		elseif(isset($rs[$i]['video_id']) && $rs[$i]['video_id']!="")
		{
			array_push($result, array("id"=>str_replace("'","`",$rs[$i]['video_name']).' (Video)', "label"=>str_replace("'","`",$rs[$i]['video_name']).' (Video)', "value" => str_replace("'","`",$rs[$i]['video_name']).' (Video)', "value1" => $rs[$i]['table_name'], "value2" => $rs[$i]['video_id']));
		}
		elseif(isset($rs[$i]['gallery_id']) && $rs[$i]['gallery_id']!="")
		{
			array_push($result, array("id"=>str_replace("'","`",$rs[$i]['gallery_title']).' (Galleries/Images)', "label"=>str_replace("'","`",$rs[$i]['gallery_title']).' (Galleries/Images)', "value" => str_replace("'","`",$rs[$i]['gallery_title']).' (Galleries/Images)', "value1" => $rs[$i]['table_name'], "value2" => $rs[$i]['gallery_id']));
		}
		elseif($rs[$i]['table_name']=='type' || $rs[$i]['table_name']=='subtype')
		{
			if($rs[$i]['table_name']=='type')
			{
				array_push($result, array("id"=>str_replace("'","`",$rs[$i]['name']).' (Category)', "label"=>str_replace("'","`",$rs[$i]["parent_name"].'/'.$rs[$i]['name']).' (Category)', "value" => str_replace("'","`",$rs[$i]['name']).' (Category)', "value1" => $rs[$i]['table_name'], "value2" => $rs[$i]['type_id']));
			}
			elseif($rs[$i]['table_name']=='subtype')
			{
				array_push($result, array("id"=>str_replace("'","`",$rs[$i]['name']).' (Category)', "label"=>str_replace("'","`",$rs[$i]["parent_name_1"].'/'.$rs[$i]["parent_name"].'/'.$rs[$i]['name']).' (Category)', "value" => str_replace("'","`",$rs[$i]['name']).' (Category)', "value1" => $rs[$i]['table_name'], "value2" => $rs[$i]['subtype_id']));
			}
		}
		elseif($rs[$i]['table_name']=='meta_type')
		{
			array_push($result, array("id"=>str_replace("'","`",$rs[$i]['name']).' (Category)', "label"=>str_replace("'","`",$rs[$i]["parent_name_2"].'/'.$rs[$i]["parent_name_1"].'/'.$rs[$i]["parent_name"].'/'.$rs[$i]['name']).' (Category)', "value" => str_replace("'","`",$rs[$i]['name']).' (Category)', "value1" => $rs[$i]['table_name'], "value2" => $rs[$i]['meta_id']));
		}
		else
		{
			if($rs[$i]['table_name']=='general_artist')
			{
				array_push($result, array("id"=>str_replace("'","`",$rs[$i]['name']).' (Artist)', "label"=>str_replace("'","`",$rs[$i]['name']).' (Artist)', "value" => str_replace("'","`",$rs[$i]['name']).' (Artist)', "value1" => $rs[$i]['table_name'], "value2" => $rs[$i]['artist_id']));
			}
			elseif($rs[$i]['table_name']=='general_community')
			{
				array_push($result, array("id"=>str_replace("'","`",$rs[$i]['name']).' (Company)', "label"=>str_replace("'","`",$rs[$i]['name']).' (Company)', "value" => str_replace("'","`",$rs[$i]['name']).' (Company)', "value1" => $rs[$i]['table_name'], "value2" => $rs[$i]['community_id']));
			}
		}
	}
	echo array_to_json($result);

function array_to_json( $array ){

    if( !is_array( $array ) ){
        return false;
    }

    $associative = count( array_diff( array_keys($array), array_keys( array_keys( $array )) ));
    if( $associative ){

        $construct = array();
        foreach( $array as $key => $value ){

            // We first copy each key/value pair into a staging array,
            // formatting each key and value properly as we go.

            // Format the key:
            if( is_numeric($key) ){
                $key = "key_$key";
            }
            $key = "\"".addslashes($key)."\"";

            // Format the value:
            if( is_array( $value )){
                $value = array_to_json( $value );
            } else if( !is_numeric( $value ) || is_string( $value ) ){
                $value = "\"".addslashes($value)."\"";
            }

            // Add to staging array:
            $construct[] = "$key: $value";
        }

        // Then we collapse the staging array into the JSON form:
        $result = "{ " . implode( ", ", $construct ) . " }";

    } else { // If the array is a vector (not associative):

        $construct = array();
        foreach( $array as $value ){

            // Format the value:
            if( is_array( $value )){
                $value = array_to_json( $value );
            } else if( !is_numeric( $value ) || is_string( $value ) ){
                $value = "'".addslashes($value)."'";
            }

            // Add to staging array:
            $construct[] = $value;
        }

        // Then we collapse the staging array into the JSON form:
        $result = "[ " . implode( ", ", $construct ) . " ]";
    }
	
    return $result;
}	
?>
