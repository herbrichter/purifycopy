<?php
	session_start();
	include_once('commons/db.php');
	include_once('classes/Promotions.php');
	include_once('sendgrid/SendGrid_loader.php');
	include_once('classes/Commontabs.php');
    
    $domainname=$_SERVER['SERVER_NAME'];
	
	$promotes = new Promotions();
	$newtab = new Commontabs();
	$sendgrid = new SendGrid('','');
?>
<meta http-equiv="X-UA-Compatible" content="IE=9.0">
<script type="text/javascript">var addthis_config = {"data_track_addressbar":false, "addthis_config.ui_show_promo":false};</script>
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-52ac1fee583ddf3c"></script>
<script type="text/javascript">
function show_emails()
{
	$("#hd_emails").css({"display":"block"});
	click_yes_radio();
}

function hide_emails()
{
	$("#hd_emails").css({"display":"none"});
	click_no_radio();
}

function show_social_shares()
{
	$("#social_shares_all").css({"display":"block"});
	click_yes_radio();
}

function hide_social_shares()
{
	$("#social_shares_all").css({"display":"none"});
	click_no_radio();
}

function validate_promote()
{
	var message = document.getElementById("up_mess").value;
	if(message=="")
	{
		//alert("Please enter some message.");
		//return false;
	}
	
}

$("document").ready(function(){
	$("#email_ups").change(function(){
		if($("#email_ups").val() =="artist_project_fans" || $("#email_ups").val() =="community_project_fans")
		{
			var element = $("option:selected", this);
			var myTag = element.attr("chk_val");
			document.getElementById("project_value_text").value = myTag;
		}
	});
});
	//]]>
	
$("#send_update_form").bind("submit",function(){
	var x = confirm("Are you sure you want to send out this update?");
	if(x==false){
		return false;
	}else{
		$("#send_update_button").css({"display":"none"});
		$("#loading_image").css({"display":"block"});
		$.ajax({
			type:"POST",
			url:"Update_facebook.php?spcl=<?php echo $_GET['spcl'];?>",
			data:$(this).serializeArray(),
			success:function(data){
				window.parent.document.getElementById("chk_vals_ups").value = '1';
				parent.jQuery.fancybox.close();
			}
		});
	}
	return false;
});

function click_yes_facebook_radio(){
	/* <?php 
	$sp_exp = explode('|',$_GET['spcl']);
	?>
	window.parent.document.getElementById("post_face").value = 'yes|'+'<?php echo $sp_exp[2]; ?>'; */
}
function click_no_facebook_radio(){
window.parent.document.getElementById("post_face").value = 'no';
}
function click_yes_radio(){
	window.parent.document.getElementById("chk_vals_ups").value = '0';
}
function click_no_radio(){
	window.parent.document.getElementById("chk_vals_ups").value = '0';
}
	</script>	
	<script>
		window.parent.document.getElementById("chk_vals_ups").value = '0';
		window.parent.document.getElementById("post_face").value = 'no';
	</script>
<div id="actualContent" style="float:left;width:540px">
<div class="fancy_header">
		Send Update
</div>
<div class="fancy_send_update_content">
<form method="POST" id="send_update_form" onSubmit="return validate_promote()">
	
	<div class="fieldCont">
		<div class="fieldTitle">Update Message:</div>
		<textarea  class="jquery_ckeditor" cols="67" id="up_mess" name="up_mess" rows="10"></textarea>
	</div>
	
	<div class="fieldCont">
		<div title="Add news feed posts to all of your friends, fan club members, and subscribers news feeds." class="fieldTitle">News Feeds:</div>
		<div class="chkCont">
			<input title="Add news feed posts to all of your friends, fan club members, and subscribers news feeds." type="radio" onclick="click_yes_radio()" name="new_feed" class="chk" value="yes" id="new_feed" />
			<div class="radioTitle">Yes</div>
		</div>
		<div class="chkCont">
			<input title="Add news feed posts to all of your friends, fan club members, and subscribers news feeds." type="radio" onclick="click_no_radio()" name="new_feed" class="chk" value="no" id="new_feed" />
			<div class="radioTitle">No</div>
		</div>
	</div>
	
	<!-- <div class="fieldCont">
		<div title="Add wall posts to a page of yours on facebook linking to your profile on Purify Art with the message entered above. You will be able to select the specific page on Facebook to add the post." class="fieldTitle">Facebook:</div>
		<div class="chkCont">
			<input title="Add wall posts to a page of yours on facebook linking to your profile on Purify Art with the message entered above. You will be able to select the specific page on Facebook to add the post." type="radio" onclick="click_yes_facebook_radio()" name="facebook_up" class="chk" value="yes" id="facebook_up" />
			<div class="radioTitle">Yes</div>
		</div>
		<div class="chkCont">
			<input title="Add wall posts to a page of yours on facebook linking to your profile on Purify Art with the message entered above. You will be able to select the specific page on Facebook to add the post." type="radio" name="facebook_up" onclick="click_no_facebook_radio()" class="chk" value="no" id="facebook_up" />
			<div class="radioTitle">No</div>
		</div>
	</div> -->
	
	<div class="fieldCont">
		<div title="Send an email newsletter linking to your profile on Purify with the message above. You can choose the specific email list to send the newsletter to." class="fieldTitle">Email:</div>
		<div class="chkCont">
			<input title="Send an email newsletter linking to your profile on Purify with the message above. You can choose the specific email list to send the newsletter to." type="radio" name="email_face" class="chk" onClick="show_emails()" value="yes" id="email_face" />
			<div class="radioTitle">Yes</div>
		</div>
		<div class="chkCont">
			<input title="Send an email newsletter linking to your profile on Purify with the message above. You can choose the specific email list to send the newsletter to." type="radio" name="email_face" class="chk" onClick="hide_emails()" value="no" id="email_face" />
			<div class="radioTitle">No</div>
		</div>
	</div>
	
	<div class="fieldCont" id="hd_emails" name="hd_emails" style="display:none;">
		<div class="fieldTitle">Select Users:</div>
		<?php
		$gen_detail = $promotes->get_login_user_details();
		$get_art_info = $promotes->Get_Artist_Info_e($gen_detail['artist_id']);
		$get_comm_info = $promotes->Get_community_Info_e($gen_detail['community_id']);
		?>
		<select class="dropdown" id="email_ups" name="email_ups">
			<option value="all_profiles_share">All</option>
			<?php
			if(!empty($get_art_info)){
				$arts = $promotes->check_avail_artist($get_art_info['artist_id']);
				if($arts==1)
				{
					$sql_subs = mysql_query("SELECT * FROM email_subscription WHERE email_update=1 AND delete_status=1 AND related_id='".$get_art_info['artist_id']."' AND related_type='artist'");
					if($sql_subs!="")
					{
						if(mysql_num_rows($sql_subs)>0)
						{
				?>
							<option value="artist_share"><?php echo $get_art_info['name']." Subscribers"; ?></option>
				<?php
						}
					}
				}
			}
			if(!empty($get_comm_info)){
			
				$coms = $promotes->check_avail_community($get_comm_info['community_id']);
				$sql_subs = mysql_query("SELECT * FROM email_subscription WHERE email_update=1 AND delete_status=1 AND related_id='".$get_comm_info['community_id']."' AND related_type='community'");
				if($sql_subs!="")
				{
					if(mysql_num_rows($sql_subs)>0)
					{
						if($coms==1)
						{
						?>
							<option value="community_share"><?php echo $get_comm_info['name']." Subscribers"; ?></option>
						<?php
						}
					}
				}
			}
			
				$gen = $promotes->check_avail_general($_SESSION['login_email']);
				if($gen==1)
				{
					$sql = mysql_query("SELECT * FROM general_user WHERE email='".$_SESSION['login_email']."'");
					$ans = mysql_fetch_assoc($sql);
				}
				
				if($ans['artist_id']!=0)
				{
					$chk_artist_fan = $promotes->find_artist_fans();
					if($chk_artist_fan!=""){
						$art = $promotes->check_avail_artist($ans['artist_id']);
						if($art==1)
						{
							$sql_subs = mysql_query("SELECT * FROM email_subscription WHERE email_update=1 AND delete_status=1 AND related_id='".$ans['artist_id']."' AND related_type='artist'");
							if($sql_subs!="")
							{
								if(mysql_num_rows($sql_subs)>0)
								{
									$data = $promotes->all_profiles_share_artist($ans['artist_id']);
									if($data[0]['typefan']=="fan_club_yes"){
				?>
									<option value="artist_fan_share"><?php echo $data[0]['name']; ?> Fan Club</option>
				<?php
									}
								}
							}
						}
					}
				}
				if($ans['community_id']!=0)
				{
					$chk_community_fan = $promotes->find_community_fans();
					if($chk_community_fan!=""){
						$com = $promotes->check_avail_community($ans['community_id']);
						if($com==1)
						{
							$sql_subs = mysql_query("SELECT * FROM email_subscription WHERE email_update=1 AND delete_status=1 AND related_id='".$ans['community_id']."' AND related_type='community'");
							if($sql_subs!="")
							{
								if(mysql_num_rows($sql_subs)>0)
								{
									$data_com = $promotes->all_profiles_share_community($ans['community_id']);
									if($data_com[0]['typefan']=="fan_club_yes"){
				?>
										<option value="community_fan_share"><?php echo $data_com[0]['name']; ?> Fan Club</option>
				<?php
									}
								}
							}
						}
					}
				}
				$get_art_pro_fan = $promotes->get_all_artist_projects();
				while($art_pro = mysql_fetch_assoc($get_art_pro_fan))
				{
					$chkpro_fan_visible = $promotes->find_projects_fans($art_pro['id'],"artist_project");
					if($chkpro_fan_visible !=""){
						$rart_infos = $promotes->get_login_user_details();
						$sql_subs = mysql_query("SELECT * FROM email_subscription WHERE email_update=1 AND delete_status=1 AND related_id='".$rart_infos['artist_id']."_".$art_pro['id']."' AND related_type='artist_project'");
						if($sql_subs!="")
						{
							if(mysql_num_rows($sql_subs)>0)
							{
								if($art_pro['type']=="fan_club"){
					?>
									<option value="artist_project_fans" chk_val="<?php echo $art_pro['id'];?>" ><?php echo $art_pro['title'];?> Fan Club</option>
					<?php
								}
							}
						}
					}
				}
				$get_com_pro_fan = $promotes->get_all_community_projects();
				while($com_pro = mysql_fetch_assoc($get_com_pro_fan))
				{
					$chkpro_fan_visible = $promotes->find_projects_fans($com_pro['id'],"community_project");
					if($chkpro_fan_visible !=""){
						$rart_infos = $promotes->get_login_user_details();
						$sql_subs = mysql_query("SELECT * FROM email_subscription WHERE email_update=1 AND delete_status=1 AND related_id='".$rart_infos['community_id']."_".$com_pro['id']."' AND related_type='community_project'");
						if($sql_subs!="")
						{
							if(mysql_num_rows($sql_subs)>0)
							{
								if($com_pro['type']=="fan_club"){
					?>
									<option value="community_project_fans" chk_val="<?php echo $com_pro['id'];?>" ><?php echo $com_pro['title'];?> Fan Club</option>
					<?php
								}
							}
						}
					}
				}
			?>
		</select>
	</div>
	
	<div class="fieldCont">
		<div title="" class="fieldTitle">Social:</div>
		<div id="social_shares_all" style="margin-bottom: 12px; margin-left: 113px;">
			<div class="addthis_toolbox addthis_default_style addthis_32x32_style">
				<a class="addthis_button_facebook" addthis:url="http://<?php echo $domainname; ?>/<?php echo $_GET['url_mtch_ups']; ?>"></a>
				<a class="addthis_button_twitter" addthis:title="<?php echo $_GET['title_for_socials']; ?>" addthis:url="http://<?php echo $domainname; ?>/<?php echo $_GET['url_mtch_ups']; ?>"></a>
				<a class="addthis_button_google_plusone_share" addthis:url="http://<?php echo $domainname; ?>/<?php echo $_GET['url_mtch_ups']; ?>"></a>
				
				<a class="addthis_button_tumblr" addthis:title="<?php echo $_GET['title_for_socials']; ?>" addthis:url="http://<?php echo $domainname; ?>/<?php echo $_GET['url_mtch_ups']; ?>"></a>
				<a class="addthis_button_stumbleupon" addthis:title="<?php echo $_GET['title_for_socials']; ?>" addthis:url="http://<?php echo $domainname; ?>/<?php echo $_GET['url_mtch_ups']; ?>"></a>
			</div>
		</div>
		<!--<div class="chkCont">
			<input title="" type="radio" name="email_face" class="chk" onClick="show_social_shares()" value="yes" id="email_face" />
			<div class="radioTitle">Yes</div>
		</div>
		<div class="chkCont">
			<input title="" type="radio" name="email_face" class="chk" onClick="hide_social_shares()" value="no" id="email_face" />
			<div class="radioTitle">No</div>
		</div>-->
	</div>
	
	<div id="social_shares_all" style="display:none; margin-bottom: 12px; margin-left: 113px;">
		<div class="addthis_toolbox addthis_default_style addthis_32x32_style">
			<a class="addthis_button_facebook" addthis:url="http://<?php echo $domainname; ?>/<?php echo $_GET['url_mtch_ups']; ?>"></a>
			<a class="addthis_button_twitter" addthis:title="<?php echo $_GET['title_for_socials']; ?>" addthis:url="http://<?php echo $domainname; ?>/<?php echo $_GET['url_mtch_ups']; ?>"></a>
			<a class="addthis_button_google_plusone_share" addthis:url="http://<?php echo $domainname; ?>/<?php echo $_GET['url_mtch_ups']; ?>"></a>
			
			<a class="addthis_button_tumblr" addthis:title="<?php echo $_GET['title_for_socials']; ?>" addthis:url="http://<?php echo $domainname; ?>/<?php echo $_GET['url_mtch_ups']; ?>"></a>
			<a class="addthis_button_stumbleupon" addthis:title="<?php echo $_GET['title_for_socials']; ?>" addthis:url="http://<?php echo $domainname; ?>/<?php echo $_GET['url_mtch_ups']; ?>"></a>
		</div>
	</div>
	
	<div class="fieldCont">
	<input type="hidden" id="project_value_text" name="project_value_text"/>
		<div class="fieldTitle"></div>
		<input type="submit" class="register" id="send_update_button" value="Submit" style="border-radius:5px; font-size:13px;" />
		<img src="images/ajax_loader_large.gif" id="loading_image" style="height: 35px;position: relative;display:none;"/>
	</div>
</form>
</div>
</div>

<?php
	if(isset($_POST) && !empty($_POST))
	{
		include("newsmtp/PHPMailer/index_facebook.php");
		$sp_exp = explode('|',$_GET['spcl']);
		if($sp_exp[0]=="general_artist")
		{
			$sql_sub = mysql_query("SELECT * FROM general_artist WHERE artist_id='".$sp_exp[1]."'");
			$ans_sub = mysql_fetch_assoc($sql_sub);
			
			$list_em = '';
			$scspl = '';
		}
		elseif($sp_exp[0]=="general_community")
		{
			$sql_sub = mysql_query("SELECT * FROM general_community WHERE community_id='".$sp_exp[1]."'");
			$ans_sub = mysql_fetch_assoc($sql_sub);
			
			$list_em = '';
			$scspl = '';
		}
		elseif($sp_exp[0]=="artist_event" || $sp_exp[0]=="community_event" || $sp_exp[0]=="community_project" || $sp_exp[0]=="artist_project")
		{
			//die;
			$sql_sub = mysql_query("SELECT * FROM $sp_exp[0] WHERE id='".$sp_exp[1]."'");
			$ans_sub = mysql_fetch_assoc($sql_sub);
			
			$list_em = '';
			$scspl = '';
			$orgs_dis = explode('~',$sp_exp[1]);
			if($sp_exp[0]=="artist_event")
			{
				$scspl = 'events_post';
				$list_em = $orgs_dis[0].'~artist_event';
			}
			if($sp_exp[0]=="community_event")
			{
				$scspl = 'events_post';
				$list_em = $orgs_dis[0].'~community_event';
			}
			if($sp_exp[0]=="community_project")
			{
				$scspl = 'projects_post';
				$list_em = $orgs_dis[0].'~community_project';
			}
			if($sp_exp[0]=="artist_project")
			{
				$scspl = 'projects_post';
				$list_em = $orgs_dis[0].'~artist_project';
			}
			
			if($ans_sub['creators_info']!="")
			{
				$ans_create = $promotes->get_creators($ans_sub['creators_info']);
				if($ans_create!="" && !empty($ans_create))
				{
					if(isset($ans_create['name']))
					{
						$from_name = $ans_create['name'];
					}
					elseif(isset($ans_create['title']))
					{
						$from_name = $ans_create['title'];
					}
				}
				else
				{
					$from_name = $ans_sub['title'];
				}
			}
			else
			{
				$from_name = $ans_sub['title'];
			}
			
		}
		
		//var_dump($ans_sub);
		//die;
		
		if($_POST['email_face']=='yes')
		{
			if($_POST['email_ups']=='all_profiles_share')
			{
				$emails = $promotes->email_all();
				//var_dump($emails);
				//die;
				//$sql_all_ups = mysql_query("SELECT * FROM general_user WHERE email!='".$_SESSION['login_email']."' AND delete_status=0");
				/* var_dump($emails);
				echo count($emails);
				die; */
				if(count($emails)>0)
				{
//$emails[0] = 'mithesh@remotedataexchange.com';
					//$strTo = 'mithesh.kavrani@iniksha.com'; 
					//while($rows = mysql_fetch_assoc($sql_all_ups))
					//{
					for($alls=0;$alls<count($emails);$alls++)
					{
						if($emails[$alls]!="")
						{
							$strTo = $emails[$alls];
						//	$mail->AddAddress($strTo);
							$mail->addBcc($strTo);
							//echo $strTo."</br>";
							if($ans_sub['name']!="" && isset($ans_sub['name']))
							{
								$strSubject = $ans_sub['name'].' Update';
								//$mail->FromName = $ans_sub['name'];
								$forms_name = $ans_sub['name'];
								if(isset($ans_sub['artist_id']))
								{
									$dsteals = 'artist@'.$ans_sub['artist_id'];
								}
								elseif(isset($ans_sub['community_id']))
								{
									$dsteals = 'community@'.$ans_sub['community_id'];
								}
								$unsub_name = $ans_sub['name'];
							}
							else
							{
								$strSubject = $ans_sub['title'].' Update';
								
								//$mail->FromName = $from_name;
								$forms_name = $from_name;
								if(isset($ans_sub['artist_id']))
								{
									$dsteals = 'artist_project@'.$ans_sub['id'];
								}
								elseif(isset($ans_sub['community_id']))
								{
									$dsteals = 'community_project@'.$ans_sub['id'];
								}
								$unsub_name = $ans_sub['title'];
							}
							$details = 'all';
							$unsub_name = 'Purify Art Updates';
							
							$strHeader = "";
							$strHeader .= "from: Purify Art < no-reply@purifyart.com>"."\r\n";
							$strHeader .= "MIME-Version: 1.0\r\n";
							$strHeader .= "Content-Type: text/html; charset=ISO-8859-1\r\n";

							$message = '<html><body>';
							if($_POST['up_mess']!="")
							{
								$message .= $_POST['up_mess'];
							}
						//	$message .= '<img src="http://artjcropthumb.s3.amazonaws.com/Noimage.png" style="float:left;" height=100 />';
							$message .= "<br><a href=http://".$domainname."/$ans_sub[profile_url]>purifyart.com/$ans_sub[profile_url]</a><br><br>";
							$message .= '</body></html>';
							//$mail->Subject = $strSubject;
							//mail($strTo,$strSubject,$message,$strHeader);
							//$mail->Body    = $message;
							
							$mail_grid = new SendGrid\Mail();

							$mail_grid->addTo($strTo)->
								   setFromName($unsub_name)->
									setFrom('no-reply@purifyart.com')->
								   setSubject($strSubject)->
								   setHtml($message.'<div style="background-color: #FFFFFF; height: 5px; display:block;"></div>
	<div style="background-color: #cbcaca; height: 1px; display:block;"></div>
	<div style="background-color: #FFFFFF; height: 5px; display:block;"></div><font size="-1" color="#666666" face="Arial, Helvetica, sans-serif" style="font-size: 11px; color: #666666; font-family:  Geneva, Arial, Helvetica, sans-serif; font-weight: normal;">&#169;2014 Purify Art - <a style="color: black;" href="http://". $domainname"./example_all.php?type_unsub=subscrb&alldt='.$dsteals.'">Unsubscribe to '.$unsub_name.'</a></font>');
							//$sendgrid->web->send($mail_grid);
							$sendgrid -> smtp -> send($mail_grid); 
							
							/* if($mail->Send()){
								$sentmail = true;
							} */
						}
					}
				}
			}
			elseif($_POST['email_ups']=='artist_share')
			{
				$emails_art = $promotes->email_all_artist();
				//$sql_all_ups = mysql_query("SELECT * FROM general_user WHERE email!='".$_SESSION['login_email']."' AND delete_status=0 AND artist_id!=0");
				if(count($emails_art)>0)
				{
					//$strTo = 'mithesh.kavrani@iniksha.com';
					for($allas=0;$allas<count($emails_art);$allas++)
					{
						if($emails_art[$allas]!="")
						{
							$mail = new PHPMailer();

							$mail->IsSMTP();                                      // set mailer to use SMTP
							$mail->Host = "smtp.gmail.com";  // specify main and backup server
							$mail->SMTPAuth = true;     // turn on SMTP authentication
							$mail->Username = "purifyart17@gmail.com";  // SMTP username
							$mail->Password = "purify1717"; // SMTP password
							$mail->SMTPSecure = "ssl";
							$mail->From = "purifyart17@gmail.com";
							//$mail->FromName = "Purify Art";
							//$mail->AddReplyTo("purifyart17@gmail.com", "Information");
							$mail->WordWrap = 50;                                 // set word wrap to 50 characters
							//$mail->AddAttachment("/var/tmp/file.tar.gz");         // add attachments
							//$mail->AddAttachment("/tmp/image.jpg", "new.jpg");    // optional name
							$mail->IsHTML(true);
							
							$to = $emails_art[$allas];
							//$mail->AddAddress($to);
							$mail->addBcc($to);
							if($ans_sub['name']!="" && isset($ans_sub['name']))
							{
								$subject = $ans_sub['name'].' Update';
								//$mail->FromName = $ans_sub['name'];
								$forms_name = $ans_sub['name'];
								if(isset($ans_sub['artist_id']))
								{
									$dsteals = 'artist@'.$ans_sub['artist_id'];
								}
								elseif(isset($ans_sub['community_id']))
								{
									$dsteals = 'community@'.$ans_sub['community_id'];
								}
								$unsub_name = $ans_sub['name'];
							}
							else
							{
								$subject = $ans_sub['title'].' Update';
								//$mail->FromName = $from_name;
								$forms_name = $from_name;
								if(isset($ans_sub['artist_id']))
								{
									$dsteals = 'artist_project@'.$ans_sub['id'];
								}
								elseif(isset($ans_sub['community_id']))
								{
									$dsteals = 'community_project@'.$ans_sub['id'];
								}
								$unsub_name = $ans_sub['title'];
							}
							$profiles_detail = $newtab->artist_status();
							$dsteals = 'artist@'.$profiles_detail['artist_id'];
							$unsub_name = $profiles_detail['name'];
							
							$message1 = $_POST['up_mess'];
							$message1 .= "<html><body><br><a href=http://".$domainname."/$ans_sub[profile_url]>purifyart.com/$ans_sub[profile_url]</a><br><br></body></html>";
							$header = "";
							$header .= "from: Purify Art < no-reply@purifyart.com>"."\r\n";
							$header .= "MIME-Version: 1.0\r\n";
							$header .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
							
							//$retval = mail ( $to, $subject, $message1, $header );
							//$mail->Subject = $subject;
							//$mail->Body    = $message1;
							/* if($mail->Send()){
								$sentmail = true;
							} */
							
							$mail_grid = new SendGrid\Mail();

							$mail_grid->addTo($to)->
								   setFromName($unsub_name)->
									setFrom('no-reply@purifyart.com')->
								   setSubject($subject)->
								   setHtml($message1.'<div style="background-color: #FFFFFF; height: 5px; display:block;"></div>
	<div style="background-color: #cbcaca; height: 1px; display:block;"></div>
	<div style="background-color: #FFFFFF; height: 5px; display:block;"></div><font size="-1" color="#666666" face="Arial, Helvetica, sans-serif" style="font-size: 11px; color: #666666; font-family:  Geneva, Arial, Helvetica, sans-serif; font-weight: normal;">&#169;2014 Purify Art - <a style="color: black;" href="http://".$domainname."/example_adv.php?type_unsub=subscrb&alldt='.$dsteals.'">Unsubscribe to '.$unsub_name.'</a></font>');
							//$sendgrid->web->send($mail_grid);
							$sendgrid -> smtp -> send($mail_grid); 
							
							/*$strTo = $emails_art[$allas];
							
							if($ans_sub['name']!="" && isset($ans_sub['name']))
							{
								$strSubject = $ans_sub['name'].' Update';
							}
							else
							{
								$strSubject = $ans_sub['title'].' Update';
							}
							$strMessage= $_POST['up_mess'];
							$strHeader = "";
							$strHeader .= "From: Purify Art < no-reply@purifyart.com>";
							$strHeader .= "MIME-Version: 1.0\r\n";
							$strHeader .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
							mail($strTo,$strSubject,$strMessage,$strHeader);*/
						}
					}
				}
			}
			elseif($_POST['email_ups']=='community_share')
			{
				//$sql_all_ups = mysql_query("SELECT * FROM general_user WHERE email!='".$_SESSION['login_email']."' AND delete_status=0 AND community_id!=0");
				$emails_com = $promotes->email_all_community();
				if(count($emails_com)>0)
				{
					//$strTo = 'mithesh.kavrani@iniksha.com';
					for($allcs=0;$allcs<count($emails_com);$allcs++)
					{
						if($emails_com[$allcs]!="")
						{
							$mail = new PHPMailer();

							$mail->IsSMTP();                                      // set mailer to use SMTP
							$mail->Host = "smtp.gmail.com";  // specify main and backup server
							$mail->SMTPAuth = true;     // turn on SMTP authentication
							$mail->Username = "purifyart17@gmail.com";  // SMTP username
							$mail->Password = "purify1717"; // SMTP password
							$mail->SMTPSecure = "ssl";
							$mail->From = "purifyart17@gmail.com";
							//$mail->FromName = "Purify Art";
							//$mail->AddReplyTo("purifyart17@gmail.com", "Information");
							$mail->WordWrap = 50;                                 // set word wrap to 50 characters
							//$mail->AddAttachment("/var/tmp/file.tar.gz");         // add attachments
							//$mail->AddAttachment("/tmp/image.jpg", "new.jpg");    // optional name
							$mail->IsHTML(true);
							
							$strTo1 = $emails_com[$allcs];
							//$mail->AddAddress($strTo1);
							$mail->addBcc($strTo1);
							if($ans_sub['name']!="" && isset($ans_sub['name']))
							{
								$strSubject1 = $ans_sub['name'].' Update';
								//$mail->FromName = 
								$forms_name = $ans_sub['name'];
								if(isset($ans_sub['artist_id']))
								{
									$dsteals = 'artist@'.$ans_sub['artist_id'];
								}
								elseif(isset($ans_sub['community_id']))
								{
									$dsteals = 'community@'.$ans_sub['community_id'];
								}
								$unsub_name = $ans_sub['name'];
							}
							else
							{
								$strSubject1 = $ans_sub['title'].' Update';
								//$mail->FromName = 
								$forms_name = $from_name;
								if(isset($ans_sub['artist_id']))
								{
									$dsteals = 'artist_project@'.$ans_sub['id'];
								}
								elseif(isset($ans_sub['community_id']))
								{
									$dsteals = 'community_project@'.$ans_sub['id'];
								}
								$unsub_name = $ans_sub['title'];
							}
							
							$profiles_detail = $newtab->community_status();
							$dsteals = 'community@'.$profiles_detail['community_id'];
							$unsub_name = $profiles_detail['name'];
		
							$strMessage1 = $_POST['up_mess'];
							$strMessage1 .= "<html><body><br><a href=http://".$domainname."/$ans_sub[profile_url]>purifyart.com/$ans_sub[profile_url]</a><br><br></body></html>";
							$strHeader1 .= "from: Purify Art < no-reply@purifyart.com>"."\r\n";
							$strHeader1 .= "MIME-Version: 1.0\r\n";
							$strHeader1 .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
							//mail($strTo1,$strSubject1,$strMessage1,$strHeader1);
							/* $mail->Subject = $strSubject1;
							$mail->Body    = $strMessage1;
							if($mail->Send()){
								$sentmail = true;
							} */
							
							$mail_grid = new SendGrid\Mail();

							$mail_grid->addTo($strTo1)->
								  setFromName($unsub_name)->
									setFrom('no-reply@purifyart.com')->
								   setSubject($strSubject1)->
								   setHtml($strMessage1.'<div style="background-color: #FFFFFF; height: 5px; display:block;"></div>
	<div style="background-color: #cbcaca; height: 1px; display:block;"></div>
	<div style="background-color: #FFFFFF; height: 5px; display:block;"></div><font size="-1" color="#666666" face="Arial, Helvetica, sans-serif" style="font-size: 11px; color: #666666; font-family:  Geneva, Arial, Helvetica, sans-serif; font-weight: normal;">&#169;2014 Purify Art - <a style="color: black;" href="http://".$domainname."/example_adv.php?type_unsub=subscrb&alldt='.$dsteals.'">Unsubscribe to '.$unsub_name.'</a></font>');
							//$sendgrid->web->send($mail_grid);
							$sendgrid -> smtp -> send($mail_grid); 
						}
					}
				}
			}
			elseif($_POST['email_ups']=='artist_fan_share')
			{
				//$sql_all_ups = mysql_query("SELECT * FROM general_user WHERE email!='".$_SESSION['login_email']."' AND delete_status=0 AND community_id!=0");
				$emails_art_fan = $promotes->email_all_artist_fans();
				if(count($emails_art_fan)>0)
				{
					//$strTo = 'mithesh.kavrani@iniksha.com';
					for($allaf=0;$allaf<count($emails_art_fan);$allaf++)
					{
						if($emails_art_fan[$allaf]!="")
						{
							$mail = new PHPMailer();

							$mail->IsSMTP();                                      // set mailer to use SMTP
							$mail->Host = "smtp.gmail.com";  // specify main and backup server
							$mail->SMTPAuth = true;     // turn on SMTP authentication
							$mail->Username = "purifyart17@gmail.com";  // SMTP username
							$mail->Password = "purify1717"; // SMTP password
							$mail->SMTPSecure = "ssl";
							$mail->From = "purifyart17@gmail.com";
							//$mail->FromName = "Purify Art";
							//$mail->AddReplyTo("purifyart17@gmail.com", "Information");
							$mail->WordWrap = 50;                                 // set word wrap to 50 characters
							//$mail->AddAttachment("/var/tmp/file.tar.gz");         // add attachments
							//$mail->AddAttachment("/tmp/image.jpg", "new.jpg");    // optional name
							$mail->IsHTML(true);
							
							$strTo2 = $emails_art_fan[$allaf];
							//$mail->AddAddress($strTo2);
							$mail->addBcc($strTo2);
							//echo $strTo2."</br>";
							if($ans_sub['name']!="" && isset($ans_sub['name']))
							{
								$strSubject2 = $ans_sub['name'].' Update';
								//$mail->FromName = 
								$forms_name = $ans_sub['name'];
								if(isset($ans_sub['artist_id']))
								{
									$dsteals = 'artist@'.$ans_sub['artist_id'];
								}
								elseif(isset($ans_sub['community_id']))
								{
									$dsteals = 'community@'.$ans_sub['community_id'];
								}
								$unsub_name = $ans_sub['name'];
							}
							else
							{
								$strSubject2 = $ans_sub['title'].' Update';
								//$mail->FromName = 
								$forms_name = $from_name;
								if(isset($ans_sub['artist_id']))
								{
									$dsteals = 'artist_project@'.$ans_sub['id'];
								}
								elseif(isset($ans_sub['community_id']))
								{
									$dsteals = 'community_project@'.$ans_sub['id'];
								}
								$unsub_name = $ans_sub['title'];
							}
							
							$profiles_detail = $newtab->artist_status();
							$dsteals = 'artist@'.$profiles_detail['artist_id'];
							$unsub_name = $profiles_detail['name'];
							
							$strMessage2 = $_POST['up_mess'];
							$strMessage2 .= "<html><body><br><a href=http://".$domainname."/$ans_sub[profile_url]>purifyart.com/$ans_sub[profile_url]</a><br><br></body></html>";
							$strHeader2 .= "from: Purify Art < no-reply@purifyart.com>"."\r\n";
							$strHeader2 .= "MIME-Version: 1.0\r\n";
							$strHeader2 .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
							//mail($strTo2,$strSubject2,$strMessage2,$strHeader2);
							/* $mail->Subject = $strSubject2;
							$mail->Body    = $strMessage2;
							if($mail->Send()){
								$sentmail = true;
							} */
							
							$mail_grid = new SendGrid\Mail();

							$mail_grid->addTo($strTo2)->
								   setFromName($unsub_name)->
									setFrom('no-reply@purifyart.com')->
								   setSubject($strSubject2)->
								   setHtml($strMessage2.'<div style="background-color: #FFFFFF; height: 5px; display:block;"></div>
	<div style="background-color: #cbcaca; height: 1px; display:block;"></div>
	<div style="background-color: #FFFFFF; height: 5px; display:block;"></div><font size="-1" color="#666666" face="Arial, Helvetica, sans-serif" style="font-size: 11px; color: #666666; font-family:  Geneva, Arial, Helvetica, sans-serif; font-weight: normal;">&#169;2014 Purify Art - <a style="color: black;" href="http://".$domainname."/example_adv.php?type_unsub=subscrb&alldt='.$dsteals.'">Unsubscribe to '.$unsub_name.'</a></font>');
							//$sendgrid->web->send($mail_grid);
							$sendgrid -> smtp -> send($mail_grid); 
						}
					}
				}
			}
			elseif($_POST['email_ups']=='community_fan_share')
			{
				//$sql_all_ups = mysql_query("SELECT * FROM general_user WHERE email!='".$_SESSION['login_email']."' AND delete_status=0 AND community_id!=0");
				$emails_com_fan = $promotes->email_all_community_fans();
				if(count($emails_com_fan)>0)
				{
					//$strTo = 'mithesh.kavrani@iniksha.com';
					for($allcf=0;$allcf<count($emails_com_fan);$allcf++)
					{
						if($emails_com_fan[$allcf]!="")
						{
							$mail = new PHPMailer();

							$mail->IsSMTP();                                      // set mailer to use SMTP
							$mail->Host = "smtp.gmail.com";  // specify main and backup server
							$mail->SMTPAuth = true;     // turn on SMTP authentication
							$mail->Username = "purifyart17@gmail.com";  // SMTP username
							$mail->Password = "purify1717"; // SMTP password
							$mail->SMTPSecure = "ssl";
							$mail->From = "purifyart17@gmail.com";
							//$mail->FromName = "Purify Art";
							//$mail->AddReplyTo("purifyart17@gmail.com", "Information");
							$mail->WordWrap = 50;                                 // set word wrap to 50 characters
							//$mail->AddAttachment("/var/tmp/file.tar.gz");         // add attachments
							//$mail->AddAttachment("/tmp/image.jpg", "new.jpg");    // optional name
							$mail->IsHTML(true);
							
							$strTo3 = $emails_com_fan[$allcf];
							//$mail->AddAddress($strTo3);
							//$mail->addBcc($strTo3);
							if($ans_sub['name']!="" && isset($ans_sub['name']))
							{
								$strSubject3 = $ans_sub['name'].' Update';
							//	$mail->FromName = 
								$forms_name = $ans_sub['name'];
								if(isset($ans_sub['artist_id']))
								{
									$dsteals = 'artist@'.$ans_sub['artist_id'];
								}
								elseif(isset($ans_sub['community_id']))
								{
									$dsteals = 'community@'.$ans_sub['community_id'];
								}
								$unsub_name = $ans_sub['name'];
							}
							else
							{
								$strSubject3 = $ans_sub['title'].' Update';
							//	$mail->FromName = 
								$forms_name = $from_name;
								if(isset($ans_sub['artist_id']))
								{
									$dsteals = 'artist_project@'.$ans_sub['id'];
								}
								elseif(isset($ans_sub['community_id']))
								{
									$dsteals = 'community_project@'.$ans_sub['id'];
								}
								$unsub_name = $ans_sub['title'];
							}
							
							$profiles_detail = $newtab->community_status();
							$dsteals = 'community@'.$profiles_detail['community_id'];
							$unsub_name = $profiles_detail['name'];
							
							$strMessage3 = $_POST['up_mess'];
							$strMessage3 .= "<html><body><br><a href=http://".$domainname."/$ans_sub[profile_url]>purifyart.com/$ans_sub[profile_url]</a><br><br></body></html>";
							$strHeader3 .= "from: Purify Art < no-reply@purifyart.com>"."\r\n";
							$strHeader3 .= "MIME-Version: 1.0\r\n";
							$strHeader3 .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
							//mail($strTo3,$strSubject3,$strMessage3,$strHeader3);
							/* $mail->Subject = $strSubject3;
							$mail->Body    = $strMessage3;
							if($mail->Send()){
								$sentmail = true;
							} */
							
							$mail_grid = new SendGrid\Mail();

							$mail_grid->addTo($strTo3)->
								  setFromName($unsub_name)->
									setFrom('no-reply@purifyart.com')->
								   setSubject($strSubject3)->
								   setHtml($strMessage3.'<div style="background-color: #FFFFFF; height: 5px; display:block;"></div>
	<div style="background-color: #cbcaca; height: 1px; display:block;"></div>
	<div style="background-color: #FFFFFF; height: 5px; display:block;"></div><font size="-1" color="#666666" face="Arial, Helvetica, sans-serif" style="font-size: 11px; color: #666666; font-family:  Geneva, Arial, Helvetica, sans-serif; font-weight: normal;">&#169;2014 Purify Art - <a style="color: black;" href="http://".$domainname."/example_adv.php?type_unsub=subscrb&alldt='.$dsteals.'">Unsubscribe to '.$unsub_name.'</a></font>');
							//$sendgrid->web->send($mail_grid);
							$sendgrid -> smtp -> send($mail_grid); 
						}
					}
				}
			}
			elseif($_POST['email_ups']=='artist_project_fans')
			{
				//$sql_all_ups = mysql_query("SELECT * FROM general_user WHERE email!='".$_SESSION['login_email']."' AND delete_status=0 AND community_id!=0");
				$emails_com_fan = $promotes->email_all_artistpro_fans($_POST['project_value_text']);
				if(count($emails_com_fan)>0)
				{
					//$strTo = 'mithesh.kavrani@iniksha.com';
					for($allcf=0;$allcf<count($emails_com_fan);$allcf++)
					{
						if($emails_com_fan[$allcf]!="")
						{
							$mail = new PHPMailer();

							$mail->IsSMTP();                                      // set mailer to use SMTP
							$mail->Host = "smtp.gmail.com";  // specify main and backup server
							$mail->SMTPAuth = true;     // turn on SMTP authentication
							$mail->Username = "purifyart17@gmail.com";  // SMTP username
							$mail->Password = "purify1717"; // SMTP password
							$mail->SMTPSecure = "ssl";
							$mail->From = "purifyart17@gmail.com";
							//$mail->FromName = "Purify Art";
							//$mail->AddReplyTo("purifyart17@gmail.com", "Information");
							$mail->WordWrap = 50;                                 // set word wrap to 50 characters
							//$mail->AddAttachment("/var/tmp/file.tar.gz");         // add attachments
							//$mail->AddAttachment("/tmp/image.jpg", "new.jpg");    // optional name
							$mail->IsHTML(true);
							
							$strTo4 = $emails_com_fan[$allcf];
							//$mail->AddAddress($strTo4);
							//$mail->addBcc($strTo4);
							if($ans_sub['name']!="" && isset($ans_sub['name']))
							{
								$strSubject4 = $ans_sub['name'].' Update';
							//	$mail->FromName = 
								$forms_name = $ans_sub['name'];
								if(isset($ans_sub['artist_id']))
								{
									$dsteals = 'artist@'.$ans_sub['artist_id'];
								}
								elseif(isset($ans_sub['community_id']))
								{
									$dsteals = 'community@'.$ans_sub['community_id'];
								}
								$unsub_name = $ans_sub['name'];
							}
							else
							{
								$strSubject4 = $ans_sub['title'].' Update';
							///	$mail->FromName = 
								$forms_name = $from_name;
								if(isset($ans_sub['artist_id']))
								{
									$dsteals = 'artist_project@'.$ans_sub['id'];
								}
								elseif(isset($ans_sub['community_id']))
								{
									$dsteals = 'community_project@'.$ans_sub['id'];
								}
								$unsub_name = $ans_sub['title'];
							}
							
							$dsteals = 'artist_project@'.$_POST['project_value_text'];
							$profiles_detail = $newtab->get_projectdetails('artist_project',$_POST['project_value_text']);
							$unsub_name = $profiles_detail['title'];
			
							$strMessage4 = $_POST['up_mess'];
							$strMessage4 .= "<html><body><br><a href=http://".$domainname."/$ans_sub[profile_url]>purifyart.com/$ans_sub[profile_url]</a><br><br></body></html>";
							$strHeader4 .= "from: Purify Art < no-reply@purifyart.com>"."\r\n";
							$strHeader4 .= "MIME-Version: 1.0\r\n";
							$strHeader4 .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
							//mail($strTo4,$strSubject4,$strMessage4,$strHeader4);
							/* $mail->Subject = $strSubject4;
							$mail->Body    = $strMessage4;
							if($mail->Send()){
								$sentmail = true;
							} */
							
							$mail_grid = new SendGrid\Mail();

							$mail_grid->addTo($strTo4)->
								    setFromName($unsub_name)->
									setFrom('no-reply@purifyart.com')->
								   setSubject($strSubject4)->
								   setHtml($strMessage4.'<div style="background-color: #FFFFFF; height: 5px; display:block;"></div>
	<div style="background-color: #cbcaca; height: 1px; display:block;"></div>
	<div style="background-color: #FFFFFF; height: 5px; display:block;"></div><font size="-1" color="#666666" face="Arial, Helvetica, sans-serif" style="font-size: 11px; color: #666666; font-family:  Geneva, Arial, Helvetica, sans-serif; font-weight: normal;">&#169;2014 Purify Art - <a style="color: black;" href="http://".$domainname."/example_adv.php?type_unsub=subscrb&alldt='.$dsteals.'">Unsubscribe to '.$unsub_name.'</a></font>');
							//$sendgrid->web->send($mail_grid);
							$sendgrid -> smtp -> send($mail_grid); 
						}
					}
				}
			}
			elseif($_POST['email_ups']=='community_project_fans')
			{
				//$sql_all_ups = mysql_query("SELECT * FROM general_user WHERE email!='".$_SESSION['login_email']."' AND delete_status=0 AND community_id!=0");
				$emails_com_fan = $promotes->email_all_communitypro_fans($_POST['project_value_text']);
				if(count($emails_com_fan)>0)
				{
					//$strTo = 'mithesh.kavrani@iniksha.com';
					for($allcf=0;$allcf<count($emails_com_fan);$allcf++)
					{
						if($emails_com_fan[$allcf]!="")
						{
							$mail = new PHPMailer();

							$mail->IsSMTP();                                      // set mailer to use SMTP
							$mail->Host = "smtp.gmail.com";  // specify main and backup server
							$mail->SMTPAuth = true;     // turn on SMTP authentication
							$mail->Username = "purifyart17@gmail.com";  // SMTP username
							$mail->Password = "purify1717"; // SMTP password
							$mail->SMTPSecure = "ssl";
							$mail->From = "purifyart17@gmail.com";
							//$mail->FromName = "Purify Art";
							//$mail->AddReplyTo("purifyart17@gmail.com", "Information");
							$mail->WordWrap = 50;                                 // set word wrap to 50 characters
							//$mail->AddAttachment("/var/tmp/file.tar.gz");         // add attachments
							//$mail->AddAttachment("/tmp/image.jpg", "new.jpg");    // optional name
							$mail->IsHTML(true);
							
							$strTo5 = $emails_com_fan[$allcf];
							//$mail->AddAddress($strTo5);
							//$mail->addBcc($strTo5);
							//echo $strTo5;
							if($ans_sub['name']!="" && isset($ans_sub['name']))
							{
								$strSubject5 = $ans_sub['name'].' Update';
								//$mail->FromName = 
								$forms_name = $ans_sub['name'];
								if(isset($ans_sub['artist_id']))
								{
									$dsteals = 'artist@'.$ans_sub['artist_id'];
								}
								elseif(isset($ans_sub['community_id']))
								{
									$dsteals = 'community@'.$ans_sub['community_id'];
								}
								
								$unsub_name = $ans_sub['name'];
							}
							else
							{
								$strSubject5 = $ans_sub['title'].' Update';
								//$mail->FromName = 
								$forms_name = $from_name;
								if(isset($ans_sub['artist_id']))
								{
									$dsteals = 'artist_project@'.$ans_sub['id'];
								}
								elseif(isset($ans_sub['community_id']))
								{
									$dsteals = 'community_project@'.$ans_sub['id'];
								}
								
								$unsub_name = $ans_sub['title'];
							}
							
							$dsteals = 'community_project@'.$_POST['project_value_text'];
							$profiles_detail = $newtab->get_projectdetails('community_project',$_POST['project_value_text']);
							$unsub_name = $profiles_detail['title'];
							
							$strMessage5 = $_POST['up_mess'];
							$strMessage5 .= "<html><body><br><a href=http://".$domainname."/$ans_sub[profile_url]>purifyart.com/$ans_sub[profile_url]</a><br><br></body></html>";
							$strHeader5 .= "from: Purify Art < no-reply@purifyart.com>"."\r\n";
							$strHeader5 .= "MIME-Version: 1.0\r\n";
							$strHeader5 .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
							//mail($strTo5,$strSubject5,$strMessage5,$strHeader5);
							/* $mail->Subject = $strSubject5;
							$mail->Body    = $strMessage5;
							if($mail->Send()){
								$sentmail = true;
							} */
							
							$mail_grid = new SendGrid\Mail();

							$mail_grid->addTo($strTo5)->
								   setFromName($unsub_name)->
									setFrom('no-reply@purifyart.com')->
								   setSubject($strSubject5)->
								   setHtml($strMessage5.'<div style="background-color: #FFFFFF; height: 5px; display:block;"></div>
	<div style="background-color: #cbcaca; height: 1px; display:block;"></div>
	<div style="background-color: #FFFFFF; height: 5px; display:block;"></div><font size="-1" color="#666666" face="Arial, Helvetica, sans-serif" style="font-size: 11px; color: #666666; font-family:  Geneva, Arial, Helvetica, sans-serif; font-weight: normal;">&#169;2014 Purify Art - <a style="color: black;" href="http://".$domainname."/example_adv.php?type_unsub=subscrb&alldt='.$dsteals.'">Unsubscribe to '.$unsub_name.'</a></font>');
							//$sendgrid->web->send($mail_grid);
							$sendgrid -> smtp -> send($mail_grid); 
						}
					}
				}
			}
		}
		//var_dump($sp_exp[0]);
		//var_dump($list_em);
		if($_POST['new_feed']=='yes')
		{
			//echo "ddd";
			$add_news = $promotes->add_news_feed($_POST['up_mess'],$scspl,$list_em);
			//var_dump($add_news);
		}
		if($_POST['facebook_up']=='yes')
		{
			//var_dump("azher");
		?>
			<script>
				//$("document").ready(function(){
				//	alert("hii");
				//});
				//alert("hii");
				//alert('#faces_'+<?php echo $sp_exp[2]; ?>+' span.stButton');
				//$("#faces_"+<?php echo $sp_exp[2]; ?>+" span.stButton").click();
				//window.parent.document.getElementById("post_face").value = 'yes|'+'<?php echo $sp_exp[2]; ?>';
				//parent.jQuery.fancybox.close();
			</script>
		<?php
		}
		elseif($_POST['facebook_up']=='no' || $_POST['facebook_up']=='')
		{
		?>
			<script>
				//alert("hii");
				//alert('#faces_'+<?php echo $sp_exp[2]; ?>+' span.stButton');
				//$("#faces_"+<?php echo $sp_exp[2]; ?>+" span.stButton").click();
				//window.parent.document.getElementById("post_face").value = 'no';
				//parent.jQuery.fancybox.close();
			</script>
		<?php
		}
		
		if($_POST['facebook_up']=='yes' || $_POST['new_feed']=='yes' || $_POST['email_face']=='yes' || $_POST['facebook_up']=='no' || $_POST['new_feed']=='no' || $_POST['email_face']=='no')
		{
		?>
			<script>
			//window.parent.document.getElementById("chk_vals_ups").value = '1';
			</script>
		<?php
		}
		else
		{
		?>
			<script>
			//window.parent.document.getElementById("chk_vals_ups").value = '0';
			</script>
		<?php
		}
		
		?>
		<script>
			//parent.jQuery.fancybox.close();
		</script>
		<?php
	}
	//$("#faces span.stButton").click()
?>

<style>
#at3winfooter { display: none !important; }
		#at3winssi { display: none !important; }
<!--.chk_Cont
{
	float: left;
    margin: 0 25px;
    padding-top: 5px;
}

#actualContent .fieldCont .chk_Cont .radioTitle {
    color: #000000;
    float: right;
    font-family: Arial,Helvetica,sans-serif;
    font-size: 12px;
}-->
</style>
