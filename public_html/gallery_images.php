<?php
include("commons/db.php");
include('classes/EditGallery.php');
$edit_gal=new EditGallery();
?>
<div class="fancy_header">
	Edit Gallery
</div>
<div class="whole_pop_content">
<form action="" id="edit_gallery_images_form" method="POST" >
<?php

$get_img=$edit_gal->Get_MediaImages($_GET['gal_id']);
//echo mysql_num_rows($get_img);
while($row=mysql_fetch_assoc($get_img))
{
?>
	<div class="image_block_pop"> 
		<div class="gallery_pop_text">
		Title  :  <input type="text" name="img_title<?php echo $row['id'];?>" id="img_title" value="<?php echo $row['image_title'];?>" />
		<input type="hidden" name="img_title_id[]" id="img_title_id" value="<?php echo $row['id'];?>" />
		</div>
		<div class="image_radio_pop">
			<!--<img src="uploads/Media/thumb/<?php //echo $row['image_name'];?>" width="" style="float:right;" />-->
			<img src="<?php if($row['gallery_id']==0){echo "http://medgalhighres.s3.amazonaws.com/";}else{echo "http://medgalthumb.s3.amazonaws.com/";}?><?php echo $row['image_name'];?>" width="200" height="200" style="float:right;" />
			<!--<input type="radio" value="<?php echo $row['image_name'];?>" <?php if($row['image_name']==$row['cover_pic']){echo "checked";}?> name="cover" />This is the album cover</br>-->
			<div class="gallery_pop_radio">
				<input type="checkbox" value="<?php echo $row['id'];?>" name="delete[]" />Delete this photo
			</div>
		</div>
	</div>
<?php
}
if(mysql_num_rows($get_img)==0)
{
	echo "No Images Found";
}
else
{
?>
<input type="submit" value=" Submit " class="round_black_button" />
<?php
}
?>
</form>
</div>
<script type="text/javascript">
	$("#edit_gallery_images_form").bind("submit",function(){
		$.ajax({
			type        : "POST",
			cache       : false,
			url :"gallery_images.php?Id=<?php echo $_GET['gal_id'];?>",
			data :$(this).serializeArray(),
			success :function(data){
						parent.jQuery.fancybox.close();
					}
		});
		return false;
	});
</script>
<?php
//var_dump($_POST);
if(isset($_POST) && $_POST!= null)
{
	$update_gal=$edit_gal->Update_GalleryImage($_POST['cover'],$_GET['Id']);
	if(isset($_POST['img_title_id']))
	{
		foreach($_POST['img_title_id'] as $img_id)
		{
			$id=$img_id;
			if($_POST['img_title'.$id]!="" || $_POST['img_title'.$id]!= null)
			{
				$edit_image_title=$edit_gal->Edit_Media_Images_Title($id,mysql_escape_string($_POST['img_title'.$id]));
			}
		}
	}
	if(isset($_POST['delete']))
	{
		foreach($_POST['delete'] as $id_to)
		{
			$id=$id_to;
			echo $id."</br>";
			$delete_images=$edit_gal->Delete_MediaImages($id);
		}
	}
	?>
	<!--<script type="text/javascript">
		parent.jQuery.fancybox.close();
	</script>-->
	<?php
}
?>