<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Purify Art: Help: Pricing</title>
<?php include("includes/header.php");?>
<div id="contentContainer" >
	<div class="wrapper">
		<div class="priceCont">
			<div class="pricetititle">
				Register for free. Premium services for only $25/year.<br /><span class="subtitle">Artists and Companies receive 95% of sales profits after Paypal fees which are 5% and $.50 per purchase. <a href="https://purifyart.zendesk.com/hc/en-us/articles/201015408-Fees-Associated-With-Selling-Through-Purify-Art" target="_blank">More</a></span>
			</div>
			<div class="topblk">
				<div class="titleCont">
					<div class="icon"><img src="images/icon-user.png" /></div>
					<div class="title">Registered Users</div>
				</div>
				<div class="blkCont">
					<p><img src="images/bullet.png"><strong>Subscribe</strong> to posts and updates from Artists and Companies</p>
					<p><img src="images/bullet.png"><strong>Register as an Artist or Company</strong> to build pages, share media, and use our promotional tools</p>
					<p><img src="images/bullet.png"><strong>View</strong> profile statistics</p>
				</div>
			</div>
			<div class="divider"></div>
			<div class="topblk">
				<div class="titleCont">
					<div class="icon"><img src="images/icon-purchase.png" /></div>
					<div class="title">Purchase</div>
				</div>
				<div class="blkCont">
					<p><img src="images/bullet.png"><strong>Purify Memberships</strong> for free downloads, custom playlists, and selling tools for Artists and Companies. ($25/year)</p>
					<p><img src="images/bullet.png"><strong>Media and Fan Club Memberships</strong> from Artists and Companies on our network</p>
				</div>
			</div>
			<div class="divider"></div>
			<div class="topblk">
				<div class="titleCont">
					<div class="icon"><img src="images/icon-artist.png" /></div>
					<div class="title">Artists and Companies</div>
				</div>
				<div class="blkCont">
					<p><img src="images/bullet.png"><strong>Sell Music</strong> and receive 95% of profits</p>
					<p><img src="images/bullet.png"><strong>Share Free Music</strong> and receive tips on free downloads and purchases</p>
					<p><img src="images/bullet.png"><strong>Distribute and Publish</strong> songs, albums, and videos with no per song/album/video fees. Learn More</p>
				</div>
			</div>
			
			<div class="pricetable">
				<div class="titlrow">
					<div class="colA">&nbsp;</div>
					<div class="colB">NON REGISTERED<br /><span class="sub">Free</span></div>
					<div class="colC">REGISTERED<br /><span class="sub">Free</span></div>
					<div class="colD">Member<br /><span class="sub">$25/year</span></div>
					<div class="colE">ARTIST/COMPANY<br /><span class="sub">Free</span></div>
					<div class="colF">ARTIST/COMPANY MEMBERS<br /><span class="sub">$25/year</span></div>
				</div>
				<div class="tablerow">
					<div class="colA">View Profiles and Demo</div>
					<div class="colB"><div class="circle"></div></div>
					<div class="colC"><div class="circle"></div></div>
					<div class="colD"><div class="circle"></div></div>
					<div class="colE"><div class="circle"></div></div>
					<div class="colF"><div class="circle"></div></div>
				</div>
				<div class="tablerowB">
					<div class="colA">Search Engine</div>
					<div class="colB"><div class="circle"></div></div>
					<div class="colC"><div class="circle"></div></div>
					<div class="colD"><div class="circle"></div></div>
					<div class="colE"><div class="circle"></div></div>
					<div class="colF"><div class="circle"></div></div>
				</div>
				<div class="tablerow">
					<div class="colA">View Media</div>
					<div class="colB"><div class="circle"></div></div>
					<div class="colC"><div class="circle"></div></div>
					<div class="colD"><div class="circle"></div></div>
					<div class="colE"><div class="circle"></div></div>
					<div class="colF"><div class="circle"></div></div>
				</div>
				<div class="tablerowB">
					<div class="colA">Download Free Media</div>
					<div class="colB"><div class="circle"></div></div>
					<div class="colC"><div class="circle"></div></div>
					<div class="colD"><div class="circle"></div></div>
					<div class="colE"><div class="circle"></div></div>
					<div class="colF"><div class="circle"></div></div>
				</div>
				<div class="tablerow">
					<div class="colA">Purchase Media</div>
					<div class="colB"><div class="circle"></div></div>
					<div class="colC"><div class="circle"></div></div>
					<div class="colD"><div class="circle"></div></div>
					<div class="colE"><div class="circle"></div></div>
					<div class="colF"><div class="circle"></div></div>
				</div>
				<div class="tablerowB">
					<div class="colA">View Profile Statistics</div>
					<div class="colB"></div>
					<div class="colC"><div class="circle"></div></div>
					<div class="colD"><div class="circle"></div></div>
					<div class="colE"><div class="circle"></div></div>
					<div class="colF"><div class="circle"></div></div>
				</div>
				<div class="tablerow">
					<div class="colA">Purchase Fan Club Memberships</div>
					<div class="colB"></div>
					<div class="colC"><div class="circle"></div></div>
					<div class="colD"><div class="circle"></div></div>
					<div class="colE"><div class="circle"></div></div>
					<div class="colF"><div class="circle"></div></div>
				</div>
				<div class="tablerowB">
					<div class="colA">News feed and email updates</div>
					<div class="colB"></div>
					<div class="colC"><div class="circle"></div></div>
					<div class="colD"><div class="circle"></div></div>
					<div class="colE"><div class="circle"></div></div>
					<div class="colF"><div class="circle"></div></div>
				</div>
				<div class="tablerow">
					<div class="colA">Register as Artist, Company, Team</div>
					<div class="colB"></div>
					<div class="colC"><div class="circle"></div></div>
					<div class="colD"><div class="circle"></div></div>
					<div class="colE"><div class="circle"></div></div>
					<div class="colF"><div class="circle"></div></div>
				</div>
				<div class="tablerowB">
					<div class="colA">Email Support</div>
					<div class="colB"></div>
					<div class="colC"></div>
					<div class="colD"><div class="circle"></div></div>
					<div class="colE"><div class="circle"></div></div>
					<div class="colF"><div class="circle"></div></div>
				</div>
				<div class="tablerow">
					<div class="colA">Create Playlists</div>
					<div class="colB"></div>
					<div class="colC"></div>
					<div class="colD"><div class="circle"></div></div>
					<div class="colE"><div class="circle"></div></div>
					<div class="colF"><div class="circle"></div></div>
				</div>
				<div class="tablerowB">
					<div class="colA">Artist/Company Profile</div>
					<div class="colB"></div>
					<div class="colC"></div>
					<div class="colD"></div>
					<div class="colE"><div class="circle"></div></div>
					<div class="colF"><div class="circle"></div></div>
				</div>
				<div class="tablerow">
					<div class="colA">Event and Project Profiles</div>
					<div class="colB"></div>
					<div class="colC"></div>
					<div class="colD"></div>
					<div class="colE"><div class="circle"></div></div>
					<div class="colF"><div class="circle"></div></div>
				</div>
				<div class="tablerowB">
					<div class="colA">Promote to subscribers & email list</div>
					<div class="colB"></div>
					<div class="colC"></div>
					<div class="colD"></div>
					<div class="colE"><div class="circle"></div></div>
					<div class="colF"><div class="circle"></div></div>
				</div>
				<div class="tablerow">
					<div class="colA">Facebook integration</div>
					<div class="colB"></div>
					<div class="colC"></div>
					<div class="colD"></div>
					<div class="colE"></div>
					<div class="colF"><div class="circle"></div></div>
				</div>
				<div class="tablerowB">
					<div class="colA">Sell Music</div>
					<div class="colB"></div>
					<div class="colC"></div>
					<div class="colD"></div>
					<div class="colE"></div>
					<div class="colF"><div class="circle"></div></div>
				</div>
				<div class="tablerow">
					<div class="colA">Sell Fan Club Memberships</div>
					<div class="colB"></div>
					<div class="colC"></div>
					<div class="colD"></div>
					<div class="colE"></div>
					<div class="colF"><div class="circle"></div></div>
				</div>
			</div>
		</div>
		<?php include_once("displayfooter.php"); ?>
	</div>
</div>	
<style>
	#miniFooter{ clear: both; float: left; }
	p#copyright { margin: 0; padding-top: 10px; }
</style>
</body>
</html>
