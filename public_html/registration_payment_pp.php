<?php 
	include_once('login_includes.php');
	include_once('classes/Subscription.php');
	
	$user_id=$_GET['uid'];
	$subscription_id=$_GET['sid'];
	
	$obj=new Subscription();
	$row=$obj->getSubscriptionById($subscription_id);
	$subscription=$row['name'];
	$desc=$row['desc'];
	$cost=$row['cost'];

?>

<?php include_once('includes/header.php'); ?>
<title>Purify Entertainment: Six Feature</title>

  <div id="contentContainer">
    <h1>Purify Membership</h1>
    <p><a href="register.php">Back to All User Sign Up Page</a></p>
    <table width="100%" border="0">
      <tr>
        <td width="83%"><p>Confirm your membership below  using PayPal.</p></td>
        <td width="17%"></td>
      </tr>
    </table>
  </div>
	
  <h5 class="withLine">Your Membership:</h5>
    <div id="featuredHeads">
      <div class="featuredItemStackMid">
      <div id="featureHead"></div><div class="featuredItemLeftMid">
      <div class="featuredContentWide">
        <h3><?php echo $subscription; ?> Membership</h3>
        <p><?php echo $desc; ?><br /><a>USD <?php echo $cost; ?>.00</a></p>
      </div>
      <div class="clearMe"></div>
    </div>
    <div class="featuredItemRightMid">
      <div class="featuredButton">
      <?php switch($subscription_id) 
	  		{
				case 1:
	  ?>
		<form action="https://www.paypal.com/cgi-bin/webscr" method="post">
        <input type="hidden" name="cmd" value="_s-xclick">
        <input type="hidden" name="hosted_button_id" value="3644633">
        <input type="image" src="https://www.paypal.com/en_US/i/btn/btn_buynow_SM.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">
        <img alt="" border="0" src="https://www.paypal.com/en_US/i/scr/pixel.gif" width="1" height="1">
        </form>
      <?php
	   					break;
				case 2: 
	   ?>
		<form action="https://www.paypal.com/cgi-bin/webscr" method="post">
        <input type="hidden" name="cmd" value="_s-xclick">
        <input type="hidden" name="hosted_button_id" value="3644602">
        <input type="image" src="https://www.paypal.com/en_US/i/btn/btn_buynow_SM.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">
        <img alt="" border="0" src="https://www.paypal.com/en_US/i/scr/pixel.gif" width="1" height="1">
        </form>
       <?php
	   					break;
				case 3:
		?>
        
        <?php
		/* 
        Just remove the commenting & echo statement to make  this option work.
		
        <form action="https://www.paypal.com/cgi-bin/webscr" method="post">
        <input type="hidden" name="cmd" value="_s-xclick">
        <input type="hidden" name="hosted_button_id" value="3644653">
        <input type="image" src="https://www.paypal.com/en_US/i/btn/btn_buynow_SM.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">
        <img alt="" border="0" src="https://www.paypal.com/en_US/i/scr/pixel.gif" width="1" height="1">
        </form>
		*/
			echo "<div class='hint'>This subscription is currently not available.</div>";
		?>
        
		<?php
						break;
				default:echo "Error! No subscription selected.";
						break; 
			}
	   ?>
      </div>
      <div class="clearMe"></div>
    </div>
</div>

<?php include_once('includes/footer.php'); ?>