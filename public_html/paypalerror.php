<?php
	if(session_id() == '') {
    session_start();
    }    
    
    include_once('includes/header.php');

    ?>
    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "//www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="//www.w3.org/1999/xhtml" xml:lang="en" lang="en">
    <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Purify Art Paypal Error</title>
    <link rel="stylesheet" type="text/css" media="screen" href="includes/purify.css" />
    </head>
    <body>
    <div id="outerContainer">
    
    <p>&nbsp;</p>
    <h2>Your paypal transaction had an error and did not complete.</h2>
    <p>&nbsp;</p>
    <p style="color: #000000; font-size: 14px;font-family: Arial, Helvetica, sans-serif;">
    Please contact the site administrator to process your payment.</p>
    <p>&nbsp;</p>
    <p style="color: #000000; font-size: 14px;font-family: Arial, Helvetica, sans-serif;">
    If you have any questions contact sales@purifyart.net.
    </p>
    <p>&nbsp;</p>
    <p>&nbsp;</p>
    
<?php
    include_once("displayfooter.php"); 
?>
</body>
</html>