
var playlist = [];
var playlist_clip = 0;
var playlist_shuffle = false;
var playlist_repeat = false;
var addplay = '';
function getUid()
{
    var url = window.location.toString();
    var params = url.split("&");
    var split = params[1].split("=");
    var uid = split[1];
    return uid;
}

$f("player", "js/flowplayer.commercial-3.2.6.swf", {
        key: '#$d2f91ffaa69a3bd3c15',
	clip: {
		autoPlay: true,
		autoBuffering: true,
                baseUrl: 'http://purifyentertainment.net/purify/Design',
		//baseUrl: 'http://localhost:8080/purify/Design',
//		baseUrl: 'http://www.purify.remotedataexchange.com/Design',
		onFinish: function() {
			setTimeout('player_next()',11);
		}
	},
        playlist: [
                        {
                            url:'uploads/mp3s/placeholder.mp3',
                            title:' '
                        }
                    ],
//        plugins: { audio: { url: 'js/flowplayer.audio-3.2.1.swf' } },
        onLoad: function() {
		refresh_playlist();
	}
});

function getUid()
{
    var url = window.location.toString();
    var params = url.split("&");
    var split = params[1].split("=");
    var uid = split[1];
    return uid;
}

var indexofsong = 0;
function add_clip(id)
{
	old_len = playlist.length;
        indexofsong  = old_len + 1;
	$.ajax({
	    type: 'GET',
	    url: 'playlist.php?getfile=playlist'+getUid()+'.xml',
	    dataType: "text",
	    success: function(response) {
                        response = load_xml_content_string(response);
			old_len = playlist.length;
        
			playlist.push ({
				url: $("song[id="+id+"] > url", response).text(),
				title: $("song[id="+id+"] > title", response).text(),
                                coverImage: $("song[id="+id+"] > profile_image", response).text(),
				profileurl: $("song[id="+id+"] > home_page", response).text(),
				username: $("song[id="+id+"] > username", response).text()
                                
			});
                    $f().addClip({url:playlist[playlist.length - 1]['url'], title:playlist[playlist.length - 1]['title'], coverImage:playlist[playlist.length - 1]['profile_image']},0)
                    refresh_playlist(old_len);

			
	    }
	});
        return indexofsong;
        
}
function load_xml_content_string(xmlData) {
		if (window.ActiveXObject) {
			//for IE
			xmlDoc=new ActiveXObject("Microsoft.XMLDOM");
			xmlDoc.async="false";
			xmlDoc.loadXML(xmlData);
			return xmlDoc;
		} else if (document.implementation && document.implementation.createDocument) {
			//for Mozila
			parser=new DOMParser();
			xmlDoc=parser.parseFromString(xmlData,"text/xml");
			return xmlDoc;
		}
	}

function load_existing_playlist(userid)
{
    var url = window.location.toString();
//    var params = url.split("&");
//    var split = params[1].split("=");
//    var uid = split[1];

    $.ajax({
	    type: 'GET',
	    url: 'playlist.php?getplaylist=1&uid='+getUid(),
	    dataType: "text",
	    success: function(response) {
			 arrSongids = response.toString().split("|");
                        var i = 0;
                        for(i=0; i<arrSongids.length-1; i++)
                        {
                            add_clip(arrSongids[i]);
                        }
                        play_clip(0);

	    }
	});
        
}

function play_clip(id)
{
	$("#playlist_clip_" + playlist_clip).removeClass('playlist_active');
	$f().play(playlist[id]);
	playlist_clip = id;
	$("#playlist_clip_" + playlist_clip).addClass('playlist_active');
}

function refresh_playlist(old_len)
{
	html = '';
	for(var i=0;i<playlist.length;i++){
		html = html
                         + '<div class="playlistitem" id="playlist_clip_'  + i + '">'
                         + '<a class="up" onClick="playlist_move(' + i + ',\'up\')"></a>'
			 + '<a class="down" onClick="playlist_move(' + i + ',\'down\')"></a>'
		 	 + '<a class="song"  onClick="play_clip(' + i + ')">' + playlist[i].title + '</a> <span style="float:left;font-size:11px; color:white">by</span> '
                         + '<a class="delete" onClick="delete_clip(' + i + ')"></a>'
		 	 + '<a class="download" href="' + playlist[i].url + '"></a>'
                         + '<a class="user" target="_blank" href="'+ playlist[i].profileurl +'" >' + playlist[i].username + '</a>'
                         + '</div>';
	}
	$("#playlist").html(html);
	$("#playlist_clip_" + playlist_clip).addClass('playlist_active');
}


var id_to_delete = 0;
function delete_clip(id)
{
    id_to_delete = id;
    
    refresh_playlist();

            $.ajax({
	    type: 'GET',
	    url: 'playlist.php?delete=yes&uid='+getUid()+'&url=' + playlist[id]['url'],
	    dataType: "text",
	    success: function(response) {
                playlist.splice(id_to_delete,1);
                if (playlist_clip == id_to_delete) {
                        playlist_clip = playlist_clip - 1;
                }
                refresh_playlist();

	    }
	});


        
	
}

Array.prototype.move = function(index, delta)
{
	var index2, temp_item;
	if (index < 0 || index >= this.length) {
		return false;
	}
	index2 = index + delta;
	if (index2 < 0 || index2 >= this.length || index2 == index) {
		return false;
	}
	temp_item = this[index2];
	this[index2] = this[index];
	this[index] = temp_item;
	return true;
}

function playlist_move(id, delta)
{
	if (delta == 'up' && id != 0) {
		playlist.move(id, -1);
		if (id == playlist_clip) {
			playlist_clip = id - 1;
		} else if (playlist_clip == id - 1) {
			playlist_clip = id;
		}
		refresh_playlist();
	} else if (delta == 'down' && id != (playlist.length - 1)) {
		playlist.move(id, 1);
		if (playlist_clip == id) {
			playlist_clip = id + 1;
		} else if (playlist_clip == id + 1) {
			playlist_clip = id;
		}
		refresh_playlist();
	}
}

function playlist_randomize()
{
	oldpl = playlist;
	newpl = [];
	while (oldpl.length > 0) {
		rand = Math.floor(Math.random() * oldpl.length);
		newpl.push(oldpl[rand]);
		oldpl.splice(rand,1);
	}
	playlist = newpl;
	play_clip(playlist_clip);
	refresh_playlist();
}

function playlist_clear(uid)
{
	playlist = [];
	refresh_playlist();
        
        $.ajax({
	    type: 'GET',
	    url: 'playlist.php?delete=all&uid='+uid,
	    dataType: "text",
	    success: function(response) {
               
	    }
	});
}

function player_next()
{
	if (playlist_shuffle == true) {
		rand = Math.floor(Math.random() * playlist.length);
		play_clip(rand);
	} else {
		if (playlist_clip != (playlist.length - 1)) {
			play_clip(playlist_clip + 1);
		} else if (playlist_clip==(playlist.length-1) && playlist_repeat==true) {
			play_clip(0);
		}
	}
}

function player_previous()
{
	if (playlist_shuffle == true) {
		rand = Math.floor(Math.random() * playlist.length);
		play_clip(rand);
	} else {
		if (playlist_clip != 0) {
			play_clip(playlist_clip - 1);
		} else if (playlist_clip == 0 && playlist_repeat == true) {
			play_clip(playlist.length - 1);
		}
	}
}

function player_repeat()
{
	if (playlist_repeat == false) {
		playlist_repeat = true;
		$("#repeat").html('<img src="js/repeat_on.gif" width="20" height="20"/>');
		if (playlist_shuffle == true) {
			playlist_shuffle = false;
			$("#shuffle").html('<img src="js/shuffle.gif" width="20" height="20"/>');
		}
	} else {
		playlist_repeat = false;
		$("#repeat").html('<img src="js/repeat.gif" width="20" height="20"/>');
	}
}

function player_shuffle()
{
	if (playlist_shuffle == false) {
		playlist_shuffle = true;
		$("#shuffle").html('<img src="js/shuffle_on.gif" width="20" height="20"/>');
		if (playlist_repeat == true) {
			playlist_repeat = false;
			$("#repeat").html('<img src="js/repeat.gif" width="20" height="20"/>');
		}
	} else {
		playlist_shuffle = false;
		$("#shuffle").html('<img src="js/shuffle.gif" width="20" height="20"/>');
	}
}




