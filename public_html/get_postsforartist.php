<?php
$result = array();
$errorflag='no';

$errormsg=array();
$inputparams=array();
$username='nouser';
$userpass='nopass';
$profileurl='leerick';
$format='json';

$data = file_get_contents("php://input");
$dataary=json_decode($data,true);

if (isset($dataary['format'])){    
    $format = strtolower($dataary['format']);
}
if(isset($dataary['profileurl'])) {
    $profileurl=$dataary['profileurl'];
}else {
    $errormessage='missing profile';
    $errormsg[]=array('errormsg'=>$errormessage);
    $result[] = array('error'=>$errormsg);
    $inputparams[] = array('profileurl'=>$profileurl);
    $inputparams[] = array('format'=>$format);
    $inputparams[] = array('post'=>$dataary);
    $result[] = array('inputparams'=>$inputparams);
    $errorflag='yes';
}

if ($errorflag='no'){
    include_once("commons/db.php");
    include_once("classes/ViewArtistProfileURL.php");
    include_once("classes/PersonalWallDisplayFeeds.php");
    include_once('classes/ProfileDisplay.php');
   
    $display = new ProfileDisplay();
    $artist_check = $display->artistCheck($profileurl);
    //echo '<br> artist id = '.$artist_check['artist_id'].' for profileurl= '.$profileurl.'<br>';
    $newPersonalWallDisplayFeeds = new PersonalWallDisplayFeeds();
    $new_profile_class_obj = new ViewArtistProfileURL();
    $getgeneral =  $new_profile_class_obj->get_user_info($artist_check['artist_id']);
    //echo '<br> general user id = '.$getgeneral['general_user_id'].' for artist id= '.$artist_check['artist_id'].'<br>';
      
    $get_posts_for_profileurl =$newPersonalWallDisplayFeeds->myposts($getgeneral['general_user_id']);   

    $get_all_posts_for_profileurl = array();
	if($get_posts_for_profileurl!="" && $get_posts_for_profileurl!=Null)
	{
		if(mysql_num_rows($get_posts_for_profileurl)>0)
		{
            while($row = mysql_fetch_assoc($get_posts_for_profileurl))
	        {
		        //print_r($row);
                $get_all_posts_for_profileurl[] = $row;
	        }
        }
    }
    
    $result[] = array('postsforprofileurl'=>$get_all_posts_for_profileurl);
    $result[] = array('generaluser'=>$getgeneral);


} //end of if error == no

/* output in necessary format */
if($format == 'json') {
    header('Content-type: application/json');
    echo json_encode($result);
    //echo json_encode(array('artistprofile'=>$result));
    //echo array_to_json($result);
}
else {
    if ($format == 'xml'){
        header('Content-type: text/xml');
        echo '<result>';
        foreach($result as $index => $resultentry) {
            if(is_array($resultentry)) {
                foreach($resultentry as $key => $value) {
                    echo '<',$key,'>';
                    if(is_array($value)) {
                        foreach($value as $tag => $val) {
                            echo '<',$tag,'>',htmlentities($val),'</',$tag,'>';
                        }
                    }
                    echo '</',$key,'>';
                }
            }
        }
        echo '</result>';
    }
}

function array_to_json( $array ){

    if( !is_array( $array ) ){
        return false;
    }

    $associative = count( array_diff( array_keys($array), array_keys( array_keys( $array )) ));
    if( $associative ){

        $construct = array();
        foreach( $array as $key => $value ){

            // We first copy each key/value pair into a staging array,
            // formatting each key and value properly as we go.

            // Format the key:
            if( is_numeric($key) ){
                $key = "key_$key";
            }
            $key = "\"".addslashes($key)."\"";

            // Format the value:
            if( is_array( $value )){
                $value = array_to_json( $value );
            } else if( !is_numeric( $value ) || is_string( $value ) ){
                $value = "\"".addslashes($value)."\"";
            }

            // Add to staging array:
            $construct[] = "$key: $value";
        }

        // Then we collapse the staging array into the JSON form:
        $result = "{ " . implode( ", ", $construct ) . " }";

    } else { // If the array is a vector (not associative):

        $construct = array();
        foreach( $array as $value ){

            // Format the value:
            if( is_array( $value )){
                $value = array_to_json( $value );
            } else if( !is_numeric( $value ) || is_string( $value ) ){
                $value = "'".addslashes($value)."'";
            }

            // Add to staging array:
            $construct[] = $value;
        }

        // Then we collapse the staging array into the JSON form:
        $result = "[ " . implode( ", ", $construct ) . " ]";
    }
	
    return $result;
}

?>