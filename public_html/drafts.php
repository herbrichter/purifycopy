<?php
	session_start();
	ob_start();
	include_once('commons/db.php');
	include_once('loggedin_includes.php');
	include_once('classes/ProfileType.php');
	include_once('classes/Commontabs.php');
	include_once('classes/AddContact.php');
	include_once('classes/GeneralInfo.php');
	include_once('classes/Mails.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Purify Art</title>
<?php 
$newtab=new Commontabs();
include("header.php");
?>
<div id="outerContainer">
<script src="includes/jquery.js"></script>
   
       <!-- <p>
        <a href="../logout.php">Logout</a></p>
</div>
    </div>
  </div>
  </div>-->
  <link rel="stylesheet" type="text/css" media="screen" href="includes/purify.css" />
<!--<script src="http://jqueryjs.googlecode.com/files/jquery-1.2.6.min.js" type="text/javascript"></script>-->
<script src="includes/popup.js" type="text/javascript"></script>
<!--<script src='http://ajax.googleapis.com/ajax/libs/jquery/1.6.1/jquery.min.js'></script>-->
<script src="includes/organictabs-jquery.js"></script>
<script>
	$(function() {

		$("#personalTab").organicTabs();
	});
</script>

<script>
var timeout	= 500;
var closetimer	= 0;
var ddmenuitem	= 0;

// open hidden layer
function mopen(id)
{	
	// cancel close timer
	mcancelclosetime();

	// close old layer
	if(ddmenuitem) ddmenuitem.style.visibility = 'hidden';

	// get new layer and show it
	ddmenuitem = document.getElementById(id);
	ddmenuitem.style.visibility = 'visible';

}
// close showed layer
function mclose()
{
	if(ddmenuitem) ddmenuitem.style.visibility = 'hidden';
}

// go close timer
function mclosetime()
{
	closetimer = window.setTimeout(mclose, timeout);
}

// cancel close timer
function mcancelclosetime()
{
	if(closetimer)
	{
		window.clearTimeout(closetimer);
		closetimer = null;
	}
}

// close layer when click-out
document.onclick = mclose; 
</script>
<script>
var ContentHeight = 700;
var TimeToSlide = 250.0;

var openAccordion = '';

function runAccordion(index)
{
  var nID = "Accordion" + index + "Content";
  if(openAccordion == nID)
    nID = '';
    
  setTimeout("animate(" + new Date().getTime() + "," + TimeToSlide + ",'" + openAccordion + "','" + nID + "')", 33);
  
  openAccordion = nID;
}

function animate(lastTick, timeLeft, closingId, openingId)
{  
  var curTick = new Date().getTime();
  var elapsedTicks = curTick - lastTick;
  
  var opening = (openingId == '') ? null : document.getElementById(openingId);
  var closing = (closingId == '') ? null : document.getElementById(closingId);
 
  if(timeLeft <= elapsedTicks)
  {
    if(opening != null)
      opening.style.height = ContentHeight + 'px';
    
    if(closing != null)
    {
      closing.style.display = 'none';
      closing.style.height = '0px';
    }
    return;
  }
 
  timeLeft -= elapsedTicks;
  var newClosedHeight = Math.round((timeLeft/TimeToSlide) * ContentHeight);

  if(opening != null)
  {
    if(opening.style.display != 'block')
      opening.style.display = 'block';
    opening.style.height = (ContentHeight - newClosedHeight) + 'px';
  }
  
  if(closing != null)
    closing.style.height = newClosedHeight + 'px';

  setTimeout("animate(" + curTick + "," + timeLeft +",'" + closingId + "','" + openingId + "')", 33);
}</script>
<div>
  <div id="toplevelNav"></div>
      <div id="profileTabs">
          <ul>
           <?php
		   $check=new AddContact();
		   
		   $new_artist=$newtab->artist_status();
		   $new_community=$newtab->community_status();
		   $new_team=$newtab->team_status();
		   
		   $fan_var = 0;
		   $member_var = 0;
		   $check=new AddContact();
		   
		   $new_artist=$newtab->artist_status();
		   $new_community=$newtab->community_status();
		   $new_team=$newtab->team_status();
		   $sql_fan_chk = mysql_query("SELECT * FROM fan_club_membership WHERE email='".$_SESSION['login_email']."'");
		   if(mysql_num_rows($sql_fan_chk)>0)
		   {
				while($row_fan_chk = mysql_fetch_assoc($sql_fan_chk))
				{
					$fan_var = $fan_var + 1;
				}
		   }
		   $gen_det = mysql_query("SELECT * FROM general_user WHERE email='".$_SESSION['login_email']."'");
		   if(mysql_num_rows($gen_det)>0){
			$res_gen_det = mysql_fetch_assoc($gen_det);
		   }
		   $date = date("Y-m-d");
			$sql_member_chk = mysql_query("SELECT * FROM purify_membership WHERE general_user_id='".$res_gen_det['general_user_id']."' AND (expiry_date>'".$date."' OR lifetime=1)");
		   if(mysql_num_rows($sql_member_chk)>0)
		   {
				$member_var = $member_var + 1;
		   }
		   
		  // var_dump($newres1);
		  //echo $res1['artist_id'];
		  if(($newres1['artist_id']==0 && $new_artist['status']==1) && $newres1['member_id']==0 && ($newres1['community_id']==0 && $new_community['status']==1) && ($newres1['team_id']==0 && $new_team['status']==1))
		  {		  
		  ?>
            <li class="active">HOME</li>		
            <!--<li><a href="profileedit_support.php">SUPPORT</a></li>-->
		   <?php
		   }
		   elseif(($newres1['artist_id']!=0 && $new_artist['status']==0 && $new_artist['active']==0) && ($newres1['community_id']!=0 && $new_community['status']==0 && $new_community['active']==0) && ($newres1['team_id']!=0 && $new_team['status']==0))
		   {
		   ?>
		   
		   <li class="active">HOME</li>
		   <li><a href="profileedit_media.php">Media</a></li>
		   <li><a href="profileedit_artist.php">ARTIST</a></li>
		   <!--<li><a href="profileedit_community.php">COMPANY</a></li>-->
		   <li><a href="profileedit_community.php">COMPANY</a></li>
		   <li><a href="profileedit_team.php">TEAM</a></li>
           <!--<li><a href="profileedit_support.php">SUPPORT</a></li>-->
		   
		   <?php
		   }
		   elseif(($newres1['artist_id']!=0 && $new_artist['status']==0 && $new_artist['active']==0) && ($newres1['community_id']!=0 && $new_community['status']==0 && $new_community['active']==0))
		   {
		   ?>
		   <li class="active">HOME</li>	
		   <li><a href="profileedit_media.php">Media</a></li>		   
		   <li><a href="profileedit_artist.php">ARTIST</a></li>
		   <!--<li><a href="profileedit_community.php">COMPANY</a></li>-->
		   <li><a href="profileedit_community.php">COMPANY</a></li>
           <!--<li><a href="profileedit_support.php">SUPPORT</a></li>-->
		   <?php
		   }
		   elseif(($newres1['artist_id']!=0 && $new_artist['status']==0 && $new_artist['active']==0) && ($newres1['team_id']!=0 && $new_team['status']==0))
		   {
		   ?>
			<li class="active">HOME</li>	
			<li><a href="profileedit_media.php">Media</a></li>
		    <li><a href="profileedit_artist.php">ARTIST</a></li>
			<li><a href="profileedit_team.php">TEAM</a></li>
            <!--<li><a href="profileedit_support.php">SUPPORT</a></li>-->
		   <?php
		   }
		   elseif(($newres1['community_id']!=0 && $new_community['status']==0 && $new_community['active']==0) && ($newres1['team_id']!=0 && $new_team['status']==0))
		   {
		   ?>
		   <li class="active">HOME</li>
		   <li><a href="profileedit_media.php">Media</a></li>
		   <!--<li><a href="profileedit_community.php">COMPANY</a></li>-->
		   <li><a href="profileedit_community.php">COMPANY</a></li>
		   <li><a href="profileedit_team.php">TEAM</a></li>
           <!--<li><a href="profileedit_support.php">SUPPORT</a></li>-->
		   <?php
		   }
		   elseif($newres1['artist_id']!=0 && $new_artist['status']==0 && $new_artist['active']==0)
		   {?>
		    <li class="active">HOME</li>
			<li><a href="profileedit_media.php">Media</a></li>			
			<li><a href="profileedit_artist.php">ARTIST</a></li>
            <!--<li><a href="profileedit_support.php">SUPPORT</a></li>-->
		   <?php
		   }
		   elseif($newres1['member_id']!=0)
		   {
		   ?>
		   <?php
		   }
		   elseif($newres1['community_id']!=0 && $new_community['status']==0 && $new_community['active']==0)
		   {
		   ?>
		    <li class="active">HOME</li>	
			<li><a href="profileedit_media.php">Media</a></li>
			<!--<li>COMPANY</a></li>-->
			<li><a href="profileedit_community.php">COMPANY</a></li>
            <!--<li><a href="profileedit_support.php">SUPPORT</a></li>-->
		   <?php
		   }
		   elseif($newres1['team_id']!=0 && $new_team['status']==0)
		   {
		   ?>
		    <li class="active">HOME</li>
			<li><a href="profileedit_media.php">Media</a></li>
			<li><a href="profileedit_team.php">TEAM</a></li>
            <!--<li><a href="profileedit_support.php">SUPPORT</a></li>-->
`		   <?php
		   }
		   else
		   {
		   ?>
			<li class="active">HOME</li>		
            <!--<li><a href="profileedit_support.php">SUPPORT</a></li>-->
		   <?php
		   }
			?>
          </ul>
      </div>

          <div id="personalTab">
            <div id="subNavigation" class="tabs">
              <ul class="nav">
                
				<?php
					if(($newres1['artist_id']!=0 && $new_artist['status']==0 && $new_artist['active']==0) || ($newres1['community_id']!=0 && $new_community['status']==0 && $new_community['active']==0))
					{
				?>
						<li><a href="profileedit.php#addressbook">Addressbook</a></li>
				<?php
					}
				?>
				<li><a href="profileedit.php#Become_a_member">Become A Member</a></li>
                <li><a href="profileedit.php#events">Events</a></li>
				<?php
					if(($newres1['artist_id']!=0 && $new_artist['status']==0 && $new_artist['active']==0) || ($newres1['community_id']!=0 && $new_community['status']==0 && $new_community['active']==0))
					{
				?>
						<li><a href="profileedit.php#friends">Friends</a></li>
				<?php
					}
				?>
                <li><a href="profileedit.php#general">General Info </a></li>
				<?php
					if(($newres1['artist_id']!=0 && $new_artist['status']==0 && $new_artist['active']==0) || ($newres1['community_id']!=0 && $new_community['status']==0 && $new_community['active']==0))
					{
				?>
						<li><a href="compose.php">Mail</a></li>
				<?php
					}
				?>
                <li><a href="profileedit.php#mymemberships">My Memberships</a></li>
                <li><a href="profileedit.php" class="current">News Feeds</a></li>
				<?php
					if(($newres1['artist_id']!=0 && $new_artist['status']==0 && $new_artist['active']==0) || ($newres1['community_id']!=0 && $new_community['status']==0 && $new_community['active']==0))
					{
				?>
						<li><a href="profileedit.php#permissions">Permissions</a></li>
				<?php
					}
				?>
                <!--<li><a href="profileedit.php#points">Points</a></li>-->
                <li><a href="profileedit.php#registration">Registration</a></li>
                <li><a href="profileedit.php#statistics">Statistics</a></li>
                <li><a href="profileedit.php#subscrriptions">Subscriptions</a></li>
               <!-- <li><a href="profileedit.php#services">Sales Profile</a></li>-->
			   <li><a href="profileedit.php#chng_pass">Change Password</a></li>
			   <?php
					if((($newres1['artist_id']!=0 && $new_artist['status']==0 && $new_artist['active']==0) || ($newres1['community_id']!=0 && $new_community['status']==0 && $new_community['active']==0)) && $member_var!=0)
					{
				?>
						 <li><a href="profileedit.php#transactions">Transactions</a></li>
				<?php
					}
				?>
               
                
              </ul>
            </div>
            <div class="list-wrap">

                <div class="subTabs" id="mail">
                	<h1>Drafts</h1>
                    <div id="mediaContent">
                    	<div class="topLinks" style="border-bottom:1px dotted #999;">
                            <div class="links" style="float:left;">
                                <ul>
                                    <li><a href="compose.php">Compose</a> |</li>
                                    <!--<li><a href="profileedit.php#mail">Inbox</a> |</li>-->
                                    <li><a href="">Drafts</a> |</li>
                                    <li><a href="sent-mails.php">Sent Mails</a></li>
                                </ul>
                            </div>
                        </div>
                         <div class="titleCont">
                            <div class="blkA">To</div>
                            <div class="blkB">Subject</div>
                            <div class="blkM">Date</div>
                           
                        </div>
						<?php
							$draft_data_mail = new Mails();
							$draft = $draft_data_mail->draftmails($newres1['general_user_id']);
							//echo $draft;
							//die;
							$draft_run = mysql_query($draft);
							
							if(mysql_num_rows($draft_run)>0)
							{
								$draft_ans = array();
								while($row = mysql_fetch_assoc($draft_run))
								{
									$draft_ans[] = $row;
								}
								
								$sort = array();
								foreach($draft_ans as $k=>$v)
								{
									$sort['date'][$k] = $v['date'];
									//$sort['from'][$k] = $end_f;
									//$sort['track'][$k] = $v['track'];
									
								}
								//var_dump($sort);
								array_multisort($sort['date'], SORT_DESC, $draft_ans);
								
								for($m=0;$m<count($draft_ans);$m++)
								{
									$count_db_draft = explode('_',$draft_ans[$m]['related_mail_id']);
									if(isset($count_db_draft[1]) && $count_db_draft!=0)
									{
										$count_org_draft = $count_db_draft[1] + 1;
						?>
										<div class="tableCont">
										<?php
											if($draft_ans[$m]['attachments']=="")
											{
										?>
											<div class="blkA"><a href="mail.php?view_mail_draft=<?php echo $draft_ans[$m]['id']; ?>&check_attach_remove"><?php 
											
											if($draft_ans[$m]['users']=='custom_users')
											{
												echo "Custom";
											}
											elseif($draft_ans[$m]['users']=='all_users')
											{
												echo "All";
											}
											elseif($draft_ans[$m]['users']=='artist_users')
											{
												echo "Artist";
											}
											elseif($draft_ans[$m]['users']=='community_users')
											{
												echo "Community";
											}
											elseif($draft_ans[$m]['users']=='purifyart_fans')
											{
												echo "Fans";
											}
											echo ' ('.$count_org_draft.')'; ?></a></div>
											<div class="blkB"><a href="mail.php?view_mail_draft=<?php echo $draft_ans[$m]['id']; ?>&check_attach_remove"><?php if($draft_ans[$m]['subject']!=""){ echo $draft_ans[$m]['subject']; }else{ ?>&nbsp;<?php } ?></a></div>
											<div class="blkM"><a href="mail.php?view_mail_draft=<?php echo $draft_ans[$m]['id']; ?>&check_attach_remove"><?php echo date("Y-M-d", strtotime($draft_ans[$m]['date'])); ?></a></div>
											<div class="blkD"><a href="javascript:confirmDelete('drafts.php?delete=<?php echo $draft_ans[$m]['id']; ?>')">Delete</a></div>
										<?php
											}
											else
											{
										?>
											<div class="blkA"><a href="mail.php?view_mail_draft=<?php echo $draft_ans[$m]['id']; ?>"><?php 
											if($draft_ans[$m]['users']=='custom_users')
											{
												echo "Custom";
											}
											elseif($draft_ans[$m]['users']=='all_users')
											{
												echo "All";
											}
											elseif($draft_ans[$m]['users']=='artist_users')
											{
												echo "Artist";
											}
											elseif($draft_ans[$m]['users']=='community_users')
											{
												echo "Community";
											}
											elseif($draft_ans[$m]['users']=='purifyart_fans')
											{
												echo "Fans";
											}
											echo ' ('.$count_org_draft.')'; ?></a></div>
											<div class="blkB"><a href="mail.php?view_mail_draft=<?php echo $draft_ans[$m]['id']; ?>"><?php if($draft_ans[$m]['subject']!=""){ echo $draft_ans[$m]['subject']; }else{ ?>&nbsp;<?php } ?></a></div>
											<div class="blkM"><a href="mail.php?view_mail_draft=<?php echo $draft_ans[$m]['id']; ?>"><?php echo date("Y-M-d", strtotime($draft_ans[$m]['date'])); ?></a></div>
											<div class="blkD"><a href="javascript:confirmDelete('drafts.php?delete=<?php echo $draft_ans[$m]['id']; ?>')">Delete</a></div>
										<?php
											}
										?>
										</div>
						<?php
									}
									elseif(isset($count_db_draft) && $count_db_draft==0)
									{
						?>
										<div class="tableCont">
										<?php
											if($draft_ans[$m]['attachments']=="")
											{
										?>
											<div class="blkA"><a href="mail.php?view_mail_draft=<?php echo $draft_ans[$m]['id']; ?>&check_attach_remove"><?php echo $draft_ans[$m]['to']; ?></a></div>
											<div class="blkB"><a href="mail.php?view_mail_draft=<?php echo $draft_ans[$m]['id']; ?>&check_attach_remove"><?php if($draft_ans[$m]['subject']!=""){ echo $draft_ans[$m]['subject']; }else{ ?>&nbsp;<?php } ?></a></div>
											<div class="blkM"><a href="mail.php?view_mail_draft=<?php echo $draft_ans[$m]['id']; ?>&check_attach_remove"><?php echo date("Y-M-d", strtotime($draft_ans[$m]['date'])); ?></a></div>
											<div class="blkD"><a href="javascript:confirmDelete('drafts.php?delete=<?php echo $draft_ans[$m]['id']; ?>')">Delete</a></div>
										<?php
											}
											else
											{
										?>
											<div class="blkA"><a href="mail.php?view_mail_draft=<?php echo $draft_ans[$m]['id']; ?>"><?php echo $draft_ans[$m]['to']; ?></a></div>
											<div class="blkB"><a href="mail.php?view_mail_draft=<?php echo $draft_ans[$m]['id']; ?>"><?php if($draft_ans[$m]['subject']!=""){ echo $draft_ans[$m]['subject']; }else{ ?>&nbsp;<?php } ?></a></div>
											<div class="blkM"><a href="mail.php?view_mail_draft=<?php echo $draft_ans[$m]['id']; ?>"><?php echo date("Y-M-d", strtotime($draft_ans[$m]['date'])); ?></a></div>
											<div class="blkD"><a href="javascript:confirmDelete('drafts.php?delete=<?php echo $draft_ans[$m]['id']; ?>')">Delete</a></div>
										<?php
											}
										?>
										</div>
						<?php
									}
								}
							}
							
						?>
                    </div>
                    <!--<p><strong>Use the general Mail function from Cypress Groove with the following differences:</strong></p>
     				<p><strong>        1. 
        Send out all emails in regualar design format rather than the format used for Cypress Groove Emails. <br />
        2. &quot;Remove Members&quot; and &quot;Unregistered Contacts&quot; from the user categories<br />
        3. Add &quot;Out of Network&quot; to the user categories<br />
        4. Change &quot;Add Image&quot; field to &quot;Attachments&quot; field where user can select from original media that they have the right to share.<br />
        5. Add Compose, Drafts, Inbox, and Sent tabs on top of form<br />
      6. When a user mails a registered user the message will be sent to their Purify Inbox as well as the users email address.</strong></p>-->
      			</div>
                
                
                
               </div>
          </div>  
          
           
          
          
   </div>
</div>

  <!-- end of main container -->
</div><?php include_once("displayfooter.php"); ?>
<div id="popupContact">
		<a id="popupContactClose">x</a>
		<p id="contactArea">
			Only registered users can view media. 
		</p>
        <h1>Register <a href="index.php">Here</a>, It's Free!</h1><br />
        <p>Already Registered ? Login below.</p>
        <div class="formCont">
            <div class="formFieldCont">
        	<div class="fieldTitle">Username</div>
            <input type="text" class="textfield" />
        </div>
       		<div class="formFieldCont">
        	<div class="fieldTitle">Password</div>
            <input type="password" class="textfield" />
        </div>
        	<div class="formFieldCont">
        	<div class="fieldTitle"></div>
            <input type="image" class="login" src="images/profile/login.png" width="47" height="23" />
        </div>
        </div>
	</div>
<div id="backgroundPopup"></div>
</body>
</html>
<script>
function confirmDelete(delUrl) {
  if (confirm("Are you sure you want to delete.")) {
    alert("Mail is deleted successfully.");
	document.location = delUrl;
  }
}
</script>
<?php
	
	if(isset($_GET['delete']))
	{
		$count_draft_del = 0;
		$delete = $draft_data_mail->delete_mail($_GET['delete'],$count_draft_del);
		//var_dump($delete);
		//die;
		header("Location: drafts.php");
	}
	
?>