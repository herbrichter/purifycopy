<?php 
	ob_start();
	include_once('classes/UnregUser.php');
	include_once('classes/Artist.php');
	include_once('classes/UserType.php');
	include_once('classes/UserImg.php');
	include_once('classes/UserSubscription.php');
	include_once('classes/RegistrationEmail.php');
	include_once('classes/NotifyAdminEmail.php');
	include_once('login_includes.php');
	$parent=$_POST['member_type'];
?>
<style type="text/css" rel="stylesheet">
#cke_bottom_detail,.cke_bottom{display:none};
/*.cke_bottom{display:none};*/
</style>
<!--<link rel="stylesheet" type="text/css" media="screen" href="includes/multiple_select.css"/>
<script type="text/javascript" src="uploadify/swfobject.js"></script>
<script type="text/javascript" src="uploadify/jquery.uploadify.v2.1.4.min.js"></script>
<link href="uploadify/uploadify.css" rel="stylesheet"/>
<script type="text/javascript" src="/ckeditor/ckeditor.js"></script>-->

<script>

	window.onload = function(){ 
		$.post("State.php", { country_id:$("#countrySelect_team").val() },
			function(data)
			{
					//alert("Data Loaded: " + data);											
					$("#stateSelect_team").html(data);
			}
		);
	}

	$(document).ready(function(){			
	//Select State o change in values of Country
	$("#countrySelect_team").change(function (){
			//alert("hi");
			$.post("State.php", { country_id:$("#countrySelect_team").val() },
				function(data)
				{
				//alert("Data Loaded: " + data);											
					$("#stateSelect_team").html(data);
				});
		});										
	});
$(document).ready(function(){
//alert($("#countrySelect_artist").val());
$.post("State.php", { country_id:$("#countrySelect_team").val() },
function(data)
{
//alert("Data Loaded: " + data);           
$("#stateSelect_team").html(data);
});
});
</script>
<script type="text/javascript" language="javascript">

	function validate()
	{
		var email_val = /^([a-zA-Z0-9_.-])+@(([a-zA-Z0-9-])+.)+([a-zA-Z])+$/;
		
		if (CKEDITOR != undefined) {
   for (var instance in CKEDITOR.instances)
       CKEDITOR.instances[instance].updateElement();
}
		
		if($("#name").val()==''){
			alert("Please Enter Username.");
			return false;
		}else if($("#type-select option:selected").val()==0){
			alert("Please Select Type.");
			return false;			
		}else if($("#countrySelect_team option:selected").val()==0){
			alert("Please Select Country.");
			return false;			
		}else if($("#stateSelect_team option:selected").val()==0){
			alert("Please Select State.");
			return false;			
		}else if($("#city").val()==''){
			alert("Please Enter City Name.");
			return false;
		}else if($("#imgupload").val()==''){
			alert("Please Select Profile Image.");
			return false;
		}else if($("#bio").val()=='')
		{
			alert("Please enter Bio Details");
			return false;
		}else if($("#uaccept").attr("checked")!="checked")
		{
			alert("Please read and accept the Originality Agreement.");
			return false;
		}else if($("#accept").attr("checked")!="checked")
		{
			alert("Please read and accept the User Agreement & Privacy Policy.");
			return false;
		}
			
		return true;
}
</script>

	<!--<h1>Team Membership</h1>-->
<? $temp=$_POST['member_type']; ?>
 <form id="registration_form" name="registration_form" method="post" action="add_general_team.php" onsubmit="return validate();" enctype="multipart/form-data">
<div style="width:665px; float:left;">

      <div id="actualContent">
	  <!--onsubmit="return validate()">-->

			<div class="fieldCont">
				<div id="user_email_msg" class="hint" style="color:#FF0000">
				  <?php if(isset($error_flag)) echo $error_flag; ?>            
				</div>           
			</div>
		
			<div class="fieldCont">
			  <div class="fieldTitle">*Name</div>
			  <input name="name" type="text" class="fieldText" id="name" value="<?php if(isset($name)) echo $name; ?>" />
			  <div class="hint">Team Member Name</div>
			</div>
			<!--<div class="fieldCont">
				<?php //if(isset($msg2)) echo $msg2; ?>
			</div>-->
			<?php include_once('registration_block_type.php'); ?>
			</div>
			<!--<div class="fieldCont">
				<span class="hintp">&nbsp;&nbsp;&nbsp;&nbsp;Suggest a new type by emailing info@purifyart.com</span>			</div>-->
			<div class="fieldCont">
			  <div class="fieldTitle">*Country</div>
			  <select id='countrySelect_team' name='country'  class="dropdown">
					<option value="0" selected="selected">Select Country</option>					

			  <?php include('Country.php'); ?>
			  </select>
			</div>
			<div class="fieldCont">
			  <div class="fieldTitle">*State/Province</div>
			  <select id='stateSelect_team' name='state' class="dropdown"></select><script type="text/javascript">//initCountry('US'); </script>
			 <!-- <td><input name="state" type="text" class="regularField" id="state" value="<?php //echo $state_or_province; ?>" /></td>-->
			</div>
			<div class="fieldCont">
			  <div class="fieldTitle">*City</div>
			  <input name="city" type="text" class="fieldText" id="city" value="<?php if(isset($city)) echo $city; ?>" />
			</div>
			
			
			<div class="fieldCont">
			  <div class="fieldTitle">Homepage</div>
			  <input name="homepage" type="text" class="fieldText" id="homepage" value="<?php if(isset($homepage)) echo $homepage; ?>" />
			</div>          

			<div id="selected-types-hide" style="display:none;"></div>


        <!--<div class="fieldCont">
          <div title="You may crop and change your picture once your account is activated." class="fieldTitle">*Profile Picture</div>
          <input title="You may crop and change your picture once your account is activated." name="image" type="file" class="fieldText" id="imgupload" accept="image/*" />
          <div class="hintUpload">
			<span class="hintp">Upload a .jpg/.png/.gif image no larger than 10MB.</span></br>
		   <span class="hintp">For best results upload a square image of the resolution 100x100 or higher.</span> 
		  </div>
          
        </div>-->
	
		
        
        <div class="fieldCont">
			<div class="fieldTitle" title="You may edit your bio once your account is activated.">*Bio</div>
			 <textarea name="bio" title="You may edit your bio once your account is activated." class="fieldTextArea"  id="bio" rows="5" cols="25" /></textarea>
		</div>				
		<div class="fieldCont">
			<span class="hintp">Enter bio of team</span>
		</div>
		<script type="text/javascript">
		
			CKEDITOR.config.width=350;
			CKEDITOR.config.height=250;
			CKEDITOR.config.removePlugins = 'elementspath';
//			CKEDITOR.config.plugins = 'basicstyles,button,htmldataprocessor,toolbar,wysiwygarea';
			CKEDITOR.replace( 'bio' ,{
			
			skin : 'office2003',
			
			toolbar :
			[
				[ 'Bold', 'Italic', '-', 'NumberedList', 'BulletedList', '-', 'Link', 'Unlink' ],
				
			]
		});
			</script>
		<?php
				//CK Editor
/*	require_once('ckeditor/ckeditor.php');
 $CKEditor = new CKEditor();
 $CKEditor->editor("bio", );
	$CKeditor->Width = '250px';
$CKeditor->Height = '200px';*/
//	-------------------------------
		
		?>
		
				
				        <div class="fieldCont">
        	<div class="chkCont">
              <input id="uaccept" name="uaccept" type="checkbox" class="chk" validate="required:true" />
              <div class="chkTitle">I agree that the content uploaded is original and accept the <a href="about_report_copyright_infringement.php" target="_blank">Originality Agreement</a>.</div>
          	</div>
        </div>
         <div class="fieldCont">
        	<div class="chkCont">
              <input id="accept" name="accept" type="checkbox" class="chk" validate="required:true" />
              <div class="chkTitle">I accept and agree to the Purify Art <a href="about_agreements_user.php" target="_blank">User Agreement</a> and <a href="about_agreements_privacy.php" target="_blank">Privacy Policy</a>.</div>
           </div>
        </div>
        
        <div class="fieldCont">
          	<input type="submit" name="register_btn" id="register_btn" value=" Register " class="register" style="border-radius:5px; -webkit-border-radius:5px; -moz-border-radius:5px; -ms-border-radius:5px; -o-border-radius:5px;"   /> 
		</div> 
		<br/>
     </div>

<div style="float:right; width:155px;" id="selected-types" ></div>  
      </form>
	  </div>
<script>
var container = $('#container');
</script>
<script type="text/javascript">
window.onload = function () {
$(".list-wrap").css({"height":""});
}
</script>