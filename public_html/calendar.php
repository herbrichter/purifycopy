<?php 
	session_start();
	include_once('commons/db.php');
	include_once('classes/calendar_events.php');
	include_once('classes/ProfileeditCommunity.php');
	$newgeneral = new ProfileeditCommunity();
	$cal_eve = new calendar_events();
	
	// At line 2 of our calendar.php script, add the MySQL connection information:
	
	// Now we need to define "A DAY", which will be used later in the script:
	define("ADAY", (60*60*24));
	 
	// The rest of the script will stay the same until about line 82
	 
	if ((!isset($_POST['month'])) || (!isset($_POST['year']))) {
	    $nowArray = getdate();
	    $month = $nowArray['mon'];
	    $year = $nowArray['year'];
	} else {
	    $month = $_POST['month'];
	    $year = $_POST['year'];
	}
	$start = mktime(12,0,0,$month,1,$year);
	$firstDayArray = getdate($start);
	?>
	
	<!--<script type="text/javascript">
		!window.jQuery && document.write('<script src="jquery-1.4.3.min.js"><\/script>');
	</script>
	<script type="text/javascript" src="fancybox/jquery.fancybox-1.3.4.pack.js"></script>
	<link rel="stylesheet" type="text/css" href="fancybox/jquery.fancybox-1.3.4.css" media="screen" />-->
	
	<h5>Select a Month/Year</h5>
	<form method="post" action="<?php echo $_SERVER['PHP_SELF']."#events"; ?>" id="calendar_form">
	<select name="month" class="eventdrop" id ="month">
	<?php
	$months = Array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");
	 
	for ($x=1; $x<=count($months); $x++){
	    echo "<option value=\"$x\"";
	    if ($x == $month){
	        echo " selected";
	    }
	    echo ">".$months[$x-1]."</option>";
	}
	?>
	</select>
	<select name="year" class="dropdown" id ="year">
	<?php
	for ($x=1980; $x<=2020; $x++){
	    echo "<option";
	    if ($x == $year){
	        echo " selected";
	    }
	    echo ">$x</option>";
	}
	?>
	</select>
	<input type="submit" value="Go" class="button" style="color: rgb(255, 255, 255); font-family: 'futura-pt',sans-serif; font-size: 0.825em; font-weight: bold; padding: 6px 10px; float: left; margin-right: 10px; border: 0px none; background-color: rgb(0, 0, 0); border-radius:5px; -webkit-border-radius:5px; -moz-border-radius:5px; -ms-border-radius:5px; -o-border-radius:5px;" />
	<a href="javascript:void(0);" onclick="previous()"><div style ="background-image: url('images/calendericons.jpeg');background-position: 188px center;float: left;position: relative;width: 34px;height:33px">
	</div></a>
	<a href="javascript:void(0);" onclick="next()"><div style ="background-image: url('images/calendericons.jpeg');background-position: 37px center;float: left;position: relative;width: 34px;height:33px;">
	</div></a>
	</form>
	<br />
	<?php
	$days = Array("Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat");
	echo "<table border=\"1\" cellpadding=\"5\" style='width:580px;'><tr>\n";
	foreach ($days as $day) {
	    echo "<td style=\"background-color: #bbb; text-align: center; width: 14%\">
	          <strong>$day</strong></td>\n";
	}
	 
	for ($count=0; $count < (6*7); $count++) {
	    $dayArray = getdate($start);
	    if (($count % 7) == 0) {
	        if ($dayArray["mon"] != $month) {
	            break;
	        } else {
	            echo "</tr><tr>\n";
	        }
	    }
	    if ($count < $firstDayArray["wday"] || $dayArray["mon"] != $month) {
	        echo "<td> </td>\n";
	    } else {
			$mail=$_SESSION['login_email'];
			$sql_user=mysql_query("SELECT * FROM general_user WHERE email='".$_SESSION['login_email']."'");
			$sql_user_ans=mysql_fetch_assoc($sql_user);
			//var_dump($sql_user_ans);
			
			if($sql_user_ans['community_id']!=0 && $sql_user_ans['community_id']!="" && !empty($sql_user_ans['community_id']))
			{
				$chkEvent_sql_community = "SELECT * FROM community_event WHERE month(date) = '".$month."' AND dayofmonth(date) = '".$dayArray["mday"]."' AND year(date) = '".$year."' AND community_id='".$sql_user_ans['community_id']."' AND del_status=0 ORDER BY date";
				$chkEvent_res_community = mysql_query($chkEvent_sql_community);
				//$temp=mysql_fetch_assoc($chkEvent_res_community);
				//var_dump($temp);	
			}	
				$sql_event_community = mysql_query("SELECT * FROM community_event WHERE month(date) = '".$month."' AND dayofmonth(date) = '".$dayArray["mday"]."' AND year(date) = '".$year."' AND del_status=0 ORDER BY date");
				$com_event = Array();
				while($row = mysql_fetch_assoc($sql_event_community))
				{
					$exp = explode(",",$row['tagged_user_email']);
					for($exp_count=0;$exp_count<count($exp);$exp_count++)
					{
						if($exp[$exp_count]=="")
						{
							continue;
						}
						else
						{
							if($exp[$exp_count] == $_SESSION['login_email'])
							{
								$com_event[] = $row;
							}
						}
					}
				}
				
	$getat_ev = array();
	$getat_ev = $newgeneral->only_creator_calup($dayArray["mday"],$month,$year);	
	
	$getgve_rev = array();
	$getgve_rev = $newgeneral->only_creator_calrec($dayArray["mday"],$month,$year);
	
	$get_only2cre_rev = array();
	$get_only2cre_rev = $newgeneral->only_creator2_calrec($dayArray["mday"],$month,$year);
	
	$get_only2cre_ev = array();
	$get_only2cre_ev = $newgeneral->only_creator2ev_calup($dayArray["mday"],$month,$year);
	
	$tagged2_eves = $cal_eve->Tagged2_eves($_SESSION['login_id'],$dayArray["mday"],$month,$year);
	
	$dureps_ceves_4 = array_merge($get_only2cre_ev,$getat_ev,$getgve_rev,$get_only2cre_rev,$tagged2_eves);
	$reals = array();
	$real_ids = array();
	for($crets=0;$crets<count($dureps_ceves_4);$crets++)
	{
		if(isset($dureps_ceves_4[$crets]['artist_id']))
		{
			$dureps_ceves_4[$crets]['id'] = $dureps_ceves_4[$crets]['id'].'~art';
		}
		elseif(isset($dureps_ceves_4[$crets]['community_id']))
		{
			$dureps_ceves_4[$crets]['id'] = $dureps_ceves_4[$crets]['id'].'~com';
		}
		if(in_array($dureps_ceves_4[$crets]['id'],$reals))
		{
			$chkr_eves = 1;
		}
		else
		{
			$reals[] = $dureps_ceves_4[$crets]['id'];
			$real_ids[] = $dureps_ceves_4[$crets]['id'];
			$whos_ids[] = $dureps_ceves_4[$crets]['whos_event'];
		}
	}
			
			if($sql_user_ans['artist_id']!=0 && $sql_user_ans['artist_id']!="" && !empty($sql_user_ans['artist_id']))
			{
				$chkEvent_sql_artist = "SELECT * FROM artist_event WHERE month(date) = '".$month."' AND dayofmonth(date) = '".$dayArray["mday"]."' AND year(date) = '".$year."' AND artist_id='".$sql_user_ans['artist_id']."'  AND del_status=0 ORDER BY date";
				//echo $chkEvent_sql_artist;
				$chkEvent_res_artist = mysql_query($chkEvent_sql_artist);
				//$num_artist_ans=mysql_num_rows($chkEvent_res_artist);
				//var_dump($num_artist_ans);
			}	
				$sql_event_artist = mysql_query("SELECT * FROM artist_event WHERE month(date) = '".$month."' AND dayofmonth(date) = '".$dayArray["mday"]."' AND year(date) = '".$year."' AND del_status=0 ORDER BY date");
				//echo "SELECT title FROM artist_event WHERE month(date) = '".$month."' AND dayofmonth(date) = '".$dayArray["mday"]."' AND year(date) = '".$year."' AND del_status=0 ORDER BY date";
				$art_event = Array();
				while($row = mysql_fetch_assoc($sql_event_artist))
				{
					$exp = explode(",",$row['tagged_user_email']);
					for($exp_count=0;$exp_count<count($exp);$exp_count++)
					{
						if($exp[$exp_count]=="")
						{
							continue;
						}
						else
						{
							if($exp[$exp_count] == $_SESSION['login_email'])
							{
								$art_event[] = $row;
							}
						}
					}
				}
				
			$ans_nums = array();
			if($chkEvent_res_community!="")
			{
				if(mysql_num_rows($chkEvent_res_community)>0)
				{
					while($num_comm = mysql_fetch_assoc($chkEvent_res_community))
					{
						$ans_nums[] = $num_comm;
					}
				}
			}
			
			$comns_nums = array();
			if($chkEvent_res_artist!="")
			{
				if(mysql_num_rows($chkEvent_res_artist)>0)
				{
					while($num_atr = mysql_fetch_assoc($chkEvent_res_artist))
					{
						$comns_nums[] = $num_atr;
					}
				}
			}
			
			$owns_eves = array_merge($art_event,$com_event,$comns_nums,$ans_nums);
			for($cr=0;$cr<count($owns_eves);$cr++)
			{
				if(isset($owns_eves[$cr]['artist_id']))
				{
					$owns_eves[$cr]['id'] = $owns_eves[$cr]['id'].'~art';
					$real_ids[] = $owns_eves[$cr]['id'];
				}
				elseif(isset($owns_eves[$cr]['community_id']))
				{
					$owns_eves[$cr]['id'] = $owns_eves[$cr]['id'].'~com';
					$real_ids[] = $owns_eves[$cr]['id'];
				}
			}
			
			$var_is = "";
			$real_ids = array_unique($real_ids);
			
			$get_all_del_id = $cal_eve->get_deleted_project_id();
			$exp_del_id = explode(",",$get_all_del_id['deleted_event_id']);
			for($count_all=0;$count_all<count($real_ids);$count_all++)
			{
				if($real_ids[$count_all]!="" && $real_ids!="~art" && $real_ids!="~com")
				{
					for($count_del=0;$count_del<count($exp_del_id);$count_del++)
					{
						if($exp_del_id[$count_del]!="")
						{
							if($real_ids[$count_all] == $exp_del_id[$count_del])
							{
								$real_ids[$count_all] ="";
							}
						}
					}
				}
			}
			
			$org_count = 0;
			for($r_i=0;$r_i<count($real_ids);$r_i++)
			{
				if($real_ids[$r_i]!="" && $real_ids!="~art" && $real_ids!="~com")
				{
					$org_count = $org_count + 1;
					$var_is = $var_is.','.$real_ids[$r_i];
				}
			}
			for($r_i=0;$r_i<count($whos_ids);$r_i++)
			{
				$who_is = $who_is.','.$whos_ids[$r_i];
			}
			
	       // if (mysql_num_rows($chkEvent_res_community) > 0 || mysql_num_rows($chkEvent_res_artist) > 0 || count($art_event)>0 || count($com_event)>0 || count($reals)>0) {
		   if($org_count!=0)
			{
				$num_community = mysql_num_rows($chkEvent_res_community);
				$num_artist = mysql_num_rows($chkEvent_res_artist);
				$num_art_tag = count($art_event);
				$num_com_tag = count($com_event);
				$cret2_tag = count($reals);
				//$cret3_tag = count($dureps_ceves_1);
				
				//echo $num_artist;
	            $event_title = "<br/>";
				//$total=$num_community + $num_artist + $num_art_tag + $num_com_tag + $cret2_tag;
				/* if(count($real_ids)>0 && !empty($real_ids))
				{
					$total = count($real_ids);
				}
				else
				{
					$total = 0;
				} */
				
				if($org_count!=0)
				{
					if($org_count>1)
					{
						$event_title=$org_count." Events";
					}
					else
					{
						$event_title=$org_count." Event";
					}
				}
				//while ($ev = mysql_fetch_array($chkEvent_res)) {
	            //    $event_title .= stripslashes($ev["title"])."<br/>";
	           // }
	           // mysql_free_result($chkEvent_res);
	        } else {
	            $event_title = "";
	        }
			
			
			
	 
	       // echo "<td valign=\"top\"><a href='#events' onClick=\"eventWindow('event.php?m=".$month."&d=".$dayArray["mday"]."&y=$year');\">".$dayArray["mday"]."</a><br/>".$event_title."</td>\n";
			if($event_title=="")
			{
				echo "<td onMouseOver=this.style.background='#bbb' onMouseOut=this.style.background='#f0f0f0' valign='top' height='70' width='70' style='background:#f0f0f0;'>$dayArray[mday]<br/><br/>$event_title</td>";
			}
			else
			{
				echo "<td class='xyz' onClick=\"clicklink('$month','$year','$dayArray[mday]','$mail','$var_is','$who_is')\"  onMouseOut=this.style.background='lightgray' valign='top' height='70' width='70' style='background:lightgray;'>$dayArray[mday]<br/><br/>$event_title</td>";
			}
			
			unset($event_title);
					
	        $start += ADAY;
	    }
	}
	echo "</tr></table>";
	
	?>
	<a id="ShowEvent" class="fancybox fancybox.ajax"> </a>
 <script type="text/javascript">
 function clicklink(m,y,d,mail,vars,who)
 {
  /*alert(e);
  alert(y);
  alert(d);*/
  var a=document.getElementById("ShowEvent");
  a.href = "event.php?m=" + m +"&d=" + d + "&y="+y + "&mail=" + mail + "&vars=" + vars + "&whos_event=" + who;
  a.click();
 }
 
 function next()
 {
	var month = $("#month").val();
	var year ;
	if(month ==12){
		year = parseInt($("#year").val()) + parseInt(1);}
	else{
		year = $("#year").val();}
	$("#month").val(parseInt(month) + parseInt(1));
	$("#year").val(parseInt(year));
	$("#calendar_form").submit();
 }
 function previous()
 {
	var month = $("#month").val();
	var year ;
	if(month ==1){
		year = parseInt($("#year").val()) - parseInt(1);}
	else{
		year = $("#year").val();}
	if(month ==1){
	$("#month").val(12);
	}else{
	$("#month").val(parseInt(month) - parseInt(1));}
	$("#year").val(parseInt(year));
	$("#calendar_form").submit();
 }
 </script>
 <script type="text/javascript">
  $(document).ready(function() {
    $("#ShowEvent").fancybox({
			helpers : {
				media : {},
				buttons : {},
				title : null
			}
		});
  });
 </script>
 
 <style>
	.xyz
	{
		cursor:pointer;
	}
 </style>
	