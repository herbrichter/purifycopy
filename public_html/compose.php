<?php
	session_start();
	ob_start();
	include_once('commons/db.php');
	include_once('loggedin_includes.php');
	include_once('classes/ProfileType.php');
	include_once('classes/Commontabs.php');
	include_once('classes/AddContact.php');
	include_once('classes/GeneralInfo.php');
	include_once('classes/Mails.php');
	include_once('classes/GetFanClubMember.php');
	include_once('sendgrid/SendGrid_loader.php');

	$sendgrid = new SendGrid('','');
	$mails_objs = new Mails();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Purify Art</title>
<script src="includes/jquery.js" type="text/javascript"></script>
<?php 
	$newtab = new Commontabs();
	include("header.php");
?>
<div id="outerContainer">

<!--<p>
        <a href="../logout.php">Logout</a></p>
</div>
    </div>
  </div>
  </div>-->
 <!-- <link rel="stylesheet" type="text/css" media="screen" href="includes/purify.css" />
<script src="http://jqueryjs.googlecode.com/files/jquery-1.2.6.min.js" type="text/javascript"></script>-->
<script src="includes/popup.js" type="text/javascript"></script>
<!--<script src='http://ajax.googleapis.com/ajax/libs/jquery/1.6.1/jquery.min.js'></script>
<script src="includes/organictabs-jquery.js"></script>-->
<script>
	$(function() {

		$("#personalTab").organicTabs();
	});
</script>

<script type="text/javascript" src="ckeditor.js"></script>
<script type="text/javascript" src="jquery.js"></script>
<script src="sample.js" type="text/javascript"></script>
<link href="sample.css" rel="stylesheet" type="text/css" />


<script type="text/javascript">
	//<![CDATA[

$(function()
{
	var config = {
		toolbar:
		[
			['Bold', 'Italic', '-', 'NumberedList', 'BulletedList', '-', 'Link', 'Unlink'],
			['UIColor']
		]
	};

	// Initialize the editor.
	// Callback function can be passed and executed after full instance creation.
	$('.jquery_ckeditor').ckeditor(config);
});

	//]]>
	</script>	
<!--Ends here-->

<script>
var timeout	= 500;
var closetimer	= 0;
var ddmenuitem	= 0;

// open hidden layer
function mopen(id)
{	
	// cancel close timer
	mcancelclosetime();

	// close old layer
	if(ddmenuitem) ddmenuitem.style.visibility = 'hidden';

	// get new layer and show it
	ddmenuitem = document.getElementById(id);
	ddmenuitem.style.visibility = 'visible';

}
// close showed layer
function mclose()
{
	if(ddmenuitem) ddmenuitem.style.visibility = 'hidden';
}

// go close timer
function mclosetime()
{
	closetimer = window.setTimeout(mclose, timeout);
}

// cancel close timer
function mcancelclosetime()
{
	if(closetimer)
	{
		window.clearTimeout(closetimer);
		closetimer = null;
	}
}

// close layer when click-out
document.onclick = mclose; 
</script>
<script>
var ContentHeight = 700;
var TimeToSlide = 250.0;

var openAccordion = '';

function runAccordion(index)
{
  var nID = "Accordion" + index + "Content";
  if(openAccordion == nID)
    nID = '';
    
  setTimeout("animate(" + new Date().getTime() + "," + TimeToSlide + ",'" + openAccordion + "','" + nID + "')", 33);
  
  openAccordion = nID;
}

function animate(lastTick, timeLeft, closingId, openingId)
{  
  var curTick = new Date().getTime();
  var elapsedTicks = curTick - lastTick;
  
  var opening = (openingId == '') ? null : document.getElementById(openingId);
  var closing = (closingId == '') ? null : document.getElementById(closingId);
 
  if(timeLeft <= elapsedTicks)
  {
    if(opening != null)
      opening.style.height = ContentHeight + 'px';
    
    if(closing != null)
    {
      closing.style.display = 'none';
      closing.style.height = '0px';
    }
    return;
  }
 
  timeLeft -= elapsedTicks;
  var newClosedHeight = Math.round((timeLeft/TimeToSlide) * ContentHeight);

  if(opening != null)
  {
    if(opening.style.display != 'block')
      opening.style.display = 'block';
    opening.style.height = (ContentHeight - newClosedHeight) + 'px';
  }
  
  if(closing != null)
    closing.style.height = newClosedHeight + 'px';

  setTimeout("animate(" + curTick + "," + timeLeft +",'" + closingId + "','" + openingId + "')", 33);
}

$("document").ready(function(){

	// $("#selected_users").change(function() 
  // {
	//alert("sss");
		var selected = $("#selected_users").val();
		<?php
		if(isset($_GET['reply']))
		{
		?>
			var dataString = 'reg='+ selected+'&view='+<?php echo $_GET['reply']; ?>;
		<?php
		}
		elseif(isset($_GET['send_draft']))
		{
		?>
			var dataString = 'reg='+ selected+'&send_draft='+<?php echo $_GET['send_draft']; ?>;
		<?php
		}
		elseif(isset($_GET['send_sent']))
		{
		?>
			var dataString = 'reg='+ selected+'&send_sent='+<?php echo $_GET['send_sent']; ?>;
		<?php
		}
		else
		{
		?>
			var dataString = 'reg='+ selected;
		<?php
		}
		?>
		$("#loader").show();
		$("#fieldCont_users").hide();
		 $.ajax({	 
			
			type: 'POST',
			url : 'UsersMail.php',
			data: dataString,
			success: function(data) 
			{
				if(selected=='custom_users')
				{
					<?php
						if(isset($_GET['address_book_email']))
						{
					?>
							data = "<input type='text' name='mails_users[]' id='mails_users[]' class='fieldText' value=<?php echo $_GET['address_book_email']; ?> />";
							$("#fieldCont_users").html(data);
					<?php
						}
						else
						{
					?>
							$("#fieldCont_users").html(data);
					<?php
						}
					?>
				}
				else
				{
					$("#fieldCont_users").html(data);
				}
						$("#loader").hide();
						$("#fieldCont_users").show();
			}
		//});
	});
	
	$("#selected_users").change(function() 
    {
	//alert("sss");
		var selected = $("#selected_users").val();
		<?php
		if(isset($_GET['reply']))
		{
		?>
			var dataString = 'reg='+ selected+'&view='+<?php echo $_GET['reply']; ?>;
		<?php
		}
		elseif(isset($_GET['send_draft']))
		{
		?>
			var dataString = 'reg='+ selected+'&send_draft='+<?php echo $_GET['send_draft']; ?>;
		<?php
		}
		elseif(isset($_GET['send_sent']))
		{
		?>
			var dataString = 'reg='+ selected+'&send_sent='+<?php echo $_GET['send_sent']; ?>;
		<?php
		}
		else
		{
		?>
			var dataString = 'reg='+ selected;
		<?php
		}
		?>
		$("#loader").show();
		$("#fieldCont_users").hide();
		 $.ajax({	 
			
			type: 'POST',
			url : 'UsersMail.php',
			data: dataString,
			success: function(data) 
			{
				if(selected=='custom_users')
				{
					<?php
						if(isset($_GET['address_book_email']))
						{
					?>
							data = "<input type='text' name='mails_users[]' id='mails_users[]' class='fieldText' value=<?php echo $_GET['address_book_email']; ?> />";
							$("#fieldCont_users").html(data);
					<?php
						}
						else
						{
					?>
							$("#fieldCont_users").html(data);
					<?php
						}
					?>
				}
				else
				{
					$("#fieldCont_users").html(data);
				}
				$("#loader").hide();
				$("#fieldCont_users").show();
			}
		});
	});
});

</script>
  <div>
  <div id="toplevelNav"></div>
      <div id="profileTabs">
          <ul>
           <?php
			$get_fan_club= new GetFanClubMember();
			$get_gen_info = $get_fan_club->Get_loggedin_Info();
			$get_art_info = $get_fan_club->Get_Artist_Info_e($get_gen_info['artist_id']);
			$get_comm_info = $get_fan_club->Get_community_Info_e($get_gen_info['community_id']);

		   $check=new AddContact();
		   
		   $new_artist=$newtab->artist_status();
		   $new_community=$newtab->community_status();
		   $new_team=$newtab->team_status();
		   
		    $fan_var = 0;
		   $member_var = 0;
		   $check=new AddContact();
		   
		   $new_artist=$newtab->artist_status();
		   $new_community=$newtab->community_status();
		   $new_team=$newtab->team_status();
		   $sql_fan_chk = mysql_query("SELECT * FROM fan_club_membership WHERE email='".$_SESSION['login_email']."'");
		   if($sql_fan_chk!="")
		   {
			   if(mysql_num_rows($sql_fan_chk)>0)
			   {
					while($row_fan_chk = mysql_fetch_assoc($sql_fan_chk))
					{
						$fan_var = $fan_var + 1;
					}
			   }
			}
		   $gen_det = mysql_query("SELECT * FROM general_user WHERE email='".$_SESSION['login_email']."'");
			if($gen_det!="")
			{
			   if(mysql_num_rows($gen_det)>0){
				$res_gen_det = mysql_fetch_assoc($gen_det);
			   }
			}
		   $date = date("Y-m-d");
			$sql_member_chk = mysql_query("SELECT * FROM purify_membership WHERE general_user_id='".$res_gen_det['general_user_id']."' AND (expiry_date>'".$date."' OR lifetime=1)");
		   if(mysql_num_rows($sql_member_chk)>0)
		   {
				$member_var = $member_var + 1;
		   }
		   
		  // var_dump($newres1);
		  //echo $res1['artist_id'];
		  if(($newres1['artist_id']==0 && $new_artist['status']==1) && $newres1['member_id']==0 && ($newres1['community_id']==0 && $new_community['status']==1) && ($newres1['team_id']==0 && $new_team['status']==1))
		  {		  
		  ?>
            <li class="active">HOME</li>		
            <!--<li><a href="profileedit_support.php">SUPPORT</a></li>-->
		   <?php
		   }
		   elseif(($newres1['artist_id']!=0 && $new_artist['status']==0 && $new_artist['active']==0) && ($newres1['community_id']!=0 && $new_community['status']==0 && $new_community['active']==0) && ($newres1['team_id']!=0 && $new_team['status']==0))
		   {
		   ?>
		   
		   <li class="active">HOME</li>
		   <li><a href="profileedit_media.php">Media</a></li>
		   <li><a href="profileedit_artist.php">ARTIST</a></li>
		   <!--<li><a href="profileedit_community.php">COMPANY</a></li>-->
		   <li><a href="profileedit_community.php">COMPANY</a></li>
		   <li><a href="profileedit_team.php">TEAM</a></li>
          <!--<li><a href="profileedit_support.php">SUPPORT</a></li>-->
		   
		   <?php
		   }
		   elseif(($newres1['artist_id']!=0 && $new_artist['status']==0 && $new_artist['active']==0) && ($newres1['community_id']!=0 && $new_community['status']==0 && $new_community['active']==0))
		   {
		   ?>
		   <li class="active">HOME</li>	
		   <li><a href="profileedit_media.php">Media</a></li>		   
		   <li><a href="profileedit_artist.php">ARTIST</a></li>
		   <!--<li><a href="profileedit_community.php">COMPANY</a></li>-->
		   <li><a href="profileedit_community.php">COMPANY</a></li>
           <!--<li><a href="profileedit_support.php">SUPPORT</a></li>-->
		   <?php
		   }
		   elseif(($newres1['artist_id']!=0 && $new_artist['status']==0 && $new_artist['active']==0) && ($newres1['team_id']!=0 && $new_team['status']==0))
		   {
		   ?>
			<li class="active">HOME</li>	
			<li><a href="profileedit_media.php">Media</a></li>
		    <li><a href="profileedit_artist.php">ARTIST</a></li>
			<li><a href="profileedit_team.php">TEAM</a></li>
            <!--<li><a href="profileedit_support.php">SUPPORT</a></li>-->
		   <?php
		   }
		   elseif(($newres1['community_id']!=0 && $new_community['status']==0 && $new_community['active']==0) && ($newres1['team_id']!=0 && $new_team['status']==0))
		   {
		   ?>
		   <li class="active">HOME</li>
		   <li><a href="profileedit_media.php">Media</a></li>
		   <!--<li><a href="profileedit_community.php">COMPANY</a></li>-->
		   <li><a href="profileedit_community.php">COMPANY</a></li>
		   <li><a href="profileedit_team.php">TEAM</a></li>
           <!--<li><a href="profileedit_support.php">SUPPORT</a></li>-->
		   <?php
		   }
		   elseif($newres1['artist_id']!=0 && $new_artist['status']==0 && $new_artist['active']==0)
		   {?>
		    <li class="active">HOME</li>
			<li><a href="profileedit_media.php">Media</a></li>			
			<li><a href="profileedit_artist.php">ARTIST</a></li>
            <!--<li><a href="profileedit_support.php">SUPPORT</a></li>-->
		   <?php
		   }
		   elseif($newres1['member_id']!=0)
		   {
		   ?>
		   <?php
		   }
		   elseif($newres1['community_id']!=0 && $new_community['status']==0 && $new_community['active']==0)
		   {
		   ?>
		    <li class="active">HOME</li>	
			<li><a href="profileedit_media.php">Media</a></li>
			<!--<li>COMPANY</a></li>-->
			<li><a href="profileedit_community.php">COMPANY</a></li>
            <!--<li><a href="profileedit_support.php">SUPPORT</a></li>-->
		   <?php
		   }
		   elseif($newres1['team_id']!=0 && $new_team['status']==0)
		   {
		   ?>
		    <li class="active">HOME</li>
			<li><a href="profileedit_media.php">Media</a></li>
			<li><a href="profileedit_team.php">TEAM</a></li>
            <!--<li><a href="profileedit_support.php">SUPPORT</a></li>-->
`		   <?php
		   }
		   else
		   {
		   ?>
			 <li class="active">HOME</li>		
            <!--<li><a href="profileedit_support.php">SUPPORT</a></li>-->
		   <?php
		   }
			?>
          </ul>
      </div>

          <div id="personalTab">
            <div id="subNavigation" class="tabs">
              <ul class="nav">
                
				<?php
					if(($newres1['artist_id']!=0 && $new_artist['status']==0 && $new_artist['active']==0) || ($newres1['community_id']!=0 && $new_community['status']==0 && $new_community['active']==0))
					{
				?>
						<li><a href="profileedit.php#addressbook">Addressbook</a></li>
				<?php
					}
				?>
				<li><a href="profileedit.php#Become_a_member">Become A Member</a></li>
                <li><a href="profileedit.php#events">Events</a></li>
				<?php
					if(($newres1['artist_id']!=0 && $new_artist['status']==0 && $new_artist['active']==0) || ($newres1['community_id']!=0 && $new_community['status']==0 && $new_community['active']==0))
					{
				?>
						<li><a href="profileedit.php#friends">Friends</a></li>
				<?php
					}
				?>
                <li><a href="profileedit.php#general">General Info </a></li>
				<?php
					if(($newres1['artist_id']!=0 && $new_artist['status']==0 && $new_artist['active']==0) || ($newres1['community_id']!=0 && $new_community['status']==0 && $new_community['active']==0))
					{
				?>
						<li><a href="compose.php">Mail</a></li>
				<?php
					}
				?>
                <li><a href="profileedit.php#mymemberships">My Memberships</a></li>
                <li><a href="profileedit.php" class="current">News Feeds</a></li>
				<?php
					if(($newres1['artist_id']!=0 && $new_artist['status']==0 && $new_artist['active']==0) || ($newres1['community_id']!=0 && $new_community['status']==0 && $new_community['active']==0))
					{
				?>
						<li><a href="profileedit.php#permissions">Permissions</a></li>
				<?php
					}
				?>
                <!--<li><a href="profileedit.php#points">Points</a></li>-->
                <li><a href="profileedit.php#registration">Registration</a></li>
                <li><a href="profileedit.php#statistics">Statistics</a></li>
                <li><a href="profileedit.php#subscrriptions">Subscriptions</a></li>
                <!--<li><a href="profileedit.php#services">Sales Profile</a></li>-->
				<li><a href="profileedit.php#chng_pass">Change Password</a></li>
                <?php
					if((($newres1['artist_id']!=0 && $new_artist['status']==0 && $new_artist['active']==0) || ($newres1['community_id']!=0 && $new_community['status']==0 && $new_community['active']==0)) && $member_var!=0)
					{
				?>
						 <li><a href="profileedit.php#transactions">Transactions</a></li>
				<?php
					}
				?>
                
              </ul>
            </div>
            <div class="list-wrap">

                <div class="subTabs" id="mail">
                	<h1>Compose Mail</h1>
                    <div id="mediaContent">
                    	<div class="topLinks" style="border-bottom:1px dotted #999;">
                            <div class="links" style="float:left;">
                                <ul>
                                    <li><a href="compose.php">Compose</a> |</li>
                                    <!--<li><a href="profileedit.php#mail">Inbox</a> |</li>-->
                                    <li><a href="drafts.php">Drafts</a> |</li>
                                    <li><a href="sent-mails.php">Sent Mails</a></li>
                                </ul>
                            </div>
                        </div>
                        
                    </div>
                    <div id="actualContent">
					<form method="POST" name="mails" id="mails" enctype="multipart/form-data">
                    	<?php
							$data_mail = new Mails();
							if(isset($_GET['reply']))
							{
								$reply_sql = $data_mail->viewMail($_GET['reply']);
								$reply_run = mysql_query($reply_sql);
								$reply_ans = mysql_fetch_assoc($reply_run);
								//var_dump($reply_ans);
								
							?>
								<div class="fieldCont">
									<div class="fieldTitle" title="Select users to send mail to.">Select Users</div> 
									<select title="Select users to send mail to." class="dropdown" id="selected_users" name="selected_users">
									<?php
									
										$check_cust_all = $data_mail->checkCustom_All($newres1['general_user_id']);
										$chck_must = mysql_query($check_cust_all);
										if(mysql_num_rows($chck_must))
										{
									?>
											<option value="custom_users" <?php if($reply_ans['users']=='custom_users') { echo "selected"; } ?>>Custom</option>
											<option value="all_users" <?php if($reply_ans['users']=='all_users') { echo "selected"; } ?>>All</option>
											
											
											<?php
				if($get_art_info!= null && $get_art_info != "no")
				{
					$get_art_mem = $get_fan_club->art_subs();
					if($get_art_mem=="1")
					{
				?>
						<option value="artist_users" <?php if($reply_ans['users']=='artist_users') { echo "selected"; } ?>><?php echo $get_art_info['name']." Subscribers"; ?></option>
				<?php
					}
				}
				if($get_comm_info!= null && $get_comm_info != "no")
				{
					$get_com_mem = $get_fan_club->com_subs();
					if($get_com_mem=="1")
					{
				?>
						<option value="community_users" <?php if($reply_ans['users']=='community_users') { echo "selected"; } ?>><?php echo $get_comm_info['name']." Subscribers"; ?></option>
				<?php
					}
				}
											
											$chk_artist_fan = $data_mail->find_artist_fans();
											if($chk_artist_fan!=""){
												$get_artist_name = $data_mail->get_fanclub_name("artist");
													if($get_artist_name['type']=="fan_club_yes"){
											?>
												<option value="artist_fans" <?php if($reply_ans['users']=='artist_fans') { echo "selected"; } ?>><?php echo $get_artist_name['name'];?> Fan Club</option>
											<?php
												}
											}
											$chk_community_fan = $data_mail->find_community_fans();
											if($chk_community_fan!=""){
												$get_community_name = $data_mail->get_fanclub_name("community");
													if($get_community_name['type']=="fan_club_yes"){
											?>
											<option value="community_fans" <?php if($reply_ans['users']=='community_fans') { echo "selected"; } ?>><?php echo $get_community_name['name'];?> Fan Club</option>
											<?php
												}
											}
											$get_art_pro_fan = $data_mail->get_all_artist_projects();
											if($get_art_pro_fan!="")
											{
												while($art_pro = mysql_fetch_assoc($get_art_pro_fan))
												{
													$chkpro_fan_visible = $data_mail->find_projects_fans($art_pro['id'],"artist_project");
													if($chkpro_fan_visible !=""){
														if($art_pro['type']=="fan_club"){
													?>
													<option value="artist_project_fans" <?php if($reply_ans['users']=='artist_project_fans') { echo "selected"; } ?>><?php echo $art_pro['title'];?> Fan Club</option>
													<?php
														}
													}
												}
											}
											$get_com_pro_fan = $data_mail->get_all_community_projects();
											if($get_com_pro_fan!="")
											{
												while($com_pro = mysql_fetch_assoc($get_com_pro_fan))
												{
													$chkpro_fan_visible = $data_mail->find_projects_fans($com_pro['id'],"community_project");
													if($chkpro_fan_visible !=""){
														if($com_pro['type']=="fan_club"){
													?>
													<option value="community_project_fans" <?php if($reply_ans['users']=='community_project_fans') { echo "selected"; } ?>><?php echo $com_pro['title'];?> Fan Club</option>
													<?php
														}
													}
												}
											}
										}
										else
										{
									?>
											<option value="custom_users" <?php echo "selected"; ?>>Custom</option>
											<option value="all_users">All</option>
							<?php
									if($get_art_info!= null && $get_art_info != "no")
				{
					$get_art_mem = $get_fan_club->art_subs();
					if($get_art_mem=="1")
					{
							?>
											<option value="artist_users"><?php echo $get_art_info['name']." Subscribers"; ?></option>
						<?php
								}
							}
							if($get_comm_info!= null && $get_comm_info != "no")
				{
					$get_com_mem = $get_fan_club->com_subs();
					if($get_com_mem=="1")
					{
						?>
											<option value="community_users"><?php echo $get_comm_info['name']." Subscribers"; ?></option>
											<?php
											}
										}
												$chk_artist_fan = $data_mail->find_artist_fans();
												if($chk_artist_fan!=""){
													$get_artist_name = $data_mail->get_fanclub_name("artist");
													if($get_artist_name['type']=="fan_club_yes"){
											?>
											<option value="artist_fans"><?php echo $get_artist_name['name'];?> Fan Club</option>
											<?php
												}
											}
											$chk_community_fan = $data_mail->find_community_fans();
												if($chk_community_fan!=""){
													$get_community_name = $data_mail->get_fanclub_name("community");
													if($get_community_name['type']=="fan_club_yes"){
											?>
											<option value="community_fans"><?php echo $get_community_name['name'];?> Fan Club</option>
											<?php
												}
											}
											$get_art_pro_fan = $data_mail->get_all_artist_projects();
											if($get_art_pro_fan!="")
											{
												while($art_pro = mysql_fetch_assoc($get_art_pro_fan))
												{
													$chkpro_fan_visible = $data_mail->find_projects_fans($art_pro['id'],"artist_project");
													if($chkpro_fan_visible !=""){
														if($art_pro['type']=="fan_club"){
													?>
													<option value="artist_project_fans"><?php echo $art_pro['title'];?> Fan Club</option>
													<?php
														}
													}
												}
											}
											$get_com_pro_fan = $data_mail->get_all_community_projects();
											if($get_com_pro_fan!="")
											{
												while($com_pro = mysql_fetch_assoc($get_com_pro_fan))
												{
													$chkpro_fan_visible = $data_mail->find_projects_fans($com_pro['id'],"community_project");
													if($chkpro_fan_visible !=""){
														if($com_pro['type']=="fan_club"){
													?>
													<option value="community_project_fans"><?php echo $com_pro['title'];?> Fan Club</option>
													<?php
														}
													}
												}
											}
										}
									?>
									</select>
								</div>
								<div class="fieldCont">
									<div class="fieldTitle">To</div>
									<div id="loader" style="display:none;">
										<img src="images/ajax_loader_large.gif" style="height: 25px; position:relative; left:72px;">
									</div>
									<div id="fieldCont_users">
									</div>
								</div>
								<div class="fieldCont">
									<div class="fieldTitle">Subject</div>
									<input id="subject" name="subject" type="text"  class="fieldText" value="<?php echo $reply_ans['subject']; ?>"/>
								</div>
								<div class="fieldCont">
									<div class="fieldTitle">Attachments</div>
									<?php
										if(isset($_GET['check_attach_remove']))
										{
										?>
											<input type="file" name="attachment" id="attachment" />
										<?php
										}
										else
										{
										?>
											<input type="text" disabled="true" value="<?php echo $reply_ans['attachments']; ?> ">
											<a href="javascript:confirmRemove('compose.php?reply=<?php echo $_GET['reply']; ?>&file_name=<?php echo $reply_ans['attachments']; ?>')">Change the Attachment</a>
										<?php
										}
										?>
								</div>
								<div class="fieldCont">
									<div class="fieldTitle">Body</div>
									<textarea id="mail_body" name="mail_body" class="jquery_ckeditor"><?php echo $reply_ans['mail_body']; ?></textarea>
								</div>
							<?php	
							}
							if(isset($_GET['forward']))
							{
								$reply_sql = $data_mail->viewMail($_GET['forward']);
								$reply_run = mysql_query($reply_sql);
								$reply_ans = mysql_fetch_assoc($reply_run);
							?>
								<div class="fieldCont">
									<div title="Select users to send mail to." class="fieldTitle">Select Users</div>
									<select  title="Select users to send mail to."class="dropdown" id="selected_users" name="selected_users">
										<?php
									
										$check_cust_all = $data_mail->checkCustom_All($newres1['general_user_id']);
										$chck_must = mysql_query($check_cust_all);
										if(mysql_num_rows($chck_must))
										{
									?>
											<option value="custom_users" <?php if($reply_ans['users']=='custom_users') { echo "selected"; } ?>>Custom</option>
											<option value="all_users" <?php if($reply_ans['users']=='all_users') { echo "selected"; } ?>>All</option>
											<?php
									if($get_art_info!= null && $get_art_info != "no")
				{
					$get_art_mem = $get_fan_club->art_subs();
					if($get_art_mem=="1")
					{
							?>
											<option value="artist_users" <?php if($reply_ans['users']=='artist_users') { echo "selected"; } ?>><?php echo $get_art_info['name']." Subscribers"; ?></option>
											<?php
											}
											}
											if($get_comm_info!= null && $get_comm_info != "no")
				{
					$get_com_mem = $get_fan_club->com_subs();
					if($get_com_mem=="1")
					{
											?>
											<option value="community_users" <?php if($reply_ans['users']=='community_users') { echo "selected"; } ?>><?php echo $get_comm_info['name']." Subscribers"; ?></option>
											<?php
											}
											}
												$chk_artist_fan = $data_mail->find_artist_fans();
												if($chk_artist_fan!=""){
													$get_artist_name = $data_mail->get_fanclub_name("artist");
													if($get_artist_name['type']=="fan_club_yes"){
											?>
											<option value="artist_fans" <?php if($reply_ans['users']=='artist_fans') { echo "selected"; } ?>><?php echo $get_artist_name['name'];?> Fan Club</option>
											<?php
													}
											}
											$chk_community_fan = $data_mail->find_community_fans();
											if($chk_community_fan!=""){
												$get_community_name = $data_mail->get_fanclub_name("community");
												if($get_community_name['type']=="fan_club_yes"){
											?>
											<option value="community_fans" <?php if($reply_ans['users']=='community_fans') { echo "selected"; } ?>><?php echo $get_community_name['name'];?> Fan Club</option>
											<?php
												}
											}
											$get_art_pro_fan = $data_mail->get_all_artist_projects();
											if($get_art_pro_fan!="")
											{
												while($art_pro = mysql_fetch_assoc($get_art_pro_fan))
												{
													$chkpro_fan_visible = $data_mail->find_projects_fans($art_pro['id'],"artist_project");
													if($chkpro_fan_visible !=""){
														if($art_pro['type']=="fan_club"){
													?>
													<option value="artist_project_fans_<?php echo $art_pro['id']; ?>" <?php if($reply_ans['users']=='artist_project_fans') { echo "selected"; } ?>><?php echo $art_pro['title'];?> Fan Club</option>
													<?php
														}
													}
												}
											}
											$get_com_pro_fan = $data_mail->get_all_community_projects();
											if($get_com_pro_fan!="")
											{
												while($com_pro = mysql_fetch_assoc($get_com_pro_fan))
												{
													$chkpro_fan_visible = $data_mail->find_projects_fans($com_pro['id'],"community_project");
													if($chkpro_fan_visible !=""){
														if($com_pro['type']=="fan_club"){
													?>
													<option value="community_project_fans_<?php echo $com_pro['id']; ?>" <?php if($reply_ans['users']=='community_project_fans') { echo "selected"; } ?>><?php echo $com_pro['title'];?> Fan Club</option>
													<?php
														}
													}
												}
											}
										}
										else
										{
									?>
											<option value="custom_users" <?php echo "selected"; ?>>Custom</option>
											<option value="all_users">All</option>
											<?php
									if($get_art_info!= null && $get_art_info != "no")
				{
					$get_art_mem = $get_fan_club->art_subs();
					if($get_art_mem=="1")
					{
							?>
											<option value="artist_users"><?php echo $get_art_info['name']." Subscribers"; ?></option>
											<?php
											}
											}
											if($get_comm_info!= null && $get_comm_info != "no")
				{
					$get_com_mem = $get_fan_club->com_subs();
					if($get_com_mem=="1")
					{
											?>
											<option value="community_users"><?php echo $get_comm_info['name']." Subscribers"; ?></option>
											<?php
											}
											}
												$chk_artist_fan = $data_mail->find_artist_fans();
												if($chk_artist_fan!=""){
													$get_artist_name = $data_mail->get_fanclub_name("artist");
													if($get_artist_name['type']=="fan_club_yes"){
											?>
											<option value="artist_fans"><?php echo $get_artist_name['name'];?> Fan Club</option>
											<?php
												}
											}
											$chk_community_fan = $data_mail->find_community_fans();
											if($chk_community_fan!=""){
												$get_community_name = $data_mail->get_fanclub_name("community");
												if($get_community_name['type']=="fan_club_yes"){
											?>
											<option value="community_fans"><?php echo $get_community_name['name'];?> Fan Club</option>
											<?php
												}
											}
											$get_art_pro_fan = $data_mail->get_all_artist_projects();
											if($get_art_pro_fan!="")
											{
												while($art_pro = mysql_fetch_assoc($get_art_pro_fan))
												{
													$chkpro_fan_visible = $data_mail->find_projects_fans($art_pro['id'],"artist_project");
													if($chkpro_fan_visible !=""){
														if($art_pro['type']=="fan_club"){
													?>
													<option value="artist_project_fans"><?php echo $art_pro['title'];?> Fan Club</option>
													<?php
														}
													}
												}
											}
											$get_com_pro_fan = $data_mail->get_all_community_projects();
											if($get_com_pro_fan!="")
											{
												while($com_pro = mysql_fetch_assoc($get_com_pro_fan))
												{
													$chkpro_fan_visible = $data_mail->find_projects_fans($com_pro['id'],"community_project");
													if($chkpro_fan_visible !=""){
														if($com_pro['type']=="fan_club"){
													?>
													<option value="community_project_fans"><?php echo $com_pro['title'];?> Fan Club</option>
													<?php
														}
													}
												}
											}
										}
									?>
									</select>
								</div>
								<div class="fieldCont">
									<div class="fieldTitle">To</div>
									<div id="loader" style="display:none;">
										<img src="images/ajax_loader_large.gif" style="height: 25px; position:relative; left:72px;">
									</div>
									<div id="fieldCont_users">
									</div>
								</div>
								<div class="fieldCont">
									<div class="fieldTitle">Subject</div>
									<input id="subject" name="subject" type="text"  class="fieldText" value="<?php echo $reply_ans['subject']; ?>"/>
								</div>
								<div class="fieldCont">
									<div class="fieldTitle">Attachments</div>
									<?php
										if(isset($_GET['check_attach_remove']))
										{
										?>
											<input type="file" name="attachment" id="attachment" />
										<?php
										}
										else
										{
										?>
											<input type="text" disabled="true" value="<?php echo $reply_ans['attachments']; ?> ">
											<a href="javascript:confirmRemove('compose.php?forward=<?php echo $_GET['forward']; ?>&file_name=<?php echo $reply_ans['attachments']; ?>')">Change the Attachment</a>
										<?php
										}
										?>
								</div>
								<div class="fieldCont">
									<div class="fieldTitle">Body</div>
									<textarea class="jquery_ckeditor" id="mail_body" name="mail_body"><?php echo $reply_ans['mail_body']; ?></textarea>
								</div>
								<?php	
							}
							if(isset($_GET['send_draft']))
							{
								$reply_sql = $data_mail->viewMail($_GET['send_draft']);
								$reply_run = mysql_query($reply_sql);
								$reply_ans = mysql_fetch_assoc($reply_run);
							?>
								<div class="fieldCont">
									<div title="Select users to send mail to." class="fieldTitle">Select Users</div>
									<select title="Select users to send mail to." class="dropdown" id="selected_users" name="selected_users">
										<option value="custom_users" <?php if($reply_ans['users']=='custom_users') { echo "selected"; } ?>>Custom</option>
										<option value="all_users" <?php if($reply_ans['users']=='all_users') { echo "selected"; } ?>>All</option>
										<?php
									if($get_art_info!= null && $get_art_info != "no")
				{
					$get_art_mem = $get_fan_club->art_subs();
					if($get_art_mem=="1")
					{
							?>
										<option value="artist_users" <?php if($reply_ans['users']=='artist_users') { echo "selected"; } ?>><?php echo $get_art_info['name']." Subscribers"; ?></option>
										<?php
										}
										}
										if($get_comm_info!= null && $get_comm_info != "no")
				{
					$get_com_mem = $get_fan_club->com_subs();
					if($get_com_mem=="1")
					{
					?>
										<option value="community_users" <?php if($reply_ans['users']=='community_users') { echo "selected"; } ?>><?php echo $get_comm_info['name']." Subscribers"; ?></option>
										<?php
										}
										}
										$chk_artist_fan = $data_mail->find_artist_fans();
										if($chk_artist_fan!=""){
											$get_artist_name = $data_mail->get_fanclub_name("artist");
											if($get_artist_name['type']=="fan_club_yes"){
										?>
										<option value="artist_fans" <?php if($reply_ans['users']=='artist_fans') { echo "selected"; } ?>><?php echo $get_artist_name['name'];?> Fan Club</option>
										<?php
											}
										}
										$chk_community_fan = $data_mail->find_community_fans();
										if($chk_community_fan!=""){
											$get_community_name = $data_mail->get_fanclub_name("community");
											if($get_community_name['type']=="fan_club_yes"){
										?>
										<option value="community_fans" <?php if($reply_ans['users']=='community_fans') { echo "selected"; } ?>><?php echo $get_community_name['name'];?> Fan Club</option>
										<?php
											}
										}
											$get_art_pro_fan = $data_mail->get_all_artist_projects();
											if($get_art_pro_fan!="")
											{
												while($art_pro = mysql_fetch_assoc($get_art_pro_fan))
												{
													$chkpro_fan_visible = $data_mail->find_projects_fans($art_pro['id'],"artist_project");
													if($chkpro_fan_visible !=""){
														if($get_art_pro_fan['type']=="fan_club"){
													?>
													<option value="artist_project_fans_<?php echo $art_pro['id']; ?>" <?php if($reply_ans['users']=='artist_project_fans_'.$art_pro['id']) { echo "selected"; } ?>><?php echo $art_pro['title'];?> Fan Club</option>
													<?php
														}
													}
												}
											}
											$get_com_pro_fan = $data_mail->get_all_community_projects();
											if($get_com_pro_fan!="")
											{
												while($com_pro = mysql_fetch_assoc($get_com_pro_fan))
												{
													$chkpro_fan_visible = $data_mail->find_projects_fans($com_pro['id'],"community_project");
													if($chkpro_fan_visible !=""){
														if($get_com_pro_fan['type']=="fan_club"){
													?>
													<option value="community_project_fans_<?php echo $com_pro['id']; ?>" <?php if($reply_ans['users']=='community_project_fans_'.$com_pro['id']) { echo "selected"; } ?>><?php echo $com_pro['title'];?> Fan Club</option>
													<?php
														}
													}
												}
											}
											?>
									</select>
								</div>
								<div class="fieldCont">
									<div class="fieldTitle">To</div>
									<div id="loader" style="display:none;">
										<img src="images/ajax_loader_large.gif" style="height: 25px; position:relative; left:72px;">
									</div>
									<div id="fieldCont_users">
									</div>
								</div>
								<div class="fieldCont">
									<div class="fieldTitle">Subject</div>
									<input id="subject" name="subject" type="text"  class="fieldText" value="<?php echo $reply_ans['subject']; ?>"/>
								</div>
								<div class="fieldCont">
									<div class="fieldTitle">Attachments</div>
									<?php
										if(isset($_GET['check_attach_remove']))
										{
										?>
											<input type="file" name="attachment" id="attachment" />
										<?php
										}
										else
										{
										?>
											<input type="text" disabled="true" value="<?php echo $reply_ans['attachments']; ?> ">
											<a href="javascript:confirmRemove('compose.php?send_draft=<?php echo $_GET['send_draft']; ?>&file_name=<?php echo $reply_ans['attachments']; ?>')">Change the Attachment</a>
										<?php
										}
										?>
								</div>
								<div class="fieldCont">
									<div class="fieldTitle">Body</div>
									<textarea id="mail_body" name="mail_body" class="jquery_ckeditor"><?php echo $reply_ans['mail_body']; ?></textarea>
								</div>
							<?php	
							}
							if(isset($_GET['send_sent']))
							{
								$reply_sql = $data_mail->viewMail($_GET['send_sent']);
								$reply_run = mysql_query($reply_sql);
								$reply_ans = mysql_fetch_assoc($reply_run);
							?>
								<div title="Select users to send mail to." class="fieldCont">
									<div title="Select users to send mail to." class="fieldTitle">Select Users</div>
									<select class="dropdown" id="selected_users" name="selected_users">
										<option value="custom_users" <?php if($reply_ans['users']=='custom_users') { echo "selected"; } ?>>Custom</option>
										<option value="all_users" <?php if($reply_ans['users']=='all_users') { echo "selected"; } ?>>All</option>
										<?php
									if($get_art_info!= null && $get_art_info != "no")
				{
					$get_art_mem = $get_fan_club->art_subs();
					if($get_art_mem=="1")
					{
							?>
										<option value="artist_users" <?php if($reply_ans['users']=='artist_users') { echo "selected"; } ?>><?php echo $get_art_info['name']." Subscribers"; ?></option>
										<?php
										}
										}
										if($get_comm_info!= null && $get_comm_info != "no")
				{
					$get_com_mem = $get_fan_club->com_subs();
					if($get_com_mem=="1")
					{
										?>
										<option value="community_users" <?php if($reply_ans['users']=='community_users') { echo "selected"; } ?>><?php echo $get_comm_info['name']." Subscribers"; ?></option>
										<?php
										}
										}
										$chk_artist_fan = $data_mail->find_artist_fans();
										if($chk_artist_fan!=""){
											$get_artist_name = $data_mail->get_fanclub_name("artist");
											if($get_artist_name['type']=="fan_club_yes"){
										?>
										<option value="artist_fans" <?php if($reply_ans['users']=='artist_fans') { echo "selected"; } ?>><?php echo $get_artist_name['name'];?> Fan Club</option>
										<?php
											}
										}
										$chk_community_fan = $data_mail->find_community_fans();
										if($chk_community_fan!=""){
											$get_community_name = $data_mail->get_fanclub_name("community");
											if($get_community_name['type']=="fan_club_yes"){
										?>
										<option value="community_fans" <?php if($reply_ans['users']=='community_fans') { echo "selected"; } ?>><?php echo $get_community_name['name'];?> Fan Club</option>
										<?php
											}
										}
											$get_art_pro_fan = $data_mail->get_all_artist_projects();
											if($get_art_pro_fan!="")
											{
												while($art_pro = mysql_fetch_assoc($get_art_pro_fan))
												{
													$chkpro_fan_visible = $data_mail->find_projects_fans($art_pro['id'],"artist_project");
													if($chkpro_fan_visible !=""){
														if($art_pro['type']=="fan_club"){
													?>
													<option value="artist_project_fans_<?php echo $art_pro['id']; ?>" <?php if($reply_ans['users']=='artist_project_fans_'.$art_pro['id']) { echo "selected"; } ?>><?php echo $art_pro['title'];?> Fan Club</option>
													<?php
														}
													}
												}
											}
											$get_com_pro_fan = $data_mail->get_all_community_projects();
											if($get_com_pro_fan!="")
											{
												while($com_pro = mysql_fetch_assoc($get_com_pro_fan))
												{
													$chkpro_fan_visible = $data_mail->find_projects_fans($com_pro['id'],"community_project");
													if($chkpro_fan_visible !=""){
														if($com_pro['type']=="fan_club"){
													?>
													<option value="community_project_fans_<?php echo $com_pro['id']; ?>" <?php if($reply_ans['users']=='community_project_fans_'.$com_pro['id']) { echo "selected"; } ?>><?php echo $com_pro['title'];?> Fan Club</option>
													<?php
														}
													}
												}
											}
											?>
									</select>
								</div>
								<div class="fieldCont">
									<div class="fieldTitle">To</div>
									<div id="loader" style="display:none;">
										<img src="images/ajax_loader_large.gif" style="height: 25px; position:relative; left:72px;">
									</div>
									<div id="fieldCont_users">
									</div>
								</div>
								<div class="fieldCont">
									<div class="fieldTitle">Subject</div>
									<input id="subject" name="subject" type="text"  class="fieldText" value="<?php echo $reply_ans['subject']; ?>"/>
								</div>
								<div class="fieldCont">
									<div class="fieldTitle">Attachments</div>
									<?php
										if(isset($_GET['check_attach_remove']))
										{
										?>
											<input type="file" name="attachment" id="attachment" />
										<?php
										}
										else
										{
										?>
											<input type="text" disabled="true" value="<?php echo $reply_ans['attachments']; ?> ">
											<a href="javascript:confirmRemove('compose.php?send_sent=<?php echo $_GET['send_sent']; ?>&file_name=<?php echo $reply_ans['attachments']; ?>')">Change the Attachment</a>
										<?php
										}
										?>
								</div>
								<div class="fieldCont">
									<div class="fieldTitle">Body</div>
									<textarea id="mail_body" name="mail_body" class="jquery_ckeditor"><?php echo $reply_ans['mail_body']; ?></textarea>
								</div>
							<?php	
							}
							
							if(!isset($_GET['reply']) && !isset($_GET['forward']) && !isset($_GET['send_draft']) && !isset($_GET['send_sent']))
							{
							?>
								<div class="fieldCont">
									<div title="Select users to send mail to." class="fieldTitle">Select Users</div>
									<select title="Select users to send mail to." class="dropdown" id="selected_users" name="selected_users">
										<option value="custom_users">Custom</option>
										<option value="all_users">All</option>
										<!--<option value="artist_users">Artist</option>
										<option value="community_users">Community</option>-->
										<?php
				if($get_art_info!= null && $get_art_info != "no")
				{
					$get_art_mem = $get_fan_club->art_subs();
					if($get_art_mem=="1")
					{
				?>
					
					<option value="artist_users"><?php echo $get_art_info['name']." Subscribers"; ?></option>
				<?php
					}
				}
				if($get_comm_info!= null && $get_comm_info != "no")
				{
					$get_com_mem = $get_fan_club->com_subs();
					if($get_com_mem=="1")
					{
				?>
					
					<option value="community_users"><?php echo $get_comm_info['name']." Subscribers"; ?></option>
				<?php
					}
				}
										$chk_artist_fan = $data_mail->find_artist_fans();
										if($chk_artist_fan!=""){
											$get_artist_name = $data_mail->get_fanclub_name("artist");
											if($get_artist_name['type']=="fan_club_yes"){
										?>
											<option value="artist_fans"><?php echo $get_artist_name['name'];?> Fan Club</option>
										<?php
											}
										}
										$chk_community_fan = $data_mail->find_community_fans();
										if($chk_community_fan!=""){
											$get_community_name = $data_mail->get_fanclub_name("community");
											if($get_community_name['type']=="fan_club_yes"){
										?>
										<option value="community_fans"><?php echo $get_community_name['name'];?> Fan Club</option>
										<?php
											}
										}
										$get_art_pro_fan = $data_mail->get_all_artist_projects();
										if($get_art_pro_fan!="")
										{
											while($art_pro = mysql_fetch_assoc($get_art_pro_fan))
											{
												$chkpro_fan_visible = $data_mail->find_projects_fans($art_pro['id'],"artist_project");
												
												if($chkpro_fan_visible !=""){
													if($art_pro['type']=="fan_club"){
												?>
												<option value="artist_project_fans_<?php echo $art_pro['id']; ?>"><?php echo $art_pro['title'];?> Fan Club</option>
												<?php
													}
												}
											}
										}
										$get_com_pro_fan = $data_mail->get_all_community_projects();
										if($get_com_pro_fan!="")
										{
											while($com_pro = mysql_fetch_assoc($get_com_pro_fan))
											{
												$chkpro_fan_visible = $data_mail->find_projects_fans($com_pro['id'],"community_project");
												if($chkpro_fan_visible !=""){
													if($com_pro['type']=="fan_club"){
												?>
												<option value="community_project_fans_<?php echo $com_pro['id']; ?>"><?php echo $com_pro['title'];?> Fan Club</option>
												<?php
													}
												}
											}
										}
										?>
									</select>
								</div>
								<div class="fieldCont">
									<div class="fieldTitle">To</div>
									<div id="loader" style="display:none;">
										<img src="images/ajax_loader_large.gif" style="height: 25px; position:relative; left:72px;">
									</div>
									<div id="fieldCont_users">
									</div>
								</div>
								<div class="fieldCont">
									<div class="fieldTitle">Subject</div>
									<input id="subject" name="subject" type="text"  class="fieldText" />
								</div>
								<div class="fieldCont">
									<div class="fieldTitle">Attachments</div>
									<input type="file" name="attachment" id="attachment" class="fieldText"/>
								</div>
								<div class="fieldCont">
									<div class="fieldTitle">Body</div>
									<textarea class="jquery_ckeditor" id="mail_body" name="mail_body"></textarea>
								</div>
							<?php
							}
							?>
                        <div class="fieldCont">
                        	<div class="fieldTitle"></div>
                        	<input type="submit" class="register" value="Send"  onClick="return send();"/>
							<input type="submit" class="register" value="Save" onClick="draft()" />
                        </div>
                    </div>
					</form>
                    <!--<p><strong>Use the general Mail function from Cypress Groove with the following differences:</strong></p>
     				<p><strong>        1. 
        Send out all emails in regualar design format rather than the format used for Cypress Groove Emails. <br />
        2. &quot;Remove Members&quot; and &quot;Unregistered Contacts&quot; from the user categories<br />
        3. Add &quot;Out of Network&quot; to the user categories<br />
        4. Change &quot;Add Image&quot; field to &quot;Attachments&quot; field where user can select from original media that they have the right to share.<br />
        5. Add Compose, Drafts, Inbox, and Sent tabs on top of form<br />
      6. When a user mails a registered user the message will be sent to their Purify Inbox as well as the users email address.</strong></p>-->
      			</div>          
               </div>
          </div>
   </div>
</div>

  <!-- end of main container -->
</div><?php include_once("displayfooter.php"); ?>
<div id="popupContact">
		<a id="popupContactClose">x</a>
		<p id="contactArea">
			Only registered users can view media. 
		</p>
        <h1>Register <a href="index.php">Here</a>, It's Free!</h1><br />
        <p>Already Registered ? Login below.</p>
        <div class="formCont">
            <div class="formFieldCont">
        	<div class="fieldTitle">Username</div>
            <input type="text" class="textfield" />
        </div>
       		<div class="formFieldCont">
        	<div class="fieldTitle">Password</div>
            <input type="password" class="textfield" />
        </div>
        	<div class="formFieldCont">
        	<div class="fieldTitle"></div>
            <input type="image" class="login" src="images/profile/login.png" width="47" height="23" />
        </div>
        </div>
	</div>
<div id="backgroundPopup"></div>
</body>
</html>
<script>
	
	function send()
	{
	
		var select_type = document.getElementById("selected_users");
		var mail_to = document.getElementById("mails_users[]");
		var sub = document.getElementById("subject");
		var body = $("#mail_body").val();
		
		if(mail_to==null || mail_to.value==null || mail_to.value=="")
		{
			alert("Please enter Email IDs to send mail.");
			return false;
		}
		if(select_type.value=='custom_users')
		{
			
			var email_split = mail_to.value.split(',');
			var r;
			for(ei=0;ei<email_split.length;ei++)
			{
				if(email_split[ei]!="")
				{
					//alert(email_split[ei]);
					var atpos = email_split[ei].indexOf("@");
					var dotpos = email_split[ei].lastIndexOf(".");
					if (atpos<1 || dotpos<atpos+2 || dotpos+2>=email_split[ei].lenght)
					{
						alert("Please enter a valid Email Id.");
						return false;
					}
				}
			}
		}
		if(sub.value=="" || sub.value==null)
		{
			if (!confirm("Are you sure you want to send mail without Subject?")) 
			{
				return false;
			}
		}
		if(body=="" || body==null)
		{
			if (!confirm("Are you sure you want to send mail without Body?")) 
			{
				return false;
			}
		}
		
		
		var form_mail = document.getElementById("mails");
		
		<?php
		
		if(isset($_GET['send_draft']))
		{
			if(isset($_GET['check_attach_remove']))
			{
			?>
				form_mail.action = "compose.php?mail_status=1&send_draft="+<?php echo $_GET['send_draft']; ?>+"&check_attach_remove";  /////////Mail Sent To Users
			<?php
			}
			else
			{
			?>
				form_mail.action = "compose.php?mail_status=1&send_draft="+<?php echo $_GET['send_draft']; ?>;  /////////Mail Sent To Users
			<?php
			}
		}
		elseif(isset($_GET['send_sent']))
		{
			if(isset($_GET['check_attach_remove']))
			{
			?>
				form_mail.action = "compose.php?mail_status=1&send_sent="+<?php echo $_GET['send_sent']; ?>+"&check_attach_remove";  /////////Mail Sent To Users
			<?php
			}
			else
			{
			?>
				form_mail.action = "compose.php?mail_status=1&send_sent="+<?php echo $_GET['send_sent']; ?>;  /////////Mail Sent To Users
			<?php
			}
		}
		elseif(isset($_GET['reply']))
		{
			if(isset($_GET['check_attach_remove']))
			{
			?>
				form_mail.action = "compose.php?mail_status=1&reply="+<?php echo $_GET['reply']; ?>+"&check_attach_remove";  /////////Mail Sent To Users
			<?php
			}
			else
			{
			?>
				form_mail.action = "compose.php?mail_status=1&reply="+<?php echo $_GET['reply']; ?>;   /////////Mail Sent To Users
			<?php	
			}
		}
		elseif(isset($_GET['forward']))
		{
			if(isset($_GET['check_attach_remove']))
			{
			?>
				form_mail.action = "compose.php?mail_status=1&forward="+<?php echo $_GET['forward']; ?>+"&check_attach_remove";   /////////Mail Sent To Users
			<?php	
			}
			else
			{
			?>
				form_mail.action = "compose.php?mail_status=1&forward="+<?php echo $_GET['forward']; ?>;   /////////Mail Sent To Users
			<?php	
			}
		}
		else
		{
		?>
			form_mail.action = "compose.php?mail_status=1";   /////////Mail Sent To Users
		<?php	
		}
		?>
	}
	
	function draft()
	{
		alert("Your Mail has been saved in Drafts.");
		var form_mail = document.getElementById("mails");
	<?php
		if(isset($_GET['send_draft']))
		{
			if(isset($_GET['check_attach_remove']))
			{
			?>
				form_mail.action = "compose.php?mail_status=2&send_draft="+<?php echo $_GET['send_draft']; ?>+"&check_attach_remove";     /////////Mail Saved In Draft
			<?php
			}
			else
			{
				?>
				form_mail.action = "compose.php?mail_status=2&send_draft="+<?php echo $_GET['send_draft']; ?>;  /////////Mail Saved In Draft
			<?php
			}
		}
		else
		{
			if(isset($_GET['check_attach_remove']))
			{
			?>
				form_mail.action = "compose.php?mail_status=2&check_attach_remove";  /////////Mail Saved In Draft
			<?php
			}
			else
			{
			?>
				form_mail.action = "compose.php?mail_status=2";  /////////Mail Saved In Draft
			<?php
			}
		}
		?>
	}
	
	function confirmRemove(delUrl) 
	{
		if (confirm("Are you sure you want to change the file?")) 
		{
			<?php //$update_attach_sql = $data_mail->updateAttachments($_GET['send'],$reply_ans['attachments']); ?>
			document.location = delUrl;
		}
	}
	
</script>
<?php
//var_dump($_POST);
if(isset($_GET['file_name']) && $_GET['file_name']!="")
{
	if(isset($_GET['send_draft']))
	{
		//$update_attach_sql = $data_mail->updateAttachments($_GET['send_draft'],$_GET['file_name']);
		$check_attach_remove = "";
		header("Location: compose.php?send_draft=".$_GET['send_draft']."&check_attach_remove");
	}
	if(isset($_GET['send_sent']))
	{
		//$update_attach_sql = $data_mail->updateAttachments($_GET['send_sent'],$_GET['file_name']);
		$check_attach_remove = "";
		header("Location: compose.php?send_sent=".$_GET['send_sent']."&check_attach_remove");
	}
	if(isset($_GET['reply']))
	{
		//$update_attach_sql = $data_mail->updateAttachments($_GET['reply'],$_GET['file_name']);
		$check_attach_remove = "";
		header("Location: compose.php?reply=".$_GET['reply']."&check_attach_remove");
	}
	if(isset($_GET['forward']))
	{
		//$update_attach_sql = $data_mail->updateAttachments($_GET['forward'],$_GET['file_name']);
		$check_attach_remove = "";
		header("Location: compose.php?forward=".$_GET['forward']."&check_attach_remove");
	}
}
/* var_dump($_SESSION);
die; */
if(isset($_POST) && $_POST!=null)
{
	include("newsmtp/PHPMailer/index.php");
	$mailstat = $_GET['mail_status'];
	
	if(isset($_FILES["attachment"]["name"]) && $_FILES["attachment"]["name"]!="")
	{
		//echo "in 2 if";
		$attach_name = time().'_'.$_FILES["attachment"]["name"];
		$attach_name1 = $_FILES["attachment"]["name"];
		move_uploaded_file($_FILES["attachment"]["tmp_name"],"uploads/mail attachments/" . $attach_name);
		//var_dump($attach_name);
		//die;
	}
	else
	{
		$attach_name = "";
	}
	
	if($_POST['selected_users']!='custom_users')
	{
		$mail_to_users = $_POST['mails_users'];
		//$mails_users_new = implode(',',$_POST['mails_users']);
		$gcic = implode(',',$_POST['mails_users']);
	}
	if($_POST['selected_users']=='custom_users')
	{
		$gcic = implode(',',$_POST['mails_users']);
		$mail_to_users1 = implode(',',$_POST['mails_users']);
		$mail_to_users = explode(',',$mail_to_users1);
	}
	
	if(!isset($_GET['send_draft']))
	{
		//echo "in 3 if";
		//die;
		for($ux=0;$ux<count($mail_to_users);$ux++)
		{
			if($mail_to_users[$ux]!="")
			{
				if(isset($_FILES["attachment"]["name"]) && $_FILES["attachment"]["name"]!="")
				{
					//echo "in 4 if";
					$attach_name = time().'_'.$_FILES["attachment"]["name"];
				}
				elseif(!isset($_GET['check_attach_remove']))
				{
					//echo "in 5 if";
					if(isset($_GET['reply']))
					{
						$attachment_draft_sql = $data_mail->viewMail($_GET['reply']);
						$attachment_draft_run = mysql_query($attachment_draft_sql);
						$attachment_draft_ans = mysql_fetch_assoc($attachment_draft_run);
						if(isset($attachment_draft_ans['attachments']))
						{
							$attach_name = $attachment_draft_ans['attachments'];
						}
					}
					if(isset($_GET['forward']))
					{
						$attachment_draft_sql = $data_mail->viewMail($_GET['forward']);
						$attachment_draft_run = mysql_query($attachment_draft_sql);
						$attachment_draft_ans = mysql_fetch_assoc($attachment_draft_run);
						if(isset($attachment_draft_ans['attachments']))
						{
							$attach_name = $attachment_draft_ans['attachments'];
						}
					}
				}
				
				$sql_mail = $data_mail->save_send_mail($newres1['general_user_id'],$_POST['selected_users'],mysql_escape_string($mail_to_users[$ux]),mysql_escape_string($_POST['subject']),mysql_escape_string($attach_name),mysql_escape_string($_POST['mail_body']),$mailstat);
				//var_dump($sql_mail);
				//die;
				mysql_query($sql_mail);
				if($ux==0)
				{
					$last_id_count_main = mysql_insert_id();
					$rev_str = strrev($gcic);
					if($rev_str==',')
					{
						$count_ids = count($mail_to_users) - 2;
					}
					else
					{
						$count_ids = count($mail_to_users) - 1;
					}
					
					$sql_count = "UPDATE `mails` SET `related_mail_id`='".$last_id_count_main.'_'.$count_ids."' WHERE `id`=$last_id_count_main";
					
				}
				else
				{
					$last_id_count = mysql_insert_id();
					$sql_count = "UPDATE `mails` SET `related_mail_id`='".$last_id_count_main."' WHERE `id`=$last_id_count";
				}
				mysql_query($sql_count);
			}
			else
			{
				$mail_to_users = "";
				$sql_mail = $data_mail->save_send_mail($newres1['general_user_id'],$_POST['selected_users'],mysql_escape_string($mail_to_users),mysql_escape_string($_POST['subject']),mysql_escape_string($attach_name),mysql_escape_string($_POST['mail_body']),$mailstat);
				//var_dump($sql_mail);
				//die;
				mysql_query($sql_mail);
				$last_id_count = mysql_insert_id();
				$sql_count = "UPDATE `mails` SET `related_mail_id`='".$last_id_count."_0' WHERE `id`=$last_id_count";
				mysql_query($sql_count);
			}
		}
		//echo "in  1 if";
	}
	//die;

	if(isset($_GET['send_draft']) && $mailstat==2)
	{
		if(isset($_FILES["attachment"]["name"]) && $_FILES["attachment"]["name"]!="")
		{
			$attach_name = time().'_'.$_FILES["attachment"]["name"];
		}
		elseif(!isset($_GET['check_attach_remove']))
		{
			$attachment_draft_sql = $data_mail->viewMail($_GET['send_draft']);
			$attachment_draft_run = mysql_query($attachment_draft_sql);
			$attachment_draft_ans = mysql_fetch_assoc($attachment_draft_run);
			if(isset($attachment_draft_ans['attachments']))
			{
				$attach_name = $attachment_draft_ans['attachments'];
			}
		}
		
		if(count($mail_to_users)>1)
		{
			$rev_str = strrev($gcic);
			if(strpos($rev_str,',')==0)
			{
				$count_ids = count($mail_to_users) - 2;
			}
			else
			{
				$count_ids = count($mail_to_users) - 1;
			}
			
			$count_db_draftm = $data_mail->viewMail($_GET['send_draft']);
			$run_count_draftm = mysql_query($count_db_draftm);
			$ans_count_draftm = mysql_fetch_assoc($run_count_draftm);
			$rel_count_id = explode('_',$ans_count_draftm['related_mail_id']);
			
			if($count_ids==$rel_count_id[1])
			{
				$for_count = $rel_count_id[1] + 1;
				$send_draft = $_GET['send_draft'];
				for($uxw=0;$uxw<$for_count;$uxw++)
				{
					if($mail_to_users[$uxw]!="")
					{
						if($uxw==0)
						{
							$update_drafts_sql_more = $data_mail->updateDrafts($_POST['selected_users'],mysql_escape_string($mail_to_users[$uxw]),mysql_escape_string($_POST['subject']),mysql_escape_string($attach_name),mysql_escape_string($_POST['mail_body']),$send_draft);
							mysql_query($update_drafts_sql_more);
							//$send_draft = $send_draft + 1;
						}
						else
						{
							$update_drafts_sql_more_rel = $data_mail->updateDrafts_Rel($_POST['selected_users'],mysql_escape_string($mail_to_users[$uxw]),mysql_escape_string($_POST['subject']),mysql_escape_string($attach_name),mysql_escape_string($_POST['mail_body']),$send_draft);
							mysql_query($update_drafts_sql_more_rel);
						}
					}
				}
			}
			else
			{
				$stat = 2;
				$for_count_more = $rel_count_id[1] + 1;
				$send_draft_rel = $_GET['send_draft'];
				
				for($uxw=0;$uxw<$for_count_more;$uxw++)
				{
					if($mail_to_users[$uxw]!="")
					{
						//echo "in if";
						
						if($uxw==0)
						{
							$update_drafts_sql_more = $data_mail->updateDrafts($_POST['selected_users'],mysql_escape_string($mail_to_users[$uxw]),mysql_escape_string($_POST['subject']),mysql_escape_string($attach_name),mysql_escape_string($_POST['mail_body']),$send_draft_rel);
							mysql_query($update_drafts_sql_more);
						
							$sql_count_more_draft_new = "UPDATE `mails` SET `related_mail_id`='".$_GET['send_draft']."_".$count_ids."' WHERE `id`=$_GET[send_draft]";
							mysql_query($sql_count_more_draft_new);
						}
						else
						{
							$update_drafts_sql_more_relm = $data_mail->updateDrafts_Rel($_POST['selected_users'],mysql_escape_string($mail_to_users[$uxw]),mysql_escape_string($_POST['subject']),mysql_escape_string($attach_name),mysql_escape_string($_POST['mail_body']),$send_draft_rel);
							mysql_query($update_drafts_sql_more_relm);
						}
						//$send_draft_rel = $send_draft_rel + 1;
					}
				}
				
				if($count_ids<$rel_count_id[1])
				{
					$diff_main_new =  $rel_count_id[1] - $count_ids;
					for($eim=0;$eim<$diff_main_new;$eim++)
					{
						//$sql_delete_rem = "DELETE FROM mails WHERE related_mail_id='".$_GET['send_draft']."' AND "
						$sql_max_id = mysql_query("SELECT MAX(id) FROM mails WHERE related_mail_id='".$_GET['send_draft']."'");
						while($row_max = mysql_fetch_assoc($sql_max_id))
						{
							$ans_max_id = $row_max;
						}
						$delete_ma_id = mysql_query("DELETE FROM mails WHERE id='".$ans_max_id['MAX(id)']."'");
					}
				}
				
				if($count_ids>$rel_count_id[1])
				{
					
					$diff_main = $count_ids - $rel_count_id[1];
					for($nuxw=0;$nuxw<$diff_main;$nuxw++)
					{
						$save_update_sql_draft = $data_mail->save_send_MoreDrafts($newres1['general_user_id'],$_POST['selected_users'],mysql_escape_string($mail_to_users[$uxw]),mysql_escape_string($_POST['subject']),mysql_escape_string($attach_name),mysql_escape_string($_POST['mail_body']),$stat);
						mysql_query($save_update_sql_draft);
						$id_more_draft = mysql_insert_id(); 
						
						$sql_count_more_draft = "UPDATE `mails` SET `related_mail_id`='".$_GET['send_draft']."' WHERE `id`=$id_more_draft";
						mysql_query($sql_count_more_draft);
						
						$uxw = $uxw + 1;
					}
				}
			}
		}
		else
		{
			if($_POST['mails_users'][0]!=NULL)
			{
				for($em_org_users=0;$em_org_users<count($_POST['mails_users']);$em_org_users++)
				{
					$update_drafts_sql = $data_mail->updateDrafts($_POST['selected_users'],mysql_escape_string($_POST['mails_users'][$em_org_users]),mysql_escape_string($_POST['subject']),mysql_escape_string($attach_name),mysql_escape_string($_POST['mail_body']),$_GET['send_draft']);
					mysql_query($update_drafts_sql);
					
					$sql_delete_oc = mysql_query("DELETE FROM mails WHERE related_mail_id='".$_GET['send_draft']."'");
					$sql_count_more_draft = "UPDATE `mails` SET `related_mail_id`='".$_GET['send_draft']."_0' WHERE `id`=$_GET[send_draft]";
					mysql_query($sql_count_more_draft);
				}
			}
			// else
			// {
				// $update_drafts_sql = $data_mail->updateDrafts($_POST['selected_users'],mysql_escape_string($_POST['mails_users']),mysql_escape_string($_POST['subject']),mysql_escape_string($attach_name),mysql_escape_string($_POST['mail_body']),$_GET['send_draft']);
				// mysql_query($update_drafts_sql);
			// }	
		}
	}
	
	if(isset($_GET['send_draft']) && $mailstat==1)
	{
		if(isset($_FILES["attachment"]["name"]) && $_FILES["attachment"]["name"]!="")
		{
			$attach_name = time().'_'.$_FILES["attachment"]["name"];
		}
		elseif(!isset($_GET['check_attach_remove']))
		{
			//echo "hii";
			$attachment_draft_sql = $data_mail->viewMail($_GET['send_draft']);
			$attachment_draft_run = mysql_query($attachment_draft_sql);
			$attachment_draft_ans = mysql_fetch_assoc($attachment_draft_run);
			if(isset($attachment_draft_ans['attachments']))
			{
				$attach_name = $attachment_draft_ans['attachments'];
			}
		}
		
		if(count($mail_to_users)>1)
		{
			$rev_str = strrev($gcic);
			if(strpos($rev_str,',')==0)
			{
				$count_ids = count($mail_to_users) - 2;
			}
			else
			{
				$count_ids = count($mail_to_users) - 1;
			}
			
			$count_db_draftm = $data_mail->viewMail($_GET['send_draft']);
			$run_count_draftm = mysql_query($count_db_draftm);
			$ans_count_draftm = mysql_fetch_assoc($run_count_draftm);
			$rel_count_id = explode('_',$ans_count_draftm['related_mail_id']);
		
			if($count_ids==$rel_count_id[1])
			{
				$for_count = $rel_count_id[1] + 1;
				$send_draft = $_GET['send_draft'];

				for($uxw=0;$uxw<$for_count;$uxw++)
				{
					if($mail_to_users[$uxw]!="")
					{
						if($uxw==0)
						{
							//$update_related_data = $_GET['send_draft'].'_'.$count_ids;
							$update_drafts_sql = $data_mail->updateDrafts_sendmore($_POST['selected_users'],mysql_escape_string($mail_to_users[$uxw]),mysql_escape_string($_POST['subject']),mysql_escape_string($attach_name),mysql_escape_string($_POST['mail_body']),$_GET['send_draft']);
							mysql_query($update_drafts_sql);
						}
						else
						{
							$stat = 1;
							$save_update_sql_draft = $data_mail->updateDrafts_sendmore_Rel($_POST['selected_users'],mysql_escape_string($mail_to_users[$uxw]),mysql_escape_string($_POST['subject']),mysql_escape_string($attach_name),mysql_escape_string($_POST['mail_body']),$_GET['send_draft']);
							mysql_query($save_update_sql_draft);
						}
					}
				}
			}
			else
			{
				$stat = 2;
				$for_count_more = $rel_count_id[1] + 1;
				$send_draft_rel = $_GET['send_draft'];
				
				for($uxw=0;$uxw<$for_count_more;$uxw++)
				{
					if($mail_to_users[$uxw]!="")
					{
						//echo "in if";
						
						if($uxw==0)
						{
							$update_drafts_sql_more = $data_mail->updateDrafts_sendmore($_POST['selected_users'],mysql_escape_string($mail_to_users[$uxw]),mysql_escape_string($_POST['subject']),mysql_escape_string($attach_name),mysql_escape_string($_POST['mail_body']),$send_draft_rel);
							mysql_query($update_drafts_sql_more);
						
							$sql_count_more_draft_new = "UPDATE `mails` SET `related_mail_id`='".$_GET['send_draft']."_".$count_ids."' WHERE `id`=$_GET[send_draft]";
							mysql_query($sql_count_more_draft_new);
						}
						else
						{
							$update_drafts_sql_more_relm = $data_mail->updateDrafts_sendmore_Rel($_POST['selected_users'],mysql_escape_string($mail_to_users[$uxw]),mysql_escape_string($_POST['subject']),mysql_escape_string($attach_name),mysql_escape_string($_POST['mail_body']),$send_draft_rel);
							mysql_query($update_drafts_sql_more_relm);
						}
						//$send_draft_rel = $send_draft_rel + 1;
					}
				}
				
				if($count_ids<$rel_count_id[1])
				{
					$diff_main_new =  $rel_count_id[1] - $count_ids;
					for($eim=0;$eim<$diff_main_new;$eim++)
					{
						//$sql_delete_rem = "DELETE FROM mails WHERE related_mail_id='".$_GET['send_draft']."' AND "
						$sql_max_id = mysql_query("SELECT MAX(id) FROM mails WHERE related_mail_id='".$_GET['send_draft']."'");
						while($row_max = mysql_fetch_assoc($sql_max_id))
						{
							$ans_max_id = $row_max;
						}
						$delete_ma_id = mysql_query("DELETE FROM mails WHERE id='".$ans_max_id['MAX(id)']."'");
					}
				}
				
				if($count_ids>$rel_count_id[1])
				{
					$diff_main = $count_ids - $rel_count_id[1];
					for($nuxw=0;$nuxw<$diff_main;$nuxw++)
					{
						$save_update_sql_draft = $data_mail->save_send_MoreDrafts($newres1['general_user_id'],$_POST['selected_users'],mysql_escape_string($mail_to_users[$uxw]),mysql_escape_string($_POST['subject']),mysql_escape_string($attach_name),mysql_escape_string($_POST['mail_body']),$stat);
						mysql_query($save_update_sql_draft);
						$id_more_draft = mysql_insert_id(); 
						
						$sql_count_more_draft = "UPDATE `mails` SET `related_mail_id`='".$_GET['send_draft']."' WHERE `id`=$id_more_draft";
						mysql_query($sql_count_more_draft);
						
						$uxw = $uxw + 1;
					}
				}
			}
		}
		else
		{
			if($_POST['mails_users'][0]!=NULL)
			{
				for($em_org_users=0;$em_org_users<count($_POST['mails_users']);$em_org_users++)
				{
					$update_drafts_sql = $data_mail->updateDrafts($_POST['selected_users'],mysql_escape_string($_POST['mails_users'][$em_org_users]),mysql_escape_string($_POST['subject']),mysql_escape_string($attach_name),mysql_escape_string($_POST['mail_body']),$_GET['send_draft']);
					mysql_query($update_drafts_sql);
					
					$sql_delete_ocn = mysql_query("DELETE FROM mails WHERE related_mail_id='".$_GET['send_draft']."'");
					$sql_count_more_draft = "UPDATE `mails` SET `related_mail_id`='".$_GET['send_draft']."_0' WHERE `id`=$_GET[send_draft]";
					mysql_query($sql_count_more_draft);
				}
			}
			// else
			// {
				// $update_drafts_sql = $data_mail->updateDrafts_send($_POST['selected_users'],mysql_escape_string($_POST['mails_users']),mysql_escape_string($_POST['subject']),mysql_escape_string($attach_name),mysql_escape_string($_POST['mail_body']),$_GET['send_draft']);
				// echo $update_drafts_sql;
				// var_dump($_POST);
				//mysql_query($update_drafts_sql);
			// }
		}
	}
	
	if($_POST['selected_users']=='artist_users' || $_POST['selected_users']=='artist_fans')
	{
		$profiles_detail = $newtab->artist_status();
		$details = 'artist@'.$profiles_detail['artist_id'];
		$profiles_detail = $profiles_detail['name'];
		
		$link_unsub = '<div style="background-color: #FFFFFF; height: 5px; display:block;"></div>
	<div style="background-color: #cbcaca; height: 1px; display:block;"></div>
	<div style="background-color: #FFFFFF; height: 5px; display:block;"></div><font size="-1" color="#666666" face="Arial, Helvetica, sans-serif" style="font-size: 11px; color: #666666; font-family:  Geneva, Arial, Helvetica, sans-serif; font-weight: normal;">&#169;2013 Purify Art - <a style="color: black;" href="purifyart.com/example_adv.php?type_unsub=subscrb&alldt='.$details.'">Unsubscribe to '.$profiles_detail.'</a></font>';
	}
	elseif($_POST['selected_users']=='community_users' || $_POST['selected_users']=='community_fans')
	{
		$profiles_detail = $newtab->community_status();
		$details = 'community@'.$profiles_detail['community_id'];
		$profiles_detail = $profiles_detail['name'];
		
		$link_unsub = '<div style="background-color: #FFFFFF; height: 5px; display:block;"></div>
	<div style="background-color: #cbcaca; height: 1px; display:block;"></div>
	<div style="background-color: #FFFFFF; height: 5px; display:block;"></div><font size="-1" color="#666666" face="Arial, Helvetica, sans-serif" style="font-size: 11px; color: #666666; font-family:  Geneva, Arial, Helvetica, sans-serif; font-weight: normal;">&#169;2013 Purify Art - <a style="color: black;" href="purifyart.com/example_adv.php?type_unsub=subscrb&alldt='.$details.'">Unsubscribe to '.$profiles_detail.'</a></font>';
	}
	elseif($_POST['selected_users']=='all_users')
	{
		$details = 'all';
		$profiles_detail = 'Purify Art Updates';
		
		$link_unsub = '<div style="background-color: #FFFFFF; height: 5px; display:block;"></div>
	<div style="background-color: #cbcaca; height: 1px; display:block;"></div>
	<div style="background-color: #FFFFFF; height: 5px; display:block;"></div><font size="-1" color="#666666" face="Arial, Helvetica, sans-serif" style="font-size: 11px; color: #666666; font-family:  Geneva, Arial, Helvetica, sans-serif; font-weight: normal;">&#169;2013 Purify Art - <a style="color: black;" href="purifyart.com/example_all.php?type_unsub=subscrb&alldt='.$details.'">Unsubscribe to '.$profiles_detail.'</a></font>';
	}
	elseif($_POST['selected_users']=='custom_users')
	{
		$details = 'all';
		$profiles_detail = 'Purify Art Updates';
		
		$link_unsub = '';
	}
	elseif($_POST['selected_users']!='all_users' && $_POST['selected_users']!='custom_users')
	{
		$act_users = explode('_',$_POST['selected_users']);
		
		if($act_users[0]=='artist')
		{
			$details = 'artist_project@'.$act_users[3];
			$profiles_detail = $newtab->get_projectdetails('artist_project',$act_users[3]);
		}
		elseif($act_users[0]=='community')
		{
			$details = 'community_project@'.$act_users[3];
			$profiles_detail = $newtab->get_projectdetails('community_project',$act_users[3]);
		}
		$profiles_detail = $profiles_detail['title'];
		
		$link_unsub = '<div style="background-color: #FFFFFF; height: 5px; display:block;"></div>
	<div style="background-color: #cbcaca; height: 1px; display:block;"></div>
	<div style="background-color: #FFFFFF; height: 5px; display:block;"></div><font size="-1" color="#666666" face="Arial, Helvetica, sans-serif" style="font-size: 11px; color: #666666; font-family:  Geneva, Arial, Helvetica, sans-serif; font-weight: normal;">&#169;2013 Purify Art - <a style="color: black;" href="purifyart.com/example_adv.php?type_unsub=subscrb&alldt='.$details.'">Unsubscribe to '.$profiles_detail.'</a></font>';
	
		$chng_from_name = $profiles_detail;
		$chng_from_email = 'no-reply@purifyart.com';
	}
	
	if($_POST['selected_users']=='artist_fans' || $_POST['selected_users']=='community_fans' || $_POST['selected_users']=='artist_users' || $_POST['selected_users']=='community_users')
	{
		$chng_from_name = $profiles_detail;
		$chng_from_email = 'no-reply@purifyart.com';
	}
	elseif($_POST['selected_users']=='all_users' || $_POST['selected_users']=='custom_users')
	{
		$chng_from_name = $_SESSION['name'];
		$chng_from_email = $_SESSION['login_email'];
	}
	
	if($mailstat==1)
	{	
		for($mu=0;$mu<count($mail_to_users);$mu++)
		{
			
			
			$strTo = $mail_to_users[$mu];
			/*
			$mail->AddAddress($strTo);
			//$headers="From: Purify Art < no-reply@purifyart.com>"."\r\n";
			//$from = "Purify Art < no-reply@purifyart.com>";
			//$from = "info@7tech.co.in";
			
			//$strTo = $_POST["to"];
			$strSubject = $_POST['subject'];
			//$strMessage = nl2br($_POST['mail_body']);
			$strMessage = $_POST['mail_body'];
			//$mail->From = "purifyart16@gmail.com";
			$mail->FromName = $_SESSION['name'];
			$mail->AddReplyTo($_SESSION['login_email'], $_SESSION['name']);
			$mail->Subject = $strSubject;
			$mail->Body    = $strMessage;
			*/
			//var_dump($_POST);
			//die;
			$mail_grid = new SendGrid\Mail();
			
			$mail_grid->addTo($strTo)->
					setFromName($chng_from_name)->
					setFrom('no-reply@purifyart.com')->
					setSubject($_POST['subject'])->
					setHtml($_POST['mail_body'].$link_unsub);
			
			//*** Uniqid Session ***//
		//	$strSid = md5(uniqid(time()));
		//	$strSid_n = md5(uniqid(time()));

		//	$strHeader = "";
		//	$strHeader .= "from: Purify Art < no-reply@purifyart.com>"."\r\n";
			
			//$strHeader .= "Reply-To: mithesh.kavrani@iniksha.com";

			// $strHeader .= "MIME-Version: 1.0\r\n";
			//$strHeader .= "Content-Type: multipart/mixed; boundary=\"".$strSid."\"\r\n";
			//$strHeader .= "This is a multi-part message in MIME format.\n";
			
			
			/* $strHeader .= "--".$strSid."\n";
			$strHeader .= "Content-type: text/html; charset=utf-8\r\n";
			$strHeader .= "Content-Transfer-Encoding: 7bit\r\n"; */
			
			//$strHeader .= "MIME-Version: 1.0\r\n";
			//$strHeader .= "Content-Type: multipart/mixed; boundary=\"".$strSid."\"\r\n";
			//$strHeader .= "This is a multi-part message in MIME format.\n";
			
			//*** Attachment ***//
			if((isset($attachment_draft_ans['attachments']) && $attachment_draft_ans['attachments']!="") || (isset($_FILES["attachment"]["name"]) && $_FILES["attachment"]["name"]!=""))
			{	
			//	$strHeader .= "MIME-Version: 1.0\r\n";
			//	$strHeader .= "Content-Type: multipart/mixed; ";
			//	$strHeader .= "boundary=$strSid\r\n";
			//	$strHeader .= "--$strSid\r\n";
				if($_FILES["attachment"]["name"] != "")
				{	
					//$strHeader .= "--".$strSid_n."\n";
					//$strHeader .= "Content-type: text/html; charset=utf-8\r\n";
					//$strHeader .= "MIME-Version: 1.0\r\n";
					//$strHeader .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
					//$strHeader .= "Content-Transfer-Encoding: 7bit\r\n";
					/* $strHeader .= "Content-type: text/html; charset=utf-8\r\n";
					$strHeader .= "Content-Transfer-Encoding: 7bit\r\n";
					
					//$strHeader .= "Content-Type: multipart/mixed; boundary=\"".$strSid."\"\r\n";
					$strHeader .= "This is a multi-part message in MIME format.\n";
					$strHeader .= $strMessage."\n\n";  */
					
				//	$strHeader .= "Content-Type: text/html; charset=utf-8\r\n";
				//	$strHeader .= "Content-Transfer-Encoding: 8bit\r\n\n";
				//	$strHeader .= "$strMessage\r\n";
				//	$strHeader .= "--$strSid\r\n";
				
					$strFilesName = $_FILES["attachment"]["name"];
				//	$encoded_content = chunk_split(base64_encode(file_get_contents("uploads/mail attachments/".$attach_name))); 
					/*$strHeader .= "--".$strSid."\n";
					$strHeader .= "Content-Type: application/octet-stream; name=\"".$strFilesName."\"\n"; 
					$strHeader .= "Content-Transfer-Encoding: base64\n";
					$strHeader .= "Content-Disposition: attachment; filename=\"".$strFilesName."\"\n\n";
					$strHeader .= $strContent."\n\n"; */
					
				//	$strHeader .= "Content-Type: application/octet-stream; ";
				//	$strHeader .= "name=\"".$strFilesName."\"\n";
				//	$strHeader .= "Content-Transfer-Encoding:base64\r\n";
				//	$strHeader .= "Content-Disposition:attachment; ";
				//	$strHeader .= "filename=\"".$strFilesName."\"\n\n";
				//	$strHeader .= "$encoded_content\r\n";
				//	$strHeader .= "--$strSid--";
				
				//$mail->AddAttachment("uploads/mail attachments/".$attach_name, $strFilesName);
				$mail_grid->addAttachment("uploads/mail attachments/".$attach_name);
				}
				else
				{
					//$strHeader .= "--".$strSid_n."\n";
					//$strHeader .= "Content-type: text/html; charset=utf-8\r\n";
					//$strHeader .= "Content-Transfer-Encoding: 7bit\r\n";
					/* $strHeader .= "Content-type: text/html; charset=utf-8\r\n";
					$strHeader .= "Content-Transfer-Encoding: 7bit\r\n";
				
					$strHeader .= "MIME-Version: 1.0\r\n";
					//$strHeader .= "Content-Type: multipart/mixed; boundary=\"".$strSid."\"\r\n";
					$strHeader .= "This is a multi-part message in MIME format.\n";
					$strHeader .= $strMessage."\n\n"; 
				
					$strFilesName = $attach_name;
					$strContent = chunk_split(base64_encode(file_get_contents("uploads/mail attachments/".$attach_name))); 
					$strHeader .= "--".$strSid."\n";
					$strHeader .= "Content-Type: application/octet-stream; name=\"".$strFilesName."\"\n"; 
					$strHeader .= "Content-Transfer-Encoding: base64\n";
					$strHeader .= "Content-Disposition: attachment; filename=\"".$strFilesName."\"\n\n";
					$strHeader .= $strContent."\n\n"; */
					
				//	$strHeader .= "Content-Type: text/html; charset=utf-8\r\n";
				//	$strHeader .= "Content-Transfer-Encoding: 8bit\r\n\n";
				//	$strHeader .= "$strMessage\r\n";
				//	$strHeader .= "--$strSid\r\n"; 
				
					$strFilesName = $attach_name;
				//	$encoded_content = chunk_split(base64_encode(file_get_contents("uploads/mail attachments/".$attach_name))); 
					/*$strHeader .= "--".$strSid."\n";
					$strHeader .= "Content-Type: application/octet-stream; name=\"".$strFilesName."\"\n"; 
					$strHeader .= "Content-Transfer-Encoding: base64\n";
					$strHeader .= "Content-Disposition: attachment; filename=\"".$strFilesName."\"\n\n";
					$strHeader .= $strContent."\n\n"; */
					
				//	$strHeader .= "Content-Type: application/octet-stream; ";
				//	$strHeader .= "name=\"".$strFilesName."\"\n";
				//	$strHeader .= "Content-Transfer-Encoding:base64\r\n";
				//	$strHeader .= "Content-Disposition:attachment; ";
				//	$strHeader .= "filename=\"".$strFilesName."\"\n\n";
				//	$strHeader .= "$encoded_content\r\n";
				//	$strHeader .= "--$strSid--";
				
				//$mail->AddAttachment("uploads/mail attachments/".$attach_name, $strFilesName);
				$mail_grid->addAttachment("uploads/mail attachments/".$attach_name);
				}
			}
			else
			{
				//$strHeader .= "--".$strSid."\n";
				//$strHeader .= "MIME-Version: 1.0\r\n";
				//$strHeader .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
				//$strHeader .= "Content-type: text/html; charset=utf-8\r\n";
				//$strHeader .= "Content-Transfer-Encoding: 7bit\r\n";
				//$strHeader .= $strMessage."\n\n"; 
			}
			//mail($strTo,$strSubject,"",$strHeader);
			//if($mail->Send()){
							//$sentmail = true;
						//}
						//$sendgrid->web->send($mail_grid);
						$sendgrid -> smtp -> send($mail_grid); 
			
				
			
			//$sentmail = mail($to,$_POST['subject'],$_POST['mail_body'],$header);
		}
		?>
			<script>
				alert("Your Mail has been sent successfully.");
				//window.location = "compose.php";
			</script>
				<?php
	}
	//header("Location: compose.php");
}
?>
