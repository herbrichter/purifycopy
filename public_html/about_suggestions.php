<?php
	ob_start();
	include_once('commons/session_check.php');
	include_once('classes/Suggestion.php');
	include_once('classes/Email.php');
		
	session_start();
	$username = $_SESSION['username'];
	if(!$username == '')
	{
		$login_flag=1;
	}
	
	if($login_flag) include_once('loggedin_includes.php');
	else include_once('login_includes.php');
	
	if($_GET['done'])
	{
		$done=$_GET['done'];
	}
	if(isset($_POST['submitsugg']))
	{
		$name=$_POST['name'];
		$email=$_POST['email'];
		$sugg=$_POST['sugg'];
		
		$obj=new Suggestion();
		if($sugg_id=$obj->addSuggestion($name,$email,$sugg))
		{
			$obj2=new Email();
			$obj2->name=$name;
			$obj2->sugg=$sugg;
			$obj2->from=$email;
			$obj->sugg_id=$sugg_id;
			$obj2->send();
			header('location:about_suggestions.php?done=1');			
		}
		else
		{
			header('location:about_suggestions.php?done=2');
		}
	}
?>

<?php include_once('includes/header.php'); ?>
<title>Purify Entertainment: About</title>
<script type="text/javascript" language="javascript">
function suggsubmit()
{
	$name=document.getElementById('name');
	$email=document.getElementById('email');
	$sugg=document.getElementById('sugg');
	$sugg_form=document.getElementById('sugg_form');
	
	if($name == '' || $email == '' || $sugg == '')
	{
		alert('Please fill all the fields.');
	}
	else
	{
		$sugg_form.submit();
	}			
}
</script>
	
  <div id="contentContainer">
    <div id="subNavigation">
      <div id="sectionTitle"><a href="about.html">About Us</a></div>
      <ul>
        <li><a href="affiliates.html">Affiliate Program</a></li>
        <li><a href="about_agreements.html">Agreements</a><a href="busplan.html"></a></li>
        <li><a href="about_contacts.html">Contact Us</a></li>
        <li><a href="../Design/about_news.php">News History</a></li>
        <li><a href="../Design/about_newsletter.php">Newsletter</a></li>
        <li><a href="../Design/about_quotes.php">Quotes</a></li>
        <li><a href="../Design/about_suggestions.php">Suggestions</a></li>
        <li><a href="../Design/about_team.php">Team</a></li>
      </ul>
    </div>
    <div id="actualContent">
      <h1>Suggestions</h1>
<?php
   if(!$done)
   {
?>
      <p>Please fill out the following form if you have any suggestions, comments, or insights. Thank you. </p>
      <form id="sugg_form" name="sugg_form" method="post" action="">
        <table width="502" border="0" cellpadding="0" cellspacing="0">
          <tr>
            <td width="100">Name</td>
            <td width="402"><input name="name" type="text" class="regularField" id="name" /></td>
          </tr>
          <tr>
            <td colspan="2"><img src="images/spacer.gif" alt=" " width="10" height="10" /></td>
          </tr>
          <tr>
            <td>Email</td>
            <td><input name="email" type="text" class="regularField" id="email" /></td>
          </tr>
          <tr>
            <td colspan="2"><img src="images/spacer.gif" alt=" " width="10" height="10" /></td>
          </tr>
          <tr>
            <td valign="top">Suggestion</td>
            <td><textarea name="sugg" rows="10" class="regularField" id="sugg"></textarea></td>
          </tr>
          <tr>
            <td colspan="2"><img src="images/spacer.gif" alt=" " width="10" height="10" /></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td align="right"><input type="button" name="submitsugg" id="submitsugg" value="Submit Suggestion" onclick="suggsubmit();" />
            <input type="hidden" name="submitsugg" />
            </td>
          </tr>
        </table>
      </form>
<?php
	}
	else if($done==1)
	{
?>
	<p>Thank you! Your suggestion is submitted successfully.<br />
       <a href="about_suggestions.php" title="Suggestions">Click here</a> to post another suggestion, comment or insight.
	</p>
<?php
	}
	else
	{
?>
	<p class="hintRed">Sorry! We are unable to process your request right now, please try again later.</p>
<?php
	}
?>


    </div>
    
    <div class="clearMe"></div>
    
  </div>
  
<?php include_once('includes/footer.php'); ?>