﻿/***************************/
//@Author: Adrian "yEnS" Mato Gondelle
//@website: www.yensdesign.com
//@email: yensamg@gmail.com
//@license: Feel free to use it, but keep this credits please!					
/***************************/

//SETTING UP OUR POPUP
//0 means disabled; 1 means enabled;
var popupStatus = 0;
var popupStatus_msg = 0;
var popupStatus_member = 0;
var popupStatus_subs = 0;
var popupStatus_fanclub = 0;
var popupStatus_fanclub_proceed = 0;
var popupStatus_desc = 0;


//loading popup with jQuery magic!
function loadPopup(){
	//loads popup only if it is disabled
	if(popupStatus==0){
		$("#backgroundPopup").css({
			"opacity": "0.7"
		});
		$("#backgroundPopup").fadeIn("slow");
		$("#popupContact").fadeIn("slow");
		//$("#popupContact").centerScreen();
		popupStatus = 1;
	}
}

//disabling popup with jQuery magic!
function disablePopup(){
	//disables popup only if it is enabled
	if(popupStatus==1){
		$("#backgroundPopup").fadeOut("slow");
		$("#popupContact").fadeOut("slow");
		popupStatus = 0;
	}
}

//centering popup
function centerPopup(){
	//request data for centering
	var windowWidth = document.documentElement.clientWidth;
	var windowHeight = document.documentElement.clientHeight;
	var popupHeight = $("#popupContact").height();
	var popupWidth = $("#popupContact").width();
	//centering
	$("#popupContact").css({
		"top": 200, //windowHeight/2-popupHeight/2,
		"left": windowWidth/2-popupWidth/2
	});
	//only need force for IE6
	
	$("#backgroundPopup").css({
		"height": windowHeight
	});
	
}

function loadPopup_desc(){
	//loads popup only if it is disabled
	if(popupStatus_desc==0){
		$("#backgroundPopup").css({
			"opacity": "0.7"
		});
		$("#backgroundPopup").fadeIn("slow");
		$("#popupdescription").fadeIn("slow");
		//$("#popupdescription").centerScreen();
		popupStatus_desc = 1;
	}
}

function centerPopup_desc(){
	//request data for centering
	var windowWidth = document.documentElement.clientWidth;
	var windowHeight = document.documentElement.clientHeight;
	var popupHeight = $("#popupdescription").height();
	var popupWidth = $("#popupdescription").width();
	//centering
	$("#popupdescription").css({
		"top": 100, //windowHeight/2-popupHeight/2,
		"left": windowWidth/2-popupWidth/2
	});
	//only need force for IE6
	
	$("#backgroundPopup").css({
		"height": windowHeight
	});
	
}

function disablePopup_desc(){
	//disables popup only if it is enabled
	if(popupStatus_desc==1){
		$("#backgroundPopup").fadeOut("slow");
		$("#popupdescription").fadeOut("slow");
		popupStatus_desc = 0;
	}
}

//CONTROLLING EVENTS IN jQuery
$(document).ready(function(){
	
	//LOADING POPUP
	//Click the button event!
	$("#button").click(function(){
		//centering with css
		centerPopup();
		//load popup
		loadPopup();
	});

	//CLOSING POPUP
	//Click the x event!
	$("#popupContactClose").click(function(){
		disablePopup();
	});
	//Click out event!
	$("#backgroundPopup").click(function(){
		disablePopup();
	});
	//Press Escape event!
	$(document).keypress(function(e){
		if(e.keyCode==27 && popupStatus==1){
			disablePopup();
		}
	});


 jQuery.fn.centerScreen = function(loaded) {
	var obj = this;
	if(!loaded) {
			obj.css('top', $(window).height()/2-this.height()/2+$(window).scrollTop());
			obj.css('left', $(window).width()/2-this.width()/2+$(window).scrollLeft());
		$(window).resize(function(){ obj.centerScreen(!loaded); });
	} else {
			obj.stop();
			obj.animate({ top: $(window).height()/2-this.height()/2, left: $(window).width()/2-this.width()/2}, 200, 'linear');
	}
 }

}); 


$(document).ready(function(){
	
	//LOADING POPUP
	//Click the button event!
	$("#button").click(function(){
		//centering with css
		centerPopup_desc();
		//load popup
		loadPopup_desc();
	});

	//CLOSING POPUP
	//Click the x event!
	$("#popupContactClose_desc").click(function(){
		disablePopup_desc();
	});
	//Click out event!
	$("#backgroundPopup").click(function(){
		disablePopup_desc();
	});
	//Press Escape event!
	$(document).keypress(function(e){
		if(e.keyCode==27 && popupStatus_desc==1){
			disablePopup_desc();
		}
	});


 jQuery.fn.centerScreen = function(loaded) {
	var obj = this;
	if(!loaded) {
			obj.css('top', $(window).height()/2-this.height()/2+$(window).scrollTop());
			obj.css('left', $(window).width()/2-this.width()/2+$(window).scrollLeft());
		$(window).resize(function(){ obj.centerScreen(!loaded); });
	} else {
			obj.stop();
			obj.animate({ top: $(window).height()/2-this.height()/2, left: $(window).width()/2-this.width()/2}, 200, 'linear');
	}
 }

}); 

//*****************************************************MESSAGES******************************************//

function loadPopup_msg(){
	//loads popup only if it is disabled
	/* $(".header").css({
		"position": "inherit",
	}); */
	if(popupStatus_msg==0){
		$("#backgroundPopup").css({
			"opacity": "0.7"
		});
		$("#backgroundPopup").fadeIn("slow");
		$("#messages_send").fadeIn("slow");
		//$("#popupContact").centerScreen();
		popupStatus_msg = 1;
	}
}

//disabling popup with jQuery magic!
function disablePopup_msg(){
	/* $(".header").css({
		"position": "inherit",
	}); */
	//disables popup only if it is enabled
	if(popupStatus_msg==1){
		$("#backgroundPopup").fadeOut("slow");
		$("#messages_send").fadeOut("slow");
		popupStatus_msg = 0;
	}
}

//centering popup
function centerPopup_msg(){
	//request data for centering
	var windowWidth = document.documentElement.clientWidth;
	var windowHeight = document.documentElement.clientHeight;
	var popupHeight = $("#messages_send").height();
	var popupWidth = $("#messages_send").width();
	//centering
	$("#messages_send").css({
		"top": 21, //windowHeight/2-popupHeight/2,
		"left": windowWidth/2-popupWidth/2
	});
	//only need force for IE6
	
	$("#backgroundPopup").css({
		"height": windowHeight
	});
	
}

$(document).ready(function(){
	
	//LOADING POPUP
	//Click the button event!
	$("#button").click(function(){
		//centering with css
		centerPopup_msg();
		//load popup
		loadPopup_msg();
	});

	//CLOSING POPUP
	//Click the x event!
	$("#popupContactClose_msg").click(function(){
		disablePopup_msg();
	});
	//Click out event!
	$("#backgroundPopup").click(function(){
		disablePopup_msg();
	});
	//Press Escape event!
	$(document).keypress(function(e){
		if(e.keyCode==27 && popupStatus_msg==1){
			disablePopup_msg();
		}
	});


 jQuery.fn.centerScreen = function(loaded) {
	var obj = this;
	if(!loaded) {
			obj.css('top', $(window).height()/2-this.height()/2+$(window).scrollTop());
			obj.css('left', $(window).width()/2-this.width()/2+$(window).scrollLeft());
		$(window).resize(function(){ obj.centerScreen(!loaded); });
	} else {
			obj.stop();
			obj.animate({ top: $(window).height()/2-this.height()/2, left: $(window).width()/2-this.width()/2}, 200, 'linear');
	}
 }

});


//////////////////////////////////// *************Memberships***** //////////////////////////////////

function loadPopup_member(){
	//loads popup only if it is disabled
	/* $(".header").css({
		"position": "inherit",
	}); */
	if(popupStatus_member==0){
		$("#backgroundPopup_member").css({
			"opacity": "0.7"
		});
		$("#backgroundPopup_member").fadeIn("slow");
		$("#memberships_box").fadeIn("slow");
		//$("#popupContact").centerScreen();
		popupStatus_member = 1;
	}
}

//disabling popup with jQuery magic!
function disablePopup_member(){
	/* $(".header").css({
		"position": "inherit",
	}); */
	//disables popup only if it is enabled
	if(popupStatus_member==1){
		$("#backgroundPopup_member").fadeOut("slow");
		$("#memberships_box").fadeOut("slow");
		popupStatus_member = 0;
	}
}

//centering popup
function centerPopup_member(){
	//request data for centering
	var windowWidth = document.documentElement.clientWidth;
	var windowHeight = document.documentElement.clientHeight;
	var popupHeight = $("#memberships_box").height();
	var popupWidth = $("#memberships_box").width();
	//centering
	$("#memberships_box").css({
		"top": 200, //windowHeight/2-popupHeight/2,
		"left": windowWidth/2-popupWidth/2
	});
	//only need force for IE6
	
	$("#backgroundPopup_member").css({
		"height": windowHeight
	});
	
}

$(document).ready(function(){
	
	//LOADING POPUP
	//Click the button event!
	$("#button").click(function(){
		//centering with css
		centerPopup_member();
		//load popup
		loadPopup_member();
	});

	//CLOSING POPUP
	//Click the x event!
	$("#popupContactClose_member").click(function(){
		disablePopup_member();
	});
	//Click out event!
	$("#backgroundPopup_member").click(function(){
		disablePopup_member();
	});
	//Press Escape event!
	$(document).keypress(function(e){
		if(e.keyCode==27 && popupStatus_member==1){
			disablePopup_member();
		}
	});


 jQuery.fn.centerScreen = function(loaded) {
	var obj = this;
	if(!loaded) {
			obj.css('top', $(window).height()/2-this.height()/2+$(window).scrollTop());
			obj.css('left', $(window).width()/2-this.width()/2+$(window).scrollLeft());
		$(window).resize(function(){ obj.centerScreen(!loaded); });
	} else {
			obj.stop();
			obj.animate({ top: $(window).height()/2-this.height()/2, left: $(window).width()/2-this.width()/2}, 200, 'linear');
	}
 }

});