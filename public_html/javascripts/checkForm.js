// JavaScript Document
var r={
  'special':/[\W]/g,
  'quotes':/['\''&'\"']/g,
  'notnumbers':/[^\d]/g
}
function m_valid(o,w){
  o.value = o.value.replace(r[w],'');
}
function checkForm()
{
 if(document.form.Fname.value=="")
 {
  alert("First Name can not be blank");
  document.form.Fname.focus();
  return false;
 }
 if(document.form.emailerror.value !="")
 {
  alert("Sorry, "+$("#email").val()+" is already registered. If you forgot your password than use the Password Help link in the header. If you deleted your account and would like to reregister than email Bryan@purifyart.net.");
  document.form.emailerror.focus();
  return false;
 }
 if(document.form.Lname.value=="")
 {
  alert("Last Name can not be blank");
  document.form.Lname.focus();
  return false;
 }
 if(document.form.email.value=="")
 {
  alert("Email can not be blank");
  document.form.email.focus();
  return false;
 }
 else if(!IsValidEmail(document.form.email.value))
{
  alert("Invalid Email Id");
 document.form.email.focus();
  return false;  
 }
 if(document.form.email.value!=document.form.remail.value)
 {
 alert("Please Re-enter Email Id");
 document.form.remail.focus();
  return false; 
 	
 }

 if(!validatePwd())
 {
    return false;
 }
 
 else
 {
  return true;
 }
}

function validatePwd() {
var invalid = " "; // Invalid character is a space
var minLength = 6; // Minimum length
var pw1 = document.form.pass.value;
var pw2 = document.form.rpass.value;

// check for minimum length
if (document.form.pass.value.length < minLength) {
alert('Your password must be at least ' + minLength + ' characters long. Try again.');
document.form.pass.focus();
return false;
}
// check for a value in both fields.
if (pw1 == '' || pw2 == '') {
alert('Please Re-enter your password twice ');
document.form.rpass.focus();
return false;
}
// check for spaces
if (document.form.pass.value.indexOf(invalid) > -1) {
alert("Sorry, spaces are not allowed.");
document.form.pass.focus();
return false;
}
else {
if (pw1 != pw2) {
alert ("You did not enter the same new password twice. Please re-enter your password.");
document.form.rpass.focus();
return false;
}

if ( document.form.terms.checked == false )
    {
        alert ( "Please check the box." );
		document.form.terms.focus();
        return false;
    }


else {

return true;
      }
   }
}

function IsValidEmail(strValue)
{
 //var objRegExp  =/(^[a-z0-9]([a-z0-9_\.]*)@([a-z0-9_\-\.]*)([.][a-z]{3})$)|(^[a-z0-9]([a-z0-9_\.]*)@([a-z0-9_\-\.]*)(\.[a-z]{2,3})(\.[a-z]{2})*$)/i;
 var objRegExp  = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
 return objRegExp.test(strValue);
}

function checkfield(x)
{
	document.getElementById(x).value="";
}

