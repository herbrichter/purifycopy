<?php
$dir = dirname(__FILE__);
$count=0;
$path = $dir.'/playlist/';
if ($handle = opendir($path)) {
    while (false !== ($file = readdir($handle))) { 
		if (fnmatch("playlist*",$file))
		{
			$filelastmodified = filemtime($path . $file);
			//24 hours in a day * 3600 seconds per hour
			if((time() - $filelastmodified) > 24*3600)
			{
				//echo "Deleting file - ".$path.$file.PHP_EOL;
			    unlink($path . $file);
                $count++;
			} else {
				//echo "Skipping new file - ".$path.$file.PHP_EOL; 
			}
		} else {
			//echo "Skipping file not playlist - ".$path.$file.PHP_EOL; 
		}
    }

    echo "All Good - ".$count." files deleted".
    
    closedir($handle); 
}
?>