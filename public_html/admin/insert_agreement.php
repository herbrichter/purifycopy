<?php
include('classes/AddAgreement.php');

$new_obj = new AddAgreement();
?>
<head>
<link href="includes/purify-admin.css" media="screen" type="text/css" rel="stylesheet">
</head>
<body>
<div id="actualContent">

		<div class="fieldCont">
			<div class="fieldTitle"></div>
			<?php
				if(isset($_REQUEST['edit']) && $_REQUEST['edit']!="")
				{
					$get_agreement = $new_obj->Select_Agreement_Id($_REQUEST['edit']);
					echo "<h1>Edit Agreement</h1>";
				}
				else
				{
					echo "<h1>Add Agreement</h1>";
				}
			?>
			
		</div>
	<form method="POST" action="insert_agreement.php?edit=<?php if(isset($_REQUEST['edit'])) {echo $_REQUEST['edit'];}?>">
		<div class="fieldCont">
		  <div class="fieldTitle">Agreement Title</div>
		  <input type="text" id="agreementtitle" class="fieldText" name="agreementtitle" value ="<?php if(isset($_REQUEST['edit'])){echo $get_agreement['agreement_title'];}?>"/>
		</div>
		<div class="fieldCont">
		  <div class="fieldTitle">Agreement</div>
		  <textarea id="agreement" name="agreement"><?php if(isset($_REQUEST['edit'])){echo $get_agreement['agreement'];}?></textarea>
		</div>
		<div class="fieldCont">
		  <div class="fieldTitle"></div>
		  <input type="submit" value="Submit"></input>
		</div>
	</form>
</div>

</body>

<?php
if (isset($_POST) && $_POST !="" && $_POST !=null)
{

	if(!isset($_REQUEST['edit']) || $_REQUEST['edit'] == null || $_REQUEST['edit']=="")
	{
		$add = $new_obj->Agreement($_POST['agreementtitle'],$_POST['agreement']);
		//var_dump($_POST);
		?>
		
		<script src="fancybox/jquery-1.4.3.min.js" type="text/javascript"></script>
		<script type="text/javascript">
		$("document").ready(function(){
			parent.location.reload();
			parent.jQuery.fancybox.close();
		});
		</script>
	<?php
	}
	else
	{
		$Edit_agreement = $new_obj->Edit_Agreement($_POST['agreementtitle'],$_POST['agreement'],$_REQUEST['edit']);
		?>
		
		<script src="fancybox/jquery-1.4.3.min.js" type="text/javascript"></script>
		<script type="text/javascript">
		$("document").ready(function(){
			parent.location.reload();
			parent.jQuery.fancybox.close();
		});
		</script>
		<?php
	}
}
?>