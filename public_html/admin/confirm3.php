<?php
	include_once('../classes/SubType.php');
	include_once('../classes/Type.php');
	include_once('../classes/UserType.php');	

	$type_id=$_GET['id'];
	$obj=new Type();
	$row=$obj->getType($type_id);
	$obj2=new SubType();
	$obj3=new UserType();
	
	$flag=0;
	$msg="Are you sure you want to remove this type from ".$row['parent']."?";
	
	if(isset($_POST['yes']))
	{
		$rs=$obj->removeType($type_id);
		$rs2=$obj2->removeSubTypeByType($type_id);
		$rs3=$obj3->removeUserType($type_id);
		$flag=1;
		if($rs)	$msg="Type removed successfully.";
		else $msg="Error in removing type. Please try again.";
	}
?>
<html>
<head>
<title>Confirm Remove User</title>
<link rel="stylesheet" type="text/css" media="screen" href="includes/purify.css" />
<script language="javascript" type="text/javascript">
function closeWindow()
{
	window.opener.location.reload();
	window.close();
}
</script>
</head>
<div id="confirmBox" align="center">
<?php echo $msg; ?><br />
<p align="center">
<table align="center" width="200">
<tr>
  	<td colspan="2" bgcolor="#FFFFFF"><img src="../images/spacer.gif" alt=" " width="10" height="10" /></td>
</tr>
<tr>
  	<td colspan="2" bgcolor="#FFFFFF"><img src="../images/spacer.gif" alt=" " width="10" height="10" /></td>
</tr>
<tr>
  	<td colspan="2" bgcolor="#FFFFFF"><img src="../images/spacer.gif" alt=" " width="10" height="10" /></td>
</tr>
<tr>
  <?php
  	if(!$flag)
	{
		echo <<<EOD
	  <form name="confirm" method="post" action="">
		<td width="100" align="right"><input name="yes" type="submit" value="   Yes   "></td>
		<td width="100" align="left"><input name="no" type="button" value="   No   " onClick="window.close();"></td>
	  </form>
EOD;
	}
	else
	{
		echo "<td colspan='2' align='center'><a class='hintConfirmBox' href='#' onClick='closeWindow();'>Close</a></td>";	
	}
  ?>
</tr>
</table>

</p>
</div>
</html>