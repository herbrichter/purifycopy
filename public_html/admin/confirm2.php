<?php
	include_once('../classes/SubType.php');
	include_once('../classes/Type.php');
	include_once('../classes/UserType.php');	

	$subtype_id=$_GET['id'];
	$obj=new SubType();
	$row=$obj->getSubtype($subtype_id);
	$obj2=new Type();
	$type=$obj2->getTypeById($row['type_id']);
	$obj3=new UserType();	
	
	$flag=0;
	$msg="Are you sure you want to remove this subtype from ".$type."?";
	
	if(isset($_POST['yes']))
	{
		$rs=$obj->removeSubType($subtype_id);
		$rs2=$obj3->removeUserSubType($row['type_id'],$subtype_id);		
		$flag=1;
		if($rs)	$msg="Subtype removed successfully.";
		else $msg="Error in removing subtype. Please try again.";
	}
?>
<html>
<head>
<title>Confirm Remove User</title>
<link rel="stylesheet" type="text/css" media="screen" href="includes/purify.css" />
<script language="javascript" type="text/javascript">
function closeWindow()
{
	window.opener.location.reload();
	window.close();
}
</script>
</head>
<div id="confirmBox" align="center">
<?php echo $msg; ?><br />
<p align="center">
<table align="center" width="200">
<tr>
  	<td colspan="2" bgcolor="#FFFFFF"><img src="../images/spacer.gif" alt=" " width="10" height="10" /></td>
</tr>
<tr>
  	<td colspan="2" bgcolor="#FFFFFF"><img src="../images/spacer.gif" alt=" " width="10" height="10" /></td>
</tr>
<tr>
  	<td colspan="2" bgcolor="#FFFFFF"><img src="../images/spacer.gif" alt=" " width="10" height="10" /></td>
</tr>
<tr>
  <?php
  	if(!$flag)
	{
		echo <<<EOD
	  <form name="confirm" method="post" action="">
		<td width="100" align="right"><input name="yes" type="submit" value="   Yes   "></td>
		<td width="100" align="left"><input name="no" type="button" value="   No   " onClick="window.close();"></td>
	  </form>
EOD;
	}
	else
	{
		echo "<td colspan='2' align='center'><a class='hintConfirmBox' href='#' onClick='closeWindow();'>Close</a></td>";	
	}
  ?>
</tr>
</table>

</p>
</div>
</html>