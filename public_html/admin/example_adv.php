<?php
//Logging in with Google accounts requires setting special identity, so this example shows how to do it.
	include_once('commons/db_new.php');
    $domainname=$_SERVER['SERVER_NAME'];
	//if(!isset($_POST))
	//{
	$date = date('Y-m-d');
	$futureDate = date('Y-m-d', strtotime('+1 year'));
?>
	<link rel="stylesheet" type="text/css" media="screen" href="../includes/purify.css" />
		<div id="outerContainer">
			<div id="purifyMasthead">
				<div id="purifyLogo">
					<a href="index.php"><img src="../images/purelogo.gif" alt="Purify Entertainment" width="302" height="60" border="0"/></a>
				</div>
			</div>
<?php
		if($_POST['sub_news']=="")
		{
			require 'openid.php';
		
			try
			{
				# Change 'localhost' to your domain name.
				$openid = new LightOpenID($domainname);
				 
				//Not already logged in
				if(!$openid->mode)
				{
					//The google openid url
					//https://me.yahoo.com/[your_username]
					$openid->identity = 'https://www.google.com/accounts/o8/id';
					 
					//Get additional google account information about the user , name , email , country
					$openid->required = array('contact/email' , 'namePerson/first' , 'namePerson/last' , 'pref/language' , 'contact/country/home');
					 
					//start discovery
					header('Location: ' . $openid->authUrl());
				}
				 
				else if($openid->mode == 'cancel')
				{
					echo 'User has canceled authentication!';
					//redirect back to login page ??
				}
				 
				//Echo login information by default
				else
				{
					if($openid->validate())
					{
						//User logged in
						$d = $openid->getAttributes();
						 
						$first_name = $d['namePerson/first'];
						$last_name = $d['namePerson/last'];
						$email = $d['contact/email'];
						$language_code = $d['pref/language'];
						$country_code = $d['contact/country/home'];
						 
						$data = array(
							'first_name' => $first_name ,
							'last_name' => $last_name ,
							'email' => $email ,
						);
	 
						//now signup/login the user.
						//process_google_data($data);
					}
					else
					{
						//user is not logged in
					}
				}
			}

			catch(ErrorException $e)
			{
				echo $e->getMessage();
			}
	//}
	//var_dump($data);
	
		if(isset($_GET['alldt']))
		{
			if($_GET['alldt']!="")
			{
				$details_get = explode('@',$_GET['alldt']);
				if($details_get[0]=='artist')
				{
					$sql = mysql_query("SELECT * FROM general_artist WHERE artist_id='".$details_get[1]."'");
				}
				elseif($details_get[0]=='community')
				{
					$sql = mysql_query("SELECT * FROM general_community WHERE community_id='".$details_get[1]."'");
				}
				elseif($details_get[0]=='artist_project' || $details_get[0]=='community_project')
				{
					$sql = mysql_query("SELECT * FROM $details_get[0] WHERE id='".$details_get[1]."'");
				}
				
				if(isset($sql))
				{
					if($sql!="" && $sql!=NULL)
					{
						if(mysql_num_rows($sql)>0)
						{
							$ans_unsub_dts = mysql_fetch_assoc($sql);
						}
					}
				}
			}
		}
?>
			<div id="actualContent" style="margin-left: 222px; margin-top: 19px;">
				<h1>Subscription</h1>
				<?php
					if(!isset($_GET['type_unsub']))
					{
				?>
						<div class="fieldCont">Subscribe to updates from Purify Art on new services, changes to site, and promotional opportunities.</div>
				<?php
					}
					elseif($details_get[0]=='community' || $details_get[0]=='artist')
					{
				?>
						<div class="fieldCont">Subscribe to updates from <?php echo $ans_unsub_dts['name']; ?> on new services, changes to site, and promotional opportunities.</div>
				<?php
					}
					elseif($details_get[0]=='community_project' || $details_get[0]=='artist_project')
					{
				?>
						<div class="fieldCont">Subscribe to updates from <?php echo $ans_unsub_dts['title']; ?> on new services, changes to site, and promotional opportunities.</div>
				<?php
					}
				?>
			
				<div class="list-wrap" id="list_height_incr" style="">
					<form method="POST" action="">
						<div class="fieldCont">
							<div class="fieldTitle">Email</div>
							<div class="chkCont">
								<input type="radio" checked class="chk" name="sub_news" value="on"/>
								<div class="radioTitle" >On</div>
							</div>
							<div class="chkCont">
								<input type="radio" class="chk" name="sub_news" value="off"/>
								<div class="radioTitle">Off</div>
							</div>
							<input type="hidden" id="unsub_email" name="unsub_email" value="<?php echo $data['email']; ?>" />
							<input type="hidden" id="unsub_details" name="unsub_details" value="<?php if(isset($_GET['alldt'])) { echo $_GET['alldt']; } else { echo ""; } ?>" />
						</div>
						<div class="fieldCont">
							<input type="submit" class="register" style="padding:5px 5px;" value="Save" />
						</div>
					</form>
				</div>
			</div>
		
<?php
		}
		
	if(isset($_POST))
	{
		if($_POST['sub_news']!="" && $_POST['unsub_email']!="")
		{
			if($_POST['unsub_details']!="" && isset($_GET['type_unsub']))
			{
				$details_to_sub = explode('@',$_GET['alldt']);
				
				if($details_to_sub[0]=='artist_project' || $details_to_sub[0]=='community_project')
				{	
					$sql_org = mysql_query("SELECT * FROM $details_to_sub[0] WHERE id='".$details_to_sub[1]."'");
					$ans_org = mysql_fetch_assoc($sql_org);
					
					if(isset($ans_org['artist_id']))
					{
						$sql_get_un = mysql_query("SELECT * FROM email_subscription WHERE email='".$_POST['unsub_email']."' AND related_id='".$ans_org['artist_id'].'_'.$details_to_sub[1]."' AND related_type='".$details_to_sub[0]."'");
						$org_id = $ans_org['artist_id'];
					}
					elseif(isset($ans_org['community_id']))
					{
						$sql_get_un = mysql_query("SELECT * FROM email_subscription WHERE email='".$_POST['unsub_email']."' AND related_id='".$ans_org['community_id'].'_'.$details_to_sub[1]."' AND related_type='".$details_to_sub[0]."'");
						$org_id = $ans_org['community_id'];
					}
				}
				else
				{
					$sql_get_un = mysql_query("SELECT * FROM email_subscription WHERE email='".$_POST['unsub_email']."' AND related_id='".$details_to_sub[1]."' AND related_type='".$details_to_sub[0]."'");
				}
				if(mysql_num_rows($sql_get_un)>0 && $_POST['sub_news']=='off')
				{
					$sql_get_gen = mysql_query("SELECT * FROM general_user WHERE email='".$_POST['unsub_email']."'");
					if($details_to_sub[0]=='artist_project' || $details_to_sub[0]=='community_project')
					{					
						mysql_query("DELETE FROM email_subscription WHERE email='".$_POST['unsub_email']."' AND related_id='".$org_id.'_'.$details_to_sub[1]."' AND related_type='".$details_to_sub[0]."'");
					}
					else
					{
						mysql_query("DELETE FROM email_subscription WHERE email='".$_POST['unsub_email']."' AND related_id='".$details_to_sub[1]."' AND related_type='".$details_to_sub[0]."'");
					}
				}
				elseif(mysql_num_rows($sql_get_un)<=0 && $_POST['sub_news']=='on')
				{
					$sql_get_gen = mysql_query("SELECT * FROM general_user WHERE email='".$_POST['unsub_email']."'");
					if($details_to_sub[0]=='artist')
					{
						$sql_sub_to = mysql_query("SELECT * FROM general_user WHERE artist_id='".$details_to_sub[1]."'");
					}
					elseif($details_to_sub[0]=='community')
					{
						$sql_sub_to = mysql_query("SELECT * FROM general_user WHERE community_id='".$details_to_sub[1]."'");
					}
					elseif($details_to_sub[0]=='artist_project' || $details_to_sub[0]=='community_project')
					{
						if($details_to_sub[0]=='artist_project')
						{
							$sql_sub_to = mysql_query("SELECT * FROM general_user WHERE artist_id='".$org_id."'");
						}
						elseif($details_to_sub[0]=='community_project')
						{
							$sql_sub_to = mysql_query("SELECT * FROM general_user WHERE community_id='".$org_id."'");
						}
					}
					
					
					if(mysql_num_rows($sql_get_gen)>0 && mysql_num_rows($sql_sub_to)>0)
					{
						$ans_get_gen = mysql_fetch_assoc($sql_get_gen);
						$ans_sub_to = mysql_fetch_assoc($sql_sub_to);
						
						if($details_to_sub[0]=='artist_project' || $details_to_sub[0]=='community_project')
						{
							$sql_query = mysql_query("INSERT INTO `email_subscription` (`purchase_date`,`expiry_date`,`name`,`email`,`related_id`,`related_type`,`news_feed`,`email_update`,`subscribe_to_id`) VALUES ('".$date."','".$futureDate."','".$ans_get_gen['fname'].' '.$ans_get_gen['lname']."','".$_POST['unsub_email']."','".$org_id.'_'.$details_to_sub[1]."','".$details_to_sub[0]."',1,1,'".$ans_sub_to['general_user_id']."')");
						}
						else
						{
							$sql_query = mysql_query("INSERT INTO `email_subscription` (`purchase_date`,`expiry_date`,`name`,`email`,`related_id`,`related_type`,`news_feed`,`email_update`,`subscribe_to_id`) VALUES ('".$date."','".$futureDate."','".$ans_get_gen['fname'].' '.$ans_get_gen['lname']."','".$_POST['unsub_email']."','".$details_to_sub[1]."','".$details_to_sub[0]."',1,1,'".$ans_sub_to['general_user_id']."')");
						}
					}
				}
				elseif(mysql_num_rows($sql_get_un)>0 && $_POST['sub_news']=='on')
				{
					$ans_get_sub_dts = mysql_fetch_assoc($sql_get_un);
					if($ans_get_sub_dts['expiry_date']>$date)
					{
						mysql_query("UPDATE email_subscription SET expiry_date='".$futureDate."',purchase_date='".$date."' WHERE id='".$ans_get_sub_dts['id']."'");
					}
				}
			}
			else
			{
				$sql_get_un = mysql_query("SELECT * FROM unsubscribe_newsletter WHERE email='".$_POST['unsub_email']."'");
				if(mysql_num_rows($sql_get_un)<=0 && $_POST['sub_news']=='off')
				{
					$sql_get_gen = mysql_query("SELECT * FROM general_user WHERE email='".$_POST['unsub_email']."'");
					if(mysql_num_rows($sql_get_gen)>0)
					{
						$ans_get_gen = mysql_fetch_assoc($sql_get_gen);
						$sql_query = mysql_query("INSERT INTO `unsubscribe_newsletter` (`general_user_id` ,`email`) VALUES ('".$ans_get_gen['general_user_id']."', '".$_POST['unsub_email']."')");
					}
					else
					{
						$sql_query = mysql_query("INSERT INTO `unsubscribe_newsletter` (`general_user_id` ,`email`) VALUES ('0', '".$_POST['unsub_email']."')");
					}
				}
				elseif(mysql_num_rows($sql_get_un)>0 && $_POST['sub_news']=='on')
				{
					mysql_query("DELETE FROM unsubscribe_newsletter WHERE email='".$_POST['unsub_email']."'");
				}
			}
			
			if($_POST['sub_news']=='on')
			{
				echo "You have successfully Subscribed.";
			}
			else
			{
				echo "You have successfully UnSubscribed.";
			}
		}
	}

	include_once('../includes/footer.php');
	/* require 'openid.php';
	 
	try
	{
	    # Change 'localhost' to your domain name.
	    $openid = new LightOpenID('purifyart.net');
	     
	    //Not already logged in
	    if(!$openid->mode)
	    {
	        //The google openid url
			//https://me.yahoo.com/[your_username]
	        $openid->identity = 'https://www.google.com/accounts/o8/id';
	         
	        //Get additional google account information about the user , name , email , country
	        $openid->required = array('contact/email' , 'namePerson/first' , 'namePerson/last' , 'pref/language' , 'contact/country/home');
	         
	        //start discovery
	        header('Location: ' . $openid->authUrl());
	    }
	     
	    else if($openid->mode == 'cancel')
	    {
	        echo 'User has canceled authentication!';
	        //redirect back to login page ??
	    }
	     
	    //Echo login information by default
	    else
	    {
	        if($openid->validate())
	        {
	            //User logged in
	            $d = $openid->getAttributes();
	             
	            $first_name = $d['namePerson/first'];
	            $last_name = $d['namePerson/last'];
	            $email = $d['contact/email'];
	            $language_code = $d['pref/language'];
	            $country_code = $d['contact/country/home'];
	             
	            $data = array(
	                'first_name' => $first_name ,
	                'last_name' => $last_name ,
	                'email' => $email ,
	            );
	             
	                        //now signup/login the user.
	            process_google_data($data);
	        }
	        else
	        {
	            //user is not logged in
	        }
	    }
	}
	 
	catch(ErrorException $e)
	{
	    echo $e->getMessage();
	}
	
	function process_google_data($data)
	{
		$sql_get_un = mysql_query("SELECT * FROM unsubscribe_newsletter WHERE email='".$data['email']."'");
		if(mysql_num_rows($sql_get_un)<=0)
		{
			$sql_get_gen = mysql_query("SELECT * FROM general_user WHERE email='".$data['email']."'");
			if(mysql_num_rows($sql_get_gen)>0)
			{
				$ans_get_gen = mysql_fetch_assoc($sql_get_gen);
				$sql_query = mysql_query("INSERT INTO `unsubscribe_newsletter` (`general_user_id` ,`email`) VALUES ('".$ans_get_gen['general_user_id']."', '".$data['email']."')");
			}
			else
			{
				$sql_query = mysql_query("INSERT INTO `unsubscribe_newsletter` (`general_user_id` ,`email`) VALUES ('0', '".$data['email']."')");
			}
		}
		
		echo "You have successfully UnSubscribed.";
	} */
?>