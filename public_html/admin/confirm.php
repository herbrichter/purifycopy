<?php
	include_once('classes/User.php');
	
	$user_id=$_GET['id'];
	$obj=new User();
	$username=$obj->getUsername($user_id);
	$upload_path="../".$obj->getUploadPath($user_id);
	$flag=0;
	$msg="Are you sure you want to remove this user?";
	
	if(isset($_POST['yes']))
	{
		$rs=$obj->removeUser($user_id,$upload_path);
		$flag=1;
		if($rs)	$msg="User removed successfully.";
		else $msg="Error in removing user. Please try again.";
	}
?>
<html>
<head>
<title>Confirm Remove User</title>
<link rel="stylesheet" type="text/css" media="screen" href="includes/purify.css" />
<script language="javascript" type="text/javascript">
function closeWindow()
{
	window.opener.location.reload();
	window.close();
}
</script>
</head>
<div id="confirmBox" align="center">
<?php echo $msg; ?><br />
<p align="center">
<table align="center" width="200">
<tr>
  	<td colspan="2" bgcolor="#FFFFFF"><img src="../images/spacer.gif" alt=" " width="10" height="10" /></td>
</tr>
<tr>
    <td width="100" bgcolor="#F5F5F5"><div align="center" class="hintBig" style="outline-width:thin;outline-color:#000000; outline-style:dotted; background-color:#818181; color:#FFFFFF">#<?php echo $user_id; ?></div></td>
    <td width="100" bgcolor="#F5F5F5"><div align="left" class="hintBig"><?php echo $username; ?></div></td>
</tr>
<tr>
  	<td colspan="2" bgcolor="#FFFFFF"><img src="../images/spacer.gif" alt=" " width="10" height="10" /></td>
</tr>
<tr>
  	<td colspan="2" bgcolor="#FFFFFF"><img src="../images/spacer.gif" alt=" " width="10" height="10" /></td>
</tr>
<tr>
  <?php
  	if(!$flag)
	{
		echo <<<EOD
	  <form name="confirm" method="post" action="">
		<td width="100" align="right"><input name="yes" type="submit" value="   Yes   "></td>
		<td width="100" align="left"><input name="no" type="button" value="   No   " onClick="window.close();"></td>
	  </form>
EOD;
	}
	else
	{
		echo "<td colspan='2' align='center'><a class='hintConfirmBox' href='#' onClick='closeWindow();'>Close</a></td>";	
	}
  ?>
</tr>
</table>

</p>
</div>
</html>