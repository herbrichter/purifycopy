<?php
	include_once('includes/header.php');
?>

<script>
	$(function() 
	{
		$("#bday").datepicker({
		dateFormat: 'yy-mm-dd',
		changeMonth: true,
		changeYear: true
		});
	});
function CheckEmail() 
{
	var ajaxFile = 'emailexist.php';
	$.post(ajaxFile,{"email":$("#email").val()},function(data){
		if(data == "Fail")	{	
			$("#emailerror").html('Email id already present.').show();
		}
		else if(data == "Valid")	{
			$("#emailerror").html('').show();
		}
		return false;
	});
}
</script>
<div id="contentContainer">
	<div id="subNavigation">
    	<?php 
			include_once('admin_leftnavigarion.php');
		?>
    </div>
    <div id="actualContent">
		<h1 class="withLine">Register User</h1>
        <form onsubmit="return checkForm();" action="general_user_registration.php" method="post" name="form"> 
        	<div class="fieldCont">
            	<div class="fieldTitle">* First Name</div>
                <input type="text" class="fieldText" id="Fname" name="Fname">
            </div> 
            <div class="fieldCont">
            	<div class="fieldTitle">* Last Name</div>
                <input type="text" class="fieldText" name="Lname">
            </div>
			<div class="fieldCont">
            	<div class="fieldTitle">Birth Date</div>
                <input type="text" class="fieldText hasDatepicker" id="bday" name="bday">
            </div>
            <div class="fieldCont">
            	<div class="fieldTitle">* Your Email</div>
                <input type="text" onblur="CheckEmail()" id="email" class="fieldText" name="email">
				<div style="color:#FF0000; font-size:10px; margin-left:25px; width:300px; float:right;" id="emailerror"></div>
            </div>
            <div class="fieldCont">
            	<div class="fieldTitle">* Re - enter Email</div>
                <input type="text" class="fieldText" name="remail">
            </div> 
            <div class="fieldCont">
            	<div class="fieldTitle">* Password</div>
                <input type="password" class="fieldText" name="pass">
            </div> 
            <div class="fieldCont">
            	<div class="fieldTitle">* Confirm Password</div>
                <input type="password" class="fieldText" name="rpass">
            </div> 
             
		  <!--<div class="fieldCont">
		  	<div class="chkCont">
            	<input type="checkbox" class="chk" name="terms">
				<div class="chkTitle">I accept the <a href="http://purifyart.net/Demo/about_agreements_user.html">Terms</a> &amp; <a href="http://purifyart.net/Demo/about_agreements_privacy.html">Privacy</a> Agreements</div>
            </div>	 
			</div>-->
			<div class="fieldCont">
				<div class="regiCont">
					<input type="submit" value="Register" class="register" name="submit">
				</div>
            </div>
      	</form>
    </div>
    <div class="clearMe"></div>
</div>
<?php include_once('includes/footer.php'); ?>
