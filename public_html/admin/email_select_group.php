<div>
    <form id="email_group" name="email_group" method="post" action="">
      <div style="width:650px;float:left;">
      <input type="hidden" name="type" value="1" />
      <table width="650" border="0" cellpadding="0" cellspacing="0">
        <tr>
          <td width="100">Group</td>
          <td width="402">
          	<select name="select-email-group" class="pulldownField" id="select-email-group">
            	<option value="1">All Users</option>
                <option value="2"> All Members</option>
                <option value="3"> All Non Members</option>
                <option value="4"> Artists-Members	 </option>
                <option value="5"> Artists- Non Members	</option>
                <option value="6"> Community-Members		</option>
                <option value="7"> Community- Non Members		</option>
                <option value="8"> Fans-Members		</option>
                <option value="9"> Fans-Non Members		</option>
                <option value="11">Unregistered Users	</option>
				<option value="12">Uploaded Users	</option>

            </select>
            <input type="hidden" name="select-email-group" value="" />
          </td>
          <td width="148" align="left"><div class="hint">Choose from a list of all groups.</div></td>
        </tr>
        <tr>
          <td colspan="3"><img src="images/spacer.gif" alt=" " width="10" height="10" /></td>
        </tr>
        <tr id="email_ids">
          <td width="100" valign="top">Email ids</td>
          <td width="402">
			<?php include_once('email_list_groups.php'); ?>
          </td>
          <td width="148" align="left"><div class="hint">Copy paste to your email.</div></td>
        </tr>
      </table>
      </div>
	  <div style="width:255px;float:right;" id="selected-types" >
	  </div>
    </form>
  </div>