<?php
	ob_start();
	include_once('loggedin_includes.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Purify Entertainment Administrator Control</title>
<link rel="stylesheet" type="text/css" media="screen" href="includes/purify-admin.css" />
<link rel="stylesheet" type="text/css" media="screen" href="../includes/lightbox.css" />
<script type="text/javascript" src="javascripts/lightbox.js"></script>
<script src="../javascripts/jquery-1.2.6.js"></script>
<script>



var ContentHeight = 350;
var TimeToSlide = 250.0;

var openAccordion = '';

function runAccordion(index)
{
  var nID = "Accordion" + index + "Content";
  if(openAccordion == nID)
    nID = '';
    
  setTimeout("animate(" + new Date().getTime() + "," + TimeToSlide + ",'" + openAccordion + "','" + nID + "')", 33);
  
  openAccordion = nID;
}

function animate(lastTick, timeLeft, closingId, openingId)
{  
  var curTick = new Date().getTime();
  var elapsedTicks = curTick - lastTick;
  
  var opening = (openingId == '') ? null : document.getElementById(openingId);
  var closing = (closingId == '') ? null : document.getElementById(closingId);
 
  if(timeLeft <= elapsedTicks)
  {
    if(opening != null)
      opening.style.height = ContentHeight + 'px';
    
    if(closing != null)
    {
      closing.style.display = 'none';
      closing.style.height = '0px';
    }
    return;
  }
 
  timeLeft -= elapsedTicks;
  var newClosedHeight = Math.round((timeLeft/TimeToSlide) * ContentHeight);

  if(opening != null)
  {
    if(opening.style.display != 'block')
      opening.style.display = 'block';
    opening.style.height = (ContentHeight - newClosedHeight) + 'px';
  }
  
  if(closing != null)
    closing.style.height = newClosedHeight + 'px';

  setTimeout("animate(" + curTick + "," + timeLeft +",'" + closingId + "','" + openingId + "')", 33);
}</script>
<script src="includes/mediaelement-and-player.min.js"></script>
<link rel="stylesheet" href="includes/mediaelementplayer.min.css" />
</head>
<body>
<div id="outerContainer">


  <div id="purifyMasthead">
    <div id="purifyLogo"><a href="index.php"><img src="images/4.gif" alt="Purify Entertainment" width="302" height="53" border="0" /></a></div>
	<?php include("loggedin_area.php"); ?>
  </div>
