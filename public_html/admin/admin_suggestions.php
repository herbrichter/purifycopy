<?php
	include_once('includes/header.php');
	include_once('../classes/Suggestion.php');
	
	$obj=new Suggestion();
	
	$search_key=$_POST['search'];
	$criteria_id=$_POST['criteria'];
	if($criteria_id=="") $criteria_id=1;

	$read_sugg_id=$_POST['read_sugg_id'];
	if($read_sugg_id)
	{
		$obj->mark_as_read($read_sugg_id);
	}
	
	$no_unread=$obj->countUnread();
	$no_read=$obj->countRead();
		
	switch($criteria_id)
	{	//All
		case 0: $criteria='sugg_id';
				$orderby='sugg_id';
				$dir='desc';
				if(!$search_key) $flag_pg=1;
				break;
		//Unread
		case 1:	$criteria='unread';
				$orderby='sugg_id';
				$dir='desc';
				break;
		//Read
		case 2:	$criteria='read';
				$orderby='sugg_id';
				$dir='desc';
				break;
		//First Load
		default:$criteria='sugg_id';
				$orderby='sugg_id';
				$dir='desc';
				$flag_pg=1;
				break;
	} 
	
	$no=$obj->countSuggestionsByCriteria($search_key,$criteria);
	
	if(!$no) $flag=1;

	if($flag_pg)
	{
		if(!(isset($pagenum))) 
		{ 
			$pagenum = 1; 
		} 
		
		$currentpage="admin_suggestions.php?pagenum=".$pagenum;
		$page_rows = 10;
		$last = ceil($no/$page_rows);
		
		if ($pagenum < 1) 
		{ 
			$pagenum = 1; 
		} 
		elseif ($pagenum > $last) 
		{ 
			$pagenum = $last; 
		} 
		if($last) $max = ' limit ' .($pagenum - 1) * $page_rows .',' .$page_rows;
		else
		{
		 $max='';
		 $flag=1;
		}
	}
	
	$rs=$obj->sortByCriteria($search_key,$max,$criteria,$orderby,$dir);
?>

<script language="JavaScript" type="text/JavaScript">
<!--
function MM_openBrWindow(theURL,winName,features) { //v2.0
  window.open(theURL,winName,features);
}

function mark_as_read(SID)
{
	var frm=document.getElementById('read_form_'+SID);
	frm.submit();   
}//-->

function search_sort()
{
	var frm1=document.getElementById('search_form');
	frm1.submit();
}
</script>
	
  <div id="contentContainer">
    <div id="subNavigation">
   <?php 
	$active_nav = "suggestions";
	$active_sub_nav = "";
   include_once('admin_leftnavigarion.php'); ?>   
    </div>
    <div id="actualContent">
      <div id="searchContainer" class="hint">
      	<form id="search_form" name="search_form" method="post" action="">
      	<table width="100%" border="0" align="right">
          <tr>
            <td width="25%" align="right">Search</td>
            <td width="31%" align="right"><input  id="search" name="search" type="text" class="regularField2" <?php //if($flag_sd=="1") echo " disabled='disabled' " ?>/></td>
            <td width="4%" align="right">by</td>
            <td width="32%" align="right">
              <select id="criteria" name="criteria" class="pulldownField2">
                  <option value="0" <?php if($criteria_id=="") echo " selected='selected' " ?>>Select All</option>
                  <option value="1" <?php if($criteria_id=="1") echo " selected='selected' " ?>>Unread</option>
                  <option value="2" <?php if($criteria_id=="2") echo " selected='selected' " ?>>Read</option>
              </select>     
            </td>
            <td width="8%" align="right"><a href="#" title="Go"><img src="images/go.jpg" width="30" height="30" onclick="search_sort();" alt="" /></a></td>
          </tr>
        </table>
        </form>
      </div>
      <h1 class="withLine">Suggestions</h1>
      <div id="pageBox"><?php if($flag_pg) echo "Page ".$pagenum." of ".$last; else echo "Search results: ".$no; ?></div>
      <div id="pageNoBox" align="right"><?php include('pagination.php'); ?></div>
	  <?php 
	  	echo "<br />";
		echo "<br />";
		if($flag) echo "No records found.<br /><br />";		
	  	while($row=mysql_fetch_assoc($rs))
	  	{
			//Read_type
			switch($row['seen'])
			{
				case '0':	$bgcolor='#000000';
							$read_type='Unread';
							break;
				case '1':	$bgcolor='#CCCCCC';
							$read_type='Read';				
							break;
			}
?>
	  <table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#F5F5F5" class="tableBg" style="border-bottom-color:<?php echo $bgcolor; ?>;">
          <tr>
            <td bgcolor="#FFFFFF" class="tableBg" style="border-bottom-color:<?php echo $bgcolor; ?>;">&nbsp;</td>
            <td width="91" bgcolor="#FFFFFF" class="tableBg" style="border-bottom-color:<?php echo $bgcolor; ?>;">&nbsp;</td>
            <td width="209" bgcolor="#FFFFFF" class="tableBg" style="border-bottom-color:<?php echo $bgcolor; ?>;">&nbsp;</td>
            <td width="151" bgcolor="#FFFFFF" class="tableBg" style="border-bottom-color:<?php echo $bgcolor; ?>;">&nbsp;</td>
            <td width="86" bgcolor="<?php echo $bgcolor; ?>"><div align="left" class="hintWhite"><strong><?php echo $read_type; ?></strong></div></td>
          <tr>
            <td width="128" bgcolor="#F5F5F5"><div align="center" class="hintBig" style="outline-width:thin;outline-color:#000000; outline-style:dotted; background-color:#818181; color:#FFFFFF">Suggestion #<?php echo $row['sugg_id']; ?></div></td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td colspan="2" class="hintBgWhite"><strong><?php echo $row['name']; ?></strong></td>
            <td colspan="3" rowspan="3" valign="top" class="hintLeftLine" align="left"><?php echo $row['sugg']; ?></td>
          </tr>
          <tr>
            <td colspan="2" class="hintBgWhite">&nbsp;</td>
          </tr>
          <tr>
            <td colspan="2" class="hintBgWhite">email id: <?php echo $row['email']; ?></td>
          </tr>
          <tr>
          <form id="read_form_<?php echo $row['sugg_id']; ?>" name="read_form" method="post" action="">
            <td colspan="2" class="hint">Posted on: <?php echo $row['posted_date']; ?></td>
            <td>&nbsp;</td>
            <td><input type="hidden" name="read_sugg_id" id="read_sugg_id" value="<?php echo $row['sugg_id']; ?>" /></td>
            <td><div align="right"><a href='#' onclick="mark_as_read('<?php echo $row['sugg_id']; ?>');" ><?php if(!$row['seen']) echo "Mark as read";?></a></div></td>
          </form>
          </tr>
        </table>
    <table>
          <tr>
            <td colspan="5" bgcolor="#FFFFFF"><img src="../images/spacer.gif" alt=" " width="10" height="10" /></td>
          </tr>
        </table>        
<?php			
		}
	  ?>
      <div id="pageBox"><?php if($flag_pg) echo "Page ".$pagenum." of ".$last; ?></div>
      <div id="pageNoBox" align="right"><?php	include('pagination.php'); ?></div>
    </div>
    <div class="clearMe"></div>
  </div>
  
<?php include_once('includes/footer.php'); ?>
