<?php 
	include_once('../classes/Type.php');
	include_once('../classes/SubType.php');
			
    $newtype=$_POST['newtype'];
	$parent=$_POST['parent'];
	$section_id=$_POST['section_id'];	
	$obj=new Type();
	$type_id=$obj->addType($newtype,$parent);
	$obj2=new SubType();
	$obj2->addSubType('None',$type_id);
	
	header('location:admin_type.php?id='.$section_id);
?>