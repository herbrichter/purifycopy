<?php
	include_once('includes/header.php');
	include_once('classes/Features.php');
	include_once('classes/ArtistFeatures.php');
	include_once('classes/CommunityFeatures.php');
	include_once('classes/TypeFeatures.php');
	include_once('classes/SubTypeFeatures.php');
	include_once('classes/User.php');
	
	if(isset($_POST['postfeature']))
	{
		$feature_page=$_POST['select-feature-page'];
		$feature_category=$_POST['select-feature-category'];
		$feature_id=$_POST['feature_id'];
		$type=$_POST['type-select'];
		$subtype=$_POST['subtype-select'];		
		
		switch($feature_page)
		{
			//----------------------------------------FEATURE PAGE: INDEX------------------------------------------------//		
			case 'index':	
							switch($feature_category)
							{
								case 'artist':$objUser=new User();
											if($objUser->markAsFeatured('artist',$feature_id))
												$flag=1;
											else
												$msg="<div style='background:#CC0000; padding:5px; float:right; text-align:center;' class='hintWhite'>Error in adding user with id ".$feature_id." as feature.</div>";	
											break;
								case 'community':$objUser=new User();
											if($objUser->markAsFeatured('community',$feature_id))
												$flag=1;
											else
												$msg="<div style='background:#CC0000; padding:5px; float:right; text-align:center;' class='hintWhite'>Error in adding user with id ".$feature_id." as feature.</div>";	
											break;											
								default:	break;
							}	
					
							if($flag)
							{
								$obj=new Features();
								if($obj->addFeature($feature_category,$feature_id))
									$msg="<div style='background:#CCCCCC; padding:5px; float:right; text-align:center;' class='hintWhite'>Feature has been added.</div>";
								else
									$msg="<div style='background:#CC0000; padding:5px; float:right; text-align:center;' class='hintWhite'>Error in adding feature.</div>";
							}
							
							break;
			//----------------------------------------FEATURE PAGE: ARTIST------------------------------------------------//		
			case 'artist':	
							$objUser=new User();
							if($objUser->markAsFeaturedArtist($feature_id))
								$flag=1;
							else
								$msg="<div style='background:#CC0000; padding:5px; float:right; text-align:center;' class='hintWhite'>Error in adding artist with id ".$feature_id." as feature.</div>";	
					
							if($flag)
							{
								$obj=new ArtistFeatures();
								if($obj->addFeature('artist',$feature_id))
									$msg="<div style='background:#CCCCCC; padding:5px; float:right; text-align:center;' class='hintWhite'>Feature has been added.</div>";
								else
									$msg="<div style='background:#CC0000; padding:5px; float:right; text-align:center;' class='hintWhite'>Error in adding feature.</div>";
							}
							
							break;
			//----------------------------------------FEATURE PAGE: COMMUNITY------------------------------------------------//		
			case 'community':	
							$objUser=new User();
							if($objUser->markAsFeaturedCommunity($feature_id))
								$flag=1;
							else
								$msg="<div style='background:#CC0000; padding:5px; float:right; text-align:center;' class='hintWhite'>Error in adding community with id ".$feature_id." as feature.</div>";	
					
							if($flag)
							{
								$obj=new CommunityFeatures();
								if($obj->addFeature('community',$feature_id))
									$msg="<div style='background:#CCCCCC; padding:5px; float:right; text-align:center;' class='hintWhite'>Feature has been added.</div>";
								else
									$msg="<div style='background:#CC0000; padding:5px; float:right; text-align:center;' class='hintWhite'>Error in adding feature.</div>";
							}
							
							break;
			//----------------------------------------FEATURE PAGE: TYPE------------------------------------------------//
			case 'type':	
							switch($feature_category)
							{
								case 'artist':$objUser=new User();
											$parent=$objUser->getUserInfo($feature_id);
											if($objUser->markAsFeaturedType('artist',$feature_id,$type))
												$flag=1;
											else
												$msg="<div style='background:#CC0000; padding:5px; float:right; text-align:center;' class='hintWhite'>Error in adding user with id ".$feature_id." as feature.</div>";	
											break;
								case 'community':$objUser=new User();
											$parent=$objUser->getUserInfo($feature_id);
											if($objUser->markAsFeaturedType('community',$feature_id,$type))
												$flag=1;
											else
												$msg="<div style='background:#CC0000; padding:5px; float:right; text-align:center;' class='hintWhite'>Error in adding user with id ".$feature_id." as feature.</div>";	
											break;											
								default:	break;
							}	
					
							if($flag!=NULL)
							{
								$obj=new TypeFeatures();
								if($obj->addFeature($feature_category,$feature_id,$type))
									$msg="<div style='background:#CCCCCC; padding:5px; float:right; text-align:center;' class='hintWhite'>Feature has been added.</div>";
								else
									$msg="<div style='background:#CC0000; padding:5px; float:right; text-align:center;' class='hintWhite'>Error in adding feature.</div>";
							}
										
							break;
			//----------------------------------------FEATURE PAGE: SUBTYPE------------------------------------------------//
			case 'subtype':	
							switch($feature_category)
							{
								case 'artist':$objUser=new User();
											$parent=$objUser->getUserInfo($feature_id);
											if($objUser->markAsFeaturedSubType('artist',$feature_id,$type,$subtype))
												$flag=1;
											else
												$msg="<div style='background:#CC0000; padding:5px; float:right; text-align:center;' class='hintWhite'>Error in adding user with id ".$feature_id." as feature.</div>";	
											break;
								case 'community':$objUser=new User();
											$parent=$objUser->getUserInfo($feature_id);
											if($objUser->markAsFeaturedSubType('community',$feature_id,$type,$subtype))
												$flag=1;
											else
												$msg="<div style='background:#CC0000; padding:5px; float:right; text-align:center;' class='hintWhite'>Error in adding user with id ".$feature_id." as feature.</div>";	
											break;											
								default:	break;
							}	
					
							if($flag)
							{
								$obj=new SubTypeFeatures();
								if($obj->addFeature($feature_category,$feature_id,$type,$subtype))
									$msg="<div style='background:#CCCCCC; padding:5px; float:right; text-align:center;' class='hintWhite'>Feature has been added.</div>";
								else
									$msg="<div style='background:#CC0000; padding:5px; float:right; text-align:center;' class='hintWhite'>Error in adding feature.</div>";
							}
							
							break;
		}
	}	
?>

<script>
	$(document).ready(
						function()
						{
							$("#select-feature-page").change(
														function ()
														{
															
															//alert($("#type-select").val());
															
  																//alert("Data Loaded: " + data);
																if($("#select-feature-page").val()=="subtype")
																{
																	$("#type_block").show();
																	$("#type_block_space").show();
																	$("#subtype_block").show();
																	$("#subtype_block_space").show();
																	$("#feature_block").show();
																	$("#feature_block_space").show();
																	$("#type-select").change();
																	
																}
																else if($("#select-feature-page").val()=="type")
																{	
																	$("#feature_block").show();
																	$("#feature_block_space").show();
																	$("#type_block").show();
																	$("#type_block_space").show();
																	$("#subtype_block").hide();
																	$("#subtype_block_space").hide();
																}
																else if($("#select-feature-page").val()=="artist"||$("#select-feature-page").val()=="community")
																{
																	$("#type_block").hide();
																	$("#type_block_space").hide();
																	$("#subtype_block").hide();
																	$("#subtype_block_space").hide();
																	$("#feature_block").hide();
																	$("#feature_block_space").hide();
																}
																else
																{
																	$("#type_block").hide();
																	$("#type_block_space").hide();
																	$("#subtype_block").hide();
																	$("#subtype_block_space").hide();
																	$("#feature_block").show();
																	$("#feature_block_space").show();
																}
														}
													)
													.change();						
						
							$("#select-feature-category").change(
														function ()
														{
															//alert($("#type-select").val());
															$.post("select_type_block.php", { parent:$("#select-feature-category").val() },
																function(data)
																{
																//alert("Data Loaded: " + data);
																	$("#type-select").replaceWith(data);
																	$("#type-select").change();
																}
															);
														}
													)
													.change();
										
																
					    }
				    );
					
function populateList(ele)
{
	$.post("select_subtype_block.php", { type_id:ele.value },
		function(data)
		{
		//alert("Data Loaded: " + data);
			$("#subtype-select").replaceWith(data);
			
		}
	);	
}	
</script>

  <div id="contentContainer">
    <div id="subNavigation">
   <?php 
	$active_nav = "features";
	$active_sub_nav = "";
   include_once('admin_leftnavigarion.php'); ?>   
    </div>
    <div id="actualContent">
      <?php echo $msg; ?>
      <h1 class="withLine">Features</h1>
	  <?php include_once('features_block.php'); ?>
    </div>
    
    <div class="clearMe"></div>
    
  </div>
  
<?php include_once('includes/footer.php'); ?>
