<?php
include_once('../commons/db.php');
include_once('classes/Features.php');
include_once('classes/ArtistFeatures.php');
include_once('classes/CommunityFeatures.php');
include_once('classes/TypeFeatures.php');
include_once('classes/SubTypeFeatures.php');

class User
{
  function checkVerificationNo($verification_no)
  {
	$sql="select * from user where verification_no='$verification_no' and verified!='1' and del<>'1'";
	$rs=mysql_query($sql) or die(mysql_error());
	return mysql_num_rows($rs);
  }
  
  function getUserInfo($id)
  {
  	$sql="select * from user,user_parent where user.id='$id' and user.id=user_parent.user_id";
	$rs=mysql_query($sql) or die(mysql_error());
	$row=(mysql_fetch_assoc($rs));
	return($row);   
  }
  
  function getUserByRef($ref_no)
  {
  	$sql="SELECT * FROM user WHERE ref_no='$ref_no'";
	$rs=mysql_query($sql) or die(mysql_error());
	$row=(mysql_fetch_assoc($rs));
	return($row);
  }
  
  function countAllUsers()
  {
   	$sql="select * from user,user_parent where user.del<>'1' and user.id=user_parent.user_id";
	$rs=mysql_query($sql) or die(mysql_error());
	return mysql_num_rows($rs);
  }
  
  function countUsers($user_type)
  {
  	$sql="select * from user,user_parent where user.id=user_parent.user_id and user_parent.user_type='$user_type' and user.del<>'1'";
	$rs=mysql_query($sql) or die(mysql_error());
	return mysql_num_rows($rs);
  }
  
  function countUsersByCriteria($search_key,$criteria)
  {
  	if($search_key==NULL)
	{
		$sql="select * from user,user_parent where user.del<>'1' and user.id=user_parent.user_id and user.$criteria<>''";
		$rs=mysql_query($sql) or die(mysql_error());
		return mysql_num_rows($rs);
	}
	else
	{
		$sql="select * from user,user_parent where user.del<>'1' and user.id=user_parent.user_id and user.$criteria='$search_key'";
		$rs=mysql_query($sql) or die(mysql_error());
		return mysql_num_rows($rs);	
	}
  }
  
  function countUsersByCriteriaByType($search_key,$criteria,$user_type)
  {
  	if($search_key==NULL)
	{
		$sql="select * from user,user_parent where user.del<>'1' and user_parent.user_type='$user_type' and user.id=user_parent.user_id and user.$criteria<>''";
		$rs=mysql_query($sql) or die(mysql_error());
		return mysql_num_rows($rs);
	}
	else
	{
		$sql="select * from user,user_parent where user.del<>'1' and user_parent.user_type='$user_type' and user.id=user_parent.user_id and user.$criteria='$search_key'";
		$rs=mysql_query($sql) or die(mysql_error());
		return mysql_num_rows($rs);	
	}
  }
  
  function getUserList($max)
  {
  	$sql="select * from user,user_parent where user.del<>'1' order by user.id desc".$max;
	$rs=mysql_query($sql) or die(mysql_error());
	return($rs);  	
  }
  
  function getUserListByType($user_type,$max)
  {
  	$sql="select * from user,user_parent where user_parent.user_type='$user_type' and user.del<>'1' and user.id=user_parent.user_id order by user.id desc".$max;
	$rs=mysql_query($sql) or die(mysql_error());
	return($rs);  	
  }
  
  function getUsername($id)
  {
  	$sql="select * from user where id='$id'";
	$rs=mysql_query($sql) or die(mysql_error());
	$row=mysql_fetch_assoc($rs);
	return($row['username']);
  }
  
  function getUploadPath($id)
  {
  	$sql="select * from user where id='$id'";
	$rs=mysql_query($sql) or die(mysql_error());
	$row=mysql_fetch_assoc($rs);
	return($row['upload_path']);
  }
  
  function acceptUser($id)
  {
  	$sql="UPDATE user SET status='1' WHERE id='$id'";
	$rs=mysql_query($sql) or die(mysql_error());
  }
  
  function rejectUser($id,$upload_path)
  {
  	$query="SELECT * FROM user_img WHERE id='$id'";
	$rsq=mysql_query($query) or die(mysql_error());  	
	$row=mysql_fetch_assoc($rsq);
	
	if($row)
	{
		if(is_file($row['img_path']))
			unlink("../".$row['img_path']);
		if(is_file($row['img_small_path']))
			unlink("../".$row['img_small_path']);
		if(is_file($row['img_tiny_path']))
			unlink("../".$row['img_tiny_path']);
	}
	
	if(is_file($upload_path))
	{
		unlink($upload_path);
	}
	
	$sql="DELETE FROM user WHERE id='$id'";
	$rs=mysql_query($sql) or die(mysql_error());
	
	
  	$sql2="DELETE FROM user_subscription WHERE id='$id'";
	$rs2=mysql_query($sql2) or die(mysql_error());
	
 	$sql3="DELETE FROM user_type WHERE id='$id'";
	$rs3=mysql_query($sql3) or die(mysql_error());
	
	$sql4="DELETE FROM user_parent WHERE user_id='$id'";
	$rs4=mysql_query($sql4) or die(mysql_error());
	
	$sql5="DELETE FROM user_img WHERE id='$id'";
	$rs5=mysql_query($sql5) or die(mysql_error());
		
  }
  
  function removeUser($id,$upload_path)
  {
  	$query="SELECT * FROM user_img WHERE id='$id'";
	$rsq=mysql_query($query) or die(mysql_error());  	
	$row=mysql_fetch_assoc($rsq);
	
	if($row)
	{
		if(is_file($row['img_path']))
			unlink("../".$row['img_path']);
		if(is_file($row['img_small_path']))
			unlink("../".$row['img_small_path']);
		if(is_file($row['img_tiny_path']))
			unlink("../".$row['img_tiny_path']);
	}
	
	if(is_file($upload_path))
	{
		unlink($upload_path);
	}
	
	$sql="DELETE FROM user WHERE id='$id'";
	$rs=mysql_query($sql) or die(mysql_error());
	
	
  	$sql2="DELETE FROM user_subscription WHERE id='$id'";
	$rs2=mysql_query($sql2) or die(mysql_error());
	
 	$sql3="DELETE FROM user_type WHERE id='$id'";
	$rs3=mysql_query($sql3) or die(mysql_error());
	
	$sql4="DELETE FROM user_parent WHERE user_id='$id'";
	$rs4=mysql_query($sql4) or die(mysql_error());
	
	$sql5="DELETE FROM user_img WHERE id='$id'";
	$rs5=mysql_query($sql5) or die(mysql_error());
	
	$sql6="DELETE FROM features WHERE category_id='$id'";
	$rs6=mysql_query($sql6) or die(mysql_error());	
	
	$sql7="DELETE FROM artist_features WHERE category_id='$id'";
	$rs7=mysql_query($sql7) or die(mysql_error());	
	
	$sql8="DELETE FROM community_features WHERE category_id='$id'";
	$rs8=mysql_query($sql8) or die(mysql_error());	
	
	$sql9="DELETE FROM type_features WHERE category_id='$id'";
	$rs9=mysql_query($sql9) or die(mysql_error());	
	
	$sql10="DELETE FROM subtype_features WHERE category_id='$id'";
	$rs10=mysql_query($sql10) or die(mysql_error());
	
	$obj=new Features();
	$obj->reorganise();	
	$obj=new ArtistFeatures();
	$obj->reorganise();
	$obj=new CommunityFeatures();
	$obj->reorganise();
	$obj=new TypeFeatures();
	$obj->reorganise();
	$obj=new SubTypeFeatures();
	$obj->reorganise();
	
	return 1;
  }
  
  function listEmail($email_group)
  {
	$date_mem = date('Y-m-d');
  	switch($email_group)
	{
		//All Users
		case '1':	$sql="select * from general_user";
					//$sql="select * from user,unreg_user";
					$rs=mysql_query($sql) or die(mysql_error());
					break;
		//All Registered Users
		case '2':	$sql="select * from purify_membership WHERE (expiry_date>'".$date_mem."' OR lifetime=1)";
					$rs=mysql_query($sql) or die(mysql_error());
					break;
		//All Unregistered Users
		case '3':	$sql="select * from purify_membership WHERE (expiry_date>'".$date_mem."' OR lifetime=1)";
					$rs = mysql_query($sql);
					break;
		//Artists
		case '4':	$sql="select * from purify_membership WHERE (expiry_date>'".$date_mem."' OR lifetime=1)";
					$rs=mysql_query($sql) or die(mysql_error());
					break;
		//Community Members
		case '5':	$sql="select * from purify_membership WHERE (expiry_date>'".$date_mem."' OR lifetime=1)";
					$rs=mysql_query($sql) or die(mysql_error());
					break;
		//Teams
		case '6':	$sql="select * from purify_membership WHERE (expiry_date>'".$date_mem."' OR lifetime=1)";
					$rs=mysql_query($sql) or die(mysql_error());
					break;
		//Fans
		case '7':	$sql="select * from purify_membership WHERE (expiry_date>'".$date_mem."' OR lifetime=1)";
					$rs=mysql_query($sql) or die(mysql_error());
					break;
		//Directors
		case '8':	$sql="select * from fan_club_membership where expiry_date>'".$date_mem."' AND del_status=0";
					$rs=mysql_query($sql) or die(mysql_error());
					break;
		//Unsubscribed Artists
		case '9':	$sql="select * from fan_club_membership where expiry_date>'".$date_mem."' AND del_status=0";
					$rs=mysql_query($sql) or die(mysql_error());
					break;
		case '11':	$sql="select * from temp_members_db";
					$rs=mysql_query($sql) or die(mysql_error());
					break;
		case '12':	$sql="select * from unreg_user";
					$rs=mysql_query($sql) or die(mysql_error());
					break;
					
					
		/* //Subscribed Artists
		case '10':	$sql="select * from user,user_parent where user_parent.user_type='Artist' && user.subscribed='1' && user.id=user_parent.user_id";
					$rs=mysql_query($sql) or die(mysql_error());
					break;
		//Unsubscribed Community Members
		case '11':	$sql="select * from user,user_parent where user_parent.user_type='Community' && user.subscribed='0' && user.id=user_parent.user_id";
					$rs=mysql_query($sql) or die(mysql_error());
					break;
		//Subscribed Community Members
		case '12':	$sql="select * from user,user_parent where user_parent.user_type='Community' && user.subscribed='1' && user.id=user_parent.user_id";
					$rs=mysql_query($sql) or die(mysql_error());
					break;
		//Unsubscribed Team
		case '13':	$sql="select * from user,user_parent where user_parent.user_type='Team' && user.subscribed='0' && user.id=user_parent.user_id";
					$rs=mysql_query($sql) or die(mysql_error());
					break;
		//Subscribed Team
		case '14':	$sql="select * from user,user_parent where user_parent.user_type='Team' && user.subscribed='1' && user.id=user_parent.user_id";
					$rs=mysql_query($sql) or die(mysql_error());
					break;
		//Founding Fans
		case '15':	$sql="select * from user,user_subscription where user_subscription.subscription_id='1' && user.id=user_subscription.id";
					$rs=mysql_query($sql) or die(mysql_error());
					break;
		//Life Time Fans
		case '16':	$sql="select * from user,user_subscription where user_subscription.subscription_id='2' && user.id=user_subscription.id";
					$rs=mysql_query($sql) or die(mysql_error());
					break;
		//Annual Fans
		case '17':	$sql="select * from user,user_subscription where user_subscription.subscription_id='3' && user.id=user_subscription.id";
					$rs=mysql_query($sql) or die(mysql_error());
					break;	 */				
	}
	return($rs);
  }
  
  //Search/sort functions
  function sortByCriteria($search_key,$max,$field,$orderby,$dir)
  {
	if($search_key==NULL)
	{
		$sql="select * from user,user_parent where user.del<>'1' and user.$field<>'' and user.id=user_parent.user_id order by user.$orderby $dir".$max;
		$rs=mysql_query($sql) or die(mysql_error());
		return($rs);
	}
	else
	{
		$sql="select * from user,user_parent where user.del<>'1' and user.id=user_parent.user_id and user.$field='$search_key' order by user.$orderby $dir".$max;
		$rs=mysql_query($sql) or die(mysql_error());
		return($rs);
	}
  }
  
  function sortByCriteriaByType($search_key,$max,$field,$orderby,$dir,$user_type)
  {
	if($search_key==NULL)
	{
		$sql="select * from user,user_parent where user.del<>'1' and user.$field<>'' and user_parent.user_type='$user_type' and user.id=user_parent.user_id order by user.$orderby $dir".$max;
		$rs=mysql_query($sql) or die(mysql_error());
		return($rs);
	}
	else
	{
		$sql="select * from user,user_parent where user.del<>'1' and user_parent.user_type='$user_type' and user.id=user_parent.user_id and user.$field='$search_key' order by user.$orderby $dir".$max;
		$rs=mysql_query($sql) or die(mysql_error());
		return($rs);
	}
  }
  
  function getNewsletterUsers()
  {
		$sql="select * from user where del<>'1' and newsletter='1'";
		$rs=mysql_query($sql) or die(mysql_error());
		return($rs);  	
  }
  
  function markAsFeatured($parent,$id)
  {
  	$sql0="SELECT user_type FROM user_parent WHERE user_id='$id'";
	$rs0=mysql_query($sql0) or die(mysql_error());
	$row0=mysql_fetch_row($rs0);
	$user_type=$row0[0];
  
  	if($user_type==$parent)
	{
  		$sql="UPDATE user SET featured='1' WHERE id='$id' and status='1' and del<>'1'";
		$rs=mysql_query($sql) or die(mysql_error());
		//return(mysql_affected_rows());
		//A line above commented & a line below added to overcome 'multiple times adding' check.
		return 1;
	}
	//else
		return 0;
  }
  
  function markAsFeaturedArtist($id)
  {
  	$sql0="SELECT user_type FROM user_parent WHERE user_id='$id'";
	$rs0=mysql_query($sql0) or die(mysql_error());
	$row0=mysql_fetch_row($rs0);
	$user_type=$row0[0];
  
  	if($user_type=='artist')
	{
  		$sql="UPDATE user_parent SET featured_parent='1' WHERE user_id='$id' and del<>'1'";
		$rs=mysql_query($sql) or die(mysql_error());
		//return(mysql_affected_rows());
		//A line above commented & a line below added to overcome 'multiple times adding' check.
		return 1;
	}
	else
		return 0;
  }  

  function markAsFeaturedCommunity($id)
  {
  	$sql0="SELECT user_type FROM user_parent WHERE user_id='$id'";
	$rs0=mysql_query($sql0) or die(mysql_error());
	$row0=mysql_fetch_row($rs0);
	$user_type=$row0[0];
  
  	if($user_type=='community')
	{
  		$sql="UPDATE user_parent SET featured_parent='1' WHERE user_id='$id' and del<>'1'";
		$rs=mysql_query($sql) or die(mysql_error());
		//return(mysql_affected_rows());
		//A line above commented & a line below added to overcome 'multiple times adding' check.
		return 1;
	}
	else
		return 0;
  }  
  
  function markAsFeaturedType($parent,$id,$type_id)
  {
  	$sql0="SELECT user_type FROM user_parent WHERE user_id='$id'";
	$rs0=mysql_query($sql0) or die(mysql_error());
	$row0=mysql_fetch_row($rs0);
	$user_type=$row0[0];
  
  	if($user_type==$parent)
	{
		$sql1="SELECT type_id FROM user_type WHERE id='$id'";
		$rs1=mysql_query($sql1) or die(mysql_error());
		while($row1=mysql_fetch_row($rs1))
		{
			if($type_id==$row1[0])
			{
				$flag=1;
				break;
			}
			else
			{
				$flag=0;
			}				
		}

		if($flag==1)
		{
			$sql="UPDATE user_type SET featured_type='1' WHERE id='$id' and type_id='$type_id' and del<>'1'";
			$rs=mysql_query($sql) or die(mysql_error());
			//return(mysql_affected_rows());
			//A line above commented & a line below added to overcome 'multiple times adding' check.
			return 1;
		}
		else
		{
			return 0;
		}
	}
	else
		return 0;
  }  
  function uploadEmail($user_type,$email)
  {
  	$query="insert into emails (`user_type`,`email`) values ('$user_type','$email')";
	$rs=mysql_query($query) or die(mysql_error());
	$id=mysql_insert_id();
	return($id);
  }
  
  function findEmails()
  {
  	$sql="select * from emails";
	$rs=mysql_query($sql) or die(mysql_error());
	return($rs);
  }
  function markAsFeaturedSubType($parent,$id,$type_id,$subtype_id)
  {
  	$sql0="SELECT user_type FROM user_parent WHERE user_id='$id'";
	$rs0=mysql_query($sql0) or die(mysql_error());
	$row0=mysql_fetch_row($rs0);
	$user_type=$row0[0];
  
  	if($user_type==$parent)
	{
		$sql1="SELECT type_id,subtype_id FROM user_type WHERE id='$id'";
		$rs1=mysql_query($sql1) or die(mysql_error());
		while($row1=mysql_fetch_row($rs1))
		{
			if($type_id==$row1[0]&&$subtype_id==$row1[1])
			{
				$flag=1;
				break;
			}
			else
			{
				$flag=0;
			}				
		}

		if($flag==1)
		{
			$sql="UPDATE user_type SET featured_subtype='1' WHERE id='$id' and type_id='$type_id' and subtype_id='$subtype_id' and del<>'1'";
			$rs=mysql_query($sql) or die(mysql_error());
			//return(mysql_affected_rows());
			//A line above commented & a line below added to overcome 'multiple times adding' check.
			return 1;
		}
		else
		{
			return 0;
		}
	}
	else
		return 0;
  }   
  
  function getUserById($id)
  {
	$sql="select * from user,user_parent,user_img where user.id='$id' and user.id=user_parent.user_id and user.id=user_img.id";
	$rs=mysql_query($sql) or die(mysql_error());
	return mysql_fetch_assoc($rs);	  
  }
}
?>