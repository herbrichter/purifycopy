<?php
	session_start();
	include_once('commons/db_new.php');
	
	class Admin_Transactions
	{
		function get_requested()
		{
			$sql = mysql_query("SELECT * FROM request_payment WHERE del_status=0 ORDER BY date_sent DESC");
			return $sql;
		}
		
		function get_gen_user($email)
		{
			$sql = mysql_query("SELECT * FROM general_user WHERE email='".$email."' AND status=0 AND delete_status=0 AND active=0");
			return $sql;
		}
		
		function get_gen_id($id)
		{
			$sql = mysql_query("SELECT * FROM general_user WHERE general_user_id='".$id."'");
			$ans = mysql_fetch_assoc($sql);
			return $ans;
		}
		
		function send_payment($id)
		{
			$date = date("Y-m-d");
			$time = date("H:i:s");
			
			$sql = mysql_query("UPDATE request_payment SET date_received='".$date."',time_received='".$time."' WHERE id='".$id."'");
		}
		
		function get_transaction($id)
		{
			$sql = mysql_query("SELECT * FROM request_payment WHERE del_status=0 AND id='".$id."'");
			$ans = mysql_fetch_assoc($sql);
			return $ans;
		}
		
		function get_all_trans()
		{
			$sql = mysql_query("SELECT * FROM addtocart WHERE del_status=0 AND buy_status=1");
			return $sql;
		}
		
		/*Function Added By Azhar*/
		function get_all_trans_projectsale()
		{
			$sql = mysql_query("SELECT * FROM add_to_cart_fan_pro WHERE del_status=0 AND buy_status=1 AND fan_member_id=0");
			return $sql;
		}
		function get_all_trans_projectsale_monthly($month,$year)
		{
			$sql = mysql_query("SELECT * FROM add_to_cart_fan_pro WHERE del_status=0 AND buy_status=1 AND fan_member_id=0 AND YEAR(chk_date)='".$year."' AND MONTH(chk_date)='".$month."'");
			return $sql;
		}
		/*Function Added By Azhar Ends here*/
		
		function get_all_purify_members()
		{
			$final_purify = array();
			$sql = mysql_query("SELECT * FROM purify_membership");
			if($sql!="")
			{
				if(mysql_num_rows($sql)>0)
				{
					while($ans_purify = mysql_fetch_assoc($sql))
					{
						$ans_purify['table_name'] = 'purify_membership';
						if($ans_purify['extend_chks']=="0")
						{	
							if($ans_purify['lifetime']!="1" && $ans_purify['admin_mode']!="1,")
							{
								$final_purify[] = $ans_purify;
							}
						}
						else
						{
							$org_purchase_date = $ans_purify['purchase_date'];
							$org_expiry_date = $ans_purify['expiry_date'];
							$ans_purify['mchks_dates'] = trim($ans_purify['mchks_dates'],',');
							$orgs_chks_dates = explode(",",$ans_purify['mchks_dates']);
							$adm_modes = explode(",",$ans_purify['admin_mode']);
							
							for($p_i=0;$p_i<=$ans_purify['extend_chks'];$p_i++)
							{
								if($adm_modes[$p_i]!=1)
								{
									$expiry_date_new = strtotime(date("Y-m-d", strtotime($org_expiry_date)) . " -".$p_i." year");
									$expiry_date_new = date('Y-m-d', $expiry_date_new);
									
									$purchase_date_new = strtotime(date("Y-m-d", strtotime($org_purchase_date)) . " -".$p_i." year");
									$purchase_date_new = date('Y-m-d', $purchase_date_new);
									
									//$ans_purify['purchase_date'] = $purchase_date_new;
									$ans_purify['purchase_date'] = $orgs_chks_dates[$p_i];
									$ans_purify['expiry_date'] = $expiry_date_new;
									
									$final_purify[] = $ans_purify;
								}
							}
						}
					}
				}
			}
			return $final_purify;
		}
		
		function get_media($id)
		{
			$sql = mysql_query("SELECT * FROM general_media WHERE id='".$id."' AND media_status=0 AND delete_status=0");
			return $sql;
		}
		
		function get_creators($id)
		{
			$ans = "";
			$exps = explode('|',$id);
			if($exps[0]=='general_artist')
			{
				$sql = mysql_query("SELECT * FROM general_artist WHERE artist_id='".$exps[1]."'");
			}
			elseif($exps[0]=='general_community')
			{
				$sql = mysql_query("SELECT * FROM general_community WHERE community_id='".$exps[1]."'");
			}
			elseif($exps[0]=='artist_project' || $exps[0]=='community_project')
			{
				$sql = mysql_query("SELECT * FROM $exps[0] WHERE id='".$exps[1]."'");
			}
			
			if(isset($sql))
			{
				if(mysql_num_rows($sql)>0)
				{
					$ans = mysql_fetch_assoc($sql);
				}
			}
			
			return $ans;
		}
		
		function get_buyer($id)
		{
			$sql = mysql_query("SELECT * FROM general_user WHERE general_user_id='".$id."'");
			$ans = mysql_fetch_assoc($sql);
			return $ans;
		}
		
		function get_buyer_mail($id)
		{
			$sql = mysql_query("SELECT * FROM general_user WHERE email='".$id."'");
			$ans = mysql_fetch_assoc($sql);
			return $ans;
		}
		
		function get_bal()
		{
			$sql = mysql_query("SELECT * FROM request_payment WHERE login_email='".$_SESSION['login_email']."'");
			return $sql;
		}
		
		function get_received_bal()
		{
			$sql = mysql_query("SELECT * FROM request_payment WHERE login_email='".$_SESSION['login_email']."' AND date_received!='0000-00-00'");
			return $sql;
		}
		
		function get_trans($id)
		{
			$sql = mysql_query("SELECT * FROM addtocart WHERE id='".$id."' AND del_status=0");
			return $sql;
		}
		
		function get_all_fan_trans()
		{
			$sql = mysql_query("SELECT * FROM add_to_cart_fan_pro WHERE del_status=0 AND buy_status=1");
			return $sql;
		}
		
		function get_all_onlyfanclub_trans()
		{
			$sql = mysql_query("SELECT * FROM add_to_cart_fan_pro WHERE del_status=0 AND buy_status=1 AND fan_member_id=1");
			return $sql;
		}
		
		function get_trans_fans($id)
		{
			
			$sql = mysql_query("SELECT * FROM add_to_cart_fan_pro WHERE id='".$id."' AND del_status=0");
			return $sql;
		}
		
		function get_all_donate()
		{
			$sql = mysql_query("SELECT * FROM donate_to_purify WHERE buy_status=1");
			return $sql;
		}
		
		function get_all_purify_member()
		{
			$sql = mysql_query("SELECT * FROM purify_membership");
			return $sql;
		}
		
		function get_all_totals_cart($month,$year)
		{
				$sql = mysql_query("SELECT SUM( total ) FROM `addtocart` WHERE `buy_status` =1 AND MONTH(chk_date)='".$month."' AND YEAR(chk_date)='".$year."'");
				return $sql;
		}
		
		function get_all_tips_cart($month,$year)
		{
				$sql = mysql_query("SELECT * FROM `addtocart` WHERE `buy_status` =1 AND MONTH(chk_date)='".$month."' AND YEAR(chk_date)='".$year."'");
				return $sql;
		}
		
		function get_all_totals_fan($month,$year)
		{
				$sql = mysql_query("SELECT SUM( total ) FROM `add_to_cart_fan_pro` WHERE `buy_status` =1 AND MONTH(chk_date)='".$month."' AND YEAR(chk_date)='".$year."'");
				return $sql;
		}
		
		function get_all_tips_fan($month,$year)
		{
				$sql = mysql_query("SELECT * FROM `add_to_cart_fan_pro` WHERE `buy_status` =1 AND MONTH(chk_date)='".$month."' AND YEAR(chk_date)='".$year."'");
				return $sql;
		}
		
		function get_all_totals_don($month,$year)
		{
				$sql = mysql_query("SELECT SUM( amount ), SUM( paypal_fee ) FROM `donate_to_purify` WHERE `buy_status` =1 AND MONTH(date)='".$month."' AND YEAR(date)='".$year."'");
				return $sql;
		}
		
		function get_all_totals_cart_year($month)
		{
				$sql = mysql_query("SELECT SUM( total ) FROM `addtocart` WHERE `buy_status` =1 AND YEAR(chk_date)='".$month."'");
				return $sql;
		}
		
		function get_all_tips_cart_year($month)
		{
				$sql = mysql_query("SELECT * FROM `addtocart` WHERE `buy_status` =1 AND YEAR(chk_date)='".$month."'");
				return $sql;
		}
		
		function get_all_totals_fan_year($month)
		{
				$sql = mysql_query("SELECT SUM( total ) FROM `add_to_cart_fan_pro` WHERE `buy_status` =1 AND YEAR(chk_date)='".$month."'");
				return $sql;
		}
		
		function get_all_tips_fan_year($month)
		{
				$sql = mysql_query("SELECT * FROM `add_to_cart_fan_pro` WHERE `buy_status` =1 AND YEAR(chk_date)='".$month."'");
				return $sql;
		}
		
		function get_all_totals_don_year($month)
		{
				$sql = mysql_query("SELECT SUM( amount ), SUM( paypal_fee ) FROM `donate_to_purify` WHERE `buy_status` =1 AND YEAR(date)='".$month."'");
				return $sql;
		}
		
		function get_donation_details($ips)
		{
			$rows = "";
			$sql = mysql_query("SELECT * FROM addtocart WHERE IP_address='".$ips."' AND buy_status=1");
			if($sql!=NULL)
			{
				if(mysql_num_rows($sql)>0)
				{
					$rows = mysql_fetch_assoc($sql);
				}
			}
			else
			{
				$sql = mysql_query("SELECT * FROM add_to_cart_fan_pro WHERE IP_address='".$ips."' AND buy_status=1");
				if($sql!=NULL)
				{
					if(mysql_num_rows($sql)>0)
					{
						$rows = mysql_fetch_assoc($sql);
					}
				}
			}
			return $rows;
		}
		
		function get_purify_member_month($mon,$year)
		{			
			/* $sql = mysql_query("SELECT SUM( cost ) FROM `purify_membership` WHERE MONTH(purchase_date)='".$mon."'");
			return $sql; */
			
			$cost_total = 0;
			$sql = mysql_query("SELECT * FROM purify_membership");
			if($sql!="")
			{
				if(mysql_num_rows($sql)>0)
				{
					while($ans_purify = mysql_fetch_assoc($sql))
					{
						$ans_purify['table_name'] = 'purify_membership';
						$month_chk_curr = date("m",strtotime($ans_purify['purchase_date']));
						$year_chk_curr = date("Y",strtotime($ans_purify['purchase_date']));
						
						$org_purchase_date = $ans_purify['purchase_date'];
						$org_expiry_date = $ans_purify['expiry_date'];
						$ans_purify['mchks_dates'] = trim($ans_purify['mchks_dates'],',');
						$orgs_chks_dates = explode(",",$ans_purify['mchks_dates']);
							
						/* if($month_chk_curr==$mon && $year_chk_curr==$year)
						{							
							$cost_total = $cost_total + $ans_purify['cost'];
						} */
						
						//if($ans_purify['extend_chks']!=0)
						//{
						if($ans_purify['extend_chks']>0)
						{
							for($p_i=0;$p_i<=$ans_purify['extend_chks'];$p_i++)
							{								
								$purchase_date_new = strtotime(date("Y-m-d", strtotime($orgs_chks_dates[$p_i])) . " -".$p_i." year");
								$purchase_date_new = date('Y-m-d', $purchase_date_new);
								
								$month_ex_curr = date("m",strtotime($orgs_chks_dates[$p_i]));
								$year_ex_curr = date("Y",strtotime($orgs_chks_dates[$p_i]));
								
								if($month_ex_curr==$mon && $year_ex_curr==$year)
								{
									$cost_total = $cost_total + $ans_purify['cost'];
								}
							}
						}
						else
						{
							if($ans_purify['lifetime']!="1" && $ans_purify['admin_mode']!="1,")
							{
								$purchase_date_new = strtotime(date("Y-m-d", strtotime($org_purchase_date)) . " -".$p_i." year");
								$purchase_date_new = date('Y-m-d', $purchase_date_new);
								
								$month_ex_curr = date("m",strtotime($org_purchase_date));
								$year_ex_curr = date("Y",strtotime($org_purchase_date));
								
								if($month_ex_curr==$mon && $year_ex_curr==$year)
								{
									$cost_total = $cost_total + $ans_purify['cost'];
								}
							}
						}
						//}
					}
				}
			}
			return $cost_total;
		}
		
		function get_purify_member_year($mon)
		{			
			/* $sql = mysql_query("SELECT SUM( cost ) FROM `purify_membership` WHERE YEAR(purchase_date)='".$mon."'");
			if($sql=="")
			{
				
				for($p_i=0;$p_i<=$whole_data[$mon]['extend_chks'];$p_i++)
				{
					$expiry_date_new = strtotime(date("Y-m-d", strtotime($whole_data[$mon]['expiry_date'])) . " -".$p_i." year");
					$expiry_date_new = date('Y-m-d', $expiry_date_new);
					
					$purchase_date_new = strtotime(date("Y-m-d", strtotime($whole_data[$mon]['purchase_date'])) . " -".$p_i." year");
					$purchase_date_new = date('Y-m-d', $purchase_date_new);
					
					$whole_data[$mon]['purchase_date'] = $purchase_date_new;
					$whole_data[$mon]['expiry_date'] = $expiry_date_new;
					
					//$final_purify[] = $ans_purify;
				}
			}
			else
			{
				if(mysql_num_rows($sql)<=0)
				{
					
				}
			} */
			$cost_total = 0;
			$sql = mysql_query("SELECT * FROM purify_membership");
			if($sql!="")
			{
				if(mysql_num_rows($sql)>0)
				{
					while($ans_purify = mysql_fetch_assoc($sql))
					{
						$ans_purify['table_name'] = 'purify_membership';
						$month_chk_curr = date("Y",strtotime($ans_purify['purchase_date']));
						
						$org_purchase_date = $ans_purify['purchase_date'];
						$org_expiry_date = $ans_purify['expiry_date'];
						$ans_purify['mchks_dates'] = trim($ans_purify['mchks_dates'],',');
						$orgs_chks_dates = explode(",",$ans_purify['mchks_dates']);
						
						/* if($ans_purify['extend_chks']==0 && $month_chk_curr==$mon)
						{							
							$cost_total = $cost_total + $ans_purify['cost'];
						} */
						
						//if($ans_purify['extend_chks']!=0)
						//{
						if($ans_purify['extend_chks']>0)
						{
							for($p_i=0;$p_i<=$ans_purify['extend_chks'];$p_i++)
							{								
								$purchase_date_new = strtotime(date("Y-m-d", strtotime($orgs_chks_dates[$p_i])) . " -".$p_i." year");
								$purchase_date_new = date('Y-m-d', $purchase_date_new);
								
								$month_ex_curr = date("Y",strtotime($orgs_chks_dates[$p_i]));
								
								if($month_ex_curr==$mon)
								{
									$cost_total = $cost_total + $ans_purify['cost'];
								}
							}
						}
						else
						{
							if($ans_purify['lifetime']!="1" && $ans_purify['admin_mode']!="1,")
							{
								$purchase_date_new = strtotime(date("Y-m-d", strtotime($org_purchase_date)) . " -".$p_i." year");
								$purchase_date_new = date('Y-m-d', $purchase_date_new);
								
								$month_ex_curr = date("Y",strtotime($org_purchase_date));
								
								if($month_ex_curr==$mon)
								{
									$cost_total = $cost_total + $ans_purify['cost'];
								}
							}
						}
						//}
					}
				}
			}
			return $cost_total;
		}
		
		function checks_payment()
		{
			$ans = array();
			$sql = mysql_query("SELECT * FROM request_payment WHERE check_pay=1 AND paypal=0");
			if($sql!="")
			{
				if(mysql_num_rows($sql)>0)
				{
					while($row = mysql_fetch_assoc($sql))
					{
						$ans[] = $row;
					}
				}
			}
			return $ans;
		}
		
		function checks_month_pay($month,$year)
		{
			$sql = mysql_query("SELECT *,count(*) FROM `request_payment` WHERE MONTH(date_sent)='".$month."' AND YEAR(date_sent)='".$year."' AND check_pay=1 AND paypal=0");
			return $sql;
		}
		
		function checks_year_pay($month)
		{
			$sql = mysql_query("SELECT *,count(*) FROM `request_payment` WHERE YEAR(date_sent)='".$month."' AND check_pay=1 AND paypal=0");
			return $sql;
		}
	}
?>