<?php
	
	
	
	class NewsletterEmail
	{
		
		function send()
		{
			$sendgrid = new SendGrid('','');
			$boundary = uniqid('np');
			
			$to=$this->recipient;
			$from="web@purifyentertainment.net";
			$subject="Newsletter";
			
			$headers = "MIME-Version: 1.0\r\n";
			$headers .= "From: Purify Entertainment <".$from.">\r\n";
			$headers .= "Subject: Newsletter\r\n";
			$headers .= "Content-Type: multipart/alternative;boundary=" . $boundary . "\r\n";
			//$headers .= "Bcc: ".$this->recipient;
			
			$message = "This is a MIME encoded message."; 
			
			$message .= "\r\n\r\n--" . $boundary . "\r\n";
			$message .= "Content-type: text/plain;charset=utf-8\r\n\r\n";
			
			$message .= "\r\n\r\n--" . $boundary . "\r\n";
			$message .= "Content-type: text/html;charset=utf-8\r\n\r\n";
			
			$this->appendlink ="
			
------------------------------------------------------------------------------------------------------------------------------------------<br />
Click below if you wish to unsubscribe our newsletter service.<br />
".$this->elink ."<br />
------------------------------------------------------------------------------------------------------------------------------------------
<br />Purify Entertainment<br />";
			$message .= "<br />Hi ".$this->name.",<br />";
			//$message .= "You are receiving the Purify newsletter... <br/>";
			$message .=$this->content;
			$message .=$this->appendlink;
			
			$url = "../newsletter/";  
			$myFile = $url . "other_templates.txt";
			$fh = fopen($myFile, 'r');
			$theData = fread($fh, filesize($myFile));
			fclose($fh);
			
			$data_email = explode('|~|', $theData);
			
			$ns_user_n = str_replace('{::message::}', $message, $data_email['1']);
			
			$body_email = $data_email['0'] . $ns_user_n . $data_email['2'];
			
			$mail_grid = new SendGrid\Mail();
			
			$mail_grid->addTo($to)->
					setFromName('Purify Art')->
					setFrom('no-reply@purifyart.com')->
					setSubject($subject)->
					setHtml($body_email);
					
			//$sendgrid->web->send($mail_grid);
			$sendgrid -> smtp -> send($mail_grid);
			
			//mail($to,$subject,$message,$headers);
		}
	}
?>
