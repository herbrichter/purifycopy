<?php
include_once('commons/db.php');

class Media
{

	function addGalleryImagesEntry($title,$files,$profile_id,$profile_type)
	{
		$query="insert into general_".$profile_type."_gallery_list (`gallery_title`,`created_date`) values ('".$title."',CURDATE())";
		$rs=mysql_query($query) or die(mysql_error());
		$gallery_id=mysql_insert_id();
		if($gallery_id){
			$fileList = explode("|",$files);
			for($i=0;$i<count($fileList);$i++){
				if($fileList[$i]!=""){
					$query="insert into general_".$profile_type."_gallery (`profile_id`,`gallery_id`,`image_name`,`created_date`) values ('$profile_id','".$gallery_id."','".$fileList[$i]."',CURDATE())";
					$rs=mysql_query($query) or die(mysql_error());
				}
			}
		}
	}

	function addMediaAudiosEntry($title,$files,$profile_id,$profile_type)
	{
		$fileList = explode("|",$files);
		for($i=0;$i<count($fileList);$i++){
			if($fileList[$i]!=""){
				$query="insert into general_".$profile_type."_audio (`profile_id`,`audio_name`,`audio_file_name`,`created_date`) values ('$profile_id','".$title."','".$fileList[$i]."',CURDATE())";
				$rs=mysql_query($query) or die(mysql_error());
			}
		}
	}

	function addMediaAudioLinkEntry($title,$link,$profile_id,$profile_type)
	{
		if($link!=""){
			$query="insert into general_".$profile_type."_audio (`profile_id`,`audio_name`,`audio_link`,`created_date`) values ('$profile_id','".$title."','".$link."',CURDATE())";
			$rs=mysql_query($query) or die(mysql_error());
		}
	}

	function addMediaVideoLinkEntry($title,$link,$profile_id,$profile_type)
	{
		if($link!=""){
			$query="insert into general_".$profile_type."_video (`profile_id`,`video_name`,`video_link`,`created_date`) values ('$profile_id','".$title."','".$link."',CURDATE())";
			$rs=mysql_query($query) or die(mysql_error());
		}
	}

	function getGalleryImagesByProfileId($profile_id,$profile_type)
	{
		$sql="select image_name from general_".$profile_type."_gallery where profile_id='".$profile_id."'";
		$rs=mysql_query($sql) or die(mysql_error());
		return mysql_fetch_assoc($rs);	  	
	}  

	/*function searchMediaByProfileId($profile_id,$profile_type){
		$mediaType = "";
		$resultMediaArray = Array();
		$sql="SELECT L.*,G.image_name,G.image_title FROM general_".$profile_type."_gallery as G, general_".$profile_type."_gallery_list as L 
			WHERE L.gallery_id=G.gallery_id 
			AND G.profile_id='".$profile_id."' GROUP BY L.gallery_id";
		$rs=mysql_query($sql) or die(mysql_error());
		if(mysql_num_rows($rs)){
			$mediaType = "gallery";
			$resultArray = Array();
			$gCount = 0;
			while ($row = mysql_fetch_assoc($rs)) {
				$resultArray[$gCount][0] = $row;
				$imagesArr = Array();
				$sql1="SELECT G.* FROM general_".$profile_type."_gallery as G, general_".$profile_type."_gallery_list as L 
					WHERE L.gallery_id=G.gallery_id 
					AND G.profile_id='".$profile_id."' AND L.gallery_id='".$row['gallery_id']."'";
				$rs1=mysql_query($sql1) or die(mysql_error());
				if(mysql_num_rows($rs1)){
					while ($row1 = mysql_fetch_assoc($rs1)) {						
						$imagesArr[] = $row1;						
					}
					$resultArray[$gCount][1] = $imagesArr;
					$gCount++;
				}
			}

			$resultMediaArray[0][0] = $mediaType;
			$resultMediaArray[0][1] = $resultArray;
		}
		
		$sql="SELECT * FROM general_".$profile_type."_audio WHERE profile_id='".$profile_id."'";
		$rs=mysql_query($sql) or die(mysql_error());
		if(mysql_num_rows($rs)){
			$mediaType = "audio";
			$resultArray = Array();
			while ($row = mysql_fetch_assoc($rs)) {
				$resultArray[] = $row;
			}			
			$resultMediaArray[1][0] = $mediaType;
			$resultMediaArray[1][1] = $resultArray;
		}
				
		$sql="SELECT * FROM general_".$profile_type."_video WHERE profile_id='".$profile_id."'";
		$rs=mysql_query($sql) or die(mysql_error());
		if(mysql_num_rows($rs)){
			$mediaType = "video";
			$resultArray = Array();
			while ($row = mysql_fetch_assoc($rs)) {
				$resultArray[] = $row;
			}			
			$resultMediaArray[2][0] = $mediaType;
			$resultMediaArray[2][1] = $resultArray;
		}
//		print_r($resultMediaArray);
		return $resultMediaArray;
	}*/
	
	function searchMediaByProfileId($profile_id,$profile_type){
		$mediaType = "";
		$resultMediaArray = Array();
		$resultMediaArray[0] = "";
		$resultMediaArray[1] = "";
		
		$sql="SELECT L.*, G.* FROM general_".$profile_type."_gallery as G, general_".$profile_type."_gallery_list as L 
			WHERE L.gallery_id=G.gallery_id 
			AND G.profile_id='".$profile_id."' ";
		$rs=mysql_query($sql) or die(mysql_error());
		if(mysql_num_rows($rs)){
			$mediaType = "gallery";
			$resultArray = Array();
			while ($row = mysql_fetch_assoc($rs)) {
				//$row['table_name'] = "general_".$profile_type."_gallery";
				$resultArray[] = $row;
			}
		}
		else{
				$sql="SELECT * FROM general_".$profile_type."_audio WHERE profile_id='".$profile_id."'";
				$rs=mysql_query($sql) or die(mysql_error());
				if(mysql_num_rows($rs)){
					$mediaType = "audio";
					$resultArray = Array();
					while ($row = mysql_fetch_assoc($rs)) {
						$row['table_name'] = "general_".$profile_type."_audio";
						$resultArray[] = $row;
					}			
				}
				else{
					$sql="SELECT * FROM general_".$profile_type."_video WHERE profile_id='".$profile_id."'";
					$rs=mysql_query($sql) or die(mysql_error());
					if(mysql_num_rows($rs)){
						$mediaType = "video";
						$resultArray = Array();
						while ($row = mysql_fetch_assoc($rs)) {
							$row['table_name'] = "general_".$profile_type."_video";
							$resultArray[] = $row;
						}			
					}
				}
		}
		if($mediaType!=""){
			$resultMediaArray[0] = $mediaType;
			//echo "Media Type".$resultMediaArray[0]."";
			$resultMediaArray[1] = $resultArray;
		}
		return $resultMediaArray;
	}

}
?>