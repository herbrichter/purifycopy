<?php
	$sendgrid = new SendGrid('','');
	
	class ApprovalEmail
	{
		function send()
		{
			$boundary = uniqid('np');
			
			$to=$this->recipient;
			$from="web@purifyentertainment.net";
			$subject="Account approved";
			
			$headers = "MIME-Version: 1.0\r\n";
			$headers .= "From: Purify Entertainment <".$from.">\r\n";
			$headers .= "Subject: ".$subject."\r\n";
			$headers .= "Content-Type: multipart/alternative;boundary=" . $boundary . "\r\n";
			//$headers .= "Bcc: ".$this->recipient;
			
			$message = "This is a MIME encoded message."; 
			
			$message .= "\r\n\r\n--" . $boundary . "\r\n";
			$message .= "Content-type: text/plain;charset=utf-8\r\n\r\n";
			
			$message .= "\r\n\r\n--" . $boundary . "\r\n";
			$message .= "Content-type: text/html;charset=utf-8\r\n\r\n";
			
			$this->content="
<br />
Hello ".$this->name.",<br />
<br />
Your user account has been approved and you can now login to <a href='www.purifyentertainment.net'>Purify Entertainment</a> and start streaming media.<br />
<br />";
			$this->appendlink ="
------------------------------------------------------------------------------------------------------------------------------------------
<br />Purify Entertainment<br />";
			
			$message .=$this->content;
			$message .=$this->appendlink;
			$url = "../newsletter/";  
			$myFile = $url . "other_templates.txt";
			$fh = fopen($myFile, 'r');
			$theData = fread($fh, filesize($myFile));
			fclose($fh);
			
			$data_email = explode('|~|', $theData);
			
			$ns_user_n = str_replace('{::message::}', $message, $data_email['1']);
			
			$body_email = $data_email['0'] . $ns_user_n . $data_email['2'];
			
			$mail_grid = new SendGrid\Mail();
			
			$mail_grid->addTo($to)->
					setFromName('Purify Art')->
					setFrom('no-reply@purifyart.com')->
					setSubject($subject)->
					setHtml($body_email);
					
			//$sendgrid->web->send($mail_grid);
			$sendgrid -> smtp -> send($mail_grid);
			
			//mail($to,$subject,$message,$headers);
		}
	}
?>
