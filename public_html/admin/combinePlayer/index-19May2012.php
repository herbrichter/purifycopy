<!doctype html>
<?php 
	$playlist = "../player/playlist/playlist".$_GET['pl'].".xml";
	$xml = new DOMDocument();
	if (file_exists($playlist)) {
		$xml->load($playlist);
		$songtags = $xml->getElementsByTagName('song');
		$songCounter=0;
		foreach ($songtags as $songtag)
		{
			$type = $songtag->getElementsByTagName('type');
			foreach ($type as $attr) 
			{ 
				$songProps[$songCounter]['type'] = $attr->nodeValue; 
			} 
			switch($songProps[$songCounter]['type']){
				case "audio":
						$title = $songtag->getElementsByTagName('songtitle');
						foreach ($title as $attr) 
						{ 
							$songProps[$songCounter]['title'] = $attr->nodeValue; 
						} 
						$filename = $songtag->getElementsByTagName('songfilename');
						foreach ($filename as $attr) 
						{ 
							$songProps[$songCounter]['filename'] = $attr->nodeValue; 
						} 

						$gallerytags = $songtag->getElementsByTagName('image');
						$galleryArray = Array();
						foreach ($gallerytags as $galleryImageTag)
						{
							$id = $galleryImageTag->getElementsByTagName('id');
							foreach ($id as $attr) 
							{ 
								$gArray['id'] = $attr->nodeValue; 
							} 
							//$gArray['id'] = $id->firstChild->nodeValue; 
							$title = $galleryImageTag->getElementsByTagName('title');
							foreach ($title as $attr) 
							{ 
								$gArray['title'] = $attr->nodeValue; 
							} 
							//$gArray['title'] = $title->nodeValue;
							$filename = $galleryImageTag->getElementsByTagName('filename');
							foreach ($filename as $attr) 
							{ 
								$gArray['filename'] = $attr->nodeValue; 
							} 
							//$gArray['filename'] = $filename->nodeValue;
							
							$galleryArray[] = $gArray;
						}
						$songProps[$songCounter]['galleryArray'] = $galleryArray;
				
				break;
				case "video":
						$title = $songtag->getElementsByTagName('title');
						foreach ($title as $attr) 
						{ 
							$songProps[$songCounter]['title'] = $attr->nodeValue; 
						} 
						$videoId = $songtag->getElementsByTagName('videoId');
						foreach ($videoId as $attr) 
						{ 
							$songProps[$songCounter]['videoId'] = $attr->nodeValue; 
						} 
				break;
			}
			$songCounter++;
		}
	}
	//print_r($galleryArray);
	
?>
<html style="height: 100%;">
	<head>
	<link rel="stylesheet" href="sc-player-minimal.css" type="text/css" />
    <script src="../js/jquery.min.js"></script>
<!--		<script src="popcorn-complete-0.4.1-dist.js"></script>
		 <script src="popcorn.soundcloud.js"></script>
		<script src="popcorn.image.js"></script>
	    <script src="popcorn.code.js"></script>
 -->	    <script src="soundcloud.player.api.js"></script>
	    <script src="sc-player.js"></script>
	<script>
	var playlist = '<?php echo $playlist;?>';
	var $whiplash;
	document.addEventListener("DOMContentLoaded", function() {
/*		var $whiplash1 = Popcorn( Popcorn.soundcloud( "whiplash1", {
				  api_key: "PRaNFlda6Bhf5utPjUsptg",
				  commentdiv: "commentDisplay",
				}) )
				.play();
				  
				document.getElementById( "btnPlay" ).addEventListener( "click", function() {
				  $whiplash1.play();
				}, false);
				
				document.getElementById( "btnPause" ).addEventListener( "click", function() {
				  $whiplash1.pause();
				}, false);
				
				document.getElementById( "btnSeek" ).addEventListener( "click", function() {
				  $whiplash1.currentTime( 30 );
				}, false);
				
				document.getElementById( "btnVolume" ).addEventListener( "click", function() {
				  if ( popcorn.volume() >= 0.5 ) {
					popcorn.volume( popcorn.volume()/2 );
					this.innerHTML = "Double Volume";
				  } else {
					popcorn.volume( popcorn.volume()*2 );
					this.innerHTML = "Halve Volume";
				  }
				}, false);
				
				document.getElementById( "btnMute" ).addEventListener( "click", function() {
				  popcorn.mute();
				}, false);*/
				
	}, false);

	$(document).ready(function(){		
		/*$.ajax({
			type: "GET",
			url: playlist,
			dataType: "xml",
			success: parseXml
		  });
		  */
	});
	function parseXml(xml)
	{
	  //find every Tutorial and print the author
	  $(xml).find("song").each(function()
	  {
		//alert($(this).attr("id"));
	  });

	  // Output:
	  // The Reddest
	  // The Hairiest
	  // The Tallest
	  // The Fattest
	}

	function loadytPlayer(vid){
		$whiplash = Popcorn( Popcorn.youtube( "whiplash", "http://www.youtube.com/watch?v="+vid ) );
	}
	</script>	
</head>
<body style="height: 100%;margin:0px;">
<div id="tempd" style="width:100%;height:100%; background:#ccc">
<?php if($songProps[0]['type']=="audio"){ ?>
	<?php $encodedGallery = urlencode(json_encode($songProps[0]['galleryArray'])); ?>
	<iframe id="gFrame" src="youtube.php?gl=<?php echo $encodedGallery;?>" width="100%" height="350px"  style="padding: 0px; margin: 0px;border:0px;"></iframe>
	<div id="audioPlayer_wrapper">
		<a href="<?php echo $songProps['filename'];?>" class="sc-player"><?php echo $songProps[0]['title'];?></a>
		<!-- <div id="whiplash1" data-width="" data-height="" data-src="<?php echo $songProps['filename'];?>" ></div>
		<div>
			<button class="simple" id="btnPlay">Play</button>
			<button class="simple" id="btnPause">Pause</button>
			<button class="seek" id="btnSeek">Seek to 30</button>
			<button class="volume" id="btnVolume">Toggle Volume</button>
			<button class="volume" id="btnMute">Mute</button><br />
		</div> -->
	</div>
<?php }else if($songProps[0]['type']=="video"){ ?>
	<div id="whiplash" width="575" height="400"></div>
	<script>loadytPlayer('<?php echo $songProps[0]["videoId"];?>');</script>
<?php } ?>
</div>
</body>
</html>