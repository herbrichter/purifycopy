<!doctype html>
<?php 
	$playlist = "../player/playlist/playlist".$_GET['pl'].".xml";
	$xml = new DOMDocument();
	if (file_exists($playlist)) {
		$xml->load($playlist);
		$songtags = $xml->getElementsByTagName('song');
		$songCounter=0;
		foreach ($songtags as $songtag)
		{
			$type = $songtag->getElementsByTagName('type');
			foreach ($type as $attr) 
			{ 
				$songProps[$songCounter]['type'] = $attr->nodeValue; 
			} 
			switch($songProps[$songCounter]['type']){
				case "audio":
						$title = $songtag->getElementsByTagName('songtitle');
						foreach ($title as $attr) 
						{ 
							$songProps[$songCounter]['title'] = $attr->nodeValue; 
						} 
						$creatorNode = $songtag->getElementsByTagName('creator');
						foreach ($creatorNode as $attr) 
						{ 
							$songProps[$songCounter]['creator'] = $attr->nodeValue; 
						} 
						$gallery_titleNode = $songtag->getElementsByTagName('gallery_title');
						foreach ($gallery_titleNode as $attr) 
						{ 
							$songProps[$songCounter]['gallery_title'] = $attr->nodeValue; 
						} 
						$filename = $songtag->getElementsByTagName('songfilename');
						foreach ($filename as $attr) 
						{ 
							$songProps[$songCounter]['filename'] = $attr->nodeValue; 
						} 

						$gallerytags = $songtag->getElementsByTagName('image');
						$galleryArray = Array();
						foreach ($gallerytags as $galleryImageTag)
						{
							$id = $galleryImageTag->getElementsByTagName('id');
							foreach ($id as $attr) 
							{ 
								$gArray['id'] = $attr->nodeValue; 
							} 
							$imageType = $galleryImageTag->getElementsByTagName('imageType');
							foreach ($imageType as $attr) 
							{ 
								$gArray['imageType'] = $attr->nodeValue; 
							} 
							//$gArray['id'] = $id->firstChild->nodeValue; 
							$title = $galleryImageTag->getElementsByTagName('title');
							foreach ($title as $attr) 
							{ 
								$gArray['title'] = $attr->nodeValue; 
							} 
							//$gArray['title'] = $title->nodeValue;
							$filename = $galleryImageTag->getElementsByTagName('filename');
							foreach ($filename as $attr) 
							{ 
								$gArray['filename'] = $attr->nodeValue; 
							} 
							//$gArray['filename'] = $filename->nodeValue;
							
							$galleryArray[] = $gArray;
						}
						$songProps[$songCounter]['galleryArray'] = urlencode(json_encode($galleryArray));
				
				break;
				case "video":
						$title = $songtag->getElementsByTagName('title');
						foreach ($title as $attr) 
						{ 
							$songProps[$songCounter]['title'] = $attr->nodeValue; 
						} 
						$videoId = $songtag->getElementsByTagName('videoId');
						foreach ($videoId as $attr) 
						{ 
							$songProps[$songCounter]['videoId'] = $attr->nodeValue; 
						} 
						$videoLink = $songtag->getElementsByTagName('videoLink');
						foreach ($videoLink as $attr) 
						{ 
							$songProps[$songCounter]['videolink'] = $attr->nodeValue; 
						} 
				break;
			}
			$songCounter++;
		}
	}
	//print_r($galleryArray);
	
?>
<html style="height: 100%;">
	<head>
	<link rel="stylesheet" href="sc-player-minimal.css" type="text/css" />
	
    <script src="../js/jquery.min.js"></script>
		<script src="popcorn-complete-0.4.1-dist.js"></script>
<!--		 <script src="popcorn.soundcloud.js"></script>
		<script src="popcorn.image.js"></script>
	    <script src="popcorn.code.js"></script>
 -->	    
		<script src="soundcloud.player.api.js"></script>
	    <script src="sc-player.js"></script>
		<script src="mediaelement-and-player.js"></script>
		<link rel="stylesheet" href="mediaelementplayer.css" />
		<script src="http://www.google.com/jsapi" type="text/javascript"></script>
	<script>
      google.load("swfobject", "2.1");
	var playlist = '<?php echo $playlist;?>';
	var playlistTime = '<?php echo $_GET["pl"];?>';
	var $tyPlayer;
	var $scPlayer;
	var arrFrames = "";
	var currMediaTrackIndex = "";
	var currPlayingTrackID = 0;
	var syncPlaylistTimer = null;
	var ShuffleFlag = 0;
	var player_state = "pause";
	var mp3PlayerObj = null;
	var flvPlayerObj = null;
	var mp3Path = "http://localhost/purifyart5/uploads/audio/";
	var flvPath = "http://localhost/purifyart5/uploads/video/";
	document.addEventListener("DOMContentLoaded", function() {
		arrFrames = parent.document.getElementsByTagName("IFRAME");
		$tyPlayer = Popcorn.youtube( "whiplash");
		currMediaTrackIndex=0;
		//loadPlayer();
		playThisMedia(currMediaTrackIndex);
/*		var $whiplash1 = Popcorn( Popcorn.soundcloud( "whiplash1", {
				  api_key: "PRaNFlda6Bhf5utPjUsptg",
				  commentdiv: "commentDisplay",
				}) )
				.play();
				  
				document.getElementById( "btnPlay" ).addEventListener( "click", function() {
				  $whiplash1.play();
				}, false);
				
				document.getElementById( "btnPause" ).addEventListener( "click", function() {
				  $whiplash1.pause();
				}, false);
				
				document.getElementById( "btnSeek" ).addEventListener( "click", function() {
				  $whiplash1.currentTime( 30 );
				}, false);
				
				document.getElementById( "btnVolume" ).addEventListener( "click", function() {
				  if ( popcorn.volume() >= 0.5 ) {
					popcorn.volume( popcorn.volume()/2 );
					this.innerHTML = "Double Volume";
				  } else {
					popcorn.volume( popcorn.volume()*2 );
					this.innerHTML = "Halve Volume";
				  }
				}, false);
				
				document.getElementById( "btnMute" ).addEventListener( "click", function() {
				  popcorn.mute();
				}, false);*/
				
	}, false);

	$(document).ready(function(){		
		
		$("#play_btn").click(function(){
			if(player_state=="pause"){
				$(this).removeClass("pause_btn").addClass("play_btn");
			}else{
				$(this).removeClass("play_btn").addClass("pause_btn");
			}
			playThisMedia(currMediaTrackIndex);
		});
		$("#prev_btn").click(function(){ //alert(currMediaTrackIndex);
			if(ShuffleFlag==1){
				var randomNumber = Math.floor(Math.random() * ((mediaList.length-1) - 0 + 1)) + 0;
				currMediaTrackIndex = randomNumber;
			}else if(currMediaTrackIndex>0)
				currMediaTrackIndex--;

			player_state="pause";
			playThisMedia(currMediaTrackIndex);		
		});
		$("#next_btn").click(function(){  
			var mediaList = eval('('+encodedMedia+')');
			if(currMediaTrackIndex==mediaList.length-1)
				currMediaTrackIndex=0;
			else if(ShuffleFlag==1){
				var randomNumber = Math.floor(Math.random() * ((mediaList.length-1) - 0 + 1)) + 0;
				currMediaTrackIndex = randomNumber;
			}else
				currMediaTrackIndex++;

			player_state="pause";
			playThisMedia(currMediaTrackIndex);
		
		});
		$("#shuffle_btn").click(function(){  
			toggleShuffle();
		});
		$("#clearlist_btn").click(function(){ 
			clearInterval(syncPlaylistTimer);
			$.ajax({
				url: "../player/getMediaInfo.php",
				data: { "act":"clearList", "pl":playlistTime },
				success: function(){
					$(".tableCont").remove();
					hideOtherPlayers();
					//syncPlaylist();
					syncPlaylistTimer=self.setInterval("syncPlaylist()",20000);
				}
			});
		});
		
		$("#fs_btn").click(function(){ 
			toggleFullScreen('player_wall');
		});

		//syncPlaylistTimer=self.setInterval("syncPlaylist()",20000);
	});
	function syncPlaylist(){
		$.ajax({
			url: "../player/getMediaInfo.php",
			data: { "act":"getList", "pl":playlistTime },
			success: parseXml
		  });
	}
	function parseXml(xml)
	{
		encodedMedia = xml;
	  //find every Tutorial and print the author
	  $(".tableCont").remove();
	  var mediaList = eval('('+encodedMedia+')');
	  if(mediaList==null)return;
	  for(var i=0;i<mediaList.length;i++){
	    var mediaData = mediaList[i];
		var newHTML = '<div class="tableCont">';
			newHTML += '<div class="blkB"><a href="javascript:void(0);" onclick="javascript:playThisMedia('+i+');">'+mediaData.title+'</a></div>';
			newHTML += '<div class="blkC">'+mediaData.creator+'</div>';
			newHTML += '<div class="blkD">'+((mediaData.gallery_title)?mediaData.gallery_title:'')+'</div>';		
			newHTML += '<div class="blkE">'+mediaData.type+'</div>';		
			newHTML += '<div class="blkF">D</div>';		
			newHTML += '<div class="blkA"><div class="closeSong" onclick="removeSong(\''+mediaData.songId+'\',this,'+i+')"></div></div>';
			newHTML += '</div>';
		$("#mediascroller").append(newHTML);
		if(currMediaTrackIndex==i){
			$(".currSelected").removeClass('currSelected');
			$(".tableCont:eq("+currMediaTrackIndex+")").addClass('currSelected');
		}
	  }
	}
	function removeSong(songId,obj,sIndex){
		$.ajax({
			url: "../player/getMediaInfo.php",
			data: { "act":"removeSong","songId":songId, "pl":playlistTime },
			success: function(){
				$(obj).parent().parent().remove();
				syncPlaylist();
				if(currMediaTrackIndex==sIndex){
					hideOtherPlayers();
					trackEndFunc();
				}
			}
		});
	}
	function loadytPlayer(vid){
		hideOtherPlayers();
		$("#videoPlayerContainer").html('<div id="whiplash" width="575" height="400"></div>').show();
		$tyPlayer = Popcorn( Popcorn.youtube( "whiplash", "http://www.youtube.com/watch?autoplay=1&v="+vid ) ).playVideo() ;
		//$tyPlayer.listen( "ended", function() { trackEndFunc(); });
	}
	function loadscPlayer(slink,stitle,gArr){
		var obj = eval('('+unescape(gArr)+')');
		if(obj.length>1){ timer=1; }else{	timer=0; }
		arrFrames[0].src = "youtube.php?gl="+gArr+"&timer="+timer;
		$("#audioPlayer_wrapper").html('<a href="'+slink+'" class="sc-player">'+stitle+'</a>');
		$('.sc-player').scPlayer({
		  links: [{url: slink, title: stitle}],
		  autoPlay  :  true
		});
		$("#audioPlayer_wrapper").show();
	}
	function loadMp3Player(slink,gArr){
	//alert(mp3Path);
	//alert(slink);
		var obj = eval('('+unescape(gArr)+')');
		if(obj.length>1){ timer=1; }else{	timer=0; }
		arrFrames[0].src = "youtube.php?gl="+gArr+"&timer="+timer;
		$("#mp3Player_wrapper").html('<audio id="mp3player" src="'+slink+'" type="audio/mp3" controls="controls"  style="width:100%;"></audio>');
		mp3PlayerObj = new MediaElementPlayer('#mp3player', {success: function(player, node){ player.play(); },pauseOtherPlayers: true });
		//mp3PlayerObj.setSrc(mp3Path + mediaData.filename);
		//mp3PlayerObj.play();
		$("#mp3Player_wrapper").show();
	}
	function loadFlvPlayer(slink){
		hideOtherPlayers();
		$("#flvPlayer_wrapper").html('<video src="'+ flvPath + slink +'" width="575" height="360" type="video/flv" id="flvPlayer" controls="controls" preload="none"></video><span id="player1-mode"></span>');
		flvPlayerObj = new MediaElementPlayer('#flvPlayer',{
						features: ['playpause','progress','current','duration','tracks','volume','fullscreen'], 
						success: function(player, node){ player.play(); player.addEventListener('ended', trackEndFunc, false); },
						pauseOtherPlayers: true
				   });		
		$("#flvPlayer_wrapper").show();
	}
	function readyPlayerCheck(playerController){
		playerController.api_setVolume(50);
	}
	function trackEndFunc(){
		player_state="pause";
		if(ShuffleFlag==1){
			var mediaList = eval('('+encodedMedia+')');
			var randomNumber = Math.floor(Math.random() * ((mediaList.length-1) - 0 + 1)) + 0;
			currMediaTrackIndex = randomNumber;			
		}else{
			currMediaTrackIndex = currMediaTrackIndex+1;		
		}
		playThisMedia(currMediaTrackIndex);
	}
	function playThisMedia(mediaIndex){
		var mediaList = eval('('+encodedMedia+')');
		if(mediaList==null)return;
		var mediaData = mediaList[mediaIndex];//alert(mediaData.toSource());
		if(player_state=="pause"){
			player_state="play";
			if(mediaData.type=="audio"){
				hideOtherPlayers();
				$("#audioPlayerContainer").show();
				if(mediaData.filename.match(/\.(mp3)$/)){
					loadMp3Player(mediaData.filename,mediaData.galleryArray)
				}else{
					//$('.sc-controls a.sc-play').click();
					loadscPlayer(mediaData.filename,mediaData.title,mediaData.galleryArray);
				}
					currMediaTrackIndex = mediaIndex;
				//}
			}else if(mediaData.type=="video"){//alert(mediaData.toSource());
				hideOtherPlayers();
				if(mediaData.videolink && mediaData.videolink.match(/\.(flv)$/)){
					loadFlvPlayer(mediaData.videolink);
				}else{
					loadytPlayer(mediaData.videoId);
				}
				currMediaTrackIndex=mediaIndex;
			}
		}else if(player_state=="play"){
			player_state="pause";
			if(currMediaTrackIndex==mediaIndex){
				if(mediaData.type=="audio"){				
					$('.sc-player.playing a.sc-pause').click();//$scPlayer.pause();
				}else if(mediaData.type=="video"){
					$tyPlayer.pauseVideo();
				}
			}else{
				$("div[rel=Track-"+mediaIndex+"]").click();
			}
			currMediaTrackIndex = mediaIndex;
		}
		$(".currSelected").removeClass('currSelected');
		$(".tableCont:eq("+currMediaTrackIndex+")").addClass('currSelected');
		//for(i=0;i<mediaList.length;i++){	}
	}

	function toggleShuffle(){
		if(ShuffleFlag==1){
			ShuffleFlag=0;
			$("#shuffle_btn").removeClass("shuffle_btn").addClass("repeat_btn");
			//$("#shuffleDiv").css('background-color',"#000");
		}else{
			ShuffleFlag=1;
			$("#shuffle_btn").removeClass("repeat_btn").addClass("shuffle_btn");
			//$("#shuffleDiv").css('background-color',"red");
		}
	}

	function hideOtherPlayers(){
		if(mp3PlayerObj) mp3PlayerObj.pause();
		if(flvPlayerObj) flvPlayerObj.pause();
		//if($tyPlayer)$tyPlayer.pauseVideo();
		$('.sc-player.playing a.sc-pause').click();
		$("#audioPlayer_wrapper").html('').hide();
		$("#mp3Player_wrapper").hide();
		$("#flvPlayer_wrapper").hide();
		$("#videoPlayerContainer").html('').hide();
		$("#audioPlayerContainer").hide();
	}
	var encodedMedia = '<?php echo json_encode($songProps); ?>';
	//alert(encodedMedia);

	  function goFullscreen(id) {
		// Get the element that we want to take into fullscreen mode
		var element = document.getElementById(id);
		
		// These function will not exist in the browsers that don't support fullscreen mode yet, 
		// so we'll have to check to see if they're available before calling them.
		
		if (element.mozRequestFullScreen) {
		  // This is how to go into fullscren mode in Firefox
		  // Note the "moz" prefix, which is short for Mozilla.
		  element.mozRequestFullScreen();
		} else if (element.webkitRequestFullScreen) {
		  // This is how to go into fullscreen mode in Chrome and Safari
		  // Both of those browsers are based on the Webkit project, hence the same prefix.
		  element.webkitRequestFullScreen();
	   }
	   // Hooray, now we're in fullscreen mode!
	  }
var FrameHeight = 350;
var FrameWidth = 550;

  function toggleFullScreen(id) {
	var element = document.getElementById(id);
   if (!document.mozFullScreen && !document.webkitFullScreen) {
      if (element.mozRequestFullScreen) {
          element.mozRequestFullScreen();
	  } else if (element.webkitRequestFullScreen) {
		  element.webkitRequestFullScreen();
	  }
	  $("#playlistView").hide();
	  	var arrFrames = parent.document.getElementsByTagName("IFRAME");
		var controlBarHieght = 0; // 55 to decrease height of gallery to set controls at the top of the gallery
		arrFrames[0].top = "0";
		arrFrames[0].left = "0";
		arrFrames[0].height = parent.document.body.clientHeight - controlBarHieght; 
		arrFrames[0].width = "100%";

    } else {
      if (document.mozCancelFullScreen) {
        document.mozCancelFullScreen();
      } else {
        document.webkitCancelFullScreen();
      }
	  $("#playlistView").show();
	  	var arrFrames = parent.document.getElementsByTagName("IFRAME");
		arrFrames[0].top = "0";
		arrFrames[0].left = "0";
		arrFrames[0].height = FrameHeight;

    }
  }


//Youtube Video Embed

      function loadPlayer() {
		// Firstly let's check if the user has Flash 8+
		
		if (swfobject.hasFlashPlayerVersion("8.0.0")) {
			// insert the chromeless player with custom controls and skinning
			alert('you have flash, so getting the chromeless/skinned player');
			
			// Lets Flash from another domain call JavaScript
	        var params = { allowScriptAccess: "always", allowfullscreen:"true" };
	        // The element id of the Flash embed
	        var atts = { id: "ytPlayer" };
	        // All of the magic handled by SWFObject (http://code.google.com/p/swfobject/)
	        swfobject.embedSWF("http://www.youtube.com/apiplayer?" +
	                           "&enablejsapi=1&playerapiid=player1", 
	                           "videoPlayerContainer", "600", "360", "8", null, null, params, atts);
			
			
		}
		else {
			// insert the standard embed, standard controls but html5 supported
			alert('no flash, using html5 player if possible');
			
			var videoDiv = document.getElementById('videoPlayerContainer');
			videoDiv.innerHTML = '<object width="640" height="385"><param name="movie" value="http://www.youtube.com/v/YJp7tqRyJAI?fs=1&amp;hl=en_US"></param><param name="allowFullScreen" value="true"></param><param name="allowscriptaccess" value="always"></param><embed src="http://www.youtube.com/v/YJp7tqRyJAI?fs=1&amp;hl=en_US" type="application/x-shockwave-flash" allowscriptaccess="always" allowfullscreen="true" width="640" height="385"></embed></object>';
			
		}
      }

      // This function is automatically called by the player once it loads
      function onYouTubePlayerReady(playerId) {
        ytplayer = document.getElementById("ytPlayer");
        // This causes the updatePlayerInfo function to be called every 250ms to
        // get fresh data from the player
        setInterval(updatePlayerInfo, 250);
        updatePlayerInfo();
        //ytplayer.addEventListener("onStateChange", "onPlayerStateChange");
        ytplayer.addEventListener("onError", "onPlayerError");
        //Load an initial video into the player
        ytplayer.cueVideoById("YJp7tqRyJAI");
		//ytplayer.playVideo();
		//playVideo();
      }

      // Display information about the current state of the player
      function updatePlayerInfo() {
        // Also check that at least one function exists since when IE unloads the
        // page, it will destroy the SWF before clearing the interval.
        if(ytplayer && ytplayer.getDuration) {
		
			if(ytplayer.getDuration()>0 && ytplayer.getDuration()==ytplayer.getCurrentTime()){
				trackEndFunc();		
			}
			updateHTML("plD", ytplayer.getDuration());
			updateHTML("plC", ytplayer.getCurrentTime());
          /*updateHTML("videoDuration", ytplayer.getDuration());
          updateHTML("videoCurrentTime", ytplayer.getCurrentTime());
          updateHTML("bytesTotal", ytplayer.getVideoBytesTotal());
          updateHTML("startBytes", ytplayer.getVideoStartBytes());
          updateHTML("bytesLoaded", ytplayer.getVideoBytesLoaded());
          updateHTML("volume", ytplayer.getVolume());*/
        }
      }

	  // Update a particular HTML element with a new value
      function updateHTML(elmId, value) {
        document.getElementById(elmId).innerHTML = value;
      }

	  function playVideo() {
        if (ytplayer) {
          ytplayer.playVideo();
        }
      }
      function pauseVideo() {
        if (ytplayer) {
          ytplayer.pauseVideo();
        }
      }

	</script>	
</head>
<body style="height: 100%;margin:0px;">
<div id="tempd" style="width:100%;height:100%; background:#ccc">
	<div class="player_wall" id='player_wall'>
<?php //if($songProps[0]['type']=="audio"){ ?>
	<div id="audioPlayerContainer" style="display:none;">
		<?php $encodedGallery = urlencode(json_encode($songProps[0]['galleryArray'])); ?>
		<iframe id="gFrame" src="youtube.php?gl=<?php echo $encodedGallery;?>" width="100%" height="350px"  style="padding: 0px; margin: 0px;border:0px;"></iframe>
		<div id="audioPlayer_wrapper" style="background-color:#000;">
			<a id="soundPlayer" href="<?php echo $songProps[0]['filename'];?>" class="sc-player"><?php echo $songProps[0]["title"];?></a>
		</div>
		<div id="mp3Player_wrapper" style="position:relative;top:0px;>
			<audio id="mp3player" type="audio/mp3" controls="controls"  style="width:100%;"></audio>
		</div>
		<script>//loadscPlayer('<?php echo $songProps[0]["filename"];?>','<?php echo $songProps[0]["title"];?>');</script>
	</div>
<?php //}else if($songProps[0]['type']=="video"){ ?>
	<div id="videoPlayerContainer" style="display:block;"  width="575" height="400"></div>
	<div id="flvPlayer_wrapper" style="display:none;">
	</div>
<?php //} ?>
		<div id="playerControls">
			<!-- <div id="play_btn" class="player_sprite play_btn"></div> -->
			<div id="prev_btn" class="player_sprite prev_btn" title="Previous"></div>
			<div id="next_btn" class="player_sprite next_btn" title="Next"></div>
			<div id="shuffle_btn" class="player_sprite shuffle_btn" title="Toggle Sequence"></div>
			<div id="clearlist_btn" class="player_sprite clearlist_btn" title="Clear Playlist"></div>
			<div id="fs_btn" class="player_sprite fullscreen_btn" title="Full Screen"></div>
			<div id="plD"></div>
			<div id="plC" ></div>
		<div>
	</div>
<!-- PlayList Coding -->
	<div id="playlistView">
		<div id="listFiles">
			<div class="titleCont">
				<div class="blkB">Title</div>
				<div class="blkC">Creator</div>
				<div class="blkD">From</div>
				<div class="blkE">Type</div>
				<div class="blkF">Download</div>
				<div class="blkA">-</div>
			</div>
			<div id="mediascroller">
				<?php if(!empty($songProps)){
						for($i=0;$i<count($songProps);$i++){
				?>
				<div class="tableCont">
					<div class="blkB"><a href="javascript:void(0);" onclick="javascript:playThisMedia(<?php echo $i;?>);" style="underline:none;"><b><?php echo $songProps[$i]['title'];?></b></a></div>
					<div class="blkC"><?php if(ucwords($songProps[$i]['creator']) =="" ){echo "creator";}else {echo ucwords($songProps[$i]['creator']);}?></div>
					<div class="blkD"><?php if(ucwords($songProps[$i]['gallery_title']) ==""){echo "Gallery Title";} else {echo ucwords($songProps[$i]['gallery_title']);}?></div>
					<div class="blkE"><?php echo ucwords($songProps[$i]['type']);?></div>
					<div class="blkF">D</div>
					<div class="blkA"><div class="closeSong" onclick="removeSong('<?php echo $songProps[$i]['songId'];?>',this,<?php echo $i;?>)"></div></div>
				</div>
				<?php }	}	?>
			</div>
		</div>
	</div>
</div>

</body>
</html>