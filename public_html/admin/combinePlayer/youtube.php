<!doctype html>
<html>
  <head>
    <script src="../js/jquery.min.js"></script>
	<link href="../galleryfiles/galleryplayer.css" rel="stylesheet"/>
	<script src="../galleryfiles/jquery.easing.1.3.js"></script>
<?php 
	//$encodedGallery = urldecode($_GET['gl']);
	$encodedGallery = urldecode($_POST['gl']);
	$decodedGallery = json_decode(str_replace('\\', '', $encodedGallery));
	//$setTimer = ($_GET['timer']!='')?$_GET['timer']:0;
	$setTimer = ($_GET['timer']!='')?$_POST['timer']:0;
?>
<script>
var setTimer = <?php echo $setTimer; ?>;
var WinHeight = 635;
var WinWidth = 580;
var FrameHeight = 350;
var FrameWidth = 550;
var fullscr = 0;
/*$("document").ready(function(){
window.onkeypress = function(evt) {
	evt = evt || window.event;
	var charCode = evt.keyCode || evt.which;
};
});*/
function setFullScreen(){
	//$("#gFrame").attr('width','100%').attr('height','100%');
	//autoResize('gFrame');
	//window.setTimeout("$('.mejs-controls').css({'top':'30px'})",1000);
	parent.window.moveTo(0, 0);
	parent.window.resizeTo(screen.availWidth, screen.availHeight);

	var arrFrames = parent.document.getElementsByTagName("IFRAME");

	arrFrames[0].top = "0";
	arrFrames[0].left = "0";
	arrFrames[0].height = parent.document.body.clientHeight - 35;
	arrFrames[0].width = "100%";
	var is_chrome = navigator.userAgent.toLowerCase().indexOf('chrome') > -1;
	if(is_chrome ==true){
		arrFrames[0].height = parent.document.body.clientHeight + 50;
	}
	if(parent.document.getElementById("audioPlayer_wrapper").style.display != "none")
	{
		parent.document.getElementById("audioPlayer_wrapper").style.display = "block";
		parent.document.getElementById("audioPlayer_wrapper").style.height = "31px";
		parent.document.getElementById("audioPlayer_wrapper").style.position = "relative";
		parent.document.getElementById("audioPlayer_wrapper").style.top = "-30px";
		//parent.document.getElementById("audioPlayer_wrapper").style.background-color = "#000";
	}
	parent.document.getElementById("playlistView").style.display = "none";
	parent.document.getElementById("tempd").style.backgroundColor = "black";
	window.onkeypress = function(evt) {
		evt = evt || window.event;
		var charCode = evt.keyCode || evt.which;
		if(charCode==122){
			arrFrames[0].height = parent.document.body.clientHeight + 70;
			var is_chrome = navigator.userAgent.toLowerCase().indexOf('chrome') > -1;
			if(is_chrome ==true){
				arrFrames[0].height = parent.document.body.clientHeight + 200;
			}
			if(fullscr ==1){
			fullscr =0;
			arrFrames[0].height = parent.document.body.clientHeight - 145;
			}else{
			fullscr =1;
			}
		}
	};
	//loadGallery();
	//window.clearInterval(GalleryAutoPlayTimer);
	//setTimeout(function(){	loadGallery();	$("#light-gal").css('visibility','visible'); autoRotate();	},200);
	//GalleryAutoPlayTimer=self.setInterval("autoRotate()",4000);	
	//ImageViewMode('full');
	
}
function setRestoreScreen(){
	//$("#gFrame").attr('width','100%').attr('height','100%');
	//autoResize('gFrame');
	parent.window.moveTo(0, 0);
	parent.window.resizeTo(WinWidth, WinHeight);

	var arrFrames = parent.document.getElementsByTagName("IFRAME");
	arrFrames[0].top = "0";
	arrFrames[0].left = "0";
	arrFrames[0].height = FrameHeight;
	if(parent.document.getElementById("audioPlayer_wrapper").style.display != "none")
	{
		parent.document.getElementById("audioPlayer_wrapper").style.display = "block";
		parent.document.getElementById("audioPlayer_wrapper").style.top = "-23px";
	}
	parent.document.getElementById("playlistView").style.display = "block";
	parent.document.getElementById("tempd").style.backgroundColor = "#cccccc";
	//arrFrames[0].width = FrameWidth;
	//window.clearInterval(GalleryAutoPlayTimer);
	//setTimeout(function(){	loadGallery();	$("#light-gal").css('visibility','visible'); autoRotate();	},200);
	//GalleryAutoPlayTimer=self.setInterval("autoRotate()",4000);	
	//$(".mejs-controls").css({"top":"0px"});
}
</script>
  </head>
  <body>
  <?php 
   	if(count($decodedGallery)>0){
		if($decodedGallery[0]->imageType=="profile_pic"){
			$picPath = $picThumb = "profile_pic"; 
		}else{
			$picThumb = "thumb"; 
			$picPath="gallery";
		}
?>
	<div id="light-gal" class="gallery-wrapper">
		<div id="bg">
			<a href="#" class="nextImageBtn" title="next"></a>
			<a href="#" class="prevImageBtn" title="previous"></a>
			<img width="1680" src="<?php echo $decodedGallery[0]->img_path ."". $decodedGallery[0]->filename;?>" height="1050" alt="" title="" id="bgimg" />
		</div>
		<div id="preloader"><img src="../galleryfiles/ajax-loader_dark.gif" width="32" height="32" /></div>
		<div id="img_title"></div>
		<div id="toolbar">
			<a href="#" title="Maximize" id="maximizeImg" onClick="ImageViewMode('full-');return false"><img src="../galleryfiles/toolbar_fs_icon.png" width="50" height="50"  /></a>
			<!--<a href="#" title="Play" id="playImg" onClick="ImagePlayMode('play')"><img src="../galleryfiles/play.png" width="50" height="50"  /></a>-->
			<a href="#" title="Pause" id="pauseImg" onClick="setimage('pause')"><img src="../galleryfiles/pause.png" width="50" height="50"  /></a>
			<a href="#" title="Close" id="closeGal" ><img src="../galleryfiles/close.png" width="50" height="50"  /></a>
		</div>
		<div id="thumbnails_wrapper">
			<div id="outer_container">
				<div class="thumbScroller">
					<div class="container" id="gallery-container">
					<?php for($i=0;$i<count($decodedGallery);$i++){	?>
						<div class="content">
							<div>
								<a href="<?php echo $decodedGallery[$i]->img_path ."". $decodedGallery[$i]->filename;?>">
									<img src="<?php echo $decodedGallery[$i]->img_path ."". $decodedGallery[$i]->filename;?>" title="<?php echo $decodedGallery[$i]->title;?>" alt="<?php echo $decodedGallery[$i]->title;?>" class="thumb" />
								</a>
							</div>
						</div>
					<?php } ?>
					</div>
				</div>
			</div>
		</div>
	</div>  

  <!-- Gallery END -->

<script type="text/javascript" src="../galleryfiles/media_jqgal.js"></script>
<script  type="text/javascript">
var GalleryAutoPlayTimer = null;
$(document).ready(function(){
	<?php if(count($encodedGallery)){ ?>
		//setTimeout(function(){	loadGallery();	$("#light-gal").css('visibility','visible'); /*autoRotate();*/	},500);	
		loadGallery();
		$rotate_val=1;
		$("#light-gal").css('visibility','visible');
		//if(setTimer==1){
			GalleryAutoPlayTimer=self.setInterval("autoRotate()",4000);
		//}
		$("#closeGal").hide();
	<?php } ?>
});
function autoRotate(){
	if(setTimer==1){
		$("a.nextImageBtn").click();
	}
}
function setimage(playMode){
	if(playMode=="pause"){
		$("#pauseImg").find("img").attr("src", "../galleryfiles/play.png");
		$("#pauseImg").attr("onClick", "ImagePlayMode('play');return false").attr("title", "Play");
		setTimer=0;
		$rotate_val =1;
		GalleryAutoPlayTimer = null;
		//window.clearInterval(GalleryAutoPlayTimer);
	} else {
		$("#pauseImg").find("img").attr("src", "../galleryfiles/pause.png");
		$("#pauseImg").attr("onClick", "ImagePlayMode('pause');return false").attr("title", "Pause");
		setTimer=1;
		GalleryAutoPlayTimer=self.setInterval("autoRotate()",5000);
	}

}
</script>
<?php } ?>
</body>
</html>