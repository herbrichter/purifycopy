<div style="float:left; width:650px; margin-top: 10px; margin-bottom: 10px; border: 1px solid #CCCCCC;">
    <div style="float:left; width:645px; margin-bottom: 10px; padding: 2px; border: 1px solid #CCCCCC; background:#777777;" align="center" class="hintWhite">Add a Feature
    </div>
    <div style="float:left; width:645px; padding: 10px;">
        <form action="" method="post" id="feature_post">
        <table width="100%" border="0" cellpadding="0" cellspacing="0" align="center">
            <tr>
              <td width="104">&nbsp;</td>
              <td width="103">Feature Page</td>
              <td width="144">
                <select name="select-feature-page" class="regularField2" id="select-feature-page">
                	<option value="index">Index</option>
                	<option value="artist">Artists</option>
                	<option value="community">Community</option>
                    <option value="type">Type</option>
                    <option value="subtype">Subtype</option>
                </select>              
              </td>
              <td width="294" align="left"><div class="hint"></div></td>
            </tr>        
            <tr>
              <td colspan="4"><img src="images/spacer.gif" alt=" " width="10" height="10" /></td>
            </tr>            
            <tr id="feature_block" style="display:none;">
              <td width="104">&nbsp;</td>
              <td width="103">Feature</td>
              <td width="144">
                <select name="select-feature-category" class="regularField2" id="select-feature-category">
                	<option value="artist">Artist</option>
                    <option value="community">Community</option>
                </select>              
              </td>
              <td width="294" align="left"><div class="hint"></div></td>
            </tr>
            <tr id="feature_block_space" style="display:none;">
              <td colspan="4"><img src="images/spacer.gif" alt=" " width="10" height="10" /></td>
            </tr>
			<tr id="type_block" style="display:none;">
              <td width="104">&nbsp;</td>
              <td width="103">Type</td>
              <td width="144">
	              <?php include_once('select_type_block.php'); ?>
              </td>
              <td width="294" align="left"><div class="hint"></div></td>
            </tr>
            <tr id="type_block_space" style="display:none;">
              <td colspan="4"><img src="images/spacer.gif" alt=" " width="10" height="10" /></td>
            </tr>
			<tr id="subtype_block" style="display:none;">
              <td width="104">&nbsp;</td>
              <td width="103">Subtype</td>
              <td width="144">
	              <?php include_once('select_subtype_block.php'); ?>
              </td>
              <td width="294" align="left"><div class="hint"></div></td>
            </tr>
            <tr id="subtype_block_space" style="display:none;">
              <td colspan="4"><img src="images/spacer.gif" alt=" " width="10" height="10" /></td>
            </tr>                         
            <tr>
              <td width="104" valign="top">&nbsp;</td>
              <td width="103" valign="top">Feature ID</td>
              <td width="144">
              	<input name="feature_id" type="text" class="regularField2" id="feature_id" />              
              </td>
              <td width="294" align="left"><div class="hint">Enter resp. ID for the selected feature.</div></td>
            </tr>
            <tr>
              <td colspan="4"><img src="images/spacer.gif" alt=" " width="10" height="10" /></td>
            </tr>
            <tr>
              <td width="104" valign="top"></td>
              <td width="103" valign="top"></td>
              <td width="144" align="right">
              	<input type="submit" name="postfeature" id="postfeature" value=" Save " />              
              </td>
              <td width="294" align="left"><div class="hint"></div></td>
            </tr>
          </table>
      </form>
    </div>
</div>
