<?php
	include_once('includes/header.php');

	$email_group=$_POST['select-email-group'];
	$email_list=$email_group;
?>
<script>
	$(document).ready(
						function()
						{
							$("#select-email-group").change(
														function ()
														{
															//alert($("#type-select").val());
															$.post("email_list_groups.php", { email_group:$("#select-email-group").val() },
  															function(data)
															{
    														//alert("Data Loaded: " + data);
																$("#email_list").replaceWith(data);
  															}
													);
											}
										)
										.change();
					}
				);
</script>
<script type="text/javascript">
	function listEmail()
	{
		$frm=document.getElementById('email-group');
		$frm.submit();
	}
</script>
	
  <div id="contentContainer">
    <div id="subNavigation">
      		<?php 
			$active_nav = "emailgroup";
			$active_sub_nav = "";
			include_once('admin_leftnavigarion.php'); ?>

    </div>
    <div id="actualContent">
      <h1 class="withLine">Emails</h1>
		<?php include_once('email_select_group.php'); ?>
    </div>
    
    <div class="clearMe"></div>
    
  </div>
  
<?php include_once('includes/footer.php'); ?>
