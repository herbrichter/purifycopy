<?php
	include_once('includes/header.php');
	include_once('classes/User.php');
	include_once('../classes/UserType.php');
	include_once('../classes/Type.php');
	include_once('../classes/SubType.php');
	include_once('../classes/Subscription.php');
	include_once('../classes/UserSubscription.php');									
	
	$search_key=$_POST['search'];
	$criteria_id=$_POST['criteria'];
	$pagenum=$_GET['pagenum'];
	
	if($criteria_id==0||"") $flag_sd=1;
	
	$obj=new User();
	$obj2=new UserType();
	$no_all=$obj->countAllUsers();
	$no_artist=$obj->countUsers('Artist');
	$no_fan=$obj->countUsers('Fan');
	$no_community=$obj->countUsers('Community');
	$no_team=$obj->countUsers('Team');
	
	switch($criteria_id)
	{	//All
		case 0: $criteria='id';
				$search_key='';
				$orderby='id';
				$dir='desc';
				$flag_pg=1;
				break;
		//Create Date
		case 1:	$criteria='create_date';
				$orderby='create_date';
				$dir='desc';
				break;
		//City
		case 2:	$criteria='city';
				$orderby='city';
				$dir='asc';
				break;
		//State or Province
		case 3:	$criteria='state_or_province';
				$orderby='state_or_province';
				$dir='asc';
				break;
		//Country
		case 4:	$criteria='country';
				$orderby='country';
				$dir='asc';
				break;
		//Subscribed
		case 5:	$criteria='subscribed';
				$search_key='1';
				$orderby='id';
				$dir='desc';
				break;
		//Not Subscribed
		case 6:	$criteria='subscribed';
				$search_key='0';
				$orderby='id';
				$dir='desc';
				break;
		//Verified
		case 7:	$criteria='verified';
				$search_key='1';
				$orderby='id';
				$dir='desc';
				break;
		//Not verified
		case 8:	$criteria='verified';
				$search_key='0';
				$orderby='id';
				$dir='desc';
				break;
		//Active
		case 9:	$criteria='status';
				$search_key='1';
				$orderby='id';
				$dir='desc';
				break;
		//In request
		case 10:$criteria='status';
				$search_key='0';
				$orderby='id';
				$dir='desc';
				break;
		//Type
		case 11:$criteria='type_id';
				$orderby='id';
				$dir='desc';
				$flag_tb=1;
				$criteria2='type';
				break;
		//Subtype
		case 12:$criteria='subtype_id';
				$orderby='id';
				$dir='desc';
				$flag_tb=1;
				$criteria2='subtype';
				break;
												
		//First Load
		default:$criteria='id';
				$search_key='';
				$orderby='id';
				$dir='desc';
				$flag_pg=1;
				break;
	} 
	
	if($flag_tb) $no=$obj2->countUsersByCriteriaByType($search_key,$criteria,$criteria2,'community');
	else $no=$obj->countUsersByCriteriaByType($search_key,$criteria,'community');
	
	if(!$no) $flag=1;

	if($flag_pg)
	{
		if (!(isset($pagenum))) 
		{ 
			$pagenum = 1; 
		} 
		
		$currentpage="admin_community_members.php?pagenum=".$pagenum;
		$page_rows = 10;
		$last = ceil($no_community/$page_rows);
		
		if ($pagenum < 1) 
		{ 
			$pagenum = 1; 
		} 
		elseif ($pagenum > $last) 
		{ 
			$pagenum = $last; 
		} 
		if($last) $max = ' limit ' .($pagenum - 1) * $page_rows .',' .$page_rows;
		else
		{
		 $max='';
		 $flag=1;
		}
	}
	 
	if($flag_tb) $rs=$obj2->sortByCriteriaByType($search_key,$max,$criteria,$criteria2,$orderby,$dir,'community');
	else $rs=$obj->sortByCriteriaByType($search_key,$max,$criteria,$orderby,$dir,'community');
?>

<script language="JavaScript" type="text/JavaScript">
<!--
function MM_openBrWindow(theURL,winName,features) { //v2.0
  window.open(theURL,winName,features);
}

function Confirm(UID)
{
   var objWin, varURL;
   varURL = 'confirm.php?id=' + UID ;
   objWin = window.open(varURL,"","left=300,top=300,toolbar=no,status=no,directories=no,titlebar=no,location=no,menubar=no,scrollbars=no,resizable=no,copyhistory=no,width=337,height=135");
}//-->

function search_sort()
{
	var frm=document.getElementById('search_form');
	frm.submit();
}
</script>

  <div id="contentContainer">
    <div id="subNavigation">
   <?php 
	$active_nav = "admin-users";
	$active_sub_nav = "admin-community";
   include_once('admin_leftnavigarion.php'); ?>   
    </div>
    <div id="actualContent">
      <div id="searchContainer" class="hint">
      	<form id="search_form" name="search_form" method="post" action="">
      	<table width="100%" border="0" align="right">
          <tr>
            <td width="25%" align="right">Search</td>
            <td width="31%" align="right"><input  id="search" name="search" type="text" class="regularField2" <?php //if($flag_sd=="1") echo " disabled='disabled' " ?>/></td>
            <td width="4%" align="right">by</td>
            <td width="32%" align="right">
              <select id="criteria" name="criteria" class="pulldownField2">
                  <option value="0" <?php if($criteria_id=="") echo " selected='selected' " ?>>Select All</option>
                  <option disabled="disabled">--DATE--</option>              
                  <option value="1" <?php if($criteria_id=="1") echo " selected='selected' " ?>>Create Date</option>
				  <option disabled="disabled">--LOCATION--</option>                  
                  <option value="2" <?php if($criteria_id=="2") echo " selected='selected' " ?>>City</option>
                  <option value="3" <?php if($criteria_id=="3") echo " selected='selected' " ?>>State or Province</option>
                  <option value="4" <?php if($criteria_id=="4") echo " selected='selected' " ?>>Country</option>
                  <option disabled="disabled">--SUBSCRIPTION--</option>
                  <option value="5" <?php if($criteria_id=="5") echo " selected='selected' " ?>>Subscribed</option>
                  <option value="6" <?php if($criteria_id=="6") echo " selected='selected' " ?>>Not subscribed</option>
                  <option disabled="disabled">--EMAIL VERIFICATION--</option>
                  <option value="7" <?php if($criteria_id=="7") echo " selected='selected' " ?>>Verified</option>
                  <option value="8" <?php if($criteria_id=="8") echo " selected='selected' " ?>>Not verified</option>
                  <option disabled="disabled">--STATUS--</option>
                  <option value="9" <?php if($criteria_id=="9") echo " selected='selected' " ?>>Active</option>
                  <option value="10" <?php if($criteria_id=="10") echo " selected='selected' " ?>>In request</option>
                  <option disabled="disabled">--OTHER--</option>
                  <option value="11" <?php if($criteria_id=="11") echo " selected='selected' " ?>>Type</option>
                  <option value="12" <?php if($criteria_id=="12") echo " selected='selected' " ?>>Subtype</option>
              </select>     
            </td>
            <td width="8%" align="right"><a href="#" title="Go"><img src="images/go.jpg" width="30" height="30" onclick="search_sort();" alt="" /></a></td>
          </tr>
        </table>
        </form>
      </div>
      <h1 class="withLine">Users (CM)</h1>
      <div id="pageBox"><?php if($flag_pg) echo "Page ".$pagenum." of ".$last; else echo "Search results: ".$no; ?></div>
      <div id="pageNoBox" align="right"><?php	include('pagination.php'); ?></div>
	  <?php 
	  	echo "<br />";
		echo "<br />";
		if($flag) echo "No records found.<br /><br />";		
	  	while($row=mysql_fetch_assoc($rs))
	  	{
			//User_type
			switch($row['user_type'])
			{
				case 'artist':	$bgcolor='#000000';
								$user_type='Artist';
								break;
				case 'fan':		$bgcolor='#000000';
								$user_type='Fan';				
								break;
				case 'community':	$bgcolor='#000000';
								$user_type='Community';				
								break;
				case 'team':	$bgcolor='#000000';
								$user_type='Team';				
								break;					
								
				// #FFCCFF #CCFF66 #99FFFF #FFFF99
				// #FF3333 #00CCFF #FFCC33 #00CC00		
			}
			
			//Status
			switch($row['status'])
			{
				case 0:	$status="<span class='hintRed'>In request</span>";
						break;
				case 1: $status="<span class='hintGreen'>Active</span>";
						break;
			}
			
			//Verified
			switch($row['verified'])
			{
				case 0:	$verified="<span class='hintRed'>No</span>";
						break;
				case 1: $verified="<span class='hintGreen'>Yes</span>";
						break;
			}
			
			//Subscribed
			switch($row['subscribed'])
			{
				case 0:	$subscribed="<span class='hintRed'>No</span>";
						break;
				case 1: $subscribed="<span class='hintGreen'>Yes</span>";
						break;
			}
			
			//type-subtype for artists & friends
			if($row['user_type']=='artist'||$row['user_type']=='community')
			{
				$obj2=new UserType;
				$obj3=new Type;
				$obj4=new SubType;
				
				$rs2=$obj2->getUserList($row['id']);
				$num=mysql_num_rows($rs2);
			}
			
			//subscription for fans
			if($row['user_type']=='fan')
			{
				$obj5=new UserSubscription();
				$obj6=new Subscription();
				
				$rs3=$obj5->getUserSubscription($row['id']);
			}
			
			//category for teams
			if($row['user_type']=='team')
			{
				$category=$row['category'];
			}
			
			include('user_row.php'); 
		}
	  ?>
      <div id="pageBox"><?php if($flag_pg) echo "Page ".$pagenum." of ".$last; else echo "Search results: ".$no; ?></div>
      <div id="pageNoBox" align="right"><?php	include('pagination.php'); ?></div>
    </div>
    <div class="clearMe"></div>
  </div>
  
<?php include_once('includes/footer.php'); ?>
