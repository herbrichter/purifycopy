<?php
		
	
	include_once('includes/header.php');
	include_once('commons/db.php');
	include_once('classes/ProfileType.php');
	include_once('classes/Type.php');
	include_once('classes/SubType.php');
	include_once('classes/metaType.php');
	


if(isset($_REQUEST['editdelete'])){
	$submitId = $_REQUEST['submitId'];
	$submitType = $_REQUEST['submitType'];
	$submitAction = $_REQUEST['submitAction'];

	switch($submitType){
		case "profile":
					$obj=new ProfileType();
					$obj->deleteData($submitId);
					break;
		case "types":
					$obj=new Type();
					$obj->deleteData($submitId);
					break;
		case "subtypes":
					$obj=new SubType();
					$obj->deleteData($submitId);
					break;
		case "meta":
					$obj=new Meta();
					$obj->deleteData($submitId);
					break;
	}
}

if(isset($_POST['profile_type']))
{
	$selected_profle_type_id = $_POST['profile_type'];
	//$dummy1=$_POST['profile_type'];
}
else
	$selected_profle_type_id = "";

	
if(isset($_POST['type1']))
{
		$selected_type_id= $_POST['type1'];
		//$dummy=$_POST['type1'];
}
else
		$selected_type_id= "";
	


if(isset($_POST['subtype1']))
{
	$selected_subtype_id = $_POST['subtype1'];
	//$dummy3=$_POST['subtype1'];
}
else
	$selected_subtype_id = "";
	
	
	$objMeta=new Meta();
	
		
	if(isset($_POST['metadata']) and ($_POST['metadata'] != ""))
	{
		$objMeta->insertData($_POST['metadata'],$_POST['subtype1']);
		unset($_POST['metadata']);
	}
	
	
	$obj=new ProfileType();	    //ProfileType object is created
	$value3=$obj->displayData();
	
	$objType=new Type();		//Type object is created
	$value1=$objType->displayData();
	
	$objSubType=new SubType();	//SubType Object is created
	$value2=$objSubType->displayData();
	
	$rs2=$obj->displayData();
	
	if(isset($_POST['profile']) and ($_POST['profile'] != ""))
	{
		$page_name = str_replace(' ','_',strtolower($_POST['profile']));
		$page_name = "register_".$page_name.".php";
		$obj->insertData($_POST['profile'],$page_name );
		unset($_POST['profile']);
		header("Location: ".$_SERVER['PHP_SELF']);
		die;
	}
	
	
	if(isset($_POST['profile_type1']) and ($_POST['profile_type1'] != ""))
	{
		$objType->inserData($_POST['profile_type1'],$_POST['profile_type']);
		unset($_POST['profile_type1']);
		
	}
	
	
	if(isset($_POST['sub_type_value']) and ($_POST['sub_type_value'] != "") and isset($_POST['profile_type']) and ($_POST['profile_type'] != ""))
	{
		$objSubType->inserData($_POST['sub_type_value'],$_POST['type1'],$_POST['profile_type']);
		unset($_POST['sub_type_value']);
	}
	
	$rs=$objType->listTypes($_POST['profile_type']);
	$rs1=$objSubType->listSubTypes($_POST['type1'],$_POST['profile_type']);
	
?>
<script type="text/javascript" language="javascript">
	$(document).ready(function(){
		$("#profile_type1").change(function (){
				$.post("admin_req.php", {casetype:"gettypes", pid:$(this).val() },function(data){
					if(data){
						var jsonData = eval('('+data+')');
						$("#type1").append('<option value="'+jsonData.type_id+'">'+jsonData.name+'</option>');
						$("#type1").attr('disabled','false');
					}
				});
			}
		);
	});

	function submitActionFrm(submitAction, submitType, id){
		var frm = document.typeListFrm;
		frm.submitAction.value = submitAction;
		frm.submitType.value = submitType;
		frm.submitId.value = id;
		frm.submit();
	}
</script>


  <div id="contentContainer">
    <div id="subNavigation">
   <?php 
	$active_nav = "types-subtypes";
	$active_sub_nav = "";
   include_once('admin_leftnavigarion.php'); ?>   
    </div>
    <div id="actualContent">
      <div id="searchContainer">

      </div>
      <h1 class="withLine">ProfileTypes/PageTypes/SubTypes/MetaTypes</h1>

       <div>
       		<form method="post" >
            
            	<table width="100%" border="0">
                	<tr>
                    	<!--*********************Profile type************************-->
                    	<td>
                        	<div align="right">Profile Type: </div>
                        </td>
                        <td>
                        	<select id="profile_type" name="profile_type" class="pulldownField2"  onchange="submit();">
							<option value="">Select Profile</option>
                            <?php 
									while($row=mysql_fetch_array($rs2))
									{
			 				 ?>		
									   <option value="<?php echo $row['id']; ?>" <?php if($selected_profle_type_id==$row['id']) echo "selected='true'"; ?>> <?php echo $row['name']; ?></option>
						   <?php 	} ?> 
                		</select>
                        </td>
                        <td>
                        	Add New Profile
                      </td>
                        <td>
                        	<input name="profile" type="text" size="15"/>
                        </td>
                        <td>
                        	<input type="submit" value="Add" />
                        </td>
                     </tr>
                     
                     <!--*********************type************************-->
                     <tr>
                     	<td>
                        	<div align="right">Page Type: </div>
                        </td>
                        
                         <td>
                        	<select id="type1" name="type1" class="pulldownField2" onchange="submit();" <?php if(!mysql_num_rows($rs)){ echo "disabled='true'"; } ?>>
                            <?php 
								while($row=mysql_fetch_array($rs))
								{
			 				 ?>	
								 <option value="<?php echo $row['type_id']; ?>" <?php if($selected_type_id==$row['type_id']) echo "selected='true'"; ?>> <?php echo $row['name']; ?></option>
                           
						<?php	} 	?>
                     </select>
                        </td>
                        <td>
                        	Add New Page Type
                       </td>
                        <td>
                        	<input name="profile_type1" type="text" size="15" <?php if(!$_POST['profile_type']){ echo "disabled='true'"; } ?>/>
                        </td>
                        <td>
                        	<input type="submit" value="Add" <?php if(!$_POST['profile_type']){ echo "disabled='true'"; } ?>/>
                        </td>
                       </tr>
                     
                     
                      <!--*********************Sub type************************-->
                      <tr>
                     	<td>
                        	<div align="right">Sub Type: </div>
                        </td>
                        
                         <td>
                        	<select name="subtype1" class="pulldownField2" onchange="submit();" <?php if(!mysql_num_rows($rs1)){ echo "disabled='true'"; } ?>>
                            <?php 
									while($row=mysql_fetch_assoc($rs1))
									{
			 				 ?>	
                             <option value="<?php echo $row['subtype_id']; ?>" <?php if($selected_subtype_id==$row['subtype_id']) echo "selected='true'"; ?>> <?php echo $row['name']; ?></option>
                             	
                           <
                           <?php 
			  	} 	?>
                </select>
                        </td>
                        <td>Add New Sub Type </td>
                        <td>
                        	<input name="sub_type_value" type="text" size="15" <?php if(!mysql_num_rows($rs)){ echo "disabled='true'"; } ?>/>
                        </td>
                        <td>
                        	<input type="submit" value="Add" <?php if(!mysql_num_rows($rs)){ echo "disabled='true'"; } ?> />
                        </td>
                       </tr>
                     </tr>
                     
                     <!--*********************Meta type************************-->
                        
                     <tr>
                     	
                        
                         <td>
                         
                        	<!--<select name="type" class="pulldownField2">
                            <?php 
									//while($row_type=mysql_fetch_assoc($rstype))
									//{
			 				 ?>		
                           <option value="<?php //echo $row_type['type_id']; ?>"><?php //echo $row_type['name']; ?></option>
                           <?php 
			  	//}
				?></select>-->
                        </td>
                        <td></td>
                        <td>
                        	 Add New Meta Type
                        </td>
                        <td>
                        	<input name="metadata" type="text" size="15" <?php if(!mysql_num_rows($rs1)){ echo "disabled='true'"; } ?>/>
                        </td>
                        <td>
                        	<input type="submit" value="Add" <?php if(!mysql_num_rows($rs1)){ echo "disabled='true'"; } ?>/>
                        </td>
                       </tr>
                     </tr>
                   </table>
         		
            </form>
            <br><br>

		<form name="typeListFrm" method="post">
		<?php if($selected_profle_type_id && isset($value3)){ 
			$profile_selected = "";
			while($row=mysql_fetch_array($value3))
			{
				if($selected_profle_type_id==$row['id']){
					$profile_selected = $row['name'];
				}
			} 
			if($profile_selected!=""){
			?>
			<div id="typeSubtypeContainer1">	<!-- Profile Selected -->
				<div id="typeTitle" class="hintBig">
				  <table width="100%" border="0">
					<tbody><tr>
					  <td>
							<?php 
								echo $profile_selected;
							?> 
						</td>
					  <td>
						<div align="right">
							<a href="#" title="Remove" onclick="submitActionFrm('delete','profile','<?php echo $selected_profle_type_id;?>');"><img src="images/remove.png" width="15" height="15"></a>
						</div>
					  </td>
					</tr>
					</tbody>
				  </table>
				</div>
			</div>
		<?php	}
			} ?>

			</br></br>
		<?php
			$rs=$objType->listTypes($_POST['profile_type']);
			while($row=mysql_fetch_assoc($rs)){
				$rs2=$objSubType->listSubTypes($row['type_id'],$_POST['profile_type']);
		?>	<div id="typeSubtypeContainer">	<!-- Type Listing -->
				<div id="typeTitle" class="hintBig">
				  <table width="100%" border="0">
					<tbody><tr>
					  <td width="90%"><?php echo $row['name']; ?></td>
					  <td width="10%">
					  <div align="right">
							<a href="#" title="Remove" onclick="submitActionFrm('delete','types','<?php echo $row['type_id'];?>');"><img src="images/remove.png" width="15" height="15"></a>
						</div>
					  </td>
					</tr>
					</tbody>
				  </table>
				</div>
				<div id="subTypeTitle"> <!-- SubType Listing -->
				  <ul>
					  <?php
						while($row2=mysql_fetch_assoc($rs2)){
							$rs3=$objMeta->listMetadata($row2['subtype_id']);
					  ?>
					  <table width="100%" border="0">
						<tr>
						  <td width="90%"><li style="color:#777777"><?php echo $row2['name']; ?></li></td>
						  <td width="10%"><a href="#" title="Remove" onclick="submitActionFrm('delete','subtypes','<?php echo $row2['subtype_id'];?>');"><img src="images/remove.png" width="15" height="15"></a></td>
						</tr>
						<?php if($rs3){ ?>
						<tr> <!-- Metadata Listing -->
							<td colspan=2><div id="subSubTypeTitle" >
							  <ul>
								  <?php
									while($row3=mysql_fetch_assoc($rs3)){
								  ?>
								  <table width="100%" border="0">
									<tr>
									  <td width="91%"><li style="color:#777777"><?php echo $row3['name']; ?></li></td>
									  <td width="9%"><a href="#" title="Remove" onclick="submitActionFrm('delete','meta','<?php echo $row3['meta_id'];?>');"><img src="images/remove.png" width="15" height="15"></a></td>
									</tr>
								  </table>
								  
								  <?php
									}
								  ?>
							  </ul>
							</div>
							</td>
						</tr>
						<?php } ?>
					  </table>
					  
					  <?php
						}
					  ?>
				  </ul>
				</div>
			  </div>
		<?php } ?>
		<input type="hidden" name="submitAction" />
		<input type="hidden" name="submitType" />
		<input type="hidden" name="submitId" />
		<input type="hidden" name="profile_type" value="<?php echo $_POST['profile_type']; ?>" />
		<input type="hidden" name="editdelete" value="1" />
		</form>

   
  </div>
  </div>
  </div>
 
<?php include_once('includes/footer.php'); ?>