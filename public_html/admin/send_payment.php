<?php
	include_once('classes/Admin_Transactions.php');
	include_once('../sendgrid/SendGrid_loader.php');
	$sendgrid = new SendGrid('','');
	$trans_obj = new Admin_Transactions();
	
	if(isset($_GET['id']))
	{
		$send = $trans_obj->send_payment($_GET['id']);
		$trans = $trans_obj->get_transaction($_GET['id']);
		if($trans['paypal']==1)
		{
			$type = "Paypal";
			$time = "48 hours";
		}
		elseif($trans['check_pay']==1)
		{
			$type = "Check";
			$time = "upto 2 weeks";
		}
		
		$to = $trans['login_email'];
				
			
				$subject = "Payment Sent";
				
				
				$header = "from: Purify Art < no-reply@purifyart.com>"."\r\n";
				

				$message = "Your payment request of ".$_GET['amt']." has been sent via ".$type." using the information you provided when requesting payment. Please allow ".$time." to receive your payment. If you have any questions regarding this payment email sales@purifyart.com. Thank you for selling through Purify Art where you receive most of the profits and enable us to provide you with the most comprehensive online sales and marketing service possible.";
				
				$url = "newsletter/";  
				$myFile = $url . "other_templates.txt";
				$fh = fopen($myFile, 'r');
				$theData = fread($fh, filesize($myFile));
				fclose($fh);
				
				$data_email = explode('|~|', $theData);
				
				$ns_user_n = str_replace('{::message::}', $message, $data_email['1']);
				
				$body_email = $data_email['0'] . $ns_user_n . $data_email['2'];
				
				$mail_grid = new SendGrid\Mail();
				
				$mail_grid->addTo($to)->
					setFromName('Purify Art')->
					setFrom('no-reply@purifyart.com')->
					setSubject($subject)->
					setHtml($body_email);
					
				//$sendgrid->web->send($mail_grid);
				$sendgrid -> smtp -> send($mail_grid);
			
				//$sentmail = mail($to,$subject,$message,$header);
	}
	
	header("Location: admin_transaction.php");
?>
