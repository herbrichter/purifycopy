<?php 
	//Included files
	include_once('../commons/db.php');
	include_once('../sendgrid/SendGrid_loader.php');
	
	include('classes/GeneralUser.php');
    $domainname=$_SERVER['SERVER_NAME'];
	
	$sendgrid = new SendGrid('','');
	$user_id = $_REQUEST['guid'];
	$profile_id = $_REQUEST['pro_id'];
	$status = $_REQUEST['status'];
	$profile_type = $_REQUEST['profile_type'];
	
	//echo $user_id." ".$artist_id." ".$status;
	//die;
	
	//Object created to display data from from Manage class
	$obj= new GeneralUser();
	$reply=$obj->setStatus($profile_id,$status,$profile_type);
	//echo "Reply : ".$reply;
	$userInfo=$obj->getUserInfoByProfileId($profile_id,$profile_type);	
	$row = mysql_fetch_assoc($userInfo);
	//echo "User Info : ";
	//print_r($row);
	//echo $row['email'];
	include('newsmtp/PHPMailer/index.php');
	if($reply==0){
		// Mail sent for aprove
		//$to=$row['email'];
		$mail->AddAddress($row['email']);
		//$subject="You ".$profile_type." profile was approved";
		
		//$mail->Subject = "You ".$profile_type." profile was approved";
		$mail->Subject = "Profile Approved";
		//$header="from: Purify Art < no-reply@purifyart.net>"."\r\n";
		//$message="You ".$profile_type." profile was approved. Login to begin using it."; 
		$message = "Your ".$profile_type." profile has been approved. Login to begin building your profile.
		<html><body><div style='background-color:#d3d3d3;color:#ffffff;font-size:18px;line-height:35px;margin:20px auto;width:95px;'><a href='http://".$domainname."/profileedit_$profile_type.php' style='text-decoration:none;color:#000000;' target='_blank'>Profile</a></div></body></html>";
		//$sentmail = mail($to,$subject,$message,$header);
		$mail->Body    = $message;
		/* if($mail->Send()){
			$sentmail = true;
		} */
		
		$url = "newsletter/"; 
			$myFile = $url . "other_templates.txt";
			$fh = fopen($myFile, 'r');
			$theData = fread($fh, filesize($myFile));
			fclose($fh);
			
			$data_email = explode('|~|', $theData);
			
			$ns_user_n = str_replace('{::message::}', $message, $data_email['1']);
			
			$body_email = $data_email['0'] . $ns_user_n . $data_email['2'];
			
		$mail_grid = new SendGrid\Mail();

		$mail_grid->addTo($row['email'])->
			   setFromName('Purify Art')->
				setFrom('no-reply@purifyart.com')->
			   //setSubject("You ".$profile_type." profile was approved")->
			   setSubject("Profile Approved")->
			   setHtml($body_email);
		//$sendgrid->web->send($mail_grid);
		$sendgrid -> smtp -> send($mail_grid); 
	}else{
		// Mail sent for Deny
		//$to=$row['email'];
		$mail->AddAddress($row['email']);
		//$subject="You ".$profile_type." profile was denied";
		$mail->Subject = "You ".$profile_type." profile was denied";
		//$header="from: Purify Art < no-reply@purifyart.net>"."\r\n";
		$message="Your ".$profile_type." profile request was denied because your uploaded media and/or links were not deemed a high enough quality or they were not related to \"Art\". Please register again if you feel this was in error or email info@purifyart.com";
		//$sentmail = mail($to,$subject,$message,$header);
		$mail->Body    = $message;
		/* if($mail->Send()){
			$sentmail = true;
		} */
		
		$url = "newsletter/"; 
			$myFile = $url . "other_templates.txt";
			$fh = fopen($myFile, 'r');
			$theData = fread($fh, filesize($myFile));
			fclose($fh);
			
			$data_email = explode('|~|', $theData);
			
			$ns_user_n = str_replace('{::message::}', $message, $data_email['1']);
			
			$body_email = $data_email['0'] . $ns_user_n . $data_email['2'];
			
		$mail_grid = new SendGrid\Mail();

		$mail_grid->addTo($row['email'])->
			   setFromName('Purify Art')->
				setFrom('no-reply@purifyart.com')->
			   setSubject("You ".$profile_type." profile was denied")->
			   setHtml($body_email);
		//$sendgrid->web->send($mail_grid);
		$sendgrid -> smtp -> send($mail_grid);
	}
	
	echo $reply;
?>
