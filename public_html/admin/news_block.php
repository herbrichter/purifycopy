<?php include_once('classes/News.php'); $edit_obj = new News();  ?>
<div style="float:left; width:650px; margin-top: 10px; margin-bottom: 10px; border: 1px solid #CCCCCC;">
    <div style="float:left; width:645px; margin-bottom: 10px; padding: 2px; border: 1px solid #CCCCCC; background:#777777;" align="center" class="hintWhite">Post a news
    </div>
    <div style="float:left; width:645px; padding: 10px;">
        <form action="admin_news.php" method="post" id="news_post">
        <table width="100%" border="0" cellpadding="0" cellspacing="0" align="center">
            <tr>
              <td width="100">Title</td>
              <td width="402">
			  <?php
				if(isset($_GET['edit_newsid']) && !empty($_GET['edit_newsid']))
				{
					$gets_title = $edit_obj->edit_news($_GET['edit_newsid']);
			  ?>
					<input name="title" type="text" class="regularField" id="title" value="<?php echo $gets_title['title']; ?>" />
					<input type="hidden" id="edit_newsid" name="edit_newsid" value="<?php echo $_GET['edit_newsid']; ?>" />
			<?php
				}
				else
				{
			?>
					<input name="title" type="text" class="regularField" id="title" />
			<?php
				}
			?>
              </td>
              <td width="148" align="left"><div class="hint"></div></td>
            </tr>
            <tr>
              <td colspan="3"><img src="images/spacer.gif" alt=" " width="10" height="10" /></td>
            </tr>        
            <tr>
              <td width="100" valign="top">Paragraph</td>
              <td width="402">
            <?php
				if(isset($_GET['edit_newsid']) && !empty($_GET['edit_newsid']))
				{
					$gets_title = $edit_obj->edit_news($_GET['edit_newsid']);
			?>
					<textarea name="paragraph" class="regularField" id="paragraph"><?php echo $gets_title['paragraph']; ?></textarea>
			<?php
				}
				else
				{
			?>
					<textarea name="paragraph" class="regularField" id="paragraph"></textarea>
			<?php
				}
			?>
              </td>
              <td width="148" align="left"><div class="hint">Upto 1000 characters</div></td>
            </tr>
            <tr>
              <td colspan="3"><img src="images/spacer.gif" alt=" " width="10" height="10" /></td>
            </tr>
            <tr>
              <td width="100" valign="top"></td>
              <td width="402" align="right">
                <input type="submit" name="postnews" id="postnews" value=" Save " />
              </td>
              <td width="148" align="left"><div class="hint"></div></td>
            </tr>
          </table>
          </form>
    </div>
</div>