<?php
# Logging in with Google accounts requires setting special identity, so this example shows how to do it.
require 'openid.php';
try {
    # Change 'localhost' to your domain name.
    $domainname=$_SERVER['SERVER_NAME'];
    $openid = new LightOpenID($domainname);
    if(!$openid->mode) {
        if(isset($_GET['login'])) {
			//var_dump($_GET);
            $openid->identity = 'https://www.google.com/accounts/o8/id';
			$openid->required = array('contact/email' , 'namePerson/first' , 'namePerson/last' , 'pref/language' , 'contact/country/home'); 
            header('Location: ' . $openid->authUrl());
        }
?>
<script>
	$("docuemnt").ready(function(){
	document.getElementById("log_google").click();
	});
</script>
<form action="?login" method="post">
    <button id="log_google">Login with Google</button>
</form>
<?php
    } elseif($openid->mode == 'cancel') {
        echo 'User has canceled authentication!';
    } else {
        echo 'User ' . ($openid->validate() ? $openid->identity . ' has ' : 'has not ') . 'logged in.';
    }
} catch(ErrorException $e) {
    echo $e->getMessage();
}
