<?php
//Logging in with Google accounts requires setting special identity, so this example shows how to do it.
	include_once('commons/db_new.php');
	include_once('../classes/EmailSubscription.php');
	$sub_listings = new EmailSubscription();
    $domainname=$_SERVER['SERVER_NAME'];
	//if(!isset($_POST))
	//{
	$date = date('Y-m-d');
	$futureDate = date('Y-m-d', strtotime('+1 year'));
	
	//echo $date;
?>
	<link rel="stylesheet" type="text/css" media="screen" href="../includes/purify.css" />
		<div id="outerContainer">
			<div id="purifyMasthead">
				<div id="purifyLogo">
					<a href="index.php"><img src="../images/purelogo.gif" alt="Purify Entertainment" width="302" height="60" border="0"/></a>
				</div>
			</div>
<?php
		if($_POST['sub_news']=="")
		{
			require 'openid.php';
		
			try
			{
				# Change 'localhost' to your domain name.
				$openid = new LightOpenID($domainname);
				 
				//Not already logged in
				if(!$openid->mode)
				{
					//The google openid url
					//https://me.yahoo.com/[your_username]
					$openid->identity = 'https://www.google.com/accounts/o8/id';
					 
					//Get additional google account information about the user , name , email , country
					$openid->required = array('contact/email' , 'namePerson/first' , 'namePerson/last' , 'pref/language' , 'contact/country/home');
					 
					//start discovery
					header('Location: ' . $openid->authUrl());
				}
				 
				else if($openid->mode == 'cancel')
				{
					echo 'User has canceled authentication!';
					//redirect back to login page ??
				}
				 
				//Echo login information by default
				else
				{
					if($openid->validate())
					{
						//User logged in
						$d = $openid->getAttributes();
						 
						$first_name = $d['namePerson/first'];
						$last_name = $d['namePerson/last'];
						$email = $d['contact/email'];
						$language_code = $d['pref/language'];
						$country_code = $d['contact/country/home'];
						 
						$data = array(
							'first_name' => $first_name ,
							'last_name' => $last_name ,
							'email' => $email ,
						);
	 
						//now signup/login the user.
						//process_google_data($data);
					}
					else
					{
						//user is not logged in
					}
				}
			}

			catch(ErrorException $e)
			{
				echo $e->getMessage();
			}
	//}
	//var_dump($data);
	
		if(isset($_GET['alldt']))
		{
			if($_GET['alldt']!="")
			{
				$details_get = explode('@',$_GET['alldt']);
				if($details_get[0]=='artist')
				{
					$sql = mysql_query("SELECT * FROM general_artist WHERE artist_id='".$details_get[1]."'");
				}
				elseif($details_get[0]=='community')
				{
					$sql = mysql_query("SELECT * FROM general_community WHERE community_id='".$details_get[1]."'");
				}
				elseif($details_get[0]=='artist_project' || $details_get[0]=='community_project')
				{
					$sql = mysql_query("SELECT * FROM $details_get[0] WHERE id='".$details_get[1]."'");
				}
				
				if(isset($sql))
				{
					if($sql!="" && $sql!=NULL)
					{
						if(mysql_num_rows($sql)>0)
						{
							$ans_unsub_dts = mysql_fetch_assoc($sql);
						}
					}
				}
			}
		}
?>
			<div id="actualContent" style="margin-left: 129px; margin-top: 19px; margin-bottom: 30px;">
				<h1>Subscriptions</h1>
				<div id="mediaContent">
					<div class="titleCont">
						<!--<div class="blkD">Profile</div>-->
						<div class="blkA" style="width:281px;">Name</div>
						<div class="blkB">Location</div>
						<div class="blkE">Type</div>
						<div class="blkF" style="float:right; width:49px;">Delete</div>
					</div>
					<?php
						$sub_alls = array();
						$get_all_art_subscriber = $sub_listings->get_all_org_sub($data['email']);
						$get_gen_info = $sub_listings->get_general_info($data['email']);
						if($get_all_art_subscriber!="" && $get_all_art_subscriber!=NULL)
						{
							if(mysql_num_rows($get_all_art_subscriber)>0)
							{
								while($get_art = mysql_fetch_assoc($get_all_art_subscriber))
								{
									$sub_alls[] = $get_art;
								}
							}
						}
						
						for($s_u=0;$s_u<count($sub_alls);$s_u++)
						{
							if($sub_alls[$s_u]['related_type'] == 'artist')
							{
								$types = 'artist';
							}
							elseif($sub_alls[$s_u]['related_type'] == 'community')
							{
								$types = 'community';
							}
							elseif($sub_alls[$s_u]['related_type'] == 'community_project' || $sub_alls[$s_u]['related_type'] == 'artist_project' || $sub_alls[$s_u]['related_type'] == 'community_event' || $sub_alls[$s_u]['related_type'] == 'artist_event')
							{
								$types = $sub_alls[$s_u]['related_type'];
							}
							
							if($sub_alls[$s_u]['related_type'] == 'community_project' || $sub_alls[$s_u]['related_type'] == 'artist_project' || $sub_alls[$s_u]['related_type'] == 'community_event' || $sub_alls[$s_u]['related_type'] == 'artist_event')
							{
								$related = explode('_',$sub_alls[$s_u]['related_id']);
								$get_subscriber_info = $sub_listings->get_subscriber_info_org($related[1],$types);
									
								$sub_alls[$s_u]['name'] = $get_subscriber_info['title'];
							}
							elseif($sub_alls[$s_u]['related_type'] == 'artist' || $sub_alls[$s_u]['related_type'] == 'community')
							{
								$get_subscriber_info = $sub_listings->get_subscriber_info_org($sub_alls[$s_u]['related_id'],$types);
								
								$sub_alls[$s_u]['name'] = $get_subscriber_info['name'];
							}
						}
						
						$sort = array();
						foreach($sub_alls as $k=>$v)
						{
							//if(isset($v['name']) && $v['name']!="")
							//{
								$sort['name'][$k] = strtolower($v['name']);
							//}
							
						}
						if($sort!="" && !empty($sort))
						{
							array_multisort($sort['name'], SORT_ASC,$sub_alls);
						}
						
						for($s_u=0;$s_u<count($sub_alls);$s_u++)
						{
							if($sub_alls[$s_u]['related_type'] == 'artist')
							{
								$types = 'artist';
								
								$get_subscriber_info = $sub_listings->get_subscriber_info_org($sub_alls[$s_u]['related_id'],$types);
								
								$get_gen_info_del = $sub_listings->artist_communitySubscribedTo($types,$sub_alls[$s_u]['related_id']);
							}
							elseif($sub_alls[$s_u]['related_type'] == 'community')
							{
								$types = 'community';
								
								$get_subscriber_info = $sub_listings->get_subscriber_info_org($sub_alls[$s_u]['related_id'],$types);
								
								$get_gen_info_del = $sub_listings->artist_communitySubscribedTo($types,$sub_alls[$s_u]['related_id']);
							}
							elseif($sub_alls[$s_u]['related_type'] == 'community_project' || $sub_alls[$s_u]['related_type'] == 'artist_project' || $sub_alls[$s_u]['related_type'] == 'community_event' || $sub_alls[$s_u]['related_type'] == 'artist_event')
							{										
								$types = $sub_alls[$s_u]['related_type'];
								$types_1 = explode('_',$sub_alls[$s_u]['related_type']);
								
								$related = explode('_',$sub_alls[$s_u]['related_id']);
								$get_subscriber_info = $sub_listings->get_subscriber_info_org($related[1],$types);
								
								if($sub_alls[$s_u]['related_type'] == 'community_project' || $sub_alls[$s_u]['related_type'] == 'community_event')
								{											
									$get_gen_info_del = $sub_listings->artist_communitySubscribedTo($types_1[0],$get_subscriber_info['community_id']);
								}
								elseif($sub_alls[$s_u]['related_type'] == 'artist_project' || $sub_alls[$s_u]['related_type'] == 'artist_event')
								{											
									$get_gen_info_del = $sub_listings->artist_communitySubscribedTo($types_1[0],$get_subscriber_info['artist_id']);
								}
							}

							if(!empty($get_subscriber_info) && $get_subscriber_info!="")
							{
							
					?>
							<div class="tableCont">
								<!--<div class="blkD"><img src="general_jcrop/croppedFiles/thumb/<?php //if(isset($run_sub_user_to['image_name']) && $run_sub_user_to['image_name']!="") {echo $run_sub_user_to['image_name']; } else{ echo "Noimage.png"; }?>" /></div>-->
								<div class="blkA" style="width:280px;"><?php if(isset($get_subscriber_info['name']) && !empty($get_subscriber_info['name'])) { echo $get_subscriber_info['name']; }elseif(isset($get_subscriber_info['title']) && !empty($get_subscriber_info['title'])) { echo $get_subscriber_info['title']; } else { echo "&nbsp"; } ?></div>
									<div class="blkB"><?php if(!empty($get_subscriber_info['city'])) { echo $get_subscriber_info['city']; } else { echo "&nbsp"; } ?></div>
									<div class="blkE" style="width:138px;"><?php
									if($sub_alls[$s_u]['related_type']=='artist')
									{
										$subs = "";
										$subs =  $sub_listings->get_artist_user_subtype($get_subscriber_info['artist_id']);
									}
									elseif($sub_alls[$s_u]['related_type']=='community')
									{
										$subs = "";
										$subs =  $sub_listings->get_community_user_subtype($get_subscriber_info['community_id']);
									}
									elseif($sub_alls[$s_u]['related_type']=='artist_project')
									{
										$subs = "";
										$subs =  $sub_listings->get_artistp_user_subtype($get_subscriber_info['id']);
									}
									elseif($sub_alls[$s_u]['related_type']=='artist_event')
									{
										$subs = "";
										$subs =  $sub_listings->get_artiste_user_subtype($get_subscriber_info['id']);
									}
									elseif($sub_alls[$s_u]['related_type']=='community_project')
									{
										$subs = "";
										$subs =  $sub_listings->get_communityp_user_subtype($get_subscriber_info['id']);
									}
									elseif($sub_alls[$s_u]['related_type']=='community_event')
									{
										$subs = "";
										$subs =  $sub_listings->get_communitye_user_subtype($get_subscriber_info['id']);
									}
									
								if($subs!="" && !empty($subs))
								{
									echo $subs['name'];
								}
								else
								{
									echo "&nbsp";
								}
								?></div>
								<!--<div class="blkF" onclick="runAccordion(<?php //echo $ans_sub_user[$sub_i]['id']; ?>);">
									<div onselectstart="return false;"><img src="images/profile/view.jpg" /></div>
								</div>-->
								<div class="icon" style="width:37px;" onclick="runAccordion_new('<?php echo $s_u; ?>_pay');">
									<div onselectstart="return false;"><img id="trcn_info_<?php echo $s_u; ?>" src="../images/profile/view.jpg" /></div>
								</div>

								<div class="blkF" style="width:39px;"><a href="javascript:void(0);" onclick="return confirm_subscriber_delete('<?php if(isset($get_subscriber_info['name']) && !empty($get_subscriber_info['name'])) { echo $get_subscriber_info['name']; }elseif(isset($get_subscriber_info['title']) && !empty($get_subscriber_info['title'])) { echo $get_subscriber_info['title']; } ?>','<?php echo $sub_alls[$s_u]['id']; ?>','<?php echo $get_gen_info_del['general_user_id']; ?>')"><img src="../images/profile/delete.png" style="margin-top:5px;" /></a></div>
								
								<div class="expandable" id="Accordion_new<?php echo $s_u; ?>_payContent" style="margin-left: 80px; top: 17px;">
									<div id="actualContent" style="width:400px;">
										<div class="fieldCont">
											<div class="fieldTitle">News Feeds :-</div>
	<?php
											if($sub_alls[$s_u]['news_feed']=='1')
											{
	?>
												<div class="chkCont">
													<input checked type="radio" class="chk" name="feeds_news_<?php echo $sub_alls[$s_u]['id']; ?>"  onClick="on_feeds('<?php echo $sub_alls[$s_u]['id']; ?>')" value="on"/>
													<div class="radioTitle" >On</div>
												</div>
												<div class="chkCont">
													<input type="radio" class="chk" name="feeds_news_<?php echo $sub_alls[$s_u]['id']; ?>" onClick="off_feeds('<?php echo $sub_alls[$s_u]['id']; ?>')" value="off"/>
													<div class="radioTitle">Off</div>
												</div>
	<?php
											}
											else
											{
	?>
												<div class="chkCont">
													<input type="radio" class="chk" name="feeds_news_<?php echo $sub_alls[$s_u]['id']; ?>"  onClick="on_feeds('<?php echo $sub_alls[$s_u]['id']; ?>')" value="on"/>
													<div class="radioTitle" >On</div>
												</div>
												<div class="chkCont">
													<input checked type="radio" class="chk"  name="feeds_news_<?php echo $sub_alls[$s_u]['id']; ?>" onClick="off_feeds('<?php echo $sub_alls[$s_u]['id']; ?>')" value="off"/>
													<div class="radioTitle">Off</div>
												</div>
	<?php
											}
	?>
										</div>
										<div class="fieldCont">
											<div class="fieldTitle">Email :-</div>
	<?php
											$type = '';
											if($sub_alls[$s_u]['related_type']=='artist' || $sub_alls[$s_u]['related_type']=='artist_project' || $sub_alls[$s_u]['related_type']=='artist_event')
											{
												$type = 'artist';
											}
											elseif($sub_alls[$s_u]['related_type']=='community' || $sub_alls[$s_u]['related_type']=='community_project' || $sub_alls[$s_u]['related_type']=='community_event')
											{
												$type = 'community';
											}
											
											if($sub_alls[$s_u]['email_update']=='1')
											{
	?>
												<div class="chkCont">
													<input checked type="radio" class="chk" name="emails_<?php echo $sub_alls[$s_u]['id']; ?>"  onClick="on_emails('<?php echo $sub_alls[$s_u]['id']; ?>','<?php echo $sub_alls[$s_u]['email']; ?>','<?php echo $sub_alls[$s_u]['subscribe_to_id']; ?>','<?php echo $type; ?>')" value="on"/>
													<div class="radioTitle" >On</div>
												</div>
												<div class="chkCont">
													<input type="radio" class="chk" name="emails_<?php echo $sub_alls[$s_u]['id']; ?>" onClick="off_emails('<?php echo $sub_alls[$s_u]['id']; ?>','<?php echo $sub_alls[$s_u]['email']; ?>','<?php echo $sub_alls[$s_u]['subscribe_to_id']; ?>','<?php echo $type; ?>')" value="off"/>
													<div class="radioTitle">Off</div>
												</div>
	<?php
											}
											else
											{
	?>
												<div class="chkCont">
													<input type="radio" class="chk" name="emails_<?php echo $sub_alls[$s_u]['id']; ?>"  onClick="on_emails('<?php echo $sub_alls[$s_u]['id']; ?>','<?php echo $sub_alls[$s_u]['email']; ?>','<?php echo $sub_alls[$s_u]['subscribe_to_id']; ?>','<?php echo $type; ?>')" value="on"/>
													<div class="radioTitle" >On</div>
												</div>
												<div class="chkCont">
													<input checked type="radio" class="chk"  name="emails_<?php echo $sub_alls[$s_u]['id']; ?>" onClick="off_emails('<?php echo $sub_alls[$s_u]['id']; ?>','<?php echo $sub_alls[$s_u]['email']; ?>','<?php echo $sub_alls[$s_u]['subscribe_to_id']; ?>','<?php echo $type; ?>')" value="off"/>
													<div class="radioTitle">Off</div>
												</div>
	<?php
											}
	?>
										</div>
									</div>
								</div>
							</div>
					<?php
								//}
							}
						}
				?>
				</div>
			</div>
<?php
		}
		
	/* if(isset($_POST))
	{
		if($_POST['sub_news']!="" && $_POST['unsub_email']!="")
		{
			if($_POST['unsub_details']!="" && isset($_GET['type_unsub']))
			{
				$details_to_sub = explode('@',$_GET['alldt']);
				
				if($details_to_sub[0]=='artist_project' || $details_to_sub[0]=='community_project')
				{	
					$sql_org = mysql_query("SELECT * FROM $details_to_sub[0] WHERE id='".$details_to_sub[1]."'");
					$ans_org = mysql_fetch_assoc($sql_org);
					
					if(isset($ans_org['artist_id']))
					{
						$sql_get_un = mysql_query("SELECT * FROM email_subscription WHERE email='".$_POST['unsub_email']."' AND related_id='".$ans_org['artist_id'].'_'.$details_to_sub[1]."' AND related_type='".$details_to_sub[0]."'");
						$org_id = $ans_org['artist_id'];
					}
					elseif(isset($ans_org['community_id']))
					{
						$sql_get_un = mysql_query("SELECT * FROM email_subscription WHERE email='".$_POST['unsub_email']."' AND related_id='".$ans_org['community_id'].'_'.$details_to_sub[1]."' AND related_type='".$details_to_sub[0]."'");
						$org_id = $ans_org['community_id'];
					}
				}
				else
				{
					$sql_get_un = mysql_query("SELECT * FROM email_subscription WHERE email='".$_POST['unsub_email']."' AND related_id='".$details_to_sub[1]."' AND related_type='".$details_to_sub[0]."'");
				}
				if(mysql_num_rows($sql_get_un)>0 && $_POST['sub_news']=='off')
				{
					$sql_get_gen = mysql_query("SELECT * FROM general_user WHERE email='".$_POST['unsub_email']."'");
					if($details_to_sub[0]=='artist_project' || $details_to_sub[0]=='community_project')
					{					
						mysql_query("DELETE FROM email_subscription WHERE email='".$_POST['unsub_email']."' AND related_id='".$org_id.'_'.$details_to_sub[1]."' AND related_type='".$details_to_sub[0]."'");
					}
					else
					{
						mysql_query("DELETE FROM email_subscription WHERE email='".$_POST['unsub_email']."' AND related_id='".$details_to_sub[1]."' AND related_type='".$details_to_sub[0]."'");
					}
				}
				elseif(mysql_num_rows($sql_get_un)<=0 && $_POST['sub_news']=='on')
				{
					$sql_get_gen = mysql_query("SELECT * FROM general_user WHERE email='".$_POST['unsub_email']."'");
					if($details_to_sub[0]=='artist')
					{
						$sql_sub_to = mysql_query("SELECT * FROM general_user WHERE artist_id='".$details_to_sub[1]."'");
					}
					elseif($details_to_sub[0]=='community')
					{
						$sql_sub_to = mysql_query("SELECT * FROM general_user WHERE community_id='".$details_to_sub[1]."'");
					}
					elseif($details_to_sub[0]=='artist_project' || $details_to_sub[0]=='community_project')
					{
						if($details_to_sub[0]=='artist_project')
						{
							$sql_sub_to = mysql_query("SELECT * FROM general_user WHERE artist_id='".$org_id."'");
						}
						elseif($details_to_sub[0]=='community_project')
						{
							$sql_sub_to = mysql_query("SELECT * FROM general_user WHERE community_id='".$org_id."'");
						}
					}
					
					
					if(mysql_num_rows($sql_get_gen)>0 && mysql_num_rows($sql_sub_to)>0)
					{
						$ans_get_gen = mysql_fetch_assoc($sql_get_gen);
						$ans_sub_to = mysql_fetch_assoc($sql_sub_to);
						
						if($details_to_sub[0]=='artist_project' || $details_to_sub[0]=='community_project')
						{
							$sql_query = mysql_query("INSERT INTO `email_subscription` (`purchase_date`,`expiry_date`,`name`,`email`,`related_id`,`related_type`,`news_feed`,`email_update`,`subscribe_to_id`) VALUES ('".$date."','".$futureDate."','".$ans_get_gen['fname'].' '.$ans_get_gen['lname']."','".$_POST['unsub_email']."','".$org_id.'_'.$details_to_sub[1]."','".$details_to_sub[0]."',1,1,'".$ans_sub_to['general_user_id']."')");
						}
						else
						{
							$sql_query = mysql_query("INSERT INTO `email_subscription` (`purchase_date`,`expiry_date`,`name`,`email`,`related_id`,`related_type`,`news_feed`,`email_update`,`subscribe_to_id`) VALUES ('".$date."','".$futureDate."','".$ans_get_gen['fname'].' '.$ans_get_gen['lname']."','".$_POST['unsub_email']."','".$details_to_sub[1]."','".$details_to_sub[0]."',1,1,'".$ans_sub_to['general_user_id']."')");
						}
					}
				}
				elseif(mysql_num_rows($sql_get_un)>0 && $_POST['sub_news']=='on')
				{
					$ans_get_sub_dts = mysql_fetch_assoc($sql_get_un);
					if($ans_get_sub_dts['expiry_date']>$date)
					{
						mysql_query("UPDATE email_subscription SET expiry_date='".$futureDate."',purchase_date='".$date."' WHERE id='".$ans_get_sub_dts['id']."'");
					}
				}
			}
			else
			{
				$sql_get_un = mysql_query("SELECT * FROM unsubscribe_newsletter WHERE email='".$_POST['unsub_email']."'");
				if(mysql_num_rows($sql_get_un)<=0 && $_POST['sub_news']=='off')
				{
					$sql_get_gen = mysql_query("SELECT * FROM general_user WHERE email='".$_POST['unsub_email']."'");
					if(mysql_num_rows($sql_get_gen)>0)
					{
						$ans_get_gen = mysql_fetch_assoc($sql_get_gen);
						$sql_query = mysql_query("INSERT INTO `unsubscribe_newsletter` (`general_user_id` ,`email`) VALUES ('".$ans_get_gen['general_user_id']."', '".$_POST['unsub_email']."')");
					}
					else
					{
						$sql_query = mysql_query("INSERT INTO `unsubscribe_newsletter` (`general_user_id` ,`email`) VALUES ('0', '".$_POST['unsub_email']."')");
					}
				}
				elseif(mysql_num_rows($sql_get_un)>0 && $_POST['sub_news']=='on')
				{
					mysql_query("DELETE FROM unsubscribe_newsletter WHERE email='".$_POST['unsub_email']."'");
				}
			}
			
			if($_POST['sub_news']=='on')
			{
				echo "You have successfully Subscribed.";
			}
			else
			{
				echo "You have successfully UnSubscribed.";
			}
		}
	} */

	include_once('../includes/footer.php');
	/* require 'openid.php';
	 
	try
	{
	    # Change 'localhost' to your domain name.
	    $openid = new LightOpenID('purifyart.net');
	     
	    //Not already logged in
	    if(!$openid->mode)
	    {
	        //The google openid url
			//https://me.yahoo.com/[your_username]
	        $openid->identity = 'https://www.google.com/accounts/o8/id';
	         
	        //Get additional google account information about the user , name , email , country
	        $openid->required = array('contact/email' , 'namePerson/first' , 'namePerson/last' , 'pref/language' , 'contact/country/home');
	         
	        //start discovery
	        header('Location: ' . $openid->authUrl());
	    }
	     
	    else if($openid->mode == 'cancel')
	    {
	        echo 'User has canceled authentication!';
	        //redirect back to login page ??
	    }
	     
	    //Echo login information by default
	    else
	    {
	        if($openid->validate())
	        {
	            //User logged in
	            $d = $openid->getAttributes();
	             
	            $first_name = $d['namePerson/first'];
	            $last_name = $d['namePerson/last'];
	            $email = $d['contact/email'];
	            $language_code = $d['pref/language'];
	            $country_code = $d['contact/country/home'];
	             
	            $data = array(
	                'first_name' => $first_name ,
	                'last_name' => $last_name ,
	                'email' => $email ,
	            );
	             
	                        //now signup/login the user.
	            process_google_data($data);
	        }
	        else
	        {
	            //user is not logged in
	        }
	    }
	}
	 
	catch(ErrorException $e)
	{
	    echo $e->getMessage();
	}
	
	function process_google_data($data)
	{
		$sql_get_un = mysql_query("SELECT * FROM unsubscribe_newsletter WHERE email='".$data['email']."'");
		if(mysql_num_rows($sql_get_un)<=0)
		{
			$sql_get_gen = mysql_query("SELECT * FROM general_user WHERE email='".$data['email']."'");
			if(mysql_num_rows($sql_get_gen)>0)
			{
				$ans_get_gen = mysql_fetch_assoc($sql_get_gen);
				$sql_query = mysql_query("INSERT INTO `unsubscribe_newsletter` (`general_user_id` ,`email`) VALUES ('".$ans_get_gen['general_user_id']."', '".$data['email']."')");
			}
			else
			{
				$sql_query = mysql_query("INSERT INTO `unsubscribe_newsletter` (`general_user_id` ,`email`) VALUES ('0', '".$data['email']."')");
			}
		}
		
		echo "You have successfully UnSubscribed.";
	} */
?>
<script src="../ui/jquery-1.7.2.js"></script>
<script>
	var ContentHeight = 250;
	var TimeToSlide = 250.0;
	var openAccordion = '';

	function runAccordion(index)
	{
	  var nID = "Accordion" + index + "Content";
	  if(openAccordion == nID)
		nID = '';
		
	  setTimeout("animate(" + new Date().getTime() + "," + TimeToSlide + ",'" + openAccordion + "','" + nID + "')", 33);
	  
	  openAccordion = nID;
	}

	function animate(lastTick, timeLeft, closingId, openingId)
	{  
	  var curTick = new Date().getTime();
	  var elapsedTicks = curTick - lastTick;
	  
	  var opening = (openingId == '') ? null : document.getElementById(openingId);
	  var closing = (closingId == '') ? null : document.getElementById(closingId);
	 
	  if(timeLeft <= elapsedTicks)
	  {
		if(opening != null)
		  opening.style.height = ContentHeight + 'px';
		
		if(closing != null)
		{
		  closing.style.display = 'none';
		  closing.style.height = '0px';
		}
		return;
	  }
	 
	  timeLeft -= elapsedTicks;
	  var newClosedHeight = Math.round((timeLeft/TimeToSlide) * ContentHeight);

	  if(opening != null)
	  {
		if(opening.style.display != 'block')
		  opening.style.display = 'block';
		opening.style.height = (ContentHeight - newClosedHeight) + 'px';
	  }
	  
	  if(closing != null)
		closing.style.height = newClosedHeight + 'px';

	  setTimeout("animate(" + curTick + "," + timeLeft +",'" + closingId + "','" + openingId + "')", 33);
	}
	
	var ContentHeight_new = 115;
	var TimeToSlide = 250.0;
	var openAccordion = '';

	function runAccordion_new(index)
	{
	  var nID = "Accordion_new" + index + "Content";
	  if(openAccordion == nID)
		nID = '';
		
	  setTimeout("animate_new(" + new Date().getTime() + "," + TimeToSlide + ",'" + openAccordion + "','" + nID + "')", 33);
	  
	  openAccordion = nID;
	}

	function animate_new(lastTick, timeLeft, closingId, openingId)
	{  
	  var curTick = new Date().getTime();
	  var elapsedTicks = curTick - lastTick;
	  
	  var opening = (openingId == '') ? null : document.getElementById(openingId);
	  var closing = (closingId == '') ? null : document.getElementById(closingId);
	 
	  if(timeLeft <= elapsedTicks)
	  {
		if(opening != null)
		  opening.style.height = ContentHeight_new + 'px';
		
		if(closing != null)
		{
		  closing.style.display = 'none';
		  closing.style.height = '0px';
		}
		return;
	  }
	 
	  timeLeft -= elapsedTicks;
	  var newClosedHeight = Math.round((timeLeft/TimeToSlide) * ContentHeight_new);

	  if(opening != null)
	  {
		if(opening.style.display != 'block')
		  opening.style.display = 'block';
		opening.style.height = (ContentHeight_new - newClosedHeight) + 'px';
	  }
	  
	  if(closing != null)
		closing.style.height = newClosedHeight + 'px';

	  setTimeout("animate_new(" + curTick + "," + timeLeft +",'" + closingId + "','" + openingId + "')", 33);
	}
	
	function on_feeds(id)
	{
		var dataString_news = 'feeds=on&ids='+id;
			
		$.ajax({
			type: 'POST',
			url : '../SubscriptionAjaxRadio.php',
			data: dataString_news,
			success: function(data) 
			{
				alert("Your settings are saved.");
			}
		});
	}
	
	function off_feeds(id)
	{
		var dataString_news = 'feeds=off&ids='+id;
			
		$.ajax({
			type: 'POST',
			url : '../SubscriptionAjaxRadio.php',
			data: dataString_news,
			success: function(data) 
			{
				alert("Your settings are saved.");
			}
		});
	}
	
	function on_emails(id,from,to,type)
	{
		var dataString_news = 'emails=on&ids='+id+'&from='+from+'&to='+to+'&type='+type;
			
		$.ajax({
			type: 'POST',
			url : '../SubscriptionAjaxRadio.php',
			data: dataString_news,
			success: function(data) 
			{
				alert("Your settings are saved.");
			}
		});
	}
	
	function off_emails(id,from,to,type)
	{
		var dataString_news = 'emails=off&ids='+id+'&from='+from+'&to='+to+'&type='+type;
			
		$.ajax({
			type: 'POST',
			url : '../SubscriptionAjaxRadio.php',
			data: dataString_news,
			success: function(data) 
			{
				alert("Your settings are saved.");
			}
		});
	}
	
	function confirm_subscriber_delete(s,emsil,id)
	{
		var dataString_news = 'emails=<?php echo $data['email']; ?>&ids='+id+'&act_id='+emsil;
		
		var find = confirm("Are You Sure You Want To Delete "+s+" Subscription?");
		if(find == true)
		{
			$.ajax({
				type: 'POST',
				url : '../delete_sub_from_email.php',
				data: dataString_news,
				success: function(data) 
				{
					alert("Your Subscription is deleted successfully.");
					document.getElementById('mediaContent').innerHTML = data;
				}
			});
		}
		else
		{
			return false;
		}
	}
</script>