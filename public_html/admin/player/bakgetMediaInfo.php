<?php include_once('../classes/newMedia.php');
$objMedia=new Media();	

if($_REQUEST['username']){
	$logedinFlag=0;
	$username=$_REQUEST['username'];
	$userpass=$_REQUEST['userpass'];
	$flag=$objMedia->checkUserLogin($username,$userpass);
	if($flag){
		$_SESSION['userInfo'] = $username;
		$logedinFlag=1;
	}	
	echo $logedinFlag;
	exit;
}



$profile_id=$_REQUEST['source'];
$media_type=$_REQUEST['tp'];
$media_id=$_REQUEST['ref'];
$action=$_REQUEST['act'];
$currPlaylist=$_REQUEST['pl'];

$creator=$_REQUEST['creator'];
$profile_type="artist";
$update=$_REQUEST['update'];
if(isset($update) && $update !="")
{
	$update_media = $objMedia->update_media($media_id);
	$update_user = $objMedia->update_login_media();
}

if($currPlaylist!="")
	$fileNameTime = $currPlaylist;
else
	$fileNameTime = time();

$filename = 'playlist/playlist'.$fileNameTime.'.xml';


if($action=="removeSong"){
	$songId = $_REQUEST['songId'];
	$playlist = "playlist/playlist".$currPlaylist.".xml";
	$xml = new DOMDocument();
	if (file_exists($playlist)) {
		$xml->load($playlist);
		$thedocument = $xml->documentElement;
		$songtags = $thedocument->getElementsByTagName('song');

		$nodeToRemove = null;
		foreach ($songtags as $songtag){
		  $attrValue = $songtag->getAttribute('id');
		  if ($attrValue == $songId) {
			$nodeToRemove = $songtag; //will only remember last one- but this is just an example :)
		  }
		}

		//Now remove it.
		if ($nodeToRemove != null)
			$thedocument->removeChild($nodeToRemove);

		$xml->save($filename);
	}
	exit;
}

if($action=="moveup"){
	$songId1 = $_REQUEST['songId'];
	$songId2 = $_REQUEST['songId1'];
	$playlist = "playlist/playlist".$currPlaylist.".xml";
	if (file_exists($playlist)) {
		$dom = new DOMDocument;
		$dom->load($playlist);

		$songtags = $dom->getElementsByTagName('song');
		$nodeToRemove = null;
		$nodeToBeSwapped =null;
		$nodeToBeSwappedWith = null;
		foreach ($songtags as $songtag){
		  $attrValue = $songtag->getAttribute('id');
		  if ($attrValue == $songId1) {
			$nodeToBeSwapped = $songtag;
		  }
		  if ($attrValue == $songId2) {
			$nodeToBeSwappedWith = $songtag;
		  }
		}


		$nodeToBeSwappedClone = $nodeToBeSwapped->cloneNode(true);
		$nodeToBeSwappedWithClone = $nodeToBeSwappedWith->cloneNode(true);

		$nodeToBeSwapped->parentNode->replaceChild($nodeToBeSwappedWithClone, $nodeToBeSwapped);
		$nodeToBeSwappedWith->parentNode->replaceChild($nodeToBeSwappedClone, $nodeToBeSwappedWith);
		$dom->save($playlist);
	}
	exit;
}
if($action=="movedown"){
	$songId1 = $_REQUEST['songId'];
	$songId2 = $_REQUEST['songId1'];
	$playlist = "playlist/playlist".$currPlaylist.".xml";
	if (file_exists($playlist)) {
		$dom = new DOMDocument;
		$dom->load($playlist);

		$songtags = $dom->getElementsByTagName('song');
		$nodeToRemove = null;
		$nodeToBeSwapped =null;
		$nodeToBeSwappedWith = null;
		foreach ($songtags as $songtag){
		  $attrValue = $songtag->getAttribute('id');
		  if ($attrValue == $songId2) {
			$nodeToBeSwapped = $songtag;
		  }
		  if ($attrValue == $songId1) {
			$nodeToBeSwappedWith = $songtag;
		  }
		}


		$nodeToBeSwappedClone = $nodeToBeSwapped->cloneNode(true);
		$nodeToBeSwappedWithClone = $nodeToBeSwappedWith->cloneNode(true);

		$nodeToBeSwapped->parentNode->replaceChild($nodeToBeSwappedWithClone, $nodeToBeSwapped);
		$nodeToBeSwappedWith->parentNode->replaceChild($nodeToBeSwappedClone, $nodeToBeSwappedWith);
		$dom->save($playlist);
	}
	exit;
}




if($action=="clearList"){
	$songId = $_REQUEST['songId'];
	$playlist = "playlist/playlist".$currPlaylist.".xml";
	$xml = new DOMDocument();
	if (file_exists($playlist)) {
	unlink($playlist);
	echo $filename;
		/*$xml->load($playlist);
		$thedocument = $xml->documentElement;
		$songtags = $thedocument->getElementsByTagName('song');
		$count = $songtags->length;
		foreach ($songtags as $songtag){
			$attrValue = $songtag->getAttribute('id');
			$thedocument->removeChild($songtag);
		}
		$xml->save($filename);*/
	}
	exit;
}

if($action=="getList"){
	$playlist = "playlist/playlist".$currPlaylist.".xml";
	$xml = new DOMDocument();
	if (file_exists($playlist)) {
		$xml->load($playlist);
		$songtags = $xml->getElementsByTagName('song');
		$songCounter=0;
		foreach ($songtags as $songtag)
		{
			$songProps[$songCounter]['songId'] = $songtag->getAttribute('id');
			$type = $songtag->getElementsByTagName('type');
			foreach ($type as $attr) 
			{ 
				$songProps[$songCounter]['type'] = $attr->nodeValue; 
			} 
			switch($songProps[$songCounter]['type']){
				case "audio":
						$title = $songtag->getElementsByTagName('songtitle');
						foreach ($title as $attr) 
						{ 
							$songProps[$songCounter]['title'] = $attr->nodeValue; 
						} 
						$gallery_title = $songtag->getElementsByTagName('gallery_title');
						foreach ($gallery_title as $attr) 
						{ 
							$songProps[$songCounter]['gallery_title'] = $attr->nodeValue; 
						} 
						$creatorNode = $songtag->getElementsByTagName('creator');
						foreach ($creatorNode as $attr) 
						{ 
							$songProps[$songCounter]['creator'] = $attr->nodeValue; 
						}
						$fromNode = $songtag->getElementsByTagName('from');
						foreach ($fromNode as $attr) 
						{ 
							$songProps[$songCounter]['from'] = $attr->nodeValue; 
						}
						$fromproNode = $songtag->getElementsByTagName('from_profileurl');
						foreach ($fromproNode as $attr) 
						{ 
							$songProps[$songCounter]['from_profileurl'] = $attr->nodeValue; 
						}
						$creatorproNode = $songtag->getElementsByTagName('creator_profileurl');
						foreach ($creatorproNode as $attr) 
						{ 
							$songProps[$songCounter]['creator_profileurl'] = $attr->nodeValue; 
						}
						
						$filename = $songtag->getElementsByTagName('songfilename');
						foreach ($filename as $attr) 
						{ 
							$songProps[$songCounter]['filename'] = $attr->nodeValue; 
						} 
						$media_id = $songtag->getElementsByTagName('media_id');
						foreach ($media_id as $attr) 
						{ 
							$songProps[$songCounter]['media_id'] = $attr->nodeValue; 
						} 

						$gallerytags = $songtag->getElementsByTagName('image');
						$galleryArray = Array();
						foreach ($gallerytags as $galleryImageTag)
						{
							$id = $galleryImageTag->getElementsByTagName('id');
							foreach ($id as $attr) 
							{ 
								$gArray['id'] = $attr->nodeValue; 
							} 
							$imageType = $galleryImageTag->getElementsByTagName('imageType');
							foreach ($imageType as $attr) 
							{ 
								$gArray['imageType'] = $attr->nodeValue; 
							} 
							//$gArray['id'] = $id->firstChild->nodeValue; 
							$title = $galleryImageTag->getElementsByTagName('title');
							foreach ($title as $attr) 
							{ 
								$gArray['title'] = $attr->nodeValue; 
							} 
							//$gArray['title'] = $title->nodeValue;
							$filename = $galleryImageTag->getElementsByTagName('filename');
							foreach ($filename as $attr) 
							{ 
								$gArray['filename'] = $attr->nodeValue; 
							} 
							$img_path = $galleryImageTag->getElementsByTagName('img_path');
							foreach ($img_path as $attr) 
							{ 
								$gArray['img_path'] = $attr->nodeValue; 
							} 
							//$gArray['img_path'] = $img_path->nodeValue;
							
							$galleryArray[] = $gArray;
						}
						$songProps[$songCounter]['galleryArray'] = urlencode(json_encode($galleryArray));
				
				break;
				case "video":
						$title = $songtag->getElementsByTagName('title');
						foreach ($title as $attr) 
						{ 
							$songProps[$songCounter]['title'] = $attr->nodeValue; 
						} 
						$songProps[$songCounter]['gallery_title'] = "-"; 

						$creatorNode = $songtag->getElementsByTagName('creator');
						foreach ($creatorNode as $attr) 
						{ 
							$songProps[$songCounter]['creator'] = $attr->nodeValue; 
						} 
						$fromNode = $songtag->getElementsByTagName('from');
						foreach ($fromNode as $attr) 
						{ 
							$songProps[$songCounter]['from'] = $attr->nodeValue; 
						} 
						
						$creatorProNode = $songtag->getElementsByTagName('creator_profileurl');
						foreach ($creatorProNode as $attr) 
						{ 
							$songProps[$songCounter]['creator_profileurl'] = $attr->nodeValue; 
						} 
						$fromProNode = $songtag->getElementsByTagName('from_profileurl');
						foreach ($fromProNode as $attr) 
						{ 
							$songProps[$songCounter]['from_profileurl'] = $attr->nodeValue; 
						} 
						$videoId = $songtag->getElementsByTagName('videoId');
						foreach ($videoId as $attr) 
						{ 
							$songProps[$songCounter]['videoId'] = $attr->nodeValue; 
						} 
						$videolink = $songtag->getElementsByTagName('videoLink');
						foreach ($videolink as $attr) 
						{ 
							$songProps[$songCounter]['videolink'] = $attr->nodeValue; 
						} 
						$media_id = $songtag->getElementsByTagName('media_id');
						foreach ($media_id as $attr) 
						{ 
							$songProps[$songCounter]['media_id'] = $attr->nodeValue; 
						} 
				break;
			}
			$songCounter++;
		}
	}

	echo json_encode($songProps);
exit;
}

$resultArray = $objMedia->getMediaById($media_type,$media_id,$profile_id,$profile_type);
//echo "<pre>";print_r($resultArray);

$bucketName = "medaudio";


/* read existing PlayList */
$prevResultArray = Array();
$xml = new DOMDocument('1.0', 'ISO-8859-1');
$xml->formatOutput = true;
if($currPlaylist!=""){
	if (file_exists($filename)) {
		$xml->load($filename);
		$root = $xml->firstChild;
	}else{
		$root = $xml->createElement("response");
	}
}else{
	$root = $xml->createElement("response");
}

/* playlist read End */
	switch($media_type){
		case "v":
			if($action=="playlist"){
				$resultArray = $objMedia->newgetMediaListByArtistId($media_type,$profile_id,$profile_type,$media_id);
				//print_r($resultArray);
				$songtags = $xml->getElementsByTagName('song');
			for($i=0;$i<count($resultArray);$i++){
				foreach ($songtags as $songtag){
				  $attrValue = $songtag->getAttribute('id');
				  if ($attrValue == $resultArray[$i]['id']) {
					$songId = $resultArray[$i]['id'];
						$thedocument = $xml->documentElement;
						$songtags = $thedocument->getElementsByTagName('song');

						$nodeToRemove = null;
						foreach ($songtags as $songtag){
						  $attrValue = $songtag->getAttribute('id');
						  if ($attrValue == $songId) {
							$nodeToRemove = $songtag; //will only remember last one- but this is just an example :)
						  }
						}
						//Now remove it.
						if ($nodeToRemove != null)
							$thedocument->removeChild($nodeToRemove);

						$xml->save($filename);
					}
				}
			
				$b = $xml->createElement("song");
				$mediaFile = $resultArray[$i]['videolink'];
				preg_match("#(?<=v=)[a-zA-Z0-9-]+(?=&)|(?<=v\/)[^&\n]+(?=\?)|(?<=v=)[^&\n]+|(?<=youtu.be/)[^&\n]+#", $mediaFile, $matches);
				$videoId = $matches[0];

				$b->setAttribute("id", $resultArray[$i]['id']);
				//$profile = $xml->createElement("profile");
				//$profile->appendChild($xml->createTextNode($resultArray[$i]['profile_id']));
				//$b->appendChild($profile);

				$type = $xml->createElement("type");
				$type->appendChild($xml->createTextNode('video'));
				$b->appendChild($type);
				
				$type = $xml->createElement("media_id");
				$type->appendChild($xml->createTextNode($resultArray[$i]['media_id']));
				$b->appendChild($type);

				if($resultArray[$i]['videotitle']=="")
				{
					$title = $xml->createElement("title");
					$title->appendChild($xml->createTextNode($resultArray[$i]['upload_video_title']));
					$b->appendChild($title);

				}
				else
				{
					$title = $xml->createElement("title");
					$title->appendChild($xml->createTextNode($resultArray[$i]['videotitle']));
					$b->appendChild($title);
				}

				$creatorNode = $xml->createElement("creator");
				$creatorNode->appendChild($xml->createTextNode($resultArray[$i]['creator']));
				$b->appendChild($creatorNode);
				
				$fromNode = $xml->createElement("from");
				$fromNode->appendChild($xml->createTextNode($resultArray[$i]['from']));
				$b->appendChild($fromNode);
				
				$creatorProNode = $xml->createElement("creator_profileurl");
				$creatorProNode->appendChild($xml->createTextNode($resultArray[$i]['creator_profileurl']));
				$b->appendChild($creatorProNode);
				
				$fromProNode = $xml->createElement("from_profileurl");
				$fromProNode->appendChild($xml->createTextNode($resultArray[$i]['from_profileurl']));
				$b->appendChild($fromProNode);

				$url = $xml->createElement("videoId");
				$url->appendChild($xml->createTextNode($videoId));
				$b->appendChild($url);

				if($resultArray[$i]['videolink']=="")
				{
					$url = $xml->createElement("videoLink");
					$url->appendChild($xml->createTextNode('http://medvideo.s3.amazonaws.com/'.$resultArray[$i]['video_name']));
					$b->appendChild($url);
				}
				else
				{
					/*if($videoId !="")
					{
						$url = $xml->createElement("videoLink");
						$url->appendChild($xml->createTextNode("http://youtu.be/".$videoId));
						$b->appendChild($url);
					}
					else
					{*/
						$url = $xml->createElement("videoLink");
						$url->appendChild($xml->createTextNode($resultArray[$i]['videolink']));
						$b->appendChild($url);
					//}
				}

				if($action=="playlist"){
					$ori = $root->childNodes->item(1);
					$root->insertBefore($b,$ori);
				}
				else{
					$root->appendChild($b);
				}
			}
			}
			if($action=="addvi")
			{
				$resultArray = $objMedia->newAddMediaListByArtistId($action,$profile_id,$profile_type,$media_id);
				//print_r($resultArray);
				$songtags = $xml->getElementsByTagName('song');
				for($i=0;$i<count($resultArray);$i++){
					$attr_found=0;
					foreach ($songtags as $songtag){
					  $attrValue = $songtag->getAttribute('id');
					  if ($attrValue == $resultArray[$i]['id']) {
							$attr_found=1;
						}
					}
				if($attr_found ==0)
				{
						$b = $xml->createElement("song");
						$mediaFile = $resultArray[$i]['videolink'];
						preg_match("#(?<=v=)[a-zA-Z0-9-]+(?=&)|(?<=v\/)[^&\n]+(?=\?)|(?<=v=)[^&\n]+|(?<=youtu.be/)[^&\n]+#", $mediaFile, $matches);
						$videoId = $matches[0];

						$b->setAttribute("id", $resultArray[$i]['id']);
						//$profile = $xml->createElement("profile");
						//$profile->appendChild($xml->createTextNode($resultArray[$i]['profile_id']));
						//$b->appendChild($profile);

						$type = $xml->createElement("type");
						$type->appendChild($xml->createTextNode('video'));
						$b->appendChild($type);
						
						$type = $xml->createElement("media_id");
						$type->appendChild($xml->createTextNode($resultArray[$i]['media_id']));
						$b->appendChild($type);

						if($resultArray[$i]['videotitle']=="")
						{
							$title = $xml->createElement("title");
							$title->appendChild($xml->createTextNode($resultArray[$i]['upload_video_title']));
							$b->appendChild($title);

						}
						else
						{
							$title = $xml->createElement("title");
							$title->appendChild($xml->createTextNode($resultArray[$i]['videotitle']));
							$b->appendChild($title);
						}

						$creatorNode = $xml->createElement("creator");
						$creatorNode->appendChild($xml->createTextNode($resultArray[$i]['creator']));
						$b->appendChild($creatorNode);
						
						$fromNode = $xml->createElement("from");
						$fromNode->appendChild($xml->createTextNode($resultArray[$i]['from']));
						$b->appendChild($fromNode);
						
						$creatorProNode = $xml->createElement("creator_profileurl");
						$creatorProNode->appendChild($xml->createTextNode($resultArray[$i]['creator_profileurl']));
						$b->appendChild($creatorProNode);
						
						$fromProNode = $xml->createElement("from_profileurl");
						$fromProNode->appendChild($xml->createTextNode($resultArray[$i]['from_profileurl']));
						$b->appendChild($fromProNode);

						$url = $xml->createElement("videoId");
						$url->appendChild($xml->createTextNode($videoId));
						$b->appendChild($url);

						if($resultArray[$i]['videolink']=="")
						{
							$url = $xml->createElement("videoLink");
							$url->appendChild($xml->createTextNode('http://medvideo.s3.amazonaws.com/'.$resultArray[$i]['video_name']));
							$b->appendChild($url);
						}
						else
						{
							/*if($videoId !="")
							{
								$url = $xml->createElement("videoLink");
								$url->appendChild($xml->createTextNode("http://youtu.be/".$videoId));
								$b->appendChild($url);
							}
							else
							{*/
								$url = $xml->createElement("videoLink");
								$url->appendChild($xml->createTextNode($resultArray[$i]['videolink']));
								$b->appendChild($url);
							//}
						}

						if($action=="play"){
							$ori = $root->childNodes->item(1);
							$root->insertBefore($b,$ori);
						}
						else{
							$root->appendChild($b);
						}
					}
				}
			
			}
			break;
		
		case "a":
			if($action=="play"){
				$resultArray = $objMedia->newgetMediaListByArtistId($media_type,$profile_id,$profile_type,$media_id);
				//print_r($resultArray);
				$songtags = $xml->getElementsByTagName('song');
			for($i=0;$i<count($resultArray);$i++){
				foreach ($songtags as $songtag){
				  $attrValue = $songtag->getAttribute('id');
				  if ($attrValue == $resultArray[$i]['id']) {
					$songId = $resultArray[$i]['id'];
						$thedocument = $xml->documentElement;
						$songtags = $thedocument->getElementsByTagName('song');

						$nodeToRemove = null;
						foreach ($songtags as $songtag){
						  $attrValue = $songtag->getAttribute('id');
						  if ($attrValue == $songId) {
							$nodeToRemove = $songtag; //will only remember last one- but this is just an example :)
						  }
						}
						//Now remove it.
						if ($nodeToRemove != null)
							$thedocument->removeChild($nodeToRemove);

						$xml->save($filename);
					}
				}
				$b = $xml->createElement("song");
				$b->setAttribute("id", $resultArray[$i]['id']);

				$bucket = $xml->createElement("bucketName");
				$bucket->appendChild($xml->createTextNode($bucketName));
				$b->appendChild($bucket);

				$type = $xml->createElement("type");
				$type->appendChild($xml->createTextNode('audio'));
				$b->appendChild($type);
				
				$type = $xml->createElement("media_id");
				$type->appendChild($xml->createTextNode($resultArray[$i]['media_id']));
				$b->appendChild($type);

				//$profile = $xml->createElement("profile");
				//$profile->appendChild($xml->createTextNode($resultArray[$i]['profile_id']));
				//$b->appendChild($profile);

				$title = $xml->createElement("songtitle");
				$title->appendChild($xml->createTextNode($resultArray[$i]['song_title_read']));
				$b->appendChild($title);
	
				if($resultArray[$i]['song_name'] =="")
				{
					$creatorNode = $xml->createElement("songfilename");
					$creatorNode->appendChild($xml->createTextNode($resultArray[$i]['audiolinkurl']));
					$b->appendChild($creatorNode);
				}
				else
				{
					$creatorNode = $xml->createElement("songfilename");
					$creatorNode->appendChild($xml->createTextNode('http://medaudio.s3.amazonaws.com/'.$resultArray[$i]['song_name']));
					$b->appendChild($creatorNode);
				}

			/*	if($resultArray[$i]['audio_link']=='0'){
					$file = $xml->createElement("songfilename");
					$file->appendChild($xml->createTextNode($resultArray[$i]['audio_file_name']));
					$b->appendChild($file);
				}else{
					$file = $xml->createElement("songfilename");
					$file->appendChild($xml->createTextNode($resultArray[$i]['audio_link']));
					$b->appendChild($file);
				}*/
				
				$creatorNode = $xml->createElement("creator");
				$creatorNode->appendChild($xml->createTextNode($resultArray[$i]['media_creator']));
				$b->appendChild($creatorNode);
				
				$creatorNode = $xml->createElement("from");
				$creatorNode->appendChild($xml->createTextNode($resultArray[$i]['media_from']));
				$b->appendChild($creatorNode);
				
				$fromproNode = $xml->createElement("from_profileurl");
				$fromproNode->appendChild($xml->createTextNode($resultArray[$i]['from_profileurl']));
				$b->appendChild($fromproNode);
				
				$creatorproNode = $xml->createElement("creator_profileurl");
				$creatorproNode->appendChild($xml->createTextNode($resultArray[$i]['creator_profileurl']));
				$b->appendChild($creatorproNode);
				
				if(is_array($resultArray[$i]['gallery_files']) && !empty($resultArray[$i]['gallery_files'])){
					$gallery_files = $resultArray[$i]['gallery_files'];

					$gallery_title = $xml->createElement("gallery_title");
					$gallery_title->appendChild($xml->createTextNode($resultArray[$i]['gallery_title_new']));
					$b->appendChild($gallery_title);


					$gallery = $xml->createElement("gallery");
					for($j=0;$j<count($gallery_files);$j++){

						$image = $xml->createElement("image");
							$id = $xml->createElement("id");
							$id->appendChild($xml->createTextNode($gallery_files[$j]['img_id']));
							$image->appendChild($id);

							$imageType = $xml->createElement("imageType");
							$imageType->appendChild($xml->createTextNode($gallery_files[$j]['image_type']));
							$image->appendChild($imageType);

							$title = $xml->createElement("title");
							$title->appendChild($xml->createTextNode($gallery_files[$j]['image_title']));
							$image->appendChild($title);

							$file = $xml->createElement("filename");
							$file->appendChild($xml->createTextNode($gallery_files[$j]['image_name']));
							$image->appendChild($file);
							
							$file_path = $xml->createElement("img_path");
							$file_path->appendChild($xml->createTextNode($gallery_files[$j]['img_path']));
							$image->appendChild($file_path);
						
						$gallery->appendChild($image);
					}
					$b->appendChild($gallery);						
				}
				if($action=="play"){
					$ori = $root->childNodes->item(1);
					$root->insertBefore($b,$ori);
				}
				else{
					$root->appendChild($b);
				}
			}
			}
			if($action=="add"){
				$resultArray = $objMedia->newAddMediaListByArtistId($action,$profile_id,$profile_type,$media_id);
				//print_r($resultArray);
			$songtags = $xml->getElementsByTagName('song');
			for($i=0;$i<count($resultArray);$i++){
				$attr_found=0;
				foreach ($songtags as $songtag){
				  $attrValue = $songtag->getAttribute('id');
				  if ($attrValue == $resultArray[$i]['id']) {
						$attr_found=1;
					}
				}
				if($attr_found ==0)
				{
					$b = $xml->createElement("song");
					$b->setAttribute("id", $resultArray[$i]['id']);

					$bucket = $xml->createElement("bucketName");
					$bucket->appendChild($xml->createTextNode($bucketName));
					$b->appendChild($bucket);

					$type = $xml->createElement("type");
					$type->appendChild($xml->createTextNode('audio'));
					$b->appendChild($type);
					
					$type = $xml->createElement("media_id");
					$type->appendChild($xml->createTextNode($resultArray[$i]['media_id']));
					$b->appendChild($type);
					
					$creatorNode = $xml->createElement("creator");
					$creatorNode->appendChild($xml->createTextNode($resultArray[$i]['media_creator']));
					$b->appendChild($creatorNode);
					
					$creatorNode = $xml->createElement("from");
					$creatorNode->appendChild($xml->createTextNode($resultArray[$i]['media_from']));
					$b->appendChild($creatorNode);
					
					$fromproNode = $xml->createElement("from_profileurl");
					$fromproNode->appendChild($xml->createTextNode($resultArray[$i]['from_profileurl']));
					$b->appendChild($fromproNode);
					
					$creatorproNode = $xml->createElement("creator_profileurl");
					$creatorproNode->appendChild($xml->createTextNode($resultArray[$i]['creator_profileurl']));
					$b->appendChild($creatorproNode);
					//$profile = $xml->createElement("profile");
					//$profile->appendChild($xml->createTextNode($resultArray[$i]['profile_id']));
					//$b->appendChild($profile);

					$title = $xml->createElement("songtitle");
					$title->appendChild($xml->createTextNode($resultArray[$i]['song_title_read']));
					$b->appendChild($title);
					if($resultArray[$i]['song_name'] =="")
					{
						$creatorNode = $xml->createElement("songfilename");
						$creatorNode->appendChild($xml->createTextNode($resultArray[$i]['audiolinkurl']));
						$b->appendChild($creatorNode);
					}
					else
					{
						$creatorNode = $xml->createElement("songfilename");
						$creatorNode->appendChild($xml->createTextNode('http://medaudio.s3.amazonaws.com/'.$resultArray[$i]['song_name']));
						$b->appendChild($creatorNode);
					}

				/*	if($resultArray[$i]['audio_link']=='0'){
						$file = $xml->createElement("songfilename");
						$file->appendChild($xml->createTextNode($resultArray[$i]['audio_file_name']));
						$b->appendChild($file);
					}else{
						$file = $xml->createElement("songfilename");
						$file->appendChild($xml->createTextNode($resultArray[$i]['audio_link']));
						$b->appendChild($file);
					}*/
					if(is_array($resultArray[$i]['gallery_files']) && !empty($resultArray[$i]['gallery_files'])){
						$gallery_files = $resultArray[$i]['gallery_files'];

						$gallery_title = $xml->createElement("gallery_title");
						$gallery_title->appendChild($xml->createTextNode($resultArray[$i]['gallery_title_new']));
						$b->appendChild($gallery_title);


						$gallery = $xml->createElement("gallery");
						for($j=0;$j<count($gallery_files);$j++){

							$image = $xml->createElement("image");
								$id = $xml->createElement("id");
								$id->appendChild($xml->createTextNode($gallery_files[$j]['img_id']));
								$image->appendChild($id);

								$imageType = $xml->createElement("imageType");
								$imageType->appendChild($xml->createTextNode($gallery_files[$j]['image_type']));
								$image->appendChild($imageType);

								$title = $xml->createElement("title");
								$title->appendChild($xml->createTextNode($gallery_files[$j]['image_title']));
								$image->appendChild($title);

								$file = $xml->createElement("filename");
								$file->appendChild($xml->createTextNode($gallery_files[$j]['image_name']));
								$image->appendChild($file);
								
								$file_path = $xml->createElement("img_path");
								$file_path->appendChild($xml->createTextNode($gallery_files[$j]['img_path']));
								$image->appendChild($file_path);
							
							$gallery->appendChild($image);
						}
						$b->appendChild($gallery);						
					}
					/*if($action=="play"){
						$ori = $root->childNodes->item(1);
						$root->insertBefore($b,$ori);
					}
					else{*/
						$root->appendChild($b);
				//	}
				}
			}
		}
			break;
	}
	$xml->appendChild($root);
	//header("Content-type: text/xml");
	$xml->save($filename);

	echo $fileNameTime;
	//echo $action;
	//var_dump($resultArray);
?>