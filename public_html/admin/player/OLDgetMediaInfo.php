<?php 
include_once('../classes/newMedia.php');

echo $media_type;
$objMedia=new Media();	

$profile_id=$_REQUEST['source'];
$media_type=$_REQUEST['tp'];
$media_id=$_REQUEST['ref'];
$action=$_REQUEST['act'];
$currPlaylist=$_REQUEST['pl'];
$profile_type="artist";

$resultArray = $objMedia->getMediaById($media_type,$media_id,$profile_id,$profile_type);
//echo "<pre>";print_r($resultArray);

$bucketName = "medaudio";

if($currPlaylist!="")
	$fileNameTime = $currPlaylist;
else
	$fileNameTime = time();

$filename = 'playlist/playlist'.$fileNameTime.'.xml';

/* read existing PlayList */
$prevResultArray = Array();
$xml = new DOMDocument('1.0', 'ISO-8859-1');
$xml->formatOutput = true;
	if($currPlaylist!=""){
		if (file_exists($filename)) {
			$xml->load($filename);
			$root = $xml->firstChild;
		}else{
			$root = $xml->createElement("response");
		}
	}else{
		$root = $xml->createElement("response");
	}

/* playlist read End */

	switch($media_type){
		case "v":
			if($action=="playlist"){
				$resultArray = $objMedia->getMediaListByArtistId($media_type,$profile_id,$profile_type);
				//print_r($resultArray);
			}
			for($i=0;$i<count($resultArray);$i++){
				$b = $xml->createElement("song");
				$mediaFile = $resultArray[$i]['video_link'];
				preg_match("#(?<=v=)[a-zA-Z0-9-]+(?=&)|(?<=v\/)[^&\n]+(?=\?)|(?<=v=)[^&\n]+|(?<=youtu.be/)[^&\n]+#", $mediaFile, $matches);
				$videoId = $matches[0];

				$b->setAttribute("id", $resultArray[$i]['video_id']);
				$profile = $xml->createElement("profile");
				$profile->appendChild($xml->createTextNode($resultArray[$i]['profile_id']));
				$b->appendChild($profile);

				$type = $xml->createElement("type");
				$type->appendChild($xml->createTextNode('video'));
				$b->appendChild($type);

				$title = $xml->createElement("title");
				$title->appendChild($xml->createTextNode($resultArray[$i]['video_name']));
				$b->appendChild($title);

				$url = $xml->createElement("videoId");
				$url->appendChild($xml->createTextNode($videoId));
				$b->appendChild($url);

				$root->appendChild($b);
			}
			break;
		
		case "a":
			if($action=="play"){
				$resultArray = $objMedia->newgetMediaListByArtistId($media_type,$profile_id,$profile_type,$media_id);
				//var_dump($resultArray);
			}
			for($i=0;$i<count($resultArray);$i++){
				$b = $xml->createElement("song");
				$b->setAttribute("id", $resultArray[$i]['id']);
				
				$bucket = $xml->createElement("bucketName");
				$bucket->appendChild($xml->createTextNode($bucketName));
				$b->appendChild($bucket);

				$type = $xml->createElement("type");
				$type->appendChild($xml->createTextNode('audio'));
				$b->appendChild($type);

				// $profile = $xml->createElement("profile");
				// $profile->appendChild($xml->createTextNode($resultArray[$i]['profile_id']));
				// $b->appendChild($profile);

				$title = $xml->createElement("songtitle");
				$title->appendChild($xml->createTextNode($resultArray[$i]['song_name']));
				$b->appendChild($title);

				$file = $xml->createElement("songfilename");
				$file->appendChild($xml->createTextNode('http://medaudio.s3.amazonaws.com/'.$resultArray[$i]['song_name']));
				$b->appendChild($file);
				
				if(is_array($resultArray[$i]['gallery_files']) && !empty($resultArray[$i]['gallery_files'])){
					$gallery_files = $resultArray[$i]['gallery_files'];

					$gallery = $xml->createElement("gallery");
					for($j=0;$j<count($gallery_files);$j++){

						$image = $xml->createElement("image");
							$id = $xml->createElement("id");
							$id->appendChild($xml->createTextNode($gallery_files[$j]['img_id']));
							$image->appendChild($id);

							$imageType = $xml->createElement("imageType");
							$imageType->appendChild($xml->createTextNode($gallery_files[$j]['image_type']));
							$image->appendChild($imageType);

							$title = $xml->createElement("title");
							$title->appendChild($xml->createTextNode($gallery_files[$j]['image_title']));
							$image->appendChild($title);

							$file = $xml->createElement("filename");
							$file->appendChild($xml->createTextNode($gallery_files[$j]['image_name']));
							$image->appendChild($file);
						
						$gallery->appendChild($image);
					}
					$b->appendChild($gallery);						
				}

				$root->appendChild($b);
			}
			break;
	}
	$xml->appendChild($root);
	$xml->save($filename);

	echo $fileNameTime;
	//var_dump($resultArray);
?>