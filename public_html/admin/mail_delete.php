<?php
	include_once('classes/Unverified.php');
	include('../sendgrid/SendGrid_loader.php');
	
	$sendgrid = new SendGrid('','');
	$verify_obj = new Unverified();
	
	if(isset($_GET['mail']) && $_GET['mail']!="")
	{
		$sql = $verify_obj->get_email($_GET['mail']); 
		$ans = mysql_fetch_assoc($sql);
		$to = $ans['email'];
				
			
				$subject = "Purify Art | Signup | Your confirmation link here";
				
				
				$header = "from: Purify Art < no-reply@purifyart.com>"."\r\n";
			
				$hostname = $_SERVER['SERVER_NAME']; //getting domain name
				$hostname = "http://".$hostname ;
				

				$message = "
Thanks for signing up! 
Your account has been created and you can login using the ".$ans['email']." after
you have activated your account by pressing the url below. If you ever forget your password
you may use the 'Password Help' link in the header of the site.<br/>

Please click this link to activate your account:
";
				
				
				$links_act = "$hostname/confirmation.php?passkey=".$_GET['mail'];
			
				//$sentmail = mail($to,$subject,$message,$header);
				$url = "newsletter/"; 
				$myFile = $url . "mail_confirmation.txt";
				$fh = fopen($myFile, 'r');
				$theData = fread($fh, filesize($myFile));
				fclose($fh);
				
				$data_email = explode('|~|', $theData);
				
				$ns_user_n = str_replace('{::message::}', $message, $data_email['1']);
				$para = str_replace('{::links::}', $links_act, $data_email['3']);
				
				$body_email = $data_email['0'] . $ns_user_n . $data_email['2'] . $para . $data_email['4'];
				
				$mail_grid = new SendGrid\Mail();

				$mail_grid->addTo($to)->
					   setFromName('Purify Art')->
						setFrom('no-reply@purifyart.com')->
					   setSubject($subject)->
					   //setHtml($message);
					   setHtml($body_email);
				$sendgrid -> smtp -> send($mail_grid);
	?>
		<script>
			alert("Verification Email has been sent successfully.");
			window.location.href = "verify_users.php";
		</script>
	<?php
	}
	elseif(isset($_GET['del']) && $_GET['del']!="")
	{
		$sql = $verify_obj->delete_user($_GET['del']); 
		?>
		<script>
			alert("User has been deleted successfully.");
			window.location.href = "verify_users.php";
		</script>
	<?php
	}
?>
