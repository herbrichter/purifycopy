<?php
include_once("classes/GeneralUser.php");

$new_obj = new GeneralUser();

if(isset($_GET['type']) && !isset($_GET['whom']))
{
	$set_active = $new_obj->Activate_or_Deactivate_general($_GET['id'],$_GET['active'],$_GET['type']);
	
	$set_active_aep = $new_obj->Activate_or_Deactivate_eventsandprojects_all($_GET['id'],$_GET['active'],$_GET['type']);
	
	$get_user = $new_obj->get_generals($_GET['id'],$_GET['type']);
	if($get_user['artist_id']!=0 && !empty($get_user['artist_id']))
	{
		$chk_art_val = 0;
		$type = 'artist';
		$sql_get_art = $new_obj->get_artcom_act($get_user['artist_id'],$type);
		if($sql_get_art['active']==1)
		{
			$chk_art_val = 1;
		}
	}
	
	if($get_user['community_id']!=0 && !empty($get_user['community_id']))
	{
		$chk_com_val = 0;
		$type = 'community';
		$sql_get_com = $new_obj->get_artcom_act($get_user['community_id'],$type);
		if($sql_get_com['active']==1)
		{
			$chk_com_val = 1;
		}
	}
	
	if(isset($chk_art_val))
	{
		if($chk_art_val==1)
		{
			if(isset($chk_com_val))
			{
				if($chk_com_val==1)
				{
					$all = 'done';
				}
			}
			else
			{
				$all = 'done';
			}
		}
	}
	
	if(isset($chk_com_val))
	{
		if($chk_com_val==1)
		{
			if(isset($chk_art_val))
			{
				if($chk_art_val==1)
				{
					$all = 'done';
				}
			}
			else
			{
				$all = 'done';
			}
		}
	}
	
	if(isset($all) && $all=='done')
	{
		$_GET['active'] = 1;
		$set_active = $new_obj->Activate_or_Deactivate($get_user['general_user_id'],$_GET['active']);
	}
	else
	{
		$_GET['active'] = 0;
		$set_active = $new_obj->Activate_or_Deactivate($get_user['general_user_id'],$_GET['active']);
	}
}
else if(isset($_GET['type']) && isset($_GET['whom']))
{
	$set_active = $new_obj->Activate_or_Deactivate_eventsandprojects($_GET['id'],$_GET['whom'],$_GET['value'],$_GET['type']);
}
else
{
	$set_active = $new_obj->Activate_or_Deactivate($_GET['id'],$_GET['active']);
	$art_com_act = $new_obj->getUserInfo($_GET['id']);
	if($art_com_act['artist_id']!=0 && !empty($art_com_act['artist_id']))
	{
		$_GET['type'] = 'artist';
		$set_active_a = $new_obj->Activate_or_Deactivate_general($art_com_act['artist_id'],$_GET['active'],$_GET['type']);
		
		$set_active_aep = $new_obj->Activate_or_Deactivate_eventsandprojects_all($art_com_act['artist_id'],$_GET['active'],$_GET['type']);
	}
	
	if($art_com_act['community_id']!=0 && !empty($art_com_act['community_id']))
	{
		$_GET['type'] = 'community';
		$set_active_c = $new_obj->Activate_or_Deactivate_general($art_com_act['community_id'],$_GET['active'],$_GET['type']);
		
		$set_active_cep = $new_obj->Activate_or_Deactivate_eventsandprojects_all($art_com_act['community_id'],$_GET['active'],$_GET['type']);
	}
	$del_media = $new_obj->delete_media($_GET['id'],$_GET['active']);
	
}
//echo $_SERVER['HTTP_REFERER'];
header("Location: ".$_SERVER['HTTP_REFERER']);