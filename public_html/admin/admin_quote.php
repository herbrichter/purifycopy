<?php
	include_once('includes/header.php');
	include_once('classes/Quote.php');
	
	if(isset($_POST['postquote']))
	{
		$quote_para=$_POST['quote_para'];
		$source=$_POST['source'];
		$source_link=$_POST['source_link'];
		$author=$_POST['author'];
		$author_link=$_POST['author_link'];
		
		$obj=new Quote();
		if($obj->addQuote($quote_para,$source,$source_link,$author,$author_link))
			$msg="<div style='background:#CCCCCC; padding:5px; float:right; text-align:center;' class='hintWhite'>Quote has been added.</div>";
		else
			$msg="<div style='background:#CC0000; padding:5px; float:right; text-align:center;' class='hintWhite'>Error in posting quote.</div>";
	}
	
	if(isset($_POST['accept']))
	{
		$quote_id=$_POST['quote_id'];
		
		$obj=new Quote();
		if($obj->markQuote($quote_id))
			$msg="<div style='background:#CCCCCC; padding:5px; float:right; text-align:center;' class='hintWhite'>Quote has been accepted.</div>";
		else
			$msg="<div style='background:#CC0000; padding:5px; float:right; text-align:center;' class='hintWhite'>Error in accepting quote.</div>";
	}	
	
	if(isset($_POST['reject']))
	{
		$quote_id=$_POST['quote_id'];
		
		$obj=new Quote();
		if($obj->delQuote($quote_id))
			$msg="<div style='background:#CCCCCC; padding:5px; float:right; text-align:center;' class='hintWhite'>Quote has been deleted.</div>";
		else
			$msg="<div style='background:#CC0000; padding:5px; float:right; text-align:center;' class='hintWhite'>Error in deleting quote.</div>";
	}	
	
	if(isset($_POST['remove']))
	{
		$quote_id=$_POST['quote_id'];
		
		$obj=new Quote();
		if($obj->removeQuote($quote_id))
			$msg="<div style='background:#CCCCCC; padding:5px; float:right; text-align:center;' class='hintWhite'>Quote has been removed.</div>";
		else
			$msg="<div style='background:#CC0000; padding:5px; float:right; text-align:center;' class='hintWhite'>Error in removing quote.</div>";
	}	
	
	$objQuote=new Quote();
	$rs=$objQuote->getQuote();	
	$rs2=$objQuote->getUserQuote();		
?>

  <div id="contentContainer">
    <div id="subNavigation">
   <?php 
	$active_nav = "quote";
	$active_sub_nav = "";
   include_once('admin_leftnavigarion.php'); ?>   
    </div>
    <div id="actualContent">
      <?php echo $msg; ?>
      <h1 class="withLine">Quotes</h1>
	  <?php include_once('quote_block.php'); ?>
      
        <div id="area-separator"></div>    
            <h3>Quotes for approval</h3>
            <?php 
                while($row2=mysql_fetch_assoc($rs2))
                {
            ?>
            <div id="quote-box">
                <div id="quote-separator" class="hintGray"></div>             
                <div id="quote-line"><?php echo $row2['quote_para'] ?></div>
                <div id="quote-line" class="hint"><a href="<?php echo $row2['source_link'] ?>" target="_blank"><?php echo $row2['source'] ?></a> | <a href="<?php echo $row2['author_link'] ?>" target="_blank"><?php echo $row2['author'] ?></a></div>
                
                <div id="quote-buttons" align="center">
                    <form id="<?php echo "form_a".$row2['quote_id']; ?>" name="<?php echo "form_".$row2['quote_id']; ?>" method="post" action="admin_quote.php">
                        <input name="quote_id" type="hidden" value="<?php echo $row2['quote_id']; ?>" />
                        <input name="reject" type="submit" value=" Reject " />
                    </form>
				</div>
                <div id="quote-buttons" align="center">
                    <form id="<?php echo "form_a".$row2['quote_id']; ?>" name="<?php echo "form_".$row2['quote_id']; ?>" method="post" action="admin_quote.php">
                        <input name="quote_id" type="hidden" value="<?php echo $row2['quote_id']; ?>" />
                        <input name="accept" type="submit" value=" Accept " />
                    </form>
                </div>
                            
            </div>
            <?php
                }
            ?>      
      
        <div id="area-separator"></div>    
            <h3>Published Quotes</h3>
            <?php 
                while($row=mysql_fetch_assoc($rs))
                {
            ?>
            		<div id="quote-box">
                		<div id="quote-line"><?php echo $row['quote_para'] ?></div>
                		<div id="quote-line" class="hint"><a href="<?php echo $row['source_link'] ?>" target="_blank"><?php echo $row['source'] ?></a> | <a href="<?php echo $row['author_link'] ?>" target="_blank"><?php echo $row['author'] ?></a></div>                        <div id="quote-buttons" align="center">
							<input name="quote_id" type="hidden" />
                		</div>   
						<div id="quote-buttons" align="center">
                    		<form id="<?php echo "form_a".$row['quote_id']; ?>" name="<?php echo "form_".$row['quote_id']; ?>" method=				"post" action="admin_quote.php">
                   		     	<input name="quote_id" type="hidden" value="<?php echo $row['quote_id']; ?>" />
                    	    	<input name="remove" type="submit" value="Remove" />
                    		</form>
						</div>
						<div id="quote-separator" class="hintGray"></div>            
            		</div>
            <?php
                }
            ?>      
        </div>
    <div class="clearMe"></div>
  </div>
  
<?php include_once('includes/footer.php'); ?>
