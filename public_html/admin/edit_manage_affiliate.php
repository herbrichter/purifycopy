<?php 

		//files included
		include_once('includes/header.php'); 	
		include_once('commons/db.php');
		include_once('classes/ProfileType.php');
		include_once('classes/Type.php');
		include_once('classes/SubType.php');
		include_once('classes/metaType.php');	
		include_once('../admin_type_.php');
		
		
		
		//profiletype object
		$objPofile_one = new ProfileType();
		$display_profile_data=$objPofile_one->displayData();
		
		//Type object
		$objType_one= new Type();
		$dispaly_type_data = $objType_one->displayData();
		
		//Subtype object
		$objSubType_one = new SubType();
		$display_subtype_data = $objSubType_one->displayData();
		
		//Metatype object
		$objMeta_one= new Meta();
		$display_Metatype_one = $objMeta_one->displayData();
		
		
		
		
		
?>



  <div id="contentContainer">
    <div id="subNavigation">
         <?php 
			$active_nav = "registration-add";
			$active_sub_nav = "registration-add";
		 include_once('admin_leftnavigarion.php'); ?>   
    </div>
    <div id="actualContent">
    	<h1>Purify Affiliate Registration</h1>
    	<form method="post" action="purify_affiliate_registration.php">
        <!--<form method="post" action="purify_affiliate_registration.php">-->
        	<div class="fieldCont">
            	<div class="fieldTitle">Website Name</div>
                <input name="web_name" type="text" class="fieldText" />
               
            </div>
            <div class="fieldCont">
            	<div class="fieldTitle">Domain <span class="subtitle">(Transferred or Sub Domain)</span></div>
               		 <select name="domain_type" class="dropdown">
                     		<option selected="selected">Select</option>
                			<option value="Transferred">Transferred</option>
                            <option value="Sub Domain">Sub Domain</option>
                	</select>
                <!--<input name="domain_type" type="text" class="fieldText" />-->
                
            </div>
        	<div class="fieldCont">
            	<div class="fieldTitle">Domain Name</div>
                <input name="domain_name" type="text" class="fieldText" />
            </div>
            <div class="fieldCont">
            	<div class="fieldTitle">Profile Type</div>
                <select name="profile_type" class="dropdown" >
                <?php
					while($row=mysql_fetch_array($display_profile_data))
					{
				?>
                		<option value="<?php $row['id']?>" <?php if($selected_profle_type_id==$row['id']) echo "selected='true'"; ?>><?php echo $row['name']?></option>
                    <?php } ?>
                </select>
            </div>
            <div class="fieldCont">
            	<div class="fieldTitle">Page Type</div>
                	<select name="type_val" class="dropdown">
                    <?php
					while($row=mysql_fetch_array($dispaly_type_data))
					{
						
				?>
                <option value="<?php $row['id']?>"><?php echo $row['name']?></option>
                <?php } ?>
                </select>
                <!--<input name="page_type" type="text" class="fieldText" />-->
            </div>
            <div class="fieldCont">
            	<div class="fieldTitle">Sub Type</div>
                <select name="sub_type_val" class="dropdown">
                <?php
					while($row=mysql_fetch_array($display_subtype_data))
					{
						
				?>
                	<option value="<?php $row['id']?>"><?php echo $row['name']?></option>
                    <?php } ?>
                </select>
                <!--<input name="sub_type" type="text" class="fieldText" />-->
            </div>
            <div class="fieldCont">
            	<div class="fieldTitle">Meta Type</div>
                <select name="meta_type_val" class="dropdown">
						<?php while($row=mysql_fetch_array($display_Metatype_one))
                            {
                        ?>
                            <option value="<?php $row['id']?>"><?php echo $row['name']?></option>
                            <?php } ?>
                
                 </select>
               <!-- <input name="meta_type" type="text" class="fieldText" />-->
            </div>
            <div class="fieldCont">
            	<div class="fieldTitle">Profile Image</div>
                <input name="image_path" type="file" class="fieldText" />
            </div>
            <div class="fieldCont">
            	<div class="fieldTitle">Admin Name</div>
                <input name="admin_name" type="text" class="fieldText" />
            </div>
            <div class="fieldCont">
            	<div class="fieldTitle">Admin Email</div>
                <input name="admin_email" type="text" class="fieldText" />
            </div>
            <div class="fieldCont">
            	<div class="fieldTitle">Admin Phone 1</div>
                <input name="admin_phone1" type="text" class="fieldText" />
            </div>
            <div class="fieldCont">
            	<div class="fieldTitle">Admin Phone 2</div>
                <input name="admin_phone2" type="text" class="fieldText" />
            </div>
            <input type="submit" value="Register" class="register" />
    	</form>
    </div>
    
    <div class="clearMe"></div>
    
  </div>


<?php include_once('includes/footer.php'); ?>
