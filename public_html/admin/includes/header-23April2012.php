<?php
	ob_start();
	include_once('loggedin_includes.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Purify Entertainment Administrator Control</title>
<link rel="stylesheet" type="text/css" media="screen" href="includes/purify-admin.css" />
<link rel="stylesheet" type="text/css" media="screen" href="../includes/lightbox.css" />
<script type="text/javascript" src="javascripts/lightbox.js"></script>
<script src="../javascripts/jquery-1.2.6.js"></script>
</head>
<body>
<div id="outerContainer">


  <div id="purifyMasthead">
    <div id="purifyLogo"><a href="index.php"><img src="images/4.gif" alt="Purify Entertainment" width="302" height="53" border="0" /></a></div>
	<?php include("loggedin_area.php"); ?>
  </div>
