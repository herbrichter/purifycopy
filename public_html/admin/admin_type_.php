<?php

//print_r($_POST);

	include_once('includes/header.php');
	include_once('commons/db.php');

	
	
		
	
	include_once('../classes/Type.php');
	include_once('../classes/SubType.php');	
	
	$section_id=$_GET['id'];
	if(!$_GET['id']) $section_id=1;
	
	$objType=new Type();
	$objSubType=new SubType();
	
	switch($section_id)
	{
		case 1:	$section="Artists";
				$parent="Artist";
				$rs=$objType->listTypes('Artist');
				$rstype=$objType->listTypes('Artist');
				$rstype2=$objType->listTypes('Artist');
				$rstype3=$objType->listTypes('Artist');
				
				break;
		case 2:	$section="Events";
				$parent="Event";
				$rs=$objType->listTypes('Event');
				$rstype=$objType->listTypes('Event');
				$rstype2=$objType->listTypes('Event');
				$rstype3=$objType->listTypes('Event');
				break;
		case 3:	$section="Community Members";
				$parent="Community";		
				$rs=$objType->listTypes('Community');		
				$rstype=$objType->listTypes('Community');
				$rstype2=$objType->listTypes('Community');
				$rstype3=$objType->listTypes('Community');								
				break;
		case 4:	$section="Projects";
				$parent="Project";
				$rs=$objType->listTypes('Project');
				$rstype=$objType->listTypes('Project');
				$rstype2=$objType->listTypes('Project');
				$rstype3=$objType->listTypes('Project');
				break;
		case 5:	$section="Services";
				$parent="Service";
				$rs=$objType->listTypes('Service');
				$rstype=$objType->listTypes('Service');
				$rstype2=$objType->listTypes('Service');
				$rstype3=$objType->listTypes('Service');
				break;
		case 6:	$section="Channels";
				$parent="Channel";
				$rs=$objType->listTypes('Channel');
				$rstype=$objType->listTypes('Channel');
				$rstype2=$objType->listTypes('Channel');
				$rstype3=$objType->listTypes('Channel');
				break;						
		default:$section="";
				break;																		
	}
		
	if($_POST['select-section'])
	{
		header('location:admin_type.php?id='.$_POST['select-section']);
	}

?>
<script>
	$(document).ready(
						function()
						{
    						$("#type-select").change(
														function ()
														{
															//alert($("#type-select").val());
															$.post("admin_block_subtype.php", { type_id:$("#type-select").val() },
  															function(data)
															{
    														//alert("Data Loaded: " + data);
																$("#subtype-select").replaceWith(data);
  															}
													);
											}
										)
										.change();
		
					}
				);
</script>
<script type="text/javascript" language="javascript">
	function Confirm(UID)
	{
	   var objWin, varURL;
	   varURL = 'confirm2.php?id=' + UID ;
	   objWin = window.open(varURL,"","left=300,top=300,toolbar=no,status=no,directories=no,titlebar=no,location=no,menubar=no,scrollbars=no,resizable=no,copyhistory=no,width=337,height=135");
	}//-->
	
	function Confirm2(UID)
	{
	   var objWin, varURL;
	   varURL = 'confirm3.php?id=' + UID ;
	   objWin = window.open(varURL,"","left=300,top=300,toolbar=no,status=no,directories=no,titlebar=no,location=no,menubar=no,scrollbars=no,resizable=no,copyhistory=no,width=337,height=135");
	}//-->
</script>
	
  <div id="contentContainer">
    <div id="subNavigation">
   <?php 
	$active_nav = "types-subtypes";
	$active_sub_nav = "";
   include_once('admin_leftnavigarion.php'); ?>   
    </div>
    <div id="actualContent">
      <div id="searchContainer">
      <form id="section" name="section" method="post" action="admin_type.php">
		<table width="100%" border="0">
          <tr>
            <td align="left" class="hint">Current Section: <span style="font-weight:bold"><?php echo $section; ?></span></td>
            <td align="right" class="hint">Select Profile Type: 
           
              <select name="select-section" class="pulldownField2" onchange="submit();">
				  <option value="0">--Select--</option>  
                  <?php 
				  //$count=1;
				   //while($row=mysql_fetch_array($result))
          				//{
				  ?>
                  <option value="<?php //echo $count?>"><?php //echo $row['User'] ?> </option>
                  <?php
				  		//$count++;
						
						//}
						
						
					?>
                              
                  <option value="1">Artists</option>
                  <option value="6">Channels</option>
                  <option value="3">Community Members</option>
                  <option value="2">Events</option>
                  <option value="4">Projects</option>
                  <option value="5">Services</option>
              </select> 
                      
            </td>
          </tr>
        </table>
      </form>
      </div>
      <h1 class="withLine">Types/Subtypes </h1>
      
      
      <div id="addTSContainer">
      <div id="addTypeTitle" class="hintTitle">Add a new Type</div>
      <div id="addSubTypeTitle" class="hintTitle">Add a new Subtype</div>
      
      <div id="addTypeContainer">
      	<form id="newtypeform" name="newtypeform" method="post" action="add_type.php">
      	<table width="100%" border="0">
          <tr>
            <td><div align="right">Section: </div></td>
            <td><input name="section-name" class="regularField2" type="text" disabled="disabled" value="<?php echo $section; ?>" /></td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td colspan="3"><div align="right"><img src="images/spacer.gif" alt=" " width="10" height="10" /></div></td>
          </tr>
          <tr>
            <td><div align="right">New type: </div></td>
            <td><input name="newtype" class="regularField2" type="text" /></td>
            <td></td>
          </tr>
          <tr>
            <td colspan="3"><div align="right"><img src="images/spacer.gif" alt=" " width="10" height="10" /></div></td>
          </tr>
		  <tr>
            <td><div align="right"> </div></td>
            <td><div id="dummytextfilled"></div></td>
            <td></td>
          </tr>
          <tr>
            <td colspan="3"><div align="right"><img src="images/spacer.gif" alt=" " width="10" height="10" /></div></td>
          </tr>                       
          <tr>
            <td><div align="right"></div></td>
            <td><input name="addtype" type="submit" value=" Add " /></td>
            <td><input name="parent" type="hidden" value="<?php echo $parent; ?>" /><input name="section_id" type="hidden" value="<?php echo $section_id; ?>" /></td>
          </tr>          
        </table>
        </form>
      </div>
      <div id="addSubTypeContainer">
      	<form id="newsubtypeform" name="newsubtypeform" method="post" action="add_subtype.php">
      	<table width="100%" border="0">
          <tr>
            <td><div align="right">Section: </div></td>
            <td><input name="section-name" class="regularField2" type="text" disabled="disabled" value="<?php echo $section; ?>" /></td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td colspan="3"><div align="right"><img src="images/spacer.gif" alt=" " width="10" height="10" /></div></td>
          </tr>
          <tr>
            <td><div align="right">Type: </div></td>
            <td> 
              <select name="select-type" class="pulldownField2">
              <?php 
			    while($row_type=mysql_fetch_assoc($rstype))
				{
			  ?>
              	<option value="<?php echo $row_type['type_id']; ?>"><?php echo $row_type['name']; ?></option>
			  <?php 
			  	}
				mysql_data_seek ($rstype, 0);
			  ?>              
              </select>
            </td>
            <td></td>
          </tr>
          <tr>
            <td colspan="3"><div align="right"><img src="images/spacer.gif" alt=" " width="10" height="10" /></div></td>
          </tr>          
          <tr>
            <td><div align="right">New subtype: </div></td>
            <td><input name="newsubtype" class="regularField2" type="text" /></td>
            <td></td>
          </tr>
          <tr>
            <td colspan="3"><div align="right"><img src="images/spacer.gif" alt=" " width="10" height="10" /></div></td>
          </tr>          
          <tr>
            <td><div align="right"></div></td>
            <td><input name="addsubtype" type="submit" value=" Add " /></td>
            <td><input name="section_id" type="hidden" value="<?php echo $section_id; ?>" /></td>
          </tr>
        </table>      
        </form>	
      </div>
      
      
      <div id="addSubTypeTitle" class="hintTitle">Add a new metatype</div>
      <div id="addTypeContainer">
      	<form id="newtypeform" name="newtypeform" method="post" action="add_type.php">
      	<table width="100%" border="0">
          <tr>
            <td><div align="right">Section: </div></td>
            <td><input name="section-name" class="regularField2" type="text" disabled="disabled" value="<?php echo $section; ?>" /></td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td colspan="3"><div align="right"><img src="images/spacer.gif" alt=" " width="10" height="10" /></div></td>
          </tr>
          <tr>
            <td><div align="right">Subtype: </div></td>
            <td><input name="section-name" class="regularField2" type="text" disabled="disabled" value="<?php  ?>" /></td>
            <td></td>
          </tr>
          <tr>
            <td colspan="3"><div align="right"><img src="images/spacer.gif" alt=" " width="10" height="10" /></div></td>
          </tr>          
          <tr>
            <td><div align="right">Type: </div></td>
            <td>
              <select name="select-type" class="pulldownField2">
              <?php 
			    while($row_type=mysql_fetch_assoc($rstype))
				{
			  ?>
              	<option value="<?php echo $row_type['type_id']; ?>"><?php echo $row_type['name']; ?></option>
			  <?php 
			  	}
			  ?>              
              </select>
            </td>
            <td></td>
          </tr>
          <tr>
            <td colspan="3"><div align="right"><img src="images/spacer.gif" alt=" " width="10" height="10" /></div></td>
          </tr>          
          <tr>
            <td><div align="right">NewMetatype: </div></td>
            <td><input name="newsubtype" class="regularField2" type="text" /></td>
            <tr>
            <td><div align="right"></div></td>
            <td><input name="addsubtype" type="submit" value=" Add " /></td>
            <td><input name="section_id" type="hidden" value="<?php echo $section_id; ?>" /></td>
          </tr>          
        </table>
        </form>
      </div>
      
      </div>
      
	  <?php 
	  	while($row=mysql_fetch_assoc($rs))
		{
			$rs2=$objSubType->listSubTypes($row['type_id']);
	  ?>      
      <div id="typeSubtypeContainer">
      	<div id="typeTitle" class="hintBig">
          <table width="100%" border="0">
            <tr>
              <td><?php echo $row['name']; ?></td>
              <td><?php include('remove_type_form.php'); ?></td>
            </tr>
          </table>
        </div>
        <div id="subTypeTitle">
          <ul>
          <?php
            while($row2=mysql_fetch_assoc($rs2))
          	{
          ?>
          <table width="100%" border="0">
            <tr>
              <td><li style="color:#777777"><?php echo $row2['name']; ?></li></td>
              <td><?php include('remove_subtype_form.php'); ?></td>
            </tr>
          </table>
          
          <?php
          	}
		  ?>
          </ul>
        </div>
      </div>
      <?php 
	  	}
	  ?>
      
    </div>
    
    <div class="clearMe"></div>
    
  </div>
  
<?php include_once('includes/footer.php'); ?>