<?php
	include_once('includes/header.php');
	include_once('classes/User.php');
	include_once('../classes/UnregUser.php');
?>
<script type="text/javascript" language="javascript">
	function validate()
	{
		var emails=$('#emails');
		var frm=document.getElementById('frm');
		
		if(emails.val()=='')
		{
			alert("File is not selected.");
		}
		else
		{
			frm.action = "admin_email_upload.php";
			document.frm.submit();
		}
		
	}
</script>
  <div id="contentContainer">
    <div id="subNavigation">
   <?php 
	$active_nav = "upload-emails";
	$active_sub_nav = "";
	include_once('admin_leftnavigarion.php'); ?>   
    </div>
    <div id="actualContent">
	 <h1 class="withLine">Upload Emails</h1>
	
<form name="frm" id="frm" method="post" enctype="multipart/form-data">
      <input type="file" name="emails" id="emails" />
	  <input type="submit" name="submit" value="Upload Emails"/>
</form>

<?php
	
		if($_POST['submit'])
		{
			if($_FILES['emails']['name']!='')
			{
				$row = 1;
				$file = $_FILES['emails'];
				if (($handle = fopen($_FILES['emails']['tmp_name'], "r")) !== FALSE) 
				{
					$count = 1;
					$error_record = array();
					while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) 
					{
						$parent='None';
						$uname=$data[0];
						$uemail=$data[1];
						if(eregi("^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$", $uemail))
						{
							$uobj=new UnregUser();
							if($uobj->checkPresent($uemail)==NULL)
							{		
								if($uobj->addUser($parent,mysql_real_escape_string($uemail),mysql_real_escape_string($uname)))
								if($uobj->addUser(mysql_real_escape_string($uemail)))
								{
									$uflag=1;
								}
							}
						}
						else
						{
							//echo "Email format error - <br/>Record No : ".$count." - '".$uemail."' does not have correct email format. Make it correct or delete the record and again upload the entire file.";
							$error_record[$count] = $uemail;
							$eflag = 1;
							
						}
						$count++;
					}
					fclose($handle);
					if($eflag != 1)
					{
						echo "Emails are uploaded to unregistered users group.";
					}
					else
					{
						echo '<b style="color:#FF0000">Email format error -</b> Following email records are not in correct format. Correct them and upload again. No need to upload other correct emails again. Correct emails are already stored in the database.<br/>';
						echo "<table>";
						echo "<tr><th>Record No.</th><th>Email</th></tr>";
						foreach($error_record as $record => $email)
						{
							echo "<tr><td>".$record."</td><td>".$email."</td></tr>";
						}
						echo "</table>";
					}
				}
			}
			else
			{
				echo "File is not selected. Please select .csv file";
			}
		}
		
	?>
    </div>
    
    <div class="clearMe"></div>
    
  </div>

<?php include_once('includes/footer.php'); ?>
