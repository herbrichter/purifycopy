<?php
	include_once('includes/header.php');
	include('../sendgrid/SendGrid_loader.php');
?>
<style>
/* This rule is read by Galleria to define the gallery height: */
    .galleria-class{height:320px}
</style>
<link href="../galleryfiles/gallery.css" rel="stylesheet"/>
<script src="../galleryfiles/jquery.easing.1.3.js"></script>
<script src="../javascripts/swfobject.js"></script>
  
	<div id="contentContainer">
		<div id="subNavigation">
			<?php  include_once('admin_leftnavigarion.php'); ?>   
		</div>
	
		<div id="actualContent">
			<h1 class="withLine">Register User</h1>
			<form onsubmit="return login_vals();" action="" method="POST"> 
				<div class="fieldCont">
					<div class="fieldTitle">Artist Name</div>
					<input type="text" class="fieldText" id="art_dumname" name="art_dumname">
				</div> 
				
				<div class="fieldCont">
					<div class="fieldTitle">Email</div>
					<input type="text" id="email_reset" class="fieldText" name="email_reset">
				</div>
				<input type="hidden" value="" id="type_id" class="fieldText" name="type_id">
					
			  
				<div class="fieldCont">
					<div class="fieldTitle">Password</div>
					<input type="text" class="fieldText" id="password_reset" name="password_reset">
				</div>
			 
				<div class="fieldCont">
					<div class="regiCont">
						<input type="submit" value="Register" class="register" name="submit">
					</div>
				</div>
			</form>
		</div>
	</div>

<?php include_once('includes/footer.php'); ?>

<!--files for search technique-->
<link rel="stylesheet" href="../includes/jquery.ui.all.css">
<link rel="stylesheet" href="../css/jquery.autocomplete.css" type="text/css" />
<link  rel="stylesheet" type="text/css" media="screen" href="../css/jquery.ui.autocomplete.css"></link>
<style>
	.ui-autocomplete-loading { background: white url('../css/ui-anim_basic_16x16.gif') right center no-repeat; }
	.ui-menu { z-index: 100 !important; }
</style>

<script src="../ui/jquery-1.7.2.js"></script>
<script src="../includes/jquery.ui.core.js"></script>
<script src="../includes/jquery.ui.widget.js"></script>

<script type="text/javascript" src="../javascripts/jquery.ui.position.js"></script>
<script type="text/javascript" src="../javascripts/jquery.ui.autocomplete.js"></script>
<!--files for search technique ends here -->
<script type="text/javascript">
	function login_vals()
	{
		var name_art = $("#art_dumname").val();
		var email = $("#email_reset").val();
		var password = $("#password_reset").val();
		var atpos = email.indexOf("@");
		var dotpos = email.lastIndexOf(".");
		
		if(name_art=="" || name_art==null)
		{
			  alert("Please enter artist name.");
			  return false;
		}
		
		else if(email=="" || email==null)
		{
			  alert("Please enter email ID.");
			  return false;
		}
		
		else if (atpos<1 || dotpos<atpos+2 || dotpos+2>=email.lenght || email=="" || email==null)
		{
			alert("Please enter a valid email address.");
			return false;
		}
		 
		else if(password=="" || password==null)
		{
			 alert("Please enter password.");
			 return false;
		}
	}
	
	$(function() {
		$( "#art_dumname" ).autocomplete({
			source: "imported_profiles.php",
			minLength: 1,
			focus: function (event, ui) {
					document.getElementById("type_id").value = ui.item.value1;
					},
			select: function( event, ui ) {			
			document.getElementById("type_id").value = ui.item.value1;
			},
			open:function(event,ui){ 
				var maxListLength=10;
				var ul = jQuery( "#art_dumname" ).autocomplete("widget")[0];
				var len_org = ul.childNodes.length;
				while(ul.childNodes.length > maxListLength)
				{
					  ul.removeChild(ul.childNodes[ul.childNodes.length-1]);
				}
			}
		});
		
		$('.ui-autocomplete').on('click', '.ui-menu-item', function(){
			$('#searchlink').trigger('click');
		});
	});
</script>

<?php
	if(isset($_POST))
	{
		if(!empty($_POST))
		{
			if($_POST['art_dumname']!="" && $_POST['email_reset']!="" && $_POST['password_reset']!="" && $_POST['type_id']!="")
			{
				$hostname = $_SERVER['SERVER_NAME']; //getting domain name
				$hostname = "http://".$hostname ;
						
				$email = $_POST['email_reset'];
				$sendgrid = new SendGrid('','');

				$mail_grid = new SendGrid\Mail();
				$confirm_code = '123456';
				
				$message = "
To claim your artist page, verify by clicking the button below.<br/>";

				$links_act = "$hostname/verif_fication_dump.php?email_dump=".$_POST['email_reset'].'&ids_dumart='.$_POST['type_id'].'&pass_change='.$_POST['password_reset'];
						
				$url = "newsletter/"; 
				$myFile = $url . "mail_verification_dump.txt";
				$fh = fopen($myFile, 'r');
				$theData = fread($fh, filesize($myFile));
				fclose($fh);
				
				$data_email = explode('|~|', $theData);
				
				$ns_user_n = str_replace('{::message::}', $message, $data_email['1']);
				$para = str_replace('{::links::}', $links_act, $data_email['3']);
				
				$body_email = $data_email['0'] . $ns_user_n . $data_email['2'] . $para . $data_email['4'];		
				
				$mail_grid = new SendGrid\Mail();
				$mail_grid->addTo($email)->
							   setFromName('Purify Art')->
							   setFrom('no-reply@purifyart.net')->
							   setSubject('Purify Art | Signup | Your confirmation link here')->
							   setHtml($body_email);
							   
				if($sendgrid -> smtp -> send($mail_grid)) 
				{
					$sentmail = true;
				}
?>
				<script>
					alert("Verification mail sent successfully.");
				</script>
<?php
			}
		}
	}
?>
