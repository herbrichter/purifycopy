<?php
	include_once('includes/header.php');
	include_once('classes/User.php');
	include_once('classes/AddAgreement.php');
	include_once('../classes/UnregUser.php');
?>
<script type="text/javascript" language="javascript">
	function validate()
	{
		var emails=$('#emails');
		var frm=document.getElementById('frm');
		
		if(emails.val()=='')
		{
			alert("File is not selected.");
		}
		else
		{
			frm.action = "admin_email_upload.php";
			document.frm.submit();
		}
		
	}
</script>

<script>
!window.jQuery && document.write('<script src="./fancybox/jquery-1.4.3.min.js"><\/script>');
</script>
<script type="text/javascript" src="./fancybox/jquery.fancybox-1.3.4.pack.js"></script>
<link rel="stylesheet" type="text/css" href="./fancybox/jquery.fancybox-1.3.4.css" media="screen" />

<script type="text/javascript">             
	$(document).ready(function() {
		$("#Add_Agreement").fancybox({
		'autoScale'			: true,
		'transitionIn'		: 'none',
		'transitionOut'		: 'none',
		'type'				: 'iframe'

		});
	});
</script>

<div id="contentContainer">
    <div id="subNavigation">
	   <?php 
		$active_nav = "upload-emails";
		$active_sub_nav = "";
		include_once('admin_leftnavigarion.php'); 
		$Agreement = new AddAgreement();
		$get_agreement = $Agreement->Select_Agreement();
	?>   
		
    </div>
	
	<div id="actualContent">
    	<h1>Agreement</h1>
        <div class="manageCont">
			<a href ="insert_agreement.php" id="Add_Agreement"><input type="button" value=" Add New "/><br/></a>
			<?php echo "";?>
				<div class="titleCont">
					<div class="blkA" style="width:200px;">Name</div>
					<div class="blkB" style="width:350px;text-align:start;">Agreement</div>
					<div class="blkD" >Edit</div>
				</div>
			
			<?php
				while($agree_row=mysql_fetch_assoc($get_agreement))
				{
			?>
					<div id="AccordionContainer" class="tableCont">
						<div class="blkA" style="width:200px;">
							<?php
								echo $agree_row['agreement_title'];
							?>
						</div>
						<div class="blkB" style="text-align:start;width:350px;">
							<?php
								echo $agree_row['agreement'];
							?>
						</div>
						<div class="blkD">
							<a href="insert_agreement.php?edit=<?php echo $agree_row['id'];?>" id="Edit_agreement<?php echo $agree_row['id'];?>">Edit</a>
						</div>
						<script type="text/javascript">             
							$(document).ready(function() {
								$("#Edit_agreement<?php echo $agree_row['id'];?>").fancybox({
								'autoScale'			: true,
								'transitionIn'		: 'none',
								'transitionOut'		: 'none',
								'type'				: 'iframe'

								});
							});
						</script>
					</div>
			<?php
			}
			?>
       </div>    
    </div>
    <div class="clearMe"></div>
</div>

<?php include_once('includes/footer.php'); ?>
