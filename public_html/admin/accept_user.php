<?php
	ob_start();
	include_once('../sendgrid/SendGrid_loader.php');
	include_once("classes/User.php");
	include_once('../classes/RegistrationEmail.php');
	include_once('classes/ApprovalEmail.php');
	include_once('../classes/UserType.php');
	include_once('classes/TypeFeatures.php');
	include_once('classes/SubTypeFeatures.php');
	include_once('../classes/UserSubscription.php');			
	
	$obj=new User();
	$user_id=$_POST['uid'];
	$url=$_POST['url'];
	$obj->acceptUser($user_id);
	$row=$obj->getUserInfo($user_id);
	$objUserType=new UserType();
	
	
	if($row['user_type']=='fan')
	{
		$objSub=new UserSubscription();
		$rs2=$objSub->getUserSubscription($user_id);
		$row2=mysql_fetch_assoc($rs2);
		
		$objReg=new RegistrationEmail();
		$objReg->name=$row['name'];
		$objReg->elink="http://www.purifyentertainment.net/Design/registration_fan_verify.php?s=".$row2['subscription_id']."&id=".$row['verification_no'];
		$objReg->recipient=$row['email'];
		$objReg->username=$row['username'];
		$objReg->ref_no=$row['ref_no'];
		$objReg->sendFan($row2['subscription_id']);
	}
	else
	{
		$objApp=new ApprovalEmail();
		$objApp->name=$row['name'];
		$objApp->recipient=$row['email'];
		$objApp->send();	
	}
	
	if($row['user_type']=='artist'||$row['user_type']=='community')
	{
		$feature_category=$row['user_type'];
		$feature_id=$user_id;
		$usertypers=$objUserType->getUserTypesByUserId($user_id);
		while($usertyperow=mysql_fetch_assoc($usertypers))
		{
			$type_id=$usertyperow['type_id'];
			$subtype_id=$usertyperow['subtype_id'];
			
			$objTypeFeatures=new TypeFeatures();
			$objTypeFeatures->addFeature($feature_category,$feature_id,$type_id);
			$obj->markAsFeaturedType($feature_category,$feature_id,$type_id);
			
			$objSubTypeFeatures=new SubTypeFeatures();
			$objSubTypeFeatures->addFeature($feature_category,$feature_id,$type_id,$subtype_id);
			$obj->markAsFeaturedSubType($feature_category,$feature_id,$type_id,$subtype_id);				
		}
		
		
		if($row['user_type']=='artist')
		{
			$objUser1=new User();
			if($objUser1->markAsFeaturedArtist($user_id))
				$flag=1;
			else
				$msg="<div style='background:#CC0000; padding:5px; float:right; text-align:center;' class='hintWhite'>Error in adding artist with id ".$user_id." as feature.</div>";	
						
			if($flag)
			{
				$objFeature=new ArtistFeatures();
				if($objFeature->addFeature('artist',$user_id))
					$msg="<div style='background:#CCCCCC; padding:5px; float:right; text-align:center;' class='hintWhite'>Feature has been added.</div>";
				else
					$msg="<div style='background:#CC0000; padding:5px; float:right; text-align:center;' class='hintWhite'>Error in adding feature.</div>";
			}
								
		}
		
		if($row['user_type']=='community')
		{
		$objUser1=new User();
							if($objUser1->markAsFeaturedCommunity($user_id))
								$flag=1;
							else
								$msg="<div style='background:#CC0000; padding:5px; float:right; text-align:center;' class='hintWhite'>Error in adding community with id ".$user_id." as feature.</div>";	
					
							if($flag)
							{
								$obj=new CommunityFeatures();
								if($obj->addFeature('community',$user_id))
									$msg="<div style='background:#CCCCCC; padding:5px; float:right; text-align:center;' class='hintWhite'>Feature has been added.</div>";
								else
									$msg="<div style='background:#CC0000; padding:5px; float:right; text-align:center;' class='hintWhite'>Error in adding feature.</div>";
							}
			}
			
			switch($row['user_type'])
			{
				case 'artist':$objUser1=new User();
					if($objUser1->markAsFeatured('artist',$user_id))
						$flag_i=1;
					else
						$msg="<div style='background:#CC0000; padding:5px; float:right; text-align:center;' class='hintWhite'>Error in adding user with id ".$user_id." as feature.</div>";	
					break;
				case 'community':$objUser1=new User();
					if($objUser1->markAsFeatured('community',$user_id))
						$flag_i=1;
					else
						$msg="<div style='background:#CC0000; padding:5px; float:right; text-align:center;' class='hintWhite'>Error in adding user with id ".$user_id." as feature.</div>";			break;											
				default:	
						break;
			}	
			if($flag_i)
			{
				$obj=new Features();
				if($obj->addFeature($feature_category,$user_id))
					$msg="<div style='background:#CCCCCC; padding:5px; float:right; text-align:center;' class='hintWhite'>Feature has been added.</div>";
				else
					$msg="<div style='background:#CC0000; padding:5px; float:right; text-align:center;' class='hintWhite'>Error in adding feature.</div>";
			}
			
			//----------------------------------------FEATURE PAGE: TYPE------------------------------------------------//
			
			switch($row['user_type'])
			{
				case 'artist':$objUser1=new User();
					$parent=$objUser1->getUserInfo($user_id);
					if($objUser1->markAsFeaturedType('artist',$user_id,$type))
						$flag_t=1;
					else
						$msg="<div style='background:#CC0000; padding:5px; float:right; text-align:center;' class='hintWhite'>Error in adding user with id ".$user_id." as feature.</div>";	
					break;
					case 'community':$objUser1=new User();
						$parent=$objUser1->getUserInfo($user_id);
						if($objUser1->markAsFeaturedType('community',$user_id,$type))
							$flag_t=1;
						else
							$msg="<div style='background:#CC0000; padding:5px; float:right; text-align:center;' class='hintWhite'>Error in adding user with id ".$user_id." as feature.</div>";	
						break;											
					default:	break;
			}	
				
			if($flag_t!=NULL)
			{
				$obj=new TypeFeatures();
				if($obj->addFeature($feature_category,$user_id,$type))
					$msg="<div style='background:#CCCCCC; padding:5px; float:right; text-align:center;' class='hintWhite'>Feature has been added.</div>";
				else
					$msg="<div style='background:#CC0000; padding:5px; float:right; text-align:center;' class='hintWhite'>Error in adding feature.</div>";
			}
										
	
			//----------------------------------------FEATURE PAGE: SUBTYPE------------------------------------------------//
			
							switch($row['user_type'])
							{
								case 'artist':$objUser1=new User();
											$parent=$objUser1->getUserInfo($user_id);
											if($objUser1->markAsFeaturedSubType('artist',$user_id,$type,$subtype))
												$flag_st=1;
											else
												$msg="<div style='background:#CC0000; padding:5px; float:right; text-align:center;' class='hintWhite'>Error in adding user with id ".$user_id." as feature.</div>";	
											break;
								case 'community':$objUser1=new User();
											$parent=$objUser1->getUserInfo($user_id);
											if($objUser1->markAsFeaturedSubType('community',$user_id,$type,$subtype))
												$flag_st=1;
											else
												$msg="<div style='background:#CC0000; padding:5px; float:right; text-align:center;' class='hintWhite'>Error in adding user with id ".$user_id." as feature.</div>";	
											break;											
								default:	break;
							}	
					
							if($flag_st)
							{
								$obj=new SubTypeFeatures();
								if($obj->addFeature($feature_category,$user_id,$type,$subtype))
									$msg="<div style='background:#CCCCCC; padding:5px; float:right; text-align:center;' class='hintWhite'>Feature has been added.</div>";
								else
									$msg="<div style='background:#CC0000; padding:5px; float:right; text-align:center;' class='hintWhite'>Error in adding feature.</div>";
							}
							
						
	}

	header('location:'.$url);
?>