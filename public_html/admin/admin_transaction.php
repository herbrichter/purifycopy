<?php
	include_once('includes/header.php');
	include_once('classes/Admin_Transactions.php');
	$trans_obj = new Admin_Transactions();
	
/*	include_once('../classes/UserType.php');
	include_once('../classes/Type.php');
	include_once('../classes/SubType.php');
	include_once('../classes/Subscription.php');
	include_once('../classes/UserSubscription.php');									
*/	
?>
<style>
/* This rule is read by Galleria to define the gallery height: */
    .galleria-class{height:320px}
</style>
<link href="../galleryfiles/gallery.css" rel="stylesheet"/>
<script src="../galleryfiles/jquery.easing.1.3.js"></script>
<!-- <script src="./galleria/galleria-1.2.7.min.js"></script> -->
<script src="../javascripts/swfobject.js"></script>
<script src='http://ajax.googleapis.com/ajax/libs/jquery/1.6.1/jquery.min.js'></script>
<script src="javascripts/jquery.js"></script>
<script>
	function handleSelection(choice)
	{
		//document.getElementById('select').disabled=true;
		if(choice=="total")
		{
			$("#"+choice).css({"display":"block"});
			$("#salesLog").css({"display":"none"});
			$("#payments").css({"display":"none"});
			$("#monthly_totals").css({"display":"none"});
			$("#annual_totals").css({"display":"none"});
			//$().style.display="none";
			//$().style.display="none";
		}
		else if(choice=="payments")
		{
			$("#"+choice).css({"display":"block"});
			$("#salesLog").css({"display":"none"});
			$("#total").css({"display":"none"});
			$("#monthly_totals").css({"display":"none"});
			$("#annual_totals").css({"display":"none"});
		}
		else if(choice=="monthly_totals")
		{
			$("#"+choice).css({"display":"block"});
			$("#salesLog").css({"display":"none"});
			$("#total").css({"display":"none"});
			$("#payments").css({"display":"none"});
			$("#annual_totals").css({"display":"none"});
		}
		else if(choice=="annual_totals")
		{
			$("#"+choice).css({"display":"block"});
			$("#salesLog").css({"display":"none"});
			$("#total").css({"display":"none"});
			$("#payments").css({"display":"none"});
			$("#monthly_totals").css({"display":"none"});
		}
		else
		{
			$("#"+choice).css({"display":"block"});
			$("#payments").css({"display":"none"});
			$("#total").css({"display":"none"});
			$("#monthly_totals").css({"display":"none"});
			$("#annual_totals").css({"display":"none"});
		}
	}
	
	function confirm_dels(val)
	{
		if(val==1)
		{
			var r = confirm("Are you sure you want to delete this user?")
			if (r==false)
			{
				return false;
			}
		}
		else if(val==2)
		{
			var r = confirm("Are you sure you want to resend verification link to this user?")
			if (r==false)
			{
				return false;
			}
		}
	}
</script>
  <script>
var ContentHeight = 211;
var TimeToSlide = 150.0;

var openAccordion = '';

function runAccordion(index)
{
  var nID = "Accordion" + index + "Content";
  if(openAccordion == nID)
    nID = '';
    
  setTimeout("animate(" + new Date().getTime() + "," + TimeToSlide + ",'" + openAccordion + "','" + nID + "')", 33);
  
  openAccordion = nID;
}

function animate(lastTick, timeLeft, closingId, openingId)
{  
  var curTick = new Date().getTime();
  var elapsedTicks = curTick - lastTick;
  
  var opening = (openingId == '') ? null : document.getElementById(openingId);
  var closing = (closingId == '') ? null : document.getElementById(closingId);
 
  if(timeLeft <= elapsedTicks)
  {
    if(opening != null)
      opening.style.height = ContentHeight + 'px';
    
    if(closing != null)
    {
      closing.style.display = 'none';
      closing.style.height = '0px';
    }
    return;
  }
 
  timeLeft -= elapsedTicks;
  var newClosedHeight = Math.round((timeLeft/TimeToSlide) * ContentHeight);

  if(opening != null)
  {
    if(opening.style.display != 'block')
      opening.style.display = 'block';
    opening.style.height = (ContentHeight - newClosedHeight) + 'px';
  }
  
  if(closing != null)
    closing.style.height = newClosedHeight + 'px';

  setTimeout("animate(" + curTick + "," + timeLeft +",'" + closingId + "','" + openingId + "')", 33);
}

var ContentHeight_sale = 218;
var TimeToSlide = 150.0;

var openAccordion = '';

function runAccordion_sale(index)
{
  var nID = "Accordion" + index + "Content";
  if(openAccordion == nID)
    nID = '';
    
  setTimeout("animate_sale(" + new Date().getTime() + "," + TimeToSlide + ",'" + openAccordion + "','" + nID + "')", 33);
  
  openAccordion = nID;
}

function animate_sale(lastTick, timeLeft, closingId, openingId)
{  
  var curTick = new Date().getTime();
  var elapsedTicks = curTick - lastTick;
  
  var opening = (openingId == '') ? null : document.getElementById(openingId);
  var closing = (closingId == '') ? null : document.getElementById(closingId);
 
  if(timeLeft <= elapsedTicks)
  {
    if(opening != null)
      opening.style.height = ContentHeight_sale + 'px';
    
    if(closing != null)
    {
      closing.style.display = 'none';
      closing.style.height = '0px';
    }
    return;
  }
 
  timeLeft -= elapsedTicks;
  var newClosedHeight = Math.round((timeLeft/TimeToSlide) * ContentHeight_sale);

  if(opening != null)
  {
    if(opening.style.display != 'block')
      opening.style.display = 'block';
    opening.style.height = (ContentHeight_sale - newClosedHeight) + 'px';
  }
  
  if(closing != null)
    closing.style.height = newClosedHeight + 'px';

  setTimeout("animate_sale(" + curTick + "," + timeLeft +",'" + closingId + "','" + openingId + "')", 33);
}

function confirm_delete(){
	var x = confirm("Are you sure you want to remove this transaction?");
	if(x==false){
		return false;
	}
}
</script>
  <div id="contentContainer">
  <?php 
  
  ?>
    <div id="subNavigation">
   <?php 
  	include_once('admin_leftnavigarion.php'); ?>   
    </div>
	
	<div id="actualContent">
    	<h1>Transactions</h1>
                                    <div class="fieldCont">
								<a title="Request transaction download." href="exports_scvs_for_admin.php" id="req_pay" style="border-radius:5px; -webkit-border-radius:5px; -moz-border-radius:5px; -ms-border-radius:5px; -o-border-radius:5px; cursor: pointer; text-decoration:none; background-color: #000000; border: 0 none; color: #FFFFFF; float: left; font-family: 'futura-pt',sans-serif; font-size: 0.825em; font-weight: bold; margin-right: 10px; padding: 6px 10px;">Download Sales Data</a>
                            </div>
		<div class="manageCont">
			<div class="topLinks">
				<div class="links" style="float:left;">
					<select class="dropdown" onChange="handleSelection(value)" style="margin-left:0;">
						<option value="salesLog">Sales Log</option>
						<option value="total">Totals</option>
						<option value="payments">Payments</option>
						<option value="monthly_totals">Monthly Totals</option>
						<option value="annual_totals">Annual Totals</option>
					</select>
					<!--<ul>
						<li>|</li>
						<li>Total Payments Received - $2,500 since 2012</li>
					</ul>-->
				</div>
			</div>
		</div>
		<div class="manageCont">
            <!--<div style="width:665px; float:left;">
                <div id="mediaContent">
					<div class="titleCont">-->
						<div id="salesLog">        
							<div class="titleCont">
								<div class="blkC" style="width:40px; padding: 0px 0 0 10px;">#</div>
								<div class="blkE" style="width:205px;">Seller</div>
								<div class="blkG">Profits</div>
								<div class="blkG">Type</div>
								<div class="blkF" style="text-align: center;">Date</div>    
							</div>
							<?php
										$no = 1;
										$pay_pal = 0;
										$all_crets = "";
										$act_months = array();
										$act_years = array();
										$all_stores = array();
										$meds_buy = array();
										$fans_buy = array();
										$all_donate = array();
										$month_fees_data = array();
										$month_fees_media = array();
										$month_fees_mems = array();
										$month_fees_data_year = array();
										$month_fees_fan = array();
										$year_fees_media = array();
										$year_fees_mems = array();
										$year_fees_fan = array();
										$donate_field = array();
										$profit_ofcs = "";
										$total_profit_ofcs = "";
										
										$whole_profit = 0;
										$all_sales = $trans_obj->get_all_trans();
										if(mysql_num_rows($all_sales)>0)
										{
											$no = 1;
											
											$meds_don = 0;
											while($rows = mysql_fetch_assoc($all_sales))
											{
												$total_profit = 0;
												$total_profit_do = 0;
												$exp_fee = explode(',',$rows['paypal_fee']);
												/* $total_trnc = ($rows['total'] * 0.05) + 0.05;
												$Paypal_Fee = round($total_trnc); */
												if($rows['total']!=0)
												{
													if($rows['price']!=0)
													{
														$purify_fee = round(($rows['price'] - $exp_fee[0]) * 0.05, 2);
														/* if($purify_fee<=0)
														{
															$purify_fee = 1;
														} */
														
														$total_profit = $rows['price'] - $exp_fee[0] - $purify_fee;
													}	
													$buy = $trans_obj->get_buyer($rows['buyer_id']);
													
													$get_media = $trans_obj->get_media($rows['media_id']);
													if(mysql_num_rows($get_media)>0)
													{
														if($rows['price']!=0)
														{
															$meds_buy[$meds_don] = $rows;
														}
														if($rows['donate']!=0)
														{
															
															$purify_fee = round(($rows['donate'] - $exp_fee[1]) * 0.05, 2);
															$total_profit_do = $rows['donate'] - $exp_fee[1] - $purify_fee;
															
															$donate_field[$meds_don] = $rows;
															$donate_field[$meds_don]['donate_field'] = 'yes';
														}
														$whole_profit = $whole_profit + $total_profit + $total_profit_do;
														$meds_don = $meds_don + 1;
													}
												}
											}
										}
										
										$all_fan_sales = $trans_obj->get_all_fan_trans();
										if(mysql_num_rows($all_fan_sales)>0)
										{
											$no = 1;
											$fan_don = $meds_don;
											
											while($rows = mysql_fetch_assoc($all_fan_sales))
											{
												$total_profit_do = 0;
												/* $total_trnc = ($rows['total'] * 0.05) + 0.05;
												$Paypal_Fee = round($total_trnc); */
												
												$exp_fee = explode(',',$rows['paypal_fee']);
									
												$purify_fee = round(($rows['price'] - $exp_fee[0]) * 0.05, 2);
												$total_profit = $rows['price'] - $exp_fee[0] - $purify_fee;
												//$total_profit = $rows['total'] - $rows['paypal_fee'] - $purify_fee;
												
												//$buy = $trans->get_buyer($rows['buyer_id']);
												
												//$get_media = $trans->get_media($rows['media_id']);
												//if(mysql_num_rows($get_media)>0)
												//{
													
													$fans_buy[$fan_don] = $rows;
													if($rows['donate']!=0)
													{
														$purify_fee = round(($rows['donate'] - $exp_fee[1]) * 0.05, 2);
														$total_profit_do = $rows['donate'] - $exp_fee[1] - $purify_fee;
															
														$donate_field[$fan_don] = $rows;
														$donate_field[$fan_don]['donate_field'] = 'yes';
													}
												//}
												$whole_profit = $whole_profit + $total_profit + $total_profit_do;
												$fan_don = $fan_don + 1;
												//}
											}
										}
										
										$sql_donate = $trans_obj->get_all_donate();
										if(mysql_num_rows($sql_donate)>0)
										{
											$no = 1;
											
											while($rows = mysql_fetch_assoc($sql_donate))
											{
												/* $total_trnc = ($rows['total'] * 0.05) + 0.05;
												$Paypal_Fee = round($total_trnc); */
												
												/* $purify_fee = round(($rows['total'] - $rows['paypal_fee']) * 0.05);
												if($purify_fee<=0)
												{
													$purify_fee = 1;
												} */
												
												$total_profit = $rows['amount'] - $rows['paypal_fee'];
												
												//$buy = $trans->get_buyer($rows['buyer_id']);
												
												//$get_media = $trans->get_media($rows['media_id']);
												//if(mysql_num_rows($get_media)>0)
												//{
													
													$whole_profit = $whole_profit + $total_profit;
													$all_donate[] = $rows;
												//}
											}
										}
										
										$get_purify_mems = array();
										$get_purify_mems = $trans_obj->get_all_purify_members();
										
										$chekcs_payments = array();
										$chekcs_payments = $trans_obj->checks_payment();
										
										$all_stores = array_merge($meds_buy,$fans_buy,$all_donate,$donate_field,$get_purify_mems,$chekcs_payments);
										
										$sort_able = array();
										foreach($all_stores as $k=>$v)
										{
											//if(isset($v['name']) && $v['name']!="")
											//{
											if(isset($v['chk_date']) && $v['chk_date']!="")
											{
												$sort_able['chk_date_ables'][$k] = strtotime($v['chk_date'].' '.$v['time_chk']);
												//$sort['chk_date_ables'][$k] = date('Y-m-d H:i:s');
											}
											elseif(isset($v['date']) && $v['date']!="")
											{
												$sort_able['chk_date_ables'][$k] = strtotime($v['date'].' '.$v['time_chk']);
												//$sort['chk_date_ables'][$k] = date('Y-m-d H:i:s');
											}
											elseif(isset($v['purchase_date']) && $v['purchase_date']!="")
											{
												$sort_able['chk_date_ables'][$k] = strtotime($v['purchase_date']);
											}
											elseif(isset($v['date_sent']) && $v['date_sent']!="")
											{
												$sort_able['chk_date_ables'][$k] = strtotime($v['date_sent'].' '.$v['time_sent']);
											}
											//}									
										}
										array_multisort($sort_able['chk_date_ables'], SORT_DESC, $all_stores);
										
										$hasing = count($all_stores);
										
										for($se=0;$se<count($all_stores);$se++)
										{
											
											//while($rows = mysql_fetch_assoc($all_sales))
											//{
												//$total_trnc = ($rows['total'] * 0.05) + 0.05;
												//$Paypal_Fee = round($total_trnc);
												$exp_fee = explode(',',$all_stores[$se]['paypal_fee']);
												
												if(isset($all_stores[$se]['cost']))
												{
													$purify_fee = 0;
													$total_profit = $all_stores[$se]['cost'] - $all_stores[$se]['purify_fee'] - $purify_fee;
												}
												elseif(isset($all_stores[$se]['date_sent']))
												{
													$purify_fee = 0;
													$total_profit = 10;
												}
												else
												{
													if(!isset($all_stores[$se]['amount']))
													{
														$purify_fee = round(($all_stores[$se]['price'] - $exp_fee[0]) * 0.05, 2);
													
														$purify_fee_2 = round(($all_stores[$se]['donate'] - $exp_fee[1]) * 0.05, 2);
														
														if(isset($all_stores[$se]['donate_field']))
														{
															$total_profit = $all_stores[$se]['donate'] - $purify_fee_2 - $exp_fee[1];
														}
														else
														{
															$total_profit = $all_stores[$se]['price'] - $exp_fee[0] - $purify_fee;
														}
													}
													else
													{
														$purify_fee = 0;
														$total_profit = $all_stores[$se]['amount'] - $all_stores[$se]['paypal_fee'] - $purify_fee;
													}
												}
												
												if(isset($all_stores[$se]['cost']))
												{
													$buy = $trans_obj->get_buyer($all_stores[$se]['general_user_id']);
												}
												elseif(isset($all_stores[$se]['date_sent']))
												{
													$buy = $trans_obj->get_buyer_mail($all_stores[$se]['login_email']);
												}
												else
												{
													$buy = $trans_obj->get_buyer($all_stores[$se]['buyer_id']);
												}
												
												if(isset($all_stores[$se]['media_id']))
												{
													$get_media = $trans_obj->get_media($all_stores[$se]['media_id']);
													if(mysql_num_rows($get_media)>0)
													{
														$gets_meds = mysql_fetch_assoc($get_media);
														$get_creator = $trans_obj->get_creators($gets_meds['creator_info']);
													}
												}
												else
												{
													
													$gets_meds = "";
													if(isset($all_stores[$se]['table_name'])){
														if($all_stores[$se]['table_name']=='artist_project' || $all_stores[$se]['table_name']=='community_project')
														{
															
															$vars = $all_stores[$se]['table_name'].'|'.$all_stores[$se]['type_id'];
															
															$get_vars = $trans_obj->get_creators($vars);
															$get_creator = $trans_obj->get_creators($get_vars['creators_info']);
														}
														elseif($all_stores[$se]['table_name']=='general_artist' || $all_stores[$se]['table_name']=='general_community')
														{
															$vars = $all_stores[$se]['table_name'].'|'.$all_stores[$se]['type_id'];
															$get_creator = $trans_obj->get_creators($vars);
														}
													}
												}
											
										if($total_profit!=0)
										{
									?>
											<div class="tableCont" id="AccordionContainer">
												<div class="blkC" style="padding: 0px 0 0 10px;"><?php echo $hasing; ?></div>
												<div class="blkE" style="width:205px;"><?php 
												if(isset($all_stores[$se]['total']))
												{
													if($get_creator!="")
													{
														if(isset($get_creator['name']))
														{
															echo $get_creator['name'];
														}
														elseif(isset($get_creator['title']))
														{
															echo $get_creator['title'];
														}
													}else{ ?>&nbsp;<?php }
												}else{ echo "Purify Art"; }
													?></div>
												<div class="blkG" style="width:80px;">$<?php echo $total_profit; ?></div>
												<div class="blkG"><?php
												if(isset($all_stores[$se]['cost']))
												{
													echo "Purify Membership";
												}
												elseif(isset($all_stores[$se]['date_sent']))
												{
													echo "Check Fee";
												}
												else
												{
													if(is_array($gets_meds) && isset($gets_meds['media_type']) && $gets_meds['media_type']==114)
													{
														if(isset($all_stores[$se]['donate_field']))
														{
															echo "Tip";
														}
														else
														{
															echo "Song";
														}
													}
													else
													{
														if(!isset($all_stores[$se]['amount']))
														{
															if(isset($all_stores[$se]['donate_field']))
															{
																echo "Tip";
															}
															else
															{
																if(isset($all_stores[$se]['type_pro']))
																{
																	if($all_stores[$se]['type_pro']!="")
																	{
																		if($all_stores[$se]['type_pro']=='fan_club')
																		{
																			echo "Fan Club Membership";
																		}
																		elseif($all_stores[$se]['type_pro']=='for_sale')
																		{
																			echo "Project";
																		}
																		elseif($all_stores[$se]['type_pro']=='free_club')
																		{
																			echo "Free Project";
																		}
																	}
																	else
																	{
																		echo "Fan Club Membership";
																	}
																}
																else
																{
																	echo "Fan Club Membership";
																}
															}
														}
														else
														{
															echo "Donation";
														}
													}
												}
												?></div>
												<div class="blkF">
												<?php 
													if(isset($all_stores[$se]['cost']))
													{
														echo date("m.d.y", strtotime($all_stores[$se]['purchase_date']));
													}
													elseif(isset($all_stores[$se]['date_sent']))
													{
														echo date("m.d.y", strtotime($all_stores[$se]['date_sent']));
													}
													else
													{
														if(isset($all_stores[$se]['total']))
														{	 
															echo date("m.d.y", strtotime($all_stores[$se]['chk_date']));
														}
														else
														{
															echo date("m.d.y", strtotime($all_stores[$se]['date']));
														}
													}
												?>
												</div>
												<div class="blkC" onclick="runAccordion('<?php echo $no; ?>_pay');">
													<div onselectstart="return false;"><img id="trcn_info_<?php echo $no; ?>" onclick="lists_views();" src="images/view.jpg" /></div>
												</div>
												<div class="blkD" style="padding:5px; width:35px;"><a href="
												<?php
													if(isset($all_stores[$se]['cost']))
													{
														
													}
													elseif(isset($all_stores[$se]['date_sent']))
													{
														echo "remove_transaction.php?id=".$all_stores[$se]['id']."&type=check_fee";
													}
													else
													{
														if(isset($all_stores[$se]['total']))
														{	 
															if($all_stores[$se]['type_pro']==""){ $mine_type = "song"; }else { $mine_type = $all_stores[$se]['type_pro']; }
															echo "remove_transaction.php?id=".$all_stores[$se]['id']."&type=".$mine_type;
														}
														else
														{
															echo "remove_transaction.php?id=".$all_stores[$se]['id']."&type=donation";
														}
													}
												?>" onclick="return confirm_delete()" ><img src="images/delete.png" /></a></div>
												<div class="expandable" id="Accordion<?php echo $no; ?>_payContent" style="
												background: none repeat scroll 0 0 #F3F3F3; height: 215px !important; margin-left: 50px;">
													<div class="affiBlock">
														<div class="RowCont">
															<div class="sideL">Item Title :</div>
															<div class="sideR"><?php 
															if(isset($all_stores[$se]['total']))
															{
																if(isset($gets_meds))
																{
																	if($gets_meds!="")
																	{
																		echo $gets_meds['title'];
																	}
																	else
																	{
																		if($all_stores[$se]['table_name']=='artist_project' || $all_stores[$se]['table_name']=='community_project')
																		{
																			echo $get_vars['title'];
																		}
																		else
																		{
																			if(isset($get_creator['name']))
																			{
																				echo $get_creator['name'];
																			}
																			elseif(isset($get_creator['title']))
																			{
																				echo $get_creator['title'];
																			}
																		} 
																	}
																}
																else
																{
																	
																	if($all_stores[$se]['table_name']=='artist_project' || $all_stores[$se]['table_name']=='community_project')
																	{
																		echo $get_vars['title'];
																	}
																	else
																	{
																		if(isset($get_creator['name']))
																		{
																			echo $get_creator['name'];
																		}
																		elseif(isset($get_creator['title']))
																		{
																			echo $get_creator['title'];
																		}
																	} 
																}
															}elseif(isset($all_stores[$se]['date_sent'])){ echo "Check Fee"; }elseif(isset($all_stores[$se]['amount'])){ echo "Donation To Purify"; }elseif(isset($all_stores[$se]['cost'])){ echo "Purify Membership"; }else{ ?>&nbsp;<?php } ?></div>
														</div>
														<div class="RowCont">
															<div class="sideL">Name :</div>
															<div class="sideR"><?php
															if(isset($all_stores[$se]['IP_address']))
															{
																if($all_stores[$se]['IP_address']!=NULL)
																{
																	echo $all_stores[$se]['buy_name'].' '.$all_stores[$se]['buy_lname'];
																}
																else
																{
																	echo $buy['fname'].' '.$buy['lname'];
																}
															}
															elseif(isset($all_stores[$se]['date_sent']))
															{
																echo $buy['fname'].' '.$buy['lname'];
															}
															elseif(isset($all_stores[$se]['amount']))
															{
																if($all_stores[$se]['general_user_id']!=0)
																{
																	$gen_data = $trans_obj->get_gen_id($all_stores[$se]['general_user_id']);
																	echo $gen_data['fname'].' '.$gen_data['lname'];
																}
																elseif($all_stores[$se]['general_user_id']==0 && $all_stores[$se]['buy_name']=="")
																{
																	$get_data = $trans_obj->get_donation_details($all_stores[$se]['cart_rel_ids']);
																	echo $get_data['buy_name'];
																}
																else
																{
																	echo $all_stores[$se]['buy_name'];
																}
															}
															else{ echo $buy['fname'].' '.$buy['lname']; } ?></div>
														</div>
														<div class="RowCont">
															<div class="sideL">Email :</div>
															<div class="sideR"><?php
															if(isset($all_stores[$se]['IP_address']))
															{
																
																if($all_stores[$se]['IP_address']!=NULL)
																{
																	echo $all_stores[$se]['buy_email'];
																}
																else
																{
																	echo $buy['email'];
																}
															}
															elseif(isset($all_stores[$se]['date_sent']))
															{
																echo $buy['email'];
															}
															elseif(isset($all_stores[$se]['amount']))
															{
																if($all_stores[$se]['general_user_id']!=0)
																{
																	$gen_data = $trans_obj->get_gen_id($all_stores[$se]['general_user_id']);
																	echo $gen_data['email'];
																}
																elseif($all_stores[$se]['general_user_id']==0 && $all_stores[$se]['buy_email']=="")
																{
																	$get_data = $trans_obj->get_donation_details($all_stores[$se]['cart_rel_ids']);
																	echo $get_data['buy_email'];
																}
																else
																{
																	echo $all_stores[$se]['buy_email'];
																}
															}
															else
															{ echo $buy['email']; } ?></div>
														</div>
														<div class="RowCont">
															<div class="sideL">Amount :</div>
															<div class="sideR">$<?php 
															if(isset($all_stores[$se]['date_sent']))
															{
																echo "10";
															}
															elseif(isset($all_stores[$se]['amount']))
															{
																echo $all_stores[$se]['amount'];
															}
															else
															{
																if(isset($all_stores[$se]['donate_field']))
																{
																	echo $all_stores[$se]['donate'];
																}
																elseif(isset($all_stores[$se]['cost']))
																{
																	echo $all_stores[$se]['cost'];
																}
																else
																{
																	echo $all_stores[$se]['price'];
																}
															}
															?></div>
														</div>
														<div class="RowCont">
															<div class="sideL">Paypal Fee :</div>
															<div class="sideR">$<?php 
															if(isset($all_stores[$se]['date_sent']))
															{
																echo "0";
															}
															elseif(isset($all_stores[$se]['amount']))
															{
																echo $all_stores[$se]['paypal_fee'];
															}
															else
															{
																if(isset($all_stores[$se]['donate_field']))
																{
																	echo $exp_fee[1];
																}
																elseif(isset($all_stores[$se]['cost']))
																{
																	echo $all_stores[$se]['purify_fee'];
																}
																else
																{
																	echo $exp_fee[0];
																}
															}
															?></div>
														</div>
														<div class="RowCont">
															<div class="sideL">Purify Fee :</div>
															<div class="sideR">$<?php
															if(isset($all_stores[$se]['date_sent']))
															{
																echo "0";
															}
															elseif(isset($all_stores[$se]['donate_field'])) { echo $purify_fee_2; }else{ echo $purify_fee; } ?></div>
														</div>
														
														
														<div class="RowCont">
															<div class="sideL">Net Profit :</div>
															<div class="sideR">$<?php echo $total_profit; ?></div>
														</div>
														<!--<div class="RowCont">
															<div class="sideL">Payment :</div>
															<select class="fieldUpload">
																<option value="">Paypal</option>
																<option value="">Xoom</option>
																<option value="">Check</option>
															</select>
														</div>-->
													</div>
												</div>
											</div>
										<?php
										}
										if(isset($all_stores[$se]['media_id']))
										{
											$cart = 'cart';
											$cretr = $gets_meds['creator_info'];
										}
										else
										{
											if(!isset($all_stores[$se]['date_sent']))
											{ 
												$cart = 'all';
												if(isset($all_stores[$se]['table_name'])){
													if($all_stores[$se]['table_name']=='artist_project' || $all_stores[$se]['table_name']=='community_project')
													{
														$cretr = $get_vars['creators_info'];
													}
													elseif($all_stores[$se]['table_name']=='general_artist' || $all_stores[$se]['table_name']=='general_community')
													{
														$cretr = $get_creator['creators_info'];
													}
												}
											}
										}
										if(isset($all_stores[$se]['total']) && !isset($all_stores[$se]['donate_field']))
										{
											$all_crets = $all_crets.','.$cretr.'~'.$all_stores[$se]['id'].'*'.$cart.'@'.$total_profit;
										}
										$no = $no + 1;
												//}
											//}
										
										if(isset($all_stores[$se]['chk_date']) || isset($all_stores[$se]['cost']) || isset($all_stores[$se]['cart_rel_ids']))
										{
											if(isset($all_stores[$se]['chk_date']))
											{												
												$curr_year = date("Y",strtotime($all_stores[$se]['chk_date']));
												$curr_mon = date("m",strtotime($all_stores[$se]['chk_date'])).$curr_year;	
											}
											elseif(isset($all_stores[$se]['cost']))
											{												
												$curr_year = date("Y",strtotime($all_stores[$se]['purchase_date']));
												$curr_mon = date("m",strtotime($all_stores[$se]['purchase_date'])).$curr_year;	
											}
											elseif(isset($all_stores[$se]['cart_rel_ids']))
											{												
												$curr_year = date("Y",strtotime($all_stores[$se]['date']));
												$curr_mon = date("m",strtotime($all_stores[$se]['date'])).$curr_year;	
											}
											
											if(in_array($curr_mon, $act_months))
											{
												if(isset($all_stores[$se]['donate_field'])) 
												{
													$month_fees_data[$curr_mon]['payapl_fee'] = $month_fees_data[$curr_mon]['payapl_fee'] + $exp_fee[1];
													
													$month_fees_data[$curr_mon]['purify_fes'] =$month_fees_data[$curr_mon]['purify_fes'] + $purify_fee_2;
													
													$month_fees_data[$curr_mon]['user_profs'] =$month_fees_data[$curr_mon]['user_profs'] + $total_profit;
												}
												elseif(isset($all_stores[$se]['cost'])) 
												{
													$month_fees_data[$curr_mon]['payapl_fee'] = $month_fees_data[$curr_mon]['payapl_fee'] + $all_stores[$se]['purify_fee'];
													
													$month_fees_mems[$curr_mon]['mems_profs'] =$month_fees_mems[$curr_mon]['mems_profs'] +$total_profit;
													
													if($month[$mon]=='09' && $year_name_mon[$mon]=='2013') {
															echo $all_stores[$se]['purify_fee'];
															echo $total_profit;
												}
													
													/* $month_fees_data[$curr_mon]['purify_fes'] =$month_fees_data[$curr_mon]['purify_fes'] + $purify_fee; */
													
													/* $month_fees_data[$curr_mon]['user_profs'] =$month_fees_data[$curr_mon]['user_profs'] + $total_profit; */
												}
												elseif(isset($all_stores[$se]['cart_rel_ids']))
												{
													$month_fees_data[$curr_mon]['payapl_fee'] = $month_fees_data[$curr_mon]['payapl_fee'] + $all_stores[$se]['paypal_fee'];
												}
												else
												{ 
													$month_fees_data[$curr_mon]['payapl_fee'] = $month_fees_data[$curr_mon]['payapl_fee'] + $exp_fee[0];
													
													$month_fees_data[$curr_mon]['purify_fes'] =$month_fees_data[$curr_mon]['purify_fes'] +$purify_fee;
													
													$month_fees_data[$curr_mon]['user_profs'] =$month_fees_data[$curr_mon]['user_profs'] +$total_profit;
													
													if(isset($all_stores[$se]['media_id']))
													{
														
														$month_fees_media[$curr_mon]['media_profs'] =$month_fees_media[$curr_mon]['media_profs'] +$total_profit;
													}
													elseif(isset($all_stores[$se]['type_pro']))
													{
														/*Code Added by Azhar for displaying only fan club total Starts here
														$all_onlyfanclud_sales = $trans_obj->get_all_onlyfanclub_trans();
														if(mysql_num_rows($all_onlyfanclud_sales)>0)
														{
															while($rowsofc = mysql_fetch_assoc($all_onlyfanclud_sales))
															{
																$exp_fee = explode(',',$rowsofc['paypal_fee']);
																$purify_fee = round(($rowsofc['price'] - $exp_fee[0]) * 0.05, 2);
																$total_profit = $rowsofc['price'] - $exp_fee[0] - $purify_fee;
																$profit_ofcs = $profit_ofcs + $total_profit_ofcs;
															}
														}
														/*Code Added by Azhar for displaying only fan club total ends here*/
														$month_fees_fan[$curr_mon]['fan_profs'] =$month_fees_fan[$curr_mon]['fan_profs'] +$total_profit; //Previously this line was this
														//$month_fees_fan[$curr_mon]['fan_profs'] =$month_fees_fan[$curr_mon]['fan_profs'] +$profit_ofcs;
													}
												}
											}
											else
											{
												if(isset($all_stores[$se]['donate_field'])) 
												{
													$month_fees_data[$curr_mon]['payapl_fee'] =$exp_fee[1];
													$month_fees_data[$curr_mon]['purify_fes'] =$purify_fee_2;
													$month_fees_data[$curr_mon]['user_profs'] =$total_profit;
													
												}
												elseif(isset($all_stores[$se]['cost'])) 
												{
													$month_fees_data[$curr_mon]['payapl_fee'] = $all_stores[$se]['purify_fee'];
													
													$month_fees_mems[$curr_mon]['mems_profs'] =$total_profit;
													if($month[$mon]=='09' && $year_name_mon[$mon]=='2013') {
															echo $all_stores[$se]['purify_fee'];
															echo $total_profit;
												}
													/* $month_fees_data[$curr_mon]['purify_fes'] =$month_fees_data[$curr_mon]['purify_fes'] + $purify_fee;
													
													$month_fees_data[$curr_mon]['user_profs'] =$month_fees_data[$curr_mon]['user_profs'] + $total_profit; */
												}
												elseif(isset($all_stores[$se]['cart_rel_ids']))
												{
													$month_fees_data[$curr_mon]['payapl_fee'] = $all_stores[$se]['paypal_fee'];
												}
												else
												{ 
													$month_fees_data[$curr_mon]['payapl_fee'] = $exp_fee[0];
													$month_fees_data[$curr_mon]['purify_fes'] =$purify_fee;
													$month_fees_data[$curr_mon]['user_profs'] =$total_profit;
													
													if(isset($all_stores[$se]['media_id']))
													{
														$month_fees_media[$curr_mon]['media_profs'] =$total_profit;
													}
													elseif(isset($all_stores[$se]['type_pro']))
													{
														$month_fees_fan[$curr_mon]['fan_profs'] =$total_profit;
													}
												}
												$act_months[] = $curr_mon;
											}
											
											if(in_array($curr_year, $act_years))
											{
												if(isset($all_stores[$se]['donate_field'])) 
												{
													$month_fees_data_year[$curr_year]['payapl_fee'] = $month_fees_data_year[$curr_year]['payapl_fee'] + $exp_fee[1];
													
													$month_fees_data_year[$curr_year]['purify_fes'] =$month_fees_data_year[$curr_year]['purify_fes'] + $purify_fee_2;
													
													$month_fees_data_year[$curr_year]['user_profs'] =$month_fees_data_year[$curr_year]['user_profs'] + $total_profit;
												}
												elseif(isset($all_stores[$se]['cost'])) 
												{
													$month_fees_data_year[$curr_year]['payapl_fee'] = $month_fees_data_year[$curr_year]['payapl_fee'] + $all_stores[$se]['purify_fee'];
													
													$year_fees_mems[$curr_year]['mems_profs'] =$year_fees_mems[$curr_year]['mems_profs'] +$total_profit;
													
													/* $month_fees_data[$curr_mon]['purify_fes'] =$month_fees_data[$curr_mon]['purify_fes'] + $purify_fee;
													
													$month_fees_data[$curr_mon]['user_profs'] =$month_fees_data[$curr_mon]['user_profs'] + $total_profit; */
												}
												elseif(isset($all_stores[$se]['cart_rel_ids']))
												{
													$month_fees_data_year[$curr_year]['payapl_fee'] = $month_fees_data_year[$curr_year]['payapl_fee'] + $all_stores[$se]['paypal_fee'];
												}
												else
												{ 
													$month_fees_data_year[$curr_year]['payapl_fee'] = $month_fees_data_year[$curr_year]['payapl_fee'] + $exp_fee[0];
													
													$month_fees_data_year[$curr_year]['purify_fes'] =$month_fees_data_year[$curr_year]['purify_fes'] +$purify_fee;
													
													$month_fees_data_year[$curr_year]['user_profs'] =$month_fees_data_year[$curr_year]['user_profs'] +$total_profit;
													
													if(isset($all_stores[$se]['media_id']))
													{
														$year_fees_media[$curr_year]['media_profs'] =$year_fees_media[$curr_year]['media_profs'] +$total_profit;
													}
													elseif(isset($all_stores[$se]['type_pro']))
													{
														/*Code Added by Azhar for displaying only fan club total Starts here
														$all_onlyfanclud_sales = $trans_obj->get_all_onlyfanclub_trans();
														if(mysql_num_rows($all_onlyfanclud_sales)>0)
														{
															while($rowsofc = mysql_fetch_assoc($all_onlyfanclud_sales))
															{
																$exp_feeofc = explode(',',$rowsofc['paypal_fee']);
																$purify_feeofc = round(($rowsofc['price'] - $exp_feeofc[0]) * 0.05, 2);
																$total_profit = $rowsofc['price'] - $exp_feeofc[0] - $purify_feeofc;
																$profit_ofcs = $profit_ofcs + $total_profit_ofcs;
															}
														}
														Code Added by Azhar for displaying only fan club total ends here*/
														$year_fees_fan[$curr_year]['fan_profs'] =$year_fees_fan[$curr_year]['fan_profs'] +$total_profit;  //Previously this line was this
														//$year_fees_fan[$curr_year]['fan_profs'] =$year_fees_fan[$curr_year]['fan_profs'] +$profit_ofcs;
													}
												}
											}
											else
											{
												if(isset($all_stores[$se]['donate_field'])) 
												{
													$month_fees_data_year[$curr_year]['payapl_fee'] =$exp_fee[1];
													$month_fees_data_year[$curr_year]['purify_fes'] =$purify_fee_2;
													$month_fees_data_year[$curr_year]['user_profs'] =$total_profit;
													
												}
												elseif(isset($all_stores[$se]['cost'])) 
												{
													$month_fees_data_year[$curr_year]['payapl_fee'] = $all_stores[$se]['purify_fee'];
													
													$year_fees_mems[$curr_year]['mems_profs'] =$total_profit;
													
													/* $month_fees_data[$curr_mon]['purify_fes'] =$month_fees_data[$curr_mon]['purify_fes'] + $purify_fee;
													
													$month_fees_data[$curr_mon]['user_profs'] =$month_fees_data[$curr_mon]['user_profs'] + $total_profit; */
												}
												elseif(isset($all_stores[$se]['cart_rel_ids']))
												{
													$month_fees_data_year[$curr_year]['payapl_fee'] = $all_stores[$se]['paypal_fee'];
												}
												else
												{ 
													$month_fees_data_year[$curr_year]['payapl_fee'] = $exp_fee[0];
													$month_fees_data_year[$curr_year]['purify_fes'] =$purify_fee;
													$month_fees_data_year[$curr_year]['user_profs'] =$total_profit;
													
													if(isset($all_stores[$se]['media_id']))
													{
														$year_fees_media[$curr_year]['media_profs'] =$total_profit;
													}
													elseif(isset($all_stores[$se]['type_pro']))
													{
														$year_fees_fan[$curr_year]['fan_profs'] =$total_profit;
													}
												}
												$act_years[] = $curr_year;
											}
										}
										$hasing = $hasing - 1;
									}
									/*Code added by azhar for media transaction starts here*/
									$get_project_sales = $trans_obj->get_all_trans_projectsale();
									$total_profit_all_med = "";
									if(mysql_num_rows($get_project_sales)>0)
									{
										while($rows = mysql_fetch_assoc($get_project_sales))
										{
											$exp_fee = explode(',',$rows['paypal_fee']);
											if($rows['total']!=0)
											{
												if($rows['price']!=0)
												{
													$purify_fee = round(($rows['price'] - $exp_fee[0]) * 0.05, 2);
													$get_total_profit = $rows['price'] - $exp_fee[0] - $purify_fee;
													$total_profit_all_med = $total_profit_all_med + $get_total_profit;
												}	
												//echo $get_total_profit."</br>";
											}
										}
										//$month_fees_media[$curr_mon]['media_profs'] = $month_fees_media[$curr_mon]['media_profs'] + $total_profit_all_med;
										$year_fees_media[$curr_year]['media_profs'] = $year_fees_media[$curr_year]['media_profs'] + $total_profit_all_med;
									}
									/*Code added by azhar for media transaction ends here*/
									
									/*Code Added by Azhar for displaying only fan club total Starts here*/
									$all_onlyfanclud_sales = $trans_obj->get_all_onlyfanclub_trans();
									$profit_ofcs_month ="";
									$month_fees_fan = array();
									if(mysql_num_rows($all_onlyfanclud_sales)>0)
									{
										while($rowsofc = mysql_fetch_assoc($all_onlyfanclud_sales))
										{
											$exp_fee_m_ofcs = explode(',',$rowsofc['paypal_fee']);
											$purify_fee_m_ofcs = round(($rowsofc['price'] - $exp_fee_m_ofcs[0]) * 0.05, 2);
											$total_profit_ofcs_month = $rowsofc['price'] - $exp_fee_m_ofcs[0] - $purify_fee_m_ofcs;
											$profit_ofcs_month = $profit_ofcs_month + $total_profit_ofcs_month;
										}
									}
									if($profit_ofcs_month>0){
										$month_fees_fan[$curr_mon]['fan_profs'] = $month_fees_fan[$curr_mon]['fan_profs'] + $profit_ofcs_month;
									}else{
										$month_fees_fan[$curr_mon]['fan_profs'] = "0";
									}
									/*Code Added by Azhar for displaying only fan club total ends here*/
									
									
									/*Code Added by Azhar for displaying only fan club total Starts here*/
									$all_onlyfanclud_sales = $trans_obj->get_all_onlyfanclub_trans();
									$profit_ofcs_year ="";
									if(mysql_num_rows($all_onlyfanclud_sales)>0)
									{
										while($rowsofc = mysql_fetch_assoc($all_onlyfanclud_sales))
										{
											$exp_feeofc = explode(',',$rowsofc['paypal_fee']);
											$purify_feeofc = round(($rowsofc['price'] - $exp_feeofc[0]) * 0.05, 2);
											$total_profit_ofcs = $rowsofc['price'] - $exp_feeofc[0] - $purify_feeofc;
											$profit_ofcs_year = $profit_ofcs_year + $total_profit_ofcs;
										}
									}
									if($profit_ofcs_year>0){
										$year_fees_fan[$curr_year]['fan_profs'] =$year_fees_fan[$curr_year]['fan_profs'] + $profit_ofcs_year;
									}else{
										$year_fees_fan[$curr_year]['fan_profs'] ="0";
									}
									
									/*Code Added by Azhar for displaying only fan club total ends here*/
									

								?>
                        </div>
						
                        <div id="total" style="display:none;">		
                            <div class="titleCont">
								<div class="blkD" style="text-align:center;">Year</div>
								<div class="blkG">Month</div>
								<div class="blkE">Total</div> 
								<div class="blkE">Creator</div>
								<div class="blkE">User</div> 								
							</div>
<?php
							$same = array();
									if($all_crets!="")
									{
										$exp_crets = explode(',',$all_crets);
										$st = 1;
										for($c=0;$c<count($exp_crets);$c++)
										{
											
											$exp_ids[$c] = explode('~',$exp_crets[$c]);
											$exp_chk_t[$c] = explode('*',$exp_ids[$c][1]);
											$exps_at_rates[$c] = explode('@',$exp_chk_t[$c][1]);
											
											if($exps_at_rates[$c][0]=='cart')
											{
												$get_trans = $trans_obj->get_trans($exp_chk_t[$c][0]);
											}
											elseif($exps_at_rates[$c][0]=='all')
											{
												$get_trans = $trans_obj->get_trans_fans($exp_chk_t[$c][0]);
											}
											
											if(isset($get_trans))
											{
												if($get_trans!=NULL)
												{
												if(mysql_num_rows($get_trans)>0)
												{ 
													$ans_trans[$c] = mysql_fetch_assoc($get_trans);
													if($exps_at_rates[$c][0]=='all')
													{
														if($exp_ids[$c][0]=='')
														{
															//echo "hii";
															$exp_ids[$c][0] = $ans_trans[$c]['table_name'].'|'.$ans_trans[$c]['type_id'];
															//echo $exp_ids[$c][0];
														}													
													}
													$mth[$c] = date("F",strtotime($ans_trans[$c]['chk_date']));
												
													$yr[$c] = date("Y",strtotime($ans_trans[$c]['chk_date']));
													
													if(isset($ans_trans[$c]['donate']))
													{
														if($ans_trans[$c]['donate']!="" && $ans_trans[$c]['donate']!=0)
														{
															$do_paypal_fee[$c] = explode(',',$ans_trans[$c]['paypal_fee']);
															$do_purify_fee[$c] = round(($ans_trans[$c]['donate'] - $do_paypal_fee[$c][1]) * 0.05, 2);
															$do_total_profit_do[$c] = $ans_trans[$c]['donate'] - $do_paypal_fee[$c][1] - $do_purify_fee[$c];
														}
														else
														{
															$do_total_profit_do[$c] = 0;
														}
													}
													else
													{
														$do_total_profit_do[$c] = 0;
													}
													
													$not_all = "";
													for($i=$c-1;$i>=0;$i--)
													{	
														if($exp_ids[$i][0]==$exp_ids[$c][0] && $mth[$c]==$mth[$i] && $yr[$i]==$yr[$c])
														{
															$not_all = $i;
															//echo $i;
														}	
													}
													if($not_all!="")
													{	
														//echo $not_all;
														$same[$not_all]['amt'] = $same[$not_all]['amt'] + $exps_at_rates[$c][1] + $do_total_profit_do[$c];	
														 
														$same[$c] = "";
													}
													else
													{	
														$same[$c]['creator'] = $exp_ids[$c][0];
														$same[$c]['mth'] = $mth[$c];
														$same[$c]['year'] = $yr[$c];
														$same[$c]['amt'] = $exps_at_rates[$c][1] + $do_total_profit_do[$c];
														$same[$c]['seller'] = $ans_trans[$c]['seller_id'];
													//	$st = $st + 1;
													}
												}
											}
											}
										}
										
										if(!empty($same))
										{
											//$same_new = array_unique($same);
											$dummy = array();
											for($r=1;$r<=count($same);$r++)
											{
												if(!empty($same[$r]))
												{
												//if(in_array($same[$r]['creator'],$dummy))
												//{}
												//else
												//{
													//$dummy[] = $same[$r]['creator'];
?>													<div class="tableCont">
														<div class="blkD"><?php echo $same[$r]['year']; ?></div>
														<div class="blkG"><?php echo $same[$r]['mth']; ?></div>
														<div class="blkE">$<?php echo $same[$r]['amt']; ?></div>
														<?php 
														$get_creator_name = $trans_obj->get_creators($same[$r]['creator']);
														if((isset($get_creator_name['name']) && $get_creator_name['name']!="") || $get_creator_name['title']!="")
														{
															if(isset($get_creator_name['name']) && $get_creator_name['name']!=""){
															?>
															<div class="blkE"><?php echo $get_creator_name['name']; ?></div>
															<?php
															}else{
															?>
															<div class="blkE"><?php echo $get_creator_name['title']; ?></div>
															<?php
															}
														}
														else
														{
														?> &nbsp; <?php
														}
														$get_act_name = $trans_obj->get_gen_id($same[$r]['seller']);
														?>
														<div class="blkE"><?php echo $get_act_name['fname'].' '.$get_act_name['lname']; ?></div>
													</div>
<?php
												}
											}
										}
									}
?>									
						</div>
						
						<div id="payments" style="display:none;">
							<div class="titleCont">
								<div class="blkD" style="width:100px;">Date Requested</div>
								<div class="blkB">Amount</div>
								<div class="blkD">Date Sent</div>
								<div class="blkE" style="width:214px;">User</div>
							</div>
							<?php
								$get_reqs = $trans_obj->get_requested();
								if(mysql_num_rows($get_reqs)>0)
								{
									while($orws = mysql_fetch_assoc($get_reqs))
									{
										$gen = $trans_obj->get_gen_user($orws['login_email']);
										if(mysql_num_rows($gen)>0)
										{
											$ans = mysql_fetch_assoc($gen);
							?>
											<div class="tableCont">
												<div class="blkD" style="width:100px;"><?php echo date("m.d.y", strtotime($orws['date_sent'])); ?></div>
												<div class="blkB"><?php echo '$'.$orws['amount']; ?>												</div>
																													
												<div class="blkD"><?php if($orws['date_received']!='0000-00-00') { echo date("m.d.y", strtotime($orws['date_received'])); }else { ?> &nbsp; <?php } ?></div>	
												
												<div class="blkE" style="width:214px;"><?php echo $ans['fname'].' '.$ans['lname']; ?></div>
												<?php
													if($orws['date_received']=='0000-00-00')
													{
												?>
														<div class="blkB"><a href="send_payment.php?id=<?php echo $orws['id']; ?>&amt=<?php echo $orws['amount']; ?>">Send Payment</a></div>
												<?php
													}
												?>
											</div>
							<?php
										}
									}
								}
							?>
						<!--</div>	
					</div>
				</div>-->
            </div>
			<div id="monthly_totals" style="display:none;">
				<?php
					$data_cart = array();
					$data_fan = array();
					$data_donate = array();
					$data_pmemeber = array();
					$data_checks_paymnt = array();
					
					$all_sales = $trans_obj->get_all_trans();
					$c_dump = 0;
					$c_act = 0;
					
					if(mysql_num_rows($all_sales)>0)
					{
						while($rows = mysql_fetch_assoc($all_sales))
						{
							$data_cart[] = $rows;
							if($rows['donate']!=0)
							{
								$data_cart[] = "";
							}
							/* $month[$c_dump]['months'] = date("F",strtotime($rows['chk_date']));
							if(in_array($month[$c_dump]['months'], $month_cart))
							{}
							else
							{								
								$act_month[$c_act]['months'] = $month[$c_dump]['months'];
								$get_total = $trans_obj->get_all_totals_cart();
								if($get_total!=NULL && $get_total!="")
								{
									if(mysql_num_rows($get_total)>0)
									{
										$rows = mysql_fetch_assoc($get_total);
										$act_month[$c_act]['totals'] = $rows['SUM( total )'];
									}
								}
								
								$c_act = $c_act + 1;
								$month_cart[] = $month[$c_dump]['months'];
							}
							$c_dump = $c_dump + 1; */
						}
					}
					
					$all_fan_sales = $trans_obj->get_all_fan_trans();
					if(mysql_num_rows($all_fan_sales)>0)
					{
						while($rows = mysql_fetch_assoc($all_fan_sales))
						{
							$data_fan[] = $rows;
							if($rows['donate']!=0)
							{
								$data_fan[] = "";
							}
							/* $month[$cart_i]['months'] = date("F",strtotime($rows['chk_date']));
							$month[$cart_i]['totals'] = $rows['total'];
							$cart_i = $cart_i + 1; */
						}
					}
					
					$sql_donate = $trans_obj->get_all_donate();
					if(mysql_num_rows($sql_donate)>0)
					{
						while($rows = mysql_fetch_assoc($sql_donate))
						{
							$data_donate[] = $rows;
							/* $month[$cart_i]['months'] = date("F",strtotime($rows['date']));
							$cart_i = $cart_i + 1; */
						}
					}
					
					$data_pmemeber = $trans_obj->get_all_purify_members();
					/* $sql_pmembership = $trans_obj->get_all_purify_member();
					if(mysql_num_rows($sql_pmembership)>0)
					{
						while($rows = mysql_fetch_assoc($sql_pmembership))
						{
							$data_pmemeber[] = $rows;
							/* $month[$cart_i]['months'] = date("F",strtotime($rows['date']));
							$cart_i = $cart_i + 1; 
						}
					} */
					
					$data_checks_paymnt = $trans_obj->checks_payment();
					
					$whole_data = array_merge($data_donate,$data_fan,$data_cart,$data_pmemeber,$data_checks_paymnt);
					foreach($whole_data as $k=>$v)
					{
						//if(isset($v['name']) && $v['name']!="")
						//{
						
						
						/*
						if(isset($v['chk_date']) && $v['chk_date']!="")
						{
							$sort['chk_date'][$k] = strtotime($v['chk_date'].' '.$v['time_chk']);
						}
						elseif(isset($v['date']) && $v['date']!="")
						{
							$sort['chk_date'][$k] = strtotime($v['date'].' '.$v['time_chk']);
						}
						elseif(isset($v['purchase_date']) && $v['purchase_date']!="")
						{
							$sort['chk_date'][$k] = strtotime($v['purchase_date']);
						}
						elseif(isset($v['date_sent']) && $v['date_sent']!="")
						{
							$sort['chk_date'][$k] = strtotime($v['date_sent'].' '.$v['time_sent']);
						}
						*/
						
						if(isset($v['chk_date']) && $v['chk_date']!="")
						{
							$llppoo[$k] = strtotime($v['chk_date']);
						}
						elseif(isset($v['date']) && $v['date']!="")
						{
							$llppoo[$k] = strtotime($v['date']);
						}
						elseif(isset($v['purchase_date']) && $v['purchase_date']!="")
						{
							$llppoo[$k] = strtotime($v['purchase_date']);
						}
						elseif(isset($v['date_sent']) && $v['date_sent']!="")
						{
							$llppoo[$k] = strtotime($v['date_sent']);
						}else{
							$llppoo[$k] ="";
						}
						
						//}									
					}
					array_multisort($llppoo, SORT_DESC, $whole_data);
				?>
					<div class="titleCont">
						<div class="blkE" >Month</div>
						<div class="blkD" style="width:64px;">Total Sales</div>
						<div class="blkE">Paypal Fees</div>
						<div class="blkE">User Profits</div>
						<div class="blkG">Purify Profits</div>
						<!--<div class="blkG" style="width:91px;">Purify Donations</div>-->
					</div>
				<?php
					$month_chk = array();
					$mon_year_dbhk = array();
					$year_mon_chk = array();
					$totals_all = 0;
					$total_cart = array();
					$total_fan = array();
					$total_don = array();
					$moth_name = array();
					$total_cart_tip = array();
					$total_fan_tip = array();
					$purify_mem = array();
					$year_name_mon = array();
					$check_pays = array();
					
					for($mon=0;$mon<count($whole_data);$mon++)
					{
						$total_cart[$mon] = 0;
						$total_fan[$mon] = 0;
						$total_don[$mon] = 0;
						$total_don_als[$mon] = 0;
						$total_cart_tip[$mon] = 0;
						$total_fan_tip[$mon] = 0;
						$purify_mem[$mon] = 0;
						$check_pays[$mon] = 0;
						
						if($whole_data[$mon]!="")
						{
							if(isset($whole_data[$mon]['chk_date']))
							{
								$month[$mon] = date("m",strtotime($whole_data[$mon]['chk_date']));
								$moth_name[$mon] = date("F",strtotime($whole_data[$mon]['chk_date']));
								
								$year_name_mon[$mon] = date("Y",strtotime($whole_data[$mon]['chk_date']));
							}
							elseif(isset($whole_data[$mon]['date']))
							{
								$month[$mon] = date("m",strtotime($whole_data[$mon]['date']));
								$moth_name[$mon] = date("F",strtotime($whole_data[$mon]['date']));
								
								$year_name_mon[$mon] = date("Y",strtotime($whole_data[$mon]['date']));
							}
							elseif(isset($whole_data[$mon]['purchase_date']))
							{
								$month[$mon] = date("m",strtotime($whole_data[$mon]['purchase_date']));
								$moth_name[$mon] = date("F",strtotime($whole_data[$mon]['purchase_date']));
								
								$year_name_mon[$mon] = date("Y",strtotime($whole_data[$mon]['purchase_date']));							
							}
							elseif(isset($whole_data[$mon]['date_sent']))
							{
								$month[$mon] = date("m",strtotime($whole_data[$mon]['date_sent']));
								$moth_name[$mon] = date("F",strtotime($whole_data[$mon]['date_sent']));
								
								$year_name_mon[$mon] = date("Y",strtotime($whole_data[$mon]['date_sent']));							
							}
						
							if(in_array($month[$mon], $month_chk) && in_array($month[$mon].'-'.$year_name_mon[$mon],$mon_year_dbhk))
							{
							}
							else
							{								
								if(in_array($year_name_mon[$mon], $year_mon_chk))
								//if(in_array($month[$mon].'-'.$year_name_mon[$mon],$mon_year_dbhk))
								{
									$month_chk[] = $month[$mon];
									$mon_year_dbhk[] = $month[$mon].'-'.$year_name_mon[$mon];
									$get_total_cart[$mon] = $trans_obj->get_all_totals_cart($month[$mon],$year_name_mon[$mon]);
									if($get_total_cart[$mon]!=NULL && $get_total_cart[$mon]!="")
									{
										if(mysql_num_rows($get_total_cart[$mon])>0)
										{
											$rows = mysql_fetch_assoc($get_total_cart[$mon]);
											$total_cart[$mon] = $rows['SUM( total )'];
											if($total_cart[$mon]==NULL)
											{
												$total_cart[$mon] = 0;
											}
										}
									}
									$get_tip_cart[$mon] = $trans_obj->get_all_tips_cart($month[$mon],$year_name_mon[$mon]);
									if($get_tip_cart[$mon]!=NULL && $get_tip_cart[$mon]!="")
									{
										if(mysql_num_rows($get_tip_cart[$mon])>0)
										{
											$pay_fee__ips = 0;
											$don_fee__ips = 0;
											$total_cart_tip[$mon] = "";
											$purify_fee = 0;
											$org_puriy_fee = 0;
											while($rows = mysql_fetch_assoc($get_tip_cart[$mon]))
											{
												$purify_fee = 0;
												$rem_paypal = explode(",",$rows['paypal_fee']);
												$pay_fee__ips = $rem_paypal[1] + $pay_fee__ips;
												$don_fee__ips = $rows['donate'] + $don_fee__ips;
												
												$purify_fee = round(($rows['donate'] - $rem_paypal[1]) * 0.05, 2);
												
												$org_puriy_fee = $purify_fee + $org_puriy_fee;
											}
											
											$total_cart_tip[$mon] = $don_fee__ips - $pay_fee__ips - $org_puriy_fee;
											
											if($total_cart_tip[$mon]==NULL)
											{
												$total_cart_tip[$mon] = 0;
											}
										}
									}
									
									$get_total_fan[$mon] = $trans_obj->get_all_totals_fan($month[$mon],$year_name_mon[$mon]);
									if($get_total_fan[$mon]!=NULL && $get_total_fan[$mon]!="")
									{
										if(mysql_num_rows($get_total_fan[$mon])>0)
										{
											$rows = mysql_fetch_assoc($get_total_fan[$mon]);
											$total_fan[$mon] = $rows['SUM( total )'];
											if($total_fan[$mon]==NULL)
											{
												$total_fan[$mon] = 0;
											}
										}
									}
									
									$get_tips_fan[$mon] = $trans_obj->get_all_tips_fan($month[$mon],$year_name_mon[$mon]);
									if($get_tips_fan[$mon]!=NULL && $get_tips_fan[$mon]!="")
									{
										$pay_fee__ips = 0;
										$don_fee__ips = 0;
										$total_fan_tip[$mon] = "";
										$purify_fee = 0;
										$org_puriy_fee = 0;
										while($rows = mysql_fetch_assoc($get_tips_fan[$mon]))
										{
											$purify_fee = 0;
											$rem_paypal = explode(",",$rows['paypal_fee']);
											$pay_fee__ips = $rem_paypal[1] + $pay_fee__ips;
											$don_fee__ips = $rows['donate'] + $don_fee__ips;
											
											$purify_fee = round(($rows['donate'] - $rem_paypal[1]) * 0.05, 2);
												
											$org_puriy_fee = $purify_fee + $org_puriy_fee;
										}
										
										$total_fan_tip[$mon] = $don_fee__ips - $pay_fee__ips - $org_puriy_fee;
										
										if($total_fan_tip[$mon]==NULL)
										{
											$total_fan_tip[$mon] = 0;
										}
									}
									
									$get_total_don[$mon] = $trans_obj->get_all_totals_don($month[$mon],$year_name_mon[$mon]);
									if($get_total_don[$mon]!=NULL && $get_total_don[$mon]!="")
									{
										if(mysql_num_rows($get_total_don[$mon])>0)
										{
											$rows = mysql_fetch_assoc($get_total_don[$mon]);
											$total_don[$mon] = ($rows['SUM( amount )'] - round($rows['SUM( paypal_fee )'], 2));
											$total_don_als[$mon] =  $rows['SUM( amount )'];
											if($total_don[$mon]==NULL)
											{
												$total_don[$mon] = 0;
											}
											
											if($total_don_als[$mon]==NULL)
											{
												$total_don_als[$mon] = 0;
											}
										}
									}
									
									$purify_mem[$mon] = $trans_obj->get_purify_member_month($month[$mon],$year_name_mon[$mon]);
									
									/* $get_total_pmem[$mon] = $trans_obj->get_purify_member_month($month[$mon]);
									if($get_total_pmem[$mon]!=NULL && $get_total_pmem[$mon]!="")
									{
										if(mysql_num_rows($get_total_pmem[$mon])>0)
										{
											$rows = mysql_fetch_assoc($get_total_pmem[$mon]);
											$purify_mem[$mon] = $rows['SUM( cost )'];
											if($purify_mem[$mon]==NULL)
											{
												$purify_mem[$mon] = 0;
											}
										}
										else
										{
											$purify_mem[$mon] = 0;
										}
									}
									else
									{
										$purify_mem[$mon] = 0;
									} */
									
									$purify_prosf[$mon] = 0;
									/* if($month_fees_data[$month[$mon].$year_name_mon[$mon]]['purify_fes']!="")
									{
										$purify_prosf[$mon] = $month_fees_data[$month[$mon].$year_name_mon[$mon]]['purify_fes'];
									}
									else
									{
										$purify_prosf[$mon] = 0;
									} */
									
									if($total_don[$mon]!=NULL)
									{
										$purify_prosf[$mon] =  $total_don[$mon] + $purify_prosf[$mon];
									}
									else
									{
										//$purify_prosf[$mon] =  0 + $purify_prosf[$mon];
										$purify_prosf[$mon] = 0;
									}
									
									if(isset($month_fees_mems[$month[$mon].$year_name_mon[$mon]]['mems_profs']) && $month_fees_mems[$month[$mon].$year_name_mon[$mon]]['mems_profs']!=NULL)
									{
										$purify_prosf[$mon] = $month_fees_mems[$month[$mon].$year_name_mon[$mon]]['mems_profs'] + $purify_prosf[$mon];
									}
									else
									{
										$purify_prosf[$mon] =  0 + $purify_prosf[$mon];
									}
	
									$get_check_pays[$mon] = $trans_obj->checks_month_pay($month[$mon],$year_name_mon[$mon]);
									if($get_check_pays[$mon]!=NULL && $get_check_pays[$mon]!="")
									{
										if(mysql_num_rows($get_check_pays[$mon])>0)
										{
											$rows = mysql_fetch_assoc($get_check_pays[$mon]);
											
											$check_pays[$mon] = $rows['count(*)'] * 10;
											if($check_pays[$mon]==NULL)
											{
												$check_pays[$mon] = 0;
											}
										}
									}
									
									$purify_nsorfs[$mon] = 0;
									if($check_pays[$mon]!=NULL)
									{
										$purify_nsorfs[$mon] = $check_pays[$mon];
									}
									if($month_fees_data[$month[$mon].$year_name_mon[$mon]]['purify_fes']!="")
									{
										$purify_nsorfs[$mon] = $month_fees_data[$month[$mon].$year_name_mon[$mon]]['purify_fes'] + $purify_nsorfs[$mon];
									}
									if($purify_prosf[$mon]!="")
									{
										$purify_nsorfs[$mon] = $purify_prosf[$mon] + $purify_nsorfs[$mon];
									}									
									?>
										<div class="tableCont">
											<div class="blkE" >
												<?php echo $moth_name[$mon]; ?>
											</div>
											<div class="blkD">
												$<?php /* if($month[$mon]=='09' && $year_name_mon[$mon]=='2013') { echo $total_cart[$mon].'--1'; echo $total_fan[$mon].'--1'; echo $total_don_als[$mon].'--1'; echo $purify_mem[$mon].'--12'; } */  
												echo round($total_cart[$mon] + $total_fan[$mon] + $total_don_als[$mon] + $purify_mem[$mon] + $check_pays[$mon],2); 
												//echo $purify_mem[$mon];
												?>
											</div>
											<div class="blkE">
												$<?php if($month_fees_data[$month[$mon].$year_name_mon[$mon]]['payapl_fee']!="") { echo round($month_fees_data[$month[$mon].$year_name_mon[$mon]]['payapl_fee'],2); } else { echo "0"; } ?>
											</div>
											<div class="blkE">
												$<?php if($month_fees_data[$month[$mon].$year_name_mon[$mon]]['user_profs']!="") { echo round($month_fees_data[$month[$mon].$year_name_mon[$mon]]['user_profs'],2); } else { echo "0"; } ?>
											</div>
											<!--<div class="blkG">
												$<?php echo round($purify_prosf[$mon],2); ?>
											</div>-->
											<div class="blkG">
												$<?php echo round($purify_nsorfs[$mon],2); ?>
											</div>
											<div class="blkC" onclick="runAccordion_sale('<?php echo $month[$mon].$year_name_mon[$mon]; ?>_month');">
												<div onselectstart="return false;"><img id="year_totals_<?php echo $mon; ?>" onclick="lists_views();" src="images/view.jpg" /></div>
											</div>
											<div class="expandable" id="Accordion<?php echo $month[$mon].$year_name_mon[$mon]; ?>_monthContent" style="
													background: none repeat scroll 0 0 #F3F3F3; height: 215px !important; margin-left: 50px;">
														<div class="affiBlock">
															<div class="RowCont">
																<div class="sideL">Tips To User :</div>
																<div class="sideR">$<?php echo round($total_cart_tip[$mon] + $total_fan_tip[$mon],2); ?>
																</div>
															</div>
															<div class="RowCont">
																<div class="sideL">Fan Club Memberships :</div>
																<div class="sideR">$<?php 
																if($month_fees_fan[$month[$mon].$year_name_mon[$mon]]['fan_profs']!=NULL) { echo round($month_fees_fan[$month[$mon].$year_name_mon[$mon]]['fan_profs'],2); }else{ echo "0"; } ?></div>
															</div>
															<div class="RowCont">
																<div class="sideL">Media Sales :</div>
																<?php
																/*Code added by azhar for media transaction starts here*/
																	$get_project_sales_monthly = $trans_obj->get_all_trans_projectsale_monthly($month[$mon],$year_name_mon[$mon]);
																	$total_profit_all_med_month = "";
																	if(mysql_num_rows($get_project_sales_monthly)>0)
																	{
																		while($rows = mysql_fetch_assoc($get_project_sales_monthly))
																		{
																			$exp_fee = explode(',',$rows['paypal_fee']);
																			if($rows['total']!=0)
																			{
																				if($rows['price']!=0)
																				{
																					$purify_fee = round(($rows['price'] - $exp_fee[0]) * 0.05, 2);
																					$get_total_profit_month = $rows['price'] - $exp_fee[0] - $purify_fee;
																					$total_profit_all_med_month = $total_profit_all_med_month + $get_total_profit_month;
																				}	
																			}
																		}
																		$month_fees_media[$month[$mon].$year_name_mon[$mon]]['media_profs'] = $month_fees_media[$month[$mon].$year_name_mon[$mon]]['media_profs'] + $total_profit_all_med_month;
																	}
																	/*Code added by azhar for media transaction ends here*/
																?>
																<div class="sideR">$<?php if(isset($month_fees_media[$month[$mon].$year_name_mon[$mon]]['media_profs']) && $month_fees_media[$month[$mon].$year_name_mon[$mon]]['media_profs']!=NULL) { echo round($month_fees_media[$month[$mon].$year_name_mon[$mon]]['media_profs'],2); }else{ echo "0"; } ?></div>
															</div>
															<div class="RowCont">
																<div class="sideL">Purify Fees :</div>
																<div class="sideR">$<?php if($month_fees_data[$month[$mon].$year_name_mon[$mon]]['purify_fes']!="") { echo round($month_fees_data[$month[$mon].$year_name_mon[$mon]]['purify_fes'],2); } else { echo "0"; } ?></div>
															</div>
															<div class="RowCont">
																<div class="sideL">Purify Donations :</div>
																<div class="sideR">$<?php echo round($total_don[$mon],2); ?></div>
															</div>
															<div class="RowCont">
																<div class="sideL">Purify Memberships :</div>
																<div class="sideR">$<?php if(isset($month_fees_mems[$month[$mon].$year_name_mon[$mon]]['mems_profs']) && $month_fees_mems[$month[$mon].$year_name_mon[$mon]]['mems_profs']!=NULL) { echo round($month_fees_mems[$month[$mon].$year_name_mon[$mon]]['mems_profs'],2); }else{ echo "0"; } ?></div>
															</div>
															<div class="RowCont">
																<div class="sideL">Requested Check Fees :</div>
																<div class="sideR">$<?php echo round($check_pays[$mon],2); ?></div>
															</div>
															<!--<div class="RowCont">
																<div class="sideL">Purify Profits :</div>
																<div class="sideR">$<?php echo $purify_nsorfs[$mon]; ?></div>
															</div>-->
														</div>
													</div>
										</div>
									<?php
							
								}
								else
								{
									$year_mon_chk[] = $year_name_mon[$mon];
									$mon_year_dbhk[] = $month[$mon].'-'.$year_name_mon[$mon];
							?>
									<div class="tableCont">
										<div class="blkA" >
											<?php echo $year_name_mon[$mon]; ?>
										</div>
									</div>
							<?php
									$month_chk[] = $month[$mon];
									$get_total_cart[$mon] = $trans_obj->get_all_totals_cart($month[$mon],$year_name_mon[$mon]);
									if($get_total_cart[$mon]!=NULL && $get_total_cart[$mon]!="")
									{
										if(mysql_num_rows($get_total_cart[$mon])>0)
										{
											$rows = mysql_fetch_assoc($get_total_cart[$mon]);
											$total_cart[$mon] = $rows['SUM( total )'];
											if($total_cart[$mon]==NULL)
											{
												$total_cart[$mon] = 0;
											}
										}
									}
									
									
									$get_tip_cart[$mon] = $trans_obj->get_all_tips_cart($month[$mon],$year_name_mon[$mon]);
									if($get_tip_cart[$mon]!=NULL && $get_tip_cart[$mon]!="")
									{
										if(mysql_num_rows($get_tip_cart[$mon])>0)
										{
											$pay_fee__ips = 0;
											$don_fee__ips = 0;
											$total_cart_tip[$mon] = "";
											$purify_fee = 0;
											$org_puriy_fee = 0;
											while($rows = mysql_fetch_assoc($get_tip_cart[$mon]))
											{
												$purify_fee = 0;
												$rem_paypal = explode(",",$rows['paypal_fee']);
												$pay_fee__ips = $rem_paypal[1] + $pay_fee__ips;
												$don_fee__ips = $rows['donate'] + $don_fee__ips;
												
												$purify_fee = round(($rows['donate'] - $rem_paypal[1]) * 0.05, 2);
												
												$org_puriy_fee = $purify_fee + $org_puriy_fee;
											}
											
											$total_cart_tip[$mon] = $don_fee__ips - $pay_fee__ips - $org_puriy_fee;
											
											if($total_cart_tip[$mon]==NULL)
											{
												$total_cart_tip[$mon] = 0;
											}
										}
									}
									
									$get_total_fan[$mon] = $trans_obj->get_all_totals_fan($month[$mon],$year_name_mon[$mon]);
									if($get_total_fan[$mon]!=NULL && $get_total_fan[$mon]!="")
									{
										if(mysql_num_rows($get_total_fan[$mon])>0)
										{
											$rows = mysql_fetch_assoc($get_total_fan[$mon]);
											$total_fan[$mon] = $rows['SUM( total )'];
											if($total_fan[$mon]==NULL)
											{
												$total_fan[$mon] = 0;
											}
										}
									}
									$get_tips_fan[$mon] = $trans_obj->get_all_tips_fan($month[$mon],$year_name_mon[$mon]);
									if($get_tips_fan[$mon]!=NULL && $get_tips_fan[$mon]!="")
									{
										$pay_fee__ips = 0;
										$don_fee__ips = 0;
										$total_fan_tip[$mon] = "";
										$purify_fee = 0;
										$org_puriy_fee = 0;
										while($rows = mysql_fetch_assoc($get_tips_fan[$mon]))
										{
											$purify_fee = 0;
											$rem_paypal = explode(",",$rows['paypal_fee']);
											$pay_fee__ips = $rem_paypal[1] + $pay_fee__ips;
											$don_fee__ips = $rows['donate'] + $don_fee__ips;
											
											$purify_fee = round(($rows['donate'] - $rem_paypal[1]) * 0.05, 2);
												
											$org_puriy_fee = $purify_fee + $org_puriy_fee;
										}
										
										$total_fan_tip[$mon] = $don_fee__ips - $pay_fee__ips - $org_puriy_fee;
										
										if($total_fan_tip[$mon]==NULL)
										{
											$total_fan_tip[$mon] = 0;
										}
									}
									
									$get_total_don[$mon] = $trans_obj->get_all_totals_don($month[$mon],$year_name_mon[$mon]);
									if($get_total_don[$mon]!=NULL && $get_total_don[$mon]!="")
									{
										if(mysql_num_rows($get_total_don[$mon])>0)
										{
											$rows = mysql_fetch_assoc($get_total_don[$mon]);
											$total_don[$mon] =  ($rows['SUM( amount )'] - round($rows['SUM( paypal_fee )'], 2));
											$total_don_als[$mon] =  $rows['SUM( amount )'];
											if($total_don[$mon]==NULL)
											{
												$total_don[$mon] = 0;
											}
											
											if($total_don_als[$mon]==NULL)
											{
												$total_don_als[$mon] = 0;
											}
										}
									}
									
									$purify_mem[$mon] = $trans_obj->get_purify_member_month($month[$mon],$year_name_mon[$mon]);
									/* $get_total_pmem[$mon] = $trans_obj->get_purify_member_month($month[$mon]);
									if($get_total_pmem[$mon]!=NULL && $get_total_pmem[$mon]!="")
									{
										if(mysql_num_rows($get_total_pmem[$mon])>0)
										{
											$rows = mysql_fetch_assoc($get_total_pmem[$mon]);
											$purify_mem[$mon] = $rows['SUM( cost )'];
											if($purify_mem[$mon]==NULL)
											{
												$purify_mem[$mon] = 0;
											}
										}
										else
										{
											$purify_mem[$mon] = 0;
										}
									}
									else
									{
										$purify_mem[$mon] = 0;
									} */
									
									$purify_prosf[$mon] = 0;
									/* if($month_fees_data[$month[$mon].$year_name_mon[$mon]]['purify_fes']!="")
									{
										$purify_prosf[$mon] = $month_fees_data[$month[$mon].$year_name_mon[$mon]]['purify_fes'];
									}
									else
									{
										$purify_prosf[$mon] = 0;
									} */
									
									if($total_don[$mon]!=NULL)
									{
										$purify_prosf[$mon] =  $total_don[$mon] + $purify_prosf[$mon];
									}
									else
									{
										//$purify_prosf[$mon] =  0 + $purify_prosf[$mon];
										$purify_prosf[$mon] = 0;
									}
									
									if(isset($month_fees_mems[$month[$mon].$year_name_mon[$mon]]['mems_profs']) && $month_fees_mems[$month[$mon].$year_name_mon[$mon]]['mems_profs']!=NULL)
									{
										$purify_prosf[$mon] = $month_fees_mems[$month[$mon].$year_name_mon[$mon]]['mems_profs'] + $purify_prosf[$mon];
									}
									else
									{
										$purify_prosf[$mon] =  0 + $purify_prosf[$mon];
									}
									
									$get_check_pays[$mon] = $trans_obj->checks_month_pay($month[$mon],$year_name_mon[$mon]);
									if($get_check_pays[$mon]!=NULL && $get_check_pays[$mon]!="")
									{
										if(mysql_num_rows($get_check_pays[$mon])>0)
										{
											$rows = mysql_fetch_assoc($get_check_pays[$mon]);
											
											$check_pays[$mon] = $rows['count(*)'] * 10;
											if($check_pays[$mon]==NULL)
											{
												$check_pays[$mon] = 0;
											}
										}
									}
									
									$purify_nsorfs[$mon] = 0;
									if($check_pays[$mon]!=NULL)
									{
										$purify_nsorfs[$mon] = $check_pays[$mon];
									}
									if($month_fees_data[$month[$mon].$year_name_mon[$mon]]['purify_fes']!="")
									{
										$purify_nsorfs[$mon] = $month_fees_data[$month[$mon].$year_name_mon[$mon]]['purify_fes'] + $purify_nsorfs[$mon];
									}
									if($purify_prosf[$mon]!="")
									{
										$purify_nsorfs[$mon] = $purify_prosf[$mon] + $purify_nsorfs[$mon];
									}
									?>
										<div class="tableCont">
											<div class="blkE" >
												<?php echo $moth_name[$mon]; ?>
											</div>
											<div class="blkD">
												$<?php echo round($total_cart[$mon] + $total_fan[$mon] + $total_don_als[$mon] + $purify_mem[$mon] + $check_pays[$mon],2);
												//echo $purify_mem[$mon].'123';
												?>
											</div>
											<div class="blkE">
												$<?php if($month_fees_data[$month[$mon].$year_name_mon[$mon]]['payapl_fee']!="") { echo round($month_fees_data[$month[$mon].$year_name_mon[$mon]]['payapl_fee'],2); } else { echo "0"; } ?>
											</div>
											<div class="blkE">
												$<?php if($month_fees_data[$month[$mon].$year_name_mon[$mon]]['user_profs']!="") { echo round($month_fees_data[$month[$mon].$year_name_mon[$mon]]['user_profs'],2); } else { echo "0"; } ?>
											</div>
											<!--<div class="blkG">
												$<?php echo round($purify_prosf[$mon],2); ?>
											</div>-->
											<div class="blkG">
												$<?php echo round($purify_nsorfs[$mon],2); ?>
											</div>
											<div class="blkC" onclick="runAccordion_sale('<?php echo $month[$mon].$year_name_mon[$mon]; ?>_month');">
												<div onselectstart="return false;"><img id="year_totals_<?php echo $mon; ?>" onclick="lists_views();" src="images/view.jpg" /></div>
											</div>
											<div class="expandable" id="Accordion<?php echo $month[$mon].$year_name_mon[$mon]; ?>_monthContent" style="
													background: none repeat scroll 0 0 #F3F3F3; height: 215px !important; margin-left: 50px;">
														<div class="affiBlock">
															<div class="RowCont">
																<div class="sideL">Tips To User :</div>
																<div class="sideR">$<?php echo round($total_cart_tip[$mon] + $total_fan_tip[$mon],2); ?>
																</div>
															</div>
															<div class="RowCont">
																<div class="sideL">Fan Club Memberships :</div>
																<div class="sideR">$<?php 
																if($month_fees_fan[$month[$mon].$year_name_mon[$mon]]['fan_profs']!=NULL) { echo round($month_fees_fan[$month[$mon].$year_name_mon[$mon]]['fan_profs'],2); }else{ echo "0"; } ?></div>
															</div>
															<div class="RowCont">
																<div class="sideL">Media Sales :</div>
																<?php
																	/*Code added by azhar for media transaction starts here*/
																	$get_project_sales_monthly = $trans_obj->get_all_trans_projectsale_monthly($month[$mon],$year_name_mon[$mon]);
																	$total_profit_all_med_month = "";
																	if(mysql_num_rows($get_project_sales_monthly)>0)
																	{
																		while($rows = mysql_fetch_assoc($get_project_sales_monthly))
																		{
																			$exp_fee = explode(',',$rows['paypal_fee']);
																			if($rows['total']!=0)
																			{
																				if($rows['price']!=0)
																				{
																					$purify_fee = round(($rows['price'] - $exp_fee[0]) * 0.05, 2);
																					$get_total_profit_month = $rows['price'] - $exp_fee[0] - $purify_fee;
																					$total_profit_all_med_month = $total_profit_all_med_month + $get_total_profit_month;
																				}	
																			}
																		}
																		$month_fees_media[$month[$mon].$year_name_mon[$mon]]['media_profs'] = $month_fees_media[$month[$mon].$year_name_mon[$mon]]['media_profs'] + $total_profit_all_med_month;
																	}
																	/*Code added by azhar for media transaction ends here*/
																?>
																<div class="sideR">$<?php if($month_fees_media[$month[$mon].$year_name_mon[$mon]]['media_profs']!=NULL) { echo round($month_fees_media[$month[$mon].$year_name_mon[$mon]]['media_profs'],2); }else{ echo "0"; } ?></div>
															</div>
															<div class="RowCont">
																<div class="sideL">Purify Fees :</div>
																<div class="sideR">$<?php if($month_fees_data[$month[$mon].$year_name_mon[$mon]]['purify_fes']!="") { echo round($month_fees_data[$month[$mon].$year_name_mon[$mon]]['purify_fes'],2); } else { echo "0"; } ?></div>
															</div>
															<div class="RowCont">
																<div class="sideL">Purify Donations :</div>
																<div class="sideR">$<?php echo round($total_don[$mon],2); ?></div>
															</div>
															<div class="RowCont">
																<div class="sideL">Purify Memberships :</div>
																<div class="sideR">$<?php if(isset($month_fees_mems[$month[$mon].$year_name_mon[$mon]]['mems_profs']) && $month_fees_mems[$month[$mon].$year_name_mon[$mon]]['mems_profs']!=NULL) { echo round($month_fees_mems[$month[$mon].$year_name_mon[$mon]]['mems_profs'],2); }else{ echo "0"; } ?></div>
															</div>
															<div class="RowCont">
																<div class="sideL">Requested Check Fees :</div>
																<div class="sideR">$<?php echo round($check_pays[$mon],2); ?></div>
															</div>
															<!--<div class="RowCont">
																<div class="sideL">Purify Profits :</div>
																<div class="sideR">$<?php echo round($purify_nsorfs[$mon],2); ?></div>
															</div>-->
														</div>
													</div>
										</div>
									<?php
								}
							}
						/* if($whole_data[$mon]['chk_date']!="")
						{
							if(in_array($whole_data[$mon]['months'], $month_chk))
							{
							}
							else
							{
					?>
								<div class="tableCont">
									<div class="blkD" style="width:100px;">
										<?php echo $whole_data[$mon]['months']; ?>
									</div>
								</div>
					<?php
								$month_chk[] = $whole_data[$mon]['months'];
								
							}
							
						} */
						}
					}
				?>
			</div>
			<div id="annual_totals" style="display:none;">
				<?php
					$data_cart = array();
					$data_fan = array();
					$data_donate = array();
					$data_pmemeber = array();
					$data_checks_paymnt = array();
					
					$all_sales = $trans_obj->get_all_trans();
					$c_dump = 0;
					$c_act = 0;
					
					if(mysql_num_rows($all_sales)>0)
					{
						while($rows = mysql_fetch_assoc($all_sales))
						{
							$data_cart[] = $rows;
							/* $month[$c_dump]['months'] = date("F",strtotime($rows['chk_date']));
							if(in_array($month[$c_dump]['months'], $month_cart))
							{}
							else
							{								
								$act_month[$c_act]['months'] = $month[$c_dump]['months'];
								$get_total = $trans_obj->get_all_totals_cart();
								if($get_total!=NULL && $get_total!="")
								{
									if(mysql_num_rows($get_total)>0)
									{
										$rows = mysql_fetch_assoc($get_total);
										$act_month[$c_act]['totals'] = $rows['SUM( total )'];
									}
								}
								
								$c_act = $c_act + 1;
								$month_cart[] = $month[$c_dump]['months'];
							}
							$c_dump = $c_dump + 1; */
						}
					}
					
					$all_fan_sales = $trans_obj->get_all_fan_trans();
					if(mysql_num_rows($all_fan_sales)>0)
					{
						while($rows = mysql_fetch_assoc($all_fan_sales))
						{
							$data_fan[] = $rows;
							/* $month[$cart_i]['months'] = date("F",strtotime($rows['chk_date']));
							$month[$cart_i]['totals'] = $rows['total'];
							$cart_i = $cart_i + 1; */
						}
					}
					
					$sql_donate = $trans_obj->get_all_donate();
					if(mysql_num_rows($sql_donate)>0)
					{
						while($rows = mysql_fetch_assoc($sql_donate))
						{
							$data_donate[] = $rows;
							/* $month[$cart_i]['months'] = date("F",strtotime($rows['date']));
							$cart_i = $cart_i + 1; */
						}
					}
					
					$data_pmemeber = $trans_obj->get_all_purify_members();
					/* $sql_pmembership = $trans_obj->get_all_purify_member();
					if(mysql_num_rows($sql_pmembership)>0)
					{
						while($rows = mysql_fetch_assoc($sql_pmembership))
						{
							$data_pmemeber[] = $rows;
							/* $month[$cart_i]['months'] = date("F",strtotime($rows['date']));
							$cart_i = $cart_i + 1; 
						}
					} */
					$data_checks_paymnt = $trans_obj->checks_payment();
					
					$whole_data = array_merge($data_donate,$data_fan,$data_cart,$data_pmemeber,$data_checks_paymnt);
				?>
					<div class="titleCont">
						<div class="blkE" >Year</div>
						<div class="blkD" style="width:70px;">Total Sales</div>
						<div class="blkE">Paypal Fees</div>
						<div class="blkE">User Profits</div>
						<div class="blkG">Purify Profits</div>
						<!--<div class="blkG" style="width:91px;">Purify Donations</div>-->
					</div>
				<?php
					
					$month_chk = array();
					$totals_all = 0;
					$total_cart = array();
					$total_fan = array();
					$total_don = array();
					$moth_name = array();
					$total_fan_tip = array();
					$total_cart_tip = array();
					$purify_mem = array();
					$check_pays = array();
					
					for($mon=0;$mon<count($whole_data);$mon++)
					{
						$total_cart[$mon] = 0;
						$total_fan[$mon] = 0;
						$total_don[$mon] = 0;
						$total_don_alsy[$mon] = 0;
						$total_fan_tip[$mon] = 0;
						$total_cart_tip[$mon] = 0;
						$purify_mem[$mon] = 0;
						$check_pays[$mon] = 0;
						
						if(isset($whole_data[$mon]['chk_date']))
						{
							$month[$mon] = date("Y",strtotime($whole_data[$mon]['chk_date']));
							//$moth_name[$mon] = date("F",strtotime($whole_data[$mon]['chk_date']));
						}
						elseif(isset($whole_data[$mon]['date']))
						{
							$month[$mon] = date("Y",strtotime($whole_data[$mon]['date']));
							//$moth_name[$mon] = date("F",strtotime($whole_data[$mon]['date']));
						}
						elseif(isset($whole_data[$mon]['purchase_date']))
						{
							$month[$mon] = date("Y",strtotime($whole_data[$mon]['purchase_date']));
						}
						elseif(isset($whole_data[$mon]['date_sent']))
						{
							$month[$mon] = date("Y",strtotime($whole_data[$mon]['date_sent']));
						}
						
						if(in_array($month[$mon], $month_chk))
						{}
						else
						{
							$month_chk[] = $month[$mon];
							$get_total_cart[$mon] = $trans_obj->get_all_totals_cart_year($month[$mon]);
							if($get_total_cart[$mon]!=NULL && $get_total_cart[$mon]!="")
							{
								if(mysql_num_rows($get_total_cart[$mon])>0)
								{
									$rows = mysql_fetch_assoc($get_total_cart[$mon]);
									$total_cart[$mon] = $rows['SUM( total )'];
								}
							}
							
							$get_tip_cart[$mon] = $trans_obj->get_all_tips_cart_year($month[$mon]);
							if($get_tip_cart[$mon]!=NULL && $get_tip_cart[$mon]!="")
							{	
								$pay_fee__ips = 0;
								$don_fee__ips = 0;
								$total_cart_tip[$mon] = "";
								$purify_fee = 0;
								$org_puriy_fee = 0;
								
								while($rows = mysql_fetch_assoc($get_tip_cart[$mon]))
								{
									$purify_fee = 0;
									$rem_paypal = explode(",",$rows['paypal_fee']);
									$pay_fee__ips = $rem_paypal[1] + $pay_fee__ips;
									$don_fee__ips = $rows['donate'] + $don_fee__ips;
									
									$purify_fee = round(($rows['donate'] - $rem_paypal[1]) * 0.05, 2);
									$org_puriy_fee = $purify_fee + $org_puriy_fee;
								}
								
								$total_cart_tip[$mon] = $don_fee__ips - $pay_fee__ips - $org_puriy_fee;
							}
							
							$get_total_fan[$mon] = $trans_obj->get_all_totals_fan_year($month[$mon]);
							if($get_total_fan[$mon]!=NULL && $get_total_fan[$mon]!="")
							{
								if(mysql_num_rows($get_total_fan[$mon])>0)
								{
									$rows = mysql_fetch_assoc($get_total_fan[$mon]);
									$total_fan[$mon] = $rows['SUM( total )'];
								}
							}
							
							$get_tip_fan[$mon] = $trans_obj->get_all_tips_fan_year($month[$mon]);
							if($get_tip_fan[$mon]!=NULL && $get_tip_fan[$mon]!="")
							{
								$pay_fee__ips = 0;
								$don_fee__ips = 0;
								$total_fan_tip[$mon] = "";
								$purify_fee = 0;
								$org_puriy_fee = 0;
								
								while($rows = mysql_fetch_assoc($get_tip_fan[$mon]))
								{
									$purify_fee = 0;
									$rem_paypal = explode(",",$rows['paypal_fee']);
									$pay_fee__ips = $rem_paypal[1] + $pay_fee__ips;
									$don_fee__ips = $rows['donate'] + $don_fee__ips;
									
									$purify_fee = round(($rows['donate'] - $rem_paypal[1]) * 0.05, 2);
									$org_puriy_fee = $purify_fee + $org_puriy_fee;
								}
								
								$total_fan_tip[$mon] = $don_fee__ips - $pay_fee__ips - $org_puriy_fee;
							}
							
							$get_total_don[$mon] = $trans_obj->get_all_totals_don_year($month[$mon]);
							if($get_total_don[$mon]!=NULL && $get_total_don[$mon]!="")
							{
								if(mysql_num_rows($get_total_don[$mon])>0)
								{
									$rows = mysql_fetch_assoc($get_total_don[$mon]);
									$total_don[$mon] = ($rows['SUM( amount )'] - round($rows['SUM( paypal_fee )'], 2));
									$total_don_alsy[$mon] = $rows['SUM( amount )'];
								}
							}
							
							/* $get_total_pmem[$mon] = $trans_obj->get_purify_member_year($month[$mon]);
							if($get_total_pmem[$mon]!=NULL && $get_total_pmem[$mon]!="")
							{
								if(mysql_num_rows($get_total_pmem[$mon])>0)
								{
									$rows = mysql_fetch_assoc($get_total_pmem[$mon]);
									$purify_mem[$mon] = $rows['SUM( cost )'];
									if($purify_mem[$mon]==NULL)
									{
										$purify_mem[$mon] = 0;
									}
								}
								else
								{
									$purify_mem[$mon] = 0;
								}
							}
							else
							{
								$purify_mem[$mon] = 0;
							} */
							$purify_prosf[$mon] = 0;
							/* if($month_fees_data_year[$month[$mon]]['purify_fes']!="")
							{
								$purify_prosf[$mon] =  $month_fees_data_year[$month[$mon]]['purify_fes'];
							}
							else
							{
								$purify_prosf[$mon] = 0;
							} */
							
							if($total_don[$mon]!=NULL)
							{
								$purify_prosf[$mon] =  $total_don[$mon] + $purify_prosf[$mon];
							}
							else
							{
								//$purify_prosf[$mon] =  0 + $purify_prosf[$mon];
								$purify_prosf[$mon] = 0;
							}
							
							if(isset($year_fees_mems[$month[$mon]]['mems_profs']) && $year_fees_mems[$month[$mon]]['mems_profs']!=NULL)
							{
								$purify_prosf[$mon] =  $year_fees_mems[$month[$mon]]['mems_profs'] + $purify_prosf[$mon];
							}
							else
							{
								$purify_prosf[$mon] =  0 + $purify_prosf[$mon];
							}	
							
							$get_check_pays[$mon] = $trans_obj->checks_year_pay($month[$mon]);
							if($get_check_pays[$mon]!=NULL && $get_check_pays[$mon]!="")
							{
								if(mysql_num_rows($get_check_pays[$mon])>0)
								{									
									$rows = mysql_fetch_assoc($get_check_pays[$mon]);

									$check_pays[$mon] = $rows['count(*)'] * 10;
									if($check_pays[$mon]==NULL)
									{
										$check_pays[$mon] = 0;
									}
								}
							}
							
							$purify_nsorfs[$mon] = 0;
							if($check_pays[$mon]!=NULL)
							{
								$purify_nsorfs[$mon] = $check_pays[$mon];
							}
							if($month_fees_data_year[$month[$mon]]['purify_fes']!="")
							{
								$purify_nsorfs[$mon] = $month_fees_data_year[$month[$mon]]['purify_fes'] + $purify_nsorfs[$mon];
							}
							if($purify_prosf[$mon]!="")
							{
								$purify_nsorfs[$mon] = $purify_prosf[$mon] + $purify_nsorfs[$mon];
							}
							
							$purify_mem[$mon] = $trans_obj->get_purify_member_year($month[$mon]);
							?>
								<div class="tableCont">
									<div class="blkE" >
										<?php echo $month[$mon]; ?>
									</div>
									<div class="blkD">
										$<?php echo round($total_cart[$mon] + $total_fan[$mon] + $total_don_alsy[$mon] + $purify_mem[$mon] + $check_pays[$mon],2); ?>
									</div>
									<div class="blkE">
										$<?php if($month_fees_data_year[$month[$mon]]['payapl_fee']!="") { echo round($month_fees_data_year[$month[$mon]]['payapl_fee'],2); } else { echo "0"; } ?>
									</div>
									<div class="blkE">
										$<?php if($month_fees_data_year[$month[$mon]]['user_profs']!="") { echo round($month_fees_data_year[$month[$mon]]['user_profs'],2); } else { echo "0"; } ?>
									</div>
									<!--<div class="blkG">
										$<?php echo round($purify_prosf[$mon],2); ?>
									</div>-->
									<div class="blkG">
										$<?php echo round($purify_nsorfs[$mon],2); ?>
									</div>
									<div class="blkC" onclick="runAccordion_sale('<?php echo $month[$mon]; ?>_year');">
										<div onselectstart="return false;"><img id="year_totals_<?php echo $mon; ?>" onclick="lists_views();" src="images/view.jpg" /></div>
									</div>
									<div class="expandable" id="Accordion<?php echo $month[$mon]; ?>_yearContent" style="
											background: none repeat scroll 0 0 #F3F3F3; height: 215px !important; margin-left: 50px;">
												<div class="affiBlock">
													<div class="RowCont">
														<div class="sideL">Tips To User :</div>
														<div class="sideR">$<?php echo round($total_cart_tip[$mon] + $total_fan_tip[$mon],2); ?></div>
													</div>
													<div class="RowCont">
														<div class="sideL">Fan Club Memberships :</div>
														<div class="sideR">$<?php if($year_fees_fan[$month[$mon]]['fan_profs']!=NULL) { echo round($year_fees_fan[$month[$mon]]['fan_profs'],2); }else{ echo "0"; } ?></div>
													</div>
													<div class="RowCont">
														<div class="sideL">Media Sales :</div>
														<div class="sideR">$<?php if($year_fees_media[$month[$mon]]['media_profs']!=NULL) { echo round($year_fees_media[$month[$mon]]['media_profs'],2); }else{ echo "0"; } ?></div>
													</div>
													<div class="RowCont">
														<div class="sideL">Purify Fees :</div>
														<div class="sideR">$<?php if($month_fees_data_year[$month[$mon]]['purify_fes']!="") { echo round($month_fees_data_year[$month[$mon]]['purify_fes'],2); } else { echo "0"; } ?></div>
													</div>
													<div class="RowCont">
														<div class="sideL">Purify Donations :</div>
														<div class="sideR">$<?php if($total_don[$mon]!=NULL) { echo round($total_don[$mon],2); }else{ echo "0"; } ?></div>
													</div>
													<div class="RowCont">
														<div class="sideL">Purify Memberships :</div>
														<div class="sideR">$<?php if(isset($year_fees_mems[$month[$mon]]['mems_profs']) && $year_fees_mems[$month[$mon]]['mems_profs']!=NULL) { echo round($year_fees_mems[$month[$mon]]['mems_profs'],2); }else{ echo "0"; } ?></div>
													</div>
													<div class="RowCont">
														<div class="sideL">Requested Check Fees :</div>
														<div class="sideR">$<?php echo round($check_pays[$mon],2); ?></div>
													</div>
													<div class="RowCont">
														<div class="sideL">Purify Profits :</div>
														<div class="sideR">$<?php echo round($purify_nsorfs[$mon],2); ?></div>
													</div>
												</div>
											</div>
								</div>
							<?php
						}
						/* if($whole_data[$mon]['chk_date']!="")
						{
							if(in_array($whole_data[$mon]['months'], $month_chk))
							{
							}
							else
							{
					?>
								<div class="tableCont">
									<div class="blkD" style="width:100px;">
										<?php echo $whole_data[$mon]['months']; ?>
									</div>
								</div>
					<?php
								$month_chk[] = $whole_data[$mon]['months'];
								
							}
							
						} */
					}
				?>
			</div>
			<!--<div id="annual_totals" style="display:none;">
				Annual Totals
			</div>-->
<?php
	/* $sql_users = $verify_obj->unverify_users();
	$all_users = array();
	if(mysql_num_rows($sql_users)>0)
	{
		while($rows = mysql_fetch_assoc($sql_users))
		{
			$all_users[] = $rows;
		}
		
		for($i=0;$i<count($all_users);$i++)
		{
?>
			<div class="tableCont" id="AccordionContainer">
				<div class="blkA"><?php echo $all_users[$i]['fname'].' '.$all_users[$i]['lname']; ?></div>
				<div class="blkB"><a onclick="return confirm_dels(2);" href="mail_delete.php?mail=<?php echo $all_users[$i]['confirm_code']; ?>">Resend Email</a></div>
				<div class="blkD"><a href="mail_delete.php?del=<?php echo $all_users[$i]['confirm_code']; ?>" onclick="return confirm_dels(1);">Delete</a></div>
			</div>
<?php
		}
	} */
?>
		</div>
	</div>
	 
<?php include_once('includes/footer.php'); ?>