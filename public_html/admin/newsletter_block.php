<?php
	$email_group=$_POST['select-email-group'];
	$email_list=$email_group;
?>
<script>
$(document).ready(
function()
{
	$("#select-email-group2").change(function (){
		//alert($("#type-select").val());
		$.post("email_list_groups2.php", { email_group:$("#select-email-group2").val() },
		function(data)
		{
		//alert("Data Loaded: " + data);
			$("#email_list").replaceWith(data);
		});
	}).change();
});
</script>
<script type="text/javascript">
	function listEmail()
	{
		$frm=document.getElementById('email-group');
		$frm.submit();
	}
</script>
<script>
	function validate_newsletter()
	{
		var con = confirm("Are you sure you want send this newsletter?");
		if(con==false)
		{
			return false;
		}
	}
</script>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1/jquery.js"></script>
<script type="text/javascript" src="../ckeditor.js"></script>
<script type="text/javascript" src="../jquery.js"></script>
<script src="../sample.js" type="text/javascript"></script>
<link href="../sample.css" rel="stylesheet" type="text/css" />
<script type="text/javascript">
	//<![CDATA[

$(function()
{
	var config = {
		toolbar:
		[
			['Bold', 'Italic', '-', 'NumberedList', 'BulletedList', '-', 'Link', 'Unlink'],
			['UIColor']
		]
	};

	// Initialize the editor.
	// Callback function can be passed and executed after full instance creation.
	$('.jquery_ckeditor').ckeditor(config);
	config.forcePasteAsPlainText = true;
});

	//]]>

</script>

<html>
<body>
<div style="float:left; width:650px; margin-top: 10px; margin-bottom: 10px; border: 1px solid #CCCCCC;">
    <div style="float:left; width:645px; margin-bottom: 10px; padding: 2px; border: 1px solid #CCCCCC; background:#777777;" align="center" class="hintWhite">Upload a Newsletter
    </div>
    <div style="float:left; width:645px; padding: 10px;">
        <form action="" method="post" id="nl_upload" enctype="multipart/form-data">
		
        <table width="100%" border="0" cellpadding="0" cellspacing="0" align="center">
			<tr>
              <td width="100">Group</td>
              <td width="402">
                <select name="select-email-group2" class="pulldownField" id="select-email-group2">
					<option value="1">All Users</option>
					<option value="2"> All Members</option>
					<option value="3"> All Non Members</option>
					<option value="4"> Artists-Members	 </option>
					<option value="5"> Artists- Non Members	</option>
					<option value="6"> Community-Members		</option>
					<option value="7"> Community- Non Members		</option>
					<option value="8"> Fans-Members		</option>
					<option value="9"> Fans-Non Members		</option>
					<option value="11">Unregistered Users	</option>
					<option value="12">Uploaded Users	</option>
					<option value="13">All Contacts</option>
				</select>
              </td>
              <td width="148" align="left"><div class="hint"></div></td>
            </tr>
			<tr>
              <td width="100">Email ids</td>
              <td width="402">
                <?php include_once('email_list_groups2.php'); ?>
              </td>
              <td width="148" align="left"><div class="hint"></div></td>
            </tr>
			<!--<tr>
              <td width="100">To</td>
              <td width="402">
                <input name="to" type="text" class="regularField" id="to" />
              </td>
              <td width="148" align="left"><div class="hint"></div></td>
            </tr>-->
			  <tr>
              <td colspan="3"><img src="images/spacer.gif" alt=" " width="10" height="10" /></td>
            </tr>  
            <tr>
              <td width="100">Subject</td>
              <td width="402">
                <input name="title" type="text" class="regularField" id="title" />
              </td>
              <td width="148" align="left"><div class="hint"></div></td>
            </tr>
            <tr>
              <td colspan="3"><img src="images/spacer.gif" alt=" " width="10" height="10" /></td>
            </tr>        
            <tr>
              <td width="100" valign="top"></td>
              <td width="402">
                <?php echo $msg; ?>
              </td>
              <td width="148" align="left"><div class="hint"></div></td>
            </tr>
            <tr>
              <td width="100">Paragraph</td>
              <td width="402">
                <textarea name="paragraph" class="jquery_ckeditor" id="paragraph"></textarea>
              </td>
        	</tr>
			<tr>
              <td colspan="3"><img src="images/spacer.gif" alt=" " width="10" height="10" /></td>
            </tr>    
			<!--<tr>  
			  <td width="100">User ID</td>
              <td width="144">
              	<input name="feature_id1" type="text" class="regularField" id="feature_id1" />              
              </td>
              <td width="148" align="left"><div class="hint">Enter 6 User IDs resp. for the newsletter.</div></td>
     	    </tr>
			<tr>
              <td colspan="3"><img src="images/spacer.gif" alt=" " width="10" height="10" /></td>
            </tr>    
			<tr>  
			  <td width="100">User ID</td>
              <td width="144">
              	<input name="feature_id2" type="text" class="regularField" id="feature_id2" />              
              </td>
              <td width="148" align="left"></td>
      	    </tr>
			<tr>
              <td colspan="3"><img src="images/spacer.gif" alt=" " width="10" height="10" /></td>
            </tr>    
			<tr>  
			  <td width="100">User ID</td>
              <td width="144">
              	<input name="feature_id3" type="text" class="regularField" id="feature_id3" />              
              </td>
              <td width="148" align="left"></td>
     	    </tr>
			<tr>
              <td colspan="3"><img src="images/spacer.gif" alt=" " width="10" height="10" /></td>
            </tr>    
			<tr>  
			  <td width="100">User ID</td>
              <td width="144">
              	<input name="feature_id4" type="text" class="regularField" id="feature_id4" />              
              </td>
              <td width="148" align="left"></td>
      	    </tr>
			<tr>
              <td colspan="3"><img src="images/spacer.gif" alt=" " width="10" height="10" /></td>
            </tr>    
			<tr>  
			  <td width="100">User ID</td>
              <td width="144">
              	<input name="feature_id5" type="text" class="regularField" id="feature_id5" />              
              </td>
              <td width="148" align="left"></td>
     	    </tr>
			<tr>
              <td colspan="3"><img src="images/spacer.gif" alt=" " width="10" height="10" /></td>
            </tr>    
			<tr>  
			  <td width="100">User ID</td>
              <td width="144">
              	<input name="feature_id6" type="text" class="regularField" id="feature_id6" />              
              </td>
              <td width="148" align="left"></td>
      	    </tr>-->
		    <tr>
              <td colspan="3"><img src="images/spacer.gif" alt=" " width="10" height="10" /></td>
            </tr>
            <tr>
              <td width="100" valign="top"></td>
              <td width="402" align="right">
                <input type="submit" onClick="return validate_newsletter()" name="upload" id="upload" value=" Send " />
              </td>
              <td width="148" align="left"><div class="hint"></div></td>
            </tr>
          </table>
          </form>
    </div>
</div>
<!--<div style="float:left; width:100%; margin-top: 10px; padding: 5px;">
  <h4 class="withLine">Current newsletter: </h4>
</div>
<div style="float:left; width:650px; height:300px; margin: 0px; overflow:scroll; border: 1px solid #CCCCCC; color:#000000" class="hint">
  <?php if($flag_current)
  			include_once('uploaded_newsletter.php');
		else
			echo "--None--";
  ?>
</div>
<div style="float:left; width:650px; margin-top: 10px; padding: 0px;" class="withLine">
    <div style="float:left; width:350px; margin-top: 0px; padding: 5px;" align="left"><a href="<?php echo $newsletter_path; ?>" target="_blank" class="hint"><?php echo $row['title']."".$row['post_date'].""; ?></a>
    </div>
    <div style="float:right; width:200px; margin-top: 0px; padding: 5px;" align="right">
      <?php 
        if($sent=='0') 
        {
      ?>
          <form action="" method="post" id="send_nl">
            <input type="submit" name="send" id="send" value=" Send " />
          </form>
      <?php
        }
      ?>  
  </div>
</div>
<div style="float:left; width:650px; margin-top: 10px; padding: 5px;">
 <h4 class="withLine">Links to previous newsletters:</h4>
</div>
<div style="float:left; width:650px; height:100px; margin: 0px; overflow:scroll; border: 1px solid #CCCCCC;" class="hint">
  <ul>  
	  <?php 
        $rs=$obj->getNewsletters();
        while($row=mysql_fetch_assoc($rs))
        {
      ?>
		  	<li><a href="<?php echo "../".$row['upload_path'] ?>" target="_blank"><?php echo $row['title']." (".$row['post_date'].")"; ?></a></li>
	  <?php 
        }
      ?>
  </ul>
</div>-->
</body>

</html>