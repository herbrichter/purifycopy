<?php
	session_start();
	ob_start();
	include_once('commons/db.php');
	include_once('loggedin_includes.php');
	include_once('classes/ProfileType.php');
	include_once('classes/Commontabs.php');
	include_once('classes/AddContact.php');
	include_once('classes/GeneralInfo.php');
	include_once('classes/CountryState.php');
	//include('recaptchalib.php');
	include_once('classes/ProfileEvent.php');
	include_once('classes/Addfriend.php');
	include_once('classes/GetAllFans.php');
	include_once('classes/NewSuggestion.php');
	include_once('classes/Mails.php');
	include_once('classes/CreatePurifyMember.php');
	include_once('classes/SubscriptionListings.php');
	include_once('classes/PersonalWallDisplayFeeds.php');
	include_once('classes/AddPermission.php');
	include_once('classes/ProfileeditCommunity.php');
	include_once('classes/GetSalesInfo.php');
	include_once('classes/DisplayStatistics.php');
	include_once('classes/Get_Transactions.php');
	include_once("classes/DeleteMembership.php");
	require_once('vimeo-vimeo-php-lib/vimeo.php');
	require_once('classes/Social_details.php');
	require_once("src/MCAPI.class.php");
    if (!class_exists('\user_keys'))
    {require_once($_SERVER["DOCUMENT_ROOT"].'/classes/user_keys.php');} 
    
    $domainname=$_SERVER['SERVER_NAME'];
	
	$delete_obj_pros = new DeleteMembership();
	$sql_feeds = new PersonalWallDisplayFeeds();
	$newgeneral = new ProfileeditCommunity();
	$trans = new Get_Transactions();
	$all_stats = new DisplayStatistics();
	$member_obj = new CreatePurifyMember();
	$social_obj = new SocialDetails();   
    
    // Get user and password from user_keys table
    $uk = new \user_keys();
    $sgk = $uk->get_UserKeys("Vimeo");
    $key = $sgk['clientid'];
    $secret = $sgk['clientsecret']; 
	$vimeo = new phpVimeo($key, $secret);
    
    // add user name to bucketname in s3 url
    $root = realpath($_SERVER["DOCUMENT_ROOT"]);
    if (!class_exists('S3')) require_once ($root.'/S3.php');    
    $s3 = new S3();
    $generalproimageURI=$s3->getNewURI("generalproimage");
    
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en" /><head>
	<meta https-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Purify Art: Personal Profile</title>
	<script src="includes/jquery.js"></script>

<?php
	$newtab=new Commontabs();

	include_once("header.php");

	$sub_listings = new SubscriptionListings();	
	$get_sales_info = new GetSalesInfo();	
	$get_permission = new AddPermission();	
	$mails_object = new Mails();
	if(isset($_GET['mail_delete']))
	{
		$count_inbox_del = 0;
		$delete = $mails_object->delete_mail($_GET['mail_delete'],$count_inbox_del);
		header("Location: profileedit.php#mail");
	}
	if(isset($_GET['delete_sub_stat_art']))
	{
		$delete = $sub_listings->deleteSubscription($_GET['delete_sub_stat_art']);
		header("Location: profileedit.php#subscrriptions");
	}
?>
<link href="/galleryfiles/gallery.css" rel="stylesheet"/>
<script src="/galleryfiles/jquery.easing.1.3.js"></script>
<script src="/includes/functionsmedia.js" type="text/javascript"></script>
<script src="/includes/functionsURL.js" type="text/javascript"></script>
<script src="/includes/mediaelement-and-player.min.js"></script>
<link rel="stylesheet" href="/includes/mediaelementplayer.min.css" />

<link rel="stylesheet" href="../lightbox/prettyPhoto.css" type="text/css" media="screen" title="prettyPhoto main stylesheet" charset="utf-8" />
<script src="../lightbox/jquery.prettyPhoto.js" type="text/javascript" charset="utf-8"></script>
<script src="../javascripts/popup.js" type="text/javascript" charset="utf-8"></script>


<div id="light-gal" style="z-index:99999;" class="gallery-wrapper">
	<div id="bg">
		<a href="#" class="nextImageBtn" title="next"></a>
		<a href="#" class="prevImageBtn" title="previous"></a>
		<!--<img width="1680" src=".././uploads/gallery/1335074394-7032884797_475ce129ca.jpg" height="1050" alt="" title="" id="bgimg" />-->
	</div>
	<div id="preloader"><img src=".././galleryfiles/ajax-loader_dark.gif" width="32" height="32" /></div>
	<div id="img_title"></div>
	<div id="toolbar">
		<a href="#" title="Pause" id="pauseImg" onClick="setimage('pause')"><img src=".././galleryfiles/pause.png" width="50" height="50"  /></a>
		<a href="#" title="Maximize" id="maximizeImg" onClick="ImageViewMode('full');return false"><img src=".././galleryfiles/toolbar_fs_icon.png" width="50" height="50"  /></a>
		<script type="text/javascript">
		<!--[if IE]>
		$("#maximizeImg").css({"display":"none"});
		<![endif]-->
		</script>
		<a href="#" title="Close" id="closeGal" onClick="CloseGallery();return false"><img src=".././galleryfiles/close.png" width="50" height="50"  /></a>
	</div>
	<div id="thumbnails_wrapper">
	<div id="outer_container">
	<div class="thumbScroller">
		<div class="container" id="gallery-container">
			
		</div>
	</div>
	</div>
	</div>
</div>
<div id="outerContainer">
				<!--<p><a href="../logout.php">Logout</a> </p>
			</div>
		</div>
	</div> 
</div>-->
<!--<link rel="stylesheet" type="text/css" media="screen" href="includes/purify.css" />
	<script src='http://ajax.googleapis.com/ajax/libs/jquery/1.6.1/jquery.min.js'></script>-->
	
	<!--<link rel="stylesheet" href="includes/jquery.ui.all.css" />
	<script src="includes/jquery.ui.core.js"></script>
	<script src="includes/jquery.ui.widget.js"></script>
	<script src="includes/jquery.ui.datepicker.js"></script>-->
	<script>
			$(function() 
			{
				$("#editbday").datepicker({
				dateFormat: 'yy-mm-dd',
				changeMonth: true,
				changeYear: true
				});
			});
		</script>
	<script type="text/javascript">
		$(function() {

			$("#personalTab").organicTabs();
		});
		
		function changeForm()
		{
			document.membership_type.submit();
		}
	</script>
	<script type="text/javascript">
	var $_returnvalue ="";
	var $_returnclicktext ="";
	var $_returnsong_id ="";
	function confirm_renew_member(text,name)
	{
		var a =confirm("Are you sure you want to "+text+" "+name+" membership?");
		if(a == false){
			return false;
		}
	}
	function confirm_renew_my_member(text,name,id)
	{
		var a =confirm("Are you sure you want to "+text+" "+name+" membership?");
		if(a == false){
			return false;
		}else{
			$("#"+id).click();
		}
	}
	function confirmdelete()
	{
		var x;
		var name=confirm("Are You Sure You want To Delete?");
		if (name==false)
		{
			return false;
		}
	}
	function download_directly_song(id,t,c,f){
		var val = confirm("Are you sure you want to download "+t+" by "+c+" from "+f+"?");
		if(val==true){
		window.location = "zipes_1.php?download_meds="+id;
		}
	}

	function ask_to_donate(name,creator,song_id,clicktext)
	{
		$("#get_donationpop").attr("href", "asktodonate.php?name="+name+"&creator="+creator+"&clicktext="+clicktext+"&song_id="+song_id);
		$("#get_donationpop").click();
	}
	function down_video_pop(id)
	{
		var Dvideolink = document.getElementById("download_video"+id);
		Dvideolink.click();
	}

	function download_directly_song(id,t,c,f){
		var val = confirm("Are you sure you want to download "+t+" by "+c+" from "+f+"?");
		if(val==true){
		window.location = "zipes_1.php?download_meds="+id;
		}
	}
	function down_song_pop(id)
	{
		var Dvideolink = document.getElementById("download_song"+id);
		Dvideolink.click();

	}
	function down_song_pop_project(id)
	{
		var Dvideolink = document.getElementById("download_songproject"+id);
		Dvideolink.click();
	}
	function down_video_pop_project(id)
	{
		var Dvideolink = document.getElementById("download_videoproject"+id);
		Dvideolink.click();
	}
	function confirm_delposts()
	{
		var x;
		var name=confirm("Are You Sure You want To Delete?");
		if (name==false)
		{
			return false;
		}
	}
	$(document).ready(function() {
	$("#get_donationpop").fancybox({
		'width'				: '35%',
		'height'			: '25%',
		'transitionIn'		: 'none',
		'transitionOut'		: 'none',
		'type'				: 'iframe',
		'onClosed'			: function(){
								ask_to_donate_result();	
							}
	});
});


function ask_to_donate_result()
{
	if($_returnvalue == 1){
		$_returnvalue ="";
		var Dvideolink = document.getElementById($_returnclicktext);
		setTimeout(function(){
		Dvideolink.click();
		}
		,2000);
	}
	else if($_returnvalue == 2){
		setTimeout(function(){
		$_returnvalue ="";
		window.location = "zipes_1.php?download_meds="+$_returnsong_id;
		}
		,1000);
	}
}
	$("document").ready(function(){
		$("#pass_sub").click(function()
		{
			var pw1 = $("#new_pass").val();
			var pw2 = $("#new_conf_pass").val();
			var pw3 = $("#old_pass").val();
			var invalid = " ";
			if(pw3!='' && pw1!="" && pw2!="")
			{
				var ajax_mess = 'oldie='+ pw3 +'&newie='+ pw1 +'&newiec='+ pw2;
				$.ajax({
					type: 'POST',
					url : 'Change_password.php',
					data: ajax_mess,
					success: function(data)  
					{
						if(data!="")
						{
							$("#error_pass").html(data);
						}
						else
						{
							$("#error_pass").html('');
							alert("Your Password is changed.");
							window.location = "profileedit.php";
						}
					}
				});
			}
		});
	});
	
	function validate_passesform()
	{
		var invalid = " "; // Invalid character is a space
		var minLength = 6; // Minimum length
		var pw1 = $("#new_pass").val();
		var pw2 = $("#new_conf_pass").val();
		var pw3 = $("#old_pass").val();
		
		if(pw3=='')
		{
			alert("Please enter your old password.");
			$("#old_pass").focus();
			return false;
		}
		
		if($("#old_pass").val().indexOf(invalid) > -1)
		{
			alert("Please enter valid password.");
			$("#old_pass").focus();
			return false;
		}
		
		// check for minimum length
		if ($("#new_pass").val().length < minLength)
		{
			alert('Your password must be at least ' + minLength + ' characters long. Try again.');
			$("#new_pass").focus();
			return false;
		}
		// check for a value in both fields.
		if (pw1 == '' || pw2 == '')
		{
			alert('Please Re-enter your password twice ');
			$("#new_conf_pass").focus();
			return false;
		}
		// check for spaces
		if ($("#new_pass").val().indexOf(invalid) > -1)
		{
			alert("Sorry, spaces are not allowed.");
			$("#new_pass").focus();
			return false;
		}
		
		if (pw1 != pw2)
		{
			alert ("You did not enter the same new password twice. Please re-enter your password.");
			$("#new_conf_pass").focus();
			return false;
		}
	}
	
	function confirm_delete(y)
	{
		var x;
		var name=confirm("Are You Sure You want To Delete  "+y+" ?");
		if (name==false)
		{
			return false;
		}
	}
	function confirm_creditcard_delete(s)
	{
		var card = confirm("Are You Sure You want To Remove  "+s+" Credit Card?");
		if (card==false)
		{
			return false;
		}
	}
	function confirm_member_delete(s)
	{
		var member = confirm("Are You Sure You want To delete  "+s+" Member?");
		if (member==false)
		{
			return false;
		}
	}
	function runscript()
	{
		$("#searchlink").click();
	}
	function find_firends(){
		$("#searchlink").click();
	}
	</script>
	<script type="text/javascript">
	
	function handleSelection2(choice)
	{
		document.getElementById('mymember').style.display="block";
		document.getElementById('purimember').style.display="none";
	}
	function handleSelection(choice) {
	$(".list-wrap").css({"height":""});
	//document.getElementById('select').disabled=true;
	if(choice=='total')
	{
		document.getElementById(choice).style.display="block";
		document.getElementById('salesLog').style.display="none";
		document.getElementById('payments').style.display="none";
	}
	else if(choice=='payments')
	{
		document.getElementById(choice).style.display="block";
		document.getElementById('total').style.display="none";
		document.getElementById('salesLog').style.display="none";
	}
	else
	{
		if(document.getElementById(choice)!=null){ document.getElementById(choice).style.display="block"; }
		if(document.getElementById('payments')!=null){ document.getElementById('payments').style.display="none";}
		if(document.getElementById('total')!=null){ document.getElementById('total').style.display="none";}
	}
	
	if(choice=='upcoming')
		{
		  document.getElementById(choice).style.display="block";
		  document.getElementById('published').style.display="none";
		  document.getElementById('allevents').style.display="none";
		}
	if(choice=='published')
		{
		  document.getElementById(choice).style.display="block";
		  document.getElementById('upcoming').style.display="none";
		  document.getElementById('allevents').style.display="none";
		}
	if(choice=='allevents')
		{
		  document.getElementById(choice).style.display="block";
		  document.getElementById('upcoming').style.display="none";
		  document.getElementById('published').style.display="none";
		}

	if(choice=='artist')
		{
		  document.getElementById(choice).style.display="block";
		  document.getElementById('select').style.display="none";
		}
		else
		{
			if(document.getElementById(choice)!=null){ document.getElementById(choice).style.display="block"; }
			if(document.getElementById('artist')!=null){ document.getElementById('artist').style.display="none"; }
		}

	if(choice=='community')
		{
		  document.getElementById(choice).style.display="block";
		  document.getElementById('select').style.display="none";
		}
		else
		{
			if(document.getElementById(choice)!=null){ document.getElementById(choice).style.display="block"; }
			if(document.getElementById('community')!=null){ document.getElementById('community').style.display="none"; }
		}

	if(choice=='artistFans')
		{
		  document.getElementById(choice).style.display="block";
		  document.getElementById('select').style.display="none";
		}
		else
		{
			if(document.getElementById(choice)!=null){ document.getElementById(choice).style.display="block"; }
			if(document.getElementById('artistFans')!=null){ document.getElementById('artistFans').style.display="none"; }
		}

	if(choice=='communityFans')
		{
		  document.getElementById(choice).style.display="block";
		  document.getElementById('select').style.display="none";
		}
		else
		{
			if(document.getElementById(choice)!=null){ document.getElementById(choice).style.display="block"; }
			if(document.getElementById('communityFans')!=null){ document.getElementById('communityFans').style.display="none"; }
		}
		
		if(choice=='purimember')
		{
		  document.getElementById(choice).style.display="block";
		 // document.getElementById('purimember').style.display="none";
		}
		else
		{
			if(document.getElementById(choice)!=null){ document.getElementById(choice).style.display="block"; }
			if(document.getElementById('purimember')!=null){ document.getElementById('purimember').style.display="none"; }
		}	
		
	if(choice=='mymember')
		{
		  document.getElementById(choice).style.display="block";
		  document.getElementById('purimember').style.display="none";
		}
		else
		{
			if(document.getElementById(choice)!=null){ document.getElementById(choice).style.display="block"; }
			if(document.getElementById('mymember')!=null){ document.getElementById('mymember').style.display="none"; }
		}		

	if(choice=='artmember')
		{
			document.getElementById(choice).style.display="block";
			if(document.getElementById("total_artist")!=null){
				document.getElementById("total_artist").style.display="block";
			}
			document.getElementById('purimember').style.display="none";
		}
		else
		{
			
			document.getElementById(choice).style.display="block";
			if(document.getElementById("total_artist")!= null){
				document.getElementById("total_artist").style.display="none";
			}
			//document.getElementById(choice).style.display="none";
			//document.getElementById('artmember').style.display="none";
			if(document.getElementById('artmember')!=null){ document.getElementById('artmember').style.display="none"; }
		}		

	if(choice=='commember')
		{
			document.getElementById(choice).style.display="block";
			if(document.getElementById("total_community")!= null){
				document.getElementById("total_community").style.display="block";
			}
			document.getElementById('purimember').style.display="none";
		}
		else
		{
			if(document.getElementById("total_community")!= null){
				document.getElementById("total_community").style.display="none";
			}
			//document.getElementById(choice).style.display="block";
			//document.getElementById('commember').style.display="none";
			if(document.getElementById(choice)!=null){ document.getElementById(choice).style.display="block"; }
			if(document.getElementById('commember')!=null){ document.getElementById('commember').style.display="none"; }
		}
	if(choice=='comprojectmember')
	{
		var text_value1 = $('#viewmembership option:selected').attr('name');
		var text_value = $('#viewmembership').val();
		var ajax_mess = 'id='+ text_value1 +'&type='+ text_value;
		$.ajax({
			type: 'POST',
			url : 'get_project_memberlist.php',
			data: ajax_mess,
			success: function(data)  
			{
				if(data!="")
				{
					$("#comprojectmember").html(data);
					//$("#comprojectmember").html(data);
				}
			}
		});
		document.getElementById(choice).style.display="block";
		document.getElementById('purimember').style.display="none";
		
	}
	else
	{
		if(document.getElementById(choice)!=null){ document.getElementById(choice).style.display="block"; }
		if(document.getElementById('comprojectmember')!=null){ document.getElementById('comprojectmember').style.display="none"; }
	}
	if(choice=='artprojectmember')
	{
		var text_value1 = $('#viewmembership option:selected').attr('name');
		var text_value = $('#viewmembership').val();
		var ajax_mess = 'id='+ text_value1 +'&type='+ text_value;
		$.ajax({
			type: 'POST',
			url : 'get_project_memberlist.php',
			data: ajax_mess,
			success: function(data)  
			{
				if(data!="")
				{
					//$("#artprojectmember").innerhtml(data);
					$("#artprojectmember").html(data);
				}
			}
		});
		document.getElementById(choice).style.display="block";
		document.getElementById('purimember').style.display="none";
		
	}
	else
	{
		if(document.getElementById(choice)!=null){ document.getElementById(choice).style.display="block"; }
		if(document.getElementById('artprojectmember')!=null){ document.getElementById('artprojectmember').style.display="none"; }
	}
	if(choice=='mycal')
		{
		  document.getElementById(choice).style.display="block";
		  document.getElementById('mylist').style.display="none";
		}
		else
		{
			if(document.getElementById(choice)!=null){ document.getElementById(choice).style.display="block"; }
			if(document.getElementById('mycal')!=null){ document.getElementById('mycal').style.display="none"; }
		}	
	
	
	
	if(choice=='subPutify')
	{
		if(document.getElementById(choice)!=null){ document.getElementById(choice).style.display="block"; }
		if(document.getElementById('all_putify')!=null){ document.getElementById('all_putify').style.display="none"; }
		if(document.getElementById('subArtist')!=null){ document.getElementById('subArtist').style.display="none"; }
		if(document.getElementById('subCommunity')!=null){ document.getElementById('subCommunity').style.display="none"; }
		if(document.getElementById('sub_Projects')!=null){ document.getElementById('sub_Projects').style.display="none"; }
		if(document.getElementById('sub_Events')!=null){ document.getElementById('sub_Events').style.display="none"; }
	}
	/*else
	{
	  document.getElementById(choice).style.display="block";
	  document.getElementById('subPutify').style.display="none";
	}*/
		
	if(choice=='subArtist')
	{
		if(document.getElementById(choice)!=null){ document.getElementById(choice).style.display="block"; }
		if(document.getElementById('all_putify')!=null){ document.getElementById('all_putify').style.display="none"; }
		if(document.getElementById('subPutify')!=null){ document.getElementById('subPutify').style.display="none"; }
		if(document.getElementById('subCommunity')!=null){ document.getElementById('subCommunity').style.display="none"; }
		if(document.getElementById('sub_Projects')!=null){ document.getElementById('sub_Projects').style.display="none"; }
		if(document.getElementById('sub_Events')!=null){ document.getElementById('sub_Events').style.display="none"; }
	}
		/*else
		{
		  document.getElementById(choice).style.display="block";
		  document.getElementById('subArtist').style.display="none";
		}*/

	if(choice=='subCommunity')
	{	
		if(document.getElementById(choice)!=null){ document.getElementById(choice).style.display="block"; }
		if(document.getElementById('all_putify')!=null){ document.getElementById('all_putify').style.display="none"; }
		if(document.getElementById('subArtist')!=null){ document.getElementById('subArtist').style.display="none"; }
		if(document.getElementById('subPutify')!=null){ document.getElementById('subPutify').style.display="none"; }
		if(document.getElementById('sub_Projects')!=null){ document.getElementById('sub_Projects').style.display="none"; }
		if(document.getElementById('sub_Events')!=null){ document.getElementById('sub_Events').style.display="none"; }
	}
		/*else
		{
		  document.getElementById(choice).style.display="block";
		  document.getElementById('subCommunity').style.display="none";
		}*/
		
	if(choice=='sub_Projects')
	{
		if(document.getElementById(choice)!=null){ document.getElementById(choice).style.display="block"; }
		if(document.getElementById('subCommunity')!=null){ document.getElementById('subCommunity').style.display="none"; }
		if(document.getElementById('all_putify')!=null){ document.getElementById('all_putify').style.display="none"; }
		if(document.getElementById('subArtist')!=null){ document.getElementById('subArtist').style.display="none"; }
		if(document.getElementById('subPutify')!=null){ document.getElementById('subPutify').style.display="none"; }
		if(document.getElementById('sub_Events')!=null){ document.getElementById('sub_Events').style.display="none"; }
	}
		/*else
		{
		  document.getElementById(choice).style.display="block";
		  document.getElementById('sub_Projects').style.display="none";
		}*/
		
	if(choice=='sub_Events')
	{
		if(document.getElementById(choice)!=null){ document.getElementById(choice).style.display="block"; }
		if(document.getElementById('sub_Projects')!=null){ document.getElementById('sub_Projects').style.display="none"; }
		if(document.getElementById('subCommunity')!=null){ document.getElementById('subCommunity').style.display="none"; }
		if(document.getElementById('all_putify')!=null){ document.getElementById('all_putify').style.display="none"; }
		if(document.getElementById('subArtist')!=null){ document.getElementById('subArtist').style.display="none"; }
		if(document.getElementById('subPutify')!=null){ document.getElementById('subPutify').style.display="none"; }
	}
		/*else
		{
		  document.getElementById(choice).style.display="block";
		  document.getElementById('sub_Events').style.display="none";
		}*/
		
		
		
		
	if(choice=='events_feeds')
		{
		  document.getElementById(choice).style.display="block";
		  document.getElementById('all_news_feeds').style.display="none";
		}
		else
		{
			if(document.getElementById(choice)!=null){ document.getElementById(choice).style.display="block"; }
			if(document.getElementById('events_feeds')!=null){ document.getElementById('events_feeds').style.display="none"; }
		}
		
	if(choice=='project_feeds')
		{
		  document.getElementById(choice).style.display="block";
		  document.getElementById('all_news_feeds').style.display="none";
		}
		else
		{
			if(document.getElementById(choice)!=null){ document.getElementById(choice).style.display="block"; }
			if(document.getElementById('project_feeds')!=null){ document.getElementById('project_feeds').style.display="none"; }
		}

	if(choice=='music_feeds')
		{
		  document.getElementById(choice).style.display="block";
		  document.getElementById('all_news_feeds').style.display="none";
		}
		else
		{
			if(document.getElementById(choice)!=null){ document.getElementById(choice).style.display="block"; }
			if(document.getElementById('music_feeds')!=null){ document.getElementById('music_feeds').style.display="none"; }
		}

	if(choice=='photo_feeds')
		{
		  document.getElementById(choice).style.display="block";
		  document.getElementById('all_news_feeds').style.display="none";
		}
		else
		{
			if(document.getElementById(choice)!=null){ document.getElementById(choice).style.display="block"; }
			if(document.getElementById('photo_feeds')!=null){ document.getElementById('photo_feeds').style.display="none"; }
		}
		
	if(choice=='video_feeds')
		{
		  document.getElementById(choice).style.display="block";
		  document.getElementById('all_news_feeds').style.display="none";
		}
		else
		{
			if(document.getElementById(choice)!=null){ document.getElementById(choice).style.display="block"; }
			if(document.getElementById('video_feeds')!=null){ document.getElementById('video_feeds').style.display="none"; }
		}
	if(choice=='events_feeds_p')
		{
		  document.getElementById(choice).style.display="block";
		  document.getElementById('all_news_feeds_p').style.display="none";
		}
		else
		{
			if(document.getElementById(choice)!=null){ document.getElementById(choice).style.display="block"; }
			if(document.getElementById('events_feeds_p')!=null){ document.getElementById('events_feeds_p').style.display="none"; }
		}
		
	if(choice=='project_feeds_p')
		{
		  document.getElementById(choice).style.display="block";
		  document.getElementById('all_news_feeds_p').style.display="none";
		}
		else
		{
			if(document.getElementById(choice)!=null){ document.getElementById(choice).style.display="block"; }
			if(document.getElementById('project_feeds_p')!=null){ document.getElementById('project_feeds_p').style.display="none"; }
		}

	if(choice=='music_feeds_p')
		{
		  document.getElementById(choice).style.display="block";
		  document.getElementById('all_news_feeds_p').style.display="none";
		}
		else
		{
			if(document.getElementById(choice)!=null){ document.getElementById(choice).style.display="block"; }
			if(document.getElementById('music_feeds_p')!=null){ document.getElementById('music_feeds_p').style.display="none"; }
		}

	if(choice=='photo_feeds_p')
		{
		  document.getElementById(choice).style.display="block";
		  document.getElementById('all_news_feeds_p').style.display="none";
		}
		else
		{
			if(document.getElementById(choice)!=null){ document.getElementById(choice).style.display="block"; }
			if(document.getElementById('photo_feeds_p')!=null){ document.getElementById('photo_feeds_p').style.display="none"; }
		}
		
	if(choice=='video_feeds_p')
		{
		  document.getElementById(choice).style.display="block";
		  document.getElementById('all_news_feeds_p').style.display="none";
		}
		else
		{
			if(document.getElementById(choice)!=null){ document.getElementById(choice).style.display="block"; }
			if(document.getElementById('video_feeds_p')!=null){ document.getElementById('video_feeds_p').style.display="none"; }
		}
		
	if(choice=='statmedia')
		{
		  document.getElementById(choice).style.display="block";
		  document.getElementById('statpersonal').style.display="none";
		}
		else
		{
			if(document.getElementById(choice)!=null){ document.getElementById(choice).style.display="block"; }
			if(document.getElementById('statmedia')!=null){ document.getElementById('statmedia').style.display="none"; }
		}

	if(choice=='statartist')
		{
		  document.getElementById(choice).style.display="block";
		  document.getElementById('statpersonal').style.display="none";
		}
		else
		{
			if(document.getElementById(choice)!=null){ document.getElementById(choice).style.display="block"; }
			if(document.getElementById('statartist')!=null){ document.getElementById('statartist').style.display="none"; }
		}		

	if(choice=='statcommunity')
		{
		  document.getElementById(choice).style.display="block";
		  document.getElementById('statpersonal').style.display="none";
		}
		else
		{
			if(document.getElementById(choice)!=null){ document.getElementById(choice).style.display="block"; }
			if(document.getElementById('statcommunity')!=null){ document.getElementById('statcommunity').style.display="none"; }
		}
	
	if(choice=='awaiting_response')
		{
			document.getElementById(choice).style.display="block";
			document.getElementById('all_friends_fans').style.display="block";
		}
		else
		{
			if(document.getElementById(choice)!=null){ document.getElementById(choice).style.display="block"; }
			if(document.getElementById('awaiting_response')!=null){ document.getElementById('awaiting_response').style.display="none"; }
		}
		
	if(choice=='artist_fansa')
		{
			document.getElementById(choice).style.display="block";
			document.getElementById('all_friends_fans').style.display="none";
		}
		else
		{
			if(document.getElementById(choice)!=null){ document.getElementById(choice).style.display="block"; } 
			if(document.getElementById('artist_fansa')!=null){ document.getElementById('artist_fansa').style.display="none"; }
		}
		
	if(choice=='artist_friendsa')
		{
			document.getElementById(choice).style.display="block";
			document.getElementById('all_friends_fans').style.display="none";
		}
		else
		{
			if(document.getElementById(choice)!=null){ document.getElementById(choice).style.display="block"; }
			if(document.getElementById('artist_friendsa')!=null){ document.getElementById('artist_friendsa').style.display="none"; }
		}
		
	if(choice=='community_fansa')
		{
			document.getElementById(choice).style.display="block";
			document.getElementById('all_friends_fans').style.display="none";
		}
		else
		{
			if(document.getElementById(choice)!=null){ document.getElementById(choice).style.display="block"; }
			if(document.getElementById('community_fansa')!=null){ document.getElementById('community_fansa').style.display="none"; }
		}
		
	if(choice=='community_friendsa')
		{
			document.getElementById(choice).style.display="block";
			document.getElementById('all_friends_fans').style.display="none";
		}
		else
		{
			if(document.getElementById(choice)!=null){ document.getElementById(choice).style.display="block"; }
			if(document.getElementById('community_friendsa')!=null){ document.getElementById('community_friendsa').style.display="none"; }
		}
	}
	
	//if($("#viewmembership").val()=='mymember')
	//{
		//$('#viewmembership').change();
	//}
	
	$("document").ready(function(){
	$("#countrySelect").change(function (){
			$.post("State.php", { country_id:$("#countrySelect").val() },
				function(data)
				{
				//alert("Data Loaded: " + data);											
					$("#stateSelect").html(data);
				});
		});
	});	
	$("document").ready(function(){
	$("#countrySelectmember").change(function (){
			$.post("State.php", { country_id:$("#countrySelectmember").val() },
				function(data)
				{
				//alert("Data Loaded: " + data);											
					$("#stateSelectmember").html(data);
				});
		});
	});		
	/*window.onload = function(){ 
			$.post("State.php", { country_id:$("#countrySelect").val() },
				function(data)
				{
						//alert("Data Loaded: " + data);											
						$("#stateSelect").html(data);
				}
			);
		}*/
		
	function selectChange()
	{
		var select2 = document.getElementById("select").value;
		window.location = "profileedit.php?sel_val="+document.getElementById("select").value +"#addressbook";
	}

	function validate()
	{
		var fname = document.getElementById("editfname");
		if(fname.value=="" || fname.value==null)
		{
			alert("Please enter First Name.")
			return false;
		}
	}

	var ContentHeight = 250;
	var TimeToSlide = 250.0;
	var openAccordion = '';

	function runAccordion(index)
	{
	  var nID = "Accordion" + index + "Content";
	  if(openAccordion == nID)
		nID = '';
		
	  setTimeout("animate(" + new Date().getTime() + "," + TimeToSlide + ",'" + openAccordion + "','" + nID + "')", 33);
	  
	  openAccordion = nID;
	}

	function animate(lastTick, timeLeft, closingId, openingId)
	{  
	  var curTick = new Date().getTime();
	  var elapsedTicks = curTick - lastTick;
	  
	  var opening = (openingId == '') ? null : document.getElementById(openingId);
	  var closing = (closingId == '') ? null : document.getElementById(closingId);
	 
	  if(timeLeft <= elapsedTicks)
	  {
		if(opening != null)
		  opening.style.height = ContentHeight + 'px';
		
		if(closing != null)
		{
		  closing.style.display = 'none';
		  closing.style.height = '0px';
		}
		return;
	  }
	 
	  timeLeft -= elapsedTicks;
	  var newClosedHeight = Math.round((timeLeft/TimeToSlide) * ContentHeight);

	  if(opening != null)
	  {
		if(opening.style.display != 'block')
		  opening.style.display = 'block';
		opening.style.height = (ContentHeight - newClosedHeight) + 'px';
	  }
	  
	  if(closing != null)
		closing.style.height = newClosedHeight + 'px';

	  setTimeout("animate(" + curTick + "," + timeLeft +",'" + closingId + "','" + openingId + "')", 33);
	}
	
	var ContentHeight_new = 115;
	var TimeToSlide = 250.0;
	var openAccordion = '';

	function runAccordion_new(index)
	{
	  var nID = "Accordion_new" + index + "Content";
	  if(openAccordion == nID)
		nID = '';
		
	  setTimeout("animate_new(" + new Date().getTime() + "," + TimeToSlide + ",'" + openAccordion + "','" + nID + "')", 33);
	  
	  openAccordion = nID;
	}

	function animate_new(lastTick, timeLeft, closingId, openingId)
	{  
	  var curTick = new Date().getTime();
	  var elapsedTicks = curTick - lastTick;
	  
	  var opening = (openingId == '') ? null : document.getElementById(openingId);
	  var closing = (closingId == '') ? null : document.getElementById(closingId);
	 
	  if(timeLeft <= elapsedTicks)
	  {
		if(opening != null)
		  opening.style.height = ContentHeight_new + 'px';
		
		if(closing != null)
		{
		  closing.style.display = 'none';
		  closing.style.height = '0px';
		}
		return;
	  }
	 
	  timeLeft -= elapsedTicks;
	  var newClosedHeight = Math.round((timeLeft/TimeToSlide) * ContentHeight_new);

	  if(opening != null)
	  {
		if(opening.style.display != 'block')
		  opening.style.display = 'block';
		opening.style.height = (ContentHeight_new - newClosedHeight) + 'px';
	  }
	  
	  if(closing != null)
		closing.style.height = newClosedHeight + 'px';

	  setTimeout("animate_new(" + curTick + "," + timeLeft +",'" + closingId + "','" + openingId + "')", 33);
	}
	
	function confirmdelete(name)
	{
		var con_del = confirm("Are You Sure You Want To UnSubscribe From  "+ name +".");
		if(con_del == false)
		{
			return false;
		}
	}
	
	function confirmdelete_addr(name,email)
	{
		var con_del = confirm("Are you sure you want to delete the contact, "+ name +" ("+email+").");
		if(con_del == false)
		{
			return false;
		}
		else if(con_del == true)
		{
			loader_body.style.display = 'block';
			all_wrap_personal.style.display = 'none';
		}
	}
	</script>
	<script type="text/javascript">
		var timeout	= 500;
		var closetimer	= 0;
		var ddmenuitem	= 0;

		// open hidden layer
		function mopen(id)
		{	
			// cancel close timer
			mcancelclosetime();

			// close old layer
			if(ddmenuitem) ddmenuitem.style.visibility = 'hidden';

			// get new layer and show it
			ddmenuitem = document.getElementById(id);
			ddmenuitem.style.visibility = 'visible';

		}
		// close showed layer
		function mclose()
		{
			if(ddmenuitem) ddmenuitem.style.visibility = 'hidden';
		}

		// go close timer
		function mclosetime()
		{
			closetimer = window.setTimeout(mclose, timeout);
		}

		// cancel close timer
		function mcancelclosetime()
		{
			if(closetimer)
			{
				window.clearTimeout(closetimer);
				closetimer = null;
			}
		}

		// close layer when click-out
		document.onclick = mclose; 
	</script>
	<script type="text/javascript">
		function link_add()
		{
			window.location=("add_contact.php");
		}
		
		function on_values()
		{
			var dataString_news = 'newsletter=on';
				
			$.ajax({
				type: 'POST',
				url : 'SubscriptionAjaxRadio.php',
				data: dataString_news,
				success: function(data) 
				{
					alert("Your settings are saved.");
				}
			});
		}
		
		function off_values()
		{
			var dataString_news = 'newsletter=off';
				
			$.ajax({
				type: 'POST',
				url : 'SubscriptionAjaxRadio.php',
				data: dataString_news,
				success: function(data) 
				{
					alert("Your settings are saved.");
				}
			});
		}
		
		function on_feeds(id)
		{
			var dataString_news = 'feeds=on&ids='+id;
				
			$.ajax({
				type: 'POST',
				url : 'SubscriptionAjaxRadio.php',
				data: dataString_news,
				success: function(data) 
				{
					alert("Your settings are saved.");
				}
			});
		}
		
		function off_feeds(id)
		{
			var dataString_news = 'feeds=off&ids='+id;
				
			$.ajax({
				type: 'POST',
				url : 'SubscriptionAjaxRadio.php',
				data: dataString_news,
				success: function(data) 
				{
					alert("Your settings are saved.");
				}
			});
		}
		
		/* function a_on_feeds(id)
		{
			var dataString_news = 'feeds=on&ids='+id;
				
			$.ajax({
				type: 'POST',
				url : 'SubscriptionAjaxRadio.php',
				data: dataString_news,
				success: function(data) 
				{
					alert("Your settings are saved.");
				}
			});
		}
		
		function a_off_feeds(id)
		{
			var dataString_news = 'feeds=off&ids='+id;
				
			$.ajax({
				type: 'POST',
				url : 'SubscriptionAjaxRadio.php',
				data: dataString_news,
				success: function(data) 
				{
					alert("Your settings are saved.");
				}
			});
		} */
		
		function on_emails(id,from,to,type)
		{
			var dataString_news = 'emails=on&ids='+id+'&from='+from+'&to='+to+'&type='+type;
				
			$.ajax({
				type: 'POST',
				url : 'SubscriptionAjaxRadio.php',
				data: dataString_news,
				success: function(data) 
				{
					alert("Your settings are saved.");
				}
			});
		}
		
		function off_emails(id,from,to,type)
		{
			var dataString_news = 'emails=off&ids='+id+'&from='+from+'&to='+to+'&type='+type;
				
			$.ajax({
				type: 'POST',
				url : 'SubscriptionAjaxRadio.php',
				data: dataString_news,
				success: function(data) 
				{
					alert("Your settings are saved.");
				}
			});
		}
		
		/* function a_on_emails(id,from,to,type)
		{
			var dataString_news = 'emails=on&ids='+id+'&from='+from+'&to='+to+'&type='+type;
				
			$.ajax({
				type: 'POST',
				url : 'SubscriptionAjaxRadio.php',
				data: dataString_news,
				success: function(data) 
				{
					alert("Your settings are saved.");
				}
			});
		}
		
		function a_off_emails(id,from,to,type)
		{
			var dataString_news = 'emails=off&ids='+id+'&from='+from+'&to='+to+'&type='+type;
				
			$.ajax({
				type: 'POST',
				url : 'SubscriptionAjaxRadio.php',
				data: dataString_news,
				success: function(data) 
				{
					alert("Your settings are saved.");
				}
			});
		} */
		
		/* $(document).ready(function()
		{			
			$('div#subscrriptions div#mediaContent div#subArtist input.chk').click(function()
			{
				//alert("hii");
				var id = $(this).attr("id");
				//alert(id);
				
				var dataString_art = 'art_chk_radio='+ id;
				
				$.ajax({
					type: 'POST',
					url : 'SubscriptionAjaxRadio.php',
					data: dataString_art,
					success: function(data) 
					{
						alert("Your settings are saved.");
					}
				});
			});
			
			$('div#subscrriptions div#mediaContent div#subCommunity input.chk').click(function()
			{
				//alert("hii");
				var id = $(this).attr("id");
				//alert(id);
				
				var dataString_art = 'art_chk_radio='+ id;
				
				$.ajax({
					type: 'POST',
					url : 'SubscriptionAjaxRadio.php',
					data: dataString_art,
					success: function(data) 
					{
						alert("Your settings are saved.");
					}
				});
			});
			
			$('div#subscrriptions div#mediaContent div#sub_Events input.chk').click(function()
			{
				//alert("hii");
				var id = $(this).attr("id");
				//alert(id);
				
				var dataString_art = 'art_chk_radio='+ id;
				
				$.ajax({
					type: 'POST',
					url : 'SubscriptionAjaxRadio.php',
					data: dataString_art,
					success: function(data) 
					{
						alert("Your settings are saved.");
					}
				});
			});
			
			$('div#subscrriptions div#mediaContent div#sub_Projects input.chk').click(function()
			{
				//alert("hii");
				var id = $(this).attr("id");
				//alert(id);
				
				var dataString_art = 'art_chk_radio='+ id;
				
				$.ajax({
					type: 'POST',
					url : 'SubscriptionAjaxRadio.php',
					data: dataString_art,
					success: function(data) 
					{
						alert("Your settings are saved.");
					}
				});
			});
		}); */
	</script>
	<script>
		$(document).ready(function(){
		  $("#toggle_div").click(function(){
		  var gets = document.getElementById("toggle_div");
			if(gets.value=='Add Post ( + )')
			{
				document.getElementById("toggle_div").value = 'Add Post ( - )';
			}
			else if(gets.value=='Add Post ( - )')
			{
				document.getElementById("toggle_div").value = 'Add Post ( + )';
			}
			$("#toogles").toggle("slow");
		  });
		});
		
		$(document).ready(function(){
		  $("#toggle_my_div").click(function(){
		  var gets = document.getElementById("toggle_my_div");
			if(gets.value=='My Posts ( + )')
			{
				document.getElementById("toggle_my_div").value = 'My Posts ( - )';
			}
			else if(gets.value=='My Posts ( - )')
			{
				document.getElementById("toggle_my_div").value = 'My Posts ( + )';
			}
			$("#toogles_my").toggle("slow");
		  });
		});
		function confirm_subscription_delete(x)
		{
			var a = confirm("Are you sure you want to unsubscribe from "+x+".");
			if(a==false){
			return(false);
			}
		}
	</script>
	<?php
	if(isset($_GET['yes']) && $_GET['yes']=="in")
	{
	?>
		<script type="text/javascript">
		$("document").ready(function(){
		alert("User Contact has been inserted.");
		});
		</script>
	<?php
	}
	if(isset($_GET['yes']) && $_GET['yes']=="up")
	{
	?>
		<script type="text/javascript">
		$("document").ready(function(){
		alert("User Contact has been updated.");
		});
		</script>
	<?php
	}
	?>
<!--<script type="text/javascript" src="./fancybox/jquery.fancybox-1.3.4.pack.js"></script>
<link rel="stylesheet" type="text/css" href="./fancybox/jquery.fancybox-1.3.4.css" media="screen" />-->
<script type="text/javascript">
//!window.jQuery && document.write('<script src="./fancybox/jquery-1.4.3.min.js"><\/script>');
	$(document).ready(function() {
				$("#popup_s3_error").fancybox({
				'width'				: '75%',
				'height'			: '70%',
				'autoScale'			: true,
				'transitionIn'		: 'none',
				'transitionOut'		: 'none',
				'type'				: 'iframe'
			});
		});
		
		$(document).ready(function() {
			$("#become_member_fancy_box").fancybox({
				helpers : {
					media : {},
					buttons : {},
					title : null
				}
			});
		});
	$(document).ready(function() {
		$("#req_pay_new").fancybox({
			helpers : {
				media : {},
				buttons : {},
				title : null
			},
			afterClose: function() { 
				$("#all_wrap_personal").css({"display":"none"});
				$("#loader_body").css({"display":"block"});
				parent.location.reload(true); 
			} 
		});
		/*$("#req_pay_new").fancybox({
			'width'				: '55%',
			'height'			: '65%',
			'autoScale'			: true,
			'transitionIn'		: 'none',
			'transitionOut'		: 'none',
			'type'				: 'iframe',
			'onClosed': function() {   
				 parent.location.reload(true); 
				}
		});*/
	});
</script>
		<!--Added files for displaying fancy box 2-->
		<script type="text/javascript" src="new_fancy_box/jquery.fancybox.js?v=2.1.4"></script>
		<link rel="stylesheet" type="text/css" href="new_fancy_box/jquery.fancybox.css?v=2.1.4" media="screen" />
		<script type="text/javascript" src="new_fancy_box/helpers/jquery.fancybox-media.js?v=1.0.5"></script>
	<!--Added files for displaying fancy box 2 Ends here-->
<div id="toplevelNav"></div>
<div id="loader_body" style="display: block; text-align: center;">
	<img src="images/ajax_loader_large.gif" style="height: 40px; position: relative; width: 40px;">
</div>
<div id="all_wrap_personal" style="display:none;">
      <div id="profileTabs">
          <ul>
           <?php
		   $fan_var = 0;
		   $member_var = 0;
		   $check=new AddContact();
		   
		   $new_artist=$newtab->artist_status();
		   $new_community=$newtab->community_status();
		   $new_team=$newtab->team_status();
		   $sql_fan_chk = mysql_query("SELECT * FROM fan_club_membership WHERE email='".$_SESSION['login_email']."' AND accept=1");
			if($sql_fan_chk!="" && $sql_fan_chk!=NULL)
			{
				if(mysql_num_rows($sql_fan_chk)>0)
				{
					while($row_fan_chk = mysql_fetch_assoc($sql_fan_chk))
					{
						$fan_var = $fan_var + 1;
					}
				}
			}
		   $gen_det = mysql_query("SELECT * FROM general_user WHERE email='".$_SESSION['login_email']."'");
			if($gen_det!="" && $gen_det!=NULL)
			{
				if(mysql_num_rows($gen_det)>0){
					$res_gen_det = mysql_fetch_assoc($gen_det);
				}
			}
		   $date = date("Y-m-d");
			$sql_member_chk = mysql_query("SELECT * FROM purify_membership WHERE general_user_id='".$res_gen_det['general_user_id']."' AND (expiry_date>'".$date."' OR lifetime=1)");
			if($sql_member_chk!="" && $sql_member_chk!=NULL)
			{
				if(mysql_num_rows($sql_member_chk)>0)
				{
					$member_var = $member_var + 1;
				}
			}
			// echo $newres1['artist_id'];
			// echo "<br/>";
			// echo $new_artist['status'];
			// echo "<br/>";
			// echo $newres1['member_id'];
			// echo "<br/>";
			// echo $newres1['community_id'];
			// echo "<br/>";
			// echo $new_community['status'];
			// echo "<br/>";
			// echo $newres1['team_id'];
			// echo "<br/>";
			// echo $new_team['status'];
			// echo "<br/>";
			// echo $fan_var;
			
		  if(($newres1['artist_id']==0 && $new_artist['status']==1) && $newres1['member_id']==0 && ($newres1['community_id']==0 && $new_community['status']==1) && ($newres1['team_id']==0 && $new_team['status']==1) && $fan_var==0 && $avail_meds_tabs_chk==0)
		  {
		  ?>
            <li class="active">HOME</li>		
            <!--<li><a href="profileedit_support.php">SUPPORT</a></li>-->
		   <?php
		   }
		   elseif(($newres1['artist_id']==0 && ($new_artist['status']!=0 ||  $new_artist['status']=="")) && ($newres1['community_id']==0 && ($new_community['status']!=0 || $new_community['status']=="")) && ($newres1['team_id']==0 && ($new_team['status']!=0 || $new_team['status']=="")) && $fan_var!=0)
		   {
		   ?>
			<li class="active">HOME</li>
			<li><a href="profileedit_media.php">Media</a></li>
            <!--<li><a href="profileedit_support.php">SUPPORT</a></li>-->
		   <?php
		   }
		    elseif(($newres1['artist_id']==0 && ($new_artist['status']!=0 ||  $new_artist['status']=="")) && ($newres1['community_id']==0 && ($new_community['status']!=0 || $new_community['status']=="")) && ($newres1['team_id']==0 && ($new_team['status']!=0 || $new_team['status']=="")) && $member_var!=0)
		   {
		   ?>
			<li class="active">HOME</li>
			<li><a href="profileedit_media.php">Media</a></li>
            <!--<li><a href="profileedit_support.php">SUPPORT</a></li>-->
		   <?php
		   }
		   elseif(($newres1['artist_id']==0 && ($new_artist['status']!=0 ||  $new_artist['status']=="")) && ($newres1['community_id']==0 && ($new_community['status']!=0 || $new_community['status']=="")) && ($newres1['team_id']==0 && ($new_team['status']!=0 || $new_team['status']=="")) && $avail_meds_tabs_chk!=0)
		   {
		   ?>
			<li class="active">HOME</li>
			<li><a href="profileedit_media.php">Media</a></li>
            <!--<li><a href="profileedit_support.php">SUPPORT</a></li>-->
		   <?php
		   }
		   elseif(($newres1['artist_id']!=0 && $new_artist['status']==0 && $new_artist['active']==0) && ($newres1['community_id']!=0 && $new_community['status']==0 && $new_community['active']==0) && ($newres1['team_id']!=0 && $new_team['status']==0))
		   {
		   ?>
		   
		   <li class="active">HOME</li>
		   <li><a href="profileedit_media.php">Media</a></li>
		   <li><a href="profileedit_artist.php">ARTIST</a></li>
		   <!--<li><a href="profileedit_community.php">COMPANY</a></li>-->
		   <li><a href="profileedit_community.php">COMPANY</a></li>
		   <li><a href="profileedit_team.php">TEAM</a></li>
           <!--<li><a href="profileedit_support.php">SUPPORT</a></li>-->
		   
		   <?php
		   }
		   elseif(($newres1['artist_id']!=0 && $new_artist['status']==0 && $new_artist['active']==0) && ($newres1['community_id']!=0 && $new_community['status']==0 && $new_community['active']==0))
		   {
		   ?>
		   <li class="active">HOME</li>	
		   <li><a href="profileedit_media.php">Media</a></li>		   
		   <li><a href="profileedit_artist.php">ARTIST</a></li>
		   <!--<li><a href="profileedit_community.php">COMPANY</a></li>-->
		   <li><a href="profileedit_community.php">COMPANY</a></li>
           <!--<li><a href="profileedit_support.php">SUPPORT</a></li>-->
		   <?php
		   }
		   elseif(($newres1['artist_id']!=0 && $new_artist['status']==0 && $new_artist['active']==0) && ($newres1['team_id']!=0 && $new_team['status']==0))
		   {
		   ?>
			<li class="active">HOME</li>	
			<li><a href="profileedit_media.php">Media</a></li>
		    <li><a href="profileedit_artist.php">ARTIST</a></li>
			<li><a href="profileedit_team.php">TEAM</a></li>
            <!--<li><a href="profileedit_support.php">SUPPORT</a></li>-->
		   <?php
		   }
		   elseif(($newres1['community_id']!=0 && $new_community['status']==0 && $new_community['active']==0) && ($newres1['team_id']!=0 && $new_team['status']==0))
		   {
		   ?>
		   <li class="active">HOME</li>
		   <li><a href="profileedit_media.php">Media</a></li>
		   <!--<li><a href="profileedit_community.php">COMPANY</a></li>-->
		   <li><a href="profileedit_community.php">COMPANY</a></li>
		   <li><a href="profileedit_team.php">TEAM</a></li>
           <!--<li><a href="profileedit_support.php">SUPPORT</a></li>-->
		   <?php
		   }
		   elseif($newres1['artist_id']!=0 && $new_artist['status']==0 && $new_artist['active']==0)
		   {?>
		    <li class="active">HOME</li>
			<li><a href="profileedit_media.php">Media</a></li>			
			<li><a href="profileedit_artist.php">ARTIST</a></li>
            <!--<li><a href="profileedit_support.php">SUPPORT</a></li>-->
		   <?php
		   }
		   elseif($newres1['member_id']!=0)
		   {
		   ?>
		   <?php
		   }
		   elseif($newres1['community_id']!=0 && $new_community['status']==0 && $new_community['active']==0)
		   {
		   ?>
		    <li class="active">HOME</li>	
			<li><a href="profileedit_media.php">Media</a></li>
			<!--<li>COMPANY</a></li>-->
			<li><a href="profileedit_community.php">COMPANY</a></li>
            <!--<li><a href="profileedit_support.php">SUPPORT</a></li>-->
		   <?php
		   }
		   elseif($newres1['team_id']!=0 && $new_team['status']==0)
		   {
		   ?>
		    <li class="active">HOME</li>
			<li><a href="profileedit_media.php">Media</a></li>
			<li><a href="profileedit_team.php">TEAM</a></li>
            <!--<li><a href="profileedit_support.php">SUPPORT</a></li>-->
`		   <?php
		   }
		   else
		   {
		   ?>
			 <li class="active">HOME</li>		
            <!--<li><a href="profileedit_support.php">SUPPORT</a></li>-->
		   <?php
		   }
			?>
          </ul>
      </div>
	<script type="text/javascript" src="/galleryfiles/jqgal.js"></script>
	<div id="personalTab">
		<div id="subNavigation" class="tabs">
			<ul class="nav">
			<!--<li><a href="#testpage" ></a></li>-->
			<!--<li><a href="#profile" class="current" ></a></li>-->
			
			<?php
				if(($newres1['artist_id']!=0 && $new_artist['status']==0 && $new_artist['active']==0) || ($newres1['community_id']!=0 && $new_community['status']==0 && $new_community['active']==0))
				{
			?>
					<li><a href="#addressbook" class="current">Addressbook</a></li>
			<?php
				}
				$check_become_mem = 0;
				$purify_member_list = $member_obj->select_purify_member();
				if($purify_member_list!="" && $purify_member_list!=NULL)
				{
					if(mysql_num_rows($purify_member_list)>0)
					{
						while($row = mysql_fetch_assoc($purify_member_list))
						{
							if($row['lifetime']==1)
							{
								$check_become_mem = 1;
							}
						}
					}
				}
			?>
			<li><a href="<?php if($check_become_mem==1){ echo "mynew_fancy.php"; }else { echo "#Become_a_member"; }?>" class="<?php if($check_become_mem==1){ echo "fancybox fancybox.ajax"; }?>" <?php if($check_become_mem==1){ ?> id="become_member_fancy_box"<?php } ?>>Become A Member </a></li>
			<li><a href="#events">Events</a></li>
			<?php
				if(($newres1['artist_id']!=0 && $new_artist['status']==0 && $new_artist['active']==0) || ($newres1['community_id']!=0 && $new_community['status']==0 && $new_community['active']==0))
				{
			?>
					<li><a href="#friends">Friends</a></li>
			<?php
				}
			?>
			<li><a href="#general">General Info </a></li>
			<?php
				if(($newres1['artist_id']!=0 && $new_artist['status']==0 && $new_artist['active']==0) || ($newres1['community_id']!=0 && $new_community['status']==0 && $new_community['active']==0))
				{
			?>
					<li><a href="compose.php">Mail</a></li>
			<?php
				}
			?>
			<li><a href="#mymemberships">My Memberships</a></li>
			<!--<li><a href="#profile">News Feeds</a></li>-->
			<?php
				if(($newres1['artist_id']!=0 && $new_artist['status']==0 && $new_artist['active']==0) || ($newres1['community_id']!=0 && $new_community['status']==0 && $new_community['active']==0))
				{
			?>
					<li><a href="#permissions">Permissions</a></li>
			<?php
				}
			?>
			<!--<li><a href="#points">Points</a></li>-->
			<li><a href="#registration">Registration</a></li>
			<li><a href="#social_connection">Social Connect</a></li>
			<li><a href="#statistics">Statistics</a></li>
			<!--<li><a href="#services">Services</a></li>-->
			<li><a href="#subscrriptions">Subscriptions</a></li>
			<!--<li><a href="#sales_distribution">Sales Profile</a></li>-->
			<li><a href="#chng_pass">Change Password</a></li>
			<li style="display:none;"><a href="#addcontact">new</a></li>
			<li style="display:none;"><a href="#addmailchimp">new</a></li>
			<li style="display:none;"><a href="#addressbook">back</a></li>
			<li style="display:none;"><a href="#addsalecards">cards</a></li>
			<?php
				if((($newres1['artist_id']!=0 && $new_artist['status']==0 && $new_artist['active']==0) || ($newres1['community_id']!=0 && $new_community['status']==0 && $new_community['active']==0)) && $member_var!=0)
				{
			?>
					<li><a href="#transactions">Transactions</a></li>
			<?php
				}
			?>
			<li style="display:none;"><a href="#welcome"></a></li>
		  </ul>
		</div>
			
			<a href="asktodonate.php" id="get_donationpop" style="display:none">fs</a>
				<?php
				$acc_event=explode(',',$newres1['taggedcommunityevents']);
				$event_avail = 0;
				for($k=0;$k<count($acc_event);$k++)
				{
					if($acc_event[$k]!="")
					{
						$event_avail = $event_avail + 1;
					}
				}

				$acc_aevent=explode(',',$newres1['taggedartistevents']);
				$event_aavail = 0;
				for($k=0;$k<count($acc_aevent);$k++)
				{
					if($acc_aevent[$k]!="")
					{
						$event_aavail = $event_aavail + 1;
					}
				}

				$acc_project = explode(',',$newres1['taggedcommunityprojects']);
				$project_avail = 0;
				for($k=0;$k<count($acc_project);$k++)
				{
					if($acc_project[$k]!="")
					{
						$project_avail = $project_avail + 1;
					}
				}

				$acc_aproject = explode(',',$newres1['taggedartistprojects']);
				$project_aavail = 0;
				for($k=0;$k<count($acc_aproject);$k++)
				{
					if($acc_aproject[$k]!="")
					{
						$project_aavail = $project_aavail + 1;
					}
				}
			?>
			<div class="list-wrap" id="list_height_incr" style="margin-bottom:30px;">
				<div class="subTabs" id="social_connection" style="display:none;" name="social_connection" >
					<h1>Connect</h1>
					<div id="mediaContent">
                        <div class="topLinks">
                            <div class="links" style="width:665px;">
                                <ul>
                                    <li>
									<?php
										if (!class_exists('\user_keys'))
										{
											require_once($_SERVER["DOCUMENT_ROOT"].'/classes/user_keys.php');
										}
										require_once("facebook/facebook.php");
										$config = array();
                                        $sgk = $uk->get_UserKeys("Facebook"); 
										$config['appId'] = $sgk['clientid'];
										$config['secret'] = $sgk['clientsecret'];
										$fb = new Facebook($config);
										$params = array(
											'scope' => 'email,user_likes,publish_actions,publish_stream,read_stream,manage_pages,rsvp_event',
											'redirect_uri' => 'http://purifycopy.com/get_facebook_code.php'
										);
										$loginUrl = $fb->getLoginUrl($params);
										?>
										<a href="<?php echo $loginUrl; ?>">
											<img src="images/facebook.png"/>
										</a>
										
										
										<!--<a href="https://www.facebook.com/dialog/oauth?client_id=1451572388433988&redirect_uri=http://purifycopy.com/get_facebook_code.php&scope=email,publish_actions,read_stream">
											<img src="images/facebook.png"/>
										</a>-->
									</li>
									<li>
										<?php
											require_once('twitter/config.php');
										?>
										<a href="twitter/redirect.php"><img src="images/twitter.png" alt="Sign in with Twitter"/></a>
									</li>
									<li>
										<a style="color: rgb(255, 255, 255); font-family: 'futura-pt',sans-serif; font-size: 0.825em; font-weight: bold; padding: 6px 10px; float: left; margin-right: 10px; border: 0px none; background-color: rgb(0, 0, 0);text-decoration:none;border-radius:5px;" href="mywebsite_data.php" class="fancybox fancybox.ajax" id="mywebsite">My Website</a>
										<script type="text/javascript">
											$(document).ready(function() {
												$("#mywebsite").fancybox({
													helpers : {
														media : {},
														buttons : {},
														title : null,
					 								}
												});
											});
										</script>
									</li>
                                </ul>
							</div>
							<h2>Connected Accounts</h2>
							<div>
							<?php
							$get_fb_accounts = $social_obj -> select_facebook_accounts($_SESSION['login_id']);
							if(!empty($get_fb_accounts)){
								for($social_count=0;$social_count<count($get_fb_accounts);$social_count++){
									?>
									<div class="social_account_name"><img src="images/icon-facebook.png"/><?php echo $get_fb_accounts[$social_count]['acc_name']; if($get_fb_accounts[$social_count]['fb_id']==0){ echo "(Timeline)"; } ?></div>
									<div class="social_account_image"><a href="get_facebook_code.php?activate_me=<?php echo $get_fb_accounts[$social_count]['id']; ?>&status=<?php if($get_fb_accounts[$social_count]['status']==0){ echo "1"; }else{ echo "0"; }  ?>"><img src="images/profile/<?php if($get_fb_accounts[$social_count]['status']==0){ echo "decline.png"; }else { echo "accept.png"; } ?>"/></div>
									<div class="social_disconnect"><a href="remove_social_account.php?fb_remove=<?php echo $get_fb_accounts[$social_count]['id'];?>"><img src="images/disconnect.png"/></a></div>
									<?php
								}
							}
							
							$get_twit_accounts = $social_obj -> select_twitter_accounts($_SESSION['login_id']);
							if(!empty($get_twit_accounts)){
								for($social_count_twit=1;$social_count_twit<=$get_twit_accounts['count(*)'];$social_count_twit++){
									?>
									<div class="social_account_name"><img src="images/icon-twitter.png"/><?php echo $get_twit_accounts['acc_name']; ?></div>
									<div class="social_account_image"><a href="remove_social_account.php?activate_me=<?php echo $get_twit_accounts['id']; ?>&status=<?php if($get_twit_accounts['status']==0){ echo "1"; }else{ echo "0"; }  ?>"><img src="images/profile/<?php if($get_twit_accounts['status']==0){ echo "decline.png"; }else { echo "accept.png"; } ?>"/></div>
									<div class="social_disconnect"><a href="remove_social_account.php?twit_remove=<?php echo $get_twit_accounts['id'];?>"><img src="images/disconnect.png"/></a></div>
									<?php
								}
							}
							$get_my_website = $social_obj -> select_mywebsite($_SESSION['login_id']);
							if(!empty($get_my_website['domain_name'])){
								?>
								<div class="social_account_name"><img src="images/website-icon.png"/><?php echo $get_my_website['domain_name']; ?></div>
								<div class="social_account_image"><a href=""><img src="images/profile/accept.png"/></div>
								<div class="social_disconnect"><a href="remove_social_account.php?myweb_remove=<?php echo $_SESSION['login_id'];?>"><img src="images/disconnect.png"/></a></div>
								<?php
							}
							?>
							</div>
						</div>
					</div>
					
				</div>
				
                <div class="subTabs" id="addressbook" style="display:none;" name="addressbook" >
                	<h1>Addressbook</h1>
                    <div id="mediaContent">
                        <div class="topLinks">
                            <div class="links" style="width:665px;">
                                <ul>
									<li><a href ="#addmailchimp" style ="color: rgb(255, 255, 255); font-family: 'futura-pt',sans-serif; font-size: 0.825em; font-weight: bold; padding: 6px 10px; float: left; margin-right: 10px; border: 0px none; background-color: rgb(0, 0, 0);text-decoration:none;border-radius:5px;">Mailchimp Sync</a></li>
                                    <li><a href ="#addcontact" style ="color: rgb(255, 255, 255); font-family: 'futura-pt',sans-serif; font-size: 0.825em; font-weight: bold; padding: 6px 10px; float: left; margin-right: 10px; border: 0px none; background-color: rgb(0, 0, 0);text-decoration:none;border-radius:5px;">AddContact</a></li>
                                </ul>
								<ul style="padding-top:6px;">
									<label id="count_contcts">Total Contact: </label>
								</ul>
								<!--<select class="eventdrop" id="select" onChange="selectChange()" style="float:right;">
									<!--<option value="select">Select</option>
									<option value="all contacts" <?php /*if($_GET['sel_val']=='all contacts') { echo "Selected"; } ?> >All Contacts</option>
                                    <option value="Artist" <?php if($_GET['sel_val']=='Artist') { echo "Selected"; } ?> >Artist</option>
									<option value="Community" <?php if($_GET['sel_val']=='Community') { echo "Selected"; } ?> >Community</option>
                                    <option value="General" <?php if($_GET['sel_val']=='General') { echo "Selected"; } ?> >General</option>
									<option value="artist_fans" <?php if($_GET['sel_val']=='artist_fans') { echo "Selected"; } ?> >Artist Fans</option>
									<option value="community_fans" <?php if($_GET['sel_val']=='community_fans') { echo "Selected"; } */?> >Community Fans</option>
									<!--<option value="unregistered contacts">Unregistered Contacts</option>
								</select>-->

                            </div>
								<?php
									if(isset($_GET['delete'])=="true")
									{
								?>
                                    <label style="color:red">Your Data Is Succesfullly Deleted.</label>
								<?php
									}
									if(isset($_GET['add'])=='true')
									{
								?>
								     <label style="color:red">Your Data Is Succesfullly Added.</label>
								<?php
									}
									if(isset($_GET['update'])=='true')
									{
									?>
								     <label style="color:red">Your Data Is Succesfullly Updated.</label>
								<?php
									}
									//if(isset($_GET['add'])=='false')
									//{
									?>	
									<!--<label style="color:red">Your Data Was Not Inserted.Please Try Again.</label>-->
									<?php
									//}
									?>
                        </div>
                        <div class="titleCont">
						<?php 
						$order_name="";
						if(isset($_GET['sel_val']))
						{
							$sel_val=$_GET['sel_val'];	
						}
						else
						{
							$sel_val = "all contacts";
						}
						
						if(isset($_GET['sort_name']))
						{
							$order_name=$_GET['sort_name']; 
						}
						else
						{
							$order_name = 'ASC';
						}
						if($order_name=="" || $order_name=='DESC')
						{
						?>
							 <div class="blkA"><a href="?sel_val=<?php if(isset($_GET['sel_val'])) { echo $sel_val; } else {echo "all contacts";  } ?>&sort_name=<?php  echo "ASC"; ?>#addressbook">Name</a></div>
						<?php
						}
						else
						{
						?>
							<div class="blkA"><a href="?sel_val=<?php if(isset($_GET['sel_val'])) { echo $sel_val; } else {echo "all contacts";  } ?>&sort_name=<?php echo "DESC"; ?>#addressbook">Name</a></div>
						<?php
						}
						$order_email="";
						if(isset($_GET['sort_email']))
						{
							$order_email=$_GET['sort_email']; 
							//echo $order;
						}
						if($order_email=="" || $order_email=='DESC')
						{
						?>
							 <div class="blkA"><a href="?sel_val=<?php if(isset($_GET['sel_val'])) { echo $sel_val; } else {echo "all contacts";  } ?>&sort_email=<?php  echo "ASC"; ?>#addressbook">Email</a></div>
						<?php
						}
						else
						{
						?>
							<div class="blkA"><a href="?sel_val=<?php if(isset($_GET['sel_val'])) { echo $sel_val; } else {echo "all contacts";  } ?>&sort_email=<?php echo "DESC"; ?>#addressbook">Email</a></div>
						<?php
						}
						$order_category="";
						if(isset($_GET['sort_category']))
						{
							$order_category=$_GET['sort_category']; 
							//echo $order;
						}
						if($order_category=="" || $order_category=='DESC')
						{
						?>
							 <!--<div class="blkG"><a href="?sel_val=<?php /*if(isset($_GET['sel_val'])) { echo $sel_val; } else {echo "all contacts";  } ?>&sort_category=<?php  echo "ASC"; */?>#addressbook">Category</a></div>-->
						<?php
						}
						else
						{
						?>
							<!--<div class="blkG"><a href="?sel_val=<?php /*if(isset($_GET['sel_val'])) { echo $sel_val; } else {echo "all contacts";  } ?>&sort_category=<?php echo "DESC"; */?>#addressbook">Category</a></div>-->
						<?php
						}
						?>
						
						
                            
                            <!--<div class="blkA">Email</div>-->
                            <!--<div class="blkG">Category</div>-->
							<div class="blkG"><a href="#">Actions</a></div>
                        </div>
				<?php 
				$all_fan_addr = "";
				/*Code for displaying all Projects*/
							$getproject=$all_stats->GetProject();
							$getproject_community=$all_stats->selcommunity_project();
							$sel_general=$all_stats->get_gen_info();
							$acc_project=explode(',',$sel_general['taggedartistprojects']);
							$project_avail = 0;
							for($k=0;$k<count($acc_project);$k++)
							{
								if($acc_project[$k]!="")
								{
									$project_avail = $project_avail + 1;
								}
							}

							$acc_cproject = explode(',',$sel_general['taggedcommunityprojects']);
							$project_cavail = 0;
							for($k=0;$k<count($acc_cproject);$k++)
							{
								if($acc_cproject[$k]!="")
								{
									$project_cavail = $project_cavail + 1;
								}
							}
								$all_artist_project = array();
								if($getproject!="" && $getproject!=NULL)
								{
									while($showproject=mysql_fetch_assoc($getproject))
									{
										$showproject['whos_project'] = "own_artist";
										$showproject['id'] = $showproject['id'] ."~art";
										$all_artist_project[] =  $showproject;
									}
								}
								$all_friends_project = array();
								$get_all_friends = $all_stats->Get_all_friends();
								if($get_all_friends !="" && $get_all_friends !=NULL)
								{
									while($res_all_friends = mysql_fetch_assoc($get_all_friends))
									{
										$get_friend_art_info = $all_stats->get_friend_art_info($res_all_friends['fgeneral_user_id']);
										$get_all_tag_pro = $all_stats->get_all_tagged_pro($get_friend_art_info);
										if($get_all_tag_pro!="" && $get_all_tag_pro!=NULL)
										{
											if(mysql_num_rows($get_all_tag_pro)>0)
											{
												while($res_projects = mysql_fetch_assoc($get_all_tag_pro))
												{
													//$get_project_type = $all_stats->Get_Project_subtype($res_projects['id']);
													$exp_creator = explode('(',$res_projects['creator']);
													$exp_tagged_email = explode(",",$res_projects['tagged_user_email']);
													for($exp_count = 0;$exp_count<count($exp_tagged_email);$exp_count++)
													{
														if($exp_tagged_email[$exp_count]==""){continue;}
														else{
															if($exp_tagged_email[$exp_count]==$_SESSION['login_email'])
															{
																//$res_projects['project_type_name'] = $get_project_type['name'];
																$res_projects['whos_project'] = "tag_pro_art";
																$res_projects['id'] = $res_projects['id'] ."~art";
																$all_friends_project[] = $res_projects;
															}
														}
														
													}
												}
											}
										}
									}
								}

							/***Projects From Community Starts Here ***/
							$all_community_project = array();
							if($getproject_community!="" && $getproject_community!=NULL)
							{
								if(mysql_num_rows($getproject_community)>0)
								{
									while($showproject=mysql_fetch_assoc($getproject_community))
									{
										$exp_creator= explode('(',$showproject['creator']);
										$showproject['whos_project'] = "own_community";
										$showproject['id'] = $showproject['id'] ."~com";
										$all_community_project[] = $showproject;
									}
								}
							}
							$all_community_friends_project = array();
							$get_all_friends = $all_stats->Get_all_friends();
							if($get_all_friends!="" && $get_all_friends!=NULL)
							{
								while($res_all_friends = mysql_fetch_assoc($get_all_friends))
								{
									$get_friend_art_info = $all_stats->get_friend_community_info($res_all_friends['fgeneral_user_id']);
									$get_all_tag_pro = $all_stats->get_all_tagged_community_pro($get_friend_art_info);
									if($get_all_tag_pro!="" && $get_all_tag_pro!=NULL)
									{
										if(mysql_num_rows($get_all_tag_pro)>0)
										{
											while($res_projects = mysql_fetch_assoc($get_all_tag_pro))
											{
												//$get_project_type = $all_stats->Get_Project_subtype($res_projects['id']);
												$exp_creator = explode('(',$res_projects['creator']);
												$exp_tagged_email = explode(",",$res_projects['tagged_user_email']);
												for($exp_count = 0;$exp_count<count($exp_tagged_email);$exp_count++)
												{
													if($exp_tagged_email[$exp_count]==""){continue;}
													else{
														if($exp_tagged_email[$exp_count]==$_SESSION['login_email'])
														{
															//$res_projects['project_type_name'] = $get_project_type['name'];
															$res_projects['whos_project'] = "tag_pro_com";
															$res_projects['id'] = $res_projects['id'] ."~com";
															$all_community_friends_project[] = $res_projects;
														}
													}
													
												}
											}
										}
									}
								}
							}
							$get_onlycre_id = array();
							$get_onlycre_id = $all_stats->only_creator_this();

							$get_only2cre = array();
							//$get_only2cre = $all_stats->only_creator2pr_this();
							$get_only2cre = $all_stats->only_creator2pr_othis();
							$sel_art2tagged = array();
							if($project_avail!=0)
							{
								$acc_project = array_unique($acc_project);
								for($acc_pro=0;$acc_pro<count($acc_project);$acc_pro++)
								{												
									if($acc_project[$acc_pro]!="" && $acc_project[$acc_pro]!=0)
									{													
										$sql_1_cre = $all_stats->get_project_tagged_by_other2_user($acc_project[$acc_pro]);
										if($sql_1_cre!="" && $sql_1_cre!=NULL)
										{
											if(mysql_num_rows($sql_1_cre)>0)
											{
												while($run = mysql_fetch_assoc($sql_1_cre))
												{
													if($newres1['artist_id']!=$run['artist_id'])
													{
														$run['whos_project'] = "art_creator_tag_pro";
														$run['id'] = $run['id'].'~'.'art';
														$sel_art2tagged[] = $run;
													}
												}
											}
										}
										
										$sql_2_cre = $all_stats->get_project_ctagged_by_other2_user($acc_project[$acc_pro]);
										if($sql_2_cre!="" && $sql_2_cre!=NULL)
										{
											if(mysql_num_rows($sql_2_cre)>0)
											{
												while($run_c = mysql_fetch_assoc($sql_2_cre))
												{
													if($newres1['community_id']!=$run_c['community_id'])
													{
														$run_c['whos_project'] = "com_creator_tag_pro";
														$run_c['id'] = $run_c['id'].'~'.'com';
														$sel_art2tagged[] = $run_c;
													}
												}
											}
										}
									}
								}
							}

							$sel_com2tagged = array();
							if($project_cavail!=0)
							{
								$acc_cproject = array_unique($acc_cproject);
								for($acc_pro=0;$acc_pro<count($acc_cproject);$acc_pro++)
								{
									if($acc_cproject[$acc_pro]!="" && $acc_cproject[$acc_pro]!=0)
									{
										$sql_1_cre = $all_stats->get_cproject_tagged_by_other2_user($acc_cproject[$acc_pro]);
										if($sql_1_cre!="" && $sql_1_cre!=NULL)
										{
											if(mysql_num_rows($sql_1_cre)>0)
											{
												while($run = mysql_fetch_assoc($sql_1_cre))
												{
													if($newres1['artist_id']!=$run['artist_id'])
													{
														$run['whos_project'] = "art_creator_tag_pro";
														$run['id'] = $run['id'].'~art';
														$sel_com2tagged[] = $run;
													}
												}
											}
										}
										
										$sql_2_cre = $all_stats->get_cproject_ctagged_by_other2_user($acc_cproject[$acc_pro]);
										if($sql_2_cre!="" && $sql_2_cre!=NULL)
										{
											if(mysql_num_rows($sql_2_cre)>0)
											{
												while($run_c = mysql_fetch_assoc($sql_2_cre))
												{
													if($newres1['community_id']!=$run_c['community_id'])
													{
														$run_c['whos_project'] = "com_creator_tag_pro";
														$run_c['id'] = $run_c['id'].'~com';
														$sel_com2tagged[] = $run_c;
													}
												}
											}
										}
									}
								}
							}

							$sel_arttagged = array();
							if($project_avail!=0)
							{
								$acc_project = array_unique($acc_project);
								for($acc_pro=0;$acc_pro<count($acc_project);$acc_pro++)
								{
									if($acc_project[$acc_pro]!="" && $acc_project[$acc_pro]!=0)
									{
										$dum_art = $all_stats->get_project_tagged_by_other_user($acc_project[$acc_pro]);
										if(!empty($dum_art) && $dum_art['id']!="" && $dum_art['id']!="")
										{
											if($newres1['artist_id']!=$dum_art['artist_id'])
											{
												$dum_art['id'] = $dum_art['id'].'~art';
												$dum_art['whos_project'] = "tag_pro_art";
												$sel_arttagged[] = $dum_art;
											}
										}
									}
								}
							}

							$sel_comtagged = array();
							if($project_cavail!=0)
							{
								$acc_cproject = array_unique($acc_cproject);
								for($acc_pro=0;$acc_pro<count($acc_cproject);$acc_pro++)
								{
									if($acc_cproject[$acc_pro]!="" && $acc_cproject[$acc_pro]!=0)
									{
										$dum_com = $all_stats->get_cproject_tagged_by_other_user($acc_cproject[$acc_pro]);
										if(!empty($dum_com) && $dum_com['id']!="" && $dum_com['id']!="")
										{
											if($newres1['community_id']!=$dum_com['community_id'])
											{
												$dum_com['id'] = $dum_com['id'].'~com';
												$dum_com['whos_project'] = "tag_pro_com";
												$sel_comtagged[] = $dum_com;
											}
										}
									}
								}
							}

							$all_combine_projects = array_merge($all_artist_project,$all_friends_project,$all_community_project,$all_community_friends_project,$get_onlycre_id,$sel_comtagged,$sel_arttagged,$get_only2cre,$sel_com2tagged,$sel_art2tagged);

							$get_all_del_id = $all_stats->get_deleted_project_id();
							$exp_del_id = explode(",",$get_all_del_id['deleted_project_id']);
							for($count_all=0;$count_all<count($all_combine_projects);$count_all++){
								for($count_del=0;$count_del<count($exp_del_id);$count_del++){
									if($exp_del_id[$count_del]!=""){
										if($all_combine_projects[$count_all]['id'] == $exp_del_id[$count_del]){
											$all_combine_projects[$count_all] ="";
										}
									}
								}
							}
							$dumpsa_pros = array();
							$dumpsc_pros = array();
							for($count_all=0;$count_all<count($all_combine_projects);$count_all++)
							{
								if($all_combine_projects[$count_all] !="")
								{
									$chk_ac_ep = 0;
									if(isset($all_combine_projects[$count_all]['artist_id']))
									{
										$get_project_type = $all_stats->Get_Project_subtype($all_combine_projects[$count_all]['id']);
										if(in_array($all_combine_projects[$count_all]['id'],$dumpsa_pros))
										{
											$chk_ac_ep = 1;
										}
										else
										{
											$dumpsa_pros[] = $all_combine_projects[$count_all]['id'];
										}
									}
									elseif(isset($all_combine_projects[$count_all]['community_id']))
									{
										$get_project_type = $all_stats->Get_ACProject_subtype($all_combine_projects[$count_all]['id']);
										if(in_array($all_combine_projects[$count_all]['id'],$dumpsc_pros))
										{
											$chk_ac_ep = 1;
										}
										else
										{
											$dumpsc_pros[] = $all_combine_projects[$count_all]['id'];
										}
									}
									if($chk_ac_ep==1)
									{}
									else
									{
										$all_fan_addr = $all_fan_addr.','.$all_combine_projects[$count_all]['id'];
										
									}
								}
							}
							/*Code for displaying all Projects Ends here*/
							$ids_songs_alls = "";
						$get_song_id = array();
						$get_community_song_id = $newgeneral->get_community_song_id();
						while($get_song_ids = mysql_fetch_assoc($get_community_song_id))
						{
							$get_song_id[] = $get_song_ids;
						}
						
						$get_media_tag_art_pro = $newgeneral->get_artist_project_tag($_SESSION['login_email']);
						if(mysql_num_rows($get_media_tag_art_pro)>0)
						{
							$is = 0;
							while($art_pro_tag = mysql_fetch_assoc($get_media_tag_art_pro))
							{
								$get_songs = explode(",",$art_pro_tag['tagged_songs']);
								for($count_art_tag_song=0;$count_art_tag_song<count($get_songs);$count_art_tag_song++)
								{
									if($get_songs[$count_art_tag_song]!="")
									{
										$get_songs_new[$is] = $get_songs[$count_art_tag_song];
										$is = $is + 1;
									}
								}
							}
							if(!empty($get_songs_new))
							{
								$get_songs_n = array_unique($get_songs_new);
							}
						}
						
						$get_media_tag_art_eve = $newgeneral->get_artist_event_tag($_SESSION['login_email']);
						if(mysql_num_rows($get_media_tag_art_eve)>0)
						{
							$is = 0;
							while($art_pro_tag = mysql_fetch_assoc($get_media_tag_art_eve))
							{
								$get_songs = explode(",",$art_pro_tag['tagged_songs']);
								for($count_art_tag_song=0;$count_art_tag_song<count($get_songs);$count_art_tag_song++)
								{
									if($get_songs[$count_art_tag_song]!="")
									{
										$get_songs_newe[$is] = $get_songs[$count_art_tag_song];
										$is = $is + 1;
									}
								}
							}
							if(!empty($get_songs_newe))
							{
								$get_songs_edn = array_unique($get_songs_newe);
							}
						}
						
						$get_media_tag_com_pro = $newgeneral->get_community_project_tag($_SESSION['login_email']);
						if(mysql_num_rows($get_media_tag_com_pro)>0)
						{
							$is_c = 0;
							while($com_pro_tag = mysql_fetch_assoc($get_media_tag_com_pro))
							{
								$get_com_songs = explode(",",$com_pro_tag['tagged_songs']);
								for($count_com_tag_song=0;$count_com_tag_song<count($get_com_songs);$count_com_tag_song++)
								{
									if($get_com_songs[$count_com_tag_song]!="")
									{
										$get_songs_new_c[$is_c] = $get_com_songs[$count_com_tag_song];
										$is_c = $is_c + 1;
									}
								}
							}
							if(!empty($get_songs_new_c))
							{
								$get_songs_n_c = array_unique($get_songs_new_c);
							}
						}
						
						$get_media_tag_com_eve = $newgeneral->get_community_event_tag($_SESSION['login_email']);
						if(mysql_num_rows($get_media_tag_com_eve)>0)
						{
							$is_c = 0;
							while($com_pro_tag = mysql_fetch_assoc($get_media_tag_com_eve))
							{
								$get_com_songs = explode(",",$com_pro_tag['tagged_songs']);
								for($count_com_tag_song=0;$count_com_tag_song<count($get_com_songs);$count_com_tag_song++)
								{
									if($get_com_songs[$count_com_tag_song]!="")
									{
										$get_sonegs_new_c[$is_c] = $get_com_songs[$count_com_tag_song];
										$is_c = $is_c + 1;
									}
								}
							}
							if(!empty($get_sonegs_new_c))
							{
								$get_songs_en_ec = array_unique($get_sonegs_new_c);
							}
						}
						
						$get_media_cre_art_pro = $newgeneral->get_project_create();
						if(!empty($get_media_cre_art_pro))
						{
							//while($art_pro_tag = mysql_fetch_assoc($get_media_cre_art_pro))
							$is_p = 0;
							for($cr_i=0;$cr_i<count($get_media_cre_art_pro);$cr_i++)
							{
								$get_songs = explode(",",$get_media_cre_art_pro[$cr_i]['tagged_songs']);
								for($count_art_cr_song=0;$count_art_cr_song<count($get_songs);$count_art_cr_song++)
								{
									if($get_songs[$count_art_cr_song]!="")
									{
										$get_songs_pn[$is_p] = $get_songs[$count_art_cr_song];
										$is_p = $is_p + 1;
									}
								}
							}	
							if(!empty($get_songs_pn))
							{
								$get_songs_p_new = array_unique($get_songs_pn);
							}
						}	
						
						if(!isset($get_songs_n))
						{
							$get_songs_n = array();
						}
						
						if(!isset($get_songs_en_ec))
						{
							$get_songs_en_ec = array();
						}
						
						if(!isset($get_songs_edn))
						{
							$get_songs_edn = array();
						}
						
						if(!isset($get_songs_n_c))
						{
							$get_songs_n_c = array();
						}
						
						if(!isset($get_songs_p_new))
						{
							$get_songs_p_new = array();
						}
						
						$new_medi = $newgeneral->get_creator_heis();
						$new_creator_creator_media_array = array();
						if(!empty($new_medi))
						{
							for($count_cre_m=0;$count_cre_m<count($new_medi);$count_cre_m++)
							{
								if($new_medi[$count_cre_m]['media_type']==114){
									$new_creator_creator_media_array[] = $new_medi[$count_cre_m];
								}
							}
						}
						
						$new_medi_from = $newgeneral->get_from_heis();
						$new_from_from_media_array = array();
						if(!empty($new_medi_from))
						{
							for($count_cre_m=0;$count_cre_m<count($new_medi_from);$count_cre_m++)
							{
								if($new_medi_from[$count_cre_m]['media_type']==114){
								$new_from_from_media_array[] = $new_medi_from[$count_cre_m];
								}
							}
						}
						
						$new_medi_tagged = $newgeneral->get_the_user_tagin_project($_SESSION['login_email']);
						$new_tagged_from_media_array = array();
						if(!empty($new_medi_tagged))
						{
							for($count_cre_m=0;$count_cre_m<count($new_medi_tagged);$count_cre_m++)
							{
								if($new_medi_tagged[$count_cre_m]['media_type']==114){
								$new_tagged_from_media_array[] = $new_medi_tagged[$count_cre_m];
								}
							}
						}
						
						$get_where_creator_media = $newgeneral->get_media_where_creator_or_from();
						$new_creator_media_array = array();
						if(!empty($get_where_creator_media))
						{
							for($count_tag_m=0;$count_tag_m<count($get_where_creator_media);$count_tag_m++)
							{
								if($get_where_creator_media[$count_tag_m]['media_type']==114){
									$new_creator_media_array[] = $get_where_creator_media[$count_tag_m];
								}
							}
						}
						
						$get_songs_pnsew_w = array_merge($get_songs_n,$get_songs_n_c,$get_songs_p_new,$get_songs_edn,$get_songs_en_ec);
										
						$get_songs_pnew_ct = array_unique($get_songs_pnsew_w);
						$get_songs_final_ct = array();
						
						for($count_art_cr_song=0;$count_art_cr_song<count($get_songs_pnew_ct);$count_art_cr_song++)
						{
							if($get_songs_pnew_ct[$count_art_cr_song]!="")
							{
								$get_media_tag_art_pro_song = $newgeneral->get_tag_media_info($get_songs_pnew_ct[$count_art_cr_song]);
								$get_songs_final_ct[] = $get_media_tag_art_pro_song;
							}
						}
						
						$res = $check->globe();
						$acc_media=explode(',',$res['accepted_media_id']);

						$media_avail = 0;

						for($k=0;$k<count($acc_media);$k++)
						{
							if($acc_media[$k]!="")
							{
								$media_avail = $media_avail + 1;
							}
						}
						
						$acc_medss = array();
						for($acc_vis=0;$acc_vis<count($acc_media);$acc_vis++)
						{
							if($acc_media[$acc_vis]!="")
							{
								$sels_tagged = $newgeneral->get_community_media_type_tagged_by_other_user($acc_media[$acc_vis]);
								if(!empty($sels_tagged))
								{
									
									$acc_medss[] = $sels_tagged;
								}
							}
						}
						
						$get_songs_final_ct = array();
						$type = '114';
						$cre_frm_med = array();
						$cre_frm_med = $newgeneral->get_media_create($type);
						
						$get_songs_pnew_w = array_merge($acc_medss,$get_song_id,$get_songs_final_ct,$cre_frm_med,$new_creator_creator_media_array,$new_from_from_media_array,$new_tagged_from_media_array,$new_creator_media_array);
						
						for($acc_song1=0;$acc_song1<count($get_songs_pnew_w);$acc_song1++)
						{
							if($get_songs_pnew_w[$acc_song1]['id']!="" && $get_songs_pnew_w[$acc_song1]['id']!=0)
							{
								$get_artist_song1 = $newgeneral->get_community_song($get_songs_pnew_w[$acc_song1]['id']);
								$getsong1 = mysql_fetch_assoc($get_artist_song1);
								$ids = $get_songs_pnew_w[$acc_song1]['id'];
								$get_songs_pnew_w[$acc_song1]['track'] = $getsong1['track'];
							}
						}
						
						if(!empty($get_songs_pnew_w))
						{
							$dummy_s = array();
							$get_all_del_id = $newgeneral->get_all_del_media_id();
							$deleted_id = explode(",",$get_all_del_id);
							for($count_del=0;$count_del<=count($get_songs_pnew_w);$count_del++)
							{
								if($get_songs_pnew_w[$count_del]==""){continue;}
								else{
									for($count_deleted=0;$count_deleted<=count($deleted_id);$count_deleted++)
									{
										if($deleted_id[$count_deleted]==""){continue;}
										else{
											if($deleted_id[$count_deleted] == $get_songs_pnew_w[$count_del]['id'])
											{
												$get_songs_pnew_w[$count_del]="";
											}
										}
									}
								}
							}
							for($acc_song=0;$acc_song<count($get_songs_pnew_w);$acc_song++)
							{
								if($get_songs_pnew_w[$acc_song]['delete_status']==0)
								{
									if(isset($get_songs_pnew_w[$acc_song]['id']))
									{
										if(in_array($get_songs_pnew_w[$acc_song]['id'],$dummy_s))
										{
											
										}
										else
										{
											$dummy_s[] = $get_songs_pnew_w[$acc_song]['id'];
											if($get_songs_pnew_w[$acc_song]['id']!="" && $get_songs_pnew_w[$acc_song]['media_type']==114)
											{
												$ids_songs_alls = $ids_songs_alls.','.$get_songs_pnew_w[$acc_song]['id'];
											}
										}
									}
								}
							}
						}
				
				if(isset($_GET['sel_val'])) 
				{
						//echo $_GET['sel_val']; ?>
						<?php
						
							
							$res=$check->globe();
							$general_id=$res['general_user_id'];
					if($_GET['sel_val']=="all contacts")
					{
							$rs=$check->allContacts($general_id,$order_name,$order_email,$order_category);
							$fans_add = $check->get_all_fans();
							$resultArray = Array();
							$resultArray_fans = Array();
							
							if($rs!="" && $rs!=NULL)
							{
								while ($row = mysql_fetch_assoc($rs)) 
								{
									$resultArray[] = $row;
								}
							}
							
							if($fans_add!="" && $fans_add!=NULL)
							{
								while ($row_fan = mysql_fetch_assoc($fans_add)) 
								{
									$resultArray_fans[] = $row_fan;
								}
							}
							//$resultArray = array_unique($resultArray);
							//$resultArray_fans = array_unique($resultArray_fans);
							?>
							<div style="height:310px; overflow-y:auto; overflow-x:hidden; width:665px;">
							<?php
						for($u=0;$u<count($resultArray);$u++)
						{ 
								$userData = $resultArray[$u];
						?>
                        <div class="tableCont" id="AccordionContainer" rel="text">
                            <div class="blkA"><a href=""><?php if($userData['name']=="" || $userData['name']==null){echo "fsfs";}else{ echo $userData['name']; } ?></a></div>
                            <div class="blkA"><a href=""><?php if($userData['email']==""){echo "&nbsp;";}else{ echo $userData['email']; } ?></a></div>
                            <!--<div class="blkG"><?php //if($userData['category']==""){echo "&nbsp;";}else{ echo $userData['category']; } ?></div>-->
							<div class="icon"><a href="compose.php?address_book_email=<?php echo $userData['email']; ?>"><img src="images/profile/mail.png" /></a></div>
                            <div class="icon"><a href="?edit=<?php echo $userData['id'];?>#addcontact"><img src="images/profile/edit.png" /></a></div>
                            <div class="icon"><a href="adduser.php?delete=<?php echo $userData['id'];?>"><img src="images/profile/delete.png" onclick="return confirmdelete('<?php echo $userData['name']; ?>')" /></a></div> 
                        </div>
						
						<?php
						}
						for($u_f=0;$u_f<count($resultArray_fans);$u_f++)
						{ 
								$userData_fan = $resultArray_fans[$u_f];
						
						?>
						
                        <div class="tableCont" id="AccordionContainer">
                            <div class="blkA"><a href=""><?php if($userData_fan['name']=="" || $userData_fan['name']==null){ echo "&nbsp;"; }else{ echo $userData_fan['name']; } ?></a></div>
                            <div class="blkA"><a href=""><?php if($userData_fan['email']=="" || $userData_fan['email']==null){ echo "&nbsp;"; }else{ echo $userData_fan['email'];} ?></a></div>
                            <!--<div class="blkG"><?php /*if($userData_fan['related_type']=="" || $userData_fan['related_type']==null){ echo "&nbsp;"; }else{ echo $userData_fan['related_type']; }*/ ?></div>-->
							<div class="icon"><a href="compose.php?address_book_email=<?php echo $userData_fan['email']; ?>"><img src="images/profile/mail.png" /></a></div>
                        </div>
						
						<?php
						}
						?>
						</div>
						<?php
						
					}
					
					if($_GET['sel_val']=="Artist")
					{
							$filter="artist";
							$rs=$check->filterContacts($general_id,$filter,$order_name,$order_email,$order_category);
							//$rs=mysql_query($sql);
							$resultArray = Array();
							if($rs!="" && $rs!=NULL)
							{
								while ($row = mysql_fetch_assoc($rs)) 
								{
									$resultArray[] = $row;
								}
							}
							//$resultArray = array_unique($resultArray);
							?>
							<div style="height:310px; overflow-y:auto; overflow-x:hidden; width:665px;">
							<?php
							for($u=0;$u<count($resultArray);$u++)
							{ 
								$userData = $resultArray[$u];
						
						?>
                        <div class="tableCont" id="AccordionContainer">
                            <div class="blkA"><a href=""><?php if($userData['name']==""){echo "&nbsp;";}else{ echo $userData['name']; } ?></a></div>
                            <div class="blkA"><a href=""><?php if($userData['email']==""){echo "&nbsp;";}else{ echo $userData['email']; } ?></a></div>
                            <!--<div class="blkG"><?php /*if($userData['category']==""){echo "&nbsp;";}else{ echo $userData['category']; }*/?></div>-->
							<div class="icon"><a href="compose.php?address_book_email=<?php echo $userData['email']; ?>"><img src="images/profile/mail.png" /></a></div>
                            <div class="icon"><a href="?edit=<?php echo $userData['id'];?>#addcontact"><img src="images/profile/edit.png" /></a></div>
                            <div class="icon"><a href="adduser.php?delete=<?php echo $userData['id'];?>"><img src="images/profile/delete.png"  onclick="return confirmdelete('<?php echo $userData['name']; ?>')"/></a></div>
                        </div>
						<?php
							}
							?>
							</div >
							<?php
					}
						?>
                   
                    <!--<p><strong>Use the addressbook on CypressGroove.com and change:<br />
                      A. Change contact type &quot;venue&quot; to &quot;community&quot;<br />
                      B. Add &quot;Out of Network&quot; as a contact  type<br />
                      C. When user adds Artist, Community, or Fan they will add registered users only<br />
                      D. When user adds &quot;Out of Network&quot; they will enter the email and name. </strong></p>-->
      			
                <?php
					if($_GET['sel_val']=="Community")
					{
							$filter="community";
							$rs=$check->filterContacts($general_id,$filter,$order_name,$order_email,$order_category);
							$resultArray = Array();
							if($rs!="" && $rs!=NULL)
							{
								while ($row = mysql_fetch_assoc($rs)) 
								{
									$resultArray[] = $row;
								}
							}
							//$resultArray = array_unique($resultArray);
							?>
							<div style="height:310px; overflow-y:auto; overflow-x:hidden; width:665px;">
							<?php
						for($u=0;$u<count($resultArray);$u++)
						{ 
								$userData = $resultArray[$u];

							?>
							<div class="tableCont" id="AccordionContainer">
								<div class="blkA"><a href=""><?php if($userData['name']==""){ echo "&nbsp;"; }else{ echo $userData['name']; } ?></a></div>
								<div class="blkA"><a href=""><?php if($userData['email']==""){ echo "&nbsp;"; }else{ echo $userData['email']; } ?></a></div>
								<!--<div class="blkG"><?php /*if($userData['category']==""){ echo "&nbsp;"; }else{ echo $userData['category']; } */?></div>-->
								<div class="icon"><a href="compose.php?address_book_email=<?php echo $userData['email']; ?>"><img src="images/profile/mail.png" /></a></div>
								<div class="icon"><a href="?edit=<?php echo $userData['id'];?>#addcontact"><img src="images/profile/edit.png" /></a></div>
								<div class="icon"><a href="adduser.php?delete=<?php echo $userData['id'];?>"><img src="images/profile/delete.png" onclick="return confirmdelete('<?php echo $userData['name']; ?>')"/></a></div>
							</div>
						<?php
						}
						?>
							</div >
							<?php
					}
					if($_GET['sel_val']=="artist_fans")
					{
							$rs=$check->get_artist_fans();
							$resultArray = Array();
							if($rs!="" && $rs!=NULL)
							{
								while ($row = mysql_fetch_assoc($rs)) 
								{
									$resultArray[] = $row;
								}
							}
							//$resultArray = array_unique($resultArray);
							?>
							<div style="height:310px; overflow-y:auto; overflow-x:hidden; width:665px;">
							<?php
							for($u=0;$u<count($resultArray);$u++)
							{ 
									$userData = $resultArray[$u];

								?>
								<div class="tableCont" id="AccordionContainer">
									<div class="blkA"><a href=""><?php if($userData['name']==""){ echo "&nbsp;";} else{ echo $userData['name']; } ?></a></div>
									<div class="blkA"><a href=""><?php if($userData['email']==""){ echo "&nbsp;";} else{ echo $userData['email']; } ?></a></div>
									<!--<div class="blkG"><?php /*if($userData['related_type']==""){ echo "&nbsp;";} else{ echo $userData['related_type']; } */?></div>-->
									<div class="icon"><a href="compose.php?address_book_email=<?php echo $userData['email']; ?>"><img src="images/profile/mail.png" /></a></div>
								</div>
							<?php
							}
							?>
							</div >
							<?php
					}
					if($_GET['sel_val']=="community_fans")
					{
							$rs=$check->get_community_fans();
							$resultArray = Array();
							if($rs!="" && $rs!=NULL)
							{
								while ($row = mysql_fetch_assoc($rs)) 
								{
									$resultArray[] = $row;
								}
							}
							//$resultArray = array_unique($resultArray);
							?>
							<div style="height:310px; overflow-y:auto; overflow-x:hidden; width:665px;">
							<?php
							for($u=0;$u<count($resultArray);$u++)
							{ 
									$userData = $resultArray[$u];

								?>
								<div class="tableCont" id="AccordionContainer">
									<div class="blkA"><a href=""><?php if($userData['name']==""){ echo "&nbsp;";} else{ echo $userData['name']; } ?></a></div>
									<div class="blkA"><a href=""><?php if($userData['email']==""){ echo "&nbsp;";} else{ echo $userData['name']; } ?></a></div>
									<!--<div class="blkG"><?php /*if($userData['related_type']==""){ echo "&nbsp;";} else{ echo $userData['related_type']; } */?></div>-->
									<div class="icon"><a href="compose.php?address_book_email=<?php echo $userData['email']; ?>"><img src="images/profile/mail.png" /></a></div>
								</div>
							<?php
							}
							?>
							</div >
							<?php
					}
					
					if($_GET['sel_val']=="General")
					{
							$filter="general";
							$rs=$check->filterContacts($general_id,$filter,$order_name,$order_email,$order_category);
							$resultArray = Array();
							if($rs!="" && $rs!=NULL)
							{
								while ($row = mysql_fetch_assoc($rs)) 
								{
									$resultArray[] = $row;
								}
							}
							//$resultArray = array_unique($resultArray);
							?>
							<div style="height:310px; overflow-y:auto; overflow-x:hidden; width:665px;">
							<?php
						for($u=0;$u<count($resultArray);$u++)
						{ 
								$userData = $resultArray[$u];

							?>
							<div class="tableCont" id="AccordionContainer">
								<div class="blkA"><a href=""><?php if($userData['name']==""){ echo "&nbsp;";} else{echo $userData['name']; } ?></a></div>
								<div class="blkA"><a href=""><?php if($userData['email']==""){ echo "&nbsp;";} else{echo $userData['email']; } ?></a></div>
								<!--<div class="blkG"><?php /*if($userData['related_type']==""){ echo "&nbsp;";} else{echo $userData['category']; } */?></div>-->
								<div class="icon"><a href="compose.php?address_book_email=<?php echo $userData['email']; ?>"><img src="images/profile/mail.png" /></a></div>
								<div class="icon"><a href="?edit=<?php echo $userData['id'];?>#addcontact"><img src="images/profile/edit.png" /></a></div>
								<div class="icon"><a href="adduser.php?delete=<?php echo $userData['id'];?>"><img src="images/profile/delete.png" onclick="return confirmdelete('<?php echo $userData['name']; ?>')"/></a></div>
							</div>
						<?php
						}
						?>
							</div >
							<?php
					}
					
					/*if($_GET['sel_val']=="unregistered contacts")
					{
							$filter="Unregistered";
							$rs=$check->unregisteredContacts($general_id,$filter);
							$resultArray = Array();
							while ($row = mysql_fetch_assoc($rs)) 
							{
								$resultArray[] = $row;
							}
						for($u=0;$u<count($resultArray);$u++)
						{ 
								$userData = $resultArray[$u];
						
						?>
                        <div class="tableCont" id="AccordionContainer">
                            <div class="blkA"><a href=""><?php echo $userData['name']; ?></a></div>
                            <div class="blkA"><a href=""><?php echo $userData['email']; ?></a></div>
                            <div class="blkG"><?php echo $userData['category']; ?></div>
							<div class="icon"><a href="#"><img src="images/profile/mail.png" /></a></div>
                            <div class="icon"><a href="add_contact.php?edit=<?php echo $userData['id'];?>"><img src="images/profile/edit.png" /></a></div>
                            <div class="icon"><a href="adduser.php?delete=<?php echo $userData['id'];?>"><img src="images/profile/delete.png" /></a></div>
                        </div>
						<?php
						}
					}*/
						
						
				}
				else
				{
					$res=$check->globe();
					$general_id=$res['general_user_id'];
					$rs=$check->allContacts($general_id,$order_name,$order_email,$order_category);
					$rs_del = $check->allContacts_dels($general_id);
					$fans_add = $check->get_all_fans();
				
							$resultArray = Array();
							$resultArray_fans = Array();
							$resultArray_new = array();
							$resultArray_del = array();
							
							if($rs!="" && $rs!=NULL)
							{
								while ($row = mysql_fetch_assoc($rs)) 
								{
									$get_name1 = $check->get_contact_name($row['email']);
									if($get_name1['fname']=="" && $get_name1['lname']==""){
										$row['name'] = $row['name'];
									}else{
										$row['name'] = $get_name1['fname']." ".$get_name1['lname'];
									}
									$resultArray[] = $row;
								}
							}
							
							if($rs_del!="" && $rs_del!=NULL)
							{
								while ($row = mysql_fetch_assoc($rs_del)) 
								{
									$resultArray_del[] = $row['email'];
								}
							}
							
							if($fans_add!="" && $fans_add!=NULL)
							{
								while ($row_fan = mysql_fetch_assoc($fans_add)) 
								{
									$get_name = $check->get_contact_name($row_fan['email']);
									$row_fan['name'] = $get_name['fname']." ".$get_name['lname'];
									$resultArray_fans[] = $row_fan;
								}
							}
							
							$all_pros_fans = array();
							$all_pros_fans = $check->get_all_fans_pros($all_fan_addr);
							
							$all_tip_down_email = array();
							$all_tip_down_email = $check->get_tip_email();
							
							$all_meds_cntcts = array();
							$all_meds_cntcts = $check->get_all_meds_cntcts($ids_songs_alls);
							
							$all_buys_cont = array();
							$all_buys_cont = $check->get_buys_concts();
							
							$resultArray_new1 = array_merge($resultArray,$resultArray_fans,$all_pros_fans,$all_buys_cont,$all_meds_cntcts,$all_tip_down_email);
							
							foreach ($resultArray_new1 as $user)
							{								
								$resultArray_new_lat[$user['email']] = $user;
							}
							
							if(isset($resultArray_new_lat))
							{
								foreach($resultArray_new_lat as $k)
								{
									$resultArray_new[] = $k;
								}
							}
							
							$sort = array();
							foreach($resultArray_new as $k=>$v)
							{
								//$sort['email'][$k] = $v['email'];
								$sort['name'][$k] = $v['name'];
							}
							if(!empty($sort))
							{
								$array_lowercase = array_map('strtolower', $sort['name']);
								array_multisort($array_lowercase, SORT_ASC,$resultArray_new);
							}
							
							$chk__email_arr = array();
							$chk__email_arr = $check->get_all_disallow($res['general_user_id']);
							
							?>
							<div style="height:310px; overflow-y:auto; overflow-x:hidden; width:665px;">
							<?php
							$count_contcst = 0;
							$pass_email_to_mailchimp = array();
							for($u=0;$u<count($resultArray_new);$u++)
							{
								if(!empty($resultArray_new[$u]))
								{
									$userData = $resultArray_new[$u];
									if($userData['email']!="")
									{
										if(!in_array($userData['email'],$chk__email_arr) && !in_array($userData['email'],$resultArray_del))
										{
											$pass_email_to_mailchimp[$count_contcst]['name'] = $userData['name'];
											$pass_email_to_mailchimp[$count_contcst]['email'] = $userData['email'];
											$count_contcst = $count_contcst + 1;
							?>
							
							<div class="tableCont" id="AccordionContainer">
								<div class="blkA"><a href=""><?php if($userData['name']==""){echo "&nbsp;";}else { echo $userData['name']; }?></a></div>
								<div class="blkA"><a href=""><?php if($userData['email']==""){echo "&nbsp;";}else { echo $userData['email']; } ?></a></div>
								
								<!--<div class="blkG"><?php /*if($userData['category']==""){echo "&nbsp;";}else { echo $userData['category']; } */?></div>-->
								<div class="icon"><a href="compose.php?address_book_email=<?php echo $userData['email']; ?>"><img src="images/profile/mail.png" /></a></div>
								<?php
									if(!isset($userData['related_id']))
									{
								?>
										<div class="icon"><a href="?edit=<?php echo $userData['id'];?>#addcontact"><img src="images/profile/edit.png" /></a></div>
										<div class="icon"><a href="adduser.php?delete=<?php echo $userData['id'];?>"><img src="images/profile/delete.png" onclick="return confirmdelete_addr('<?php echo $userData['name']; ?>','<?php echo $userData['email']; ?>')" /></a></div> 
								<?php
									}
									else
									{
								?>
										<div class="icon"></div>
										<div class="icon"><a href="adduser.php?delete_allow=<?php echo $res['general_user_id']; ?>&whose_delete=<?php echo $userData['email']; ?>"><img src="images/profile/delete.png" onclick="return confirmdelete_addr('<?php echo $userData['name']; ?>','<?php echo $userData['email']; ?>')" /></a></div> 
								<?php		
									}
								?>
							</div>
							
							<?php
										}
									}
								}
							}
							?>
							<script>
								$(document).ready(function(){
									$("#count_contcts").append(<?php echo $count_contcst; ?>);
									
								});
							</script>
							<?php
						/*for($u=0;$u<count($resultArray);$u++)
						{
								$userData = $resultArray[$u];
						
						?>
						
                        <div class="tableCont" id="AccordionContainer">
                            <div class="blkA"><a href=""><?php if($userData['name']==""){echo "&nbsp;";}else { echo $userData['name']; }?></a></div>
                            <div class="blkA"><a href=""><?php if($userData['email']==""){echo "&nbsp;";}else { echo $userData['email']; } ?></a></div>
                            <!--<div class="blkG"><?php /*if($userData['category']==""){echo "&nbsp;";}else { echo $userData['category']; } *//*?></div>-->
							<div class="icon"><a href="compose.php?address_book_email=<?php echo $userData['email']; ?>"><img src="images/profile/mail.png" /></a></div>
                            <div class="icon"><a href="?edit=<?php echo $userData['id'];?>#addcontact"><img src="images/profile/edit.png" /></a></div>
                            <div class="icon"><a href="adduser.php?delete=<?php echo $userData['id'];?>"><img src="images/profile/delete.png" onclick="return confirmdelete('<?php echo $userData['name']; ?>')" /></a></div> 
                        </div>
						
						<?php
						}
						for($u_f=0;$u_f<count($resultArray_fans);$u_f++)
						{ 
								$userData_fan = $resultArray_fans[$u_f];
						
						?>
						
                        <div class="tableCont" id="AccordionContainer">
                            <div class="blkA"><a href=""><?php if($userData_fan['name']=="" || $userData_fan['name']==" " || $userData_fan['name']==null){echo "&nbsp;";}else { echo $userData_fan['name']; } ?></a></div>
                            <div class="blkA"><a href=""><?php if($userData_fan['email']=="" || $userData_fan['email']==null){echo "&nbsp;";}else { echo $userData_fan['email']; } ?></a></div>
                            <!--<div class="blkG"><?php /*if($userData['related_type']==""){echo "&nbsp;";}else { echo $userData_fan['related_type']; } *//*?></div>-->
							<div class="icon"><a href="compose.php?address_book_email=<?php echo $userData_fan['email']; ?>"><img src="images/profile/mail.png" /></a></div>
                        </div>
						
						<?php
						}*/
						?>
						</div>
				<?php
				}
				?>
                 </div>
				 
				 </div>
				 
                <div class="subTabs" id="events" style="display:none;">
                	<h1>Events</h1>
                    
                    <div style="width:665px; float:left;">
                    	<div id="mediaContent">
                        	<div class="topLinks">
                                <div class="links" style="float:left;">
                                    <select class="eventdrop" onChange="handleSelection(value)" style=" margin-left:0;">
                                        <option value="mycal">Calender</option>
                                        <option value="mylist">Events List</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div id="mycal">
                    		<div id="mediaContent">
                        	<div class="topLinks">
                            	<div class="Llinks">
								<?php 
                                    include("calendar.php");
                                    $date=date("Y-m-d");
                                    $time=strtotime($date);
                                ?>
                                </div>
                            </div>
                        </div>
                        </div>
                        <div id="mylist" style="display:none;">
                            <div id="mediaContent">
                                <div class="topLinks">
                                    <div class="links" style="float:left;">
                                        <select class="eventdrop" onChange="handleSelection(value)" style=" margin-left:0;">
                                            <option value="allevents">My Events</option>
                                            <option value="published">Recorded Events</option>
                                            <option value="upcoming">Upcoming Events</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div id="allevents">
                                <div id="mediaContent">
                                    <div class="titleCont">
                                        <div class="blkB" style="width:60px;">Date</div>
										<div class="blkB" style="width:160px;">Title</div>
										<div class="blkB" style="width:110px;">Time</div>
										<div class="blkB" style="width:140px;">Venue</div>
										<!--<div class="blkB" style="width:200px;">Creator</div>-->
										<div class="blkI" style="width:110px;">Type</div>
                                    </div>
                                    <div style="height:400px; overflow-y:auto; overflow-x:hidden; width:664px;">
                                        <?php
											$get_levent = $newgeneral->getEventUpcoming();
											$get_leventrec = $newgeneral->getEventRecorded();
											$get_laeevent = $newgeneral->getArtEventUpcoming();
											$get_laeeventrec = $newgeneral->getArtEventRecorded();
											
											$gett_id = array();
											if($get_levent!="" && $get_levent!=NULL)
											{
												while($get_upev_ids = mysql_fetch_assoc($get_levent))
												{
													//$get_upev_ids['id'] = $get_upev_ids['id'].'~com';
													$get_upev_ids['edit_com'] = 'yes';
													$get_upev_ids['whos_event'] = "own_com_event";
													$gett_id[] = $get_upev_ids;
												}
											}
											
											$getcrq_id = array();
											if($get_laeevent!="" && $get_laeevent!=NULL)
											{
												while($get_aupev_ids = mysql_fetch_assoc($get_laeevent))
												{
													//$get_aupev_ids['id'] = $get_aupev_ids['id'].'~art';
													$get_aupev_ids['whos_event'] = "own_art_event";
													$getcrq_id[] = $get_aupev_ids;
												}	
											}

											$getat_ev = array();
											$getat_ev = $newgeneral->only_creator_oupevent();
											
											$sel_ged = array();
											if($event_avail!=0)
											{
												$acc_event = array_unique($acc_event);
												for($h=0;$h<count($acc_event);$h++)
												{
													if($acc_event[$h]!="" && $acc_event[$h]!=0)
													{
														$dumcomu = $newgeneral->get_event_tagged_by_other_user($acc_event[$h]);
														if(!empty($dumcomu) && $dumcomu['id']!="" && $dumcomu['id']!="")
														{
															//$dumcomu['id'] = $dumcomu['id'].'~'.'com';
															$dumcomu['edit'] = 'yes';
															$dumcomu['edit_com'] = 'yes';
															$dumcomu['whos_event'] = "com_tag_eve";
															$sel_ged[] = $dumcomu;
														}	
													}
												}
											}
											
											$sela_ged = array();
											if($event_aavail!=0)
											{
												$acc_aevent = array_unique($acc_aevent);
												for($h=0;$h<count($acc_aevent);$h++)
												{
													if($acc_aevent[$h]!="" && $acc_aevent[$h]!=0)
													{
														$dumartu = $newgeneral->get_aevent_tagged_by_other_user($acc_aevent[$h]);
														if(!empty($dumartu) && $dumartu['id']!="" && $dumartu['id']!="")
														{
															//$dumartu['id'] = $dumartu['id'].'~'.'art';
															$dumartu['edit'] = 'yes';
															$dumartu['whos_event'] = "art_tag_eve";
															$sela_ged[] = $dumartu;
														}	
													}
												}
											}
											
											$getcr_id = array();
											if($get_leventrec!="" && $get_leventrec!=NULL)
											{
												while($get_ever_ids = mysql_fetch_assoc($get_leventrec))
												{
													//$get_ever_ids['id'] = $get_ever_ids['id'].'~com';
													$get_ever_ids['edit_com'] = 'yes';
													$get_ever_ids['whos_event'] = "own_com_event";
													$getcr_id[] = $get_ever_ids;											
												}
											}
											$getarer_id = array();
											if($get_laeeventrec!="" && $get_laeeventrec!=NULL)
											{
												while($get_aever_ids = mysql_fetch_assoc($get_laeeventrec))
												{
													//$get_aever_ids['id'] = $get_aever_ids['id'].'~art';
													$get_aever_ids['whos_event'] = "own_art_event";
													$getarer_id[] = $get_aever_ids;											
												}
											}
											$getgve_rev = array();
											$getgve_rev = $newgeneral->only_creator_orecevent();
											
											$get_only2cre_rev = array();
											$get_only2cre_rev = $newgeneral->only_creator2_recevent();
											
											$get_only2cre_ev = array();
											$get_only2cre_ev = $newgeneral->only_creator2ev_upevent();
											
											$selcomu_tc = array();
											if($event_avail!=0)
											{
												$acc_event = array_unique($acc_event);
												for($k=0;$k<count($acc_event);$k++)
												{
													if($acc_event[$k]!="" && $acc_event[$k]!=0)
													{
														$dumec = $newgeneral->get_event_tagged_by_other_userrec($acc_event[$k]);
														if(!empty($dumec) && $dumec['id']!="" && $dumec['id']!="")
														{
															//$dumec['id'] = $dumec['id'].'~'.'com';
															$dumec['edit'] = 'yes';
															$dumec['edit_com'] = 'yes';
															$dumec['whos_event'] = "com_tag_eve";
															$selcomu_tc[] = $dumec;
														}												
													}
												}
											}
											
											$selart_yi = array();
											if($event_aavail!=0)
											{
												$acc_aevent = array_unique($acc_aevent);
												for($ki=0;$ki<count($acc_aevent);$ki++)
												{
													if($acc_aevent[$ki]!="" && $acc_aevent[$ki]!=0)
													{
														$dumc = $newgeneral->get_aevent_tagged_by_other_userrec($acc_aevent[$ki]);
														if(!empty($dumc) && $dumc['id']!="" && $dumc['id']!="")
														{
															//$dumc['id'] = $dumc['id'].'~'.'art';
															$dumc['edit'] = 'yes';
															$dumc['whos_event'] = "art_tag_eve";
															$selart_yi[] = $dumc;
														}												
													}
												}
											}
											
											$selarts2u_tagged = array();
											if($project_aavail!=0)
											{
												$acc_aproject = array_unique($acc_aproject);
												for($acc_pro=0;$acc_pro<count($acc_aproject);$acc_pro++)
												{												
													if($acc_aproject[$acc_pro]!="" && $acc_aproject[$acc_pro]!=0)
													{													
														$sql_1_cre = $newgeneral->get_event_tagged_by_other2_user($acc_aproject[$acc_pro]);
														if($sql_1_cre!="" && $sql_1_cre!=NULL)
														{
															if(mysql_num_rows($sql_1_cre)>0)
																{
																	while($run = mysql_fetch_assoc($sql_1_cre))
																	{
																		
																		//$ans_a_als[] = $run;
																		$selarts2u_tagged[] = $run;
																	}
																}
														}
														
														$sql_2_cre = $newgeneral->get_event_atagged_by_other2_user($acc_aproject[$acc_pro]);
														if($sql_2_cre!="" && $sql_2_cre!=NULL)
														{
																if(mysql_num_rows($sql_2_cre)>0)
																{
																	while($run_c = mysql_fetch_assoc($sql_2_cre))
																	{
																		
																		//$ans_c_als[]
																		$selarts2u_tagged[] = $run_c;
																	}
																}
														}
													}
												}
											}
											
											$selcoms2u_tagged = array();
											if($project_avail!=0)
											{
												$acc_project = array_unique($acc_project);
												for($acc_pro=0;$acc_pro<count($acc_project);$acc_pro++)
												{												
													if($acc_project[$acc_pro]!="" && $acc_project[$acc_pro]!=0)
													{													
														$sql_1_cre = $newgeneral->get_aevent_tagged_by_other2_user($acc_project[$acc_pro]);
														if($sql_1_cre!="" && $sql_1_cre!=NULL)
														{
															if(mysql_num_rows($sql_1_cre)>0)
															{
																while($run = mysql_fetch_assoc($sql_1_cre))
																{
																	
																	//$ans_a_als[] = $run;
																	$selcoms2u_tagged[] = $run;
																}
															}
														}
														
														$sql_2_cre = $newgeneral->get_aevent_atagged_by_other2_user($acc_project[$acc_pro]);
														if($sql_2_cre!="" && $sql_2_cre!=NULL)
														{
															if(mysql_num_rows($sql_2_cre)>0)
															{
																while($run_c = mysql_fetch_assoc($sql_2_cre))
																{
																	
																	//$ans_c_als[]
																	$selcoms2u_tagged[] = $run_c;
																}
															}
														}
													}
												}
											}
											
											$selarts2r_tagged = array();
											if($project_aavail!=0)
											{
												$acc_aproject = array_unique($acc_aproject);
												for($acc_pro=0;$acc_pro<count($acc_aproject);$acc_pro++)
												{												
													if($acc_aproject[$acc_pro]!="" && $acc_aproject[$acc_pro]!=0)
													{													
														$sql_1_cre = $newgeneral->get_event_tagged_by_other2_userrec($acc_aproject[$acc_pro]);
														if($sql_1_cre!="" && $sql_1_cre!=NULL)
														{
															if(mysql_num_rows($sql_1_cre)>0)
															{
																while($run = mysql_fetch_assoc($sql_1_cre))
																{
																	
																	//$ans_a_als[] = $run;
																	$selarts2r_tagged[] = $run;
																}
															}
														}
														
														$sql_2_cre = $newgeneral->get_event_atagged_by_other2_userrec($acc_aproject[$acc_pro]);
														if($sql_2_cre!="" && $sql_2_cre!=NULL)
														{
															if(mysql_num_rows($sql_2_cre)>0)
															{
																while($run_c = mysql_fetch_assoc($sql_2_cre))
																{
																	
																	//$ans_c_als[]
																	$selarts2r_tagged[] = $run_c;
																}
															}
														}
													}
												}
											}
											
											$selcoms2r_tagged = array();
											if($project_avail!=0)
											{
												$acc_project = array_unique($acc_project);
												for($acc_pro=0;$acc_pro<count($acc_project);$acc_pro++)
												{												
													if($acc_project[$acc_pro]!="" && $acc_project[$acc_pro]!=0)
													{													
														$sql_1_cre = $newgeneral->get_aevent_tagged_by_other2_userrec($acc_project[$acc_pro]);
														if($sql_1_cre!="" && $sql_1_cre!=NULL)
														{
															if(mysql_num_rows($sql_1_cre)>0)
															{
																while($run = mysql_fetch_assoc($sql_1_cre))
																{
																	
																	//$ans_a_als[] = $run;
																	$selcoms2r_tagged[] = $run;
																}
															}
														}
														
														$sql_2_cre = $newgeneral->get_aevent_atagged_by_other2_userrec($acc_project[$acc_pro]);
														if($sql_2_cre!="" && $sql_2_cre!=NULL)
														{
															if(mysql_num_rows($sql_2_cre)>0)
															{
																while($run_c = mysql_fetch_assoc($sql_2_cre))
																{
																	
																	//$ans_c_als[]
																	$selcoms2r_tagged[] = $run_c;
																}
															}
														}
													}
												}
											}
											
											$var_event = array_merge($selart_yi,$selcomu_tc,$getgve_rev,$getarer_id,$getcr_id,$sela_ged,$sel_ged,$getat_ev,$getcrq_id,$gett_id,$get_only2cre_rev,$get_only2cre_ev,$selcoms2u_tagged,$selarts2u_tagged,$selcoms2r_tagged,$selarts2r_tagged);
								
											$sort = array();
											foreach($var_event as $k=>$v) {
											
												//$create_c = strpos($v['creator'],'(');
												//$end_c = substr($v['creator'],0,$create_c);
												
												//$sort['creator'][$k] = $end_c;
												//$sort['from'][$k] = $end_f;
												//$sort['track'][$k] = $v['track'];
												$sort['date'][$k] = $v['date'];
											}
											if(!empty($sort))
											{
												array_multisort($sort['date'], SORT_DESC,$var_event);
											}
                                            $event=new ProfileEdit();
                                           /*  $var_event=$event->allEvents();
											$tagged_event = $event ->artist_tagged_Events();
											$tagged_community_event = $event ->community_tagged_Events();
											$get_onlycre_recv = array();
											$get_onlycre_recv = $event->only_creator_recevent();
											$get_onlycre_upv = array();
											$get_onlycre_upv = $event->only_creator_upevent(); */
											
											$dumps_aeves = array();
											$dumps_ceves = array();
											$get_all_del_id = $newgeneral->get_deleted_project_id();
											$exp_del_id = explode(",",$get_all_del_id['deleted_event_id']);
											for($count_all=0;$count_all<count($var_event);$count_all++){
												for($count_del=0;$count_del<count($exp_del_id);$count_del++){
													if($exp_del_id[$count_del]!=""){
														if($var_event[$count_all]['id'] == $exp_del_id[$count_del]){
															$var_event[$count_all] ="";
														}
													}
												}
											}
                                            for($u=0;$u<count($var_event);$u++)
                                            {
												if($var_event[$u]!="")
												{
													$userData = $var_event[$u];
													$chk_eves = 0;
													//print_r($var_event);
													if(isset($userData['artist_id']) && $userData['artist_id']!=0 && $userData['artist_id']!="")
													{
														$get_event_type = $newgeneral->Get_ACEvent_subtype($userData['id']);
														if(in_array($userData['id'],$dumps_aeves))
														{
															$chk_eves = 1;
														}
														else
														{
															$dumps_aeves[] = $userData['id'];
														}
													}
													if(isset($userData['community_id']) && $userData['community_id']!=0 && $userData['community_id']!="")
													{
														$get_event_type = $newgeneral->Get_Event_subtype($userData['id']);
														if(in_array($userData['id'],$dumps_ceves))
														{
															$chk_eves = 1;
														}
														else
														{
															$dumps_ceves[] = $userData['id'];
														}
													}
													
													if($chk_eves==0)
													{
														?>
														<div class="tableCont">
															<div class="blkB" style="width:60px;"><?php echo date("m.d.y", strtotime($userData['date'])); ?></div>
															<div class="blkB" style="width:160px;"><a href="<?php if($userData['profile_url']!=""){ echo $userData['profile_url'];}else{ echo "javascript:void(0)";}?>" ><?php echo $userData['title']; ?></a></div>
															<div class="blkB" style="width:110px;"><?php if($userData['start_time']!="" && $userData['end_time']!="") { echo $userData['start_time'].$userData['start_timeap'].' - '.$userData['end_time'].$userData['end_timeap']; } else{?>&nbsp;<?php } ?></div>
															<div class="blkB" style="width:140px;"><?php
																if($userData['venue_name']!="")
																{
																	if($userData['venue_website']!="" && $userData['venue_website']!="http://" && $userData['venue_website']!="https://")
																	{
																		echo '<a href='.$userData['venue_website'].' target=_blank>'.$userData['venue_name'].'</a>';
																	}
																	else
																	{
																		echo $userData['venue_name']; 
																	}
																}
																else{?>&nbsp;<?php } ?></div>
																<div class="blkB" style="width:110px;"><?php if($get_event_type['name']!="") { echo $get_event_type['name'];}else{?>&nbsp;<?php }?></div>
															<div class="blkT" style="width:73px;"><a href="<?php if($userData['profile_url']!=""){ echo $userData['profile_url'];}else{ echo "javascript:void(0)";}?>" ><img width="20" height="20" src="images/magnifying_glass_icon.gif" style="position:relative;top:5px;"/></a>
															<?php
																if(isset($userData['artist_id']) && $userData['artist_id']!=0 && $userData['artist_id']!=""){
																	if($userData['whos_event']=="own_art_event"){
																	?>
																		<div class="icon"><a href="add_artist_event.php?id=<?php echo "$userData[id]"; ?>"><img src="images/profile/edit.png" /></a></div>
																	<?php
																	}else{
																	?>
																	<div class="icon">&nbsp;</div>
																	<?php
																	}
																	?>
																	<!--<div class="icon"><a href="delete_profile_events.php?table_name=artist_event&id=<?php //echo $userData['id']; ?>&id_name=artist_id" onclick="return confirm_delete('<?php //echo $userData['title']; ?>')"><img src="images/profile/delete.png" /></a></div>-->
																	<div class="icon"><a href="insertartistevent.php?delete=<?php echo $userData['id']; ?>&whos_event=<?php echo $userData['whos_event']; ?>" onclick="return confirm_delete('<?php echo $userData['title']; ?>')"><img src="images/profile/delete.png" /></a></div>
																	<?php
																}
																if(isset($userData['community_id']) && $userData['community_id']!=0 && $userData['community_id']!=""){
																	 if($userData['whos_event']=="own_com_event"){
																	?>
																	<div class="icon"><a href="add_community_event.php?id=<?php echo "$userData[id]"; ?>"><img src="images/profile/edit.png" /></a></div>
																	<?php
																	}else{
																	?>
																	<div class="icon">&nbsp;</div>
																	<?php
																	}
																	?>
																	<!--<div class="icon"><a href="delete_profile_events.php?table_name=community_event&id=<?php //echo $userData['id']; ?>&id_name=community_id" onclick="return confirm_delete('<?php //echo $userData['title']; ?>')"><img src="images/profile/delete.png" /></a></div>-->
																	<div class="icon"><a href="insertcommunityevent.php?delete=<?php echo $userData['id']; ?>&whos_event=<?php echo $userData['whos_event']; ?>" onclick="return confirm_delete('<?php echo $userData['title']; ?>')"><img src="images/profile/delete.png" /></a></div>
																<?php
																}
															?>
															</div>
														</div>
														<?php
													}
												}
											}
											/* for($crerec=0;$crerec<count($get_onlycre_recv);$crerec++)
                                            {
                                                $userData = $get_onlycre_recv[$crerec];
                                                //print_r($var_event);
                                        ?>
                                        <div class="tableCont">
                                            <div class="blkB"><?php echo date("d.m.y", strtotime($userData['date'])); ?></div>
                                            <div class="blkH" style="width:405px;"><a href="<?php if($userData['profile_url']!=""){ echo $userData['profile_url'];}else{ echo "javascript:void(0)";}?>" target="_blank"><?php echo $userData['title']; ?></a></div>
											<div class="blkT" style="width:73px;"><a href="<?php if($userData['profile_url']!=""){ echo $userData['profile_url'];}else{ echo "javascript:void(0)";}?>" target="_blank"><img width="20" height="20" src="images/magnifying_glass_icon.gif" style="position:relative;top:5px;"/></a>
                                            <?php
                                                if(isset($userData['artist_id']) && $userData['artist_id']!=0 && $userData['artist_id']!="")
                                                {
													$get_id = explode("~",$userData['id']);
													?>
													<!--<div class="icon"><a href="add_artist_event.php?id=<?php// echo "$userData[id]"; ?>"><img src="images/profile/edit.png" /></a></div>
													<div class="icon"><a href="delete_profile_events.php?table_name=artist_event&id=<?php //echo $userData['id']; ?>&id_name=artist_id" onclick="return confirm_delete('<?php //echo $userData['title']; ?>')"><img src="images/profile/delete.png" /></a></div>-->
													<div class="icon"><a href="insertartistevent.php?delete=<?php echo $get_id[0]; ?>&whos_event=only_creator" onclick="return confirm_delete('<?php echo $userData['title']; ?>')"><img src="images/profile/delete.png" /></a></div>
													<?php
                                                }
                                                if(isset($userData['community_id']) && $userData['community_id']!=0 && $userData['community_id']!="")
                                                {
													$get_id = explode("~",$userData['id']);
												?>
													<!--<div class="icon"><a href="add_community_event.php?id=<?php //echo "$userData[id]"; ?>"><img src="images/profile/edit.png" /></a></div>
													<div class="icon"><a href="delete_profile_events.php?table_name=community_event&id=<?php //echo $userData['id']; ?>&id_name=community_id" onclick="return confirm_delete('<?php //echo $userData['title']; ?>')"><img src="images/profile/delete.png" /></a></div>-->
													<div class="icon"><a href="insertcommunityevent.php?delete=<?php echo $get_id[0]; ?>&whos_event=only_creator" onclick="return confirm_delete('<?php echo $userData['title']; ?>')"><img src="images/profile/delete.png" /></a></div>
												<?php
                                                }
                                            ?>
                                            </div>
                                        </div>
                                        <?php
                                            }
											for($creupc=0;$creupc<count($get_onlycre_upv);$creupc++)
                                            {
                                                $userData = $get_onlycre_upv[$creupc];
                                                //print_r($var_event);
                                        ?>
                                        <div class="tableCont">
                                            <div class="blkB"><?php echo date("d.m.y", strtotime($userData['date'])); ?></div>
                                            <div class="blkH" style="width:405px;"><a href="<?php if($userData['profile_url']!=""){ echo $userData['profile_url'];}else{ echo "javascript:void(0)";}?>" target="_blank"><?php echo $userData['title']; ?></a></div>
											<div class="blkT" style="width:73px;"><a href="<?php if($userData['profile_url']!=""){ echo $userData['profile_url'];}else{ echo "javascript:void(0)";}?>" target="_blank"><img width="20" height="20" src="images/magnifying_glass_icon.gif" style="position:relative;top:5px;"/></a>
                                            <?php
                                                if(isset($userData['artist_id']) && $userData['artist_id']!=0 && $userData['artist_id']!="")
                                                {
													$get_id = explode("~",$userData['id']);
													?>
														<!--<div class="icon"><a href="add_artist_event.php?id=<?php //echo "$userData[id]"; ?>"><img src="images/profile/edit.png" /></a></div>
												<div class="icon"><a href="delete_profile_events.php?table_name=artist_event&id=<?php //echo $userData['id']; ?>&id_name=artist_id" onclick="return confirm_delete('<?php //echo $userData['title']; ?>')"><img src="images/profile/delete.png" /></a></div>-->
													<div class="icon"><a href="insertartistevent.php?delete=<?php echo $get_id[0]; ?>&whos_event=only_creator" onclick="return confirm_delete('<?php echo $userData['title']; ?>')"><img src="images/profile/delete.png" /></a></div>
													<?php
                                                }
                                                if(isset($userData['community_id']) && $userData['community_id']!=0 && $userData['community_id']!="")
                                                {
													$get_id = explode("~",$userData['id']);
												?>
													<!--<div class="icon"><a href="add_community_event.php?id=<?php //echo "$userData[id]"; ?>"><img src="images/profile/edit.png" /></a></div>
													<div class="icon"><a href="delete_profile_events.php?table_name=community_event&id=<?php //echo $userData['id']; ?>&id_name=community_id" onclick="return confirm_delete('<?php //echo $userData['title']; ?>')"><img src="images/profile/delete.png" /></a></div>-->
													<div class="icon"><a href="insertcommunityevent.php?delete=<?php echo $get_id[0]; ?>&whos_event=only_creator" onclick="return confirm_delete('<?php echo $userData['title']; ?>')"><img src="images/profile/delete.png" /></a></div>
												<?php
                                                }
                                            ?>
                                            </div>
                                        </div>
                                        <?php
                                            }
											for($tag_count=0;$tag_count<count($tagged_event);$tag_count++)
                                            {
											?>
											<div class="tableCont">
												<div class="blkB"><?php echo $tagged_event[$tag_count]['date']; ?></div>
												<div class="blkH" style="width:400px;"><a href="<?php if($tagged_event[$tag_count]['profile_url']!=""){ echo $tagged_event[$tag_count]['profile_url'];}else{ echo "javascript:void(0)";}?>" target="_blank"><?php echo $tagged_event[$tag_count]['title']; ?></a></div>
												<div class="blkT"><a href="<?php if($tagged_event[$tag_count]['profile_url']!=""){ echo $tagged_event[$tag_count]['profile_url'];}else{ echo "javascript:void(0)";}?>" target="_blank"><img width="20" height="20" src="images/magnifying_glass_icon.gif" style="position:relative;top:5px;"/></a>
												<?php
													if(isset($tagged_event[$tag_count]['artist_id']))
													{
														?>
														<!--<div class="icon"><a href="delete_profile_events.php?table_name=artist_event&id=<?php //echo $tagged_event[$tag_count]['id']; ?>&id_name=artist_id&type=artist" onclick="return confirm_delete('<?php //echo $tagged_event[$tag_count]['title']; ?>')"><img src="images/profile/delete.png" /></a></div>-->
														<div class="icon"><a href="insertartistevent.php?delete=<?php echo $tagged_event[$tag_count]['id']; ?>&whos_event=art_tag_eve" onclick="return confirm_delete('<?php echo $tagged_event[$tag_count]['title']; ?>')"><img src="images/profile/delete.png" /></a></div>
														<?php
													}
												?></div>
                                        </div>
                                        <?php
                                            }
											for($tag_com_count=0;$tag_com_count<count($tagged_community_event);$tag_com_count++)
                                            {
											?>
											<div class="tableCont">
												<div class="blkB"><?php echo $tagged_community_event[$tag_com_count]['date']; ?></div>
												<div class="blkH" style="width:400px;"><a href="<?php if($tagged_community_event[$tag_com_count]['profile_url']!=""){ echo $tagged_community_event[$tag_com_count]['profile_url'];}else{ echo "javascript:void(0)";}?>" target="_blank"><?php echo $tagged_community_event[$tag_com_count]['title']; ?></a></div>
												<div class="blkT"><a href="<?php if($tagged_community_event[$tag_com_count]['profile_url']!=""){ echo $tagged_community_event[$tag_com_count]['profile_url'];}else{ echo "javascript:void(0)";}?>" target="_blank"><img width="20" height="20" src="images/magnifying_glass_icon.gif" style="position:relative;top:5px;"/></a>
												<?php
													if(isset($tagged_community_event[$tag_com_count]['community_id']))
													{
														?>
														<!--<div class="icon"><a href="delete_profile_events.php?table_name=community_event&id=<?php //echo $tagged_community_event[$tag_com_count]['id']; ?>&id_name=community_id&type=community" onclick="return confirm_delete('<?php //echo $tagged_community_event[$tag_com_count]['title']; ?>')"><img src="images/profile/delete.png" /></a></div>-->
														<div class="icon"><a href="insertcommunityevent.php?delete=<?php echo $tagged_community_event[$tag_com_count]['id']; ?>&whos_event=com_tag_eve" onclick="return confirm_delete('<?php echo $tagged_community_event[$tag_com_count]['title']; ?>')"><img src="images/profile/delete.png" /></a></div>
														<?php
													}
												?></div>
                                        </div>
                                        <?php
                                            } */
                                        ?>
                                    </div>
                                </div>
                            </div>
                            <div id="upcoming" style="display:none;">
                                <div id="mediaContent">
                                    <div class="titleCont">
                                        <div class="blkB" style="width:73px;">Date</div>
										<div class="blkB" style="width:145px;">Title</div>
										<div class="blkB" style="width:150px;">Time</div>
										<div class="blkB" style="width:100px;">Venue</div>
										<!--<div class="blkB" style="width:200px;">Creator</div>-->
										<div class="blkI" style="width:110px;">Type</div>
                                    </div>
                                    
                                    <?php
                                        $event=new ProfileEdit();
										$date = date('Y-m-d');
										$duupps_aeves = array();
										$duups_ceves = array();
                                       /*  $var_event=$event->upcomingEvents($date);
										$get_onlycre_upv1 = array();
										$get_onlycre_upv1 = $event->only_creator_upevent(); */
										for($u=0;$u<count($var_event);$u++)
										{
											if($var_event[$u]!="")
											{
												if($var_event[$u]['date']>=$date)
												{
													$userData = $var_event[$u];
													$chk_eves = 0;
													//print_r($var_event);
													if(isset($userData['artist_id']) && $userData['artist_id']!=0 && $userData['artist_id']!="")
													{
														$get_event_type = $newgeneral->Get_ACEvent_subtype($userData['id']);
														if(in_array($userData['id'],$duupps_aeves))
														{
															$chk_eves = 1;
														}
														else
														{
															$duupps_aeves[] = $userData['id'];
														}
													}
													if(isset($userData['community_id']) && $userData['community_id']!=0 && $userData['community_id']!="")
													{
														$get_event_type = $newgeneral->Get_Event_subtype($userData['id']);
														if(in_array($userData['id'],$duups_ceves))
														{
															$chk_eves = 1;
														}
														else
														{
															$duups_ceves[] = $userData['id'];
														}
													}
													
													if($chk_eves==0)
													{
														?>
														<div class="tableCont">
															<div class="blkB" style="width:75px;"><?php echo date("m.d.y", strtotime($userData['date'])); ?></div>
															<div class="blkB" style="width:145px;"><a href="<?php if($userData['profile_url']!=""){ echo $userData['profile_url'];}else{ echo "javascript:void(0)";}?>" ><?php echo $userData['title']; ?></a></div>
															<div class="blkB" style="width:150px;"><?php if($userData['start_time']!="" && $userData['end_time']!="") { echo $userData['start_time'].$userData['start_timeap'].' - '.$userData['end_time'].$userData['end_timeap']; } else{?>&nbsp;<?php } ?></div>
															<div class="blkB" style="width:100px;"><?php
																if($userData['venue_name']!="")
																{
																	if($userData['venue_website']!="" && $userData['venue_website']!="http://" && $userData['venue_website']!="https://")
																	{
																		echo '<a href='.$userData['venue_website'].' target=_blank>'.$userData['venue_name'].'</a>';
																	}
																	else
																	{
																		echo $userData['venue_name']; 
																	}
																}
																else{?>&nbsp;<?php } ?></div>
																<div class="blkB" style="width:110px;"><?php if($get_event_type['name']!="") { echo $get_event_type['name'];}else{?>&nbsp;<?php }?></div>
															<div class="blkT" style="width:73px;"><a href="<?php if($userData['profile_url']!=""){ echo $userData['profile_url'];}else{ echo "javascript:void(0)";}?>" ><img width="20" height="20" src="images/magnifying_glass_icon.gif" style="position:relative;top:5px;"/></a>
															<?php
																if(isset($userData['artist_id']) && $userData['artist_id']!=0 && $userData['artist_id']!=""){
																	if($userData['whos_event']=="own_art_event"){
																	?>
																		<div class="icon"><a href="add_artist_event.php?id=<?php echo "$userData[id]"; ?>"><img src="images/profile/edit.png" /></a></div>
																	<?php
																	}else{
																	?>
																	<div class="icon">&nbsp;</div>
																	<?php
																	}
																	?>
																	<!--<div class="icon"><a href="delete_profile_events.php?table_name=artist_event&id=<?php //echo $userData['id']; ?>&id_name=artist_id" onclick="return confirm_delete('<?php //echo $userData['title']; ?>')"><img src="images/profile/delete.png" /></a></div>-->
																	<div class="icon"><a href="insertartistevent.php?delete=<?php echo $userData['id']; ?>&whos_event=<?php echo $userData['whos_event']; ?>" onclick="return confirm_delete('<?php echo $userData['title']; ?>')"><img src="images/profile/delete.png" /></a></div>
																	<?php
																}
																if(isset($userData['community_id']) && $userData['community_id']!=0 && $userData['community_id']!=""){
																	 if($userData['whos_event']=="own_com_event"){
																	?>
																	<div class="icon"><a href="add_community_event.php?id=<?php echo "$userData[id]"; ?>"><img src="images/profile/edit.png" /></a></div>
																	<?php
																	}else{
																	?>
																	<div class="icon">&nbsp;</div>
																	<?php
																	}
																	?>
																	<!--<div class="icon"><a href="delete_profile_events.php?table_name=community_event&id=<?php //echo $userData['id']; ?>&id_name=community_id" onclick="return confirm_delete('<?php //echo $userData['title']; ?>')"><img src="images/profile/delete.png" /></a></div>-->
																	<div class="icon"><a href="insertcommunityevent.php?delete=<?php echo $userData['id']; ?>&whos_event=<?php echo $userData['whos_event']; ?>" onclick="return confirm_delete('<?php echo $userData['title']; ?>')"><img src="images/profile/delete.png" /></a></div>
																<?php
																}
															?>
															</div>
														</div>
														<?php
													}
												}
											}
										}
										/* for($count_cre_up=0;$count_cre_up<count($get_onlycre_upv1);$count_cre_up++)
                                        { 
                                            $userData = $get_onlycre_upv1[$count_cre_up];
                                            //print_r($var_event);
                                    ?>
                                    <div class="tableCont">
                                        <div class="blkB"><?php echo date("d.m.y", strtotime($userData['date'])); ?></div>
                                        <div class="blkH"><a href="<?php if($userData['profile_url']!=""){ echo $userData['profile_url'];}else{ echo "javascript:void(0)";}?>" target="_blank"><?php echo $userData['title']; ?></a></div>
										<div class="blkT"><a href="<?php if($userData['profile_url']!=""){ echo $userData['profile_url'];}else{ echo "javascript:void(0)";}?>" target="_blank"><img width="20" height="20" src="images/magnifying_glass_icon.gif" style="position:relative;top:5px;"/></a>
                                        <?php
                                            if(isset($userData['artist_id']) && $userData['artist_id']!=0 && $userData['artist_id']!="")
                                            {
												$get_id = explode("~",$userData['id']);
											?>
												<!--<div class="icon"><a href="add_artist_event.php?id=<?php //echo "$userData[id]"; ?>"><img src="images/profile/edit.png" /></a></div>
												<div class="icon"><a href="delete_profile_events.php?table_name=community_event&id=<?php //echo $userData['id']; ?>&id_name=community_id" onclick="return confirm_delete('<?php //echo $userData['title']; ?>')"><img src="images/profile/delete.png" /></a></div>-->
												<div class="icon"><a href="insertartistevent.php?delete=<?php echo $get_id[0]; ?>&whos_event=only_creator" onclick="return confirm_delete('<?php echo $userData['title']; ?>')"><img src="images/profile/delete.png" /></a></div>
											<?php
                                            }
                                            if(isset($userData['community_id']) && $userData['community_id']!=0 && $userData['community_id']!="")
                                            {
												$get_id = explode("~",$userData['id']);
											?>
												<!--<div class="icon"><a href="add_community_event.php?id=<?php// echo "$userData[id]"; ?>"><img src="images/profile/edit.png" /></a></div>
												<div class="icon"><a href="delete_profile_events.php?table_name=community_event&id=<?php //echo $userData['id']; ?>&id_name=community_id" onclick="return confirm_delete('<?php //echo $userData['title']; ?>')"><img src="images/profile/delete.png" /></a></div>-->
												<div class="icon"><a href="insertcommunityevent.php?delete=<?php echo $get_id[0]; ?>&whos_event=only_creator" onclick="return confirm_delete('<?php echo $userData['title']; ?>')"><img src="images/profile/delete.png" /></a></div>
											<?php
                                            }
                                        ?></div>
                                    </div>
                                    <?php
                                        } */
                                    ?>
                                </div>
                            </div>
                            <div id="published" style="display:none;">
                                <div id="mediaContent">
                                    <div class="titleCont">
										<div class="blkB" style="width:73px;">Date</div>
										<div class="blkB" style="width:145px;">Title</div>
										<div class="blkB" style="width:150px;">Time</div>
										<div class="blkB" style="width:100px;">Venue</div>
										<!--<div class="blkB" style="width:200px;">Creator</div>-->
										<div class="blkI" style="width:110px;">Type</div>
                                    </div>
                                    
                                        <?php
                                            /* $var_event_recorded=$event->recordedEvents($date);
											$get_onlycre_recv1 = array();
											$get_onlycre_recv1 = $event->only_creator_recevent(); */
											$dureps_aeves = array();
											$dureps_ceves = array();
											$date = date('Y-m-d');
                                           for($u=0;$u<count($var_event);$u++)
											{
												if($var_event[$u]!="")
												{
													if($var_event[$u]['date']<$date)
													{
														$userData = $var_event[$u];
														$chk_eves = 0;
														//print_r($var_event);
														if(isset($userData['artist_id']) && $userData['artist_id']!=0 && $userData['artist_id']!="")
														{
															$get_event_type = $newgeneral->Get_ACEvent_subtype($userData['id']);
															if(in_array($userData['id'],$dureps_aeves))
															{
																$chk_eves = 1;
															}
															else
															{
																$dureps_aeves[] = $userData['id'];
															}
														}
														if(isset($userData['community_id']) && $userData['community_id']!=0 && $userData['community_id']!="")
														{
															$get_event_type = $newgeneral->Get_Event_subtype($userData['id']);
															if(in_array($userData['id'],$dureps_ceves))
															{
																$chk_eves = 1;
															}
															else
															{
																$dureps_ceves[] = $userData['id'];
															}
														}
														
														if($chk_eves==0)
														{
															?>
															<div class="tableCont">
																<div class="blkB" style="width:75px;"><?php echo date("m.d.y", strtotime($userData['date'])); ?></div>
																<div class="blkB" style="width:145px;"><a href="<?php if($userData['profile_url']!=""){ echo $userData['profile_url'];}else{ echo "javascript:void(0)";}?>" ><?php echo $userData['title']; ?></a></div>
																<div class="blkB" style="width:150px;"><?php if($userData['start_time']!="" && $userData['end_time']!="") { echo $userData['start_time'].$userData['start_timeap'].' - '.$userData['end_time'].$userData['end_timeap']; } else{?>&nbsp;<?php } ?></div>
																<div class="blkB" style="width:100px;"><?php
																	if($userData['venue_name']!="")
																	{
																		if($userData['venue_website']!="" && $userData['venue_website']!="http://" && $userData['venue_website']!="https://")
																		{
																			echo '<a href='.$userData['venue_website'].' target=_blank>'.$userData['venue_name'].'</a>';
																		}
																		else
																		{
																			echo $userData['venue_name']; 
																		}
																	}
																	else{?>&nbsp;<?php } ?></div>
																	<div class="blkB" style="width:110px;"><?php if($get_event_type['name']!="") { echo $get_event_type['name'];}else{?>&nbsp;<?php }?></div>
																<div class="blkT" style="width:73px;"><a href="<?php if($userData['profile_url']!=""){ echo $userData['profile_url'];}else{ echo "javascript:void(0)";}?>" ><img width="20" height="20" src="images/magnifying_glass_icon.gif" style="position:relative;top:5px;"/></a>
																<?php
																	if(isset($userData['artist_id']) && $userData['artist_id']!=0 && $userData['artist_id']!=""){
																		if($userData['whos_event']=="own_art_event"){
																		?>
																			<div class="icon"><a href="add_artist_event.php?id=<?php echo "$userData[id]"; ?>"><img src="images/profile/edit.png" /></a></div>
																		<?php
																		}else{
																		?>
																		<div class="icon">&nbsp;</div>
																		<?php
																		}
																		?>
																		<!--<div class="icon"><a href="delete_profile_events.php?table_name=artist_event&id=<?php //echo $userData['id']; ?>&id_name=artist_id" onclick="return confirm_delete('<?php //echo $userData['title']; ?>')"><img src="images/profile/delete.png" /></a></div>-->
																		<div class="icon"><a href="insertartistevent.php?delete=<?php echo $userData['id']; ?>&whos_event=<?php echo $userData['whos_event']; ?>" onclick="return confirm_delete('<?php echo $userData['title']; ?>')"><img src="images/profile/delete.png" /></a></div>
																		<?php
																	}
																	if(isset($userData['community_id']) && $userData['community_id']!=0 && $userData['community_id']!=""){
																		 if($userData['whos_event']=="own_com_event"){
																		?>
																		<div class="icon"><a href="add_community_event.php?id=<?php echo "$userData[id]"; ?>"><img src="images/profile/edit.png" /></a></div>
																		<?php
																		}else{
																		?>
																		<div class="icon">&nbsp;</div>
																		<?php
																		}
																		?>
																		<!--<div class="icon"><a href="delete_profile_events.php?table_name=community_event&id=<?php //echo $userData['id']; ?>&id_name=community_id" onclick="return confirm_delete('<?php //echo $userData['title']; ?>')"><img src="images/profile/delete.png" /></a></div>-->
																		<div class="icon"><a href="insertcommunityevent.php?delete=<?php echo $userData['id']; ?>&whos_event=<?php echo $userData['whos_event']; ?>" onclick="return confirm_delete('<?php echo $userData['title']; ?>')"><img src="images/profile/delete.png" /></a></div>
																	<?php
																	}
																?>
																</div>
															</div>
															<?php
														}
													}
												}
											}
											/* for($count_cre_rec=0;$count_cre_rec<count($get_onlycre_recv1);$count_cre_rec++)
                                            { 
                                                $userData = $get_onlycre_recv1[$count_cre_rec];
                                                //print_r($userData);
                                        ?>
                                        <div class="tableCont">
                                            <div class="blkB"><?php echo date("d.m.y", strtotime($userData['date'])); ?></div>
                                            <div class="blkH"><a href="<?php if($userData['profile_url']!=""){ echo $userData['profile_url'];}else{ echo "javascript:void(0)";}?>" target="_blank"><?php echo $userData['title']; ?></a></div>
											<div class="blkT"><a href="<?php if($userData['profile_url']!=""){ echo $userData['profile_url'];}else{ echo "javascript:void(0)";}?>" target="_blank"><img width="20" height="20" src="images/magnifying_glass_icon.gif" style="position:relative;top:5px;"/></a>
                                            <?php
                                                if(isset($userData['artist_id']))
                                                {
													$get_id = explode("~",$userData['id']);
												?>
													<!--<div class="icon"><a href="add_artist_event.php?id=<?php //echo "$userData[id]"; ?>"><img src="images/profile/edit.png" /></a></div>-->
													<!--<div class="icon"><a href="delete_profile_events.php?table_name=community_event&id=<?php //echo $userData['id']; ?>&id_name=community_id" onclick="return confirm_delete('<?php //echo $userData['title']; ?>')"><img src="images/profile/delete.png" /></a></div>-->
													<div class="icon"><a href="insertartistevent.php?delete=<?php echo $get_id[0]; ?>&whos_event=only_creator" onclick="return confirm_delete('<?php echo $userData['title']; ?>')"><img src="images/profile/delete.png" /></a></div>
												<?php
                                                }
                                                if(isset($userData['community_id']))
                                                {
													$get_id = explode("~",$userData['id']);
												?>
													<!--<div class="icon"><a href="add_community_event.php?id=<?php //echo "$userData[id]"; ?>"><img src="images/profile/edit.png" /></a></div>-->
													<!--<div class="icon"><a href="delete_profile_events.php?table_name=community_event&id=<?php //echo $userData['id']; ?>&id_name=community_id" onclick="return confirm_delete('<?php //echo $userData['title']; ?>')"><img src="images/profile/delete.png" /></a></div>-->
													<div class="icon"><a href="insertcommunityevent.php?delete=<?php echo $get_id[0]; ?>&whos_event=only_creator" onclick="return confirm_delete('<?php echo $userData['title']; ?>')"><img src="images/profile/delete.png" /></a></div>
												<?php
												}
                                            ?>
											</div>
                                        </div>
                                        <?php
                                            } */
                                        ?>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
				</div>
				<?php
						if(isset($_GET['frd_gen_acc_id']))
						{
							if($_GET['frd_gen_acc_id']!="")
							{
								if($_GET['frd_gen_acc_id']!=$_SESSION['login_id'])
								{
									header("Location: add_friends.php?confirm_friend_id=".$_SESSION['login_id']."&confirm_gen=".$_GET['frd_gen_acc_id']."&type=artist");
								}
							}
						}
						$fans_new_obj = new GetAllFans();
						$friends_new_obj = new Addfriend();
						$friends_suggest_obj = new NewSuggestion();
				?>
                <div class="subTabs" id="friends" style="display:none;">
                	<h1>Friends</h1>
					<div id="mediaContent">
						<div class="topLinks">
							<div class="links">
								<select class="eventdrop" onChange="handleSelection(value)" style="float:right;">
									<option value="awaiting_response">Awaiting response</option>
									<option value="all_friends_fans">All Friends</option>
									<option value="artist_friendsa">Artist Friends</option>
									<option value="community_friendsa">Company Friends</option>
									<!--<option value="artist_fansa">Artist Fans</option>
									<option value="community_fansa">Community Fans</option>-->
								</select>
								<a href="javascript:void(0);" onclick="runscript()" style="float:right;position:relative;top:6px;">Find Friends</a>
							</div>
						</div>
					</div>
                    <div id="actualContent">
					<div id="awaiting_response" >
					<?php
						$await_sql = $friends_new_obj->await_friend($_SESSION['login_id']);
								if(mysql_num_rows($await_sql)>0)
								{
					?>
							
								<div class="fieldCont"><b>Friends Awaiting Response</b></div>
								<div class="friendCont">
		<?php
								$await_sql = $friends_new_obj->await_friend($_SESSION['login_id']);
								if($await_sql!="" && $await_sql!=NULL)
								{
									if(mysql_num_rows($await_sql)>0)
									{
										$i_a_c = 0;
										while($row_await = mysql_fetch_assoc($await_sql))
										{
											//if($row_await['fartist_general_user_id']!=0)
											//{ 
												$sql_gen_a_d = $friends_new_obj->general_user_sec($row_await['general_user_id']);
												
												$sql_artist_d = $friends_new_obj->await_artist_details($sql_gen_a_d['artist_id']);
			?>
													<div class="friendBlkR">
														<div class="friendImg"><img src="http://generalproimage.s3.amazonaws.com/<?php if($sql_gen_a_d['image_name']!="") { echo $sql_gen_a_d['image_name']; } else { echo "Noimage.png"; } ?>" width="100" height="100" /></div>
														<div class="friendDetails">
															<span class="name"><?php echo $sql_gen_a_d['fname'].' '.$sql_gen_a_d['lname']; ?></span><br /><!--Some Details<br />-->
															<div id="friend_status_disp_<?php echo $i_a_c; ?>"><a style="cursor:pointer;" id="aacc_<?php echo $row_await['general_user_id']; ?>" class="faccpt_<?php echo $i_a_c; ?>"><img src="images/profile/accept.png" /></a><a style="cursor:pointer;" id="adec_<?php echo $row_await['general_user_id']; ?>" class="fdcline_<?php echo $i_a_c; ?>"><img src="images/profile/decline.png" /></a></div>
															<div id="friend_status_dis_<?php echo $i_a_c; ?>" style="display:none;"></div>
														</div>
													</div>
												
			<?php		
											/*}
											elseif($row_await['fcommunity_general_user_id']!=0)
											{
												$sql_gen_c_d = $friends_new_obj->general_user_sec($row_await['general_user_id']);
												
												$sql_community_d = $friends_new_obj->await_community_details($sql_gen_c_d['community_id']);
			?>
													<div class="friendBlk">
														<div class="friendImg"><img src="http://comjcropthumb.s3.amazonaws.com/<?php if($sql_community_d['listing_image_name']!="") { echo $sql_community_d['listing_image_name']; } else { echo "Noimage.png"; } ?>" width="100" height="100" /></div>
														<div class="friendDetails">
															<span class="name"><?php echo $sql_community_d['name']; ?></span><br />Some Details<br />
															<div id="friend_status_disp_<?php echo $i_a_c; ?>"><a style="cursor:pointer;" id="cacc_<?php echo $row_await['general_user_id'].'_'.$row_await['fcommunity_general_user_id']; ?>" class="faccpt_<?php echo $i_a_c; ?>">Accept</a> | <a style="cursor:pointer;" id="cdec_<?php echo $row_await['general_user_id'].'_'.$row_await['fcommunity_general_user_id']; ?>" class="fdcline_<?php echo $i_a_c; ?>">Decline</a></div>
															<div id="friend_status_dis_<?php echo $i_a_c; ?>" style="display:none;"></div>
														</div>
													</div>
												
			<?php
											}*/
			?>
										<script>
											$('div#awaiting_response div.friendCont div.friendBlkR div.friendDetails a.faccpt_<?php echo $i_a_c; ?>').click(function()
											{
												var chk_confirm = confirm("Are you sure that you want to accept the request?")
												if(chk_confirm==true)
												{
													var id = $(this).attr("id");
													//alert(id);
													
													var dataString_art = 'acc_dec='+ id;
													
													$.ajax({
														type: 'POST',
														url : 'FriendAjaxacc_dec.php',
														data: dataString_art,
														success: function(data)  
														{ 
															document.getElementById("friend_status_disp_<?php echo $i_a_c; ?>").style.display="none";
															document.getElementById("friend_status_dis_<?php echo $i_a_c; ?>").style.display="block";
															$("#friend_status_dis_<?php echo $i_a_c; ?>").html(data);
															//alert(data);
															
															location.reload();
														}
													});
												}
											});
											
											$('div#awaiting_response div.friendCont div.friendBlkR div.friendDetails a.fdcline_<?php echo $i_a_c; ?>').click(function()
											{
												var chk_confirm = confirm("Are you sure that you want to ignore the request?")
												if(chk_confirm==true)
												{
													var id = $(this).attr("id");
													
													var dataString_art = 'acc_dec='+ id;
													
													$.ajax({
														type: 'POST',
														url : 'FriendAjaxacc_dec.php',
														data: dataString_art,
														success: function(data) 
														{ 
															document.getElementById("friend_status_disp_<?php echo $i_a_c; ?>").style.display="none";
															document.getElementById("friend_status_dis_<?php echo $i_a_c; ?>").style.display="block";
															$("#friend_status_dis_<?php echo $i_a_c; ?>").html(data);
															//alert(data);
														}
													});
												}
											});
										</script>
											<?php
											$i_a_c = $i_a_c + 1;
										}
									}
									else
									{
			?>
										<div style="font-weight: bold; text-align: left;">NONE</div>
										<script>
											document.getElementById('awaiting_response').style.display="none";
										</script>
			<?php
									}
								}
								else
								{
		?>
									<div style="font-weight: bold; text-align: left;">NONE</div>
									<script>
										document.getElementById('awaiting_response').style.display="none";
									</script>
		<?php
								}
		?>
								</div>
							
					<?php
						}
					?>
					</div>
							<div id="all_friends_fans" style="">
								<div class="fieldCont"><b>All Friends</b></div>
								<div class="friendCont">
								<?php 
								$get_all_friend = $friends_new_obj->get_all_friends($_SESSION['login_id']);
								$get_gen = $friends_new_obj->general_user_sec($_SESSION['login_id']);
								$get_all_fans = $friends_new_obj->get_all_fans($get_gen['artist_id'],$get_gen['community_id']);
								/* if($get_all_fans!="" && count($get_all_fans)!=0)
								{
									$users_email_fans = array();
									$new_array = array();
									for($chk_mail_fans=0;$chk_mail_fans<count($get_all_fans);$chk_mail_fans++)
									{
										$users_email_fans[] = $get_all_fans[$chk_mail_fans]['email'];
									}
									
									foreach($users_email_fans as $key => $value)
									{
										if(isset($new_array[$value]))
										{
											//$new_array[$value] +=1;
										}
										else
										{
											$new_array[$value] =1;
											$get_fan_in = $friends_new_obj->general_user_email_gen($value);
											if($get_fan_in['fname']!="" && $get_fan_in['lname']!="")
											{
				?>
												<div class="friendBlkR">
													<div class="friendImg"><img src="http://generalproimage.s3.amazonaws.com/<?php if($get_fan_in['image_name']!="") { echo $get_fan_in['image_name']; }else{ echo "Noimage.png"; } ?>" width="100" height="100" /></div>
													<div class="friendDetails">
														<span class="name"><?php echo $get_fan_in['fname'].' '.$get_fan_in['lname']; ?></span><br />
													</div>
												</div>
				<?php
											}
										}
									}
								} */
								
								if($get_all_friend!="" && $get_all_friend!=NULL)
								{
									if(mysql_num_rows($get_all_friend)>0 && !empty($get_all_friend))
									{
										$all_h_d = 0;
										while($row_friend = mysql_fetch_assoc($get_all_friend))
										{
											$c_spr_e = 0;
											
											$art_log_g = $friends_suggest_obj->general_user_sec($_SESSION['login_id']);
											$art_log_g_e = $friends_suggest_obj->user_art_friend($art_log_g['artist_id']);
											$art_log_g_ec = $friends_suggest_obj->user_com_friend($art_log_g['community_id']);
											
											$art_run = $friends_new_obj->general_user_sec($row_friend['fgeneral_user_id']);
											$get_all_friend_artist = $friends_new_obj->get_friends_artist($row_friend['fgeneral_user_id']);
											if($get_all_friend_artist !="" && (!empty($art_log_g_e) || !empty($art_log_g_ec)))
											{
												$get_all_friend_artist_type = $friends_new_obj->get_friends_artist_type($get_all_friend_artist['artist_id']);
											?>
												
													<div class="friendBlkR">
													<?php
														if($get_all_friend_artist['profile_url']!="")
														{
														?>
														<a href="<?php echo $get_all_friend_artist['profile_url']; ?>"><div class="friendImg"><img src="http://artjcropthumb.s3.amazonaws.com/<?php if(isset($get_all_friend_artist['listing_image_name']) && !empty($get_all_friend_artist['listing_image_name'])) { echo $get_all_friend_artist['listing_image_name']; } else { echo "Noimage.png"; } ?>" width="100" height="100" /></div>
														<div class="friendDetails">
														
															<span class="name"><?php echo $get_all_friend_artist['name'];?></span></a><br />
															<?php
														}
														else
														{
														?>
															<div class="friendImg"><img src="http://artjcropthumb.s3.amazonaws.com/<?php if(isset($get_all_friend_artist['listing_image_name']) && !empty($get_all_friend_artist['listing_image_name'])) { echo $get_all_friend_artist['listing_image_name']; } else { echo "Noimage.png"; } ?>" width="100" height="100" /></div>
															
															<div class="friendDetails">
															<span class="name"><?php echo $get_all_friend_artist['name'];?></span><br />
														<?php
														}
															if($get_all_friend_artist_type !="")
															{
																echo $get_all_friend_artist_type['name']."</br>";
															}
															if(isset($get_all_friend_artist['city']) && !empty($get_all_friend_artist['city']))
															{
																echo substr($get_all_friend_artist['city'],0,5);
															}
															if(isset($get_all_friend_artist['state_name']) && !empty($get_all_friend_artist['state_name']))
															{
																echo ", ".substr($get_all_friend_artist['state_name'],0,5)."</br>";
															}
															?>
															<!--<a href=""><img src="images/profile/accept.png"></a>
															<a href=""><img src="images/profile/decline.png"></a>-->
											<?php
												if($art_log_g_e['artist_view_selected']!="" || $art_log_g_ec['artist_view_selected']!="")
												{
													$c_spr_e = 0;
													if(!empty($art_log_g_e))
													{
														
														$artist_E_view_exp = explode(',',$art_log_g_e['artist_view_selected']);
													}
													elseif(!empty($art_log_g_ec))
													{
														
														$artist_E_view_exp = explode(',',$art_log_g_ec['artist_view_selected']);
													}
													for($is_art_v=0;$is_art_v<count($artist_E_view_exp);$is_art_v++)
													{
														if(!empty($artist_E_view_exp[$is_art_v]) && $artist_E_view_exp[$is_art_v]!="" && $art_run['artist_id']!=0 && !empty($art_run['artist_id']) && $art_run['artist_id']!="")
														{
															if($artist_E_view_exp[$is_art_v]==$art_run['artist_id'])
															{
																$c_spr_e = $c_spr_e + 1;
															}
														}
													}
													
													if($c_spr_e>=1) 
													{
					?>
														<a style="cursor:pointer;" class="a_delete_<?php echo $all_h_d; ?>_<?php echo $art_run['artist_id']; ?>_<?php echo $art_log_g['artist_id']; ?>_<?php echo $art_log_g['community_id'] ?>" id="1a_delete_<?php echo $all_h_d; ?>_<?php echo $art_run['artist_id']; ?>_<?php echo $art_log_g['artist_id']; ?>_<?php echo $art_log_g['community_id'] ?>"><img style="margin-top:3px;"  src="images/profile/decline.png" /></a>
														
														<a title="Hide this friend from displaying on your profile page." style="cursor:pointer;" class="a_hide_<?php echo $all_h_d; ?>" id="1a_hide_<?php echo $all_h_d; ?>_<?php echo $art_run['artist_id']; ?>_<?php echo $art_log_g['artist_id']; ?>_<?php echo $art_log_g['community_id'] ?>">Hide </a>
																
														<a title="Display this friend on your profile page." style="cursor:pointer; display:none;" class="a_display_<?php echo $all_h_d; ?>" id="1a_display_<?php echo $all_h_d; ?>_<?php echo $art_run['artist_id']; ?>_<?php echo $art_log_g['artist_id']; ?>_<?php echo $art_log_g['community_id'] ?>"> Display</a>
					<?php
													}
													else
													{
					?>
														<a style="cursor:pointer;" class="a_delete_<?php echo $all_h_d; ?>_<?php echo $art_run['artist_id']; ?>_<?php echo $art_log_g['artist_id']; ?>_<?php echo $art_log_g['community_id'] ?>" id="1a_delete_<?php echo $all_h_d; ?>_<?php echo $art_run['artist_id']; ?>_<?php echo $art_log_g['artist_id']; ?>_<?php echo $art_log_g['community_id'] ?>"><img style="margin-top:3px;" src="images/profile/decline.png" /></a>
														
														<a title="Hide this friend from displaying on your profile page." style="cursor:pointer; display:none;" class="a_hide_<?php echo $all_h_d; ?>" id="1a_hide_<?php echo $all_h_d; ?>_<?php echo $art_run['artist_id']; ?>_<?php echo $art_log_g['artist_id']; ?>_<?php echo $art_log_g['community_id'] ?>">Hide </a>
																		
														<a title="Display this friend on your profile page." style="cursor:pointer;" class="a_display_<?php echo $all_h_d; ?>" id="1a_display_<?php echo $all_h_d; ?>_<?php echo $art_run['artist_id']; ?>_<?php echo $art_log_g['artist_id']; ?>_<?php echo $art_log_g['community_id'] ?>"> Display</a>
					<?php
													}
												}
												else
												{
					?>
													<a style="cursor:pointer; " class="a_delete_<?php echo $all_h_d; ?>_<?php echo $art_run['artist_id']; ?>_<?php echo $art_log_g['artist_id']; ?>_<?php echo $art_log_g['community_id'] ?>" id="1a_delete_<?php echo $all_h_d; ?>_<?php echo $art_run['artist_id']; ?>_<?php echo $art_log_g['artist_id']; ?>_<?php echo $art_log_g['community_id'] ?>"><img style="margin-top:3px;" src="images/profile/decline.png" /></a>
													
													<a title="Hide this friend from displaying on your profile page." style="cursor:pointer;" class="a_hide_<?php echo $all_h_d; ?>" id="1a_hide_<?php echo $all_h_d; ?>_<?php echo $art_run['artist_id']; ?>_<?php echo $art_log_g['artist_id']; ?>_<?php echo $art_log_g['community_id'] ?>">Hide </a>
																	
													<a title="Display this friend on your profile page." style="cursor:pointer;" class="a_display_<?php echo $all_h_d; ?>" id="1a_display_<?php echo $all_h_d; ?>_<?php echo $art_run['artist_id']; ?>_<?php echo $art_log_g['artist_id']; ?>_<?php echo $art_log_g['community_id'] ?>"> Display</a>
					<?php
												}
					?>
										
													</div>
													</div>
										<script>
											$('div#all_friends_fans div.friendCont div.friendBlkR div.friendDetails a.a_hide_<?php echo $all_h_d; ?>').click(function()
											{
												var id = $(this).attr("id");
												var id_h_b = '1a_display_<?php echo $all_h_d; ?>_<?php echo $art_run['artist_id']; ?>_<?php echo $art_log_g['artist_id']; ?>_<?php echo $art_log_g['community_id'] ?>';
												//alert(id);
												
												var dataString_art = 'hide_dis='+ id;
												
												$.ajax({
													type: 'POST',
													url : 'FriendAjaxacc_dec.php',
													data: dataString_art,
													success: function(data)  
													{ 
														document.getElementById(id).style.display="none";
														document.getElementById(id_h_b).style.display="inline";
														alert("Your settings are saved.");
														//alert(data);
														//document.getElementById("friend_status_dis_<?php echo $all_h_d; ?>").style.display="block";
														//$("#friend_status_dis_<?php echo $all_h_d; ?>").html(data);
														//alert(data);
													}
												});
											});
											
											$('div#all_friends_fans div.friendCont div.friendBlkR div.friendDetails a.a_display_<?php echo $all_h_d; ?>').click(function()
											{
												var id = $(this).attr("id");
												var id_d_b = '1a_hide_<?php echo $all_h_d; ?>_<?php echo $art_run['artist_id']; ?>_<?php echo $art_log_g['artist_id']; ?>_<?php echo $art_log_g['community_id'] ?>';
												
												var dataString_art = 'hide_dis='+ id;
												
												$.ajax({
													type: 'POST',
													url : 'FriendAjaxacc_dec.php',
													data: dataString_art,
													success: function(data)  
													{ 
														document.getElementById(id).style.display="none";
														document.getElementById(id_d_b).style.display="inline";
														alert("Your settings are saved.");
														//alert(data);
														//document.getElementById("friend_status_dis_<?php echo $all_h_d; ?>").style.display="block";
														//$("#friend_status_dis_<?php echo $all_h_d; ?>").html(data);
														//alert(data);
													}
												});
											});
											
											$('div#all_friends_fans div.friendCont div.friendBlkR div.friendDetails a.a_delete_<?php echo $all_h_d; ?>_<?php echo $art_run['artist_id']; ?>_<?php echo $art_log_g['artist_id']; ?>_<?php echo $art_log_g['community_id'] ?>').click(function()
											{
												var id = $(this).attr("id");
												var dataString_art = 'hide_dis='+ id;
												<?php 
													if($art_run['community_id'] !="" && $art_run['community_id'] !=0 && $art_run['artist_id'] !="" && $art_run['artist_id'] !=0){
														$get_artist_name_res = $friends_new_obj->get_artist_name($art_run['artist_id']);
														$get_community_name_res = $friends_new_obj->get_community_name($art_run['community_id']);
												?>
													var con_x = confirm("Are you sure you want to remove <?php echo $get_artist_name_res['name'];?> and <?php echo $get_community_name_res['name'];?> as friends?");
												<?php	
													}
													else if($art_run['artist_id'] !="" && $art_run['artist_id'] !=0 && ($art_run['community_id'] =="" || $art_run['community_id'] ==0)){
													$get_artist_name_res = $friends_new_obj->get_artist_name($art_run['artist_id']);
												?>
													var con_x = confirm("Are you sure you want to remove <?php echo $get_artist_name_res['name'];?> as friends?");
												<?php	
													}
												?>
												//var con_x = confirm("Are you sure you want to delete? Along with these your Friend's Community Profile will also be deleted will be deleted from your Friend List.");
												
												if(con_x==true)
												{
													$.ajax({
														type: 'POST',
														url : 'FriendAjaxacc_dec.php',
														data: dataString_art,
														success: function(data)  
														{
															//alert("Your settings are saved.");
															alert("Your Friend <?php echo $art_run['fname'].' '.$art_run['lname']; ?> is not your friend anymore.");
															window.location.reload(true);
															//alert(data);
															//document.getElementById("friend_status_dis_<?php echo $all_h_d; ?>").style.display="block";
															//$("#friend_status_dis_<?php echo $all_h_d; ?>").html(data);
															//alert(data);
														}
													});
												}
												
												
												if(con_x==false)
												{}
											});
										</script>
				<?php
											}
											$get_all_friend_community = $friends_new_obj->get_friends_community($row_friend['fgeneral_user_id']);
											
											if($get_all_friend_community !="" && (!empty($art_log_g_e) || !empty($art_log_g_ec))) 
											{
												$get_all_friend_community_type = $friends_new_obj->get_friends_community_type($get_all_friend_community['community_id']);
											?>
													<div class="friendBlkR">
													<?php
														if($get_all_friend_community['profile_url']!="")
														{
														?>
														<a href="<?php echo $get_all_friend_community['profile_url']; ?>"><div class="friendImg"><img src="http://comjcropthumb.s3.amazonaws.com/<?php if(isset($get_all_friend_community['listing_image_name']) && !empty($get_all_friend_community['listing_image_name'])) { echo $get_all_friend_community['listing_image_name']; } else { echo "Noimage.png"; } ?>" width="100" height="100" /></div>
														<div class="friendDetails">
														
															<span class="name"><?php echo $get_all_friend_community['name'];?></span></a><br />
														<?php
														}
														else
														{
														?>
															<div class="friendImg"><img src="http://comjcropthumb.s3.amazonaws.com/<?php if(isset($get_all_friend_community['listing_image_name']) && !empty($get_all_friend_community['listing_image_name'])) { echo $get_all_friend_community['listing_image_name']; } else { echo "Noimage.png"; } ?>" width="100" height="100" /></div>
															
															<div class="friendDetails">
															<span class="name"><?php echo $get_all_friend_community['name'];?></span><br />
														<?php
														}
															if($get_all_friend_community_type !="")
															{
																echo $get_all_friend_community_type['name']."</br>";
															}
															if(isset($get_all_friend_community['city']) && !empty($get_all_friend_community['city']))
															{
																echo substr($get_all_friend_community['city'],0,5);
															}
															if(isset($get_all_friend_community['state_name']) && !empty($get_all_friend_community['state_name']))
															{
																echo ", ".substr($get_all_friend_community['state_name'],0,5)."</br>";
															}
															?>
															<!--<a href=""><img src="images/profile/accept.png"></a>
															<a href=""><img src="images/profile/decline.png"></a>-->
														
											<?php	
												
												if($art_log_g_e['community_view_selected']!="" || $art_log_g_ec['community_view_selected']!="")
												{
													$c_spr_e = 0;
													if(!empty($art_log_g_e))
													{
														$artist_E_view_exp = explode(',',$art_log_g_e['community_view_selected']);
													}
													elseif(!empty($art_log_g_ec))
													{
														$artist_E_view_exp = explode(',',$art_log_g_ec['community_view_selected']);
													}
													for($ais_art_v=0;$ais_art_v<count($artist_E_view_exp);$ais_art_v++)
													{
														if(!empty($artist_E_view_exp[$ais_art_v]) && $artist_E_view_exp[$ais_art_v]!="" && $art_run['community_id']!=0 && !empty($art_run['community_id']) && $art_run['community_id']!="")
														{
															if($artist_E_view_exp[$ais_art_v]==$art_run['community_id'])
															{
																$c_spr_e = $c_spr_e + 1;
															}
														}
													}
													//echo $art_run['community_id'];
													if($c_spr_e>=1) 
													{
				?>
																	<a style="cursor:pointer; " class="c_delete_<?php echo $all_h_d; ?>_<?php echo $art_run['community_id']; ?>_<?php echo $art_log_g['artist_id']; ?>_<?php echo $art_log_g['community_id']; ?>" id="1c_delete_<?php echo $all_h_d; ?>_<?php echo $art_run['community_id']; ?>_<?php echo $art_log_g['artist_id']; ?>_<?php echo $art_log_g['community_id']; ?>"><img style="margin-top:3px;" src="images/profile/decline.png" /></a>
																	
																	<a title="Hide this friend from displaying on your profile page." style="cursor:pointer;" class="c_hide_<?php echo $all_h_d; ?>" id="1c_hide_<?php echo $all_h_d; ?>_<?php echo $art_run['community_id']; ?>_<?php echo $art_log_g['artist_id']; ?>_<?php echo $art_log_g['community_id']; ?>">Hide </a>
																	
																	<a title="Display this friend on your profile page." style="cursor:pointer; display:none;" class="c_display_<?php echo $all_h_d; ?>" id="1c_display_<?php echo $all_h_d; ?>_<?php echo $art_run['community_id']; ?>_<?php echo $art_log_g['artist_id']; ?>_<?php echo $art_log_g['community_id']; ?>"> Display</a>
				<?php
																}
																else
																{
																	
				?>
																	<a style="cursor:pointer; " class="c_delete_<?php echo $all_h_d; ?>_<?php echo $art_run['community_id']; ?>_<?php echo $art_log_g['artist_id']; ?>_<?php echo $art_log_g['community_id']; ?>" id="1c_delete_<?php echo $all_h_d; ?>_<?php echo $art_run['community_id']; ?>_<?php echo $art_log_g['artist_id']; ?>_<?php echo $art_log_g['community_id']; ?>"><img style="margin-top:3px;" src="images/profile/decline.png" /></a>
																	
																	<a title="Hide this friend from displaying on your profile page." style="cursor:pointer; display:none;" class="c_hide_<?php echo $all_h_d; ?>" id="1c_hide_<?php echo $all_h_d; ?>_<?php echo $art_run['community_id']; ?>_<?php echo $art_log_g['artist_id']; ?>_<?php echo $art_log_g['community_id']; ?>">Hide </a>
																	
																	<a title="Display this friend on your profile page." style="cursor:pointer;" class="c_display_<?php echo $all_h_d; ?>" id="1c_display_<?php echo $all_h_d; ?>_<?php echo $art_run['community_id']; ?>_<?php echo $art_log_g['artist_id']; ?>_<?php echo $art_log_g['community_id']; ?>"> Display</a>
				<?php
																}
												}
												else
												{
													echo "hii";
				?>
													<a style="cursor:pointer;" class="c_delete_<?php echo $all_h_d; ?>_<?php echo $art_run['community_id']; ?>_<?php echo $art_log_g['artist_id']; ?>_<?php echo $art_log_g['community_id']; ?>" id="1c_delete_<?php echo $all_h_d; ?>_<?php echo $art_run['community_id']; ?>_<?php echo $art_log_g['artist_id']; ?>_<?php echo $art_log_g['community_id']; ?>"><img style="margin-top:3px;" src="images/profile/decline.png" /></a>
													
													<a title="Hide this friend from displaying on your profile page." style="cursor:pointer;" class="c_hide_<?php echo $all_h_d; ?>" id="1c_hide_<?php echo $all_h_d; ?>_<?php echo $art_run['community_id']; ?>_<?php echo $art_log_g['artist_id']; ?>_<?php echo $art_log_g['community_id']; ?>">Hide </a>
																
													<a title="Display this friend on your profile page." style="cursor:pointer;" class="c_display_<?php echo $all_h_d; ?>" id="1c_display_<?php echo $all_h_d; ?>_<?php echo $art_run['community_id']; ?>_<?php echo $art_log_g['artist_id']; ?>_<?php echo $art_log_g['community_id']; ?>"> Display</a>
				<?php
												}
				?>
														</div>
													</div>
													
													<script>
														$('div#all_friends_fans div.friendCont div.friendBlkR div.friendDetails a.c_hide_<?php echo $all_h_d; ?>').click(function()
														{
															var id = $(this).attr("id");
															var id_h_b = '1c_display_<?php echo $all_h_d; ?>_<?php echo $art_run['community_id']; ?>_<?php echo $art_log_g['artist_id']; ?>_<?php echo $art_log_g['community_id']; ?>';
															//alert(id);
															
															var dataString_art = 'hide_dis='+ id;
															
															$.ajax({
																type: 'POST',
																url : 'FriendAjaxacc_dec.php',
																data: dataString_art,
																success: function(data)  
																{ 
																	document.getElementById(id).style.display="none";
																	document.getElementById(id_h_b).style.display="inline";
																	alert("Your settings are saved.");
																	//alert(data);
																	//document.getElementById("friend_status_dis_<?php echo $all_h_d; ?>").style.display="block";
																	//$("#friend_status_dis_<?php echo $all_h_d; ?>").html(data);
																	//alert(data);
																}
															});
														});
														
														$('div#all_friends_fans div.friendCont div.friendBlkR div.friendDetails a.c_display_<?php echo $all_h_d; ?>').click(function()
														{
															var id = $(this).attr("id");
															var id_d_b = '1c_hide_<?php echo $all_h_d; ?>_<?php echo $art_run['community_id']; ?>_<?php echo $art_log_g['artist_id']; ?>_<?php echo $art_log_g['community_id']; ?>';
															
															var dataString_art = 'hide_dis='+ id;
															
															$.ajax({
																type: 'POST',
																url : 'FriendAjaxacc_dec.php',
																data: dataString_art,
																success: function(data)  
																{ 
																	document.getElementById(id).style.display="none";
																	document.getElementById(id_d_b).style.display="inline";
																	alert("Your settings are saved.");
																	//alert(data);
																	//document.getElementById("friend_status_dis_<?php echo $all_h_d; ?>").style.display="block";
																	//$("#friend_status_dis_<?php echo $all_h_d; ?>").html(data);
																	//alert(data);
																}
															});
														});
														
														$('div#all_friends_fans div.friendCont div.friendBlkR div.friendDetails a.c_delete_<?php echo $all_h_d; ?>_<?php echo $art_run['community_id']; ?>_<?php echo $art_log_g['artist_id']; ?>_<?php echo $art_log_g['community_id']; ?>').click(function()
														{
															var id = $(this).attr("id");
															var dataString_art = 'hide_dis='+ id;
															<?php 
																if($art_run['community_id'] !="" && $art_run['community_id'] != 0 && $art_run['artist_id'] !="" && $art_run['artist_id'] !=0){
																	$get_artist_name_res = $friends_new_obj->get_artist_name($art_run['artist_id']);
																	$get_community_name_res = $friends_new_obj->get_community_name($art_run['community_id']);
															?>
																var con_x = confirm("Are you sure you want to remove <?php echo $get_artist_name_res['name'];?> and <?php echo $get_community_name_res['name'];?> as friends?");
															<?php	
																}
																else if($art_run['community_id'] !="" && $art_run['community_id'] != 0 && ($art_run['artist_id'] =="" || $art_run['artist_id'] ==0)){
																$get_community_name_res = $friends_new_obj->get_community_name($art_run['community_id']);
															?>
																var con_x = confirm("Are you sure you want to remove <?php echo $get_community_name_res['name'];?> as friends?");
															<?php	
																}
															?>
															//var con_x = confirm("Are you sure you want to delete? Along with these your Friend's Artist Profile will also be deleted From your Friend List.");
															
															if(con_x==true)
															{
																$.ajax({
																	type: 'POST',
																	url : 'FriendAjaxacc_dec.php',
																	data: dataString_art,
																	success: function(data)  
																	{
																		alert("Your Friend <?php echo $art_run['fname'].' '.$art_run['lname']; ?> is not your friend anymore.");
																		window.location.reload(true);

																		//alert("Your settings are saved.");
																		//alert(data);
																		//document.getElementById("friend_status_dis_<?php echo $a_h_d; ?>").style.display="block";
																		//$("#friend_status_dis_<?php echo $a_h_d; ?>").html(data);
																		//alert(data);
																	}
																});
															}
															
															
															if(con_x==false)
															{}
														});
													</script>
				<?php
												
											}
											$all_h_d = $all_h_d + 1;
										} 
									}
								}
								?>
								</div>
							</div>
							
							<div id="artist_fansa" style="display:none;">
								<div class="fieldCont"><b>Artist Fans</b></div>
									<div class="friendCont">
		<?php
								$get_art_log = $friends_new_obj->general_user_email_gen($_SESSION['login_email']);
								if($get_art_log['artist_id']!=0)
								{
									$get_art_fan = $fans_new_obj->getartist_fans($get_art_log['artist_id']);
									if(count($get_art_fan)>0)
									{
										foreach ($get_art_fan as $uid => $n)
										{
											$ex_uid1 = $ex_uid1.','.$uid;
										}
										$ex_uid2 = trim($ex_uid1, ",");
										$get_art_fan_ex = explode(',',$ex_uid2);
										
										for($f_ai=0;$f_ai<count($get_art_fan_ex);$f_ai++)
										{
											if($get_art_fan_ex[$f_ai]!="")
											{
												$get_fan_type = $fans_new_obj->get_types_art($get_art_log['artist_id'],$get_art_fan_ex[$f_ai]);
												//echo $get_art_fan['type'.$f_ai];
												$get_genart_fan = $friends_new_obj->general_user_email_gen($get_art_fan_ex[$f_ai]);
				?>
											
												<div class="friendBlkR">
													<div class="friendImg"><img src="http://generalproimage.s3.amazonaws.com/<?php if($get_genart_fan['image_name']!="") { echo $get_genart_fan['image_name']; } else { echo "Noimage.png"; } ?>" width="100" height="100" /></div>
													<div class="friendDetails">
														<span class="name"><?php echo $get_genart_fan['fname'].' '.$get_genart_fan['lname']; ?></span><br />
														<?php 
														if(isset($get_fan_type) && $get_fan_type!="" && $get_fan_type!='0') 
														{
															echo $get_fan_type.'<br />';
														}
														if($get_genart_fan['city']!="" && isset($get_genart_fan['city']))
														{
															echo substr($get_genart_fan['city'],0,5);
														}
														if($get_genart_fan['state_id']!="" && $get_genart_fan['state_id']!=0 && isset($get_genart_fan['state_id']))
														{
															$get_state = $fans_new_obj->user_gen_fan($get_genart_fan['general_user_id']);
															echo ', '.substr($get_state['state_name'],0,5); 
														}
														?><br />
													</div>
												</div>
			<?php
											}
										}
									}
								}
		?>
								</div>
							</div>
							
							<div id="artist_friendsa" style="display:none;">
								<div class="fieldCont"><b>Artist Friends</b></div>
								<div class="friendCont">
			<?php
									$art_sql = $friends_suggest_obj->friend_art($_SESSION['login_id']);
									if($art_sql!="" && $art_sql!=NULL)
									{
									if(mysql_num_rows($art_sql)>0)
									{
										$a_h_d = 0;
										
										while($rows_a_f = mysql_fetch_assoc($art_sql))
										{
											$c_pr_e = 0;
											$art_run = $friends_suggest_obj->general_user_sec($rows_a_f['fgeneral_user_id']);
											$art_log_g = $friends_suggest_obj->general_user_sec($_SESSION['login_id']);
											if($art_run['artist_id']!=0)
											{
												$art_ans = $friends_suggest_obj->user_art_friend($art_run['artist_id']);
												$art_log_g_e = $friends_suggest_obj->user_art_friend($art_log_g['artist_id']);
												$art_log_g_ec = $friends_suggest_obj->user_com_friend($art_log_g['community_id']);
												$artist_type = $friends_suggest_obj->get_friends_artist_type($art_run['artist_id']);
												
												if(!empty($art_ans))
												{
			?>
												<div class="friendBlkR">
												<?php
													if($art_ans['profile_url']!="")
													{
													?>
													<a href="<?php echo $art_ans['profile_url']; ?>"><div class="friendImg"><img src="http://artjcropthumb.s3.amazonaws.com/<?php if($art_ans['listing_image_name']!="") { echo $art_ans['listing_image_name']; } else { echo "Noimage.png"; } ?>" width="100" height="100" /></div>
													<div class="friendDetails">
													
														<span class="name"><?php echo $art_ans['name']; ?></span></a><br />
													<?php
													}
													else
													{
													?>
														<div class="friendImg"><img src="http://artjcropthumb.s3.amazonaws.com/<?php if($art_ans['listing_image_name']!="") { echo $art_ans['listing_image_name']; } else { echo "Noimage.png"; } ?>" width="100" height="100" /></div>
														<div class="friendDetails">
													
														<span class="name"><?php echo $art_ans['name']; ?></span><br />
													<?php
													}
															if($artist_type!="")
															{
																echo $artist_type['name'];
															}	
															?><br />
														<?php if($art_ans['city']!="" && !empty($art_ans['city'])) { echo substr($art_ans['city'],0,5); }
															  if(isset($art_ans['state_name']) && !empty($art_ans['state_name'])) { echo ', '.substr($art_ans['state_name'],0,5); } ?><br />
			<?php
														
														if($art_log_g_e['artist_view_selected']!="" || $art_log_g_ec['artist_view_selected']!="")
														{
															if(!empty($art_log_g_e))
															{
																$artist_E_view_exp = explode(',',$art_log_g_e['artist_view_selected']);
															}
															elseif(!empty($art_log_g_ec))
															{
																$artist_E_view_exp = explode(',',$art_log_g_ec['artist_view_selected']);
															}
															for($i_art_v=0;$i_art_v<count($artist_E_view_exp);$i_art_v++)
															{
																if($artist_E_view_exp[$i_art_v]==$art_run['artist_id'])
																{
																	$c_pr_e = $c_pr_e + 1;
																}
															}
															if($c_pr_e>=1) 
															{
			?>
																<a style="cursor:pointer; " class="a_delete_<?php echo $a_h_d; ?>_<?php echo $art_run['artist_id']; ?>_<?php echo $art_log_g['artist_id']; ?>_<?php echo $art_log_g['community_id'] ?>" id="a_delete_<?php echo $a_h_d; ?>_<?php echo $art_run['artist_id']; ?>_<?php echo $art_log_g['artist_id']; ?>_<?php echo $art_log_g['community_id'] ?>"><img style="margin-top:3px;" src="images/profile/decline.png" /></a>
																
																<a title="Hide this friend from displaying on your profile page." style="cursor:pointer;" class="a_hide_<?php echo $a_h_d; ?>" id="a_hide_<?php echo $a_h_d; ?>_<?php echo $art_run['artist_id']; ?>_<?php echo $art_log_g['artist_id']; ?>_<?php echo $art_log_g['community_id'] ?>">Hide </a>
																
																<a title="Display this friend on your profile page." style="cursor:pointer; display:none;" class="a_display_<?php echo $a_h_d; ?>" id="a_display_<?php echo $a_h_d; ?>_<?php echo $art_run['artist_id']; ?>_<?php echo $art_log_g['artist_id']; ?>_<?php echo $art_log_g['community_id'] ?>"> Display</a>
			<?php
															}
															else
															{
			?>
																<a style="cursor:pointer; " class="a_delete_<?php echo $a_h_d; ?>_<?php echo $art_run['artist_id']; ?>_<?php echo $art_log_g['artist_id']; ?>_<?php echo $art_log_g['community_id'] ?>" id="a_delete_<?php echo $a_h_d; ?>_<?php echo $art_run['artist_id']; ?>_<?php echo $art_log_g['artist_id']; ?>_<?php echo $art_log_g['community_id'] ?>"><img style="margin-top:3px;" src="images/profile/decline.png" /></a>
																
																<a title="Hide this friend from displaying on your profile page." style="cursor:pointer; display:none;" class="a_hide_<?php echo $a_h_d; ?>" id="a_hide_<?php echo $a_h_d; ?>_<?php echo $art_run['artist_id']; ?>_<?php echo $art_log_g['artist_id']; ?>_<?php echo $art_log_g['community_id'] ?>">Hide </a>
																
																<a title="Display this friend on your profile page." style="cursor:pointer;" class="a_display_<?php echo $a_h_d; ?>" id="a_display_<?php echo $a_h_d; ?>_<?php echo $art_run['artist_id']; ?>_<?php echo $art_log_g['artist_id']; ?>_<?php echo $art_log_g['community_id'] ?>"> Display</a>
			<?php
															}
														}
														else
														{
			?>
															<a style="cursor:pointer; " class="a_delete_<?php echo $a_h_d; ?>_<?php echo $art_run['artist_id']; ?>_<?php echo $art_log_g['artist_id']; ?>_<?php echo $art_log_g['community_id'] ?>" id="a_delete_<?php echo $a_h_d; ?>_<?php echo $art_run['artist_id']; ?>_<?php echo $art_log_g['artist_id']; ?>_<?php echo $art_log_g['community_id'] ?>"><img style="margin-top:3px;" src="images/profile/decline.png" /></a>
															
															<a title="Hide this friend from displaying on your profile page." style="cursor:pointer;" class="a_hide_<?php echo $a_h_d; ?>" id="a_hide_<?php echo $a_h_d; ?>_<?php echo $art_run['artist_id']; ?>_<?php echo $art_log_g['artist_id']; ?>_<?php echo $art_log_g['community_id'] ?>">Hide </a>
															
															<a title="Display this friend on your profile page." style="cursor:pointer;" class="a_display_<?php echo $a_h_d; ?>" id="a_display_<?php echo $a_h_d; ?>_<?php echo $art_run['artist_id']; ?>_<?php echo $art_log_g['artist_id']; ?>_<?php echo $art_log_g['community_id'] ?>"> Display</a>
			<?php
														}
			?>
													</div>
												</div>
												
												<script>
													$('div#artist_friendsa div.friendCont div.friendBlkR div.friendDetails a.a_hide_<?php echo $a_h_d; ?>').click(function()
													{
														var id = $(this).attr("id");
														var id_h_b = 'a_display_<?php echo $a_h_d; ?>_<?php echo $art_run['artist_id']; ?>_<?php echo $art_log_g['artist_id']; ?>_<?php echo $art_log_g['community_id'] ?>';
														//alert(id);
														
														var dataString_art = 'hide_dis='+ id;
														
														$.ajax({
															type: 'POST',
															url : 'FriendAjaxacc_dec.php',
															data: dataString_art,
															success: function(data)  
															{ 
																document.getElementById(id).style.display="none";
																document.getElementById(id_h_b).style.display="inline";
																alert("Your settings are saved.");
																//alert(data);
																//document.getElementById("friend_status_dis_<?php echo $a_h_d; ?>").style.display="block";
																//$("#friend_status_dis_<?php echo $a_h_d; ?>").html(data);
																//alert(data);
															}
														});
													});
													
													$('div#artist_friendsa div.friendCont div.friendBlkR div.friendDetails a.a_display_<?php echo $a_h_d; ?>').click(function()
													{
														var id = $(this).attr("id");
														var id_d_b = 'a_hide_<?php echo $a_h_d; ?>_<?php echo $art_run['artist_id']; ?>_<?php echo $art_log_g['artist_id']; ?>_<?php echo $art_log_g['community_id'] ?>';
														
														var dataString_art = 'hide_dis='+ id;
														
														$.ajax({
															type: 'POST',
															url : 'FriendAjaxacc_dec.php',
															data: dataString_art,
															success: function(data)  
															{ 
																document.getElementById(id).style.display="none";
																document.getElementById(id_d_b).style.display="inline";
																alert("Your settings are saved.");
																//alert(data);
																//document.getElementById("friend_status_dis_<?php echo $a_h_d; ?>").style.display="block";
																//$("#friend_status_dis_<?php echo $a_h_d; ?>").html(data);
																//alert(data);
															}
														});
													});
													
													$('div#artist_friendsa div.friendCont div.friendBlkR div.friendDetails a.a_delete_<?php echo $a_h_d; ?>_<?php echo $art_run['artist_id']; ?>_<?php echo $art_log_g['artist_id']; ?>_<?php echo $art_log_g['community_id'] ?>').click(function()
													{
														var id = $(this).attr("id");
														var dataString_art = 'hide_dis='+ id;
														
														var con_x = confirm("Are you sure you want to delete? Along with these your Friend's Community Profile will also be deleted will be deleted from your Friend List.");
														
														if(con_x==true)
														{
															$.ajax({
																type: 'POST',
																url : 'FriendAjaxacc_dec.php',
																data: dataString_art,
																success: function(data)  
																{
																	//alert("Your settings are saved.");
																	alert("Your Friend <?php echo $art_run['fname'].' '.$art_run['lname']; ?> is not your friend anymore.");
																	window.location.reload(true);
																	//alert(data);
																	//document.getElementById("friend_status_dis_<?php echo $a_h_d; ?>").style.display="block";
																	//$("#friend_status_dis_<?php echo $a_h_d; ?>").html(data);
																	//alert(data);
																}
															});
														}
														
														
														if(con_x==false)
														{}
													});
												</script>
			<?php
												$a_h_d = $a_h_d + 1;
												}
											}
										}
									}
								}
			?>
								</div>
							</div>
							
							<div id="community_fansa" style="display:none;">
								<div class="fieldCont"><b>Community Fans</b></div>
								<div class="friendCont">
			<?php
								$get_com_log = $friends_new_obj->general_user_email_gen($_SESSION['login_email']);
								if($get_com_log['community_id']!=0)
								{
									$get_com_fan = $fans_new_obj->getcommunity_fans($get_com_log['community_id']);
									if(count($get_com_fan)>0)
									{
										foreach ($get_com_fan as $uid => $n)
										{
											$ex_uid_1 = $ex_uid_1.','.$uid;
										}
										$ex_uid_2 = trim($ex_uid_1, ",");
										$get_com_fan_ex = explode(',',$ex_uid_2);
										
										for($f_ai=0;$f_ai<count($get_com_fan_ex);$f_ai++)
										{
											if($get_com_fan_ex[$f_ai]!="")
											{
												$get_fan_type = $fans_new_obj->get_types_com($get_com_log['community_id'],$get_com_fan_ex[$f_ai]);
												//echo $get_com_fan['type'.$f_ai];
												$get_gencom_fan = $friends_new_obj->general_user_email_gen($get_com_fan_ex[$f_ai]);
												if($get_gencom_fan!="")
												{
				?>
											
													<div class="friendBlkR"> 
														<div class="friendImg"><img src="http://generalproimage.s3.amazonaws.com/<?php if($get_gencom_fan['image_name']!="") { echo $get_gencom_fan['image_name']; } else { echo "Noimage.png"; } ?>" width="100" height="100" /></div>
														<div class="friendDetails">
															<span class="name"><?php echo $get_gencom_fan['fname'].' '.$get_gencom_fan['lname']; ?></span><br />
															<?php 
															if(isset($get_fan_type) && $get_fan_type!="" && $get_fan_type!='0') 
															{
																echo $get_fan_type.'<br />';
															}
															if($get_gencom_fan['city']!="" && !empty($get_gencom_fan['city']))
															{
																echo substr($get_gencom_fan['city'],0,5);
															}
															if($get_gencom_fan['state_id']!="" && $get_gencom_fan['state_id']!=0 && !empty($get_gencom_fan['state_id']))
															{
																$get_state = $fans_new_obj->user_gen_fan($get_gencom_fan['general_user_id']);
																echo ', '.substr($get_state['state_name'],0,5); 
															}
															?><br />
														</div>
													</div>
			<?php
												}
											}
										}
									}
								}
			?>
								</div>
							</div>
							
							<div id="community_friendsa" style="display:none;">
								<div class="fieldCont"><b>Company Friends</b></div>
								<div class="friendCont">
		<?php
									$com_sql = $friends_suggest_obj->friend_art($_SESSION['login_id']);
									if($com_sql!="" && $com_sql!=NULL)
									{
									if(mysql_num_rows($com_sql)>0)
									{
										$c_h_d = 0;
										
										while($rows_c_f = mysql_fetch_assoc($com_sql))
										{
											$ca_pr_e = 0;
											$com_run = $friends_suggest_obj->general_user_sec($rows_c_f['fgeneral_user_id']);
											$com_log_g = $friends_suggest_obj->general_user_sec($_SESSION['login_id']);
											if($com_run['community_id']!=0)
											{
												$com_ans = $friends_suggest_obj->user_com_friend($com_run['community_id']);
												$art_log_g_e = $friends_suggest_obj->user_art_friend($com_log_g['artist_id']);
												$com_log_g_e = $friends_suggest_obj->user_com_friend($com_log_g['community_id']);
												$community_type = $friends_suggest_obj->get_friends_community_type($com_run['community_id']);
												
												if(!empty($com_ans))
												{
			?>
												<div class="friendBlkR">
												<?php
													if($com_ans['profile_url']!="")
													{
												?>
													<a href="<?php echo $com_ans['profile_url']; ?>"><div class="friendImg"><img src="http://comjcropthumb.s3.amazonaws.com/<?php if($com_ans['listing_image_name']!="") { echo $com_ans['listing_image_name']; } else { echo "Noimage.png"; } ?>" width="100" height="100" /></div>
													<div class="friendDetails">
													
														<span class="name"><?php echo $com_ans['name']; ?></span></a><br />
													<?php
													}
													else
													{
													?>
														<div class="friendImg"><img src="http://comjcropthumb.s3.amazonaws.com/<?php if($com_ans['listing_image_name']!="") { echo $com_ans['listing_image_name']; } else { echo "Noimage.png"; } ?>" width="100" height="100" /></div>
														<div class="friendDetails">
													
														<span class="name"><?php echo $com_ans['name']; ?></span><br />
													<?php
													}
														if($community_type !="")
														{
															echo $community_type['name'];
														}
														
														?>
														<?php if($com_ans['city']!="" && !empty($com_ans['city'])) { echo substr($com_ans['city'],0,5); }
														      if(isset($com_ans['state_name']) && !empty($com_ans['state_name'])) { echo ', '.substr($com_ans['state_name'],0,5); } ?><br />
			<?php
														
														if($com_log_g_e['community_view_selected']!="" || $art_log_g_e['community_view_selected']!="")
														{
															if(!empty($art_log_g_e))
															{
																$artist_E_view_exp = explode(',',$art_log_g_e['community_view_selected']);
															}
															elseif(!empty($com_log_g_e))
															{
																$artist_E_view_exp = explode(',',$com_log_g_e['community_view_selected']);
															}
															for($i_art_v=0;$i_art_v<count($artist_E_view_exp);$i_art_v++)
															{
																if($artist_E_view_exp[$i_art_v]==$com_run['community_id'])
																{
																	$ca_pr_e = $ca_pr_e + 1;
																}
															}
															if($ca_pr_e>=1)
															{
			?>
																<a style="cursor:pointer; " class="c_delete_<?php echo $c_h_d; ?>_<?php echo $com_run['community_id']; ?>_<?php echo $com_log_g['artist_id']; ?>_<?php echo $com_log_g['community_id']; ?>" id="c_delete_<?php echo $c_h_d; ?>_<?php echo $com_run['community_id']; ?>_<?php echo $com_log_g['artist_id']; ?>_<?php echo $com_log_g['community_id']; ?>"><img style="margin-top:3px;" src="images/profile/decline.png" /></a>
																
																<a title="Hide this friend from displaying on your profile page." style="cursor:pointer;" class="c_hide_<?php echo $c_h_d; ?>" id="c_hide_<?php echo $c_h_d; ?>_<?php echo $com_run['community_id']; ?>_<?php echo $com_log_g['artist_id']; ?>_<?php echo $com_log_g['community_id']; ?>">Hide </a>
																
																<a title="Display this friend on your profile page." style="cursor:pointer; display:none;" class="c_display_<?php echo $c_h_d; ?>" id="c_display_<?php echo $c_h_d; ?>_<?php echo $com_run['community_id']; ?>_<?php echo $com_log_g['artist_id']; ?>_<?php echo $com_log_g['community_id']; ?>"> Display</a>
			<?php
															}
															else
															{
			?>
																<a style="cursor:pointer; " class="c_delete_<?php echo $c_h_d; ?>_<?php echo $com_run['community_id']; ?>_<?php echo $com_log_g['artist_id']; ?>_<?php echo $com_log_g['community_id']; ?>" id="c_delete_<?php echo $c_h_d; ?>_<?php echo $com_run['community_id']; ?>_<?php echo $com_log_g['artist_id']; ?>_<?php echo $com_log_g['community_id']; ?>"><img style="margin-top:3px;" src="images/profile/decline.png" /></a>
																
																<a title="Hide this friend from displaying on your profile page." style="cursor:pointer; display:none;" class="c_hide_<?php echo $c_h_d; ?>" id="c_hide_<?php echo $c_h_d; ?>_<?php echo $com_run['community_id']; ?>_<?php echo $com_log_g['artist_id']; ?>_<?php echo $com_log_g['community_id']; ?>">Hide </a>
																
																<a title="Display this friend on your profile page." style="cursor:pointer;" class="c_display_<?php echo $c_h_d; ?>" id="c_display_<?php echo $c_h_d; ?>_<?php echo $com_run['community_id']; ?>_<?php echo $com_log_g['artist_id']; ?>_<?php echo $com_log_g['community_id']; ?>"> Display</a>
			<?php
															}
														}
														else
														{
			?>
															<a style="cursor:pointer; " class="c_delete_<?php echo $c_h_d; ?>_<?php echo $com_run['community_id']; ?>_<?php echo $com_log_g['artist_id']; ?>_<?php echo $com_log_g['community_id']; ?>" id="c_delete_<?php echo $c_h_d; ?>_<?php echo $com_run['community_id']; ?>_<?php echo $com_log_g['artist_id']; ?>_<?php echo $com_log_g['community_id']; ?>"><img style="margin-top:3px;" src="images/profile/decline.png" /></a>
															
															<a title="Hide this friend from displaying on your profile page." style="cursor:pointer;" class="c_hide_<?php echo $c_h_d; ?>" id="c_hide_<?php echo $c_h_d; ?>_<?php echo $com_run['community_id']; ?>_<?php echo $com_log_g['artist_id']; ?>_<?php echo $com_log_g['community_id']; ?>">Hide </a>
															
															<a title="Display this friend on your profile page." style="cursor:pointer;" class="c_display_<?php echo $c_h_d; ?>" id="c_display_<?php echo $c_h_d; ?>_<?php echo $com_run['community_id']; ?>_<?php echo $com_log_g['artist_id']; ?>_<?php echo $com_log_g['community_id']; ?>"> Display</a>
			<?php
														}
			?>
													</div>
												</div>
												
												<script>
													$('div#community_friendsa div.friendCont div.friendBlkR div.friendDetails a.c_hide_<?php echo $c_h_d; ?>').click(function()
													{
														var id = $(this).attr("id");
														var id_h_b = 'c_display_<?php echo $c_h_d; ?>_<?php echo $com_run['community_id']; ?>_<?php echo $com_log_g['artist_id']; ?>_<?php echo $com_log_g['community_id']; ?>';
														//alert(id);
														
														var dataString_art = 'hide_dis='+ id;
														
														$.ajax({
															type: 'POST',
															url : 'FriendAjaxacc_dec.php',
															data: dataString_art,
															success: function(data)  
															{ 
																document.getElementById(id).style.display="none";
																document.getElementById(id_h_b).style.display="inline";
																alert("Your settings are saved.");
																//alert(data);
																//document.getElementById("friend_status_dis_<?php echo $c_h_d; ?>").style.display="block";
																//$("#friend_status_dis_<?php echo $c_h_d; ?>").html(data);
																//alert(data);
															}
														});
													});
													
													$('div#community_friendsa div.friendCont div.friendBlkR div.friendDetails a.c_display_<?php echo $c_h_d; ?>').click(function()
													{
														var id = $(this).attr("id");
														var id_d_b = 'c_hide_<?php echo $c_h_d; ?>_<?php echo $com_run['community_id']; ?>_<?php echo $com_log_g['artist_id']; ?>_<?php echo $com_log_g['community_id']; ?>';
														
														var dataString_art = 'hide_dis='+ id;
														
														$.ajax({
															type: 'POST',
															url : 'FriendAjaxacc_dec.php',
															data: dataString_art,
															success: function(data)  
															{ 
																document.getElementById(id).style.display="none";
																document.getElementById(id_d_b).style.display="inline";
																alert("Your settings are saved.");
																//alert(data);
																//document.getElementById("friend_status_dis_<?php echo $c_h_d; ?>").style.display="block";
																//$("#friend_status_dis_<?php echo $c_h_d; ?>").html(data);
																//alert(data);
															}
														});
													});
													
													$('div#community_friendsa div.friendCont div.friendBlkR div.friendDetails a.c_delete_<?php echo $c_h_d; ?>_<?php echo $com_run['community_id']; ?>_<?php echo $com_log_g['artist_id']; ?>_<?php echo $com_log_g['community_id']; ?>').click(function()
													{
														var id = $(this).attr("id");
														var dataString_art = 'hide_dis='+ id;
														
														var con_x = confirm("Are you sure you want to delete? Along with these your Friend's Artist Profile will also be deleted From your Friend List.");
														
														if(con_x==true)
														{
															$.ajax({
																type: 'POST',
																url : 'FriendAjaxacc_dec.php',
																data: dataString_art,
																success: function(data)  
																{
																	alert("Your Friend <?php echo $com_run['fname'].' '.$com_run['lname']; ?> is not your friend anymore.");
																	window.location.reload(true);

																	//alert("Your settings are saved.");
																	//alert(data);
																	//document.getElementById("friend_status_dis_<?php echo $a_h_d; ?>").style.display="block";
																	//$("#friend_status_dis_<?php echo $a_h_d; ?>").html(data);
																	//alert(data);
																}
															});
														}
														
														
														if(con_x==false)
														{}
													});
												</script>
			<?php
												$c_h_d = $c_h_d + 1;
												}
											}
										}
									}
								}
			?>
								</div>
							</div>

		<!--				 <script>
		!window.jQuery && document.write('<script src="./fancybox/jquery-1.4.3.min.js"><\/script>');
	</script>
	<script type="text/javascript" src="./fancybox/jquery.fancybox-1.3.4.pack.js"></script>
	<link rel="stylesheet" type="text/css" href="./fancybox/jquery.fancybox-1.3.4.css" media="screen" />-->
	
	<script type="text/javascript">             
	$(document).ready(function() {
				$("#firstname").fancybox({
				'width'				: '35%',
				'height'			: '15%',
				'autoScale'			: true,
				'transitionIn'		: 'none',
				'transitionOut'		: 'none',
				'type'				: 'iframe'
			
			});

			
		});
		$(document).ready(function() {
				$("#lastname").fancybox({
				'width'				: '35%',
				'height'			: '15%',
				'autoScale'			: true,
				'transitionIn'		: 'none',
				'transitionOut'		: 'none',
				'type'				: 'iframe'
			});

			
		});
		$(document).ready(function() {
				$("#email").fancybox({
				'width'				: '35%',
				'height'			: '15%',
				'autoScale'			: true,
				'transitionIn'		: 'none',
				'transitionOut'		: 'none',
				'type'				: 'iframe'
			});

			
		});
		$(document).ready(function() {
				$("#phone").fancybox({
				'width'				: '35%',
				'height'			: '15%',
				'autoScale'			: true,
				'transitionIn'		: 'none',
				'transitionOut'		: 'none',
				'type'				: 'iframe'
			});

			
		});
		$(document).ready(function() {
				$("#popcity").fancybox({
				'width'				: '35%',
				'height'			: '15%',
				'autoScale'			: true,
				'transitionIn'		: 'none',
				'transitionOut'		: 'none',
				'type'				: 'iframe'
			});
		});
		$(document).ready(function() {
				$("#profileimage").fancybox({
				'width'				: '35%',
				'height'			: '35%',
				'autoScale'			: true,
				'transitionIn'		: 'none',
				'transitionOut'		: 'none',
				'type'				: 'iframe'
			});
		});
		$(document).ready(function() {
				$("#status").fancybox({
				helpers : {
					media : {},
					buttons : {},
					title : null
				}
			});
		});
		$(document).ready(function() {
				$("#street").fancybox({
				'width'				: '45%',
				'height'			: '25%',
				'autoScale'			: true,
				'transitionIn'		: 'none',
				'transitionOut'		: 'none',
				'type'				: 'iframe'
			});
		});
		$(document).ready(function() {
				$("#postal").fancybox({
				'width'				: '35%',
				'height'			: '15%',
				'autoScale'			: true,
				'transitionIn'		: 'none',
				'transitionOut'		: 'none',
				'type'				: 'iframe'
			});
		});
		$(document).ready(function() {
			$("#remove_account").fancybox({
				helpers : {
					media : {},
					buttons : {},
					title : null
				}
			});
		});
		
		$(document).ready(function() {
			$("#add_member").fancybox({
				helpers : {
					media : {},
					buttons : {},
					title : null
				}
			});
		});
		
		/*****************************/
		/*Code for pay pal pages*/
		/*****************************/
		$(document).ready(function() {
				$("#paypal_through_credit_card").fancybox({
				'autoScale'			: true,
				'transitionIn'		: 'none',
				'transitionOut'		: 'none',
				'type'				: 'iframe'
			});
		});
		/*****************************/
		/*Code for pay pal pages Ends Here*/
		/*****************************/
		
		function clickfirstname()
		{
			var link =document.getElementById("firstname");
			link.click();
		}
		function clickstreet()
		{
			var link =document.getElementById("street");
			link.click();
		}
		function clickpostal()
		{
			var link =document.getElementById("postal");
			link.click();
		}
		function clickstatus()
		{
			var link =document.getElementById("status");
			link.click();
		}
		function clicklastname()
		{
			var link =document.getElementById("lastname");
			link.click();
		}
		function clickemail()
		{
			var link =document.getElementById("email");
			link.click();
		}
		function clickphone()
		{
			var link =document.getElementById("phone");
			link.click();
		}
		function clickcity()
		{
			var link =document.getElementById("popcity");
			link.click();
		}
		function clickprofileimage()
		{
			var link =document.getElementById("profileimage");
			link.click();
		}
		function get_payment_form()
		{
			var isChecked = $("#member_age").is(":checked");
			if($("#member_ship_type").val() == "select")
			{
				alert("You Have To Select MemberShip Type.");
				return false;
			}
			else if($("#countrySelectmember").val() ==0 || $("#countrySelectmember").val()==" ")
			{
				alert("You must select country.");
				return false;
			}
			else if($("#stateSelectmember").val() ==0 || $("#stateSelectmember").val()==" ")
			{
				alert("You must select state.");
				return false;
			}
			else if($("#editcitymember").val() =="" || $("#editcitymember").val()==" ")
			{
				alert("You must enter city.");
				return false;
			}
			
			else if(!isChecked)
			{
				alert("You must confirm that you are 18 years old to purchase a Purify Art membership.");
				return false;
			}
			else
			{
				//if($("#payment_mode").val() == "Credit_card")
				//{
					//$("#paypal_through_credit_card").click();
				//}
				//if($("#payment_mode").val() == "pay_pal")
				//{
					/*var pay = <?php if(isset($_REQUEST['order']) && $_REQUEST['order']!=""){ echo $_REQUEST['order'];} else {echo "0";}?>;
					if(pay != "success")
					{
						location.href="paypal.php?memtype="+$("#member_ship_type").val()+"&paymode=pay_pal";
					}*/
				//}
				location.href="https://<?php echo $domainname; ?>/member_pay.php?chkout=25&country="+$("#countrySelectmember").val()+"&state="+$("#stateSelectmember").val()+"&city="+$("#editcitymember").val()+"&session="+'<?php echo $_SESSION['login_email'];?>';
			}
		}
	</script>
					</div>
                    
      			</div>
                <?php  
					$newres=$check->globe();
					$general_info=new GeneralInfo();
					
					$newsql1=$general_info->allArtists($newres['artist_id']);
					if($newsql1!="" && $newsql1!=NULL)
					{
						$newres1=mysql_fetch_assoc($newsql1);
					}
					
					$newsql2=$general_info->getCountryName($newres['country_id']);
					if($newsql2!="" && $newsql2!=NULL)
					{
						$newres2=mysql_fetch_assoc($newsql2);
					}
					
					$newsql3=$general_info->getStateName($newres['state_id']);
					if($newsql3!="" && $newsql3!=NULL)
					{
						$newres3=mysql_fetch_assoc($newsql3);
					}
					
					$newsql4=$general_info->getStateName($newres['state_id']);
					
					$bill_newsql="SELECT * FROM billing_info WHERE general_user_id='".$newres['general_user_id']."'";
					$bill_ans=mysql_query($bill_newsql);
				?>
                <div class="subTabs" id="general" style="display:none;">
				<script>
				function submit_form()
				{
					//alert("hi");
					$("#general_form").submit();
				}
				</script>
				<h1>General Info</h1>
				<div id="mediaContent">
					<div style="border-bottom:1px dotted #999;" class="topLinks">
						<div style="float:left;" class="links">
							<ul>
								<li><input type="button" style="background-color: #000; border: 0 none; color: #FFFFFF; font-family: 'futura-pt',sans-serif; font-size: 0.825em; font-weight: bold; padding: 6px 10px; float:left; border-radius:5px; -webkit-border-radius:5px; -moz-border-radius:5px; -ms-border-radius:5px; -o-border-radius:5px;" onclick="submit_form()" value=" Save "></li>
							</ul>
						</div>
					</div>
				</div>
					
					<div style="width:665px; float:left;">
						<div id="actualContent">
						<div class="fieldCont">
							  <div class="fieldTitle">Profile Image</div>
							  <?php
							  if($newres['image_name']=="")
							  {
							  ?>
							  <img src="http://generalproimage.s3.amazonaws.com/Noimage.png" width="100"/>
							  <?php
							  }
							  else
							  {
							  ?>
							  <img src="<?php echo $generalproimageURI.$newres['image_name']; ?>"/>
							  <?php
							  }
							  ?>
						
									<?php include('general_jcrop/general_user_pro.php'); ?>
						</div>
						<form id="general_form" name="general_form" onSubmit="return validate()" method="post" action="add_general_info.php"> <!--onsubmit="return validate()">-->
							
							<div class="fieldCont">
							  <div class="fieldTitle">Status</div>
							  <div class="status" id="dis_status_text"><?php if($newres['general_msg_status']== null && $newres['general_msg_status']==""){echo "None Added";}else {echo $newres['general_msg_status'];} ?></div>
							  
							  
							  <input name="editstatus" type="hidden" class="fieldText" id="editstatus" value="<?php echo $newres['general_msg_status']; ?>">
								 <div class="hint"><input type="button" value="<?php if($newres['general_msg_status']== null && $newres['general_msg_status']==""){echo "ADD";}else{echo "Change";} ?>"  onclick="clickstatus()" style="border-radius:5px; -webkit-border-radius:5px; -moz-border-radius:5px; -ms-border-radius:5px; -o-border-radius:5px;"  /><a  id="status" style="display:none;" href="popup.php?whom=status&status=<?php echo $newres['general_msg_status']; ?>" class="fancybox fancybox.ajax">click</a></div>
							<!-- <?php //if($newres['status']==0) {  ?> <div class="status" value="0">Approved</div> <?php //} ?>
							 -->
							  <!--<div class="hint"><input type="button" value="Change" /></div>-->
							</div>
							<div class="fieldCont">
							  <div class="fieldTitle">First Name</div>
							  <input name="editfname" type="text" class="fieldText" id="editfname" value="<?php echo $newres['fname']; ?>">
							  <!--<div class="hint"><input type="button" value="Change"  onclick="clickfirstname()"  /><a  id="firstname" style="display:none;" href="popup.php?whom=fname&firstname=<?php //echo $newres['fname']; ?>">click</a></div>-->
							</div>
							<div class="fieldCont">
							  <div class="fieldTitle">Last Name</div>
							  <input name="editlname" type="text" class="fieldText" id="editlname" value="<?php echo $newres['lname']; ?>">
							  <!--<div class="hint"><input type="button" value="Change" onclick="clicklastname()"  /><a  id="lastname" style="display:none;" href="popup.php?whom=lname&lastname=<?php //echo $newres['lname']; ?>">click</a></div>-->
							</div>
							<div class="fieldCont">
							  <div class="fieldTitle">Birth Date</div>
							  <input name="editbday" type="text" class="fieldText" id="editbday" value="<?php echo $newres['bday_date']; ?>">
							</div>
							<div class="fieldCont">
							  <div class="fieldTitle">Email</div>
							  <input name="editemail" type="text" disabled="disabled" class="fieldText" id="editemail" value="<?php echo $newres['email']; ?>">
							  <div class="hint"><!--<input type="button" value="Change" onclick="clickemail()"  />--><a  id="email" style="display:none;" href="popup.php?whom=email&Emailid=<?php echo $newres['email']; ?>">click</a></div>
							</div>
							<div class="fieldCont">
							  <div class="fieldTitle">Phone</div>
							  <input name="editphone" type="text" class="fieldText" id="editphone" value="<?php echo $newres['phone']; ?>">
							  <!--<div class="hint"><input type="button" value="Change" onclick="clickphone()"  /><a  id="phone" style="display:none;" href="popup.php?whom=phone&phoneno=<?php //echo $newres['phone']; ?>">click</a></div>-->
							</div>
							<div class="fieldCont">
								<div class="fieldTitle">Country</div>
								 <select name="countrySelect" class="dropdown" id="countrySelect">
								 <option value="0">Select Country</option>
									<?php include_once('Country.php'); ?>
									<option value="<?php echo $newres['country_id']; ?>" selected="selected"><?php echo $newres2['country_name']; ?></option>
								</select>
							</div>
							<div class="fieldCont">
								<div class="fieldTitle">State / Province</div>
								 <select name="stateSelect" class="dropdown" id="stateSelect">
									<?php
									while($staterow = mysql_fetch_array($newsql4))
									{
										if($staterow['state_id']==$newres['state_id'])
										{
										?>
											<option value="<?php echo $staterow['state_id']; ?>" selected="selected" ><?php echo $staterow['state_name']; ?></option>
										<?php
										}
									?>
										<option value="<?php echo  $staterow['state_id']; ?>" ><?php echo $staterow['state_name']; ?></option>
									<?php
									}
									?>
									
								</select>
							</div>
							<div class="fieldCont">
								<div class="fieldTitle">City</div>
									<input name="editcity" type="text" class="fieldText" id="editcity" value="<?php if(isset($newres['city'])) echo $newres['city']; ?>" />
								<!--<div class="hint">	<input type="button" value="Change" onclick="clickcity()"/><a  id="popcity" style="display:none;" href="popup.php?whom=city&cityname=<?php //echo $newres['city']; ?>">click</a></div>-->
							</div>
							<div class="fieldCont">
								<div class="fieldTitle">Address</div>
								<textarea class="textbox" name="editaddress" id="editaddress"><?php echo $newres['street_address']; ?></textarea>
								<!--<div class="hint"><input type="button" value="Change"  onclick="clickstreet()"  /><a  id="street" style="display:none;" href="popup.php?whom=street&street=<?php //echo $newres['street_address']; ?>">click</a></div>-->
							</div>
							<div class="fieldCont">
								<div class="fieldTitle">Zip Code</div>
								<input type="text" class="fieldText" name="edit_postal_code" id="edit_postal_code" value="<?php if(isset($newres['postal_code'])) echo $newres['postal_code']; ?>"/>
								<!--<div class="hint"><input type="button" value="Change"  onclick="clickpostal()"  /><a  id="postal" style="display:none;" href="popup.php?whom=postal&postal=<?php //echo $newres['postal_code']; ?>">click</a></div>-->
							</div>
							<div class="fieldCont">
								<div class="fieldTitle">Close Account</div>
								<div class="hint" ><a href="popup.php?whom=removeaccount" id="remove_account" class="fancybox fancybox.ajax"><input type="button" value="Remove My Account" style="border-radius:5px; -webkit-border-radius:5px; -moz-border-radius:5px; -ms-border-radius:5px; -o-border-radius:5px;"/></a></div>
							</div>
						<!--	<div class="fieldCont">
								<b>Sales &amp; Distribution</b>
							</div>
							<div class="fieldCont">
							  <div class="fieldTitle">EIN/SSN</div>
							  <input name="edit_ein_ssn" type="text" class="fieldText" id="edit_ein_ssn" value="<?php //if(isset($newres['EIN_SSN'])) echo $newres['EIN_SSN']; ?>">
							  <div class="hint"><input type="button" value="Change" /></div>
							</div>
							<div class="fieldCont">
							  <div class="fieldTitle">Distribution Agreement</div>
							  <div class="status">Agreement Unsigned</div>
							  <div class="hint"><input type="button" value="Sign" /></div>
							</div>
							<div class="fieldCont">
							  <div class="fieldTitle">Credit Card</div>
							  <input name="credit_card" type="text" class="fieldText" id="credit_card" value="<?php //if(isset($newres['credit_card'])) echo $newres['credit_card']; ?>">
							</div>
							<div class="fieldCont">
							  <div class="fieldTitle">Add Card</div>
							  <input name="add_card" type="text" class="fieldText" id="add_card" value="<?php //if(isset($newres['add_card'])) echo $newres['add_card']; ?>">
							</div>
							 <div class="fieldCont">
                                <b>New Account Details</b>
                            </div>
                            <div class="fieldCont">
                                <div class="fieldTitle">*Card Type</div>
                                 <select name="cardtype_select" class="dropdown" id="cardtype_select">
                                    <!--<option value="select card">Select Card</option> 
									<?php //if($newres['card_type']=="visa")
										//	{ 
									?>
                                    <option value="visa" selected="selected">Visa</option>
                                    <option value="mastercard">Mastercard</option>
                                    <option value="discover">Discover</option>
									<?php
									//}
									//elseif($newres['card_type']=="mastercard")
									//{
									?>
									<option value="visa" >Visa</option>
                                    <option value="mastercard" selected="selected">Mastercard</option>
                                    <option value="discover">Discover</option>
									<?php
									//}
									//elseif($newres['card_type']=="discover")
									//{
									?>
									<option value="visa" >Visa</option>
                                    <option value="mastercard" >Mastercard</option>
                                    <option value="discover" selected="selected">Discover</option>
									<?php
									//}
									//else
									//{
									?>
									<option value="select card">Select Card</option>
									<option value="visa" >Visa</option>
                                    <option value="mastercard" >Mastercard</option>
                                    <option value="discover">Discover</option>
									<?php
									//}
									?>
                                </select>
                            </div>
                            <div class="fieldCont">
                              <div class="fieldTitle">*Name on Account</div>
                              <input name="account_name" type="text" class="fieldText" id="account_name" value="<?php //if(isset($newres['account_name'])) echo $newres['account_name']; ?>">
                            </div>
                            <div class="fieldCont">
                              <div class="fieldTitle">*Card Number</div>
                              <input name="card_number" type="text" class="fieldText" id="card_number" value="<?php //if(isset($newres['card_number'])) echo $newres['card_number']; ?>">
                            </div>
                            <div class="fieldCont">
                              <div class="fieldTitle">*Exp Date</div>
                              <div class="CChint">MM</div>
                              <input name="edit_exp_mm" type="text" class="CCfield" id="edit_exp_mm" value="<?php //if(isset($newres['card_number'])) echo $newres['card_number']; ?>">
                              <div class="CChint">YY</div>
                              <input name="edit_exp_yy" type="text" class="CCfield" id="edit_exp_yy" value="">
                            </div>
                            <div class="fieldCont">
                                <b>Billing Information</b>
                            </div>
							
                            <div class="fieldCont">
                              <div class="fieldTitle">*First Name</div>
                              <input name="bill_fname" type="text" class="fieldText" id="bill_fname" value="<?php //if($bill_ans){ $ans=mysql_fetch_assoc($bill_ans); echo $ans['first_name']; } ?>">
                            </div>
                            <div class="fieldCont">
                              <div class="fieldTitle">Middle Name</div>
                              <input name="bill_mname" type="text" class="fieldText" id="bill_mname" value="<?php  //echo $ans['middle_name'];  ?>">
                            </div>
                            <div class="fieldCont">
                              <div class="fieldTitle">*Last Name</div>
                              <input name="bill_lname" type="text" class="fieldText" id="bill_lname" value="<?php  //echo $ans['last_name']; ?>">
                            </div>
                            <div class="fieldCont">
                              <div class="fieldTitle">Organization</div>
                              <input name="bill_organization" type="text" class="fieldText" id="bill_organization" value="<?php  //echo $ans['organisation']; ?>">
                            </div>
                            <div class="fieldCont">
                              <div class="fieldTitle">*Address 1</div>
                              <textarea class="textbox" id="bill_address1" name="bill_address1"><?php  //echo $ans['address1'];?></textarea>
                            </div>
                            <div class="fieldCont">
                              <div class="fieldTitle">Address 2</div>
                              <textarea class="textbox" name="bill_address2" id="bill_address2"><?php  //echo $ans['address1']; ?></textarea>
                            </div>
                            <div class="fieldCont">
                              <div class="fieldTitle">*City</div>
                              <input name="bill_city" type="text" class="fieldText" id="bill_city" value="<?php  //echo $ans['city']; ?>">
                            </div>
                            <div class="fieldCont">
                              <div class="fieldTitle">*State</div>
                              <input name="bill_state" type="text" class="fieldText" id="bill_state" value="<?php //echo $ans['state']; ?>">
                            </div>
                            <div class="fieldCont">
                              <div class="fieldTitle">*Zip Code</div>
                              <input name="bill_zip_code" type="text" class="fieldText" id="bill_zip_code" value="<?php //echo $ans['zip_code']; ?>">
                            </div>
                            <div class="fieldCont">
                              <div class="fieldTitle">Country</div>
                              <input name="bill_country" type="text" class="fieldText" id="bill_country" value="<?php //echo $ans['country']; ?>">
                            </div>
                            <div class="fieldCont">
                              <div class="fieldTitle">*Phone 1</div>
                              <input name="bill_phone1" type="text" class="fieldText" id="bill_phone1" value="<?php //echo $ans['phone1']; ?>">
                            </div>
                            <div class="fieldCont">
                              <div class="fieldTitle">Phone 2</div>
                              <input name="bill_phone2" type="text" class="fieldText" id="bill_phone2" value="<?php// echo $ans['phone2']; ?>">
                            </div>
                            <div class="fieldCont">
                              <div class="fieldTitle">Fax</div>
                              <input name="bill_fax" type="text" class="fieldText" id="bill_fax" value="<?php //echo $ans['fax']; ?>">
                            </div>
							-->
							<!--<div class="fieldCont">
                                    <div class="fieldTitle"></div>
                                    <input type="submit" class="register" value="Save"/>
                                    
                                </div>-->
						</div>
						<!--<div style="float:right;" id="selected-types"></div>-->
						</form>
					</div>
		
        </div>
                <div class="subTabs" id="mail" style="display:none;">
                	<h1>Mail</h1>
                    <div id="mediaContent">
                    	<div class="topLinks" style="border-bottom:1px dotted #999;">
                            <div class="links" style="float:left;">
                                <ul>
                                    <li><a href="compose.php">Compose</a> |</li>
                                    <!--<li><a href="profileedit.php#mail">Inbox</a> |</li>-->
                                    <li><a href="drafts.php">Drafts</a> |</li>
                                    <li><a href="sent-mails.php">Sent Mails</a></li>
                                </ul>
                            </div>
                        </div>
                         <div class="titleCont">
                            <div class="blkA">From</div>
                            <div class="blkB">Subject</div>
                            <div class="blkM">Date</div>
                           
                        </div>
						<?php
							
							$inbox_sql = $mails_object->inbox($_SESSION['login_email']);
							$inbox_run = mysql_query($inbox_sql);
							if($inbox_run!="" && $inbox_run!=NULL)
							{
								if(mysql_num_rows($inbox_run)>0)
								{
									$inbox_ans = array();
									while($row_inbox = mysql_fetch_assoc($inbox_run))
									{
										$inbox_ans[] = $row_inbox;
									}
									for($in=0;$in<count($inbox_ans);$in++)
									{
										$from_sql = $mails_object->from($inbox_ans[$in]['general_user_id']);
										$from_run = mysql_query($from_sql);
										$from_ans = mysql_fetch_assoc($from_run);
							?>
										<div class="tableCont">
										<?php
											if($inbox_ans[$in]['attachments']=="")
											{
										?>
												<div class="blkA"><a href="mail.php?view_mail=<?php echo $inbox_ans[$in]['id']; ?>&check_attach_remove"><?php echo $from_ans['fname'].' '.$from_ans['lname']; ?></a></div>
												<div class="blkB"><a href="mail.php?view_mail=<?php echo $inbox_ans[$in]['id']; ?>&check_attach_remove"><?php echo $inbox_ans[$in]['subject']; ?></a></div>
												<div class="blkM"><a href="mail.php?view_mail=<?php echo $inbox_ans[$in]['id']; ?>&check_attach_remove"><?php echo $inbox_ans[$in]['date']; ?></a></div>
												<div class="blkD"><a href="javascript:confirmDeleteMail('profileedit.php?mail_delete=<?php echo $inbox_ans[$in]['id']; ?>#mail')">Delete</a></div>
										<?php
											}
											else
											{
										?>
												<div class="blkA"><a href="mail.php?view_mail=<?php echo $inbox_ans[$in]['id']; ?>"><?php echo $from_ans['fname'].' '.$from_ans['lname']; ?></a></div>
												<div class="blkB"><a href="mail.php?view_mail=<?php echo $inbox_ans[$in]['id']; ?>"><?php echo $inbox_ans[$in]['subject']; ?></a></div>
												<div class="blkM"><a href="mail.php?view_mail=<?php echo $inbox_ans[$in]['id']; ?>"><?php echo $inbox_ans[$in]['date']; ?></a></div>
												<div class="blkD"><a href="javascript:confirmDeleteMail('profileedit.php?mail_delete=<?php echo $inbox_ans[$in]['id']; ?>#mail')">Delete</a></div>
										<?php
											}
											?>
										</div>
						<?php
									}
								}
							}
						?>
                    </div>
      			</div>
                <div class="subTabs" id="mymemberships" style="display:none;">
				<?php
					if(isset($_GET['rel_is']) && isset($_GET['rel_type']))
					{						
						$sql_get_acc = mysql_query("SELECT * FROM fan_club_membership WHERE email='".$_SESSION['login_email']."' AND related_type='".$_GET['rel_type']."' AND related_id='".$_GET['rel_is']."' AND accept=0");
						if($sql_get_acc!="")
						{
							if(mysql_num_rows($sql_get_acc)>0)
							{
								$row_acc_get = mysql_fetch_assoc($sql_get_acc);
								$delete_obj_pros->accept_fan_membership($row_acc_get['id'],1);
							}
						}
					}
					
					$purify_member = $member_obj->select_purify_member();
					$show_button_mem = "";
				?>
                	<h1>My Memberships</h1>
						<?php
						$get_artist_fan =$member_obj->findartistfan();
						$get_community_fan =$member_obj->findcommunityfan();
						$get_artistproject_fan =$member_obj->findartistprojectfan();
						$get_communityproject_fan =$member_obj->findcommunityprojectfan();
						$chk_artist_fan =$member_obj->find_artist_fans();
						$chk_community_fan =$member_obj->find_community_fans();
						if($purify_member!="")
						{
							if(mysql_num_rows($purify_member)>0)
							{
								$show_button_mem = "yes";
							}
							else
							{
								$show_button_mem = "yes";
							}
						}
						else
						{
							$show_button_mem = "yes";
						}
						
						if($get_artist_fan!=""){
							if($get_artist_fan == "no"){
							$show_button = "no";
							}else{
							$show_button = "yes";
							}
						}
						if($get_community_fan!=""){
							if($get_community_fan == "no" && $show_button!="yes"){
							$show_button = "no";
							}else{
								$show_button = "yes";
							}
						}
						if(empty($get_artistproject_fan) && $show_button!="yes"){
							$show_button = "no";
						}else{
							$show_button = "yes";
						}
						if(empty($get_communityproject_fan) && $show_button!="yes"){
							$show_button = "no";
						}else{
							$show_button = "yes";
						}
						?>
                    <div id="mediaContent">
                    	<div class="topLinks">
                        	<div class="links" style="float:left;">
                            	<ul>
								<?php 
								if($show_button=="yes" && $show_button_mem=='yes')
								{
								?>
                                	<li><a title="Add a member to one of your fan clubs. Only artists and community users with Purify Art Memberships can sell Fan Club memberships." class="fancybox fancybox.ajax" href="add_member.php" id="add_member" style="background-color: #000; border: 0 none; color: #FFFFFF; font-family: 'futura-pt',sans-serif; font-size: 0.825em; font-weight: bold; padding: 6px 10px;text-decoration:none; border-radius:5px; -webkit-border-radius:5px; -moz-border-radius:5px; -ms-border-radius:5px; -o-border-radius:5px;">Add Member</a></li>
                                    <li>&nbsp;</li>
								<?php
								}
								?>
                                </ul>
                                <select class="eventdrop" id="viewmembership" onChange="handleSelection(value)">
                                	<option value="purimember">Purify Membership</option>
									<?php
										
										if($_SERVER['HTTP_REFERER']==NULL)
										{
									?>
											<option selected value="mymember">My Memberships</option>
										<script>
											var u_r_l = document.URL;
											var u_r_l1 = u_r_l.split('#');
											if(u_r_l1[1]=='mymemberships')
											{
												setTimeout(function(){handleSelection2($('#viewmembership').val())}, 1000);
											}
											else
											{
												document.getElementById("viewmembership").value = 'purimember';
											}
											//$('#viewmembership').change();
										</script>
									<?php
										}
										elseif(isset($_GET['code_chks_member']))
										{
											if($_GET['code_chks_member']!="")
											{
									?>
											<option selected value="mymember">My Memberships</option>
										<script>
											var u_r_l = document.URL;
											var u_r_l1 = u_r_l.split('#');
											if(u_r_l1[1]=='mymemberships')
											{
												setTimeout(function(){handleSelection2($('#viewmembership').val())}, 1000);
											}
											else
											{
												document.getElementById("viewmembership").value = 'purimember';
											}
											//$('#viewmembership').change();
										</script>
									<?php			
											}
										}
										else
										{
									?>
											<option value="mymember">My Memberships</option>
									<?php		
										}
									//$get_artist_fan =$member_obj->findartistfan();
									//$get_community_fan =$member_obj->findcommunityfan();
									//$get_artistproject_fan =$member_obj->findartistprojectfan();
									//$get_communityproject_fan =$member_obj->findcommunityprojectfan();
									
									
									if($get_artist_fan!= null && $get_artist_fan != "no" && $chk_artist_fan!=""){
									?>
										<option value="artmember"><?php echo $get_artist_fan['name'];?> Fan Club</option>
									<?php
									}if($get_community_fan!= null && $get_community_fan != "no" && $chk_community_fan!=""){
									?>
										<option value="commember"><?php echo $get_community_fan['name'];?> Fan Club</option>
									<?php
									}if(!empty($get_artistproject_fan)){
										for($art_pro=0;$art_pro<count($get_artistproject_fan);$art_pro++)
										{
										?>
											<option value="artprojectmember" name="<?php echo $get_artistproject_fan[$art_pro]['id'];?>"><?php echo $get_artistproject_fan[$art_pro]['title'];?> Fan Club</option>
										<?php
										}
									}if(!empty($get_communityproject_fan)){
										for($com_pro=0;$com_pro<count($get_communityproject_fan);$com_pro++)
										{
										?>
											<option value="comprojectmember" name="<?php echo $get_communityproject_fan[$com_pro]['id'];?>"><?php echo $get_communityproject_fan[$com_pro]['title'];?> Fan Club</option>
										<?php
										}
									}
									?>
                                </select>
								<?php
								//$get_total_mem = $member_obj->get_total_fans();
								//echo "<span style='font-weight:bold' >".$get_total_mem['countval']." Members: $".$get_total_mem['total']."</span>";
								$get_total_mem = $member_obj->get_total_artistfans();
								if(!empty($get_total_mem)){
									echo "<span id='total_artist' style='font-weight:bold;display:none;position:relative;padding-left:15px;top:5px;float:right;'>".$get_total_mem['countval']." Members: $".$get_total_mem['total']."</span>";
								}	
								$get_total_mem = $member_obj->get_total_communityfans();
								if(!empty($get_total_mem)){
									echo "<span id='total_community' style='font-weight:bold;display:none;position:relative;padding-left:10px;top:5px;float:right;'>".$get_total_mem['countval']." Members: $".$get_total_mem['total']."</span>";
								}
								?>
								
								
                            </div>
                        </div>
                        <div id="purimember">
                        	<div class="titleCont">
                            	<div class="blkA">Type</div>
                                <div class="blkA">Expires</div>
                                <div class="blkA">Renew</div>
                            </div>
                            <div class="tableCont" style="border-bottom:none;">
							<?php
							if($purify_member!="" && $purify_member!=NULL)
							{
								if(mysql_num_rows($purify_member)>0)
								{
									while($row = mysql_fetch_assoc($purify_member))
									{
										$today_date = date('Y-m-d');
										$new_exp_dt = date("m.d.y", strtotime($row['expiry_date']));
									?>
										<div id="AccordionContainer" class="tableCont" >
											<div class="blkA"><?php echo $row['member_ship_type'];?></div>
											<div class="blkA"><?php if($row['lifetime']==0) { echo $new_exp_dt; } else { echo "Never"; } ?></div>
											<div class="blkA"><a href="#Become_a_member" onclick="return confirm_renew_member('<?php  if($row['lifetime']==0) { if($today_date>$row['expiry_date']){ echo "Renew"; }else { echo "Extend"; } } ?>','Purify');"><?php if($row['lifetime']==0) { if($today_date>$row['expiry_date']){ echo "Renew"; }else { echo "Extend"; } } ?></a></div>
										</div>
									<?php
									}
								}
								else
								{
			?>
									<p style="color: #666666; font-size: 13px; line-height: 24px; margin: 0 0 20px;">You do not currently have a Purify Membership. To support our network and receive premium media services,   <a style="background: none repeat scroll 0 0 #515151; border-radius: 5px 5px 5px 5px; color: #FFFFFF; font-size: 14px; padding: 5px 12px; text-decoration: none;" id="beome_not" href="#Become_a_member">Become a Member.</a></p>
			<?php
								}
							}
							else
							{
			?>
								<p style="color: #666666; font-size: 13px; line-height: 24px; margin: 0 0 20px;">You do not currently have a Purify Membership. To support our network and receive premium media services,   <a style="background: none repeat scroll 0 0 #515151; border-radius: 5px 5px 5px 5px; color: #FFFFFF; font-size: 14px; padding: 5px 12px; text-decoration: none;" id="beome_not" href="#Become_a_member">Become a Member.</a></p>
			<?php
							}
							?>	
                            </div>
                        </div>
                        <div id="mymember" style="display:none;">
                        	<div class="titleCont">
                            	<!--<div class="blkG" style="width:100px;">Type</div>-->
                                <div class="blkA" style="width:195px;">Name</div>
                                <!--<div class="blkG" style="width:100px;">Expires</div>-->
                                <div class="blkG">Cost</div>
								<div class="blkG">Expiration</div>
								<!--<div class="blkG">Renew</div>-->
                            </div>
                            <div class="tableCont" style="border-bottom:none;">
							<?php
							$get_all_my_mem = $member_obj->get_my_all_mebership();
							if($get_all_my_mem!="" && $get_all_my_mem!=NULL)
							{
							if(mysql_num_rows($get_all_my_mem)>0){
								while($row_all_my_mem=mysql_fetch_assoc($get_all_my_mem))
								{
									$today_date = date('Y-m-d');
									$new_exp_dt1 = date("m.d.y", strtotime($row_all_my_mem['expiry_date']));
									$counter = $counter+1;
									$get_fan_price=$member_obj->get_fan_cost($row_all_my_mem['id']);
								?>
									<div id="AccordionContainer" class="tableCont" style="padding-left:0px;width:665px;">
										<div class="blkA"><?php 
										$get_fan_club_name = $member_obj->getfanclub_name($row_all_my_mem['related_id'],$row_all_my_mem['related_type']);
										if($row_all_my_mem['related_type']=="artist" || $row_all_my_mem['related_type']=="community")
										{
											echo $get_fan_club_name['name']." Fan club";
										}else if($row_all_my_mem['related_type']=="artist_project" || $row_all_my_mem['related_type']=="community_project")
										{
											echo $get_fan_club_name['title']." Fan club";
										}
										
										?></div>
										<!--<div class="blkG" style="width:100px;">Lifetime</div>
										
										<div class="blkG" style="width:100px;">Never</div>-->
										<div class="blkG" style="width:80px;"><?php echo "$".$get_fan_price['price'];?></div>
										<div class="blkG" style="width:80px;"><?php echo $new_exp_dt1;?></div>
										
										<?php
										if($row_all_my_mem['accept']==0){
										?>
										<div class="blkG" style="width:80px;">
											<a href="delete_membership.php?accept=<?php echo $row_all_my_mem['id'];?>&status=<?php if($row_all_my_mem['accept']==0){echo "1";}else{echo "0";}?>">Accept</a>/
											<a href="delete_membership.php?deny=<?php echo $row_all_my_mem['id'];?>">Deny</a>
										</div>
										<?php
										}else{
										$get_url = $member_obj->get_profile_url($row_all_my_mem['related_type'],$row_all_my_mem['related_id']);
										?>
										<div class="blkG" style="width:80px;">
										<a href="javascript:void(0)" onclick="return confirm_renew_my_member('<?php /* if($today_date>$row_all_my_mem['expiry_date']){ echo "Renew";}else{ echo "Extend";} */?>','<?php if($row_all_my_mem['related_type']=="artist" || $row_all_my_mem['related_type']=="community"){ echo $get_fan_club_name['name']." Fan club"; }else if($row_all_my_mem['related_type']=="artist_project" || $row_all_my_mem['related_type']=="community_project"){ echo $get_fan_club_name['title']." Fan club"; }?>','fan_club_pro<?php echo $counter;?>');"><?php /* if($today_date>$row_all_my_mem['expiry_date']){ echo "Renew";}else{ echo "Extend";} */?>&nbsp;</a>
										<a href="fan_club.php?data=<?php echo $get_url['profile_url'];?>" id="fan_club_pro<?php echo $counter;?>" style="display:none;">&nbsp;<?php /* if($today_date>$row_all_my_mem['expiry_date']){ echo "Renew";}else{ echo "Extend";} */?></a>
										</div>
											<script type="text/javascript">
												$(document).ready(function() {
														$("#fan_club_pro<?php echo $counter;?>").fancybox({
														'width'				: '75%',
														'height'			: '75%',
														'transitionIn'		: 'none',
														'transitionOut'		: 'none',
														'type'				: 'iframe'
														});
												});
											</script>
										<?php
										}
										?>
										<div class="icon">
										<a href="delete_membership.php?delete=<?php echo $row_all_my_mem['id'];?>" onclick="return confirm_member_delete('<?php// echo $mem_name;?>')"><img src="images/profile/delete.png"></a></div>
									</div>
								<?php							
								}
							}
							}
							
							/*while($row_community=mysql_fetch_assoc($purify_community_member))
							{
								$get_com_name=$member_obj->get_fan_name($row_community['email']);
								$Get_com_cost=$member_obj->get_fan_member_price($row_community['related_id'],"community");
							?>
								<div id="AccordionContainer" class="tableCont" >
									<div class="blkG" style="width:100px;">Community</div>
									<div class="blkA"><?php if(isset($get_com_name) && $get_com_name!="")
															{
																echo $get_com_name['fname'] . $get_com_name['lname'];
																$com_mem_name = $get_com_name['fname'] . $get_com_name['lname'];
															}
															else
															{
																echo $row_community['email']; 
																$com_mem_name = $row_community['email']; 
															}
														?></div>
									<!--<div class="blkG" style="width:100px;">Lifetime</div>-->
									<div class="blkG" style="width:100px;">Never</div>
									<div class="blkG"><?php echo "$".$Get_com_cost['cost'];?></div>
									<div class="icon">
									<a href="delete_membership.php?delete=<?php echo $row_community['id'];?>" onclick="return confirm_member_delete('<?php echo $com_mem_name;?>')"><img src="images/profile/delete.png"></a></div>
								</div>	
							<?php							
							}*/
							?>
                            	
                            </div>
                        </div>
                        <div id="artmember" style="display:none;">
                        	<div class="titleCont">
								<div class="blkE" style="width:200px;">Name</div>
								<div class="blkE" style="width:100px;">Date</div>
                                <div class="blkG" style="width:100px;">Cost</div>
								<div class="blkB" style="width:100px;">Expiration</div>
                                <!--<div class="blkB" style="width:100px;">Renew</div>-->
                                <!--<div class="blkB" style="width:120px;">Total Revenue</div>-->
                                
                            </div>
							<?php
							$purify_artist_member=$member_obj->select_my_member_ship_artist();
							if($purify_artist_member!="" && $purify_artist_member!=NULL)
							{
								while($row_artist=mysql_fetch_assoc($purify_artist_member))
								{
									$today_date = date('Y-m-d');
									$get_art_fan_price=$member_obj->get_fan_cost($row_artist['id']);
									$new_exp_dt2 = date("m.d.y", strtotime($row_artist['expiry_date']));
									$new_pur_dt2 = date("m.d.y", strtotime($row_artist['purchase_date']));
								?>
								<div class="tableCont">
									<div class="blkE" style="width:200px;"><?php 
										$get_fan_club_name = $member_obj->get_fan_name($row_artist['email']);
										if($get_fan_club_name['fname']!="" || $get_fan_club_name['lname']!=""){
											echo $get_fan_club_name['fname']." ".$get_fan_club_name['lname'];
										}else{
										echo "Unregistered";
										}
										?></div>
									<div class="blkE" style="width:100px;"><?php echo $new_pur_dt2;?></div>
									<div class="blkG" style="width:100px;">$<?php echo $get_art_fan_price['price'];?></div>
									<div class="blkE" style="width:100px;"><?php echo $new_exp_dt2;?></div>
									<div class="blkB" style="width:100px;"><a href="delete_membership.php?<?php /* if($today_date>$row_artist['expiry_date']){ echo "renew";}else{ echo "extend";} */?>=<?php echo $row_artist['id'];?>" onclick="return confirm_renew_member('<?php /* if($today_date>$row_artist['expiry_date']){ echo "Renew";}else{ echo "Extend";} */?>','<?php if($get_fan_club_name['fname']!="" || $get_fan_club_name['lname']!=""){ echo $get_fan_club_name['fname']." ".$get_fan_club_name['lname']; }?>');">&nbsp;<?php /* if($today_date>$row_artist['expiry_date']){ echo "Renew";}else{ echo "Extend";} */?></a></div>
									<!--<div class="blkB" style="width:120px;">50</div>
									<div class="blkB" style="width:120px;"></div>-->
								</div>
								<?php
								}
							}
							?>
                        </div>
                        <div id="commember" style="display:none;">
							<div class="titleCont">
								<div class="blkE" style="width:200px;">Name</div>
								<div class="blkE" style="width:100px;">Date</div>
                                <div class="blkG" style="width:100px;">Cost</div>
								<div class="blkB" style="width:100px;">Expiration</div>
                                <!--<div class="blkB" style="width:100px;">Renew</div>-->
                                <!--<div class="blkB" style="width:178px;">Annual Members</div>
                                <div class="blkB" style="width:120px;">Lifetime Members</div>
                                <div class="blkB" style="width:120px;">Total Revenue</div>-->
                                
                            </div>
							<?php
							$purify_community_member=$member_obj->select_my_member_ship_community();
							if($purify_community_member!="" && $purify_community_member!=NULL)
							{
								while($row_community=mysql_fetch_assoc($purify_community_member))
								{
									$today_date = date('Y-m-d');
									$get_com_fan_price=$member_obj->get_fan_cost($row_community['id']);
									$new_exp_dt3 = date("m.d.y", strtotime($row_community['expiry_date']));
									$new_pur_dt3 = date("m.d.y", strtotime($row_community['purchase_date']));
								?>
								<div class="tableCont">
									 <div class="blkE" style="width:200px;"><?php 
										$get_fan_club_name = $member_obj->get_fan_name($row_community['email']);
										if($get_fan_club_name['fname']!="" && $get_fan_club_name['lname']!=""){
										echo $get_fan_club_name['fname']." ".$get_fan_club_name['lname'];
										}else{
										echo "Unregistered";
										}
										?></div>
									<div class="blkE" style="width:100px;"><?php echo $new_pur_dt3;?></div>
									<div class="blkG" style="width:100px;">$<?php echo $get_com_fan_price['price'];?></div>
									<div class="blkE" style="width:100px;"><?php echo $new_exp_dt3;?></div>
									<div class="blkB" style="width:100px;"><a href="delete_membership.php?<?php /* if($today_date>$row_community['expiry_date']){ echo "renew";}else{ echo "extend";} */?>=<?php echo $row_community['id'];?>" onclick="return confirm_renew_member('<?php /* if($today_date>$row_community['expiry_date']){ echo "Renew";}else{ echo "Extend";} */?>','<?php if($get_fan_club_name['fname']!="" && $get_fan_club_name['lname']!=""){ echo $get_fan_club_name['fname']." ".$get_fan_club_name['lname']; }?>')">&nbsp;<?php /* if($today_date>$row_community['expiry_date']){ echo "Renew";}else{ echo "Extend";} */?></a></div>
								</div>
								<?php
								}
							}
							?>
                        </div>
						<div id="comprojectmember" style="display:none;">
                        </div>
						<div id="artprojectmember" style="display:none;">
                        </div>
                    </div>
                </div>
                <div class="subTabs" id="permissions" style="display:none;">
                	<h1>Permissions</h1>
					<!--<script>
				function submit_form_perm()
				{
					//alert("hi");
					$("#permission_form").submit();
				}
				</script>
				<div id="mediaContent">
					<div style="border-bottom:1px dotted #999;" class="topLinks">
						<div style="float:left;" class="links">
							<ul>
								<li><input type="button" style="background-color: #000; border: 0 none; color: #FFFFFF; font-family: 'futura-pt',sans-serif; font-size: 0.825em; font-weight: bold; padding: 6px 10px; float:left;" onclick="submit_form_perm()" value=" Save "></li>
							</ul>
						</div>
					</div>
				</div>-->
					<?php 
					$get_gen_info = $get_permission->get_general_user();
					$edit_permission = $get_permission->get_permission($get_gen_info['general_user_id']);
					if(isset($get_gen_info['artist_id']) && $get_gen_info['artist_id']!="")
					{
						$get_artist_info = $get_permission->get_artist_info($get_gen_info['artist_id']);
					}
					if(isset($get_gen_info['community_id']) && $get_gen_info['community_id']!="")
					{
						$get_community_info = $get_permission->get_community_info($get_gen_info['community_id']);
					}
					?>
                    <div id="actualContent">
					<form action="<?php if(isset($edit_permission) && $edit_permission !=""){echo "addpermission.php?edit=1";}else{echo "addpermission.php";}?>" method="POST" id="permission_form">
                    	<!--<div class="fieldCont">
                        	<div class="fieldTitle">Events</div>
							
                            <select class="dropdown" id="generalevent" name="generalevent">
								<option value="open" <?php /*if($edit_permission !="" && $edit_permission['general_event']=="open"){echo "selected";}?>>Open (Everyone)</option>
                                <option value="public"  <?php if($edit_permission !="" && $edit_permission['general_event']=="public"){echo "selected";}?>   >Public (Only registered users)</option>
                                <option value="members" <?php if($edit_permission !="" && $edit_permission['general_event']=="members"){echo "selected";}?>  >Members (Only members)</option>
                                <option value="private" <?php if($edit_permission !="" && $edit_permission['general_event']=="private"){echo "selected";}?>  >Private (Only admin)</option>
                            </select>
                        </div>
                        <div class="fieldCont">
                        	<div class="fieldTitle">Friends</div>
                            <select class="dropdown" id="generalfriend" name="generalfriend">
                            	<option value="open"    <?php if($edit_permission !="" && $edit_permission['general_friends']=="open"){echo "selected";}?>    >Open (Everyone)</option>
                                <option value="public"  <?php if($edit_permission !="" && $edit_permission['general_friends']=="public"){echo "selected";}?>  >Public (Only registered users)</option>
                                <option value="members" <?php if($edit_permission !="" && $edit_permission['general_friends']=="members"){echo "selected";}?> >Members (Only members)</option>
                                <option value="private" <?php if($edit_permission !="" && $edit_permission['general_friends']=="private"){echo "selected";}?> >Private (Only admin)</option>
                            </select>
                        </div>
                        <div class="fieldCont">
                        	<div class="fieldTitle">General Info</div>
                            <select class="dropdown" id="generalinfo" name="generalinfo">
                            	<option value="open"      <?php if($edit_permission !="" && $edit_permission['general_info']=="open"){echo "selected";}?>     >Open (Everyone)</option>
                                <option value="public"    <?php if($edit_permission !="" && $edit_permission['general_info']=="public"){echo "selected";}?>   >Public (Only registered users)</option>
                                <option value="members"   <?php if($edit_permission !="" && $edit_permission['general_info']=="members"){echo "selected";}?>  >Members (Only members)</option>
                                <option value="private"   <?php if($edit_permission !="" && $edit_permission['general_info']=="private"){echo "selected";}?>  >Private (Only admin)</option>
                            </select>
                        </div>
                        <div class="fieldCont">
                        	<div class="fieldTitle">Location</div>
                            <select class="dropdown" id="generallocation" name="generallocation">
                            	<option value="open"    <?php if($edit_permission !="" && $edit_permission['general_location']=="open"){echo "selected";}?>     >Open (Everyone)</option>
                                <option value="public"  <?php if($edit_permission !="" && $edit_permission['general_location']=="public"){echo "selected";}?>   >Public (Only registered users)</option>
                                <option value="members" <?php if($edit_permission !="" && $edit_permission['general_location']=="members"){echo "selected";}?>  >Members (Only members)</option>
                                <option value="private" <?php if($edit_permission !="" && $edit_permission['general_location']=="private"){echo "selected";}?>  >Private (Only admin)</option>
                            </select>
                        </div>
                        <div class="fieldCont">
                        	<div class="fieldTitle">Points</div>
                            <select class="dropdown" id="generalpoint" name="generalpoint">
                            	<option value="open"    <?php if($edit_permission !="" && $edit_permission['general_points']=="open"){echo "selected";}?>    >Open (Everyone)</option>
                                <option value="public"  <?php if($edit_permission !="" && $edit_permission['general_points']=="public"){echo "selected";}?>  >Public (Only registered users)</option>
                                <option value="members" <?php if($edit_permission !="" && $edit_permission['general_points']=="members"){echo "selected";}?> >Members (Only members)</option>
                                <option value="private" <?php if($edit_permission !="" && $edit_permission['general_points']=="private"){echo "selected";}?> >Private (Only admin)</option>
                            </select>
                        </div>
                        <div class="fieldCont">
                        	<div class="fieldTitle">Services</div>
                            <select class="dropdown" id="generalservice" name="generalservice">
                            	<option value="open"    <?php if($edit_permission !="" && $edit_permission['general_services']=="open"){echo "selected";}?>     >Open (Everyone)</option>
                                <option value="public"  <?php if($edit_permission !="" && $edit_permission['general_services']=="public"){echo "selected";}?>   >Public (Only registered users)</option>
                                <option value="members" <?php if($edit_permission !="" && $edit_permission['general_services']=="members"){echo "selected";}?>  >Members (Only members)</option>
                                <option value="private" <?php if($edit_permission !="" && $edit_permission['general_services']=="private"){echo "selected";}?>  >Private (Only admin)</option>
                            </select>
                        </div>
                        <div class="fieldCont">
                        	<div class="fieldTitle">Statistics</div>
                            <select class="dropdown" id="generalstatistics" name="generalstatistics">
                            	<option value="open"    <?php if($edit_permission !="" && $edit_permission['general_statistics']=="open"){echo "selected";}?>    >Open (Everyone)</option>
                                <option value="public"  <?php if($edit_permission !="" && $edit_permission['general_statistics']=="public"){echo "selected";}?>  >Public (Only registered users)</option>
                                <option value="members" <?php if($edit_permission !="" && $edit_permission['general_statistics']=="members"){echo "selected";}?> >Members (Only members)</option>
                                <option value="private" <?php if($edit_permission !="" && $edit_permission['general_statistics']=="private"){echo "selected";}?> >Private (Only admin)</option>
                            </select>
                        </div>
                        <div class="fieldCont">
                        	<div class="fieldTitle">Subscriptions</div>
                            <select class="dropdown" id="generalsubscription" name="generalsubscription">
                            	<option value="open"    <?php if($edit_permission !="" && $edit_permission['general_subscription']=="open"){echo "selected";}?>    >Open (Everyone)</option>
                                <option value="public"  <?php if($edit_permission !="" && $edit_permission['general_subscription']=="public"){echo "selected";}?>  >Public (Only registered users)</option>
                                <option value="members" <?php if($edit_permission !="" && $edit_permission['general_subscription']=="members"){echo "selected";}?> >Members (Only members)</option>
                                <option value="private" <?php if($edit_permission !="" && $edit_permission['general_subscription']=="private"){echo "selected";}*/?> >Private (Only admin)</option>
                            </select>
                        </div>-->
						<?php
						if(isset($get_gen_info['artist_id']) && $get_gen_info['artist_id']!="" && $get_gen_info['artist_id']!=0 && $get_artist_info['status']==0)
						{
						?>
							<div class="fieldCont"><b><?php echo $get_artist_info['name'];?></b></div>
							<div class="fieldCont">
								<div class="fieldTitle">Biography</div>
								<select class="dropdown" id="artistbiography" name="artistbiography">
									<option value="open"    <?php if($edit_permission !="" && $edit_permission['artist_biography']=="open"){echo "selected";}?>    >Open (Everyone)</option>
									<option value="public"  <?php if($edit_permission !="" && $edit_permission['artist_biography']=="public"){echo "selected";}?>  >Public (Only registered users)</option>
									<option value="members" <?php if($edit_permission !="" && $edit_permission['artist_biography']=="members"){echo "selected";}?> >Fans (Fan club members only)</option>
									<option value="private" <?php if($edit_permission !="" && $edit_permission['artist_biography']=="private"){echo "selected";}?> >Private (Only admin)</option>
								</select>
							</div>
							<div class="fieldCont">
								<div class="fieldTitle">Home Page</div>
								<select class="dropdown" id="artisthomepage" name="artisthomepage">
									<option value="open"     <?php if($edit_permission !="" && $edit_permission['artist_homepage']=="open"){echo "selected";}?>    >Open (Everyone)</option>
									<option value="public"   <?php if($edit_permission !="" && $edit_permission['artist_homepage']=="public"){echo "selected";}?>  >Public (Only registered users)</option>
									<option value="members"  <?php if($edit_permission !="" && $edit_permission['artist_homepage']=="members"){echo "selected";}?> >Fans (Fan club members only)</option>
									<option value="private"  <?php if($edit_permission !="" && $edit_permission['artist_homepage']=="private"){echo "selected";}?> >Private (Only admin)</option>
								</select>
							</div>
							<div class="fieldCont">
								<div class="fieldTitle">Projects</div>
								<select class="dropdown" id="artistproject" name="artistproject">
									<option value="open"    <?php if($edit_permission !="" && $edit_permission['artist_project']=="open"){echo "selected";}?>    >Open (Everyone)</option>
									<option value="public"  <?php if($edit_permission !="" && $edit_permission['artist_project']=="public"){echo "selected";}?>  >Public (Only registered users)</option>
									<option value="members" <?php if($edit_permission !="" && $edit_permission['artist_project']=="members"){echo "selected";}?> >Fans (Fan club members only)</option>
									<option value="private" <?php if($edit_permission !="" && $edit_permission['artist_project']=="private"){echo "selected";}?> >Private (Only admin)</option>
								</select>
							</div>
							<div class="fieldCont">
								<div class="fieldTitle">Events</div>
								<select class="dropdown" id="artistevent" name="artistevent">
									<option value="open"    <?php if($edit_permission !="" && $edit_permission['artist_event']=="open"){echo "selected";}?>    >Open (Everyone)</option>
									<option value="public"  <?php if($edit_permission !="" && $edit_permission['artist_event']=="public"){echo "selected";}?>  >Public (Only registered users)</option>
									<option value="members" <?php if($edit_permission !="" && $edit_permission['artist_event']=="members"){echo "selected";}?> >Fans (Fan club members only)</option>
									<option value="private" <?php if($edit_permission !="" && $edit_permission['artist_event']=="private"){echo "selected";}?> >Private (Only admin)</option>
								</select>
							</div>
						<?php
						}
						if(isset($get_gen_info['community_id']) && $get_gen_info['community_id']!="" && $get_gen_info['community_id']!=0 && $get_community_info['status']==0)
						{
						?>	
							<div class="fieldCont"><b><?php echo $get_community_info['name'];?></b></div>
							<div class="fieldCont">
								<div class="fieldTitle">Biography</div>
								<select class="dropdown" id="communitybiography" name="communitybiography">
									<option value="open"    <?php if($edit_permission !="" && $edit_permission['community_biography']=="open"){echo "selected";}?>    >Open (Everyone)</option>
									<option value="public"  <?php if($edit_permission !="" && $edit_permission['community_biography']=="public"){echo "selected";}?>  >Public (Only registered users)</option>
									<option value="members" <?php if($edit_permission !="" && $edit_permission['community_biography']=="members"){echo "selected";}?> >Fans (Fan club members only)</option>
									<option value="private" <?php if($edit_permission !="" && $edit_permission['community_biography']=="private"){echo "selected";}?> >Private (Only admin)</option>
								</select>
							</div>
							<div class="fieldCont">
								<div class="fieldTitle">Home Page</div>
								<select class="dropdown" id="communityhomepage" name="communityhomepage">
									<option value="open"     <?php if($edit_permission !="" && $edit_permission['community_homepage']=="open"){echo "selected";}?>     >Open (Everyone)</option>
									<option value="public"   <?php if($edit_permission !="" && $edit_permission['community_homepage']=="public"){echo "selected";}?>   >Public (Only registered users)</option>
									<option value="members"  <?php if($edit_permission !="" && $edit_permission['community_homepage']=="members"){echo "selected";}?>  >Fans (Fan club members only)</option>
									<option value="private"  <?php if($edit_permission !="" && $edit_permission['community_homepage']=="private"){echo "selected";}?>  >Private (Only admin)</option>
								</select>
							</div>
							<div class="fieldCont">
								<div class="fieldTitle">Projects</div>
								<select class="dropdown" id="communityproject" name="communityproject">
									<option value="open"    <?php if($edit_permission !="" && $edit_permission['community_project']=="open"){echo "selected";}?>    >Open (Everyone)</option>
									<option value="public"  <?php if($edit_permission !="" && $edit_permission['community_project']=="public"){echo "selected";}?>  >Public (Only registered users)</option>
									<option value="members" <?php if($edit_permission !="" && $edit_permission['community_project']=="members"){echo "selected";}?> >Fans (Fan club members only)</option>
									<option value="private" <?php if($edit_permission !="" && $edit_permission['community_project']=="private"){echo "selected";}?> >Private (Only admin)</option>
								</select>
							</div>
							<div class="fieldCont">
								<div class="fieldTitle">Events</div>
								<select class="dropdown" id="communityevent" name="communityevent">
									<option value="open"    <?php if($edit_permission !="" && $edit_permission['community_event']=="open"){echo "selected";}?>    >Open (Everyone)</option>
									<option value="public"  <?php if($edit_permission !="" && $edit_permission['community_event']=="public"){echo "selected";}?>  >Public (Only registered users)</option>
									<option value="members" <?php if($edit_permission !="" && $edit_permission['community_event']=="members"){echo "selected";}?> >Fans (Fan club members only)</option>
									<option value="private" <?php if($edit_permission !="" && $edit_permission['community_event']=="private"){echo "selected";}?> >Private (Only admin)</option>
								</select>
							</div>
						<?php
						}
						if((isset($get_gen_info['artist_id']) && $get_gen_info['artist_id']!=0 && $get_artist_info['status']==0)||(isset($get_gen_info['community_id']) && $get_gen_info['community_id']!=0 && $get_community_info['status']==0))
						{
						?>
						<div class="fieldCont">
                        	<div class="fieldTitle"></div>
                            <input type ="submit" style="border-radius:5px; -webkit-border-radius:5px; -moz-border-radius:5px; -ms-border-radius:5px; -o-border-radius:5px;" value = "Save" class="register"/>
                        </div>
						<?php
						}
						?>
						</form>
                    </div>
                    <!--<br /><p><strong>Events</strong>: <em>Open(everyone), Public(only registered users), Members(only members), Private (only admin) </em><strong><br />
  Friends</strong>: <em>Open(everyone), Public(only registered users), Members(only members), Private (only admin) </em><strong><br />
  General Info</strong>: <em>Open(everyone), Public(only registered users), Members(only members), Private (only admin) </em><br />
        <strong>Location</strong>: <em>Open(everyone), Public(only registered users), Members(only members), Private (only admin) </em><br />
        <strong>Points</strong><em>: <em>Open(everyone), Public(only registered users), Members(only members), Private (only admin)<br />
      </em></em><strong>Services</strong><em><em>: <em>Open(everyone), Public(only registered users), Members(only members), Private (only admin) </em><br />
      </em></em><strong>Statistics</strong><em><em>: <em>Open(everyone), Public(only registered users), Members(only members), Private (only admin) </em><br />
      </em></em><strong>Subscriptions</strong><em><em>: <em>Open(everyone), Public(only registered users), Members(only members), Private (only admin) </em></em></em></p>
      <h3>Lee Rick </h3>
      <p><strong>Biography</strong><em>: <em>Open(everyone), Public(only registered users), Members(only members), Private (only admin) </em><br />
        </em><strong>Homepage</strong><em>: <em>Open(everyone), Public(only registered users), Members(only members), Private (only admin) </em></em><br />
        <strong>Projects</strong><em>: <em>Open(everyone), Public(only registered users), Members(only members), Private (only admin) </em></em></p>
      <h3>Cypress Groove</h3>
      <p><strong>Biography</strong><em>: <em>Open(everyone), Public(only registered users), Members(only members), Private (only admin) </em><br />
            </em><strong>Homepage</strong><em>: <em>Open(everyone), Public(only registered users), Members(only members), Private (only admin) </em><br />
            </em><strong>Projects</strong><em><em>: <em>Open(everyone), Public(only registered users), Members(only members), Private (only admin) </em></em><br />
                  </em></p>-->
                </div>
                <div class="subTabs" id="points" style="display:none;">
					<h1>Points</h1>
					<br />
					<p><strong>Purify Points</strong></p>
					  <p><em>5,000 Total Current Points</em> (<a href="#">Request Check</a>) </p>
					  <p><em><a href="#">Purchase More Points (.10 per Point)</a></em> </p>
					  <p>&nbsp;</p>
					  <p><strong>Library Time </strong>(disk space) </p>
					  <p><em> Total Library Space (2G General Members, 10G Affiliate Sites)</em></p>
					  <p>Library Space Used - 3.23G</p>
					  <p>Library Space Remaining - .73G</p>
					  <p><em><a href="#">Purchase More Library Time (15 Points each Gb/year)</a></em></p>
					  <p>&nbsp;</p>
					  <p><strong>Play Time </strong>(data transfer) </p>
					  <p><em>Total Yearly Play Time (6G General Members, 30G Affiliate Sites)</em></p>
					  <p>Total Play Time Used This Year - 3G</p>
					  <p>Total Play Time Remaining This Year - 3G </p>
					  <p><em><a href="#">Purchase More Play Time (15 Points each Gb/year)</a></em></p>
				</div>      

                <div class="subTabs" id="registration" style="display:none;float:left;">

	<h1>Registration</h1><br/>
	<div id="actualContent">
    	<div style="float:left;">
    	<div class="regTypeCol" id="artist1">
            <div class="topText">
                <ul>
                    <li>Artist</li>
                    <li>Artists with original, creative,
                    digital content to share and sell.</li>
                </ul>
            </div>
            
            <div class="bulletList">
                <ul>
                    <li>Share Media</li>
                    <li>Add Events and Projects</li>
                    <li>Promote Pages</li>
                    <li>Sell Media (Members Only)</li>
                </ul>
            </div>
       	</div>
            
        <div class="regTypeCol" id="community1">
            
        	<div class="topText">
                <ul>
                    <li>Company</li>
                    <li>Businesses and Organizations with
                    original, creative, digital content to share and sell.</li>
                </ul>
            </div>
            
            <div class="bulletList">
                <ul>
                    <li>Share Media</li>
                    <li>Add Events and Projects</li>
                    <li>Promote Pages</li>
                    <li>Sell Media (Members Only)</li>
                </ul>
            </div>
        </div>
            
        <div class="regTypeCol" id ="team">
            <div class="topText">
                <ul>
                    <li>Team</li>
                    <li>Purify Art is always looking for new team members
                    to help Purify the Art industry.</li>
                </ul>
            </div>
            
            <div class="bulletList">
                <ul>
                    <li>Advisers</li>
                    <li>Programmers</li>
                    <li>Ambassadors</li>
                    <li>Eligible for work contracts</li>
                </ul>
            </div>
        </div>
        </div>
		<script type="text/javascript">
	$("document").ready(function(){
		var value = $("#member_type").val();
		if(value == 42)
		{
			$("#artist1").attr("id","selected");
		}
		if(value == 43)
		{
			$("#community1").attr("id","selected");
		}
		if(value == 44)
		{
			$("#team").attr("id","selected");
		}
	});
</script>
         <!--<div class="regiL">
             <div class="artistTitle">Artist</div>
             <div class="artistDetail">Artists who have original digital content to share may register. As a  Purify Artist you can: 
                 <ul>
                     <li>Store and share your original media </li>
                     <li>Represent yourself and promote online </li>
                     <li>Sell subscriptions to your art and services and receive 100% of your sales </li>
                 </ul>
             </div>
         </div>
         <div class="regiR">
             <div class="artistTitle">Member</div>
             <div class="artistDetail">Any Internet user may browse the listings, as a registered  Fan you  may:
                 <ul>
                     <li>Store and view your favorite media </li>
                     <li>Subscribe to both free and exclusive media</li>
                     <li>Use the Purify Player via the web </li>
                 </ul>
             </div>
         </div>
         <div class="regiL">
             <div class="artistTitle">Community</div>
             <div class="artistDetail">Businesses and Organizations in the arts may also register, as a Community Member you can: 
                 <ul>
                     <li>Host and share your media</li>
                     <li>Represent yourself and promote online </li>
                     <li>Sell subscriptions to your media and services and receive 100% of your sales </li>
                 </ul>
             </div>
         </div>
         <div class="regiR">
             <div class="artistTitle">Team</div>
             <div class="artistDetail">Purify  is always looking for volunteers and potential contractors. Programmers,  Promoters, and Directors may register and help Purify the Entertainment  Industry. Once registered you will be eligible to receive a Purify Work  Contract.</div>
         </div>-->
			</div>	

				<!--<div class="list-wrap">-->
				    <?php 
		 if(isset($_GET['inva']) && $_GET['inva']==1)
		 {
			$_POST['member_type']=42;
		 }
		 if(isset($_GET['inva']) && $_GET['inva']==2)
		 {
			$_POST['member_type']=43;
		 }
		 
		$profileObj = new ProfileType();
		$profileRes = $profileObj->displayData();
		while($profileList[] = mysql_fetch_array($profileRes));
		
		
		//$temp=$_POST['member_type'];
		//echo $temp;
	?>
	<div class="select_membership">
		<form name="membership_type" action="profileedit.php#registration" id="membership_type" method="post">
			<div id="mediaContent">
				<div class="topLinks">
					 <div class="links" style="width:665px;">
						 <ul>
							<li>Select Profile Type&nbsp;</li>
						 </ul>
						<select name="member_type" id="member_type" onchange="changeForm()" class="dropdown">
							<option value="0" >Select One</option>
							<?php foreach($profileList as $profile){ 
									if($profile['type_flag']==1 && $profile['profile_page_name']!=""){
									$sql="select artist_id,member_id,community_id,team_id from general_user where email='".$_SESSION['login_email']."'";
									$res=mysql_query($sql);
									$res1=mysql_fetch_array($res);
									$sel_artist_status=mysql_query("select * from general_artist where artist_id='".$res1['artist_id']."'");
									if($sel_artist_status!="" && $sel_artist_status!=NULL)
									{
										$get_artist_status=mysql_fetch_assoc($sel_artist_status);
									}
									
									$sel_community_status=mysql_query("select * from general_community where community_id='".$res1['community_id']."'");
									if($sel_community_status!="" && $sel_community_status!=NULL)
									{
										$get_community_status=mysql_fetch_assoc($sel_community_status);
									}
									
									$sel_team_status=mysql_query("select * from general_team where team_id='".$res1['team_id']."'");
									if($sel_team_status!="" && $sel_team_status!=NULL)
									{
										$get_team_status=mysql_fetch_assoc($sel_team_status);
									}
							if($res1['artist_id']!=0)
							{
								$dump_artist=42;
								$dump_status_artist=$get_artist_status['status'];
							}
							
							if($res1['community_id']!=0)
							{
								$dump_community=43;
								$dump_status_community=$get_community_status['status'];
							}
							if($res1['member_id']!=0)
							{
								$dump_member=45;
								//$dump_status_community=$get_community_status['status'];
							}
							if($res1['team_id']!=0)
							{
								$dump_team=44;
								$dump_status_team=$get_team_status['status'];
							}
							//if($res1['artist_id']!=0 && $res1['member_id']!=0 && $res1['community_id']!=0 && $res1['team_id']!=0)
						   //{
						   //}
						   if(($dump_artist==$profile['id'] && ($dump_status_artist==1 || $dump_status_artist==0))  || ($dump_community==$profile['id'] && ($dump_status_community==1 || $dump_status_community==0)) || $dump_member==$profile['id'] || ($dump_team==$profile['id'] && ($dump_status_team==1 || $dump_status_team==0)))
						   {
						   }
						   else
						   {
								if($profile['id']=='43')
								{
									$profile['name'] = "Company";
								}
							?>
							<option value="<?php echo $profile['id']; ?>" <?php if(isset($_POST['member_type']) && $_POST['member_type'] == $profile['id']) echo "selected='true'"; ?>><?php echo $profile['name']; ?></option>
							<?php
							}
							}
							}
							?>
						</select>
					</div>
				</div>       
			</div>
		</form>
	</div>
	<br />
	
	<div id="membership_form">
		<?php
		$register_page = "";
		foreach($profileList as $profile){ 
			if(isset($_POST['member_type']))
			{
				if($profile['id']==$_POST['member_type'] && $profile['type_flag']==1 && $profile['profile_page_name']!="")
				{
					$register_page = $profile['profile_page_name'];
					break;
				}
			}
		}					

			if(isset($_POST['member_type']) && $register_page!="")
			{
				include($register_page);
			}
		?>	
		<?php
			if(isset($_GET['f']) && $_GET['f']==1)
			{
				if($_GET['e'])
				{
					$error_flag = $_GET['e'];
					include("register_artist.php");
					
				}
			}
			if(isset($_GET['f']) && $_GET['f']==2)
			{
				if($_GET['e'])
				{
					$error_flag = $_GET['e'];
					include("register_fan.php");
				}
			}
			if(isset($_GET['f']) && $_GET['f']==3)
			{
				if($_GET['e'])
				{
					$error_flag = $_GET['e'];
					include("register_community.php");
				}
			}
			if(isset($_GET['f']) && $_GET['f']==4)
			{
				if($_GET['e'])
				{
					$error_flag = $_GET['e'];
					include("register_team.php");
				}
			}
		?>  
	</div>
    
                 <!-- </div>-->
</div>   
                <div class="subTabs" id="services" style="display:none;">

					<h1>Services</h1><br />
						<p><strong>Published Services </strong></p>
					  <p><a href="#">Studio Time</a>,  Cypress Groove,  Interval, 300 Points/Hr, 50000 Total Points Received (<a href="#">Edit</a>)<br />
					  <a href="#">Performance</a>,  Lee Rick,  Interval, 500 Points/Hr, 2000 Total Points Received (<a href="#">Edit</a>)</p>
					  <p><strong>Purchased Services </strong></p>
					  <p><a href="#">Hadley Ceramics</a>,  Hadley,  One Time, 500 Points, <a href="#">Confirm</a></p>
				</div>
				
                <div class="subTabs" id="statistics" style="display:none; float:none;">
				<a href="add_user_s3.php" id="popup_s3_error"></a>
				<?php				
				$general_stat = $all_stats->get_gen_info();
				$artist_stat = $all_stats->get_art_info();
				$community_stat = $all_stats->get_com_info();
				
				$address = $all_stats->get_contacts();
				$frds = $all_stats->get_friends();
				$members = $all_stats->get_memberships();
				$subscribers = $all_stats->get_subscriptions();
				$art_subscribers = $all_stats->get_art_subscriptions();
				$com_subscribers = $all_stats->get_com_subscriptions();
				$pro_subscribers = $all_stats->get_pro_subscriptions();
				$eve_subscribers = $all_stats->get_eve_subscriptions();
				
				$eve_art = $all_stats->get_artist_eve();
				$eve_com = $all_stats->get_community_eve();
				$all_eve_art = $all_stats->get_allevents('artist');
				$all_eve_com = $all_stats->get_allevents('community');
				
				$pro_art = $all_stats->get_artist_pro();
				$pro_com = $all_stats->get_community_pro();
				$all_pro_art = $all_stats->get_allprojects('artist');
				$all_pro_com = $all_stats->get_allprojects('community');
				 ?>
					<h1>Statistics</h1>
					<div id="mediaContent">
                    	<div class="topLinks" style="border-bottom:1px dotted #999;">
                            <div class="links" style="float:left;">
							<?php
								if(!empty($artist_stat) && !empty($community_stat))
								{
							?>
									<select class="dropdown" onChange="handleSelection(value)">
										<option value="statpersonal">Personal</option>
										<option value="statmedia">Media</option>
										<option value="statartist"><?php echo $artist_stat['name'];?></option>
										<option value="statcommunity"><?php echo $community_stat['name'];?></option>
									</select>
							<?php
								}
								elseif(empty($artist_stat) && !empty($community_stat))
								{
							?>
									<select class="dropdown" onChange="handleSelection(value)">
										<option value="statpersonal">Personal</option>
										<option value="statmedia">Media</option>
										<option value="statcommunity"><?php echo $community_stat['name'];?></option>
									</select>

							<?php
								}
								elseif(!empty($artist_stat) && empty($community_stat))
								{
							?>
									<select class="dropdown" onChange="handleSelection(value)">
										<option value="statpersonal">Personal</option>
										<option value="statmedia">Media</option>
										<option value="statartist"><?php echo $artist_stat['name'];?></option>
									</select>
							<?php
								}
								else
								{
							?>
									<select class="dropdown" onChange="handleSelection(value)">
										<option value="statpersonal">Personal</option>
										<option value="statmedia">Media</option>
									</select>
							<?php
								}
							?>
							</div>
						</div>
					</div>
					<div id="actualContent">
						<div id="statpersonal">
                        	<div class="fieldCont"><b>Personal</b></div>
      						<div class="fieldCont">Emails in Addressbook: <?php if($address!="" || $address!=NULL) { echo $address; } else { echo "0"; } ?></div>
        					<div class="fieldCont">Friends: <?php if($frds!="" || $frds!=NULL) { echo $frds; } else { echo "0"; } ?></div>
        					<div title="The number of fan club memberships you have purchased from other users." class="fieldCont">Memberships Purchased: <?php if($members['count']=="" && $members['price']==""){ echo "0 ($0)"; } else { echo $members['count']." ($".$members['price'].")";}?></div>
      						<div title="The number of profiles you have subscribed to." class="fieldCont">My Subscriptions: <?php if(!empty($subscribers)) { echo $subscribers; } else { echo "0"; } ?> (<?php if(!empty($art_subscribers)) { echo $art_subscribers; } else { echo "0"; } ?> artist, <?php if(!empty($com_subscribers)) { echo $com_subscribers; } else { echo "0"; } ?> community, <?php if(!empty($pro_subscribers)) { echo $pro_subscribers; } else { echo "0"; } ?> projects, <?php if(!empty($eve_subscribers)) { echo $eve_subscribers; }  else { echo "0"; }?> events)</div>
						</div>
                        <div id="statmedia" style="display:none;">
						<?php 
							$get_all_media = $all_stats->get_all_media();
							$get_all_media_purify = $all_stats->get_all_media_stored_onpurify();
							$get_soundcloud = $all_stats->get_soundcloud_media();
							$get_youtube_media = $all_stats->get_youtube_media();
							$get_media_playbyme = $all_stats->get_media_play_me();
							$get_S3_account =$all_stats->getS3_account();
						?>
                        	<div class="fieldCont"><b>Media</b></div>
      						<div class="fieldCont" title="The total number of media files you have in your library.">Total Media Files: <?php echo $get_all_media;?></div>
        					<div class="fieldCont" title="The total number of files you have uploaded to Purify.">Purify Files: <?php echo $get_all_media_purify['count'];?> (<?php echo substr(($get_all_media_purify['size']/100) * 100,0,5);?>% full, <?php echo substr($get_all_media_purify['size'],0,5);?>mb of your 100mb)</div>
        					
        					<div class="fieldCont">Soundcloud Files: <?php echo $get_soundcloud;?></div>
      						<div class="fieldCont">Youtube Files: <?php echo $get_youtube_media;?></div>
        					<!--<div class="fieldCont">Total Plays by me: <?php //echo $get_media_playbyme['play_count'];?></div>
      						<div class="fieldCont">Total Downloads by me: 567</div>-->
                        </div>
                        <div id="statartist" style="display:none;">
						<?php
							$get_art_fan = $all_stats->get_artist_fanclub();
							$get_all_art_fan = $all_stats->get_all_artist_fanclub();
							$get_all_art_event = $all_stats->get_all_artist_event_createdbyme();
							/*Code for displaying all event*/
							$sel_general=$all_stats->get_gen_info();
							$acc_event=explode(',',$sel_general['taggedartistevents']);
							$event_avail = 0;
							for($k=0;$k<count($acc_event);$k++)
							{
								if($acc_event[$k]!="")
								{
									$event_avail = $event_avail + 1;
								}
							}

							$acc_cevent=explode(',',$sel_general['taggedcommunityevents']);
							$event_cavail = 0;
							for($k=0;$k<count($acc_cevent);$k++)
							{
								if($acc_cevent[$k]!="")
								{
									$event_cavail = $event_cavail + 1;
								}
							}
							$acc_project=explode(',',$sel_general['taggedartistprojects']);
							$project_avail = 0;
							for($k=0;$k<count($acc_project);$k++)
							{
								if($acc_project[$k]!="")
								{
									$project_avail = $project_avail + 1;
								}
							}
							$acc_cproject = explode(',',$sel_general['taggedcommunityprojects']);
							$project_cavail = 0;
							for($k=0;$k<count($acc_cproject);$k++)
							{
								if($acc_cproject[$k]!="")
								{
									$project_cavail = $project_cavail + 1;
								}
							}
							$get_levent = $all_stats->getEventUpcoming();
							$get_leventrec = $all_stats->getEventRecorded();
							$get_lceeventrec = $all_stats->getComEventRecorded();

							$getuer_id = array();
							if($get_levent!="" && $get_levent!=NULL)
							{
								while($get_eveup_ids = mysql_fetch_assoc($get_levent))
								{
									//$get_eveup_ids['id'] = $get_eveup_ids['id'].'~art';
									$get_eveup_ids['whos_event'] = "own_art_event";
									$get_eveup_ids['id'] = $get_eveup_ids['id'] ."~art";
									$getuer_id[] = $get_eveup_ids;
								}
							}
							
							$getua_ev = array();
							$getua_ev = $all_stats->only_creator_oupevent();
							$selartsup = array();
							if($event_avail!=0)
							{
								$acc_event = array_unique($acc_event);
								for($i=0;$i<count($acc_event);$i++)
								{
										//$sel_tagged="";
									if($acc_event[$i]!="" && $acc_event[$i]!=0)
									{
										$dumartr = $all_stats->get_event_tagged_by_other_user($acc_event[$i]);
										if(!empty($dumartr) && $dumartr['id']!="" && $dumartr['id']!="")
										{
											$dumartr['id'] = $dumartr['id'].'~'.'art';
											$dumartr['edit'] = 'yes';
											$dumartr['whos_event'] = 'art_tag_eve';
											$selartsup[] = $dumartr;
										}												
									}
								}
							} 

							$selcomsup = array();
							if($event_cavail!=0)
							{
								$acc_cevent = array_unique($acc_cevent);
								for($t_i=0;$t_i<count($acc_cevent);$t_i++)
								{
										//$sel_tagged="";
									if($acc_cevent[$t_i]!="" && $acc_cevent[$t_i]!=0)
									{
										$dumacomr = $all_stats->get_cevent_tagged_by_other_user($acc_cevent[$t_i]);
										if(!empty($dumacomr) && $dumacomr['id']!="" && $dumacomr['id']!="")
										{
											$dumacomr['id'] = $dumacomr['id'].'~'.'com';
											$dumacomr['edit'] = 'yes';
											$dumacomr['edit_com'] = 'yes';
											$dumacomr['whos_event'] = 'com_tag_eve';
											$selcomsup[] = $dumacomr;
										}												
									}
								}
							} 

							$getiu_id = array();
							if($get_leventrec!="" && $get_leventrec!=NULL)
							{
								while($get_channel_ids = mysql_fetch_assoc($get_leventrec))
								{
									//$get_channel_ids['id'] = $get_channel_ids['id'].'~art';
									$get_channel_ids['whos_event'] = "own_art_event";
									$get_channel_ids['id'] = $get_channel_ids['id'] ."~art";
									$getiu_id[] = $get_channel_ids;
								}
							}

							$getic_id = array();
							if($get_lceeventrec!="" && $get_lceeventrec!=NULL)
							{
								while($get_rchannel_ids = mysql_fetch_assoc($get_lceeventrec))
								{
									$get_rchannel_ids['edit_com'] = 'yes';	
									$get_rchannel_ids['whos_event'] = "own_com_event";
									$get_rchannel_ids['id'] = $get_rchannel_ids['id'] ."~com";
									$getic_id[] = $get_rchannel_ids; 
								}
							}

							$getonl_rev = array();
							$getonl_rev = $all_stats->only_creator_orecevent();

							$get_only2cre_rev = array();
							//$get_only2cre_rev = $all_stats->only_creator2_recevent();
							$get_only2cre_rev = $all_stats->only_creator2_orecevent();

							$get_only2cre_ev = array();
							//$get_only2cre_ev = $all_stats->only_creator2ev_upevent();
							$get_only2cre_ev = $all_stats->only_creator2ev_oupevent();

							$selarts2u_tagged = array();
							if($project_avail!=0)
							{
								$acc_project = array_unique($acc_project);
								for($acc_pro=0;$acc_pro<count($acc_project);$acc_pro++)
								{												
									if($acc_project[$acc_pro]!="" && $acc_project[$acc_pro]!=0)
									{													
										$sql_1_cre = $all_stats->get_event_tagged_by_other2_user($acc_project[$acc_pro]);
										if($sql_1_cre!="" && $sql_1_cre!=NULL)
										{
											if(mysql_num_rows($sql_1_cre)>0)
											{
												while($run = mysql_fetch_assoc($sql_1_cre))
												{
													if($newres1['artist_id']!=$run['artist_id'])
													{
														$run['whos_event'] = "art_creator_tag_eve";
														$run['id'] = $run['id'].'~'.'art';
														$selarts2u_tagged[] = $run;
													}
												}
											}
										}
										
										$sql_2_cre = $all_stats->get_event_ctagged_by_other2_user($acc_project[$acc_pro]);
										if($sql_2_cre!="" && $sql_2_cre!=NULL)
										{
											if(mysql_num_rows($sql_2_cre)>0)
											{
												while($run_c = mysql_fetch_assoc($sql_2_cre))
												{
													if($newres1['community_id']!=$run_c['community_id'])
													{
														$run_c['whos_event'] = "com_creator_tag_eve";
														$run_c['id'] = $run_c['id'].'~'.'com';
														$selarts2u_tagged[] = $run_c;
													}
												}
											}
										}
									}
								}
							}

							$selcoms2u_tagged = array();
							if($project_cavail!=0)
							{
								$acc_cproject = array_unique($acc_cproject);
								for($acc_pro=0;$acc_pro<count($acc_cproject);$acc_pro++)
								{												
									if($acc_cproject[$acc_pro]!="" && $acc_cproject[$acc_pro]!=0)
									{													
										$sql_1_cre = $all_stats->get_cevent_tagged_by_other2_user($acc_cproject[$acc_pro]);
										if($sql_1_cre!="" && $sql_1_cre!=NULL)
										{
											if(mysql_num_rows($sql_1_cre)>0)
											{
												while($run = mysql_fetch_assoc($sql_1_cre))
												{
													if($newres1['artist_id']!=$run['artist_id'])
													{
														$run['whos_event'] = "art_creator_tag_eve";
														$run['id'] = $run['id'].'~art';
														$selcoms2u_tagged[] = $run;
													}
												}
											}
										}
										
										$sql_2_cre = $all_stats->get_cevent_ctagged_by_other2_user($acc_cproject[$acc_pro]);
										if($sql_2_cre!="" && $sql_2_cre!=NULL)
										{
											if(mysql_num_rows($sql_2_cre)>0)
											{
												while($run_c = mysql_fetch_assoc($sql_2_cre))
												{
													if($newres1['community_id']!=$run_c['community_id'])
													{
														$run_c['whos_event'] = "com_creator_tag_eve";
														$run_c['id'] = $run_c['id'].'~com';
														$selcoms2u_tagged[] = $run_c;
													}
												}
											}
										}
									}
								}
							}

							$sel_arts = array();
							if($event_avail!=0)
							{
								$acc_event = array_unique($acc_event);
								for($t=0;$t<count($acc_event);$t++)
								{
									//$sel_tagged="";
									if($acc_event[$t]!="" && $acc_event[$t]!=0)
									{
										$dum_arte = $all_stats->get_event_tagged_by_other_userrec($acc_event[$t]);
										if(!empty($dum_arte) && $dum_arte['id']!="" && $dum_arte['id']!="")
										{
											if($newres1['artist_id']!=$dum_arte['artist_id'])
											{
												$dum_arte['edit'] = 'yes';
												$dum_arte['whos_event'] = "art_tag_eve";
												$dum_arte['id'] = $dum_arte['id'] ."~art";
												$sel_arts[] = $dum_arte;
											}
										}
									}
								}
							}

							$sel_coms = array();
							if($event_cavail!=0)
							{
								$acc_cevent = array_unique($acc_cevent);
								for($t=0;$t<count($acc_cevent);$t++)
								{
									//$sel_tagged="";
									if($acc_cevent[$t]!="" && $acc_cevent[$t]!=0)
									{
										$dum_artec = $all_stats->get_cevent_tagged_by_other_userrec($acc_cevent[$t]);
										if(!empty($dum_artec) && $dum_artec['id']!="" && $dum_artec['id']!="")
										{
											if($newres1['community_id']!=$dum_artec['community_id'])
											{
												$dum_artec['edit'] = 'yes';
												$dum_artec['edit_com'] = 'yes';
												$dum_artec['whos_event'] = "com_tag_eve";
												$dum_artec['id'] = $dum_artec['id'] ."~com";
												$sel_coms[] = $dum_artec;
											}
										}
									}
								}
							}

							$selarts2r_tagged = array();
							if($project_avail!=0)
							{
								$acc_project = array_unique($acc_project);
								for($acc_pro=0;$acc_pro<count($acc_project);$acc_pro++)
								{												
									if($acc_project[$acc_pro]!="" && $acc_project[$acc_pro]!=0)
									{													
										$sql_1_cre = $all_stats->get_event_tagged_by_other2_userrec($acc_project[$acc_pro]);
										if($sql_1_cre!="" && $sql_1_cre!=NULL)
										{
											if(mysql_num_rows($sql_1_cre)>0)
											{
												while($run = mysql_fetch_assoc($sql_1_cre))
												{
													if($newres1['artist_id']!=$run['artist_id'])
													{
														$run['id'] = $run['id'].'~art';
														$run['whos_event'] = "art_creator_tag_eve";
														$selarts2r_tagged[] = $run;
													}
												}
											}
										}
										
										$sql_2_cre = $all_stats->get_event_ctagged_by_other2_userrec($acc_project[$acc_pro]);
										if($sql_2_cre!="" && $sql_2_cre!=NULL)
										{
											if(mysql_num_rows($sql_2_cre)>0)
											{
												while($run_c = mysql_fetch_assoc($sql_2_cre))
												{
													if($newres1['community_id']!=$run_c['community_id'])
													{
														$run_c['id'] = $run_c['id'].'~com';
														$run_c['whos_event'] = "com_creator_tag_eve";
														$selarts2r_tagged[] = $run_c;
													}
												}
											}
										}
									}
								}
							}

							$selcoms2r_tagged = array();
							if($project_cavail!=0)
							{
								$acc_cproject = array_unique($acc_cproject);
								for($acc_pro=0;$acc_pro<count($acc_cproject);$acc_pro++)
								{												
									if($acc_cproject[$acc_pro]!="" && $acc_cproject[$acc_pro]!=0)
									{													
										$sql_1_cre = $all_stats->get_cevent_tagged_by_other2_userrec($acc_cproject[$acc_pro]);
										if($sql_1_cre!="" && $sql_1_cre!=NULL)
										{
											if(mysql_num_rows($sql_1_cre)>0)
											{
												while($run = mysql_fetch_assoc($sql_1_cre))
												{
													if($newres1['artist_id']!=$run['artist_id'])
													{
														$run['id'] = $run['id'].'~art';
														$run['whos_event'] = "art_creator_tag_eve";
														$selcoms2r_tagged[] = $run;
													}
												}
											}
										}
										
										$sql_2_cre = $all_stats->get_cevent_ctagged_by_other2_userrec($acc_cproject[$acc_pro]);
										if($sql_2_cre!="" && $sql_2_cre!=NULL)
										{
											if(mysql_num_rows($sql_2_cre)>0)
											{
												while($run_c = mysql_fetch_assoc($sql_2_cre))
												{
													if($newres1['community_id']!=$run_c['community_id'])
													{
														$run_c['id'] = $run_c['id'].'~com';
														$run_c['whos_event'] = "com_creator_tag_eve";
														$selcoms2r_tagged[] = $run_c;
													}
												}
											}
										}
									}
								}
							}

							//$final_eves = array();
							$final_ed = array_merge($getiu_id,$sel_arts,$sel_coms,$getonl_rev,$getic_id,$selcomsup,$selartsup,$getuer_id,$getua_ev,$get_only2cre_rev,$get_only2cre_ev,$selcoms2u_tagged,$selarts2u_tagged,$selarts2r_tagged,$selcoms2r_tagged);
							$get_all_del_id = $all_stats->get_deleted_project_id();
							$exp_del_id = explode(",",$get_all_del_id['deleted_event_id']);
							for($count_all=0;$count_all<count($final_ed);$count_all++){
								for($count_del=0;$count_del<count($exp_del_id);$count_del++){
									if($exp_del_id[$count_del]!=""){
										if($final_ed[$count_all]['id'] == $exp_del_id[$count_del]){
											$final_ed[$count_all] ="";
										}
									}
								}
							}
							$dummy_ape = array();
							$dummy_cpe = array();
							for($evp_f=0;$evp_f<count($final_ed);$evp_f++)
							{
								if($final_ed[$evp_f]!=""){
									$chk_ac_pe = 0;
									if(isset($final_ed[$evp_f]['artist_id'])){
										$get_event_type = $all_stats->Get_Event_subtype($final_ed[$evp_f]['id']);
										if(in_array($final_ed[$evp_f]['id'],$dummy_ape))
										{
											$chk_ac_pe = 1;
										}
										else
										{
											$dummy_ape[] = $final_ed[$evp_f]['id'];
										}
									}
									elseif(isset($final_ed[$evp_f]['community_id']))
									{
										$get_event_type = $all_stats->Get_ACEvent_subtype($final_ed[$evp_f]['id']);
										if(in_array($final_ed[$evp_f]['id'],$dummy_cpe))
										{
											$chk_ac_pe = 1;
										}
										else
										{
											$dummy_cpe[] = $final_ed[$evp_f]['id'];
										}
									}
									if($chk_ac_pe==1){
									}else{
										$Alleventcount = $Alleventcount +1;
									}
								}
							}
							/*Code for displaying all event Ends Here*/
							
							
							/*Code for displaying all Projects*/
							$getproject=$all_stats->GetProject();
							$getproject_community=$all_stats->selcommunity_project();
							$sel_general=$all_stats->get_gen_info();
							$acc_project=explode(',',$sel_general['taggedartistprojects']);
							$project_avail = 0;
							for($k=0;$k<count($acc_project);$k++)
							{
								if($acc_project[$k]!="")
								{
									$project_avail = $project_avail + 1;
								}
							}

							$acc_cproject = explode(',',$sel_general['taggedcommunityprojects']);
							$project_cavail = 0;
							for($k=0;$k<count($acc_cproject);$k++)
							{
								if($acc_cproject[$k]!="")
								{
									$project_cavail = $project_cavail + 1;
								}
							}
								$all_artist_project = array();
								if($getproject!="" && $getproject!=NULL)
								{
									while($showproject=mysql_fetch_assoc($getproject))
									{
										$showproject['whos_project'] = "own_artist";
										$showproject['id'] = $showproject['id'] ."~art";
										$all_artist_project[] =  $showproject;
									}
								}
								$all_friends_project = array();
								$get_all_friends = $all_stats->Get_all_friends();
								if($get_all_friends !="" && $get_all_friends !=NULL)
								{
									while($res_all_friends = mysql_fetch_assoc($get_all_friends))
									{
										$get_friend_art_info = $all_stats->get_friend_art_info($res_all_friends['fgeneral_user_id']);
										$get_all_tag_pro = $all_stats->get_all_tagged_pro($get_friend_art_info);
										if($get_all_tag_pro!="" && $get_all_tag_pro!=NULL)
										{
											if(mysql_num_rows($get_all_tag_pro)>0)
											{
												while($res_projects = mysql_fetch_assoc($get_all_tag_pro))
												{
													//$get_project_type = $all_stats->Get_Project_subtype($res_projects['id']);
													$exp_creator = explode('(',$res_projects['creator']);
													$exp_tagged_email = explode(",",$res_projects['tagged_user_email']);
													for($exp_count = 0;$exp_count<count($exp_tagged_email);$exp_count++)
													{
														if($exp_tagged_email[$exp_count]==""){continue;}
														else{
															if($exp_tagged_email[$exp_count]==$_SESSION['login_email'])
															{
																//$res_projects['project_type_name'] = $get_project_type['name'];
																$res_projects['whos_project'] = "tag_pro_art";
																$res_projects['id'] = $res_projects['id'] ."~art";
																$all_friends_project[] = $res_projects;
															}
														}
														
													}
												}
											}
										}
									}
								}

							/***Projects From Community Starts Here ***/
							$all_community_project = array();
							if($getproject_community!="" && $getproject_community!=NULL)
							{
								if(mysql_num_rows($getproject_community)>0)
								{
									while($showproject=mysql_fetch_assoc($getproject_community))
									{
										$exp_creator= explode('(',$showproject['creator']);
										$showproject['whos_project'] = "own_community";
										$showproject['id'] = $showproject['id'] ."~com";
										$all_community_project[] = $showproject;
									}
								}
							}
							$all_community_friends_project = array();
							$get_all_friends = $all_stats->Get_all_friends();
							if($get_all_friends!="" && $get_all_friends!=NULL)
							{
								while($res_all_friends = mysql_fetch_assoc($get_all_friends))
								{
									$get_friend_art_info = $all_stats->get_friend_community_info($res_all_friends['fgeneral_user_id']);
									$get_all_tag_pro = $all_stats->get_all_tagged_community_pro($get_friend_art_info);
									if($get_all_tag_pro!="" && $get_all_tag_pro!=NULL)
									{
										if(mysql_num_rows($get_all_tag_pro)>0)
										{
											while($res_projects = mysql_fetch_assoc($get_all_tag_pro))
											{
												//$get_project_type = $all_stats->Get_Project_subtype($res_projects['id']);
												$exp_creator = explode('(',$res_projects['creator']);
												$exp_tagged_email = explode(",",$res_projects['tagged_user_email']);
												for($exp_count = 0;$exp_count<count($exp_tagged_email);$exp_count++)
												{
													if($exp_tagged_email[$exp_count]==""){continue;}
													else{
														if($exp_tagged_email[$exp_count]==$_SESSION['login_email'])
														{
															//$res_projects['project_type_name'] = $get_project_type['name'];
															$res_projects['whos_project'] = "tag_pro_com";
															$res_projects['id'] = $res_projects['id'] ."~com";
															$all_community_friends_project[] = $res_projects;
														}
													}
													
												}
											}
										}
									}
								}
							}
							$get_onlycre_id = array();
							$get_onlycre_id = $all_stats->only_creator_this();

							$get_only2cre = array();
							//$get_only2cre = $all_stats->only_creator2pr_this();
							$get_only2cre = $all_stats->only_creator2pr_othis();
							$sel_art2tagged = array();
							if($project_avail!=0)
							{
								$acc_project = array_unique($acc_project);
								for($acc_pro=0;$acc_pro<count($acc_project);$acc_pro++)
								{												
									if($acc_project[$acc_pro]!="" && $acc_project[$acc_pro]!=0)
									{													
										$sql_1_cre = $all_stats->get_project_tagged_by_other2_user($acc_project[$acc_pro]);
										if($sql_1_cre!="" && $sql_1_cre!=NULL)
										{
											if(mysql_num_rows($sql_1_cre)>0)
											{
												while($run = mysql_fetch_assoc($sql_1_cre))
												{
													if($newres1['artist_id']!=$run['artist_id'])
													{
														$run['whos_project'] = "art_creator_tag_pro";
														$run['id'] = $run['id'].'~'.'art';
														$sel_art2tagged[] = $run;
													}
												}
											}
										}
										
										$sql_2_cre = $all_stats->get_project_ctagged_by_other2_user($acc_project[$acc_pro]);
										if($sql_2_cre!="" && $sql_2_cre!=NULL)
										{
											if(mysql_num_rows($sql_2_cre)>0)
											{
												while($run_c = mysql_fetch_assoc($sql_2_cre))
												{
													if($newres1['community_id']!=$run_c['community_id'])
													{
														$run_c['whos_project'] = "com_creator_tag_pro";
														$run_c['id'] = $run_c['id'].'~'.'com';
														$sel_art2tagged[] = $run_c;
													}
												}
											}
										}
									}
								}
							}

							$sel_com2tagged = array();
							if($project_cavail!=0)
							{
								$acc_cproject = array_unique($acc_cproject);
								for($acc_pro=0;$acc_pro<count($acc_cproject);$acc_pro++)
								{
									if($acc_cproject[$acc_pro]!="" && $acc_cproject[$acc_pro]!=0)
									{
										$sql_1_cre = $all_stats->get_cproject_tagged_by_other2_user($acc_cproject[$acc_pro]);
										if($sql_1_cre!="" && $sql_1_cre!=NULL)
										{
											if(mysql_num_rows($sql_1_cre)>0)
											{
												while($run = mysql_fetch_assoc($sql_1_cre))
												{
													if($newres1['artist_id']!=$run['artist_id'])
													{
														$run['whos_project'] = "art_creator_tag_pro";
														$run['id'] = $run['id'].'~art';
														$sel_com2tagged[] = $run;
													}
												}
											}
										}
										
										$sql_2_cre = $all_stats->get_cproject_ctagged_by_other2_user($acc_cproject[$acc_pro]);
										if($sql_2_cre!="" && $sql_2_cre!=NULL)
										{
											if(mysql_num_rows($sql_2_cre)>0)
											{
												while($run_c = mysql_fetch_assoc($sql_2_cre))
												{
													if($newres1['community_id']!=$run_c['community_id'])
													{
														$run_c['whos_project'] = "com_creator_tag_pro";
														$run_c['id'] = $run_c['id'].'~com';
														$sel_com2tagged[] = $run_c;
													}
												}
											}
										}
									}
								}
							}

							$sel_arttagged = array();
							if($project_avail!=0)
							{
								$acc_project = array_unique($acc_project);
								for($acc_pro=0;$acc_pro<count($acc_project);$acc_pro++)
								{
									if($acc_project[$acc_pro]!="" && $acc_project[$acc_pro]!=0)
									{
										$dum_art = $all_stats->get_project_tagged_by_other_user($acc_project[$acc_pro]);
										if(!empty($dum_art) && $dum_art['id']!="" && $dum_art['id']!="")
										{
											if($newres1['artist_id']!=$dum_art['artist_id'])
											{
												$dum_art['id'] = $dum_art['id'].'~art';
												$dum_art['whos_project'] = "tag_pro_art";
												$sel_arttagged[] = $dum_art;
											}
										}
									}
								}
							}

							$sel_comtagged = array();
							if($project_cavail!=0)
							{
								$acc_cproject = array_unique($acc_cproject);
								for($acc_pro=0;$acc_pro<count($acc_cproject);$acc_pro++)
								{
									if($acc_cproject[$acc_pro]!="" && $acc_cproject[$acc_pro]!=0)
									{
										$dum_com = $all_stats->get_cproject_tagged_by_other_user($acc_cproject[$acc_pro]);
										if(!empty($dum_com) && $dum_com['id']!="" && $dum_com['id']!="")
										{
											if($newres1['community_id']!=$dum_com['community_id'])
											{
												$dum_com['id'] = $dum_com['id'].'~com';
												$dum_com['whos_project'] = "tag_pro_com";
												$sel_comtagged[] = $dum_com;
											}
										}
									}
								}
							}

							$all_combine_projects = array_merge($all_artist_project,$all_friends_project,$all_community_project,$all_community_friends_project,$get_onlycre_id,$sel_comtagged,$sel_arttagged,$get_only2cre,$sel_com2tagged,$sel_art2tagged);

							$get_all_del_id = $all_stats->get_deleted_project_id();
							$exp_del_id = explode(",",$get_all_del_id['deleted_project_id']);
							for($count_all=0;$count_all<count($all_combine_projects);$count_all++){
								for($count_del=0;$count_del<count($exp_del_id);$count_del++){
									if($exp_del_id[$count_del]!=""){
										if($all_combine_projects[$count_all]['id'] == $exp_del_id[$count_del]){
											$all_combine_projects[$count_all] ="";
										}
									}
								}
							}
							$dumpsa_pros = array();
							$dumpsc_pros = array();
							for($count_all=0;$count_all<count($all_combine_projects);$count_all++)
							{
								if($all_combine_projects[$count_all] !="")
								{
									$chk_ac_ep = 0;
									if(isset($all_combine_projects[$count_all]['artist_id']))
									{
										$get_project_type = $all_stats->Get_Project_subtype($all_combine_projects[$count_all]['id']);
										if(in_array($all_combine_projects[$count_all]['id'],$dumpsa_pros))
										{
											$chk_ac_ep = 1;
										}
										else
										{
											$dumpsa_pros[] = $all_combine_projects[$count_all]['id'];
										}
									}
									elseif(isset($all_combine_projects[$count_all]['community_id']))
									{
										$get_project_type = $all_stats->Get_ACProject_subtype($all_combine_projects[$count_all]['id']);
										if(in_array($all_combine_projects[$count_all]['id'],$dumpsc_pros))
										{
											$chk_ac_ep = 1;
										}
										else
										{
											$dumpsc_pros[] = $all_combine_projects[$count_all]['id'];
										}
									}
									if($chk_ac_ep==1)
									{}
									else
									{
										$Allprojectcount = $Allprojectcount +1;
									}
								}
							}
							/*Code for displaying all Projects Ends here*/
							
							
							$get_all_art_project = $all_stats->get_all_artist_project_createdbyme();
							$get_allmedia_detail = $all_stats->get_all_media_played();
						?>
                        	<div class="fieldCont"><b><?php echo $artist_stat['name']; ?></b></div>
      						<div class="fieldCont" title="The total number of current fan club members you have to your artist fan club and your project fan clubs created under your artist profile.">Current Fan Club Memberships: <?php echo $get_art_fan['count'] ;?> ($<?php echo $get_art_fan['price'];?>/year)</div>
        					<div class="fieldCont" title="The total number of fan club members you have sold to your artist fan club and your project fan clubs created under your artist profile.">Total Fan Club Memberships: <?php echo $get_all_art_fan['count'];?> ($<?php echo $get_all_art_fan['price'];?> to Date)</div>
        					<div class="fieldCont" title="The total number of events you have either created or been tagged in.">Events: <?php echo $Alleventcount; ?>(<?php echo $get_all_art_event;?> created by me)</div>
        					<div class="fieldCont" title="The total number of projects you have either created or been tagged in.">Projects: <?php echo $Allprojectcount;?> (<?php echo $get_all_art_project; ?> created by me)</div>

                        </div>
                        <div id="statcommunity" style="display:none;">
						<?php
							$get_com_fan = $all_stats->get_community_fanclub();
							$get_all_com_fan = $all_stats->get_all_community_fanclub();
							$get_all_com_event = $all_stats->get_all_community_event_createdbyme();
							$get_all_com_project = $all_stats->get_all_community_project_createdbyme();
							$get_allmedia_detail_com = $all_stats->get_all_media_played_community();
						?>
                        	<div class="fieldCont"><b><?php echo $community_stat['name']; ?></b></div>
      						<div class="fieldCont" title="The total number of current fan club members you have to your artist fan club and your project fan clubs created under your artist profile.">Current Fan Club Memberships: <?php echo $get_com_fan['count'] ;?> ($<?php echo $get_com_fan['price'] ;?>/year)</div>
        					<div class="fieldCont" title="The total number of fan club members you have sold to your artist fan club and your project fan clubs created under your artist profile.">Total Fan Club Memberships: <?php echo $get_all_com_fan['count'] ;?> ($<?php echo $get_all_com_fan['price'] ;?> to Date)</div>
        					<div class="fieldCont" title="The total number of events you have either created or been tagged in.">Events: <?php echo $Alleventcount; ?>(<?php echo $get_all_com_event;?> created by me)</div>
        					<div class="fieldCont" title="The total number of projects you have either created or been tagged in.">Projects: <?php echo $Allprojectcount; ?>(<?php echo $get_all_com_project;?> created by me)</div>
        					<!--<div class="fieldCont" title="The total number of plays from files you have either created or been tagged in.">Plays: <?php /*echo $get_allmedia_detail_com[1];?> (<?php echo $get_allmedia_detail_com[0];*/?> Files created by Cypress Groove)</div>
      						<div class="fieldCont" title="The total number of downloads from files you have either created or been tagged in.">Downloads: 345 (234 Files created by Cypress Groove)</div>-->
      						<!--<div class="fieldCont">Files for Digital Distrubtion: 234 (120 Files created by Cypress Groove)</div>-->
                        </div>
					</div>
				</div>
				
				<div class="subTabs" id="subscrriptions" style="display:none;"><!---->
                	<h1>Subscriptions</h1>
                    <div id="mediaContent">
                    	<div class="topLinks" style="border-bottom:1px dotted #999;">
                            <div class="links" style="float:left;">
								<select class="dropdown" onChange="handleSelection(value)">
									<option value="all_putify">All</option>
                                    <option value="subPutify">Purify</option>
									<option value="subArtist">Artist</option>
									<option value="subCommunity">Company</option>
                                    <option value="sub_Projects">Projects</option>
									<option value="sub_Events">Events</option>
								</select>
							</div>
						</div>
					<div id="all_putify">
						<div id="mediaContent">
                            <div class="titleCont">
                                <!--<div class="blkD">Profile</div>-->
                                <div class="blkA" style="width:281px;">Name</div>
                                <div class="blkB">Location</div>
                                <div class="blkE">Type</div>
                                <div class="blkF" style="float:right; width:49px;">Delete</div>
                            </div>
							<?php
								$sub_alls = array();
								$get_all_art_subscriber = $sub_listings->get_all_org_sub();
								$get_gen_info = $sub_listings->get_general_info();
								if($get_all_art_subscriber!="" && $get_all_art_subscriber!=NULL)
								{
									if(mysql_num_rows($get_all_art_subscriber)>0)
									{
										while($get_art = mysql_fetch_assoc($get_all_art_subscriber))
										{
											$sub_alls[] = $get_art;
										}
									}
								}
								
								for($s_u=0;$s_u<count($sub_alls);$s_u++)
								{
									if($sub_alls[$s_u]['related_type'] == 'artist')
									{
										$types = 'artist';
									}
									elseif($sub_alls[$s_u]['related_type'] == 'community')
									{
										$types = 'community';
									}
									elseif($sub_alls[$s_u]['related_type'] == 'community_project' || $sub_alls[$s_u]['related_type'] == 'artist_project' || $sub_alls[$s_u]['related_type'] == 'community_event' || $sub_alls[$s_u]['related_type'] == 'artist_event')
									{
										$types = $sub_alls[$s_u]['related_type'];
									}
									
									if($sub_alls[$s_u]['related_type'] == 'community_project' || $sub_alls[$s_u]['related_type'] == 'artist_project' || $sub_alls[$s_u]['related_type'] == 'community_event' || $sub_alls[$s_u]['related_type'] == 'artist_event')
									{
										$related = explode('_',$sub_alls[$s_u]['related_id']);
										$get_subscriber_info = $sub_listings->get_subscriber_info_org($related[1],$types);
											
										$sub_alls[$s_u]['name'] = $get_subscriber_info['title'];
									}
									elseif($sub_alls[$s_u]['related_type'] == 'artist' || $sub_alls[$s_u]['related_type'] == 'community')
									{
										$get_subscriber_info = $sub_listings->get_subscriber_info_org($sub_alls[$s_u]['related_id'],$types);
										
										$sub_alls[$s_u]['name'] = $get_subscriber_info['name'];
									}
								}
								
								$sort = array();
								foreach($sub_alls as $k=>$v)
								{
									//if(isset($v['name']) && $v['name']!="")
									//{
										$sort['name'][$k] = strtolower($v['name']);
									//}
									
								}
								if(!empty($sort))
								{
									array_multisort($sort['name'], SORT_ASC,$sub_alls);
								}								
								
								for($s_u=0;$s_u<count($sub_alls);$s_u++)
								{
									if($sub_alls[$s_u]['related_type'] == 'artist')
									{
										$types = 'artist';
										
										$get_subscriber_info = $sub_listings->get_subscriber_info_org($sub_alls[$s_u]['related_id'],$types);
										
										$get_gen_info_del = $sub_listings->artist_communitySubscribedTo($types,$sub_alls[$s_u]['related_id']);
									}
									elseif($sub_alls[$s_u]['related_type'] == 'community')
									{
										$types = 'community';
										
										$get_subscriber_info = $sub_listings->get_subscriber_info_org($sub_alls[$s_u]['related_id'],$types);
										
										$get_gen_info_del = $sub_listings->artist_communitySubscribedTo($types,$sub_alls[$s_u]['related_id']);
									}
									elseif($sub_alls[$s_u]['related_type'] == 'community_project' || $sub_alls[$s_u]['related_type'] == 'artist_project' || $sub_alls[$s_u]['related_type'] == 'community_event' || $sub_alls[$s_u]['related_type'] == 'artist_event')
									{										
										$types = $sub_alls[$s_u]['related_type'];
										$types_1 = explode('_',$sub_alls[$s_u]['related_type']);
										
										$related = explode('_',$sub_alls[$s_u]['related_id']);
										$get_subscriber_info = $sub_listings->get_subscriber_info_org($related[1],$types);
										
										if($sub_alls[$s_u]['related_type'] == 'community_project' || $sub_alls[$s_u]['related_type'] == 'community_event')
										{											
											$get_gen_info_del = $sub_listings->artist_communitySubscribedTo($types_1[0],$get_subscriber_info['community_id']);
										}
										elseif($sub_alls[$s_u]['related_type'] == 'artist_project' || $sub_alls[$s_u]['related_type'] == 'artist_event')
										{											
											$get_gen_info_del = $sub_listings->artist_communitySubscribedTo($types_1[0],$get_subscriber_info['artist_id']);
										}
									}

									if(!empty($get_subscriber_info) && $get_subscriber_info!="")
									{
									
							?>
									<div class="tableCont">
										<!--<div class="blkD"><img src="general_jcrop/croppedFiles/thumb/<?php //if(isset($run_sub_user_to['image_name']) && $run_sub_user_to['image_name']!="") {echo $run_sub_user_to['image_name']; } else{ echo "Noimage.png"; }?>" /></div>-->
										<div class="blkA" style="width:280px;"><?php if(isset($get_subscriber_info['name']) && !empty($get_subscriber_info['name'])) { echo $get_subscriber_info['name']; }elseif(isset($get_subscriber_info['title']) && !empty($get_subscriber_info['title'])) { echo $get_subscriber_info['title']; } else { echo "&nbsp"; } ?></div>
											<div class="blkB"><?php if(!empty($get_subscriber_info['city']) && preg_match('/^\s+$/',$get_subscriber_info['city'])!=1) { echo $get_subscriber_info['city']; } else { echo "&nbsp"; } ?></div>
											<div class="blkE" style="width:138px;"><?php
											if($sub_alls[$s_u]['related_type']=='artist')
											{
												$subs = "";
												$subs =  $sub_listings->get_artist_user_subtype($get_subscriber_info['artist_id']);
											}
											elseif($sub_alls[$s_u]['related_type']=='community')
											{
												$subs = "";
												$subs =  $sub_listings->get_community_user_subtype($get_subscriber_info['community_id']);
											}
											elseif($sub_alls[$s_u]['related_type']=='artist_project')
											{
												$subs = "";
												$subs =  $sub_listings->get_artistp_user_subtype($get_subscriber_info['id']);
											}
											elseif($sub_alls[$s_u]['related_type']=='artist_event')
											{
												$subs = "";
												$subs =  $sub_listings->get_artiste_user_subtype($get_subscriber_info['id']);
											}
											elseif($sub_alls[$s_u]['related_type']=='community_project')
											{
												$subs = "";
												$subs =  $sub_listings->get_communityp_user_subtype($get_subscriber_info['id']);
											}
											elseif($sub_alls[$s_u]['related_type']=='community_event')
											{
												$subs = "";
												$subs =  $sub_listings->get_communitye_user_subtype($get_subscriber_info['id']);
											}
											
										if($subs!="" && !empty($subs))
										{
											echo $subs['name'];
										}
										else
										{
											echo "&nbsp";
										}
										?></div>
										<!--<div class="blkF" onclick="runAccordion(<?php //echo $ans_sub_user[$sub_i]['id']; ?>);">
											<div onselectstart="return false;"><img src="images/profile/view.jpg" /></div>
										</div>-->
										<div class="icon" style="width:37px;" onclick="runAccordion_new('<?php echo $s_u; ?>_pay');">
											<div onselectstart="return false;"><img id="trcn_info_<?php echo $s_u; ?>" onclick="lists_views();" src="images/profile/view.jpg" /></div>
										</div>

										<div class="blkF" style="width:39px;"><a href="delete_subscriber.php?emsil=<?php echo $sub_alls[$s_u]['id'];?>&id=<?php echo $get_gen_info_del['general_user_id'];?>" onclick="return confirm_subscriber_delete('<?php if(isset($get_subscriber_info['name']) && !empty($get_subscriber_info['name'])) { echo $get_subscriber_info['name']; }elseif(isset($get_subscriber_info['title']) && !empty($get_subscriber_info['title'])) { echo $get_subscriber_info['title']; } ?>')"><img src="images/profile/delete.png" style="margin-top:5px;" /></a></div>
										
										<div class="expandable" id="Accordion_new<?php echo $s_u; ?>_payContent" style="margin-left: 80px; top: 17px;">
											<div id="actualContent" style="width:400px;">
												<div class="fieldCont">
													<div class="fieldTitle">News Feeds :-</div>
<?php
													
													if($sub_alls[$s_u]['news_feed']=='1')
													{
?>
														<div class="chkCont">
															<input checked type="radio" class="chk" name="feeds_news_<?php echo $sub_alls[$s_u]['id']; ?>"  onClick="on_feeds('<?php echo $sub_alls[$s_u]['id']; ?>')" value="on"/>
															<div class="radioTitle" >On</div>
														</div>
														<div class="chkCont">
															<input type="radio" class="chk" name="feeds_news_<?php echo $sub_alls[$s_u]['id']; ?>" onClick="off_feeds('<?php echo $sub_alls[$s_u]['id']; ?>')" value="off"/>
															<div class="radioTitle">Off</div>
														</div>
<?php
													}
													else
													{
?>
														<div class="chkCont">
															<input type="radio" class="chk" name="feeds_news_<?php echo $sub_alls[$s_u]['id']; ?>"  onClick="on_feeds('<?php echo $sub_alls[$s_u]['id']; ?>')" value="on"/>
															<div class="radioTitle" >On</div>
														</div>
														<div class="chkCont">
															<input checked type="radio" class="chk"  name="feeds_news_<?php echo $sub_alls[$s_u]['id']; ?>" onClick="off_feeds('<?php echo $sub_alls[$s_u]['id']; ?>')" value="off"/>
															<div class="radioTitle">Off</div>
														</div>
<?php
													}
?>
												</div>
												<div class="fieldCont">
													<div class="fieldTitle">Email :-</div>
<?php
													$type = '';
													if($sub_alls[$s_u]['related_type']=='artist' || $sub_alls[$s_u]['related_type']=='artist_project' || $sub_alls[$s_u]['related_type']=='artist_event')
													{
														$type = 'artist';
													}
													elseif($sub_alls[$s_u]['related_type']=='community' || $sub_alls[$s_u]['related_type']=='community_project' || $sub_alls[$s_u]['related_type']=='community_event')
													{
														$type = 'community';
													}
													
													if($sub_alls[$s_u]['email_update']=='1')
													{
?>
														<div class="chkCont">
															<input checked type="radio" class="chk" name="emails_<?php echo $sub_alls[$s_u]['id']; ?>"  onClick="on_emails('<?php echo $sub_alls[$s_u]['id']; ?>','<?php echo $sub_alls[$s_u]['email']; ?>','<?php echo $sub_alls[$s_u]['subscribe_to_id']; ?>','<?php echo $type; ?>')" value="on"/>
															<div class="radioTitle" >On</div>
														</div>
														<div class="chkCont">
															<input type="radio" class="chk" name="emails_<?php echo $sub_alls[$s_u]['id']; ?>" onClick="off_emails('<?php echo $sub_alls[$s_u]['id']; ?>','<?php echo $sub_alls[$s_u]['email']; ?>','<?php echo $sub_alls[$s_u]['subscribe_to_id']; ?>','<?php echo $type; ?>')" value="off"/>
															<div class="radioTitle">Off</div>
														</div>
<?php
													}
													else
													{
?>
														<div class="chkCont">
															<input type="radio" class="chk" name="emails_<?php echo $sub_alls[$s_u]['id']; ?>"  onClick="on_emails('<?php echo $sub_alls[$s_u]['id']; ?>','<?php echo $sub_alls[$s_u]['email']; ?>','<?php echo $sub_alls[$s_u]['subscribe_to_id']; ?>','<?php echo $type; ?>')" value="on"/>
															<div class="radioTitle" >On</div>
														</div>
														<div class="chkCont">
															<input checked type="radio" class="chk"  name="emails_<?php echo $sub_alls[$s_u]['id']; ?>" onClick="off_emails('<?php echo $sub_alls[$s_u]['id']; ?>','<?php echo $sub_alls[$s_u]['email']; ?>','<?php echo $sub_alls[$s_u]['subscribe_to_id']; ?>','<?php echo $type; ?>')" value="off"/>
															<div class="radioTitle">Off</div>
														</div>
<?php
													}
?>
												</div>
											</div>
										</div>
									</div>
							<?php
										//}
									}
								}
									//}
								//}
								/* $get_all_art_subscriber = $sub_listings->get_all_org_sub();
								$get_gen_info = $sub_listings->get_general_info();
								if(mysql_num_rows($get_all_art_subscriber)>0)
								{
									while($get_art = mysql_fetch_assoc($get_all_art_subscriber))
									{
										if($get_art['related_type'] == 'community')
										{
											$types = 'community';
											$get_subscriber_info = $sub_listings->get_subscriber_info_org($get_art['related_id'],$types);
											if(!empty($get_subscriber_info) && $get_subscriber_info!="")
											{
											$get_gen_info_del = $sub_listings->artist_communitySubscribedTo($types,$get_art['related_id']);

										
							?>
										<div class="tableCont">
											<!--<div class="blkD"><img src="general_jcrop/croppedFiles/thumb/<?php //if(isset($run_sub_user_to['image_name']) && $run_sub_user_to['image_name']!="") {echo $run_sub_user_to['image_name']; } else{ echo "Noimage.png"; }?>" /></div>-->
											<div class="blkA" style="width:280px;"><?php if(!empty($get_subscriber_info['name'])) { echo $get_subscriber_info['name']; } else { echo "&nbsp"; } ?></div>
											<div class="blkB"><?php if(!empty($get_subscriber_info['city'])) { echo $get_subscriber_info['city']; } else { echo "&nbsp"; } ?></div>
											<div class="blkE" style="width:80px;"><?php $subs_c = ""; $subs_c =  $sub_listings->get_community_user_subtype($get_subscriber_info['community_id']);
											if($subs_c!="" && !empty($subs_c))
											{
												echo $subs_c['name'];
											}
											else
											{
												echo "&nbsp";
											}
											?></div>
											<!--<div class="blkF" onclick="runAccordion(<?php //echo $ans_sub_user[$sub_i]['id']; ?>);">
												<div onselectstart="return false;"><img src="images/profile/view.jpg" /></div>
											</div>-->
											<div class="blkF" style="float:right; width:39px;"><a href="delete_subscriber.php?emsil=<?php echo $get_art['id'];?>&id=<?php echo $get_gen_info_del['general_user_id'];?>" onclick="return confirm_subscriber_delete('<?php echo $get_subscriber_info['name']; ?>')"><img src="images/profile/delete.png" style="margin-top:5px;" /></a></div>
										</div>
							<?php
											}
										}
									}
								}
								$get_all_art_subscriber = $sub_listings->get_all_org_sub();
								$get_gen_info = $sub_listings->get_general_info();
								if(mysql_num_rows($get_all_art_subscriber)>0)
								{
									while($get_art = mysql_fetch_assoc($get_all_art_subscriber))
									{
										if($get_art['related_type'] == 'community_project' || $get_art['related_type'] == 'artist_project')
										{
											$types = $get_art['related_type'];
											$related = explode('_',$get_art['related_id']);
											$get_subscriber_info = $sub_listings->get_subscriber_info_org($related[1],$types);
											
											if($get_art['related_type'] == 'community_project')
											{
												$get_gen_info_del = $sub_listings->artist_communitySubscribedTo($types,$get_subscriber_info['community_id']);
											}
											elseif($get_art['related_type'] == 'artist_project')
											{
												$get_gen_info_del = $sub_listings->artist_communitySubscribedTo($types,$get_subscriber_info['artist_id']);
											}
										
							?>
										<div class="tableCont">
											<!--<div class="blkD"><img src="general_jcrop/croppedFiles/thumb/<?php //if(isset($run_sub_user_to['image_name']) && $run_sub_user_to['image_name']!="") {echo $run_sub_user_to['image_name']; } else{ echo "Noimage.png"; }?>" /></div>-->
											<div class="blkA" style="width:280px;"><?php if(!empty($get_subscriber_info['title'])) { echo $get_subscriber_info['title']; } else { echo "&nbsp"; } ?></div>
											<div class="blkB"><?php if(!empty($get_subscriber_info['city'])) { echo $get_subscriber_info['city']; } else { echo "&nbsp"; } ?></div>
											<div class="blkE" style="width:80px;"><?php
											$subs_p = "";
											if(isset($get_subscriber_info['artist_id']))
											{
												$subs_p =  $sub_listings->get_artistp_user_subtype($get_subscriber_info['id']);
											}
											elseif(isset($get_subscriber_info['community_id']))
											{
												$subs_p =  $sub_listings->get_communityp_user_subtype($get_subscriber_info['id']);
											}
											if($subs_p!="" && !empty($subs_p))
											{
												echo $subs_p['name'];
											}
											else
											{
												echo "&nbsp";
											}
											?></div>
											<!--<div class="blkF" onclick="runAccordion(<?php //echo $ans_sub_user[$sub_i]['id']; ?>);">
												<div onselectstart="return false;"><img src="images/profile/view.jpg" /></div>
											</div>-->
											<div class="blkF" style="float:right; width:39px;"><a href="delete_subscriber.php?id=<?php echo $get_art['id'];?>&email=<?php echo $get_gen_info_del['email'];?>" onclick="return confirm_subscriber_delete('<?php echo $get_subscriber_info['title']; ?>')"><img src="images/profile/delete.png" style="margin-top:5px;" /></a></div>
										</div>
							<?php
										}
									}
								}
								$get_all_art_subscriber = $sub_listings->get_all_org_sub();
								$get_gen_info = $sub_listings->get_general_info();
								if(mysql_num_rows($get_all_art_subscriber)>0)
								{
									while($get_art = mysql_fetch_assoc($get_all_art_subscriber))
									{
										if($get_art['related_type'] == 'community_event' || $get_art['related_type'] == 'artist_event')
										{
											$types = $get_art['related_type'];
											$related = explode('_',$get_art['related_id']);
											$get_subscriber_info = $sub_listings->get_subscriber_info_org($related[1],$types);
											$get_subscriber_info = $sub_listings->get_subscriber_info_org($related[1],$types);
											if($get_art['related_type'] == 'community_event')
											{
												$get_gen_info_del = $sub_listings->artist_communitySubscribedTo($types,$get_subscriber_info['community_id']);
											}
											elseif($get_art['related_type'] == 'artist_event')
											{
												$get_gen_info_del = $sub_listings->artist_communitySubscribedTo($types,$get_subscriber_info['artist_id']);
											}
										
							?>
										<div class="tableCont">
											<!--<div class="blkD"><img src="general_jcrop/croppedFiles/thumb/<?php //if(isset($run_sub_user_to['image_name']) && $run_sub_user_to['image_name']!="") {echo $run_sub_user_to['image_name']; } else{ echo "Noimage.png"; }?>" /></div>-->
											<div class="blkA" style="width:280px;"><?php if(!empty($get_subscriber_info['title'])) { echo $get_subscriber_info['title']; } else { echo "&nbsp"; } ?></div>
											<div class="blkB"><?php if(!empty($get_subscriber_info['city'])) { echo $get_subscriber_info['city']; } else { echo "&nbsp"; } ?></div>
											<div class="blkE" style="width:80px;"><?php
											$subs_e = "";
											if(isset($get_subscriber_info['artist_id']))
											{
												$subs_e =  $sub_listings->get_artiste_user_subtype($get_subscriber_info['id']);
											}
											elseif(isset($get_subscriber_info['community_id']))
											{
												$subs_e =  $sub_listings->get_communitye_user_subtype($get_subscriber_info['id']);
											}
											if($subs_e!="" && !empty($subs_e))
											{
												echo $subs_e['name'];
											}
											else
											{
												echo "&nbsp";
											}
											?></div>
											<!--<div class="blkF" onclick="runAccordion(<?php //echo $ans_sub_user[$sub_i]['id']; ?>);">
												<div onselectstart="return false;"><img src="images/profile/view.jpg" /></div>
											</div>-->
											<div class="blkF" style="float:right; width:39px;"><a href="delete_subscriber.php?id=<?php echo $get_art['id'];?>&email=<?php echo $get_gen_info_del['email'];?>" onclick="return confirm_subscriber_delete('<?php echo $get_subscriber_info['title']; ?>')"><img src="images/profile/delete.png" style="margin-top:5px;" /></a></div>
										</div>
							<?php
										}
									}
								} */
								
							?>
						</div>
                    </div>
                    <div id="subPutify"  style="display:none;">
                        <div id="actualContent">
                            <div class="fieldCont">Subscribe to updates from Purify Art on new services, changes to site, and promotional opportunities.</div>
							<?php
								$sql_chk = $sub_listings->Newsletter($_SESSION['login_email']);
							?>
                            <div class="fieldCont">
                                <div class="fieldTitle">Email</div>
                                <?php
									if(mysql_num_rows($sql_chk)>0)
									{
								?>
										<div class="chkCont">
											<input type="radio" class="chk" name="sub_news" onClick="on_values()" value="on"/>
											<div class="radioTitle" >On</div>
										</div>
										<div class="chkCont">
											<input type="radio" class="chk" name="sub_news" checked onClick="off_values()" value="off"/>
											<div class="radioTitle">Off</div>
										</div>
								<?php
									}
									else
									{
								?>
										<div class="chkCont">
											<input type="radio" class="chk" name="sub_news" checked onClick="on_values()" value="on"/>
											<div class="radioTitle" >On</div>
										</div>
										<div class="chkCont">
											<input type="radio" class="chk" name="sub_news" onClick="off_values()" value="off"/>
											<div class="radioTitle">Off</div>
										</div>
								<?php								
									}
								?>
                            </div>
                            <!--<div class="fieldCont">
                                <div class="fieldTitle">RSS Feed</div>
                                <div class="status"><a href=""><?php echo $domainname; ?></a></div>
                            </div>-->
                            
                        </div>
                    </div>
                    <div id="subArtist" style="display:none;">
                    	<div id="mediaContent">
                            <div class="titleCont">
                                <!--<div class="blkD">Profile</div>-->
                                <div class="blkA">Name</div>
                                <div class="blkB">Location</div>
                                <div class="blkE">Type</div>
                                <div class="blkF">Delete</div>
                            </div>
							<?php
								$subs_arts = array();
								$get_all_art_subscriber = $sub_listings->get_all_org_sub();
								$get_gen_info = $sub_listings->get_general_info();
								if($get_all_art_subscriber!="" && $get_all_art_subscriber!=NULL)
								{
									if(mysql_num_rows($get_all_art_subscriber)>0)
									{
										$rr_in = 0;
										while($get_art = mysql_fetch_assoc($get_all_art_subscriber))
										{
											
											if($get_art['related_type'] == 'artist')
											{
												$types = 'artist';
												$get_subscriber_info = $sub_listings->get_subscriber_info_org($get_art['related_id'],$types);
												$get_gen_info_del = $sub_listings->artist_communitySubscribedTo($types,$get_art['related_id']);
												
												if(!empty($get_subscriber_info) && $get_subscriber_info!="")
												{
													$subs_arts[$rr_in] = $get_subscriber_info;
													//$subs_arts['name_sor'] = $get_subscriber_info['name'];
													$subs_arts[$rr_in]['general_user_id'] = $get_gen_info_del['general_user_id'];
													$subs_arts[$rr_in]['id'] = $get_art['id'];
													$subs_arts[$rr_in]['email_update'] = $get_art['email_update'];
													$subs_arts[$rr_in]['news_feed'] = $get_art['news_feed'];
												}
											}										
											$rr_in = $rr_in + 1;
										}
									}
								}
								
								$sort = array();
								foreach($subs_arts as $k=>$v)
								{
									//if(isset($v['name']) && $v['name']!="")
									//{
										$sort['name'][$k] = strtolower($v['name']);
									//}
									
								}
								if(!empty($sort))
								{
									array_multisort($sort['name'], SORT_ASC,$subs_arts);
								}
									
									for($a_u=0;$a_u<count($subs_arts);$a_u++)
									{
										if(!empty($subs_arts[$a_u]['artist_id']) && $subs_arts[$a_u]['artist_id']!=0)
										{
							?>
										<div class="tableCont">
											<!--<div class="blkD"><img src="general_jcrop/croppedFiles/thumb/<?php //if(isset($run_sub_user_to['image_name']) && $run_sub_user_to['image_name']!="") {echo $run_sub_user_to['image_name']; } else{ echo "Noimage.png"; }?>" /></div>-->
											<div class="blkA"><?php if(!empty($subs_arts[$a_u]['name'])) { echo $subs_arts[$a_u]['name']; } else { echo "&nbsp"; } ?></div>
											<div class="blkB"><?php if(!empty($subs_arts[$a_u]['city'])) { echo $subs_arts[$a_u]['city']; } else { echo "&nbsp"; } ?></div>
											<div class="blkE" style="width:138px;"><?php  $subs = ""; $subs =  $sub_listings->get_artist_user_subtype($subs_arts[$a_u]['artist_id']);
											if($subs!="" && !empty($subs))
											{
												echo $subs['name'];
											}
											else
											{
												echo "&nbsp";
											}
											?></div>
											<!--<div class="blkF" onclick="runAccordion(<?php //echo $ans_sub_user[$sub_i]['id']; ?>);">
												<div onselectstart="return false;"><img src="images/profile/view.jpg" /></div>
											</div>-->
											<div class="icon" style="width:37px;" onclick="runAccordion_new('<?php echo $a_u; ?>_pay1');">
											<div onselectstart="return false;"><img  id="trcn_info_<?php echo $a_u; ?>" onclick="lists_views();" src="images/profile/view.jpg" /></div>
											</div>
										
											<div class="blkF" style="width:39px;"><a href="delete_subscriber.php?emsil=<?php echo $subs_arts[$a_u]['id']; ?>&id=<?php echo $subs_arts[$a_u]['general_user_id']; ?>" onclick="return confirm_subscriber_delete('<?php echo $subs_arts[$a_u]['name']; ?>')"><img style="margin-top: 5px;" src="images/profile/delete.png" /></a></div>
											
											<div class="expandable" id="Accordion_new<?php echo $a_u; ?>_pay1Content" style="margin-left: 80px; top: 17px;">
											<div id="actualContent" style="width:400px;">
												<div class="fieldCont">
													<div class="fieldTitle">News Feeds :-</div>
<?php
													
													if($subs_arts[$a_u]['news_feed']=='1')
													{
?>
														<div class="chkCont">
															<input checked  type="radio" class="chk" name="a_feeds_news_<?php echo $subs_arts[$a_u]['id']; ?>"  onClick="on_feeds('<?php echo $subs_arts[$a_u]['id']; ?>')" value="on"/>
															<div class="radioTitle" >On</div>
														</div>
														<div class="chkCont">
															<input type="radio" class="chk" name="a_feeds_news_<?php echo $subs_arts[$a_u]['id']; ?>" onClick="off_feeds('<?php echo $subs_arts[$a_u]['id']; ?>')" value="off"/>
															<div class="radioTitle">Off</div>
														</div>
<?php
													}
													else
													{
?>
														<div class="chkCont">
															<input type="radio" class="chk" name="a_feeds_news_<?php echo $subs_arts[$a_u]['id']; ?>"  onClick="on_feeds('<?php echo $subs_arts[$a_u]['id']; ?>')" value="on"/>
															<div class="radioTitle" >On</div>
														</div>
														<div class="chkCont">
															<input checked type="radio" class="chk"  name="a_feeds_news_<?php echo $subs_arts[$a_u]['id']; ?>" onClick="off_feeds('<?php echo $subs_arts[$a_u]['id']; ?>')" value="off"/>
															<div class="radioTitle">Off</div>
														</div>
<?php
													}
?>
												</div>
												<div class="fieldCont">
													<div class="fieldTitle">Email :-</div>
<?php
													$type = 'artist';
													/* if($subs_arts[$a_u]['related_type']=='artist' || $subs_arts[$a_u]['related_type']=='artist_project')
													{
														$type = 'artist';
													}
													elseif($subs_arts[$a_u]['related_type']=='community' || $subs_arts[$a_u]['related_type']=='community_project')
													{
														$type = 'community';
													} */
													
													if($subs_arts[$a_u]['email_update']=='1')
													{
?>
														<div class="chkCont">
															<input checked type="radio" class="chk" name="a_emails_<?php echo $subs_arts[$a_u]['id']; ?>"  onClick="on_emails('<?php echo $subs_arts[$a_u]['id']; ?>','<?php echo $_SESSION['login_email']; ?>','<?php echo $subs_arts[$a_u]['general_user_id']; ?>','<?php echo $type; ?>')" value="on"/>
															<div class="radioTitle" >On</div>
														</div>
														<div class="chkCont">
															<input type="radio" class="chk" name="a_emails_<?php echo $subs_arts[$a_u]['id']; ?>" onClick="off_emails('<?php echo $subs_arts[$a_u]['id']; ?>','<?php echo $_SESSION['login_email']; ?>','<?php echo $subs_arts[$a_u]['general_user_id']; ?>','<?php echo $type; ?>')" value="off"/>
															<div class="radioTitle">Off</div>
														</div>
<?php
													}
													else
													{
?>
														<div class="chkCont">
															<input type="radio" class="chk" name="a_emails_<?php echo $subs_arts[$a_u]['id']; ?>"  onClick="on_emails('<?php echo $subs_arts[$a_u]['id']; ?>','<?php echo $_SESSION['login_email']; ?>','<?php echo $subs_arts[$a_u]['general_user_id']; ?>','<?php echo $type; ?>')" value="on"/>
															<div class="radioTitle" >On</div>
														</div>
														<div class="chkCont">
															<input checked type="radio" class="chk" name="a_emails_<?php echo $subs_arts[$a_u]['id']; ?>" onClick="off_emails('<?php echo $subs_arts[$a_u]['id']; ?>','<?php echo $_SESSION['login_email']; ?>','<?php echo $subs_arts[$a_u]['general_user_id']; ?>','<?php echo $type; ?>')" value="off"/>
															<div class="radioTitle">Off</div>
														</div>
<?php
													}
?>
												</div>
											</div>
										</div>
										</div>
							<?php
										}
									}
								
								/*$get_all_art_subscriber = $sub_listings->get_all_subscriber();
								$get_gen_info = $sub_listings->get_general_info();
								if(mysql_num_rows($get_all_art_subscriber)>0)
								{
									while($get_art = mysql_fetch_assoc($get_all_art_subscriber))
									{
										$get_subscriber_info = $sub_listings->get_subscriber_info($get_art['email']);

										if($get_art['related_id'] == $get_gen_info['artist_id'])
										{
							?>
										<div class="tableCont">
											<!--<div class="blkD"><img src="general_jcrop/croppedFiles/thumb/<?php //if(isset($run_sub_user_to['image_name']) && $run_sub_user_to['image_name']!="") {echo $run_sub_user_to['image_name']; } else{ echo "Noimage.png"; }?>" /></div>-->
											<div class="blkB"><?php if(!empty($get_subscriber_info['fname'])) { echo $get_subscriber_info['fname'].' '.$get_subscriber_info['lname']; } else { echo "&nbsp"; } ?></div>
											<div class="blkE"><?php if(!empty($get_subscriber_info['city'])) { echo $get_subscriber_info['city']; } else { echo "&nbsp"; } ?></div>
											<div class="blkG" style="width:80px;">Artist</div>
											<!--<div class="blkF" onclick="runAccordion(<?php //echo $ans_sub_user[$sub_i]['id']; ?>);">
												<div onselectstart="return false;"><img src="images/profile/view.jpg" /></div>
											</div>-->
											<div class="icon"><a href="delete_subscriber.php?id=<?php echo $get_art['id'];?>&email=<?php echo $get_art['email'];?>" onclick="return confirm_subscriber_delete('<?php echo $get_art['name']; ?>')"><img src="images/profile/delete.png" /></a></div>
										</div>
							<?php
										}
									}
								} */
							?>
						</div>
                    </div>
                    <div id="subCommunity" style="display:none;">
                    	<div id="mediaContent">
                            <div class="titleCont">
                                <!--<div class="blkD">Profile</div>-->
                                <div class="blkA">Name</div>
                                <div class="blkB">Location</div>
                                <div class="blkE">Type</div>
                                <div class="blkF">Delete</div>
                            </div>
							<?php
								$subs_coms = array();
								$get_all_art_subscriber = $sub_listings->get_all_org_sub();
								$get_gen_info = $sub_listings->get_general_info();
								if($get_all_art_subscriber!="" && $get_all_art_subscriber!=NULL)
								{
									if(mysql_num_rows($get_all_art_subscriber)>0)
									{
										$rr_in = 0;
										while($get_art = mysql_fetch_assoc($get_all_art_subscriber))
										{
											if($get_art['related_type'] == 'community')
											{
												$types = 'community';
												$get_subscriber_info = $sub_listings->get_subscriber_info_org($get_art['related_id'],$types);
												$get_gen_info_del = $sub_listings->artist_communitySubscribedTo($types,$get_art['related_id']);
												
												if(!empty($get_subscriber_info) && $get_subscriber_info!="")
												{
													$subs_coms[$rr_in] = $get_subscriber_info;
													//$subs_arts['name_sor'] = $get_subscriber_info['name'];
													$subs_coms[$rr_in]['general_user_id'] = $get_gen_info_del['general_user_id'];
													$subs_coms[$rr_in]['id'] = $get_art['id'];
													$subs_coms[$rr_in]['email_update'] = $get_art['email_update'];
													$subs_coms[$rr_in]['news_feed'] = $get_art['news_feed'];
												}
											}
											$rr_in = $rr_in + 1;
										}
									}
								}
								
								$sort = array();
								foreach($subs_coms as $k=>$v)
								{
									//if(isset($v['name']) && $v['name']!="")
									//{
										$sort['name'][$k] = strtolower($v['name']);
									//}									
								}
								if(!empty($sort))
								{
									array_multisort($sort['name'], SORT_ASC,$subs_coms);
								}
									for($c_u=0;$c_u<count($subs_coms);$c_u++)
									{
										if(!empty($subs_coms[$c_u]['community_id']) && $subs_coms[$c_u]['community_id']!=0)
										{
										
							?>
										<div class="tableCont">
											<!--<div class="blkD"><img src="general_jcrop/croppedFiles/thumb/<?php //if(isset($run_sub_user_to['image_name']) && $run_sub_user_to['image_name']!="") {echo $run_sub_user_to['image_name']; } else{ echo "Noimage.png"; }?>" /></div>-->
											<div class="blkA"><?php if(!empty($subs_coms[$c_u]['name'])) { echo $subs_coms[$c_u]['name']; } else { echo "&nbsp"; } ?></div>
											<div class="blkB"><?php if(!empty($subs_coms[$c_u]['city'])) { echo $subs_coms[$c_u]['city']; } else { echo "&nbsp"; } ?></div>
											<div class="blkE" style="width:138px;"><?php $subs_c = ""; $subs_c =  $sub_listings->get_community_user_subtype($subs_coms[$c_u]['community_id']);
											if($subs_c!="" && !empty($subs_c))
											{
												echo $subs_c['name'];
											}
											else
											{
												echo "&nbsp";
											}
											?></div>
											<!--<div class="blkF" onclick="runAccordion(<?php //echo $ans_sub_user[$sub_i]['id']; ?>);">
												<div onselectstart="return false;"><img src="images/profile/view.jpg" /></div>
											</div>-->
											<div class="icon" style="width:37px;" onclick="runAccordion_new('<?php echo $c_u; ?>_pay2');">
											<div onselectstart="return false;"><img  id="trcn_info_<?php echo $c_u; ?>" onclick="lists_views();" src="images/profile/view.jpg" /></div>
											</div>
											
											<div class="blkF" style="width:39px;"><a href="delete_subscriber.php?emsil=<?php echo $get_art['id'];?>&id=<?php echo $subs_coms[$c_u]['general_user_id'];?>" onclick="return confirm_subscriber_delete('<?php echo $subs_coms[$c_u]['name']; ?>')"><img style="margin-top: 5px;" src="images/profile/delete.png" /></a></div>
											
											<div class="expandable" id="Accordion_new<?php echo $c_u; ?>_pay2Content" style="margin-left: 80px; top: 17px;">
											<div id="actualContent" style="width:400px;">
												<div class="fieldCont">
													<div class="fieldTitle">News Feeds :-</div>
<?php
													
													if($subs_coms[$c_u]['news_feed']=='1')
													{
?>
														<div class="chkCont">
															<input checked  type="radio" class="chk" name="a_feeds_news_<?php echo $subs_coms[$c_u]['id']; ?>"  onClick="on_feeds('<?php echo $subs_coms[$c_u]['id']; ?>')" value="on"/>
															<div class="radioTitle" >On</div>
														</div>
														<div class="chkCont">
															<input type="radio" class="chk" name="a_feeds_news_<?php echo $subs_coms[$c_u]['id']; ?>" onClick="off_feeds('<?php echo $subs_coms[$c_u]['id']; ?>')" value="off"/>
															<div class="radioTitle">Off</div>
														</div>
<?php
													}
													else
													{
?>
														<div class="chkCont">
															<input type="radio" class="chk" name="a_feeds_news_<?php echo $subs_coms[$c_u]['id']; ?>"  onClick="on_feeds('<?php echo $subs_coms[$c_u]['id']; ?>')" value="on"/>
															<div class="radioTitle" >On</div>
														</div>
														<div class="chkCont">
															<input checked type="radio" class="chk"  name="a_feeds_news_<?php echo $subs_coms[$c_u]['id']; ?>" onClick="off_feeds('<?php echo $subs_coms[$c_u]['id']; ?>')" value="off"/>
															<div class="radioTitle">Off</div>
														</div>
<?php
													}
?>
												</div>
												<div class="fieldCont">
													<div class="fieldTitle">Email :-</div>
<?php
													$type = 'community';
													/* if($subs_coms[$a_u]['related_type']=='artist' || $subs_coms[$a_u]['related_type']=='artist_project')
													{
														$type = 'artist';
													}
													elseif($subs_coms[$a_u]['related_type']=='community' || $subs_coms[$a_u]['related_type']=='community_project')
													{
														$type = 'community';
													} */
													
													if($subs_coms[$c_u]['email_update']=='1')
													{
?>
														<div class="chkCont">
															<input checked type="radio" class="chk" name="a_emails_<?php echo $subs_coms[$c_u]['id']; ?>"  onClick="on_emails('<?php echo $subs_coms[$c_u]['id']; ?>','<?php echo $_SESSION['login_email']; ?>','<?php echo $subs_coms[$c_u]['general_user_id']; ?>','<?php echo $type; ?>')" value="on"/>
															<div class="radioTitle" >On</div>
														</div>
														<div class="chkCont">
															<input type="radio" class="chk" name="a_emails_<?php echo $subs_coms[$c_u]['id']; ?>" onClick="off_emails('<?php echo $subs_coms[$c_u]['id']; ?>','<?php echo $_SESSION['login_email']; ?>','<?php echo $subs_coms[$c_u]['general_user_id']; ?>','<?php echo $type; ?>')" value="off"/>
															<div class="radioTitle">Off</div>
														</div>
<?php
													}
													else
													{
?>
														<div class="chkCont">
															<input type="radio" class="chk" name="a_emails_<?php echo $subs_coms[$c_u]['id']; ?>"  onClick="on_emails('<?php echo $subs_coms[$c_u]['id']; ?>','<?php echo $_SESSION['login_email']; ?>','<?php echo $subs_coms[$c_u]['general_user_id']; ?>','<?php echo $type; ?>')" value="on"/>
															<div class="radioTitle" >On</div>
														</div>
														<div class="chkCont">
															<input checked type="radio" class="chk" name="a_emails_<?php echo $subs_coms[$c_u]['id']; ?>" onClick="off_emails('<?php echo $subs_coms[$c_u]['id']; ?>','<?php echo $_SESSION['login_email']; ?>','<?php echo $subs_coms[$c_u]['general_user_id']; ?>','<?php echo $type; ?>')" value="off"/>
															<div class="radioTitle">Off</div>
														</div>
<?php
													}
?>
												</div>
											</div>
										</div>
										</div>
							<?php
										}
									}
										
								
								/*$get_all_com_subscriber = $sub_listings->get_all_subscriber();
								$get_gen_info = $sub_listings->get_general_info();
								if(mysql_num_rows($get_all_com_subscriber)>0)
								{
									while($get_com = mysql_fetch_assoc($get_all_com_subscriber))
									{
										$get_subscriber_info = $sub_listings->get_subscriber_info($get_com['email']);
										if($get_com['related_id'] == $get_gen_info['community_id'])
										{
							?>
										<div class="tableCont">
											<!--<div class="blkD"><img src="general_jcrop/croppedFiles/thumb/<?php //if(isset($run_sub_user_to['image_name']) && $run_sub_user_to['image_name']!="") {echo $run_sub_user_to['image_name']; } else{ echo "Noimage.png"; }?>" /></div>-->
											<div class="blkB"><?php if($get_subscriber_info['fname']!="") { echo $get_subscriber_info['fname'].' '.$get_subscriber_info['lname']; } else { echo "&nbsp"; } ?></div>
											<div class="blkE"><?php if($get_subscriber_info['city']!="") { echo $get_subscriber_info['city']; } else { echo "&nbsp"; } ?></div>
											<div class="blkG" style="width:80px;">Community</div>
											<!--<div class="blkF" onclick="runAccordion(<?php //echo $ans_sub_user[$sub_i]['id']; ?>);">
												<div onselectstart="return false;"><img src="images/profile/view.jpg" /></div>
											</div>-->
											<div class="icon"><a href="delete_subscriber.php?id=<?php echo $get_com['id'];?>&email=<?php echo $get_com['email'];?>" onclick="return confirm_subscriber_delete('<?php echo $get_com['name']; ?>')"><img src="images/profile/delete.png" /></a></div>
										</div>
							<?php
										}
									}
								}*/
							?>
                        </div>
                    </div>
					
					<div id="sub_Projects" style="display:none;">
                    	<div id="mediaContent">
                            <div class="titleCont">
                                <!--<div class="blkD">Profile</div>-->
                                <div class="blkA">Name</div>
                                <div class="blkB">Location</div>
                                <div class="blkE">Type</div>
                                <div class="blkF">Delete</div>
                            </div>
                           <?php
								$subs_pros = array();
								$get_all_art_subscriber = $sub_listings->get_all_org_sub();
								$get_gen_info = $sub_listings->get_general_info();
								if($get_all_art_subscriber!="" && $get_all_art_subscriber!=NULL)
								{
									if(mysql_num_rows($get_all_art_subscriber)>0)
									{
										$rr_in = 0;
										while($get_art = mysql_fetch_assoc($get_all_art_subscriber))
										{
											if($get_art['related_type'] == 'community_project' || $get_art['related_type'] == 'artist_project')
											{
												$types = $get_art['related_type'];
												$types_1 = explode('_',$get_art['related_type']);
												
												$related = explode('_',$get_art['related_id']);
												$get_subscriber_info = $sub_listings->get_subscriber_info_org($related[1],$types);
												if($get_art['related_type'] == 'community_project')
												{
													$get_gen_info_del = $sub_listings->artist_communitySubscribedTo($types_1[0],$get_subscriber_info['community_id']);
												}
												elseif($get_art['related_type'] == 'artist_project')
												{
													$get_gen_info_del = $sub_listings->artist_communitySubscribedTo($types_1[0],$get_subscriber_info['artist_id']);
												}
												
												if(!empty($get_subscriber_info) && $get_subscriber_info!="")
												{
													$subs_pros[$rr_in] = $get_subscriber_info;
													//$subs_arts['name_sor'] = $get_subscriber_info['name'];
													$subs_pros[$rr_in]['general_user_id'] = $get_gen_info_del['general_user_id'];
													$subs_pros[$rr_in]['id'] = $get_art['id'];
													$subs_pros[$rr_in]['email_update'] = $get_art['email_update'];
													$subs_pros[$rr_in]['news_feed'] = $get_art['news_feed'];
													$subs_pros[$rr_in]['related_type'] = $get_art['related_type'];
												}
											}
											$rr_in = $rr_in + 1;
										}
									}
								}
								
								$sort = array();
								foreach($subs_pros as $k=>$v)
								{
									//if(isset($v['name']) && $v['name']!="")
									//{
										$sort['title'][$k] = strtolower($v['title']);
									//}									
								}
								if(!empty($sort))
								{
									array_multisort($sort['title'], SORT_ASC,$subs_pros);
								}
								
								for($p_u=0;$p_u<count($subs_pros);$p_u++)
								{
									if(!empty($subs_pros[$p_u]['id']) && $subs_pros[$p_u]['id']!=0)
									{
										
							?>
										<div class="tableCont">
											<!--<div class="blkD"><img src="general_jcrop/croppedFiles/thumb/<?php //if(isset($run_sub_user_to['image_name']) && $run_sub_user_to['image_name']!="") {echo $run_sub_user_to['image_name']; } else{ echo "Noimage.png"; }?>" /></div>-->
											<div class="blkA"><?php if(!empty($subs_pros[$p_u]['title'])) { echo $subs_pros[$p_u]['title']; } else { echo "&nbsp"; } ?></div>
											<div class="blkB"><?php if(!empty($subs_pros[$p_u]['city'])) { echo $subs_pros[$p_u]['city']; } else { echo "&nbsp"; } ?></div>
											<div class="blkE" style="width:138px;"><?php
											$subs_p = "";
											if(isset($subs_pros[$p_u]['artist_id']))
											{
												$subs_p =  $sub_listings->get_artistp_user_subtype($subs_pros[$p_u]['id']);
											}
											elseif(isset($subs_pros[$p_u]['community_id']))
											{
												$subs_p =  $sub_listings->get_communityp_user_subtype($subs_pros[$p_u]['id']);
											}
											if($subs_p!="" && !empty($subs_p))
											{
												echo $subs_p['name'];
											}
											else
											{
												echo "&nbsp";
											}
											?></div>
											<!--<div class="blkF" onclick="runAccordion(<?php //echo $ans_sub_user[$sub_i]['id']; ?>);">
												<div onselectstart="return false;"><img src="images/profile/view.jpg" /></div>
											</div>-->
											<div class="icon" style="width:37px;" onclick="runAccordion_new('<?php echo $p_u; ?>_pay3');">
											<div onselectstart="return false;"><img  id="trcn_info_<?php echo $p_u; ?>" onclick="lists_views();" src="images/profile/view.jpg" /></div>
											</div>
											
											<div class="blkF" style="width:39px;"><a href="delete_subscriber.php?emsil=<?php echo $get_art['id'];?>&id=<?php echo $subs_pros[$p_u]['general_user_id'];?>" onclick="return confirm_subscriber_delete('<?php echo $subs_pros[$p_u]['title']; ?>')"><img style="margin-top: 5px;" src="images/profile/delete.png" /></a></div>
											
											<div class="expandable" id="Accordion_new<?php echo $p_u; ?>_pay3Content" style="margin-left: 80px; top: 17px;">
											<div id="actualContent" style="width:400px;">
												<div class="fieldCont">
													<div class="fieldTitle">News Feeds :-</div>
<?php
													
													if($subs_pros[$p_u]['news_feed']=='1')
													{
?>
														<div class="chkCont">
															<input checked  type="radio" class="chk" name="a_feeds_news_<?php echo $subs_pros[$p_u]['id']; ?>"  onClick="on_feeds('<?php echo $subs_pros[$p_u]['id']; ?>')" value="on"/>
															<div class="radioTitle" >On</div>
														</div>
														<div class="chkCont">
															<input type="radio" class="chk" name="a_feeds_news_<?php echo $subs_pros[$p_u]['id']; ?>" onClick="off_feeds('<?php echo $subs_pros[$p_u]['id']; ?>')" value="off"/>
															<div class="radioTitle">Off</div>
														</div>
<?php
													}
													else
													{
?>
														<div class="chkCont">
															<input type="radio" class="chk" name="a_feeds_news_<?php echo $subs_pros[$p_u]['id']; ?>"  onClick="on_feeds('<?php echo $subs_pros[$p_u]['id']; ?>')" value="on"/>
															<div class="radioTitle" >On</div>
														</div>
														<div class="chkCont">
															<input checked type="radio" class="chk"  name="a_feeds_news_<?php echo $subs_pros[$p_u]['id']; ?>" onClick="off_feeds('<?php echo $subs_pros[$p_u]['id']; ?>')" value="off"/>
															<div class="radioTitle">Off</div>
														</div>
<?php
													}
?>
												</div>
												<div class="fieldCont">
													<div class="fieldTitle">Email :-</div>
<?php
													$type = '';
													if($subs_pros[$p_u]['related_type']=='artist' || $subs_pros[$p_u]['related_type']=='artist_project')
													{
														$type = 'artist';
													}
													elseif($subs_pros[$p_u]['related_type']=='community' || $subs_pros[$p_u]['related_type']=='community_project')
													{
														$type = 'community';
													}
													
													if($subs_pros[$p_u]['email_update']=='1')
													{
?>
														<div class="chkCont">
															<input checked type="radio" class="chk" name="a_emails_<?php echo $subs_pros[$p_u]['id']; ?>"  onClick="on_emails('<?php echo $subs_pros[$p_u]['id']; ?>','<?php echo $_SESSION['login_email']; ?>','<?php echo $subs_pros[$p_u]['general_user_id']; ?>','<?php echo $type; ?>')" value="on"/>
															<div class="radioTitle" >On</div>
														</div>
														<div class="chkCont">
															<input type="radio" class="chk" name="a_emails_<?php echo $subs_pros[$p_u]['id']; ?>" onClick="off_emails('<?php echo $subs_pros[$p_u]['id']; ?>','<?php echo $_SESSION['login_email']; ?>','<?php echo $subs_pros[$p_u]['general_user_id']; ?>','<?php echo $type; ?>')" value="off"/>
															<div class="radioTitle">Off</div>
														</div>
<?php
													}
													else
													{
?>
														<div class="chkCont">
															<input type="radio" class="chk" name="a_emails_<?php echo $subs_pros[$p_u]['id']; ?>"  onClick="on_emails('<?php echo $subs_pros[$p_u]['id']; ?>','<?php echo $_SESSION['login_email']; ?>','<?php echo $subs_pros[$p_u]['general_user_id']; ?>','<?php echo $type; ?>')" value="on"/>
															<div class="radioTitle" >On</div>
														</div>
														<div class="chkCont">
															<input checked type="radio" class="chk" name="a_emails_<?php echo $subs_pros[$p_u]['id']; ?>" onClick="off_emails('<?php echo $subs_pros[$p_u]['id']; ?>','<?php echo $_SESSION['login_email']; ?>','<?php echo $subs_pros[$p_u]['general_user_id']; ?>','<?php echo $type; ?>')" value="off"/>
															<div class="radioTitle">Off</div>
														</div>
<?php
													}
?>
												</div>
											</div>
										</div>
										</div>
							<?php
									}
								}										

								/*$get_all_pro_subscriber = $sub_listings->get_all_subscriber();
								$get_gen_info = $sub_listings->get_general_info();
								if(mysql_num_rows($get_all_pro_subscriber)>0)
								{
									while($get_pro = mysql_fetch_assoc($get_all_pro_subscriber))
									{
										$get_subscriber_info = $sub_listings->get_subscriber_info($get_pro['email']);
										if($get_pro['related_type']=="artist_project" || $get_pro['related_type']=="community_project")
										{
							?>
										<div class="tableCont">
											<!--<div class="blkD"><img src="general_jcrop/croppedFiles/thumb/<?php //if(isset($run_sub_user_to['image_name']) && $run_sub_user_to['image_name']!="") {echo $run_sub_user_to['image_name']; } else{ echo "Noimage.png"; }?>" /></div>-->
											<div class="blkB"><?php echo $get_pro['name']; ?></div>
											<div class="blkE"><?php echo $get_subscriber_info['city'];?></div>
											<div class="blkG" style="width:80px;"><?php if($get_pro['related_type']=="artist_project"){echo "Artist";}else if($get_pro['related_type']=="community_project"){echo "Community";}?></div>
											<!--<div class="blkF" onclick="runAccordion(<?php //echo $ans_sub_user[$sub_i]['id']; ?>);">
												<div onselectstart="return false;"><img src="images/profile/view.jpg" /></div>
											</div>-->
											<div class="icon"><a href="delete_subscriber.php?id=<?php echo $get_pro['id'];?>&email=<?php echo $get_pro['email'];?>" onclick="return confirm_subscriber_delete('<?php echo $get_pro['name']; ?>')"><img src="images/profile/delete.png" /></a></div>
										</div>
							<?php
										}
									}
								}*/
							?>
                        </div>
                    </div>
					
					<div id="sub_Events" style="display:none;">
                    	<div id="mediaContent">
                            <div class="titleCont">
                                <!--<div class="blkD">Profile</div>-->
                                <div class="blkA">Name</div>
                                <div class="blkB">Location</div>
                                <div class="blkE">Type</div>
                                <div class="blkF">Delete</div>
                            </div>
							<?php
								$subs_eves = array();
								$get_all_art_subscriber = $sub_listings->get_all_org_sub();
								$get_gen_info = $sub_listings->get_general_info();
								if($get_all_art_subscriber!="" && $get_all_art_subscriber!=NULL)
								{
									if(mysql_num_rows($get_all_art_subscriber)>0)
									{
										$rr_in = 0;
										while($get_art = mysql_fetch_assoc($get_all_art_subscriber))
										{
											if($get_art['related_type'] == 'community_event' || $get_art['related_type'] == 'artist_event')
											{
												$types = $get_art['related_type'];
												$types_1 = explode('_',$get_art['related_type']);
												
												$related = explode('_',$get_art['related_id']);
												$get_subscriber_info = $sub_listings->get_subscriber_info_org($related[1],$types);
												if($get_art['related_type'] == 'community_event')
												{
													$get_gen_info_del = $sub_listings->artist_communitySubscribedTo($types_1[0],$get_subscriber_info['community_id']);
												}
												elseif($get_art['related_type'] == 'artist_event')
												{
													$get_gen_info_del = $sub_listings->artist_communitySubscribedTo($types_1[0],$get_subscriber_info['artist_id']);
												}
												
												if(!empty($get_subscriber_info) && $get_subscriber_info!="")
												{
													$subs_eves[$rr_in] = $get_subscriber_info;
													//$subs_arts['name_sor'] = $get_subscriber_info['name'];
													$subs_eves[$rr_in]['general_user_id'] = $get_gen_info_del['general_user_id'];
													$subs_eves[$rr_in]['id'] = $get_art['id'];
													$subs_eves[$rr_in]['email_update'] = $get_art['email_update'];
													$subs_eves[$rr_in]['news_feed'] = $get_art['news_feed'];
													$subs_eves[$rr_in]['related_type'] = $get_art['related_type'];
												}
											}
											$rr_in = $rr_in + 1;
										}
									}
								}
								
								$sort = array();
								foreach($subs_eves as $k=>$v)
								{
									//if(isset($v['name']) && $v['name']!="")
									//{
										$sort['title'][$k] = strtolower($v['title']);
									//}									
								}
								if(!empty($sort))
								{
									array_multisort($sort['title'], SORT_ASC,$subs_eves);
								}
								
								for($e_u=0;$e_u<count($subs_eves);$e_u++)
								{
									if(!empty($subs_eves[$e_u]['id']) && $subs_eves[$e_u]['id']!=0)
									{
										
							?>
										<div class="tableCont">
											<!--<div class="blkD"><img src="general_jcrop/croppedFiles/thumb/<?php //if(isset($run_sub_user_to['image_name']) && $run_sub_user_to['image_name']!="") {echo $run_sub_user_to['image_name']; } else{ echo "Noimage.png"; }?>" /></div>-->
											<div class="blkA"><?php if(!empty($subs_eves[$e_u]['title'])) { echo $subs_eves[$e_u]['title']; } else { echo "&nbsp"; } ?></div>
											<div class="blkB"><?php if(!empty($subs_eves[$e_u]['city'])) { echo $subs_eves[$e_u]['city']; } else { echo "&nbsp"; } ?></div>
											<div class="blkE" style="width:80px;"><?php
											$subs_e = "";
											if(isset($subs_eves[$e_u]['artist_id']))
											{
												$subs_e =  $sub_listings->get_artiste_user_subtype($subs_eves[$e_u]['id']);
											}
											elseif(isset($subs_eves[$e_u]['community_id']))
											{
												$subs_e =  $sub_listings->get_communitye_user_subtype($subs_eves[$e_u]['id']);
											}
											if($subs_e!="" && !empty($subs_e))
											{
												echo $subs_e['name'];
											}
											else
											{
												echo "&nbsp";
											}
											?></div>
											<!--<div class="blkF" onclick="runAccordion(<?php //echo $ans_sub_user[$sub_i]['id']; ?>);">
												<div onselectstart="return false;"><img src="images/profile/view.jpg" /></div>
											</div>-->
											<div class="icon" style="width:37px;" onclick="runAccordion_new('<?php echo $e_u; ?>_pay4');">
											<div onselectstart="return false;"><img  id="trcn_info_<?php echo $e_u; ?>" onclick="lists_views();" src="images/profile/view.jpg" /></div>
											</div>
											
											<div class="blkF" style="float:right;"><a href="delete_subscriber.php?emsil=<?php echo $get_art['id'];?>&id=<?php echo $subs_eves[$e_u]['general_user_id'];?>" onclick="return confirm_subscriber_delete('<?php echo $subs_eves[$e_u]['title']; ?>')"><img style="margin-top: 5px;" src="images/profile/delete.png" /></a></div>
											
											<div class="expandable" id="Accordion_new<?php echo $e_u; ?>_pay4Content" style="margin-left: 80px; top: 17px; height: 115px !important;">
											<div id="actualContent" style="width:400px;">
												<div class="fieldCont">
													<div class="fieldTitle">News Feeds :-</div>
<?php
													
													if($subs_eves[$e_u]['news_feed']=='1')
													{
?>
														<div class="chkCont">
															<input checked  type="radio" class="chk" name="a_feeds_news_<?php echo $subs_eves[$e_u]['id']; ?>"  onClick="on_feeds('<?php echo $subs_eves[$e_u]['id']; ?>')" value="on"/>
															<div class="radioTitle" >On</div>
														</div>
														<div class="chkCont">
															<input type="radio" class="chk" name="a_feeds_news_<?php echo $subs_eves[$e_u]['id']; ?>" onClick="off_feeds('<?php echo $subs_eves[$e_u]['id']; ?>')" value="off"/>
															<div class="radioTitle">Off</div>
														</div>
<?php
													}
													else
													{
?>
														<div class="chkCont">
															<input type="radio" class="chk" name="a_feeds_news_<?php echo $subs_eves[$e_u]['id']; ?>"  onClick="on_feeds('<?php echo $subs_eves[$e_u]['id']; ?>')" value="on"/>
															<div class="radioTitle" >On</div>
														</div>
														<div class="chkCont">
															<input checked type="radio" class="chk"  name="a_feeds_news_<?php echo $subs_eves[$e_u]['id']; ?>" onClick="off_feeds('<?php echo $subs_eves[$e_u]['id']; ?>')" value="off"/>
															<div class="radioTitle">Off</div>
														</div>
<?php
													}
?>
												</div>
												<div class="fieldCont">
													<div class="fieldTitle">Email :-</div>
<?php
													$type = '';
													if($subs_eves[$e_u]['related_type']=='artist' || $subs_eves[$e_u]['related_type']=='artist_project' || $subs_eves[$e_u]['related_type']=='artist_event')
													{
														$type = 'artist';
													}
													elseif($subs_eves[$e_u]['related_type']=='community' || $subs_eves[$e_u]['related_type']=='community_project' || $subs_eves[$e_u]['related_type']=='community_event')
													{
														$type = 'community';
													}
													
													if($subs_eves[$e_u]['email_update']=='1')
													{
?>
														<div class="chkCont">
															<input checked type="radio" class="chk" name="a_emails_<?php echo $subs_eves[$e_u]['id']; ?>"  onClick="on_emails('<?php echo $subs_eves[$e_u]['id']; ?>','<?php echo $_SESSION['login_email']; ?>','<?php echo $subs_eves[$e_u]['general_user_id']; ?>','<?php echo $type; ?>')" value="on"/>
															<div class="radioTitle" >On</div>
														</div>
														<div class="chkCont">
															<input type="radio" class="chk" name="a_emails_<?php echo $subs_eves[$e_u]['id']; ?>" onClick="off_emails('<?php echo $subs_eves[$e_u]['id']; ?>','<?php echo $_SESSION['login_email']; ?>','<?php echo $subs_eves[$e_u]['general_user_id']; ?>','<?php echo $type; ?>')" value="off"/>
															<div class="radioTitle">Off</div>
														</div>
<?php
													}
													else
													{
?>
														<div class="chkCont">
															<input type="radio" class="chk" name="a_emails_<?php echo $subs_eves[$e_u]['id']; ?>"  onClick="on_emails('<?php echo $subs_eves[$e_u]['id']; ?>','<?php echo $_SESSION['login_email']; ?>','<?php echo $subs_eves[$e_u]['general_user_id']; ?>','<?php echo $type; ?>')" value="on"/>
															<div class="radioTitle" >On</div>
														</div>
														<div class="chkCont">
															<input checked type="radio" class="chk" name="a_emails_<?php echo $subs_eves[$e_u]['id']; ?>" onClick="off_emails('<?php echo $subs_eves[$e_u]['id']; ?>','<?php echo $_SESSION['login_email']; ?>','<?php echo $subs_eves[$e_u]['general_user_id']; ?>','<?php echo $type; ?>')" value="off"/>
															<div class="radioTitle">Off</div>
														</div>
<?php
													}
?>
												</div>
											</div>
										</div>
										</div>
							<?php
									}
								}										
								
								/*$get_all_event_subscriber = $sub_listings->get_all_subscriber();
								$get_gen_info = $sub_listings->get_general_info();
								if(mysql_num_rows($get_all_event_subscriber)>0)
								{
									while($get_event = mysql_fetch_assoc($get_all_event_subscriber))
									{
										$get_subscriber_info = $sub_listings->get_subscriber_info($get_event['email']);
										if($get_event['related_type']=="artist_event" || $get_event['related_type']=="community_event")
										{
							?>
										<div class="tableCont">
											<!--<div class="blkD"><img src="general_jcrop/croppedFiles/thumb/<?php// if(isset($run_sub_user_to['image_name']) && $run_sub_user_to['image_name']!="") {echo $run_sub_user_to['image_name']; } else{ echo "Noimage.png"; }?>" /></div>-->
											<div class="blkB"><?php echo $get_event['name']; ?></div>
											<div class="blkE"><?php echo $get_subscriber_info['city'];?></div>
											<div class="blkG" style="width:80px;"><?php if($get_event['related_type']=="artist_event"){echo "Artist";}else if($get_event['related_type']=="community_event"){echo "Community";}?></div>
											<!--<div class="blkF" onclick="runAccordion(<?php //echo $ans_sub_user[$sub_i]['id']; ?>);">
												<div onselectstart="return false;"><img src="images/profile/view.jpg" /></div>
											</div>-->
											<div class="icon"><a href="delete_subscriber.php?id=<?php echo $get_event['id'];?>&email=<?php echo $get_event['email'];?>" onclick="return confirm_subscriber_delete('<?php echo $get_event['name']; ?>')"><img src="images/profile/delete.png" /></a></div>
										</div>
							<?php
										}
									}
								}*/
							?>
                        </div>
                    </div>
					
					</div>
                    
                </div>
				<div class="subTabs" id="Become_a_member" style="display:none;">
		  <h1>Purify Membership</h1><br/>
          <div id="actualContent">
          
          <div class="regTypeCol" id="selected">
              <div class="topText">
                    <ul>
                        <li>Annual ($25/year)</li>
                        <li>Support art around the world and receive a media profile and services.</li>
                    </ul>
              </div>
                    
              <div class="bulletList">
                    <ul>
                        <li>Cloud Media Library</li>
                        <li>Free media downloads</li>
                        <li>Create custom channels</li>
                        <li>Sell Original Media</li>
                    </ul>
              </div>
          </div>
                
          <div class="regTypeCol">
                <div class="topText">
                	<ul>
                		<li>Affiliate($350 +$120/year)</li>
                		<li>Create your own social network. Affiliate members receive a customized multi user website. Coming Soon...</li>
                	</ul>
          		</div>
                
                <div class="bulletList">
                	<ul>
                		<li>Unique domain and emails</li>
                		<li>Sync with social networks</li>
                		<li>Multiple Users (No Limit)</li>
                		<li>Support and Consultation</li>
                	</ul>
                </div>
          </div>
          
			<?php
				$Createmember = new CreatePurifyMember();
				$already_mem = $Createmember->alreadyPurifyMember();
				$get_pay_mode = $Createmember->get_all_paymentmode();
				$get_gen_info_member = $Createmember->general_user();
				$test_mem = 0;
				if(mysql_num_rows($already_mem)>0)
				{
					$test_mem = 1;
				}
			?>
		  <form action="purify_member.php" method="POST" onsubmit="return alreadyMember(<?php echo $test_mem;?>)" id="member_form">
				<div class="fieldCont">
					<div class="fieldTitle">Membership Type</div>
						<select class="dropdown" name="member_ship_type" id="member_ship_type" >
							<option value="select">Select</option>
							<option value="Annual">Annual</option>
							<option value="lifetime" style="background-color:light-grey;" disabled>Affiliate(Coming Soon)</option>
						</select>
				</div>
				<div class="fieldCont">
					<div class="fieldTitle">Country</div>
					 <select name="countrySelectmember" class="dropdown" id="countrySelectmember">
					 <option value="0">Select Country</option>
						<?php include('Country.php'); ?>
						<option value="<?php echo $newres['country_id']; ?>" selected="selected"><?php echo $newres2['country_name']; ?></option>
					</select>
				</div>
				<div class="fieldCont">
					<div class="fieldTitle">State / Province</div>
					 <select name="stateSelectmember" class="dropdown" id="stateSelectmember">
						<?php
						$newsql4=$general_info->getStateName($newres['state_id']);
						while($staterow = mysql_fetch_array($newsql4))
						{
							if($staterow['state_id']==$newres['state_id'])
							{
							?>
								<option value="<?php echo $staterow['state_id']; ?>" selected="selected" ><?php echo $staterow['state_name']; ?></option>
							<?php
							}
						?>
							<option value="<?php echo  $staterow['state_id']; ?>" ><?php echo $staterow['state_name']; ?></option>
						<?php
						}
						?>
						
					</select>
				</div>
				<div class="fieldCont">
					<div class="fieldTitle">City</div>
						<input name="editcitymember" type="text" class="fieldText" id="editcitymember" value="<?php if(isset($get_gen_info_member['city'])) echo $get_gen_info_member['city']; ?>" />
				</div>				
				
				<!--<div class="fieldCont">
					<div class="fieldTitle">Payment Mode</div>
						<select class="dropdown" name="payment_mode" id="payment_mode" onchange ="<?php// if($test_mem == 1){echo "return alreadyMember($test_mem)";}else{echo "get_payment_form()";} ?>" >
							<option value="select">Select</option>
							<option value="pay_pal">Pay pal</option>
							<option value="Credit_card">Credit Card</option>
							<?php
								/*while($pay_mode = mysql_fetch_assoc($get_pay_mode))
								{
									if($pay_mode['first_name'] == "" || $pay_mode['last_name']=="")
									{
										continue;
									}
									else
									{
								?>
									<option value="Credit_card"><?php echo $pay_mode['first_name']." ".$pay_mode['last_name'];?></option>
								<?php
									}
								}*/
							?>
						</select>
						
						<a href="paymentthroughcreditcard.html" id="paypal_through_credit_card" style="display:none;"> cred</a>
				</div>-->
				<div class="fieldCont">
					<div class="fieldTitle"></div>
					<input type="checkbox" name="member_age" id="member_age"/>I am 18 years of age or older
				</div>
				<div class="fieldCont">
					<div class="fieldTitle"></div>
					<!--old lines <input type="button" value=" Submit "  onclick="return alreadyMember(<?php //echo $test_mem;?>)" />
					<input type="button" value=" Submit "  onclick="<?php /*if($test_mem == 1){echo "return alreadyMember($test_mem)";}else{echo "get_payment_form()";}*/ ?>" />-->
					<input style="padding: 7px; font-size: 14px;" type="button" value=" Submit "  onclick="get_payment_form()" />
					
				</div>
			</form>	
		   </div>	
	  </div> 
				<div style="display:none" id="sales_distribution" class="subTabs">
				
				<?php
				$sale_info =$get_sales_info->Get_sale_info();
				if($sale_info['artist_id']!=0 && $sale_info['community_id']!=0)
				{
				?>
                	<h1>Business Information</h1>
					<p>Put the information of your business or sole proprietorship for you to enter into sales contracts as if you wish. If you don't have a business setup than use your personal information. </p>
                    <div id="actualContent">
                    	<form method="POST" action="add_sales.php"> 
							<div class="fieldCont">
							  <div class="fieldTitle">Business Name</div>
							  <input type="text" value="" id="businessname" class="fieldText" name="businessname"/>
							  <div class="hint">Name to enter into contracts as</div>
							</div>
							<div class="fieldCont">
							  <div class="fieldTitle">Business Address</div>
							  <input type="text" value="" id="businessaddress" class="fieldText" name="businessaddress"/>
							  
							</div>
							<div class="fieldCont">
							  <div class="fieldTitle">City</div>
							  <input type="text" value="" id="city" class="fieldText" name="city"/>
							</div>
							<div class="fieldCont">
							  <div class="fieldTitle">State</div>
							  <input type="text" value="" id="state" class="fieldText" name="state"/>
							</div>
							<div class="fieldCont">
							  <div class="fieldTitle">Country</div>
							  <input type="text" value="" id="country" class="fieldText" name="country"/>
							</div>
							<div class="fieldCont">
							  <div class="fieldTitle">Zip</div>
							  <input type="text" value="" id="zip" class="fieldText" name="zip"/>
							</div>
							<div class="fieldCont">
							  <div class="fieldTitle">Business Phone</div>
							  <input type="text" value="" id="businessphone" class="fieldText" name="businessphone"/>
							</div>
							<div class="fieldCont">
							  <div class="fieldTitle">EIN/SSN</div>
							  <input type="text" value="" id="ein/ssn" class="fieldText" name="ein/ssn"/>
							  <div class="hint"><input type="button" value="Change"></div>
							</div>
							<!--<div class="fieldCont">
							  <div class="fieldTitle">Distribution Agreement</div>
							  <div class="status">Agreement Unsigned</div>
							  <div class="hint"><input type="button" value="Sign"></div>
							</div>-->
							
							<div class="fieldCont">
									<h1>Cards</h1>
								</div>
							<div class="fieldCont">
							  <div class="fieldTitle">Credit Card</div>
							  <?php
							  $card_info =$get_sales_info->Get_sale_card_info();
							  $cardcount=0;
							  if($card_info!="" && $card_info!=NULL)
								{
								  while($get_card = mysql_fetch_assoc($card_info))
								  {
										if($cardcount>=1)
										{
										?>
										<div class="fieldTitle"></div>
										<?php
										}
								  ?>
								  
									  <div class="status"><?php echo $get_card['first_name']." ".$get_card['last_name'];?> ending in <?php echo $get_card['expiry_date'];?>
									   <?php
									   $card_id = $card_id.",".$get_card['card_id'];
									   ?>
									  </div>
									  <div class="hint"><a href="delete_credit_card.php?id=<?php echo $get_card['card_id'];?>" onclick="return confirm_creditcard_delete('<?php echo $get_card['first_name']." ".$get_card['last_name'];?>')"><input type="button" value="Remove"></a></div>
								  
								  <?php
									$cardcount++;
								  }
								 }
							  ?>
							  <input type="text" name="cardid" id="cardid" value="<?php echo $card_id;?>" style="display:none;"/>
							</div>
							<div class="fieldCont">
								<div class="hint">
									<a href="#addsalecards"><input title="Add a card to use for your secure purchases on the Purify Art site." type="button" value="Add Card"></a>
								</div>
							</div>
							
								
							<div class="fieldCont">
								<div class="fieldTitle"></div>
								<input type="submit" value="Save" class="register">
							</div>
						</form>
                    </div>
				<?php
				}
				else
				{
				?>
					<h4>This Information Is Only For Artist And Community Users.</h4>
				<?php
				}
				?>
      			</div>	  
				<div class="subTabs" id="addsalecards" style="display:none;" name="addsalecards" >
				<script type="text/javascript">
				function validate_card()
				{
					if(document.getElementById("cardtype").value=="select"){
						alert("Please Select Card Type");
						return false;
					}if(document.getElementById("cardnumber").value==""){
						alert("Please Enter Card Number");
						return false;
					}if(document.getElementById("ExpDD").value==""){
						alert("Please Enter Expiry Date");
						return false;
					}if(document.getElementById("ExpMM").value==""){
						alert("Please Enter Expiry Date");
						return false;
					}if(document.getElementById("ExpYY").value==""){
						alert("Please Enter Expiry Date");
						return false;
					}if(document.getElementById("cvc").value==""){
						alert("Please Enter CVC");
						return false;
					}if(document.getElementById("billfirstname").value==""){
						alert("Please Enter First Name");
						return false;
					}if(document.getElementById("billmiddlename").value==""){
						alert("Please Enter Middle Name");
						return false;
					}if(document.getElementById("billlastname").value==""){
						alert("Please Enter Last Name");
						return false;
					}if(document.getElementById("billcountry").value==""){
						alert("Please Enter Country");
						return false;
					}if(document.getElementById("address").value==""){
						alert("Please Enter Address");
						return false;
					}if(document.getElementById("billcity").value==""){
						alert("Please Enter City");
						return false;
					}if(document.getElementById("billstate").value==""){
						alert("Please Enter State");
						return false;
					}if(document.getElementById("zipcode").value==""){
						alert("Please Enter Zip Code");
						return false;
					}if(document.getElementById("cardphone").value==""){
						alert("Please Enter Zip Phone");
						return false;
					}
				}
				</script>
                	<h1>Add Cards</h1>
					<div id="actualContent">
						<form action="add_sale_card.php" Method="POST" onsubmit="return validate_card()">
							<div class="fieldCont">
								<div class="fieldTitle">*Card Type</div>
								 <select id="cardtype" class="dropdown" name="cardtype">
									<option value="select">Select</option>
									<option value="visa">Visa</option>
									<option value="mastercard">Mastercard</option>
									<option value="discover">Discover</option>
									<option value="americanexpress">American Express</option>
								</select>
							</div>
							<div class="fieldCont">
							  <div class="fieldTitle">*Card Number</div>
							  <input type="text" value="" id="cardnumber" class="fieldText" name="cardnumber"/>
							</div>
							<div class="fieldCont">
							  <div class="fieldTitle">*Exp Date</div>
							  <div class="CChint">DD</div>
							  <input type="text" value="" id="ExpDD" class="CCfield" name="ExpDD"/>
							  <div class="CChint">MM</div>
							  <input type="text" value="" id="ExpMM" class="CCfield" name="ExpMM"/>
							  <div class="CChint">YY</div>
							  <input type="text" value="" id="ExpYY" class="CCfield" name="ExpYY"/>
							</div>
							<div class="fieldCont">
							  <div class="fieldTitle">*CVC</div>
							  <input type="text" value="" id="cvc" class="CCfield" name="cvc"/>
							</div>
							<!--<div class="fieldCont">
							  <div class="fieldTitle">*Name on Account</div>
							  <input type="text" value="" id="nameonaccount" class="fieldText" name="nameonaccount"/>
							</div>-->
							<div class="fieldCont">
								<h3>Cards Billing Information</h3>
								<p style="color: #000000;font-size: 14px;line-height: 15px;border-top:none;">Enter same information as the account of the card you are adding.</p>
							</div>
							<div class="fieldCont">
							  <div class="fieldTitle">*First Name</div>
							  <input type="text" value="" id="billfirstname" class="fieldText" name="billfirstname"/>
							</div>
							<div class="fieldCont">
							  <div class="fieldTitle">Middle Name</div>
							  <input type="text" value="" id="billmiddlename" class="fieldText" name="billmiddlename"/>
							</div>
							<div class="fieldCont">
							  <div class="fieldTitle">*Last Name</div>
							  <input type="text" value="" id="billlastname" class="fieldText" name="billlastname"/>
							</div>
							<div class="fieldCont">
							  <div class="fieldTitle">*Country</div>
							  <input type="text" value="" id="billcountry" class="fieldText" name="billcountry"/>
							</div>
							<div class="fieldCont">
							  <div class="fieldTitle">*Address</div>
							  <input type="text" value="" id="address" class="fieldText" name="address"/>
							</div>
							<div class="fieldCont">
							  <div class="fieldTitle">*City</div>
							  <input type="text" value="" id="billcity" class="fieldText" name="billcity"/>
							</div>
							<div class="fieldCont">
							  <div class="fieldTitle">*State</div>
							  <input type="text" value="" id="billstate" class="fieldText" name="billstate"/>
							</div>
							<div class="fieldCont">
							  <div class="fieldTitle">*Zip Code</div>
							  <input type="text" value="" id="zipcode" class="fieldText" name="zipcode"/>
							</div>
							
							<div class="fieldCont">
							  <div class="fieldTitle">*Phone</div>
							  <input type="text" value="" id="cardphone" class="fieldText" name="cardphone"/>
							</div>
							<div class="fieldCont">
								<div class="fieldTitle"></div>
								<input type="submit" class="register" value="Save">
							</div>
						</form>
					</div>
				</div>	
				
				<div id="newsfeeds" class="subTabs" style="display:none;">
					<h1>News Post</h1>
					<div id="actualContent">
						<form method="POST" action="insertpost.php"  id="news_form">
							<div class="fieldCont">
								<div class="fieldTitle" title="Select a topic to post about.">Topic</div>
								<select class="dropdown" title="Select a topic to post about." id="post_list" name="post_list">
									<option value="images_post">Images</option>
									<option value="songs_post">Songs</option>
									<option value="videos_post">Videos</option>
									<option value="events_post">Events</option>
									<option value="projects_post">Projects</option>
								</select>
							</div>
							
							<div id="loader" style="display:none;">
								<img src="images/ajax_loader_large.gif" style="height: 25px; position: relative; right: 285px; top: 38px; width: 25px;">
							</div>
							
							<div class="fieldCont" id="fieldCont_media">
							</div>
							
							<div class="fieldCont">
								<textarea class="jquery_ckeditor" id="description_post" name="description_post"></textarea>
							</div>
							
							<div class="fieldContainer">
								<div class="rightSide">
									<a href="#newsfeeds" onmouseover="mopen('m_post')" onmouseout="mclosetime()"><input id="allow_to" name="allow_to" type="button" value="All" style="background: url('images/arrow_down.png') no-repeat scroll 32px 10px black; padding: 8px 15px;color:white;border:none;font-family: futura-pt,sans-serif;font-size: 0.825em;font-weight: bold;" /></a>
									<div style="visibility:hidden; border: 1px solid #777777; width: 150px; text-decoration:none; height:56px;" id="m_post" class="popUp" onmouseover="mcancelclosetime()" onmouseout="mclosetime()">
										<ul style="list-style: none outside none;">
											<li><a id="1o_post" style="cursor:pointer;" onclick="allowpost('1o_post')">Fans</a></li>
											<li><a id="2o_post" style="cursor:pointer;" onclick="allowpost('2o_post')">Friends</a></li>
											<li><a id="3o_post" style="cursor:pointer;" onclick="allowpost('3o_post')">Subscribers</a></li>
										</ul>
									</div>
								</div>
							</div>		
							
							<div class="fieldCont">
								<div class="fieldTitle"></div>
								<input type="hidden" id="allowh_post" name="allowh_post" value="All" />
								<input type="submit" value="Post" class="register" style="position:relative;float:right;left:112px;"/>
							</div>
						</form>
                    </div>
					
					<?php

			?>
                	<h1 style="padding: 0 0 6px 0 !important;">News Feed</h1>
                    <div id="mediaContent">
                        <div class="topLinks">
                            <div class="links" style="width:665px;">
                                <select class="eventdrop" onChange="handleSelection(value)">
                                	<option value="all_news_feeds_p">All News</option>
									<option value="photo_feeds_p">Photo Feed</option>
                                    <option value="music_feeds_p">Music Feed</option>
									<option value="video_feeds_p">Video Feed</option>
                                    <option value="events_feeds_p">Event Feed</option>
                                    <option value="project_feeds_p">Project Feed</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div id="actualContent">
		<?php
					$chk_sql = $sql_feeds->get_log_info($_SESSION['login_email']);
								
					if($chk_sql!="")
					{
		?>
                    	<div id="all_news_feeds_p">
							<?php
								$get_loged = $sql_feeds->myposts($_SESSION['login_id']);
								if($get_loged!="" && $get_loged!=NULL)
								{
										if(mysql_num_rows($get_loged)>0)
										{
											while($mydata = mysql_fetch_assoc($get_loged))
											{
												$gens = $sql_feeds->get_log_ids($mydata['general_user_id']);
									?>
									
									<div class="storyCont">
										<div class="leftSide">
											<div class="avatar">
												<img src="<?php echo $generalproimageURI.$gens['image_name']; ?>" />
										</div>
										<?php //echo "table=".$reg_table_media_type;
										//echo $ans_audio_feeds[$audio_fed_lo]['id'];
										?>
											<div class="storyContainer">
												<div class="storyTitle">
													<span class="userName"><a href=""><?php echo $gens['fname'].' '.$gens['lname']; ?></a></span> <?php echo $mydata['description']; ?>
												</div>
												<?php
												if($ans_all_posts[$po_all]['profile_type']!="" && $ans_all_posts[$po_all]['media']!="")
												{
												?>
												<div class="activityCont">
													<div class="sideL">
														<div class="storyImage"><!--<img src="images/profile/leerickfeed.jpg" />--></div>
														
														<div class="player">
															<img src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'">
															<img src="images/profile/add.gif" onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'">
															<!--<img src="images/profile/download.gif" onmouseover="this.src='images/profile/download-over.gif'" onmouseout="this.src='images/profile/download.gif'">--><img style="cursor: default;" src="images/profile/download-over.gif">
														</div>
														
													</div>
													<div class="sideR">
														<div class="activityTitle"><a href=""><?php
														if($exp_meds[1]=='reg_med' || $ans_all_posts[$po_all]['profile_type']=='projects_post' || $ans_all_posts[$po_all]['profile_type']=='events_post')
														{
															echo $sql_meds['title'];
														}
														elseif($exp_meds[1]=='reg_art' || $exp_meds[1]=='reg_com')
														{
															if($ans_all_posts[$po_all]['profile_type']=='images_post')
															{
																echo $sql_meds['gallery_title'];
															}
															elseif($ans_all_posts[$po_all]['profile_type']=='songs_post')
															{
																echo $sql_meds['audio_name'];
															}
															elseif($ans_all_posts[$po_all]['profile_type']=='videos_post')
															{
																echo $sql_meds['video_name'];
															}
														}
														?></a></div>
														<!--<div class="activityContent">Tagged: <?php //echo $medias['tagged_users']; 
															//$name_org = explode(')',$medias['tagged_users']);
															//for($i_name_org=0;$i_name_org<count($name_org);$i_name_org++)
															//{
															//	$end_pos = strpos($name_org[$i_name_org],'(');
															//	$org_name = substr($name_org[$i_name_org],0,$end_pos);
														//		echo $org_name;
														//	}
														?></div>-->
													</div>
												</div>
												<?php
												}
												?>
											</div>
										</div>
										<div class="rightSide">
											<a href="#" onmouseover="mopen('mm<?php echo $po_all;?>1x')" onmouseout="mclosetime()"><img src="images/profile/arrow.png" onmouseover="this.src='images/profile/arrow-down.png'" onmouseout="this.src='images/profile/arrow.png'" /></a>
											<div id="mm<?php echo $po_all;?>1x" class="popUp" onmouseover="mcancelclosetime()" onmouseout="mclosetime()">
												<ul>
													<li><a href="hideposts.php?id=<?php echo $ans_all_posts[$po_all]['id'];?>">Hide Feed</a></li>
													<li><a href="hideposts.php?report_id=<?php echo $ans_all_posts[$po_all]['id'];?>">Report spam</a></li>
													<li><div class="seperator"></div></li>
													<li><a href="">Unsubscribe from <?php echo $gens['fname'].' '.$gens['lname']; ?></a></li>
												</ul>
											</div>
										</div>
									</div>
		<?php	
									}
								}
							}
								$ans_all_feeds = array();
								$ans_sub_feeds = array();
								$ans_fan_club = array();
								$pro_eve_id = array();
								$art_com_id = array();
								$get_posts = array();
								$arts_ids = array();
								$coms_ids = array();
								$ans_all_posts = array();
								
									$sql_all_feeds = $sql_feeds->all_feeds($chk_sql['email']);
									//echo $sql_all_feeds;
									$run_all_feeds = mysql_query($sql_all_feeds);
									
									$sql_fan_feeds = $sql_feeds->fan_club($chk_sql['email']);
									$run_fan_feeds = mysql_query($sql_fan_feeds);
									
									if($run_all_feeds!="" && $run_all_feeds!=NULL)
									{
										if(mysql_num_rows($run_all_feeds)>0)
										{
											while($row_all_feeds = mysql_fetch_assoc($run_all_feeds))
											{
												$ans_sub_feeds[] = $row_all_feeds;
											}
										}
									}
									
									if($run_fan_feeds!="" && $run_fan_feeds!=NULL)
									{
										if(mysql_num_rows($run_fan_feeds)>0)
										{
											while($row_fan_feeds = mysql_fetch_assoc($run_fan_feeds))
											{
												$ans_fan_club[] = $row_fan_feeds;
											}
										}
									}
									
									$ans_all_feeds = array_merge($ans_sub_feeds,$ans_fan_club);
									if($ans_all_feeds!=NULL)
									{
										for($all_fed_lo=0;$all_fed_lo<count($ans_all_feeds);$all_fed_lo++)
										{
											if($ans_all_feeds[$all_fed_lo]['related_type']!='artist' && $ans_all_feeds[$all_fed_lo]['related_type']!='community')
											{
												$num_ids = explode('_',$ans_all_feeds[$all_fed_lo]['related_id']);
												if($ans_all_feeds[$all_fed_lo]['related_type']=='artist_project' || $ans_all_feeds[$all_fed_lo]['related_type']=='artist_event')
												{
													$pro_eve_id[$all_fed_lo]['related_id_art'] = $num_ids[0];
												}
												elseif($ans_all_feeds[$all_fed_lo]['related_type']=='community_project' || $ans_all_feeds[$all_fed_lo]['related_type']=='community_event')
												{
													$art_com_id[$all_fed_lo]['related_id_com'] = $num_ids[0];
												}
											}
											elseif($ans_all_feeds[$all_fed_lo]['related_type']=='artist' || $ans_all_feeds[$all_fed_lo]['related_type']=='community')
											{
												if($ans_all_feeds[$all_fed_lo]['related_type']=='artist')
												{
													$pro_eve_id[$all_fed_lo]['related_id_art'] = $ans_all_feeds[$all_fed_lo]['related_id'];
												}
												elseif($ans_all_feeds[$all_fed_lo]['related_type']=='community')
												{
													$art_com_id[$all_fed_lo]['related_id_com'] = $ans_all_feeds[$all_fed_lo]['related_id'];
												}
											}
										}
										
										if($pro_eve_id!=0 && count($pro_eve_id)>0)
										{
											for($arts=0;$arts<count($pro_eve_id);$arts++)
											{
												$org_art[] = $pro_eve_id[$arts]['related_id_art'];
											}
											
											$new_art = array();
											$is = 0;
											foreach ($org_art as $key => $value)
											{
												if(isset($new_art[$value]))
												{
													$new_art[$value] += 1;
												}
												else
												{
													$new_art[$value] = 1;
													
													$sqls = $sql_feeds->arts_gen($value);
													
													if($sqls!="")
													{
														$arts_ids[$is] = $sqls;
													}
												}
												$is = $is + 1;
											}
										}
										
										if($art_com_id!="" && count($art_com_id)>0)
										{
											for($coms=0;$coms<count($art_com_id);$coms++)
											{
												$org_com[] = $art_com_id[$coms]['related_id_com'];
											}
											
											$new_com = array();
											$ie_c = 0;
											foreach ($org_com as $key => $value)
											{
												if(isset($new_com[$value]))
												{
													$new_com[$value] += 1;
												}
												else
												{
													$new_com[$value] = 1;
													
													$sqls_c = $sql_feeds->coms_gen($value);
													if($sqls_c!="")
													{
														$coms_ids[$ie_c] = $sqls_c;
													}
												}
												$ie_c = $ie_c + 1;
											}
										}
										
										$doub_chk = array_merge($arts_ids,$coms_ids);
										
										for($ch_d=0;$ch_d<count($doub_chk);$ch_d++)
										{
											$rel_d_id[] = $doub_chk[$ch_d]['general_user_id'];
										}
										
										$new_array_re = array();
										foreach ($rel_d_id as $key => $value)
										{
											if(isset($new_array_re[$value]))
												$new_array_re[$value] += 1;
											else
												$new_array_re[$value] = 1;
										}
										foreach ($new_array_re as $uid => $n)
										{
											$ex_uid1 = $ex_uid1.','.$uid;
										}
										$ex_tr = trim($ex_uid1, ",");
										$sep_ids = explode(',',$ex_tr);
										
										if($sep_ids!="" && count($sep_ids)>0)
										{
											for($se_ca=0;$se_ca<count($sep_ids);$se_ca++)
											{
												$ids = $sql_feeds->all_posts_check($sep_ids[$se_ca],$_SESSION['login_id']);
												if($ids!="" && count($ids)>0)
												{
													$posts_all = $sql_feeds->all_posts($sep_ids[$se_ca],$_SESSION['login_id']);
													if($posts_all!="" && $posts_all!=NULL)
													{
														if($posts_all!="" && mysql_num_rows($posts_all)>0)
														{
															while($rows_posts_all = mysql_fetch_assoc($posts_all))
															{
																$ans_all_posts[] = $rows_posts_all;
															}
														}
													}
												}
											}
										}
										
										if($ans_all_posts!="" && count($ans_all_posts)>0)
										{
											for($po_all=0;$po_all<count($ans_all_posts);$po_all++)
											{
												if($ans_all_posts[$po_all]['profile_type']=='songs_post' || $ans_all_posts[$po_all]['profile_type']=='videos_post' || $ans_all_posts[$po_all]['profile_type']=='images_post')
												{
													$exp_meds = explode('~',$ans_all_posts[$po_all]['media']);
													if($exp_meds[1]=='reg_med')
													{
														$sql_meds = $sql_feeds->get_mediaSelected($exp_meds[0]);
													}
													elseif(($exp_meds[1]=='reg_art' || $exp_meds[1]=='reg_com') && $ans_all_posts[$po_all]['profile_type']=='images_post')
													{
														if($exp_meds[1]=='reg_art')
														{
															$reg_table_type = 'general_artist_gallery_list';
														}
														elseif($exp_meds[1]=='reg_com')
														{
															$reg_table_type = 'general_community_gallery_list';
														}
														
														$sql_meds = $sql_feeds->get_galleryData($reg_table_type,$exp_meds[0]);
													}
													elseif(($exp_meds[1]=='reg_art' || $exp_meds[1]=='reg_com') && $ans_all_posts[$po_all]['profile_type']=='videos_post')
													{
														if($exp_meds[1]=='reg_art')
														{
															$reg_table_type = 'general_artist_video';
														}
														elseif($exp_meds[1]=='reg_com')
														{
															$reg_table_type = 'general_community_video';
														}
														
														$sql_meds = $sql_feeds->get_MediaReg($reg_table_type,$exp_meds[0]);
													}
													elseif(($exp_meds[1]=='reg_art' || $exp_meds[1]=='reg_com') && $ans_all_posts[$po_all]['profile_type']=='songs_post')
													{
														if($exp_meds[1]=='reg_art')
														{
															$reg_table_type = 'general_artist_audio';
														}
														elseif($exp_meds[1]=='reg_com')
														{
															$reg_table_type = 'general_community_audio';
														}
														
														$sql_meds = $sql_feeds->get_MediaReg($reg_table_type,$exp_meds[0]);
													}
												}
												elseif($ans_all_posts[$po_all]['profile_type']=='projects_post' || $ans_all_posts[$po_all]['profile_type']=='events_post')
												{
													$exp_meds = explode('~',$ans_all_posts[$po_all]['media']);
													$sql_meds = $sql_feeds->featured_pro_eve($exp_meds[1],$exp_meds[0]);
												}
												
												if(isset($sql_meds) && $sql_meds!='0' && $sql_meds!=NULL)
												{
													$gens = $sql_feeds->get_log_ids($ans_all_posts[$po_all]['general_user_id']);
													if($ans_all_posts[$po_all]['display_to']=='All')
													{
			?>
														<div class="storyCont">
																	<div class="leftSide">
																		<div class="avatar">
																			<img src="<?php echo $generalproimageURI.$gens['image_name']; ?>" />
																	</div>
																	<?php //echo "table=".$reg_table_media_type;
																	//echo $ans_audio_feeds[$audio_fed_lo]['id'];
																	?>
																		<div class="storyContainer">
																			<div class="storyTitle">
																				<span class="userName"><a href=""><?php echo $gens['fname'].' '.$gens['lname']; ?></a></span> <?php echo $ans_all_posts[$po_all]['description']; ?>
																			</div>
																			<div class="activityCont">
																				<div class="sideL">
																					<div class="storyImage"><!--<img src="images/profile/leerickfeed.jpg" />--></div>
																					<div class="player">
																						<img src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'">
																						<img src="images/profile/add.gif" onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'">
																						<!--<img src="images/profile/download.gif" onmouseover="this.src='images/profile/download-over.gif'" onmouseout="this.src='images/profile/download.gif'">--><img style="cursor: default;" src="images/profile/download-over.gif">
																					</div>
																				</div>
																				<div class="sideR">
																					<div class="activityTitle"><a href=""><?php
																					if($exp_meds[1]=='reg_med' || $ans_all_posts[$po_all]['profile_type']=='projects_post' || $ans_all_posts[$po_all]['profile_type']=='events_post')
																					{
																						echo $sql_meds['title'];
																					}
																					elseif($exp_meds[1]=='reg_art' || $exp_meds[1]=='reg_com')
																					{
																						if($ans_all_posts[$po_all]['profile_type']=='images_post')
																						{
																							echo $sql_meds['gallery_title'];
																						}
																						elseif($ans_all_posts[$po_all]['profile_type']=='songs_post')
																						{
																							echo $sql_meds['audio_name'];
																						}
																						elseif($ans_all_posts[$po_all]['profile_type']=='videos_post')
																						{
																							echo $sql_meds['video_name'];
																						}
																					}
																					?></a></div>
																					<!--<div class="activityContent">Tagged: <?php //echo $medias['tagged_users']; 
																						//$name_org = explode(')',$medias['tagged_users']);
																						//for($i_name_org=0;$i_name_org<count($name_org);$i_name_org++)
																						//{
																						//	$end_pos = strpos($name_org[$i_name_org],'(');
																						//	$org_name = substr($name_org[$i_name_org],0,$end_pos);
																					//		echo $org_name;
																					//	}
																					?></div>-->
																				</div>
																			</div>
																		</div>
																	</div>
																	<div class="rightSide">
																		<a href="#" onmouseover="mopen('mms<?php echo $po_all;?>1')" onmouseout="mclosetime()"><img src="images/profile/arrow.png" onmouseover="this.src='images/profile/arrow-down.png'" onmouseout="this.src='images/profile/arrow.png'" /></a>
																		<div id="mms<?php echo $po_all;?>1" class="popUp" onmouseover="mcancelclosetime()" onmouseout="mclosetime()">
																			<ul>
																				<li><a href="">Hide Feed</a></li>
																				<li><a href="">Report spam</a></li>
																				<li><div class="seperator"></div></li>
																				<li><a href="">Unsubscribe from <?php echo $gens['fname'].' '.$gens['lname']; ?></a></li>
																			</ul>
																		</div>
																	</div>
																</div>
			<?php
													}
													elseif($ans_all_posts[$po_all]['display_to']=='Fans')
													{
														if($gens!="")
														{
															$fans_all =	$sql_feeds->fans_cond($chk_sql['email'],$gens['community_id'],$gens['artist_id']);
															if($fans_all=='1')
															{
			?>
																<div class="storyCont">
																	<div class="leftSide">
																		<div class="avatar">
																			<img src="<?php echo $generalproimageURI.$gens['image_name']; ?>" />
																	</div>
																	<?php //echo "table=".$reg_table_media_type;
																	//echo $ans_audio_feeds[$audio_fed_lo]['id'];
																	?>
																		<div class="storyContainer">
																			<div class="storyTitle">
																				<span class="userName"><a href=""><?php echo $gens['fname'].' '.$gens['lname']; ?></a></span>  <?php echo $ans_all_posts[$po_all]['description']; ?>
																			</div>
																			<div class="activityCont">
																				<div class="sideL">
																					<div class="storyImage"><!--<img src="images/profile/leerickfeed.jpg" />--></div>
																					<div class="player">
																						<img src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'">
																						<img src="images/profile/add.gif" onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'">
																						<!--<img src="images/profile/download.gif" onmouseover="this.src='images/profile/download-over.gif'" onmouseout="this.src='images/profile/download.gif'">--><img style="cursor: default;" src="images/profile/download-over.gif">
																					</div>
																				</div>
																				<div class="sideR">
																					<div class="activityTitle"><a href=""><?php
																					if($exp_meds[1]=='reg_med' || $ans_all_posts[$po_all]['profile_type']=='projects_post' || $ans_all_posts[$po_all]['profile_type']=='events_post')
																					{
																						echo $sql_meds['title'];
																					}
																					elseif($exp_meds[1]=='reg_art' || $exp_meds[1]=='reg_com')
																					{
																						if($ans_all_posts[$po_all]['profile_type']=='images_post')
																						{
																							echo $sql_meds['gallery_title'];
																						}
																						elseif($ans_all_posts[$po_all]['profile_type']=='songs_post')
																						{
																							echo $sql_meds['audio_name'];
																						}
																						elseif($ans_all_posts[$po_all]['profile_type']=='videos_post')
																						{
																							echo $sql_meds['video_name'];
																						}
																					}?></a></div>
																					<!--<div class="activityContent">Tagged: <?php //echo $medias['tagged_users']; 
																						//$name_org = explode(')',$medias['tagged_users']);
																						//for($i_name_org=0;$i_name_org<count($name_org);$i_name_org++)
																						//{
																						//	$end_pos = strpos($name_org[$i_name_org],'(');
																						//	$org_name = substr($name_org[$i_name_org],0,$end_pos);
																					//		echo $org_name;
																					//	}
																					?></div>-->
																				</div>
																			</div>
																		</div>
																	</div>
																	<div class="rightSide">
																		<a href="#" onmouseover="mopen('mms<?php echo $po_all;?>2')" onmouseout="mclosetime()"><img src="images/profile/arrow.png" onmouseover="this.src='images/profile/arrow-down.png'" onmouseout="this.src='images/profile/arrow.png'" /></a>
																		<div id="mms<?php echo $po_all;?>2" class="popUp" onmouseover="mcancelclosetime()" onmouseout="mclosetime()">
																			<ul>
																				<li><a href="">Hide Feed</a></li>
																				<li><a href="">Report spam</a></li>
																				<li><div class="seperator"></div></li>
																				<li><a href="">Unsubscribe from <?php echo $gens['fname'].' '.$gens['lname']; ?></a></li>
																			</ul>
																		</div>
																	</div>
																</div>
			<?php
															}
														}
													}
													elseif($ans_all_posts[$po_all]['display_to']=='Subscribers')
													{
														if($gens!="")
														{
															$subscribers_all = $sql_feeds->subscribers_cond($chk_sql['email'],$gens['community_id'],$gens['artist_id']);
															echo $subscribers_all;
															if($subscribers_all==1)
															{
			?>
																<div class="storyCont">
																	<div class="leftSide">
																		<div class="avatar">
																			<img src="<?php echo $generalproimageURI.$gens['image_name']; ?>" />
																	</div>
																	<?php //echo "table=".$reg_table_media_type;
																	//echo $ans_audio_feeds[$audio_fed_lo]['id'];
																	?>
																		<div class="storyContainer">
																			<div class="storyTitle">
																				<span class="userName"><a href=""><?php //echo $gens['fname'].' '.$gens['lname']; ?></a></span>  <?php echo $ans_all_posts[$po_all]['description']."subscript"; ?>
																			</div>
																			<div class="activityCont">
																				<div class="sideL">
																					<div class="storyImage"><!--<img src="images/profile/leerickfeed.jpg" />--></div>
																					<div class="player">
																						<img src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'">
																						<img src="images/profile/add.gif" onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'">
																						<!--<img src="images/profile/download.gif" onmouseover="this.src='images/profile/download-over.gif'" onmouseout="this.src='images/profile/download.gif'">--><img style="cursor: default;" src="images/profile/download-over.gif">
																					</div>
																				</div>
																				<div class="sideR">
																					<div class="activityTitle"><a href=""><?php
																					if($exp_meds[1]=='reg_med' || $ans_all_posts[$po_all]['profile_type']=='projects_post' || $ans_all_posts[$po_all]['profile_type']=='events_post')
																					{
																						echo $sql_meds['title'];
																					}
																					elseif($exp_meds[1]=='reg_art' || $exp_meds[1]=='reg_com')
																					{
																						if($ans_all_posts[$po_all]['profile_type']=='images_post')
																						{
																							echo $sql_meds['gallery_title'];
																						}
																						elseif($ans_all_posts[$po_all]['profile_type']=='songs_post')
																						{
																							echo $sql_meds['audio_name'];
																						}
																						elseif($ans_all_posts[$po_all]['profile_type']=='videos_post')
																						{
																							echo $sql_meds['video_name'];
																						}
																					}
																					?></a></div>
																					<!--<div class="activityContent">Tagged: <?php //echo $medias['tagged_users']; 
																						//$name_org = explode(')',$medias['tagged_users']);
																						//for($i_name_org=0;$i_name_org<count($name_org);$i_name_org++)
																						//{
																						//	$end_pos = strpos($name_org[$i_name_org],'(');
																						//	$org_name = substr($name_org[$i_name_org],0,$end_pos);
																					//		echo $org_name;
																					//	}
																					?></div>-->
																				</div>
																			</div>
																		</div>
																	</div>
																	<div class="rightSide">
																		<a href="#" onmouseover="mopen('mms<?php echo $po_all;?>3')" onmouseout="mclosetime()"><img src="images/profile/arrow.png" onmouseover="this.src='images/profile/arrow-down.png'" onmouseout="this.src='images/profile/arrow.png'" /></a>
																		<div id="mms<?php echo $po_all;?>3" class="popUp" onmouseover="mcancelclosetime()" onmouseout="mclosetime()">
																			<ul>
																				<li><a href="">Hide Feed</a></li>
																				<li><a href="">Report spam</a></li>
																				<li><div class="seperator"></div></li>
																				<li><a href="">Unsubscribe from <?php echo $gens['fname'].' '.$gens['lname']; ?></a></li>
																			</ul>
																		</div>
																	</div>
																</div>
			<?php
															}
														}
													}
													elseif($ans_all_posts[$po_all]['display_to']=='Friends')
													{
														if($gens!="")
														{
															$friends_c = $sql_feeds->friends_cond($ans_all_posts[$po_all]['general_user_id'],$_SESSION['login_id']);
															
															if($friends_c=='1')
															{
			?>
																<div class="storyCont">
																	<div class="leftSide">
																		<div class="avatar">
																			<img src="<?php echo $generalproimageURI.$gens['image_name']; ?>" />
																	</div>
																	<?php //echo "table=".$reg_table_media_type;
																	//echo $ans_audio_feeds[$audio_fed_lo]['id'];
																	?>
																		<div class="storyContainer">
																			<div class="storyTitle">
																				<span class="userName"><a href=""><?php echo $gens['fname'].' '.$gens['lname']; ?></a></span>  <?php echo $ans_all_posts[$po_all]['description']; ?>
																			</div>
																			<div class="activityCont">
																				<div class="sideL">
																					<div class="storyImage"><!--<img src="images/profile/leerickfeed.jpg" />--></div>
																					<div class="player">
																						<img src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'">
																						<img src="images/profile/add.gif" onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'">
																						<!--<img src="images/profile/download.gif" onmouseover="this.src='images/profile/download-over.gif'" onmouseout="this.src='images/profile/download.gif'">--><img style="cursor: default;" src="images/profile/download-over.gif">
																					</div>
																				</div>
																				<div class="sideR">
																					<div class="activityTitle"><a href=""><?php
																					if($exp_meds[1]=='reg_med' || $ans_all_posts[$po_all]['profile_type']=='projects_post' || $ans_all_posts[$po_all]['profile_type']=='events_post')
																					{
																						echo $sql_meds['title'];
																					}
																					elseif($exp_meds[1]=='reg_art' || $exp_meds[1]=='reg_com')
																					{
																						if($ans_all_posts[$po_all]['profile_type']=='images_post')
																						{
																							echo $sql_meds['gallery_title'];
																						}
																						elseif($ans_all_posts[$po_all]['profile_type']=='songs_post')
																						{
																							echo $sql_meds['audio_name'];
																						}
																						elseif($ans_all_posts[$po_all]['profile_type']=='videos_post')
																						{
																							echo $sql_meds['video_name'];
																						}
																					}
																					?></a></div>
																					<!--<div class="activityContent">Tagged: <?php //echo $medias['tagged_users']; 
																						//$name_org = explode(')',$medias['tagged_users']);
																						//for($i_name_org=0;$i_name_org<count($name_org);$i_name_org++)
																						//{
																						//	$end_pos = strpos($name_org[$i_name_org],'(');
																						//	$org_name = substr($name_org[$i_name_org],0,$end_pos);
																					//		echo $org_name;
																					//	}
																					?></div>-->
																				</div>
																			</div>
																		</div>
																	</div>
																	<div class="rightSide">
																		<a href="#" onmouseover="mopen('mms<?php echo $po_all;?>4')" onmouseout="mclosetime()"><img src="images/profile/arrow.png" onmouseover="this.src='images/profile/arrow-down.png'" onmouseout="this.src='images/profile/arrow.png'" /></a>
																		<div id="mms<?php echo $po_all;?>4" class="popUp" onmouseover="mcancelclosetime()" onmouseout="mclosetime()">
																			<ul>
																				<li><a href="">Hide Feed</a></li>
																				<li><a href="">Report spam</a></li>
																				<li><div class="seperator"></div></li>
																				<li><a href="">Unsubscribe from <?php echo $gens['fname'].' '.$gens['lname']; ?></a></li>
																			</ul>
																		</div>
																	</div>
																</div>
			<?php
															}
														}
													}
												}
											}
										}
									}
		?>
							</div>
							<div id="photo_feeds_p" style="display:none;">
		<?php
									$ans_photo_feeds = array();
									$ans_photo_sub_feeds = array();
									$ans_fan_club_photo = array();
									$ph_pro_eve_id = array();
									$ph_art_com_id = array();
									$ph_arts_ids = array();
									$ph_coms_ids = array();
									$ans_photo_posts = array();
									
									$sql_photo_feeds = $sql_feeds->all_feeds($chk_sql['email']);
									$run_photo_feeds = mysql_query($sql_photo_feeds);
									
									$fan_sql_photo_feeds = $sql_feeds->fan_club($chk_sql['email']);
									$fan_run_photo_feeds = mysql_query($fan_sql_photo_feeds);
									
									if($run_photo_feeds!="" && $run_photo_feeds!=NULL)
									{
										while($row_photo_feeds = mysql_fetch_assoc($run_photo_feeds))
										{
											$ans_photo_sub_feeds[] = $row_photo_feeds;
										}
									}
									
									if($fan_run_photo_feeds!="" && $fan_run_photo_feeds!=NULL)
									{
										while($fan_row_photo_feeds = mysql_fetch_assoc($fan_run_photo_feeds))
										{
											$ans_fan_club_photo[] = $fan_row_photo_feeds;
										}
									}
									
									$ans_photo_feeds = array_merge($ans_photo_sub_feeds,$ans_fan_club_photo);
									if($ans_photo_feeds!=NULL)
									{
										for($ph_fed_lo=0;$ph_fed_lo<count($ans_photo_feeds);$ph_fed_lo++)
										{
											if($ans_photo_feeds[$ph_fed_lo]['related_type']!='artist' && $ans_photo_feeds[$ph_fed_lo]['related_type']!='community')
											{
												$num_ids = explode('_',$ans_photo_feeds[$ph_fed_lo]['related_id']);
												if($ans_photo_feeds[$ph_fed_lo]['related_type']=='artist_project' || $ans_photo_feeds[$ph_fed_lo]['related_type']=='artist_event')
												{
													$ph_pro_eve_id[$ph_fed_lo]['related_id_art'] = $num_ids[0];
												}
												elseif($ans_photo_feeds[$ph_fed_lo]['related_type']=='community_project' || $ans_photo_feeds[$ph_fed_lo]['related_type']=='community_event')
												{
													$ph_art_com_id[$ph_fed_lo]['related_id_com'] = $num_ids[0];
												}
											}
											elseif($ans_photo_feeds[$ph_fed_lo]['related_type']=='artist' || $ans_photo_feeds[$ph_fed_lo]['related_type']=='community')
											{
												if($ans_photo_feeds[$ph_fed_lo]['related_type']=='artist')
												{
													$ph_pro_eve_id[$ph_fed_lo]['related_id_art'] = $ans_photo_feeds[$ph_fed_lo]['related_id'];
												}
												elseif($ans_photo_feeds[$ph_fed_lo]['related_type']=='community')
												{
													$ph_art_com_id[$ph_fed_lo]['related_id_com'] = $ans_photo_feeds[$ph_fed_lo]['related_id'];
												}
											}
										}
										
										if($ph_pro_eve_id!=0 && count($ph_pro_eve_id)>0)
										{
											for($arts=0;$arts<count($ph_pro_eve_id);$arts++)
											{
												$org_art[] = $ph_pro_eve_id[$arts]['related_id_art'];
											}
											
											$new_art = array();
											$pho = 0;
											foreach ($org_art as $key => $value)
											{
												if(isset($new_art[$value]))
												{
													$new_art[$value] += 1;
												}
												else
												{
													$new_art[$value] = 1;
													
													$sqls = $sql_feeds->arts_gen($value);
													
													if($sqls!="")
													{
														$ph_arts_ids[$pho] = $sqls;
													}
												}
												$pho = $pho + 1;
											}
										}
										
										if($ph_art_com_id!="" && count($ph_art_com_id)>0)
										{
											for($coms=0;$coms<count($ph_art_com_id);$coms++)
											{
												$org_com[] = $ph_art_com_id[$coms]['related_id_com'];
											}
											
											$new_com = array();
											$iph_c = 0;
											foreach ($org_com as $key => $value)
											{
												if(isset($new_com[$value]))
												{
													$new_com[$value] += 1;
												}
												else
												{
													$new_com[$value] = 1;
													
													$sqls_c = $sql_feeds->coms_gen($value);
													if($sqls_c!="")
													{
														$ph_coms_ids[$iph_c] = $sqls_c;
													}
												}
												$iph_c = $iph_c + 1;
											}
										}
										
										$doub_ph_chk = array_merge($ph_arts_ids,$ph_coms_ids);
										
										for($chp_d=0;$chp_d<count($doub_ph_chk);$chp_d++)
										{
											$rel_d_id[] = $doub_ph_chk[$chp_d]['general_user_id'];
										}
										
										$new_array_rep_p = array();
										foreach ($rel_d_id as $key => $value)
										{
											if(isset($new_array_rep_p[$value]))
												$new_array_rep_p[$value] += 1;
											else
												$new_array_rep_p[$value] = 1;
										}
										foreach ($new_array_rep_p as $uid => $n)
										{
											$ex_uid_ph = $ex_uid_ph.','.$uid;
										}
										$ex_tr_p = trim($ex_uid_ph, ",");
										$sep_ids_p = explode(',',$ex_tr_p);
										
										if($sep_ids_p!="" && count($sep_ids_p)>0)
										{
											for($se_ca=0;$se_ca<count($sep_ids_p);$se_ca++)
											{
												$types = 'images_post';
												$ids = $sql_feeds->pmva_posts_check($sep_ids_p[$se_ca],$_SESSION['login_id'],$types);
												if($ids!="" && count($ids)>0)
												{
													$posts_photo = $sql_feeds->pmv_posts_check($sep_ids_p[$se_ca],$_SESSION['login_id'],$types);
													if($posts_photo!="" && $posts_photo!=NULL)
													{
														if($posts_photo!="" && mysql_num_rows($posts_photo)>0)
														{
															while($rows_posts_all = mysql_fetch_assoc($posts_photo))
															{
																$ans_photo_posts[] = $rows_posts_all;
															}
														}
													}
												}
											}
										}
										
										if($ans_photo_posts!="" && count($ans_photo_posts)>0)
										{
											for($hpo_all=0;$hpo_all<count($ans_photo_posts);$hpo_all++)
											{
												if($ans_photo_posts[$hpo_all]['profile_type']=='images_post')
												{
													$exp_meds = explode('~',$ans_photo_posts[$hpo_all]['media']);
													if($exp_meds[1]=='reg_med')
													{
														$sql_meds_ph = $sql_feeds->get_mediaSelected($exp_meds[0]);
													}
													elseif(($exp_meds[1]=='reg_art' || $exp_meds[1]=='reg_com') && $ans_photo_posts[$hpo_all]['profile_type']=='images_post')
													{
														if($exp_meds[1]=='reg_art')
														{
															$reg_table_type = 'general_artist_gallery_list';
														}
														elseif($exp_meds[1]=='reg_com')
														{
															$reg_table_type = 'general_community_gallery_list';
														}
														
														$sql_meds_ph = $sql_feeds->get_galleryData($reg_table_type,$exp_meds[0]);
													}
												}
												
												if(isset($sql_meds_ph) && $sql_meds_ph!='0' && $sql_meds_ph!=NULL)
												{
													$gens = $sql_feeds->get_log_ids($ans_photo_posts[$hpo_all]['general_user_id']);
													if($ans_photo_posts[$hpo_all]['display_to']=='All' && $ans_photo_posts[$hpo_all]['profile_type']=='images_post')
													{
			?>
														<div class="storyCont">
																	<div class="leftSide">
																		<div class="avatar">
																			<img src="<?php echo $generalproimageURI.$gens['image_name']; ?>" />
																	</div>
																	<?php //echo "table=".$reg_table_media_type;
																	//echo $ans_audio_feeds[$audio_fed_lo]['id'];
																	?>
																		<div class="storyContainer">
																			<div class="storyTitle">
																				<span class="userName"><a href=""><?php echo $gens['fname'].' '.$gens['lname']; ?></a></span> <?php echo $ans_photo_posts[$hpo_all]['description']; ?>
																			</div>
																			<div class="activityCont">
																				<div class="sideL">
																					<div class="storyImage"><!--<img src="images/profile/leerickfeed.jpg" />--></div>
																					<div class="player">
																						<img src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'">
																						<img src="images/profile/add.gif" onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'">
																						<!--<img src="images/profile/download.gif" onmouseover="this.src='images/profile/download-over.gif'" onmouseout="this.src='images/profile/download.gif'">--><img style="cursor: default;" src="images/profile/download-over.gif">
																					</div>
																				</div>
																				<div class="sideR">
																					<div class="activityTitle"><a href=""><?php
																					if($exp_meds[1]=='reg_med')
																					{
																						echo $sql_meds_ph['title'];
																					}
																					elseif($exp_meds[1]=='reg_art' || $exp_meds[1]=='reg_com')
																					{
																						if($ans_photo_posts[$hpo_all]['profile_type']=='images_post')
																						{
																							echo $sql_meds_ph['gallery_title'];
																						}
																					}
																					?></a></div>
																					<!--<div class="activityContent">Tagged: <?php //echo $medias['tagged_users']; 
																						//$name_org = explode(')',$medias['tagged_users']);
																						//for($i_name_org=0;$i_name_org<count($name_org);$i_name_org++)
																						//{
																						//	$end_pos = strpos($name_org[$i_name_org],'(');
																						//	$org_name = substr($name_org[$i_name_org],0,$end_pos);
																					//		echo $org_name;
																					//	}
																					?></div>-->
																				</div>
																			</div>
																		</div>
																	</div>
																	<div class="rightSide">
																		<a href="#" onmouseover="mopen('mms<?php echo $hpo_all;?>5')" onmouseout="mclosetime()"><img src="images/profile/arrow.png" onmouseover="this.src='images/profile/arrow-down.png'" onmouseout="this.src='images/profile/arrow.png'" /></a>
																		<div id="mms<?php echo $hpo_all;?>5" class="popUp" onmouseover="mcancelclosetime()" onmouseout="mclosetime()">
																			<ul>
																				<li><a href="">Hide Feed</a></li>
																				<li><a href="">Report spam</a></li>
																				<li><div class="seperator"></div></li>
																				<li><a href="">Unsubscribe from <?php echo $gens['fname'].' '.$gens['lname']; ?></a></li>
																			</ul>
																		</div>
																	</div>
																</div>
			<?php
													}
													elseif($ans_photo_posts[$hpo_all]['display_to']=='Fans')
													{
														if($gens!="")
														{
															$fans_all =	$sql_feeds->fans_cond($chk_sql['email'],$gens['community_id'],$gens['artist_id']);
															if($fans_all=='1')
															{
			?>
																<div class="storyCont">
																	<div class="leftSide">
																		<div class="avatar">
																			<img src="<?php echo $generalproimageURI.$gens['image_name']; ?>" />
																	</div>
																	<?php //echo "table=".$reg_table_media_type;
																	//echo $ans_audio_feeds[$audio_fed_lo]['id'];
																	?>
																		<div class="storyContainer">
																			<div class="storyTitle">
																				<span class="userName"><a href=""><?php echo $gens['fname'].' '.$gens['lname']; ?></a></span>  <?php echo $ans_photo_posts[$hpo_all]['description']; ?>
																			</div>
																			<div class="activityCont">
																				<div class="sideL">
																					<div class="storyImage"><!--<img src="images/profile/leerickfeed.jpg" />--></div>
																					<div class="player">
																						<img src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'">
																						<img src="images/profile/add.gif" onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'">
																						<!--<img src="images/profile/download.gif" onmouseover="this.src='images/profile/download-over.gif'" onmouseout="this.src='images/profile/download.gif'">--><img style="cursor: default;" src="images/profile/download-over.gif">
																					</div>
																				</div>
																				<div class="sideR">
																					<div class="activityTitle"><a href=""><?php
																					if($exp_meds[1]=='reg_med')
																					{
																						echo $sql_meds_ph['title'];
																					}
																					elseif($exp_meds[1]=='reg_art' || $exp_meds[1]=='reg_com')
																					{
																						if($ans_photo_posts[$hpo_all]['profile_type']=='images_post')
																						{
																							echo $sql_meds_ph['gallery_title'];
																						}
																						
																					}?></a></div>
																					<!--<div class="activityContent">Tagged: <?php //echo $medias['tagged_users']; 
																						//$name_org = explode(')',$medias['tagged_users']);
																						//for($i_name_org=0;$i_name_org<count($name_org);$i_name_org++)
																						//{
																						//	$end_pos = strpos($name_org[$i_name_org],'(');
																						//	$org_name = substr($name_org[$i_name_org],0,$end_pos);
																					//		echo $org_name;
																					//	}
																					?></div>-->
																				</div>
																			</div>
																		</div>
																	</div>
																	<div class="rightSide">
																		<a href="#" onmouseover="mopen('mms<?php echo $hpo_all;?>6')" onmouseout="mclosetime()"><img src="images/profile/arrow.png" onmouseover="this.src='images/profile/arrow-down.png'" onmouseout="this.src='images/profile/arrow.png'" /></a>
																		<div id="mms<?php echo $hpo_all;?>6" class="popUp" onmouseover="mcancelclosetime()" onmouseout="mclosetime()">
																			<ul>
																				<li><a href="">Hide Feed</a></li>
																				<li><a href="">Report spam</a></li>
																				<li><div class="seperator"></div></li>
																				<li><a href="">Unsubscribe from <?php echo $gens['fname'].' '.$gens['lname']; ?></a></li>
																			</ul>
																		</div>
																	</div>
																</div>
			<?php
															}
														}
													}
													elseif($ans_photo_posts[$hpo_all]['display_to']=='Subscribers')
													{
														if($gens!="")
														{
															$subscribers_all =	$sql_feeds->subscribers_cond($chk_sql['email'],$gens['community_id'],$gens['artist_id']);
															if($subscribers_all=='1')
															{
			?>
																<div class="storyCont">
																	<div class="leftSide">
																		<div class="avatar">
																			<img src="<?php echo $generalproimageURI.$gens['image_name']; ?>" />
																	</div>
																	<?php //echo "table=".$reg_table_media_type;
																	//echo $ans_audio_feeds[$audio_fed_lo]['id'];
																	?>
																		<div class="storyContainer">
																			<div class="storyTitle">
																				<span class="userName"><a href=""><?php echo $gens['fname'].' '.$gens['lname']; ?></a></span>  <?php echo $ans_photo_posts[$hpo_all]['description']; ?>
																			</div>
																			<div class="activityCont">
																				<div class="sideL">
																					<div class="storyImage"><!--<img src="images/profile/leerickfeed.jpg" />--></div>
																					<div class="player">
																						<img src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'">
																						<img src="images/profile/add.gif" onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'">
																						<!--<img src="images/profile/download.gif" onmouseover="this.src='images/profile/download-over.gif'" onmouseout="this.src='images/profile/download.gif'">--><img style="cursor: default;" src="images/profile/download-over.gif">
																					</div>
																				</div>
																				<div class="sideR">
																					<div class="activityTitle"><a href=""><?php
																					if($exp_meds[1]=='reg_med')
																					{
																						echo $sql_meds_ph['title'];
																					}
																					elseif($exp_meds[1]=='reg_art' || $exp_meds[1]=='reg_com')
																					{
																						if($ans_photo_posts[$hpo_all]['profile_type']=='images_post')
																						{
																							echo $sql_meds_ph['gallery_title'];
																						}
																					}
																					?></a></div>
																					<!--<div class="activityContent">Tagged: <?php //echo $medias['tagged_users']; 
																						//$name_org = explode(')',$medias['tagged_users']);
																						//for($i_name_org=0;$i_name_org<count($name_org);$i_name_org++)
																						//{
																						//	$end_pos = strpos($name_org[$i_name_org],'(');
																						//	$org_name = substr($name_org[$i_name_org],0,$end_pos);
																					//		echo $org_name;
																					//	}
																					?></div>-->
																				</div>
																			</div>
																		</div>
																	</div>
																	<div class="rightSide">
																		<a href="#" onmouseover="mopen('mms<?php echo $hpo_all;?>7')" onmouseout="mclosetime()"><img src="images/profile/arrow.png" onmouseover="this.src='images/profile/arrow-down.png'" onmouseout="this.src='images/profile/arrow.png'" /></a>
																		<div id="mms<?php echo $hpo_all;?>7" class="popUp" onmouseover="mcancelclosetime()" onmouseout="mclosetime()">
																			<ul>
																				<li><a href="">Hide Feed</a></li>
																				<li><a href="">Report spam</a></li>
																				<li><div class="seperator"></div></li>
																				<li><a href="">Unsubscribe from <?php echo $gens['fname'].' '.$gens['lname']; ?></a></li>
																			</ul>
																		</div>
																	</div>
																</div>
			<?php
															}
														}
													}
													elseif($ans_photo_posts[$hpo_all]['display_to']=='Friends')
													{
														if($gens!="")
														{
															$friends_c = $sql_feeds->friends_cond($ans_photo_posts[$hpo_all]['general_user_id'],$_SESSION['login_id']);
															
															if($friends_c=='1')
															{
			?>
																<div class="storyCont">
																	<div class="leftSide">
																		<div class="avatar">
																			<img src="<?php echo $generalproimageURI.$gens['image_name']; ?>" />
																	</div>
																	<?php //echo "table=".$reg_table_media_type;
																	//echo $ans_audio_feeds[$audio_fed_lo]['id'];
																	?>
																		<div class="storyContainer">
																			<div class="storyTitle">
																				<span class="userName"><a href=""><?php echo $gens['fname'].' '.$gens['lname']; ?></a></span>  <?php echo $ans_photo_posts[$hpo_all]['description']; ?>
																			</div>
																			<div class="activityCont">
																				<div class="sideL">
																					<div class="storyImage"><!--<img src="images/profile/leerickfeed.jpg" />--></div>
																					<div class="player">
																						<img src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'">
																						<img src="images/profile/add.gif" onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'">
																						<!--<img src="images/profile/download.gif" onmouseover="this.src='images/profile/download-over.gif'" onmouseout="this.src='images/profile/download.gif'">--><img style="cursor: default;" src="images/profile/download-over.gif">
																					</div>
																				</div>
																				<div class="sideR">
																					<div class="activityTitle"><a href=""><?php
																					if($exp_meds[1]=='reg_med')
																					{
																						echo $sql_meds_ph['title'];
																					}
																					elseif($exp_meds[1]=='reg_art' || $exp_meds[1]=='reg_com')
																					{
																						if($ans_photo_posts[$hpo_all]['profile_type']=='images_post')
																						{
																							echo $sql_meds_ph['gallery_title'];
																						}
																					}
																					?></a></div>
																					<!--<div class="activityContent">Tagged: <?php //echo $medias['tagged_users']; 
																						//$name_org = explode(')',$medias['tagged_users']);
																						//for($i_name_org=0;$i_name_org<count($name_org);$i_name_org++)
																						//{
																						//	$end_pos = strpos($name_org[$i_name_org],'(');
																						//	$org_name = substr($name_org[$i_name_org],0,$end_pos);
																					//		echo $org_name;
																					//	}
																					?></div>-->
																				</div>
																			</div>
																		</div>
																	</div>
																	<div class="rightSide">
																		<a href="#" onmouseover="mopen('mms<?php echo $hpo_all;?>8')" onmouseout="mclosetime()"><img src="images/profile/arrow.png" onmouseover="this.src='images/profile/arrow-down.png'" onmouseout="this.src='images/profile/arrow.png'" /></a>
																		<div id="mms<?php echo $hpo_all;?>8" class="popUp" onmouseover="mcancelclosetime()" onmouseout="mclosetime()">
																			<ul>
																				<li><a href="">Hide Feed</a></li>
																				<li><a href="">Report spam</a></li>
																				<li><div class="seperator"></div></li>
																				<li><a href="">Unsubscribe from <?php echo $gens['fname'].' '.$gens['lname']; ?></a></li>
																			</ul>
																		</div>
																	</div>
																</div>
			<?php
															}
														}
													}
												}
											}
										}
									}
							?>
							</div>
							
							
							<div id="music_feeds_p" style="display:none;">
							<?php
									$ans_audio_feeds = array();
									$ans_audio_sub_feeds = array();
									$ans_audio_fan_club_feeds = array();
									$mu_art_com_id = array();
									$mu_pro_eve_id = array();
									$mu_arts_ids = array();
									$mu_coms_ids = array();
									$ans_music_posts = array();
									
									
									$sql_audio_feeds = $sql_feeds->all_feeds($chk_sql['email']);
									$run_audio_feeds = mysql_query($sql_audio_feeds);
									
									$fan_sql_audio_feeds = $sql_feeds->fan_club($chk_sql['email']);
									$fan_run_audio_feeds = mysql_query($fan_sql_audio_feeds);
									
									if($run_audio_feeds!="" && $run_audio_feeds!=NULL)
									{
										while($row_audio_feeds = mysql_fetch_assoc($run_audio_feeds))
										{
											$ans_audio_sub_feeds[] = $row_audio_feeds;
										}
									}
									
									if($fan_run_audio_feeds!="" && $fan_run_audio_feeds!=NULL)
									{
										while($fan_row_audio_feeds = mysql_fetch_assoc($fan_run_audio_feeds))
										{
											$ans_audio_fan_club_feeds[] = $fan_row_audio_feeds;
										}
									}
									
									$ans_audio_feeds = array_merge($ans_audio_sub_feeds,$ans_audio_fan_club_feeds);
									
									if($ans_audio_feeds!=NULL)
									{
										for($mu_fed_lo=0;$mu_fed_lo<count($ans_audio_feeds);$mu_fed_lo++)
										{
											if($ans_audio_feeds[$mu_fed_lo]['related_type']!='artist' && $ans_audio_feeds[$mu_fed_lo]['related_type']!='community')
											{
												$num_ids = explode('_',$ans_audio_feeds[$mu_fed_lo]['related_id']);
												if($ans_audio_feeds[$mu_fed_lo]['related_type']=='artist_project' || $ans_audio_feeds[$mu_fed_lo]['related_type']=='artist_event')
												{
													$mu_pro_eve_id[$mu_fed_lo]['related_id_art'] = $num_ids[0];
												}
												elseif($ans_audio_feeds[$mu_fed_lo]['related_type']=='community_project' || $ans_audio_feeds[$mu_fed_lo]['related_type']=='community_event')
												{
													$mu_art_com_id[$mu_fed_lo]['related_id_com'] = $num_ids[0];
												}
											}
											elseif($ans_audio_feeds[$mu_fed_lo]['related_type']=='artist' || $ans_audio_feeds[$mu_fed_lo]['related_type']=='community')
											{
												if($ans_audio_feeds[$mu_fed_lo]['related_type']=='artist')
												{
													$mu_pro_eve_id[$mu_fed_lo]['related_id_art'] = $ans_audio_feeds[$mu_fed_lo]['related_id'];
												}
												elseif($ans_audio_feeds[$mu_fed_lo]['related_type']=='community')
												{
													$mu_art_com_id[$mu_fed_lo]['related_id_com'] = $ans_audio_feeds[$mu_fed_lo]['related_id'];
												}
											}
										}
										
										if($mu_pro_eve_id!=0 && count($mu_pro_eve_id)>0)
										{
											for($arts=0;$arts<count($mu_pro_eve_id);$arts++)
											{
												$org_art[] = $mu_pro_eve_id[$arts]['related_id_art'];
											}
											
											$new_art_mu = array();
											$mu = 0;
											foreach ($org_art as $key => $value)
											{
												if(isset($new_art_mu[$value]))
												{
													$new_art_mu[$value] += 1;
												}
												else
												{
													$new_art_mu[$value] = 1;
													
													$sqls = $sql_feeds->arts_gen($value);
													
													if($sqls!="")
													{
														$mu_arts_ids[$mu] = $sqls;
													}
												}
												$mu = $mu + 1;
											}
										}
										
										if($mu_art_com_id!="" && count($mu_art_com_id)>0)
										{
											for($coms=0;$coms<count($mu_art_com_id);$coms++)
											{
												$org_com[] = $mu_art_com_id[$coms]['related_id_com'];
											}
											
											$new_com_mu = array();
											$muh_c = 0;
											foreach ($org_com as $key => $value)
											{
												if(isset($new_com_mu[$value]))
												{
													$new_com_mu[$value] += 1;
												}
												else
												{
													$new_com_mu[$value] = 1;
													
													$sqls_c = $sql_feeds->coms_gen($value);
													if($sqls_c!="")
													{
														$mu_coms_ids[$muh_c] = $sqls_c;
													}
												}
												$muh_c = $muh_c + 1;
											}
										}
										
										$doub_mu_chk = array_merge($mu_arts_ids,$mu_coms_ids);
										
										for($chm_d=0;$chm_d<count($doub_mu_chk);$chm_d++)
										{
											$rel_d_id[] = $doub_mu_chk[$chm_d]['general_user_id'];
										}
										
										$new_array_rep_mu = array();
										foreach ($rel_d_id as $key => $value)
										{
											if(isset($new_array_rep_mu[$value]))
												$new_array_rep_mu[$value] += 1;
											else
												$new_array_rep_mu[$value] = 1;
										}
										
										foreach ($new_array_rep_mu as $uid => $n)
										{
											$ex_uid_mu = $ex_uid_mu.','.$uid;
										}
										$ex_tr_mu = trim($ex_uid_mu, ",");
										$sep_ids_mp = explode(',',$ex_tr_mu);
										
										if($sep_ids_mp!="" && count($sep_ids_mp)>0)
										{
											for($se_ca=0;$se_ca<count($sep_ids_mp);$se_ca++)
											{
												$types = 'songs_post';
												$ids = $sql_feeds->pmva_posts_check($sep_ids_mp[$se_ca],$_SESSION['login_id'],$types);
												
												//die;
												if($ids!="" && count($ids)>0)
												{
													$posts_music = $sql_feeds->pmv_posts_check($sep_ids_mp[$se_ca],$_SESSION['login_id'],$types);
													if($posts_music!="" && $posts_music!=NULL)
													{
														if($posts_music!="" && mysql_num_rows($posts_music)>0)
														{
															while($rows_posts_all = mysql_fetch_assoc($posts_music))
															{
																$ans_music_posts[] = $rows_posts_all;
															}
														}
													}
												}
											}
										}
										
										if($ans_music_posts!="" && count($ans_music_posts)>0)
										{
											for($mpu_all=0;$mpu_all<count($ans_music_posts);$mpu_all++)
											{
												if($ans_music_posts[$mpu_all]['profile_type']=='songs_post')
												{
													$exp_meds = explode('~',$ans_music_posts[$mpu_all]['media']);
													if($exp_meds[1]=='reg_med')
													{
														$sep_ids_m = $sql_feeds->get_mediaSelected($exp_meds[0]);
													}
													elseif(($exp_meds[1]=='reg_art' || $exp_meds[1]=='reg_com') && $ans_music_posts[$mpu_all]['profile_type']=='songs_post')
													{
														if($exp_meds[1]=='reg_art')
														{
															$reg_table_type = 'general_artist_audio';
														}
														elseif($exp_meds[1]=='reg_com')
														{
															$reg_table_type = 'general_community_audio';
														}
														
														$sep_ids_m = $sql_feeds->get_MediaReg($reg_table_type,$exp_meds[0]);
													}
												}
												
												if(isset($sep_ids_m) && $sep_ids_m!='0' && $sep_ids_m!=NULL)
												{
													$gens = $sql_feeds->get_log_ids($ans_music_posts[$mpu_all]['general_user_id']);
													if($ans_music_posts[$mpu_all]['display_to']=='All' && $ans_music_posts[$mpu_all]['profile_type']=='songs_post')
													{
			?>
														<div class="storyCont">
																	<div class="leftSide">
																		<div class="avatar">
																			<img src="<?php echo $generalproimageURI.$gens['image_name']; ?>" />
																	</div>
																	<?php //echo "table=".$reg_table_media_type;
																	//echo $ans_audio_feeds[$audio_fed_lo]['id'];
																	?>
																		<div class="storyContainer">
																			<div class="storyTitle">
																				<span class="userName"><a href=""><?php echo $gens['fname'].' '.$gens['lname']; ?></a></span> <?php echo $ans_music_posts[$mpu_all]['description']; ?>
																			</div>
																			<div class="activityCont">
																				<div class="sideL">
																					<div class="storyImage"><!--<img src="images/profile/leerickfeed.jpg" />--></div>
																					<div class="player">
																						<img src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'">
																						<img src="images/profile/add.gif" onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'">
																						<!--<img src="images/profile/download.gif" onmouseover="this.src='images/profile/download-over.gif'" onmouseout="this.src='images/profile/download.gif'">--><img style="cursor: default;" src="images/profile/download-over.gif">
																					</div>
																				</div>
																				<div class="sideR">
																					<div class="activityTitle"><a href=""><?php
																					if($exp_meds[1]=='reg_med')
																					{
																						echo $sep_ids_m['title'];
																					}
																					elseif($exp_meds[1]=='reg_art' || $exp_meds[1]=='reg_com')
																					{
																						if($ans_music_posts[$mpu_all]['profile_type']=='songs_post')
																						{
																							echo $sep_ids_m['audio_name'];
																						}
																					}
																					?></a></div>
																					<!--<div class="activityContent">Tagged: <?php //echo $medias['tagged_users']; 
																						//$name_org = explode(')',$medias['tagged_users']);
																						//for($i_name_org=0;$i_name_org<count($name_org);$i_name_org++)
																						//{
																						//	$end_pos = strpos($name_org[$i_name_org],'(');
																						//	$org_name = substr($name_org[$i_name_org],0,$end_pos);
																					//		echo $org_name;
																					//	}
																					?></div>-->
																				</div>
																			</div>
																		</div>
																	</div>
																	<div class="rightSide">
																		<a href="#" onmouseover="mopen('mms<?php echo $mpu_all;?>9')" onmouseout="mclosetime()"><img src="images/profile/arrow.png" onmouseover="this.src='images/profile/arrow-down.png'" onmouseout="this.src='images/profile/arrow.png'" /></a>
																		<div id="mms<?php echo $mpu_all;?>9" class="popUp" onmouseover="mcancelclosetime()" onmouseout="mclosetime()">
																			<ul>
																				<li><a href="">Hide Feed</a></li>
																				<li><a href="">Report spam</a></li>
																				<li><div class="seperator"></div></li>
																				<li><a href="">Unsubscribe from <?php echo $gens['fname'].' '.$gens['lname']; ?></a></li>
																			</ul>
																		</div>
																	</div>
																</div>
			<?php
													}
													elseif($ans_music_posts[$mpu_all]['display_to']=='Fans')
													{
														if($gens!="")
														{
															$fans_all =	$sql_feeds->fans_cond($chk_sql['email'],$gens['community_id'],$gens['artist_id']);
															if($fans_all=='1')
															{
			?>
																<div class="storyCont">
																	<div class="leftSide">
																		<div class="avatar">
																			<img src="<?php echo $generalproimageURI.$gens['image_name']; ?>" />
																	</div>
																	<?php //echo "table=".$reg_table_media_type;
																	//echo $ans_audio_feeds[$audio_fed_lo]['id'];
																	?>
																		<div class="storyContainer">
																			<div class="storyTitle">
																				<span class="userName"><a href=""><?php echo $gens['fname'].' '.$gens['lname']; ?></a></span>  <?php echo $ans_music_posts[$mpu_all]['description']; ?>
																			</div>
																			<div class="activityCont">
																				<div class="sideL">
																					<div class="storyImage"><!--<img src="images/profile/leerickfeed.jpg" />--></div>
																					<div class="player">
																						<img src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'">
																						<img src="images/profile/add.gif" onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'">
																						<!--<img src="images/profile/download.gif" onmouseover="this.src='images/profile/download-over.gif'" onmouseout="this.src='images/profile/download.gif'">--><img style="cursor: default;" src="images/profile/download-over.gif">
																					</div>
																				</div>
																				<div class="sideR">
																					<div class="activityTitle"><a href=""><?php
																					if($exp_meds[1]=='reg_med')
																					{
																						echo $sep_ids_m['title'];
																					}
																					elseif($exp_meds[1]=='reg_art' || $exp_meds[1]=='reg_com')
																					{
																						if($ans_music_posts[$mpu_all]['profile_type']=='songs_post')
																						{
																							echo $sep_ids_m['audio_name'];
																						}
																						
																					}?></a></div>
																					<!--<div class="activityContent">Tagged: <?php //echo $medias['tagged_users']; 
																						//$name_org = explode(')',$medias['tagged_users']);
																						//for($i_name_org=0;$i_name_org<count($name_org);$i_name_org++)
																						//{
																						//	$end_pos = strpos($name_org[$i_name_org],'(');
																						//	$org_name = substr($name_org[$i_name_org],0,$end_pos);
																					//		echo $org_name;
																					//	}
																					?></div>-->
																				</div>
																			</div>
																		</div>
																	</div>
																	<div class="rightSide">
																		<a href="#" onmouseover="mopen('mms<?php echo $mpu_all;?>10')" onmouseout="mclosetime()"><img src="images/profile/arrow.png" onmouseover="this.src='images/profile/arrow-down.png'" onmouseout="this.src='images/profile/arrow.png'" /></a>
																		<div id="mms<?php echo $mpu_all;?>10" class="popUp" onmouseover="mcancelclosetime()" onmouseout="mclosetime()">
																			<ul>
																				<li><a href="">Hide Feed</a></li>
																				<li><a href="">Report spam</a></li>
																				<li><div class="seperator"></div></li>
																				<li><a href="">Unsubscribe from <?php echo $gens['fname'].' '.$gens['lname']; ?></a></li>
																			</ul>
																		</div>
																	</div>
																</div>
			<?php
															}
														}
													}
													elseif($ans_music_posts[$mpu_all]['display_to']=='Subscribers')
													{
														if($gens!="")
														{
															$subscribers_all =	$sql_feeds->subscribers_cond($chk_sql['email'],$gens['community_id'],$gens['artist_id']);
															if($subscribers_all=='1')
															{
			?>
																<div class="storyCont">
																	<div class="leftSide">
																		<div class="avatar">
																			<img src="<?php echo $generalproimageURI.$gens['image_name']; ?>" />
																	</div>
																	<?php //echo "table=".$reg_table_media_type;
																	//echo $ans_audio_feeds[$audio_fed_lo]['id'];
																	?>
																		<div class="storyContainer">
																			<div class="storyTitle">
																				<span class="userName"><a href=""><?php echo $gens['fname'].' '.$gens['lname']; ?></a></span>  <?php echo $ans_music_posts[$mpu_all]['description']; ?>
																			</div>
																			<div class="activityCont">
																				<div class="sideL">
																					<div class="storyImage"><!--<img src="images/profile/leerickfeed.jpg" />--></div>
																					<div class="player">
																						<img src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'">
																						<img src="images/profile/add.gif" onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'">
																						<!--<img src="images/profile/download.gif" onmouseover="this.src='images/profile/download-over.gif'" onmouseout="this.src='images/profile/download.gif'">--><img style="cursor: default;" src="images/profile/download-over.gif">
																					</div>
																				</div>
																				<div class="sideR">
																					<div class="activityTitle"><a href=""><?php
																					if($exp_meds[1]=='reg_med')
																					{
																						echo $sep_ids_m['title'];
																					}
																					elseif($exp_meds[1]=='reg_art' || $exp_meds[1]=='reg_com')
																					{
																						if($ans_music_posts[$mpu_all]['profile_type']=='songs_post')
																						{
																							echo $sep_ids_m['audio_name'];
																						}
																					}
																					?></a></div>
																					<!--<div class="activityContent">Tagged: <?php //echo $medias['tagged_users']; 
																						//$name_org = explode(')',$medias['tagged_users']);
																						//for($i_name_org=0;$i_name_org<count($name_org);$i_name_org++)
																						//{
																						//	$end_pos = strpos($name_org[$i_name_org],'(');
																						//	$org_name = substr($name_org[$i_name_org],0,$end_pos);
																					//		echo $org_name;
																					//	}
																					?></div>-->
																				</div>
																			</div>
																		</div>
																	</div>
																	<div class="rightSide">
																		<a href="#" onmouseover="mopen('mms<?php echo $mpu_all;?>11')" onmouseout="mclosetime()"><img src="images/profile/arrow.png" onmouseover="this.src='images/profile/arrow-down.png'" onmouseout="this.src='images/profile/arrow.png'" /></a>
																		<div id="mms<?php echo $mpu_all;?>11" class="popUp" onmouseover="mcancelclosetime()" onmouseout="mclosetime()">
																			<ul>
																				<li><a href="">Hide Feed</a></li>
																				<li><a href="">Report spam</a></li>
																				<li><div class="seperator"></div></li>
																				<li><a href="">Unsubscribe from <?php echo $gens['fname'].' '.$gens['lname']; ?></a></li>
																			</ul>
																		</div>
																	</div>
																</div>
			<?php
															}
														}
													}
													elseif($ans_music_posts[$mpu_all]['display_to']=='Friends')
													{
														if($gens!="")
														{
															$friends_c = $sql_feeds->friends_cond($ans_music_posts[$mpu_all]['general_user_id'],$_SESSION['login_id']);
															
															if($friends_c=='1')
															{
			?>
																<div class="storyCont">
																	<div class="leftSide">
																		<div class="avatar">
																			<img src="<?php echo $generalproimageURI.$gens['image_name']; ?>" />
																	</div>
																	<?php //echo "table=".$reg_table_media_type;
																	//echo $ans_audio_feeds[$audio_fed_lo]['id'];
																	?>
																		<div class="storyContainer">
																			<div class="storyTitle">
																				<span class="userName"><a href=""><?php echo $gens['fname'].' '.$gens['lname']; ?></a></span>  <?php echo $ans_music_posts[$mpu_all]['description']; ?>
																			</div>
																			<div class="activityCont">
																				<div class="sideL">
																					<div class="storyImage"><!--<img src="images/profile/leerickfeed.jpg" />--></div>
																					<div class="player">
																						<img src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'">
																						<img src="images/profile/add.gif" onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'">
																						<!--<img src="images/profile/download.gif" onmouseover="this.src='images/profile/download-over.gif'" onmouseout="this.src='images/profile/download.gif'">--><img style="cursor: default;" src="images/profile/download-over.gif">
																					</div>
																				</div>
																				<div class="sideR">
																					<div class="activityTitle"><a href=""><?php
																					if($exp_meds[1]=='reg_med')
																					{
																						echo $sep_ids_m['title'];
																					}
																					elseif($exp_meds[1]=='reg_art' || $exp_meds[1]=='reg_com')
																					{
																						if($ans_music_posts[$mpu_all]['profile_type']=='songs_post')
																						{
																							echo $sep_ids_m['audio_name'];
																						}
																					}
																					?></a></div>
																					<!--<div class="activityContent">Tagged: <?php //echo $medias['tagged_users']; 
																						//$name_org = explode(')',$medias['tagged_users']);
																						//for($i_name_org=0;$i_name_org<count($name_org);$i_name_org++)
																						//{
																						//	$end_pos = strpos($name_org[$i_name_org],'(');
																						//	$org_name = substr($name_org[$i_name_org],0,$end_pos);
																					//		echo $org_name;
																					//	}
																					?></div>-->
																				</div>
																			</div>
																		</div>
																	</div>
																	<div class="rightSide">
																		<a href="#" onmouseover="mopen('mms<?php echo $mpu_all;?>12')" onmouseout="mclosetime()"><img src="images/profile/arrow.png" onmouseover="this.src='images/profile/arrow-down.png'" onmouseout="this.src='images/profile/arrow.png'" /></a>
																		<div id="mms<?php echo $mpu_all;?>12" class="popUp" onmouseover="mcancelclosetime()" onmouseout="mclosetime()">
																			<ul>
																				<li><a href="">Hide Feed</a></li>
																				<li><a href="">Report spam</a></li>
																				<li><div class="seperator"></div></li>
																				<li><a href="">Unsubscribe from <?php echo $gens['fname'].' '.$gens['lname']; ?></a></li>
																			</ul>
																		</div>
																	</div>
																</div>
			<?php
															}
														}
													}
												}
											}
										}
									}
							?>
							</div>
							<div id="video_feeds_p" style="display:none;">
							<?php
									$ans_video_feeds = array();
									$ans_sub_video_feeds = array();
									$fan_ans_video_feeds = array();
									$vi_pro_eve_id = array();
									$vi_art_com_id = array();
									$vi_arts_ids = array();
									$vi_coms_ids = array();
									$ans_videos_posts = array();
									
									$sql_video_feeds = $sql_feeds->all_feeds($chk_sql['email']);
									$run_video_feeds = mysql_query($sql_video_feeds);
									
									$fan_sql_video_feeds = $sql_feeds->fan_club($chk_sql['email']);
									$fan_run_video_feeds = mysql_query($fan_sql_video_feeds);
									
									if($run_video_feeds!="" && $run_video_feeds!=NULL)
									{
										while($row_video_feeds = mysql_fetch_assoc($run_video_feeds))
										{
											$ans_sub_video_feeds[] = $row_video_feeds;
										}
									}
									
									if($fan_run_video_feeds!="" && $fan_run_video_feeds!=NULL)
									{
										while($fan_row_video_feeds = mysql_fetch_assoc($fan_run_video_feeds))
										{
											$fan_ans_video_feeds[] = $fan_row_video_feeds;
										}
									}
									
									$ans_video_feeds = array_merge($ans_sub_video_feeds,$fan_ans_video_feeds);
									
									if($ans_video_feeds!=NULL)
									{
										for($vi_fed_lo=0;$vi_fed_lo<count($ans_video_feeds);$vi_fed_lo++)
										{
											if($ans_video_feeds[$vi_fed_lo]['related_type']!='artist' && $ans_video_feeds[$vi_fed_lo]['related_type']!='community')
											{
												$num_ids = explode('_',$ans_video_feeds[$vi_fed_lo]['related_id']);
												if($ans_video_feeds[$vi_fed_lo]['related_type']=='artist_project' || $ans_video_feeds[$vi_fed_lo]['related_type']=='artist_event')
												{
													$vi_pro_eve_id[$vi_fed_lo]['related_id_art'] = $num_ids[0];
												}
												elseif($ans_video_feeds[$vi_fed_lo]['related_type']=='community_project' || $ans_video_feeds[$vi_fed_lo]['related_type']=='community_event')
												{
													$vi_art_com_id[$vi_fed_lo]['related_id_com'] = $num_ids[0];
												}
											}
											elseif($ans_video_feeds[$vi_fed_lo]['related_type']=='artist' || $ans_video_feeds[$vi_fed_lo]['related_type']=='community')
											{
												if($ans_video_feeds[$vi_fed_lo]['related_type']=='artist')
												{
													$vi_pro_eve_id[$vi_fed_lo]['related_id_art'] = $ans_video_feeds[$vi_fed_lo]['related_id'];
												}
												elseif($ans_video_feeds[$vi_fed_lo]['related_type']=='community')
												{
													$vi_art_com_id[$vi_fed_lo]['related_id_com'] = $ans_video_feeds[$vi_fed_lo]['related_id'];
												}
											}
										}
										
										if($vi_pro_eve_id!=0 && count($vi_pro_eve_id)>0)
										{
											for($arts=0;$arts<count($vi_pro_eve_id);$arts++)
											{
												$org_art[] = $vi_pro_eve_id[$arts]['related_id_art'];
											}
											
											$new_art_vi = array();
											$vi = 0;
											foreach ($org_art as $key => $value)
											{
												if(isset($new_art_vi[$value]))
												{
													$new_art_vi[$value] += 1;
												}
												else
												{
													$new_art_vi[$value] = 1;
													
													$sqls = $sql_feeds->arts_gen($value);
													
													if($sqls!="")
													{
														$vi_arts_ids[$vi] = $sqls;
													}
												}
												$vi = $vi + 1;
											}
										}
										
										if($vi_art_com_id!="" && count($vi_art_com_id)>0)
										{
											for($coms=0;$coms<count($vi_art_com_id);$coms++)
											{
												$org_com[] = $vi_art_com_id[$coms]['related_id_com'];
											}
											
											$new_com_vi = array();
											$vih_c = 0;
											foreach ($org_com as $key => $value)
											{
												if(isset($new_com_vi[$value]))
												{
													$new_com_vi[$value] += 1;
												}
												else
												{
													$new_com_vi[$value] = 1;
													
													$sqls_c = $sql_feeds->coms_gen($value);
													if($sqls_c!="")
													{
														$vi_coms_ids[$vih_c] = $sqls_c;
													}
												}
												$vih_c = $vih_c + 1;
											}
										}
										
										$doub_vi_chk = array_merge($vi_arts_ids,$vi_coms_ids);
										
										for($cvm_d=0;$cvm_d<count($doub_vi_chk);$cvm_d++)
										{
											$rel_d_id[] = $doub_vi_chk[$cvm_d]['general_user_id'];
										}
										
										$new_array_rep_vi = array();
										foreach ($rel_d_id as $key => $value)
										{
											if(isset($new_array_rep_vi[$value]))
												$new_array_rep_vi[$value] += 1;
											else
												$new_array_rep_vi[$value] = 1;
										}
										
										foreach ($new_array_rep_vi as $uid => $n)
										{
											$ex_uid_vi = $ex_uid_vi.','.$uid;
										}
										$ex_tr_vi = trim($ex_uid_vi, ",");
										$sep_ids_vv = explode(',',$ex_tr_vi);
										
										if($sep_ids_vv!="" && count($sep_ids_vv)>0)
										{
											for($se_ca=0;$se_ca<count($sep_ids_vv);$se_ca++)
											{
												$types = 'videos_post';
												$ids = $sql_feeds->pmva_posts_check($sep_ids_vv[$se_ca],$_SESSION['login_id'],$types);
												
												//die;
												if($ids!="" && count($ids)>0)
												{
													$posts_video = $sql_feeds->pmv_posts_check($sep_ids_vv[$se_ca],$_SESSION['login_id'],$types);
													if($posts_video!="" && $posts_video!=NULL)
													{
														if($posts_video!="" && mysql_num_rows($posts_video)>0)
														{
															while($rows_posts_all = mysql_fetch_assoc($posts_video))
															{
																$ans_videos_posts[] = $rows_posts_all;
															}
														}
													}
												}
											}
										}
										
										if($ans_videos_posts!="" && count($ans_videos_posts)>0)
										{
											for($vpi_all=0;$vpi_all<count($ans_videos_posts);$vpi_all++)
											{
												if($ans_videos_posts[$vpi_all]['profile_type']=='videos_post')
												{
													$exp_meds = explode('~',$ans_videos_posts[$vpi_all]['media']);
													if($exp_meds[1]=='reg_med')
													{
														$sep_ids_v = $sql_feeds->get_mediaSelected($exp_meds[0]);
													}
													elseif(($exp_meds[1]=='reg_art' || $exp_meds[1]=='reg_com') && $ans_videos_posts[$vpi_all]['profile_type']=='videos_post')
													{
														if($exp_meds[1]=='reg_art')
														{
															$reg_table_type = 'general_artist_video';
														}
														elseif($exp_meds[1]=='reg_com')
														{
															$reg_table_type = 'general_community_video';
														}
														
														$sep_ids_v = $sql_feeds->get_MediaReg($reg_table_type,$exp_meds[0]);
													}
												}
												
												if(isset($sep_ids_v) && $sep_ids_v!='0' && $sep_ids_v!=NULL)
												{
													$gens = $sql_feeds->get_log_ids($ans_videos_posts[$vpi_all]['general_user_id']);
													if($ans_videos_posts[$vpi_all]['display_to']=='All' && $ans_videos_posts[$vpi_all]['profile_type']=='videos_post')
													{
			?>
														<div class="storyCont">
																	<div class="leftSide">
																		<div class="avatar">
																			<img src="<?php echo $generalproimageURI.$gens['image_name']; ?>" />
																	</div>
																	<?php //echo "table=".$reg_table_media_type;
																	//echo $ans_audio_feeds[$audio_fed_lo]['id'];
																	?>
																		<div class="storyContainer">
																			<div class="storyTitle">
																				<span class="userName"><a href=""><?php echo $gens['fname'].' '.$gens['lname']; ?></a></span> <?php echo $ans_videos_posts[$vpi_all]['description']; ?>
																			</div>
																			<div class="activityCont">
																				<div class="sideL">
																					<div class="storyImage"></div>
																					<div class="player">
																						<img src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'">
																						<img src="images/profile/add.gif" onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'">
																						<!--<img src="images/profile/download.gif" onmouseover="this.src='images/profile/download-over.gif'" onmouseout="this.src='images/profile/download.gif'">--><img style="cursor: default;" src="images/profile/download-over.gif">
																					</div>
																				</div>
																				<div class="sideR">
																					<div class="activityTitle"><a href=""><?php
																					if($exp_meds[1]=='reg_med')
																					{
																						echo $sep_ids_v['title'];
																					}
																					elseif($exp_meds[1]=='reg_art' || $exp_meds[1]=='reg_com')
																					{
																						if($ans_videos_posts[$vpi_all]['profile_type']=='videos_post')
																						{
																							echo $sep_ids_v['video_name'];
																						}
																					}
																					?></a></div>
																					<!--<div class="activityContent">Tagged: <?php //echo $medias['tagged_users']; 
																						//$name_org = explode(')',$medias['tagged_users']);
																						//for($i_name_org=0;$i_name_org<count($name_org);$i_name_org++)
																						//{
																						//	$end_pos = strpos($name_org[$i_name_org],'(');
																						//	$org_name = substr($name_org[$i_name_org],0,$end_pos);
																					//		echo $org_name;
																					//	}
																					?></div>-->
																				</div>
																			</div>
																		</div>
																	</div>
																	<div class="rightSide">
																		<a href="#" onmouseover="mopen('mms<?php echo $vpi_all;?>13')" onmouseout="mclosetime()"><img src="images/profile/arrow.png" onmouseover="this.src='images/profile/arrow-down.png'" onmouseout="this.src='images/profile/arrow.png'" /></a>
																		<div id="mms<?php echo $vpi_all;?>13" class="popUp" onmouseover="mcancelclosetime()" onmouseout="mclosetime()">
																			<ul>
																				<li><a href="">Hide Feed</a></li>
																				<li><a href="">Report spam</a></li>
																				<li><div class="seperator"></div></li>
																				<li><a href="">Unsubscribe from <?php echo $gens['fname'].' '.$gens['lname']; ?></a></li>
																			</ul>
																		</div>
																	</div>
																</div>
			<?php
													}
													elseif($ans_videos_posts[$vpi_all]['display_to']=='Fans')
													{
														if($gens!="")
														{
															$fans_all =	$sql_feeds->fans_cond($chk_sql['email'],$gens['community_id'],$gens['artist_id']);
															if($fans_all=='1')
															{
			?>
																<div class="storyCont">
																	<div class="leftSide">
																		<div class="avatar">
																			<img src="<?php echo $generalproimageURI.$gens['image_name']; ?>" />
																	</div>
																	<?php //echo "table=".$reg_table_media_type;
																	//echo $ans_audio_feeds[$audio_fed_lo]['id'];
																	?>
																		<div class="storyContainer">
																			<div class="storyTitle">
																				<span class="userName"><a href=""><?php echo $gens['fname'].' '.$gens['lname']; ?></a></span>  <?php echo $ans_videos_posts[$vpi_all]['description']; ?>
																			</div>
																			<div class="activityCont">
																				<div class="sideL">
																					<div class="storyImage"></div>
																					<div class="player">
																						<img src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'">
																						<img src="images/profile/add.gif" onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'">
																						<!--<img src="images/profile/download.gif" onmouseover="this.src='images/profile/download-over.gif'" onmouseout="this.src='images/profile/download.gif'">--><img style="cursor: default;" src="images/profile/download-over.gif">
																					</div>
																				</div>
																				<div class="sideR">
																					<div class="activityTitle"><a href=""><?php
																					if($exp_meds[1]=='reg_med')
																					{
																						echo $sep_ids_v['title'];
																					}
																					elseif($exp_meds[1]=='reg_art' || $exp_meds[1]=='reg_com')
																					{
																						if($ans_videos_posts[$vpi_all]['profile_type']=='videos_post')
																						{
																							echo $sep_ids_v['video_name'];
																						}
																						
																					}?></a></div>
																					<!--<div class="activityContent">Tagged: <?php //echo $medias['tagged_users']; 
																						//$name_org = explode(')',$medias['tagged_users']);
																						//for($i_name_org=0;$i_name_org<count($name_org);$i_name_org++)
																						//{
																						//	$end_pos = strpos($name_org[$i_name_org],'(');
																						//	$org_name = substr($name_org[$i_name_org],0,$end_pos);
																					//		echo $org_name;
																					//	}
																					?></div>-->
																				</div>
																			</div>
																		</div>
																	</div>
																	<div class="rightSide">
																		<a href="#" onmouseover="mopen('mms<?php echo $vpi_all;?>14')" onmouseout="mclosetime()"><img src="images/profile/arrow.png" onmouseover="this.src='images/profile/arrow-down.png'" onmouseout="this.src='images/profile/arrow.png'" /></a>
																		<div id="mms<?php echo $vpi_all;?>14" class="popUp" onmouseover="mcancelclosetime()" onmouseout="mclosetime()">
																			<ul>
																				<li><a href="">Hide Feed</a></li>
																				<li><a href="">Report spam</a></li>
																				<li><div class="seperator"></div></li>
																				<li><a href="">Unsubscribe from <?php echo $gens['fname'].' '.$gens['lname']; ?></a></li>
																			</ul>
																		</div>
																	</div>
																</div>
			<?php
															}
														}
													}
													elseif($ans_videos_posts[$vpi_all]['display_to']=='Subscribers')
													{
														if($gens!="")
														{
															$subscribers_all =	$sql_feeds->subscribers_cond($chk_sql['email'],$gens['community_id'],$gens['artist_id']);
															if($subscribers_all=='1')
															{
			?>
																<div class="storyCont">
																	<div class="leftSide">
																		<div class="avatar">
																			<img src="<?php echo $generalproimageURI.$gens['image_name']; ?>" />
																	</div>
																	<?php //echo "table=".$reg_table_media_type;
																	//echo $ans_audio_feeds[$audio_fed_lo]['id'];
																	?>
																		<div class="storyContainer">
																			<div class="storyTitle">
																				<span class="userName"><a href=""><?php echo $gens['fname'].' '.$gens['lname']; ?></a></span>  <?php echo $ans_videos_posts[$vpi_all]['description']; ?>
																			</div>
																			<div class="activityCont">
																				<div class="sideL">
																					<div class="storyImage"></div>
																					<div class="player">
																						<img src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'">
																						<img src="images/profile/add.gif" onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'">
																						<!--<img src="images/profile/download.gif" onmouseover="this.src='images/profile/download-over.gif'" onmouseout="this.src='images/profile/download.gif'">--><img style="cursor: default;" src="images/profile/download-over.gif">
																					</div>
																				</div>
																				<div class="sideR">
																					<div class="activityTitle"><a href=""><?php
																					if($exp_meds[1]=='reg_med')
																					{
																						echo $sep_ids_v['title'];
																					}
																					elseif($exp_meds[1]=='reg_art' || $exp_meds[1]=='reg_com')
																					{
																						if($ans_videos_posts[$vpi_all]['profile_type']=='videos_post')
																						{
																							echo $sep_ids_v['video_name'];
																						}
																					}
																					?></a></div>
																					<!--<div class="activityContent">Tagged: <?php //echo $medias['tagged_users']; 
																						//$name_org = explode(')',$medias['tagged_users']);
																						//for($i_name_org=0;$i_name_org<count($name_org);$i_name_org++)
																						//{
																						//	$end_pos = strpos($name_org[$i_name_org],'(');
																						//	$org_name = substr($name_org[$i_name_org],0,$end_pos);
																					//		echo $org_name;
																					//	}
																					?></div>-->
																				</div>
																			</div>
																		</div>
																	</div>
																	<div class="rightSide">
																		<a href="#" onmouseover="mopen('mms<?php echo $vpi_all;?>15')" onmouseout="mclosetime()"><img src="images/profile/arrow.png" onmouseover="this.src='images/profile/arrow-down.png'" onmouseout="this.src='images/profile/arrow.png'" /></a>
																		<div id="mms<?php echo $vpi_all;?>15" class="popUp" onmouseover="mcancelclosetime()" onmouseout="mclosetime()">
																			<ul>
																				<li><a href="">Hide Feed</a></li>
																				<li><a href="">Report spam</a></li>
																				<li><div class="seperator"></div></li>
																				<li><a href="">Unsubscribe from <?php echo $gens['fname'].' '.$gens['lname']; ?></a></li>
																			</ul>
																		</div>
																	</div>
																</div>
			<?php
															}
														}
													}
													elseif($ans_videos_posts[$vpi_all]['display_to']=='Friends')
													{
														if($gens!="")
														{
															$friends_c = $sql_feeds->friends_cond($ans_videos_posts[$vpi_all]['general_user_id'],$_SESSION['login_id']);
															
															if($friends_c=='1')
															{
			?>
																<div class="storyCont">
																	<div class="leftSide">
																		<div class="avatar">
																			<img src="<?php echo $generalproimageURI.$gens['image_name']; ?>" />
																	</div>
																	<?php //echo "table=".$reg_table_media_type;
																	//echo $ans_audio_feeds[$audio_fed_lo]['id'];
																	?>
																		<div class="storyContainer">
																			<div class="storyTitle">
																				<span class="userName"><a href=""><?php echo $gens['fname'].' '.$gens['lname']; ?></a></span>  <?php echo $ans_videos_posts[$vpi_all]['description']; ?>
																			</div>
																			<div class="activityCont">
																				<div class="sideL">
																					<div class="storyImage"></div>
																					<div class="player">
																						<img src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'">
																						<img src="images/profile/add.gif" onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'">
																						<!--<img src="images/profile/download.gif" onmouseover="this.src='images/profile/download-over.gif'" onmouseout="this.src='images/profile/download.gif'">--><img style="cursor: default;" src="images/profile/download-over.gif">
																					</div>
																				</div>
																				<div class="sideR">
																					<div class="activityTitle"><a href=""><?php
																					if($exp_meds[1]=='reg_med')
																					{
																						echo $sep_ids_v['title'];
																					}
																					elseif($exp_meds[1]=='reg_art' || $exp_meds[1]=='reg_com')
																					{
																						if($ans_videos_posts[$vpi_all]['profile_type']=='videos_post')
																						{
																							echo $sep_ids_v['video_name'];
																						}
																					}
																					?></a></div>
																					<!--<div class="activityContent">Tagged: <?php //echo $medias['tagged_users']; 
																						//$name_org = explode(')',$medias['tagged_users']);
																						//for($i_name_org=0;$i_name_org<count($name_org);$i_name_org++)
																						//{
																						//	$end_pos = strpos($name_org[$i_name_org],'(');
																						//	$org_name = substr($name_org[$i_name_org],0,$end_pos);
																					//		echo $org_name;
																					//	}
																					?></div>-->
																				</div>
																			</div>
																		</div>
																	</div>
																	<div class="rightSide">
																		<a href="#" onmouseover="mopen('mms<?php echo $vpi_all;?>16')" onmouseout="mclosetime()"><img src="images/profile/arrow.png" onmouseover="this.src='images/profile/arrow-down.png'" onmouseout="this.src='images/profile/arrow.png'" /></a>
																		<div id="mms<?php echo $vpi_all;?>16" class="popUp" onmouseover="mcancelclosetime()" onmouseout="mclosetime()">
																			<ul>
																				<li><a href="">Hide Feed</a></li>
																				<li><a href="">Report spam</a></li>
																				<li><div class="seperator"></div></li>
																				<li><a href="">Unsubscribe from <?php echo $gens['fname'].' '.$gens['lname']; ?></a></li>
																			</ul>
																		</div>
																	</div>
																</div>
			<?php
															}
														}
													}
												}
											}
										}
									}
							?>
							</div>
							
							
							<div id="events_feeds_p" style="display:none;">
							<?php
									$eve_ans_all_feeds = array();
									$sub_eve_ans_all_feeds = array();
									$fan_eve_ans_all_feeds = array();
									$ve_art_com_id = array();
									$ve_pro_eve_id = array();
									$evi_arts_ids = array();
									$evi_coms_ids = array();
									$ans_events_posts = array();
									
									$eve_sql_all_feeds = $sql_feeds->all_feeds($chk_sql['email']);
									$e_run_all_feeds = mysql_query($eve_sql_all_feeds);
									
									$eve_sql_fan_feeds = $sql_feeds->fan_club($chk_sql['email']);
									$e_run_fan_feeds = mysql_query($eve_sql_fan_feeds);
									
									if($e_run_all_feeds!="" && $e_run_all_feeds!=NULL)
									{
										while($e_row_all_feeds = mysql_fetch_assoc($e_run_all_feeds))
										{
											$sub_eve_ans_all_feeds[] = $e_row_all_feeds;
										}
									}
									
									if($e_run_fan_feeds!="" && $e_run_fan_feeds!=NULL)
									{
										while($e_row_fan_feeds = mysql_fetch_assoc($e_run_fan_feeds))
										{
											$fan_eve_ans_all_feeds[] = $e_row_fan_feeds;
										}
									}
									
									$eve_ans_all_feeds = array_merge($sub_eve_ans_all_feeds,$fan_eve_ans_all_feeds);
									if($eve_ans_all_feeds!=NULL)
									{
										for($vev_fed_lo=0;$vev_fed_lo<count($eve_ans_all_feeds);$vev_fed_lo++)
										{
											if($eve_ans_all_feeds[$vev_fed_lo]['related_type']!='artist' && $eve_ans_all_feeds[$vev_fed_lo]['related_type']!='community')
											{
												$num_ids = explode('_',$eve_ans_all_feeds[$vev_fed_lo]['related_id']);
												if($eve_ans_all_feeds[$vev_fed_lo]['related_type']=='artist_project' || $eve_ans_all_feeds[$vev_fed_lo]['related_type']=='artist_event')
												{
													$ve_pro_eve_id[$vev_fed_lo]['related_id_art'] = $num_ids[0];
												}
												elseif($eve_ans_all_feeds[$vev_fed_lo]['related_type']=='community_project' || $eve_ans_all_feeds[$vev_fed_lo]['related_type']=='community_event')
												{
													$ve_art_com_id[$vev_fed_lo]['related_id_com'] = $num_ids[0];
												}
											}
											elseif($eve_ans_all_feeds[$vev_fed_lo]['related_type']=='artist' || $eve_ans_all_feeds[$vev_fed_lo]['related_type']=='community')
											{
												if($eve_ans_all_feeds[$vev_fed_lo]['related_type']=='artist')
												{
													$ve_pro_eve_id[$vev_fed_lo]['related_id_art'] = $eve_ans_all_feeds[$vev_fed_lo]['related_id'];
												}
												elseif($eve_ans_all_feeds[$vev_fed_lo]['related_type']=='community')
												{
													$ve_art_com_id[$vev_fed_lo]['related_id_com'] = $eve_ans_all_feeds[$vev_fed_lo]['related_id'];
												}
											}
										}
										
										if($ve_pro_eve_id!=0 && count($ve_pro_eve_id)>0)
										{
											for($arts=0;$arts<count($ve_pro_eve_id);$arts++)
											{
												$org_art[] = $ve_pro_eve_id[$arts]['related_id_art'];
											}
											
											$new_art_vd = array();
											$vi = 0;
											foreach ($org_art as $key => $value)
											{
												if(isset($new_art_vd[$value]))
												{
													$new_art_vd[$value] += 1;
												}
												else
												{
													$new_art_vd[$value] = 1;
													
													$sqls = $sql_feeds->arts_gen($value);
													
													if($sqls!="")
													{
														$evi_arts_ids[$vi] = $sqls;
													}
												}
												$vi = $vi + 1;
											}
										}
										
										if($ve_art_com_id!="" && count($ve_art_com_id)>0)
										{
											for($coms=0;$coms<count($ve_art_com_id);$coms++)
											{
												$org_com[] = $ve_art_com_id[$coms]['related_id_com'];
											}
											
											$new_com_ves = array();
											$vih_c = 0;
											foreach ($org_com as $key => $value)
											{
												if(isset($new_com_ves[$value]))
												{
													$new_com_ves[$value] += 1;
												}
												else
												{
													$new_com_ves[$value] = 1;
													
													$sqls_c = $sql_feeds->coms_gen($value);
													if($sqls_c!="")
													{
														$evi_coms_ids[$vih_c] = $sqls_c;
													}
												}
												$vih_c = $vih_c + 1;
											}
										}
										
										$doub_er_chk = array_merge($evi_arts_ids,$evi_coms_ids);
										
										for($cevm_d=0;$cevm_d<count($doub_er_chk);$cevm_d++)
										{
											$rel_d_id[] = $doub_er_chk[$cevm_d]['general_user_id'];
										}
										
										$new_array_rep_edv = array();
										foreach ($rel_d_id as $key => $value)
										{
											if(isset($new_array_rep_edv[$value]))
												$new_array_rep_edv[$value] += 1;
											else
												$new_array_rep_edv[$value] = 1;
										}
										
										foreach ($new_array_rep_edv as $uid => $n)
										{
											$ex_uid_devs = $ex_uid_devs.','.$uid;
										}
										$ex_tr_esi = trim($ex_uid_devs, ",");
										$sep_ids_edvp = explode(',',$ex_tr_esi);
										
										if($sep_ids_edvp!="" && count($sep_ids_edvp)>0)
										{
											for($se_ca=0;$se_ca<count($sep_ids_edvp);$se_ca++)
											{
												$types = 'events_post';
												$ids = $sql_feeds->pmva_posts_check($sep_ids_edvp[$se_ca],$_SESSION['login_id'],$types);
												
												//die;
												if($ids!="" && count($ids)>0)
												{
													$posts_event = $sql_feeds->pmv_posts_check($sep_ids_edvp[$se_ca],$_SESSION['login_id'],$types);
													if($posts_event!="" && $posts_event!=NULL)
													{
														if($posts_event!="" && mysql_num_rows($posts_event)>0)
														{
															while($rows_posts_all = mysql_fetch_assoc($posts_event))
															{
																$ans_events_posts[] = $rows_posts_all;
															}
														}
													}
												}
											}
										}
										
										if($ans_events_posts!="" && count($ans_events_posts)>0)
										{
											for($vpev_all=0;$vpev_all<count($ans_events_posts);$vpev_all++)
											{
												if($ans_events_posts[$vpev_all]['profile_type']=='events_post')
												{
													$exp_meds = explode('~',$ans_events_posts[$vpev_all]['media']);
													
													if($ans_events_posts[$vpev_all]['profile_type']=='events_post')
													{
														$exp_meds = explode('~',$ans_events_posts[$vpev_all]['media']);
														$sep_ids_edv = $sql_feeds->featured_pro_eve($exp_meds[1],$exp_meds[0]);
													}
												}
												
												if(isset($sep_ids_edv) && $sep_ids_edv!='0' && $sep_ids_edv!=NULL)
												{
													$gens = $sql_feeds->get_log_ids($ans_events_posts[$vpev_all]['general_user_id']);
													if($ans_events_posts[$vpev_all]['display_to']=='All' && $ans_events_posts[$vpev_all]['profile_type']=='events_post')
													{
			?>
														<div class="storyCont">
																	<div class="leftSide">
																		<div class="avatar">
																			<img src="<?php echo $generalproimageURI.$gens['image_name']; ?>" />
																	</div>
																	<?php //echo "table=".$reg_table_media_type;
																	//echo $ans_audio_feeds[$audio_fed_lo]['id'];
																	?>
																		<div class="storyContainer">
																			<div class="storyTitle">
																				<span class="userName"><a href=""><?php echo $gens['fname'].' '.$gens['lname']; ?></a></span> <?php echo $ans_events_posts[$vpev_all]['description']; ?>
																			</div>
																			<div class="activityCont">
																				<div class="sideL">
																					<div class="storyImage"></div>
																					<div class="player">
																						<img src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'">
																						<img src="images/profile/add.gif" onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'">
																						<!--<img src="images/profile/download.gif" onmouseover="this.src='images/profile/download-over.gif'" onmouseout="this.src='images/profile/download.gif'">--><img style="cursor: default;" src="images/profile/download-over.gif">
																					</div>
																				</div>
																				<div class="sideR">
																					<div class="activityTitle"><a href=""><?php
																					if($ans_events_posts[$vpev_all]['profile_type']=='events_post')
																					{
																						echo $sep_ids_edv['title'];
																					}
																					?></a></div>
																					<!--<div class="activityContent">Tagged: <?php //echo $medias['tagged_users']; 
																						//$name_org = explode(')',$medias['tagged_users']);
																						//for($i_name_org=0;$i_name_org<count($name_org);$i_name_org++)
																						//{
																						//	$end_pos = strpos($name_org[$i_name_org],'(');
																						//	$org_name = substr($name_org[$i_name_org],0,$end_pos);
																					//		echo $org_name;
																					//	}
																					?></div>-->
																				</div>
																			</div>
																		</div>
																	</div>
																	<div class="rightSide">
																		<a href="#" onmouseover="mopen('mms<?php echo $vpev_all;?>17')" onmouseout="mclosetime()"><img src="images/profile/arrow.png" onmouseover="this.src='images/profile/arrow-down.png'" onmouseout="this.src='images/profile/arrow.png'" /></a>
																		<div id="mms<?php echo $vpev_all;?>17" class="popUp" onmouseover="mcancelclosetime()" onmouseout="mclosetime()">
																			<ul>
																				<li><a href="">Hide Feed</a></li>
																				<li><a href="">Report spam</a></li>
																				<li><div class="seperator"></div></li>
																				<li><a href="">Unsubscribe from <?php echo $gens['fname'].' '.$gens['lname']; ?></a></li>
																			</ul>
																		</div>
																	</div>
																</div>
			<?php
													}
													elseif($ans_events_posts[$vpev_all]['display_to']=='Fans')
													{
														if($gens!="")
														{
															$fans_all =	$sql_feeds->fans_cond($chk_sql['email'],$gens['community_id'],$gens['artist_id']);
															if($fans_all=='1')
															{
			?>
																<div class="storyCont">
																	<div class="leftSide">
																		<div class="avatar">
																			<img src="<?php echo $generalproimageURI.$gens['image_name']; ?>" />
																	</div>
																	<?php //echo "table=".$reg_table_media_type;
																	//echo $ans_audio_feeds[$audio_fed_lo]['id'];
																	?>
																		<div class="storyContainer">
																			<div class="storyTitle">
																				<span class="userName"><a href=""><?php echo $gens['fname'].' '.$gens['lname']; ?></a></span>  <?php echo $ans_events_posts[$vpev_all]['description']; ?>
																			</div>
																			<div class="activityCont">
																				<div class="sideL">
																					<div class="storyImage"></div>
																					<div class="player">
																						<img src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'">
																						<img src="images/profile/add.gif" onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'">
																						<!--<img src="images/profile/download.gif" onmouseover="this.src='images/profile/download-over.gif'" onmouseout="this.src='images/profile/download.gif'">--><img style="cursor: default;" src="images/profile/download-over.gif">
																					</div>
																				</div>
																				<div class="sideR">
																					<div class="activityTitle"><a href=""><?php
																					if($ans_events_posts[$vpev_all]['profile_type']=='events_post')
																					{
																						echo $sep_ids_edv['title'];
																					}?></a></div>
																					<!--<div class="activityContent">Tagged: <?php //echo $medias['tagged_users']; 
																						//$name_org = explode(')',$medias['tagged_users']);
																						//for($i_name_org=0;$i_name_org<count($name_org);$i_name_org++)
																						//{
																						//	$end_pos = strpos($name_org[$i_name_org],'(');
																						//	$org_name = substr($name_org[$i_name_org],0,$end_pos);
																					//		echo $org_name;
																					//	}
																					?></div>-->
																				</div>
																			</div>
																		</div>
																	</div>
																	<div class="rightSide">
																		<a href="#" onmouseover="mopen('mms<?php echo $vpev_all;?>18')" onmouseout="mclosetime()"><img src="images/profile/arrow.png" onmouseover="this.src='images/profile/arrow-down.png'" onmouseout="this.src='images/profile/arrow.png'" /></a>
																		<div id="mms<?php echo $vpev_all;?>18" class="popUp" onmouseover="mcancelclosetime()" onmouseout="mclosetime()">
																			<ul>
																				<li><a href="">Hide Feed</a></li>
																				<li><a href="">Report spam</a></li>
																				<li><div class="seperator"></div></li>
																				<li><a href="">Unsubscribe from <?php echo $gens['fname'].' '.$gens['lname']; ?></a></li>
																			</ul>
																		</div>
																	</div>
																</div>
			<?php
															}
														}
													}
													elseif($ans_events_posts[$vpev_all]['display_to']=='Subscribers')
													{
														if($gens!="")
														{
															$subscribers_all =	$sql_feeds->subscribers_cond($chk_sql['email'],$gens['community_id'],$gens['artist_id']);
															if($subscribers_all=='1')
															{
			?>
																<div class="storyCont">
																	<div class="leftSide">
																		<div class="avatar">
																			<img src="<?php echo $generalproimageURI.$gens['image_name']; ?>" />
																	</div>
																	<?php //echo "table=".$reg_table_media_type;
																	//echo $ans_audio_feeds[$audio_fed_lo]['id'];
																	?>
																		<div class="storyContainer">
																			<div class="storyTitle">
																				<span class="userName"><a href=""><?php echo $gens['fname'].' '.$gens['lname']; ?></a></span>  <?php echo $ans_events_posts[$vpev_all]['description']; ?>
																			</div>
																			<div class="activityCont">
																				<div class="sideL">
																					<div class="storyImage"></div>
																					<div class="player">
																						<img src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'">
																						<img src="images/profile/add.gif" onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'">
																						<!--<img src="images/profile/download.gif" onmouseover="this.src='images/profile/download-over.gif'" onmouseout="this.src='images/profile/download.gif'">--><img style="cursor: default;" src="images/profile/download-over.gif">
																					</div>
																				</div>
																				<div class="sideR">
																					<div class="activityTitle"><a href=""><?php
																					if($ans_events_posts[$vpev_all]['profile_type']=='events_post')
																					{
																						echo $sep_ids_edv['title'];
																					}?></a></div>
																					<!--<div class="activityContent">Tagged: <?php //echo $medias['tagged_users']; 
																						//$name_org = explode(')',$medias['tagged_users']);
																						//for($i_name_org=0;$i_name_org<count($name_org);$i_name_org++)
																						//{
																						//	$end_pos = strpos($name_org[$i_name_org],'(');
																						//	$org_name = substr($name_org[$i_name_org],0,$end_pos);
																					//		echo $org_name;
																					//	}
																					?></div>-->
																				</div>
																			</div>
																		</div>
																	</div>
																	<div class="rightSide">
																		<a href="#" onmouseover="mopen('mms<?php echo $vpev_all;?>19')" onmouseout="mclosetime()"><img src="images/profile/arrow.png" onmouseover="this.src='images/profile/arrow-down.png'" onmouseout="this.src='images/profile/arrow.png'" /></a>
																		<div id="mms<?php echo $vpev_all;?>19" class="popUp" onmouseover="mcancelclosetime()" onmouseout="mclosetime()">
																			<ul>
																				<li><a href="">Hide Feed</a></li>
																				<li><a href="">Report spam</a></li>
																				<li><div class="seperator"></div></li>
																				<li><a href="">Unsubscribe from <?php echo $gens['fname'].' '.$gens['lname']; ?></a></li>
																			</ul>
																		</div>
																	</div>
																</div>
			<?php
															}
														}
													}
													elseif($ans_events_posts[$vpev_all]['display_to']=='Friends')
													{
														if($gens!="")
														{
															$friends_c = $sql_feeds->friends_cond($ans_events_posts[$vpev_all]['general_user_id'],$_SESSION['login_id']);
															
															if($friends_c=='1')
															{
			?>
																<div class="storyCont">
																	<div class="leftSide">
																		<div class="avatar">
																			<img src="<?php echo $generalproimageURI.$gens['image_name']; ?>" />
																	</div>
																	<?php //echo "table=".$reg_table_media_type;
																	//echo $ans_audio_feeds[$audio_fed_lo]['id'];
																	?>
																		<div class="storyContainer">
																			<div class="storyTitle">
																				<span class="userName"><a href=""><?php echo $gens['fname'].' '.$gens['lname']; ?></a></span>  <?php echo $ans_events_posts[$vpev_all]['description']; ?>
																			</div>
																			<div class="activityCont">
																				<div class="sideL">
																					<div class="storyImage"></div>
																					<div class="player">
																						<img src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'">
																						<img src="images/profile/add.gif" onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'">
																						<!--<img src="images/profile/download.gif" onmouseover="this.src='images/profile/download-over.gif'" onmouseout="this.src='images/profile/download.gif'">--><img style="cursor: default;" src="images/profile/download-over.gif">
																					</div>
																				</div>
																				<div class="sideR">
																					<div class="activityTitle"><a href=""><?php
																					if($ans_events_posts[$vpev_all]['profile_type']=='events_post')
																					{
																						echo $sep_ids_edv['title'];
																					}?></a></div>
																					<!--<div class="activityContent">Tagged: <?php //echo $medias['tagged_users']; 
																						//$name_org = explode(')',$medias['tagged_users']);
																						//for($i_name_org=0;$i_name_org<count($name_org);$i_name_org++)
																						//{
																						//	$end_pos = strpos($name_org[$i_name_org],'(');
																						//	$org_name = substr($name_org[$i_name_org],0,$end_pos);
																					//		echo $org_name;
																					//	}
																					?></div>-->
																				</div>
																			</div>
																		</div>
																	</div>
																	<div class="rightSide">
																		<a href="#" onmouseover="mopen('mms<?php echo $vpev_all;?>20')" onmouseout="mclosetime()"><img src="images/profile/arrow.png" onmouseover="this.src='images/profile/arrow-down.png'" onmouseout="this.src='images/profile/arrow.png'" /></a>
																		<div id="mms<?php echo $vpev_all;?>20" class="popUp" onmouseover="mcancelclosetime()" onmouseout="mclosetime()">
																			<ul>
																				<li><a href="">Hide Feed</a></li>
																				<li><a href="">Report spam</a></li>
																				<li><div class="seperator"></div></li>
																				<li><a href="">Unsubscribe from <?php echo $gens['fname'].' '.$gens['lname']; ?></a></li>
																			</ul>
																		</div>
																	</div>
																</div>
			<?php
															}
														}
													}
												}
											}
										}
									}	
							?>
							</div>
							
							
							<div id="project_feeds_p" style="display:none;">
							<?php
									$pro_ans_all_feeds = array();
									$sub_pro_ans_all_feeds = array();
									$pro_ans_fan_feeds = array();
									$pvr_pro_eve_id = array();
									$vpr_art_com_id = array();
									$pri_arts_ids = array();
									$pri_coms_ids = array();
									$ans_projects_posts = array();
									
									$pro_sql_all_feeds = $sql_feeds->all_feeds($chk_sql['email']);
									$p_run_all_feeds = mysql_query($pro_sql_all_feeds);
									
									$pro_sql_fan_all_feeds = $sql_feeds->fan_club($chk_sql['email']);
									$p_run_fan_all_feeds = mysql_query($pro_sql_fan_all_feeds);
									
									if($p_run_all_feeds!="" && $p_run_all_feeds!=NULL)
									{
										while($pro_row_all_feeds = mysql_fetch_assoc($p_run_all_feeds))
										{
											$sub_pro_ans_all_feeds[] = $pro_row_all_feeds;
										}
									}
									
									if($p_run_fan_all_feeds!="" && $p_run_fan_all_feeds!=NULL)
									{
										while($pro_row_fan_all_feeds = mysql_fetch_assoc($p_run_fan_all_feeds))
										{
											$pro_ans_fan_feeds[] = $pro_row_fan_all_feeds;
										}
									}
									
									$pro_ans_all_feeds = array_merge($sub_pro_ans_all_feeds,$pro_ans_fan_feeds);
									
									if($pro_ans_all_feeds!=NULL)
									{
										for($vpr_fed_lo=0;$vpr_fed_lo<count($pro_ans_all_feeds);$vpr_fed_lo++)
										{
											if($pro_ans_all_feeds[$vpr_fed_lo]['related_type']!='artist' && $pro_ans_all_feeds[$vpr_fed_lo]['related_type']!='community')
											{
												$num_ids = explode('_',$pro_ans_all_feeds[$vpr_fed_lo]['related_id']);
												if($pro_ans_all_feeds[$vpr_fed_lo]['related_type']=='artist_project' || $pro_ans_all_feeds[$vpr_fed_lo]['related_type']=='artist_event')
												{
													$pvr_pro_eve_id[$vpr_fed_lo]['related_id_art'] = $num_ids[0];
												}
												elseif($pro_ans_all_feeds[$vpr_fed_lo]['related_type']=='community_project' || $pro_ans_all_feeds[$vpr_fed_lo]['related_type']=='community_event')
												{
													$vpr_art_com_id[$vpr_fed_lo]['related_id_com'] = $num_ids[0];
												}
											}
											elseif($pro_ans_all_feeds[$vpr_fed_lo]['related_type']=='artist' || $pro_ans_all_feeds[$vpr_fed_lo]['related_type']=='community')
											{
												if($pro_ans_all_feeds[$vpr_fed_lo]['related_type']=='artist')
												{
													$pvr_pro_eve_id[$vpr_fed_lo]['related_id_art'] = $pro_ans_all_feeds[$vpr_fed_lo]['related_id'];
												}
												elseif($pro_ans_all_feeds[$vpr_fed_lo]['related_type']=='community')
												{
													$vpr_art_com_id[$vpr_fed_lo]['related_id_com'] = $pro_ans_all_feeds[$vpr_fed_lo]['related_id'];
												}
											}
										}
										
										if($pvr_pro_eve_id!=0 && count($pvr_pro_eve_id)>0)
										{
											for($arts=0;$arts<count($pvr_pro_eve_id);$arts++)
											{
												$org_art[] = $pvr_pro_eve_id[$arts]['related_id_art'];
											}
											
											$new_art_rp = array();
											$rpi = 0;
											foreach ($org_art as $key => $value)
											{
												if(isset($new_art_rp[$value]))
												{
													$new_art_rp[$value] += 1;
												}
												else
												{
													$new_art_rp[$value] = 1;
													
													$sqls = $sql_feeds->arts_gen($value);
													
													if($sqls!="")
													{
														$pri_arts_ids[$rpi] = $sqls;
													}
												}
												$rpi = $rpi + 1;
											}
										}
										
										if($vpr_art_com_id!="" && count($vpr_art_com_id)>0)
										{
											for($coms=0;$coms<count($vpr_art_com_id);$coms++)
											{
												$org_com[] = $vpr_art_com_id[$coms]['related_id_com'];
											}
											
											$new_com_prs = array();
											$vrp_c = 0;
											foreach ($org_com as $key => $value)
											{
												if(isset($new_com_prs[$value]))
												{
													$new_com_prs[$value] += 1;
												}
												else
												{
													$new_com_prs[$value] = 1;
													
													$sqls_c = $sql_feeds->coms_gen($value);
													if($sqls_c!="")
													{
														$pri_coms_ids[$vrp_c] = $sqls_c;
													}
												}
												$vrp_c = $vrp_c + 1;
											}
										}
										
										$doub_psr_chk = array_merge($pri_arts_ids,$pri_coms_ids);
										
										for($prim_d=0;$prim_d<count($doub_psr_chk);$prim_d++)
										{
											$rel_d_id[] = $doub_psr_chk[$prim_d]['general_user_id'];
										}
										
										$new_array_rep_pir = array();
										foreach ($rel_d_id as $key => $value)
										{
											if(isset($new_array_rep_pir[$value]))
												$new_array_rep_pir[$value] += 1;
											else
												$new_array_rep_pir[$value] = 1;
										}
										
										foreach ($new_array_rep_pir as $uid => $n)
										{
											$ex_uid_prvs = $ex_uid_prvs.','.$uid;
										}
										$ex_tr_eri = trim($ex_uid_prvs, ",");
										$sep_ids_irpp = explode(',',$ex_tr_eri);

										if($sep_ids_irpp!="" && count($sep_ids_irpp)>0)
										{
											for($se_ca=0;$se_ca<count($sep_ids_irpp);$se_ca++)
											{
												$types = 'projects_post';
												$ids = $sql_feeds->pmva_posts_check($sep_ids_irpp[$se_ca],$_SESSION['login_id'],$types);
												
												//die;
												if($ids!="" && count($ids)>0)
												{
													$posts_event = $sql_feeds->pmv_posts_check($sep_ids_irpp[$se_ca],$_SESSION['login_id'],$types);
													if($posts_event!="" && $posts_event!=NULL)
													{
														if($posts_event!="" && mysql_num_rows($posts_event)>0)
														{
															while($rows_posts_all = mysql_fetch_assoc($posts_event))
															{
																$ans_projects_posts[] = $rows_posts_all;
															}
														}
													}
												}
											}
										}
										
										if($ans_projects_posts!="" && count($ans_projects_posts)>0) 
										{
											for($vppr_all=0;$vppr_all<count($ans_projects_posts);$vppr_all++)
											{
												if($ans_projects_posts[$vppr_all]['profile_type']=='projects_post')
												{
													$exp_meds = explode('~',$ans_projects_posts[$vppr_all]['media']);
													
													if($ans_projects_posts[$vppr_all]['profile_type']=='projects_post')
													{
														$exp_meds = explode('~',$ans_projects_posts[$vppr_all]['media']);
														$sep_ids_irp = $sql_feeds->featured_pro_eve($exp_meds[1],$exp_meds[0]);
													}
												}
										
												if(isset($sep_ids_irp) && $sep_ids_irp!='0' && $sep_ids_irp!=NULL)
												{
													$gens = $sql_feeds->get_log_ids($ans_projects_posts[$vppr_all]['general_user_id']);
													if($ans_projects_posts[$vppr_all]['display_to']=='All' && $ans_projects_posts[$vppr_all]['profile_type']=='projects_post')
													{
			?>
														<div class="storyCont">
																	<div class="leftSide">
																		<div class="avatar">
																			<img src="<?php echo $generalproimageURI.$gens['image_name']; ?>" />
																	</div>
																	<?php //echo "table=".$reg_table_media_type;
																	//echo $ans_audio_feeds[$audio_fed_lo]['id'];
																	?>
																		<div class="storyContainer">
																			<div class="storyTitle">
																				<span class="userName"><a href=""><?php echo $gens['fname'].' '.$gens['lname']; ?></a></span> <?php echo $ans_projects_posts[$vppr_all]['description']; ?>
																			</div>
																			<div class="activityCont">
																				<div class="sideL">
																					<div class="storyImage"></div>
																					<div class="player">
																						<img src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'">
																						<img src="images/profile/add.gif" onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'">
																						<!--<img src="images/profile/download.gif" onmouseover="this.src='images/profile/download-over.gif'" onmouseout="this.src='images/profile/download.gif'">--><img style="cursor: default;" src="images/profile/download-over.gif">
																					</div>
																				</div>
																				<div class="sideR">
																					<div class="activityTitle"><a href=""><?php
																					if($ans_projects_posts[$vppr_all]['profile_type']=='projects_post')
																					{
																						echo $sep_ids_irp['title'];
																					}
																					?></a></div>
																					<!--<div class="activityContent">Tagged: <?php //echo $medias['tagged_users']; 
																						//$name_org = explode(')',$medias['tagged_users']);
																						//for($i_name_org=0;$i_name_org<count($name_org);$i_name_org++)
																						//{
																						//	$end_pos = strpos($name_org[$i_name_org],'(');
																						//	$org_name = substr($name_org[$i_name_org],0,$end_pos);
																					//		echo $org_name;
																					//	}
																					?></div>-->
																				</div>
																			</div>
																		</div>
																	</div>
																	<div class="rightSide">
																		<a href="#" onmouseover="mopen('mms<?php echo $vppr_all;?>21')" onmouseout="mclosetime()"><img src="images/profile/arrow.png" onmouseover="this.src='images/profile/arrow-down.png'" onmouseout="this.src='images/profile/arrow.png'" /></a>
																		<div id="mms<?php echo $vppr_all;?>21" class="popUp" onmouseover="mcancelclosetime()" onmouseout="mclosetime()">
																			<ul>
																				<li><a href="">Hide Feed</a></li>
																				<li><a href="">Report spam</a></li>
																				<li><div class="seperator"></div></li>
																				<li><a href="">Unsubscribe from <?php echo $gens['fname'].' '.$gens['lname']; ?></a></li>
																			</ul>
																		</div>
																	</div>
																</div>
			<?php
													}
													elseif($ans_projects_posts[$vppr_all]['display_to']=='Fans')
													{
														if($gens!="")
														{
															$fans_all =	$sql_feeds->fans_cond($chk_sql['email'],$gens['community_id'],$gens['artist_id']);
															if($fans_all=='1')
															{
			?>
																<div class="storyCont">
																	<div class="leftSide">
																		<div class="avatar">
																			<img src="<?php echo $generalproimageURI.$gens['image_name']; ?>" />
																	</div>
																	<?php //echo "table=".$reg_table_media_type;
																	//echo $ans_audio_feeds[$audio_fed_lo]['id'];
																	?>
																		<div class="storyContainer">
																			<div class="storyTitle">
																				<span class="userName"><a href=""><?php echo $gens['fname'].' '.$gens['lname']; ?></a></span>  <?php echo $ans_projects_posts[$vppr_all]['description']; ?>
																			</div>
																			<div class="activityCont">
																				<div class="sideL">
																					<div class="storyImage"></div>
																					<div class="player">
																						<img src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'">
																						<img src="images/profile/add.gif" onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'">
																						<!--<img src="images/profile/download.gif" onmouseover="this.src='images/profile/download-over.gif'" onmouseout="this.src='images/profile/download.gif'">--><img style="cursor: default;" src="images/profile/download-over.gif">
																					</div>
																				</div>
																				<div class="sideR">
																					<div class="activityTitle"><a href=""><?php
																					if($ans_projects_posts[$vppr_all]['profile_type']=='projects_post')
																					{
																						echo $sep_ids_irp['title'];
																					}?></a></div>
																					<!--<div class="activityContent">Tagged: <?php //echo $medias['tagged_users']; 
																						//$name_org = explode(')',$medias['tagged_users']);
																						//for($i_name_org=0;$i_name_org<count($name_org);$i_name_org++)
																						//{
																						//	$end_pos = strpos($name_org[$i_name_org],'(');
																						//	$org_name = substr($name_org[$i_name_org],0,$end_pos);
																					//		echo $org_name;
																					//	}
																					?></div>-->
																				</div>
																			</div>
																		</div>
																	</div>
																	<div class="rightSide">
																		<a href="#" onmouseover="mopen('mms<?php echo $vppr_all;?>22')" onmouseout="mclosetime()"><img src="images/profile/arrow.png" onmouseover="this.src='images/profile/arrow-down.png'" onmouseout="this.src='images/profile/arrow.png'" /></a>
																		<div id="mms<?php echo $vppr_all;?>22" class="popUp" onmouseover="mcancelclosetime()" onmouseout="mclosetime()">
																			<ul>
																				<li><a href="">Hide Feed</a></li>
																				<li><a href="">Report spam</a></li>
																				<li><div class="seperator"></div></li>
																				<li><a href="">Unsubscribe from <?php echo $gens['fname'].' '.$gens['lname']; ?></a></li>
																			</ul>
																		</div>
																	</div>
																</div>
			<?php
															}
														}
													}
													elseif($ans_projects_posts[$vppr_all]['display_to']=='Subscribers')
													{
														if($gens!="")
														{
															$subscribers_all =	$sql_feeds->subscribers_cond($chk_sql['email'],$gens['community_id'],$gens['artist_id']);
															if($subscribers_all=='1')
															{
			?>
																<div class="storyCont">
																	<div class="leftSide">
																		<div class="avatar">
																			<img src="<?php echo $generalproimageURI.$gens['image_name']; ?>" />
																	</div>
																	<?php //echo "table=".$reg_table_media_type;
																	//echo $ans_audio_feeds[$audio_fed_lo]['id'];
																	?>
																		<div class="storyContainer">
																			<div class="storyTitle">
																				<span class="userName"><a href=""><?php echo $gens['fname'].' '.$gens['lname']; ?></a></span>  <?php echo $ans_projects_posts[$vppr_all]['description']; ?>
																			</div>
																			<div class="activityCont">
																				<div class="sideL">
																					<div class="storyImage"></div>
																					<div class="player">
																						<img src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'">
																						<img src="images/profile/add.gif" onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'">
																						<!--<img src="images/profile/download.gif" onmouseover="this.src='images/profile/download-over.gif'" onmouseout="this.src='images/profile/download.gif'">--><img style="cursor: default;" src="images/profile/download-over.gif">
																					</div>
																				</div>
																				<div class="sideR">
																					<div class="activityTitle"><a href=""><?php
																					if($ans_projects_posts[$vppr_all]['profile_type']=='projects_post')
																					{
																						echo $sep_ids_irp['title'];
																					}?></a></div>
																					<!--<div class="activityContent">Tagged: <?php //echo $medias['tagged_users']; 
																						//$name_org = explode(')',$medias['tagged_users']);
																						//for($i_name_org=0;$i_name_org<count($name_org);$i_name_org++)
																						//{
																						//	$end_pos = strpos($name_org[$i_name_org],'(');
																						//	$org_name = substr($name_org[$i_name_org],0,$end_pos);
																					//		echo $org_name;
																					//	}
																					?></div>-->
																				</div>
																			</div>
																		</div>
																	</div>
																	<div class="rightSide">
																		<a href="#" onmouseover="mopen('mms<?php echo $vppr_all;?>23')" onmouseout="mclosetime()"><img src="images/profile/arrow.png" onmouseover="this.src='images/profile/arrow-down.png'" onmouseout="this.src='images/profile/arrow.png'" /></a>
																		<div id="mms<?php echo $vppr_all;?>23" class="popUp" onmouseover="mcancelclosetime()" onmouseout="mclosetime()">
																			<ul>
																				<li><a href="">Hide Feed</a></li>
																				<li><a href="">Report spam</a></li>
																				<li><div class="seperator"></div></li>
																				<li><a href="">Unsubscribe from <?php echo $gens['fname'].' '.$gens['lname']; ?></a></li>
																			</ul>
																		</div>
																	</div>
																</div>
			<?php
															}
														}
													}
													elseif($ans_projects_posts[$vppr_all]['display_to']=='Friends')
													{
														if($gens!="")
														{
															$friends_c = $sql_feeds->friends_cond($ans_projects_posts[$vppr_all]['general_user_id'],$_SESSION['login_id']);
															
															if($friends_c=='1')
															{
			?>
																<div class="storyCont">
																	<div class="leftSide">
																		<div class="avatar">
																			<img src="<?php echo $generalproimageURI.$gens['image_name']; ?>" />
																	</div>
																	<?php //echo "table=".$reg_table_media_type;
																	//echo $ans_audio_feeds[$audio_fed_lo]['id'];
																	?>
																		<div class="storyContainer">
																			<div class="storyTitle">
																				<span class="userName"><a href=""><?php echo $gens['fname'].' '.$gens['lname']; ?></a></span>  <?php echo $ans_projects_posts[$vppr_all]['description']; ?>
																			</div>
																			<div class="activityCont">
																				<div class="sideL">
																					<div class="storyImage"></div>
																					<div class="player">
																						<img src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'">
																						<img src="images/profile/add.gif" onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'">
																						<!--<img src="images/profile/download.gif" onmouseover="this.src='images/profile/download-over.gif'" onmouseout="this.src='images/profile/download.gif'"><img style="cursor: default;" src="images/profile/download-over.gif">-->
																					</div>
																				</div>
																				<div class="sideR">
																					<div class="activityTitle"><a href=""><?php
																					if($ans_projects_posts[$vppr_all]['profile_type']=='projects_post')
																					{
																						echo $sep_ids_irp['title'];
																					}?></a></div>
																					<!--<div class="activityContent">Tagged: <?php //echo $medias['tagged_users']; 
																						//$name_org = explode(')',$medias['tagged_users']);
																						//for($i_name_org=0;$i_name_org<count($name_org);$i_name_org++)
																						//{
																						//	$end_pos = strpos($name_org[$i_name_org],'(');
																						//	$org_name = substr($name_org[$i_name_org],0,$end_pos);
																					//		echo $org_name;
																					//	}
																					?></div>-->
																				</div>
																			</div>
																		</div>
																	</div>
																	<div class="rightSide">
																		<a href="#" onmouseover="mopen('mms<?php echo $vppr_all;?>24')" onmouseout="mclosetime()"><img src="images/profile/arrow.png" onmouseover="this.src='images/profile/arrow-down.png'" onmouseout="this.src='images/profile/arrow.png'" /></a>
																		<div id="mms<?php echo $vppr_all;?>24" class="popUp" onmouseover="mcancelclosetime()" onmouseout="mclosetime()">
																			<ul>
																				<li><a href="">Hide Feed</a></li>
																				<li><a href="">Report spam</a></li>
																				<li><div class="seperator"></div></li>
																				<li><a href="">Unsubscribe from <?php echo $gens['fname'].' '.$gens['lname']; ?></a></li>
																			</ul>
																		</div>
																	</div>
																</div>
			<?php
															}
														}
													}
												}
											}
										}
									}
							?>
							</div>
		<?php
					}
		?>
                    </div>
				</div>
				<div class="subTabs" id="transactions" style="display:none;"><!---->
                	<h1>Transactions</h1>
                    <h2></h2>
                    <div style="width:665px; float:left;">
                        <div id="actualContent">
						<?php
							$all_stores = array();
							$meds_buy = array();
							$fans_buy = array();
							$donate_field = array();
							$meds_don = 0;
							
							$whole_profit = 0;
							$all_sales = $trans->get_all_trans();
							if($all_sales!="" && $all_sales!=NULL)
							{
								if(mysql_num_rows($all_sales)>0)
								{
									$no = 1;
									
									while($rows = mysql_fetch_assoc($all_sales))
									{
										$total_profit = 0;
										$total_profit_do = 0;
										$exp_fee = explode(',',$rows['paypal_fee']);
										/* $total_trnc = ($rows['total'] * 0.05) + 0.05;
										$Paypal_Fee = round($total_trnc); */
										if($rows['total']!=0)
										{
											if($rows['price']!=0)
											{
												$purify_fee = round(($rows['price'] - $exp_fee[0]) * 0.05, 2);
												/* if($purify_fee<=0)
												{
													$purify_fee = 1;
												} */
												
												$total_profit = $rows['price'] - $exp_fee[0] - $purify_fee;
											}
											$buy = $trans->get_buyer($rows['buyer_id']);
											
											$get_media = $trans->get_media($rows['media_id']);
											
											if(mysql_num_rows($get_media)>0)
											{
												if($rows['price']!=0)
												{
													$meds_buy[$meds_don] = $rows;
												}
												if($rows['donate']!=0)
												{
													$purify_fee = round(($rows['donate'] - $exp_fee[1]) * 0.05, 2);
													$total_profit_do = $rows['donate'] - $exp_fee[1] - $purify_fee;
													
													$donate_field[$meds_don] = $rows;
													$donate_field[$meds_don]['donate_field'] = 'yes';
												}
												else
												{
													$total_profit_do = 0;
												}
												
												$whole_profit = $whole_profit + $total_profit + $total_profit_do;
												$meds_don = $meds_don + 1;
											}
										}
									}
								}
							}
							
							$all_fan_sales = $trans->get_all_fan_trans();
							if($all_fan_sales!="" && $all_fan_sales!=NULL)
							{
								if(mysql_num_rows($all_fan_sales)>0)
								{
									$no = 1;
									$fan_don = $meds_don;
									while($rows = mysql_fetch_assoc($all_fan_sales))
									{
										$total_profit_do = 0;
										/* $total_trnc = ($rows['total'] * 0.05) + 0.05;
										$Paypal_Fee = round($total_trnc); */
										$exp_fee = explode(',',$rows['paypal_fee']);
										
										$purify_fee = round(($rows['price'] - $exp_fee[0]) * 0.05, 2);
										$total_profit = $rows['price'] - $exp_fee[0] - $purify_fee;
										/* if($purify_fee<=0)
										{
											$purify_fee = 1;
										} */
										
										//$total_profit = $rows['total'] - $rows['paypal_fee'] - $purify_fee;
										
										//$buy = $trans->get_buyer($rows['buyer_id']);
										
										//$get_media = $trans->get_media($rows['media_id']);
										//if(mysql_num_rows($get_media)>0)
										//{
											
											//$whole_profit = $whole_profit + $total_profit;
											$fans_buy[$fan_don] = $rows;
											if($rows['donate']!=0)
											{
												$purify_fee = round(($rows['donate'] - $exp_fee[1]) * 0.05, 2);
												$total_profit_do = $rows['donate'] - $exp_fee[1] - $purify_fee;
													
												$donate_field[$fan_don] = $rows;
												$donate_field[$fan_don]['donate_field'] = 'yes';
											}
											else
											{
												$total_profit_do = 0;
											}
										//}
										$whole_profit = $whole_profit + $total_profit + $total_profit_do;
										$fan_don = $fan_don + 1;
									}
								}
							}
							
							if($whole_profit!=0)
							{
								$sql_got = $trans->get_bal();
								if($sql_got!="" && $sql_got!=NULL)
								{
									if(mysql_num_rows($sql_got)>0)
									{
										$amt_got = 0;
										while($rgot = mysql_fetch_assoc($sql_got))
										{
											$amt_got = $amt_got + $rgot['amount'] + 10;
										}
										
										//echo $amt_got;
										//echo $whole_profit;
										
										$org_pro = round($amt_got - $whole_profit, 2);
										//echo $org_pro;
										if($org_pro<0)
										{
											$org_pro = $org_pro * (-1);
										}
									}
									else
									{
										$org_pro = $whole_profit;
									}
								}
								else
								{
									$org_pro = $whole_profit;
								}

							}
							else
							{
								$org_pro = 0;
							}
							
							//echo $whole_profit;
							//echo $amt_got;
							//echo $org_pro;
							
							$total_rece = 0;
							$no_chks_fees = 0;
							$sql_re_bal = $trans->get_received_bal();
							if(mysql_num_rows($sql_re_bal)>0)
							{
								while($row_bal = mysql_fetch_assoc($sql_re_bal))
								{
									$total_rece = $total_rece + $row_bal['amount'] - 10;
								}
							}
							
							$sql_chk_bal = $trans->get_bal();
							if(mysql_num_rows($sql_chk_bal)>0)
							{
								while($row_bal = mysql_fetch_assoc($sql_chk_bal))
								{
									$no_chks_fees = $no_chks_fees + 1;
								}
							}
							$no_chks_fees = $no_chks_fees * 10;
							
							$all_stores = array_merge($meds_buy,$fans_buy,$donate_field);
							
							$sort = array();
							foreach($all_stores as $k=>$v)
							{
								//if(isset($v['name']) && $v['name']!="")
								//{
								if(isset($v['chk_date']) && $v['chk_date']!="")
								{
									$sort['chk_date'][$k] = strtotime($v['chk_date'].' '.$v['time_chk']);
								}
								elseif(isset($v['date']) && $v['date']!="")
								{
									$sort['chk_date'][$k] = strtotime($v['date'].' '.$v['time_chk']);
								}
								elseif(isset($v['purchase_date']) && $v['purchase_date']!="")
								{
									
									$sort['chk_date'][$k] = strtotime($v['purchase_date']);
								}
								//}									
							}
							//var_dump($sort);
							if(!empty($sort))
							{
								array_multisort($sort['chk_date'], SORT_DESC,$all_stores);
							}
							$hasing = count($all_stores);
						?>                            
							<input type="hidden" value="<?php echo $org_pro; ?>" id="whole" name="whole" />
                            <p style="font-size:18px; line-height:14px;">Balance: $<?php echo $org_pro; ?></p>
							<p style="line-height:14px;">
								Total Sales Profits - $<?php echo $whole_profit; ?> | Total Payments Received - $<?php echo $total_rece; ?> | Total Check Fees - $<?php echo $no_chks_fees; ?>
							</p>
                            <div class="fieldCont">
                                <a title="Request a payment to be sent to you from you current balance. As you make sales your balance goes up." onClick="return chk_amts();" id="req_pay" style="cursor: pointer; text-decoration:none; background-color: #000000; border: 0 none; color: #FFFFFF; float: left; font-family: 'futura-pt',sans-serif; font-size: 0.825em; font-weight: bold; margin-right: 10px; padding: 6px 10px; border-radius:5px; -webkit-border-radius:5px; -moz-border-radius:5px; -ms-border-radius:5px; -o-border-radius:5px;">Request Payment</a>
								<a id="req_pay_new" style="display:none; text-decoration:none; background-color: #000000; border-radius:5px; -webkit-border-radius:5px; -moz-border-radius:5px; -ms-border-radius:5px; -o-border-radius:5px; border: 0 none; color: #FFFFFF; float: left; font-family: 'futura-pt',sans-serif; font-size: 0.825em; font-weight: bold; margin-right: 10px; padding: 6px 10px;" href="request_payment.php?amt=<?php echo $org_pro; ?>" class="fancybox fancybox.ajax">Request Payment</a>
								<a title="Request a payment to be sent to you from you current balance. As you make sales your balance goes up." href="exports_scvs.php" id="req_pay" style="border-radius:5px; -webkit-border-radius:5px; -moz-border-radius:5px; -ms-border-radius:5px; -o-border-radius:5px; cursor: pointer; text-decoration:none; background-color: #000000; border: 0 none; color: #FFFFFF; float: left; font-family: 'futura-pt',sans-serif; font-size: 0.825em; font-weight: bold; margin-right: 10px; padding: 6px 10px;">Download Sales Data</a>
                            </div>
                        </div>
                        <div id="mediaContent">
                            <div class="topLinks">
                                <div class="links" style="float:left;">
                                    
                                    <select class="dropdown" onChange="handleSelection(value)" style="margin-left:0;">
                                        <option value="salesLog">Sales Log</option>
                                        <option value="total">Totals</option>
										<option value="payments">Payments</option>
                                    </select>
                                    <ul>
                                    	<!--<li>|</li>-->
                                        <li></li>
                                        
                                    </ul>
                                </div>
                            </div>
                            <div id="salesLog">
                                
                                    <div class="titleCont">
                                        <div class="blkC" style="width:40px;">#</div>
                                        <div class="blkE">Seller</div>
                                        <div class="blkG">Profits</div>
                                        <div class="blkF">Type</div>
                                        <div class="blkD">Date</div>
                                        
                                        
                                    </div>
									<div style="position:relative;float:left;height:466px;overflow-x:hidden;overflow-y:auto;">
									<?php
										$sort = array();
										foreach($all_stores as $k=>$v)
										{
											//if(isset($v['name']) && $v['name']!="")
											//{
												$sort['chk_date'][$k] = strtotime($v['chk_date'].' '.$v['time_chk']);
											//}									
										}
										
										if($sort!="" && !empty($sort))
										{
											array_multisort($sort['chk_date'], SORT_DESC,$all_stores);
										}
								
										$all_sales = $trans->get_all_trans();
										//if(mysql_num_rows($all_sales)>0)
										$no = 1;
										$all_crets = "";
										$hasing = count($all_stores);
										
										for($se=0;$se<count($all_stores);$se++)
										{
											if(isset($all_stores[$se]['id']))
											{
											//while($rows = mysql_fetch_assoc($all_sales))
											//{
												//$total_trnc = ($rows['total'] * 0.05) + 0.05;
												//$Paypal_Fee = round($total_trnc);
												$exp_fee = explode(',',$all_stores[$se]['paypal_fee']);
												
												$floatNum = $exp_fee[0];
												$length = strlen($floatNum);

												$pos = strpos($floatNum, "."); // zero-based counting.

												$num_of_dec_places = ($length - $pos) - 1;
												/* echo $length;
												echo $pos;
												echo $num_of_dec_places; */
												if($num_of_dec_places==1)
												{
													$exp_fee[0] = $exp_fee[0].'0';
												}
												
												$purify_fee = round(($all_stores[$se]['price'] - $exp_fee[0]) * 0.05, 2);
												$purify_fee_2 = round(($all_stores[$se]['donate'] - $exp_fee[1]) * 0.05, 2);
												/* if($purify_fee<=0)
												{
													$purify_fee = 1;
												} */
												
												if(isset($all_stores[$se]['donate_field']))
												{
													$total_profit = $all_stores[$se]['donate'] - $purify_fee_2 - $exp_fee[1];
												}
												else
												{
													$total_profit = $all_stores[$se]['price'] - $exp_fee[0] - $purify_fee;
												}
												//var_dump($total_profit);
												$float_to = $total_profit;
												$length = strlen($float_to);

												$pos = strpos($float_to, "."); // zero-based counting.

												$num_of_dec_places = ($length - $pos) - 1;
												/* echo $length;
												echo $pos;
												echo $num_of_dec_places; */
												if($pos==0 || $pos ==false)
												{
													$total_profit = $total_profit.'.00';
												}
												elseif($num_of_dec_places==1)
												{
													$total_profit = $total_profit.'0';
												}
												
												$buy = $trans->get_buyer($all_stores[$se]['buyer_id']);
												
												if(isset($all_stores[$se]['media_id']))
												{
													$get_media = $trans->get_media($all_stores[$se]['media_id']);
													if(mysql_num_rows($get_media)>0)
													{
														$gets_meds = mysql_fetch_assoc($get_media);
														$get_creator = $trans->get_creators($gets_meds['creator_info']);
													}
												}
												else
												{
													$gets_meds = "";
													if($all_stores[$se]['table_name']=='artist_project' || $all_stores[$se]['table_name']=='community_project')
													{
														
														$vars = $all_stores[$se]['table_name'].'|'.$all_stores[$se]['type_id'];
														
														$get_vars = $trans->get_creators($vars);
														$get_creator = $trans->get_creators($get_vars['creators_info']);
													}
													elseif($all_stores[$se]['table_name']=='general_artist' || $all_stores[$se]['table_name']=='general_community')
													{
														$vars = $all_stores[$se]['table_name'].'|'.$all_stores[$se]['type_id'];
														$get_creator = $trans->get_creators($vars);
													}
												}
									?>
									
										<div class="tableCont" id="AccordionContainer">
											<div class="blkC"><?php echo $hasing; ?></div>
											<div class="blkE"><?php 
											if($get_creator!="")
											{ 
												if(isset($get_creator['name']))
												{
													echo $get_creator['name'];
												}
												elseif(isset($get_creator['title']))
												{
													echo $get_creator['title'];
												}
											}else{ ?>&nbsp;<?php }
											?></div>
											<div class="blkG" style="width:80px;">$<?php echo $total_profit; ?></div>
											<div class="blkF" style="width:138px;"><?php
											
											if($gets_meds['media_type']==114)
											{
												if(isset($all_stores[$se]['donate_field']))
												{
													echo "Tip";
												}
												else
												{
													echo "Song";
												}
											}
											elseif($gets_meds['media_type']==115)
											{
												if(isset($all_stores[$se]['donate_field']))
												{
													echo "Tip";
												}
												else
												{
													echo "Video";
												}
											}
											elseif($gets_meds['media_type']==113)
											{
												if(isset($all_stores[$se]['donate_field']))
												{
													echo "Tip";
												}
												else
												{
													echo "Gallery";
												}
											}
											else
											{
												if(isset($all_stores[$se]['donate_field']))
												{
													echo "Tip";
												}
												else
												{
													if($all_stores[$se]['fan_member_id']==0)
													{
														
														echo "Project";
													}
													else
													{
														echo "Membership";
													}
												}
											}
											?></div>
											<div class="blkB" style="width:145px;"><?php echo date("m.d.y", strtotime($all_stores[$se]['chk_date']));
											//$all_stores[$se]['chk_date']; ?></div>
											<div class="blkC" onclick="runAccordion('<?php echo $no; ?>_pay');">
												<div onselectstart="return false;"><img id="trcn_info_<?php echo $no; ?>" onclick="lists_views();" src="images/profile/view.jpg" /></div>
											</div>
											<div class="blkD"><a href="#"><img src="images/profile/delete.png" /></a></div>
											<div class="expandable" id="Accordion<?php echo $no; ?>_payContent" style="
											background: none repeat scroll 0 0 #F3F3F3; height: 215px !important; margin-left: 50px;">
												<div class="affiBlock">
													<div class="RowCont">
														<div class="sideL">Item Title :</div>
														<div class="sideR"><?php 
														if(isset($gets_meds) && $gets_meds['title']!="")
														{ 
															if($gets_meds['media_type']==114)
															{
																$get_trck = $trans->get_track($gets_meds['id']);
																if($get_trck!="" && $get_trck['track']!="")
																{
																	echo $get_trck['track'].' '.$gets_meds['title'];
																}
																else
																{
																	echo $gets_meds['title'];
																}
															}
															else
															{
																echo $gets_meds['title'];
															}
														}
														else
														{ 
															if($all_stores[$se]['table_name']=='artist_project' || $all_stores[$se]['table_name']=='community_project')
															{
																if($all_stores[$se]['fan_member_id']==0)
																{
																	echo $get_vars['title'];
																}
																else
																{
																	echo $get_vars['title'].' Fan Club';
																}
															}
															else
															{
																if(isset($get_creator['name']))
																{
																	echo $get_creator['name'].' Fan Club';
																}
																elseif(isset($get_creator['title']))
																{
																	echo $get_creator['title'];
																} 
															} 
														}?></div>
													</div>
													<?php
														if(isset($gets_meds) && $gets_meds['title']!="" && $gets_meds['from_info']!="")
														{ 
															$get_from = $trans->get_from($gets_meds['from_info']);
													?>
															<div class="RowCont">
																<div class="sideL">From :</div>
																<div class="sideR"><?php echo $get_from['title']; ?></div>
															</div>
													<?php
														}
													?>
													<div class="RowCont">
														<div class="sideL">Name :</div>
														<div class="sideR"><?php
														if(isset($all_stores[$se]['IP_address']))
														{
															if($all_stores[$se]['IP_address']!=NULL)
															{
																echo $all_stores[$se]['buy_name'];
															}
															else
															{
																if($buy['fname']=="" && $buy['lname']==""){ echo "N/A"; }else{ echo $buy['fname'].' '.$buy['lname'];}
															}
														}
														else
														{
															if($buy['fname']=="" && $buy['lname']==""){ echo "N/A"; }else{ echo $buy['fname'].' '.$buy['lname'];}
														} ?></div>
													</div>
													<div class="RowCont">
														<div class="sideL">Email :</div>
														<div class="sideR"><?php
														if(isset($all_stores[$se]['IP_address']))
														{
															if($all_stores[$se]['IP_address']!=NULL)
															{
																echo $all_stores[$se]['buy_email'];
															}
															else
															{
																if($buy['email']==""){ echo "N/A"; }else{ echo $buy['email']; }
															}
														}
														else
														{
															if($buy['email']==""){ echo "N/A"; }else{ echo $buy['email']; }
														} ?></div>
													</div>
													<div class="RowCont">
														<div class="sideL">Amount :</div>
														<div class="sideR">$<?php if(isset($all_stores[$se]['donate_field'])) { echo $all_stores[$se]['donate']; }else { echo $all_stores[$se]['price']; } ?></div>
													</div>
													<div class="RowCont">
														<div class="sideL">Paypal Fee :</div>
														<div class="sideR">$<?php if(isset($all_stores[$se]['donate_field'])) { echo $exp_fee[1]; }else{ echo $exp_fee[0]; } ?></div>
													</div>
													<div class="RowCont">
														<div class="sideL">Purify Fee :</div>
														<div class="sideR">$<?php if(isset($all_stores[$se]['donate_field'])) { echo $purify_fee_2; }else{ echo $purify_fee; } ?></div>
													</div>
													<div class="RowCont">
														<div class="sideL">Net Profit :</div>
														<div class="sideR">$<?php echo $total_profit; ?></div>
													</div>
													<!--<div class="RowCont">
														<div class="sideL">Payment :</div>
														<select class="fieldUpload">
															<option value="">Paypal</option>
															<option value="">Xoom</option>
															<option value="">Check</option>
														</select>
													</div>-->
												</div>
											</div>
										</div>
										<?php
											if(isset($all_stores[$se]['media_id']))
											{
												$cart = 'cart';
												$cretr = $gets_meds['creator_info'];
											}
											else
											{
												$cart = 'all';
												if($all_stores[$se]['table_name']=='artist_project' || $all_stores[$se]['table_name']=='community_project')
												{
													$cretr = $get_vars['creators_info'];
												}
												elseif($all_stores[$se]['table_name']=='general_artist' || $all_stores[$se]['table_name']=='general_community')
												{
													$cretr = $get_creator['creators_info'];
												}
											}
											if(!isset($all_stores[$se]['donate_field']))
											{
												$all_crets = $all_crets.','.$cretr.'~'.$all_stores[$se]['id'].'*'.$cart.'@'.$total_profit;
											}
											$no = $no + 1;
												//}
											//}
											}
											$hasing = $hasing - 1;
										}
										?>
									</div>
                            </div>
                            <div id="total" style="display:none;">
								<div class="titleCont">
									<div class="blkD">Year</div>
									<div class="blkB">Month</div>
									<div class="blkE">Total</div>
									<div class="blkE">Creator</div>
								</div>
								<div style="position:relative;float:left;height:466px;overflow-x:hidden;overflow-y:auto;">
<?php
									$same = array();
									if($all_crets!="")
									{
										$exp_crets = explode(',',$all_crets);
										$st = 1;
										for($c=0;$c<count($exp_crets);$c++)
										{
											$exp_ids[$c] = explode('~',$exp_crets[$c]);
											
											$exp_chk_t[$c] = explode('*',$exp_ids[$c][1]);
											$exps_at_rates[$c] = explode('@',$exp_chk_t[$c][1]);
											if($exps_at_rates[$c][0]=='cart')
											{
												$get_trans = $trans->get_trans($exp_chk_t[$c][0]);
											}
											elseif($exps_at_rates[$c][0]=='all')
											{
												$get_trans = $trans->get_trans_fans($exp_chk_t[$c][0]);
											}
											if($get_trans!=NULL)
											{
												if(mysql_num_rows($get_trans)>0)
												{
												
													$ans_trans[$c] = mysql_fetch_assoc($get_trans);
													
													if($exps_at_rates[$c][0]=='all')
													{
														if($exp_ids[$c][0]=='')
														{
															//echo "hii";
															$exp_ids[$c][0] = $ans_trans[$c]['table_name'].'|'.$ans_trans[$c]['type_id'];
															//echo $exp_ids[$c][0];
														}													
													}
													$mth[$c] = date("F",strtotime($ans_trans[$c]['chk_date']));
												
													$yr[$c] = date("Y",strtotime($ans_trans[$c]['chk_date']));
													
													
													$not_all = "";
													for($i=$c-1;$i>=0;$i--)
													{	
														if($exp_ids[$i][0]==$exp_ids[$c][0] && $mth[$c]==$mth[$i] && $yr[$i]==$yr[$c])
														{
															$not_all = $i;
															//echo $i;
														}	
													}
													if($not_all!="")
													{	
														//echo $not_all;
														//$same[$not_all]['amt'] = $same[$not_all]['amt'] + $ans_trans[$c]['total'];	
														
														/*Code Added By Azhar for Adding tip to total starts here*/
														if($ans_trans[$c]['donate']!=0)
														{
															$exp_fee = explode(',',$ans_trans[$c]['paypal_fee']);
															$purify_fee = round(($ans_trans[$c]['donate'] - $exp_fee[1]) * 0.05, 2);
															$total_profit_do1 = $ans_trans[$c]['donate'] - $exp_fee[1] - $purify_fee;
															$same[$not_all]['amt'] = $same[$not_all]['amt'] +$total_profit_do1;
														}
														else
														{
															$total_profit_do1 = 0;
														}
														/*Code Added By Azhar for Adding tip to total ends here*/
														
														
														$same[$not_all]['amt'] = $same[$not_all]['amt'] + $exps_at_rates[$c][1] + $do_total_profit_do[$c];
														$same[$c] = "";
													}
													else
													{
														/*Code Added By Azhar for Adding tip to total starts here*/
														if($ans_trans[$c]['donate']!=0)
														{
															$exp_fee = explode(',',$ans_trans[$c]['paypal_fee']);
															$purify_fee = round(($ans_trans[$c]['donate'] - $exp_fee[1]) * 0.05, 2);
															$total_profit_do2 = $ans_trans[$c]['donate'] - $exp_fee[1] - $purify_fee;
															$same[$c]['amt'] = $same[$c]['amt'] +$total_profit_do2;
														}
														else
														{
															$total_profit_do2 = 0;
														}
														/*Code Added By Azhar for Adding tip to total ends here*/
														
														$same[$c]['creator'] = $exp_ids[$c][0];
														$same[$c]['mth'] = $mth[$c];
														$same[$c]['year'] = $yr[$c];
														//$same[$c]['amt'] = $ans_trans[$c]['total'];
														$same[$c]['amt'] = $exps_at_rates[$c][1] + $do_total_profit_do[$c];
													//	$st = $st + 1;
													}
												}
											}
										}
										
										
										
										if(!empty($same))
										{
											//$same_new = array_unique($same);
											$dummy = array();
											for($r=1;$r<=count($same);$r++)
											{
												if(!empty($same[$r]))
												{
												//if(in_array($same[$r]['creator'],$dummy))
												//{}
												//else
												//{
													//$dummy[] = $same[$r]['creator'];
?>													<div class="tableCont">
														<div class="blkD"><?php echo $same[$r]['year']; ?></div>
														<div class="blkB"><?php echo $same[$r]['mth']; ?></div>
														<div class="blkE">$<?php echo $same[$r]['amt']; ?></div>
														<?php
														$get_creator_name = $trans->get_creators($same[$r]['creator']);
														if($get_creator_name['name']!="" || $get_creator_name['title']!="")
														{
															if($get_creator_name['name']!="")
															{
?>
																<div class="blkE"><?php echo $get_creator_name['name']; ?></div>
<?php
															}
															else
															{
?>
																<div class="blkE"><?php echo $get_creator_name['title']; ?></div>
<?php
															}
														}
														else
														{
														?> &nbsp; <?php
														}
?>
													</div>
<?php
												}
											}
										}
									}
?>
								</div>
                            </div>
							
							<div id="payments" style="display:none;">
								<div class="titleCont">
									<div class="blkB">Date Requested</div>
									<div class="blkA">Amount</div>
									<div class="blkE">Date Sent</div>
								</div>
								<div style="position:relative;float:left;height:466px;overflow-x:hidden;overflow-y:auto;">
<?php
									$sql_got = $trans->get_bal();
									if(mysql_num_rows($sql_got)>0)
									{
										while($ans_got = mysql_fetch_assoc($sql_got))
										{
?>								
											<div class="tableCont">
												<div class="blkB"><?php echo date("m.d.y", strtotime($ans_got['date_sent']));  ?></div>
												<div class="blkA"><?php echo $ans_got['amount'] - 10; ?></div>
												<div class="blkE"><?php if($ans_got['date_received']!='0000-00-00') { echo date("m.d.y", strtotime($ans_got['date_received'])); }else{ echo "Processing"; }?></div>
											</div>
<?php
										}
									}
								?>
								
                            </div>
							</div>
                        </div>
                    </div>
                </div>
				<div id="welcome" class="subTabs" style="display:none;">
					<h1>Welcome to Purify Art</h1>
					<div id="actualContent">
						<p style="width: 485px;">Your base Purify user account has been successfully created. <a href="#general">Personalize</a> your profile by adding images and your home location. <a href="javascript:void(0);" onclick="document.getElementById('searchlink').click();">Search</a> our art community and begin experiencing music, art and videos in a whole new way. <a href="#registration">Register</a> as an Artist, Company, or Team user to start sharing/selling media and building/promoting profile pages.</p>
						<a href="#general" style="background: none repeat scroll 0 0 #515151; border-radius: 5px 5px 5px 5px; color: #FFFFFF; font-size: 14px; padding: 5px 12px; text-decoration: none;">Personalize</a>
						<a href="javascript:void(0);" onclick="document.getElementById('searchlink').click();" style="background: none repeat scroll 0 0 #515151; border-radius: 5px 5px 5px 5px; color: #FFFFFF; font-size: 14px; padding: 5px 12px; text-decoration: none;">Search</a>
						<a href="#registration" style="background: none repeat scroll 0 0 #515151; border-radius: 5px 5px 5px 5px; color: #FFFFFF; font-size: 14px; padding: 5px 12px; text-decoration: none;">Register</a>
					</div>
				</div>
				<div id="chng_pass" class="subTabs" style="display:none;">
					<h1>Change Password</h1>
					<div id="actualContent">
						<form method="POST" autocomplete="off" action=""  id="change_password">
							<div class="fieldCont">
								<div class="fieldTitle">Old Password: </div>
								<input type="password" id="old_pass" class="fieldText" />
							</div>
							
							<div class="fieldCont">
								<div class="fieldTitle">New Password: </div>
								<input type="password" id="new_pass" class="fieldText" />
							</div>
							
							<div class="fieldCont">
								<div class="fieldTitle">Confirm Password: </div>
								<input type="password" id="new_conf_pass" class="fieldText" />
							</div>
							
							<div class="fieldCont">
								<div class="fieldTitle"></div>
								<p style="border: medium none; margin: 0; color: red;" id="error_pass"></p>
							</div>
							
							<div class="fieldCont">
                                <div class="fieldTitle"></div>
                                <input type="button" onClick="return validate_passesform()" id="pass_sub" class="register" value="Save" style="border-radius:5px; -webkit-border-radius:5px; -moz-border-radius:5px; -ms-border-radius:5px; -o-border-radius:5px;" />
                                <input type="button" class="register" value="Cancel" onClick="window.location.href='#profile';" style="border-radius:5px; -webkit-border-radius:5px; -moz-border-radius:5px; -ms-border-radius:5px; -o-border-radius:5px;"/>
                            </div>
							
						</form>
					</div>
				</div>
	<div id="addcontact" class="subTabs" style="display:none;">
<?php
if(isset($_GET['edit']))
{
	$edit=$_GET['edit'];
	$newadd=new AddContact();
	$newvalidate=$newadd->editUser($edit);
	$res=mysql_fetch_assoc($newvalidate);
	//die;
}
?>
					<h1>Add New Contact</h1>
			
                    	<div id="mediaContent">
                            <div class="topLinks">
                                <div class="links" style="width:665px; border-bottom:1px dotted #3d3d3d;">
                                    <ul>
                                        <li><a href="#addressbook" style="color: rgb(255, 255, 255); font-family: 'futura-pt',sans-serif; font-size: 0.825em; font-weight: bold; padding: 6px 10px; float: left; margin-right: 10px; border: 0px none; background-color: rgb(0, 0, 0);text-decoration:none;border-radius:5px;">Back</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
						<div id="actualContent">
						<?php
							if(isset($_GET['edit']))
							{
							?>
								<form action="adduser.php?update=<?php echo $_GET['edit'];?>" method="post" id="useradd" name="useradd" onsubmit="return validate_email()">
							<?php							
							}
							else
							{
							?>
								<form action="adduser.php" method="post" id="useradd" name="useradd" onsubmit="return validate_email()">
								
							<?php
							}
							?>
                            <!--<div class="fieldCont">
								<div class="fieldTitle">Type</div>
								<select class="dropdown" name="user_add_type">
								<?php if(isset($_GET['edit']))
									  {
											if($res['type']=="Registered")
											{
								?>
											<option value="Registered" selected="selected">Registered</option>
											<option value="Unregistered" >Unregistered</option>
								<?php
											}
											else
											{											
								?>
												<option value="Registered" >Registered</option>
												<option value="Unregistered" selected="selected">Unregistered</option>
											<?php
											}
										}
										else
										{
										?>
										<option value="Registered" >Registered</option>
										<option value="Unregistered" >Unregistered</option>
										<?php
										}
										?>
											
                                </select>
							</div>-->
							<div class="fieldCont" style="display:none">
                                <div class="fieldTitle" title="Contact categories help you organize your emails so you can send emails to an entire category.">Category</div>
                                <select title="Contact categories help you organize your emails so you can send emails to an entire category." class="dropdown" name="user_add_category" style="display:none">
                                	<?php
									if(isset($_GET['edit']))
									{
									?>
											<option value="artist" <?php /*if($res['category'] =="artist"){echo "selected";}*/?>>Artist</option>
											<option value="community" <?php /*if($res['category'] =="community"){echo "selected";}*/?>>Community</option>
											<option value="general" selected <?php /*if($res['category'] =="general"){echo "selected";}*/?>>General</option>
									<?php
									}
									else
									{
										?>
											<option value="artist" >Artist</option>
											<option value="community" >Community</option>
											<option value="general" selected>General</option>
										<?php
									}
										?>
                                </select>
                            </div>
                             <div class="fieldCont">
                                <div class="fieldTitle">Name</div>
								<div class="outerdiv">
								<!--<input  type="text" class="fieldText" name="user_add_name" id="user_add_name" value="<?php// if(isset($_GET['name'])) echo $_GET['name']; elseif(isset($_GET['edit'])) echo $res['name'];?>"/>-->
								<input name="user_add_name" type="text" value="<?php if(isset($_GET['edit'])) echo $res['name'];?>"  id="user_add_name" class="fieldText"/>
								<div id="namesuggestiondiv" class="namesuggessionbox"></div>
								</div>
                            </div>
                            <div class="fieldCont">
                                <div class="fieldTitle">Email</div>
								<div class="outerdiv">
                                <!--<input  type="text" class="fieldText" name="user_add_email" id="user_add_email" value="" />-->
								<input type="text" value="<?php if(isset($_GET['edit'])) echo $res['email'];?>" name="user_add_email" id="user_add_email" class="fieldText"/>
								<div id="suggestiondiv" class="suggessionbox"></div>
								</div>
                            </div>
                            
							<?php
							// $validate=mysql_query("SELECT * FROM general_user WHERE email='".$email."'");
							// $res=mysql_fetch_assoc($validate);
							// echo "hii";
							// if($res)
							// {
								// echo "in if";
								
								// $permission=0;
								// die;
							// }
							// else
							// {
								// echo "in else";
								// $permission=1;
								// die;
							// }	
							?>
							 
                            <div class="fieldCont">
                                <div class="fieldTitle"></div>
                                <input type="submit" class="register" value="<?php if(isset($_GET['edit'])) echo "Save";else echo "Add";?>" />
                                <input type="button" class="register" value="Cancel" onClick="window.location.href='profileedit.php#addressbook';"/>
                            </div>
							</form>
						</div>
					
			
     
	  </div>
	  
	  <div id="addmailchimp" class="subTabs" style="display:none;">
			<h1>Sync Mailchimp Account</h1>
			<div id="mediaContent">
				<div class="topLinks">
					<div class="links" style="width:665px; border-bottom:1px dotted #3d3d3d;">
						<ul>
							<li><a href="#addressbook" style="color: rgb(255, 255, 255); font-family: 'futura-pt',sans-serif; font-size: 0.825em; font-weight: bold; padding: 6px 10px; float: left; margin-right: 10px; border: 0px none; background-color: rgb(0, 0, 0);text-decoration:none;border-radius:5px;">Back</a></li>
							<?php
								if(isset($_GET['sync']) && !empty($_GET['sync'])){
								?>
									<script>
										alert("You have synced your addressbook with your mailchimp list. All contacts added to your addressbook will now be entered into your mailchimp list.");
									</script>
								<?php
								}
								$user_data = $uk -> get_Mailchimpkeys($_SESSION['login_id']);
								if(isset($user_data['clientid']) && !empty($user_data['clientid'])){
								?>
									<li><a href="add_mailchimp.php?remove=yes" onclick="show_loader()" style="color: rgb(255, 255, 255); font-family: 'futura-pt',sans-serif; font-size: 0.825em; font-weight: bold; padding: 6px 10px; float: left; margin-right: 10px; border: 0px none; background-color: rgb(0, 0, 0);text-decoration:none;border-radius:5px;">Un Sync</a></li>
								<?php
								}
							?>
							
						</ul>
					</div>
				</div>
			</div>
			<div id="actualContent">
			
				<form action="add_mailchimp.php" method="post" id="mailchimp_account" name="mailchimp_account" onsubmit="show_loader()">
					<div class="fieldCont">
					<?php
						if(isset($user_data['mailchimp_list_id']) && !empty($user_data['mailchimp_list_id'])){
							echo "Your account is synced.";
						}else if(isset($user_data['mailchimp_list_id']) && empty($user_data['mailchimp_list_id'])){
							echo "Select email list from drop down to sync your contacts with mailchimp.";
						}
						?>
					</div>
					<div class="fieldCont">
						<div class="fieldTitle">Mailchimp API Key</div>
						<div class="outerdiv">
							<input name="mailchimp_api" type="text" <?php if(isset($user_data['clientid']) && !empty($user_data['clientid'])){ echo "readonly"; } ?> value="<?php if(isset($user_data['clientid']) && !empty($user_data['clientid'])){ echo $user_data['clientid']; }?>"  id="mailchimp_api" class="fieldText"/>
							<?php
							$user_emails = array_map(function($el){ return $el['email']; }, $pass_email_to_mailchimp);
							$user_names = array_map(function($el){ return $el['name']; }, $pass_email_to_mailchimp);
							$var_user_emails = implode(',', $user_emails);
							$var_user_names = implode(',', $user_names);
							?>
							<input type="hidden" name="all_contacts_email" value="<?php echo $var_user_emails;?>"/>
							<input type="hidden" name="all_contacts_name" value="<?php echo $var_user_names;?>"/>
						</div>
					</div>
					<?php 
					if(!empty($user_data)){
					?>
					<div class="fieldCont">
						<div class="fieldTitle">Mailchimp List ID</div>
						<div class="outerdiv">
						<?php
							
							$api_key = $user_data['clientid'];
							$api = new MCAPI($api_key);
							$get_all_lists = $api->lists();
						?>
							<!--<input type="text" value="" name="mailchimp_list_id" id="mailchimp_list_id" class="fieldText"/>-->
							<select name="mailchimp_list_id" id="mailchimp_list_id" class="dropdown" <?php if(!empty($user_data['mailchimp_list_id'])){ echo "disabled"; } ?>>
								<option value="">Select List</option>
								<?php
								for($list_count=0;$list_count<count($get_all_lists['data']);$list_count++){
									if(isset($user_data['mailchimp_list_id']) && !empty($user_data['mailchimp_list_id']) && $user_data['mailchimp_list_id']==$get_all_lists['data'][$list_count]['id']){
									?>
									<option value="<?php echo $get_all_lists['data'][$list_count]['id'];?>" selected><?php echo $get_all_lists['data'][$list_count]['name'];?></option>
								<?php
									}else{
									?>
									<option value="<?php echo $get_all_lists['data'][$list_count]['id'];?>"><?php echo $get_all_lists['data'][$list_count]['name'];?></option>
								<?php
									}
								}
								?>
							</select>
						</div>
					</div>
					<?php
					}
					?>
					<div class="fieldCont">
						<div class="fieldTitle"></div>
						<?php if(empty($user_data['mailchimp_list_id'])){
						?>
						<input type="submit" class="register" value="Sync" />
						<?php
						}?>
						
					</div>
				</form>
			</div>
	  </div>
    </div>  
</div>
<input type="hidden" id="curr_playing_playlist" name="curr_playing_playlist" >

<?php 
if(isset($_GET['con_exs']) && $_GET['con_exs']==1)
{
?>

<script>
alert("Contact Already Exist.");
</script>	

<?php
}
?>
<script type="text/javascript">
 $(document).ready(function(){
 if(location.hash == "")
 {
		$("#addressbook").show();
 }
 
 
 
 $("a").each(
    function()
    {    
        if($(this).attr("href") == location.hash  && $(this).attr("href") != "" )
        {
           $(this).click();
        }
    });
 
 $(window).hashchange(
  function()
  {
   $("a").each(
   function()
   {    
    if($(this).attr("href") == location.hash  && $(this).attr("href") != "" )
    {
     $(this).click();
    }
   });
  }
 )
 
 });
</script>
<script type="text/javascript">
function confirmDeleteMail(delUrl1) 
{
	if (confirm("Are you sure you want to delete?")) 
	{
		alert("Mail is deleted successfully.");
		document.location = delUrl1;
	}
}

function confirmDeleteSup(delUrl1) 
{
	if (confirm("Are you sure you want to delete?")) 
	{
		alert("Subscription has been deleted successfully.");
		document.location = delUrl1;
	}
}

function alreadyMember(mem_var)
{
	
	/*if(mem_var==1)
	{
		alert("You are already a Member.");
		return false;
	}
	else
	{*/
		var isChecked = $("#member_age").is(":checked");
		if($("#member_ship_type").val() == "select" || $("#payment_mode").val()== "select")
		{
			alert("You Have To Select Member Ship Type.");
			return false;
		}
		else if($("#countrySelectmember").val() ==0 || $("#countrySelectmember").val()==" ")
		{
			alert("You must select country.");
			return false;
		}
		else if($("#stateSelectmember").val() ==0 || $("#stateSelectmember").val()==" ")
		{
			alert("You must select state.");
			return false;
		}
		else if($("#editcitymember").val() =="" || $("#editcitymember").val()==" ")
		{
			alert("You must enter city.");
			return false;
		}
		
		else if(!isChecked)
		{
			alert("You must confirm that you are 18 years old to purchase a Purify Art membership.");
			return false;
		}
		else
		{
			alert("First You Have To Complete Your Payment.");
			return false;
		}
		
	//}
}
</script>



<!--files for search technique
<script type="text/javascript" src="javascripts/jquery.js"></script>
<script type="text/javascript" src="javascripts/jquery.autocomplete.js"></script>
<link rel="stylesheet" href="css/jquery.autocomplete.css" type="text/css" />-->
<!--files for search technique ends here -->
<script type="text/javascript">
function newfunction(id,typ)
{
	$("#play_video_image"+id+typ).css({"background":"none repeat scroll 0 0 #000000","border-radius":"6px 6px 6px 6px"});
}function newfunctionout(id,typ)
{
	$("#play_video_image"+id+typ).css({"background":"none repeat scroll 0 0 #333333","border-radius":"6px 6px 6px 6px"});
}
					
/*
script for suggesting names in address book name field
 $(function() {
		$( "#user_add_name" ).autocomplete({
			source: "namesuggession.php",
			minLength: 1,
			select: function( event, ui ) {
			//document.getElementById("table_name").value = ui.item.value1;
			//document.getElementById("table_id").value = ui.item.value2;			
			}
		});
	});
*/
$(function()
{
	list_post();
	$("#post_list").change(function() 
	{
		//$('.jquery_ckeditor').html = "";
		$("#loader").show();
		$("#fieldCont_media_first").hide();
		$("#fieldCont_media").hide();
		$.post("PostList.php", { type_val:$("#post_list").val() },
		function(data)
		{
			$("#fieldCont_media_first").html(data);
			$("#fieldCont_media").html(data);
			$("#loader").hide();
			$("#fieldCont_media_first").show();
			$("#fieldCont_media").show();
			
		});
	});
	$('.list-wrap').css({"height":""});
});
$(function()
{
	list_post_first();
	$("#post_list_first").change(function() 
	{
		//$('.jquery_ckeditor').html = "";
		$("#loader_first").show();
		$("#fieldCont_media_first").hide();
		$.post("PostList_First.php", { type_val:$("#post_list_first").val() },
		function(data)
		{
			$("#fieldCont_media_first").html(data);
			$("#loader_first").hide();
			$("#fieldCont_media_first").show();
		});
	});
	$('.list-wrap').css({"height":""});
});
function allowpost(ids)
{
	document.getElementById('allowh_post').value = document.getElementById(ids).innerHTML;
	document.getElementById('allow_to').value = document.getElementById('allowh_post').value;
	
	if(document.getElementById('allow_to').value=='All')
	{
		document.getElementById("1o_post").innerHTML = 'Fans';
		document.getElementById("2o_post").innerHTML = 'Friends';
		document.getElementById("3o_post").innerHTML = 'Subscribers';
		$('#allow_to').css({"background-position":"32px"});
		$('#actualContent .fieldContainer').css({"float": "right","margin-bottom": "20px","position": "relative","width":"52px","top": "299px"});
	}
	else if(document.getElementById('allow_to').value=='Fans')
	{
		document.getElementById("1o_post").innerHTML = 'All';
		document.getElementById("2o_post").innerHTML = 'Friends';
		document.getElementById("3o_post").innerHTML = 'Subscribers';
		$('#allow_to').css({"background-position":"40px"});
		$('#actualContent .fieldContainer').css({"float": "right","margin-bottom": "20px","position": "relative","width":"59px","top": "299px"});
	}
	else if(document.getElementById('allow_to').value=='Friends')
	{
		document.getElementById("1o_post").innerHTML = 'All';
		document.getElementById("2o_post").innerHTML = 'Fans';
		document.getElementById("3o_post").innerHTML = 'Subscribers';
		$('#allow_to').css({"background-position":"53px"});
		$('#actualContent .fieldContainer').css({"float": "right","margin-bottom": "20px","position": "relative","width":"72px","top": "299px"});
	}
	else if(document.getElementById('allow_to').value=='Subscribers')
	{
		document.getElementById("1o_post").innerHTML = 'All';
		document.getElementById("2o_post").innerHTML = 'Fans';
		document.getElementById("3o_post").innerHTML = 'Friends';
		$('#allow_to').css({"background-position":"78px"});
		$('#actualContent .fieldContainer').css({"float": "right","margin-bottom": "20px","position": "relative","width":"97px","top": "299px"});
	}
}
function allowpost_first(ids)
{
	document.getElementById('allowh_post_first').value = document.getElementById(ids).innerHTML;
	document.getElementById('allow_to_first').value = document.getElementById('allowh_post_first').value;
	//alert(data);
	if(document.getElementById('allow_to_first').value=='All')
	{
		document.getElementById("1_post").innerHTML = 'Fans';
		document.getElementById("2_post").innerHTML = 'Friends';
		document.getElementById("3_post").innerHTML = 'Subscribers';
		$('#allow_to_first').css({"background-position":"32px"});
		$('#actualContent .fieldContainer').css({"float": "right","margin-bottom": "20px","position": "relative","width":"52px"});
	}
	else if(document.getElementById('allow_to_first').value=='Fans')
	{
		document.getElementById("1_post").innerHTML = 'All';
		document.getElementById("2_post").innerHTML = 'Friends';
		document.getElementById("3_post").innerHTML = 'Subscribers';
		$('#allow_to_first').css({"background-position":"40px"});
		$('#actualContent .fieldContainer').css({"float": "right","margin-bottom": "20px","position": "relative","width":"59px"});
	}
	else if(document.getElementById('allow_to_first').value=='Friends')
	{
		document.getElementById("1_post").innerHTML = 'All';
		document.getElementById("2_post").innerHTML = 'Fans';
		document.getElementById("3_post").innerHTML = 'Subscribers';
		$('#allow_to_first').css({"background-position":"53px"});
		$('#actualContent .fieldContainer').css({"float": "right","margin-bottom": "20px","position": "relative","width":"72px"});
	}
	else if(document.getElementById('allow_to_first').value=='Subscribers')
	{
		document.getElementById("1_post").innerHTML = 'All';
		document.getElementById("2_post").innerHTML = 'Fans';
		document.getElementById("3_post").innerHTML = 'Friends';
		$('#allow_to_first').css({"background-position":"78px"});
		$('#actualContent .fieldContainer').css({"float": "right","margin-bottom": "20px","position": "relative","width":"97px"});
	}
}
function list_post()
{
	$("#loader").show();
	$("#fieldCont_media").hide();
	$.post("PostList.php", { type_val:'images_post' },
	function(data)
	{
		$("#fieldCont_media").html(data);
		$("#loader").hide();
		$("#fieldCont_media").show();
	});
	$('.list-wrap').css({"height":""});
}
function list_post_first()
{
	$("#loader_first").show();
	$("#fieldCont_media_first").hide();
	$.post("PostList_First.php", { type_val:'images_post' },
	function(data)
	{
		$("#fieldCont_media_first").html(data);
		$("#loader_first").hide();
		$("#fieldCont_media_first").show();
	});
	$('.list-wrap').css({"height":""});
}
</script>
<!--Script For Name Suggession Box Ends Here-->

<!--Script For Email Suggession Box Starts Here for address book email field
<script type="text/javascript">
 $(function() {
		$( "#user_add_email" ).autocomplete({
			source: "suggession.php",
			minLength: 1,
			select: function( event, ui ) {
			//document.getElementById("table_name").value = ui.item.value1;
			//document.getElementById("table_id").value = ui.item.value2;			
			}
		});
	});
</script>-->
<script type="text/javascript">
function validate_email()
{
	 var email = document.getElementById("user_add_email").value;
	 var username = document.getElementById("user_add_name").value;
	 var atpos = email.indexOf("@");
	 var dotpos = email.lastIndexOf(".");
	
	 if(atpos<1 || dotpos<atpos+2 || dotpos+2>=email.lenght || email=="")
	   {
		alert("Please enter a valid email address.");
		return false;
	 }
	 else if(email=="")
	 {
		alert("Please Enter a email address.");
		return false;
	 }
	 else if(username=="")
	 {
		alert("Please Enter Name.");
		return false;
	 }
	 
}

function validate_post()
{
	var list_post = document.getElementById("list_epm[]");
	alert(list_post.value);
	if(list_post.value=="" || list_post.value==null)
	{
		alert("Please select any Media to Post.");
		return false;
	}
}
function validate_post_first()
{
	var list_post_first = document.getElementById("list_epm_first[]").value;
	if(list_post_first=="" || list_post_first==null)
	{
		alert("Please select any Media to Post.");
		return false;
	}
}
function confirm_subscriber_delete(s)
{
	var find = confirm("Are You Sure You Want To Delete "+s+" Subscription?");
	if(find == false)
	{
		return false;
	}
}
</script>

<script type="text/javascript">
	//<![CDATA[

		$(function()
		{
			var config = {
				toolbar:
				[
					['Bold', 'Italic', '-', 'NumberedList', 'BulletedList', '-', 'Link', 'Unlink'],
					['UIColor']
				],
				enterMode : CKEDITOR.ENTER_BR
			};

			// Initialize the editor.
			// Callback function can be passed and executed after full instance creation.
			$('.jquery_ckeditor').ckeditor(config);
		});

			//]]>
	</script>
	</div>
	</div>
<?php include_once("displayfooter.php"); ?>
</body>

<div id="memberships_box" style="z-index:99;">
	<a style="cursor:pointer;" id="popupContactClose_member">x</a>
	<p id="contactArea_member" style="color: #666666; font-size: 14px; line-height: 27px; padding: 4px; margin-bottom: 0;">
		You still have pending fan clubs, you may accept or decline them at any time on your <a href="javascript:void(0);" style="background: none repeat scroll 0 0 #515151; border-radius: 5px 5px 5px 5px; color: #FFFFFF; font-size: 14px; padding: 5px 12px; text-decoration: none;" id="close_membership">My Memberships</a> page.
	</p>
</div>
<div id="backgroundPopup_member"></div>
<script type="text/javascript">
$(window).bind("load", function() {
	$(".list-wrap").css({"height":""});
});

function checkload(defImg,Images,orgpath,thumbpath,title,media_id){
	var arr_sep = Images.split(',');
	var title_sep = null;
	title_sep = title.split(',');
	//alert(title_sep);
	var k;
	var data = new Array();
	for(k=0;k<arr_sep.length;k++)
	{
		//arr_sep[k] = arr_sep[k].replace("\"","");
		arr_sep[k] = arr_sep[k].replace(/"/g, '');
		arr_sep[k] = arr_sep[k].replace("[","");
		arr_sep[k] = arr_sep[k].replace("]","");
		data[k] = arr_sep[k];
	}
	
	var q;
	var data1 = new Array();
	for(q=0;q<title_sep.length;q++)
	{
		//arr_sep[k] = arr_sep[k].replace("\"","");
		title_sep[q] = title_sep[q].replace(/"/g, '');
		title_sep[q] = title_sep[q].replace("[","");
		title_sep[q] = title_sep[q].replace("]","");
		data1[q] = title_sep[q];
	}
	
	if(data.length>0){
		$("#gallery-container").html('');
	}
	//$("#bgimg").attr('src','<?php echo $orgPath;?>'+defImg);
	//alert(orgpath+defImg);
	$("#bgimg").attr('src',thumbpath+defImg);
	

	for(var i=0;i<data.length;i++){

		$("#gallery-container").append("<div class='content'><div><a href='"+thumbpath+data[i]+"'><img  src='"+orgpath+data[i]+"' title='"+data1[i]+"' alt='"+data1[i]+"' class='thumb' /></a></div></div>");
	}
	//setTimeout(function(){	loadGallery();	$("#light-gal").css('visibility','visible');	},500);
	loadGallery();
	$("#light-gal").css('visibility','visible');
	$("#projectsTab").css('display','none');
	$("#miniFooter").css('display','none');
	$("#profileTabs").css('display','none');
	$("#mediaTab").css('display','none');
	$(".accountSettings").css('z-index','1');
	

	document.getElementById("gallery-container").style.left = "0px";
	$.ajax({
			type: "POST",
			url: 'update_media.php',
			data: { "media_id":media_id },
			success: function(data){
			}
	});
}



var windowRef=false;
//}
var profileName = "Mithesh";
function showPlayer(tp,ref,act){
//alert(act);
//alert('<?php echo $ans_sql['general_user_id'] ;?>');
	//var windowRef = window.open('player/?tp='+tp+'&ref='+ref+'&source='+source);
	var pl = $("#curr_playing_playlist").val();
	$.ajax({
		type: "POST",
		url: 'player/getMediaInfo.php',
		data: { "tp":tp, "ref":ref, "source":"<?php echo $ans_sql['general_user_id']; ?>", "act":act, "pl":pl, "creator":profileName, "update":"update" },
		success: function(data){
			get_cookie(data);
			var is_chrome = navigator.userAgent.toLowerCase().indexOf('chrome') > -1;
			if(is_chrome ==true){
				WinHeight = 579;
			}else{
				WinHeight = 580;
			}
			windowRef = window.open('', 'formpopup', 'height='+WinHeight+',width=560,resizable=no,scrollbars');
			if(windowRef == null || typeof(windowRef) == "undefined") {
			   alert("The media player is a pop up player to give you uninterrupted streaming while browsing the site. To view it you must allow pop ups from Purify Art in the settings of your browser.");
			   return false;
			}
			if(windowRef.location == "about:blank" || (act!="add" && act!="addvi")){
				windowRef.location.href='combinePlayer/index.php#tp='+tp+'&ref='+ref+'&pl='+data+'&buyer_id='+'<?php echo $_SESSION['gen_user_id'];?>'+'&act=play';
				//jQuery(window).blur();
				windowRef.focus();
			}
			else{
				windowRef.location.href='combinePlayer/index.php#tp='+tp+'&ref='+ref+'&pl='+data+'&buyer_id='+'<?php echo $_SESSION['gen_user_id'];?>';
				windowRef.focus();
			}
		}
	});
}
	
function showPlayer_reg(tp,ref,act,tbl){
	var pl = $("#curr_playing_playlist").val();
	$.ajax({
		type: "POST",
		url: 'player/getMediaInfo.php',
		data: { "tp":tp, "ref":ref, "tbl":tbl, "source":"<?php echo $ans_sql['general_user_id']; ?>", "act":act, "pl":pl, "creator":profileName, "update":"update" },
		success: function(data){
		//alert(data);
			//$("#curr_playing_playlist").val(data);
			get_cookie(data);
			var is_chrome = navigator.userAgent.toLowerCase().indexOf('chrome') > -1;
			if(is_chrome ==true){
				WinHeight = 579;
			}else{
				WinHeight = 580;
			}
			windowRef = window.open('', 'formpopup', 'height='+WinHeight+',width=560,resizable=no,scrollbars');
			if(windowRef == null || typeof(windowRef) == "undefined") {
			   alert("The media player is a pop up player to give you uninterrupted streaming while browsing the site. To view it you must allow pop ups from Purify Art in the settings of your browser.");
			   return false;
			}
			if(windowRef.location == "about:blank" || (act!="add" && act!="addvi" && act!="add_reg" && act!="addvi_reg")){
				windowRef.location.href='combinePlayer/index.php#tp='+tp+'&ref='+ref+'&pl='+data+'&buyer_id='+'<?php echo $_SESSION['gen_user_id'];?>'+'&act=play';
				//jQuery(window).blur();
				windowRef.focus();
			}
			else{
				windowRef.location.href='combinePlayer/index.php#tp='+tp+'&ref='+ref+'&pl='+data+'&buyer_id='+'<?php echo $_SESSION['gen_user_id'];?>';
				//jQuery(window).blur();
				windowRef.focus();
			}
		}
	});
}
function get_cookie(data)
{
	document.cookie="filename=" + data;
	getCookieValue("filename");
}
/*function get_cookie_obj(data)
{
	document.cookie="windowobj=" + data;
	getCookieValue_for_obj("windowobj");
}*/
function getCookieValue(key)
{
	currentcookie = document.cookie;
	if (currentcookie.length > 0)
	{
		firstidx = currentcookie.indexOf(key + "=");
		if (firstidx != -1)
		{
			firstidx = firstidx + key.length + 1;
			lastidx = currentcookie.indexOf(";",firstidx);
			if (lastidx == -1)
			{
				lastidx = currentcookie.length;
			}
			getdata = unescape(currentcookie.substring(firstidx, lastidx));
			$("#curr_playing_playlist").val(unescape(currentcookie.substring(firstidx, lastidx)));
			//return unescape(currentcookie.substring(firstidx, lastidx));
		}
		
	}
	return "";
}
$("document").ready(function(){
window.setInterval("getCookieValue('filename')",500);
});

function find_s3_account(detail)
{
	if(detail == "blank"){
		//alert("");
		document.getElementById("popup_s3_error").click();
		//document.getElementById(id).click();
	}
}

function lists_views()
{
	$("#list_height_incr").css({"height":""});	
}

function chk_amts()
{
	var whole_amt = document.getElementById("whole").value;
	if(whole_amt<=0)
	{
		alert("Your balance is zero. You cannot request the payment.");
		return false;
	}
	else
	{
		$("#req_pay_new").click();
	}
}

$("#close_membership").click(
	function () {
	$("#popupContactClose_member").click();
	window.open('profileedit.php?code_chks_member=trues#mymemberships');
});

$(window).load(function() {
	$("#all_wrap_personal").css({"display":"block"});
	$("#loader_body").css({"display":"none"});
});

function show_loader(){
	$("#all_wrap_personal").css({"display":"none"});
	$("#loader_body").css({"display":"block"});
}
</script>
<?php
	if(isset($_GET['confirm_codes_edit']))
	{
		if($_GET['confirm_codes_edit']!="")
		{
			if($_SERVER['HTTP_REFERER'] == 'example_mems_all.php?confirm_codes='.$_GET['confirm_codes_edit'])
			{
?>
				<script>
					$(document).ready(function() {
						//$("#contactArea_member").html("You still have pending fan clubs, you may accept or decline them at any time on your <a href='profileedit.php#mymemberships' target='_blank'>My Memberships</a> page.");
						centerPopup_member();
						loadPopup_member();	
						return false;
					});
				</script>
<?php
			}
		}
	}
?>
</html>
<style>
	#beome_not:hover { background: none repeat scroll 0 0 #000000 !important; }
	.navButton { margin: 0 0 4px; width: 142px; float: left; position: absolute; left: 33%; top: 35%; }
.playMedia { float: left; margin-left: 1px; }
.playMedia a { height: inherit !important; left: 74px !important; width: inherit !important; border-radius: 6px 6px 6px 6px !important; bottom: 138px !important; clear: both; background: none repeat scroll 0 0 #333333; float:left; }
.playMedia a:hover { background:none repeat scroll 0 0 #000000; }
.playMedia .list_play img { margin-bottom: 0px !important; }
</style>