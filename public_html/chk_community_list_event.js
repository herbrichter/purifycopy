function addcommunityBands()
{
	// Retrieve the elements from the document body
	var textbox = document.getElementById('communitybands');
	if(textbox.value=="")
	{
		alert("Please Enter Some Text");
		return false;
	}
	
	var olds = $("#useremail_old");
	
	if(olds.val() != '' && olds.val() != ',')
	{
		document.getElementById("useremail").value = document.getElementById("useremail").value +","+ olds.val();
		document.getElementById("useremail_old").value="";
	}
	else
	{
		document.getElementById("useremail").value = document.getElementById("useremail").value;
		document.getElementById("useremail_old").value="";
	}
	
	var listbox = document.getElementById('communitybands_tagged[]');
	var rmv_button = document.getElementById("remove");
	var options = document.getElementById('communitybands_tagged[]').options;
	for (i=0; i<options.length; i++)
	{
		if(options[i].value.toLowerCase()==textbox.value.toLowerCase())
		{
			alert(options[i].value + "  Already Exist.");
			//var listbox = document.getElementById('bands_tagged[]');
			//iterate through each option of the listbox
			var a = document.getElementById('useremail').value;
			var aexp = a.split(',');
			for(var count= listbox.options.length+1; count < aexp.length; count++)
			{
			//alert(count);
			//alert(aexp.length);
			//alert(aexp[count]);
			aexp[count]="";
			}
			document.getElementById('useremail').value = "";
			for(var count1=0; count1 < aexp.length; count1++)
			{
				if(aexp[count1] != "")
				{
					document.getElementById('useremail').value = document.getElementById('useremail').value + "," + aexp[count1];
				}
			
			}
			
			return false;
		}
	}
	// Now we need to create a new 'option' tag to add to MyListbox
	var newOption = document.createElement('option');
	newOption.value = textbox.value; // The value that this option will have
	newOption.innerHTML = textbox.value; // The displayed text inside of the <option> tags
	//newOption.selected=true;
	// Finally, add the new option to the listbox
	listbox.appendChild(newOption);
	textbox.value="";
	rmv_button.style.display="block";
}
function add_artist_project_users()
{
	// Retrieve the elements from the document body
	var textbox = document.getElementById('user');
	var olds = $("#useremail_old");
	if(textbox.value=="")
	{
		alert("Please Enter Some Text");
		return false;
	}
	
	if(olds.val() != '' && olds.val() != ',')
	{
		document.getElementById("useremail").value = document.getElementById("useremail").value +","+ olds.val();
		document.getElementById("useremail_old").value="";
	}
	else
	{
		document.getElementById("useremail").value = document.getElementById("useremail").value;
		document.getElementById("useremail_old").value="";
	}
	
	var listbox = document.getElementById('artist_project_tagged_user[]');
	var rmv_button = document.getElementById("remove");
	var options = document.getElementById('artist_project_tagged_user[]').options;
	for (i=0; i<options.length; i++)
	{
		if(options[i].value.toLowerCase()==textbox.value.toLowerCase())
		{
			alert(options[i].value + "  Already Exist.");
			//alert("hii");
			//var listbox = document.getElementById('bands_tagged[]');
			//iterate through each option of the listbox
			var a = document.getElementById('useremail').value;
			var aexp = a.split(',');
			//alert(aexp);
			for(var count= listbox.options.length+1; count < aexp.length; count++)
			{
			//alert(count);
			//alert(aexp.length);
			//alert(aexp[count]);
			aexp[count]="";
			}
			document.getElementById('useremail').value = "";
			for(var count1=0; count1 < aexp.length; count1++)
			{
				if(aexp[count1] != "")
				{
					document.getElementById('useremail').value = document.getElementById('useremail').value + "," + aexp[count1];
				}
			
			}
			
			return false;
		}
	}
	// Now we need to create a new 'option' tag to add to MyListbox
	var newOption = document.createElement('option');
	newOption.value = textbox.value; // The value that this option will have
	newOption.innerHTML = textbox.value; // The displayed text inside of the <option> tags
	//newOption.selected=true;
	// Finally, add the new option to the listbox
	listbox.appendChild(newOption);
	textbox.value="";
	rmv_button.style.display="block";
}
function addcommunityother_artist()
{
	// Retrieve the elements from the document body
	var textbox = document.getElementById('other_artist');
	if(textbox.value=="")
	{
		alert("Please Enter Some Text");
		return false;
	}
	var listbox = document.getElementById('otherartist_tagged[]');
	
	var options = document.getElementById('otherartist_tagged[]').options;
	for (i=0; i<options.length; i++)
	{
		if(options[i].value.toLowerCase()==textbox.value.toLowerCase())
		{
			alert(options[i].value + "  Already Exist.");
			
			//var listbox = document.getElementById('bands_tagged[]');
			//iterate through each option of the listbox
			var a = document.getElementById('useremail').value;
			var aexp = a.split(',');
			for(var count= listbox.options.length+1; count < aexp.length; count++)
			{
			//alert(count);
			//alert(aexp.length);
			//alert(aexp[count]);
			aexp[count]="";
			}
			document.getElementById('useremail').value = "";
			for(var count1=0; count1 < aexp.length; count1++)
			{
				if(aexp[count1] != "")
				{
					document.getElementById('useremail').value = document.getElementById('useremail').value + "," + aexp[count1];
				}
			
			}
			
			return false;
		}
	}
	// Now we need to create a new 'option' tag to add to MyListbox
	var newOption = document.createElement('option');
	newOption.value = textbox.value; // The value that this option will have
	newOption.innerHTML = textbox.value; // The displayed text inside of the <option> tags
	newOption.selected=true;
	// Finally, add the new option to the listbox
	listbox.appendChild(newOption);
	textbox.value="";
}
function addcommunityartist()
{
	// Retrieve the elements from the document body
	var textbox = document.getElementById('communityartistname');
	if(textbox.value=="")
	{
		alert("Please Enter Some Text");
		return false;
	}
	var listbox = document.getElementById('communityartists_tagged[]');
	var options = document.getElementById('communityartists_tagged[]').options;
	for(i=0;i<options.length;i++)
	{
		if(options[i].value.toLowerCase()==textbox.value.toLowerCase())
		{
			alert(options[i].value + "  Already Exist.");
			
			//var listbox = document.getElementById('bands_tagged[]');
			//iterate through each option of the listbox
			var a = document.getElementById('useremail').value;
			var aexp = a.split(',');
			for(var count= listbox.options.length+1; count < aexp.length; count++)
			{
			//alert(count);
			//alert(aexp.length);
			//alert(aexp[count]);
			aexp[count]="";
			}
			document.getElementById('useremail').value = "";
			for(var count1=0; count1 < aexp.length; count1++)
			{
				if(aexp[count1] != "")
				{
					document.getElementById('useremail').value = document.getElementById('useremail').value + "," + aexp[count1];
				}
			
			}
			
			return false;
		}
	}
	// Now we need to create a new 'option' tag to add to MyListbox
	var newOption = document.createElement('option');
	newOption.value = textbox.value; // The value that this option will have
	newOption.innerHTML = textbox.value; // The displayed text inside of the <option> tags
	newOption.selected=true;
	// Finally, add the new option to the listbox
	listbox.appendChild(newOption);
	textbox.value="";
}
function addcommunityguestname()
{
	// Retrieve the elements from the document body
	var textbox = document.getElementById('communityaddguest');
	if(textbox.value=="")
	{
		alert("Please Enter Some Text");
		return false;
	}
	var listbox = document.getElementById('communityguest_list[]');
	var options = document.getElementById('communityguest_list[]').options;
	for(i=0;i<options.length;i++)
	{
		if(options[i].value.toLowerCase()==textbox.value.toLowerCase())
		{
			alert(options[i].value + "  Already Exist.");
			
			//var listbox = document.getElementById('bands_tagged[]');
			//iterate through each option of the listbox
			var a = document.getElementById('useremail').value;
			var aexp = a.split(',');
			for(var count= listbox.options.length+1; count < aexp.length; count++)
			{
			//alert(count);
			//alert(aexp.length);
			//alert(aexp[count]);
			aexp[count]="";
			}
			document.getElementById('useremail').value = "";
			for(var count1=0; count1 < aexp.length; count1++)
			{
				if(aexp[count1] != "")
				{
					document.getElementById('useremail').value = document.getElementById('useremail').value + "," + aexp[count1];
				}
			
			}
			
			return false;
		}
	}
	// Now we need to create a new 'option' tag to add to MyListbox
	var newOption = document.createElement('option');
	newOption.value = textbox.value; // The value that this option will have
	newOption.innerHTML = textbox.value; // The displayed text inside of the <option> tags
	//newOption.selected=true;
	// Finally, add the new option to the listbox
	listbox.appendChild(newOption);
	textbox.value="";
}

function addcommunity_users()
{
	// Retrieve the elements from the document body
	var textbox = document.getElementById('user');
	if(textbox.value=="")
	{
		alert("Please Enter Some Text");
		return false;
	}
	
	var olds = $("#useremail_old");
	
	if(olds.val() != '' && olds.val() != ',')
	{
		document.getElementById("useremail").value = document.getElementById("useremail").value +","+ olds.val();
		document.getElementById("useremail_old").value="";
	}
	else
	{
		document.getElementById("useremail").value = document.getElementById("useremail").value;
		document.getElementById("useremail_old").value="";
	}
	
	var listbox = document.getElementById('communitybands_tagged[]');
	var rmv_button = document.getElementById("remove");
	var options = document.getElementById('communitybands_tagged[]').options;
	for(i=0;i<options.length;i++)
	{
		if(options[i].value.toLowerCase()==textbox.value.toLowerCase())
		{
			alert(options[i].value + "  Already Exist.");
			
			//var listbox = document.getElementById('bands_tagged[]');
			//iterate through each option of the listbox
			var a = document.getElementById('useremail').value;
			var aexp = a.split(',');
			for(var count= listbox.options.length+1; count < aexp.length; count++)
			{
			//alert(count);
			//alert(aexp.length);
			//alert(aexp[count]);
			aexp[count]="";
			}
			document.getElementById('useremail').value = "";
			for(var count1=0; count1 < aexp.length; count1++)
			{
				if(aexp[count1] != "")
				{
					document.getElementById('useremail').value = document.getElementById('useremail').value + "," + aexp[count1];
				}
			
			}
			
			return false;
		}
	}
	// Now we need to create a new 'option' tag to add to MyListbox
	var newOption = document.createElement('option');
	newOption.value = textbox.value; // The value that this option will have
	newOption.innerHTML = textbox.value; // The displayed text inside of the <option> tags
	//newOption.selected=true;
	// Finally, add the new option to the listbox
	listbox.appendChild(newOption);
	textbox.value="";
	rmv_button.style.display="block";
}


function addBands()
{
	// Retrieve the elements from the document body
	var textbox = document.getElementById('bands');
	if(textbox.value=="")
	{
		alert("Please Enter Some Text");
		return false;
	}
	
	var olds = $("#useremail_old");
	
	if(olds.val() != '' && olds.val() != ',')
	{
		document.getElementById("useremail").value = document.getElementById("useremail").value +","+ olds.val();
		document.getElementById("useremail_old").value="";
	}
	else
	{
		document.getElementById("useremail").value = document.getElementById("useremail").value;
		document.getElementById("useremail_old").value="";
	}
	
	var listbox = document.getElementById('bands_tagged[]');
	var rmv_button = document.getElementById("remove");
	var options = document.getElementById('bands_tagged[]').options;
	var optiontext,text;
	for (i=0; i<options.length; i++)
	{
	//	optiontext=options[i].value.toLowerCase();
		if(options[i].value.toLowerCase()==textbox.value.toLowerCase())
		{
			alert(options[i].value + "  Already Exist.");
			
			var listbox = document.getElementById('bands_tagged[]');
			//iterate through each option of the listbox
			var a = document.getElementById('useremail').value;
			var aexp = a.split(',');
			for(var count= listbox.options.length+1; count < aexp.length; count++)
			{
			//alert(count);
			//alert(aexp.length);
			//alert(aexp[count]);
			aexp[count]="";
			}
			document.getElementById('useremail').value = "";
			for(var count1=0; count1 < aexp.length; count1++)
			{
				if(aexp[count1] != "")
				{
					document.getElementById('useremail').value = document.getElementById('useremail').value + "," + aexp[count1];
				}
			
			}
				/*
				alert(aexp.length);
				alert(count);
				if()
				return false;
				
					if(aexp2[count + 1] == aexp[count + 1])
					{
						aexp2[count + 1] = "";
						//document.getElementById('useremail').value = "";
						for(var xz=0;xz< aexp2.length;xz++ )
						{
							if(aexp2[xz] != "")
							{
								document.getElementById('useremail').value = document.getElementById('useremail').value + "," + aexp2[xz];
							}
						}
					}
				//}
			}*/
			
			
			return false;
		}
	}
	// Now we need to create a new 'option' tag to add to MyListbox
	var newOption = document.createElement('option');
	newOption.value = textbox.value; // The value that this option will have
	newOption.innerHTML = textbox.value; // The displayed text inside of the <option> tags
	//newOption.selected=true;
	// Finally, add the new option to the listbox
	listbox.appendChild(newOption);
	textbox.value="";
	rmv_button.style.display="block";
}
function addartist()
{
	// Retrieve the elements from the document body
	var textbox = document.getElementById('artistname');
	if(textbox.value=="")
	{
		alert("Please Enter Some Text");
		return false;
	}
	var listbox = document.getElementById('artist_tagged[]');
	var options = document.getElementById('artist_tagged[]').options;
	for(i=0;i<options.length;i++)
	{
		if(options[i].value.toLowerCase()==textbox.value.toLowerCase())
		{
			alert(options[i].value + "  Already Eist.");
			
			var listbox = document.getElementById('bands_tagged[]');
			//iterate through each option of the listbox
			for(var count= listbox.options.length-1; count >= 0; count--)
			{
				var a = document.getElementById('useremail').value;
				var aexp = a.split(',');
				 //if the option is selected, delete the option
				//if(listbox.options[count].selected == true)
				//{
					listbox.remove(count);
					var aexp2 = a.split(',');
					if(aexp2[count + 1] == aexp[count + 1])
					{
						aexp2[count + 1] = "";
						document.getElementById('useremail').value = "";
						for(var xz=0;xz< aexp2.length;xz++ )
						{
							if(aexp2[xz] != "")
							{
								document.getElementById('useremail').value = document.getElementById('useremail').value + "," + aexp2[xz];
							}
						}
					}
				//}
			}
			
			return false;
		}
	}
	// Now we need to create a new 'option' tag to add to MyListbox
	var newOption = document.createElement('option');
	newOption.value = textbox.value; // The value that this option will have
	newOption.innerHTML = textbox.value; // The displayed text inside of the <option> tags
	newOption.selected=true;
	// Finally, add the new option to the listbox
	listbox.appendChild(newOption);
	textbox.value="";
}
function addguestname()
{
	// Retrieve the elements from the document body
	var textbox = document.getElementById('addguest');
	if(textbox.value=="")
	{
		alert("Please Enter Some Text");
		return false;
	}
	var listbox = document.getElementById('guest_list[]');
	var options = document.getElementById('guest_list[]').options;
	for(i=0;i<options.length;i++)
	{
		if(options[i].value.toLowerCase()==textbox.value.toLowerCase())
		{
			alert(options[i].value + "  Already xist.");
			
			var listbox = document.getElementById('bands_tagged[]');
			//iterate through each option of the listbox
			for(var count= listbox.options.length-1; count >= 0; count--)
			{
				var a = document.getElementById('useremail').value;
				var aexp = a.split(',');
				 //if the option is selected, delete the option
				//if(listbox.options[count].selected == true)
				//{
					listbox.remove(count);
					var aexp2 = a.split(',');
					if(aexp2[count + 1] == aexp[count + 1])
					{
						aexp2[count + 1] = "";
						document.getElementById('useremail').value = "";
						for(var xz=0;xz< aexp2.length;xz++ )
						{
							if(aexp2[xz] != "")
							{
								document.getElementById('useremail').value = document.getElementById('useremail').value + "," + aexp2[xz];
							}
						}
					}
				//}
			}
			
			return false;
		}
	}
	// Now we need to create a new 'option' tag to add to MyListbox
	var newOption = document.createElement('option');
	newOption.value = textbox.value; // The value that this option will have
	newOption.innerHTML = textbox.value; // The displayed text inside of the <option> tags
	//newOption.selected=true;
	// Finally, add the new option to the listbox
	listbox.appendChild(newOption);
	textbox.value="";
}

/***********************************/
/*******Function for Add_artist_event.php ********/
/***********************************/
function remove_artist_event_user()
{
	var listbox = document.getElementById('bands_tagged[]');
    //iterate through each option of the listbox
    for(var count= listbox.options.length-1; count >= 0; count--)
	{
		var a = document.getElementById('useremail').value;
		var aexp = a.split(',');
         //if the option is selected, delete the option
        if(listbox.options[count].selected == true)
		{
            listbox.remove(count);
			var aexp2 = a.split(',');
			if(aexp2[count + 1] == aexp[count + 1])
			{
				var rem_email = document.getElementById('useremail_removed').value;
				if(rem_email != "")
				{
					var rem_email_split = rem_email.split(',');
					for(var rem_count=0;rem_count<rem_email_split.length;rem_count++)
					{
						if(rem_email_split[rem_count]==""){continue;}
						else{
							if(rem_email_split[rem_count] !=aexp2[count + 1])
							{
								document.getElementById('useremail_removed').value = document.getElementById('useremail_removed').value + "," + aexp2[count + 1];
							}
						}
					}
				}
				else
				{
					document.getElementById('useremail_removed').value = document.getElementById('useremail_removed').value + "," + aexp2[count + 1];
				}
				aexp2[count + 1] = "";
				document.getElementById('useremail').value = "";
				for(var xz=0;xz< aexp2.length;xz++ )
				{
					if(aexp2[xz] != "")
					{
						document.getElementById('useremail').value = document.getElementById('useremail').value + "," + aexp2[xz];
					}
				}
			}
        }
    }
	if(listbox.options.length==0 || listbox.options.length== "")
	{
		var rmv_button = document.getElementById("remove");
		rmv_button.style.display="none";
	}

}
/***********************************/
/*******Function for Add_artist_project.php ********/
/***********************************/
function remove_artist_project_user()
{
	var listbox = document.getElementById('artist_project_tagged_user[]');
    //iterate through each option of the listbox
    for(var count= listbox.options.length-1; count >= 0; count--)
	{
		var a = document.getElementById('useremail').value;
		var aexp = a.split(',');
         //if the option is selected, delete the option
        if(listbox.options[count].selected == true)
		{
            listbox.remove(count);
			var aexp2 = a.split(',');
			if(aexp2[count + 1] == aexp[count + 1])
			{
				var rem_email = document.getElementById('useremail_removed').value;
				if(rem_email != "")
				{
					var rem_email_split = rem_email.split(',');
					for(var rem_count=0;rem_count<rem_email_split.length;rem_count++)
					{
						if(rem_email_split[rem_count]==""){continue;}
						else{
							if(rem_email_split[rem_count] !=aexp2[count + 1])
							{
								document.getElementById('useremail_removed').value = document.getElementById('useremail_removed').value + "," + aexp2[count + 1];
							}
						}
					}
				}
				else
				{
					document.getElementById('useremail_removed').value = document.getElementById('useremail_removed').value + "," + aexp2[count + 1];
				}
				aexp2[count + 1] = "";
				document.getElementById('useremail').value = "";
				for(var xz=0;xz< aexp2.length;xz++ )
				{
					if(aexp2[xz] != "")
					{
						document.getElementById('useremail').value = document.getElementById('useremail').value + "," + aexp2[xz];
					}
				}
			}
        }
    }
	if(listbox.options.length==0 || listbox.options.length== "")
	{
		var rmv_button = document.getElementById("remove");
		rmv_button.style.display="none";
	}
}

/***********************************/
/*******Function for Add_artist_project.php Ends Here ********/
/***********************************/






/***********************************/
/*******Function for Add_community_project.php Ends Here ********/
/***********************************/
function remove_community_user()
{
	var listbox = document.getElementById('communitybands_tagged[]');
    //iterate through each option of the listbox
    for(var count= listbox.options.length-1; count >= 0; count--)
	{
		var a = document.getElementById('useremail').value;
		var aexp = a.split(',');
         //if the option is selected, delete the option
        if(listbox.options[count].selected == true)
		{
            listbox.remove(count);
			var aexp2 = a.split(',');
			if(aexp2[count + 1] == aexp[count + 1])
			{
				var rem_email = document.getElementById('useremail_removed').value;
				if(rem_email != "")
				{
					var rem_email_split = rem_email.split(',');
					for(var rem_count=0;rem_count<rem_email_split.length;rem_count++)
					{
						if(rem_email_split[rem_count]==""){continue;}
						else{
							if(rem_email_split[rem_count] !=aexp2[count + 1])
							{
								document.getElementById('useremail_removed').value = document.getElementById('useremail_removed').value + "," + aexp2[count + 1];
							}
						}
					}
				}
				else
				{
					document.getElementById('useremail_removed').value = document.getElementById('useremail_removed').value + "," + aexp2[count + 1];
				}
				aexp2[count + 1] = "";
				document.getElementById('useremail').value = "";
				for(var xz=0;xz< aexp2.length;xz++ )
				{
					if(aexp2[xz] != "")
					{
						document.getElementById('useremail').value = document.getElementById('useremail').value + "," + aexp2[xz];
					}
				}
			}
        }
    }
	if(listbox.options.length==0 || listbox.options.length== "")
	{
		var rmv_button = document.getElementById("remove");
		rmv_button.style.display="none";
	}
}

/***********************************/
/*******Function for Add_community_project.php Ends Here ********/
/***********************************/


/***********************************/
/*******Function for Add_community_event.php ********/
/***********************************/
function remove_community_event_user()
{
	var listbox = document.getElementById('communitybands_tagged[]');
    //iterate through each option of the listbox
    for(var count= listbox.options.length-1; count >= 0; count--)
	{
		var a = document.getElementById('useremail').value;
		var aexp = a.split(',');
         //if the option is selected, delete the option
        if(listbox.options[count].selected == true)
		{
            listbox.remove(count);
			var aexp2 = a.split(',');
			if(aexp2[count + 1] == aexp[count + 1])
			{
				var rem_email = document.getElementById('useremail_removed').value;
				if(rem_email != "")
				{
					var rem_email_split = rem_email.split(',');
					for(var rem_count=0;rem_count<rem_email_split.length;rem_count++)
					{
						if(rem_email_split[rem_count]==""){continue;}
						else{
							if(rem_email_split[rem_count] !=aexp2[count + 1])
							{
								document.getElementById('useremail_removed').value = document.getElementById('useremail_removed').value + "," + aexp2[count + 1];
							}
						}
					}
				}
				else
				{
					document.getElementById('useremail_removed').value = document.getElementById('useremail_removed').value + "," + aexp2[count + 1];
				}
				aexp2[count + 1] = "";
				document.getElementById('useremail').value = "";
				for(var xz=0;xz< aexp2.length;xz++ )
				{
					if(aexp2[xz] != "")
					{
						document.getElementById('useremail').value = document.getElementById('useremail').value + "," + aexp2[xz];
					}
				}
			}
        }
    }
	if(listbox.options.length==0 || listbox.options.length== "")
	{
		var rmv_button = document.getElementById("remove");
		rmv_button.style.display="none";
	}
}
/***********************************/
/*******Function for Add_community_event.php Ends Here ********/
/***********************************/
$(document).ready(function(){
	var listbox1 = document.getElementById('bands_tagged[]');
	var listbox2 = document.getElementById('artist_project_tagged_user[]');
	var listbox3 = document.getElementById('communitybands_tagged[]');
	if(listbox1!=null && listbox1.options.length>0)
	{
		var rmv_button = document.getElementById("remove");
		rmv_button.style.display="block";
	}
	//alert(listbox2.options);
	if(listbox2!=null && listbox2.options.length>0)
	{
		var rmv_button = document.getElementById("remove");
		rmv_button.style.display="block";
	}
	if(listbox3!=null && listbox3.options.length>0)
	{
		var rmv_button = document.getElementById("remove");
		rmv_button.style.display="block";
	}
});