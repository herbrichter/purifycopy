<?php
	session_start();
	ob_start();
	include_once('commons/db.php');
	include_once('loggedin_includes.php');
	include_once('classes/ProfileType.php');
	include_once('classes/Commontabs.php');
	include_once('classes/CountryState.php');
	include_once('classes/SuggestSearch.php');
	//include('recaptchalib.php');
	include_once('classes/Media.php');
	include_once('classes/ProfileDisplay.php');
	include_once("AjaxButtonsTestCom.php");
	
	$display = new ProfileDisplay();
	$search_criteria = new SuggestSearch();
	$button_test_com = new AjaxButtonsTestCom();
	$newtab = new Commontabs();
	$objMedia = new Media();
	
	$down_button = "";
	if($_SESSION['login_email']!=NULL)
	{
		$down_button = $button_test_com->Community_project($_SESSION['login_id']);
	}
	
	if(isset($_POST['view_type']))
	{
		if($_POST['view_type']!=NULL)
		{				
			if($_POST['profile_type_search']!='select' || $_POST['select-category-artist']!=0 || $_POST['select-category-artist-subtype']!=0 || $_POST['select-category-artist-metatype']!=0 || $_POST['countrySelect']!=0 || $_POST['stateSelect']!=0 || $_POST['city_search']!='' || $_POST['q_match']!="Search" || $_POST['alphs']!="" || $_POST['tab_name']=='recent' || $_POST['tab_name']=='popular')
			{
				$profile_search = $search_criteria->searching($_POST['profile_type_search'],$_POST['select-category-artist'],$_POST['select-category-artist-subtype'],$_POST['select-category-artist-metatype'],$_POST['countrySelect'],$_POST['stateSelect'],$_POST['city_search'],$_POST['q_match'],$_POST['q_table'],$_POST['q_id'],$_POST['alphs'],$_POST['tab_name']);
			}
			if(!empty($profile_search))
			{
				if($_POST['view_type']=='popular')
				{
					$sort = array();
					foreach($profile_search as $k=>$v)
					{
						if(isset($v['page_views']))
						{
							$sort['counts_pros'][$k] = $v['page_views'];
						}
						elseif(isset($v['play_count']))
						{
							$sort['counts_pros'][$k] = $v['play_count'];
						}
					}
					array_multisort($sort['counts_pros'], SORT_DESC,$profile_search);
				}
				elseif($_POST['view_type']=='recent')
				{
					$sort = array();
					foreach($profile_search as $k=>$v)
					{
						$prore_sorts = "";
						if(isset($v['artist_id']) && !isset($v['id']))
						{
							$prore_sorts = $search_criteria->get_recents($v['artist_id'],'general_artist');
						}
						elseif(isset($v['community_id']) && !isset($v['id']))
						{
							$prore_sorts = $search_criteria->get_recents($v['community_id'],'general_community');
						}
						elseif(isset($v['community_id']) && isset($v['id']) && isset($v['start_time']))
						{
							$prore_sorts = $search_criteria->get_recents($v['id'],'community_event');
						}
						elseif(isset($v['artist_id']) && isset($v['id']) && isset($v['start_time']))
						{
							$prore_sorts = $search_criteria->get_recents($v['id'],'artist_event');
						}
						elseif(isset($v['community_id']) && isset($v['id']) && !isset($v['start_time']))
						{
							$prore_sorts = $search_criteria->get_recents($v['id'],'community_project');
						}
						elseif(isset($v['artist_id']) && isset($v['id']) && !isset($v['start_time']))
						{
							$prore_sorts = $search_criteria->get_recents($v['id'],'artist_project');
						}
						elseif(!isset($v['artist_id']) && !isset($v['community_id']) && isset($v['id']))
						{
							$prore_sorts = $search_criteria->get_recents($v['id'],'general_media');
						}
						if($prore_sorts!="")
						{
							$sort['recents_pros'][$k] = strtotime($prore_sorts['date']);
						}
						/* if(isset($v['page_views']))
						{
							$sort['counts_pros'][$k] = $v['page_views'];
						}
						elseif(isset($v['play_count']))
						{
							$sort['counts_pros'][$k] = $v['play_count'];
						} */
					}
					array_multisort($sort['recents_pros'], SORT_DESC,$profile_search);
				}
				for($i_search_m=0;$i_search_m<count($profile_search);$i_search_m++)
				{
			?>
					<div class="tabItem">
						<div style="position:relative; float:left; height: 200px;">
			<?php
						if(($_POST['profile_type_search']=='artist') || ($profile_search[$i_search_m]['artist_id']!="" && $profile_search[$i_search_m]['id']=="" && $_POST['profile_type_search']!='media'))
						{
							if($profile_search[$i_search_m]['image_name']!="")
							{
								//echo "1";
								$thumb_expa = explode('_',$profile_search[$i_search_m]['image_name']);
								if($thumb_expa[0]=='thumbnail')
								{
									if($profile_search[$i_search_m]['profile_url']!="")
									{
			?>
										<a href="<?php echo '/'.$profile_search[$i_search_m]['profile_url']; ?>" style="text-decoration: none;"><img class="imgs_pro" src="http://artjcropprofile.s3.amazonaws.com/<?php echo $profile_search[$i_search_m]['image_name']; ?>" width="200" height="200" /></a>
			<?php
									}
									else
									{
			?>
										<img class="imgs_pro" src="http://artjcropprofile.s3.amazonaws.com/<?php echo $profile_search[$i_search_m]['image_name']; ?>" width="200" height="200" />
			<?php
									}
								}
								else
								{
									if($profile_search[$i_search_m]['profile_url']!="")
									{
			?>
										<a href="<?php echo '/'.$profile_search[$i_search_m]['profile_url']; ?>" style="text-decoration: none;"><img class="imgs_pro" src="http://arteventprofile.s3.amazonaws.com/Noimage.png" width="200" height="200" /></a>
			<?php
									}
									else
									{
			?>
										<img class="imgs_pro" src="http://arteventprofile.s3.amazonaws.com/Noimage.png" width="200" height="200" />
			<?php
									}
								}
							}
							else
							{
								//echo "2";
								if($profile_search[$i_search_m]['profile_url']!="")
								{
			?>
									<a href="<?php echo '/'.$profile_search[$i_search_m]['profile_url']; ?>" style="text-decoration: none;"><img class="imgs_pro" src="http://arteventprofile.s3.amazonaws.com/Noimage.png" width="200" height="200" /></a>
			<?php
								}
								else
								{
			?>
									<img class="imgs_pro" src="http://arteventprofile.s3.amazonaws.com/Noimage.png" width="200" height="200" />
			<?php
								}
							}
			?>
							</div>
			<?php				
						}
						if($_POST['profile_type_search']=='community' || ($profile_search[$i_search_m]['community_id']!="" && $profile_search[$i_search_m]['id']=="" && $_POST['profile_type_search']!='media')) 
						{
							if($profile_search[$i_search_m]['image_name']!="")
							{
											//echo "3";
											$thumb_expc = explode('_',$profile_search[$i_search_m]['image_name']);
											if($thumb_expc[0]=='thumbnail')
											{
												if($profile_search[$i_search_m]['profile_url']!="")
												{
			?>
													<a href="<?php echo '/'.$profile_search[$i_search_m]['profile_url']; ?>" style="text-decoration: none;"><img class="imgs_pro" src="http://comjcropprofile.s3.amazonaws.com/<?php echo $profile_search[$i_search_m]['image_name']; ?>" width="200" height="200" /></a>
			<?php
												}
												else
												{
			?>
													<img class="imgs_pro" src="http://comjcropprofile.s3.amazonaws.com/<?php echo $profile_search[$i_search_m]['image_name']; ?>" width="200" height="200" />
			<?php
												}
											}
											else
											{
												if($profile_search[$i_search_m]['profile_url']!="")
												{
			?>
													<a href="<?php echo '/'.$profile_search[$i_search_m]['profile_url']; ?>" style="text-decoration: none;"><img class="imgs_pro" src="http://arteventprofile.s3.amazonaws.com/Noimage.png" width="200" height="200" /></a>
			<?php
												}
												else
												{
			?>
													<img class="imgs_pro" src="http://arteventprofile.s3.amazonaws.com/Noimage.png" width="200" height="200" />
			<?php
												}
											}
										}
										else
										{
											//echo "4";
											if($profile_search[$i_search_m]['profile_url']!="")
											{
			?>
												<a href="<?php echo '/'.$profile_search[$i_search_m]['profile_url']; ?>" style="text-decoration: none;"><img class="imgs_pro" src="http://arteventprofile.s3.amazonaws.com/Noimage.png" width="200" height="200" /></a>
			<?php
											}
											else
											{
			?>
												<img class="imgs_pro" src="http://arteventprofile.s3.amazonaws.com/Noimage.png" width="200" height="200" />
			<?php
											}
										}
			?>
										</div>
			<?php
						}
									if($_POST['profile_type_search']=='event')
									{
										if($profile_search[$i_search_m]['image_name']!="")
										{
											//echo "5";
											if($profile_search[$i_search_m]['profile_url']!="")
											{
			?>
												<a href="<?php echo '/'.$profile_search[$i_search_m]['profile_url']; ?>" style="text-decoration: none;"><img class="imgs_pro" src="<?php echo $profile_search[$i_search_m]['image_name']; ?>" width="200" height="200" /></a>
			<?php
											}
											else
											{
			?>
												<img class="imgs_pro" src="<?php echo $profile_search[$i_search_m]['image_name']; ?>" width="200" height="200" />
			<?php
											}
										}
										else
										{
											//echo "6";
											if($profile_search[$i_search_m]['profile_url']!="")
											{
			?>
												<a href="<?php echo '/'.$profile_search[$i_search_m]['profile_url']; ?>" style="text-decoration: none;"><img class="imgs_pro" src="http://arteventprofile.s3.amazonaws.com/Noimage.png" width="200" height="200" /></a>
			<?php
											}
											else
											{
			?>
												<img class="imgs_pro" src="http://arteventprofile.s3.amazonaws.com/Noimage.png" width="200" height="200" />
			<?php
											}
										}
			?>
										</div>
			<?php
									}
									if($_POST['profile_type_search']=='project' || (($profile_search[$i_search_m]['artist_id']=="" || $profile_search[$i_search_m]['community_id']=="") && $profile_search[$i_search_m]['id']!="" && $_POST['profile_type_search']!='event' && $_POST['profile_type_search']!='media'))
									{
										// echo "hi_if12";
										// var_dump($profile_search);
										if($profile_search[$i_search_m]['image_name']!="")
										{
											//echo "7";
											if($profile_search[$i_search_m]['profile_url']!="")
											{
			?>
												<a href="<?php echo '/'.$profile_search[$i_search_m]['profile_url']; ?>" style="text-decoration: none;"><img class="imgs_pro" src="<?php echo $profile_search[$i_search_m]['image_name']; ?>" width="200" height="200" /></a>
			<?php
											}
											else
											{
			?>
												<img class="imgs_pro" src="<?php echo $profile_search[$i_search_m]['image_name']; ?>" width="200" height="200" />
			<?php
											}
										}
										
										if($profile_search[$i_search_m]['media_type']=='114')
										{
											//echo "8";
											$img_info = $search_criteria->get_audio_img($profile_search[$i_search_m]['id']);
											if($img_info !="" || $img_info != null)
											{
											?>
												<a href="javascript:void(0);"><img onMouseOver="newfunction('<?php echo $i_search_m; ?>')" onMouseOut="newfunctionout('<?php echo $i_search_m; ?>')" class="imgs_pro" src="<?php echo $img_info;?>" onclick="showPlayer('a','<?php if($profile_search[$i_search_m]['id']!=""){echo $profile_search[$i_search_m]['id'];}else{echo $profile_search[$i_search_m]['id'];}?>','play');" width="200" height="200" /></a>
												
												<div class="navButton">
													<div class="playMedia">
														<a class="list_play" href="javascript:void(0);"><img onMouseOver="newfunction('<?php echo $i_search_m; ?>')" onMouseOut="newfunctionout('<?php echo $i_search_m; ?>')" src="/images/listing/play_youtube.png" onclick="showPlayer('a','<?php if($profile_search[$i_search_m]['id']!=""){echo $profile_search[$i_search_m]['id'];}else{echo $profile_search[$i_search_m]['id'];}?>','play');" id="play_video_image<?php echo $i_search_m; ?>" title="Add file to the bottom of your playlist without stopping current media." /></a>
													</div>
												</div>
											<?php
											}
											else{
											?>
												<a href="javascript:void(0);"><img onMouseOver="newfunction('<?php echo $i_search_m; ?>')" onMouseOut="newfunctionout('<?php echo $i_search_m; ?>')" class="imgs_pro" src="http://arteventprofile.s3.amazonaws.com/Noimage.png" nclick="showPlayer('a','<?php if($profile_search[$i_search_m]['id']!=""){echo $profile_search[$i_search_m]['id'];}else{echo $profile_search[$i_search_m]['id'];}?>','play');" width="200" height="200" /></a>
												
												<div class="navButton">
													<div class="playMedia">
														<a class="list_play" href="javascript:void(0);"><img onMouseOver="newfunction('<?php echo $i_search_m; ?>')" onMouseOut="newfunctionout('<?php echo $i_search_m; ?>')" src="/images/listing/play_youtube.png" onclick="showPlayer('a','<?php if($profile_search[$i_search_m]['id']!=""){echo $profile_search[$i_search_m]['id'];}else{echo $profile_search[$i_search_m]['id'];}?>','play');" id="play_video_image<?php echo $i_search_m; ?>" title="Add file to the bottom of your playlist without stopping current media." /></a>
													</div>
												</div>
											<?php	
											}
										}
										if($profile_search[$i_search_m]['media_type']=='115')
										{
										
											$tables = 'media_video';
											$get_video_image = $search_criteria->getvideoimage($tables,$profile_search[$i_search_m]['id']);
											?>
												<a onclick="showPlayer('v','<?php echo $profile_search[$i_search_m]['id']; ?>','playlist');" href="javascript:void(0);">
													<img onMouseOver="newfunction('<?php echo $i_search_m; ?>')" onMouseOut="newfunctionout('<?php echo $i_search_m; ?>')" class="imgs_pro" src="https://img.youtube.com/vi/<?php echo $get_video_image; ?>/default.jpg" width="200" height="200" />
												</a>
												<div class="navButton">
													<div class="playMedia">
														<a class="list_play" href="javascript:void(0);"><img onMouseOver="newfunction('<?php echo $i_search_m; ?>')" onMouseOut="newfunctionout('<?php echo $i_search_m; ?>')" src="/images/listing/play_youtube.png" onclick="showPlayer('v','<?php echo $profile_search[$i_search_m]['id']; ?>','playlist');" id="play_video_image<?php echo $i_search_m;?>" title="Add file to the bottom of your playlist without stopping current media." /></a>
													</div>
												</div>
											<?php
										}
										if($profile_search[$i_search_m]['media_type']=='113')
										{
											$get_image = $search_criteria->get_media_images($profile_search[$i_search_m]['id']);
											$orgPath="http://medgallery.s3.amazonaws.com/";
											$thumbPath="http://medgalthumb.s3.amazonaws.com/";
											$all_images ="";
											//$all_images_title =""; 
											for($cg_i=0;$cg_i<count($get_image);$cg_i++)
											{
												$mediaSrc = $get_image[0]['image_name'];
												$all_images = $all_images .",".$get_image[$cg_i]['image_name'];
												$all_images_title = $all_images_title .",".$get_image[$cg_i]['image_title'];
											}
											$all_images = ltrim($all_images,',');
											$all_images_title = ltrim($all_images_title,',');
											$img_info = $search_criteria->get_gal_img($profile_search[$i_search_m]['id']);
											if(!empty($img_info))
											{
			?>
												<a href="javascript:void(0);" "checkload('<?php echo $mediaSrc;?>','<?php echo $all_images; ?>','<?php echo $thumbPath; ?>','<?php echo $orgPath; ?>','<?php echo $all_images_title; ?>','<?php echo $profile_search[$i_search_m]['id'];?>')">
													<img onMouseOver="newfunction('<?php echo $i_search_m; ?>')" onMouseOut="newfunctionout('<?php echo $i_search_m; ?>')" class="imgs_pro" src="<?php echo $img_info; ?>" width="200" height="200" />
												</a>
												
												<div class="navButton">
													<div class="playMedia">
														<a class="list_play" href="javascript:void(0);"><img id="play_video_image<?php echo $i_search_m;?>" onMouseOver="newfunction('<?php echo $i_search_m; ?>')" onMouseOut="newfunctionout('<?php echo $i_search_m; ?>')" src="/images/listing/play_youtube.png" onclick="checkload('<?php echo $mediaSrc;?>','<?php echo $all_images; ?>','<?php echo $thumbPath; ?>','<?php echo $orgPath; ?>','<?php echo $all_images_title; ?>','<?php echo $profile_search[$i_search_m]['id'];?>')" title="Add file to the bottom of your playlist without stopping current media." /></a>
													</div>
												</div>
			<?php
											}
											else
											{
			?>
												<a href="javascript:void(0);" onclick="checkload('<?php echo $mediaSrc;?>','<?php echo $all_images; ?>','<?php echo $thumbPath; ?>','<?php echo $orgPath; ?>','<?php echo $all_images_title; ?>','<?php echo $profile_search[$i_search_m]['id'];?>')">
													<img onMouseOver="newfunction('<?php echo $i_search_m; ?>')" onMouseOut="newfunctionout('<?php echo $i_search_m; ?>')" class="imgs_pro" src="http://arteventprofile.s3.amazonaws.com/Noimage.png" width="200" height="200" />
												</a>	
												
												<div class="navButton">
													<div class="playMedia">
														<a class="list_play" href="javascript:void(0);"><img id="play_video_image<?php echo $i_search_m;?>" onMouseOver="newfunction('<?php echo $i_search_m; ?>')" onMouseOut="newfunctionout('<?php echo $i_search_m; ?>')" src="/images/listing/play_youtube.png" onclick="checkload('<?php echo $mediaSrc;?>','<?php echo $all_images; ?>','<?php echo $thumbPath; ?>','<?php echo $orgPath; ?>','<?php echo $all_images_title; ?>','<?php echo $profile_search[$i_search_m]['id'];?>')" title="Add file to the bottom of your playlist without stopping current media." /></a>
													</div>
												</div>
			<?php
											}
											$mediaSrc = "";
											$all_images = "";
											$all_images_title = "";
										}
										if($profile_search[$i_search_m]['media_type']=='116')
										{
											$img_info = $search_criteria->get_channel_img($profile_search[$i_search_m]['id']);
											if(!empty($img_info))
											{
			?>
												<!--<img src="http://medgalthumb.s3.amazonaws.com/<?php// echo $img_info; ?>" width="200" height="200" />-->
												<a href="javascript:void(0);"><img onMouseOver="newfunction('<?php echo $i_search_m; ?>')" onMouseOut="newfunctionout('<?php echo $i_search_m; ?>')" onclick="PlayAll('channel_media_video<?php echo $i_search_m;?>','channel_media_song<?php echo $i_search_m; ?>')" class="imgs_pro" src="<?php echo $img_info; ?>" width="200" height="200" /></a>
												<div class="navButton">
													<div class="playMedia">
														<a class="list_play" href="javascript:void(0);"><img id="play_video_image<?php echo $i_search_m;?>" onMouseOver="newfunction('<?php echo $i_search_m; ?>')" onMouseOut="newfunctionout('<?php echo $i_search_m; ?>')" src="/images/listing/play_youtube.png" onclick="PlayAll('channel_media_video<?php echo $i_search_m;?>','channel_media_song<?php echo $i_search_m;?>')" title="Add file to the bottom of your playlist without stopping current media." /></a>
													</div>
												</div>
			<?php
											}
											else
											{
			?>
												<a href="javascript:void(0);"><img onMouseOver="newfunction('<?php echo $i_search_m; ?>')" onMouseOut="newfunctionout('<?php echo $i_search_m; ?>')" onclick="PlayAll('channel_media_video<?php echo $i_search_m;?>','channel_media_song<?php echo $i_search_m;?>')" class="imgs_pro" src="http://arteventprofile.s3.amazonaws.com/Noimage.png" width="200" height="200" /></a>
												<div class="navButton">
													<div class="playMedia">
														<a class="list_play" href="javascript:void(0);"><img id="play_video_image<?php echo $i_search_m;?>" onMouseOver="newfunction('<?php echo $i_search_m; ?>')" onMouseOut="newfunctionout('<?php echo $i_search_m; ?>')" src="/images/listing/play_youtube.png" onclick="PlayAll('channel_media_video<?php echo $i_search_m;?>','channel_media_song<?php echo $i_search_m;?>')" title="Add file to the bottom of your playlist without stopping current media." /></a>
													</div>
												</div>
			<?php
											}
										}	
			?>
										</div>
			<?php
									}
									if(($profile_search[$i_search_m]['artist_id']=="" || $profile_search[$i_search_m]['community_id']=="") && $profile_search[$i_search_m]['id']=="" && ($profile_search[$i_search_m]['video_id']!="" || $profile_search[$i_search_m]['audio_id']!="" || $profile_search[$i_search_m]['gallery_id']!="") && $_POST['profile_type_search']!='event' && $_POST['profile_type_search']!='media')
									{
										//echo "hi_if";
										// var_dump($profile_search);
										if($profile_search[$i_search_m]['profile_id']!="" && ($profile_search[$i_search_m]['audio_id']!="" || $profile_search[$i_search_m]['video_id']!="") && $profile_search[$i_search_m]['table_name']!="")
										{
											//echo "1 if";
											$profile_img_audio_video_get = $search_criteria->get_audio_video_reg_img($profile_search[$i_search_m]['profile_id'],$profile_search[$i_search_m]['table_name']);
											
											if($profile_img_audio_video_get!='0')
											{
												//echo "12 if";
												if($profile_search[$i_search_m]['table_name']=='general_artist_audio' || $profile_search[$i_search_m]['table_name']=='general_artist_video')
												{
													
			?>
													<img class="imgs_pro" src="http://artjcropprofile.s3.amazonaws.com/<?php echo $profile_img_audio_video_get; ?>" width="200" height="200" />
			<?php
												}
												elseif($profile_search[$i_search_m]['table_name']=='general_community_audio' || $profile_search[$i_search_m]['table_name']=='general_community_video')
												{
			?>
													<img class="imgs_pro" src="http://comjcropprofile.s3.amazonaws.com/<?php echo $profile_img_audio_video_get; ?>" width="200" height="200" />
			<?php
												}
											}
											else
											{
			?>
												<img class="imgs_pro" src="http://arteventprofile.s3.amazonaws.com/Noimage.png" width="200" height="200" />
			<?php
											}
										}
										
										if($profile_search[$i_search_m]['profile_id']!="" && $profile_search[$i_search_m]['gallery_id']!="")
										{
											$profile_img_audio_video_get = $search_criteria->get_gal_reg_img($profile_search[$i_search_m]['profile_id'],$profile_search[$i_search_m]['table_name'],$profile_search[$i_search_m]['gallery_id']);
			?>
											<img class="imgs_pro" src="http://reggalthumb.s3.amazonaws.com/<?php echo $img_info; ?>" width="200" height="200" />
			<?php
										}
			?>
										</div>
			<?php
									}
									if($_POST['profile_type_search']=='media')
									{
										//var_dump($profile_search[$i_search_m]['from_info']);
										if($profile_search[$i_search_m]['id']!="")
										{
											//echo "21";
											if($profile_search[$i_search_m]['media_type']=='114')
											{
												//echo "22";
												$img_info = $search_criteria->get_audio_img($profile_search[$i_search_m]['id']);
												if($img_info!=""){
												?>
													<a href="javascript:void(0);"><img onMouseOver="newfunction('<?php echo $i_search_m; ?>')" onMouseOut="newfunctionout('<?php echo $i_search_m; ?>')" class="imgs_pro" src="<?php echo $img_info;?>" width="200" height="200" onclick="showPlayer('a','<?php if($profile_search[$i_search_m]['id']!=""){echo $profile_search[$i_search_m]['id'];}else{echo $profile_search[$i_search_m]['id'];}?>','play');" /></a>
													
													<div class="navButton">
														<div class="playMedia">
															<a class="list_play" href="javascript:void(0);"><img id="play_video_image<?php echo $i_search_m;?>" onMouseOver="newfunction('<?php echo $i_search_m; ?>')" onMouseOut="newfunctionout('<?php echo $i_search_m; ?>')" src="/images/listing/play_youtube.png" onclick="showPlayer('a','<?php if($profile_search[$i_search_m]['id']!=""){echo $profile_search[$i_search_m]['id'];}else{echo $profile_search[$i_search_m]['id'];}?>','play');" title="Add file to the bottom of your playlist without stopping current media." /></a>
														</div>
													</div>
												<?php
												}
												else{
											?>
												<a href="javascript:void(0);"><img onMouseOver="newfunction('<?php echo $i_search_m; ?>')" onMouseOut="newfunctionout('<?php echo $i_search_m; ?>')" class="imgs_pro" src="http://arteventprofile.s3.amazonaws.com/Noimage.png" width="200" height="200" onclick="showPlayer('a','<?php if($profile_search[$i_search_m]['id']!=""){echo $profile_search[$i_search_m]['id'];}else{echo $profile_search[$i_search_m]['id'];}?>','play');" /></a>
												
												<div class="navButton">
													<div class="playMedia">
														<a class="list_play" href="javascript:void(0);"><img id="play_video_image<?php echo $i_search_m;?>" onMouseOver="newfunction('<?php echo $i_search_m; ?>')" onMouseOut="newfunctionout('<?php echo $i_search_m; ?>')" src="/images/listing/play_youtube.png" onclick="showPlayer('a','<?php if($profile_search[$i_search_m]['id']!=""){echo $profile_search[$i_search_m]['id'];}else{echo $profile_search[$i_search_m]['id'];}?>','play');" title="Add file to the bottom of your playlist without stopping current media." /></a>
													</div>
												</div>
											<?php	
												}
												
											}
											if($profile_search[$i_search_m]['media_type']=='115')
											{
												$tables = 'media_video';
												$get_video_image = $search_criteria->getvideoimage($tables,$profile_search[$i_search_m]['id']);
												?>
													<a onclick="showPlayer('v','<?php echo $profile_search[$i_search_m]['id']; ?>','playlist');" href="javascript:void(0);">
														<img onMouseOver="newfunction('<?php echo $i_search_m; ?>')" onMouseOut="newfunctionout('<?php echo $i_search_m; ?>')" class="imgs_pro" src="https://img.youtube.com/vi/<?php echo $get_video_image; ?>/default.jpg" width="200" height="200" />
													</a>
													<div class="navButton">
														<div class="playMedia">
															<a class="list_play" href="javascript:void(0);"><img id="play_video_image<?php echo $i_search_m;?>" onMouseOver="newfunction('<?php echo $i_search_m; ?>')" onMouseOut="newfunctionout('<?php echo $i_search_m; ?>')" src="/images/listing/play_youtube.png" onclick="showPlayer('v','<?php echo $profile_search[$i_search_m]['id']; ?>','playlist');" title="Add file to the bottom of your playlist without stopping current media." /></a>
														</div>
													</div>
												<?php
											}
											if($profile_search[$i_search_m]['media_type']=='113')
											{
												$get_image = $search_criteria->get_media_images($profile_search[$i_search_m]['id']);
												$orgPath="http://medgallery.s3.amazonaws.com/";
												$thumbPath="http://medgalthumb.s3.amazonaws.com/";
												$all_images ="";
												for($cg_i=0;$cg_i<count($get_image);$cg_i++)
												{
													$mediaSrc = $get_image[0]['image_name'];
													$all_images = $all_images .",".$get_image[$cg_i]['image_name'];
													//if($get_image[$cg_i]['image_title'] !="" || $get_image[$cg_i]['image_title']!= " " || $get_image[$cg_i]['image_title'] != null){
													$all_images_title = $all_images_title .",".$get_image[$cg_i]['image_title'];//}
												}
												$all_images = ltrim($all_images,',');
												$all_images_title = ltrim($all_images_title,',');
												
												$img_info = $search_criteria->get_gal_img($profile_search[$i_search_m]['id']);
												if(!empty($img_info))
												{
				?>
													<a href="javascript:void(0);" onclick=onclick="checkload('<?php echo $mediaSrc;?>','<?php echo $all_images; ?>','<?php echo $thumbPath; ?>','<?php echo $orgPath; ?>','<?php echo $all_images_title; ?>','<?php echo $profile_search[$i_search_m]['id'];?>')">
														<img onMouseOver="newfunction('<?php echo $i_search_m; ?>')" onMouseOut="newfunctionout('<?php echo $i_search_m; ?>')" class="imgs_pro" src="<?php echo $img_info; ?>" width="200" height="200" />
													</a>
													
													<div class="navButton">
														<div class="playMedia">
															<a class="list_play" href="javascript:void(0);"><img id="play_video_image<?php echo $i_search_m;?>" onMouseOver="newfunction('<?php echo $i_search_m; ?>')" onMouseOut="newfunctionout('<?php echo $i_search_m; ?>')" src="/images/listing/play_youtube.png" onclick="checkload('<?php echo $mediaSrc;?>','<?php echo $all_images; ?>','<?php echo $thumbPath; ?>','<?php echo $orgPath; ?>','<?php echo $all_images_title; ?>','<?php echo $profile_search[$i_search_m]['id'];?>')" title="Add file to the bottom of your playlist without stopping current media." /></a>
														</div>
													</div>
				<?php
												}
												else
												{
				?>
													<a href="javascript:void(0);" onclick="checkload('<?php echo $mediaSrc;?>','<?php echo $all_images; ?>','<?php echo $thumbPath; ?>','<?php echo $orgPath; ?>','<?php echo $all_images_title; ?>','<?php echo $profile_search[$i_search_m]['id'];?>')">
														<img onMouseOver="newfunction('<?php echo $i_search_m; ?>')" onMouseOut="newfunctionout('<?php echo $i_search_m; ?>')" class="imgs_pro" src="http://arteventprofile.s3.amazonaws.com/Noimage.png" width="200" height="200" />
													</a>	
													
													<div class="navButton">
														<div class="playMedia">
															<a class="list_play" href="javascript:void(0);"><img id="play_video_image<?php echo $i_search_m;?>" onMouseOver="newfunction('<?php echo $i_search_m; ?>')" onMouseOut="newfunctionout('<?php echo $i_search_m; ?>')" src="/images/listing/play_youtube.png" onclick="checkload('<?php echo $mediaSrc;?>','<?php echo $all_images; ?>','<?php echo $thumbPath; ?>','<?php echo $orgPath; ?>','<?php echo $all_images_title; ?>','<?php echo $profile_search[$i_search_m]['id'];?>')" title="Add file to the bottom of your playlist without stopping current media." /></a>
														</div>
													</div>
				<?php
												}
												$mediaSrc = "";
												$all_images = "";
												$all_images_title = "";
											}
											if($profile_search[$i_search_m]['media_type']=='116')
											{
												$img_info = $search_criteria->get_channel_img($profile_search[$i_search_m]['id']);
												if(!empty($img_info))
												{
				?>
													<a href="javascript:void(0);"><img onMouseOver="newfunction('<?php echo $i_search_m; ?>')" onMouseOut="newfunctionout('<?php echo $i_search_m; ?>')" onclick="PlayAll('channel_media_video<?php echo $i_search_m;?>','channel_media_song<?php echo $i_search_m;?>')" class="imgs_pro" src="<?php echo $img_info; ?>" width="200" height="200" /></a>
													
													<div class="navButton">
														<div class="playMedia">
															<a class="list_play" href="javascript:void(0);"><img id="play_video_image<?php echo $i_search_m;?>" onMouseOver="newfunction('<?php echo $i_search_m; ?>')" onMouseOut="newfunctionout('<?php echo $i_search_m; ?>')" src="/images/listing/play_youtube.png" onclick="PlayAll('channel_media_video<?php echo $i_search_m;?>','channel_media_song<?php echo $i_search_m;?>')" title="Add file to the bottom of your playlist without stopping current media." /></a>
														</div>
													</div>
				<?php
												}
												else
												{
				?>
													<a href="javascript:void(0);"><img onMouseOver="newfunction('<?php echo $i_search_m; ?>')" onMouseOut="newfunctionout('<?php echo $i_search_m; ?>')" onclick="PlayAll('channel_media_video<?php echo $i_search_m;?>','channel_media_song<?php echo $i_search_m;?>')" class="imgs_pro" src="http://arteventprofile.s3.amazonaws.com/Noimage.png" width="200" height="200" /></a>
													
													<div class="navButton">
														<div class="playMedia">
															<a class="list_play" href="javascript:void(0);"><img id="play_video_image<?php echo $i_search_m;?>" onMouseOver="newfunction('<?php echo $i_search_m; ?>')" onMouseOut="newfunctionout('<?php echo $i_search_m; ?>')" src="/images/listing/play_youtube.png" onclick="PlayAll('channel_media_video<?php echo $i_search_m;?>','channel_media_song<?php echo $i_search_m;?>')" title="Add file to the bottom of your playlist without stopping current media." /></a>
														</div>
													</div>
				<?php
												}
											}
										}
										else
										{
											//echo "26";
			?>
											<img class="imgs_pro" src="http://arteventprofile.s3.amazonaws.com/Noimage.png" width="200" height="200" />
			<?php
										}
			?>
										</div>
			<?php
									}
			?>
										<div class="both_wrap_per">
									<?php
									if($profile_search[$i_search_m]['media_type']==114)
									{
										include_once("findsale_song.php");
									?>
										<div class="player">
											<a href="javascript:void(0);"><img src="../images/profile/play.gif"  onmouseover="this.src='../images/profile/play-over.gif'" onmouseout="this.src='../images/profile/play.gif'" onclick="showPlayer('a','<?php if($profile_search[$i_search_m]['id']!=""){echo $profile_search[$i_search_m]['id'];}else{echo $profile_search[$i_search_m]['id'];}?>','play');" title="Play file now." /></a>
											<a href="javascript:void(0);"><img src="../images/profile/add.gif"  onmouseover="this.src='../images/profile/add-over.gif'" onmouseout="this.src='../images/profile/add.gif'" onclick="showPlayer('a','<?php if($profile_search[$i_search_m]['id']!=""){echo $profile_search[$i_search_m]['id'];}else{echo $profile_search[$i_search_m]['id'];}?>','add');" title="Add file to the bottom of your playlist without stopping current media."/></a>
											<?php 
											if($profile_search[$i_search_m]['sharing_preference']==5 || $profile_search[$i_search_m]['sharing_preference']==2)
											{
												$free_down_rel ="";
												$stats = 0;
												if($_SESSION['login_email']!=NULL)
												{
													if($profile_search[$i_search_m]['id']!="")
													{
														$stats = $display->checkdownloaded($profile_search[$i_search_m]['id'],$_SESSION['login_id']);
													}
													else
													{
														$stats = $display->checkdownloaded($profile_search[$i_search_m]['id'],$_SESSION['login_id']);
													}
												}
													
												if($stats==1)
													{
														$free_down_rel = "yes";
													}
													else
													{
													for($find_down=0;$find_down<=count($get_songs_pnew_w);$find_down++)
														{	
														if($get_songs_pnew_w[$find_down]['id']== $profile_search[$i_search_m]['id']){
														$free_down_rel = "yes";
														}
														else{
															if($free_down_rel =="" || $free_down_rel =="no"){
																$free_down_rel = "no";
															}
														}
													}
												}
												if($free_down_rel == "yes"){
													//if($profile_search[$i_search_m]['sharing_preference']==2)
													//{
												 ?>
												<a href="javascript:void(0);" onclick="download_directly_song('<?php echo $profile_search[$i_search_m]['id']; ?>','<?php echo str_replace("'","\'",$profile_search[$i_search_m]['title']);?>','<?php echo str_replace("'","\'",$profile_search[$i_search_m]['creator']);?>','<?php echo str_replace("'","\'",$profile_search[$i_search_m]['from']);?>')"><img src="../images/profile/download_down.gif" onmouseover="this.src='../images/profile/download-over_down.gif'" onmouseout="this.src='../images/profile/download_down.gif'" title="Add media to your cart for free download or purchase." /></a>
												 
											 <?php
													//}
													/* elseif($profile_search[$i_search_m]['sharing_preference']==5)
													{
											?>
														<a href="javascript:void(0);" onclick="download_directly_song('<?php echo $profile_search[$i_search_m]['id'];?>','<?php echo str_replace("'","\'",$profile_search[$i_search_m]['title']);?>','<?php echo str_replace("'","\'",$profile_search[$i_search_m]['creator']);?>','<?php echo str_replace("'","\'",$profile_search[$i_search_m]['from']);?>')"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>
											<?php
													} */
												}
												else if($profile_search[$i_search_m]['sharing_preference']==2){
												?>
													<a href="javascript:void(0);" onclick="ask_to_donate('<?php echo str_replace("'","\'",$profile_search[$i_search_m]['title']);?>','<?php echo str_replace("'","\'",$profile_search[$i_search_m]['creator']);?>','<?php echo $profile_search[$i_search_m]['id'];?>','download_song<?php echo $profile_search[$i_search_m]['id'];?>')"><img src="../images/profile/download_down.gif" onmouseover="this.src='../images/profile/download-over_down.gif'" onmouseout="this.src='../images/profile/download_down.gif'" title="Add media to your cart for free download or purchase." /></a>
													<!--<img src="../images/profile/download-over.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download-over.gif'" title="Add media to your cart for free download or purchase." />-->
												<?php
												}
												else if($profile_search[$i_search_m]['sharing_preference']==5){
											?>
												<a href="javascript:void(0);" onclick="down_song_pop(<?php echo $profile_search[$i_search_m]['id'];?>)"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>
											<?php
												}
											}else{
											?>
											<!--<img src="images/profile/download-dis.gif" />-->
											<?php
											}
											$song_image_name_cart = $search_criteria->get_audio_img($profile_search[$i_search_m]['id']);
											?>
											<a href="addtocart.php?seller_id=<?php echo $profile_search[$i_search_m]['general_user_id'];?>&media_id=<?php echo $profile_search[$i_search_m]['id']; ?>&buyer_id=<?php echo $_SESSION['gen_user_id']; ?>&title=<?php echo str_replace("'","\'",$profile_search[$i_search_m]['title']);?>&image_name=<?php echo $song_image_name_cart;?>" id="download_song<?php echo $profile_search[$i_search_m]['id']; ?>" style="display:none;"></a>
											
											<script type="text/javascript">
											$(document).ready(function() {
													$("#download_song<?php echo $profile_search[$i_search_m]['id']; ?>").fancybox({
													'width'				: '75%',
													'height'			: '75%',
													'transitionIn'		: 'none',
													'transitionOut'		: 'none',
													'type'				: 'iframe'
													});
											});
											</script>
										</div>
									<?php
									}if($profile_search[$i_search_m]['media_type']==116)
									{
										$get_channel_media = $search_criteria ->get_all_media_selected_channel($profile_search[$i_search_m]['id']);
									?>
										<input type="hidden" id="channel_media_song<?php echo $i_search_m;?>" value="<?php echo $get_channel_media['songs']; ?>"/>
										<input type="hidden" id="channel_media_video<?php echo $i_search_m;?>" value="<?php echo $get_channel_media['videos']; ?>"/>
										<div class="player">
											<a href="javascript:void(0);"><img src="../images/profile/play.gif"  onmouseover="this.src='../images/profile/play-over.gif'" onmouseout="this.src='../images/profile/play.gif'" onclick="PlayAll('channel_media_video<?php echo $i_search_m;?>','channel_media_song<?php echo $i_search_m;?>')" title="Play file now."/></a>
											<a href="javascript:void(0);"><img src="../images/profile/add.gif"  onmouseover="this.src='../images/profile/add-over.gif'" onmouseout="this.src='../images/profile/add.gif'" onclick="PlayAll('channel_media_video<?php echo $i_search_m;?>','channel_media_song<?php echo $i_search_m;?>')" title="Add file to the bottom of your playlist without stopping current media."/></a>
											<!--<img src="images/profile/download-dis.gif" />-->
										</div>
									<?php
									}else if($profile_search[$i_search_m]['media_type']==115){
										include_once("findsale_video.php");
									?>
										<div class="player">
											<a href="javascript:void(0);"><img src="../images/profile/play.gif"  onmouseover="this.src='../images/profile/play-over.gif'" onmouseout="this.src='../images/profile/play.gif'" onclick="showPlayer('v','<?php if($profile_search[$i_search_m]['id']!=""){echo $profile_search[$i_search_m]['id'];}else{echo $profile_search[$i_search_m]['id'];}?>','playlist');" title="Play file now."/></a>
											<a href="javascript:void(0);"><img src="../images/profile/add.gif"  onmouseover="this.src='../images/profile/add-over.gif'" onmouseout="this.src='../images/profile/add.gif'" onclick="showPlayer('v','<?php if($profile_search[$i_search_m]['id']!=""){echo $profile_search[$i_search_m]['id'];}else{echo $profile_search[$i_search_m]['id'];}?>','addvi');" title="Add file to the bottom of your playlist without stopping current media."/></a>
											<?php
											if($profile_search[$i_search_m]['sharing_preference'] == 5 || $profile_search[$i_search_m]['sharing_preference'] == 2)
											{
												$free_down_rel_video="";
												for($find_down=0;$find_down<=count($get_vids_pnew_w);$find_down++)
												{
													if($get_vids_pnew_w[$find_down]['id']==$profile_search[$i_search_m]['id']){
														$free_down_rel_video = "yes";
													}
													else{
														if($free_down_rel_video =="" || $free_down_rel_video =="no"){
															$free_down_rel_video = "no";
														}
													}
												}
												if($free_down_rel_video == "yes"){
												 ?>
												<!-- <a href="javascript:void(0);" onclick="download_directly_song('<?php echo $profile_search[$i_search_m]['id']; ?>','<?php echo str_replace("'","\'",$profile_search[$i_search_m]['title']);?>','<?php echo str_replace("'","\'",$profile_search[$i_search_m]['creator']);?>','<?php echo str_replace("'","\'",$profile_search[$i_search_m]['from']);?>')"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>
												<img src="../images/profile/download-over.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download-over.gif'" title="Add media to your cart for free download or purchase." />-->
												 
											 <?php
												}
												else if($profile_search[$i_search_m]['sharing_preference']==2){
												?>
													<!--<a href="javascript:void(0);" onclick="ask_to_donate('<?php echo str_replace("'","\'",$profile_search[$i_search_m]['title']);?>','<?php echo str_replace("'","\'",$profile_search[$i_search_m]['creator']);?>','<?php echo $profile_search[$i_search_m]['id'];?>','download_video<?php echo $profile_search[$i_search_m]['id'];?>')"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>
													<img src="../images/profile/download-over.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download-over.gif'" title="Add media to your cart for free download or purchase." />-->
												<?php
												}
												else{
											?>
												<!--<a href="javascript:void(0);" onclick="down_video_pop('<?php echo $profile_search[$i_search_m]['id'];?>','<?php echo str_replace("'","\'",$profile_search[$i_search_m]['title']);?>','<?php echo str_replace("'","\'",$profile_search[$i_search_m]['creator']);?>')"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>
												<img src="../images/profile/download-over.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download-over.gif'" title="Add media to your cart for free download or purchase." />-->
											<?php
												}
											}else{
											?>
												<!--<img src="../images/profile/download-over.gif" />-->
											<?php
											}
											$tables = 'media_video';
											$get_video_image = $search_criteria->getvideoimage($tables,$profile_search[$i_search_m]['id']);
											$image_name_cart = "https://img.youtube.com/vi/".$get_video_image."/default.jpg";
											?>
											<a href="addtocart.php?seller_id=<?php echo $profile_search[$i_search_m]['general_user_id'];?>&media_id=<?php echo $profile_search[$i_search_m]['id'];?>&buyer_id=<?php echo $_SESSION['gen_user_id'];?>&title=<?php echo str_replace("'","\'",$profile_search[$i_search_m]['title']);?>&image_name=<?php echo $image_name_cart;?>" id="download_video<?php echo $profile_search[$i_search_m]['id'];?>" style="display:none;"></a>
											<script type="text/javascript">
											$(document).ready(function() {
													$("#download_video<?php echo $profile_search[$i_search_m]['id'];?>").fancybox({
													'height'			: '75%',
													'width'				: '69%',
													'transitionIn'		: 'none',
													'transitionOut'		: 'none',
													'type'				: 'iframe'
													});
											});
											</script>
										</div>
									<?php	
									}else if($profile_search[$i_search_m]['media_type']==113){
										$get_image = $search_criteria->get_media_images($profile_search[$i_search_m]['id']);
										
										$orgPath="http://medgallery.s3.amazonaws.com/";
										$thumbPath="http://medgalthumb.s3.amazonaws.com/";
										
										$all_images ="";
										for($cg_i=0;$cg_i<count($get_image);$cg_i++)
										{
											$mediaSrc = $get_image[0]['image_name'];
											$all_images = $all_images .",".$get_image[$cg_i]['image_name'];
											$all_images_title = $all_images_title .",".$get_image[$cg_i]['image_title'];
										}
										$all_images = ltrim($all_images,',');
										$all_images_title = ltrim($all_images_title,',');
									?>
										<div class="player" >
											<a href="javascript:void(0);"><img src="../images/profile/play.gif"  onmouseover="this.src='../images/profile/play-over.gif'" onmouseout="this.src='../images/profile/play.gif'" onclick="checkload('<?php echo $mediaSrc; ?>','<?php echo $all_images; ?>','<?php echo $thumbPath; ?>','<?php echo $orgPath; ?>','<?php echo $all_images_title; ?>','<?php echo $profile_search[$i_search_m]['id'];?>')" /></a>
										</div>
									<?php	
										$mediaSrc = "";
										$all_images = "";
										$all_images_title = "";
									}
									else if($profile_search[$i_search_m]['table_name']=="general_artist_gallery_list" || $profile_search[$i_search_m]['table_name']=="general_community_gallery_list"){
									$get_reg_image = $search_criteria->get_registration_gallery($profile_search[$i_search_m]['table_name'],$profile_search[$i_search_m]['gallery_id']);
																				
										$reg_orgPath="http://reggallery.s3.amazonaws.com/";
										$reg_thumbPath="http://reggalthumb.s3.amazonaws.com/";
										
										$all_reg_images ="";
										for($reg_i=0;$reg_i<count($get_reg_image);$reg_i++)
										{
											$reg_mediaSrc = $get_reg_image[$reg_i]['image_name'];
											$all_reg_images = $all_reg_images .",".$get_reg_image[$reg_i]['image_name'];
											$all_reg_images_title = $all_reg_images_title .",".$get_reg_image[$reg_i]['image_title'];
										}
										$all_reg_images = ltrim($all_reg_images,',');
										$all_reg_images_title = ltrim($all_reg_images_title,',');
									?>
										<div class="player" >
											<a href="javascript:void(0);"><img src="../images/profile/play.gif"  onmouseover="this.src='../images/profile/play-over.gif'" onmouseout="this.src='../images/profile/play.gif'" onclick="checkload('<?php echo $reg_mediaSrc;?>','<?php echo $all_reg_images; ?>','<?php echo $reg_thumbPath; ?>','<?php echo $reg_orgPath; ?>','<?php echo $all_reg_images_title; ?>','<?php echo $profile_search[$i_search_m]['id'];?>')" title="Play file now." /></a>
											<!--<a href="javascript:void(0);"><img src="../images/profile/add.gif"  onmouseover="this.src='../images/profile/add-over.gif'" onmouseout="this.src='../images/profile/add.gif'" title="Add file to the bottom of your playlist without stopping current media."/></a>-->
											<img src="images/profile/download-dis.gif" />
										</div>
									<?php
									}
									else if($profile_search[$i_search_m]['table_name']=="general_artist_audio" || $profile_search[$i_search_m]['table_name']=="general_community_audio"){
									?>
										<div class="player" >
											<a href="javascript:void(0);"><img src="../images/profile/play.gif"  onmouseover="this.src='../images/profile/play-over.gif'" onmouseout="this.src='../images/profile/play.gif'" onclick="showPlayer_reg('a','<?php if($profile_search[$i_search_m]['audio_id']!=""){echo $profile_search[$i_search_m]['audio_id'];}else{echo $profile_search[$i_search_m]['audio_id'];}?>','play_reg','<?php echo $profile_search[$i_search_m]['table_name'];?>');" title="Play file now."/></a>
											<a href="javascript:void(0);"><img src="../images/profile/add.gif"  onmouseover="this.src='../images/profile/add-over.gif'" onmouseout="this.src='../images/profile/add.gif'" onclick="showPlayer_reg('a','<?php if($profile_search[$i_search_m]['audio_id']!=""){echo $profile_search[$i_search_m]['audio_id'];}else{echo $profile_search[$i_search_m]['audio_id'];}?>','add_reg','<?php echo $profile_search[$i_search_m]['table_name'];?>');" title="Add file to the bottom of your playlist without stopping current media."/></a>
											<img src="images/profile/download-dis.gif" />
										</div>
									<?php
									}else if($profile_search[$i_search_m]['table_name']=="general_artist_video" || $profile_search[$i_search_m]['table_name']=="general_community_video"){
									?>
										<div class="player" >
										
											<a href="javascript:void(0);"><img src="../images/profile/play.gif"  onmouseover="this.src='../images/profile/play-over.gif'" onmouseout="this.src='../images/profile/play.gif'" onclick="showPlayer_reg('v','<?php if($profile_search[$i_search_m]['video_id']!=""){echo $profile_search[$i_search_m]['video_id'];}else{echo $profile_search[$i_search_m]['video_id'];}?>','playlist_reg','<?php echo $profile_search[$i_search_m]['table_name'];?>');" title="Play file now."/></a>
											<a href="javascript:void(0);"><img src="../images/profile/add.gif"  onmouseover="this.src='../images/profile/add-over.gif'" onmouseout="this.src='../images/profile/add.gif'" onclick="showPlayer_reg('v','<?php if($profile_search[$i_search_m]['video_id']!=""){echo $profile_search[$i_search_m]['video_id'];}else{echo $profile_search[$i_search_m]['video_id'];}?>','addvi_reg','<?php echo $profile_search[$i_search_m]['table_name'];?>');" title="Add file to the bottom of your playlist without stopping current media."/></a>
											<img src="images/profile/download-dis.gif" />
										</div>
									<?php	
									}
									else if($_POST['profile_type_search']=="artist")
									{
										if(isset($_SESSION['login_email']))
										{
											$gen_detail = $newtab->getArtist_details($profile_search[$i_search_m]['artist_id']);
										}
										else
										{
											$gen_detail = "";
										}
										
										if($profile_search[$i_search_m]['community_id']!=NULL)
										{
											$anss_butts = $newtab->getComm_details($profile_search[$i_search_m]['community_id']);
										}
										elseif($profile_search[$i_search_m]['artist_id']!=NULL)
										{
											$anss_butts = $newtab->getArtist_details($profile_search[$i_search_m]['artist_id']);
										}
										$find_mem_butts_arts = $display->get_member_or_not($anss_butts['general_user_id']);
										//echo $profile_search[$i_search_m]['featured_media_table'];
									?>
									<div class="player" >
										<?php
										if($profile_search[$i_search_m]['featured_media']=="Gallery" && $profile_search[$i_search_m]['media_id']!=0)
										{
											if($profile_search[$i_search_m]['featured_media_table']!="" &&($profile_search[$i_search_m]['featured_media_table']=="general_artist_gallery_list" || $profile_search[$i_search_m]['featured_media_table']=="general_community_gallery_list"))
											{
												$get_register_featured_gallery = $search_criteria ->get_register_featured_media_gallery($profile_search[$i_search_m]['media_id'],$profile_search[$i_search_m]['featured_media_table']);
												$all_register_images ="";
												$feature_reg_big_img ="";
												$featured_reg_gal_img_title ="";
												for($cou_i=0;$cou_i<count($get_register_featured_gallery);$cou_i++)
												{
													$feature_reg_big_img = $get_register_featured_gallery[0]['image_name'];
													$all_register_images = $all_register_images.",".$get_register_featured_gallery[$cou_i]['image_name'];
													$featured_reg_gal_img_title = $featured_reg_gal_img_title .",".$get_register_featured_gallery[$cou_i]['image_title'];
												}
												$all_register_images = ltrim($all_register_images,',');
												$featured_reg_gal_img_title = ltrim($featured_reg_gal_img_title,',');
												$feature_reg_org_path = "http://reggallery.s3.amazonaws.com/";
												$feature_reg_thum_path = "http://reggalthumb.s3.amazonaws.com/";
											}
											else if($profile_search[$i_search_m]['featured_media_table']!="" && $profile_search[$i_search_m]['featured_media_table']=="general_media"){
												$get_register_featured_gallery = $search_criteria ->get_featured_gallery_images($profile_search[$i_search_m]['media_id']);
												$all_register_images ="";
												$feature_reg_big_img ="";
												$featured_reg_gal_img_title ="";
												for($cou_i=0;$cou_i<count($get_register_featured_gallery);$cou_i++)
												{
													$feature_reg_big_img = $get_register_featured_gallery[0]['image_name'];
													$all_register_images = $all_register_images.",".$get_register_featured_gallery[$cou_i]['image_name'];
													$featured_reg_gal_img_title = $featured_reg_gal_img_title .",".$get_register_featured_gallery[$cou_i]['image_title'];
												}
												$all_register_images = ltrim($all_register_images,',');
												$featured_reg_gal_img_title = ltrim($featured_reg_gal_img_title,',');
												$feature_reg_org_path = "http://medgallery.s3.amazonaws.com/";
												$feature_reg_thum_path = "http://medgalthumb.s3.amazonaws.com/";
											}
										?>
										<a href="javascript:void(0);"><img src="../images/profile/play.gif" onmouseover="this.src='../images/profile/play-over.gif'" onmouseout="this.src='../images/profile/play.gif'" onclick="checkload('<?php echo $feature_reg_big_img;?>','<?php echo $all_register_images; ?>','<?php echo $feature_reg_thum_path; ?>','<?php echo $feature_reg_org_path; ?>','<?php echo $featured_reg_gal_img_title; ?>')" title="Play file now."/></a>
										<!--<a href="javascript:void(0);"><img src="../images/profile/add.gif"  onmouseover="this.src='../images/profile/add-over.gif'" onmouseout="this.src='../images/profile/add.gif'" title="Add file to the bottom of your playlist without stopping current media."/></a>-->
										<?php
										if($find_mem_butts_arts != null && $find_mem_butts_arts['general_user_id'] != null && ($find_mem_butts_arts['expiry_date'] >$date || $find_mem_butts_arts['lifetime']==1))
									{
											if($profile_search[$i_search_m]['type']=='fan_club_yes')
									{
										if($_SESSION['login_email']==NULL || $_SESSION['login_email']!=$gen_detail['email'])
										{
									?>
										<a title="<?php if($profile_search[$i_search_m]['type']=="fan_club_yes") { echo "Purchase a fan club membership to this users exclusive media."; } elseif($profile_search[$i_search_m]['type']=="for_sale") { echo "Purchase this projects media."; } ?>" href="javascript:void(0);" onClick="fancy_login_art_coms('<?php echo $profile_search[$i_search_m]['profile_url']; ?>')" ><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>
									<?php
										}
									}
									}
											?>
											<a style="display:none;" href="fan_club.php?data=<?php echo $profile_search[$i_search_m]['profile_url']; ?>" id="fan_club_pro_ca<?php echo $profile_search[$i_search_m]['profile_url']; ?>">
											<?php if($profile_search[$i_search_m]['type']=="fan_club_yes") { echo "Fan Club"; } elseif($profile_search[$i_search_m]['type']=="for_sale") { echo "Download"; } ?></a>
											<script type="text/javascript">
											$(document).ready(function() {
													$("#fan_club_pro_ca<?php echo $profile_search[$i_search_m]['profile_url']; ?>").fancybox({
													'width'				: '75%',
													'height'			: '75%',
													'transitionIn'		: 'none',
													'transitionOut'		: 'none',
													'type'				: 'iframe'
													});
											});
											</script>
											<?php
											$all_register_images ="";
											$feature_reg_big_img ="";
											$featured_reg_gal_img_title ="";
										}
										else if($profile_search[$i_search_m]['featured_media']=="Song" && $profile_search[$i_search_m]['media_id']!=0)
										{
											include_once("findsale_song.php");
											if($profile_search[$i_search_m]['featured_media_table']=="general_artist_audio" || $profile_search[$i_search_m]['featured_media_table']=="general_community_audio")
											{
												?>
													<a href="javascript:void(0);"><img src="../images/profile/play.gif" onmouseover="this.src='../images/profile/play-over.gif'" onmouseout="this.src='../images/profile/play.gif'" onclick="showPlayer_reg('a','<?php echo $profile_search[$i_search_m]['media_id'];?>','play_reg','<?php echo $profile_search[$i_search_m]['featured_media_table'];?>')" title="Play file now."/></a>
													<a href="javascript:void(0);"><img src="../images/profile/add.gif"  onmouseover="this.src='../images/profile/add-over.gif'" onmouseout="this.src='../images/profile/add.gif'" onclick="showPlayer_reg('a','<?php echo $profile_search[$i_search_m]['media_id'];?>','add_reg','<?php echo $profile_search[$i_search_m]['featured_media_table'];?>')" title="Add file to the bottom of your playlist without stopping current media."/></a>
												<?php
											}
											else{
												?>
													<a href="javascript:void(0);"><img src="../images/profile/play.gif" onmouseover="this.src='../images/profile/play-over.gif'" onmouseout="this.src='../images/profile/play.gif'" onclick="showPlayer('a','<?php echo $profile_search[$i_search_m]['media_id'];?>','play')" title="Play file now."/></a>
													<a href="javascript:void(0);"><img src="../images/profile/add.gif"  onmouseover="this.src='../images/profile/add-over.gif'" onmouseout="this.src='../images/profile/add.gif'" onclick="showPlayer('a','<?php echo $profile_search[$i_search_m]['media_id'];?>','add')" title="Add file to the bottom of your playlist without stopping current media."/></a>
												<?php
											}
											$get_media_info_down_gal = $search_criteria ->get_fetatured_media_info($profile_search[$i_search_m]['media_id']);
											if($find_mem_butts_arts != null && $find_mem_butts_arts['general_user_id'] != null && ($find_mem_butts_arts['expiry_date'] >$date || $find_mem_butts_arts['lifetime']==1))
									{
											
											if($profile_search[$i_search_m]['type']=='fan_club_yes')
									{
										if($_SESSION['login_email']==NULL || $_SESSION['login_email']!=$gen_detail['email'])
										{
									?>
										<a title="<?php if($profile_search[$i_search_m]['type']=="fan_club_yes") { echo "Purchase a fan club membership to this users exclusive media."; } elseif($profile_search[$i_search_m]['type']=="for_sale") { echo "Purchase this projects media."; } ?>" href="javascript:void(0);" onClick="fancy_login_art_coms('<?php echo $profile_search[$i_search_m]['profile_url']; ?>')" ><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>
									<?php
										}
									}
									}
											?>
											<a style="display:none;" href="fan_club.php?data=<?php echo $profile_search[$i_search_m]['profile_url']; ?>" id="fan_club_pro_ca<?php echo $profile_search[$i_search_m]['profile_url']; ?>">
											<?php if($profile_search[$i_search_m]['type']=="fan_club_yes") { echo "Fan Club"; } elseif($profile_search[$i_search_m]['type']=="for_sale") { echo "Download"; } ?></a>
											<script type="text/javascript">
											$(document).ready(function() {
													$("#fan_club_pro_ca<?php echo $profile_search[$i_search_m]['profile_url']; ?>").fancybox({
													'width'				: '75%',
													'height'			: '75%',
													'transitionIn'		: 'none',
													'transitionOut'		: 'none',
													'type'				: 'iframe'
													});
											});
											</script>
											<?php
										}
										else if($profile_search[$i_search_m]['featured_media']=="Video" && $profile_search[$i_search_m]['media_id']!=0)
										{
											include_once("findsale_video.php");
											if($profile_search[$i_search_m]['featured_media_table']=="general_artist_video" || $profile_search[$i_search_m]['featured_media_table']=="general_community_video")
											{
												?>
													<a href="javascript:void(0);"><img src="../images/profile/play.gif" onmouseover="this.src='../images/profile/play-over.gif'" onmouseout="this.src='../images/profile/play.gif'" onclick="showPlayer_reg('v','<?php echo $profile_search[$i_search_m]['media_id'];?>','playlist_reg','<?php echo $profile_search[$i_search_m]['featured_media_table'];?>')" title="Play file now."/></a>
													<a href="javascript:void(0);"><img src="../images/profile/add.gif"  onmouseover="this.src='../images/profile/add-over.gif'" onmouseout="this.src='../images/profile/add.gif'" onclick="showPlayer_reg('v','<?php echo $profile_search[$i_search_m]['media_id'];?>','addvi_reg','<?php echo $profile_search[$i_search_m]['featured_media_table'];?>')" title="Add file to the bottom of your playlist without stopping current media."/></a>
												<?php
											}else{
												?>
													<a href="javascript:void(0);"><img src="../images/profile/play.gif" onmouseover="this.src='../images/profile/play-over.gif'" onmouseout="this.src='../images/profile/play.gif'" onclick="showPlayer('v','<?php echo $profile_search[$i_search_m]['media_id'];?>','playlist')" title="Play file now."/></a>
													<a href="javascript:void(0);"><img src="../images/profile/add.gif"  onmouseover="this.src='../images/profile/add-over.gif'" onmouseout="this.src='../images/profile/add.gif'" onclick="showPlayer('v','<?php echo $profile_search[$i_search_m]['media_id'];?>','addvi')" title="Add file to the bottom of your playlist without stopping current media."/></a>
												<?php
											}
											$get_media_info_down_gal = $search_criteria ->get_fetatured_media_info($profile_search[$i_search_m]['media_id']);
											if($find_mem_butts_arts != null && $find_mem_butts_arts['general_user_id'] != null && ($find_mem_butts_arts['expiry_date'] >$date || $find_mem_butts_arts['lifetime']==1))
									{
											if($profile_search[$i_search_m]['type']=='fan_club_yes')
									{
										if($_SESSION['login_email']==NULL || $_SESSION['login_email']!=$gen_detail['email'])
										{
									?>
										<a title="<?php if($profile_search[$i_search_m]['type']=="fan_club_yes") { echo "Purchase a fan club membership to this users exclusive media."; } elseif($profile_search[$i_search_m]['type']=="for_sale") { echo "Purchase this projects media."; } ?>" href="javascript:void(0);" onClick="fancy_login_art_coms('<?php echo $profile_search[$i_search_m]['profile_url']; ?>')" ><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>
									<?php
										}
									}
									}
											?>
											<a style="display:none;" href="fan_club.php?data=<?php echo $profile_search[$i_search_m]['profile_url']; ?>" id="fan_club_pro_ca<?php echo $profile_search[$i_search_m]['profile_url']; ?>">
											<?php if($profile_search[$i_search_m]['type']=="fan_club_yes") { echo "Fan Club"; } elseif($profile_search[$i_search_m]['type']=="for_sale") { echo "Download"; } ?></a>
											<script type="text/javascript">
											$(document).ready(function() {
													$("#fan_club_pro_ca<?php echo $profile_search[$i_search_m]['profile_url']; ?>").fancybox({
													'width'				: '75%',
													'height'			: '75%',
													'transitionIn'		: 'none',
													'transitionOut'		: 'none',
													'type'				: 'iframe'
													});
											});
											</script>
											<?php
										}
										else{
										if($find_mem_butts_arts != null && $find_mem_butts_arts['general_user_id'] != null && ($find_mem_butts_arts['expiry_date'] >$date || $find_mem_butts_arts['lifetime']==1))
									{
												if($profile_search[$i_search_m]['type']=='fan_club_yes')
										{
											if($_SESSION['login_email']==NULL || $_SESSION['login_email']!=$gen_detail['email'])
											{
										?>
											<a title="<?php if($profile_search[$i_search_m]['type']=="fan_club_yes") { echo "Purchase a fan club membership to this users exclusive media."; } elseif($profile_search[$i_search_m]['type']=="for_sale") { echo "Purchase this projects media."; } ?>" href="javascript:void(0);" onClick="fancy_login_art_coms('<?php echo $profile_search[$i_search_m]['profile_url']; ?>')" ><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>
										<?php
											}
										}
										}
									  ?>
									  <a style="display:none;" href="fan_club.php?data=<?php echo $profile_search[$i_search_m]['profile_url']; ?>" id="fan_club_pro_ca<?php echo $profile_search[$i_search_m]['profile_url']; ?>">
											<?php if($profile_search[$i_search_m]['type']=="fan_club_yes") { echo "Fan Club"; } elseif($profile_search[$i_search_m]['type']=="for_sale") { echo "Download"; } ?></a>
											<script type="text/javascript">
											$(document).ready(function() {
													$("#fan_club_pro_ca<?php echo $profile_search[$i_search_m]['profile_url']; ?>").fancybox({
													'width'				: '75%',
													'height'			: '75%',
													'transitionIn'		: 'none',
													'transitionOut'		: 'none',
													'type'				: 'iframe'
													});
											});
											</script>
										<!--<a href="javascript:void(0);"><img src="../images/profile/play.gif" onmouseover="this.src='../images/profile/play-over.gif'" onmouseout="this.src='../images/profile/play.gif'" /></a>
										<a href="javascript:void(0);"><img src="../images/profile/add.gif"  onmouseover="this.src='../images/profile/add-over.gif'" onmouseout="this.src='../images/profile/add.gif'"/></a>-->
									<?php
										}
									?>
										
									</div>
									<?php
									}
									else if($_POST['profile_type_search']=='artist' || $_POST['profile_type_search']=='community' || (($profile_search[$i_search_m]['artist_id']!="" || $profile_search[$i_search_m]['community_id']!="") && $profile_search[$i_search_m]['id']==""))
									{
										if(isset($_SESSION['login_email']))
										{
											if($profile_search[$i_search_m]['community_id']!=NULL)
											{
												$gen_detail = $newtab->getComm_details($profile_search[$i_search_m]['community_id']);
											}
											elseif($profile_search[$i_search_m]['artist_id']!=NULL)
											{
												$gen_detail = $newtab->getArtist_details($profile_search[$i_search_m]['artist_id']);
											}
										}
										else
										{
											$gen_detail = "";
										}
										
										if($profile_search[$i_search_m]['community_id']!=NULL)
										{
											$anss_butts = $newtab->getComm_details($profile_search[$i_search_m]['community_id']);
										}
										elseif($profile_search[$i_search_m]['artist_id']!=NULL)
										{
											$anss_butts = $newtab->getArtist_details($profile_search[$i_search_m]['artist_id']);
										}
										$find_mem_butts = $display->get_member_or_not($anss_butts['general_user_id']);
										//echo $profile_search[$i_search_m]['featured_media_table'];
									?>
									<div class="player" >
										<?php
										if($profile_search[$i_search_m]['featured_media']=="Gallery" && $profile_search[$i_search_m]['media_id']!=0)
										{
											if($profile_search[$i_search_m]['featured_media_table']!="" &&($profile_search[$i_search_m]['featured_media_table']=="general_artist_gallery_list" || $profile_search[$i_search_m]['featured_media_table']=="general_community_gallery_list"))
											{
												$get_register_featured_gallery = $search_criteria ->get_register_featured_media_gallery($profile_search[$i_search_m]['media_id'],$profile_search[$i_search_m]['featured_media_table']);
												$all_register_images ="";
												$feature_reg_big_img ="";
												$featured_reg_gal_img_title ="";
												for($cou_i=0;$cou_i<count($get_register_featured_gallery);$cou_i++)
												{
													$feature_reg_big_img = $get_register_featured_gallery[0]['image_name'];
													$all_register_images = $all_register_images.",".$get_register_featured_gallery[$cou_i]['image_name'];
													$featured_reg_gal_img_title = $featured_reg_gal_img_title .",".$get_register_featured_gallery[$cou_i]['image_title'];
												}
												$all_register_images = ltrim($all_register_images,',');
												$featured_reg_gal_img_title = ltrim($featured_reg_gal_img_title,',');
												$feature_reg_org_path = "http://reggallery.s3.amazonaws.com/";
												$feature_reg_thum_path = "http://reggalthumb.s3.amazonaws.com/";
											}
											else if($profile_search[$i_search_m]['featured_media_table']!="" && $profile_search[$i_search_m]['featured_media_table']=="general_media"){
												$get_register_featured_gallery = $search_criteria ->get_featured_gallery_images($profile_search[$i_search_m]['media_id']);
												$all_register_images ="";
												$feature_reg_big_img ="";
												$featured_reg_gal_img_title ="";
												for($cou_i=0;$cou_i<count($get_register_featured_gallery);$cou_i++)
												{
													$feature_reg_big_img = $get_register_featured_gallery[0]['image_name'];
													$all_register_images = $all_register_images.",".$get_register_featured_gallery[$cou_i]['image_name'];
													$featured_reg_gal_img_title = $featured_reg_gal_img_title .",".$get_register_featured_gallery[$cou_i]['image_title'];
												}
												$all_register_images = ltrim($all_register_images,',');
												$featured_reg_gal_img_title = ltrim($featured_reg_gal_img_title,',');
												$feature_reg_org_path = "http://medgallery.s3.amazonaws.com/";
												$feature_reg_thum_path = "http://medgalthumb.s3.amazonaws.com/";
											}
										?>
										<a href="javascript:void(0);"><img src="../images/profile/play.gif" onmouseover="this.src='../images/profile/play-over.gif'" onmouseout="this.src='../images/profile/play.gif'" onclick="checkload('<?php echo $feature_reg_big_img;?>','<?php echo $all_register_images; ?>','<?php echo $feature_reg_thum_path; ?>','<?php echo $feature_reg_org_path; ?>','<?php echo $featured_reg_gal_img_title; ?>')" title="Play file now."/></a>
										<!--<a href="javascript:void(0);"><img src="../images/profile/add.gif"  onmouseover="this.src='../images/profile/add-over.gif'" onmouseout="this.src='../images/profile/add.gif'" title="Add file to the bottom of your playlist without stopping current media."/></a>-->
										<?php
										if($find_mem_butts != null && $find_mem_butts['general_user_id'] != null && ($find_mem_butts['expiry_date'] >$date || $find_mem_butts['lifetime']==1))
									{
											if($profile_search[$i_search_m]['type']=='fan_club_yes')
									{
										if($_SESSION['login_email']==NULL || $_SESSION['login_email']!=$gen_detail['email'])
										{
									?>
										<a title="<?php if($profile_search[$i_search_m]['type']=="fan_club_yes") { echo "Purchase a fan club membership to this users exclusive media."; } elseif($profile_search[$i_search_m]['type']=="for_sale") { echo "Purchase this projects media."; } ?>" href="javascript:void(0);" onClick="fancy_login_art_coms('<?php echo $profile_search[$i_search_m]['profile_url']; ?>')" ><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>
									<?php
										}
									}
									}
											?>
											<a style="display:none;" href="fan_club.php?data=<?php echo $profile_search[$i_search_m]['profile_url']; ?>" id="fan_club_pro_ca<?php echo $profile_search[$i_search_m]['profile_url']; ?>">
											<?php if($profile_search[$i_search_m]['type']=="fan_club_yes") { echo "Fan Club"; } elseif($profile_search[$i_search_m]['type']=="for_sale") { echo "Download"; } ?></a>
											<script type="text/javascript">
											$(document).ready(function() {
													$("#fan_club_pro_ca<?php echo $profile_search[$i_search_m]['profile_url']; ?>").fancybox({
													'width'				: '75%',
													'height'			: '75%',
													'transitionIn'		: 'none',
													'transitionOut'		: 'none',
													'type'				: 'iframe'
													});
											});
											</script>
										<?php
											$all_register_images ="";
											$feature_reg_big_img ="";
											$featured_reg_gal_img_title ="";
										}
										else if($profile_search[$i_search_m]['featured_media']=="Song" && $profile_search[$i_search_m]['media_id']!=0)
										{
											include_once("findsale_song.php");
											if($profile_search[$i_search_m]['featured_media_table']=="general_artist_audio" || $profile_search[$i_search_m]['featured_media_table']=="general_community_audio")
											{
												?>
													<a href="javascript:void(0);"><img src="../images/profile/play.gif" onmouseover="this.src='../images/profile/play-over.gif'" onmouseout="this.src='../images/profile/play.gif'" onclick="showPlayer_reg('a','<?php echo $profile_search[$i_search_m]['media_id'];?>','play_reg','<?php echo $profile_search[$i_search_m]['featured_media_table'];?>')" title="Play file now."/></a>
													<a href="javascript:void(0);"><img src="../images/profile/add.gif"  onmouseover="this.src='../images/profile/add-over.gif'" onmouseout="this.src='../images/profile/add.gif'" onclick="showPlayer_reg('a','<?php echo $profile_search[$i_search_m]['media_id'];?>','add_reg','<?php echo $profile_search[$i_search_m]['featured_media_table'];?>')" title="Add file to the bottom of your playlist without stopping current media."/></a>
												<?php
											}
											else{
												?>
													<a href="javascript:void(0);"><img src="../images/profile/play.gif" onmouseover="this.src='../images/profile/play-over.gif'" onmouseout="this.src='../images/profile/play.gif'" onclick="showPlayer('a','<?php echo $profile_search[$i_search_m]['media_id'];?>','play')" title="Play file now."/></a>
													<a href="javascript:void(0);"><img src="../images/profile/add.gif"  onmouseover="this.src='../images/profile/add-over.gif'" onmouseout="this.src='../images/profile/add.gif'" onclick="showPlayer('a','<?php echo $profile_search[$i_search_m]['media_id'];?>','add')" title="Add file to the bottom of your playlist without stopping current media."/></a>
												<?php
											}
											$get_media_info_down_gal = $search_criteria ->get_fetatured_media_info($profile_search[$i_search_m]['media_id']);
											if($find_mem_butts != null && $find_mem_butts['general_user_id'] != null && ($find_mem_butts['expiry_date'] >$date || $find_mem_butts['lifetime']==1))
									{
											if($profile_search[$i_search_m]['type']=='fan_club_yes')
									{
										if($_SESSION['login_email']==NULL || $_SESSION['login_email']!=$gen_detail['email'])
										{
									?>
										<a title="<?php if($profile_search[$i_search_m]['type']=="fan_club_yes") { echo "Purchase a fan club membership to this users exclusive media."; } elseif($profile_search[$i_search_m]['type']=="for_sale") { echo "Purchase this projects media."; } ?>" href="javascript:void(0);" onClick="fancy_login_art_coms('<?php echo $profile_search[$i_search_m]['profile_url']; ?>')" ><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>
									<?php
										}
									}
									}
											?>
											<a style="display:none;" href="fan_club.php?data=<?php echo $profile_search[$i_search_m]['profile_url']; ?>" id="fan_club_pro_ca<?php echo $profile_search[$i_search_m]['profile_url']; ?>">
											<?php if($profile_search[$i_search_m]['type']=="fan_club_yes") { echo "Fan Club"; } elseif($profile_search[$i_search_m]['type']=="for_sale") { echo "Download"; } ?></a>
											<script type="text/javascript">
											$(document).ready(function() {
													$("#fan_club_pro_ca<?php echo $profile_search[$i_search_m]['profile_url']; ?>").fancybox({
													'width'				: '75%',
													'height'			: '75%',
													'transitionIn'		: 'none',
													'transitionOut'		: 'none',
													'type'				: 'iframe'
													});
											});
											</script>
											<?php
										}
										else if($profile_search[$i_search_m]['featured_media']=="Video" && $profile_search[$i_search_m]['media_id']!=0)
										{
											include_once("findsale_video.php");
											if($profile_search[$i_search_m]['featured_media_table']=="general_artist_video" || $profile_search[$i_search_m]['featured_media_table']=="general_community_video")
											{
												?>
													<a href="javascript:void(0);"><img src="../images/profile/play.gif" onmouseover="this.src='../images/profile/play-over.gif'" onmouseout="this.src='../images/profile/play.gif'" onclick="showPlayer_reg('v','<?php echo $profile_search[$i_search_m]['media_id'];?>','playlist_reg','<?php echo $profile_search[$i_search_m]['featured_media_table'];?>')" title="Play file now."/></a>
													<a href="javascript:void(0);"><img src="../images/profile/add.gif"  onmouseover="this.src='../images/profile/add-over.gif'" onmouseout="this.src='../images/profile/add.gif'" onclick="showPlayer_reg('v','<?php echo $profile_search[$i_search_m]['media_id'];?>','addvi_reg','<?php echo $profile_search[$i_search_m]['featured_media_table'];?>')" title="Add file to the bottom of your playlist without stopping current media."/></a>
												<?php
											}else{
												?>
													<a href="javascript:void(0);"><img src="../images/profile/play.gif" onmouseover="this.src='../images/profile/play-over.gif'" onmouseout="this.src='../images/profile/play.gif'" onclick="showPlayer('v','<?php echo $profile_search[$i_search_m]['media_id'];?>','playlist')" title="Play file now."/></a>
													<a href="javascript:void(0);"><img src="../images/profile/add.gif"  onmouseover="this.src='../images/profile/add-over.gif'" onmouseout="this.src='../images/profile/add.gif'" onclick="showPlayer('v','<?php echo $profile_search[$i_search_m]['media_id'];?>','addvi')" title="Add file to the bottom of your playlist without stopping current media."/></a>
												<?php
											}
											$get_media_info_down_gal = $search_criteria ->get_fetatured_media_info($profile_search[$i_search_m]['media_id']);
											if($find_mem_butts != null && $find_mem_butts['general_user_id'] != null && ($find_mem_butts['expiry_date'] >$date || $find_mem_butts['lifetime']==1))
									{
											if($profile_search[$i_search_m]['type']=='fan_club_yes')
									{
										if($_SESSION['login_email']==NULL || $_SESSION['login_email']!=$gen_detail['email'])
										{
									?>
										<a title="<?php if($profile_search[$i_search_m]['type']=="fan_club_yes") { echo "Purchase a fan club membership to this users exclusive media."; } elseif($profile_search[$i_search_m]['type']=="for_sale") { echo "Purchase this projects media."; } ?>" href="javascript:void(0);" onClick="fancy_login_art_coms('<?php echo $profile_search[$i_search_m]['profile_url']; ?>')" ><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>
									<?php
										}
									}
									}
											?>
											<a style="display:none;" href="fan_club.php?data=<?php echo $profile_search[$i_search_m]['profile_url']; ?>" id="fan_club_pro_ca<?php echo $profile_search[$i_search_m]['profile_url']; ?>">
											<?php if($profile_search[$i_search_m]['type']=="fan_club_yes") { echo "Fan Club"; } elseif($profile_search[$i_search_m]['type']=="for_sale") { echo "Download"; } ?></a>
											<script type="text/javascript">
											$(document).ready(function() {
													$("#fan_club_pro_ca<?php echo $profile_search[$i_search_m]['profile_url']; ?>").fancybox({
													'width'				: '75%',
													'height'			: '75%',
													'transitionIn'		: 'none',
													'transitionOut'		: 'none',
													'type'				: 'iframe'
													});
											});
											</script>
											<?php
										}
										else{
										if($find_mem_butts != null && $find_mem_butts['general_user_id'] != null && ($find_mem_butts['expiry_date'] >$date || $find_mem_butts['lifetime']==1))
									{
											if($profile_search[$i_search_m]['type']=='fan_club_yes')
									{
										if($_SESSION['login_email']==NULL || $_SESSION['login_email']!=$gen_detail['email'])
										{
									?>
										<a title="<?php if($profile_search[$i_search_m]['type']=="fan_club_yes") { echo "Purchase a fan club membership to this users exclusive media."; } elseif($profile_search[$i_search_m]['type']=="for_sale") { echo "Purchase this projects media."; } ?>" href="javascript:void(0);" onClick="fancy_login_art_coms('<?php echo $profile_search[$i_search_m]['profile_url']; ?>')" ><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>
									<?php
										}
									}
									}
									  ?>
									  <a style="display:none;" href="fan_club.php?data=<?php echo $profile_search[$i_search_m]['profile_url']; ?>" id="fan_club_pro_ca<?php echo $profile_search[$i_search_m]['profile_url']; ?>">
											<?php if($profile_search[$i_search_m]['type']=="fan_club_yes") { echo "Fan Club"; } elseif($profile_search[$i_search_m]['type']=="for_sale") { echo "Download"; } ?></a>
											<script type="text/javascript">
											$(document).ready(function() {
													$("#fan_club_pro_ca<?php echo $profile_search[$i_search_m]['profile_url']; ?>").fancybox({
													'width'				: '75%',
													'height'			: '75%',
													'transitionIn'		: 'none',
													'transitionOut'		: 'none',
													'type'				: 'iframe'
													});
											});
											</script>
										<!--<a href="javascript:void(0);"><img src="../images/profile/play.gif" onmouseover="this.src='../images/profile/play-over.gif'" onmouseout="this.src='../images/profile/play.gif'" /></a>
										<a href="javascript:void(0);"><img src="../images/profile/add.gif"  onmouseover="this.src='../images/profile/add-over.gif'" onmouseout="this.src='../images/profile/add.gif'"/></a>-->
									<?php
										}
									?>
										
									</div>
									<?php
									}
									else if($_POST['profile_type_search']=="community")
									{
										/* if(isset($_SESSION['login_email']))
										{
											$gen_detail = $newtab->getArtist_details($profile_search[$i_search_m]['community_id']);
										}
										else
										{
											$gen_detail = "";
										} */
										//echo $profile_search[$i_search_m]['featured_media_table'];
									?>
									<div class="player" >
										<?php
										if($profile_search[$i_search_m]['featured_media']=="Gallery" && $profile_search[$i_search_m]['media_id']!=0)
										{
											if($profile_search[$i_search_m]['featured_media_table']!="" &&($profile_search[$i_search_m]['featured_media_table']=="general_artist_gallery_list" || $profile_search[$i_search_m]['featured_media_table']=="general_community_gallery_list"))
											{
												$get_register_featured_gallery = $search_criteria ->get_register_featured_media_gallery($profile_search[$i_search_m]['media_id'],$profile_search[$i_search_m]['featured_media_table']);
												$all_register_images ="";
												$feature_reg_big_img ="";
												$featured_reg_gal_img_title ="";
												for($cou_i=0;$cou_i<count($get_register_featured_gallery);$cou_i++)
												{
													$feature_reg_big_img = $get_register_featured_gallery[0]['image_name'];
													$all_register_images = $all_register_images.",".$get_register_featured_gallery[$cou_i]['image_name'];
													$featured_reg_gal_img_title = $featured_reg_gal_img_title .",".$get_register_featured_gallery[$cou_i]['image_title'];
												}
												$all_register_images = ltrim($all_register_images,',');
												$featured_reg_gal_img_title = ltrim($featured_reg_gal_img_title,',');
												$feature_reg_org_path = "http://reggallery.s3.amazonaws.com/";
												$feature_reg_thum_path = "http://reggalthumb.s3.amazonaws.com/";
											}
											else if($profile_search[$i_search_m]['featured_media_table']!="" && $profile_search[$i_search_m]['featured_media_table']=="general_media"){
												$get_register_featured_gallery = $search_criteria ->get_featured_gallery_images($profile_search[$i_search_m]['media_id']);
												$all_register_images ="";
												$feature_reg_big_img ="";
												$featured_reg_gal_img_title ="";
												for($cou_i=0;$cou_i<count($get_register_featured_gallery);$cou_i++)
												{
													$feature_reg_big_img = $get_register_featured_gallery[0]['image_name'];
													$all_register_images = $all_register_images.",".$get_register_featured_gallery[$cou_i]['image_name'];
													$featured_reg_gal_img_title = $featured_reg_gal_img_title .",".$get_register_featured_gallery[$cou_i]['image_title'];
												}
												$all_register_images = ltrim($all_register_images,',');
												$featured_reg_gal_img_title = ltrim($featured_reg_gal_img_title,',');
												$feature_reg_org_path = "http://medgallery.s3.amazonaws.com/";
												$feature_reg_thum_path = "http://medgalthumb.s3.amazonaws.com/";
											}
										?>
										<a href="javascript:void(0);"><img src="../images/profile/play.gif" onmouseover="this.src='../images/profile/play-over.gif'" onmouseout="this.src='../images/profile/play.gif'" onclick="checkload('<?php echo $feature_reg_big_img;?>','<?php echo $all_register_images; ?>','<?php echo $feature_reg_thum_path; ?>','<?php echo $feature_reg_org_path; ?>','<?php echo $featured_reg_gal_img_title; ?>')" title="Play file now."/></a>
										<!--<a href="javascript:void(0);"><img src="../images/profile/add.gif"  onmouseover="this.src='../images/profile/add-over.gif'" onmouseout="this.src='../images/profile/add.gif'" title="Add file to the bottom of your playlist without stopping current media."/></a>-->
										<?php
											if($profile_search[$i_search_m]['featured_media_table']!="" && $profile_search[$i_search_m]['featured_media_table']=="general_media")
											{
												$get_media_info_down_gal = $search_criteria ->get_fetatured_media_info($profile_search[$i_search_m]['media_id']);
												$get_gal_image  = $new_profile_class_objnew_profile_class_obj ->get_gallery_info($profile_search[$i_search_m]['media_id']);
												if($get_media_info_down_gal['sharing_preference']==2){
												?>
											<!--		<a href="javascript:void(0);" onclick="ask_to_donate('<?php echo str_replace("'","\'",$get_media_info_down_gal['title']);?>','<?php echo str_replace("'","\'",$get_media_info_down_gal['creator']);?>','<?php echo $get_media_info_down_gal['id'];?>','download_gal_pop<?php echo $get_media_info_down_gal['id'];?>')"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>-->
											<img src="../images/profile/download-over.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download-over.gif'" title="Add media to your cart for free download or purchase." />
												 <?php
												}else{
												?>
												<img src="../images/profile/download-over.gif" />
												<?php
												}
											}else{
											?>
											<img src="../images/profile/download-over.gif" />
											<?php
											}
											?>
											<a href="addtocart.php?seller_id=<?php echo $get_media_info_down_gal['general_user_id'];?>&media_id=<?php echo $get_media_info_down_gal['id'];?>&buyer_id=<?php echo $_SESSION['gen_user_id'];?>&title=<?php echo str_replace("'","\'",$get_media_info_down_gal['title']);?>&image_name=<?php echo $get_gal_image['profile_image'];?>" id="download_gal_pop<?php echo $get_media_info_down_gal['id'];?>" style="display:none;"></a>
											<script type="text/javascript">
											$(document).ready(function() {
												$("#download_gal_pop<?php echo $get_media_info_down_gal['id'];?>").fancybox({
												'width'				: '75%',
												'height'			: '75%',
												'transitionIn'		: 'none',
												'transitionOut'		: 'none',
												'type'				: 'iframe'
												});
											});
											</script>
											<?php
											$all_register_images ="";
											$feature_reg_big_img ="";
											$featured_reg_gal_img_title ="";
										}
										else if($profile_search[$i_search_m]['featured_media']=="Song" && $profile_search[$i_search_m]['media_id']!=0)
										{
											include_once("findsale_song.php");
											if($profile_search[$i_search_m]['featured_media_table']=="general_artist_audio" || $profile_search[$i_search_m]['featured_media_table']=="general_community_audio")
											{
												?>
													<a href="javascript:void(0);"><img src="../images/profile/play.gif" onmouseover="this.src='../images/profile/play-over.gif'" onmouseout="this.src='../images/profile/play.gif'" onclick="showPlayer_reg('a','<?php echo $profile_search[$i_search_m]['media_id'];?>','play_reg','<?php echo $profile_search[$i_search_m]['featured_media_table'];?>')" title="Play file now."/></a>
													<a href="javascript:void(0);"><img src="../images/profile/add.gif"  onmouseover="this.src='../images/profile/add-over.gif'" onmouseout="this.src='../images/profile/add.gif'" onclick="showPlayer_reg('a','<?php echo $profile_search[$i_search_m]['media_id'];?>','add_reg','<?php echo $profile_search[$i_search_m]['featured_media_table'];?>')" title="Add file to the bottom of your playlist without stopping current media."/></a>
												<?php
											}
											else{
												?>
													<a href="javascript:void(0);"><img src="../images/profile/play.gif" onmouseover="this.src='../images/profile/play-over.gif'" onmouseout="this.src='../images/profile/play.gif'" onclick="showPlayer('a','<?php echo $profile_search[$i_search_m]['media_id'];?>','play')" title="Play file now."/></a>
													<a href="javascript:void(0);"><img src="../images/profile/add.gif"  onmouseover="this.src='../images/profile/add-over.gif'" onmouseout="this.src='../images/profile/add.gif'" onclick="showPlayer('a','<?php echo $profile_search[$i_search_m]['media_id'];?>','add')" title="Add file to the bottom of your playlist without stopping current media."/></a>
												<?php
											}
											$get_media_info_down_gal = $search_criteria ->get_fetatured_media_info($profile_search[$i_search_m]['media_id']);
											if($get_media_info_down_gal['sharing_preference']==5 || $get_media_info_down_gal['sharing_preference']==2)
											{
												$song_image_name_cart = $search_criteria->get_audio_img($get_media_info_down_gal['id']);
												$free_down_rel ="";
												for($find_down=0;$find_down<=count($get_songs_pnew_w);$find_down++)
												{
													if($get_songs_pnew_w[$find_down]['id']== $get_media_info_down_gal['id']){
													$free_down_rel = "yes";
													}
													else{
														if($free_down_rel =="" || $free_down_rel =="no"){
															$free_down_rel = "no";
														}
													}
												}
												if($free_down_rel == "yes"){
												 ?>
												<!-- <a href="javascript:void(0);" onclick="download_directly_song('<?php echo $get_media_info_down_gal['id'];?>','<?php echo str_replace("'","\'",$get_media_info_down_gal['title']); ?>','<?php echo str_replace("'","\'",$get_media_info_down_gal['creator']); ?>','<?php echo str_replace("'","\'",$get_media_info_down_gal['from']); ?>')"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>-->
												<img src="../images/profile/download-over.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download-over.gif'" title="Add media to your cart for free download or purchase." />
												 
											 <?php
												}
												else if($get_media_info_down_gal['sharing_preference']==2){
												?>
												<!--	<a href="javascript:void(0);" onclick="ask_to_donate('<?php echo str_replace("'","\'",$get_media_info_down_gal['title']);?>','<?php echo str_replace("'","\'",$get_media_info_down_gal['creator']);?>','<?php echo $get_media_info_down_gal['id'];?>','download_song<?php echo $get_media_info_down_gal['id'];?>')"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>-->
												<img src="../images/profile/download-over.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download-over.gif'" title="Add media to your cart for free download or purchase." />
												<?php
												}
												else{
											?>
											<!--	<a href="javascript:void(0);" onclick="down_song_pop('<?php echo $get_media_info_down_gal['id'];?>','<?php echo str_replace("'","\'",$get_media_info_down_gal['title']);?>','<?php echo str_replace("'","\'",$get_media_info_down_gal['creator']);?>')"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>-->
											<img src="../images/profile/download-over.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download-over.gif'" title="Add media to your cart for free download or purchase." />
											<?php
												}
											}else{
											?>
											<img src="../images/profile/download-over.gif" />
											<?php
											}
											?>
											<a href="addtocart.php?seller_id=<?php echo $get_media_info_down_gal['general_user_id']; ?>&media_id=<?php echo $get_media_info_down_gal['id']; ?>&buyer_id=<?php echo $_SESSION['gen_user_id'];?>&title=<?php echo str_replace("'","\'",$get_media_info_down_gal['title']);?>&image_name=<?php echo $song_image_name_cart;?>" id="download_song<?php echo $get_media_info_down_gal['id']; ?>" style="display:none;"></a>
											<script type="text/javascript">
											$(document).ready(function() {
													$("#download_song<?php echo $get_media_info_down_gal['id']; ?>").fancybox({
													'width'				: '75%',
													'height'			: '75%',
													'transitionIn'		: 'none',
													'transitionOut'		: 'none',
													'type'				: 'iframe'
													});
											});
											</script>
											<?php
										}
										else if($profile_search[$i_search_m]['featured_media']=="Video" && $profile_search[$i_search_m]['media_id']!=0)
										{
											include_once("findsale_video.php");
											if($profile_search[$i_search_m]['featured_media_table']=="general_artist_video" || $profile_search[$i_search_m]['featured_media_table']=="general_community_video")
											{
												?>
													<a href="javascript:void(0);"><img src="../images/profile/play.gif" onmouseover="this.src='../images/profile/play-over.gif'" onmouseout="this.src='../images/profile/play.gif'" onclick="showPlayer_reg('v','<?php echo $profile_search[$i_search_m]['media_id'];?>','playlist_reg','<?php echo $profile_search[$i_search_m]['featured_media_table'];?>')" title="Play file now."/></a>
													<a href="javascript:void(0);"><img src="../images/profile/add.gif"  onmouseover="this.src='../images/profile/add-over.gif'" onmouseout="this.src='../images/profile/add.gif'" onclick="showPlayer_reg('v','<?php echo $profile_search[$i_search_m]['media_id'];?>','addvi_reg','<?php echo $profile_search[$i_search_m]['featured_media_table'];?>')" title="Add file to the bottom of your playlist without stopping current media."/></a>
												<?php
											}else{
												?>
													<a href="javascript:void(0);"><img src="../images/profile/play.gif" onmouseover="this.src='../images/profile/play-over.gif'" onmouseout="this.src='../images/profile/play.gif'" onclick="showPlayer('v','<?php echo $profile_search[$i_search_m]['media_id'];?>','playlist')" title="Play file now."/></a>
													<a href="javascript:void(0);"><img src="../images/profile/add.gif"  onmouseover="this.src='../images/profile/add-over.gif'" onmouseout="this.src='../images/profile/add.gif'" onclick="showPlayer('v','<?php echo $profile_search[$i_search_m]['media_id'];?>','addvi')" title="Add file to the bottom of your playlist without stopping current media."/></a>
												<?php
											}
											$get_media_info_down_gal = $search_criteria ->get_fetatured_media_info($profile_search[$i_search_m]['media_id']);
											if($get_media_info_down_gal['sharing_preference']==5 || $get_media_info_down_gal['sharing_preference']==2)
											{
											$tables = 'media_video';
												$get_video_image = $search_criteria->getvideoimage($tables,$get_media_info_down_gal['id']);
												$image_name_cart = "https://img.youtube.com/vi/".$get_video_image."/default.jpg";
												$free_down_rel_video="";
												for($find_down=0;$find_down<=count($get_vids_pnew_w);$find_down++)
												{
													if($get_vids_pnew_w[$find_down]['id']==$get_media_info_down_gal['id']){
														$free_down_rel_video = "yes";
													}
													else{
														if($free_down_rel_video =="" || $free_down_rel_video =="no"){
															$free_down_rel_video = "no";
														}
													}
												}
												if($free_down_rel_video == "yes"){
												 ?>
											<!--	 <a href="javascript:void(0);" onclick="download_directly_song('<?php echo $get_media_info_down_gal['id']; ?>','<?php echo str_replace("'","\'",$get_media_info_down_gal['title']); ?>','<?php echo str_replace("'","\'",$get_media_info_down_gal['creator']); ?>','<?php echo str_replace("'","\'",$get_media_info_down_gal['from']); ?>')"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>-->
											<img src="../images/profile/download-over.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download-over.gif'" title="Add media to your cart for free download or purchase." />
												 
											 <?php
												}
												else if($get_media_info_down_gal['sharing_preference']==2){
												?>
													<!--<a href="javascript:void(0);" onclick="ask_to_donate('<?php echo str_replace("'","\'",$get_media_info_down_gal['title']);?>','<?php echo str_replace("'","\'",$get_media_info_down_gal['creator']);?>','<?php echo $get_media_info_down_gal['id'];?>','download_video<?php echo $get_media_info_down_gal['id'];?>')"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>-->
													<img src="../images/profile/download-over.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download-over.gif'" title="Add media to your cart for free download or purchase." />
												<?php
												}
												else{
											?>
												<!--<a href="javascript:void(0);" onclick="down_video_pop('<?php echo $get_media_info_down_gal['id'];?>','<?php echo str_replace("'","\'",$get_media_info_down_gal['title']);?>','<?php echo str_replace("'","\'",$get_media_info_down_gal['creator']);?>')"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>-->
												<img src="../images/profile/download-over.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download-over.gif'" title="Add media to your cart for free download or purchase." />
												
											<?php
												}
											}else{
											?>
											<img src="../images/profile/download-over.gif" />
											<?php
											}
											?>
											<a href="addtocart.php?seller_id=<?php echo $get_media_info_down_gal['general_user_id'];?>&media_id=<?php echo $get_media_info_down_gal['id'];?>&buyer_id=<?php echo $_SESSION['gen_user_id'];?>&title=<?php echo str_replace("'","\'",$get_media_info_down_gal['title']);?>&image_name=<?php echo $image_name_cart;?>" id="download_video<?php echo $get_media_info_down_gal['id'];?>" style="display:none;"></a>
											<script type="text/javascript">
											$(document).ready(function() {
													$("#download_video<?php echo $get_media_info_down_gal['id'];?>").fancybox({
													'height'			: '75%',
													'width'				: '69%',
													'transitionIn'		: 'none',
													'transitionOut'		: 'none',
													'type'				: 'iframe'
													});
											});
											</script>
											<?php
										}
										else{
									  ?>
										<!--<a href="javascript:void(0);"><img src="../images/profile/play.gif" onmouseover="this.src='../images/profile/play-over.gif'" onmouseout="this.src='../images/profile/play.gif'" /></a>
										<a href="javascript:void(0);"><img src="../images/profile/add.gif"  onmouseover="this.src='../images/profile/add-over.gif'" onmouseout="this.src='../images/profile/add.gif'"/></a>-->
									<?php
										}
									?>
									</div>
									<?php
									}
									else if($_POST['profile_type_search']=="event")
									{
										
										//echo $profile_search[$i_search_m]['featured_media_table'];
									?>
									<div class="player" >
										<?php
										if($profile_search[$i_search_m]['featured_media']=="Gallery" && $profile_search[$i_search_m]['media_id']!=0)
										{
											
											if($profile_search[$i_search_m]['featured_media_table']!="" &&($profile_search[$i_search_m]['featured_media_table']=="general_artist_gallery_list" || $profile_search[$i_search_m]['featured_media_table']=="general_community_gallery_list"))
											{
												$get_register_featured_gallery = $search_criteria ->get_register_featured_media_gallery($profile_search[$i_search_m]['media_id'],$profile_search[$i_search_m]['featured_media_table']);
												$all_register_images ="";
												$feature_reg_big_img ="";
												$featured_reg_gal_img_title ="";
												for($cou_i=0;$cou_i<count($get_register_featured_gallery);$cou_i++)
												{
													$feature_reg_big_img = $get_register_featured_gallery[0]['image_name'];
													$all_register_images = $all_register_images.",".$get_register_featured_gallery[$cou_i]['image_name'];
													$featured_reg_gal_img_title = $featured_reg_gal_img_title .",".$get_register_featured_gallery[$cou_i]['image_title'];
												}
												$all_register_images = ltrim($all_register_images,',');
												$featured_reg_gal_img_title = ltrim($featured_reg_gal_img_title,',');
												$feature_reg_org_path = "http://reggallery.s3.amazonaws.com/";
												$feature_reg_thum_path = "http://reggalthumb.s3.amazonaws.com/";
											}
											else if($profile_search[$i_search_m]['featured_media_table']!="" && $profile_search[$i_search_m]['featured_media_table']=="general_media"){
												$get_register_featured_gallery = $search_criteria ->get_featured_gallery_images($profile_search[$i_search_m]['media_id']);
												$all_register_images ="";
												$feature_reg_big_img ="";
												$featured_reg_gal_img_title ="";
												for($cou_i=0;$cou_i<count($get_register_featured_gallery);$cou_i++)
												{
													$feature_reg_big_img = $get_register_featured_gallery[0]['image_name'];
													$all_register_images = $all_register_images.",".$get_register_featured_gallery[$cou_i]['image_name'];
													$featured_reg_gal_img_title = $featured_reg_gal_img_title .",".$get_register_featured_gallery[$cou_i]['image_title'];
												}
												$all_register_images = ltrim($all_register_images,',');
												$featured_reg_gal_img_title = ltrim($featured_reg_gal_img_title,',');
												$feature_reg_org_path = "http://medgallery.s3.amazonaws.com/";
												$feature_reg_thum_path = "http://medgalthumb.s3.amazonaws.com/";
											}
										?>
										<a href="javascript:void(0);"><img src="../images/profile/play.gif" onmouseover="this.src='../images/profile/play-over.gif'" onmouseout="this.src='../images/profile/play.gif'" onclick="checkload('<?php echo $feature_reg_big_img;?>','<?php echo $all_register_images; ?>','<?php echo $feature_reg_thum_path; ?>','<?php echo $feature_reg_org_path; ?>','<?php echo $featured_reg_gal_img_title; ?>')" title="Play file now."/></a>
										<!--<a href="javascript:void(0);"><img src="../images/profile/add.gif"  onmouseover="this.src='../images/profile/add-over.gif'" onmouseout="this.src='../images/profile/add.gif'" title="Add file to the bottom of your playlist without stopping current media."/></a>-->
											<a href="addtocart.php?seller_id=<?php echo $get_media_info_down_gal['general_user_id'];?>&media_id=<?php echo $get_media_info_down_gal['id'];?>&buyer_id=<?php echo $_SESSION['gen_user_id'];?>&title=<?php echo str_replace("'","\'",$get_media_info_down_gal['title']);?>&image_name=<?php echo $get_gal_image['profile_image'];?>" id="download_gal_pop<?php echo $get_media_info_down_gal['id'];?>" style="display:none;"></a>
											<script type="text/javascript">
											$(document).ready(function() {
												$("#download_gal_pop<?php echo $get_media_info_down_gal['id'];?>").fancybox({
												'width'				: '75%',
												'height'			: '75%',
												'transitionIn'		: 'none',
												'transitionOut'		: 'none',
												'type'				: 'iframe'
												});
											});
											</script>
											<?php
											$all_register_images ="";
											$feature_reg_big_img ="";
											$featured_reg_gal_img_title ="";
										}
										else if($profile_search[$i_search_m]['featured_media']=="Song" && $profile_search[$i_search_m]['media_id']!=0)
										{
											include_once("findsale_song.php");
											if($profile_search[$i_search_m]['featured_media_table']=="general_artist_audio" || $profile_search[$i_search_m]['featured_media_table']=="general_community_audio")
											{
												?>
													<a href="javascript:void(0);"><img src="../images/profile/play.gif" onmouseover="this.src='../images/profile/play-over.gif'" onmouseout="this.src='../images/profile/play.gif'" onclick="showPlayer_reg('a','<?php echo $profile_search[$i_search_m]['media_id'];?>','play_reg','<?php echo $profile_search[$i_search_m]['featured_media_table'];?>')" title="Play file now."/></a>
													<a href="javascript:void(0);"><img src="../images/profile/add.gif"  onmouseover="this.src='../images/profile/add-over.gif'" onmouseout="this.src='../images/profile/add.gif'" onclick="showPlayer_reg('a','<?php echo $profile_search[$i_search_m]['media_id'];?>','add_reg','<?php echo $profile_search[$i_search_m]['featured_media_table'];?>')" title="Add file to the bottom of your playlist without stopping current media."/></a>
												<?php
											}
											else{
												?>
													<a href="javascript:void(0);"><img src="../images/profile/play.gif" onmouseover="this.src='../images/profile/play-over.gif'" onmouseout="this.src='../images/profile/play.gif'" onclick="showPlayer('a','<?php echo $profile_search[$i_search_m]['media_id'];?>','play')" title="Play file now."/></a>
													<a href="javascript:void(0);"><img src="../images/profile/add.gif"  onmouseover="this.src='../images/profile/add-over.gif'" onmouseout="this.src='../images/profile/add.gif'" onclick="showPlayer('a','<?php echo $profile_search[$i_search_m]['media_id'];?>','add')" title="Add file to the bottom of your playlist without stopping current media."/></a>
												<?php
											}
											$get_media_info_down_gal = $search_criteria ->get_fetatured_media_info($profile_search[$i_search_m]['media_id']);
											?>
											<a href="addtocart.php?seller_id=<?php echo $get_media_info_down_gal['general_user_id']; ?>&media_id=<?php echo $get_media_info_down_gal['id']; ?>&buyer_id=<?php echo $_SESSION['gen_user_id'];?>&title=<?php echo str_replace("'","\'",$get_media_info_down_gal['title']);?>&image_name=<?php echo $song_image_name_cart;?>" id="download_song<?php echo $get_media_info_down_gal['id']; ?>" style="display:none;"></a>
											<script type="text/javascript">
											$(document).ready(function() {
													$("#download_song<?php echo $get_media_info_down_gal['id']; ?>").fancybox({
													'width'				: '75%',
													'height'			: '75%',
													'transitionIn'		: 'none',
													'transitionOut'		: 'none',
													'type'				: 'iframe'
													});
											});
											</script>
											<?php
										}
										else if($profile_search[$i_search_m]['featured_media']=="Video" && $profile_search[$i_search_m]['media_id']!=0)
										{
											include_once("findsale_video.php");
											if($profile_search[$i_search_m]['featured_media_table']=="general_artist_video" || $profile_search[$i_search_m]['featured_media_table']=="general_community_video")
											{
												?>
													<a href="javascript:void(0);"><img src="../images/profile/play.gif" onmouseover="this.src='../images/profile/play-over.gif'" onmouseout="this.src='../images/profile/play.gif'" onclick="showPlayer_reg('v','<?php echo $profile_search[$i_search_m]['media_id'];?>','playlist_reg','<?php echo $profile_search[$i_search_m]['featured_media_table'];?>')" title="Play file now."/></a>
													<a href="javascript:void(0);"><img src="../images/profile/add.gif"  onmouseover="this.src='../images/profile/add-over.gif'" onmouseout="this.src='../images/profile/add.gif'" onclick="showPlayer_reg('v','<?php echo $profile_search[$i_search_m]['media_id'];?>','addvi_reg','<?php echo $profile_search[$i_search_m]['featured_media_table'];?>')" title="Add file to the bottom of your playlist without stopping current media."/></a>
												<?php
											}else{
												?>
													<a href="javascript:void(0);"><img src="../images/profile/play.gif" onmouseover="this.src='../images/profile/play-over.gif'" onmouseout="this.src='../images/profile/play.gif'" onclick="showPlayer('v','<?php echo $profile_search[$i_search_m]['media_id'];?>','playlist')" title="Play file now."/></a>
													<a href="javascript:void(0);"><img src="../images/profile/add.gif"  onmouseover="this.src='../images/profile/add-over.gif'" onmouseout="this.src='../images/profile/add.gif'" onclick="showPlayer('v','<?php echo $profile_search[$i_search_m]['media_id'];?>','addvi')" title="Add file to the bottom of your playlist without stopping current media."/></a>
												<?php
											}
											$get_media_info_down_gal = $search_criteria ->get_fetatured_media_info($profile_search[$i_search_m]['media_id']);
											
											?>
											<a href="addtocart.php?seller_id=<?php echo $get_media_info_down_gal['general_user_id'];?>&media_id=<?php echo $get_media_info_down_gal['id'];?>&buyer_id=<?php echo $_SESSION['gen_user_id'];?>&title=<?php echo str_replace("'","\'",$get_media_info_down_gal['title']);?>&image_name=<?php echo $image_name_cart;?>" id="download_video<?php echo $get_media_info_down_gal['id'];?>" style="display:none;"></a>
											<script type="text/javascript">
											$(document).ready(function() {
													$("#download_video<?php echo $get_media_info_down_gal['id'];?>").fancybox({
													'height'			: '75%',
													'width'				: '69%',
													'transitionIn'		: 'none',
													'transitionOut'		: 'none',
													'type'				: 'iframe'
													});
											});
											</script>
											<?php
										}
										else{
									  ?>
										<!--<a href="javascript:void(0);"><img src="../images/profile/play.gif" onmouseover="this.src='../images/profile/play-over.gif'" onmouseout="this.src='../images/profile/play.gif'" /></a>
										<a href="javascript:void(0);"><img src="../images/profile/add.gif"  onmouseover="this.src='../images/profile/add-over.gif'" onmouseout="this.src='../images/profile/add.gif'"/></a>-->
									<?php
										}
									?>
										
									</div>
									<?php
									}
									elseif($_POST['profile_type_search']=='project' || (($profile_search[$i_search_m]['artist_id']=="" || $profile_search[$i_search_m]['community_id']=="") && $profile_search[$i_search_m]['id']!="" && $_POST['profile_type_search']!='event'))
									{		
										if($profile_search[$i_search_m]['community_id']!=NULL)
										{
											$anss_butts = $newtab->getComm_details($profile_search[$i_search_m]['community_id']);
										}
										elseif($profile_search[$i_search_m]['artist_id']!=NULL)
										{
											$anss_butts = $newtab->getArtist_details($profile_search[$i_search_m]['artist_id']);
										}
										
										$find_mem_butts_pros = $display->get_member_or_not($anss_butts['general_user_id']);
										//echo $profile_search[$i_search_m]['featured_media_table'];
									?>
									<div class="player" >
										<?php
										if($profile_search[$i_search_m]['featured_media']=="Gallery" && $profile_search[$i_search_m]['media_id']!=0)
										{
											
											if($profile_search[$i_search_m]['featured_media_table']!="" &&($profile_search[$i_search_m]['featured_media_table']=="general_artist_gallery_list" || $profile_search[$i_search_m]['featured_media_table']=="general_community_gallery_list"))
											{
												$get_register_featured_gallery = $search_criteria ->get_register_featured_media_gallery($profile_search[$i_search_m]['media_id'],$profile_search[$i_search_m]['featured_media_table']);
												$all_register_images ="";
												$feature_reg_big_img ="";
												$featured_reg_gal_img_title ="";
												for($cou_i=0;$cou_i<count($get_register_featured_gallery);$cou_i++)
												{
													$feature_reg_big_img = $get_register_featured_gallery[0]['image_name'];
													$all_register_images = $all_register_images.",".$get_register_featured_gallery[$cou_i]['image_name'];
													$featured_reg_gal_img_title = $featured_reg_gal_img_title .",".$get_register_featured_gallery[$cou_i]['image_title'];
												}
												$all_register_images = ltrim($all_register_images,',');
												$featured_reg_gal_img_title = ltrim($featured_reg_gal_img_title,',');
												$feature_reg_org_path = "http://reggallery.s3.amazonaws.com/";
												$feature_reg_thum_path = "http://reggalthumb.s3.amazonaws.com/";
											}
											else if($profile_search[$i_search_m]['featured_media_table']!="" && $profile_search[$i_search_m]['featured_media_table']=="general_media"){
												$get_register_featured_gallery = $search_criteria ->get_featured_gallery_images($profile_search[$i_search_m]['media_id']);
												$all_register_images ="";
												$feature_reg_big_img ="";
												$featured_reg_gal_img_title ="";
												for($cou_i=0;$cou_i<count($get_register_featured_gallery);$cou_i++)
												{
													$feature_reg_big_img = $get_register_featured_gallery[0]['image_name'];
													$all_register_images = $all_register_images.",".$get_register_featured_gallery[$cou_i]['image_name'];
													$featured_reg_gal_img_title = $featured_reg_gal_img_title .",".$get_register_featured_gallery[$cou_i]['image_title'];
												}
												$all_register_images = ltrim($all_register_images,',');
												$featured_reg_gal_img_title = ltrim($featured_reg_gal_img_title,',');
												$feature_reg_org_path = "http://medgallery.s3.amazonaws.com/";
												$feature_reg_thum_path = "http://medgalthumb.s3.amazonaws.com/";
											}
										?>
										<a href="javascript:void(0);"><img src="../images/profile/play.gif" onmouseover="this.src='../images/profile/play-over.gif'" onmouseout="this.src='../images/profile/play.gif'" onclick="checkload('<?php echo $feature_reg_big_img;?>','<?php echo $all_register_images; ?>','<?php echo $feature_reg_thum_path; ?>','<?php echo $feature_reg_org_path; ?>','<?php echo $featured_reg_gal_img_title; ?>')" title="Play file now."/></a>
										<!--<a href="javascript:void(0);"><img src="../images/profile/add.gif"  onmouseover="this.src='../images/profile/add-over.gif'" onmouseout="this.src='../images/profile/add.gif'" title="Add file to the bottom of your playlist without stopping current media."/></a>-->
										<?php
										$chk_ac_ep = 0;
										if($find_mem_butts_pros != null && $find_mem_butts_pros['general_user_id'] != null && ($find_mem_butts_pros['expiry_date'] >$date || $find_mem_butts_pros['lifetime']==1))
									{
											if($profile_search[$i_search_m]['type']=="for_sale" || $profile_search[$i_search_m]['type']=="fan_club")
								{
									
									if($_SESSION['login_email']!=NULL)
									{
										for($set_alss=0;$set_alss<count($down_button);$set_alss++)
										{
											if($down_button[$set_alss] !="")
											{
												if(isset($down_button[$set_alss]['artist_id']) && isset($profile_search[$i_search_m]['artist_id']))
												{	
													$org_exp_ids = explode('~',$down_button[$set_alss]['id']);
													
													if($org_exp_ids[0]==$profile_search[$i_search_m]['id'])
													{
														$chk_ac_ep = 1;
														break;
													}
												}
												elseif(isset($down_button[$set_alss]['community_id']) && isset($profile_search[$i_search_m]['community_id']))
												{
													$org_exp_ids = explode('~',$down_button[$set_alss]['id']);
													
													if($org_exp_ids[0]==$profile_search[$i_search_m]['id'])
													{
														$chk_ac_ep = 1;
														break;
													}
												}
											}
										}
									}
									if($chk_ac_ep==0)
									{
							?>
										<a href="javascript:void(0);" onClick="<?php if($profile_search[$i_search_m]['type']=="fan_club") { echo "fancy_login('".$profile_search[$i_search_m]['profile_url']."')"; } elseif($profile_search[$i_search_m]['type']=="for_sale") { echo "click_fancy_login_down('".$profile_search[$i_search_m]['profile_url']."')"; } ?>"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>
							<?php
									}
									else
									{
							?>
										<a href="javascript:void(0);" onclick="download_directly_project('<?php if($profile_search[$i_search_m]['type']=="fan_club") { echo $profile_search[$i_search_m]['fan_song']; } elseif($profile_search[$i_search_m]['type']=="for_sale") { echo $profile_search[$i_search_m]['sale_song']; } ?>','<?php echo $profile_search[$i_search_m]['title']; ?>','<?php echo $profile_search[$i_search_m]['creator'];?>')"><img src="../images/profile/download_down.gif" onmouseover="this.src='../images/profile/download-over_down.gif'" onmouseout="this.src='../images/profile/download_down.gif'" title="Add media to your cart for free download or purchase." /></a>
							<?php
									}
								}
								}
											?>
											<a href="fan_club.php?data=<?php echo $profile_search[$i_search_m]['profile_url'];?>" id="fan_club_pro<?php echo $profile_search[$i_search_m]['profile_url'];?>" style="display:none;" ><?php if($profile_search[$i_search_m]['type']=="fan_club") { echo "Fan Club"; } elseif($profile_search[$i_search_m]['type']=="for_sale") { echo "Purchase"; } ?></a>
											<script type="text/javascript">
											$(document).ready(function() {
													$("#fan_club_pro<?php echo $profile_search[$i_search_m]['profile_url'];?>").fancybox({
													'width'				: '69%',
													'height'			: '80%',
													'transitionIn'		: 'none',
													'transitionOut'		: 'none',
													'type'				: 'iframe'
													});
											});
											</script>
											<?php
											$all_register_images ="";
											$feature_reg_big_img ="";
											$featured_reg_gal_img_title ="";
										}
										else if($profile_search[$i_search_m]['featured_media']=="Song" && $profile_search[$i_search_m]['media_id']!=0)
										{
											include_once("findsale_song.php");
											if($profile_search[$i_search_m]['featured_media_table']=="general_artist_audio" || $profile_search[$i_search_m]['featured_media_table']=="general_community_audio")
											{
												?>
													<a href="javascript:void(0);"><img src="../images/profile/play.gif" onmouseover="this.src='../images/profile/play-over.gif'" onmouseout="this.src='../images/profile/play.gif'" onclick="showPlayer_reg('a','<?php echo $profile_search[$i_search_m]['media_id'];?>','play_reg','<?php echo $profile_search[$i_search_m]['featured_media_table'];?>')" title="Play file now."/></a>
													<a href="javascript:void(0);"><img src="../images/profile/add.gif"  onmouseover="this.src='../images/profile/add-over.gif'" onmouseout="this.src='../images/profile/add.gif'" onclick="showPlayer_reg('a','<?php echo $profile_search[$i_search_m]['media_id'];?>','add_reg','<?php echo $profile_search[$i_search_m]['featured_media_table'];?>')" title="Add file to the bottom of your playlist without stopping current media."/></a>
												<?php
											}
											else{
												?>
													<a href="javascript:void(0);"><img src="../images/profile/play.gif" onmouseover="this.src='../images/profile/play-over.gif'" onmouseout="this.src='../images/profile/play.gif'" onclick="showPlayer('a','<?php echo $profile_search[$i_search_m]['media_id'];?>','play')" title="Play file now."/></a>
													<a href="javascript:void(0);"><img src="../images/profile/add.gif"  onmouseover="this.src='../images/profile/add-over.gif'" onmouseout="this.src='../images/profile/add.gif'" onclick="showPlayer('a','<?php echo $profile_search[$i_search_m]['media_id'];?>','add')" title="Add file to the bottom of your playlist without stopping current media."/></a>
												<?php
											}
											$get_media_info_down_gal = $search_criteria ->get_fetatured_media_info($profile_search[$i_search_m]['media_id']);
											$chk_ac_ep = 0;
											if($find_mem_butts_pros != null && $find_mem_butts_pros['general_user_id'] != null && ($find_mem_butts_pros['expiry_date'] >$date || $find_mem_butts_pros['lifetime']==1))
									{
											if($profile_search[$i_search_m]['type']=="for_sale" || $profile_search[$i_search_m]['type']=="fan_club")
								{
									
									if($_SESSION['login_email']!=NULL)
									{
										for($set_alss=0;$set_alss<count($down_button);$set_alss++)
										{
											if($down_button[$set_alss] !="")
											{
												if(isset($down_button[$set_alss]['artist_id']) && isset($profile_search[$i_search_m]['artist_id']))
												{	
													$org_exp_ids = explode('~',$down_button[$set_alss]['id']);
													
													if($org_exp_ids[0]==$profile_search[$i_search_m]['id'])
													{
														$chk_ac_ep = 1;
														break;
													}
												}
												elseif(isset($down_button[$set_alss]['community_id']) && isset($profile_search[$i_search_m]['community_id']))
												{
													$org_exp_ids = explode('~',$down_button[$set_alss]['id']);
													
													if($org_exp_ids[0]==$profile_search[$i_search_m]['id'])
													{
														$chk_ac_ep = 1;
														break;
													}
												}
											}
										}
									}
									if($chk_ac_ep==0)
									{
							?>
										<a href="javascript:void(0);" onClick="<?php if($profile_search[$i_search_m]['type']=="fan_club") { echo "fancy_login('".$profile_search[$i_search_m]['profile_url']."')"; } elseif($profile_search[$i_search_m]['type']=="for_sale") { echo "click_fancy_login_down('".$profile_search[$i_search_m]['profile_url']."')"; } ?>"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>
							<?php
									}
									else
									{
							?>
										<a href="javascript:void(0);" onclick="download_directly_project('<?php if($profile_search[$i_search_m]['type']=="fan_club") { echo $profile_search[$i_search_m]['fan_song']; } elseif($profile_search[$i_search_m]['type']=="for_sale") { echo $profile_search[$i_search_m]['sale_song']; } ?>','<?php echo $profile_search[$i_search_m]['title']; ?>','<?php echo $profile_search[$i_search_m]['creator'];?>')"><img src="../images/profile/download_down.gif" onmouseover="this.src='../images/profile/download-over_down.gif'" onmouseout="this.src='../images/profile/download_down.gif'" title="Add media to your cart for free download or purchase." /></a>
							<?php
									}
								}
								}
											?>
											<a href="fan_club.php?data=<?php echo $profile_search[$i_search_m]['profile_url'];?>" id="fan_club_pro<?php echo $profile_search[$i_search_m]['profile_url'];?>" style="display:none;" ><?php if($profile_search[$i_search_m]['type']=="fan_club") { echo "Fan Club"; } elseif($profile_search[$i_search_m]['type']=="for_sale") { echo "Purchase"; } ?></a>
											<script type="text/javascript">
											$(document).ready(function() {
													$("#fan_club_pro<?php echo $profile_search[$i_search_m]['profile_url'];?>").fancybox({
													'width'				: '69%',
													'height'			: '80%',
													'transitionIn'		: 'none',
													'transitionOut'		: 'none',
													'type'				: 'iframe'
													});
											});
											</script>
											<?php
										}
										else if($profile_search[$i_search_m]['featured_media']=="Video" && $profile_search[$i_search_m]['media_id']!=0)
										{
											include_once("findsale_video.php");
											if($profile_search[$i_search_m]['featured_media_table']=="general_artist_video" || $profile_search[$i_search_m]['featured_media_table']=="general_community_video")
											{
												?>
													<a href="javascript:void(0);"><img src="../images/profile/play.gif" onmouseover="this.src='../images/profile/play-over.gif'" onmouseout="this.src='../images/profile/play.gif'" onclick="showPlayer_reg('v','<?php echo $profile_search[$i_search_m]['media_id'];?>','playlist_reg','<?php echo $profile_search[$i_search_m]['featured_media_table'];?>')" title="Play file now."/></a>
													<a href="javascript:void(0);"><img src="../images/profile/add.gif"  onmouseover="this.src='../images/profile/add-over.gif'" onmouseout="this.src='../images/profile/add.gif'" onclick="showPlayer_reg('v','<?php echo $profile_search[$i_search_m]['media_id'];?>','addvi_reg','<?php echo $profile_search[$i_search_m]['featured_media_table'];?>')" title="Add file to the bottom of your playlist without stopping current media."/></a>
												<?php
											}else{
												?>
													<a href="javascript:void(0);"><img src="../images/profile/play.gif" onmouseover="this.src='../images/profile/play-over.gif'" onmouseout="this.src='../images/profile/play.gif'" onclick="showPlayer('v','<?php echo $profile_search[$i_search_m]['media_id'];?>','playlist')" title="Play file now."/></a>
													<a href="javascript:void(0);"><img src="../images/profile/add.gif"  onmouseover="this.src='../images/profile/add-over.gif'" onmouseout="this.src='../images/profile/add.gif'" onclick="showPlayer('v','<?php echo $profile_search[$i_search_m]['media_id'];?>','addvi')" title="Add file to the bottom of your playlist without stopping current media."/></a>
												<?php
											}
											$get_media_info_down_gal = $search_criteria ->get_fetatured_media_info($profile_search[$i_search_m]['media_id']);
											$chk_ac_ep = 0;
											if($find_mem_butts_pros != null && $find_mem_butts_pros['general_user_id'] != null && ($find_mem_butts_pros['expiry_date'] >$date || $find_mem_butts_pros['lifetime']==1))
									{
											if($profile_search[$i_search_m]['type']=="for_sale" || $profile_search[$i_search_m]['type']=="fan_club")
								{
									
									if($_SESSION['login_email']!=NULL)
									{
										for($set_alss=0;$set_alss<count($down_button);$set_alss++)
										{
											if($down_button[$set_alss] !="")
											{
												if(isset($down_button[$set_alss]['artist_id']) && isset($profile_search[$i_search_m]['artist_id']))
												{	
													$org_exp_ids = explode('~',$down_button[$set_alss]['id']);
													
													if($org_exp_ids[0]==$profile_search[$i_search_m]['id'])
													{
														$chk_ac_ep = 1;
														break;
													}
												}
												elseif(isset($down_button[$set_alss]['community_id']) && isset($profile_search[$i_search_m]['community_id']))
												{
													$org_exp_ids = explode('~',$down_button[$set_alss]['id']);
													
													if($org_exp_ids[0]==$profile_search[$i_search_m]['id'])
													{
														$chk_ac_ep = 1;
														break;
													}
												}
											}
										}
									}
									if($chk_ac_ep==0)
									{
							?>
										<a href="javascript:void(0);" onClick="<?php if($profile_search[$i_search_m]['type']=="fan_club") { echo "fancy_login('".$profile_search[$i_search_m]['profile_url']."')"; } elseif($profile_search[$i_search_m]['type']=="for_sale") { echo "click_fancy_login_down('".$profile_search[$i_search_m]['profile_url']."')"; } ?>"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>
							<?php
									}
									else
									{
							?>
										<a href="javascript:void(0);" onclick="download_directly_project('<?php if($profile_search[$i_search_m]['type']=="fan_club") { echo $profile_search[$i_search_m]['fan_song']; } elseif($profile_search[$i_search_m]['type']=="for_sale") { echo $profile_search[$i_search_m]['sale_song']; } ?>','<?php echo $profile_search[$i_search_m]['title']; ?>','<?php echo $profile_search[$i_search_m]['creator'];?>')"><img src="../images/profile/download_down.gif" onmouseover="this.src='../images/profile/download-over_down.gif'" onmouseout="this.src='../images/profile/download_down.gif'" title="Add media to your cart for free download or purchase." /></a>
							<?php
									}
								}
								}
											?>
											<a href="fan_club.php?data=<?php echo $profile_search[$i_search_m]['profile_url'];?>" id="fan_club_pro<?php echo $profile_search[$i_search_m]['profile_url'];?>" style="display:none;" ><?php if($profile_search[$i_search_m]['type']=="fan_club") { echo "Fan Club"; } elseif($profile_search[$i_search_m]['type']=="for_sale") { echo "Purchase"; } ?></a>
											<script type="text/javascript">
											$(document).ready(function() {
													$("#fan_club_pro<?php echo $profile_search[$i_search_m]['profile_url'];?>").fancybox({
													'width'				: '69%',
													'height'			: '80%',
													'transitionIn'		: 'none',
													'transitionOut'		: 'none',
													'type'				: 'iframe'
													});
											});
											</script>
											<?php
										}
										else{
										$chk_ac_ep = 0;
										if($find_mem_butts_pros != null && $find_mem_butts_pros['general_user_id'] != null && ($find_mem_butts_pros['expiry_date'] >$date || $find_mem_butts_pros['lifetime']==1))
									{
											if($profile_search[$i_search_m]['type']=="for_sale" || $profile_search[$i_search_m]['type']=="fan_club")
								{
									
									if($_SESSION['login_email']!=NULL)
									{
										for($set_alss=0;$set_alss<count($down_button);$set_alss++)
										{
											if($down_button[$set_alss] !="")
											{
												if(isset($down_button[$set_alss]['artist_id']) && isset($profile_search[$i_search_m]['artist_id']))
												{	
													$org_exp_ids = explode('~',$down_button[$set_alss]['id']);
													
													if($org_exp_ids[0]==$profile_search[$i_search_m]['id'])
													{
														$chk_ac_ep = 1;
														break;
													}
												}
												elseif(isset($down_button[$set_alss]['community_id']) && isset($profile_search[$i_search_m]['community_id']))
												{
													$org_exp_ids = explode('~',$down_button[$set_alss]['id']);
													
													if($org_exp_ids[0]==$profile_search[$i_search_m]['id'])
													{
														$chk_ac_ep = 1;
														break;
													}
												}
											}
										}
									}
									if($chk_ac_ep==0)
									{
							?>
										<a href="javascript:void(0);" onClick="<?php if($profile_search[$i_search_m]['type']=="fan_club") { echo "fancy_login('".$profile_search[$i_search_m]['profile_url']."')"; } elseif($profile_search[$i_search_m]['type']=="for_sale") { echo "click_fancy_login_down('".$profile_search[$i_search_m]['profile_url']."')"; } ?>"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>
							<?php
									}
									else
									{
							?>
										<a href="javascript:void(0);" onclick="download_directly_project('<?php if($profile_search[$i_search_m]['type']=="fan_club") { echo $profile_search[$i_search_m]['fan_song']; } elseif($profile_search[$i_search_m]['type']=="for_sale") { echo $profile_search[$i_search_m]['sale_song']; } ?>','<?php echo $profile_search[$i_search_m]['title']; ?>','<?php echo $profile_search[$i_search_m]['creator'];?>')"><img src="../images/profile/download_down.gif" onmouseover="this.src='../images/profile/download-over_down.gif'" onmouseout="this.src='../images/profile/download_down.gif'" title="Add media to your cart for free download or purchase." /></a>
							<?php
									}
								}
								}
											?>
											<a href="fan_club.php?data=<?php echo $profile_search[$i_search_m]['profile_url'];?>" id="fan_club_pro<?php echo $profile_search[$i_search_m]['profile_url'];?>" style="display:none;" ><?php if($profile_search[$i_search_m]['type']=="fan_club") { echo "Fan Club"; } elseif($profile_search[$i_search_m]['type']=="for_sale") { echo "Purchase"; } ?></a>
											<script type="text/javascript">
											$(document).ready(function() {
													$("#fan_club_pro<?php echo $profile_search[$i_search_m]['profile_url'];?>").fancybox({
													'width'				: '69%',
													'height'			: '80%',
													'transitionIn'		: 'none',
													'transitionOut'		: 'none',
													'type'				: 'iframe'
													});
											});
											</script>
										<!--<a href="javascript:void(0);"><img src="../images/profile/play.gif" onmouseover="this.src='../images/profile/play-over.gif'" onmouseout="this.src='../images/profile/play.gif'" /></a>
										<a href="javascript:void(0);"><img src="../images/profile/add.gif"  onmouseover="this.src='../images/profile/add-over.gif'" onmouseout="this.src='../images/profile/add.gif'"/></a>-->
									<?php
										}
									?>
										
									</div>
									<?php
									}
									else if($_POST['profile_type_search']=="project")
									{
										//echo $profile_search[$i_search_m]['featured_media_table'];
									?>
									<div class="player" >
										<?php
										if($profile_search[$i_search_m]['featured_media']=="Gallery" && $profile_search[$i_search_m]['media_id']!=0)
										{
											if($profile_search[$i_search_m]['featured_media_table']!="" &&($profile_search[$i_search_m]['featured_media_table']=="general_artist_gallery_list" || $profile_search[$i_search_m]['featured_media_table']=="general_community_gallery_list"))
											{
												$get_register_featured_gallery = $search_criteria ->get_register_featured_media_gallery($profile_search[$i_search_m]['media_id'],$profile_search[$i_search_m]['featured_media_table']);
												$all_register_images ="";
												$feature_reg_big_img ="";
												$featured_reg_gal_img_title ="";
												for($cou_i=0;$cou_i<count($get_register_featured_gallery);$cou_i++)
												{
													$feature_reg_big_img = $get_register_featured_gallery[0]['image_name'];
													$all_register_images = $all_register_images.",".$get_register_featured_gallery[$cou_i]['image_name'];
													$featured_reg_gal_img_title = $featured_reg_gal_img_title .",".$get_register_featured_gallery[$cou_i]['image_title'];
												}
												$all_register_images = ltrim($all_register_images,',');
												$featured_reg_gal_img_title = ltrim($featured_reg_gal_img_title,',');
												$feature_reg_org_path = "http://reggallery.s3.amazonaws.com/";
												$feature_reg_thum_path = "http://reggalthumb.s3.amazonaws.com/";
											}
											else if($profile_search[$i_search_m]['featured_media_table']!="" && $profile_search[$i_search_m]['featured_media_table']=="general_media"){
												$get_register_featured_gallery = $search_criteria ->get_featured_gallery_images($profile_search[$i_search_m]['media_id']);
												$all_register_images ="";
												$feature_reg_big_img ="";
												$featured_reg_gal_img_title ="";
												for($cou_i=0;$cou_i<count($get_register_featured_gallery);$cou_i++)
												{
													$feature_reg_big_img = $get_register_featured_gallery[0]['image_name'];
													$all_register_images = $all_register_images.",".$get_register_featured_gallery[$cou_i]['image_name'];
													$featured_reg_gal_img_title = $featured_reg_gal_img_title .",".$get_register_featured_gallery[$cou_i]['image_title'];
												}
												$all_register_images = ltrim($all_register_images,',');
												$featured_reg_gal_img_title = ltrim($featured_reg_gal_img_title,',');
												$feature_reg_org_path = "http://medgallery.s3.amazonaws.com/";
												$feature_reg_thum_path = "http://medgalthumb.s3.amazonaws.com/";
											}
										?>
										<a href="javascript:void(0);"><img src="../images/profile/play.gif" onmouseover="this.src='../images/profile/play-over.gif'" onmouseout="this.src='../images/profile/play.gif'" onclick="checkload('<?php echo $feature_reg_big_img;?>','<?php echo $all_register_images; ?>','<?php echo $feature_reg_thum_path; ?>','<?php echo $feature_reg_org_path; ?>','<?php echo $featured_reg_gal_img_title; ?>')" title="Play file now."/></a>
										<!--<a href="javascript:void(0);"><img src="../images/profile/add.gif"  onmouseover="this.src='../images/profile/add-over.gif'" onmouseout="this.src='../images/profile/add.gif'" title="Add file to the bottom of your playlist without stopping current media."/></a>-->
										<?php
											if($profile_search[$i_search_m]['featured_media_table']!="" && $profile_search[$i_search_m]['featured_media_table']=="general_media")
											{
												$get_media_info_down_gal = $search_criteria ->get_fetatured_media_info($profile_search[$i_search_m]['media_id']);
												$get_gal_image  = $search_criteria ->get_gallery_info($profile_search[$i_search_m]['media_id']);
												if($get_media_info_down_gal['sharing_preference']==2){
												?>
											<!--		<a href="javascript:void(0);" onclick="ask_to_donate('<?php echo str_replace("'","\'",$get_media_info_down_gal['title']);?>','<?php echo str_replace("'","\'",$get_media_info_down_gal['creator']);?>','<?php echo $get_media_info_down_gal['id'];?>','download_gal_pop<?php echo $get_media_info_down_gal['id'];?>')"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>-->
													<img src="../images/profile/download-over.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download-over.gif'" title="Add media to your cart for free download or purchase." />
												 <?php
												}else{
												?>
												<img src="../images/profile/download-over.gif" />
												<?php
												}
											}else{
											?>
											<img src="../images/profile/download-over.gif" />
											<?php
											}
											?>
											<a href="addtocart.php?seller_id=<?php echo $get_media_info_down_gal['general_user_id'];?>&media_id=<?php echo $get_media_info_down_gal['id'];?>&buyer_id=<?php echo $_SESSION['gen_user_id'];?>&title=<?php echo str_replace("'","\'",$get_media_info_down_gal['title']);?>&image_name=<?php echo $get_gal_image['profile_image'];?>" id="download_gal_pop<?php echo $get_media_info_down_gal['id'];?>" style="display:none;"></a>
											<script type="text/javascript">
											$(document).ready(function() {
												$("#download_gal_pop<?php echo $get_media_info_down_gal['id'];?>").fancybox({
												'width'				: '75%',
												'height'			: '75%',
												'transitionIn'		: 'none',
												'transitionOut'		: 'none',
												'type'				: 'iframe'
												});
											});
											</script>
											<?php
											$all_register_images ="";
											$feature_reg_big_img ="";
											$featured_reg_gal_img_title ="";
										}
										else if($profile_search[$i_search_m]['featured_media']=="Song" && $profile_search[$i_search_m]['media_id']!=0)
										{
											include_once("findsale_song.php");
											if($profile_search[$i_search_m]['featured_media_table']=="general_artist_audio" || $profile_search[$i_search_m]['featured_media_table']=="general_community_audio")
											{
												?>
													<a href="javascript:void(0);"><img src="../images/profile/play.gif" onmouseover="this.src='../images/profile/play-over.gif'" onmouseout="this.src='../images/profile/play.gif'" onclick="showPlayer_reg('a','<?php echo $profile_search[$i_search_m]['media_id'];?>','play_reg','<?php echo $profile_search[$i_search_m]['featured_media_table'];?>')" title="Play file now."/></a>
													<a href="javascript:void(0);"><img src="../images/profile/add.gif"  onmouseover="this.src='../images/profile/add-over.gif'" onmouseout="this.src='../images/profile/add.gif'" onclick="showPlayer_reg('a','<?php echo $profile_search[$i_search_m]['media_id'];?>','add_reg','<?php echo $profile_search[$i_search_m]['featured_media_table'];?>')" title="Add file to the bottom of your playlist without stopping current media."/></a>
												<?php
											}
											else{
												?>
													<a href="javascript:void(0);"><img src="../images/profile/play.gif" onmouseover="this.src='../images/profile/play-over.gif'" onmouseout="this.src='../images/profile/play.gif'" onclick="showPlayer('a','<?php echo $profile_search[$i_search_m]['media_id'];?>','play')" title="Play file now."/></a>
													<a href="javascript:void(0);"><img src="../images/profile/add.gif"  onmouseover="this.src='../images/profile/add-over.gif'" onmouseout="this.src='../images/profile/add.gif'" onclick="showPlayer('a','<?php echo $profile_search[$i_search_m]['media_id'];?>','add')" title="Add file to the bottom of your playlist without stopping current media."/></a>
												<?php
											}
											$get_media_info_down_gal = $search_criteria ->get_fetatured_media_info($profile_search[$i_search_m]['media_id']);
											if($get_media_info_down_gal['sharing_preference']==5 || $get_media_info_down_gal['sharing_preference']==2)
											{
											$song_image_name_cart = $search_criteria->get_audio_img($get_media_info_down_gal['id']);
												$free_down_rel ="";
												for($find_down=0;$find_down<=count($get_songs_pnew_w);$find_down++)
												{
													if($get_songs_pnew_w[$find_down]['id']== $get_media_info_down_gal['id']){
													$free_down_rel = "yes";
													}
													else{
														if($free_down_rel =="" || $free_down_rel =="no"){
															$free_down_rel = "no";
														}
													}
												}
												if($free_down_rel == "yes"){
												 ?>
											<!--	 <a href="javascript:void(0);" onclick="download_directly_song('<?php echo $get_media_info_down_gal['id'];?>','<?php echo str_replace("'","\'",$get_media_info_down_gal['title']); ?>','<?php echo str_replace("'","\'",$get_media_info_down_gal['creator']); ?>','<?php echo str_replace("'","\'",$get_media_info_down_gal['from']); ?>')"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>-->
												 <img src="../images/profile/download-over.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download-over.gif'" title="Add media to your cart for free download or purchase." />
												 
											 <?php
												}
												else if($get_media_info_down_gal['sharing_preference']==2){
												?>
													<!--<a href="javascript:void(0);" onclick="ask_to_donate('<?php echo str_replace("'","\'",$get_media_info_down_gal['title']);?>','<?php echo str_replace("'","\'",$get_media_info_down_gal['creator']);?>','<?php echo $get_media_info_down_gal['id'];?>','download_song<?php echo $get_media_info_down_gal['id'];?>')"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>
													-->
													<img src="../images/profile/download-over.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download-over.gif'" title="Add media to your cart for free download or purchase." />
												<?php
												}
												else{
											?>
												<!--<a href="javascript:void(0);" onclick="down_song_pop('<?php echo $get_media_info_down_gal['id'];?>','<?php echo str_replace("'","\'",$get_media_info_down_gal['title']);?>','<?php echo str_replace("'","\'",$get_media_info_down_gal['creator']);?>')"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>-->
												<img src="../images/profile/download-over.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download-over.gif'" title="Add media to your cart for free download or purchase." />
											<?php
												}
											}else{
											?>
											<img src="../images/profile/download-over.gif" />
											<?php
											}
											?>
											<a href="addtocart.php?seller_id=<?php echo $get_media_info_down_gal['general_user_id']; ?>&media_id=<?php echo $get_media_info_down_gal['id']; ?>&buyer_id=<?php echo $_SESSION['gen_user_id'];?>&title=<?php echo str_replace("'","\'",$get_media_info_down_gal['title']);?>&image_name=<?php echo $song_image_name_cart;?>" id="download_song<?php echo $get_media_info_down_gal['id']; ?>" style="display:none;"></a>
											<script type="text/javascript">
											$(document).ready(function() {
													$("#download_song<?php echo $get_media_info_down_gal['id']; ?>").fancybox({
													'width'				: '75%',
													'height'			: '75%',
													'transitionIn'		: 'none',
													'transitionOut'		: 'none',
													'type'				: 'iframe'
													});
											});
											</script>
											<?php
										}
										else if($profile_search[$i_search_m]['featured_media']=="Video" && $profile_search[$i_search_m]['media_id']!=0)
										{
											include_once("findsale_video.php");
											if($profile_search[$i_search_m]['featured_media_table']=="general_artist_video" || $profile_search[$i_search_m]['featured_media_table']=="general_community_video")
											{
												?>
													<a href="javascript:void(0);"><img src="../images/profile/play.gif" onmouseover="this.src='../images/profile/play-over.gif'" onmouseout="this.src='../images/profile/play.gif'" onclick="showPlayer_reg('v','<?php echo $profile_search[$i_search_m]['media_id'];?>','playlist_reg','<?php echo $profile_search[$i_search_m]['featured_media_table'];?>')" title="Play file now."/></a>
													<a href="javascript:void(0);"><img src="../images/profile/add.gif"  onmouseover="this.src='../images/profile/add-over.gif'" onmouseout="this.src='../images/profile/add.gif'" onclick="showPlayer_reg('v','<?php echo $profile_search[$i_search_m]['media_id'];?>','addvi_reg','<?php echo $profile_search[$i_search_m]['featured_media_table'];?>')" title="Add file to the bottom of your playlist without stopping current media."/></a>
												<?php
											}else{
												?>
													<a href="javascript:void(0);"><img src="../images/profile/play.gif" onmouseover="this.src='../images/profile/play-over.gif'" onmouseout="this.src='../images/profile/play.gif'" onclick="showPlayer('v','<?php echo $profile_search[$i_search_m]['media_id'];?>','playlist')" title="Play file now."/></a>
													<a href="javascript:void(0);"><img src="../images/profile/add.gif"  onmouseover="this.src='../images/profile/add-over.gif'" onmouseout="this.src='../images/profile/add.gif'" onclick="showPlayer('v','<?php echo $profile_search[$i_search_m]['media_id'];?>','addvi')" title="Add file to the bottom of your playlist without stopping current media."/></a>
												<?php
											}
											$get_media_info_down_gal = $search_criteria ->get_fetatured_media_info($profile_search[$i_search_m]['media_id']);
											if($get_media_info_down_gal['sharing_preference']==5 || $get_media_info_down_gal['sharing_preference']==2)
											{
											$tables = 'media_video';
												$get_video_image = $search_criteria->getvideoimage($tables,$profile_search[$i_search_m]['id']);
												$image_name_cart = "https://img.youtube.com/vi/".$get_video_image."/default.jpg";
												$free_down_rel_video="";
												for($find_down=0;$find_down<=count($get_vids_pnew_w);$find_down++)
												{
													if($get_vids_pnew_w[$find_down]['id']==$get_media_info_down_gal['id']){
														$free_down_rel_video = "yes";
													}
													else{
														if($free_down_rel_video =="" || $free_down_rel_video =="no"){
															$free_down_rel_video = "no";
														}
													}
												}
												if($free_down_rel_video == "yes"){
												 ?>
												<!-- <a href="javascript:void(0);" onclick="download_directly_song('<?php echo $get_media_info_down_gal['id']; ?>','<?php echo str_replace("'","\'",$get_media_info_down_gal['title']); ?>','<?php echo str_replace("'","\'",$get_media_info_down_gal['creator']); ?>','<?php echo str_replace("'","\'",$get_media_info_down_gal['from']); ?>')"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>-->
												 <img src="../images/profile/download-over.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download-over.gif'" title="Add media to your cart for free download or purchase." />
												 
											 <?php
												}
												else if($get_media_info_down_gal['sharing_preference']==2){
												?>
												<!--	<a href="javascript:void(0);" onclick="ask_to_donate('<?php echo str_replace("'","\'",$get_media_info_down_gal['title']);?>','<?php echo str_replace("'","\'",$get_media_info_down_gal['creator']);?>','<?php echo $get_media_info_down_gal['id'];?>','download_video<?php echo $get_media_info_down_gal['id'];?>')"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>-->
												<img src="../images/profile/download-over.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download-over.gif'" title="Add media to your cart for free download or purchase." />
												<?php
												}
												else{
											?>
											<!--	<a href="javascript:void(0);" onclick="down_video_pop('<?php echo $get_media_info_down_gal['id'];?>','<?php echo str_replace("'","\'",$get_media_info_down_gal['title']);?>','<?php echo str_replace("'","\'",$get_media_info_down_gal['creator']);?>')"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>-->
												<img src="../images/profile/download-over.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download-over.gif'" title="Add media to your cart for free download or purchase." />
											<?php
												}
											}else{
											?>
											<img src="../images/profile/download-over.gif" />
											<?php
											}
											?>
											<a href="addtocart.php?seller_id=<?php echo $get_media_info_down_gal['general_user_id'];?>&media_id=<?php echo $get_media_info_down_gal['id'];?>&buyer_id=<?php echo $_SESSION['gen_user_id'];?>&title=<?php echo str_replace("'","\'",$get_media_info_down_gal['title']);?>&image_name=<?php echo $image_name_cart;?>" id="download_video<?php echo $get_media_info_down_gal['id'];?>" style="display:none;"></a>
											<script type="text/javascript">
											$(document).ready(function() {
													$("#download_video<?php echo $get_media_info_down_gal['id'];?>").fancybox({
													'height'			: '75%',
													'width'				: '69%',
													'transitionIn'		: 'none',
													'transitionOut'		: 'none',
													'type'				: 'iframe'
													});
											});
											</script>
											<?php
										}
										else{
									  ?>
										<!--<a href="javascript:void(0);"><img src="../images/profile/play.gif" onmouseover="this.src='../images/profile/play-over.gif'" onmouseout="this.src='../images/profile/play.gif'" /></a>
										<a href="javascript:void(0);"><img src="../images/profile/add.gif"  onmouseover="this.src='../images/profile/add-over.gif'" onmouseout="this.src='../images/profile/add.gif'"/></a>-->
									<?php
										}
									?>
										
									</div>
									<?php
									}
									else if($_POST['city_search']!=""){
									//var_dump($profile_search[$i_search_m]);
									?>
									<div class="player" >
										<?php
										if($profile_search[$i_search_m]['featured_media']=="Gallery" && $profile_search[$i_search_m]['media_id']!=0)
										{
											if($profile_search[$i_search_m]['featured_media_table']!="" &&($profile_search[$i_search_m]['featured_media_table']=="general_artist_gallery_list" || $profile_search[$i_search_m]['featured_media_table']=="general_community_gallery_list"))
											{
												$get_register_featured_gallery = $search_criteria ->get_register_featured_media_gallery($profile_search[$i_search_m]['media_id'],$profile_search[$i_search_m]['featured_media_table']);
												$all_register_images ="";
												$feature_reg_big_img ="";
												$featured_reg_gal_img_title ="";
												for($cou_i=0;$cou_i<count($get_register_featured_gallery);$cou_i++)
												{
													$feature_reg_big_img = $get_register_featured_gallery[0]['image_name'];
													$all_register_images = $all_register_images.",".$get_register_featured_gallery[$cou_i]['image_name'];
													$featured_reg_gal_img_title = $featured_reg_gal_img_title .",".$get_register_featured_gallery[$cou_i]['image_title'];
												}
												$all_register_images = ltrim($all_register_images,',');
												$featured_reg_gal_img_title = ltrim($featured_reg_gal_img_title,',');
												$feature_reg_org_path = "http://reggallery.s3.amazonaws.com/";
												$feature_reg_thum_path = "http://reggalthumb.s3.amazonaws.com/";
											}
											else if($profile_search[$i_search_m]['featured_media_table']!="" && $profile_search[$i_search_m]['featured_media_table']=="general_media"){
												$get_register_featured_gallery = $search_criteria ->get_featured_gallery_images($profile_search[$i_search_m]['media_id']);
												$all_register_images ="";
												$feature_reg_big_img ="";
												$featured_reg_gal_img_title ="";
												for($cou_i=0;$cou_i<count($get_register_featured_gallery);$cou_i++)
												{
													$feature_reg_big_img = $get_register_featured_gallery[0]['image_name'];
													$all_register_images = $all_register_images.",".$get_register_featured_gallery[$cou_i]['image_name'];
													$featured_reg_gal_img_title = $featured_reg_gal_img_title .",".$get_register_featured_gallery[$cou_i]['image_title'];
												}
												$all_register_images = ltrim($all_register_images,',');
												$featured_reg_gal_img_title = ltrim($featured_reg_gal_img_title,',');
												$feature_reg_org_path = "http://medgallery.s3.amazonaws.com/";
												$feature_reg_thum_path = "http://medgalthumb.s3.amazonaws.com/";
											}
										?>
										<a href="javascript:void(0);"><img src="../images/profile/play.gif" onmouseover="this.src='../images/profile/play-over.gif'" onmouseout="this.src='../images/profile/play.gif'" onclick="checkload('<?php echo $feature_reg_big_img;?>','<?php echo $all_register_images; ?>','<?php echo $feature_reg_thum_path; ?>','<?php echo $feature_reg_org_path; ?>','<?php echo $featured_reg_gal_img_title; ?>')" title="Play file now."/></a>
										<!--<a href="javascript:void(0);"><img src="../images/profile/add.gif"  onmouseover="this.src='../images/profile/add-over.gif'" onmouseout="this.src='../images/profile/add.gif'" title="Add file to the bottom of your playlist without stopping current media."/></a>-->
										<?php
											if($profile_search[$i_search_m]['featured_media_table']!="" && $profile_search[$i_search_m]['featured_media_table']=="general_media")
											{
												$get_media_info_down_gal = $search_criteria ->get_fetatured_media_info($profile_search[$i_search_m]['media_id']);
												$get_gal_image  = $search_criteria ->get_gallery_info($profile_search[$i_search_m]['media_id']);
												if($get_media_info_down_gal['sharing_preference']==2){
												?>
												<!--	<a href="javascript:void(0);" onclick="ask_to_donate('<?php echo str_replace("'","\'",$get_media_info_down_gal['title']);?>','<?php echo str_replace("'","\'",$get_media_info_down_gal['creator']);?>','<?php echo $get_media_info_down_gal['id'];?>','download_gal_pop<?php echo $get_media_info_down_gal['id'];?>')"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>-->
												<img src="../images/profile/download-over.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download-over.gif'" title="Add media to your cart for free download or purchase." />
												 <?php
												}else{
												?>
												<img src="../images/profile/download-over.gif" />
												<?php
												}
											}else{
											?>
											<img src="../images/profile/download-over.gif" />
											<?php
											}
											?>
											<a href="addtocart.php?seller_id=<?php echo $get_media_info_down_gal['general_user_id'];?>&media_id=<?php echo $get_media_info_down_gal['id'];?>&buyer_id=<?php echo $_SESSION['gen_user_id'];?>&title=<?php echo str_replace("'","\'",$get_media_info_down_gal['title']);?>&image_name=<?php echo $get_gal_image['profile_image'];?>" id="download_gal_pop<?php echo $get_media_info_down_gal['id'];?>" style="display:none;"></a>
											<script type="text/javascript">
											$(document).ready(function() {
												$("#download_gal_pop<?php echo $get_media_info_down_gal['id'];?>").fancybox({
												'width'				: '75%',
												'height'			: '75%',
												'transitionIn'		: 'none',
												'transitionOut'		: 'none',
												'type'				: 'iframe'
												});
											});
											</script>
											<?php
											$all_register_images ="";
											$feature_reg_big_img ="";
											$featured_reg_gal_img_title ="";
										}
										else if($profile_search[$i_search_m]['featured_media']=="Song" && $profile_search[$i_search_m]['media_id']!=0)
										{
											include_once("findsale_song.php");
											if($profile_search[$i_search_m]['featured_media_table']=="general_artist_audio" || $profile_search[$i_search_m]['featured_media_table']=="general_community_audio")
											{
												?>
													<a href="javascript:void(0);"><img src="../images/profile/play.gif" onmouseover="this.src='../images/profile/play-over.gif'" onmouseout="this.src='../images/profile/play.gif'" onclick="showPlayer_reg('a','<?php echo $profile_search[$i_search_m]['media_id'];?>','play_reg','<?php echo $profile_search[$i_search_m]['featured_media_table'];?>')" title="Play file now."/></a>
													<a href="javascript:void(0);"><img src="../images/profile/add.gif"  onmouseover="this.src='../images/profile/add-over.gif'" onmouseout="this.src='../images/profile/add.gif'" onclick="showPlayer_reg('a','<?php echo $profile_search[$i_search_m]['media_id'];?>','add_reg','<?php echo $profile_search[$i_search_m]['featured_media_table'];?>')" title="Add file to the bottom of your playlist without stopping current media."/></a>
												<?php
											}
											else{
												?>
													<a href="javascript:void(0);"><img src="../images/profile/play.gif" onmouseover="this.src='../images/profile/play-over.gif'" onmouseout="this.src='../images/profile/play.gif'" onclick="showPlayer('a','<?php echo $profile_search[$i_search_m]['media_id'];?>','play')" title="Play file now."/></a>
													<a href="javascript:void(0);"><img src="../images/profile/add.gif"  onmouseover="this.src='../images/profile/add-over.gif'" onmouseout="this.src='../images/profile/add.gif'" onclick="showPlayer('a','<?php echo $profile_search[$i_search_m]['media_id'];?>','add')" title="Add file to the bottom of your playlist without stopping current media."/></a>
												<?php
											}
											$get_media_info_down_gal = $search_criteria ->get_fetatured_media_info($profile_search[$i_search_m]['media_id']);
											if($get_media_info_down_gal['sharing_preference']==5 || $get_media_info_down_gal['sharing_preference']==2)
											{
											$song_image_name_cart = $search_criteria->get_audio_img($get_media_info_down_gal['id']);
												$free_down_rel ="";
												for($find_down=0;$find_down<=count($get_songs_pnew_w);$find_down++)
												{
													if($get_songs_pnew_w[$find_down]['id']== $get_media_info_down_gal['id']){
													$free_down_rel = "yes";
													}
													else{
														if($free_down_rel =="" || $free_down_rel =="no"){
															$free_down_rel = "no";
														}
													}
												}
												if($free_down_rel == "yes"){
												 ?>
											<!--	 <a href="javascript:void(0);" onclick="download_directly_song('<?php echo $get_media_info_down_gal['id'];?>','<?php echo str_replace("'","\'",$get_media_info_down_gal['title']); ?>','<?php echo str_replace("'","\'",$get_media_info_down_gal['creator']); ?>','<?php echo str_replace("'","\'",$get_media_info_down_gal['from']); ?>')"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>-->
												 <img src="../images/profile/download-over.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download-over.gif'" title="Add media to your cart for free download or purchase." />
												 
											 <?php
												}
												else if($get_media_info_down_gal['sharing_preference']==2){
												?>
												<!--	<a href="javascript:void(0);" onclick="ask_to_donate('<?php echo str_replace("'","\'",$get_media_info_down_gal['title']);?>','<?php echo str_replace("'","\'",$get_media_info_down_gal['creator']);?>','<?php echo $get_media_info_down_gal['id'];?>','download_song<?php echo $get_media_info_down_gal['id'];?>')"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>-->
													<img src="../images/profile/download-over.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download-over.gif'" title="Add media to your cart for free download or purchase." />
												<?php
												}
												else{
											?>
										<!--		<a href="javascript:void(0);" onclick="down_song_pop('<?php echo $get_media_info_down_gal['id'];?>','<?php echo str_replace("'","\'",$get_media_info_down_gal['title']);?>','<?php echo str_replace("'","\'",$get_media_info_down_gal['creator']);?>')"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>-->
												<img src="../images/profile/download-over.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download-over.gif'" title="Add media to your cart for free download or purchase." />
											<?php
												}
											}else{
											?>
											<img src="../images/profile/download-over.gif" />
											<?php
											}
											?>
											<a href="addtocart.php?seller_id=<?php echo $get_media_info_down_gal['general_user_id']; ?>&media_id=<?php echo $get_media_info_down_gal['id']; ?>&buyer_id=<?php echo $_SESSION['gen_user_id'];?>&title=<?php echo str_replace("'","\'",$get_media_info_down_gal['title']);?>&image_name=<?php echo $song_image_name_cart;?>" id="download_song<?php echo $get_media_info_down_gal['id']; ?>" style="display:none;"></a>
											<script type="text/javascript">
											$(document).ready(function() {
													$("#download_song<?php echo $get_media_info_down_gal['id']; ?>").fancybox({
													'width'				: '75%',
													'height'			: '75%',
													'transitionIn'		: 'none',
													'transitionOut'		: 'none',
													'type'				: 'iframe'
													});
											});
											</script>
											<?php
										}
										else if($profile_search[$i_search_m]['featured_media']=="Video" && $profile_search[$i_search_m]['media_id']!=0)
										{
											include_once("findsale_video.php");
											if($profile_search[$i_search_m]['featured_media_table']=="general_artist_video" || $profile_search[$i_search_m]['featured_media_table']=="general_community_video")
											{
												?>
													<a href="javascript:void(0);"><img src="../images/profile/play.gif" onmouseover="this.src='../images/profile/play-over.gif'" onmouseout="this.src='../images/profile/play.gif'" onclick="showPlayer_reg('v','<?php echo $profile_search[$i_search_m]['media_id'];?>','playlist_reg','<?php echo $profile_search[$i_search_m]['featured_media_table'];?>')" title="Play file now."/></a>
													<a href="javascript:void(0);"><img src="../images/profile/add.gif"  onmouseover="this.src='../images/profile/add-over.gif'" onmouseout="this.src='../images/profile/add.gif'" onclick="showPlayer_reg('v','<?php echo $profile_search[$i_search_m]['media_id'];?>','addvi_reg','<?php echo $profile_search[$i_search_m]['featured_media_table'];?>')" title="Add file to the bottom of your playlist without stopping current media."/></a>
												<?php
											}else{
												?>
													<a href="javascript:void(0);"><img src="../images/profile/play.gif" onmouseover="this.src='../images/profile/play-over.gif'" onmouseout="this.src='../images/profile/play.gif'" onclick="showPlayer('v','<?php echo $profile_search[$i_search_m]['media_id'];?>','playlist')" title="Play file now."/></a>
													<a href="javascript:void(0);"><img src="../images/profile/add.gif"  onmouseover="this.src='../images/profile/add-over.gif'" onmouseout="this.src='../images/profile/add.gif'" onclick="showPlayer('v','<?php echo $profile_search[$i_search_m]['media_id'];?>','addvi')" title="Add file to the bottom of your playlist without stopping current media."/></a>
												<?php
											}
											$get_media_info_down_gal = $search_criteria ->get_fetatured_media_info($profile_search[$i_search_m]['media_id']);
											if($get_media_info_down_gal['sharing_preference']==5 || $get_media_info_down_gal['sharing_preference']==2)
											{
											$tables = 'media_video';
												$get_video_image = $search_criteria->getvideoimage($tables,$get_media_info_down_gal['id']);
												$image_name_cart = "https://img.youtube.com/vi/".$get_video_image."/default.jpg";
												$free_down_rel_video="";
												for($find_down=0;$find_down<=count($get_vids_pnew_w);$find_down++)
												{
													if($get_vids_pnew_w[$find_down]['id']==$get_media_info_down_gal['id']){
														$free_down_rel_video = "yes";
													}
													else{
														if($free_down_rel_video =="" || $free_down_rel_video =="no"){
															$free_down_rel_video = "no";
														}
													}
												}
												if($free_down_rel_video == "yes"){
												 ?>
											<!--	 <a href="javascript:void(0);" onclick="download_directly_song('<?php echo $get_media_info_down_gal['id']; ?>','<?php echo str_replace("'","\'",$get_media_info_down_gal['title']); ?>','<?php echo str_replace("'","\'",$get_media_info_down_gal['creator']); ?>','<?php echo str_replace("'","\'",$get_media_info_down_gal['from']); ?>')"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>-->
												 <img src="../images/profile/download-over.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download-over.gif'" title="Add media to your cart for free download or purchase." />
												 
											 <?php
												}
												else if($get_media_info_down_gal['sharing_preference']==2){
												?>
											<!--		<a href="javascript:void(0);" onclick="ask_to_donate('<?php echo str_replace("'","\'",$get_media_info_down_gal['title']);?>','<?php echo str_replace("'","\'",$get_media_info_down_gal['creator']);?>','<?php echo $get_media_info_down_gal['id'];?>','download_video<?php echo $get_media_info_down_gal['id'];?>')"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>-->
											<img src="../images/profile/download-over.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download-over.gif'" title="Add media to your cart for free download or purchase." />
												<?php
												}
												else{
											?>
											<!--	<a href="javascript:void(0);" onclick="down_video_pop('<?php echo $get_media_info_down_gal['id'];?>','<?php echo str_replace("'","\'",$get_media_info_down_gal['title']);?>','<?php echo str_replace("'","\'",$get_media_info_down_gal['creator']);?>')"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>-->
												<img src="../images/profile/download-over.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download-over.gif'" title="Add media to your cart for free download or purchase." />
											<?php
												}
											}else{
											?>
											<img src="../images/profile/download-over.gif" />
											<?php
											}
											?>
											<a href="addtocart.php?seller_id=<?php echo $get_media_info_down_gal['general_user_id'];?>&media_id=<?php echo $get_media_info_down_gal['id'];?>&buyer_id=<?php echo $_SESSION['gen_user_id'];?>&title=<?php echo str_replace("'","\'",$get_media_info_down_gal['title']);?>&image_name=<?php echo $image_name_cart;?>" id="download_video<?php echo $get_media_info_down_gal['id'];?>" style="display:none;"></a>
											<script type="text/javascript">
											$(document).ready(function() {
													$("#download_video<?php echo $get_media_info_down_gal['id'];?>").fancybox({
													'height'			: '75%',
													'width'				: '69%',
													'transitionIn'		: 'none',
													'transitionOut'		: 'none',
													'type'				: 'iframe'
													});
											});
											</script>
											<?php
										}
										else{
									  ?>
										<!--<a href="javascript:void(0);"><img src="../images/profile/play.gif" onmouseover="this.src='../images/profile/play-over.gif'" onmouseout="this.src='../images/profile/play.gif'" /></a>
										<a href="javascript:void(0);"><img src="../images/profile/add.gif"  onmouseover="this.src='../images/profile/add-over.gif'" onmouseout="this.src='../images/profile/add.gif'"/></a>-->
									<?php
										}
									?>
										
									</div>
									<?php
									}
									else if($_POST['countrySelect']!=""){
									?>
									<div class="player" >
										<?php
										if($profile_search[$i_search_m]['featured_media']=="Gallery" && $profile_search[$i_search_m]['media_id']!=0)
										{
											if($profile_search[$i_search_m]['featured_media_table']!="" &&($profile_search[$i_search_m]['featured_media_table']=="general_artist_gallery_list" || $profile_search[$i_search_m]['featured_media_table']=="general_community_gallery_list"))
											{
												$get_register_featured_gallery = $search_criteria ->get_register_featured_media_gallery($profile_search[$i_search_m]['media_id'],$profile_search[$i_search_m]['featured_media_table']);
												$all_register_images ="";
												$feature_reg_big_img ="";
												$featured_reg_gal_img_title ="";
												for($cou_i=0;$cou_i<count($get_register_featured_gallery);$cou_i++)
												{
													$feature_reg_big_img = $get_register_featured_gallery[0]['image_name'];
													$all_register_images = $all_register_images.",".$get_register_featured_gallery[$cou_i]['image_name'];
													$featured_reg_gal_img_title = $featured_reg_gal_img_title .",".$get_register_featured_gallery[$cou_i]['image_title'];
												}
												$all_register_images = ltrim($all_register_images,',');
												$featured_reg_gal_img_title = ltrim($featured_reg_gal_img_title,',');
												$feature_reg_org_path = "http://reggallery.s3.amazonaws.com/";
												$feature_reg_thum_path = "http://reggalthumb.s3.amazonaws.com/";
											}
											else if($profile_search[$i_search_m]['featured_media_table']!="" && $profile_search[$i_search_m]['featured_media_table']=="general_media"){
												$get_register_featured_gallery = $search_criteria ->get_featured_gallery_images($profile_search[$i_search_m]['media_id']);
												$all_register_images ="";
												$feature_reg_big_img ="";
												$featured_reg_gal_img_title ="";
												for($cou_i=0;$cou_i<count($get_register_featured_gallery);$cou_i++)
												{
													$feature_reg_big_img = $get_register_featured_gallery[0]['image_name'];
													$all_register_images = $all_register_images.",".$get_register_featured_gallery[$cou_i]['image_name'];
													$featured_reg_gal_img_title = $featured_reg_gal_img_title .",".$get_register_featured_gallery[$cou_i]['image_title'];
												}
												$all_register_images = ltrim($all_register_images,',');
												$featured_reg_gal_img_title = ltrim($featured_reg_gal_img_title,',');
												$feature_reg_org_path = "http://medgallery.s3.amazonaws.com/";
												$feature_reg_thum_path = "http://medgalthumb.s3.amazonaws.com/";
											}
										?>
										<a href="javascript:void(0);"><img src="../images/profile/play.gif" onmouseover="this.src='../images/profile/play-over.gif'" onmouseout="this.src='../images/profile/play.gif'" onclick="checkload('<?php echo $feature_reg_big_img;?>','<?php echo $all_register_images; ?>','<?php echo $feature_reg_thum_path; ?>','<?php echo $feature_reg_org_path; ?>','<?php echo $featured_reg_gal_img_title; ?>')" title="Play file now."/></a>
										<!--<a href="javascript:void(0);"><img src="../images/profile/add.gif"  onmouseover="this.src='../images/profile/add-over.gif'" onmouseout="this.src='../images/profile/add.gif'" title="Add file to the bottom of your playlist without stopping current media."/></a>-->
										<?php
											if($profile_search[$i_search_m]['featured_media_table']!="" && $profile_search[$i_search_m]['featured_media_table']=="general_media")
											{
												$get_media_info_down_gal = $search_criteria ->get_fetatured_media_info($profile_search[$i_search_m]['media_id']);
												$get_gal_image  = $search_criteria ->get_gallery_info($profile_search[$i_search_m]['media_id']);
												if($get_media_info_down_gal['sharing_preference']==2){
												?>
												<!--	<a href="javascript:void(0);" onclick="ask_to_donate('<?php echo str_replace("'","\'",$get_media_info_down_gal['title']);?>','<?php echo str_replace("'","\'",$get_media_info_down_gal['creator']);?>','<?php echo $get_media_info_down_gal['id'];?>','download_gal_pop<?php echo $get_media_info_down_gal['id'];?>')"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>-->
													<img src="../images/profile/download-over.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download-over.gif'" title="Add media to your cart for free download or purchase." />
												 <?php
												}else{
												?>
												<img src="../images/profile/download-over.gif" />
												<?php
												}
											}else{
											?>
											<img src="../images/profile/download-over.gif" />
											<?php
											}
											?>
											<a href="addtocart.php?seller_id=<?php echo $get_media_info_down_gal['general_user_id'];?>&media_id=<?php echo $get_media_info_down_gal['id'];?>&buyer_id=<?php echo $_SESSION['gen_user_id'];?>&title=<?php echo str_replace("'","\'",$get_media_info_down_gal['title']);?>&image_name=<?php echo $get_gal_image['profile_image'];?>" id="download_gal_pop<?php echo $get_media_info_down_gal['id'];?>" style="display:none;"></a>
											<script type="text/javascript">
											$(document).ready(function() {
												$("#download_gal_pop<?php echo $get_media_info_down_gal['id'];?>").fancybox({
												'width'				: '75%',
												'height'			: '75%',
												'transitionIn'		: 'none',
												'transitionOut'		: 'none',
												'type'				: 'iframe'
												});
											});
											</script>
											<?php
											$all_register_images ="";
											$feature_reg_big_img ="";
											$featured_reg_gal_img_title ="";
										}
										else if($profile_search[$i_search_m]['featured_media']=="Song" && $profile_search[$i_search_m]['media_id']!=0)
										{
											include_once("findsale_song.php");
											if($profile_search[$i_search_m]['featured_media_table']=="general_artist_audio" || $profile_search[$i_search_m]['featured_media_table']=="general_community_audio")
											{
												?>
													<a href="javascript:void(0);"><img src="../images/profile/play.gif" onmouseover="this.src='../images/profile/play-over.gif'" onmouseout="this.src='../images/profile/play.gif'" onclick="showPlayer_reg('a','<?php echo $profile_search[$i_search_m]['media_id'];?>','play_reg','<?php echo $profile_search[$i_search_m]['featured_media_table'];?>')" title="Play file now."/></a>
													<a href="javascript:void(0);"><img src="../images/profile/add.gif"  onmouseover="this.src='../images/profile/add-over.gif'" onmouseout="this.src='../images/profile/add.gif'" onclick="showPlayer_reg('a','<?php echo $profile_search[$i_search_m]['media_id'];?>','add_reg','<?php echo $profile_search[$i_search_m]['featured_media_table'];?>')" title="Add file to the bottom of your playlist without stopping current media."/></a>
												<?php
											}
											else{
												?>
													<a href="javascript:void(0);"><img src="../images/profile/play.gif" onmouseover="this.src='../images/profile/play-over.gif'" onmouseout="this.src='../images/profile/play.gif'" onclick="showPlayer('a','<?php echo $profile_search[$i_search_m]['media_id'];?>','play')" title="Play file now."/></a>
													<a href="javascript:void(0);"><img src="../images/profile/add.gif"  onmouseover="this.src='../images/profile/add-over.gif'" onmouseout="this.src='../images/profile/add.gif'" onclick="showPlayer('a','<?php echo $profile_search[$i_search_m]['media_id'];?>','add')" title="Add file to the bottom of your playlist without stopping current media."/></a>
												<?php
											}
											$get_media_info_down_gal = $search_criteria ->get_fetatured_media_info($profile_search[$i_search_m]['media_id']);
											if($get_media_info_down_gal['sharing_preference']==5 || $get_media_info_down_gal['sharing_preference']==2)
											{
											$song_image_name_cart = $search_criteria->get_audio_img($get_media_info_down_gal['id']);
												$free_down_rel ="";
												for($find_down=0;$find_down<=count($get_songs_pnew_w);$find_down++)
												{
													if($get_songs_pnew_w[$find_down]['id']== $get_media_info_down_gal['id']){
													$free_down_rel = "yes";
													}
													else{
														if($free_down_rel =="" || $free_down_rel =="no"){
															$free_down_rel = "no";
														}
													}
												}
												if($free_down_rel == "yes"){
												 ?>
												<!-- <a href="javascript:void(0);" onclick="download_directly_song('<?php echo $get_media_info_down_gal['id'];?>','<?php echo str_replace("'","\'",$get_media_info_down_gal['title']); ?>','<?php echo str_replace("'","\'",$get_media_info_down_gal['creator']); ?>','<?php echo str_replace("'","\'",$get_media_info_down_gal['from']); ?>')"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>-->
												<img src="../images/profile/download-over.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download-over.gif'" title="Add media to your cart for free download or purchase." />
												 
											 <?php
												}
												else if($get_media_info_down_gal['sharing_preference']==2){
												?>
													<a href="javascript:void(0);" onclick="ask_to_donate('<?php echo str_replace("'","\'",$get_media_info_down_gal['title']);?>','<?php echo str_replace("'","\'",$get_media_info_down_gal['creator']);?>','<?php echo $get_media_info_down_gal['id'];?>','download_song<?php echo $get_media_info_down_gal['id'];?>')"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>
												<?php
												}
												else{
											?>
											<!--	<a href="javascript:void(0);" onclick="down_song_pop('<?php echo $get_media_info_down_gal['id'];?>','<?php echo str_replace("'","\'",$get_media_info_down_gal['title']);?>','<?php echo str_replace("'","\'",$get_media_info_down_gal['creator']);?>')"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>-->
												<img src="../images/profile/download-over.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download-over.gif'" title="Add media to your cart for free download or purchase." />
											<?php
												}
											}else{
											?>
											<img src="../images/profile/download-over.gif" />
											<?php
											}
											?>
											<a href="addtocart.php?seller_id=<?php echo $get_media_info_down_gal['general_user_id']; ?>&media_id=<?php echo $get_media_info_down_gal['id']; ?>&buyer_id=<?php echo $_SESSION['gen_user_id'];?>&title=<?php echo str_replace("'","\'",$get_media_info_down_gal['title']);?>&image_name=<?php echo $song_image_name_cart;?>" id="download_song<?php echo $get_media_info_down_gal['id']; ?>" style="display:none;"></a>
											<script type="text/javascript">
											$(document).ready(function() {
													$("#download_song<?php echo $get_media_info_down_gal['id']; ?>").fancybox({
													'width'				: '75%',
													'height'			: '75%',
													'transitionIn'		: 'none',
													'transitionOut'		: 'none',
													'type'				: 'iframe'
													});
											});
											</script>
											<?php
										}
										else if($profile_search[$i_search_m]['featured_media']=="Video" && $profile_search[$i_search_m]['media_id']!=0)
										{
											include_once("findsale_video.php");
											if($profile_search[$i_search_m]['featured_media_table']=="general_artist_video" || $profile_search[$i_search_m]['featured_media_table']=="general_community_video")
											{
												?>
													<a href="javascript:void(0);"><img src="../images/profile/play.gif" onmouseover="this.src='../images/profile/play-over.gif'" onmouseout="this.src='../images/profile/play.gif'" onclick="showPlayer_reg('v','<?php echo $profile_search[$i_search_m]['media_id'];?>','playlist_reg','<?php echo $profile_search[$i_search_m]['featured_media_table'];?>')" title="Play file now."/></a>
													<a href="javascript:void(0);"><img src="../images/profile/add.gif"  onmouseover="this.src='../images/profile/add-over.gif'" onmouseout="this.src='../images/profile/add.gif'" onclick="showPlayer_reg('v','<?php echo $profile_search[$i_search_m]['media_id'];?>','addvi_reg','<?php echo $profile_search[$i_search_m]['featured_media_table'];?>')" title="Add file to the bottom of your playlist without stopping current media."/></a>
												<?php
											}else{
												?>
													<a href="javascript:void(0);"><img src="../images/profile/play.gif" onmouseover="this.src='../images/profile/play-over.gif'" onmouseout="this.src='../images/profile/play.gif'" onclick="showPlayer('v','<?php echo $profile_search[$i_search_m]['media_id'];?>','playlist')" title="Play file now."/></a>
													<a href="javascript:void(0);"><img src="../images/profile/add.gif"  onmouseover="this.src='../images/profile/add-over.gif'" onmouseout="this.src='../images/profile/add.gif'" onclick="showPlayer('v','<?php echo $profile_search[$i_search_m]['media_id'];?>','addvi')" title="Add file to the bottom of your playlist without stopping current media."/></a>
												<?php
											}
											$get_media_info_down_gal = $search_criteria ->get_fetatured_media_info($profile_search[$i_search_m]['media_id']);
											if($get_media_info_down_gal['sharing_preference']==5 || $get_media_info_down_gal['sharing_preference']==2)
											{
											$tables = 'media_video';
												$get_video_image = $search_criteria->getvideoimage($tables,$get_media_info_down_gal['id']);
												$image_name_cart = "https://img.youtube.com/vi/".$get_video_image."/default.jpg";
												$free_down_rel_video="";
												for($find_down=0;$find_down<=count($get_vids_pnew_w);$find_down++)
												{
													if($get_vids_pnew_w[$find_down]['id']==$get_media_info_down_gal['id']){
														$free_down_rel_video = "yes";
													}
													else{
														if($free_down_rel_video =="" || $free_down_rel_video =="no"){
															$free_down_rel_video = "no";
														}
													}
												}
												if($free_down_rel_video == "yes"){
												 ?>
											<!--	 <a href="javascript:void(0);" onclick="download_directly_song('<?php echo $get_media_info_down_gal['id']; ?>','<?php echo str_replace("'","\'",$get_media_info_down_gal['title']); ?>','<?php echo str_replace("'","\'",$get_media_info_down_gal['creator']); ?>','<?php echo str_replace("'","\'",$get_media_info_down_gal['from']); ?>')"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>-->
											<img src="../images/profile/download-over.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download-over.gif'" title="Add media to your cart for free download or purchase." />
												 
											 <?php
												}
												else if($get_media_info_down_gal['sharing_preference']==2){
												?>
												<!--	<a href="javascript:void(0);" onclick="ask_to_donate('<?php echo str_replace("'","\'",$get_media_info_down_gal['title']);?>','<?php echo str_replace("'","\'",$get_media_info_down_gal['creator']);?>','<?php echo $get_media_info_down_gal['id'];?>','download_video<?php echo $get_media_info_down_gal['id'];?>')"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>-->
													<img src="../images/profile/download-over.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download-over.gif'" title="Add media to your cart for free download or purchase." />
												<?php
												}
												else{
											?>
											<!--	<a href="javascript:void(0);" onclick="down_video_pop('<?php echo $get_media_info_down_gal['id'];?>','<?php echo str_replace("'","\'",$get_media_info_down_gal['title']);?>','<?php echo str_replace("'","\'",$get_media_info_down_gal['creator']);?>')"><img src="../images/profile/download.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download.gif'" title="Add media to your cart for free download or purchase." /></a>-->
											<img src="../images/profile/download-over.gif" onmouseover="this.src='../images/profile/download-over.gif'" onmouseout="this.src='../images/profile/download-over.gif'" title="Add media to your cart for free download or purchase." />
											<?php
												}
											}else{
											?>
											<img src="../images/profile/download-over.gif" />
											<?php
											}
											?>
											<a href="addtocart.php?seller_id=<?php echo $get_media_info_down_gal['general_user_id'];?>&media_id=<?php echo $get_media_info_down_gal['id'];?>&buyer_id=<?php echo $_SESSION['gen_user_id'];?>&title=<?php echo str_replace("'","\'",$get_media_info_down_gal['title']);?>&image_name=<?php echo $image_name_cart;?>" id="download_video<?php echo $get_media_info_down_gal['id'];?>" style="display:none;"></a>
											<script type="text/javascript">
											$(document).ready(function() {
													$("#download_video<?php echo $get_media_info_down_gal['id'];?>").fancybox({
													'height'			: '75%',
													'width'				: '69%',
													'transitionIn'		: 'none',
													'transitionOut'		: 'none',
													'type'				: 'iframe'
													});
											});
											</script>
											<?php
										}
										else{
									  ?>
										<!--<a href="javascript:void(0);"><img src="../images/profile/play.gif" onmouseover="this.src='../images/profile/play-over.gif'" onmouseout="this.src='../images/profile/play.gif'" /></a>
										<a href="javascript:void(0);"><img src="../images/profile/add.gif"  onmouseover="this.src='../images/profile/add-over.gif'" onmouseout="this.src='../images/profile/add.gif'"/></a>-->
									<?php
										}
									?>
									</div>
									<?php
									}
									else {
									?>
									<!--<div class="player">
										<img src="images/profile/play.gif" />
										<img src="images/profile/add.gif" />
										<img src="images/profile/download-dis.gif" />
									</div>-->
									<?php
									}
									
									?>
									<div class="type_subtype">
			<?php 
									
										if($_POST['profile_type_search']=='artist' || $_POST['profile_type_search']=='community' || (($profile_search[$i_search_m]['artist_id']!="" || $profile_search[$i_search_m]['community_id']!="") && $profile_search[$i_search_m]['id']=="")) 
										{
											if($profile_search[$i_search_m]['profile_url']!="")
											{
			?>
												<a href="<?php echo '/'.$profile_search[$i_search_m]['profile_url']; ?>" style=""><?php if(strlen($profile_search[$i_search_m]['name'])>41){
														echo substr($profile_search[$i_search_m]['name'],0,37)."...";
													}else{
														echo $profile_search[$i_search_m]['name'];
													} ?></a>
			<?php
											}
											else
											{
												if(strlen($profile_search[$i_search_m]['name'])>41){
														echo substr($profile_search[$i_search_m]['name'],0,37)."...";
													}else{
														echo $profile_search[$i_search_m]['name'];
													}
											}
											if($_POST['profile_type_search']=='artist' || $profile_search[$i_search_m]['artist_id']!="")
											{
												$get_profile_type = $search_criteria->types_of_search('artist',$profile_search[$i_search_m]['artist_id']);
												if(!empty($get_profile_type) && $get_profile_type!=Null)
												{
													$res_profile_type = mysql_fetch_assoc($get_profile_type);
													if($res_profile_type['type_name']!=""){
													?>
														<br/><span style="font-size:13px; font-weight:normal;">
														<?php 
														if(strlen($res_profile_type['type_name'])>26){
															echo substr($res_profile_type['type_name'],0,24)."...";
														}else{
															echo $res_profile_type['type_name'];
														}
														if($res_profile_type['subtype_name']!="" && strlen($res_profile_type['type_name']<26)){
															$type_length = strlen($res_profile_type['type_name']);
															if($type_length + strlen($res_profile_type['subtype_name'])>22){
																echo ", ".substr($res_profile_type['subtype_name'],0,22 - $type_length)."...";
															}else{
																echo ", ".$res_profile_type['subtype_name'];
															}
														}
														?>
														</span>
												<?php
													}
												}
											}
											elseif($_POST['profile_type_search']=='community' || $profile_search[$i_search_m]['community_id']!="")
											{
												$get_profile_type = $search_criteria->types_of_search('community',$profile_search[$i_search_m]['community_id']);
												if(!empty($get_profile_type) && $get_profile_type!=Null)
												{
													$res_profile_type = mysql_fetch_assoc($get_profile_type);
													if($res_profile_type['type_name']!=""){
													?>
														<br/><span style="font-size:13px; font-weight:normal;">
														<?php 
														if(strlen($res_profile_type['type_name'])>26){
															echo substr($res_profile_type['type_name'],0,24)."...";
														}else{
															echo $res_profile_type['type_name'];
														}
														
														if($res_profile_type['subtype_name']!="" && strlen($res_profile_type['type_name']<26)){
															$type_length = strlen($res_profile_type['type_name']);
															if($type_length + strlen($res_profile_type['subtype_name'])>22){
																echo ", ".substr($res_profile_type['subtype_name'],0,22 - $type_length)."...";
															}else{
																echo ", ".$res_profile_type['subtype_name'];
															}
														}
														?>
														</span>
												<?php
													}
												}
											}
										}
										elseif($_POST['profile_type_search']=='event')
										{
											if($profile_search[$i_search_m]['profile_url']!="")
											{
			?>
												<a href="<?php echo '/'.$profile_search[$i_search_m]['profile_url']; ?>" style=""><?php
												if(strlen($profile_search[$i_search_m]['title'])>=22)
													{
														echo substr($profile_search[$i_search_m]['title'],0,19)."...";
													}
													else
													{
														echo $profile_search[$i_search_m]['title'];
													}
												?></a>
			<?php
											}
											else
											{
												if(strlen($profile_search[$i_search_m]['title'])>=22)
												{
													echo substr($profile_search[$i_search_m]['title'],0,19)."...";
												}
												else
												{
													echo $profile_search[$i_search_m]['title'];
												}
											}
											if(isset($profile_search[$i_search_m]['artist_id']))
											{
					?>
											<div class="by_details">
					<?php
												$type_eves = 'artist';
												$event_name = "";
												if($profile_search[$i_search_m]['state_id']!=0)
												{
													$event_name = $display->state_event_names($profile_search[$i_search_m]['id'],$type_eves);
												}
												
												if(($profile_search[$i_search_m]['date']!="" && $profile_search[$i_search_m]['date']!='0000-00-00') || ($profile_search[$i_search_m]['state_id']!="" && $profile_search[$i_search_m]['state_id']!=0))
												{
													if($profile_search[$i_search_m]['date']!="" && $profile_search[$i_search_m]['date']!='0000-00-00')
													{
														if($event_name!="")
														{
															if(strlen($event_name)>=9)
															{
																echo date('m.d.y',strtotime($profile_search[$i_search_m]['date'])).', '.substr($event_name,0,6)."...";
															}
															else
															{
																echo date('m.d.y',strtotime($profile_search[$i_search_m]['date'])).', '.$event_name;
															}
														}
														else
														{
															echo date('m.d.y',strtotime($profile_search[$i_search_m]['date']));
														}
													}
													else
													{
														if($event_name!="")
														{
															if(strlen($event_name)>=19)
															{
																echo substr($event_name,0,16)."...";
															}
															else
															{
																echo $event_name;
															}
														}
													}
												}					
			?>
											</div>
			<?php
												$get_profile_type = $search_criteria->types_of_search('artist_event',$profile_search[$i_search_m]['id']);
												if(!empty($get_profile_type) && $get_profile_type!=Null)
												{
													$res_profile_type = mysql_fetch_assoc($get_profile_type);
													if($res_profile_type['type_name']!=""){
													?>
														<span style="font-size:13px; font-weight:normal;">
														<?php 
														if(strlen($res_profile_type['type_name'])>26){
															echo substr($res_profile_type['type_name'],0,24)."...";
														}else{
															echo $res_profile_type['type_name'];
														}
														
														if($res_profile_type['subtype_name']!="" && strlen($res_profile_type['type_name']<26)){
															$type_length = strlen($res_profile_type['type_name']);
															if($type_length + strlen($res_profile_type['subtype_name'])>22){
																echo ", ".substr($res_profile_type['subtype_name'],0,22 - $type_length)."...";
															}else{
																echo ", ".$res_profile_type['subtype_name'];
															}
														}
														?>
														</span>
												<?php
													}
												}
											}
											elseif(isset($profile_search[$i_search_m]['community_id']))
											{
											?>
											<div class="by_details">
					<?php
												$type_eves = 'community';
												$event_name = "";
												if($profile_search[$i_search_m]['state_id']!=0)
												{
													$event_name = $display->state_event_names($profile_search[$i_search_m]['id'],$type_eves);
												}
												
												if(($profile_search[$i_search_m]['date']!="" && $profile_search[$i_search_m]['date']!='0000-00-00') || ($profile_search[$i_search_m]['state_id']!="" && $profile_search[$i_search_m]['state_id']!=0))
												{
													if($profile_search[$i_search_m]['date']!="" && $profile_search[$i_search_m]['date']!='0000-00-00')
													{
														if($event_name!="")
														{
															if(strlen($event_name)>=9)
															{
																echo date('m.d.y',strtotime($profile_search[$i_search_m]['date'])).', '.substr($event_name,0,6)."...";
															}
															else
															{
																echo date('m.d.y',strtotime($profile_search[$i_search_m]['date'])).', '.$event_name;
															}
														}
														else
														{
															echo date('m.d.y',strtotime($profile_search[$i_search_m]['date']));
														}
													}
													else
													{
														if($event_name!="")
														{
															if(strlen($event_name)>=19)
															{
																echo substr($event_name,0,16)."...";
															}
															else
															{
																echo $event_name;
															}
														}
													}
												}					
			?>
											</div>
			<?php
												$get_profile_type = $search_criteria->types_of_search('community_event',$profile_search[$i_search_m]['id']);
												if(!empty($get_profile_type) && $get_profile_type!=Null)
												{
													$res_profile_type = mysql_fetch_assoc($get_profile_type);
													if($res_profile_type['type_name']!=""){
													?>
														<span style="font-size:13px; font-weight:normal;">
														<?php 
														if(strlen($res_profile_type['type_name'])>26){
															echo substr($res_profile_type['type_name'],0,24)."...";
														}else{
															echo $res_profile_type['type_name'];
														}
														
														if($res_profile_type['subtype_name']!="" && strlen($res_profile_type['type_name']<26)){
															$type_length = strlen($res_profile_type['type_name']);
															if($type_length + strlen($res_profile_type['subtype_name'])>22){
																echo ", ".substr($res_profile_type['subtype_name'],0,22 - $type_length)."...";
															}else{
																echo ", ".$res_profile_type['subtype_name'];
															}
														}
														?>
														</span>
												<?php
													}
												}
											}
										}
										elseif($_POST['profile_type_search']=='project' || (($profile_search[$i_search_m]['artist_id']=="" || $profile_search[$i_search_m]['community_id']=="") && $profile_search[$i_search_m]['id']!="" && $_POST['profile_type_search']!='event'))
										{
											
											if(isset($profile_search[$i_search_m]['profile_url']))
											{
			?>
												<a href="<?php echo '/'.$profile_search[$i_search_m]['profile_url']; ?>" style=""><?php 
												if($profile_search[$i_search_m]['table_name']=="artist_event" || $profile_search[$i_search_m]['table_name']=="community_event")
												{ 
													if(strlen($profile_search[$i_search_m]['title'])>31)
													{
														echo date('m.d.y',strtotime($profile_search[$i_search_m]['date'])).' '.substr($profile_search[$i_search_m]['title'],0,29)."...";
													}
													else
													{
														echo date('m.d.y',strtotime($profile_search[$i_search_m]['date'])).' '.$profile_search[$i_search_m]['title'];
													}
												} 
												else{ 
													if(strlen($profile_search[$i_search_m]['title'])>41){
														echo substr($profile_search[$i_search_m]['title'],0,37)."...";
													}else{
														echo $profile_search[$i_search_m]['title'];
													} 
												}
												?></a>
			<?php
											}
											else
											{
												if($profile_search[$i_search_m]['media_type']=='113'){
													$get_image = $search_criteria->get_media_images($profile_search[$i_search_m]['id']);
													$orgPath="http://medgallery.s3.amazonaws.com/";
													$thumbPath="http://medgalthumb.s3.amazonaws.com/";
													
													$all_images ="";
													for($cg_i=0;$cg_i<count($get_image);$cg_i++)
													{
														$mediaSrc = $get_image[0]['image_name'];
														$all_images = $all_images .",".$get_image[$cg_i]['image_name'];
														//if($get_image[$cg_i]['image_title'] !="" || $get_image[$cg_i]['image_title']!= " " || $get_image[$cg_i]['image_title'] != null){
														$all_images_title = $all_images_title .",".$get_image[$cg_i]['image_title'];//}
													}
													$all_images = ltrim($all_images,',');
													$all_images_title = ltrim($all_images_title,',');
													?>
														<a href="javascript:void(0);" onclick="checkload('<?php echo $mediaSrc;?>','<?php echo $all_images; ?>','<?php echo $thumbPath; ?>','<?php echo $orgPath; ?>','<?php echo $all_images_title; ?>','<?php echo $profile_search[$i_search_m]['id'];?>')" >
														<?php if(strlen($profile_search[$i_search_m]['title'])>22){
														echo substr($profile_search[$i_search_m]['title'],0,19)."...<br/>";
														}else{
														echo $profile_search[$i_search_m]['title']."<br/>";
														} 
												?>				</a>
														<div class="by_details">
										<?php		
														$by_cret = $search_criteria->get_creator_by($profile_search[$i_search_m]['id']);
														
														if($by_cret[1]!="")
														{ 
															$purl_ps = $by_cret[1]; 
														} 
														else
														{ 
															$purl_ps = 'javascript:void(0)';
														}
														
														if($by_cret[0]!="")
														{
															if(strlen($by_cret[0])>=19)
															{
																echo "by <a href='".$purl_ps."'>".substr($by_cret[0],0,16)."</a>...";
															}
															else
															{
																echo "by <a href='".$purl_ps."'>".$by_cret[0]."</a>";
															}
														}
														?>
														</div>
													<?php
													$mediaSrc = "";
													$all_images = "";
													$all_images_title = "";
												}else{
													if($profile_search[$i_search_m]['media_type']=='114')
													{
										?>
														<a href="javascript:void(0);" onclick="showPlayer('a','<?php if($profile_search[$i_search_m]['id']!=""){echo $profile_search[$i_search_m]['id'];}else{echo $profile_search[$i_search_m]['id'];}?>','play',<?php echo $profile_search[$i_search_m]['general_user_id'];?>);">
										<?php
														if(strlen($profile_search[$i_search_m]['title'])>=22){
															echo substr($profile_search[$i_search_m]['title'],0,19)."</a>...<br/>";
														}else{
															echo $profile_search[$i_search_m]['title']."</a><br/>";
														}
													}
													elseif($profile_search[$i_search_m]['media_type']=='115')
													{
										?>
														<a href="javascript:void(0);" onclick="showPlayer('v','<?php if($profile_search[$i_search_m]['id']!=""){echo $profile_search[$i_search_m]['id'];}else{ echo $profile_search[$i_search_m]['id'];}?>','playlist',<?php echo $profile_search[$i_search_m]['general_user_id'];?>);">
										<?php
														if(strlen($profile_search[$i_search_m]['title'])>=22){
															echo substr($profile_search[$i_search_m]['title'],0,19)."</a>...<br/>";
														}else{
															echo $profile_search[$i_search_m]['title']."</a><br/>";
														}
													}
													elseif($profile_search[$i_search_m]['media_type']=='116')
													{
										?>
														<a href="javascript:void(0);" onclick="PlayAll('channel_media_video<?php echo $i_search_m; ?>','channel_media_song<?php echo $i_search_m; ?>')">
										<?php
														if(strlen($profile_search[$i_search_m]['title'])>=22){
															echo substr($profile_search[$i_search_m]['title'],0,19)."</a>...<br/>";
														}else{
															echo $profile_search[$i_search_m]['title']."</a><br/>";
														}
													}
										?>				
														<div class="by_details">
										<?php			
													$by_cret = $search_criteria->get_creator_by($profile_search[$i_search_m]['id']);
													
													if($by_cret[1]!="")
													{ 
														$purl_ps = $by_cret[1]; 
													} 
													else
													{ 
														$purl_ps = 'javascript:void(0)';
													}
														
													if($by_cret[0]!="")
													{
														if(strlen($by_cret[0])>=19)
														{
															echo "by <a href='".$purl_ps."'>".substr($by_cret[0],0,16)."</a>...";
														}
														else
														{
															echo "by <a href='".$purl_ps."'>".$by_cret[0]."</a>";
														}
													}
										?>
														</div>
										<?php
												}
											}
											if(isset($profile_search[$i_search_m]['profile_url']))
											{
												if(isset($profile_search[$i_search_m]['artist_id']))
												{
													$get_profile_type = $search_criteria->types_of_search('artist_project',$profile_search[$i_search_m]['id']);
													if(!empty($get_profile_type) && $get_profile_type!=Null)
													{
														$res_profile_type = mysql_fetch_assoc($get_profile_type);
														if($res_profile_type['type_name']!=""){
														?>
															<br/><span style="font-size:13px; font-weight:normal;">
															<?php 
															if(strlen($res_profile_type['type_name'])>26){
															echo substr($res_profile_type['type_name'],0,24)."...";
															}else{
															echo $res_profile_type['type_name'];
															}
															
															if($res_profile_type['subtype_name']!="" && strlen($res_profile_type['type_name']<26)){
																$type_length = strlen($res_profile_type['type_name']);
																if($type_length + strlen($res_profile_type['subtype_name'])>22){
																	echo ", ".substr($res_profile_type['subtype_name'],0,22 - $type_length)."...";
																}else{
																	echo ", ".$res_profile_type['subtype_name'];
																}
															}
															?>
															</span>
													<?php
														}
													}
												}
												elseif(isset($profile_search[$i_search_m]['community_id']))
												{
													$get_profile_type = $search_criteria->types_of_search('community_project',$profile_search[$i_search_m]['id']);
													if(!empty($get_profile_type) && $get_profile_type!=Null)
													{
														$res_profile_type = mysql_fetch_assoc($get_profile_type);
														if($res_profile_type['type_name']!=""){
														?>
															<br/><span style="font-size:13px; font-weight:normal;">
															<?php 
															if(strlen($res_profile_type['type_name'])>26){
															echo substr($res_profile_type['type_name'],0,24)."...";
															}else{
																echo $res_profile_type['type_name'];
															}
															
															if($res_profile_type['subtype_name']!="" && strlen($res_profile_type['type_name']<26)){
																$type_length = strlen($res_profile_type['type_name']);
																if($type_length + strlen($res_profile_type['subtype_name'])>22){
																	echo ", ".substr($res_profile_type['subtype_name'],0,22 - $type_length)."...";
																}else{
																	echo ", ".$res_profile_type['subtype_name'];
																}
															}
															?>
															</span>
													<?php
														}
													}
												}
											}
											else
											{
												$get_profile_type = $search_criteria->types_of_search('media',$profile_search[$i_search_m]['id']);
												if(!empty($get_profile_type) && $get_profile_type!=Null)
												{
													$res_profile_type = mysql_fetch_assoc($get_profile_type);
													if($res_profile_type['type_name']!=""){
				?>
														<span style="font-size:13px; font-weight:normal;">
				<?php 
														if(strlen($res_profile_type['subtype_name'])>26){
															echo substr($res_profile_type['subtype_name'],0,24)."...";
														}else{
															echo $res_profile_type['subtype_name'];
														}
														
														if($res_profile_type['metatype_name']!="" && strlen($res_profile_type['subtype_name']<26)){
															$type_length = strlen($res_profile_type['subtype_name']);
															if($type_length + strlen($res_profile_type['metatype_name'])>22){
																echo ", ".substr($res_profile_type['metatype_name'],0,22 - $type_length)."...";
															}else{
																echo ", ".$res_profile_type['metatype_name'];
															}
														}
				?>
														</span>
				<?php
													}
												}
											}
										}
										elseif($_POST['profile_type_search']=='media' || (isset($profile_search[$i_search_m]['gallery_title']) && $profile_search[$i_search_m]['gallery_title']!="") || (isset($profile_search[$i_search_m]['video_name']) && $profile_search[$i_search_m]['video_name']!="") || (isset($profile_search[$i_search_m]['audio_name']) && $profile_search[$i_search_m]['audio_name']!=""))
										{
											if($profile_search[$i_search_m]['title']!="")
											{
												if($profile_search[$i_search_m]['media_type']=='113'){
													$get_image = $search_criteria->get_media_images($profile_search[$i_search_m]['id']);
													$orgPath="http://medgallery.s3.amazonaws.com/";
													$thumbPath="http://medgalthumb.s3.amazonaws.com/";
													
													$all_images ="";
													for($cg_i=0;$cg_i<count($get_image);$cg_i++)
													{
														$mediaSrc = $get_image[0]['image_name'];
														$all_images = $all_images .",".$get_image[$cg_i]['image_name'];
														//if($get_image[$cg_i]['image_title'] !="" || $get_image[$cg_i]['image_title']!= " " || $get_image[$cg_i]['image_title'] != null){
														$all_images_title = $all_images_title .",".$get_image[$cg_i]['image_title'];//}
													}
													$all_images = ltrim($all_images,',');
													$all_images_title = ltrim($all_images_title,',');
													?>
														<a href="javascript:void(0);" onclick="checkload('<?php echo $mediaSrc;?>','<?php echo $all_images; ?>','<?php echo $thumbPath; ?>','<?php echo $orgPath; ?>','<?php echo $all_images_title; ?>','<?php echo $profile_search[$i_search_m]['id'];?>')" >
														<?php if(strlen($profile_search[$i_search_m]['title'])>=22){
														echo substr($profile_search[$i_search_m]['title'],0,19)."...";
														}else{
															echo $profile_search[$i_search_m]['title'];
														} ?>
														</a>
													<?php
													$mediaSrc = "";
													$all_images = "";
													$all_images_title = "";
												}else{
													
													if($profile_search[$i_search_m]['media_type']=='114')
													{
										?>
														<a href="javascript:void(0);" onclick="showPlayer('a','<?php if($profile_search[$i_search_m]['id']!=""){echo $profile_search[$i_search_m]['id'];}else{echo $profile_search[$i_search_m]['id'];}?>','play',<?php echo $profile_search[$i_search_m]['general_user_id'];?>);">
										<?php
														if(strlen($profile_search[$i_search_m]['title'])>=22){
															echo substr($profile_search[$i_search_m]['title'],0,19)."</a>...<br/>";
														}else{
															echo $profile_search[$i_search_m]['title']."</a><br/>";
														}
													}
													elseif($profile_search[$i_search_m]['media_type']=='115')
													{
										?>
														<a href="javascript:void(0);" onclick="showPlayer('v','<?php if($profile_search[$i_search_m]['id']!=""){echo $profile_search[$i_search_m]['id'];}else{ echo $profile_search[$i_search_m]['id'];}?>','playlist',<?php echo $profile_search[$i_search_m]['general_user_id'];?>);">
										<?php
														if(strlen($profile_search[$i_search_m]['title'])>=22){
															echo substr($profile_search[$i_search_m]['title'],0,19)."</a>...<br/>";
														}else{
															echo $profile_search[$i_search_m]['title']."</a><br/>";
														}
													}
													elseif($profile_search[$i_search_m]['media_type']=='116')
													{
										?>
														<a href="javascript:void(0);" onclick="PlayAll('channel_media_video<?php echo $i_search_m; ?>','channel_media_song<?php echo $i_search_m; ?>')">
										<?php
														if(strlen($profile_search[$i_search_m]['title'])>=22){
															echo substr($profile_search[$i_search_m]['title'],0,19)."</a>...<br/>";
														}else{
															echo $profile_search[$i_search_m]['title']."</a><br/>";
														}
													}
												}
											}
											if($profile_search[$i_search_m]['gallery_title']!="")
											{
												if(strlen($profile_search[$i_search_m]['gallery_title'])>41){
													echo substr($profile_search[$i_search_m]['gallery_title'],0,37)."...";
												}else{
													echo $profile_search[$i_search_m]['gallery_title'];
												}
											}
											if($profile_search[$i_search_m]['video_name']!="")
											{
												if(strlen($profile_search[$i_search_m]['video_name'])>41){
														echo substr($profile_search[$i_search_m]['video_name'],0,37)."...";
												}else{
													echo $profile_search[$i_search_m]['video_name'];
												}
											}
											if($profile_search[$i_search_m]['audio_name']!="")
											{
												if(strlen($profile_search[$i_search_m]['audio_name'])>41){
														echo substr($profile_search[$i_search_m]['audio_name'],0,37)."...";
												}else{
													echo $profile_search[$i_search_m]['audio_name'];
												}
											}
											
											$get_profile_type = $search_criteria->types_of_search('media',$profile_search[$i_search_m]['id']);
											if(!empty($get_profile_type) && $get_profile_type!=Null)
											{
												$res_profile_type = mysql_fetch_assoc($get_profile_type);
												if($res_profile_type['type_name']!=""){
			?>
													<br/><span style="font-size:13px; font-weight:normal;">
			<?php 
													if(strlen($res_profile_type['subtype_name'])>26){
															echo substr($res_profile_type['subtype_name'],0,24)."...";
														}else{
															echo $res_profile_type['subtype_name'];
														}
														
													if($res_profile_type['metatype_name']!="" && strlen($res_profile_type['subtype_name']<26)){
															$type_length = strlen($res_profile_type['subtype_name']);
															if($type_length + strlen($res_profile_type['metatype_name'])>22){
																echo ", ".substr($res_profile_type['metatype_name'],0,22 - $type_length)."...";
															}else{
																echo ", ".$res_profile_type['metatype_name'];
															}
														}
			?>
													</span>
			<?php
												}
											}
										}
			?>
									</div>
								</div>	
							</div>								
			<?php
							}
			}
		}
	}
?>