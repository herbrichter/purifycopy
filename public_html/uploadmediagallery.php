<?php
include_once('commons/db.php');
session_start();
error_reporting(0);
if (!empty($_FILES)) {
		$get_image_ext = $_FILES['Filedata']['name'];
		$ext = pathinfo($get_image_ext, PATHINFO_EXTENSION);
		/*$tempFile = $_FILES['Filedata']['tmp_name'];
		switch (exif_imagetype($_FILES['Filedata']['tmp_name']))
		{
			case IMAGETYPE_GIF:
				$ext = '.GIF';
			break;

			case IMAGETYPE_JPEG:
				$ext = '.JPEG';
			break;

			case IMAGETYPE_PNG:
				$ext = '.PNG';
			break;
			case IMAGETYPE_JPG:
				$ext = '.JPG';
			break;
			case IMAGETYPE_jpg:
				$ext = '.jpg';
			break;
			case IMAGETYPE_png:
				$ext = '.png';
			break;
			case IMAGETYPE_gif:
				$ext = '.gif';
			break;
			case IMAGETYPE_jpeg:
				$ext = '.jpeg';
			break;
		}*/
	$fileName = time()."_image.".$ext;
	$ext="";
	$folderName = $_REQUEST['folder'];
	$tempFile = $_FILES['Filedata']['tmp_name'];
	$targetPath = $_SERVER['DOCUMENT_ROOT'] . '/Medias/Image/';
	$targetPath_del = $_SERVER['DOCUMENT_ROOT'] . '/Medias/Image/';
	
	$targetFile =  $targetPath . $fileName;
	move_uploaded_file($tempFile,$targetFile);
		
	$final_width_of_image = 1000;
	$final_height_of_image = 1000;
	
	if(preg_match('/[.](jpg)$/', $fileName)) {
		$im = imagecreatefromjpeg($targetPath . $fileName);
	} else if (preg_match('/[.](gif)$/', $fileName)) {
		$im = imagecreatefromgif($targetPath . $fileName);
	} else if (preg_match('/[.](png)$/', $fileName)) {
		$im = imagecreatefrompng($targetPath . $fileName);
	}else if (preg_match('/[.](jpeg)$/', $fileName)) {
		$im = imagecreatefromjpeg($targetPath . $fileName);
	}else if (preg_match('/[.](JPEG)$/', $fileName)) {
		$im = imagecreatefromjpeg($targetPath . $fileName);
	}else if(preg_match('/[.](JPG)$/', $fileName)) {
		$im = imagecreatefromjpeg($targetPath . $fileName);
	} else if (preg_match('/[.](GIF)$/', $fileName)) {
		$im = imagecreatefromGIF($targetPath . $fileName);
	} else if (preg_match('/[.](PNG)$/', $fileName)) {
		$im = imagecreatefromPNG($targetPath . $fileName);
	}

	$ox = imagesx($im);
	$oy = imagesy($im);

	if($ox<$oy)
	{
		$new_oy=1000;
		$nx = $oy/$new_oy;
		$nx = ($ox / $nx);
		$ny = $new_oy;
	}
	if($oy<=$ox)
	{
		$new_ox=1000;
		$ny = $ox/$new_ox;
		$ny = ($oy / $ny);
		$nx = $new_ox;
	}

	$nm = imagecreatetruecolor($nx, $ny);

	imagecopyresized($nm, $im, 0,0,0,0,$nx,$ny,$ox,$oy);

	if(!file_exists($targetPath)) {
	  if(!mkdir($targetPath)) {
           die("There was a problem. Please try again!");
	  }
       }

	
	if(preg_match('/[.](png)$/', $fileName) || preg_match('/[.](PNG)$/', $fileName))
	{
		imagepng($nm, $targetPath . $fileName);
	}
	elseif(preg_match('/[.](jpeg)$/', $fileName) || preg_match('/[.](jpg)$/', $fileName))
	{
		imagejpeg($nm, $targetPath . $fileName);
	}
	elseif(preg_match('/[.](gif)$/', $fileName) || preg_match('/[.](GIF)$/', $fileName))
	{
		imagegif($nm, $targetPath . $fileName);
	}
        $root = realpath($_SERVER["DOCUMENT_ROOT"]);
        if (!class_exists('S3')) require_once ($root.'/S3.php');
		if (!defined('awsAccessKey')) define('awsAccessKey', '');
		if (!defined('awsSecretKey')) define('awsSecretKey', '');
		$s3 = new S3(awsAccessKey, awsSecretKey);
		$sql_get_bucket = mysql_query("SELECT * FROM `general_user` WHERE `email`='".$_POST['bucket_name']."'");
		$run_get_bucket = mysql_fetch_assoc($sql_get_bucket);
		$bucket_name = "medgallery";
		$targetPath = "http://medgallery.s3.amazonaws.com/";
		$targetPathThumb = $_SERVER['DOCUMENT_ROOT'] .'/uploads/Media/thumb/';
		$s3->putBucket($bucket_name, S3::ACL_PUBLIC_READ);
		
	if($s3->putObject(S3::inputFile($targetFile),$bucket_name,$fileName, S3::ACL_PUBLIC_READ))
	{ 
		//echo  $run_get_bucket['bucket_name'].$fileName;
	}
	if($_GET['share_pref']==1){
		unlink($targetPath_del.$fileName);
	}
	createThumbnail($fileName, $targetPath, $targetPathThumb);
}

function createThumbnail($filename, $path_to_image_directory, $path_to_thumbs_directory) {

	$final_width_of_image = 200;
	//$path_to_image_directory = 'images/fullsized/';
	//$path_to_thumbs_directory = 'images/thumbs/';

	if(preg_match('/[.](jpg)$/', $filename)) {
		$im = imagecreatefromjpeg($path_to_image_directory . $filename);
	} else if (preg_match('/[.](gif)$/', $filename)) {
		$im = imagecreatefromgif($path_to_image_directory . $filename);
	} else if (preg_match('/[.](png)$/', $filename)) {
		$im = ImageCreateFromPNG($path_to_image_directory . $filename);
	}else if (preg_match('/[.](jpeg)$/', $filename)) {
		$im = imagecreatefromjpeg($path_to_image_directory . $filename);
	}else if (preg_match('/[.](JPEG)$/', $filename)) {
		$im = imagecreatefromjpeg($path_to_image_directory . $filename);
	}else if(preg_match('/[.](JPG)$/', $filename)) {
		$im = imagecreatefromjpeg($path_to_image_directory . $filename);
	} else if (preg_match('/[.](GIF)$/', $filename)) {
		$im = imagecreatefromGIF($path_to_image_directory . $filename);
	} else if (preg_match('/[.](PNG)$/', $filename)) {
		$im = ImageCreateFromPNG($path_to_image_directory . $filename);
	}

	$ox = imagesx($im);
	$oy = imagesy($im);

	$nx = $final_width_of_image;
	$ny = floor($oy * ($final_width_of_image / $ox));

	$nm = imagecreatetruecolor($nx, $ny);

	imagecopyresized($nm, $im, 0,0,0,0,$nx,$ny,$ox,$oy);

	if(!file_exists($path_to_thumbs_directory)) {
	  if(!mkdir($path_to_thumbs_directory)) {
           die("There was a problem. Please try again!");
	  }
       }
	
	if(preg_match('/[.](png)$/', $filename) || preg_match('/[.](PNG)$/', $filename))
	{
		ImagePng($nm, $path_to_thumbs_directory . $filename);
	}
	elseif(preg_match('/[.](jpeg)$/', $filename) || preg_match('/[.](jpg)$/', $filename))
	{
		imagejpeg($nm, $path_to_thumbs_directory . $filename);
	}
	elseif(preg_match('/[.](gif)$/', $filename) || preg_match('/[.](GIF)$/', $filename))
	{
		imagegif($nm, $path_to_thumbs_directory . $filename);
	}
	
	//$strSid = md5(uniqid(time()));
	//$strContent = chunk_split(base64_encode(file_get_contents($path_to_thumbs_directory . $filename))); 
    // header("--".$strSid."\n");
   // header("Content-Type: application/octet-stream; name=\"".$filename."\"\n"); 
    // header("Content-Transfer-Encoding: base64\n");
    // header("Content-Disposition: attachment; filename=\"".$filename."\"\n\n");
	 //header("Content-type: image/jpeg");
//header($strHeader);
	$tempFile = $nm;
	$bucket_name = "medgalthumb";
	if (!class_exists('S3'))require_once($root.'/S3.php');
	if (!defined('awsAccessKey')) define('awsAccessKey', '');
	if (!defined('awsSecretKey')) define('awsSecretKey', '');
	$s3 = new S3(awsAccessKey, awsSecretKey);
	$s3->putBucket($bucket_name, S3::ACL_PUBLIC_READ);
	$s3->putObject(S3::inputFile($path_to_thumbs_directory.$filename),$bucket_name,$filename,S3::ACL_PUBLIC_READ);
	unlink($path_to_thumbs_directory.$filename);
	echo $filename;
}
