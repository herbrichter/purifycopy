<?php
$result = array();
$errorflag='no';

$errormsg=array();
$inputparams=array();
$username='nouser';
$userpass='nopass';
$profileurl='leerick';
$galleryurl='leerick';
$format='json';

$data = file_get_contents("php://input");
$dataary=json_decode($data,true);

if (isset($dataary['format'])){    
    $format = strtolower($dataary['format']);
}
if (isset($dataary['galleryurl'])){    
    $galleryurl = strtolower($dataary['galleryurl']);
}
if(isset($dataary['profileurl'])) {
    $profileurl=$dataary['profileurl'];
}else {
    $errormessage='missing profile';
    $errormsg[]=array('errormsg'=>$errormessage);
    $result[] = array('error'=>$errormsg);
    $inputparams[] = array('profileurl'=>$profileurl);
    $inputparams[] = array('galleryurl'=>$galleryurl);
    $inputparams[] = array('format'=>$format);
    $inputparams[] = array('post'=>$dataary);
    $result[] = array('inputparams'=>$inputparams);
    $errorflag='yes';
}

if ($errorflag='no'){
    include_once("commons/db.php");
    include_once("classes/ViewArtistProfileURL.php");
    include_once('classes/ProfileDisplay.php');
   
    $new_profile_class_obj = new ViewArtistProfileURL();
    
    $foundurls=",";
    $foundgalleries=",";
    $dummy_gp = array();
	$galleryImagesDataIndex = 0;
	$galleryImagesData = ""; 
    $foundgalleryindex=-1;
    $outputvideoindex=0;
                    
    $display = new ProfileDisplay();
    $artist_check = $display->artistCheck($profileurl);
    $get_common_id=$artist_check['artist_id'];
    $userartist =$new_profile_class_obj->get_user_artist_profileurl($profileurl);
    //echo '<br> userartist <br>';
    //print_r($userartist);                   
                    
    $exp_tagged_galleries=explode(',',$userartist['taggedgalleries']);                    

    //echo '<br> tagged galleries <br>';
    //print_r($exp_tagged_galleries);
                    
    if(isset($exp_tagged_galleries) && count($exp_tagged_galleries)>0)
    {
        for($taggalleryindex=0;$taggalleryindex<count($exp_tagged_galleries);$taggalleryindex++)
        {
            $pos=strpos($foundgalleries,$exp_tagged_galleries[$taggalleryindex]);
            if ($pos===false)
            {                            
                $foundgalleries=$foundgalleries.','.$exp_tagged_galleries[$taggalleryindex];                      
                        
                //echo '<br> tagvideoindex '.$taggalleryindex.' tagged video ' .$exp_tagged_galleries[$taggalleryindex].'<br>';
                if($exp_tagged_galleries[$taggalleryindex]==""||$exp_tagged_galleries[$taggalleryindex]==" "||$exp_tagged_galleries[$taggalleryindex]==Null){continue;}
                else
                {                                   
		            // get gallery from general media table
                    //$exp_tagged_gallery_info = array();
                    //for($i=0;$i<count($exp_tagged_galleries);$i++)
                    //{
                    $foundgalleryindex++;
			        $temp_tagged_gallery_info=$new_profile_class_obj->get_gallery($exp_tagged_galleries[$taggalleryindex]);
                    
                    ////}
                    //echo '<br> tagged gallery info <br>';
                    //print_r($exp_tagged_gallery_info);
                                    
                    //// sort images by play count
                    //$sorttaggedgallery = array();
                    //foreach($exp_tagged_gallery_info as $k=>$v)
                    //{		
                    //    $sorttaggedgallery['play_count'][$k] = $v['play_count'];
                    //}
                    //if(!empty($sorttaggedgallery))
                    //{
                    //    array_multisort($sorttaggedgallery['play_count'], SORT_DESC,$exp_tagged_gallery_info);
                    //}

                    //echo '<br> sorted general media in tagged galleries <br>';
                    //print_r($exp_tagged_gallery_info);
                                    
                    //$dummy_gp = array();
                    //$galleryImagesDataIndex = 0;
                    //$galleryImagesData = "";
                    
                    //if (cleanstring($temp_tagged_gallery_info['title'])!=$galleryurl){
                    //    //echo '<br>gallery '.$galleryurl.' did not match '.cleanstring($exp_tagged_gallery_info[$foundgalleryindex]['title']).'  for artist ' .$profileurl. ' <br>';    
                    //    continue;
                    //}
                    
                    //echo '<br>found gallery '.$galleryurl.'  for artist ' .$profileurl. ' <br>';
                    $exp_tagged_gallery_info[$foundgalleryindex] = $temp_tagged_gallery_info;
		            $loopgalleryid=$exp_tagged_gallery_info[$foundgalleryindex]['id'];
                    $artistprofileurl[$loopgalleryid]=$profileurl;
                    $GalleryImageNames="";
                    $GalleryTitleNames="";
                    //echo "<br> gallery id = ".$loopgalleryid." <br>";                                    
                    //print_r($exp_tagged_gallery_info[$foundgalleryindex]);
                    if($loopgalleryid=="")
			        {
				        //echo '<br>tagged gallery NOT FOUND = '.$loopgalleryid.' <br>';
                        continue;
			        }
			        else
			        {
                        //echo "<br> processing gallery id = ".$loopgalleryid." <br>";
                        //print_r($exp_tagged_gallery_info);
				        if(in_array($loopgalleryid,$dummy_gp))
				        {
                            //echo "<br> duplicate gallery id = ".$loopgalleryid;
                        }
				        else
				        {
				            //echo '<br> Get images from general_artist_gallery <br>';
                            $dummy_gp[] = $loopgalleryid;
				            //echo "<br> Common id= ".$get_common_id."<br>";
                            //print_r($dummy_gp); 
                            //Get count of images in general_artist_gallery
				            $Get_count_img_at_reg=$new_profile_class_obj->get_count_Gallery_at_register($get_common_id,$loopgalleryid);
				            if($Get_count_img_at_reg['count(*)']>0)
				            {
                                //echo '<br>Found '.$Get_count_img_at_reg['count(*)']. ' gallery images<br>'; 
					            $Get_img_at_reg=$new_profile_class_obj->get_Gallery_at_register($get_common_id,$loopgalleryid);
					            $Get_medi_cover_pic="";
                                                
					            $play_array = array();
						        if($Get_img_at_reg!="" && $Get_img_at_reg!=Null)
						        {
							        if(mysql_num_rows($Get_img_at_reg)>0)
							        {
								        while($row = mysql_fetch_assoc($Get_img_at_reg))
								        {
									        $play_array[] = $row;
								        }
                                        $GalleryOrgPath[$loopgalleryid]="https://reggallery.s3.amazonaws.com/";
                                        $GalleryThumbPath[$loopgalleryid]="https://reggalthumb.s3.amazonaws.com/";							
								        //${'orgPath'.$taggalleryindex}="https://reggallery.s3.amazonaws.com/";
								        //${'thumbPath'.$taggalleryindex}="https://reggalthumb.s3.amazonaws.com/";
								        $register_cover_pic=$play_array[0]['cover_pic'];
								        $register_No_pic=$play_array[0]['image_name'];
                                        $GalleryImageName[$loopgalleryid]=setGalleryImage("","","",$register_cover_pic,$register_No_pic);
								        //$exp_tagged_gallerieslery=$new_profile_class_obj->get_Gallery_title_at_register($play_array[0]['gallery_id']);
								        //$galleryImagesDataIndex="";
								        for($l=0;$l<count($play_array);$l++)
								        {
                                            $loopmediaid=$play_array[$l]['id'];                                                        
									        $mediaSrc = $play_array[0]['image_name'];
									        $filePath = "https://reggallery.s3.amazonaws.com/".$mediaSrc;
									        //${'galleryImagesData'.$taggalleryindex}[$galleryImagesDataIndex]="";
									        $galleryImagesImageName[$loopmediaid]=$play_array[$l]['image_name'];
                                            $galleryImagesImageTitle[$loopmediaid]=$play_array[$l]['image_title']; 
                                            $GalleryImageNames=$GalleryImageNames.",".$galleryImagesImageName[$loopmediaid]=$play_array[$l]['image_name'];
                                            $GalleryImagetitles=$GalleryImagetitles.",".$galleryImagesImageName[$loopmediaid]=$play_array[$l]['image_title'];                                                            
									        //${'galleryImagesData'.$taggalleryindex}[$galleryImagesDataIndex] = $play_array[$l]['image_name'];
									        //${'galleryImagestitle'.$taggalleryindex}[$galleryImagesDataIndex] = $play_array[$l]['image_title'];
									        $galleryImagesDataIndex++;
                                            //echo '<br> $galleryImagesDataIndex = '.$galleryImagesDataIndex.'<br>';
								        }
                                        $GalleryImageNamesList[$loopgalleryid]=$GalleryImageNames;
                                        $GalleryImageTitlesList[$loopgalleryid]=$GalleryImagetitles;                                                        
							        }
						        }
				            } // no images found in general_artist_gallery
				            else
				            { // get images from media_images for gallery id not = 0
					            //echo '<br>Found '.$Get_count_img_at_reg['count(*)']. ' gallery images<br>'; 
                                //echo '<br>now try to get images from media_images for gallery id not = 0 for real gallery id = '.$loopgalleryid.' <br>';
                                //$exp_tagged_gallerieslery=$new_profile_class_obj->get_gallery($loopgalleryid);
					            $Get_medi_cover_pic=$new_profile_class_obj->get_media_cover_pic($loopgalleryid);
					            $sql_play1 = mysql_query("SELECT * FROM media_images WHERE media_id='".$loopgalleryid."' AND gallery_id!=0");
						
						        $play_array = array();
						        if($sql_play1!="" && $sql_play1!=Null)
						        {
							        //echo '<br>Number of Images Found = '.mysql_num_rows($sql_play1). ' in gallery id '.$loopgalleryid.'<br>';
                                    if(mysql_num_rows($sql_play1)>0)
							        {
								        while($row = mysql_fetch_assoc($sql_play1))
								        {
									        $play_array[] = $row;
								        }
                                        $GalleryOrgPath[$loopgalleryid]="https://medgallery.s3.amazonaws.com/";
                                        $GalleryThumbPath[$loopgalleryid]="https://medgalthumb.s3.amazonaws.com/";
                                        $GalleryImageName[$loopgalleryid]=setGalleryImage($Get_medi_cover_pic['profile_image'],$exp_tagged_gallery_info[$foundgalleryindex]['gallery_title'],$exp_tagged_gallery_info[$foundgalleryindex]['title'],"","");
								        //${'orgPath'.$taggalleryindex}="https://medgallery.s3.amazonaws.com/";
								        //${'thumbPath'.$taggalleryindex}="https://medgalthumb.s3.amazonaws.com/";
								        for($l=0;$l<count($play_array);$l++)
								        {
                                            $loopmediaid=$play_array[$l]['id'];
                                            //echo '<br>loop media id = '.$loopmediaid.' <br>';
									        $mediaSrc = $play_array[0]['image_name'];
									        $filePath = "https://medgallery.s3.amazonaws.com/".$mediaSrc;
									            
                                            //echo '<br>file path= '.$filePath.' <br>';
									        $galleryImagesImageName[$loopmediaid]=$play_array[$l]['image_name'];
                                            $galleryImagesImageTitle[$loopmediaid]=$play_array[$l]['image_title']; 
                                            $GalleryImageNames=$GalleryImageNames.",".$galleryImagesImageName[$loopmediaid]=$play_array[$l]['image_name'];
                                            $GalleryImagetitles=$GalleryImagetitles.",".$galleryImagesImageName[$loopmediaid]=$play_array[$l]['image_title']; 
									        //${'galleryImagesData'.$taggalleryindex}[$galleryImagesDataIndex] = $play_array[$l]['image_name'];
									        //${'galleryImagestitle'.$taggalleryindex}[$galleryImagesDataIndex] = $play_array[$l]['image_title'];
									        //echo '<br> galery image name '.$galleryImagesImageName[$loopmediaid];
                                            //echo '<br> galery image title '.$galleryImagesImageTitle[$loopmediaid];
									        $galleryImagesDataIndex++;
                                            //echo '<br> $galleryImagesDataIndex = '.$galleryImagesDataIndex.'<br>';
								        } // end of loop processing images
                                        $GalleryImageNamesList[$loopgalleryid]=$GalleryImageNames;
                                        $GalleryImageTitlesList[$loopgalleryid]=$GalleryImagetitles;                                                        
							        } // no images fond
							        else
							        { // get images from media_images with gallery id = 0
					                    //echo '<br>get images from media_images with gallery id = 0 for real gallery id = '.$loopgalleryid.' <br>';
                                        $GalleryOrgPath[$loopgalleryid]="https://medsingleimage.s3.amazonaws.com/";
                                        $GalleryThumbPath[$loopgalleryid]="https://medgalthumb.s3.amazonaws.com/"; 
                                        $GalleryImageName[$loopgalleryid]=setGalleryImage($Get_medi_cover_pic['profile_image'],$exp_tagged_gallery_info[$foundgalleryindex]['gallery_title'],$exp_tagged_gallery_info[$foundgalleryindex]['title'],"","");
                                        //${'orgPath'.$taggalleryindex}="https://medsingleimage.s3.amazonaws.com/";
								        //${'thumbPath'.$taggalleryindex}="https://medgalthumb.s3.amazonaws.com/";
								
								        $sql_play1 = mysql_query("SELECT * FROM media_images WHERE media_id='".$loopgalleryid."' AND gallery_id=0");
					
                                        $play_array = array();
								        if($sql_play1!="" && $sql_play1!=Null)
								        {
									        if(mysql_num_rows($sql_play1)>0)
									        {
										        while($row = mysql_fetch_assoc($sql_play1))
										        {
											        $play_array[] = $row;
										        }
										
										        for($l=0;$l<count($play_array);$l++)
										        {
											        $loopmediaid=$play_array[$l]['id'];    
                                                    $mediaSrc = $play_array[0]['image_name'];
											        if($play_array[0]['for_sale']==1)
											        {
                                                        $GalleryOrgPath[$loopgalleryid]="https://medgalhighres.s3.amazonaws.com/";
                                                        $GalleryThumbPath[$loopgalleryid]="https://medgalhighres.s3.amazonaws.com/";
                                                        $GalleryImageName[$loopgalleryid]=setGalleryImage($Get_medi_cover_pic['profile_image'],$exp_tagged_gallery_info[$foundgalleryindex]['gallery_title'],$exp_tagged_gallery_info[$foundgalleryindex]['title'],"","");                                                                        
												        $filePath = "https://medgalhighres.s3.amazonaws.com/".$mediaSrc;
												        //${'orgPath'.$taggalleryindex}="https://medgalhighres.s3.amazonaws.com/";
												        //${'thumbPath'.$taggalleryindex}="https://medgalhighres.s3.amazonaws.com/";
											        }
											        else
											        {
												        $filePath = "https://medsingleimage.s3.amazonaws.com/".$mediaSrc;
											        }
									                $galleryImagesImageName[$loopmediaid]=$play_array[$l]['image_name'];
                                                    $galleryImagesImageTitle[$loopmediaid]=$play_array[$l]['image_title']; 
                                                    $GalleryImageNames=$GalleryImageNames.",".$galleryImagesImageName[$loopmediaid]=$play_array[$l]['image_name'];
                                                    $GalleryImagetitles=$GalleryImagetitles.",".$galleryImagesImageName[$loopmediaid]=$play_array[$l]['image_title']; 											
											        //${'galleryImagesData'.$taggalleryindex}[$galleryImagesDataIndex] = $play_array[$l]['image_name'];
											        //${'galleryImagestitle'.$taggalleryindex}[$galleryImagesDataIndex] = $play_array[$l]['image_title'];
											        //echo $play_array[$l]['image_title'];
											        //die;
											        //echo ${'galleryImagesData'.$taggalleryindex}[$galleryImagesDataIndex];
											        $galleryImagesDataIndex++;
											        //echo '<br> $galleryImagesDataIndex = '.$galleryImagesDataIndex.'<br>';
										        } // end of loop getting image names and titles
                                                $GalleryImageNamesList[$loopgalleryid]=$GalleryImageNames;
                                                $GalleryImageTitlesList[$loopgalleryid]=$GalleryImagetitles;                                                                
									        } // no images found
								        } // no images
							        } // end of get images from media_images with gallery id = 0
						        } // no images found
						        else
						        { // get images from media_images with gallery id = 0
                                    //echo '<br>get images from media_images with gallery id = 0 for real gallery id = '.$loopgalleryid.' <br>';
                                    $GalleryOrgPath[$loopgalleryid]="https://medsingleimage.s3.amazonaws.com/";
                                    $GalleryThumbPath[$loopgalleryid]="https://medgalthumb.s3.amazonaws.com/";
                                    $GalleryImageName[$loopgalleryid]=setGalleryImage($Get_medi_cover_pic['profile_image'],$exp_tagged_gallery_info[$foundgalleryindex]['gallery_title'],$exp_tagged_gallery_info[$foundgalleryindex]['title'],"","");                                                    
							        //${'orgPath'.$taggalleryindex}="https://medsingleimage.s3.amazonaws.com/";
							        //${'thumbPath'.$taggalleryindex}="https://medgalthumb.s3.amazonaws.com/";
							
							        $sql_play1 = mysql_query("SELECT * FROM media_images WHERE media_id='".$loopgalleryid."' AND gallery_id=0");
							        $play_array = array();
							        if($sql_play1!="" && $sql_play1!=Null)
							        {
								        if(mysql_num_rows($sql_play1)>0)
								        {
									        while($row = mysql_fetch_assoc($sql_play1))
									        {
										        $play_array[] = $row;
									        }
									
									        for($l=0;$l<count($play_array);$l++)
									        {
                                                $loopmediaid=$play_array[$l]['id'];
										        $mediaSrc = $play_array[0]['image_name'];
										        if($play_array[0]['for_sale']==1)
										        {
											        $filePath = "https://medgalhighres.s3.amazonaws.com/".$mediaSrc;
                                                    $GalleryOrgPath[$loopgalleryid]="https://medgalhighres.s3.amazonaws.com/";
                                                    $GalleryThumbPath[$loopgalleryid]="https://medgalhighres.s3.amazonaws.com/";
                                                    $GalleryImageName[$loopgalleryid]=setGalleryImage($Get_medi_cover_pic['profile_image'],$exp_tagged_gallery_info[$foundgalleryindex]['gallery_title'],$exp_tagged_gallery_info[$foundgalleryindex]['title'],"","");                                                                    
											        //${'orgPath'.$taggalleryindex}="https://medgalhighres.s3.amazonaws.com/";
											        //${'thumbPath'.$taggalleryindex}="https://medgalhighres.s3.amazonaws.com/";
										        }
										        else
										        {
											        $filePath = "https://medsingleimage.s3.amazonaws.com/".$mediaSrc;
										        }
									            $galleryImagesImageName[$loopmediaid]=$play_array[$l]['image_name'];
                                                $galleryImagesImageTitle[$loopmediaid]=$play_array[$l]['image_title'];	 
                                                $GalleryImageNames=$GalleryImageNames.",".$galleryImagesImageName[$loopmediaid]=$play_array[$l]['image_name'];
                                                $GalleryImagetitles=$GalleryImagetitles.",".$galleryImagesImageName[$loopmediaid]=$play_array[$l]['image_title']; 									
										        //${'galleryImagesData'.$taggalleryindex}[$galleryImagesDataIndex] = $play_array[$l]['image_name'];
										        //${'galleryImagestitle'.$taggalleryindex}[$galleryImagesDataIndex] = $play_array[$l]['image_title'];
										        //echo $play_array[$l]['image_title'];
										        //die;
										        //echo ${'galleryImagesData'.$taggalleryindex}[$galleryImagesDataIndex];
										        $galleryImagesDataIndex++;
										        //echo '<br> $galleryImagesDataIndex = '.$galleryImagesDataIndex.'<br>';
									        } // end of loop processing images
                                            $GalleryImageNamesList[$loopgalleryid]=$GalleryImageNames;
                                            $GalleryImageTitlesList[$loopgalleryid]=$GalleryImagetitles;                                                            
								        } // no images found
							        } // no images found
						        } // end of if 						                        
					        } // end of if 
			            } // end of processing galleries                                         
                    } // end  of gallery id found
                } // tagged galleries loop
            } // skip gallery
        } // end of for gallery loop
    } // no galleries
    

    //print_r($songimagefile);
    //die;
        
    // Obtain a list of columns
    //foreach ($exp_tagged_gallery_info as $key => $row) {
    //    $from[$key]  = $row['creator'];
    //    $track[$key] = $row['title'];
    //}

    // Sort the data with volume descending, edition ascending
    // Add $data as the last parameter, to sort by the common key
    //array_multisort($from, SORT_ASC, $track, SORT_ASC, $exp_tagged_gallery_info);

  
    $result[] = array('galleries'=>$exp_tagged_gallery_info);
    $result[] = array('GalleryImageName'=>$GalleryImageName);
    $result[] = array('artistprofileurl'=>$artistprofileurl);
    $result[] = array('GalleryOrgPath'=>$GalleryOrgPath);
    $result[] = array('GalleryThumbPath'=>$GalleryThumbPath);
    $result[] = array('GalleryImageNamesList'=>$GalleryImageNamesList);
    $result[] = array('GalleryImageTitlesList'=>$GalleryImageTitlesList); 
    
    //    if($get_gallery['gallery_title'] !="" || $get_gallery['title']!="")
    //{
    //    if(isset(${'galleryImagesData'.$g}))
    //    {
    //        ${'galleryImagesData'.$g} = implode(",", ${'galleryImagesData'.$g});
    //        ${'galleryImagestitle'.$g} = implode(",", ${'galleryImagestitle'.$g});
    //    }
    
//echo '<br> Printing data before returning json<br>';    
//print ( '<pre>' );
//print_r($exp_tagged_gallery_info);
//print_r($galleryImagesImageName);
//print_r($galleryImagesImageTitle);
//print_r($GalleryOrgPath);
//print_r($GalleryThumbPath);
//print ( '</pre>' );    
//die;

  
    // add alluserartistsforproject to result array
  
    

} //end of if error == no

/* output in necessary format */
if($format == 'json') {
    header('Content-type: application/json');
    echo json_encode($result);
    //echo json_encode(array('artistprofile'=>$result));
    //echo array_to_json($result);
}
else {
    if ($format == 'xml'){
        header('Content-type: text/xml');
        echo '<result>';
        foreach($result as $index => $resultentry) {
            if(is_array($resultentry)) {
                foreach($resultentry as $key => $value) {
                    echo '<',$key,'>';
                    if(is_array($value)) {
                        foreach($value as $tag => $val) {
                            echo '<',$tag,'>',htmlentities($val),'</',$tag,'>';
                        }
                    }
                    echo '</',$key,'>';
                }
            }
        }
        echo '</result>';
    }
}

function array_to_json( $array ){

    if( !is_array( $array ) ){
        return false;
    }

    $associative = count( array_diff( array_keys($array), array_keys( array_keys( $array )) ));
    if( $associative ){

        $construct = array();
        foreach( $array as $key => $value ){

            // We first copy each key/value pair into a staging array,
            // formatting each key and value properly as we go.

            // Format the key:
            if( is_numeric($key) ){
                $key = "key_$key";
            }
            $key = "\"".addslashes($key)."\"";

            // Format the value:
            if( is_array( $value )){
                $value = array_to_json( $value );
            } else if( !is_numeric( $value ) || is_string( $value ) ){
                $value = "\"".addslashes($value)."\"";
            }

            // Add to staging array:
            $construct[] = "$key: $value";
        }

        // Then we collapse the staging array into the JSON form:
        $result = "{ " . implode( ", ", $construct ) . " }";

    } else { // If the array is a vector (not associative):

        $construct = array();
        foreach( $array as $value ){

            // Format the value:
            if( is_array( $value )){
                $value = array_to_json( $value );
            } else if( !is_numeric( $value ) || is_string( $value ) ){
                $value = "'".addslashes($value)."'";
            }

            // Add to staging array:
            $construct[] = $value;
        }

        // Then we collapse the staging array into the JSON form:
        $result = "[ " . implode( ", ", $construct ) . " ]";
    }
	
    return $result;
}
function cleanstring($stringin)
{
   $sanitized = strtolower(preg_replace('/[^a-zA-Z0-9-_\.]/','', $stringin)); 
    return ($sanitized);
}
function setGalleryImage($galleryimage,$gallerygallerytitle,$gallerytitle,$register_cover_pic,$register_No_pic)
{
    $gallery_image_name_cart="";    
    if($gallerygallerytitle !="" || $gallerytitle!="")
    {
        if($galleryimage !="" && $galleryimage !=null )
		{
			$gallery_image_name_cart = $galleryimage;
		}
		else{
			$gallery_image_name_cart = "https://medgalthumb.s3.amazonaws.com/Noimage.png";
		}
	}
	else
	{
		if($register_cover_pic=="")
		{
			$gallery_image_name_cart = "https://reggalthumb.s3.amazonaws.com/".$register_No_pic;
		}
		else
		{
			$gallery_image_name_cart ="https://reggalthumb.s3.amazonaws.com/".$register_cover_pic;
		}
	}
    return($gallery_image_name_cart);
}

?>