<?php
session_start();
error_reporting(0);
$file = date('dmY-His');
header("Content-type: text/csv");
header("Content-Disposition: attachment; filename=sales_log.csv");
header("Pragma: no-cache");
header("Expires: 0");

include_once("commons/db.php");
include_once('classes/Get_Transactions.php');

$trans = new Get_Transactions();

$all_stores = array();
$meds_buy = array();
$fans_buy = array();
$donate_field = array();
$data = array();

$whole_profit = 0;
$all_sales = $trans->get_all_trans_for_admin();

//print_r($all_sales);

if($all_sales!="" && $all_sales!=NULL)
{
	if(mysql_num_rows($all_sales)>0)
	{
		$no = 1;
		$meds_don = 0;
		while($rows = mysql_fetch_assoc($all_sales))
		{
			$total_profit = 0;
			$total_profit_do = 0;
			$exp_fee = explode(',',$rows['paypal_fee']);
			
			if($rows['total']!=0)
			{
				if($rows['price']!=0)
				{
					$purify_fee = round(($rows['price'] - $exp_fee[0]) * 0.05, 2);
					
					$total_profit = $rows['price'] - $exp_fee[0] - $purify_fee;
				}
				$buy = $trans->get_buyer($rows['buyer_id']);
				
				$get_media = $trans->get_media($rows['media_id']);
				
				if(mysql_num_rows($get_media)>0)
				{
					if($rows['price']!=0)
					{
						$meds_buy[$meds_don] = $rows;
					}
					if($rows['donate']!=0)
					{
						$purify_fee = round(($rows['donate'] - $exp_fee[1]) * 0.05, 2);
						$total_profit_do = $rows['donate'] - $exp_fee[1] - $purify_fee;
						
						$donate_field[$meds_don] = $rows;
						$donate_field[$meds_don]['donate_field'] = 'yes';
					}
					else
					{
						$total_profit_do = 0;
					}
					
					$whole_profit = $whole_profit + $total_profit + $total_profit_do;
					$meds_don = $meds_don + 1;
				}
			}
		}
	}
}

$all_fan_sales = $trans->get_all_fan_trans_for_admin();
if($all_fan_sales!="" && $all_fan_sales!=NULL)
{
	if(mysql_num_rows($all_fan_sales)>0)
	{
		$no = 1;
		$fan_don = $meds_don;
		while($rows = mysql_fetch_assoc($all_fan_sales))
		{
			$total_profit_do = 0;
			$exp_fee = explode(',',$rows['paypal_fee']);
			
			$purify_fee = round(($rows['price'] - $exp_fee[0]) * 0.05, 2);
			$total_profit = $rows['price'] - $exp_fee[0] - $purify_fee;
		
			$fans_buy[$fan_don] = $rows;
			if($rows['donate']!=0)
			{
				$purify_fee = round(($rows['donate'] - $exp_fee[1]) * 0.05, 2);
				$total_profit_do = $rows['donate'] - $exp_fee[1] - $purify_fee;
					
				$donate_field[$fan_don] = $rows;
				$donate_field[$fan_don]['donate_field'] = 'yes';
			}
			else
			{
				$total_profit_do = 0;
			}
			
			$whole_profit = $whole_profit + $total_profit + $total_profit_do;
			$fan_don = $fan_don + 1;
		}
	}
}

if($whole_profit!=0)
{
	$sql_got = $trans->get_bal();
	if($sql_got!="" && $sql_got!=NULL)
	{
		if(mysql_num_rows($sql_got)>0)
		{
			$amt_got = 0;
			while($rgot = mysql_fetch_assoc($sql_got))
			{
				$amt_got = $amt_got + $rgot['amount'];
			}
			
			$org_pro = round($amt_got - $whole_profit, 2);
			if($org_pro<0)
			{
				$org_pro = $org_pro * (-1);
			}
		}
		else
		{
			$org_pro = $whole_profit;
		}
	}
	else
	{
		$org_pro = $whole_profit;
	}

}
else
{
	$org_pro = 0;
}

$total_rece = 0;
$no_chks_fees = 0;
$sql_re_bal = $trans->get_received_bal();
if(mysql_num_rows($sql_re_bal)>0)
{
	while($row_bal = mysql_fetch_assoc($sql_re_bal))
	{
		$total_rece = $total_rece + $row_bal['amount'] - 10;
	}
}

$sql_chk_bal = $trans->get_bal();
if(mysql_num_rows($sql_chk_bal)>0)
{
	while($row_bal = mysql_fetch_assoc($sql_chk_bal))
	{
		$no_chks_fees = $no_chks_fees + 1;
	}
}
$no_chks_fees = $no_chks_fees * 10;

$all_stores = array_merge($meds_buy,$fans_buy,$donate_field);

$sort = array();
foreach($all_stores as $k=>$v)
{
	if(isset($v['chk_date']) && $v['chk_date']!="")
	{
		$sort['chk_date'][$k] = strtotime($v['chk_date'].' '.$v['time_chk']);
	}
	elseif(isset($v['date']) && $v['date']!="")
	{
		$sort['chk_date'][$k] = strtotime($v['date'].' '.$v['time_chk']);
	}
	elseif(isset($v['purchase_date']) && $v['purchase_date']!="")
	{
		
		$sort['chk_date'][$k] = strtotime($v['purchase_date']);
	}							
}

if(!empty($sort))
{
	array_multisort($sort['chk_date'], SORT_DESC,$all_stores);
}
$hasing = count($all_stores);

$sort = array();
foreach($all_stores as $k=>$v)
{
	$sort['chk_date'][$k] = strtotime($v['chk_date'].' '.$v['time_chk']);
}

if($sort!="" && !empty($sort))
{
	array_multisort($sort['chk_date'], SORT_DESC,$all_stores);
}

$all_sales = $trans->get_all_trans_for_admin();
$no = 1;
$all_crets = "";
$hasing = count($all_stores);

//$data[] = array('Transaction #', 'Type', 'Date', 'Title', 'Sale Amount', 'Paypal Fee', 'Purify Fee', 'Net Profit');
$data[] = array('Transaction #', 'Seller', 'Type', 'Date', 'Title', 'From', 'SKU', 'Sale Amount', 'Paypal Fee', 'Purify Fee', 'Net Profit');

for($se=0;$se<count($all_stores);$se++)
{
	if(isset($all_stores[$se]['id']))
	{
		$exp_fee = explode(',',$all_stores[$se]['paypal_fee']);
		
		$floatNum = $exp_fee[0];
		$length = strlen($floatNum);

		$pos = strpos($floatNum, "."); // zero-based counting.

		$num_of_dec_places = ($length - $pos) - 1;
		if($num_of_dec_places==1)
		{
			$exp_fee[0] = $exp_fee[0].'0';
		}
		
		$purify_fee = round(($all_stores[$se]['price'] - $exp_fee[0]) * 0.05, 2);
		$purify_fee_2 = round(($all_stores[$se]['donate'] - $exp_fee[1]) * 0.05, 2);
		
		if(isset($all_stores[$se]['donate_field']))
		{
			$total_profit = $all_stores[$se]['donate'] - $purify_fee_2 - $exp_fee[1];
		}
		else
		{
			$total_profit = $all_stores[$se]['price'] - $exp_fee[0] - $purify_fee;
		}
		
		$float_to = $total_profit;
		$length = strlen($float_to);

		$pos = strpos($float_to, "."); // zero-based counting.

		$num_of_dec_places = ($length - $pos) - 1;
		if($num_of_dec_places==1)
		{
			$total_profit = $total_profit.'0';
		}
		elseif($pos==0)
		{
			$total_profit = $total_profit.'.00';
		}
		
		$buy = $trans->get_buyer($all_stores[$se]['buyer_id']);
		
		if(isset($all_stores[$se]['media_id']))
		{
			$get_media = $trans->get_media($all_stores[$se]['media_id']);
			if(mysql_num_rows($get_media)>0)
			{
				$gets_meds = mysql_fetch_assoc($get_media);
				$get_creator = $trans->get_creators($gets_meds['creator_info']);
			}
		}
		else
		{
			$gets_meds = "";
			if($all_stores[$se]['table_name']=='artist_project' || $all_stores[$se]['table_name']=='community_project')
			{
				
				$vars = $all_stores[$se]['table_name'].'|'.$all_stores[$se]['type_id'];
				
				$get_vars = $trans->get_creators($vars);
				$get_creator = $trans->get_creators($get_vars['creators_info']);
			}
			elseif($all_stores[$se]['table_name']=='general_artist' || $all_stores[$se]['table_name']=='general_community')
			{
				$vars = $all_stores[$se]['table_name'].'|'.$all_stores[$se]['type_id'];
				$get_creator = $trans->get_creators($vars);
			}
		}

		$dump_data_arr[$se][] = $hasing; 

		if($get_creator!="")
		{ 
			if(isset($get_creator['name']))
			{
				$dump_data_arr[$se][] = $get_creator['name'];
			}
			elseif(isset($get_creator['title']))
			{
				$dump_data_arr[$se][] = $get_creator['title'];
			}
		}
		else{ $dump_data_arr[$se][] = ""; }
		
        if($gets_meds!="") {
		    if($gets_meds['media_type']==114)
		    {
			    if(isset($all_stores[$se]['donate_field']))
			    {
				    $dump_data_arr[$se][] = "Tip";
			    }
			    else
			    {
				    $dump_data_arr[$se][] = "Song";
			    }
		        }elseif($gets_meds['media_type']==115)
		        {
			        if(isset($all_stores[$se]['donate_field']))
			        {
				        $dump_data_arr[$se][] = "Tip";
			        }
			        else
			        {
				        $dump_data_arr[$se][] = "Video";
			        }
		        }
		        elseif($gets_meds['media_type']==113)
		        {
			        if(isset($all_stores[$se]['donate_field']))
			        {
				        $dump_data_arr[$se][] = "Tip";
			        }
			        else
			        {
				        $dump_data_arr[$se][] = "Gallery";
			        }
		        }
		    }else{
			    if(isset($all_stores[$se]['donate_field']))
			    {
				    $dump_data_arr[$se][] = "Tip";
			    }
			    else
			    {
				    if($all_stores[$se]['fan_member_id']==0)
				    {
					
					    $dump_data_arr[$se][] = "Project";
				    }
				    else
				    {
					    $dump_data_arr[$se][] = "Membership";
				    }
			    }
		    }

		$dump_data_arr[$se][] = date("m.d.y", strtotime($all_stores[$se]['chk_date']));
		
        
        if($gets_meds!="") {
		    if(isset($gets_meds) && $gets_meds['title']!="")
		    { 
			    if($gets_meds['media_type']==114)
			    {
				    $get_trck = $trans->get_track($gets_meds['id']);
				    if($get_trck!="" && $get_trck['track']!="")
				    {
					    $dump_data_arr[$se][] = $get_trck['track'].' '.$gets_meds['title'];
				    }
				    else
				    {
					    $dump_data_arr[$se][] = $gets_meds['title'];
				    }
			    }
			    else
			    {
				    $dump_data_arr[$se][] = $gets_meds['title'];
			    }
		    }
		}else{
			if($all_stores[$se]['table_name']=='artist_project' || $all_stores[$se]['table_name']=='community_project')
			{
				if($all_stores[$se]['fan_member_id']==0)
				{
					$dump_data_arr[$se][] = $get_vars['title'];
				}
				else
				{
					$dump_data_arr[$se][] = $get_vars['title'].' Fan Club';
				}
			}
			else
			{
				if(isset($get_creator['name']))
				{
					$dump_data_arr[$se][] = $get_creator['name'].' Fan Club';
				}
				elseif(isset($get_creator['title']))
				{
					$dump_data_arr[$se][] = $get_creator['title'];
				} 
			} 
		}
		
        if($gets_meds!="") {
		    if(isset($gets_meds) && $gets_meds['title']!="" && $gets_meds['from_info']!="")
		    { 
			    $get_from = $trans->get_from($gets_meds['from_info']);
			    $dump_data_arr[$se][] = $get_from['title'];
		    }
		}else
		{
			$dump_data_arr[$se][] = "";
		}
		
		/* if(isset($all_stores[$se]['IP_address']))
		{
			if($all_stores[$se]['IP_address']!=NULL)
			{
				$dump_data_arr[$se][] = $all_stores[$se]['buy_name'];
			}
			else
			{
				if($buy['fname']=="" && $buy['lname']=="")
				{
					$dump_data_arr[$se][] = "N/A";
				}
				else
				{
					$dump_data_arr[$se][] = $buy['fname'].' '.$buy['lname'];
				}
			}
		}
		else
		{
			if($buy['fname']=="" && $buy['lname']=="")
			{
				$dump_data_arr[$se][] = "N/A";
			}
			else
			{
				$dump_data_arr[$se][] = $buy['fname'].' '.$buy['lname'];
			}
		} 
	
		if(isset($all_stores[$se]['IP_address']))
		{
			if($all_stores[$se]['IP_address']!=NULL)
			{
				$dump_data_arr[$se][] = $all_stores[$se]['buy_email'];
			}
			else
			{
				if($buy['email']=="")
				{
					$dump_data_arr[$se][] = "N/A";
				}
				else
				{
					$dump_data_arr[$se][] = $buy['email'];
				}
			}
		}
		else
		{
			if($buy['email']=="")
			{
				$dump_data_arr[$se][] = "N/A";
			}
			else
			{
				$dump_data_arr[$se][] = $buy['email'];
			}				
		}  */
		
		if(isset($all_stores[$se]['sku']))
		{
			if($gets_meds!="") {
                if(isset($gets_meds) && $gets_meds['title']!="")
			    { 
				    if($gets_meds['media_type']==114)
				    {
					    $sql_meds_isrc = mysql_query("SELECT * FROM general_media WHERE id='".$all_stores[$se]['media_id']."'");
					    $ans_meds_isrc = mysql_fetch_assoc($sql_meds_isrc);
					
					    $dump_data_arr[$se][] = $ans_meds_isrc['sku'];
					
					    /* if($gets_meds['from_info']!="")
					    {
						    $get_from = $trans->get_from($gets_meds['from_info']);
						    //$dump_data_arr[$se][] = $get_from['title'];
						    if(isset($get_from['upc']))
						    {
							    if($get_from['upc']!="")
							    {
								    $dump_data_arr[$se][] = $get_from['upc'].'-'.$ans_meds_isrc['isrc'];
							    }
							    else
							    {
								    $dump_data_arr[$se][] = $ans_meds_isrc['isrc'];
							    }
						    }
						    else
						    {
							    $dump_data_arr[$se][] = $ans_meds_isrc['isrc'];
						    }
					    }
					    else
					    {
						    $dump_data_arr[$se][] = $ans_meds_isrc['isrc'];
					    } */
				    }
			    }
			}elseif($all_stores[$se]['table_name']=='artist_project')
			{
				$sql_pros_upc = mysql_query("SELECT * FROM artist_project WHERE id='".$get_vars['id']."'");
				$ans_pros_upc = mysql_fetch_assoc($sql_pros_upc);

				$dump_data_arr[$se][] = $ans_pros_upc['upc'];
				
			}
			else
			{
				$dump_data_arr[$se][] = $all_stores[$se]['sku'];
			}
		}
		else{ $dump_data_arr[$se][] = ""; }
		
		if(isset($all_stores[$se]['donate_field']))
		{
			$dump_data_arr[$se][] = '$'.$all_stores[$se]['donate'];
		}
		else{ $dump_data_arr[$se][] = '$'.$all_stores[$se]['price']; }
		
		if(isset($all_stores[$se]['donate_field']))
		{
			$dump_data_arr[$se][] = '$'.$exp_fee[1];
		}
		else{ $dump_data_arr[$se][] = '$'.$exp_fee[0]; } 
		
		if(isset($all_stores[$se]['donate_field']))
		{
			$dump_data_arr[$se][] = '$'.$purify_fee_2;
		}
		else{ $dump_data_arr[$se][] = '$'.$purify_fee; }
		
		$dump_data_arr[$se][] = '$'.$total_profit;
	
		$no = $no + 1;
		$data[] = array_values($dump_data_arr[$se]);
	}
	$hasing = $hasing - 1;
}

//print_r($data);
$output = fopen("php://output", "w");
foreach ($data as $val) {
    fputcsv($output, $val);    
}
fclose($output);
?>
