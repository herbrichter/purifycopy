<?php
session_start();
include_once("commons/db.php");
include_once('classes/Commontabs.php');
include("classes/ViewCommunityUserProfile.php");
include("classes/ViewArtistUserProfile.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Purify Entertainment: About</title>
<!--<link rel="stylesheet" type="text/css" media="screen" href="includes/purify.css" />
<script src="includes/jquery.js" type="text/javascript"></script>
<script src="js/jquery.min.js"></script>
<script src="includes/functionsmedia.js" type="text/javascript"></script>
<script src="includes/functions.js" type="text/javascript"></script>
<link href="galleryfiles/gallery.css" rel="stylesheet"/>
<script src="galleryfiles/jquery.easing.1.3.js"></script>
<script src="includes/mediaelement-and-player.min.js"></script>
<link rel="stylesheet" href="includes/mediaelementplayer.min.css" />
<!--<script src="javascript/jquery-1.2.6.js" type="text/javascript"></script>
<script src="http://jqueryjs.googlecode.com/files/jquery-1.2.6.min.js" type="text/javascript"></script>
<link rel="stylesheet" href="lightbox/prettyPhoto.css" type="text/css" media="screen" title="prettyPhoto main stylesheet" charset="utf-8" />
<script src="lightbox/jquery.prettyPhoto.js" type="text/javascript" charset="utf-8"></script>
<!--<script src="includes/popup.js" type="text/javascript"></script>
<script src="javascripts/popup.js" type="text/javascript" charset="utf-8"></script>-->


<link rel="stylesheet" type="text/css" media="screen" href="includes/purify.css" />
<script src="js/jquery.min.js"></script>
<script src="includes/functionsmedia.js" type="text/javascript"></script>
<script src="includes/functions.js" type="text/javascript"></script>
<link href="galleryfiles/gallery.css" rel="stylesheet"/>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4/jquery.min.js"></script>
<script src="galleryfiles/jquery.easing.1.3.js"></script>
<link rel="stylesheet" href="lightbox/prettyPhoto.css" type="text/css" media="screen" title="prettyPhoto main stylesheet" charset="utf-8" />
<script src="lightbox/jquery.prettyPhoto.js" type="text/javascript" charset="utf-8"></script>
</head>
<?php
$newtab=new Commontabs();
$newres1=$newtab->tabs();
//$newres1=mysql_fetch_array($result);

$new_profile_class_obj=new ViewCommunityUserProfile();
$new_profile_class_obj_artist=new ViewArtistUserProfile();

$getgeneral =  $new_profile_class_obj->get_user_info();
$getuser=$new_profile_class_obj->get_user_community_profile();
$getcountry=$new_profile_class_obj->get_user_country();
$getstate=$new_profile_class_obj->get_user_state();
$gettype=$new_profile_class_obj->get_community_user_type();
$get_subtype=$new_profile_class_obj->get_community_user_subtype();
$get_metatype=$new_profile_class_obj->get_artist_user_metatype();



$get_community_Info=$new_profile_class_obj->get_community_Info();

$get_video=explode(',',$get_community_Info['taggedvideos']);
$get_song=explode(',',$get_community_Info['taggedsongs']);
$get_gal=explode(',',$get_community_Info['taggedgalleries']);

$get_rec_event=explode(',',$getuser['taggedrecordedevents']);
$get_upco_event=explode(',',$getuser['taggedupcomingevents']);
$get_pro=explode(',',$getuser['taggedprojects']);


if($get_community_Info['featured_media']=='Video' && $get_community_Info['media_id']!="")
{
	$new_array_video = array();
	$get_video[].=$get_community_Info['media_id'];

	foreach ($get_video as $key1 => $value1) 
	{
		if(isset($new_array_video[$value1]))
		{
			$new_array_video[$value1] += 1;
		}
		else
			$new_array_video[$value1] = 1;
	}
	foreach ($new_array_video as $uid => $n) 
	{
		$ex_uid1=$ex_uid1.','.$uid;
	}
	$get_video=explode(',',$ex_uid1);
}

if($get_community_Info['featured_media']=='Song' && $get_community_Info['media_id']!="")
{
	$new_array_song = array();
	$get_song[].=$get_community_Info['media_id'];

	foreach ($get_song as $key2 => $value2) 
	{
		if(isset($new_array_song[$value2]))
		{
			$new_array_song[$value2] += 1;
		}
		else
			$new_array_song[$value2] = 1;
	}
	foreach ($new_array_song as $uid1 => $n) 
	{
		$ex_uid2=$ex_uid2.','.$uid1;
	}
	$get_song=explode(',',$ex_uid2);
}

if($get_community_Info['featured_media']=='Gallery' && $get_community_Info['media_id']!="")
{
	$new_array_gal = array();
	$get_gal[].=$get_community_Info['media_id'];

	foreach ($get_gal as $key3 => $value3) 
	{
		if(isset($new_array_gal[$value3]))
		{
			$new_array_gal[$value3] += 1;
		}
		else
			$new_array_gal[$value3] = 1;
	}
	foreach ($new_array_gal as $uid2 => $n) 
	{
		$ex_uid3=$ex_uid3.','.$uid2;
	}
	$get_gal=explode(',',$ex_uid3);
}

/*var_dump($get_gal);
var_dump($get_video);
var_dump($get_song);

echo count($get_gal);
echo count($get_video);
echo count($get_song);*/
$date=date('y-m-d');
//echo $date;


?>

<script type="text/javascript">
$("document").ready(function(){
var imagename = "<?php echo $newres1['image_name']; ?>";
if(imagename=="")
{
	imagename="Noimage.png";
	$('#loggedin').css({
	backgroundImage : 'url(/uploads/profile_pic/'+ imagename +')',
	backgroundSize :'50px',
	backgroundRepeat: 'no-repeat',
	backgroundPosition: 'right top'
	});
}
else
{
	$('#loggedin').css({
	backgroundImage : 'url(general_jcrop/croppedFiles/thumb/'+ imagename +')',
//backgroundSize :'50px'
	backgroundRepeat: 'no-repeat',
	backgroundPosition: 'right top'
	});
}

});
</script>
<body onload="startTab();">
<div id="outerContainer">
<?php 
$newres1=$getgeneral['fname'];
$newres2=$getgeneral['lname'];
include("header.php");
?>
<!--        <p><a href="profileedit_artist.php">Edit Profile</a><br />
        <a href="../logout.php">Logout</a></p>
      </div>
    </div>
  </div>
 </div>-->
  <div id="contentContainer">
    <!-- PROFILE START -->
    <div id="profileHeader">
      <div id="profilePhoto"><img src="http://comjcropprofile.s3.amazonaws.com/<?php echo $getuser['image_name'];?>" width="450" height="450" /></div>
      <div id="profileInfo">
        <h2><?php if($gettype['name']!="") { echo $gettype['name'];?> , <?php echo $get_subtype['name']; }?></h2>
        <div id="profileFeatured">
			<div class="fmedia">
				featured Media
			</div>
			<a href=""><img src="images/profile/play.png" width="35" height="23" onmouseover="this.src='images/profile/play-over.png'" onmouseout="this.src='images/profile/play.png'" /></a>
			<a href=""><img src="images/profile/add.png" width="27" height="23" onmouseover="this.src='images/profile/add-over.png'" onmouseout="this.src='images/profile/add.png'" /></a>
		</div>
        
        <h1><?php echo $getuser['name'];?></h1>
        <h3>Where: <?php echo $getuser['city'];?> , <?php echo $getstate['state_name'];?> , <?php echo $getcountry['country_name'];?><br/>
			<?php if(isset($get_metatype) && $get_metatype!=null) 
			{
			?>
		   Type: 
		   <?php
				if(isset($get_metatype[0]['name'])) echo $get_metatype[0]['name']; ?>
				<?php if(isset($get_metatype[1]['name'])) echo ', '. $get_metatype[1]['name']; ?>
				<?php if(isset($get_metatype[2]['name'])) echo ', '. $get_metatype[2]['name'];
			?> <br/>
			<?php
			} ?>
		    <!--Email: --><?php //echo $_SESSION['login_email'];?></h3>
        <?php //echo $getuser['bio'];?>
		<p><?php       
	   $str_count = substr_count($getuser['bio'],'<p>');
		//echo substr_count($getuser['bio'],'<p>');
		$data_len = strlen($getuser['bio']);
		//echo strlen($getuser['bio']);
		//echo $data_len/75;
		//echo round($data_len/75);
		$st_pos=0;
		$en_pos=75;
		$get_data_len=round($data_len/75);
		if($str_count==1)
		{
			for($index=1;$index<=$get_data_len;$index++)
			{
				echo substr($getuser['bio'],$st_pos,$en_pos)."</br>";
				$st_pos=$st_pos + $en_pos;
			}
		}
		else
		{
			echo $getuser['bio'];
		}
?></p>
      </div>
      <div id="profileStatsBlock">
        <div id="profileStats">
			<p>Subscribers: # of emails subscribed</p>
			<p>Fans: # of members</p>
			<p>Homepage</p>
        </div>
        <div id="profileButtons"><a href="#">Subscribe</a> <a href="#">Fan Club</a> <a href="#">Message</a></div>
      </div>
    </div>
    <div id="profileTabs">
      <ul>
	  <?php
	  if($get_community_Info['taggedvideos']!="" || ($get_community_Info['featured_media']=='Video' && $get_community_Info['media_id']!=""))
	  {
	  ?>
	    <li><a href="javascript:tabOne();" class="profileTab1" id="tabOne">Videos</a></li>
	  <?php
	  }
	  if($get_community_Info['taggedsongs']!="" || ($get_community_Info['featured_media']=='Song' && $get_community_Info['media_id']!=""))
	  {
	  ?>
	  <li><a href="javascript:tabTwo();" class="profileTab2" id="tabTwo">Songs</a></li>
	  <?php
	  }
	  if($get_community_Info['taggedgalleries']!="" || ($get_community_Info['featured_media']=='Gallery' && $get_community_Info['media_id']!=""))
	  {
	  ?>
	  <li><a href="javascript:tabThree();" class="profileTab3" id="tabThree">Galleries</a></li>
	  <?php
	  }
	  if($getuser['taggedprojects'] !="")
	  {
	  ?>
	  <li><a href="javascript:tabFour();" class="profileTab4" id="tabFour">Projects</a></li>
	  <?php
	  }
	//echo mysql_num_rows($get_community_upcoming_event);
	//echo mysql_num_rows($get_community_recorded_event);
	  if($getuser['taggedrecordedevents'] !="" || $getuser['taggedupcomingevents'] !="")
	  {
	  ?>
	  <li><a href="javascript:tabFive();" class="profileTab5" id="tabFive">Events</a></li>
	  <?php
	  }
	  if($getuser['artist_view_selected'] !="" || $getuser['community_view_selected'] !="")
	  {
	  ?>
	  <li><a href="javascript:tabSix();" class="profileTab6" id="tabSix">Friends</a></li>
	  <?php
	  }
	  ?>
  
      </ul>
    </div>
<?php
	if($get_community_Info['taggedvideos']!="" || ($get_community_Info['featured_media']!="" && $get_community_Info['featured_media']=="Video"))
	{
	?>
	<div id="mediaTab" class="hiddenBlock1">
      <div id="profileFeatured">
		<div class="playall">
			Play All
		</div>
		<a href=""><img src="images/profile/play.png" width="35" height="23" onmouseover="this.src='images/profile/play-over.png'" onmouseout="this.src='images/profile/play.png'" /></a>
		<a href=""><img src="images/profile/add.png" width="27" height="23" onmouseover="this.src='images/profile/add-over.png'" onmouseout="this.src='images/profile/add.png'" /></a>
	</div>
      <h2><a href="#">Play All Videos</a></h2>
      <div class="rowItem">
	  <?php
		for($i=0;$i<count($get_video);$i++)
		{
			$get_community_Video=$new_profile_class_obj->get_community_Video($get_video[$i]);
			$get_community_Video_title=$new_profile_class_obj->get_community_Video_title($get_video[$i]);
			
			
			$src_list ="http://comprothumjcrop.s3.amazonaws.com/";
			$from_list_pic ="";
			$creator_list_pic ="";
			if($get_community_Video_title['from_info']!="")
			{
				$expl_from_in =explode("|",$get_community_Video_title['from_info']);
				$get_from_list_pic = $new_profile_class_obj->get_from_list_pic($expl_from_in[0],$expl_from_in[1]);
				if($get_from_list_pic['listing_image_name'] =="")
				{
					$from_list_pic="http://artjcropthumb.s3.amazonaws.com/Noimage.png";
				}
				else
				{
					$from_list_pic = $get_from_list_pic['listing_image_name'];
				}
			}
			if($get_community_Video_title['from_info'] =="" && $get_community_Video_title['creator_info']!="")
			{
				$expl_creator_in =explode("|",$get_community_Video_title['creator_info']);
				if($expl_creator_in[0] == "general_community")
				{
					$col_name = "community_id";
					$creator_src = "http://comjcropthumb.s3.amazonaws.com/";
				}
				else if($expl_creator_in[0] == "general_artist")
				{
					$col_name = "artist_id";
					$creator_src = "http://artjcropthumb.s3.amazonaws.com/";
				}
				else
				{
					$col_name = "id";
					$creator_src = "";
				}
				$get_creator_list_pic = $new_profile_class_obj->get_creator_list_pic($expl_creator_in[0],$expl_creator_in[1],$col_name);
				if($get_creator_list_pic['listing_image_name'] =="")
				{
					$creator_list_pic = $creator_src."Noimage.png";
				}
				else
				{
					$creator_list_pic = $creator_src.$get_creator_list_pic['listing_image_name'];
				}
			}
	if($get_community_Video_title['title'] !="")
		{
		  ?>
			<div class="tabItem"> <img src="<?php 
			if(isset($from_list_pic) && $from_list_pic!="")
			{
				echo $from_list_pic;
			}
			else if(isset($creator_list_pic) && $creator_list_pic!="")
			{
				echo $creator_list_pic;
			}
			else
			{
			echo $src_list."Noimage.png";}?>" width="200" height="175" />
			  <h3><?php echo substr($get_community_Video_title['title'],'0','29'); ?></h3>
			  <!--<div class="player" id="button">
				<img src="images/profile/play.gif" />
				<img src="images/profile/add.gif" />
				<img src="images/profile/download-dis.gif" />
			  </div>-->
			  <div class="player" id="button">
				<a href=""><img src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" /></a>
				<a href=""><img src="images/profile/add.gif"  onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'"/></a>
				<a href=""><img src="images/profile/download.gif" onmouseover="this.src='images/profile/download-over.gif'" onmouseout="this.src='images/profile/download.gif'" /></a>
			  </div>
			</div>
			<?php
		}
		}
		?>
      </div>      
    </div>    
    <?php
	}
	else
	{
	?>
	<div id="mediaTab" class="hiddenBlock1"></div>
	<?php
	}
	if($get_community_Info['taggedsongs']!="" || ($get_community_Info['featured_media']!="" && $get_community_Info['featured_media']=="Song"))
	{
	?>
    <div id="eventsTab" class="hiddenBlock2">
		<div id="profileFeatured">
			<div class="playall">
				Play All
			</div>
			<a href=""><img src="images/profile/play.png" width="35" height="23" onmouseover="this.src='images/profile/play-over.png'" onmouseout="this.src='images/profile/play.png'" /></a>
			<a href=""><img src="images/profile/add.png" width="27" height="23" onmouseover="this.src='images/profile/add-over.png'" onmouseout="this.src='images/profile/add.png'" /></a>
		</div>
      <h2>Play All Songs</h2>
      <div class="rowItem">
	   <?php
		for($i=0;$i<count($get_song);$i++)
		{
			if($get_song[$i]=="")
			{
				continue;
			}
			$get_community_Song=$new_profile_class_obj->get_community_Song($get_song[$i]);
			$get_community_Song_title=$new_profile_class_obj->get_community_Song_title($get_song[$i]);
			$get_community_Song_regis=$new_profile_class_obj->get_community_Song_regis($get_song[$i]);
			
			
			$src_list ="http://comprothumjcrop.s3.amazonaws.com/";
			$from_list_pic_song ="";
			$creator_list_pic_song ="";
			if($get_community_Song_title['from_info']!="")
			{
				$expl_from_in =explode("|",$get_community_Song_title['from_info']);
				$get_from_list_pic = $new_profile_class_obj->get_from_list_pic($expl_from_in[0],$expl_from_in[1]);
				if($get_from_list_pic['listing_image_name'] =="")
				{
					$from_list_pic_song="http://artjcropthumb.s3.amazonaws.com/Noimage.png";
				}
				else
				{
					$from_list_pic_song = $get_from_list_pic['listing_image_name'];
				}
			}
			if($get_community_Song_title['from_info'] =="" && $get_community_Song_title['creator_info']!="")
			{
				$expl_creator_in =explode("|",$get_community_Song_title['creator_info']);
				if($expl_creator_in[0] == "general_community")
				{
					$col_name = "community_id";
					$creator_src = "http://comjcropthumb.s3.amazonaws.com/";
				}
				else if($expl_creator_in[0] == "general_artist")
				{
					$col_name = "artist_id";
					$creator_src = "http://artjcropthumb.s3.amazonaws.com/";
				}
				else
				{
					$col_name = "id";
					$creator_src = "";
				}
				$get_creator_list_pic = $new_profile_class_obj->get_creator_list_pic($expl_creator_in[0],$expl_creator_in[1],$col_name);
				if($get_creator_list_pic['listing_image_name'] =="")
				{
					$creator_list_pic_song = $creator_src."Noimage.png";
				}
				else
				{
					$creator_list_pic_song = $creator_src.$get_creator_list_pic['listing_image_name'];
				}
			}
			/*$Get_gal_img_for_song1_artist=$new_profile_class_obj_artist->get_artist_Gallery_at_register_for_song($get_community_Song['gallery_id']);
			if(mysql_num_rows($Get_gal_img_for_song1_artist)>0)
			{
				$Get_gal_img_for_song=mysql_fetch_assoc($Get_gal_img_for_song1_artist);
				$starting_path="http://reggalthumb.s3.amazonaws.com/";
			}
			$Get_gal_img_for_song1_comm=$new_profile_class_obj->get_community_Gallery_at_register_for_song($get_community_Song['gallery_id']);
			if(mysql_num_rows($Get_gal_img_for_song1_comm)>0)
			{			
				$Get_gal_img_for_song=mysql_fetch_assoc($Get_gal_img_for_song1_comm);
				$starting_path="http://reggalthumb.s3.amazonaws.com/";
			}
			
			$Get_gal_img_for_song1_media=$new_profile_class_obj->get_media_Gallery($get_community_Song['gallery_id']);
			if(mysql_num_rows($Get_gal_img_for_song1_media)>0)
			{			
				$Get_gal_img_for_song=mysql_fetch_assoc($Get_gal_img_for_song1_media);
				$starting_path="http://medgalthumb.s3.amazonaws.com/";
			}*/
	if($get_community_Song_title['title'] !="" || $get_community_Song_regis['audio_name']!="")
	{
	  ?>
        <div class="tabItem"> <img src="<?php 
		if(isset($from_list_pic_song) && $from_list_pic_song!="")
		{
			echo $from_list_pic_song;
		}
		else if(isset($creator_list_pic_song) && $creator_list_pic_song!="")
		{
			echo $creator_list_pic_song;
		}
		else
		{
		echo $src_list."Noimage.png";}
		?>" width="200" height="175" />
          <h3><?php if($get_community_Song_title['title']==""){ echo substr($get_community_Song_regis['audio_name'],'0','29'); } else { echo substr($get_community_Song_title['title'],'0','29'); }?></h3>
          <div class="player" >
			<a href=""><img src="images/profile/play.gif"  onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" /></a>
			<a href=""><img src="images/profile/add.gif"  onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'"/></a>
			<a href=""><img src="images/profile/download.gif" onmouseover="this.src='images/profile/download-over.gif'" onmouseout="this.src='images/profile/download.gif'" /></a>
		  </div>
        </div>
	<?php
	}
		}
	?>
      </div>      
    </div>
    <?php
	}
	else
	{
	?>
	    <div id="eventsTab" class="hiddenBlock2"></div>
	<?php
	}
	if($get_community_Info['taggedgalleries']!="" || ($get_community_Info['featured_media']!="" && $get_community_Info['featured_media']=="Gallery"))
	{
	?>
     <div id="projectsTab" class="hiddenBlock3">
		<div id="profileFeatured">
			<div class="playall">
				Play All
			</div>
			<a href=""><img src="images/profile/play.png" width="35" height="23" onmouseover="this.src='images/profile/play-over.png'" onmouseout="this.src='images/profile/play.png'" /></a>
			<a href=""><img src="images/profile/add.png" width="27" height="23" onmouseover="this.src='images/profile/add-over.png'" onmouseout="this.src='images/profile/add.png'" /></a>
		</div>
      <h2>Play All Galleries</h2>
      <div class="rowItem">
	 <?php
	 $galleryImagesDataIndex = 0;
	 $galleryImagesData = "";
		for($g=0;$g<count($get_gal);$g++)
		{
			if($get_gal[$g]=="" || $get_gal[$g]==0)
			{
				continue;
			} 
			else
			{
				$Get_count_img_at_reg=$new_profile_class_obj->get_count_community_Gallery_at_register($get_gal[$g]);
				//var_dump($Get_count_img_at_reg);
				if($Get_count_img_at_reg['count(*)']>0)
				{
					
					$Get_img_at_reg=$new_profile_class_obj->get_community_Gallery_at_register($get_gal[$g]);
					$play_array = array();
					$Get_com_media_cover_pic="";
						if(mysql_num_rows($Get_img_at_reg)>0)
						{
							while($row = mysql_fetch_assoc($Get_img_at_reg))
							{
								$play_array[] = $row;
							}
						
							${'orgPath'.$g}="http://reggallery.s3.amazonaws.com/";
							${'thumbPath'.$g}="http://reggalthumb.s3.amazonaws.com/";
							$get_community_gallery=$new_profile_class_obj->get_community_Gallery_title_at_register($play_array[0]['gallery_id']);
							//var_dump($get_community_gallery);
							for($l=0;$l<count($play_array);$l++)
							{
								$mediaSrc = $play_array[0]['image_name'];
								$register_cover_pic=$play_array[0]['cover_pic'];
								$register_No_pic=$play_array[0]['image_name'];
								$filePath = "http://reggallery.s3.amazonaws.com/".$mediaSrc;
								${'galleryImagesData'.$g}[$galleryImagesDataIndex] = $play_array[$l]['image_name'];
								${'galleryImagestitle'.$g}[$galleryImagesDataIndex] = $play_array[$l]['image_title'];
								$galleryImagesDataIndex++;
							}
						}
				}
				else
				{
					$register_cover_pic="";
					$get_community_gallery=$new_profile_class_obj->get_community_gallery($get_gal[$g]);
					$Get_com_media_cover_pic=$new_profile_class_obj->get_media_cover_pic($get_community_gallery['id']);
				
						$sql_play1 = mysql_query("SELECT * FROM media_images WHERE media_id='".$get_gal[$g]."' AND gallery_id!=0");
						
						$play_array = array();
						if(mysql_num_rows($sql_play1)>0)
						{
							while($row = mysql_fetch_assoc($sql_play1))
							{
								$play_array[] = $row;
							}
							
				
							${'orgPath'.$g}="http://medgallery.s3.amazonaws.com/";
							${'thumbPath'.$g}="http://medgalthumb.s3.amazonaws.com/";
							for($l=0;$l<count($play_array);$l++)
							{
								$mediaSrc = $play_array[0]['image_name'];
								$filePath = "http://medgallery.s3.amazonaws.com/".$mediaSrc;
								
								
								${'galleryImagesData'.$g}[$galleryImagesDataIndex] = $play_array[$l]['image_name'];
								${'galleryImagestitle'.$g}[$galleryImagesDataIndex] = $play_array[$l]['image_title'];
								//echo ${'galleryImagesData'.$g}[$galleryImagesDataIndex];
								$galleryImagesDataIndex++;
	
							}
						}
						else
						{
							${'orgPath'.$g}="http://medsingleimage.s3.amazonaws.com/";
							${'thumbPath'.$g}="http://medgalthumb.s3.amazonaws.com/";
							
							$sql_play1 = mysql_query("SELECT * FROM media_images WHERE media_id='".$get_gal[$g]."' AND gallery_id=0");
							$play_array = array();
							if(mysql_num_rows($sql_play1)>0)
							{
								while($row = mysql_fetch_assoc($sql_play1))
								{
									$play_array[] = $row;
								}
								
								for($l=0;$l<count($play_array);$l++)
								{
									$mediaSrc = $play_array[0]['image_name'];
									$filePath = "http://medsingleimage.s3.amazonaws.com/".$mediaSrc;
									
									${'galleryImagesData'.$g}[$galleryImagesDataIndex] = $play_array[$l]['image_name'];
									${'galleryImagestitle'.$g}[$galleryImagesDataIndex] = $play_array[$l]['image_title'];
									//echo ${'galleryImagesData'.$g}[$galleryImagesDataIndex];
									$galleryImagesDataIndex++;
									//echo $galleryImagesDataIndex;
								}
							}
						}
					//}
			}
			
	  ?>
	  
		  <?php
		  if(isset(${'galleryImagesData'.$g}))
		  {
			  ${'galleryImagesData'.$g} = implode(",", ${'galleryImagesData'.$g});
			  ${'galleryImagestitle'.$g} = implode(",", ${'galleryImagestitle'.$g});
		  }
		
		if($get_community_gallery['gallery_title'] !="" || $get_community_gallery['title']!="")
		{
		  ?>
	 
        <div class="tabItem"><a href="javascript:void(0);">
<?php
//echo $play_array[0]['cover_pic'];
?>
		<img src="<?php if(isset($Get_com_media_cover_pic) && $Get_com_media_cover_pic!=""){
				if($Get_com_media_cover_pic['cover_pic']=="")
				{
					echo "http://medgalthumb.s3.amazonaws.com/".$Get_com_media_cover_pic['image_name'];
				}
				else
				{
					echo "http://medgalthumb.s3.amazonaws.com/".$Get_com_media_cover_pic['cover_pic'];
				}
			}
			else
			{
				if($register_cover_pic=="")
				{
					echo "http://reggalthumb.s3.amazonaws.com/".$register_No_pic;
				}
				else
				{
					echo "http://reggalthumb.s3.amazonaws.com/".$register_cover_pic;
				}
			}
			
		?>" width="200" height="175" onclick="checkload('<?php echo $mediaSrc;?>','<?php echo ${'galleryImagesData'.$g}; ?>','<?php echo ${'thumbPath'.$g}; ?>','<?php echo ${'orgPath'.$g}; ?>','<?php echo ${'galleryImagestitle'.$g}; ?>')" /></a> 
        
		<h3><?php if(isset($get_community_gallery['gallery_title'])) { echo substr($get_community_gallery['gallery_title'],'0','29'); } else { echo substr($get_community_gallery['title'],'0','29'); }?></h3>
		<div class="player">
			<a href="javascript:void(0);"><img src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onclick="checkload('<?php echo $mediaSrc;?>','<?php echo ${'galleryImagesData'.$g}; ?>','<?php echo ${'thumbPath'.$g}; ?>','<?php echo ${'orgPath'.$g}; ?>','<?php echo ${'galleryImagestitle'.$g}; ?>')" onmouseout="this.src='images/profile/play.gif'" /></a>
			<a href=""><img src="images/profile/add.gif"  onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'"/></a>
			<a href=""><img src="images/profile/download.gif" onmouseover="this.src='images/profile/download-over.gif'" onmouseout="this.src='images/profile/download.gif'" /></a>
		 </div> 
		  </div>
	<?php
	}
		}
	}
	?>
       
      </div>      
    </div>
    <?php
	}
	else
	{
	?>
	<div id="projectsTab" class="hiddenBlock3"></div>
	<?php
	}
	?>
    <div id="servicesTab" class="hiddenBlock4">
		<div id="profileFeatured">
			<div class="playall">
				Play All
			</div>
			<a href=""><img src="images/profile/play.png" width="35" height="23" onmouseover="this.src='images/profile/play-over.png'" onmouseout="this.src='images/profile/play.png'" /></a>
			<a href=""><img src="images/profile/add.png" width="27" height="23" onmouseover="this.src='images/profile/add-over.png'" onmouseout="this.src='images/profile/add.png'" /></a>
		</div>
      <h2>Projects</h2>
      <div class="rowItem">
	  <?php
		for($exp_pro=0;$exp_pro<=count($get_pro);$exp_pro++)
		{
			if($get_pro[$exp_pro]=="")
			{
				continue;
			}
			else
			{
				$get_community_project=$new_profile_class_obj->get_community_projects($get_pro[$exp_pro]);
				if(mysql_num_rows($get_community_project))
				{
					$row=mysql_fetch_assoc($get_community_project);
				   ?>
					<div class="tabItem">
					<a href="profile_community_project.php?id=<?php echo $row['id']; ?>"><img src="<?php if($row['image_name']==""){echo "community_project_jcrop/./croppedFiles/profile/Noimage.png";}else {echo $row['image_name'];}?>" width="200" height="175" />
					  <h3><?php echo substr($row['title'],'0','29'); ?></h3></a>
					  <div class="player">
						<a href=""><img src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" /></a>
						<a href=""><img src="images/profile/add.gif"  onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'"/></a>
						<a href=""><img src="images/profile/download.gif" onmouseover="this.src='images/profile/download-over.gif'" onmouseout="this.src='images/profile/download.gif'" /></a>
					  </div>
					</div>
				   <?php
				}
			}
		}
	?>
	 </div>
    </div>

    <div id="friendsTab" class="hiddenBlock5">
		<div id="profileFeatured">
			<div class="playall">
				Play All
			</div>
			<a href=""><img src="images/profile/play.png" width="35" height="23" onmouseover="this.src='images/profile/play-over.png'" onmouseout="this.src='images/profile/play.png'" /></a>
			<a href=""><img src="images/profile/add.png" width="27" height="23" onmouseover="this.src='images/profile/add-over.png'" onmouseout="this.src='images/profile/add.png'" /></a>
		</div>
		<?php
		$get_count_community_upcoming_event=$new_profile_class_obj->count_upcomingEvents();
		if($get_count_community_upcoming_event['taggedupcomingevents'] != "")
		{
		?>
			  <h2>Upcoming Events</h2>
			  <div class="rowItem">
				<?php
				for($exp_up=0;$exp_up<=count($get_upco_event);$exp_up++)
				{
					if($get_upco_event[$exp_up]=="")
					{
						continue;
					}
					else
					{
						$get_community_upcoming_event=$new_profile_class_obj->upcomingEvents($date,$get_upco_event[$exp_up]);
						if(mysql_num_rows($get_community_upcoming_event))
						{
							$get_upcoming_event=mysql_fetch_assoc($get_community_upcoming_event)
							?>
							<div class="tabItem"> <a href="add_community_event.php?id=<?php echo $get_upcoming_event['id']; ?>"><img src="<?php if($get_upcoming_event['image_name']==""){echo "community_event_jcrop/./croppedFiles/profile/Noimage.png";}else {echo $get_upcoming_event['image_name'];}?>" width="200" height="175" />
							  <h3><?php echo substr($get_upcoming_event['date'].' '.$get_upcoming_event['title'],'0','29'); ?></h3></a>
							  <div class="player">
								<a href=""><img src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" /></a>
								<a href=""><img src="images/profile/add.gif"  onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'"/></a>
								<a href=""><img src="images/profile/download.gif" onmouseover="this.src='images/profile/download-over.gif'" onmouseout="this.src='images/profile/download.gif'" /></a>
							  </div>
							</div>
						<?php
						}
					}	
				}
				?>
			  </div>
	  <?php
	  }
	  $get_count_community_recorded_event=$new_profile_class_obj->count_recordedEvents();
	  if($get_count_community_recorded_event['taggedrecordedevents'] != "")
	  {
	  ?>
      <h2>Recorded Events</h2>
      <div class="rowItem">
	  <?php
		for($exp_rec=0;$exp_rec<=count($get_rec_event);$exp_rec++)
		{
			if($get_rec_event[$exp_rec]=="")
			{
				continue;
			}
			else
			{
				$get_community_recorded_event=$new_profile_class_obj->recordedEvents($date,$get_rec_event[$exp_rec]);
				if(mysql_num_rows($get_community_recorded_event)>0)
				{
					$get_recorded_event=mysql_fetch_assoc($get_community_recorded_event)
					?>
					<div class="tabItem"> <a href="add_community_event.php?id=<?php echo $get_recorded_event['id']; ?>"><img src="<?php if($get_recorded_event['image_name']==""){echo "community_event_jcrop/./croppedFiles/profile/Noimage.png";}else { echo $get_recorded_event['image_name'];}?>" width="200" height="175" />
					  <h3><?php echo substr($get_recorded_event['date'].' '.$get_recorded_event['title'],'0','29'); ?></h3></a>
					  <div class="player">
						<a href=""><img src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" /></a>
						<a href=""><img src="images/profile/add.gif"  onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'"/></a>
						<a href=""><img src="images/profile/download.gif" onmouseover="this.src='images/profile/download-over.gif'" onmouseout="this.src='images/profile/download.gif'" /></a>
					  </div>
					</div>
			<?php
				}
			}
		}
		?>
      </div>
	  <?php
	  }
	  ?>
    </div>
    
    
    
    <div id="promotionsTab" class="hiddenBlock6">
	<?php
		if($getuser['artist_view_selected'] !="" || $getuser['community_view_selected'] !="")
		{
		?>
			<div id="mediaTab" class="hiddenBlock6">
				<div id="profileFeatured">
					<div class="playall">
						Play All
					</div>
					<a href="">
						<img src="images/profile/play.png" width="35" height="23" onmouseover="this.src='images/profile/play-over.png'" onmouseout="this.src='images/profile/play.png'" />
					</a>
					<a href="">
						<img src="images/profile/add.png" width="27" height="23" onmouseover="this.src='images/profile/add-over.png'" onmouseout="this.src='images/profile/add.png'" />
					</a>
				</div>
				<div id="profileFeatured">
			<div class="fmedia">
				featured Media
			</div>
			<?php //echo $getuser['featured_media'].$getuser['media_id'];
			$table_type = "artist";
			if($getuser['featured_media'] == "Gallery")
			{
				$find_whether_regis=$new_profile_class_obj->get_count_Gallery_at_register($getuser["$table_type".'_id'],$getuser['media_id']);
				//var_dump($find_whether_regis);
				if($find_whether_regis["count(*)"]>1)
				{
					$getimages=$new_profile_class_obj->get_featured_media_gallery_at_register($getuser['media_id'],$table_type);
					$feature_image_name = array();
					$feature_image_title = array();
					$feature_count = 0;
					while($get_image = mysql_fetch_assoc($getimages))
					{
						$feature_image_name[$feature_count] = $get_image['image_name'];
						$feature_image_title[$feature_count] = $get_image['image_title'];
						$feature_count++;
					}
					$feature_big_img = $feature_image_name[0];
					$featured_gal_img = implode(",", $feature_image_name);
					$featured_gal_img_title = implode(",", $feature_image_title);
					$feature_org_path = "http://reggallery.s3.amazonaws.com/";
					$feature_thum_path = "http://reggalthumb.s3.amazonaws.com/";
				}
				else
				{
					$getimages=$new_profile_class_obj->get_featured_media_gallery($getuser['media_id']);
					$feature_image_name = array();
					$feature_image_title = array();
					$feature_count = 0;
					while($get_image = mysql_fetch_assoc($getimages))
					{
						$feature_image_name[$feature_count] = $get_image['image_name'];
						$feature_image_title[$feature_count] = $get_image['image_title'];
						$feature_count++;
					}
					$feature_big_img = $feature_image_name[0];
					$featured_gal_img = implode(",", $feature_image_name);
					$featured_gal_img_title = implode(",", $feature_image_title);
					$feature_org_path = "http://medgallery.s3.amazonaws.com/";
					$feature_thum_path = "http://medgalthumb.s3.amazonaws.com/";
				}
			?>
				<a href="javascript:void(0);"><img src="../images/profile/play.png" width="35" height="23" onmouseover="this.src='../images/profile/play-over.png'" onmouseout="this.src='../images/profile/play.png'" onclick="checkload('<?php echo $feature_big_img;?>','<?php echo $featured_gal_img; ?>','<?php echo $feature_thum_path; ?>','<?php echo $feature_org_path; ?>','<?php echo $featured_gal_img_title; ?>')" /></a>
			<?php
			}
			if($getuser['featured_media'] == "Song")
			{
			?>
				<a href="javascript:void(0);"><img src="../images/profile/play.png" width="35" height="23" onmouseover="this.src='../images/profile/play-over.png'" onmouseout="this.src='../images/profile/play.png'" onclick="showPlayer('a','<?php echo $getuser['media_id'];?>','play')" /></a>
			<?php
			}
			?>
			<?php
			if($getuser['featured_media'] == "Video")
			{
			?>
				<a href="javascript:void(0);"><img src="../images/profile/play.png" width="35" height="23" onmouseover="this.src='../images/profile/play-over.png'" onmouseout="this.src='../images/profile/play.png'" onclick="showPlayer('v','<?php echo $getuser['media_id'];?>','playlist')" /></a>
			<?php
			}
			?>
			<a href=""><img src="../images/profile/add.png" width="27" height="23" onmouseover="this.src='../images/profile/add-over.png'" onmouseout="this.src='../images/profile/add.png'" /></a>
		</div>
				<?php
				if($getuser['artist_view_selected'] !="")
				{
					$src_list = "http://artjcropthumb.s3.amazonaws.com/";
					$exp_art_id = explode(",",$getuser['artist_view_selected']);
					?>
					<h2><a href="#">Artists</a></h2>
					<div class="rowItem">
					<?php
					for($art_count = 0;$art_count< count($exp_art_id); $art_count++)
					{
						if($exp_art_id[$art_count]=="")
						{
							continue;
						}
						else
						{
							$get_friend_artist = $new_profile_class_obj->get_artist_friends($exp_art_id[$art_count]);
							while($friend_art = mysql_fetch_assoc($get_friend_artist))
							{
							?>
								<div class="tabItem"> <img width="200" height="175" src="<?php echo $src_list . $friend_art['listing_image_name'];?>">
								  <h3><?php echo $friend_art['name'];?></h3>
								  <div class="player">
									<img onmouseout="this.src='images/profile/play.gif'" onmouseover="this.src='images/profile/play-over.gif'" src="images/profile/play.gif">
									<img onmouseout="this.src='images/profile/add.gif'" onmouseover="this.src='images/profile/add-over.gif'" src="images/profile/add.gif">
									<img onmouseout="this.src='images/profile/download.gif'" onmouseover="this.src='images/profile/download-over.gif'" src="images/profile/download.gif">
								  </div>
								</div>
							<?php
							}
						}
					}
					?>
					</div>
				<?php
				}
				if($getuser['community_view_selected'] !="")
				{
					$src_list = "http://comjcropthumb.s3.amazonaws.com/";
					$exp_com_id = explode(",",$getuser['community_view_selected']);
					?>
					<h2><a href="#">Community</a></h2>
					<div class="rowItem">
					<?php
					for($com_count = 0;$com_count< count($exp_com_id); $com_count++)
					{
						if($exp_com_id[$com_count]=="")
						{
							continue;
						}
						else
						{
							$get_friend_community = $new_profile_class_obj->get_community_friends($exp_com_id[$com_count]);
							while($friend_com = mysql_fetch_assoc($get_friend_community))
							{
							?>
								<div class="tabItem"> <img width="200" height="175" src="<?php echo $src_list . $friend_com['listing_image_name'];?>">
								  <h3><?php echo $friend_com['name'];?></h3>
								  <div class="player">
									<img onmouseout="this.src='images/profile/play.gif'" onmouseover="this.src='images/profile/play-over.gif'" src="images/profile/play.gif">
									<img onmouseout="this.src='images/profile/add.gif'" onmouseover="this.src='images/profile/add-over.gif'" src="images/profile/add.gif">
									<img onmouseout="this.src='images/profile/download.gif'" onmouseover="this.src='images/profile/download-over.gif'" src="images/profile/download.gif">
								  </div>
								</div>
							<?php
							}
								
						}
					}
					?>	
						</div>
					<?php
				}
				?>
			</div>    
		<?php
		}
		else
		{
		?>
			<div id="mediaTab" class="hiddenBlock6">
			</div>
		<?php
		}
		?>
    </div>
    <div id="popupContact">
		<a id="popupContactClose">x</a>
		<p id="contactArea">
			Only registered users can view media. 
		</p>
        <h1>Register <a href="index.php">Here</a>, It's Free!</h1><br />
        <p>Already Registered ? Login below.</p>
        <div class="formCont">
            <div class="formFieldCont">
        	<div class="fieldTitle">Username</div>
            <input type="text" class="textfield" />
        </div>
       		<div class="formFieldCont">
        	<div class="fieldTitle">Password</div>
            <input type="password" class="textfield" />
        </div>
        	<div class="formFieldCont">
        	<div class="fieldTitle"></div>
            <input type="image" class="login" src="images/profile/login.png" width="47" height="23" />
        </div>
        </div>
	</div>
    <div id="backgroundPopup"></div>
    
    
    <!-- PROFILE END -->
  </div>
 <?php include_once("displayfooter.php");?>
</div>
</div>

<div id="light-gal" class="gallery-wrapper">
	<div id="bg">
		<a href="#" class="nextImageBtn" title="next"></a>
		<a href="#" class="prevImageBtn" title="previous"></a>
		<img width="1680" src="./uploads/gallery/1335074394-7032884797_475ce129ca.jpg" height="1050" alt="" title="" id="bgimg" />
	</div>
	<div id="preloader"><img src="./galleryfiles/ajax-loader_dark.gif" width="32" height="32" /></div>
	<div id="img_title"></div>
	<div id="toolbar">
		<a href="#" title="Maximize" id="maximizeImg" onClick="ImageViewMode('full');return false"><img src="./galleryfiles/toolbar_fs_icon.png" width="50" height="50"  /></a>
		<a href="#" title="Close" id="closeGal" onClick="CloseGallery();return false"><img src="./galleryfiles/close.png" width="50" height="50"  /></a>
	</div>
	<div id="thumbnails_wrapper">
	<div id="outer_container">
	<div class="thumbScroller">
		<div class="container" id="gallery-container">
			
		</div>
	</div>
	</div>
	</div>
</div>


</body>
</html>
<script type="text/javascript" src="galleryfiles/jqgal.js"></script>
<script>

function checkload(defImg,Images,orgpath,thumbpath,title){

	var arr_sep = Images.split(',');
	var k;
	var data = new Array();
	for(k=0;k<arr_sep.length;k++)
	{
		//arr_sep[k] = arr_sep[k].replace("\"","");
		arr_sep[k] = arr_sep[k].replace(/"/g, '');
		arr_sep[k] = arr_sep[k].replace("[","");
		arr_sep[k] = arr_sep[k].replace("]","");
		data[k] = arr_sep[k];
	}

	if(data.length>0){
		$("#gallery-container").html('');
	}
	
	var title_sep = title.split(',');

	var q;
	var data1 = new Array();
	for(q=0;q<title_sep.length;q++)
	{
		title_sep[q] = title_sep[q].replace(/"/g, '');
		title_sep[q] = title_sep[q].replace("[","");
		title_sep[q] = title_sep[q].replace("]","");
		data1[q] = title_sep[q];
	}
	
	//$("#bgimg").attr('src','<?php echo $orgPath;?>'+defImg);
	$("#bgimg").attr('src',thumbpath+defImg);
	$("#bgimg").attr('title',data1[0]);
	$("#bgimg").attr('title',data1[0]);
	
/*	var the1stImg = new Image();
	the1stImg.onload = CreateDelegate(the1stImg, theNewImg_onload);
	the1stImg.src = '<?php echo $orgPath;?>'+defImg;
	
	$("#bg").append('<img width="1680" src="<?php echo $orgPath;?>'+defImg+'" height="1050" alt="Denebola" title="Denebola" id="bgimg" />');
*/
	for(var i=0;i<data.length;i++){
		$("#gallery-container").append("<div class='content'><div><a href='"+thumbpath+data[i]+"'><img  src='"+orgpath+data[i]+"' title='"+data1[i]+"' class='thumb' /></a></div></div>");
	
	}
	loadGallery();	
	$("#light-gal").css('visibility','visible');
	//setTimeout(function(){	loadGallery();	$("#light-gal").css('visibility','visible');	},500);
	document.getElementById("gallery-container").style.left = "0px";
}
	$(document).ready(function(){
		// Load the classic theme
		//Galleria.loadTheme('./galleria/themes/classic/galleria.classic.min.js');
		//Galleria.loadTheme('../javascripts/themes/classic/galleria.classic.min.js');

		// Initialize Galleria
		//Galleria.run('#galleria10');

		
		$("a[rel^='prettyPhoto']").prettyPhoto({social_tools:''});
		$("a[rel^='prettyAudio']").prettyPhoto({social_tools:'',
			changepicturecallback: function(){ $('audio').mediaelementplayer(); $(".mejs-container").css('margin-left',0); }	
		});

		//$("a[rel^='prettyAudio']").prettyPhoto();
	});

/////// Related Player	
	var windowRef;
	function showPlayer(tp,ref,act){
		//var windowRef = window.open('player/?tp='+tp+'&ref='+ref+'&source='+source);
		if(logedinFlag==""){	centerPopup();	loadPopup(); return;	}
		var pl = $("#curr_playing_playlist").val();
		$.ajax({
			type: "POST",
			url: 'player/getMediaInfo.php',
			data: { "tp":tp, "ref":ref, "source":"<?php echo $getuser['community_id'] ;?>", "act":act, "pl":pl, "creator":profileName },
			success: function(data){
				$("#curr_playing_playlist").val(data);
				if(!windowRef){
					windowRef = window.open('combinePlayer/index.php?tp='+tp+'&ref='+ref+'&pl='+data, 'formpopup', 'width=580,height=730,resizeable,scrollbars');
				}
			}
		});
	}

	$(".login").click(function(){
		checkLogin();//document.checkLoginFrm.submit();
	});

</script> 