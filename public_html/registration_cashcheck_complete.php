<?php
    ob_start();
	include_once('login_includes.php');
?>

<?php include_once('includes/header.php'); ?>
<title>Purify Entertainment: Six Feature</title>
	
  <div id="contentContainer">
    <h1>Done!</h1>
    <p>Thank you for registering with  Purify Entertainment. 24hrs after your payment is processed,   you will be sent a link  to verify your email address. Please make all checks payable to <strong>Purify Entertainment  </strong>and mail  to:</p>
    <p><strong>Purify Entertainment<br />
      PO Box 333<br />
      Half Moon Bay, CA 94019</strong></p>
  </div>
  
<?php include_once('includes/footer.php'); ?>