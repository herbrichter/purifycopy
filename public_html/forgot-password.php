<?php
	include("classes/forgotPassword.php");
	include_once('sendgrid/SendGrid_loader.php');
	$sendgrid = new SendGrid('', '');
	$pass_obj = new forgotPassword();
    $domainname=$_SERVER['SERVER_NAME'];
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "https://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="https://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	<head>
		<meta https-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>Purify Art</title>
		<link rel="stylesheet" type="text/css" media="screen" href="includes/purify.css" />
		<!--<script src="https://jqueryjs.googlecode.com/files/jquery-1.2.6.min.js" type="text/javascript"></script>
		<script src="includes/popup.js" type="text/javascript"></script>-->
	</head>
	<body>
		<div>
			<div id="mesg_post" style="display:none;">
				<div class="fancy_header">
					Forgot Password
				</div>
				<div id="searchContent">
					<div id="msg_sent">
					</div>
					<div class="buttonCont">
						<input type="submit" id="close_pass" value="OK" class="button" onClick="close_fancy()" />
					</div>
				</div>
			</div>
<?php
			if(!isset($_POST) || empty($_POST))
			{
				if(!isset($_POST['email_pass_forgot']))
				{
?>
					<div id="afterPost">
						<div class="fancy_header">
							Forgot Password
						</div>
						<div id="searchContent">
							<form id="forgot_password" name="forgot_password" method="POST">
								<div class="fieldCont">
									<div class="fieldTitle" id="hide_tag">Email</div>
									<input type="text" class="fieldText" id="email_pass_forgot" name="email_pass_forgot" />
								</div>
								<div class="buttonCont">
									<input type="submit" id="send_pass" value="Get Password" class="button" onClick="return send_password()" />
								</div>
							</form>
						</div>
					</div>
<?php
				}
			}
?>
		</div>
	</body>
</html>

<script>
	$("#forgot_password").bind("submit",function(){
		$.ajax({
			type:"POST",
			url:"forgot-password.php",
			data:$(this).serializeArray(),
			success: function(data){
				$("#afterPost").hide();
				document.getElementById("mesg_post").style.display="block";
				$("#msg_sent").html(data);
				//$("#searchContent .fieldCont").css({"margin-bottom":"0px"});
				//$("#hide_tag").hide();
				//$("#email_pass_forgot").hide();
				//$("#send_pass").hide();
				//document.getElementById("megs_apost").style.display="none";				
			}
		});
		return false;
	});
	
	function send_password()
	{
		var email_check_pass = document.getElementById("email_pass_forgot").value;
		var atpos = email_check_pass.indexOf("@");
		var dotpos = email_check_pass.lastIndexOf(".");
		var form_mail_pass = document.getElementById("forgot_password");
		
		if (atpos<1 || dotpos<atpos+2 || dotpos+2>=email_check_pass.lenght || email_check_pass=="")
		{
			alert("Please enter a valid Email Id.");
			return false;
		}
		else
		{
			//var hint_msg = document.getElementById("msg_sent");
			form_mail_pass.action = "forgot-password.php";
		}
	}
	
	function close_fancy()
	{
		//parent.$.fancybox.close();
		parent.jQuery.fancybox.close();
	}
	
</script>

<?php
	if(isset($_POST) && isset($_POST['email_pass_forgot']))
	{
?>
<?php
		$send_user_pass = $pass_obj->sendMail_Pass($_POST['email_pass_forgot']);
		if($send_user_pass=='not_found')
		{
			echo "<p style='font-size:15px;'>Sorry,Email Not Found in Our Database.Please retry.</p>";
?>
			<script>
				//data  = "Sorry,Email Not Found in Our Database.Please retry.";
				//$("#msg_sent").html(data);
			</script>
<?php
		}
		else
		{
			$to = $_POST['email_pass_forgot'];
			$strSubject = "PURIFY ART - Password";
			$strHeader = "";
			$strHeader .= "From: Purify Art < no-reply@purifyart.com>";
			//$breaks = echo "<br/><br/>";Your Current password - ".$send_user_pass['pass']."<br/>
			$strMessage = "Hello ".$send_user_pass['fname'].",<br/>

You have requested to reset your password. To do so follow the link below.<br/>

<a href='http://".$domainname."/pw_request.php' style='background-color: #D3D3D3;
    border: 0 none;
    color: #000000;
	font-weight:bold;
    display: block;
    font-size: 0.75em;
    margin: 0 auto;
    text-decoration: none;
    text-transform: uppercase;
    width: 101px;'>Reset Password</a>";

$url = "admin/newsletter/"; 
			$myFile = $url . "other_templates.txt";
			$fh = fopen($myFile, 'r');
			$theData = fread($fh, filesize($myFile));
			fclose($fh);
			
			$data_email = explode('|~|', $theData);
			
			$ns_user_n = str_replace('{::message::}', $strMessage, $data_email['1']);
			
			$body_email = $data_email['0'] . $ns_user_n . $data_email['2'];
			
			$mail_grid = new SendGrid\Mail();
			
			$mail_grid->addTo($to)->
					setFromName('Purify Art')->
					setFrom('no-reply@purifyart.com')->
					setSubject($strSubject)->
					setHtml($body_email);
			
			//$sendgrid->web->send($mail_grid);
			$sendgrid -> smtp -> send($mail_grid);
			
			//mail($to,$strSubject,$strMessage,$strHeader);
			
			echo "<p style='font-size:15px;'>Password reset link has been sent to your Email Id.</p>";
?>
			<script>
				/* data  = "Password reset link has been sent to your Email Id.";
				$("#msg_sent").html(data);
				$("#searchContent .fieldCont").css({"margin-bottom":"0px"});
				$("#hide_tag").hide();
				$("#email_pass_forgot").hide();
				$("#send_pass").hide();
				$("#close_pass").show(); */
			</script>
<?php
		}
	}
?>
<style>
	
	#searchContent { float: left; margin-top: 0; border-radius: 6px; padding: 20px; width: 294px; background: none repeat scroll 0 0 #FFFFFF; }
	#searchContent_msg { float: left; margin-top: 0; border-radius: 6px; padding: 20px; width: 294px; background: none repeat scroll 0 0 #FFFFFF; }
	#outerContainer { background: none repeat scroll 0 0 rgba(0, 0, 0, 0); }
</style>