<?php
	session_start();
	ob_start();
	include_once('commons/db.php');
	include_once('classes/Commontabs.php');
	include_once('classes/CreatePurifyMember.php');
	include_once("classes/AddToCart.php");
	include_once("classes/Donate_Purify.php");
	include_once("classes/AddToCart.php");
	include_once("classes/GetFanClubInfo.php");
	include_once("newsmtp/PHPMailer/index.php");
	include_once('sendgrid/SendGrid_loader.php');
	include_once('classes/GetSubscriptionInfo.php');

	$newtab = new Commontabs();
	include_once('includes/header.php');	
	$donate_obj = new AddToCart();
	$subs_to_aftr_log = new GetSubscriptionInfo();
    $domainname=$_SERVER['SERVER_NAME'];
?>
	<div id="outerContainer">
<?php		
			//if($_SERVER['HTTP_REFERER'] == 'http://'.$domainname.'/confirmation.php?passkey='.$_GET['confirm_codes'].'&send_meds_dlink=1')
			//{
?>
			<p style="position: relative; text-align: center; line-height: 45px; padding: 10px 0px 0px;">Thank you for registering for Purify Art. Below you can download all of the songs and projects that you have purchased. Once you continue to the welcome page by clicking on the Continue button below you can setup your general profile, find and follow artists, view and download free and purchased media, and more.</p>
				<div id="actualContent" style="margin-bottom: 30px;">
<?php
					if($_SESSION['login_email']!="")
					{
						$gen_daats_meds_free = $newtab->tabs();
						$download_id = "";
						$download_ids = "";
						$fans_array = array();
						$f = 0;
						
						$sql_cart_meds = mysql_query("SELECT * FROM addtocart WHERE buy_email='".$_SESSION['login_email']."' AND buyer_id='".$gen_daats_meds_free['general_user_id']."' AND buy_status=1");
						
						$pur_date_sub_free_sin = date('Y-m-d');
						$exp_date_sub_free_sin = strtotime(date("Y-m-d", strtotime($pur_date_sub_free_sin)) . " +1 year");
						$exp_date_sub_free_sin = date('Y-m-d', $exp_date_sub_free_sin);
						
						if($sql_cart_meds!="")
						{
							if(mysql_num_rows($sql_cart_meds)>0)
							{
								while($res_all_media = mysql_fetch_assoc($sql_cart_meds))
								{
									$download_id .= $res_all_media['media_id'].',';
									
									$sql_meds_aftr_ews = mysql_query("SELECT * FROM general_media WHERE id='".$res_all_media['media_id']."'");
									$ans_meds_aftrs_ews = mysql_fetch_assoc($sql_meds_aftr_ews);
										
									if($ans_meds_aftrs_ews['creator_info']!="")
									{										
										$exp__free_single_meds = explode('|',$ans_meds_aftrs_ews['creator_info']);
										
										if($exp__free_single_meds[0]=='general_artist')
										{
											$art_free_gen_data = mysql_query("SELECT * FROM general_user WHERE artist_id='".$exp__free_single_meds[1]."'");
											$ans_free_art_gen = mysql_fetch_assoc($art_free_gen_data);
											
											$name_aftr_down = $gen_daats_meds_free['fname'].' '.$gen_daats_meds_free['lname'];
											$rel_free_ids = $exp__free_single_meds[1];
											$rel_free_types = 'artist';
											$news_feed_free = 1;
											$emails_feed_free = 1;
											$subs_free_ids_gets = $ans_free_art_gen['general_user_id'];
											
											$chk_aftr_user_subs_free = $subs_to_aftr_log->already_avail_subs($name_aftr_down,$_SESSION['login_email'],$rel_free_ids,$rel_free_types,$pur_date_sub_free_sin,$exp_date_sub_free_sin,$news_feed_free,$emails_feed_free,$subs_free_ids_gets);
											
											if($chk_aftr_user_subs_free=='0')
											{
												$subs_done_aftr_single = $subs_to_aftr_log->become_subscriber($name_aftr_down,$_SESSION['login_email'],$rel_free_ids,$rel_free_types,$pur_date_sub_free_sin,$exp_date_sub_free_sin,$news_feed_free,$emails_feed_free,$subs_free_ids_gets);
											}
										}
										elseif($exp__free_single_meds[0]=='general_community')
										{
											$com_free_gen_data = mysql_query("SELECT * FROM general_user WHERE community_id='".$exp__free_single_meds[1]."'");
											$ans_free_art_gen = mysql_fetch_assoc($com_free_gen_data);
											
											$name_aftr_down = $gen_daats_meds_free['fname'].' '.$gen_daats_meds_free['lname'];
											$rel_free_ids = $exp__free_single_meds[1];
											$rel_free_types = 'community';
											$news_feed_free = 1;
											$emails_feed_free = 1;
											$subs_free_ids_gets = $ans_free_art_gen['general_user_id'];
											
											$chk_aftr_user_subs_free = $subs_to_aftr_log->already_avail_subs($name_aftr_down,$_SESSION['login_email'],$rel_free_ids,$rel_free_types,$pur_date_sub_free_sin,$exp_date_sub_free_sin,$news_feed_free,$emails_feed_free,$subs_free_ids_gets);
											
											if($chk_aftr_user_subs_free=='0')
											{
												$subs_done_aftr_single = $subs_to_aftr_log->become_subscriber($name_aftr_down,$_SESSION['login_email'],$rel_free_ids,$rel_free_types,$pur_date_sub_free_sin,$exp_date_sub_free_sin,$news_feed_free,$emails_feed_free,$subs_free_ids_gets);
											}
										}
										elseif($exp__free_single_meds[0]=='artist_project')
										{
											$sql_art_pros_chk_aftr = mysql_query("SELECT * FROM artist_project WHERE id='".$exp__free_single_meds[1]."'");
											$ans_art_pros_chk_aftr = mysql_fetch_assoc($sql_art_pros_chk_aftr);
											
											$artp_free_gen_data = mysql_query("SELECT * FROM general_user WHERE artist_id='".$ans_art_pros_chk_aftr['artist_id']."'");
											$ans_free_art_gen = mysql_fetch_assoc($artp_free_gen_data);
											
											$name_aftr_down = $gen_daats_meds_free['fname'].' '.$gen_daats_meds_free['lname'];
											$rel_free_ids = $ans_art_pros_chk_aftr['artist_id'].'_'.$ans_art_pros_chk_aftr['id'];
											$rel_free_types = 'artist_project';
											$news_feed_free = 1;
											$emails_feed_free = 1;
											$subs_free_ids_gets = $ans_free_art_gen['general_user_id'];
											
											$chk_aftr_user_subs_free = $subs_to_aftr_log->already_avail_subs($name_aftr_down,$_SESSION['login_email'],$rel_free_ids,$rel_free_types,$pur_date_sub_free_sin,$exp_date_sub_free_sin,$news_feed_free,$emails_feed_free,$subs_free_ids_gets);
											
											if($chk_aftr_user_subs_free=='0')
											{
												$subs_done_aftr_single = $subs_to_aftr_log->become_subscriber($name_aftr_down,$_SESSION['login_email'],$rel_free_ids,$rel_free_types,$pur_date_sub_free_sin,$exp_date_sub_free_sin,$news_feed_free,$emails_feed_free,$subs_free_ids_gets);
											}
										}
										elseif($exp__free_single_meds[0]=='community_project')
										{
											$sql_art_pros_chk_aftr = mysql_query("SELECT * FROM community_project WHERE id='".$exp__free_single_meds[1]."'");
											$ans_art_pros_chk_aftr = mysql_fetch_assoc($sql_art_pros_chk_aftr);
											
											$artp_free_gen_data = mysql_query("SELECT * FROM general_user WHERE community_id='".$ans_art_pros_chk_aftr['community_id']."'");
											$ans_free_art_gen = mysql_fetch_assoc($artp_free_gen_data);
											
											$name_aftr_down = $gen_daats_meds_free['fname'].' '.$gen_daats_meds_free['lname'];
											$rel_free_ids = $ans_art_pros_chk_aftr['community_id'].'_'.$ans_art_pros_chk_aftr['id'];
											$rel_free_types = 'community_project';
											$news_feed_free = 1;
											$emails_feed_free = 1;
											$subs_free_ids_gets = $ans_free_art_gen['general_user_id'];
											
											$chk_aftr_user_subs_free = $subs_to_aftr_log->already_avail_subs($name_aftr_down,$_SESSION['login_email'],$rel_free_ids,$rel_free_types,$pur_date_sub_free_sin,$exp_date_sub_free_sin,$news_feed_free,$emails_feed_free,$subs_free_ids_gets);
											
											if($chk_aftr_user_subs_free=='0')
											{
												$subs_done_aftr_single = $subs_to_aftr_log->become_subscriber($name_aftr_down,$_SESSION['login_email'],$rel_free_ids,$rel_free_types,$pur_date_sub_free_sin,$exp_date_sub_free_sin,$news_feed_free,$emails_feed_free,$subs_free_ids_gets);
											}
										}
									}
								}
							}
						}
						
						$sql_fan_datas = mysql_query("select * from add_to_cart_fan_pro where buy_email='".$_SESSION['login_email']."' AND buyer_id='".$gen_daats_meds_free['general_user_id']."' AND buy_status=1");
						if($sql_fan_datas!="")
						{
							if(mysql_num_rows($sql_fan_datas)>0)
							{
								while($res_all_media = mysql_fetch_assoc($sql_fan_datas))
								{
									$fans_data[$f] =  $donate_obj->get_creator_profile_url($res_all_media['table_name'],$res_all_media['type_id']);
									
									$media = mysql_query("select * from addtocart_all where buy_status=1 AND buyer_id='".$gen_daats_meds_free['general_user_id']."' AND table_name='".$res_all_media['table_name']."' AND type_id='".$res_all_media['type_id']."'");
									$download_id_pro = "";
									while($res_media = mysql_fetch_assoc($media))
									{
										$download_ids .= $res_media['media_id'].',';
										$download_id_pro .= $res_media['media_id'].',';
										
										if($fans_data[$f]['creators_info']!="")
										{										
											$exp__free_single_meds = explode('|',$fans_data[$f]['creators_info']);
											
											if($exp__free_single_meds[0]=='general_artist')
											{
												$art_free_gen_data = mysql_query("SELECT * FROM general_user WHERE artist_id='".$exp__free_single_meds[1]."'");
												$ans_free_art_gen = mysql_fetch_assoc($art_free_gen_data);
												
												$name_aftr_down = $gen_daats_meds_free['fname'].' '.$gen_daats_meds_free['lname'];
												$rel_free_ids = $exp__free_single_meds[1];
												$rel_free_types = 'artist';
												$news_feed_free = 1;
												$emails_feed_free = 1;
												$subs_free_ids_gets = $ans_free_art_gen['general_user_id'];
												
												$chk_aftr_user_subs_free = $subs_to_aftr_log->already_avail_subs($name_aftr_down,$_SESSION['login_email'],$rel_free_ids,$rel_free_types,$pur_date_sub_free_sin,$exp_date_sub_free_sin,$news_feed_free,$emails_feed_free,$subs_free_ids_gets);
												
												if($chk_aftr_user_subs_free=='0')
												{
													$subs_done_aftr_single = $subs_to_aftr_log->become_subscriber($name_aftr_down,$_SESSION['login_email'],$rel_free_ids,$rel_free_types,$pur_date_sub_free_sin,$exp_date_sub_free_sin,$news_feed_free,$emails_feed_free,$subs_free_ids_gets);
												}
											}
											elseif($exp__free_single_meds[0]=='general_community')
											{
												$com_free_gen_data = mysql_query("SELECT * FROM general_user WHERE community_id='".$exp__free_single_meds[1]."'");
												$ans_free_art_gen = mysql_fetch_assoc($com_free_gen_data);
												
												$name_aftr_down = $gen_daats_meds_free['fname'].' '.$gen_daats_meds_free['lname'];
												$rel_free_ids = $exp__free_single_meds[1];
												$rel_free_types = 'community';
												$news_feed_free = 1;
												$emails_feed_free = 1;
												$subs_free_ids_gets = $ans_free_art_gen['general_user_id'];
												
												$chk_aftr_user_subs_free = $subs_to_aftr_log->already_avail_subs($name_aftr_down,$_SESSION['login_email'],$rel_free_ids,$rel_free_types,$pur_date_sub_free_sin,$exp_date_sub_free_sin,$news_feed_free,$emails_feed_free,$subs_free_ids_gets);
												
												if($chk_aftr_user_subs_free=='0')
												{
													$subs_done_aftr_single = $subs_to_aftr_log->become_subscriber($name_aftr_down,$_SESSION['login_email'],$rel_free_ids,$rel_free_types,$pur_date_sub_free_sin,$exp_date_sub_free_sin,$news_feed_free,$emails_feed_free,$subs_free_ids_gets);
												}
											}
											elseif($exp__free_single_meds[0]=='artist_project')
											{
												$sql_art_pros_chk_aftr = mysql_query("SELECT * FROM artist_project WHERE id='".$exp__free_single_meds[1]."'");
												$ans_art_pros_chk_aftr = mysql_fetch_assoc($sql_art_pros_chk_aftr);
												
												$artp_free_gen_data = mysql_query("SELECT * FROM general_user WHERE artist_id='".$ans_art_pros_chk_aftr['artist_id']."'");
												$ans_free_art_gen = mysql_fetch_assoc($artp_free_gen_data);
												
												$name_aftr_down = $gen_daats_meds_free['fname'].' '.$gen_daats_meds_free['lname'];
												$rel_free_ids = $ans_art_pros_chk_aftr['artist_id'].'_'.$ans_art_pros_chk_aftr['id'];
												$rel_free_types = 'artist_project';
												$news_feed_free = 1;
												$emails_feed_free = 1;
												$subs_free_ids_gets = $ans_free_art_gen['general_user_id'];
												
												$chk_aftr_user_subs_free = $subs_to_aftr_log->already_avail_subs($name_aftr_down,$_SESSION['login_email'],$rel_free_ids,$rel_free_types,$pur_date_sub_free_sin,$exp_date_sub_free_sin,$news_feed_free,$emails_feed_free,$subs_free_ids_gets);
												
												if($chk_aftr_user_subs_free=='0')
												{
													$subs_done_aftr_single = $subs_to_aftr_log->become_subscriber($name_aftr_down,$_SESSION['login_email'],$rel_free_ids,$rel_free_types,$pur_date_sub_free_sin,$exp_date_sub_free_sin,$news_feed_free,$emails_feed_free,$subs_free_ids_gets);
												}
											}
											elseif($exp__free_single_meds[0]=='community_project')
											{
												$sql_art_pros_chk_aftr = mysql_query("SELECT * FROM community_project WHERE id='".$exp__free_single_meds[1]."'");
												$ans_art_pros_chk_aftr = mysql_fetch_assoc($sql_art_pros_chk_aftr);
												
												$artp_free_gen_data = mysql_query("SELECT * FROM general_user WHERE community_id='".$ans_art_pros_chk_aftr['community_id']."'");
												$ans_free_art_gen = mysql_fetch_assoc($artp_free_gen_data);
												
												$name_aftr_down = $gen_daats_meds_free['fname'].' '.$gen_daats_meds_free['lname'];
												$rel_free_ids = $ans_art_pros_chk_aftr['community_id'].'_'.$ans_art_pros_chk_aftr['id'];
												$rel_free_types = 'community_project';
												$news_feed_free = 1;
												$emails_feed_free = 1;
												$subs_free_ids_gets = $ans_free_art_gen['general_user_id'];
												
												$chk_aftr_user_subs_free = $subs_to_aftr_log->already_avail_subs($name_aftr_down,$_SESSION['login_email'],$rel_free_ids,$rel_free_types,$pur_date_sub_free_sin,$exp_date_sub_free_sin,$news_feed_free,$emails_feed_free,$subs_free_ids_gets);
												
												if($chk_aftr_user_subs_free=='0')
												{
													$subs_done_aftr_single = $subs_to_aftr_log->become_subscriber($name_aftr_down,$_SESSION['login_email'],$rel_free_ids,$rel_free_types,$pur_date_sub_free_sin,$exp_date_sub_free_sin,$news_feed_free,$emails_feed_free,$subs_free_ids_gets);
												}
											}
										}
									}
									$download_id_pros[$f] = $download_id_pro;
									$f = $f + 1;
								}
							}
						}
						
						$vals_rtext = "";
						if($download_id!="" || (!empty($fans_data) && !empty($download_id_pros)))
						{
							$dwn_links_all = $download_id.','.$download_ids;
							$dwn_links = $download_id;
							$chk_more = explode(',',$dwn_links);
							if(count($chk_more)>1 || count($fans_data)>1)
							{
								if($chk_more[1]!="")
								{
									$text_dwn = 'Download All';
								}
								elseif(count($fans_data)>1)
								{
									$text_dwn = 'Download All';
								}
								else
								{
									$text_dwn = 'Download';
								}
							}
							elseif(count($chk_more)==1)
							{
								$text_dwn = 'Download';
							}
							
							/* if($text_dwn=='Download All')
							{
								echo '<a id="pops_downloads" style="font-size: 15px;
								font-weight: bold;
								margin-left: 383px;
								background-color: #000000;
								border: 0 none;
								color: #FFFFFF;
								cursor: pointer;
								float: left;
								padding: 10px 0;
								text-align: center;
								width: 150px;margin-bottom:20px;
								" href="zipes_1.php?download_meds='.$dwn_links_all.'">'.$text_dwn.'</a>';
							} */
							$start = $dwn_links_all;
							if($download_id!="")
							{
								$dwn_link = explode(',',$download_id);
							}
	?>
							<div id="playlistView" style="left: 200px;position: relative;">
								<div id="listFiles" style="height:30px;">
									<div class="titleCont">
										<div class="blkB">Title</div>
									</div>
								</div>
								<div id="listFiles" style="margin-bottom:30px;">
	<?php
									if($download_id!="")
									{
										for($i_d=0;$i_d<count($dwn_link);$i_d++)
										{
											$sql = mysql_query("SELECT * FROM general_media WHERE id='".$dwn_link[$i_d]."'");
											$ans = mysql_fetch_assoc($sql);
											$image_name ="";
											if($ans['title']!="" && $ans['media_type']!='116'  && $ans['media_type']!='115')
											{
												if($ans['media_type']=='114'){
													$image_name = $donate_obj->get_audio_img($ans['id']);
													$son_new_name_dp = mysql_query("SELECT * FROM media_songs WHERE media_id='".$ans['id']."'");
													$ans_son_new_name_dp = mysql_fetch_assoc($son_new_name_dp);
												}if($ans['media_type']=='115'){
													$image_name_video = $donate_obj->getvideoimage($ans['id']);
													$image_name = "https://img.youtube.com/vi/".$image_name_video."/default.jpg";
												}if($ans['media_type']=='113'){
													$image_name = $donate_obj->getgalleryimage($ans['id']);
												}
												$exp_creator_info = explode("|",$ans['creator_info']);
												$get_creator_url = $donate_obj->get_creator_profile_url($exp_creator_info[0],$exp_creator_info[1]);
												$creator_url = $get_creator_url['profile_url'];
		?>
												<div class="tableCont" >
													<div class="blkB" style="width:450px;">
														<img src="<?php echo $image_name; ?>" width="50" height="50" /><?php echo str_replace("\'","'",$ans['title']);?><br /><a href="/<?php echo $creator_url;?>" ><?php echo $ans['creator'];?></a>
													</div>
													<div class="blkC" style="width:90px;">
														<a class="fancybox fancybox.ajax" id="pops_download_media_<?php echo $i_d; ?>" href="zipes_meds_aftrlog.php?download_meds_zip=<?php echo urlencode($ans_son_new_name_dp['song_name']); ?>" style=" background-color: #000000;border: 0 none;color: #FFFFFF;float: left;font-size: 13px; border-radius:5px;font-weight: 700;margin-right: 0;padding: 0 17px;text-decoration: none;position:relative;top:15px;border-radius:5px; -webkit-border-radius:5px; -moz-border-radius:5px; -ms-border-radius:5px; -o-border-radius:5px;">Download</a>
													</div>
												</div>
		<?php
												$start_media = $ans_son_new_name_dp['song_name'];
		?>
												<script>
													$(document).ready(function() {
														$("#pops_download_media_"+<?php echo $i_d; ?>).fancybox({
															content: $("#waitmessage").html(),
															helpers : {
																media : {},
																buttons : {},
																title : null
															},
															'beforeLoad' : function(){ $.ajax({
																			type:'GET',
																			url:'zipes_meds_aftrlog.php?download_meds_zip=<?php echo urlencode($start_media); ?>',
																			success:function(data){
																				document.getElementById("download_file_name").value = data;
																				if (data.indexOf("DOCTYPE") !=-1) {
																					//location.reload();
																					alert("Media not present.");
																				}
																				else
																				{
																					$("#org_download").click();
																				}
																			}
																			
															})
															},
														});
													});
												</script>
		<?php
												/* if($_SESSION['login_email']==null && isset($_SESSION['guest_email']))
												{
													$token_insert = $get_fan_club->inserttoken($_SESSION['guest_email'],$num_ran,$_GET['SECURETOKEN']);
												}
												else
												{
													$token_insert = $get_fan_club->inserttoken($_SESSION['login_email'],$num_ran,$_GET['SECURETOKEN']);
												} */
											}
										}
									}
									if(!empty($fans_data))
									{
										for($i_d=0;$i_d<count($fans_data);$i_d++)
										{
											$image_mane = "";
											$creator_mane = "";
											$creator_mane_url = "";
											if(isset($fans_data[$i_d]['title']))
											{
												$titles_fan = str_replace("\'","'",$fans_data[$i_d]['title']);
												
												if(isset($fans_data[$i_d]['artist_id']))
												{
													$image_mane = '//artprothumb.s3.amazonaws.com/'.basename($fans_data[$i_d]['listing_image_name']);
												}
												elseif(isset($fans_data[$i_d]['community_id']))
												{
													$image_mane = '//comprothumjcrop.s3.amazonaws.com/'.basename($fans_data[$i_d]['listing_image_name']);
												}
												
												$title_url = $fans_data[$i_d]['profile_url'];
												if($fans_data[$i_d]['creators_info']!="")
												{
													$exp_creats_pros = explode('|',$fans_data[$i_d]['creators_info']);
													$getcreator_pros = $donate_obj->get_creator_profile_url($exp_creats_pros[0],$exp_creats_pros[1]);
													if(isset($getcreator_pros['title']))
													{
														$creator_mane = str_replace("\'","'",$getcreator_pros['title']);
													}
													else
													{
														$creator_mane = $getcreator_pros['name'];
													}
													$creator_mane_url = $getcreator_pros['profile_url'];
												}
												if(isset($fans_data[$i_d]['artist_id']))
												{
													$vals_rtext = $vals_rtext.','.'artist_project~'.$fans_data[$i_d]['id'];
												}
												elseif(isset($fans_data[$i_d]['community_id']))
												{
													$vals_rtext = $vals_rtext.','.'community_project~'.$fans_data[$i_d]['id'];
												}
											}
											else
											{
												$titles_fan = $fans_data[$i_d]['name'];
												if(isset($fans_data[$i_d]['artist_id']))
												{
													$image_mane = "//artjcropthumb.s3.amazonaws.com/".$fans_data[$i_d]['listing_image_name'];
													$vals_rtext = $vals_rtext.','.'art~'.$fans_data[$i_d]['artist_id'];
												}
												elseif(isset($fans_data[$i_d]['community_id']))
												{
													$image_mane = "//comjcropthumb.s3.amazonaws.com/".$fans_data[$i_d]['listing_image_name'];
													$vals_rtext = $vals_rtext.','.'com~'.$fans_data[$i_d]['community_id'];
												}
												$creator_mane = $fans_data[$i_d]['name'];
												$creator_mane_url = $fans_data[$i_d]['profile_url'];
											}
		?>
											<div class="tableCont" >
												<div class="blkB" style="width:450px;">
												<img src="<?php echo $image_mane; ?>" width="50" height="50" /><?php if($fans_data[$i_d]['type']=="for_sale"){ ?> <a target="_blank" href="/<?php echo $title_url; ?>" ><?php echo $titles_fan; ?></a> <?php }else if($fans_data[$i_d]['type']=="fan_club"){ echo $titles_fan.' Fan Club Membership'; }else{ echo $titles_fan; } ?><br /><a target="_blank" href="/<?php echo $creator_mane_url; ?>" ><?php echo $creator_mane; ?></a></div>
												<div class="blkC" style="width:90px;">
													<a class="fancybox fancybox.ajax" id="pops_download_fan_<?php echo $i_d; ?>" href="zipes_meds_aftrlog.php?download_meds_zip=<?php echo urlencode($fans_data[$i_d]['zip_name']); ?>" style=" background-color: #000000;border: 0 none;color: #FFFFFF;float: left;font-size: 13px; border-radius:5px;font-weight: 700;margin-right: 0;padding: 0 17px;text-decoration: none;position:relative;top:15px;border-radius:5px; -webkit-border-radius:5px; -moz-border-radius:5px; -ms-border-radius:5px; -o-border-radius:5px;">Download</a>
												</div>
											</div>
		<?php
											$start_fan = $fans_data[$i_d]['zip_name'];
		?>
											<script>
												$("#pops_download_fan_"+<?php echo $i_d; ?>).fancybox({
													content: $("#waitmessage").html(),
													helpers : {
														media : {},
														buttons : {},
														title : null
													},
													'beforeLoad' : function(){ $.ajax({
																	type:'GET',
																	url:'zipes_meds_aftrlog.php?download_meds_zip=<?php echo urlencode($start_fan); ?>',
																	success:function(data){
																		document.getElementById("download_file_name").value = data;
																		if (data.indexOf("DOCTYPE") !=-1) 
																		{
																			//location.reload();
																			alert("Media not present.");
																		}
																		else
																		{
																			$("#org_download").click();
																		}
																	}
																	
													})
													},
												});
											</script>
		<?php
											/* if($_SESSION['login_email']==null && isset($_SESSION['guest_email']))
											{
												$token_insert = $get_fan_club->inserttoken($_SESSION['guest_email'],$num_ran,$_GET['SECURETOKEN']);
											}
											else
											{
												$token_insert = $get_fan_club->inserttoken($_SESSION['login_email'],$num_ran,$_GET['SECURETOKEN']);
											} */
										}
									}
?>
								</div>
							</div>
<?php	
						}
					}
?>
				<input type="button" class="verify_sub" Onclick="window.location='profileedit.php?confirm_codes_edit=<?php echo $_GET['confirm_codes']; ?>#welcome'" value="Continue"/>
			</div>
<?php
			/* }
			else
			{
?>				
				<p style="position:relative; padding:30px 0px 0px; text-align:center; line-height:45px">There are no Medias for available for Download.</p>
<?php				
			} */
?>
<input type="hidden" id="download_file_name" />
							<a id="org_download" ></a>
							<div id="waitmessage" style="display:none;">
								<div class="fancy_header">
										Downloading in Process
								</div>
								<div id="contents_frames">
									<p>
								<?php
										echo "Your download is being created now and will begin soon.";
								?>	
									</p>
										<img style="bottom: 5px; left: 141px; position: relative; width: 25px;" src="images/ajax_loader_large.gif">
								</div>
							</div>
							<div id="zipes" style="display:none;">
								<div class="fancy_header">
									Downloading in Process
								</div>
								<div id="contents_frames" style="float:left;  width:430px;">
									<p>
<?php
										echo "Click OK to proceed with the download. If you are on a mobile device be sure to download the WINZIP app or else the .zip file with your download will not open. Thanks";
?>	
									</p>
									<div style="margin-left:94px; height:48px;">
										<a href="javascript:void(0);" onclick="close_fancies()" style="background-color: #000000;border: 0 none;color: #FFFFFF;float: left;font-size: 19px; border-radius:5px;font-weight: 700;left: 91px;padding: 7px 13px;position: relative;text-decoration: none;" id="click_download_close">OK</a>
									</div>
									<div style="text-align:center; font-size: 12px;">
										Download the WINZIP app
										<br/><br/>
									</div>
									<div style="margin-left:118px; font-size: 12px;">
										<a href="https://play.google.com/store/apps/details?id=com.winzip.android" target="_blank" style="background-color: #000000;border: 0 none;color: #FFFFFF;float: left;font-size: 0.825em;font-weight: 700;margin-right:10px;padding: 5px 10px;position: relative;text-decoration: none;" >PLAY STORE</a>
										<a href="https://itunes.apple.com/us/app/winzip/id500637987?mt=8" target="_blank" style="background-color: #000000;border: 0 none;color: #FFFFFF;float: left;font-size: 0.825em;font-weight: 700;padding: 5px 10px;position: relative;text-decoration: none;">APPLE STORE</a>
									</div>
								</div>
							</div>
<script>
$(document).ready(function() {
	$("#org_download").fancybox({
		content: $("#zipes").html(),
		helpers : {
				media : {},
				buttons : {},
				title : null
			}
	});
});	
	
	function close_fancies()
	{
		$.fancybox.close();
		var file_name = document.getElementById("download_file_name").value;
		window.location = 'downloads_aftrlog_ok.php?name='+file_name;
	}
</script>
</div>
<?php
	include_once("displayfooter.php");
?>
<style>
body { height:100%; }
#contents_frames{padding: 20px;background-color: #FFFFFF;border-radius:6px;}
.verify_sub { background-color: #000000; border: 0 none; color: #FFFFFF; font-size: 15px; left: 200px; padding: 7px 15px; position: relative; text-decoration: none; text-transform: uppercase; cursor: pointer; }
</style>