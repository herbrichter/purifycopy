<?php
	include_once("classes/ProfileDisplay.php");
?>
	<link rel="stylesheet" type="text/css" media="screen" href="includes/purify.css" />
	<!--<link rel="stylesheet" type="text/css" href="new_fancy_box/jquery.fancybox.css?v=2.1.4" media="screen" />-->
	<link href="sample.css" rel="stylesheet" type="text/css" />
	<style>
		#actualContent {padding: 20px;width: 375px;background-color: #FFFFFF;border-radius:6px;}
		#actualContent .fieldCont{width:auto;}
		#handles{ width:300px; position:relative; margin-left:38px; float:left; }
	</style>
	
	<script>
		$(document).ready(function(){
			$("#handles").submit(function(){
				var email = document.getElementById('user_name').value;
				var password = document.getElementById('user_pass').value;
				var atpos = email.indexOf("@");
				var dotpos = email.lastIndexOf(".");
				
				if(email=="" || email==null)
				{
					  alert("Please enter email ID.");
					  return false;
				}
				
				else if (atpos<1 || dotpos<atpos+2 || dotpos+2>=email.lenght || email=="" || email==null)
				{
					alert("Please enter a valid email address.");
					return false;
				}
				
				else if(document.getElementById('user_pass').type=='text')
				{
					alert("Please enter password.");
					 return false;
				}
				
				else if(password=="" || password==null)
				{
					 alert("Please enter password.");
					 return false;
				}
				
				$.ajax({
					url:"handle_chk_login.php",
					type:"POST",
					data:$(this).serializeArray(),
					success:function(data)
					{
						if(data=='no')
						{
							alert("Invalid email or password.");
						}
						else
						{
							alert("You are logged in successfully.");
							$.fancybox.close();
						}
					}
				});
				return false;
			});
			
			var pw1 = document.getElementById('user_pass').value;
			if(pw1=='Password')
			{
				document.getElementById('user_pass').type = 'text';
			}
			else
			{
				document.getElementById('user_pass').type = 'password';
			}
		});
		
		function myfunc_email(chng)
		{
			/* $("#password_fake_email").hide();
			$("#user_pass").show().focus(); */
			var pw1 = document.getElementById('user_pass').value;
			if(chng=="1_1")
			{
				if(pw1=='Password')
				{
					document.getElementById('user_pass').type = 'text';
				}
			}
			else if(chng=="1_0")
			{
				document.getElementById('user_pass').type = 'password';
			}
		}
	</script>
	
	<?php
		if(isset($_GET['datas']))
		{
			$match = $_GET['datas'];

			$display = new ProfileDisplay();
			$artist_check = $display->artistCheck($match);
			$community_check = $display->communityCheck($match);

			$community_event_check = $display->communityEventCheck($match);
			$community_project_check = $display->communityProjectCheck($match);

			$artist_event_check = $display->artistEventCheck($match);
			$artist_project_check = $display->artistProjectCheck($match);


			if($artist_check!=0)
			{
				$get_user_Info = $artist_check;
				$get_rel_id = $get_user_Info['artist_id'];
				$_GET['title'] = $get_user_Info['name'];
			}

			if($community_check!=0)
			{
				$get_user_Info = $community_check;
				$get_rel_id = $get_user_Info['community_id'];
				$_GET['title'] = $get_user_Info['name'];
			}

			if($artist_project_check!=0)
			{
				$get_user_Info = $display->eventrel_art_comm('artist',$artist_project_check['artist_id']);
				$get_rel_id = $artist_project_check['artist_id'].'_'.$artist_project_check['id'];
				$_GET['title'] = $artist_project_check['title'];
			}

			if($community_project_check!=0)
			{
				$get_user_Info = $display->eventrel_art_comm('community',$community_project_check['community_id']);
				$get_rel_id = $community_project_check['community_id'].'_'.$community_project_check['id'];
				$_GET['title'] = $community_project_check['title'];
			}
		}
	?>
	
	<div class="fancy_header">
	<?php
		if(isset($_GET['foll']))
		{
			echo "Follow ".$_GET['title'];
		}
		elseif(isset($_GET['fan']))
		{
			echo "Join ".$_GET['title']."'s Fan Club";
		}
		else
		{
			echo "Secure Log In";
		}
	?>
	</div>
	<div id="actualContent" >
		<?php
			if(isset($_GET['foll']))
			{
			?>
				<p style="float: left; font-size: 13px; padding-left:8px; line-height: 17px;">You must to be logged in to follow a profile. Log In below or <a href="registration_up.php" style="text-decoration: underline;">Sign Up.</a></p>
			<?php
			}
			elseif(isset($_GET['fan']))
			{
			?>
				<p style="float: left; font-size: 13px; padding-left:8px; line-height: 17px;">You must to be logged in to join a fan club. Log In below or <a href="registration_up.php" style="text-decoration: underline;">Sign Up.</a></p>
			<?php
			}
			else
			{
			?>
				<p style="float: left; font-size: 13px; padding-left:8px; margin: 10px 15px 30px 15px; line-height: 12px;">Don't have a Purify Art account? <a href="registration_up.php" style="text-decoration: underline;">Sign Up.</a></p>
			<?php
			}
		?>
		<form action="" id="handles" method="POST">
			<div class="logFieldCont">
				<input name="user_name" id="user_name" type="text" class="loginField" value="<?php if(isset($_COOKIE['cook_purifyname'])) { echo $_COOKIE['cook_purifyname']; } else { echo "Email"; } ?>" onfocus="if(this.value == 'Email'){this.value = '';}" onblur="if(this.value == ''){this.value='Email';}" style="width:250px;padding:10px;font-size:15px;" />
			</div>
			<div class="logFieldCont">
				<input name="password_fake_email" id="password_fake_email" onfocus="myfunc_email(0)" value="Password" type="text" class="loginField" style="width:250px;padding:10px;font-size:15px;display:none;" />
				
				<!--<input name="password_email" id="password_email" onblur="myfuncs_email()" style="display:none;width:250px;padding:10px;font-size:15px;"  type="password" class="loginField" />-->
				
				<input name="user_pass" id="user_pass" type="text" class="loginField" value="<?php if(isset($_COOKIE['cook_purifypassword'])) { echo $_COOKIE['cook_purifypassword']; } else { echo "Password"; } ?>" onfocus="if(this.value == 'Password'){this.value = '';} myfunc_email('1_0');" onblur="if(this.value == ''){this.value='Password';} myfunc_email('1_1');" style="width:250px;padding:10px;font-size:15px;"/>
			</div>
			<input type="submit" name="Submit_email" id="Submit_email" value="Login" class="loginButton" style="border-radius:5px;font-size:15px;padding:7px 15px;float:left; margin-left:10px; margin-top:25px;"/>
			<div style="margin-bottom: 18px;float:left; margin-top:29px; margin-left:10px;" class="chkfieldCont">
				<div style="height: 21px; float:left;" class="chekCont">
					<input type="checkbox" id="remember_me_email" name="remember_me_email" style=" z-index: 9; float: left; margin: 5px 8px 0px 0px;" class="chk">
					<div style="color: rgb(35, 31, 32); float: left; font-family: Arial,Helvetica,sans-serif; line-height: 25px; font-size: 13px;" class="chkTitle">Remember Me</div>
				</div>
			</div>
		</form>
		<div style="float:left;">
<?php
		include_once('fblog.php');
?>

	  </div>
	   <div style="float:left;">
       <?php
		include_once('google_login.php');
?>
       
       </div>
	</div>