<?php
	include_once('commons/session_check.php');
	include_once('classes/User.php');
	include_once('classes/Type.php');
	include_once('classes/SubType.php');
	include_once('classes/CommunityFeatures.php');
	include_once('classes/UserType.php');
	include_once('classes/UserImg.php');			
	
	session_start();
	$username = $_SESSION['username'];
	if(!$username == '')
	{
		$login_flag=1;
	}
	if($login_flag) include_once('loggedin_includes.php');
	else include_once('login_includes.php');
	
	$pagenum=$_GET['pagenum'];
	if (!(isset($pagenum)))
	{
		$pagenum = 1;
	} 
	$userObj = new User();
        $user = $userObj->getUserInfo($username);
        $uid = $user['user_id'];
        if(!($uid))
        {
            $uid = rand(0000, 9999);
        }
	$parent='Community';
	$objFeatures=new CommunityFeatures();
	$no_next=$objFeatures->countNextFeatures();

	switch($_GET['sel'])
	{
		case 'next':$prev_flag=1;
					$page_rows = 6;
					$last = ceil($no_next/$page_rows); 	
					if ($pagenum < 1)
					{
						$pagenum = 1;
					}
					elseif ($pagenum > $last)
					{
						$pagenum = $last;
					}

					$max = 'limit ' .($pagenum - 1) * $page_rows .',' .$page_rows;
					$rs3=$objFeatures->getNextFeatures($max);

					if ($pagenum == 1)
					{
						$prev_url=$_SERVER['PHP_SELF']; 
					}
					else
					{
						$prev_url=$_SERVER['PHP_SELF'].'?sel=next&&pagenum='.($pagenum-1);
					}

					if ($pagenum == $last)
					{
						$next_flag=0;
					}
					else
					{
						$next_flag=1;
					}
										
					$next_url=$_SERVER['PHP_SELF'].'?sel=next&&pagenum='.($pagenum+1);
					break;
					
		default: 	$prev_flag=0;
					if($no_next!=NULL)	$next_flag=1;
					$rs3=$objFeatures->getCurrentFeatures();
					$next_url=$_SERVER['PHP_SELF'].'?sel=next';
					break;
	}

	
	
?>

<?php include_once('includes/header.php'); ?>
<title>Purify Entertainment: Six Feature</title>
<script type="text/javascript" src="javascripts/swfobject.js"></script>
<script type="text/javascript" src="javascripts/pop-up.js"></script>
<!-- <script type="text/javascript">
	function MM_openBrWindow(uid) { //v2.0
	  window.open('mediaplayer_single.php?uid='+uid,"","toolbar=no,location=no,directories=no, status=no,menubar=no,scrollbars=no,resizable=no,copyhistory=no,width=425,height=150");
	}   
</script> -->
<script language="JavaScript">
	function MM_openBrWindow(uid) 
	{
		if (opener && !opener.closed)
		{
//			opener.focus();
			opener.close();
			var myWin =window.open('mediaplayer_single.php?uid='+uid,"","toolbar=no,location=no,directories=no, status=no,menubar=no,scrollbars=no,resizable=no,copyhistory=no,width=430,height=150");
			opener = myWin;
		}
		else 
		{
			var myWin = window.open('mediaplayer_single.php?uid='+uid,"","toolbar=no,location=no,directories=no, status=no,menubar=no,scrollbars=no,resizable=no,copyhistory=no,width=430,height=150");
			opener = myWin;
		}
	}
</script>
                               <script>

                                    function OPEN_Player(id,uid,type,name,path,profile_img,addplay,profileurl,username)
                                    {

                                        var playerwindow=window.open("player.php#id="+id+"&uid="+uid+"&type="+type+"&name="+name+"&path="+path+"&profileurl="+profileurl+"&username="+username+"&pro_img="+profile_img+"&addplay="+addplay, "Player","resizable=0,menubar=false,width=820px,height=350px,left=100,fullscreen =no,top=100,scrollbars=no,location=no");
                                        playerwindow.focus();
                                        return playerwindow;

                                    }

                               </script>
  <div id="contentContainer">
    <div id="subNavigationContainer">    
        <div id="subNavigation">
          <div id="sectionTitle">Community</div>
          <ul>
          <?php 
            $objType=new Type();
            $typers=$objType->listTypes($parent);
            while($typerow=mysql_fetch_assoc($typers))
            {	  
          ?>
                <li><a href="type.php?parent=<?php echo $parent; ?>&&id=<?php echo $typerow['type_id']; ?>"><?php echo $typerow['name']; ?></a></li>
          <?php 
            }
          ?>
          </ul>
        </div>
	</div>
    
    <div id="actualContent">
      <?php if(mysql_num_rows($rs3)==NULL)
	  		{
				echo "<p>Features Coming Soon... ";
				if(!$login_flag)
				{
					echo "To be Featured, <a href='register.php'>Sign Up!</a>";
				}
				echo "</p>";
				$prevnext=0;
			}
			else
			{
				$prevnext=1;
			}
	  ?>
      <div id="featuredHeads">
        <?php
			$rowflag=1;
			$posflag=1;
			while($row3=mysql_fetch_assoc($rs3))
			{	
				$user_id=$row3['category_id'];

				$objUserType=new UserType();
				$type_id=$objUserType->getTypeByUserId($user_id) ;
				$objType=new Type();
				$type=$objType->getType($type_id);
				$feature_type=$type['name'];
				$subtype_id=$objUserType->getSubTypeByType($user_id,$type_id);
				$objSubType=new SubType();
				$feature_subtype=$objSubType->getSubTypeById($subtype_id);				
				
				$objUser=new User();
				$row4=$objUser->getUserById($user_id);
				if($row4['homepage']) $homepage_flag=1;
				else $homepage_flag=0;

				//GET IMAGE WIDTH & HEIGHT
				list($width, $height, $type, $attr) = getimagesize($row4['img_small_path']);					
					
				if($posflag)
				{
		?>
      	<div class="featuredItemStackMid"><div id="featureHead"></div>
          <div class="featuredItemLeftMid">
            <div class="featuredContentMid_txt">
                          <h2><?php echo $feature_type; // strtolower($feature_subtype); ?></h2>
                          <h3><?php echo $row4['name'];?>
						  	 </h3>
                          <font face="Geneva, Arial, Helvetica, sans-serif" size="0" color="#626262" style="font-family: Geneva, Arial, Helvetica, sans-serif; font-size: 11px; color: #626262;">
						  <p>Where: 
							<?php 
                                echo $row4['city'];
                                if($row4['city'] && $row4['state_or_province']) echo ", ";
                                echo $row4['state_or_province'];
                            ?>
                          <br />
                            Type: <?php echo $feature_subtype; ?></font>
<?php 	/*		<br />			if($row4['upload_type']==0)
								  {
							?>
                            		ADD / <a href="<?php echo $row4['upload_path'] ?>" rel="lightbox" title="<?php echo $row4['upload_name'] ?> from <?php echo $row4['username'] ?>">VIEW</a>
                            <?php
								  }
								  else
								  {
								  	if(!$login_flag)
									{
							?>
                            			ADD / <a href="javascript:;" onClick="MM_openBrWindow('<?php echo $row4['user_id'] ?>');">PLAY</a> / <a href="#">DOWNLOAD</a>
                            <?php
                            		}
									else
									{
							?>
                            			<a href="javascript:;" onclick="openWindow('<?php echo $row4['user_id']?>');">ADD</a> / <a href="javascript:;" onclick="openWindow2('<?php echo $row4['user_id']?>');">PLAY</a>
                            <?php
									}
								  }
			*/				?>
                             <br /><?php if($homepage_flag) {?><a href="<?php echo $row4['homepage']; ?>" target="_blank">More Info</a><?php }else {?>More Info<?php } ?></p>
                        </div>
                        <div class="featuredPhoto"><img src="<?php echo $row4['img_small_path'] ?>" alt="<?php echo $row4['name']; ?>" width="<?php echo $width; ?>" height="<?php echo $height; ?>" />
							<div id="featured_buttons">
							<table width="95" border="0" cellpadding="0" cellspacing="0">
								<tr>
									<td>&nbsp;</td>
						  		
									<?php 
									if($row4['upload_type']==0)
									{?>
										<td width="27" id="vlightbox">
											<a class="vlightbox" href="
<?										echo $row4['upload_path']; ?>" title="<?php echo $row4['upload_name']; ?> from <?php echo $row4['username'] ; ?>"> <img src="images/play.gif" alt="Play" width="27" height="24" border="0" /></a>
<a id="vlb" href="http://visuallightbox.com"></a><script src="engine/js/visuallightbox.js" type="text/javascript"></script></td>


                                                                                <td width="35">
                                                                            <a href="#"><img src="images/add.gif" alt="Add" width="35" height="24" border="0" /></a>


                                                                        </td>
<?									}
									else
									{ ?>
									<td width="27">
<!--                                                                            <a href="
										
										javascript:;" onClick="MM_openBrWindow('<?php echo $row4['user_id'];?>'); 
										"><img src="images/play.gif" alt="Play" width="27" height="24" border="0" />
                                                                            </a>-->

                                                                                <a href="#"
                                                                                           onclick="OPEN_Player('<?php echo $row4['user_id']; ?>',
                                                                                                                '<?php echo $uid; ?>',
                                                                                                                '<?php echo $row4['upload_type']; ?>',
                                                                                                                '<?php echo $row4['upload_name']; ?>',
                                                                                                                '<?php echo $row4['upload_path']; ?>',
                                                                                                                '<?php echo $row4['img_path']; ?>',
                                                                                                                '<?php echo 'play'; ?>',
                                                                                                                '<?php echo $row4['homepage']; ?>',
                                                                                                                '<?php echo $row4['name']; ?>');"

                                                                                   <?php
                                                                                   if ($music_added_flag == 1) {
                                                                                       echo 'style="display:none"';
                                                                                   }
                                                                                   ?>>

                                                                                    <img  src="images/play.gif" alt="Play" width="35" height="24" border="0" />
                                                                                </a>
                                                                        </td>
						  		
									<td width="35">
<!--                                                                            <a href="#"><img src="images/add.gif" alt="Add" width="35" height="24" border="0" />
                                                                            </a>-->
                                                                            
                                                                            <a href="#"
                                                                                           onclick="OPEN_Player('<?php echo $row4['user_id']; ?>',
                                                                                                                '<?php echo $uid; ?>',
                                                                                                                '<?php echo $row4['upload_type']; ?>',
                                                                                                                '<?php echo $row4['upload_name']; ?>',
                                                                                                                '<?php echo $row4['upload_path']; ?>',
                                                                                                                '<?php echo $row4['img_path']; ?>',
                                                                                                                '<?php echo 'add'; ?>',
                                                                                                                '<?php echo $row4['homepage']; ?>',
                                                                                                                '<?php echo $row4['name']; ?>');"

                                                                                   <?php
                                                                                   if ($music_added_flag == 1) {
                                                                                       echo 'style="display:none"';
                                                                                   }
                                                                                   ?>>

                                                                                    <img  src="images/add.gif" alt="Add" width="35" height="24" border="0" />
                                                                                </a>
                                                                        </td>
						  	<?      } ?>	
							  		<td width="28"><a href="downloadFile.php?filename=<?php echo $row4['upload_path']; ?>"><img src="images/download.gif" alt="Download" width="28" height="24" border="0" /></a></td>
								</tr>
					  		</table>
						</div>
						</div>
						<div class="clearMe"></div>
                      </div>
        <?php
					$posflag=0;
				}
				else
				{
		?>
          <div class="featuredItemRightMid">
            <div class="featuredContentMid_txt">
                          <h2><?php echo $feature_type; // strtolower($feature_subtype); ?></h2>
                          <h3><?php echo $row4['name'];?>
						  	  </h3>
                          <font face="Geneva, Arial, Helvetica, sans-serif" size="0" color="#626262" style="font-family: Geneva, Arial, Helvetica, sans-serif; font-size: 11px; color: #626262;">
						  <p>Where: 
							<?php 
                                echo $row4['city'];
                                if($row4['city']) echo ", ";
                                echo $row4['state_or_province'];
                            ?>                          
                          <br />
                            Type: <?php echo $feature_subtype; ?></font>
<?php /*<br />					if($row4['upload_type']==0)
								  {
							?>
                            		ADD / <a href="<?php echo $row4['upload_path'] ?>" rel="lightbox" title="<?php echo $row4['upload_name'] ?> from <?php echo $row4['username'] ?>">VIEW</a>
                            <?php
								  }
								  else
								  {
								  	if(!$login_flag)
									{
							?>
                            			ADD / <a href="javascript:;" onClick="MM_openBrWindow('<?php echo $row4['user_id'] ?>');">PLAY</a> / <a href="#">DOWNLOAD</a>
                            <?php
                            		}
									else
									{
							?>
                            			<a href="javascript:;" onclick="openWindow('<?php echo $row4['user_id']?>');">ADD</a> / <a href="javascript:;" onclick="openWindow2('<?php echo $row4['user_id']?>');">PLAY</a>
                            <?php
									}
								  }
						*/	?>
                             <br /><?php if($homepage_flag) {?><a href="<?php echo $row4['homepage']; ?>" target="_blank">More Info</a><?php }else {?>More Info<?php } ?></p>
                        </div>
                        <div class="featuredPhoto"><img src="<?php echo $row4['img_small_path'] ?>" alt="<?php echo $row4['name']; ?>" width="<?php echo $width; ?>" height="<?php echo $height; ?>" />
							<div id="featured_buttons">
							<table width="95" border="0" cellpadding="0" cellspacing="0">
								<tr>
									<td>&nbsp;</td>
						  		
									<?php 
									if($row4['upload_type']==0)
									{?>
										<td width="27" id="vlightbox">
											<a class="vlightbox"
                                                                                           href="<?echo $row4['upload_path']; ?>"
                                                                                           title="<?php echo $row4['upload_name']; ?> from <?php echo $row4['username'] ; ?>">
                                                                                            <img src="images/play.gif" alt="Play" width="27" height="24" border="0" />
                                                                                        </a>
                                                                                        <a id="vlb" href="http://visuallightbox.com"></a>
                                                                                        <script src="engine/js/visuallightbox.js" type="text/javascript"></script>
                                                                                
                                                                                </td>

                                                                                <td width="35">
                                                                            <a href="#"><img src="images/add.gif" alt="Add" width="35" height="24" border="0" /></a>
                                                                                            

                                                                        </td>
<?									}
									else
									{ ?>
									<td width="27">
<!--                                                                            <a href="
										
										javascript:;" onClick="MM_openBrWindow('<?php echo $row4['user_id'];?>'); 
										"><img src="images/play.gif" alt="Play" width="27" height="24" border="0" /></a>
                                                                        -->
                                                                        <a href="#"
                                                                                           onclick="OPEN_Player('<?php echo $row4['user_id']; ?>',
                                                                                                                '<?php echo $uid; ?>',
                                                                                                                '<?php echo $row4['upload_type']; ?>',
                                                                                                                '<?php echo $row4['upload_name']; ?>',
                                                                                                                '<?php echo $row4['upload_path']; ?>',
                                                                                                                '<?php echo $row4['img_path']; ?>',
                                                                                                                 '<?php echo 'play'; ?>',
                                                                                                                '<?php echo $row4['homepage']; ?>',
                                                                                                                '<?php echo $row4['name']; ?>');"

                                                                                   <?php
                                                                                   if ($music_added_flag == 1) {
                                                                                       echo 'style="display:none"';
                                                                                   }
                                                                                   ?>>

                                                                                    <img  src="images/play.gif" alt="Play" width="35" height="24" border="0" />
                                                                                </a>

                                                                        </td>
			
						  		
									<td width="35">
<!--                                                                            <a href="#"><img src="images/add.gif" alt="Add" width="35" height="24" border="0" /></a>-->
                                                                                            <a href="#"
                                                                                           onclick="OPEN_Player('<?php echo $row4['user_id']; ?>',
                                                                                                                '<?php echo $uid; ?>',
                                                                                                                '<?php echo $row4['upload_type']; ?>',
                                                                                                                '<?php echo $row4['upload_name']; ?>',
                                                                                                                '<?php echo $row4['upload_path']; ?>',
                                                                                                                '<?php echo $row4['img_path']; ?>',
                                                                                                                '<?php echo 'add'; ?>',
                                                                                                                '<?php echo $row4['homepage']; ?>',
                                                                                                                '<?php echo $row4['name']; ?>');"

                                                                                   <?php
                                                                                   if ($music_added_flag == 1) {
                                                                                       echo 'style="display:none"';
                                                                                   }
                                                                                   ?>>

                                                                                    <img  src="images/add.gif" alt="Add" width="35" height="24" border="0" />
                                                                                </a>

                                                                        </td>
                                                                        			  	<?      } ?>	
							  		<td width="28"><a href="downloadFile.php?filename=<?php echo $row4['upload_path']; ?>"><img src="images/download.gif" alt="Download" width="28" height="24" border="0" /></a></td>
								</tr>
					  		</table>
							</div>
						</div>
                        <div class="clearMe"></div>
                      </div>
                      <div class="clearMe"></div>
                    </div>        
        <?php
					$posflag=1;
				}
			}
		?>
			<div class="clearMe">
        <?php 
			if($prevnext)
			{
		?>    
            <div class="nextBack">
				<?php if($prev_flag==1)
                      { 
                             echo "<a href='".$prev_url."'>&lt; Previous Features </a>";
                      }
                ?>
                <?php
                    if($next_flag==1)
                    {
                        echo "<a href='".$next_url."'>Next Features &gt;</a>";
                    }
                ?>
            </div>
        <?php
			}
		?>    
          </div>
        </div>
      </div>
    </div>
    
    <div class="clearMe"></div>
    
  </div>
  
<?php include_once('includes/footer.php'); ?>