<?php
session_start();
error_reporting(0);
include_once("commons/db.php");
include_once('classes/Commontabs.php');
include_once("classes/ProfileeditMedia.php");
include_once('classes/ProfileDisplay.php');
include_once('classes/Adds3details.php');
include_once('classes/user_keys.php');
$s3object = new Adds3details();
$user_keys_data = new user_keys();
$domainname=$_SERVER['SERVER_NAME'];
$root = realpath($_SERVER["DOCUMENT_ROOT"]);
if (!class_exists('S3')) require_once ($root.'/S3.php');
$s3 = new S3("","");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Purify Art: Media Profile</title>
<script src="includes/jquery.js"></script>

<?php
$newtab=new Commontabs();
$display = new ProfileDisplay();
include_once("header.php");

$new_profile_media_classobj=new ProfileeditMedia();
$my_media_ordered = array();

$getmedia_type=$new_profile_media_classobj->get_media_types();
if(isset($_GET['id']) && $_GET['id']>0)
{
	$delmedia=$new_profile_media_classobj->delete_media($_GET['id']);
	header("Location: profileedit_media.php?delete=done");
}
if(isset($_GET['sel_val']) && !isset($_GET['disp_val']))
{
	if($_GET['sel_val']=='113' || $_GET['sel_val']=='116' || $_GET['sel_val']=='114' || $_GET['sel_val']=='115')
	{
		if(isset($_GET['disp_val']))
		{
			if($_GET['disp_type']== "type"){$type = "ty.name";}
			if($_GET['disp_type']== "creator"){$type = "GM.creator";}
			if($_GET['disp_type']== "from"){$type = "GM.from";}
			if($_GET['disp_type']== "title"){$type = "GM.title";}
			$getmedia=$new_profile_media_classobj->sel_all_media_by_category_order($_GET['sel_val'],$type,$_GET['disp_val']);
		}
		else
		{
			$getmedia=$new_profile_media_classobj->sel_all_media_by_category($_GET['sel_val']);
		}
		
		//$getmedia_name=$new_profile_media_classobj->get_media_type_name($_GET['sel_val']);
	}
	else
	{
		$getmedia=$new_profile_media_classobj->sel_all_media();
	}
}
else if(isset($_GET['sel_val']) && isset($_GET['disp_val']))
{
	if($_GET['sel_val']=='113' || $_GET['sel_val']=='116' || $_GET['sel_val']=='114' || $_GET['sel_val']=='115')
	{
		$getmedia=$new_profile_media_classobj->sel_all_media_by_category2($_GET['sel_val']);
		if($getmedia!="" && $getmedia!=NULL)
		{
			if(mysql_num_rows($getmedia)>0)
			{
				while($resme = mysql_fetch_assoc($getmedia))
				{
					$resme['own_tag'] = "own";
					$newarray_sel[] = $resme;
				}
			}
		}
		$getgeneral=$new_profile_media_classobj->sel_general();
		$exp_media_id =explode(',',$getgeneral['accepted_media_id']);
		$get_tagged_media_ss = array();
		for($i=0;$i<count($exp_media_id);$i++)
		{
			if($exp_media_id[$i]==""){continue;}
			else{
				$get_tagged_media=$new_profile_media_classobj->sel_all_tagged_media_ordered($exp_media_id[$i]);
				if($get_tagged_media!="" && $get_tagged_media!=NULL)
				{
					if(mysql_num_rows($get_tagged_media)>0)
					{
						$res1=mysql_fetch_assoc($get_tagged_media);
						$res1['own_tag'] = "tag";
						$get_tagged_media_sele[] = $res1;
					}
				}
			}
		}
		$newarray = array_merge($get_tagged_media_sele,$newarray_sel);
		
		foreach ($newarray as $val)
		{
			$value = $_GET['disp_type'];
			if($value == "type"){$value ="name";}
			 $sortArraynamesel[] = $val[$value];
		}
		if($_GET['disp_val']=="DESC"){
	    array_multisort($sortArraynamesel, SORT_DESC , $newarray);
		}if($_GET['disp_val']=="ASC"){
			array_multisort($sortArraynamesel, $newarray);
		}
		
	}
	else
	{
		$getmedia=$new_profile_media_classobj->sel_all_media();
	}
}
else if(isset($_GET['disp_val']) || isset($_GET['sel_val']))
{
	if($_GET['disp_type']== "type"){$type = "ty.name";}
	if($_GET['disp_type']== "creator"){$type = "GM.creator";}
	if($_GET['disp_type']== "from"){$type = "GM.from";}
	if($_GET['disp_type']== "title"){$type = "GM.title";}
	$getmedia=$new_profile_media_classobj->sel_all_media();
	if($getmedia!="" && $getmedia!=NULL)
	{
		if(mysql_num_rows($getmedia)>0)
		{
			while($res_media = mysql_fetch_assoc($getmedia))
			{
				$res_media['own_tag'] = "own";
				$my_media_ordered[] = $res_media;
			}
		}
	}
	$getmedia_regis=$new_profile_media_classobj->get_registration_media();
	$getmedia_regis_com=$new_profile_media_classobj->get_registration_media_community();
	$getgeneral=$new_profile_media_classobj->sel_general();
	$exp_media_id =explode(',',$getgeneral['accepted_media_id']);
	$get_tagged_media_ss = array();
	for($i=0;$i<count($exp_media_id);$i++)
	{
		if($exp_media_id[$i]==""){continue;}
		else{
			$get_tagged_media=$new_profile_media_classobj->sel_all_tagged_media_ordered($exp_media_id[$i]);
			if($get_tagged_media!="" && $get_tagged_media!=NULL)
			{
				if(mysql_num_rows($get_tagged_media)>0)
				{
					$res1=mysql_fetch_assoc($get_tagged_media);
					$res1['own_tag'] = "tag";
					$get_tagged_media_ss[] = $res1;
				}
			}
		}
	}
	$all_sorting_media = array_merge($get_tagged_media_ss,$my_media_ordered);
	$sortArrayname = array();
	
	if(!empty($all_sorting_media))
	{
		foreach ($all_sorting_media as $val)
		{
			$value = $_GET['disp_type'];
			if($value == "type"){$value ="name";}
			 $sortArrayname[] = $val[$value];
		}
	}
	if($_GET['disp_val']=="DESC"){
		if(!empty($sortArrayname))
		{
			array_multisort($sortArrayname, SORT_DESC , $all_sorting_media);
		}
	}if($_GET['disp_val']=="ASC"){
		if(!empty($sortArrayname))
		{
			array_multisort($sortArrayname, $all_sorting_media);
		}
	}
	$getmedia=$new_profile_media_classobj->sel_all_media();
}
else
{
	$getmedia=$new_profile_media_classobj->sel_all_media();
	$getmedia_regis=$new_profile_media_classobj->get_registration_media();
	$getmedia_regis_com=$new_profile_media_classobj->get_registration_media_community();
}
?>
<div id="loaders" style="text-align:center;width:941px;">
	<img src="images/ajax_loader_large.gif" style="height: 35px;
    position: relative;" />
  </div>
<div id="outerContainer" style="display:none;">
          <!--<p><a href="logout.php">Logout</a></p>
      </div>
    </div>
  </div>
</div>-->


<link rel="stylesheet" type="text/css" media="screen" href="includes/purify.css" />
<!--<script src='http://ajax.googleapis.com/ajax/libs/jquery/1.6.1/jquery.min.js'></script>-->
<script type="text/javascript" src="javascripts/jquery.js"></script>
<script src="includes/mediaorganictabs-jquery.js"></script>
<script>
	$(function() {

		$("#mediaTab").organicTabs();
	});
</script>
<script type="text/javascript">
function confirmdelete(s,t)
{
	var x;
	if(t == "Songs"){t="song";}
	if(t == "Images"){t="gallery";}
	if(t == "Videos"){t="video";}
	if(t == "Playlist"){t="playlist";}
	var name=confirm('Are you sure you want to delete the '+t+' "'+s+'"?');
	if (name==false)
	{
		return false;
	}else{
		save_cookie();
	}
}

function save_cookie(){
	var position;
	<?php
	if(isset($_GET['sel_val']) && $_GET['sel_val']!=""){
	?>
	position = document.getElementById('mediaContentscrol').scrollTop;
	<?php
	}else{
	?>
	position = document.getElementById('scroll_act_one').scrollTop;
	<?php
	}
	?>
	document.cookie="scroll_position=" + position;
}
</script>
<script>
var ContentHeight = 100;
var TimeToSlide = 150.0;

var openAccordion = '';

function runAccordion(index)
{
  var nID = "Accordion" + index + "Content";
  if(openAccordion == nID)
    nID = '';
    
  setTimeout("animate(" + new Date().getTime() + "," + TimeToSlide + ",'" + openAccordion + "','" + nID + "')", 33);
  
  openAccordion = nID;
}

function animate(lastTick, timeLeft, closingId, openingId)
{  
  var curTick = new Date().getTime();
  var elapsedTicks = curTick - lastTick;
  
  var opening = (openingId == '') ? null : document.getElementById(openingId);
  var closing = (closingId == '') ? null : document.getElementById(closingId);
 
  if(timeLeft <= elapsedTicks)
  {
    if(opening != null){
		opening.style.display = 'block';
		opening.style.height = ContentHeight + 'px';
    }
    if(closing != null)
    {
      closing.style.display = 'none';
      closing.style.height = '0px';
    }
    return;
  }
 
  timeLeft -= elapsedTicks;
  var newClosedHeight = Math.round((timeLeft/TimeToSlide) * ContentHeight);

  if(opening != null)
  {
    if(opening.style.display != 'block')
      opening.style.display = 'block';
    opening.style.height = (ContentHeight - newClosedHeight) + 'px';
  }
  
  if(closing != null)
    closing.style.height = newClosedHeight + 'px';

  setTimeout("animate(" + curTick + "," + timeLeft +",'" + closingId + "','" + openingId + "')", 33);
}

</script>
<script type="text/javascript">
function selectmedia()
{
	if($("#media_type").val() =="all media")
	{
		window.location.href="profileedit_media.php";
	}else{
	window.location.href="profileedit_media.php?sel_val="+$("#media_type").val();
	//alert($("#media_type").val());	
	}
}
</script>
  
  <div id="toplevelNav"></div>
  <div>
      <div id="profileTabs">
          <ul>
            <?php
			$fan_var = 0;
		   //$check=new AddContact();
		   $new_artist=$newtab->artist_status();
		   $new_community=$newtab->community_status();
		   $new_team=$newtab->team_status();
		   
		   $sql_fan_chk = mysql_query("SELECT * FROM fan_club_membership WHERE email='".$_SESSION['login_email']."' AND accept=1");
		   if($sql_fan_chk!="" && $sql_fan_chk!=NULL)
		   {
			   if(mysql_num_rows($sql_fan_chk)>0)
			   {
					while($row_fan_chk = mysql_fetch_assoc($sql_fan_chk))
					{
						$fan_var = $fan_var + 1;
					}
				}
			}
		   $gen_det = mysql_query("SELECT * FROM general_user WHERE email='".$_SESSION['login_email']."'");
			if($gen_det!="" && $gen_det!=NULL)
			{
			   if(mysql_num_rows($gen_det)>0){
				$res_gen_det = mysql_fetch_assoc($gen_det);
			   }
			}
		   $date = date("Y-m-d");
			$sql_member_chk = mysql_query("SELECT * FROM purify_membership WHERE general_user_id='".$res_gen_det['general_user_id']."' AND (expiry_date>'".$date."' OR lifetime=1)");
			if($sql_member_chk!="" && $sql_member_chk!=NULL)
			{
				if(mysql_num_rows($sql_member_chk)>0)
				{
					$member_var = $member_var + 1;
				}
			}
		   
		  //print_r($res1);
		  //echo $res1['artist_id'];
		  if(($newres1['artist_id']==0 && $new_artist['status']==1) && $newres1['member_id']==0 && ($newres1['community_id']==0 && $new_community['status']==1) && ($newres1['team_id']==0 && $new_team['status']==1) && $fan_var==0 && $avail_meds_tabs_chk==0)
		  {		  
		  ?>
            <li class="active">HOME</li>		
            <!--<li><a href="profileedit_support.php">SUPPORT</a></li>-->
		   <?php
		   }
		   elseif(($newres1['artist_id']==0 && ($new_artist['status']!=0 ||  $new_artist['status']=="") && $new_artist['active']==0) && ($newres1['community_id']==0 && ($new_community['status']!=0 || $new_community['status']=="")) && ($newres1['team_id']==0 && ($new_team['status']!=0 || $new_team['status']=="")) && $fan_var!=0)
		   {
		   ?>
			<li><a href="profileedit.php">HOME</a></li>
			<li class="active">Media</li>
            <!--<li><a href="profileedit_support.php">SUPPORT</a></li>-->
		   <?php
		   }
		    elseif(($newres1['artist_id']==0 && ($new_artist['status']!=0 ||  $new_artist['status']=="")) && ($newres1['community_id']==0 && ($new_community['status']!=0 || $new_community['status']=="")) && ($newres1['team_id']==0 && ($new_team['status']!=0 || $new_team['status']=="")) && $member_var!=0)
		   {
		   ?>
			<li><a href="profileedit.php">HOME</a></li>
			<li class="active">Media</li>
           <!--<li><a href="profileedit_support.php">SUPPORT</a></li>-->
		   <?php
		   }
		    elseif(($newres1['artist_id']==0 && ($new_artist['status']!=0 ||  $new_artist['status']=="")) && ($newres1['community_id']==0 && ($new_community['status']!=0 || $new_community['status']=="")) && ($newres1['team_id']==0 && ($new_team['status']!=0 || $new_team['status']=="")) && $avail_meds_tabs_chk!=0)
		   {
		   ?>
			<li><a href="profileedit.php">HOME</a></li>
			<li class="active">Media</li>
            <!--<li><a href="profileedit_support.php">SUPPORT</a></li>-->
		   <?php
		   }
		   elseif(($newres1['artist_id']!=0 && $new_artist['status']==0 && $new_artist['active']==0) && ($newres1['community_id']!=0 && $new_community['status']==0 && $new_community['active']==0) && ($newres1['team_id']!=0 && $new_team['status']==0))
		   {
		   ?>
		   <li><a href="profileedit.php">HOME</a></li>	
		   <li class="active">Media</li>
		   <li><a href="profileedit_artist.php">ARTIST</a></li>
		   <!--<li><a href="profileedit_community.php">COMPANY</a></li>-->
		   <li><a href="profileedit_community.php">COMPANY</a></li>
		   <li><a href="profileedit_team.php">TEAM</a></li>
          <!--<li><a href="profileedit_support.php">SUPPORT</a></li>-->
		   
		   <?php
		   }
		  elseif(($newres1['artist_id']!=0 && $new_artist['status']==0 && $new_artist['active']==0) && ($newres1['community_id']!=0 && $new_community['status']==0 && $new_community['active']==0))
		   {
		   ?>
		   <li><a href="profileedit.php">HOME</a></li>	
		   <li class="active">Media</li>		   
		   <li><a href="profileedit_artist.php">ARTIST</a></li>
		   <!--<li><a href="profileedit_community.php">COMPANY</a></li>-->
		   <li><a href="profileedit_community.php">COMPANY</a></li>
           <!--<li><a href="profileedit_support.php">SUPPORT</a></li>-->
		   <?php
		   }
		   elseif(($newres1['artist_id']!=0 && $new_artist['status']==0 && $new_artist['active']==0) && ($newres1['team_id']!=0 && $new_team['status']==0))
		   {
		   ?>
			<li><a href="profileedit.php">HOME</a></li>	
			<li class="active">Media</li>
		    <li><a href="profileedit_artist.php">ARTIST</a></li>
			<li><a href="profileedit_team.php">TEAM</a></li>
           <!--<li><a href="profileedit_support.php">SUPPORT</a></li>-->
		   <?php
		   }
		   elseif(($newres1['community_id']!=0 && $new_community['status']==0 && $new_community['active']==0) && ($newres1['team_id']!=0 && $new_team['status']==0))
		   {
		   ?>
		   <li><a href="profileedit.php">HOME</a></li>	
		   <li class="active">Media</li>
		   <!--<li><a href="profileedit_community.php">COMPANY</a></li>-->
		   <li><a href="profileedit_community.php">COMPANY</a></li>
		   <li><a href="profileedit_team.php">TEAM</a></li>
           <!--<li><a href="profileedit_support.php">SUPPORT</a></li>-->
		   <?php
		   }
		  elseif($newres1['artist_id']!=0 && $new_artist['status']==0 && $new_artist['active']==0)
		   {?>
		    <li><a href="profileedit.php">HOME</a></li>	
		    <li class="active">Media</li>	
			<li><a href="profileedit_artist.php">ARTIST</a></li>
            <!--<li><a href="profileedit_support.php">SUPPORT</a></li>-->
		   <?php
		   }
		   elseif($newres1['member_id']!=0)
		   {
		   ?>
		   <?php
		   }
		  elseif($newres1['community_id']!=0 && $new_community['status']==0 && $new_community['active']==0)
		   {
		   ?>
		    <li><a href="profileedit.php">HOME</a></li>	
		    <li class="active">Media</li>
			<!--<li>COMPANY</a></li>-->
			<li><a href="profileedit_community.php">COMPANY</a></li>
            <!--<li><a href="profileedit_support.php">SUPPORT</a></li>-->
		   <?php
		   }
		   elseif($newres1['team_id']!=0 && $new_team['status']==0)
		   {
		   ?>
		    <li><a href="profileedit.php">HOME</a></li>	
		    <li class="active">Media</li>
			<li><a href="profileedit_team.php">TEAM</a></li>
            <!--<li><a href="profileedit_support.php">SUPPORT</a></li>-->
`		   <?php
		   }
		   else
		   {
		   ?>
		    <li class="active">HOME</li>		
            <!--<li><a href="profileedit_support.php">SUPPORT</a></li>-->
		   <?php
		   }
			?>
          </ul>
      </div>
   <!--<script>
		!window.jQuery && document.write('<script src="./fancybox/jquery-1.4.3.min.js"><\/script>');
	</script>
	<script type="text/javascript" src="./fancybox/jquery.fancybox-1.3.4.pack.js"></script>
<!--	<link rel="stylesheet" type="text/css" href="./fancybox/jquery.fancybox-1.3.4.css" media="screen" />
Added files for displaying fancy box 2-->
		<script type="text/javascript" src="./new_fancy_box/jquery.fancybox.js?v=2.1.4"></script>
		<link rel="stylesheet" type="text/css" href="./new_fancy_box/jquery.fancybox.css?v=2.1.4" media="screen" />
		<script type="text/javascript" src="./new_fancy_box/helpers/jquery.fancybox-media.js?v=1.0.5"></script>
	<!--Added files for displaying fancy box 2 Ends here-->
   
	<script type="text/javascript">             
	$(document).ready(function() {
				$("#artist_gallery_images").fancybox({
				'autoScale'			: true,
				'transitionIn'		: 'none',
				'transitionOut'		: 'none',
				'type'				: 'iframe'
			
			});

			
		});
		$(document).ready(function() {
				$("#Add_artist_gallery_images").fancybox({
				'autoScale'			: true,
				'transitionIn'		: 'none',
				'transitionOut'		: 'none',
				'type'				: 'iframe'
			
			});

			
		});
		$(document).ready(function() {
				$("#Add_community_gallery_images").fancybox({
				'autoScale'			: true,
				'transitionIn'		: 'none',
				'transitionOut'		: 'none',
				'type'				: 'iframe'
			
			});

			
		});
		$(document).ready(function() {
				$("#community_gallery_images").fancybox({
				'autoScale'			: true,
				'transitionIn'		: 'none',
				'transitionOut'		: 'none',
				'type'				: 'iframe'
			
			});

			
		});
		</script>
	
          <div id="mediaTab">
            <div id="subNavigation" class="tabs">
              <ul class="nav">
				
                <li><a href="#library" class="current">Media Library</a></li>
				<li><a href="#s3account">S3 Account</a></li>
                <!--<li><a href="#storage">Storage</a></li>-->
                <!--<li><a href="#s3">S3 Library</a></li>-->
				<?php
				if(($newres1['artist_id']!=0 && $new_artist['status']==0 && $new_artist['active']==0) || ($newres1['community_id']!=0 && $new_community['status']==0 && $new_community['active']==0))
				{
				?>
				<a href="tagged_user.php"><li>Tagged Media</li></a>
				<?php
				}
				?>
              </ul>
            </div>
            <div class="list-wrap">
				<div class="subTabs" id="s3account" style="display:none;">
					<h1>Add S3 Account</h1>
					<div style="width:665px; float:left;">
						<div id="actualContent">
							<?php
							$get_keys = $s3object -> get_s3_account_details($_SESSION['login_id']);
							?>
							<form method="post" action="adds3account_details.php" onsubmit="return validate_amazon_account()">
								<div class="fieldCont">
									<div class="fieldTitle">*Name</div>
									<input name="username" type="text" class="fieldText" id="username" <?php if(!empty($get_keys) && !empty($get_keys['username'])){ echo "disabled"; }?> value="<?php if(!empty($get_keys) && !empty($get_keys['username'])){ echo $get_keys['username']; }?>" />
								</div>
								<div class="fieldCont">
									<div class="fieldTitle">*Secret Key</div>
									<input name="secret_key" type="text" class="fieldText" id="secret_key" <?php if(!empty($get_keys) && !empty($get_keys['username'])){ echo "disabled"; }?> value="<?php if(!empty($get_keys) && !empty($get_keys['clientsecret'])){ echo $get_keys['clientsecret']; }?>"/>
								</div>
								<div class="fieldCont">
									<div class="fieldTitle">*Access Key</div>
									<input name="access_key" type="text" class="fieldText" id="access_key" <?php if(!empty($get_keys) && !empty($get_keys['username'])){ echo "disabled"; }?> value="<?php if(!empty($get_keys) && !empty($get_keys['clientid'])){ echo $get_keys['clientid']; }?>"/>
								</div>
								<div class="fieldCont">
									<div class="fieldTitle"></div>
									<!--<input type="button" class="register" style="border-radius:5px; -webkit-border-radius:5px; -moz-border-radius:5px; -ms-border-radius:5px; -o-border-radius:5px;" value="<?php// if(!empty($get_keys) && !empty($get_keys['clientsecret'])){ echo "Synced"; }else { echo "Not Synced"; }?>" />-->
									<?php if(!empty($get_keys) && !empty($get_keys['clientsecret'])){ 
									?>
										<a href="adds3account_details.php?removes3=yes" onclick="show_loader();" style="border-radius:5px; -webkit-border-radius:5px; -moz-border-radius:5px; -ms-border-radius:5px; -o-border-radius:5px;background: none repeat scroll 0 0 #000;border-radius: 5px;color: #ffffff;font-family: 'futura-pt',sans-serif;font-size: 0.825em;font-weight: bold;text-decoration: none;position:relative;padding:10px;top:6px;">Unsync</a>
									<?php
									}else { 
									?>
									<input type="submit" class="register" style="border-radius:5px; -webkit-border-radius:5px; -moz-border-radius:5px; -ms-border-radius:5px; -o-border-radius:5px;" value="Save" />
									<?php
									}?>
								</div>
							</form>
						</div>
					</div>
				</div>
				<div class="subTabs" id="library" style="display:none;">
				<div>
					<h1>Media Library</h1>
					<div id="mediaContent">
						<div class="topLinks">
							<div class="links" style="float:left;">
								<ul>
									<?php
									if(($newres1['artist_id']==0 && ($new_artist['status']!=0 ||  $new_artist['status']=="")) && ($newres1['community_id']==0 && ($new_community['status']!=0 || $new_community['status']=="")) && ($newres1['team_id']==0 && ($new_team['status']!=0 || $new_team['status']=="")) && $member_var!=0)
									{
									?>
										<a title="Add media to your library to be shared, sold, or for your private use." href="add_media.php" style="background-color: #000; border: 0 none; color: #FFFFFF; font-family: 'futura-pt',sans-serif; font-size: 0.825em; font-weight: bold; padding: 6px 10px;text-decoration:none; border-radius:5px; -webkit-border-radius:5px; -moz-border-radius:5px; -ms-border-radius:5px; -o-border-radius:5px;">Add Media</a> |
										<a title="Add media to your library to be shared, sold, or for your private use." href="add_media.php?adds=116" style="background-color: #000; border: 0 none; color: #FFFFFF; font-family: 'futura-pt',sans-serif; font-size: 0.825em; font-weight: bold; padding: 6px 10px;text-decoration:none; border-radius:5px; -webkit-border-radius:5px; -moz-border-radius:5px; -ms-border-radius:5px; -o-border-radius:5px;">Add Playlist</a> |
									<?php
									}
									if(($newres1['artist_id']!=0 && $new_artist['status']==0) || ($newres1['community_id']!=0 && $new_community['status']==0) || ($newres1['team_id']!=0 && $new_team['status']==0) && $member_var==0)
									{
									?>
										<a title="Add media to your library to be shared, sold, or for your private use." href="add_media.php" style="background-color: #000; border: 0 none; color: #FFFFFF; font-family: 'futura-pt',sans-serif; font-size: 0.825em; font-weight: bold; padding: 6px 10px;text-decoration:none; border-radius:5px; -webkit-border-radius:5px; -moz-border-radius:5px; -ms-border-radius:5px; -o-border-radius:5px;">Add Media</a> |
										<a title="Add media to your library to be shared, sold, or for your private use." href="add_media.php?adds=116" style="background-color: #000; border: 0 none; color: #FFFFFF; font-family: 'futura-pt',sans-serif; font-size: 0.825em; font-weight: bold; padding: 6px 10px;text-decoration:none; border-radius:5px; -webkit-border-radius:5px; -moz-border-radius:5px; -ms-border-radius:5px; -o-border-radius:5px;">Add Playlist</a> |
									<?php
									}
									?>
								</ul>
								
								<select title="View your media by type." class="dropdown" id="media_type" onChange="selectmedia()">
									<option value="all media" >All Media</option>	
									<?php
									if($getmedia_type!="" && $getmedia_type!=NULL)
									{
										while($row=mysql_fetch_assoc($getmedia_type))
										{
											if(isset($_GET['sel_val']) && $_GET['sel_val']==$row['type_id'])
											{
												if($row['type_id']!=117)
												{
										?>
												<option selected value="<?php echo $row['type_id'];?>" ><?php echo $row['name'];?></option>	
										<?php
												}
											}
											else
											{
												if($row['type_id']!=117)
												{
										?>
												<option value="<?php echo $row['type_id'];?>" ><?php echo $row['name'];?></option>	
										<?php
												}
											}
										}
									}
									?>
									<!--<option value="all media" <?php //if(isset($_GET['sel_val']) && $_GET['sel_val']== "all media"){echo "selected";}?> >All Media</option>
									<option value="gallery" <?php //if(isset($_GET['sel_val']) && $_GET['sel_val']== "gallery"){echo "selected";}?>>Galleries</option>
									<option value="song" <?php //if(isset($_GET['sel_val']) && $_GET['sel_val']== "song"){echo "selected";}?>>Songs</option>
									<option value="video" <?php// if(isset($_GET['sel_val']) && $_GET['sel_val']== "video"){echo "selected";}?>>Videos</option>
									<option value="channel" <?php //if(isset($_GET['sel_val']) && $_GET['sel_val']== "channel"){echo "selected";}?>>Channels</option>-->
								</select>
								<ul style="left: 386px;position: relative;">
									<!--<li><a href="profileedit_media.php?sel_val=113&edit_gal=1">Edit Galleries</a></li>-->
								</ul>
							</div>
							<?php
							$get_stored_size = $new_profile_media_classobj->get_media_storage();
							if(($newres1['artist_id']!=0 && $new_artist['status']==0 && $new_artist['active']==0) || ($newres1['community_id']!=0 && $new_community['status']==0 && $new_community['active']==0))
							{
								if(!empty($get_stored_size) && $get_stored_size=="s3integrated"){
								?>
								<div class="spaceremian">Storage Space: Unlimited</div>
								<?php
								}else if(!empty($get_stored_size) && $get_stored_size=="adminset"){
								?>
								<div class="spaceremian">Storage Space: Unlimited</div>
								<?php
								}else{
									$get_val = strpos($get_stored_size,".");
									$get_val = $get_val + 2 ;
									?>
									<div class="spaceremian">Storage Space: Using <?php echo substr($get_stored_size,0,$get_val);?>MB of your 100MB</div>
									<?php
								}
							}
							?>
						</div>
					</div>
				</div>
					<div style="width:665px; float:left;">
					<?php
					if(isset($_GET['delete']) && $_GET['delete']=="done")
					{
					?>
						<p style="color:red;"> Your Data Is Successfully Deleted. </p>
					<?php
					}
					?>
					
					<div id="mediaContent" style="width:685px; overflow:auto;">
						<div class="titleCont">
							<div class="blkD" style="width:60px;"><a href="<?php
							if(isset($_GET['sel_val']) && !isset($_GET['disp_val']))
							{
								echo $_SERVER['REQUEST_URI']."&disp_val=ASC&disp_type=type";
							}
							else if(isset($_GET['sel_val']) && isset($_GET['disp_val']))
							{
								if($_GET['disp_val'] == "ASC"){$show = "DESC";}else{$show = "ASC";}
								$exp = explode("&",$_SERVER['REQUEST_URI']);
								echo $exp[0]."&disp_val=$show&disp_type=type";
							}
							else if(isset($_GET['disp_val']) && !isset($_GET['sel_val']))
							{
								if($_GET['disp_val'] == "ASC"){$show = "DESC";}else{$show = "ASC";}
								echo "profileedit_media.php?disp_val=$show&disp_type=type";
							}
							else
							{
								echo "profileedit_media.php?disp_val=ASC&disp_type=type";
							}
							?>">Type</a></div>
							<div class="blkI" style="width:127px;"><a title="The user who is designated the creator of the media." href="<?php
							if(isset($_GET['sel_val']) && !isset($_GET['disp_val']))
							{
								echo $_SERVER['REQUEST_URI']."&disp_val=ASC&disp_type=creator";
							}
							else if(isset($_GET['sel_val']) && isset($_GET['disp_val']))
							{
								if($_GET['disp_val'] == "ASC"){$show = "DESC";}else{$show = "ASC";}
								$exp = explode("&",$_SERVER['REQUEST_URI']);
								echo $exp[0]."&disp_val=$show&disp_type=creator";
							}
							else if(isset($_GET['disp_val']) && !isset($_GET['sel_val']))
							{
								if($_GET['disp_val'] == "ASC"){$show = "DESC";}else{$show = "ASC";}
								echo "profileedit_media.php?disp_val=$show&disp_type=creator";
							}
							else
							{
								echo "profileedit_media.php?disp_val=ASC&disp_type=creator";
							}
							?>">Creator</a></div>
							<?php
							if(!isset($_GET['edit_gal']))
							{
							?>
							<div class="blkB" style="width:145px;"><a title="The project or event that the media is from." href="<?php
							if(isset($_GET['sel_val']) && !isset($_GET['disp_val']))
							{
								echo $_SERVER['REQUEST_URI']."&disp_val=ASC&disp_type=from";
							}
							else if(isset($_GET['sel_val']) && isset($_GET['disp_val']))
							{
								if($_GET['disp_val'] == "ASC"){$show = "DESC";}else{$show = "ASC";}
								$exp = explode("&",$_SERVER['REQUEST_URI']);
								echo $exp[0]."&disp_val=$show&disp_type=from";
							}
							else if(isset($_GET['disp_val']) && !isset($_GET['sel_val']))
							{
								if($_GET['disp_val'] == "ASC"){$show = "DESC";}else{$show = "ASC";}
								echo "profileedit_media.php?disp_val=$show&disp_type=from";
							}
							else
							{
								echo "profileedit_media.php?disp_val=ASC&disp_type=from";
							}
							?>">From</a></div>
							<div class="blkB"><a href="<?php
							if(isset($_GET['sel_val']) && !isset($_GET['disp_val']))
							{
								echo $_SERVER['REQUEST_URI']."&disp_val=ASC&disp_type=title";
							}
							else if(isset($_GET['sel_val']) && isset($_GET['disp_val']))
							{
								if($_GET['disp_val'] == "ASC"){$show = "DESC";}else{$show = "ASC";}
								$exp = explode("&",$_SERVER['REQUEST_URI']);
								echo $exp[0]."&disp_val=$show&disp_type=title";
							}
							else if(isset($_GET['disp_val']) && !isset($_GET['sel_val']))
							{
								if($_GET['disp_val'] == "ASC"){$show = "DESC";}else{$show = "ASC";}
								echo "profileedit_media.php?disp_val=$show&disp_type=title";
							}
							else
							{
								echo "profileedit_media.php?disp_val=ASC&disp_type=title";
							}
							?>">Title</a></div>
							<?php
							}
							?>
						</div>
					</div>
					<?php
					if(!isset($_GET['disp_val']) && !isset($_GET['sel_val']))
					{
					?>
					<div id="scroll_act_one" style="width:685px; height:400px; overflow:auto; float:right;">
						<div id="mediaContent" class="scroll_act_one" >
							
							<?php
							$my_media_arr = array();
							if(!isset($_GET['edit_gal']))
							{
								$k=1;
								if($getmedia!="" && $getmedia!=NULL)
								{
									while($res=mysql_fetch_assoc($getmedia))
									{
										if($res['media_type'] == 114)
										{
											$get_media_track = $new_profile_media_classobj->get_media_track($res['id']);
												$res["track"] = $get_media_track['track'];
										}
									
										$res["media_from"] = "my_media";
										$my_media_arr[] = $res;
									}
								}
								
							?>
                          <?php
						  	$getgeneral=$new_profile_media_classobj->sel_general();
							$exp_media_id =explode(',',$getgeneral['accepted_media_id']);
							$new_tag_media_arr = array();
							for($i=0;$i<count($exp_media_id);$i++)
							{
								$get_tagged_media=$new_profile_media_classobj->sel_all_tagged_media($exp_media_id[$i]);
								
								if($get_tagged_media!="" && $get_tagged_media!=NULL)
								{
									while($res1=mysql_fetch_assoc($get_tagged_media))
									{
										if($res1['media_type'] == 114)
										{
											$get_media_track = $new_profile_media_classobj->get_media_track($res1['id']); 
											$res1["track"] = $get_media_track['track'];
										}
										$res1['media_from'] = "accepted_media";
										$get_type_of_tagged_media = $new_profile_media_classobj->sel_type_of_tagged_media($res1['media_type']);
										$res1['name'] = $get_type_of_tagged_media['name'];
										$new_tag_media_arr[] = $res1;
										
									}
								}
							}
							}
							?>
							<!--code for displaying Edit gallery-->
							<?php
							$new_edi_gal_media_arr = array();
							if(isset($_GET['edit_gal']) && $_GET['edit_gal']==1)
							{
								$k=1;
								if($getmedia!="" && $getmedia!=NULL)
								{
									while($res=mysql_fetch_assoc($getmedia))
									{
										if($res['media_type'] == 114)
										{
											$get_media_track = $new_profile_media_classobj->get_media_track($res['id']); 
											$res["track"] = $get_media_track['track'];
										}
										$res['media_from'] = "newedit_media";
										$new_edi_gal_media_arr[] = $res;
									}
								}
							}
							?>
							
							
							<!-- Code For Displaying media From Fan --->
							<?php						
							$chk_fan = $new_profile_media_classobj->chk_fan();
							$new_fan_media_array = array();
							if($chk_fan!="" && $chk_fan!=NULL)
							{
								while($row_fan = mysql_fetch_assoc($chk_fan))
								{
									$new_fan_arr[] = $row_fan;
									$get_fan_media = $new_profile_media_classobj->get_fan_media($row_fan['buyer_id'],$row_fan['seller_id'],$row_fan['table_name'],$row_fan['type_id']);
									if($get_fan_media!="" && $get_fan_media!=NULL)
									{
										if(mysql_num_rows($get_fan_media)>0)
										{
											while($row_fan_media = mysql_fetch_assoc($get_fan_media))
											{
												$get_fan_generalmedia = $new_profile_media_classobj->get_all_fan_mediadetail($row_fan_media['media_id']);
												if($get_fan_generalmedia['media_type'] == 114)
												{
													$get_media_track = $new_profile_media_classobj->get_media_track($get_fan_generalmedia['id']); 
													$get_fan_generalmedia["track"] = $get_media_track['track'];
												}
												$get_type_of_tagged_media = $new_profile_media_classobj->sel_type_of_tagged_media($get_fan_generalmedia['media_type']);
												$get_fan_generalmedia['name'] = $get_type_of_tagged_media['name'];
												$get_fan_generalmedia['media_from'] = "media_coming_fan";
												$new_fan_media_array[] = $get_fan_generalmedia;
											}
										}
									}
								}
							}
							//var_dump($new_fan_media_array);
							$get_buyed_media = $new_profile_media_classobj->get_sale_media();
							$new_buyed_media_array = array();
							if($get_buyed_media!="" && $get_buyed_media!=NULL){
								if(mysql_num_rows($get_buyed_media)>0){
									while($row_buyed_media = mysql_fetch_assoc($get_buyed_media)){
										$get_buyed_generalmedia = $new_profile_media_classobj->get_all_fan_mediadetail($row_buyed_media['media_id']);
										if($get_buyed_generalmedia['media_type'] == 114)
										{
											$get_media_track = $new_profile_media_classobj->get_media_track($get_buyed_generalmedia['id']); 
											$get_buyed_generalmedia["track"] = $get_media_track['track'];
										}
										$get_type_of_tagged_media = $new_profile_media_classobj->sel_type_of_tagged_media($get_buyed_generalmedia['media_type']);
										$get_buyed_generalmedia['name'] = $get_type_of_tagged_media['name'];
										$get_buyed_generalmedia['media_from'] = "media_coming_buyed";
										$new_buyed_media_array[] = $get_buyed_generalmedia;
									}
								}
							}
							//var_dump($new_buyed_media_array);
							$get_media_tag_art_pro = $new_profile_media_classobj->get_artist_project_tag($_SESSION['login_email']);
							if($get_media_tag_art_pro!="" && $get_media_tag_art_pro!=NULL)
							{
								if(mysql_num_rows($get_media_tag_art_pro)>0)
								{
									$get_songs_new = array();
									$get_video_new = array();
									$get_gallery_new = array();
									$is = 0;
									$iv = 0;
									$ig = 0;
									while($art_pro_tag = mysql_fetch_assoc($get_media_tag_art_pro))
									{
										$get_songs = explode(",",$art_pro_tag['tagged_songs']);
										for($count_art_tag_song=0;$count_art_tag_song<count($get_songs);$count_art_tag_song++)
										{
											if($get_songs[$count_art_tag_song]!="")
											{
												$get_songs_new[$is] = $get_songs[$count_art_tag_song];
												$is = $is + 1;
											}
										}
										$get_video = explode(",",$art_pro_tag['tagged_videos']);
										for($count_art_tag_video=0;$count_art_tag_video<count($get_video);$count_art_tag_video++)
										{
											if($get_video[$count_art_tag_video]!="")
											{
												$get_video_new[$iv] = $get_video[$count_art_tag_video];
												$iv = $iv + 1;
											}
										}
										$get_gallery = explode(",",$art_pro_tag['tagged_galleries']);
										for($count_art_tag_gal=0;$count_art_tag_gal<count($get_gallery);$count_art_tag_gal++)
										{
											if($get_gallery[$count_art_tag_gal]!="")
											{
												$get_gallery_new[$ig] = $get_gallery[$count_art_tag_gal];
												$ig = $ig + 1;
											}
										}
									}
									if($get_songs_new!=NULL)
									{
										$get_songs_n = array_unique($get_songs_new);
									}
									if($get_video_new!=NULL)
									{
										$get_video_n = array_unique($get_video_new);
									}
									if($get_gallery_new!=NULL)
									{
										$get_gallery_n = array_unique($get_gallery_new);
									}
								}
							}
							
							$get_media_tag_art_eve = $new_profile_media_classobj->get_artist_event_tag($_SESSION['login_email']);
							if($get_media_tag_art_eve!="" && $get_media_tag_art_eve!=NULL)
							{
								if(mysql_num_rows($get_media_tag_art_eve)>0)
								{
									$get_songs_newe = array();
									$get_video_newe = array();
									$get_gallery_newe = array();
									$is = 0;
									$iv = 0;
									$ig = 0;
									while($art_pro_tag = mysql_fetch_assoc($get_media_tag_art_eve))
									{
										$get_songs = explode(",",$art_pro_tag['tagged_songs']);
										for($count_art_tag_song=0;$count_art_tag_song<count($get_songs);$count_art_tag_song++)
										{
											if($get_songs[$count_art_tag_song]!="")
											{
												$get_songs_newe[$is] = $get_songs[$count_art_tag_song];
												$is = $is + 1;
											}
										}
										$get_video = explode(",",$art_pro_tag['tagged_videos']);
										for($count_art_tag_video=0;$count_art_tag_video<count($get_video);$count_art_tag_video++)
										{
											if($get_video[$count_art_tag_video]!="")
											{
												$get_video_newe[$iv] = $get_video[$count_art_tag_video];
												$iv = $iv + 1;
											}
										}
										$get_gallery = explode(",",$art_pro_tag['tagged_galleries']);
										for($count_art_tag_gallery=0;$count_art_tag_gallery<count($get_gallery);$count_art_tag_gallery++)
										{
											if($get_gallery[$count_art_tag_gallery]!="")
											{
												$get_gallery_newe[$ig] = $get_gallery[$count_art_tag_gallery];
												$ig = $ig + 1;
											}
										}
									}
									if($get_songs_newe!=NULL)
									{
										$get_songs_ne = array_unique($get_songs_newe);
									}
									if($get_video_newe!=NULL)
									{
										$get_video_ne = array_unique($get_video_newe);
									}
									if($get_gallery_newe!=NULL)
									{
										$get_gallery_ne = array_unique($get_gallery_newe);
									}
								}
							}
							
							$get_media_tag_com_pro = $new_profile_media_classobj->get_community_project_tag($_SESSION['login_email']);
							if($get_media_tag_com_pro!="" && $get_media_tag_com_pro!=NULL)
							{
								if(mysql_num_rows($get_media_tag_com_pro)>0)
								{
									$get_songs_new_c = array();
									$get_video_new_c = array();
									$get_gallery_new_c = array();
									$is_c = 0;
									$iv_c = 0;
									$ig_c = 0;
									
									while($com_pro_tag = mysql_fetch_assoc($get_media_tag_com_pro))
									{
										$get_com_songs = explode(",",$com_pro_tag['tagged_songs']);
										for($count_com_tag_song=0;$count_com_tag_song<count($get_com_songs);$count_com_tag_song++)
										{
											if($get_com_songs[$count_com_tag_song]!="")
											{
												$get_songs_new_c[$is_c] = $get_com_songs[$count_com_tag_song];
												$is_c = $is_c + 1;
											}
										}
										$get_com_video = explode(",",$com_pro_tag['tagged_videos']);
										for($count_com_tag_video=0;$count_com_tag_video<count($get_com_video);$count_com_tag_video++)
										{
											if($get_com_video[$count_com_tag_video]!="")
											{
												$get_video_new_c[$iv_c] = $get_com_video[$count_com_tag_video];
												$iv_c = $iv_c + 1;
											}
										}
										$get_com_gallery = explode(",",$com_pro_tag['tagged_galleries']);
										for($count_com_tag_gallery=0;$count_com_tag_gallery<count($get_com_gallery);$count_com_tag_gallery++)
										{
											if($get_com_gallery[$count_com_tag_gallery]!="")
											{
												$get_gallery_new_c[$ig_c] = $get_com_gallery[$count_com_tag_gallery];
												$ig_c = $ig_c + 1;
											}
										}
									}
									if($get_songs_new_c!=NULL)
									{
										$get_songs_n_c = array_unique($get_songs_new_c);
									}
									if($get_video_new_c!=NULL)
									{
										$get_video_n_c = array_unique($get_video_new_c);
									}
									if($get_gallery_new_c!=NULL)
									{
										$get_gallery_n_c = array_unique($get_gallery_new_c);
									}
								}
							}
							
							$get_media_tag_com_eve = $new_profile_media_classobj->get_community_event_tag($_SESSION['login_email']);
							if($get_media_tag_com_eve!="" && $get_media_tag_com_eve!=NULL)
							{
								if(mysql_num_rows($get_media_tag_com_eve)>0)
								{
									$get_songs_new_ce = array();
									$get_video_new_ce = array();
									$get_gallery_new_ce = array();
									$is_c = 0;
									$iv_c = 0;
									$ig_c = 0;
									while($com_pro_tag = mysql_fetch_assoc($get_media_tag_com_eve))
									{
										$get_com_songs = explode(",",$com_pro_tag['tagged_songs']);
										for($count_com_tag_song=0;$count_com_tag_song<count($get_com_songs);$count_com_tag_song++)
										{
											if($get_com_songs[$count_com_tag_song]!="")
											{
												$get_songs_new_ce[$is_c] = $get_com_songs[$count_com_tag_song];
												$is_c = $is_c + 1;
											}
										}
										$get_com_video = explode(",",$com_pro_tag['tagged_videos']);
										for($count_com_tag_video=0;$count_com_tag_video<count($get_com_video);$count_com_tag_video++)
										{
											if($get_com_video[$count_com_tag_video]!="")
											{
												$get_video_new_ce[$iv_c] = $get_com_video[$count_com_tag_video];
												$iv_c = $iv_c + 1;
											}
										}
										$get_com_gallery = explode(",",$com_pro_tag['tagged_galleries']);
										for($count_com_tag_gallery=0;$count_com_tag_gallery<count($get_com_gallery);$count_com_tag_gallery++)
										{
											if($get_com_gallery[$count_com_tag_gallery]!="")
											{
												$get_gallery_new_ce[$ig_c] = $get_com_gallery[$count_com_tag_gallery];
												$ig_c = $ig_c + 1;
											}
										}
									}
									if($get_songs_new_ce!=NULL)
									{
										$get_songs_n_ec = array_unique($get_songs_new_ce);
									}
									if($get_video_new_ce!=NULL)
									{
										$get_video_n_ec = array_unique($get_video_new_ce);
									}
									if($get_gallery_new_ce!=NULL)
									{
										$get_gallery_n_ec = array_unique($get_gallery_new_ce);
									}
								}
							}
							
							$get_media_cre_art_pro = $new_profile_media_classobj->get_project_create();
							if(!empty($get_media_cre_art_pro))
							{
								$get_songs_pn = array();
								$get_video_pn = array();
								$get_gallery_pn = array();
								$is_p = 0;
								$iv_p = 0;
								$ig_p = 0;
								
								//while($art_pro_tag = mysql_fetch_assoc($get_media_cre_art_pro))
								for($cr_i=0;$cr_i<count($get_media_cre_art_pro);$cr_i++)
								{
									$get_csongs = explode(",",$get_media_cre_art_pro[$cr_i]['tagged_songs']);
									for($count_art_cr_song=0;$count_art_cr_song<count($get_csongs);$count_art_cr_song++)
									{
										if($get_csongs[$count_art_cr_song]!="")
										{
											$get_songs_pn[$is_p] = $get_csongs[$count_art_cr_song];
											$is_p = $is_p + 1;
										}
									}
									$get_cvideo = explode(",",$get_media_cre_art_pro[$cr_i]['tagged_videos']);
									for($count_art_cr_video=0;$count_art_cr_video<count($get_cvideo);$count_art_cr_video++)
									{
										if($get_cvideo[$count_art_cr_video]!="")
										{
											$get_video_pn[$iv_p] = $get_cvideo[$count_art_cr_video];
											$iv_p = $iv_p + 1;
										}
									}
									$get_cgallery = explode(",",$get_media_cre_art_pro[$cr_i]['tagged_galleries']);
									for($count_art_cr_gallery=0;$count_art_cr_gallery<count($get_cgallery);$count_art_cr_gallery++)
									{
										if($get_cgallery[$count_art_cr_gallery]!="")
										{
											$get_gallery_pn[$ig_p] = $get_cgallery[$count_art_cr_gallery];
											$ig_p = $ig_p + 1;
										}
									}
								}
								if($get_songs_pn!=NULL)
								{
									$get_songs_p_new = array_unique($get_songs_pn);
								}
								if($get_video_pn!=NULL)
								{
									$get_video_p_new = array_unique($get_video_pn);
								}
								if($get_gallery_pn!=NULL)
								{
									$get_gallery_p_new = array_unique($get_gallery_pn);
								}
							}
							
							if(!isset($get_songs_n)){
								$get_songs_n = array();}
							if(!isset($get_video_n)){
								$get_video_n = array();}
							if(!isset($get_gallery_n)){
								$get_gallery_n = array();}
							
							if(!isset($get_songs_n_ec)){
								$get_songs_n_ec = array();}
							if(!isset($get_video_n_ec)){
								$get_video_n_ec = array();}
							if(!isset($get_gallery_n_ec)){
								$get_gallery_n_ec = array();}
							
							if(!isset($get_songs_ne)){
								$get_songs_ne = array();}
							if(!isset($get_video_ne)){
								$get_video_ne = array();}
							if(!isset($get_gallery_ne)){
								$get_gallery_ne = array();}
							
							if(!isset($get_songs_n_c)){
								$get_songs_n_c = array();}
							if(!isset($get_video_n_c)){
								$get_video_n_c = array();}
							if(!isset($get_gallery_n_c)){
								$get_gallery_n_c = array();}
							
							if(!isset($get_songs_p_new)){
								$get_songs_p_new = array();}
							if(!isset($get_video_p_new)){
								$get_video_p_new = array();}
							if(!isset($get_gallery_p_new)){
								$get_gallery_p_new = array();}
								
							$get_songs_pnew_w_c = array_merge($get_songs_n,$get_video_n,$get_gallery_n,$get_songs_n_c,$get_video_n_c,$get_gallery_n_c,$get_songs_p_new,$get_video_p_new,$get_gallery_p_new,$get_songs_ne,$get_video_ne,$get_gallery_ne,$get_songs_n_ec,$get_video_n_ec,$get_gallery_n_ec);
							if($get_songs_pnew_w_c!=NULL)
							{
								$get_songs_pnew_ct = array_unique($get_songs_pnew_w_c);
							}
							$get_songs_final_ct = array();
							
							for($count_art_cr_song=0;$count_art_cr_song<count($get_songs_pnew_ct);$count_art_cr_song++)
							{
								if($get_songs_pnew_ct[$count_art_cr_song]!="")
								{
									$get_media_tag_art_pro_song = $new_profile_media_classobj->get_tag_media_info($get_songs_pnew_ct[$count_art_cr_song]);
									if(!empty($get_media_tag_art_pro_song)){
										if($get_media_tag_art_pro_song['media_type'] == 114)
										{
											$get_media_track = $new_profile_media_classobj->get_media_track($get_media_tag_art_pro_song['id']); 
											$get_media_tag_art_pro_song["track"] = $get_media_track['track'];
										}
										$get_media_tag_art_pro_song['media_from'] ="new_tagging_media";
										$get_songs_final_ct[] = $get_media_tag_art_pro_song;
									}
									
								}
							}
							$get_songs_final_ct = array();
							$new_medi = $new_profile_media_classobj->get_creator_heis();
							$new_creator_creator_media_array = array();
							if(!empty($new_medi))
							{
								for($count_cre_m=0;$count_cre_m<count($new_medi);$count_cre_m++)
								{
									$get_media_type = $new_profile_media_classobj->sel_type_of_tagged_media($new_medi[$count_cre_m]['media_type']);
									if($new_medi[$count_cre_m]['media_type'] == 114)
									{
										$get_media_track = $new_profile_media_classobj->get_media_track($new_medi[$count_cre_m]['id']); 
										$new_medi[$count_cre_m]["track"] = $get_media_track['track'];
									}
									$new_medi[$count_cre_m]['name'] = $get_media_type['name'];
									$new_medi[$count_cre_m]['media_from'] = "media_creator_creator";
									$new_creator_creator_media_array[] = $new_medi[$count_cre_m];
								}
							}

							/*This function will display the user in which profile he is creator and display media where that profile was creator Ends here*/

							/*This function will display the user in which profile he is from and display media where that profile was creator*/
							$new_medi_from = $new_profile_media_classobj->get_from_heis();
							$new_from_from_media_array = array();
							if(!empty($new_medi_from))
							{
								for($count_cre_m=0;$count_cre_m<count($new_medi_from);$count_cre_m++)
								{
									$get_media_type = $new_profile_media_classobj->sel_type_of_tagged_media($new_medi_from[$count_cre_m]['media_type']);
									if($new_medi_from[$count_cre_m]['media_type'] == 114)
									{
										$get_media_track = $new_profile_media_classobj->get_media_track($new_medi_from[$count_cre_m]['id']); 
										$new_medi_from[$count_cre_m]["track"] = $get_media_track['track'];
									}
									$new_medi_from[$count_cre_m]['name'] = $get_media_type['name'];
									$new_medi_from[$count_cre_m]['media_from'] = "media_from_from";
									$new_from_from_media_array[] = $new_medi_from[$count_cre_m];
								}
							}
							/*This function will display the user in which profile he is from and display media where that profile was creator Ends here*/
							
							/*This function will display the user in which profile he is from or creator and tagged and display media where that profile was creator*/
							$new_medi_tagged = $new_profile_media_classobj->get_the_user_tagin_project($_SESSION['login_email']);
							$new_tagged_from_media_array = array();
							if(!empty($new_medi_tagged))
							{
								for($count_cre_m=0;$count_cre_m<count($new_medi_tagged);$count_cre_m++)
								{
									$get_media_type = $new_profile_media_classobj->sel_type_of_tagged_media($new_medi_tagged[$count_cre_m]['media_type']);
									if($new_medi_tagged[$count_cre_m]['media_type'] == 114)
									{
										$get_media_track = $new_profile_media_classobj->get_media_track($new_medi_tagged[$count_cre_m]['id']); 
										$new_medi_tagged[$count_cre_m]["track"] = $get_media_track['track'];
									}
									$new_medi_tagged[$count_cre_m]['name'] = $get_media_type['name'];
									$new_medi_tagged[$count_cre_m]['media_from'] = "media_creator_from_tag";
									$new_tagged_from_media_array[] = $new_medi_tagged[$count_cre_m];
								}
							}
							/*This function will display the user in which profile he is from or creator and tagged and display media where that profile was creator Ends here*/
							
							/*This function will display in which media the curent user was creator*/
							$get_where_creator_media = $new_profile_media_classobj->get_media_where_creator_or_from();
							$new_creator_media_array = array();
							if(!empty($get_where_creator_media))
							{
								for($count_tag_m=0;$count_tag_m<count($get_where_creator_media);$count_tag_m++)
								{
									$get_media_type = $new_profile_media_classobj->sel_type_of_tagged_media($get_where_creator_media[$count_tag_m]['media_type']);
									if($get_where_creator_media[$count_tag_m]['media_type'] == 114)
									{
										$get_media_track = $new_profile_media_classobj->get_media_track($get_where_creator_media[$count_tag_m]['id']); 
										$get_where_creator_media[$count_tag_m]["track"] = $get_media_track['track'];
									}
									$get_where_creator_media[$count_tag_m]['name'] = $get_media_type['name'];
									$get_where_creator_media[$count_tag_m]['media_from'] = "media_creator";
									$new_creator_media_array[] = $get_where_creator_media[$count_tag_m];
								}
							}
							/*This function will display in which media the curent user was creator ends here*/
							$cre_frm_med = array();
							$cre_frm_med = $new_profile_media_classobj->get_media_create();
							
							/*Get Subscription media*/
							$new_subscription_media = array();
							$get_subscription_media_res = $new_profile_media_classobj->get_subscription_media();
							for($count_sub=0;$count_sub<=count($get_subscription_media_res);$count_sub++)
							{
								if($get_subscription_media_res[$count_sub]!="")
								{
									$get_media_type = $new_profile_media_classobj->sel_type_of_tagged_media($get_subscription_media_res[$count_sub]['media_type']);
									if($get_subscription_media_res[$count_sub]['media_type']==114)
									{
										$get_media_track_res = $new_profile_media_classobj->get_media_track($get_subscription_media_res[$count_sub]['id']); 
										$get_subscription_media_res[$count_sub]['track'] = $get_media_track_res['track'];
									}
									$get_subscription_media_res[$count_sub]['name'] = $get_media_type['name'];
									$get_subscription_media_res[$count_sub]['media_from'] = "email_subscription";
									$new_subscription_media[] = $get_subscription_media_res[$count_sub];
								}
							}
							
							if($member_var==0){
								$new_subscription_media = array();
							}
							
							$combine_arr[] = array_merge ($my_media_arr,$new_tag_media_arr,$new_edi_gal_media_arr,$new_fan_media_array,$new_creator_media_array,$new_creator_creator_media_array,$new_from_from_media_array,$new_tagged_from_media_array,$cre_frm_med,$get_songs_final_ct,$new_subscription_media,$new_buyed_media_array);
							$sort = array();
							foreach($combine_arr[0] as $k=>$v) {
							
								//$create_c = strpos($v['creator'],'(');
								//$end_c1 = substr($v['creator'],0,$create_c);
								$end_c1 = $v['creator'];
								$end_c = trim($end_c1);
								
								//$create_f = strpos($v['from'],'(');
								//$end_f1 = substr($v['from'],0,$create_f);
								$end_f1 = $v['from'];
								$end_f = trim($end_f1);
								
								$sort['creator'][$k] = strtolower($end_c);
								
								$sort['from'][$k] = strtolower($end_f);
								if($v['track']!=NULL){								
									$sort['track'][$k] = $v['track'];
								}else{									
									$sort['track'][$k] = " ";
								}
								$sort['title'][$k] = strtolower($v['title']);
							}
							if(!empty($sort))
							{
								array_multisort($sort['creator'], SORT_ASC, $sort['from'], SORT_ASC, $sort['track'], SORT_NUMERIC ,SORT_ASC,$sort['title'], SORT_ASC,$combine_arr[0]);
							}
							$dummy_pr = array();
							$test_count = 0;
							$get_all_del_id = $new_profile_media_classobj->get_all_del_media_id();
							$exp_all_del_id = explode(",",$get_all_del_id);
							for($count_all=0;$count_all<=count($combine_arr[$test_count]);$count_all++)
							{
								for($count_all_del=0;$count_all_del<=count($exp_all_del_id);$count_all_del++)
								{
									if($exp_all_del_id[$count_all_del]==""){continue;}
									else{
										if($exp_all_del_id[$count_all_del]==$combine_arr[$test_count][$count_all]['id'])
										{
											$combine_arr[$test_count][$count_all]="";
										}
									}
								}
							}
							for($newtest_count = 0;$newtest_count<=count($combine_arr[$test_count]);$newtest_count++){
								if(in_array($combine_arr[$test_count][$newtest_count]['id'],$dummy_pr))
							    {
								
							    }
							    else
							    {
									$dummy_pr[] = $combine_arr[$test_count][$newtest_count]['id'];
								
								if($combine_arr[$test_count][$newtest_count]['id'] !="")
								{
								//echo $combine_arr[$test_count][$newtest_count]['media_from'];
								if($combine_arr[$test_count][$newtest_count]['media_from']=="my_media")
								{
								?>
									<div class="tableCont" id="AccordionContainer">
									<div class="blkD" style="width:60px;"><?php if($combine_arr[$test_count][$newtest_count]['name']=="Galleries/Images"){echo "Images";}else{echo $combine_arr[$test_count][$newtest_count]['name'];} ?></div>
									<div class="blkI" style="width:127px;">
									<?php
									$exp_creator_info = explode("|",$combine_arr[$test_count][$newtest_count]['creator_info']);
									$pro_url = $new_profile_media_classobj->get_creator_url($exp_creator_info[0],$exp_creator_info[1]);
									?>
									<a href="<?php if($pro_url['profile_url'] !=""){ echo "/".$pro_url['profile_url']; }else { echo "javascript:void(0)"; }?>"><?php
									if($combine_arr[$test_count][$newtest_count]['creator']!="")
									{
										//$end_pos = strpos($combine_arr[$test_count][$newtest_count]['creator'],'(');
										//if($end_pos !="" || $end_pos !=false){
									//echo substr($combine_arr[$test_count][$newtest_count]['creator'],0,$end_pos);}else{
										echo $combine_arr[$test_count][$newtest_count]['creator'];
										//}
									}else{
									?>&nbsp;<?php
									}
										?></a></div>
										
									<div class="blkK">
									<?php
									$exp_from_info = explode("|",$combine_arr[$test_count][$newtest_count]['from_info']);
									$pro_url_from = $new_profile_media_classobj->get_from_url($exp_from_info[0],$exp_from_info[1]);
									?>
									<a href="<?php if($pro_url_from['profile_url'] !=""){ echo "/".$pro_url_from['profile_url']; }else { echo "javascript:void(0)"; }?>"><?php 
									if($combine_arr[$test_count][$newtest_count]['from'] !="")
									{
										//$end_pos_from = strpos($combine_arr[$test_count][$newtest_count]['from'],'(');
										//if($end_pos_from !="" || $end_pos_from !=false){
										//echo substr($combine_arr[$test_count][$newtest_count]['from'],0,$end_pos_from);}else{
										echo $combine_arr[$test_count][$newtest_count]['from'];
										//}
									}else{
									?>&nbsp;<?php
									}
										?></a></div>
									<div class="blkB"><?php
									if($combine_arr[$test_count][$newtest_count]['title']!="")
									{
										if($combine_arr[$test_count][$newtest_count]['media_type'] == 114)
										{
											//$get_media_track = $new_profile_media_classobj->get_media_track($combine_arr[$test_count][$newtest_count]['id']); 
											if($combine_arr[$test_count][$newtest_count]['track'] != "" && $combine_arr[$test_count][$newtest_count]['track'] != NULL && $combine_arr[$test_count][$newtest_count]['track'] !=" "){
												echo $combine_arr[$test_count][$newtest_count]['track'].".".stripslashes($combine_arr[$test_count][$newtest_count]['title']);
											}else{
												echo stripslashes($combine_arr[$test_count][$newtest_count]['title']);
											}
										}
										else{
										echo stripslashes($combine_arr[$test_count][$newtest_count]['title']); }
									}else{
									?>&nbsp;<?php
									}
										?></div>
									<div class="blkplayer">
										<div class="player">
										<?php
											if($combine_arr[$test_count][$newtest_count]['media_type'] == 114){
											?>
											<a href="javascript:void(0);"><img title="Play file now." src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" onclick="showPlayer('a','<?php if($combine_arr[$test_count][$newtest_count]['id']!=""){echo $combine_arr[$test_count][$newtest_count]['id'];}else{echo $combine_arr[$test_count][$newtest_count]['id'];}?>','play');"/></a>
											<a href="javascript:void(0);"><img title="Add file to the bottom of your playlist without stopping current media." src="images/profile/add.gif"  onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'" onclick="showPlayer('a','<?php if($combine_arr[$test_count][$newtest_count]['id']!=""){echo $combine_arr[$test_count][$newtest_count]['id'];}else{echo $combine_arr[$test_count][$newtest_count]['id'];}?>','add');"/></a>
											<?php
											}
											else if($combine_arr[$test_count][$newtest_count]['media_type'] == 113){
												$get_images = $new_profile_media_classobj ->get_all_gallery_images($combine_arr[$test_count][$newtest_count]['id']);
												for($count_img=0;$count_img<=count($get_images);$count_img++){
													if($get_all_img =="")
													{
														$get_all_img = $get_images[$count_img]['image_name'];
													}else{
														$get_all_img = $get_all_img.",".$get_images[$count_img]['image_name'];
													}
													if($galleryImagestitle==""){
														$galleryImagestitle = $get_images[$count_img]['image_title'];
													}else{
														$galleryImagestitle = $galleryImagestitle.",".$get_images[$count_img]['image_title'];
													}
													$mediaSrc = $get_images[0]['image_name'];
												}
												$gal_id = $get_images[$count_img]['media_id'];
												$get_other_user_id = $user_keys_data->get_general_user_idby_mediaid($combine_arr[$test_count][$newtest_count]['id']);
												$orgPath = $s3->getNewURI_otheruser("medgallery",$get_other_user_id);//"https://medgallery.s3.amazonaws.com/";
												$thumbPath = $s3->getNewURI_otheruser("medgalthumb",$get_other_user_id);//"https://medgalthumb.s3.amazonaws.com/";
											?>
											<a href="javascript:void(0);"><img title="Play file now." src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" onclick="checkload('<?php echo $mediaSrc;?>','<?php echo $get_all_img; ?>','<?php echo $thumbPath; ?>','<?php echo $orgPath; ?>','<?php echo $galleryImagestitle; ?>','<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>')"/></a>
											<!--<a href="javascript:void(0);"><img title="Add file to the bottom of your playlist without stopping current media." src="images/profile/add.gif"  onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'"/></a>-->
											<?php
												$get_all_img ="";
												$galleryImagestitle="";
												$mediaSrc="";
											}
											else if($combine_arr[$test_count][$newtest_count]['media_type'] == 115){
											?>
											<a href="javascript:void(0);"><img title="Play file now." src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" onclick="showPlayer('v','<?php if($combine_arr[$test_count][$newtest_count]['id']!=""){echo $combine_arr[$test_count][$newtest_count]['id'];}else{ echo $combine_arr[$test_count][$newtest_count]['id'];}?>','playlist');"/></a>
											<a href="javascript:void(0);"><img title="Add file to the bottom of your playlist without stopping current media." src="images/profile/add.gif"  onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'" onclick="showPlayer('v','<?php if($combine_arr[$test_count][$newtest_count]['id']!=""){echo $combine_arr[$test_count][$newtest_count]['id'];}else{ echo $combine_arr[$test_count][$newtest_count]['id'];}?>','addvi');"/></a>
											<?php
											}else if($combine_arr[$test_count][$newtest_count]['media_type'] == 116){
												$get_channel_media = $new_profile_media_classobj ->get_all_media_selected_channel($combine_arr[$test_count][$newtest_count]['id']);
											?>
												<input type="hidden" id="channel_media_song<?php echo $newtest_count;?>" value="<?php echo $get_channel_media['songs']; ?>"/>
												<input type="hidden" id="channel_media_video<?php echo $newtest_count;?>" value="<?php echo $get_channel_media['videos']; ?>"/>
												<a href="javascript:void(0);"><img title="Play file now." src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" onclick="PlayAll('channel_media_video<?php echo $newtest_count;?>','channel_media_song<?php echo $newtest_count;?>')"/></a>
												<a href="javascript:void(0);"><img title="Add file to the bottom of your playlist without stopping current media." src="images/profile/add.gif"  onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'" onclick="showPlayer('v','<?php if($combine_arr[$test_count][$newtest_count]['id']!=""){echo $combine_arr[$test_count][$newtest_count]['id'];}else{ echo $combine_arr[$test_count][$newtest_count]['id'];}?>','addvi');"/></a>
											<?php
											}
											else{
											?>
											<a href="javascript:void(0);"><img title="Play file now." src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" /></a>
											<a href="javascript:void(0);"><img title="Add file to the bottom of your playlist without stopping current media." src="images/profile/add.gif"  onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'"/></a>
											<?php
											}
											if(($combine_arr[$test_count][$newtest_count]['sharing_preference']==5 || $combine_arr[$test_count][$newtest_count]['sharing_preference']==2) && $combine_arr[$test_count][$newtest_count]['media_type'] == 114){
												?>
												<a onclick="download_directly_song('<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['title']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['creator']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['from']);?>')" href="javascript:void(0);">
												<img title="Add media to your cart for free download or purchase." onmouseout="this.src='../images/profile/download_down.gif'" onmouseover="this.src='../images/profile/download-over_down.gif'" src="../images/profile/download_down.gif"></a>
											<?php
											}/* else{
											?>
											<img src="images/profile/download-over_down.gif" />
											<?php
											} */
											?>
										
										</div>
									</div>
									<div class="icon" onclick="runAccordion(<?php echo $combine_arr[$test_count][$newtest_count]['id']; ?>);">
										<div onselectstart="return false;"><img title="View details for media." src="images/profile/view.jpg" /></div>
									</div>
									<div class="icon"><a href="add_media.php?edit=<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>" onclick="save_cookie()"><img title="Edit media." src="images/profile/edit.png" /></a></div>
									<div class="icon"><a onclick="return confirmdelete('<?php echo $combine_arr[$test_count][$newtest_count]['title'];?>','<?php if($combine_arr[$test_count][$newtest_count]['name']=="Galleries/Images"){echo "Images";}else{echo $combine_arr[$test_count][$newtest_count]['name'];} ?>')" href="delete_media.php?id=<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>&whos_media=my_media" ><img title="Remove media from your library." src="images/profile/delete.png" /></a></div>
									<div class="expandable" id="Accordion<?php echo $combine_arr[$test_count][$newtest_count]['id']; ?>Content">
										<div class="affiBlock">
											<div class="RowCont">
												<div class="sideL">Plays :</div>
												<div class="sideR"><a href="" target="_blank"><?php if($combine_arr[$test_count][$newtest_count]['play_count']!=""){ echo $combine_arr[$test_count][$newtest_count]['play_count'];}else{ echo "0";}?></a></div>
											</div>
											<?php
											if($combine_arr[$test_count][$newtest_count]['sharing_preference']==5 || $combine_arr[$test_count][$newtest_count]['sharing_preference']==2){
											?>
											<div class="RowCont">
												<div class="sideL">Downloads :</div>
												<div class="sideR"><a href="javascript:void(0);"><?php echo $combine_arr[$test_count][$newtest_count]['download_count'];?></a></div>
											</div>
											<?php
											}
												if($combine_arr[$test_count][$newtest_count]['media_type']=='113')
												{
												
											?>
											<div class="RowCont">
												<div class="sideL">Image :</div>
												<div class="sideR"><a id="gallery_images<?php echo $newtest_count; ?>" class="fancybox fancybox.ajax" href="gallery_images.php?gal_id=<?php echo $combine_arr[$test_count][$newtest_count]['id']; ?>">Edit Image</a></div>
											</div>	
											<div class="RowCont">
												<div class="sideL"></div>
												<?php 
												$find_gal = $new_profile_media_classobj->get_gallery_editable($combine_arr[$test_count][$newtest_count]['id']);
												if($find_gal['gallery_id'] !=0)
												{
												?>
													<div class="sideR"><a id="Add_gallery_images<?php echo $newtest_count; ?>" class="fancybox fancybox.ajax" href="add_gallery_images.php?gal_id=<?php echo $combine_arr[$test_count][$newtest_count]['id']; ?>">Add Image</a></div>
												<?php
												}
												?>
											</div>
											<?php
												
												}
												else
												{
											?>	
											<!--<div class="RowCont">
												<div class="sideL">Media URL :</div>
												<div class="sideR"><a href="" target="_blank">www.<?php echo $domainname; ?></a></div>
											</div>-->
											<?php
												}
											?>
										</div>
									</div>
								</div>
									<?php
									$k = $k+1;
								}
								
								else if($combine_arr[$test_count][$newtest_count]['media_from']=="accepted_media")
								{
									if(isset($_GET['sel_val']) && $_GET['sel_val']!='all media')
									{
										$chk_tag_user = $new_profile_media_classobj->get_loggedin_info($_SESSION['login_email']);
										if($chk_tag_user['general_user_id']!=$combine_arr[$test_count][$newtest_count]['general_user_id'])
										{
											if($_GET['sel_val']==$combine_arr[$test_count][$newtest_count]['media_type'])
											{
												$get_type_of_tagged_media=$new_profile_media_classobj->sel_type_of_tagged_media($combine_arr[$test_count][$newtest_count]['media_type']);
									?>
											<div class="tableCont" id="AccordionContainer">
												<div class="blkD" style="width:60px;"><?php if($get_type_of_tagged_media['name']!=""){if($get_type_of_tagged_media['name'] =="Galleries/Images"){echo "Images";}else{echo $get_type_of_tagged_media['name'];}}else{?>&nbsp;<?php } ?></div>
												<div class="blkI" style="width:127px;">
												<?php
												$exp_creator_info = explode("|",$combine_arr[$test_count][$newtest_count]['creator_info']);
												$pro_url = $new_profile_media_classobj->get_creator_url($exp_creator_info[0],$exp_creator_info[1]);
												?>
												<a href="<?php if($pro_url['profile_url'] !=""){ echo "/".$pro_url['profile_url']; }else { echo "javascript:void(0)"; }?>">
												<?php if($combine_arr[$test_count][$newtest_count]['creator']!=""){
													//$endpos = strpos($combine_arr[$test_count][$newtest_count]['creator'],"("); 
													//if($endpos !="" || $endpos !=false){
													//echo substr($combine_arr[$test_count][$newtest_count]['creator'],0,$endpos);} else { 
													echo $combine_arr[$test_count][$newtest_count]['creator']; }
													else{?>&nbsp;<?php } ?></a>
												</div>
												<div class="blkK"><?php
												$exp_from_info = explode("|",$combine_arr[$test_count][$newtest_count]['from_info']);
												$pro_url_from = $new_profile_media_classobj->get_from_url($exp_from_info[0],$exp_from_info[1]);
												?>
												<a href="<?php if($pro_url_from['profile_url'] !=""){ echo "/".$pro_url_from['profile_url']; }else { echo "javascript:void(0)"; }?>">
												<?php if($combine_arr[$test_count][$newtest_count]['from']!=""){
												//$endpos = strpos($combine_arr[$test_count][$newtest_count]['from'],"("); 
												//if($endpos !="" || $endpos != false) { 
												//echo substr($combine_arr[$test_count][$newtest_count]['from'],0,$endpos);
												//} else { 
												echo $combine_arr[$test_count][$newtest_count]['from'];
												}else{?>&nbsp;<?php } ?></a></div>
												<div class="blkB"><?php
												if($combine_arr[$test_count][$newtest_count]['title']!="")
												{
													if($combine_arr[$test_count][$newtest_count]['media_type'] == 114)
													{
														//$get_media_track = $new_profile_media_classobj->get_media_track($combine_arr[$test_count][$newtest_count]['id']); 
														if($combine_arr[$test_count][$newtest_count]['track'] != "" && $combine_arr[$test_count][$newtest_count]['track'] != NULL && $combine_arr[$test_count][$newtest_count]['track'] !=" "){
															echo $combine_arr[$test_count][$newtest_count]['track'].".".stripslashes($combine_arr[$test_count][$newtest_count]['title']);
														}else{
															echo stripslashes($combine_arr[$test_count][$newtest_count]['title']);
														}
													}
													else{
													echo stripslashes($combine_arr[$test_count][$newtest_count]['title']); }
												}else{
												?>&nbsp;<?php
												}
													?></div>
												<div class="blkplayer">
													<div class="player">
													<?php 
														if($combine_arr[$test_count][$newtest_count]['media_type']==113){
															$get_images_tag = $new_profile_media_classobj ->get_all_gallery_images($combine_arr[$test_count][$newtest_count]['id']);
															for($count_img=0;$count_img<=count($get_images_tag);$count_img++){
																if($get_all_img_tag==""){
																	$get_all_img_tag = $get_images_tag[$count_img]['image_name'];
																}else{
																	$get_all_img_tag = $get_all_img_tag.",".$get_images_tag[$count_img]['image_name'];
																}
																if($galleryImagestitle_tag==""){
																	$galleryImagestitle_tag = $get_images_tag[$count_img]['image_title'];
																}else{
																	$galleryImagestitle_tag = $galleryImagestitle_tag.",".$get_images_tag[$count_img]['image_title'];
																}
																
																$mediaSrc_tag = $get_images_tag[0]['image_name'];
															}
															$gal_id_tag = $get_images_tag[$count_img]['media_id'];
															$get_other_user_id = $user_keys_data->get_general_user_idby_mediaid($combine_arr[$test_count][$newtest_count]['id']);
															$orgPath_tag = $s3->getNewURI_otheruser("medgallery",$get_other_user_id);//"https://medgallery.s3.amazonaws.com/";
															$thumbPath_tag = $s3->getNewURI_otheruser("medgalthumb",$get_other_user_id);//"https://medgalthumb.s3.amazonaws.com/";
															
														?>
														<a href="javascript:void(0);"><img title="Play file now." src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" onclick="checkload('<?php echo $mediaSrc_tag;?>','<?php echo $get_all_img_tag; ?>','<?php echo $thumbPath_tag; ?>','<?php echo $orgPath_tag; ?>','<?php echo $galleryImagestitle_tag; ?>','<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>')"/></a>
														<!--<a href="javascript:void(0);"><img title="Add file to the bottom of your playlist without stopping current media." src="images/profile/add.gif"  onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'"/></a>-->
														<?php
															$get_all_img_tag ="";
															$galleryImagestitle_tag="";
															$mediaSrc_tag="";
														}else if($combine_arr[$test_count][$newtest_count]['media_type']==114){
														?>
														<a href="javascript:void(0);"><img title="Play file now." src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" onclick="showPlayer('a','<?php if($combine_arr[$test_count][$newtest_count]['id']!=""){echo $combine_arr[$test_count][$newtest_count]['id'];}else{echo $combine_arr[$test_count][$newtest_count]['id'];}?>','play');"/></a>
														<a href="javascript:void(0);"><img title="Add file to the bottom of your playlist without stopping current media." src="images/profile/add.gif"  onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'" onclick="showPlayer('a','<?php if($combine_arr[$test_count][$newtest_count]['id']!=""){echo $combine_arr[$test_count][$newtest_count]['id'];}else{echo $combine_arr[$test_count][$newtest_count]['id'];}?>','add');"/></a>
														<?php
														}else if($combine_arr[$test_count][$newtest_count]['media_type']==115){
														?>
														<a href="javascript:void(0);"><img title="Play file now." src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" onclick="showPlayer('v','<?php if($combine_arr[$test_count][$newtest_count]['id']!=""){echo $combine_arr[$test_count][$newtest_count]['id'];}else{ echo $combine_arr[$test_count][$newtest_count]['id'];}?>','playlist');"/></a>
														<a href="javascript:void(0);"><img title="Add file to the bottom of your playlist without stopping current media." src="images/profile/add.gif"  onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'" onclick="showPlayer('v','<?php if($combine_arr[$test_count][$newtest_count]['id']!=""){echo $combine_arr[$test_count][$newtest_count]['id'];}else{ echo $combine_arr[$test_count][$newtest_count]['id'];}?>','addvi');"/></a>
														<?php
														}else if($combine_arr[$test_count][$newtest_count]['media_type'] == 116){
															$get_channel_media = $new_profile_media_classobj ->get_all_media_selected_channel($combine_arr[$test_count][$newtest_count]['id']);
														?>
															<input type="hidden" id="channel_media_song<?php echo $newtest_count;?>" value="<?php echo $get_channel_media['songs']; ?>"/>
															<input type="hidden" id="channel_media_video<?php echo $newtest_count;?>" value="<?php echo $get_channel_media['videos']; ?>"/>
															<a href="javascript:void(0);"><img title="Play file now." src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" onclick="PlayAll('channel_media_video<?php echo $newtest_count;?>','channel_media_song<?php echo $newtest_count;?>')"/></a>
															<a href="javascript:void(0);"><img title="Add file to the bottom of your playlist without stopping current media." src="images/profile/add.gif"  onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'" onclick="showPlayer('v','<?php if($combine_arr[$test_count][$newtest_count]['id']!=""){echo $combine_arr[$test_count][$newtest_count]['id'];}else{ echo $combine_arr[$test_count][$newtest_count]['id'];}?>','addvi');"/></a>
														<?php
														}
														else{
														?>
														<a href="javascript:void(0);"><img title="Play file now." src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" /></a>
														<a href="javascript:void(0);"><img title="Add file to the bottom of your playlist without stopping current media." src="images/profile/add.gif"  onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'"/></a>
														<?php
														}
														
													if(($combine_arr[$test_count][$newtest_count]['sharing_preference']==5 || $combine_arr[$test_count][$newtest_count]['sharing_preference']==2) && $combine_arr[$test_count][$newtest_count]['media_type'] == 114){
														?>
														<a onclick="download_directly_song('<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['title']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['creator']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['from']);?>')" href="javascript:void(0);">
														<img title="Add media to your cart for free download or purchase." onmouseout="this.src='../images/profile/download_down.gif'" onmouseover="this.src='../images/profile/download-over_down.gif'" src="../images/profile/download_down.gif"></a>
													<?php
													}/* else{
													?>
													<img src="images/profile/download-over_down.gif" />
													<?php
													} */
													?>
													</div>
												</div>
												<div class="icon" onclick="runAccordion(<?php echo $combine_arr[$test_count][$newtest_count]['id']; ?>);">
													<div onselectstart="return false;"><img title="View details for media." src="images/profile/view.jpg" /></div>
												</div>
												<div class="icon">&nbsp;</div>
												<div class="icon"><a href="delete_media.php?id=<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>&type=<?php if($get_type_of_tagged_media['name'] =="Galleries/Images"){echo "Images";}else{echo $get_type_of_tagged_media['name'];}?>" onclick="return confirmdelete('<?php echo $combine_arr[$test_count][$newtest_count]['title'];?>','<?php if($combine_arr[$test_count][$newtest_count]['name']=="Galleries/Images"){echo "Images";}else{echo $combine_arr[$test_count][$newtest_count]['name'];} ?>')"><img title="Remove media from your library." src="images/profile/delete.png" /></a></div>
												<div class="expandable" id="Accordion<?php echo $combine_arr[$test_count][$newtest_count]['id']; ?>Content">
													<div class="affiBlock">
														<div class="RowCont">
															<div class="sideL">Plays :</div>
															<div class="sideR"><a href="" target="_blank"><?php if($combine_arr[$test_count][$newtest_count]['play_count']!=""){ echo $combine_arr[$test_count][$newtest_count]['play_count'];}else{ echo "0";}?></a></div>
														</div>
														<?php
														if($combine_arr[$test_count][$newtest_count]['sharing_preference']==5 || $combine_arr[$test_count][$newtest_count]['sharing_preference']==2){
														?>
														<div class="RowCont">
															<div class="sideL">Downloads :</div>
															<div class="sideR"><a href="javascript:void(0);"><?php echo $combine_arr[$test_count][$newtest_count]['download_count'];?></a></div>
														</div>
														<?php
														}
															if($combine_arr[$test_count][$newtest_count]['media_type']=='113')
															{
															
															}
															else
															{
														?>	
														<!--<div class="RowCont">
															<div class="sideL">Media URL :</div>
															<div class="sideR"><a href="" target="_blank">www.<?php echo $domainname; ?></a></div>
														</div>-->
														<?php
															}
														?>
													</div>
												</div>
											</div>
									<?php
											}
										}
									}
									else
									{
										$chk_tag_user = $new_profile_media_classobj->get_loggedin_info($_SESSION['login_email']);
										if($chk_tag_user['general_user_id']!=$combine_arr[$test_count][$newtest_count]['general_user_id'])
										{
											$get_type_of_tagged_media=$new_profile_media_classobj->sel_type_of_tagged_media($combine_arr[$test_count][$newtest_count]['media_type']);
									?>
											<div class="tableCont" id="AccordionContainer">
												<div class="blkD" style="width:60px;"><?php if($get_type_of_tagged_media['name']!=""){if($get_type_of_tagged_media['name'] =="Galleries/Images"){echo "Images";}else{echo $get_type_of_tagged_media['name'];}}else{?>&nbsp;<?php } ?></div>
												<div class="blkI" style="width:127px;"><?php
												$exp_creator_info = explode("|",$combine_arr[$test_count][$newtest_count]['creator_info']);
												$pro_url = $new_profile_media_classobj->get_creator_url($exp_creator_info[0],$exp_creator_info[1]);
												?>
												<a href="<?php if($pro_url['profile_url'] !=""){ echo "/".$pro_url['profile_url']; }else { echo "javascript:void(0)"; }?>">
												<?php if($combine_arr[$test_count][$newtest_count]['creator']!=""){ 
												//$endpos = strpos($combine_arr[$test_count][$newtest_count]['creator'],"("); 
												//if($endpos !="" || $endpos !=false) { 
												//echo substr($combine_arr[$test_count][$newtest_count]['creator'],0,$endpos);} else { 
												echo $combine_arr[$test_count][$newtest_count]['creator']; 
												}else{?>&nbsp;<?php } ?></a></div>
												<div class="blkK"><?php
												$exp_from_info = explode("|",$combine_arr[$test_count][$newtest_count]['from_info']);
												$pro_url_from = $new_profile_media_classobj->get_from_url($exp_from_info[0],$exp_from_info[1]);
												?>
												<a href="<?php if($pro_url_from['profile_url'] !=""){ echo "/".$pro_url_from['profile_url']; }else { echo "javascript:void(0)"; }?>">
												<?php if($combine_arr[$test_count][$newtest_count]['from']!=""){
												//$endpos = strpos($combine_arr[$test_count][$newtest_count]['from'],"("); 
												//if($endpos !="" || $endpos != false) { 
												//echo substr($combine_arr[$test_count][$newtest_count]['from'],0,$endpos);} else { 
												echo $combine_arr[$test_count][$newtest_count]['from'];  
												}else{?>&nbsp;<?php } ?>
												</a></div>
												<div class="blkB"><?php
												if($combine_arr[$test_count][$newtest_count]['title']!="")
												{
													if($combine_arr[$test_count][$newtest_count]['media_type'] == 114)
													{
														//$get_media_track = $new_profile_media_classobj->get_media_track($combine_arr[$test_count][$newtest_count]['id']); 
														if($combine_arr[$test_count][$newtest_count]['track'] != "" && $combine_arr[$test_count][$newtest_count]['track'] != NULL && $combine_arr[$test_count][$newtest_count]['track'] !=" "){
															echo $combine_arr[$test_count][$newtest_count]['track'].".".stripslashes($combine_arr[$test_count][$newtest_count]['title']);
														}else{
															echo stripslashes($combine_arr[$test_count][$newtest_count]['title']);
														}
													}
													else{
													echo stripslashes($combine_arr[$test_count][$newtest_count]['title']); }
												}else{
												?>&nbsp;<?php
												}
													?></div>
												<div class="blkplayer">
													<div class="player">
													<?php 
														if($combine_arr[$test_count][$newtest_count]['media_type']==113){
															$get_images_tag = $new_profile_media_classobj ->get_all_gallery_images($combine_arr[$test_count][$newtest_count]['id']);
															for($count_img=0;$count_img<=count($get_images_tag);$count_img++){
																if($get_all_img_tag==""){
																	$get_all_img_tag = $get_images_tag[$count_img]['image_name'];
																}else{
																	$get_all_img_tag = $get_all_img_tag.",".$get_images_tag[$count_img]['image_name'];
																}
																if($galleryImagestitle_tag ==""){
																	$galleryImagestitle_tag = $get_images_tag[$count_img]['image_title'];
																}else{
																	$galleryImagestitle_tag = $galleryImagestitle_tag.",".$get_images_tag[$count_img]['image_title'];
																}
																
																$mediaSrc_tag = $get_images_tag[0]['image_name'];
															}
															$gal_id_tag = $get_images_tag[$count_img]['media_id'];
															$get_other_user_id = $user_keys_data->get_general_user_idby_mediaid($combine_arr[$test_count][$newtest_count]['id']);
															$orgPath_tag = $s3->getNewURI_otheruser("medgallery",$get_other_user_id);//"https://medgallery.s3.amazonaws.com/";
															$thumbPath_tag = $s3->getNewURI_otheruser("medgalthumb",$get_other_user_id);//"https://medgalthumb.s3.amazonaws.com/";
														?>
														<a href="javascript:void(0);"><img title="Play file now." src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" onclick="checkload('<?php echo $mediaSrc_tag;?>','<?php echo $get_all_img_tag; ?>','<?php echo $thumbPath_tag; ?>','<?php echo $orgPath_tag; ?>','<?php echo $galleryImagestitle_tag; ?>','<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>')"/></a>
														<!--<a href="javascript:void(0);"><img title="Add file to the bottom of your playlist without stopping current media." src="images/profile/add.gif"  onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'"/></a>-->
														<?php
															$get_all_img_tag ="";
															$galleryImagestitle_tag="";
															$mediaSrc_tag="";
														}else if($combine_arr[$test_count][$newtest_count]['media_type']==114){
														?>
														<a href="javascript:void(0);"><img title="Play file now." src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" onclick="showPlayer('a','<?php if($combine_arr[$test_count][$newtest_count]['id']!=""){echo $combine_arr[$test_count][$newtest_count]['id'];}else{echo $combine_arr[$test_count][$newtest_count]['id'];}?>','play');"/></a>
														<a href="javascript:void(0);"><img title="Add file to the bottom of your playlist without stopping current media." src="images/profile/add.gif"  onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'" onclick="showPlayer('a','<?php if($combine_arr[$test_count][$newtest_count]['id']!=""){echo $combine_arr[$test_count][$newtest_count]['id'];}else{echo $combine_arr[$test_count][$newtest_count]['id'];}?>','add');"/></a>
														<?php
														}else if($combine_arr[$test_count][$newtest_count]['media_type']==115){
														?>
														<a href="javascript:void(0);"><img title="Play file now." src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" onclick="showPlayer('v','<?php if($combine_arr[$test_count][$newtest_count]['id']!=""){echo $combine_arr[$test_count][$newtest_count]['id'];}else{ echo $combine_arr[$test_count][$newtest_count]['id'];}?>','playlist');"/></a>
														<a href="javascript:void(0);"><img title="Add file to the bottom of your playlist without stopping current media." src="images/profile/add.gif"  onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'" onclick="showPlayer('v','<?php if($combine_arr[$test_count][$newtest_count]['id']!=""){echo $combine_arr[$test_count][$newtest_count]['id'];}else{ echo $combine_arr[$test_count][$newtest_count]['id'];}?>','addvi');"/></a>
														<?php
														}else if($combine_arr[$test_count][$newtest_count]['media_type'] == 116){
															$get_channel_media = $new_profile_media_classobj ->get_all_media_selected_channel($combine_arr[$test_count][$newtest_count]['id']);
														?>
															<input type="hidden" id="channel_media_song<?php echo $newtest_count;?>" value="<?php echo $get_channel_media['songs']; ?>"/>
															<input type="hidden" id="channel_media_video<?php echo $newtest_count;?>" value="<?php echo $get_channel_media['videos']; ?>"/>
															<a href="javascript:void(0);"><img title="Play file now." src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" onclick="PlayAll('channel_media_video<?php echo $newtest_count;?>','channel_media_song<?php echo $newtest_count;?>')"/></a>
															<a href="javascript:void(0);"><img title="Add file to the bottom of your playlist without stopping current media." src="images/profile/add.gif"  onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'" onclick="showPlayer('v','<?php if($combine_arr[$test_count][$newtest_count]['id']!=""){echo $combine_arr[$test_count][$newtest_count]['id'];}else{ echo $combine_arr[$test_count][$newtest_count]['id'];}?>','addvi');"/></a>
														<?php
														}
														else{
														?>
														<a href="javascript:void(0);"><img title="Play file now." src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" /></a>
														<a href="javascript:void(0);"><img title="Add file to the bottom of your playlist without stopping current media." src="images/profile/add.gif"  onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'"/></a>
														<?php
														}
														
														if(($combine_arr[$test_count][$newtest_count]['sharing_preference']==5 || $combine_arr[$test_count][$newtest_count]['sharing_preference']==2) && $combine_arr[$test_count][$newtest_count]['media_type'] == 114){
															?>
															<a onclick="download_directly_song('<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['title']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['creator']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['from']);?>')" href="javascript:void(0);">
															<img title="Add media to your cart for free download or purchase." onmouseout="this.src='../images/profile/download_down.gif'" onmouseover="this.src='../images/profile/download-over_down.gif'" src="../images/profile/download_down.gif"></a>
														<?php
														}/* else{
														?>
														<img src="images/profile/download-over_down.gif" />
														<?php
														} */
														?>
													</div>
												</div>
												<div class="icon" onclick="runAccordion(<?php echo $combine_arr[$test_count][$newtest_count]['id']; ?>);">
													<div onselectstart="return false;"><img title="View details for media." src="images/profile/view.jpg" /></div>
												</div>
												<div class="icon">&nbsp;</div>
												<div class="icon"><a href="delete_media.php?id=<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>&type=<?php if($get_type_of_tagged_media['name'] =="Galleries/Images"){echo "Images";}else{echo $get_type_of_tagged_media['name'];}?>" onclick="return confirmdelete('<?php echo $combine_arr[$test_count][$newtest_count]['title'];?>','<?php if($combine_arr[$test_count][$newtest_count]['name']=="Galleries/Images"){echo "Images";}else{echo $combine_arr[$test_count][$newtest_count]['name'];} ?>')"><img title="Remove media from your library." src="images/profile/delete.png" /></a></div>
												<div class="expandable" id="Accordion<?php echo $combine_arr[$test_count][$newtest_count]['id']; ?>Content">
													<div class="affiBlock">
														<div class="RowCont">
															<div class="sideL">Plays :</div>
															<div class="sideR"><a href="" target="_blank"><?php if($combine_arr[$test_count][$newtest_count]['play_count']!=""){ echo $combine_arr[$test_count][$newtest_count]['play_count'];}else{ echo "0";}?></a></div>
														</div>
														<?php
														if($combine_arr[$test_count][$newtest_count]['sharing_preference']==5 || $combine_arr[$test_count][$newtest_count]['sharing_preference']==2){
														?>
														<div class="RowCont">
															<div class="sideL">Downloads :</div>
															<div class="sideR"><a href="javascript:void(0);"><?php echo $combine_arr[$test_count][$newtest_count]['download_count'];?></a></div>
														</div>
														<?php
														}
															if($combine_arr[$test_count][$newtest_count]['media_type']=='113')
															{
															
															}
															else
															{
														?>	
														<!--<div class="RowCont">
															<div class="sideL">Media URL :</div>
															<div class="sideR"><a href="" target="_blank">www.<?php echo $domainname; ?></a></div>
														</div>-->
														<?php
															}
														?>
													</div>
												</div>
											</div>
									<?php
										}
									}
								}
								else if($combine_arr[$test_count][$newtest_count]['media_from']=="newedit_media")
								{
									$chk_tag_user = $new_profile_media_classobj->get_loggedin_info($_SESSION['login_email']);
									if($chk_tag_user['general_user_id']!=$combine_arr[$test_count][$newtest_count]['general_user_id'])
									{
									?>
										<div class="tableCont" id="AccordionContainer">
										<div class="blkD" style="width:60px;"><?php if($combine_arr[$test_count][$newtest_count]['name']!=""){ if($combine_arr[$test_count][$newtest_count]['name']=="Galleries/Images"){ echo "Images";}else {echo $combine_arr[$test_count][$newtest_count]['name'];}}else{?>&nbsp;<?php } ?></div>
										<div class="blkI" style="width:127px;"><?php
										$exp_creator_info = explode("|",$combine_arr[$test_count][$newtest_count]['creator_info']);
										$pro_url = $new_profile_media_classobj->get_creator_url($exp_creator_info[0],$exp_creator_info[1]);
										?>
										<a href="<?php if($pro_url['profile_url'] !=""){ echo "/".$pro_url['profile_url']; }else { echo "javascript:void(0)"; }?>">
										<?php if($combine_arr[$test_count][$newtest_count]['creator']!=""){
										//$endpos = strpos($combine_arr[$test_count][$newtest_count]['creator'],"("); 
										//if($endpos !="" || $endpos !=false) { 
										//echo substr($combine_arr[$test_count][$newtest_count]['creator'],0,$endpos);} else { 
										echo $combine_arr[$test_count][$newtest_count]['creator']; 
										}else{?>&nbsp;<?php } ?></a></div>
										<div class="blkK"><?php
										$exp_from_info = explode("|",$combine_arr[$test_count][$newtest_count]['from_info']);
										$pro_url_from = $new_profile_media_classobj->get_from_url($exp_from_info[0],$exp_from_info[1]);
										?>
										<a href="<?php if($pro_url_from['profile_url'] !=""){ echo "/".$pro_url_from['profile_url']; }else { echo "javascript:void(0)"; }?>">
										<?php if($combine_arr[$test_count][$newtest_count]['from']!=""){ 
										//$endpos = strpos($combine_arr[$test_count][$newtest_count]['from'],"("); 
										//if($endpos !="" || $endpos != false) { 
										//echo substr($combine_arr[$test_count][$newtest_count]['from'],0,$endpos);} else { 
										echo $combine_arr[$test_count][$newtest_count]['from'];
										}else{?>&nbsp;<?php } ?></a></div>
										<!--<div class="blkK"><a href="gallery_images.php?gal_id=<?php //echo $res['id'];?>" id="gallery_images<?php //echo $k;?>">Edit Image</a></div>
										<div class="blkB"><a href="add_gallery_images.php?gal_id=<?php echo $res['id'];?>" id="Add_gallery_images<?php echo $k;?>">Add Image</a></div>-->
										<!--<div class="blkJ"><a href=""><img src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" /></a><a href=""><img src="images/profile/add.gif"  onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'"/></a><a href=""><img src="images/profile/download_down.gif" onmouseover="this.src='images/profile/download-over_down.gif'" onmouseout="this.src='images/profile/download_down.gif'" /></a></div>-->
										<div class="icon" onclick="runAccordion(<?php echo $combine_arr[$test_count][$newtest_count]['id']; ?>);">
											<div onselectstart="return false;"><img title="View details for media." src="images/profile/view.jpg" /></div>
										</div>
										<div class="icon"><a href="add_media.php?edit=<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>" onclick="save_cookie()"><img title="Edit media." src="images/profile/edit.png" /></a></div>
										<div class="icon"><a onclick="return confirmdelete('<?php echo $combine_arr[$test_count][$newtest_count]['title'];?>','<?php if($combine_arr[$test_count][$newtest_count]['name']=="Galleries/Images"){echo "Images";}else{echo $combine_arr[$test_count][$newtest_count]['name'];} ?>')" href="delete_media.php?id=<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>&whos_media=newedit_media"><img title="Remove media from your library." src="images/profile/delete.png" /></a></div>
										<div class="expandable" id="Accordion<?php echo $combine_arr[$test_count][$newtest_count]['id']; ?>Content">
											<div class="affiBlock">
												<div class="RowCont">
													<div class="sideL">Plays :</div>
													<div class="sideR"><a href="" target="_blank"><?php if($combine_arr[$test_count][$newtest_count]['play_count']!=""){ echo $combine_arr[$test_count][$newtest_count]['play_count'];}else{ echo "0";}?></a></div>
												</div>
													<?php
													if($combine_arr[$test_count][$newtest_count]['sharing_preference']==5 || $combine_arr[$test_count][$newtest_count]['sharing_preference']==2){
													?>
													<div class="RowCont">
														<div class="sideL">Downloads :</div>
														<div class="sideR"><a href="javascript:void(0);"><?php echo $combine_arr[$test_count][$newtest_count]['download_count'];?></a></div>
													</div>
													<?php
													}
													if($combine_arr[$test_count][$newtest_count]['media_type']=='113')
													{
													
													}
													else
													{
												?>	
												<!--<div class="RowCont">
													<div class="sideL">Media URL :</div>
													<div class="sideR"><a href="" target="_blank">www.<?php echo $domainname; ?></a></div>
												</div>-->
												<?php
													}
												?>
											</div>
										</div>
									</div>
									<?php
										$k=$k+1;
									}
								}
								else if($combine_arr[$test_count][$newtest_count]['media_from']=="media_creator")
								{
									?>
										<div class="tableCont" id="AccordionContainer">
										<div class="blkD" style="width:60px;"><?php if($combine_arr[$test_count][$newtest_count]['name']=="Galleries/Images"){echo "Images";}else{echo $combine_arr[$test_count][$newtest_count]['name'];} ?></div>
										<div class="blkI" style="width:127px;"><?php
										$exp_creator_info = explode("|",$combine_arr[$test_count][$newtest_count]['creator_info']);
										$pro_url = $new_profile_media_classobj->get_creator_url($exp_creator_info[0],$exp_creator_info[1]);
										?>
										<a href="<?php if($pro_url['profile_url'] !=""){ echo "/".$pro_url['profile_url']; }else { echo "javascript:void(0)"; }?>">
										<?php if($combine_arr[$test_count][$newtest_count]['creator']!=""){
										//$endpos = strpos($combine_arr[$test_count][$newtest_count]['creator'],"("); 
										echo $combine_arr[$test_count][$newtest_count]['creator'];}
										else{?>&nbsp;<?php } ?></a></div>
										<div class="blkK"><?php
										$exp_from_info = explode("|",$combine_arr[$test_count][$newtest_count]['from_info']);
										$pro_url_from = $new_profile_media_classobj->get_from_url($exp_from_info[0],$exp_from_info[1]);
										?>
										<a href="<?php if($pro_url_from['profile_url'] !=""){ echo "/".$pro_url_from['profile_url']; }else { echo "javascript:void(0)"; }?>">
										<?php if($combine_arr[$test_count][$newtest_count]['from']!=""){ 
										//$endpos = strpos($combine_arr[$test_count][$newtest_count]['from'],"("); 
										echo $combine_arr[$test_count][$newtest_count]['from'];}
										else{?>&nbsp;<?php } ?></a></div>
										<div class="blkB"><?php
												if($combine_arr[$test_count][$newtest_count]['title']!="")
												{
													if($combine_arr[$test_count][$newtest_count]['media_type'] == 114)
													{
														//$get_media_track = $new_profile_media_classobj->get_media_track($combine_arr[$test_count][$newtest_count]['id']); 
														if($combine_arr[$test_count][$newtest_count]['track'] != "" && $combine_arr[$test_count][$newtest_count]['track'] != NULL && $combine_arr[$test_count][$newtest_count]['track'] !=" "){
															echo $combine_arr[$test_count][$newtest_count]['track'].".".stripslashes($combine_arr[$test_count][$newtest_count]['title']);
														}else{
															echo stripslashes($combine_arr[$test_count][$newtest_count]['title']);
														}
													}
													else{
													echo stripslashes($combine_arr[$test_count][$newtest_count]['title']); }
												}else{
												?>&nbsp;<?php
												}
													?></div>
										<?php
										if($combine_arr[$test_count][$newtest_count]['media_type']==114)
										{
										?>
										<div class="blkplayer">
											<div class="player">
												<a href="javascript:void(0);"><img onclick="showPlayer('a','<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','play');" onmouseout="this.src='images/profile/play.gif'" onmouseover="this.src='images/profile/play-over.gif'" src="images/profile/play.gif" title="Play file now."></a>
												<a href="javascript:void(0);"><img onclick="showPlayer('a','<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','add');" onmouseout="this.src='images/profile/add.gif'" onmouseover="this.src='images/profile/add-over.gif'" src="images/profile/add.gif" title="Add file to the bottom of your playlist without stopping current media."></a>
												<?php
												if(($combine_arr[$test_count][$newtest_count]['sharing_preference']==5 || $combine_arr[$test_count][$newtest_count]['sharing_preference']==2) && $combine_arr[$test_count][$newtest_count]['media_type'] == 114){
													?>
													<a onclick="download_directly_song('<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['title']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['creator']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['from']);?>')" href="javascript:void(0);">
													<img title="Add media to your cart for free download or purchase." onmouseout="this.src='../images/profile/download_down.gif'" onmouseover="this.src='../images/profile/download-over_down.gif'" src="../images/profile/download_down.gif"></a>
												<?php
												}/* else{
												?>
												<img src="images/profile/download-over_down.gif" />
												<?php
												} */
												?>
											</div>
										</div>
										<?php
										}else if($combine_arr[$test_count][$newtest_count]['media_type']==115)
										{
										?>
										<div class="blkplayer">
											<div class="player">
												<a href="javascript:void(0);"><img onclick="showPlayer('v','<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','playlist');" onmouseout="this.src='images/profile/play.gif'" onmouseover="this.src='images/profile/play-over.gif'" src="images/profile/play.gif" title="Play file now."></a>
												<a href="javascript:void(0);"><img onclick="showPlayer('v','<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','addvi');" onmouseout="this.src='images/profile/add.gif'" onmouseover="this.src='images/profile/add-over.gif'" src="images/profile/add.gif" title="Add file to the bottom of your playlist without stopping current media."></a>
												<?php
												if(($combine_arr[$test_count][$newtest_count]['sharing_preference']==5 || $combine_arr[$test_count][$newtest_count]['sharing_preference']==2) && $combine_arr[$test_count][$newtest_count]['media_type'] == 114){
													?>
													<a onclick="download_directly_song('<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['title']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['creator']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['from']);?>')" href="javascript:void(0);">
													<img title="Add media to your cart for free download or purchase." onmouseout="this.src='../images/profile/download_down.gif'" onmouseover="this.src='../images/profile/download-over_down.gif'" src="../images/profile/download_down.gif"></a>
												<?php
												}/* else{
												?>
												<img src="images/profile/download-over_down.gif" />
												<?php
												} */
												?>
											</div>
											</div>
										<?php
										}else if($combine_arr[$test_count][$newtest_count]['media_type']==113){
											$get_images_fan = $new_profile_media_classobj ->get_all_gallery_images($combine_arr[$test_count][$newtest_count]['id']);
											for($count_img=0;$count_img<=count($get_images_fan);$count_img++){
												if($get_all_img_fan ==""){
													$get_all_img_fan = $get_images_fan[$count_img]['image_name'];
												}else{
													$get_all_img_fan = $get_all_img_fan.",".$get_images_fan[$count_img]['image_name'];
												}
												if($galleryImagestitle_fan == ""){
													$galleryImagestitle_fan = $get_images_fan[$count_img]['image_title'];
												}else{
													$galleryImagestitle_fan = $galleryImagestitle_fan.",".$get_images_fan[$count_img]['image_title'];
												}
												
												$mediaSrc_fan = $get_images_fan[0]['image_name'];
											}
											$gal_id_fan = $get_images_fan[$count_img]['media_id'];
											$get_other_user_id = $user_keys_data->get_general_user_idby_mediaid($combine_arr[$test_count][$newtest_count]['id']);
											$orgPath_fan = $s3->getNewURI_otheruser("medgallery",$get_other_user_id);//"https://medgallery.s3.amazonaws.com/";
											$thumbPath_fan = $s3->getNewURI_otheruser("medgalthumb",$get_other_user_id);//"https://medgalthumb.s3.amazonaws.com/";

										?>
										<div class="blkplayer">
											<div class="player">
											<a href="javascript:void(0);"><img title="Play file now." src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" onclick="checkload('<?php echo $mediaSrc_fan;?>','<?php echo $get_all_img_fan; ?>','<?php echo $thumbPath_fan; ?>','<?php echo $orgPath_fan; ?>','<?php echo $galleryImagestitle_fan; ?>','<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>')"/></a>
											<!--<a href="javascript:void(0);"><img title="Add file to the bottom of your playlist without stopping current media." src="images/profile/add.gif"  onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'"/></a>-->
											<?php
											if(($combine_arr[$test_count][$newtest_count]['sharing_preference']==5 || $combine_arr[$test_count][$newtest_count]['sharing_preference']==2) && $combine_arr[$test_count][$newtest_count]['media_type'] == 114){
												?>
												<a onclick="download_directly_song('<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['title']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['creator']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['from']);?>')" href="javascript:void(0);">
												<img title="Add media to your cart for free download or purchase." onmouseout="this.src='../images/profile/download_down.gif'" onmouseover="this.src='../images/profile/download-over_down.gif'" src="../images/profile/download_down.gif"></a>
											<?php
											}/* else{
											?>
											<img src="images/profile/download-over_down.gif" />
											<?php
											} */
											?>
											</div>
											</div>
										<?php
											$get_all_img_fan ="";
											$galleryImagestitle_fan="";
											$mediaSrc_fan ="";
										}else if($combine_arr[$test_count][$newtest_count]['media_type'] == 116){
											$get_channel_media = $new_profile_media_classobj ->get_all_media_selected_channel($combine_arr[$test_count][$newtest_count]['id']);
										?>
											<div class="blkplayer"><div class="player">
											<input type="hidden" id="channel_media_song<?php echo $newtest_count;?>" value="<?php echo $get_channel_media['songs']; ?>"/>
											<input type="hidden" id="channel_media_video<?php echo $newtest_count;?>" value="<?php echo $get_channel_media['videos']; ?>"/>
											<a href="javascript:void(0);"><img title="Play file now." src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" onclick="PlayAll('channel_media_video<?php echo $newtest_count;?>','channel_media_song<?php echo $newtest_count;?>')"/></a>
											<a href="javascript:void(0);"><img title="Add file to the bottom of your playlist without stopping current media." src="images/profile/add.gif"  onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'" onclick="showPlayer('v','<?php if($combine_arr[$test_count][$newtest_count]['id']!=""){echo $combine_arr[$test_count][$newtest_count]['id'];}else{ echo $combine_arr[$test_count][$newtest_count]['id'];}?>','addvi');"/></a>
											<?php
											if(($combine_arr[$test_count][$newtest_count]['sharing_preference']==5 || $combine_arr[$test_count][$newtest_count]['sharing_preference']==2) && $combine_arr[$test_count][$newtest_count]['media_type'] == 114){
												?>
												<a onclick="download_directly_song('<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['title']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['creator']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['from']);?>')" href="javascript:void(0);">
												<img title="Add media to your cart for free download or purchase." onmouseout="this.src='../images/profile/download_down.gif'" onmouseover="this.src='../images/profile/download-over_down.gif'" src="../images/profile/download_down.gif"></a>
											<?php
											}/* else{
											?>
											<img src="images/profile/download-over_down.gif" />
											<?php
											} */
											?>
											</div>
											</div>
										<?php
										}
										else{
										?>
										<div class="blkplayer">
											<div class="player">
											<a href="javascript:void(0);"><img title="Play file now." src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" /></a>
											<a href="javascript:void(0);"><img title="Add file to the bottom of your playlist without stopping current media." src="images/profile/add.gif"  onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'"/></a>
											<?php
											if(($combine_arr[$test_count][$newtest_count]['sharing_preference']==5 || $combine_arr[$test_count][$newtest_count]['sharing_preference']==2) && $combine_arr[$test_count][$newtest_count]['media_type'] == 114){
												?>
												<a onclick="download_directly_song('<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['title']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['creator']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['from']);?>')" href="javascript:void(0);">
												<img title="Add media to your cart for free download or purchase." onmouseout="this.src='../images/profile/download_down.gif'" onmouseover="this.src='../images/profile/download-over_down.gif'" src="../images/profile/download_down.gif"></a>
											<?php
											}/* else{
											?>
											<img src="images/profile/download-over_down.gif" />
											<?php
											} */
											?>
											</div>
											</div>
										<?php
										
										}
										?>
										<div class="icon" onclick="runAccordion(<?php echo $combine_arr[$test_count][$newtest_count]['id']; ?>);">
											<div onselectstart="return false;"><img title="View details for media." src="images/profile/view.jpg" /></div>
										</div>
										<div class="icon"><!--<a href="add_media.php?edit=<?php// echo $combine_arr[$test_count][$newtest_count]['id'];?>"><img title="Edit media." src="images/profile/edit.png" /></a>--></div>
										<div class="icon"><a onclick="return confirmdelete('<?php echo $combine_arr[$test_count][$newtest_count]['title'];?>','<?php if($combine_arr[$test_count][$newtest_count]['name']=="Galleries/Images"){echo "Images";}else{echo $combine_arr[$test_count][$newtest_count]['name'];} ?>')" href="delete_media.php?id=<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>&whos_media=media_creator"><img title="Remove media from your library." src="images/profile/delete.png" /></a></div>
										<div class="expandable" id="Accordion<?php echo $combine_arr[$test_count][$newtest_count]['id']; ?>Content">
											<div class="affiBlock">
												<div class="RowCont">
													<div class="sideL">Plays :</div>
													<div class="sideR"><a href="" target="_blank"><?php if($combine_arr[$test_count][$newtest_count]['play_count']!=""){ echo $combine_arr[$test_count][$newtest_count]['play_count'];}else{ echo "0";}?></a></div>
												</div>
												<?php
													if($combine_arr[$test_count][$newtest_count]['sharing_preference']==5 || $combine_arr[$test_count][$newtest_count]['sharing_preference']==2){
													?>
													<div class="RowCont">
														<div class="sideL">Downloads :</div>
														<div class="sideR"><a href="javascript:void(0);"><?php echo $combine_arr[$test_count][$newtest_count]['download_count'];?></a></div>
													</div>
													<?php
													}
													if($combine_arr[$test_count][$newtest_count]['media_type']=='113')
													{
													
													}
													else
													{
												?>	
												<!--<div class="RowCont">
													<div class="sideL">Media URL :</div>
													<div class="sideR"><a href="" target="_blank">www.<?php echo $domainname; ?></a></div>
												</div>-->
												<?php
													}
												?>
											</div>
										</div>
									</div>
									<?php
								}
								else if($combine_arr[$test_count][$newtest_count]['media_from']=="new_tagging_media")
								{
									?>
										<div class="tableCont" id="AccordionContainer">
										<div class="blkD" style="width:60px;"><?php if($combine_arr[$test_count][$newtest_count]['name']=="Galleries/Images"){echo "Images";}else{echo $combine_arr[$test_count][$newtest_count]['name'];} ?></div>
										<div class="blkI" style="width:127px;"><?php
										$exp_creator_info = explode("|",$combine_arr[$test_count][$newtest_count]['creator_info']);
										$pro_url = $new_profile_media_classobj->get_creator_url($exp_creator_info[0],$exp_creator_info[1]);
										?>
										<a href="<?php if($pro_url['profile_url'] !=""){ echo "/".$pro_url['profile_url']; }else { echo "javascript:void(0)"; }?>">
										<?php if($combine_arr[$test_count][$newtest_count]['creator']!=""){ 
										//$endpos = strpos($combine_arr[$test_count][$newtest_count]['creator'],"("); 
										//if($endpos !="" || $endpos !=false) { 
										//echo substr($combine_arr[$test_count][$newtest_count]['creator'],0,$endpos);} else { 
										echo $combine_arr[$test_count][$newtest_count]['creator']; }
										else{?>&nbsp;<?php } ?></a></div>
										<div class="blkK"><?php
										$exp_from_info = explode("|",$combine_arr[$test_count][$newtest_count]['from_info']);
										$pro_url_from = $new_profile_media_classobj->get_from_url($exp_from_info[0],$exp_from_info[1]);
										?>
										<a href="<?php if($pro_url_from['profile_url'] !=""){ echo "/".$pro_url_from['profile_url']; }else { echo "javascript:void(0)"; }?>">
										<?php if($combine_arr[$test_count][$newtest_count]['from']!=""){
										//$endpos = strpos($combine_arr[$test_count][$newtest_count]['from'],"("); 
										//if($endpos !="" || $endpos != false) { 
										//echo substr($combine_arr[$test_count][$newtest_count]['from'],0,$endpos);} else {
										echo $combine_arr[$test_count][$newtest_count]['from']; } 
										else{?>&nbsp;<?php } ?></a></div>
										<div class="blkB"><?php
												if($combine_arr[$test_count][$newtest_count]['title']!="")
												{
													if($combine_arr[$test_count][$newtest_count]['media_type'] == 114)
													{
														//$get_media_track = $new_profile_media_classobj->get_media_track($combine_arr[$test_count][$newtest_count]['id']); 
														if($combine_arr[$test_count][$newtest_count]['track'] != "" && $combine_arr[$test_count][$newtest_count]['track'] != NULL && $combine_arr[$test_count][$newtest_count]['track'] !=" "){
															echo $combine_arr[$test_count][$newtest_count]['track'].".".stripslashes($combine_arr[$test_count][$newtest_count]['title']);
														}else{
															echo stripslashes($combine_arr[$test_count][$newtest_count]['title']);
														}
													}
													else{
													echo stripslashes($combine_arr[$test_count][$newtest_count]['title']); }
												}else{
												?>&nbsp;<?php
												}
													?></div>
										<?php
										if($combine_arr[$test_count][$newtest_count]['media_type']==114)
										{
										?>
										<div class="blkplayer">
											<div class="player">
												<a href="javascript:void(0);"><img onclick="showPlayer('a','<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','play');" onmouseout="this.src='images/profile/play.gif'" onmouseover="this.src='images/profile/play-over.gif'" src="images/profile/play.gif" title="Play file now."></a>
												<a href="javascript:void(0);"><img onclick="showPlayer('a','<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','add');" onmouseout="this.src='images/profile/add.gif'" onmouseover="this.src='images/profile/add-over.gif'" src="images/profile/add.gif" title="Add file to the bottom of your playlist without stopping current media."></a>
												<?php
												if(($combine_arr[$test_count][$newtest_count]['sharing_preference']==5 || $combine_arr[$test_count][$newtest_count]['sharing_preference']==2) && $combine_arr[$test_count][$newtest_count]['media_type'] == 114){
													?>
													<a onclick="download_directly_song('<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['title']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['creator']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['from']);?>')" href="javascript:void(0);">
													<img title="Add media to your cart for free download or purchase." onmouseout="this.src='../images/profile/download_down.gif'" onmouseover="this.src='../images/profile/download-over_down.gif'" src="../images/profile/download_down.gif"></a>
												<?php
												}/* else{
												?>
												<img src="images/profile/download-over_down.gif" />
												<?php
												} */
												?>
											</div>
											</div>
										<?php
										}else if($combine_arr[$test_count][$newtest_count]['media_type']==115)
										{
										?>
										<div class="blkplayer">
											<div class="player">
												<a href="javascript:void(0);"><img onclick="showPlayer('v','<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','playlist');" onmouseout="this.src='images/profile/play.gif'" onmouseover="this.src='images/profile/play-over.gif'" src="images/profile/play.gif" title="Play file now."></a>
												<a href="javascript:void(0);"><img onclick="showPlayer('v','<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','addvi');" onmouseout="this.src='images/profile/add.gif'" onmouseover="this.src='images/profile/add-over.gif'" src="images/profile/add.gif" title="Add file to the bottom of your playlist without stopping current media."></a>
												<?php
												if(($combine_arr[$test_count][$newtest_count]['sharing_preference']==5 || $combine_arr[$test_count][$newtest_count]['sharing_preference']==2) && $combine_arr[$test_count][$newtest_count]['media_type'] == 114){
													?>
													<a onclick="download_directly_song('<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['title']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['creator']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['from']);?>')" href="javascript:void(0);">
													<img title="Add media to your cart for free download or purchase." onmouseout="this.src='../images/profile/download_down.gif'" onmouseover="this.src='../images/profile/download-over_down.gif'" src="../images/profile/download_down.gif"></a>
												<?php
												}/* else{
												?>
												<img src="images/profile/download-over_down.gif" />
												<?php
												} */
												?>
											</div>
											</div>
										<?php
										}else if($combine_arr[$test_count][$newtest_count]['media_type']==113){
											$get_images_fan = $new_profile_media_classobj ->get_all_gallery_images($combine_arr[$test_count][$newtest_count]['id']);
											for($count_img=0;$count_img<=count($get_images_fan);$count_img++){
												if($get_all_img_fan == ""){
													$get_all_img_fan = $get_images_fan[$count_img]['image_name'];
												}else{
													$get_all_img_fan = $get_all_img_fan.",".$get_images_fan[$count_img]['image_name'];
												}
												if($galleryImagestitle_fan == ""){
													$galleryImagestitle_fan = $get_images_fan[$count_img]['image_title'];
												}else{
													$galleryImagestitle_fan = $galleryImagestitle_fan.",".$get_images_fan[$count_img]['image_title'];
												}
												$mediaSrc_fan = $get_images_fan[0]['image_name'];
											}
											$gal_id_fan = $get_images_fan[$count_img]['media_id'];
											$get_other_user_id = $user_keys_data->get_general_user_idby_mediaid($combine_arr[$test_count][$newtest_count]['id']);
											$orgPath_fan = $s3->getNewURI_otheruser("medgallery",$get_other_user_id);//"https://medgallery.s3.amazonaws.com/";
											$thumbPath_fan = $s3->getNewURI_otheruser("medgalthumb",$get_other_user_id);//"https://medgalthumb.s3.amazonaws.com/";
										?>
										<div class="blkplayer">
											<div class="player">
											<a href="javascript:void(0);"><img title="Play file now." src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" onclick="checkload('<?php echo $mediaSrc_fan;?>','<?php echo $get_all_img_fan; ?>','<?php echo $thumbPath_fan; ?>','<?php echo $orgPath_fan; ?>','<?php echo $galleryImagestitle_fan; ?>','<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>')"/></a>
											<!--<a href="javascript:void(0);"><img title="Add file to the bottom of your playlist without stopping current media." src="images/profile/add.gif"  onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'"/></a>-->
											<?php
											if(($combine_arr[$test_count][$newtest_count]['sharing_preference']==5 || $combine_arr[$test_count][$newtest_count]['sharing_preference']==2) && $combine_arr[$test_count][$newtest_count]['media_type'] == 114){
												?>
												<a onclick="download_directly_song('<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['title']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['creator']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['from']);?>')" href="javascript:void(0);">
												<img title="Add media to your cart for free download or purchase." onmouseout="this.src='../images/profile/download_down.gif'" onmouseover="this.src='../images/profile/download-over_down.gif'" src="../images/profile/download_down.gif"></a>
											<?php
											}/* else{
											?>
											<img src="images/profile/download-over_down.gif" />
											<?php
											} */
											?>
											</div>
											</div>
										<?php
											$get_all_img_fan ="";
											$galleryImagestitle_fan="";
											$mediaSrc_fan ="";
										}else if($combine_arr[$test_count][$newtest_count]['media_type'] == 116){
											$get_channel_media = $new_profile_media_classobj ->get_all_media_selected_channel($combine_arr[$test_count][$newtest_count]['id']);
										?>
											<div class="blkplayer"><div class="player">
											<input type="hidden" id="channel_media_song<?php echo $newtest_count;?>" value="<?php echo $get_channel_media['songs']; ?>"/>
											<input type="hidden" id="channel_media_video<?php echo $newtest_count;?>" value="<?php echo $get_channel_media['videos']; ?>"/>
											<a href="javascript:void(0);"><img title="Play file now." src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" onclick="PlayAll('channel_media_video<?php echo $newtest_count;?>','channel_media_song<?php echo $newtest_count;?>')"/></a>
											<a href="javascript:void(0);"><img title="Add file to the bottom of your playlist without stopping current media." src="images/profile/add.gif"  onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'" onclick="showPlayer('v','<?php if($combine_arr[$test_count][$newtest_count]['id']!=""){echo $combine_arr[$test_count][$newtest_count]['id'];}else{ echo $combine_arr[$test_count][$newtest_count]['id'];}?>','addvi');"/></a>
											<?php
											if(($combine_arr[$test_count][$newtest_count]['sharing_preference']==5 || $combine_arr[$test_count][$newtest_count]['sharing_preference']==2) && $combine_arr[$test_count][$newtest_count]['media_type'] == 114){
												?>
												<a onclick="download_directly_song('<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['title']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['creator']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['from']);?>')" href="javascript:void(0);">
												<img title="Add media to your cart for free download or purchase." onmouseout="this.src='../images/profile/download_down.gif'" onmouseover="this.src='../images/profile/download-over_down.gif'" src="../images/profile/download_down.gif"></a>
											<?php
											}/* else{
											?>
											<img src="images/profile/download-over_down.gif" />
											<?php
											} */
											?>
											</div>
											</div>
										<?php
										}
										else{
										?>
										<div class="blkplayer">
											<div class="player">
											<a href="javascript:void(0);"><img title="Play file now." src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" /></a>
											<a href="javascript:void(0);"><img title="Add file to the bottom of your playlist without stopping current media." src="images/profile/add.gif"  onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'"/></a>
											<?php
											if(($combine_arr[$test_count][$newtest_count]['sharing_preference']==5 || $combine_arr[$test_count][$newtest_count]['sharing_preference']==2) && $combine_arr[$test_count][$newtest_count]['media_type'] == 114){
												?>
												<a onclick="download_directly_song('<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['title']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['creator']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['from']);?>')" href="javascript:void(0);">
												<img title="Add media to your cart for free download or purchase." onmouseout="this.src='../images/profile/download_down.gif'" onmouseover="this.src='../images/profile/download-over_down.gif'" src="../images/profile/download_down.gif"></a>
											<?php
											}/* else{
											?>
											<img src="images/profile/download-over_down.gif" />
											<?php
											} */
											?>
											</div>
											</div>
										<?php
										}
										?>
										<div class="icon" onclick="runAccordion(<?php echo $combine_arr[$test_count][$newtest_count]['id']; ?>);">
											<div onselectstart="return false;"><img title="View details for media." src="images/profile/view.jpg" /></div>
										</div>
										<div class="icon"><!--<a href="add_media.php?edit=<?php// echo $combine_arr[$test_count][$newtest_count]['id'];?>"><img title="Edit media." src="images/profile/edit.png" /></a>--></div>
										<div class="icon"><a onclick="return confirmdelete('<?php echo $combine_arr[$test_count][$newtest_count]['title'];?>','<?php if($combine_arr[$test_count][$newtest_count]['name']=="Galleries/Images"){echo "Images";}else{echo $combine_arr[$test_count][$newtest_count]['name'];} ?>')" href="delete_media.php?id=<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>&whos_media=new_tagging_media"><img title="Remove media from your library." src="images/profile/delete.png" /></a></div>
										<div class="expandable" id="Accordion<?php echo $combine_arr[$test_count][$newtest_count]['id']; ?>Content">
											<div class="affiBlock">
												<div class="RowCont">
													<div class="sideL">Plays :</div>
													<div class="sideR"><a href="" target="_blank"><?php if($combine_arr[$test_count][$newtest_count]['play_count']!=""){ echo $combine_arr[$test_count][$newtest_count]['play_count'];}else{ echo "0";}?></a></div>
												</div>
												<?php
													if($combine_arr[$test_count][$newtest_count]['sharing_preference']==5 || $combine_arr[$test_count][$newtest_count]['sharing_preference']==2){
													?>
													<div class="RowCont">
														<div class="sideL">Downloads :</div>
														<div class="sideR"><a href="javascript:void(0);"><?php echo $combine_arr[$test_count][$newtest_count]['download_count'];?></a></div>
													</div>
													<?php
													}
													if($combine_arr[$test_count][$newtest_count]['media_type']=='113')
													{
													
													}
													else
													{
												?>	
												<!--<div class="RowCont">
													<div class="sideL">Media URL :</div>
													<div class="sideR"><a href="" target="_blank">www.<?php echo $domainname; ?></a></div>
												</div>-->
												<?php
													}
												?>
											</div>
										</div>
									</div>
									<?php
								}
								else if($combine_arr[$test_count][$newtest_count]['media_from']=="email_subscription")
								{
									?>
										<div class="tableCont" id="AccordionContainer">
										<div class="blkD" style="width:60px;"><?php if($combine_arr[$test_count][$newtest_count]['name']=="Galleries/Images"){echo "Images";}else{echo $combine_arr[$test_count][$newtest_count]['name'];} ?></div>
										<div class="blkI" style="width:127px;"><?php
										$exp_creator_info = explode("|",$combine_arr[$test_count][$newtest_count]['creator_info']);
										$pro_url = $new_profile_media_classobj->get_creator_url($exp_creator_info[0],$exp_creator_info[1]);
										?>
										<a href="<?php if($pro_url['profile_url'] !=""){ echo "/".$pro_url['profile_url']; }else { echo "javascript:void(0)"; }?>">
										<?php if($combine_arr[$test_count][$newtest_count]['creator']!=""){
										//$endpos = strpos($combine_arr[$test_count][$newtest_count]['creator'],"(");
										//if($endpos !="" || $endpos !=false) { echo substr($combine_arr[$test_count][$newtest_count]['creator'],0,$endpos);} else {
										echo $combine_arr[$test_count][$newtest_count]['creator']; }
										else{?>&nbsp;<?php } ?></a></div>
										<div class="blkK"><?php
										$exp_from_info = explode("|",$combine_arr[$test_count][$newtest_count]['from_info']);
										$pro_url_from = $new_profile_media_classobj->get_from_url($exp_from_info[0],$exp_from_info[1]);
										?>
										<a href="<?php if($pro_url_from['profile_url'] !=""){ echo "/".$pro_url_from['profile_url']; }else { echo "javascript:void(0)"; }?>">
										<?php if($combine_arr[$test_count][$newtest_count]['from']!=""){
										//$endpos = strpos($combine_arr[$test_count][$newtest_count]['from'],"(");
										//if($endpos !="" || $endpos != false) {
										//echo substr($combine_arr[$test_count][$newtest_count]['from'],0,$endpos);} else {
										echo $combine_arr[$test_count][$newtest_count]['from']; }
										else{?>&nbsp;<?php } ?></a></div>
										<div class="blkB"><?php
												if($combine_arr[$test_count][$newtest_count]['title']!="")
												{
													if($combine_arr[$test_count][$newtest_count]['media_type'] == 114)
													{
														//$get_media_track = $new_profile_media_classobj->get_media_track($combine_arr[$test_count][$newtest_count]['id']); 
														if($combine_arr[$test_count][$newtest_count]['track'] != "" && $combine_arr[$test_count][$newtest_count]['track'] != NULL && $combine_arr[$test_count][$newtest_count]['track'] !=" "){
															echo $combine_arr[$test_count][$newtest_count]['track'].".".stripslashes($combine_arr[$test_count][$newtest_count]['title']);
														}else{
															echo stripslashes($combine_arr[$test_count][$newtest_count]['title']);
														}
													}
													else{
													echo stripslashes($combine_arr[$test_count][$newtest_count]['title']); }
												}else{
												?>&nbsp;<?php
												}
													?></div>
										<?php
										if($combine_arr[$test_count][$newtest_count]['media_type']==114)
										{
										?>
										<div class="blkplayer">
											<div class="player">
												<a href="javascript:void(0);"><img onclick="showPlayer('a','<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','play');" onmouseout="this.src='images/profile/play.gif'" onmouseover="this.src='images/profile/play-over.gif'" src="images/profile/play.gif" title="Play file now."></a>
												<a href="javascript:void(0);"><img onclick="showPlayer('a','<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','add');" onmouseout="this.src='images/profile/add.gif'" onmouseover="this.src='images/profile/add-over.gif'" src="images/profile/add.gif" title="Add file to the bottom of your playlist without stopping current media."></a>
												<?php
												if($combine_arr[$test_count][$newtest_count]['sharing_preference']==5 || $combine_arr[$test_count][$newtest_count]['sharing_preference']==2){
													$song_image_name = $new_profile_media_classobj->get_audio_img($combine_arr[$test_count][$newtest_count]['id']);
													if(($combine_arr[$test_count][$newtest_count]['sharing_preference']==5) && $combine_arr[$test_count][$newtest_count]['media_type'] == 114)
													{
													?>
													<a href="javascript:void(0);" onclick="add_tocart(<?php echo $newtest_count;?>)"><img src="../images/profile/download_down.gif" onmouseover="this.src='../images/profile/download-over_down.gif'" onmouseout="this.src='../images/profile/download_down.gif'" title="Add media to your cart for free download or purchase." /></a>
													<?php
													}
													else if(($combine_arr[$test_count][$newtest_count]['sharing_preference']==2) && $combine_arr[$test_count][$newtest_count]['media_type'] == 114)
													{
														$tip_add_aftr_detils = $display->get_tip_details_for_every($combine_arr[$test_count][$newtest_count]['id'],'media');
													?>
													<a href="javascript:void(0);" onclick="ask_to_donate('<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['title']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['creator']);?>','<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','add_to_cart_media<?php echo $newtest_count;?>','<?php echo $combine_arr[$test_count][$newtest_count]['general_user_id']; ?>','<?php echo $tip_add_aftr_detils; ?>')"><img src="../images/profile/download_down.gif" onmouseover="this.src='../images/profile/download-over_down.gif'" onmouseout="this.src='../images/profile/download_down.gif'" title="Add media to your cart for free download or purchase." /></a>
													<?php
													}
												}
												/* else{
												?>
												<img src="images/profile/download-over_down.gif" />
												<?php
												} */
												?>
												<a href="addtocart.php?seller_id=<?php echo $combine_arr[$test_count][$newtest_count]['general_user_id'];?>&media_id=<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>&buyer_id=<?php echo $_SESSION['gen_user_id'];?>&title=<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['title']);?>&image_name=<?php echo $song_image_name;?>" id="add_to_cart_media<?php echo $newtest_count;?>" style="display:none;"></a>
												<script type="text/javascript">
												$(document).ready(function() {
													$("#add_to_cart_media<?php echo $newtest_count;?>").fancybox({
													'height'			: '75%',
													'width'				: '69%',
													'transitionIn'		: 'none',
													'transitionOut'		: 'none',
													'type'				: 'iframe'
													});
												});
												</script>
											</div>
											</div>
										<?php
										}else if($combine_arr[$test_count][$newtest_count]['media_type']==115)
										{
										?>
										<div class="blkplayer">
											<div class="player">
												<a href="javascript:void(0);"><img onclick="showPlayer('v','<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','playlist');" onmouseout="this.src='images/profile/play.gif'" onmouseover="this.src='images/profile/play-over.gif'" src="images/profile/play.gif" title="Play file now."></a>
												<a href="javascript:void(0);"><img onclick="showPlayer('v','<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','addvi');" onmouseout="this.src='images/profile/add.gif'" onmouseover="this.src='images/profile/add-over.gif'" src="images/profile/add.gif" title="Add file to the bottom of your playlist without stopping current media."></a>
												<?php
												if($combine_arr[$test_count][$newtest_count]['sharing_preference']==5 || $combine_arr[$test_count][$newtest_count]['sharing_preference']==2){
													$get_video_image = $new_profile_media_classobj->getvideoimage($combine_arr[$test_count][$newtest_count]['id']);
													$video_image_name = "https://img.youtube.com/vi/".$get_video_image."/default.jpg";
													if(($combine_arr[$test_count][$newtest_count]['sharing_preference']==5) && $combine_arr[$test_count][$newtest_count]['media_type'] == 114)
													{
													?>
													<a href="javascript:void(0);" onclick="add_tocart(<?php echo $newtest_count;?>)"><img src="../images/profile/download_down.gif" onmouseover="this.src='../images/profile/download-over_down.gif'" onmouseout="this.src='../images/profile/download_down.gif'" title="Add media to your cart for free download or purchase." /></a>
													<?php
													}
													else if(($combine_arr[$test_count][$newtest_count]['sharing_preference']==2) && $combine_arr[$test_count][$newtest_count]['media_type'] == 114)
													{
														$tip_add_aftr_detils = $display->get_tip_details_for_every($combine_arr[$test_count][$newtest_count]['id'],'media');
													?>
													<a href="javascript:void(0);" onclick="ask_to_donate('<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['title']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['creator']);?>','<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','add_to_cart_media<?php echo $newtest_count;?>','<?php echo $combine_arr[$test_count][$newtest_count]['general_user_id']; ?>','<?php echo $tip_add_aftr_detils; ?>')"><img src="../images/profile/download_down.gif" onmouseover="this.src='../images/profile/download-over_down.gif'" onmouseout="this.src='../images/profile/download_down.gif'" title="Add media to your cart for free download or purchase." /></a>
													<?php
													}
												}/* else{
												?>
												<img src="images/profile/download-over_down.gif" />
												<?php
												} */
												?>
												<a href="addtocart.php?seller_id=<?php echo $combine_arr[$test_count][$newtest_count]['general_user_id'];?>&media_id=<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>&buyer_id=<?php echo $_SESSION['gen_user_id'];?>&title=<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['title']);?>&image_name=<?php echo $video_image_name;?>" id="add_to_cart_media<?php echo $newtest_count;?>" style="display:none;"></a>
												<script type="text/javascript">
												$(document).ready(function() {
													$("#add_to_cart_media<?php echo $newtest_count;?>").fancybox({
													'height'			: '75%',
													'width'				: '69%',
													'transitionIn'		: 'none',
													'transitionOut'		: 'none',
													'type'				: 'iframe'
													});
												});
												</script>
											</div>
											</div>
										<?php
										}else if($combine_arr[$test_count][$newtest_count]['media_type']==113){
											$get_images_fan = $new_profile_media_classobj ->get_all_gallery_images($combine_arr[$test_count][$newtest_count]['id']);
											for($count_img=0;$count_img<=count($get_images_fan);$count_img++){
												if($get_all_img_fan == ""){
													$get_all_img_fan = $get_images_fan[$count_img]['image_name'];
												}else{
													$get_all_img_fan = $get_all_img_fan.",".$get_images_fan[$count_img]['image_name'];
												}
												if($galleryImagestitle_fan == ""){
													$galleryImagestitle_fan = $get_images_fan[$count_img]['image_title'];
												}else{
													$galleryImagestitle_fan = $galleryImagestitle_fan.",".$get_images_fan[$count_img]['image_title'];
												}
												$mediaSrc_fan = $get_images_fan[0]['image_name'];
											}
											$gal_id_fan = $get_images_fan[$count_img]['media_id'];
											$get_other_user_id = $user_keys_data->get_general_user_idby_mediaid($combine_arr[$test_count][$newtest_count]['id']);
											$orgPath_fan = $s3->getNewURI_otheruser("medgallery",$get_other_user_id);//"https://medgallery.s3.amazonaws.com/";
											$thumbPath_fan = $s3->getNewURI_otheruser("medgalthumb",$get_other_user_id);//"https://medgalthumb.s3.amazonaws.com/";
											
										?>
										<div class="blkplayer"><div class="player">
											<a href="javascript:void(0);"><img title="Play file now." src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" onclick="checkload('<?php echo $mediaSrc_fan;?>','<?php echo $get_all_img_fan; ?>','<?php echo $thumbPath_fan; ?>','<?php echo $orgPath_fan; ?>','<?php echo $galleryImagestitle_fan; ?>','<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>')"/></a>
											<!--<a href="javascript:void(0);"><img title="Add file to the bottom of your playlist without stopping current media." src="images/profile/add.gif"  onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'"/></a>-->
											<?php
												$get_gal_image_name  = $new_profile_media_classobj ->get_gallery_info2($combine_arr[$test_count][$newtest_count]['id']);
												if(($combine_arr[$test_count][$newtest_count]['sharing_preference']==2) && $combine_arr[$test_count][$newtest_count]['media_type'] == 114)
												{
													$tip_add_aftr_detils = $display->get_tip_details_for_every($combine_arr[$test_count][$newtest_count]['id'],'media');
													?>
													<a href="javascript:void(0);" onclick="ask_to_donate('<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['title']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['creator']);?>','<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','add_to_cart_media<?php echo $newtest_count;?>','<?php echo $combine_arr[$test_count][$newtest_count]['general_user_id']; ?>','<?php echo $tip_add_aftr_detils; ?>')"><img src="../images/profile/download_down.gif" onmouseover="this.src='../images/profile/download-over_down.gif'" onmouseout="this.src='../images/profile/download_down.gif'" title="Add media to your cart for free download or purchase." /></a>
													<?php
												}/* else{
												?>
												<img src="images/profile/download-over_down.gif" />
												<?php
												} */
												?>
												<a href="addtocart.php?seller_id=<?php echo $combine_arr[$test_count][$newtest_count]['general_user_id'];?>&media_id=<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>&buyer_id=<?php echo $_SESSION['gen_user_id'];?>&title=<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['title']);?>&image_name=<?php echo $get_gal_image_name;?>" id="add_to_cart_media<?php echo $newtest_count;?>" style="display:none;"></a>
												<script type="text/javascript">
												$(document).ready(function() {
													$("#add_to_cart_media<?php echo $newtest_count;?>").fancybox({
													'height'			: '75%',
													'width'				: '69%',
													'transitionIn'		: 'none',
													'transitionOut'		: 'none',
													'type'				: 'iframe'
													});
												});
												</script>
											</div>
											</div>
										<?php
											$get_all_img_fan ="";
											$galleryImagestitle_fan="";
											$mediaSrc_fan ="";
										}else if($combine_arr[$test_count][$newtest_count]['media_type'] == 116){
											$get_channel_media = $new_profile_media_classobj ->get_all_media_selected_channel($combine_arr[$test_count][$newtest_count]['id']);
										?>
											<div class="blkplayer"><div class="player">
											<input type="hidden" id="channel_media_song<?php echo $newtest_count;?>" value="<?php echo $get_channel_media['songs']; ?>"/>
											<input type="hidden" id="channel_media_video<?php echo $newtest_count;?>" value="<?php echo $get_channel_media['videos']; ?>"/>
											<a href="javascript:void(0);"><img title="Play file now." src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" onclick="PlayAll('channel_media_video<?php echo $newtest_count;?>','channel_media_song<?php echo $newtest_count;?>')"/></a>
											<a href="javascript:void(0);"><img title="Add file to the bottom of your playlist without stopping current media." src="images/profile/add.gif"  onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'" onclick="showPlayer('v','<?php if($combine_arr[$test_count][$newtest_count]['id']!=""){echo $combine_arr[$test_count][$newtest_count]['id'];}else{ echo $combine_arr[$test_count][$newtest_count]['id'];}?>','addvi');"/></a>
											<?php
											if(($combine_arr[$test_count][$newtest_count]['sharing_preference']==5 || $combine_arr[$test_count][$newtest_count]['sharing_preference']==2) && $combine_arr[$test_count][$newtest_count]['media_type'] == 114){
												?>
												<a onclick="download_directly_song('<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['title']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['creator']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['from']);?>')" href="javascript:void(0);">
												<img title="Add media to your cart for free download or purchase." onmouseout="this.src='../images/profile/download_down.gif'" onmouseover="this.src='../images/profile/download-over_down.gif'" src="../images/profile/download_down.gif"></a>
											<?php
											}/* else{
											?>
											<img src="images/profile/download-over_down.gif" />
											<?php
											} */
											?>
											</div>
											</div>
										<?php
										}
										else{
										?>
											<div class="blkplayer"><div class="player">
											<a href="javascript:void(0);"><img title="Play file now." src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" /></a>
											<a href="javascript:void(0);"><img title="Add file to the bottom of your playlist without stopping current media." src="images/profile/add.gif"  onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'"/></a>
											<?php
											if(($combine_arr[$test_count][$newtest_count]['sharing_preference']==5 || $combine_arr[$test_count][$newtest_count]['sharing_preference']==2) && $combine_arr[$test_count][$newtest_count]['media_type'] == 114){
												?>
												<a onclick="download_directly_song('<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['title']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['creator']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['from']);?>')" href="javascript:void(0);">
												<img title="Add media to your cart for free download or purchase." onmouseout="this.src='../images/profile/download_down.gif'" onmouseover="this.src='../images/profile/download-over_down.gif'" src="../images/profile/download_down.gif"></a>
											<?php
											}/* else{
											?>
											<img src="images/profile/download-over_down.gif" />
											<?php
											} */
											?>
											</div>
											</div>
										<?php
										}
										?>
										<div class="icon" onclick="runAccordion(<?php echo $combine_arr[$test_count][$newtest_count]['id']; ?>);">
											<div onselectstart="return false;"><img title="View details for media." src="images/profile/view.jpg" /></div>
										</div>
										<div class="icon"><!--<a href="add_media.php?edit=<?php// echo $combine_arr[$test_count][$newtest_count]['id'];?>"><img title="Edit media." src="images/profile/edit.png" /></a>--></div>
										<div class="icon"><a onclick="return confirmdelete('<?php echo $combine_arr[$test_count][$newtest_count]['title'];?>','<?php if($combine_arr[$test_count][$newtest_count]['name']=="Galleries/Images"){echo "Images";}else{echo $combine_arr[$test_count][$newtest_count]['name'];} ?>')" href="delete_media.php?id=<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>&whos_media=email_subscription"><img title="Remove media from your library." src="images/profile/delete.png" /></a></div>
										<div class="expandable" id="Accordion<?php echo $combine_arr[$test_count][$newtest_count]['id']; ?>Content">
											<div class="affiBlock">
												<div class="RowCont">
													<div class="sideL">Plays :</div>
													<div class="sideR"><a href="" target="_blank"><?php if($combine_arr[$test_count][$newtest_count]['play_count']!=""){ echo $combine_arr[$test_count][$newtest_count]['play_count'];}else{ echo "0";}?></a></div>
												</div>
												<?php
													if($combine_arr[$test_count][$newtest_count]['sharing_preference']==5 || $combine_arr[$test_count][$newtest_count]['sharing_preference']==2){
													?>
													<div class="RowCont">
														<div class="sideL">Downloads :</div>
														<div class="sideR"><a href="javascript:void(0);"><?php echo $combine_arr[$test_count][$newtest_count]['download_count'];?></a></div>
													</div>
													<?php
													}
													if($combine_arr[$test_count][$newtest_count]['media_type']=='113')
													{
													
													}
													else
													{
												?>	
												<!--<div class="RowCont">
													<div class="sideL">Media URL :</div>
													<div class="sideR"><a href="" target="_blank">www.<?php echo $domainname; ?></a></div>
												</div>-->
												<?php
													}
												?>
											</div>
										</div>
									</div>
									<?php
								}
								else if($combine_arr[$test_count][$newtest_count]['media_from']=="media_creator_creator")
								{
									?>
										<div class="tableCont" id="AccordionContainer">
										<div class="blkD" style="width:60px;"><?php if($combine_arr[$test_count][$newtest_count]['name']=="Galleries/Images"){echo "Images";}else{echo $combine_arr[$test_count][$newtest_count]['name'];} ?></div>
										<div class="blkI" style="width:127px;"><?php
										$exp_creator_info = explode("|",$combine_arr[$test_count][$newtest_count]['creator_info']);
										$pro_url = $new_profile_media_classobj->get_creator_url($exp_creator_info[0],$exp_creator_info[1]);
										?>
										<a href="<?php if($pro_url['profile_url'] !=""){ echo "/".$pro_url['profile_url']; }else { echo "javascript:void(0)"; }?>">
										<?php if($combine_arr[$test_count][$newtest_count]['creator']!=""){
										//$endpos = strpos($combine_arr[$test_count][$newtest_count]['creator'],"("); 
										//if($endpos !="" || $endpos !=false) {
										//echo substr($combine_arr[$test_count][$newtest_count]['creator'],0,$endpos);} else {
										echo $combine_arr[$test_count][$newtest_count]['creator']; }
										else{?>&nbsp;<?php } ?></a></div>
										<div class="blkK"><?php
										$exp_from_info = explode("|",$combine_arr[$test_count][$newtest_count]['from_info']);
										$pro_url_from = $new_profile_media_classobj->get_from_url($exp_from_info[0],$exp_from_info[1]);
										?>
										<a href="<?php if($pro_url_from['profile_url'] !=""){ echo "/".$pro_url_from['profile_url']; }else { echo "javascript:void(0)"; }?>">
										<?php if($combine_arr[$test_count][$newtest_count]['from']!=""){
										//$endpos = strpos($combine_arr[$test_count][$newtest_count]['from'],"(");
										//if($endpos !="" || $endpos != false) {
										//echo substr($combine_arr[$test_count][$newtest_count]['from'],0,$endpos);} else {
										echo $combine_arr[$test_count][$newtest_count]['from']; }
										else{?>&nbsp;<?php } ?></a></div>
										<div class="blkB"><?php
												if($combine_arr[$test_count][$newtest_count]['title']!="")
												{
													if($combine_arr[$test_count][$newtest_count]['media_type'] == 114)
													{
														//$get_media_track = $new_profile_media_classobj->get_media_track($combine_arr[$test_count][$newtest_count]['id']); 
														if($combine_arr[$test_count][$newtest_count]['track'] != "" && $combine_arr[$test_count][$newtest_count]['track'] != NULL && $combine_arr[$test_count][$newtest_count]['track'] !=" "){
															echo $combine_arr[$test_count][$newtest_count]['track'].".".stripslashes($combine_arr[$test_count][$newtest_count]['title']);
														}else{
															echo stripslashes($combine_arr[$test_count][$newtest_count]['title']);
														}
													}
													else{
													echo stripslashes($combine_arr[$test_count][$newtest_count]['title']); }
												}else{
												?>&nbsp;<?php
												}
													?></div>
										<?php
										if($combine_arr[$test_count][$newtest_count]['media_type']==114)
										{
										?>
											<div class="blkplayer"><div class="player">
												<a href="javascript:void(0);"><img onclick="showPlayer('a','<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','play');" onmouseout="this.src='images/profile/play.gif'" onmouseover="this.src='images/profile/play-over.gif'" src="images/profile/play.gif" title="Play file now."></a>
												<a href="javascript:void(0);"><img onclick="showPlayer('a','<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','add');" onmouseout="this.src='images/profile/add.gif'" onmouseover="this.src='images/profile/add-over.gif'" src="images/profile/add.gif" title="Add file to the bottom of your playlist without stopping current media."></a>
												<?php
												if(($combine_arr[$test_count][$newtest_count]['sharing_preference']==5 || $combine_arr[$test_count][$newtest_count]['sharing_preference']==2) && $combine_arr[$test_count][$newtest_count]['media_type'] == 114){
													?>
													<a onclick="download_directly_song('<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['title']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['creator']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['from']);?>')" href="javascript:void(0);">
													<img title="Add media to your cart for free download or purchase." onmouseout="this.src='../images/profile/download_down.gif'" onmouseover="this.src='../images/profile/download-over_down.gif'" src="../images/profile/download_down.gif"></a>
												<?php
												}/* else{
												?>
												<img src="images/profile/download-over_down.gif" />
												<?php
												} */
												?>
											</div>
											</div>
										<?php
										}else if($combine_arr[$test_count][$newtest_count]['media_type']==115)
										{
										?>
											<div class="blkplayer"><div class="player">
												<a href="javascript:void(0);"><img onclick="showPlayer('v','<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','playlist');" onmouseout="this.src='images/profile/play.gif'" onmouseover="this.src='images/profile/play-over.gif'" src="images/profile/play.gif" title="Play file now."></a>
												<a href="javascript:void(0);"><img onclick="showPlayer('v','<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','addvi');" onmouseout="this.src='images/profile/add.gif'" onmouseover="this.src='images/profile/add-over.gif'" src="images/profile/add.gif" title="Add file to the bottom of your playlist without stopping current media."></a>
												<?php
												if(($combine_arr[$test_count][$newtest_count]['sharing_preference']==5 || $combine_arr[$test_count][$newtest_count]['sharing_preference']==2) && $combine_arr[$test_count][$newtest_count]['media_type'] == 114){
													?>
													<a onclick="download_directly_song('<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['title']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['creator']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['from']);?>')" href="javascript:void(0);">
													<img title="Add media to your cart for free download or purchase." onmouseout="this.src='../images/profile/download_down.gif'" onmouseover="this.src='../images/profile/download-over_down.gif'" src="../images/profile/download_down.gif"></a>
												<?php
												}/* else{
												?>
												<img src="images/profile/download-over_down.gif" />
												<?php
												} */
												?>
											</div>
											</div>
										<?php
										}else if($combine_arr[$test_count][$newtest_count]['media_type']==113){
											$get_images_fan = $new_profile_media_classobj ->get_all_gallery_images($combine_arr[$test_count][$newtest_count]['id']);
											for($count_img=0;$count_img<=count($get_images_fan);$count_img++){
												if($get_all_img_fan ==""){
													$get_all_img_fan = $get_images_fan[$count_img]['image_name'];
												}else{
													$get_all_img_fan = $get_all_img_fan.",".$get_images_fan[$count_img]['image_name'];
												}
												if($galleryImagestitle_fan == ""){
													$galleryImagestitle_fan = $get_images_fan[$count_img]['image_title'];
												}else{
													$galleryImagestitle_fan = $galleryImagestitle_fan.",".$get_images_fan[$count_img]['image_title'];
												}
												
												$mediaSrc_fan = $get_images_fan[0]['image_name'];
											}
											$gal_id_fan = $get_images_fan[$count_img]['media_id'];
											$get_other_user_id = $user_keys_data->get_general_user_idby_mediaid($combine_arr[$test_count][$newtest_count]['id']);
											$orgPath_fan = $s3->getNewURI_otheruser("medgallery",$get_other_user_id);//"https://medgallery.s3.amazonaws.com/";
											$thumbPath_fan = $s3->getNewURI_otheruser("medgalthumb",$get_other_user_id);//"https://medgalthumb.s3.amazonaws.com/";
											
										?>
											<div class="blkplayer"><div class="player">
											<a href="javascript:void(0);"><img title="Play file now." src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" onclick="checkload('<?php echo $mediaSrc_fan;?>','<?php echo $get_all_img_fan; ?>','<?php echo $thumbPath_fan; ?>','<?php echo $orgPath_fan; ?>','<?php echo $galleryImagestitle_fan; ?>','<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>')"/></a>
											<!--<a href="javascript:void(0);"><img title="Add file to the bottom of your playlist without stopping current media." src="images/profile/add.gif"  onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'"/></a>-->
											<?php
											if(($combine_arr[$test_count][$newtest_count]['sharing_preference']==5 || $combine_arr[$test_count][$newtest_count]['sharing_preference']==2) && $combine_arr[$test_count][$newtest_count]['media_type'] == 114){
												?>
												<a onclick="download_directly_song('<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['title']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['creator']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['from']);?>')" href="javascript:void(0);">
												<img title="Add media to your cart for free download or purchase." onmouseout="this.src='../images/profile/download_down.gif'" onmouseover="this.src='../images/profile/download-over_down.gif'" src="../images/profile/download_down.gif"></a>
											<?php
											}/* else{
											?>
											<img src="images/profile/download-over_down.gif" />
											<?php
											} */
											?>
											</div>
											</div>
										<?php
											$get_all_img_fan ="";
											$galleryImagestitle_fan="";
											$mediaSrc_fan ="";
										}else if($combine_arr[$test_count][$newtest_count]['media_type'] == 116){
											$get_channel_media = $new_profile_media_classobj ->get_all_media_selected_channel($combine_arr[$test_count][$newtest_count]['id']);
										?>
											<div class="blkplayer"><div class="player">
											<input type="hidden" id="channel_media_song<?php echo $newtest_count;?>" value="<?php echo $get_channel_media['songs']; ?>"/>
											<input type="hidden" id="channel_media_video<?php echo $newtest_count;?>" value="<?php echo $get_channel_media['videos']; ?>"/>
											<a href="javascript:void(0);"><img title="Play file now." src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" onclick="PlayAll('channel_media_video<?php echo $newtest_count;?>','channel_media_song<?php echo $newtest_count;?>')"/></a>
											<a href="javascript:void(0);"><img title="Add file to the bottom of your playlist without stopping current media." src="images/profile/add.gif"  onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'" onclick="showPlayer('v','<?php if($combine_arr[$test_count][$newtest_count]['id']!=""){echo $combine_arr[$test_count][$newtest_count]['id'];}else{ echo $combine_arr[$test_count][$newtest_count]['id'];}?>','addvi');"/></a>
											<?php
											if(($combine_arr[$test_count][$newtest_count]['sharing_preference']==5 || $combine_arr[$test_count][$newtest_count]['sharing_preference']==2) && $combine_arr[$test_count][$newtest_count]['media_type'] == 114){
												?>
												<a onclick="download_directly_song('<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['title']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['creator']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['from']);?>')" href="javascript:void(0);">
												<img title="Add media to your cart for free download or purchase." onmouseout="this.src='../images/profile/download_down.gif'" onmouseover="this.src='../images/profile/download-over_down.gif'" src="../images/profile/download_down.gif"></a>
											<?php
											}/* else{
											?>
											<img src="images/profile/download-over_down.gif" />
											<?php
											} */
											?>
											</div>
											</div>
										<?php
										}
										else{
										?>
											<div class="blkplayer"><div class="player">
											<a href="javascript:void(0);"><img title="Play file now." src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" /></a>
											<a href="javascript:void(0);"><img title="Add file to the bottom of your playlist without stopping current media." src="images/profile/add.gif"  onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'"/></a>
											<?php
											if(($combine_arr[$test_count][$newtest_count]['sharing_preference']==5 || $combine_arr[$test_count][$newtest_count]['sharing_preference']==2) && $combine_arr[$test_count][$newtest_count]['media_type'] == 114){
												?>
												<a onclick="download_directly_song('<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['title']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['creator']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['from']);?>')" href="javascript:void(0);">
												<img title="Add media to your cart for free download or purchase." onmouseout="this.src='../images/profile/download_down.gif'" onmouseover="this.src='../images/profile/download-over_down.gif'" src="../images/profile/download_down.gif"></a>
											<?php
											}/* else{
											?>
											<img src="images/profile/download-over_down.gif" />
											<?php
											} */
											?>
											</div>
											</div>
										<?php
										}
										?>
										<div class="icon" onclick="runAccordion(<?php echo $combine_arr[$test_count][$newtest_count]['id']; ?>);">
											<div onselectstart="return false;"><img title="View details for media." src="images/profile/view.jpg" /></div>
										</div>
										<div class="icon">&nbsp;<!--<a href="add_media.php?edit=<?php// echo $combine_arr[$test_count][$newtest_count]['id'];?>"><img title="Edit media." src="images/profile/edit.png" /></a>--></div>
										<div class="icon"><a onclick="return confirmdelete('<?php echo $combine_arr[$test_count][$newtest_count]['title'];?>','<?php if($combine_arr[$test_count][$newtest_count]['name']=="Galleries/Images"){echo "Images";}else{echo $combine_arr[$test_count][$newtest_count]['name'];} ?>')" href="delete_media.php?id=<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>&whos_media=media_creator_creator"><img title="Remove media from your library." src="images/profile/delete.png" /></a></div>
										<div class="expandable" id="Accordion<?php echo $combine_arr[$test_count][$newtest_count]['id']; ?>Content">
											<div class="affiBlock">
												<div class="RowCont">
													<div class="sideL">Plays :</div>
													<div class="sideR"><a href="" target="_blank"><?php if($combine_arr[$test_count][$newtest_count]['play_count']!=""){ echo $combine_arr[$test_count][$newtest_count]['play_count'];}else{ echo "0";}?></a></div>
												</div>
												<?php
													if($combine_arr[$test_count][$newtest_count]['sharing_preference']==5 || $combine_arr[$test_count][$newtest_count]['sharing_preference']==2){
													?>
													<div class="RowCont">
														<div class="sideL">Downloads :</div>
														<div class="sideR"><a href="javascript:void(0);"><?php echo $combine_arr[$test_count][$newtest_count]['download_count'];?></a></div>
													</div>
													<?php
													}
													if($combine_arr[$test_count][$newtest_count]['media_type']=='113')
													{
												
													}
													else
													{
												?>	
												<!--<div class="RowCont">
													<div class="sideL">Media URL :</div>
													<div class="sideR"><a href="" target="_blank">www.<?php echo $domainname; ?></a></div>
												</div>-->
												<?php
													}
												?>
											</div>
										</div>
									</div>
									<?php
								}
								else if($combine_arr[$test_count][$newtest_count]['media_from']=="media_create_new")
								{
									?>
										<div class="tableCont" id="AccordionContainer">
										<div class="blkD" style="width:60px;"><?php if($combine_arr[$test_count][$newtest_count]['name']=="Galleries/Images"){echo "Images";}else{echo $combine_arr[$test_count][$newtest_count]['name'];} ?></div>
										<div class="blkI" style="width:127px;"><?php
										$exp_creator_info = explode("|",$combine_arr[$test_count][$newtest_count]['creator_info']);
										$pro_url = $new_profile_media_classobj->get_creator_url($exp_creator_info[0],$exp_creator_info[1]);
										?>
										<a href="<?php if($pro_url['profile_url'] !=""){ echo "/".$pro_url['profile_url']; }else { echo "javascript:void(0)"; }?>">
										<?php if($combine_arr[$test_count][$newtest_count]['creator']!=""){
										//$endpos = strpos($combine_arr[$test_count][$newtest_count]['creator'],"(");
										//if($endpos !="" || $endpos !=false) {
										//echo substr($combine_arr[$test_count][$newtest_count]['creator'],0,$endpos);} else {
										echo $combine_arr[$test_count][$newtest_count]['creator']; }
										else{?>&nbsp;<?php } ?></a></div>
										<div class="blkK"><?php
										$exp_from_info = explode("|",$combine_arr[$test_count][$newtest_count]['from_info']);
										$pro_url_from = $new_profile_media_classobj->get_from_url($exp_from_info[0],$exp_from_info[1]);
										?>
										<a href="<?php if($pro_url_from['profile_url'] !=""){ echo "/".$pro_url_from['profile_url']; }else { echo "javascript:void(0)"; }?>">
										<?php if($combine_arr[$test_count][$newtest_count]['from']!=""){
										//$endpos = strpos($combine_arr[$test_count][$newtest_count]['from'],"(");
										//if($endpos !="" || $endpos != false) {
										//echo substr($combine_arr[$test_count][$newtest_count]['from'],0,$endpos);} else {
										echo $combine_arr[$test_count][$newtest_count]['from']; }
										else{?>&nbsp;<?php } ?></a></div>
										<div class="blkB"><?php
												if($combine_arr[$test_count][$newtest_count]['title']!="")
												{
													if($combine_arr[$test_count][$newtest_count]['media_type'] == 114)
													{
														//$get_media_track = $new_profile_media_classobj->get_media_track($combine_arr[$test_count][$newtest_count]['id']); 
														if($combine_arr[$test_count][$newtest_count]['track'] != "" && $combine_arr[$test_count][$newtest_count]['track'] != NULL && $combine_arr[$test_count][$newtest_count]['track'] !=" "){
															echo $combine_arr[$test_count][$newtest_count]['track'].".".stripslashes($combine_arr[$test_count][$newtest_count]['title']);
														}else{
															echo stripslashes($combine_arr[$test_count][$newtest_count]['title']);
														}
													}
													else{
													echo stripslashes($combine_arr[$test_count][$newtest_count]['title']); }
												}else{
												?>&nbsp;<?php
												}
													?></div>
										<?php
										if($combine_arr[$test_count][$newtest_count]['media_type']==114)
										{
										?>
											<div class="blkplayer"><div class="player">
												<a href="javascript:void(0);"><img onclick="showPlayer('a','<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','play');" onmouseout="this.src='images/profile/play.gif'" onmouseover="this.src='images/profile/play-over.gif'" src="images/profile/play.gif" title="Play file now."></a>
												<a href="javascript:void(0);"><img onclick="showPlayer('a','<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','add');" onmouseout="this.src='images/profile/add.gif'" onmouseover="this.src='images/profile/add-over.gif'" src="images/profile/add.gif" title="Add file to the bottom of your playlist without stopping current media."></a>
												<?php
												if(($combine_arr[$test_count][$newtest_count]['sharing_preference']==5 || $combine_arr[$test_count][$newtest_count]['sharing_preference']==2) && $combine_arr[$test_count][$newtest_count]['media_type'] == 114){
													?>
													<a onclick="download_directly_song('<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['title']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['creator']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['from']);?>')" href="javascript:void(0);">
													<img title="Add media to your cart for free download or purchase." onmouseout="this.src='../images/profile/download_down.gif'" onmouseover="this.src='../images/profile/download-over_down.gif'" src="../images/profile/download_down.gif"></a>
												<?php
												}/* else{
												?>
												<img src="images/profile/download-over_down.gif" />
												<?php
												} */
												?>
											</div>
											</div>
										<?php
										}else if($combine_arr[$test_count][$newtest_count]['media_type']==115)
										{
										?>
											<div class="blkplayer"><div class="player">
												<a href="javascript:void(0);"><img onclick="showPlayer('v','<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','playlist');" onmouseout="this.src='images/profile/play.gif'" onmouseover="this.src='images/profile/play-over.gif'" src="images/profile/play.gif" title="Play file now."></a>
												<a href="javascript:void(0);"><img onclick="showPlayer('v','<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','addvi');" onmouseout="this.src='images/profile/add.gif'" onmouseover="this.src='images/profile/add-over.gif'" src="images/profile/add.gif" title="Add file to the bottom of your playlist without stopping current media."></a>
												<?php
												if(($combine_arr[$test_count][$newtest_count]['sharing_preference']==5 || $combine_arr[$test_count][$newtest_count]['sharing_preference']==2) && $combine_arr[$test_count][$newtest_count]['media_type'] == 114){
													?>
													<a onclick="download_directly_song('<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['title']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['creator']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['from']);?>')" href="javascript:void(0);">
													<img title="Add media to your cart for free download or purchase." onmouseout="this.src='../images/profile/download_down.gif'" onmouseover="this.src='../images/profile/download-over_down.gif'" src="../images/profile/download_down.gif"></a>
												<?php
												}/* else{
												?>
												<img src="images/profile/download-over_down.gif" />
												<?php
												} */
												?>
											</div>
											</div>
										<?php
										}else if($combine_arr[$test_count][$newtest_count]['media_type']==113){
											$get_images_fan = $new_profile_media_classobj ->get_all_gallery_images($combine_arr[$test_count][$newtest_count]['id']);
											for($count_img=0;$count_img<=count($get_images_fan);$count_img++){
												if($get_all_img_fan==""){
													$get_all_img_fan = $get_images_fan[$count_img]['image_name'];
												}else{
													$get_all_img_fan = $get_all_img_fan.",".$get_images_fan[$count_img]['image_name'];
												}
												if($galleryImagestitle_fan==""){
													$galleryImagestitle_fan = $get_images_fan[$count_img]['image_title'];
												}else{
													$galleryImagestitle_fan = $galleryImagestitle_fan.",".$get_images_fan[$count_img]['image_title'];
												}
												$mediaSrc_fan = $get_images_fan[0]['image_name'];
											}
											$gal_id_fan = $get_images_fan[$count_img]['media_id'];
											$get_other_user_id = $user_keys_data->get_general_user_idby_mediaid($combine_arr[$test_count][$newtest_count]['id']);
											$orgPath_fan = $s3->getNewURI_otheruser("medgallery",$get_other_user_id);//"https://medgallery.s3.amazonaws.com/";
											$thumbPath_fan = $s3->getNewURI_otheruser("medgalthumb",$get_other_user_id);//"https://medgalthumb.s3.amazonaws.com/";
											
										?>
											<div class="blkplayer"><div class="player">
											<a href="javascript:void(0);"><img title="Play file now." src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" onclick="checkload('<?php echo $mediaSrc_fan;?>','<?php echo $get_all_img_fan; ?>','<?php echo $thumbPath_fan; ?>','<?php echo $orgPath_fan; ?>','<?php echo $galleryImagestitle_fan; ?>','<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>')"/></a>
											<!--<a href="javascript:void(0);"><img title="Add file to the bottom of your playlist without stopping current media." src="images/profile/add.gif"  onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'"/></a>-->
											<?php
											if(($combine_arr[$test_count][$newtest_count]['sharing_preference']==5 || $combine_arr[$test_count][$newtest_count]['sharing_preference']==2) && $combine_arr[$test_count][$newtest_count]['media_type'] == 114){
												?>
												<a onclick="download_directly_song('<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['title']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['creator']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['from']);?>')" href="javascript:void(0);">
												<img title="Add media to your cart for free download or purchase." onmouseout="this.src='../images/profile/download_down.gif'" onmouseover="this.src='../images/profile/download-over_down.gif'" src="../images/profile/download_down.gif"></a>
											<?php
											}/* else{
											?>
											<img src="images/profile/download-over_down.gif" />
											<?php
											} */
											?>
											</div>
											</div>
										<?php
											$get_all_img_fan ="";
											$galleryImagestitle_fan="";
											$mediaSrc_fan ="";
										}else if($combine_arr[$test_count][$newtest_count]['media_type'] == 116){
											$get_channel_media = $new_profile_media_classobj ->get_all_media_selected_channel($combine_arr[$test_count][$newtest_count]['id']);
										?>
											<div class="blkplayer"><div class="player">
											<input type="hidden" id="channel_media_song<?php echo $newtest_count;?>" value="<?php echo $get_channel_media['songs']; ?>"/>
											<input type="hidden" id="channel_media_video<?php echo $newtest_count;?>" value="<?php echo $get_channel_media['videos']; ?>"/>
											<a href="javascript:void(0);"><img title="Play file now." src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" onclick="PlayAll('channel_media_video<?php echo $newtest_count;?>','channel_media_song<?php echo $newtest_count;?>')"/></a>
											<a href="javascript:void(0);"><img title="Add file to the bottom of your playlist without stopping current media." src="images/profile/add.gif"  onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'" onclick="showPlayer('v','<?php if($combine_arr[$test_count][$newtest_count]['id']!=""){echo $combine_arr[$test_count][$newtest_count]['id'];}else{ echo $combine_arr[$test_count][$newtest_count]['id'];}?>','addvi');"/></a>
											<?php
											if(($combine_arr[$test_count][$newtest_count]['sharing_preference']==5 || $combine_arr[$test_count][$newtest_count]['sharing_preference']==2) && $combine_arr[$test_count][$newtest_count]['media_type'] == 114){
												?>
												<a onclick="download_directly_song('<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['title']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['creator']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['from']);?>')" href="javascript:void(0);">
												<img title="Add media to your cart for free download or purchase." onmouseout="this.src='../images/profile/download_down.gif'" onmouseover="this.src='../images/profile/download-over_down.gif'" src="../images/profile/download_down.gif"></a>
											<?php
											}/* else{
											?>
											<img src="images/profile/download-over_down.gif" />
											<?php
											} */
											?>
											</div>
											</div>
										<?php
										}
										else{
										?>
											<div class="blkplayer"><div class="player">
											<a href="javascript:void(0);"><img title="Play file now." src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" /></a>
											<a href="javascript:void(0);"><img title="Add file to the bottom of your playlist without stopping current media." src="images/profile/add.gif"  onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'"/></a>
											<?php
											if(($combine_arr[$test_count][$newtest_count]['sharing_preference']==5 || $combine_arr[$test_count][$newtest_count]['sharing_preference']==2) && $combine_arr[$test_count][$newtest_count]['media_type'] == 114){
												?>
												<a onclick="download_directly_song('<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['title']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['creator']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['from']);?>')" href="javascript:void(0);">
												<img title="Add media to your cart for free download or purchase." onmouseout="this.src='../images/profile/download_down.gif'" onmouseover="this.src='../images/profile/download-over_down.gif'" src="../images/profile/download_down.gif"></a>
											<?php
											}/* else{
											?>
											<img src="images/profile/download-over_down.gif" />
											<?php
											} */
											?>
											</div>
											</div>
										<?php
										}
										?>
										<div class="icon" onclick="runAccordion(<?php echo $combine_arr[$test_count][$newtest_count]['id']; ?>);">
											<div onselectstart="return false;"><img title="View details for media." src="images/profile/view.jpg" /></div>
										</div>
										<div class="icon">&nbsp;<!--<a href="add_media.php?edit=<?php// echo $combine_arr[$test_count][$newtest_count]['id'];?>"><img title="Edit media." src="images/profile/edit.png" /></a>--></div>
										<div class="icon"><a onclick="return confirmdelete('<?php echo $combine_arr[$test_count][$newtest_count]['title'];?>','<?php if($combine_arr[$test_count][$newtest_count]['name']=="Galleries/Images"){echo "Images";}else{echo $combine_arr[$test_count][$newtest_count]['name'];} ?>')" href="delete_media.php?id=<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>&whos_media=media_create_new"><img title="Remove media from your library." src="images/profile/delete.png" /></a></div>
										<div class="expandable" id="Accordion<?php echo $combine_arr[$test_count][$newtest_count]['id']; ?>Content">
											<div class="affiBlock">
												<div class="RowCont">
													<div class="sideL">Plays :</div>
													<div class="sideR"><a href="" target="_blank"><?php if($combine_arr[$test_count][$newtest_count]['play_count']!=""){ echo $combine_arr[$test_count][$newtest_count]['play_count'];}else{ echo "0";}?></a></div>
												</div>
												<?php
													if($combine_arr[$test_count][$newtest_count]['sharing_preference']==5 || $combine_arr[$test_count][$newtest_count]['sharing_preference']==2){
													?>
													<div class="RowCont">
														<div class="sideL">Downloads :</div>
														<div class="sideR"><a href="javascript:void(0);"><?php echo $combine_arr[$test_count][$newtest_count]['download_count'];?></a></div>
													</div>
													<?php
													}
													if($combine_arr[$test_count][$newtest_count]['media_type']=='113')
													{
													
													}
													else
													{
												?>	
												<!--<div class="RowCont">
													<div class="sideL">Media URL :</div>
													<div class="sideR"><a href="" target="_blank">www.<?php echo $domainname; ?></a></div>
												</div>-->
												<?php
													}
												?>
											</div>
										</div>
									</div>
									<?php
								}
								else if($combine_arr[$test_count][$newtest_count]['media_from']=="media_from_from")
								{
									?>
										<div class="tableCont" id="AccordionContainer">
										<div class="blkD" style="width:60px;"><?php if($combine_arr[$test_count][$newtest_count]['name']=="Galleries/Images"){echo "Images";}else{echo $combine_arr[$test_count][$newtest_count]['name'];} ?></div>
										<div class="blkI" style="width:127px;"><?php
										$exp_creator_info = explode("|",$combine_arr[$test_count][$newtest_count]['creator_info']);
										$pro_url = $new_profile_media_classobj->get_creator_url($exp_creator_info[0],$exp_creator_info[1]);
										?>
										<a href="<?php if($pro_url['profile_url'] !=""){ echo "/".$pro_url['profile_url']; }else { echo "javascript:void(0)"; }?>">
										<?php if($combine_arr[$test_count][$newtest_count]['creator']!=""){
										//$endpos = strpos($combine_arr[$test_count][$newtest_count]['creator'],"(");
										//if($endpos !="" || $endpos !=false) {
										//echo substr($combine_arr[$test_count][$newtest_count]['creator'],0,$endpos);} else {
										echo $combine_arr[$test_count][$newtest_count]['creator']; }
										else{?>&nbsp;<?php } ?></a></div>
										<div class="blkK"><?php
										$exp_from_info = explode("|",$combine_arr[$test_count][$newtest_count]['from_info']);
										$pro_url_from = $new_profile_media_classobj->get_from_url($exp_from_info[0],$exp_from_info[1]);
										?>
										<a href="<?php if($pro_url_from['profile_url'] !=""){ echo "/".$pro_url_from['profile_url']; }else { echo "javascript:void(0)"; }?>">
										<?php if($combine_arr[$test_count][$newtest_count]['from']!=""){
										//$endpos = strpos($combine_arr[$test_count][$newtest_count]['from'],"(");
										//if($endpos !="" || $endpos != false) {
										//echo substr($combine_arr[$test_count][$newtest_count]['from'],0,$endpos);} else {
										echo $combine_arr[$test_count][$newtest_count]['from']; }
										else{?>&nbsp;<?php } ?></a></div>
										<div class="blkB"><?php
												if($combine_arr[$test_count][$newtest_count]['title']!="")
												{
													if($combine_arr[$test_count][$newtest_count]['media_type'] == 114)
													{
														//$get_media_track = $new_profile_media_classobj->get_media_track($combine_arr[$test_count][$newtest_count]['id']); 
														if($combine_arr[$test_count][$newtest_count]['track'] != "" && $combine_arr[$test_count][$newtest_count]['track'] != NULL && $combine_arr[$test_count][$newtest_count]['track'] !=" "){
															echo $combine_arr[$test_count][$newtest_count]['track'].".".stripslashes($combine_arr[$test_count][$newtest_count]['title']);
														}else{
															echo stripslashes($combine_arr[$test_count][$newtest_count]['title']);
														}
													}
													else{
													echo stripslashes($combine_arr[$test_count][$newtest_count]['title']); }
												}else{
												?>&nbsp;<?php
												}
													?></div>
										<?php
										if($combine_arr[$test_count][$newtest_count]['media_type']==114)
										{
										?>
											<div class="blkplayer"><div class="player">
												<a href="javascript:void(0);"><img onclick="showPlayer('a','<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','play');" onmouseout="this.src='images/profile/play.gif'" onmouseover="this.src='images/profile/play-over.gif'" src="images/profile/play.gif" title="Play file now."></a>
												<a href="javascript:void(0);"><img onclick="showPlayer('a','<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','add');" onmouseout="this.src='images/profile/add.gif'" onmouseover="this.src='images/profile/add-over.gif'" src="images/profile/add.gif" title="Add file to the bottom of your playlist without stopping current media."></a>
												<?php
												if(($combine_arr[$test_count][$newtest_count]['sharing_preference']==5 || $combine_arr[$test_count][$newtest_count]['sharing_preference']==2) && $combine_arr[$test_count][$newtest_count]['media_type'] == 114){
													?>
													<a onclick="download_directly_song('<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['title']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['creator']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['from']);?>')" href="javascript:void(0);">
													<img title="Add media to your cart for free download or purchase." onmouseout="this.src='../images/profile/download_down.gif'" onmouseover="this.src='../images/profile/download-over_down.gif'" src="../images/profile/download_down.gif"></a>
												<?php
												}/* else{
												?>
												<img src="images/profile/download-over_down.gif" />
												<?php
												} */
												?>
											</div>
											</div>
										<?php
										}else if($combine_arr[$test_count][$newtest_count]['media_type']==115)
										{
										?>
											<div class="blkplayer"><div class="player">
												<a href="javascript:void(0);"><img onclick="showPlayer('v','<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','playlist');" onmouseout="this.src='images/profile/play.gif'" onmouseover="this.src='images/profile/play-over.gif'" src="images/profile/play.gif" title="Play file now."></a>
												<a href="javascript:void(0);"><img onclick="showPlayer('v','<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','addvi');" onmouseout="this.src='images/profile/add.gif'" onmouseover="this.src='images/profile/add-over.gif'" src="images/profile/add.gif" title="Add file to the bottom of your playlist without stopping current media."></a>
												<?php
												if(($combine_arr[$test_count][$newtest_count]['sharing_preference']==5 || $combine_arr[$test_count][$newtest_count]['sharing_preference']==2) && $combine_arr[$test_count][$newtest_count]['media_type'] == 114){
													?>
													<a onclick="download_directly_song('<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['title']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['creator']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['from']);?>')" href="javascript:void(0);">
													<img title="Add media to your cart for free download or purchase." onmouseout="this.src='../images/profile/download_down.gif'" onmouseover="this.src='../images/profile/download-over_down.gif'" src="../images/profile/download_down.gif"></a>
												<?php
												}/* else{
												?>
												<img src="images/profile/download-over_down.gif" />
												<?php
												} */
												?>
											</div>
											</div>
										<?php
										}else if($combine_arr[$test_count][$newtest_count]['media_type']==113){
											$get_images_fan = $new_profile_media_classobj ->get_all_gallery_images($combine_arr[$test_count][$newtest_count]['id']);
											for($count_img=0;$count_img<=count($get_images_fan);$count_img++){
												if($get_all_img_fan ==""){
													$get_all_img_fan = $get_images_fan[$count_img]['image_name'];
												}else{
													$get_all_img_fan = $get_all_img_fan.",".$get_images_fan[$count_img]['image_name'];
												}
												if($galleryImagestitle_fan == ""){
													$galleryImagestitle_fan = $get_images_fan[$count_img]['image_title'];
												}else{
													$galleryImagestitle_fan = $galleryImagestitle_fan.",".$get_images_fan[$count_img]['image_title'];
												}
												$mediaSrc_fan = $get_images_fan[0]['image_name'];
											}
											$gal_id_fan = $get_images_fan[$count_img]['media_id'];
											$get_other_user_id = $user_keys_data->get_general_user_idby_mediaid($combine_arr[$test_count][$newtest_count]['id']);
											$orgPath_fan = $s3->getNewURI_otheruser("medgallery",$get_other_user_id);//"https://medgallery.s3.amazonaws.com/";
											$thumbPath_fan = $s3->getNewURI_otheruser("medgalthumb",$get_other_user_id);//"https://medgalthumb.s3.amazonaws.com/";
											
										?>
											<div class="blkplayer"><div class="player">
											<a href="javascript:void(0);"><img title="Play file now." src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" onclick="checkload('<?php echo $mediaSrc_fan;?>','<?php echo $get_all_img_fan; ?>','<?php echo $thumbPath_fan; ?>','<?php echo $orgPath_fan; ?>','<?php echo $galleryImagestitle_fan; ?>','<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>')"/></a>
											<!--<a href="javascript:void(0);"><img title="Add file to the bottom of your playlist without stopping current media." src="images/profile/add.gif"  onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'"/></a>-->
											<?php
											if(($combine_arr[$test_count][$newtest_count]['sharing_preference']==5 || $combine_arr[$test_count][$newtest_count]['sharing_preference']==2) && $combine_arr[$test_count][$newtest_count]['media_type'] == 114){
												?>
												<a onclick="download_directly_song('<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['title']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['creator']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['from']);?>')" href="javascript:void(0);">
												<img title="Add media to your cart for free download or purchase." onmouseout="this.src='../images/profile/download_down.gif'" onmouseover="this.src='../images/profile/download-over_down.gif'" src="../images/profile/download_down.gif"></a>
											<?php
											}/* else{
											?>
											<img src="images/profile/download-over_down.gif" />
											<?php
											} */
											?>
											</div>
											</div>
										<?php
											$get_all_img_fan ="";
											$galleryImagestitle_fan="";
											$mediaSrc_fan ="";
										}else if($combine_arr[$test_count][$newtest_count]['media_type'] == 116){
											$get_channel_media = $new_profile_media_classobj ->get_all_media_selected_channel($combine_arr[$test_count][$newtest_count]['id']);
										?>
											<div class="blkplayer"><div class="player">
											<input type="hidden" id="channel_media_song<?php echo $newtest_count;?>" value="<?php echo $get_channel_media['songs']; ?>"/>
											<input type="hidden" id="channel_media_video<?php echo $newtest_count;?>" value="<?php echo $get_channel_media['videos']; ?>"/>
											<a href="javascript:void(0);"><img title="Play file now." src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" onclick="PlayAll('channel_media_video<?php echo $newtest_count;?>','channel_media_song<?php echo $newtest_count;?>')"/></a>
											<a href="javascript:void(0);"><img title="Add file to the bottom of your playlist without stopping current media." src="images/profile/add.gif"  onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'" onclick="showPlayer('v','<?php if($combine_arr[$test_count][$newtest_count]['id']!=""){echo $combine_arr[$test_count][$newtest_count]['id'];}else{ echo $combine_arr[$test_count][$newtest_count]['id'];}?>','addvi');"/></a>
											<?php
											if(($combine_arr[$test_count][$newtest_count]['sharing_preference']==5 || $combine_arr[$test_count][$newtest_count]['sharing_preference']==2) && $combine_arr[$test_count][$newtest_count]['media_type'] == 114){
												?>
												<a onclick="download_directly_song('<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['title']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['creator']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['from']);?>')" href="javascript:void(0);">
												<img title="Add media to your cart for free download or purchase." onmouseout="this.src='../images/profile/download_down.gif'" onmouseover="this.src='../images/profile/download-over_down.gif'" src="../images/profile/download_down.gif"></a>
											<?php
											}/* else{
											?>
											<img src="images/profile/download-over_down.gif" />
											<?php
											} */
											?>
											</div>
											</div>
										<?php
										}
										else{
										?>
											<div class="blkplayer"><div class="player">
											<a href="javascript:void(0);"><img title="Play file now." src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" /></a>
											<a href="javascript:void(0);"><img title="Add file to the bottom of your playlist without stopping current media." src="images/profile/add.gif"  onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'"/></a>
											<?php
											if(($combine_arr[$test_count][$newtest_count]['sharing_preference']==5 || $combine_arr[$test_count][$newtest_count]['sharing_preference']==2) && $combine_arr[$test_count][$newtest_count]['media_type'] == 114){
												?>
												<a onclick="download_directly_song('<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['title']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['creator']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['from']);?>')" href="javascript:void(0);">
												<img title="Add media to your cart for free download or purchase." onmouseout="this.src='../images/profile/download_down.gif'" onmouseover="this.src='../images/profile/download-over_down.gif'" src="../images/profile/download_down.gif"></a>
											<?php
											}/* else{
											?>
											<img src="images/profile/download-over_down.gif" />
											<?php
											} */
											?>
											</div>
											</div>
										<?php
										}
										?>
										<div class="icon" onclick="runAccordion(<?php echo $combine_arr[$test_count][$newtest_count]['id']; ?>);">
											<div onselectstart="return false;"><img title="View details for media." src="images/profile/view.jpg" /></div>
										</div>
										<div class="icon">&nbsp;<!--<a href="add_media.php?edit=<?php// echo $combine_arr[$test_count][$newtest_count]['id'];?>"><img title="Edit media." src="images/profile/edit.png" /></a>--></div>
										<div class="icon"><a onclick="return confirmdelete('<?php echo $combine_arr[$test_count][$newtest_count]['title'];?>','<?php if($combine_arr[$test_count][$newtest_count]['name']=="Galleries/Images"){echo "Images";}else{echo $combine_arr[$test_count][$newtest_count]['name'];} ?>')" href="delete_media.php?id=<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>&whos_media=media_from_from"><img title="Remove media from your library." src="images/profile/delete.png" /></a></div>
										<div class="expandable" id="Accordion<?php echo $combine_arr[$test_count][$newtest_count]['id']; ?>Content">
											<div class="affiBlock">
												<div class="RowCont">
													<div class="sideL">Plays :</div>
													<div class="sideR"><a href="" target="_blank"><?php if($combine_arr[$test_count][$newtest_count]['play_count']!=""){ echo $combine_arr[$test_count][$newtest_count]['play_count'];}else{ echo "0";}?></a></div>
												</div>
												<?php
													if($combine_arr[$test_count][$newtest_count]['sharing_preference']==5 || $combine_arr[$test_count][$newtest_count]['sharing_preference']==2){
													?>
													<div class="RowCont">
														<div class="sideL">Downloads :</div>
														<div class="sideR"><a href="javascript:void(0);"><?php echo $combine_arr[$test_count][$newtest_count]['download_count'];?></a></div>
													</div>
													<?php
													}
													if($combine_arr[$test_count][$newtest_count]['media_type']=='113')
													{
													
													}
													else
													{
												?>	
												<!--<div class="RowCont">
													<div class="sideL">Media URL :</div>
													<div class="sideR"><a href="" target="_blank">www.<?php echo $domainname; ?></a></div>
												</div>-->
												<?php
													}
												?>
											</div>
										</div>
									</div>
									<?php
								}
								else if($combine_arr[$test_count][$newtest_count]['media_from']=="media_creator_from_tag")
								{
									$exp_deleted_id = explode(",",$getgeneral['deleted_media_by_tag_user']);
									if($newtest_count==0)
									{
										$newdel_count=1;
									}else{
									$newdel_count=$newtest_count;
									}
									if($exp_deleted_id[$newdel_count] != $combine_arr[$test_count][$newtest_count]['id'])
									{
									?>
										<div class="tableCont" id="AccordionContainer">
										<div class="blkD" style="width:60px;"><?php if($combine_arr[$test_count][$newtest_count]['name']=="Galleries/Images"){echo "Images";}else{echo $combine_arr[$test_count][$newtest_count]['name'];} ?></div>
										<div class="blkI" style="width:127px;"><?php
										$exp_creator_info = explode("|",$combine_arr[$test_count][$newtest_count]['creator_info']);
										$pro_url = $new_profile_media_classobj->get_creator_url($exp_creator_info[0],$exp_creator_info[1]);
										?>
										<a href="<?php if($pro_url['profile_url'] !=""){ echo "/".$pro_url['profile_url']; }else { echo "javascript:void(0)"; }?>">
										<?php if($combine_arr[$test_count][$newtest_count]['creator']!=""){
										//$endpos = strpos($combine_arr[$test_count][$newtest_count]['creator'],"(");
										//if($endpos !="" || $endpos !=false) {
										//echo substr($combine_arr[$test_count][$newtest_count]['creator'],0,$endpos);} else {
										echo $combine_arr[$test_count][$newtest_count]['creator']; }
										else{?>&nbsp;<?php } ?></a></div>
										<div class="blkK"><?php
										$exp_from_info = explode("|",$combine_arr[$test_count][$newtest_count]['from_info']);
										$pro_url_from = $new_profile_media_classobj->get_from_url($exp_from_info[0],$exp_from_info[1]);
										?>
										<a href="<?php if($pro_url_from['profile_url'] !=""){ echo "/".$pro_url_from['profile_url']; }else { echo "javascript:void(0)"; }?>">
										<?php if($combine_arr[$test_count][$newtest_count]['from']!=""){
										//$endpos = strpos($combine_arr[$test_count][$newtest_count]['from'],"(");
										//if($endpos !="" || $endpos != false) {
										//echo substr($combine_arr[$test_count][$newtest_count]['from'],0,$endpos);} else {
										echo $combine_arr[$test_count][$newtest_count]['from']; }
										else{?>&nbsp;<?php } ?></a></div>
										<div class="blkB"><?php
												if($combine_arr[$test_count][$newtest_count]['title']!="")
												{
													if($combine_arr[$test_count][$newtest_count]['media_type'] == 114)
													{
														//$get_media_track = $new_profile_media_classobj->get_media_track($combine_arr[$test_count][$newtest_count]['id']); 
														if($combine_arr[$test_count][$newtest_count]['track'] != "" && $combine_arr[$test_count][$newtest_count]['track'] != NULL && $combine_arr[$test_count][$newtest_count]['track'] !=" "){
															echo $combine_arr[$test_count][$newtest_count]['track'].".".stripslashes($combine_arr[$test_count][$newtest_count]['title']);
														}else{
															echo stripslashes($combine_arr[$test_count][$newtest_count]['title']);
														}
													}
													else{
													echo stripslashes($combine_arr[$test_count][$newtest_count]['title']); }
												}else{
												?>&nbsp;<?php
												}
													?></div>
										<?php
										if($combine_arr[$test_count][$newtest_count]['media_type']==114)
										{
										?>
											<div class="blkplayer"><div class="player">
												<a href="javascript:void(0);"><img onclick="showPlayer('a','<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','play');" onmouseout="this.src='images/profile/play.gif'" onmouseover="this.src='images/profile/play-over.gif'" src="images/profile/play.gif" title="Play file now."></a>
												<a href="javascript:void(0);"><img onclick="showPlayer('a','<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','add');" onmouseout="this.src='images/profile/add.gif'" onmouseover="this.src='images/profile/add-over.gif'" src="images/profile/add.gif" title="Add file to the bottom of your playlist without stopping current media."></a>
												<?php
												if(($combine_arr[$test_count][$newtest_count]['sharing_preference']==5 || $combine_arr[$test_count][$newtest_count]['sharing_preference']==2) && $combine_arr[$test_count][$newtest_count]['media_type'] == 114){
													?>
													<a onclick="download_directly_song('<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['title']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['creator']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['from']);?>')" href="javascript:void(0);">
													<img title="Add media to your cart for free download or purchase." onmouseout="this.src='../images/profile/download_down.gif'" onmouseover="this.src='../images/profile/download-over_down.gif'" src="../images/profile/download_down.gif"></a>
												<?php
												}/* else{
												?>
												<img src="images/profile/download-over_down.gif" />
												<?php
												} */
												?>
											</div>
											</div>
										<?php
										}else if($combine_arr[$test_count][$newtest_count]['media_type']==115)
										{
										?>
											<div class="blkplayer"><div class="player">
												<a href="javascript:void(0);"><img onclick="showPlayer('v','<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','playlist');" onmouseout="this.src='images/profile/play.gif'" onmouseover="this.src='images/profile/play-over.gif'" src="images/profile/play.gif" title="Play file now."></a>
												<a href="javascript:void(0);"><img onclick="showPlayer('v','<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','addvi');" onmouseout="this.src='images/profile/add.gif'" onmouseover="this.src='images/profile/add-over.gif'" src="images/profile/add.gif" title="Add file to the bottom of your playlist without stopping current media."></a>
												<?php
												if(($combine_arr[$test_count][$newtest_count]['sharing_preference']==5 || $combine_arr[$test_count][$newtest_count]['sharing_preference']==2) && $combine_arr[$test_count][$newtest_count]['media_type'] == 114){
													?>
													<a onclick="download_directly_song('<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['title']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['creator']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['from']);?>')" href="javascript:void(0);">
													<img title="Add media to your cart for free download or purchase." onmouseout="this.src='../images/profile/download_down.gif'" onmouseover="this.src='../images/profile/download-over_down.gif'" src="../images/profile/download_down.gif"></a>
												<?php
												}/* else{
												?>
												<img src="images/profile/download-over_down.gif" />
												<?php
												} */
												?>
											</div>
											</div>
										<?php
										}else if($combine_arr[$test_count][$newtest_count]['media_type']==113){
											$get_images_fan = $new_profile_media_classobj ->get_all_gallery_images($combine_arr[$test_count][$newtest_count]['id']);
											for($count_img=0;$count_img<=count($get_images_fan);$count_img++){
												if($get_all_img_fan ==""){
													$get_all_img_fan = $get_images_fan[$count_img]['image_name'];
												}else{
													$get_all_img_fan = $get_all_img_fan.",".$get_images_fan[$count_img]['image_name'];
												}
												if($galleryImagestitle_fan ==""){
													$galleryImagestitle_fan = $get_images_fan[$count_img]['image_title'];
												}else{
													$galleryImagestitle_fan = $galleryImagestitle_fan.",".$get_images_fan[$count_img]['image_title'];
												}
												$mediaSrc_fan = $get_images_fan[0]['image_name'];
											}
											$gal_id_fan = $get_images_fan[$count_img]['media_id'];
											$get_other_user_id = $user_keys_data->get_general_user_idby_mediaid($combine_arr[$test_count][$newtest_count]['id']);
											$orgPath_fan = $s3->getNewURI_otheruser("medgallery",$get_other_user_id);//"https://medgallery.s3.amazonaws.com/";
											$thumbPath_fan = $s3->getNewURI_otheruser("medgalthumb",$get_other_user_id);//"https://medgalthumb.s3.amazonaws.com/";
											
										?>
											<div class="blkplayer"><div class="player">
											<a href="javascript:void(0);"><img title="Play file now." src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" onclick="checkload('<?php echo $mediaSrc_fan;?>','<?php echo $get_all_img_fan; ?>','<?php echo $thumbPath_fan; ?>','<?php echo $orgPath_fan; ?>','<?php echo $galleryImagestitle_fan; ?>','<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>')"/></a>
											<!--<a href="javascript:void(0);"><img title="Add file to the bottom of your playlist without stopping current media." src="images/profile/add.gif"  onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'"/></a>-->
											<?php
												if(($combine_arr[$test_count][$newtest_count]['sharing_preference']==5 || $combine_arr[$test_count][$newtest_count]['sharing_preference']==2) && $combine_arr[$test_count][$newtest_count]['media_type'] == 114){
													?>
													<a onclick="download_directly_song('<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['title']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['creator']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['from']);?>')" href="javascript:void(0);">
													<img title="Add media to your cart for free download or purchase." onmouseout="this.src='../images/profile/download_down.gif'" onmouseover="this.src='../images/profile/download-over_down.gif'" src="../images/profile/download_down.gif"></a>
												<?php
												}/* else{
												?>
												<img src="images/profile/download-over_down.gif" />
												<?php
												} */
												?>
											</div>
											</div>
										<?php
											$get_all_img_fan ="";
											$galleryImagestitle_fan="";
											$mediaSrc_fan ="";
										}else if($combine_arr[$test_count][$newtest_count]['media_type'] == 116){
											$get_channel_media = $new_profile_media_classobj ->get_all_media_selected_channel($combine_arr[$test_count][$newtest_count]['id']);
										?>
											<div class="blkplayer"><div class="player">
											<input type="hidden" id="channel_media_song<?php echo $newtest_count;?>" value="<?php echo $get_channel_media['songs']; ?>"/>
											<input type="hidden" id="channel_media_video<?php echo $newtest_count;?>" value="<?php echo $get_channel_media['videos']; ?>"/>
											<a href="javascript:void(0);"><img title="Play file now." src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" onclick="PlayAll('channel_media_video<?php echo $newtest_count;?>','channel_media_song<?php echo $newtest_count;?>')"/></a>
											<a href="javascript:void(0);"><img title="Add file to the bottom of your playlist without stopping current media." src="images/profile/add.gif"  onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'" onclick="showPlayer('v','<?php if($combine_arr[$test_count][$newtest_count]['id']!=""){echo $combine_arr[$test_count][$newtest_count]['id'];}else{ echo $combine_arr[$test_count][$newtest_count]['id'];}?>','addvi');"/></a>
											<?php
												if(($combine_arr[$test_count][$newtest_count]['sharing_preference']==5 || $combine_arr[$test_count][$newtest_count]['sharing_preference']==2) && $combine_arr[$test_count][$newtest_count]['media_type'] == 114){
													?>
													<a onclick="download_directly_song('<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['title']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['creator']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['from']);?>')" href="javascript:void(0);">
													<img title="Add media to your cart for free download or purchase." onmouseout="this.src='../images/profile/download_down.gif'" onmouseover="this.src='../images/profile/download-over_down.gif'" src="../images/profile/download_down.gif"></a>
												<?php
												}/* else{
												?>
												<img src="images/profile/download-over_down.gif" />
												<?php
												} */
												?>
											</div>
											</div>
										<?php
										}
										else{
										?>
											<div class="blkplayer"><div class="player">
											<a href="javascript:void(0);"><img title="Play file now." src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" /></a>
											<a href="javascript:void(0);"><img title="Add file to the bottom of your playlist without stopping current media." src="images/profile/add.gif"  onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'"/></a>
											<?php
												if(($combine_arr[$test_count][$newtest_count]['sharing_preference']==5 || $combine_arr[$test_count][$newtest_count]['sharing_preference']==2) && $combine_arr[$test_count][$newtest_count]['media_type'] == 114){
													?>
													<a onclick="download_directly_song('<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['title']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['creator']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['from']);?>')" href="javascript:void(0);">
													<img title="Add media to your cart for free download or purchase." onmouseout="this.src='../images/profile/download_down.gif'" onmouseover="this.src='../images/profile/download-over_down.gif'" src="../images/profile/download_down.gif"></a>
												<?php
												}/* else{
												?>
												<img src="images/profile/download-over_down.gif" />
												<?php
												} */
												?>
											</div>
											</div>
										<?php
										}
										?>
										<div class="icon" onclick="runAccordion(<?php echo $combine_arr[$test_count][$newtest_count]['id']; ?>);">
											<div onselectstart="return false;"><img title="View details for media." src="images/profile/view.jpg" /></div>
										</div>
										<div class="icon">&nbsp;<!--<a href="add_media.php?edit=<?php// echo $combine_arr[$test_count][$newtest_count]['id'];?>"><img title="Edit media." src="images/profile/edit.png" /></a>--></div>
										<div class="icon"><a onclick="return confirmdelete('<?php echo $combine_arr[$test_count][$newtest_count]['title'];?>','<?php if($combine_arr[$test_count][$newtest_count]['name']=="Galleries/Images"){echo "Images";}else{echo $combine_arr[$test_count][$newtest_count]['name'];} ?>')" href="delete_media.php?id=<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>&whos_media=media_creator_from_tag"><img title="Remove media from your library." src="images/profile/delete.png" /></a></div>
										<div class="expandable" id="Accordion<?php echo $combine_arr[$test_count][$newtest_count]['id']; ?>Content">
											<div class="affiBlock">
												<div class="RowCont">
													<div class="sideL">Plays :</div>
													<div class="sideR"><a href="" target="_blank"><?php if($combine_arr[$test_count][$newtest_count]['play_count']!=""){ echo $combine_arr[$test_count][$newtest_count]['play_count'];}else{ echo "0";}?></a></div>
												</div>
												<?php
													if($combine_arr[$test_count][$newtest_count]['sharing_preference']==5 || $combine_arr[$test_count][$newtest_count]['sharing_preference']==2){
													?>
													<div class="RowCont">
														<div class="sideL">Downloads :</div>
														<div class="sideR"><a href="javascript:void(0);"><?php echo $combine_arr[$test_count][$newtest_count]['download_count'];?></a></div>
													</div>
													<?php
													}
													if($combine_arr[$test_count][$newtest_count]['media_type']=='113')
													{
																										
													}
													else
													{
												?>	
												<!--<div class="RowCont">
													<div class="sideL">Media URL :</div>
													<div class="sideR"><a href="" target="_blank">www.<?php echo $domainname; ?></a></div>
												</div>-->
												<?php
													}
												?>
											</div>
										</div>
									</div>
									<?php
									}
								}
								else if($combine_arr[$test_count][$newtest_count]['media_from']=="media_coming_fan")
								{
									$chk_tag_user = $new_profile_media_classobj->get_loggedin_info($_SESSION['login_email']);
									if($chk_tag_user['general_user_id']!=$combine_arr[$test_count][$newtest_count]['general_user_id'])
									{
							?>
										<div class="tableCont" id="AccordionContainer">
											<div class="blkD" style="width:60px;"><?php if($combine_arr[$test_count][$newtest_count]['name']!=""){if($combine_arr[$test_count][$newtest_count]['name'] =="Galleries/Images"){echo "Images";}else{echo $combine_arr[$test_count][$newtest_count]['name'];}}else{?>&nbsp;<?php } ?></div>
											<div class="blkI" style="width:127px;"><?php
											$exp_creator_info = explode("|",$combine_arr[$test_count][$newtest_count]['creator_info']);
											$pro_url = $new_profile_media_classobj->get_creator_url($exp_creator_info[0],$exp_creator_info[1]);
											?>
											<a href="<?php if($pro_url['profile_url'] !=""){ echo "/".$pro_url['profile_url']; }else { echo "javascript:void(0)"; }?>">
											<?php if($combine_arr[$test_count][$newtest_count]['creator']!=""){
											//$endpos = strpos($combine_arr[$test_count][$newtest_count]['creator'],"(");
											//if($endpos !="" || $endpos !=false) {
											//echo substr($combine_arr[$test_count][$newtest_count]['creator'],0,$endpos);} else {
											echo $combine_arr[$test_count][$newtest_count]['creator']; }
											else{?>&nbsp;<?php } ?></a></div>
											<div class="blkK"><?php
											$exp_from_info = explode("|",$combine_arr[$test_count][$newtest_count]['from_info']);
											$pro_url_from = $new_profile_media_classobj->get_from_url($exp_from_info[0],$exp_from_info[1]);
											?>
											<a href="<?php if($pro_url_from['profile_url'] !=""){ echo "/".$pro_url_from['profile_url']; }else { echo "javascript:void(0)"; }?>">
											<?php if($combine_arr[$test_count][$newtest_count]['from']!=""){
											//$endpos = strpos($combine_arr[$test_count][$newtest_count]['from'],"(");
											//if($endpos !="" || $endpos != false) {
											//echo substr($combine_arr[$test_count][$newtest_count]['from'],0,$endpos);} else {
											echo $combine_arr[$test_count][$newtest_count]['from']; }
											else{?>&nbsp;<?php } ?></a></div>
											<div class="blkB"><?php
												if($combine_arr[$test_count][$newtest_count]['title']!="")
												{
													if($combine_arr[$test_count][$newtest_count]['media_type'] == 114)
													{
														//$get_media_track = $new_profile_media_classobj->get_media_track($combine_arr[$test_count][$newtest_count]['id']); 
														if($combine_arr[$test_count][$newtest_count]['track'] != "" && $combine_arr[$test_count][$newtest_count]['track'] != NULL && $combine_arr[$test_count][$newtest_count]['track'] !=" "){
															echo $combine_arr[$test_count][$newtest_count]['track'].".".stripslashes($combine_arr[$test_count][$newtest_count]['title']);
														}else{
															echo stripslashes($combine_arr[$test_count][$newtest_count]['title']);
														}
													}
													else{
													echo stripslashes($combine_arr[$test_count][$newtest_count]['title']); }
												}else{
												?>&nbsp;<?php
												}
													?></div>
											<div class="blkplayer"><div class="player">
											<?php 
												if($combine_arr[$test_count][$newtest_count]['media_type']==113){
													$get_images_fan = $new_profile_media_classobj ->get_all_gallery_images($combine_arr[$test_count][$newtest_count]['id']);
													for($count_img=0;$count_img<=count($get_images_fan);$count_img++){
														if($get_all_img_fan ==""){
															$get_all_img_fan = $get_images_fan[$count_img]['image_name'];
														}else{
															$get_all_img_fan = $get_all_img_fan.",".$get_images_fan[$count_img]['image_name'];
														}
														if($galleryImagestitle_fan ==""){
															$galleryImagestitle_fan = $get_images_fan[$count_img]['image_title'];
														}else{
															$galleryImagestitle_fan = $galleryImagestitle_fan.",".$get_images_fan[$count_img]['image_title'];
														}
														$mediaSrc_fan = $get_images_fan[0]['image_name'];
													}
													$gal_id_fan = $get_images_fan[$count_img]['media_id'];
													$get_other_user_id = $user_keys_data->get_general_user_idby_mediaid($combine_arr[$test_count][$newtest_count]['id']);
													$orgPath_fan = $s3->getNewURI_otheruser("medgallery",$get_other_user_id);//"https://medgallery.s3.amazonaws.com/";
													$thumbPath_fan = $s3->getNewURI_otheruser("medgalthumb",$get_other_user_id);//"https://medgalthumb.s3.amazonaws.com/";
											
												?>
												<a href="javascript:void(0);"><img title="Play file now." src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" onclick="checkload('<?php echo $mediaSrc_fan;?>','<?php echo $get_all_img_fan; ?>','<?php echo $thumbPath_fan; ?>','<?php echo $orgPath_fan; ?>','<?php echo $galleryImagestitle_fan; ?>','<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>')"/></a>
												<!--<a href="javascript:void(0);"><img title="Add file to the bottom of your playlist without stopping current media." src="images/profile/add.gif"  onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'"/></a>-->
												<?php
													$get_all_img_fan ="";
													$galleryImagestitle_fan="";
													$mediaSrc_fan ="";
												}else if($combine_arr[$test_count][$newtest_count]['media_type']==114){
												?>
												<a href="javascript:void(0);"><img title="Play file now." src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" onclick="showPlayer('a','<?php if($combine_arr[$test_count][$newtest_count]['id']!=""){echo $combine_arr[$test_count][$newtest_count]['id'];}else{echo $combine_arr[$test_count][$newtest_count]['id'];}?>','play');"/></a>
												<a href="javascript:void(0);"><img title="Add file to the bottom of your playlist without stopping current media." src="images/profile/add.gif"  onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'" onclick="showPlayer('a','<?php if($combine_arr[$test_count][$newtest_count]['id']!=""){echo $combine_arr[$test_count][$newtest_count]['id'];}else{echo $combine_arr[$test_count][$newtest_count]['id'];}?>','add');"/></a>
												<?php
												}else if($combine_arr[$test_count][$newtest_count]['media_type']==115){
												?>
												<a href="javascript:void(0);"><img title="Play file now." src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" onclick="showPlayer('v','<?php if($combine_arr[$test_count][$newtest_count]['id']!=""){echo $combine_arr[$test_count][$newtest_count]['id'];}else{ echo $combine_arr[$test_count][$newtest_count]['id'];}?>','playlist');"/></a>
												<a href="javascript:void(0);"><img title="Add file to the bottom of your playlist without stopping current media." src="images/profile/add.gif"  onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'" onclick="showPlayer('v','<?php if($combine_arr[$test_count][$newtest_count]['id']!=""){echo $combine_arr[$test_count][$newtest_count]['id'];}else{ echo $combine_arr[$test_count][$newtest_count]['id'];}?>','addvi');"/></a>
												<?php
												}else if($combine_arr[$test_count][$newtest_count]['media_type'] == 116){
													$get_channel_media = $new_profile_media_classobj ->get_all_media_selected_channel($combine_arr[$test_count][$newtest_count]['id']);
												?>
													<input type="hidden" id="channel_media_song<?php echo $newtest_count;?>" value="<?php echo $get_channel_media['songs']; ?>"/>
													<input type="hidden" id="channel_media_video<?php echo $newtest_count;?>" value="<?php echo $get_channel_media['videos']; ?>"/>
													<a href="javascript:void(0);"><img title="Play file now." src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" onclick="PlayAll('channel_media_video<?php echo $newtest_count;?>','channel_media_song<?php echo $newtest_count;?>')"/></a>
													<a href="javascript:void(0);"><img title="Add file to the bottom of your playlist without stopping current media." src="images/profile/add.gif"  onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'" onclick="showPlayer('v','<?php if($combine_arr[$test_count][$newtest_count]['id']!=""){echo $combine_arr[$test_count][$newtest_count]['id'];}else{ echo $combine_arr[$test_count][$newtest_count]['id'];}?>','addvi');"/></a>
												<?php
												}
												else{
												?>
												<a href="javascript:void(0);"><img title="Play file now." src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" /></a>
												<a href="javascript:void(0);"><img title="Add file to the bottom of your playlist without stopping current media." src="images/profile/add.gif"  onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'"/></a>
												<?php
												}
												
												if(($combine_arr[$test_count][$newtest_count]['sharing_preference']==5 || $combine_arr[$test_count][$newtest_count]['sharing_preference']==2) && $combine_arr[$test_count][$newtest_count]['media_type'] == 114){
													?>
													<a onclick="download_directly_song('<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['title']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['creator']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['from']);?>')" href="javascript:void(0);">
													<img title="Add media to your cart for free download or purchase." onmouseout="this.src='../images/profile/download_down.gif'" onmouseover="this.src='../images/profile/download-over_down.gif'" src="../images/profile/download_down.gif"></a>
												<?php
												}/* else{
												?>
												<img src="images/profile/download-over_down.gif" />
												<?php
												} */
												?>
											</div>
											</div>
										<div class="icon" onclick="runAccordion(<?php echo $combine_arr[$test_count][$newtest_count]['id']; ?>);">
											<div onselectstart="return false;"><img title="View details for media." src="images/profile/view.jpg" /></div>
										</div>
										<div class="icon">&nbsp;<!--<a href="add_media.php?edit=<?php// echo $combine_arr[$test_count][$newtest_count]['id'];?>"><img title="Edit media." src="images/profile/edit.png" /></a>--></div>
										<div class="icon"><a onclick="return confirmdelete('<?php echo $combine_arr[$test_count][$newtest_count]['title'];?>','<?php if($combine_arr[$test_count][$newtest_count]['name']=="Galleries/Images"){echo "Images";}else{echo $combine_arr[$test_count][$newtest_count]['name'];} ?>')" href="delete_media.php?id=<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>&whos_media=media_coming_fan"><img title="Remove media from your library." src="images/profile/delete.png" /></a></div>
											<div class="expandable" id="Accordion<?php echo $combine_arr[$test_count][$newtest_count]['id']; ?>Content">
												<div class="affiBlock">
													<div class="RowCont">
														<div class="sideL">Plays :</div>
														<div class="sideR"><a href="" target="_blank"><?php if($combine_arr[$test_count][$newtest_count]['play_count']!=""){ echo $combine_arr[$test_count][$newtest_count]['play_count'];}else{ echo "0";}?></a></div>
													</div>
													<?php
														if($combine_arr[$test_count][$newtest_count]['sharing_preference']==5 || $combine_arr[$test_count][$newtest_count]['sharing_preference']==2){
														?>
														<div class="RowCont">
															<div class="sideL">Downloads :</div>
															<div class="sideR"><a href="javascript:void(0);"><?php echo $combine_arr[$test_count][$newtest_count]['download_count'];?></a></div>
														</div>
														<?php
														}
														if($combine_arr[$test_count][$newtest_count]['media_type']=='113')
														{
																											
														}
														else
														{
													?>	
													<!--<div class="RowCont">
														<div class="sideL">Media URL :</div>
														<div class="sideR"><a href="" target="_blank">www.<?php echo $domainname; ?></a></div>
													</div>-->
													<?php
														}
													?>
												</div>
											</div>
										</div>
							<?php
									}
								}
								else if($combine_arr[$test_count][$newtest_count]['media_from']=="media_coming_buyed")
								{
									$chk_tag_user = $new_profile_media_classobj->get_loggedin_info($_SESSION['login_email']);
									if($chk_tag_user['general_user_id']!=$combine_arr[$test_count][$newtest_count]['general_user_id'])
									{
							?>
										<div class="tableCont" id="AccordionContainer">
											<div class="blkD" style="width:60px;"><?php if($combine_arr[$test_count][$newtest_count]['name']!=""){if($combine_arr[$test_count][$newtest_count]['name'] =="Galleries/Images"){echo "Images";}else{echo $combine_arr[$test_count][$newtest_count]['name'];}}else{?>&nbsp;<?php } ?></div>
											<div class="blkI" style="width:127px;"><?php
											$exp_creator_info = explode("|",$combine_arr[$test_count][$newtest_count]['creator_info']);
											$pro_url = $new_profile_media_classobj->get_creator_url($exp_creator_info[0],$exp_creator_info[1]);
											?>
											<a href="<?php if($pro_url['profile_url'] !=""){ echo "/".$pro_url['profile_url']; }else { echo "javascript:void(0)"; }?>">
											<?php if($combine_arr[$test_count][$newtest_count]['creator']!=""){
											//$endpos = strpos($combine_arr[$test_count][$newtest_count]['creator'],"(");
											//if($endpos !="" || $endpos !=false) {
											//echo substr($combine_arr[$test_count][$newtest_count]['creator'],0,$endpos);} else {
											echo $combine_arr[$test_count][$newtest_count]['creator']; }
											else{?>&nbsp;<?php } ?></a></div>
											<div class="blkK"><?php
											$exp_from_info = explode("|",$combine_arr[$test_count][$newtest_count]['from_info']);
											$pro_url_from = $new_profile_media_classobj->get_from_url($exp_from_info[0],$exp_from_info[1]);
											?>
											<a href="<?php if($pro_url_from['profile_url'] !=""){ echo "/".$pro_url_from['profile_url']; }else { echo "javascript:void(0)"; }?>">
											<?php if($combine_arr[$test_count][$newtest_count]['from']!=""){
											//$endpos = strpos($combine_arr[$test_count][$newtest_count]['from'],"(");
											//if($endpos !="" || $endpos != false) {
											//echo substr($combine_arr[$test_count][$newtest_count]['from'],0,$endpos);} else {
											echo $combine_arr[$test_count][$newtest_count]['from']; }
											else{?>&nbsp;<?php } ?></a></div>
											<div class="blkB"><?php
												if($combine_arr[$test_count][$newtest_count]['title']!="")
												{
													if($combine_arr[$test_count][$newtest_count]['media_type'] == 114)
													{
														//$get_media_track = $new_profile_media_classobj->get_media_track($combine_arr[$test_count][$newtest_count]['id']); 
														if($combine_arr[$test_count][$newtest_count]['track'] != "" && $combine_arr[$test_count][$newtest_count]['track'] != NULL && $combine_arr[$test_count][$newtest_count]['track'] !=" "){
															echo $combine_arr[$test_count][$newtest_count]['track'].".".stripslashes($combine_arr[$test_count][$newtest_count]['title']);
														}else{
															echo stripslashes($combine_arr[$test_count][$newtest_count]['title']);
														}
													}
													else{
													echo stripslashes($combine_arr[$test_count][$newtest_count]['title']); }
												}else{
												?>&nbsp;<?php
												}
													?></div>
											<div class="blkplayer"><div class="player">
											<?php 
												if($combine_arr[$test_count][$newtest_count]['media_type']==113){
													$get_images_fan = $new_profile_media_classobj ->get_all_gallery_images($combine_arr[$test_count][$newtest_count]['id']);
													for($count_img=0;$count_img<=count($get_images_fan);$count_img++){
														if($get_all_img_fan == ""){
															$get_all_img_fan = $get_images_fan[$count_img]['image_name'];
														}else{
															$get_all_img_fan = $get_all_img_fan.",".$get_images_fan[$count_img]['image_name'];
														}
														if($galleryImagestitle_fan ==""){
															$galleryImagestitle_fan = $get_images_fan[$count_img]['image_title'];
														}else{
															$galleryImagestitle_fan = $galleryImagestitle_fan.",".$get_images_fan[$count_img]['image_title'];
														}
														$mediaSrc_fan = $get_images_fan[0]['image_name'];
													}
													$gal_id_fan = $get_images_fan[$count_img]['media_id'];
													$get_other_user_id = $user_keys_data->get_general_user_idby_mediaid($combine_arr[$test_count][$newtest_count]['id']);
													$orgPath_fan = $s3->getNewURI_otheruser("medgallery",$get_other_user_id);//"https://medgallery.s3.amazonaws.com/";
													$thumbPath_fan = $s3->getNewURI_otheruser("medgalthumb",$get_other_user_id);//"https://medgalthumb.s3.amazonaws.com/";
													
												?>
												<a href="javascript:void(0);"><img title="Play file now." src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" onclick="checkload('<?php echo $mediaSrc_fan;?>','<?php echo $get_all_img_fan; ?>','<?php echo $thumbPath_fan; ?>','<?php echo $orgPath_fan; ?>','<?php echo $galleryImagestitle_fan; ?>','<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>')"/></a>
												<!--<a href="javascript:void(0);"><img title="Add file to the bottom of your playlist without stopping current media." src="images/profile/add.gif"  onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'"/></a>-->
												<?php
													$get_all_img_fan ="";
													$galleryImagestitle_fan="";
													$mediaSrc_fan ="";
												}else if($combine_arr[$test_count][$newtest_count]['media_type']==114){
												?>
												<a href="javascript:void(0);"><img title="Play file now." src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" onclick="showPlayer('a','<?php if($combine_arr[$test_count][$newtest_count]['id']!=""){echo $combine_arr[$test_count][$newtest_count]['id'];}else{echo $combine_arr[$test_count][$newtest_count]['id'];}?>','play');"/></a>
												<a href="javascript:void(0);"><img title="Add file to the bottom of your playlist without stopping current media." src="images/profile/add.gif"  onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'" onclick="showPlayer('a','<?php if($combine_arr[$test_count][$newtest_count]['id']!=""){echo $combine_arr[$test_count][$newtest_count]['id'];}else{echo $combine_arr[$test_count][$newtest_count]['id'];}?>','add');"/></a>
												<?php
												}else if($combine_arr[$test_count][$newtest_count]['media_type']==115){
												?>
												<a href="javascript:void(0);"><img title="Play file now." src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" onclick="showPlayer('v','<?php if($combine_arr[$test_count][$newtest_count]['id']!=""){echo $combine_arr[$test_count][$newtest_count]['id'];}else{ echo $combine_arr[$test_count][$newtest_count]['id'];}?>','playlist');"/></a>
												<a href="javascript:void(0);"><img title="Add file to the bottom of your playlist without stopping current media." src="images/profile/add.gif"  onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'" onclick="showPlayer('v','<?php if($combine_arr[$test_count][$newtest_count]['id']!=""){echo $combine_arr[$test_count][$newtest_count]['id'];}else{ echo $combine_arr[$test_count][$newtest_count]['id'];}?>','addvi');"/></a>
												<?php
												}else if($combine_arr[$test_count][$newtest_count]['media_type'] == 116){
													$get_channel_media = $new_profile_media_classobj ->get_all_media_selected_channel($combine_arr[$test_count][$newtest_count]['id']);
												?>
													<input type="hidden" id="channel_media_song<?php echo $newtest_count;?>" value="<?php echo $get_channel_media['songs']; ?>"/>
													<input type="hidden" id="channel_media_video<?php echo $newtest_count;?>" value="<?php echo $get_channel_media['videos']; ?>"/>
													<a href="javascript:void(0);"><img title="Play file now." src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" onclick="PlayAll('channel_media_video<?php echo $newtest_count;?>','channel_media_song<?php echo $newtest_count;?>')"/></a>
													<a href="javascript:void(0);"><img title="Add file to the bottom of your playlist without stopping current media." src="images/profile/add.gif"  onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'" onclick="showPlayer('v','<?php if($combine_arr[$test_count][$newtest_count]['id']!=""){echo $combine_arr[$test_count][$newtest_count]['id'];}else{ echo $combine_arr[$test_count][$newtest_count]['id'];}?>','addvi');"/></a>
												<?php
												}
												else{
												?>
												<a href="javascript:void(0);"><img title="Play file now." src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" /></a>
												<a href="javascript:void(0);"><img title="Add file to the bottom of your playlist without stopping current media." src="images/profile/add.gif"  onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'"/></a>
												<?php
												}
												
												if(($combine_arr[$test_count][$newtest_count]['sharing_preference']==5 || $combine_arr[$test_count][$newtest_count]['sharing_preference']==2) && $combine_arr[$test_count][$newtest_count]['media_type'] == 114){
													?>
													<a onclick="download_directly_song('<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['title']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['creator']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['from']);?>')" href="javascript:void(0);">
													<img title="Add media to your cart for free download or purchase." onmouseout="this.src='../images/profile/download_down.gif'" onmouseover="this.src='../images/profile/download-over_down.gif'" src="../images/profile/download_down.gif"></a>
												<?php
												}/* else{
												?>
												<img src="images/profile/download-over_down.gif" />
												<?php
												} */
												?>
											</div>
											</div>
										<div class="icon" onclick="runAccordion(<?php echo $combine_arr[$test_count][$newtest_count]['id']; ?>);">
											<div onselectstart="return false;"><img title="View details for media." src="images/profile/view.jpg" /></div>
										</div>
										<div class="icon">&nbsp;<!--<a href="add_media.php?edit=<?php// echo $combine_arr[$test_count][$newtest_count]['id'];?>"><img title="Edit media." src="images/profile/edit.png" /></a>--></div>
										<div class="icon"><a onclick="return confirmdelete('<?php echo $combine_arr[$test_count][$newtest_count]['title'];?>','<?php if($combine_arr[$test_count][$newtest_count]['name']=="Galleries/Images"){echo "Images";}else{echo $combine_arr[$test_count][$newtest_count]['name'];} ?>')" href="delete_media.php?id=<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>&whos_media=media_coming_buyed"><img title="Remove media from your library." src="images/profile/delete.png" /></a></div>
											<div class="expandable" id="Accordion<?php echo $combine_arr[$test_count][$newtest_count]['id']; ?>Content">
												<div class="affiBlock">
													<div class="RowCont">
														<div class="sideL">Plays :</div>
														<div class="sideR"><a href="" target="_blank"><?php if($combine_arr[$test_count][$newtest_count]['play_count']!=""){ echo $combine_arr[$test_count][$newtest_count]['play_count'];}else{ echo "0";}?></a></div>
													</div>
													<?php
														if($combine_arr[$test_count][$newtest_count]['sharing_preference']==5 || $combine_arr[$test_count][$newtest_count]['sharing_preference']==2){
														?>
														<div class="RowCont">
															<div class="sideL">Downloads :</div>
															<div class="sideR"><a href="javascript:void(0);"><?php echo $combine_arr[$test_count][$newtest_count]['download_count'];?></a></div>
														</div>
														<?php
														}
														if($combine_arr[$test_count][$newtest_count]['media_type']=='113')
														{
																											
														}
														else
														{
													?>	
													<!--<div class="RowCont">
														<div class="sideL">Media URL :</div>
														<div class="sideR"><a href="" target="_blank">www.<?php echo $domainname; ?></a></div>
													</div>-->
													<?php
														}
													?>
												</div>
											</div>
										</div>
							<?php
									}
								}
								else if($combine_arr[$test_count][$newtest_count]['media_from']=="all_tagging_media")
								{
									//$get_media_tag_g_art = $new_profile_media_classobj->get_tag_media_info($combine_arr[$test_count][$newtest_count]);
									$chk_tag_user = $new_profile_media_classobj->get_loggedin_info($_SESSION['login_email']);
									if($chk_tag_user['general_user_id']!=$combine_arr[$test_count][$newtest_count]['general_user_id'])
									{
										if($combine_arr[$test_count][$newtest_count]['id']!=""){
											?>
											<div class="tableCont" id="AccordionContainer">
												<div class="blkD" style="width:60px;"><?php if($combine_arr[$test_count][$newtest_count]['name']!=""){if($combine_arr[$test_count][$newtest_count]['name'] =="Galleries/Images"){echo "Images";}else{echo $combine_arr[$test_count][$newtest_count]['name'];}}else{?>&nbsp;<?php }?></div>
												<div class="blkI" style="width:127px;"><?php
												$exp_creator_info = explode("|",$combine_arr[$test_count][$newtest_count]['creator_info']);
												$pro_url = $new_profile_media_classobj->get_creator_url($exp_creator_info[0],$exp_creator_info[1]);
												?>
												<a href="<?php if($pro_url['profile_url'] !=""){ echo "/".$pro_url['profile_url']; }else { echo "javascript:void(0)"; }?>">
												<?php if($combine_arr[$test_count][$newtest_count]['creator']!=""){
												//$endpos = strpos($combine_arr[$test_count][$newtest_count]['creator'],"(");
												//if($endpos !="" || $endpos !=false) {
												//echo substr($combine_arr[$test_count][$newtest_count]['creator'],0,$endpos);} else {
												echo $combine_arr[$test_count][$newtest_count]['creator']; }
												else{?>&nbsp;<?php } ?></a></div>
												<div class="blkK"><?php
												$exp_from_info = explode("|",$combine_arr[$test_count][$newtest_count]['from_info']);
												$pro_url_from = $new_profile_media_classobj->get_from_url($exp_from_info[0],$exp_from_info[1]);
												?>
												<a href="<?php if($pro_url_from['profile_url'] !=""){ echo "/".$pro_url_from['profile_url']; }else { echo "javascript:void(0)"; }?>">
												<?php if($combine_arr[$test_count][$newtest_count]['from']!=""){
												//$endpos = strpos($combine_arr[$test_count][$newtest_count]['from'],"(");
												//if($endpos !="" || $endpos != false) {
												//echo substr($combine_arr[$test_count][$newtest_count]['from'],0,$endpos);} else {
												echo $combine_arr[$test_count][$newtest_count]['from']; }
												else{?>&nbsp;<?php } ?></a></div>
												<div class="blkB"><?php
												if($combine_arr[$test_count][$newtest_count]['title']!="")
												{
													if($combine_arr[$test_count][$newtest_count]['media_type'] == 114)
													{
														if($combine_arr[$test_count][$newtest_count]['track'] == ""){
														echo stripslashes($combine_arr[$test_count][$newtest_count]['title']);}else{
														echo $combine_arr[$test_count][$newtest_count]['track'].".".stripslashes($combine_arr[$test_count][$newtest_count]['title']);
														}
													}
													else{
													echo stripslashes($combine_arr[$test_count][$newtest_count]['title']); }
												}else{
												?>&nbsp;<?php
												}
													?>
												</div>
												<?php
												if($combine_arr[$test_count][$newtest_count]['media_type']==114){
												?>
												<div class="blkplayer"><div class="player">
												<a href="javascript:void(0);"><img title="Play file now." src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" onclick="showPlayer('a','<?php if($combine_arr[$test_count][$newtest_count]['id']!=""){echo $combine_arr[$test_count][$newtest_count]['id'];}else{echo $combine_arr[$test_count][$newtest_count]['id'];}?>','play');"/></a>
												<a href="javascript:void(0);"><img title="Add file to the bottom of your playlist without stopping current media." src="images/profile/add.gif"  onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'" onclick="showPlayer('a','<?php if($combine_arr[$test_count][$newtest_count]['id']!=""){echo $combine_arr[$test_count][$newtest_count]['id'];}else{echo $combine_arr[$test_count][$newtest_count]['id'];}?>','add');"/></a>
												<?php
												if(($combine_arr[$test_count][$newtest_count]['sharing_preference']==5 || $combine_arr[$test_count][$newtest_count]['sharing_preference']==2) && $combine_arr[$test_count][$newtest_count]['media_type'] == 114){
													?>
													<a onclick="download_directly_song('<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['title']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['creator']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['from']);?>')" href="javascript:void(0);">
													<img title="Add media to your cart for free download or purchase." onmouseout="this.src='../images/profile/download_down.gif'" onmouseover="this.src='../images/profile/download-over_down.gif'" src="../images/profile/download_down.gif"></a>
												<?php
												}/* else{
												?>
												<img src="images/profile/download-over_down.gif" />
												<?php
												} */
												?>
												</div>
												</div>
												<?php
												}else if($combine_arr[$test_count][$newtest_count]['media_type']==115){
												?>
												<div class="blkplayer"><div class="player">
												<a href="javascript:void(0);"><img title="Play file now." src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" onclick="showPlayer('v','<?php if($combine_arr[$test_count][$newtest_count]['id']!=""){echo $combine_arr[$test_count][$newtest_count]['id'];}else{echo $combine_arr[$test_count][$newtest_count]['id'];}?>','playlist');"/></a>
												<a href="javascript:void(0);"><img title="Add file to the bottom of your playlist without stopping current media." src="images/profile/add.gif"  onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'" onclick="showPlayer('v','<?php if($combine_arr[$test_count][$newtest_count]['id']!=""){echo $combine_arr[$test_count][$newtest_count]['id'];}else{echo $combine_arr[$test_count][$newtest_count]['id'];}?>','addvi');"/></a>
												<?php
												if(($combine_arr[$test_count][$newtest_count]['sharing_preference']==5 || $combine_arr[$test_count][$newtest_count]['sharing_preference']==2) && $combine_arr[$test_count][$newtest_count]['media_type'] == 114){
													?>
													<a onclick="download_directly_song('<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['title']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['creator']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['from']);?>')" href="javascript:void(0);">
													<img title="Add media to your cart for free download or purchase." onmouseout="this.src='../images/profile/download_down.gif'" onmouseover="this.src='../images/profile/download-over_down.gif'" src="../images/profile/download_down.gif"></a>
												<?php
												}/* else{
												?>
												<img src="images/profile/download-over_down.gif" />
												<?php
												} */
												?>
												</div>
												</div>
												<?php
												}else if($combine_arr[$test_count][$newtest_count]['media_type']==113){
												?>
												<div class="blkplayer"><div class="player">
												<?php 
													$get_images_tag_pro_cre = $new_profile_media_classobj ->get_all_gallery_images($combine_arr[$test_count][$newtest_count]['id']);
													for($count_img_tag_pro_cre=0;$count_img_tag_pro_cre<=count($get_images_tag_pro_cre);$count_img_tag_pro_cre++){
														if($get_all_img_tag_pro_cre ==""){
															$get_all_img_tag_pro_cre = $get_images_tag_pro_cre[$count_img_tag_pro_cre]['image_name'];
														}else{
															$get_all_img_tag_pro_cre = $get_all_img_tag_pro_cre.",".$get_images_tag_pro_cre[$count_img_tag_pro_cre]['image_name'];
														}
														if($galleryImagestitle_tag_pro_cre ==""){
															$galleryImagestitle_tag_pro_cre= $get_images_tag_pro_cre[$count_img_tag_pro_cre]['image_title'];
														}else{
															$galleryImagestitle_tag_pro_cre= $galleryImagestitle_tag_pro_cre.",".$get_images_tag_pro_cre[$count_img_tag_pro_cre]['image_title'];
														}
														$mediaSrc_tag_pro_cre = $get_images_tag_pro_cre[0]['image_name'];
													}
													$gal_id_tag_pro_cre = $get_images_tag_pro_cre[$count_img_tag_pro_cre]['media_id'];
													$get_other_user_id = $user_keys_data->get_general_user_idby_mediaid($combine_arr[$test_count][$newtest_count]['id']);
													$orgPath_tag_pro_cre = $s3->getNewURI_otheruser("medgallery",$get_other_user_id);//"https://medgallery.s3.amazonaws.com/";
													$thumbPath_tag_pro_cre = $s3->getNewURI_otheruser("medgalthumb",$get_other_user_id);//"https://medgalthumb.s3.amazonaws.com/";
													
												?>
												<a href="javascript:void(0);"><img src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" onclick="checkload('<?php echo $mediaSrc_tag_pro_cre;?>','<?php echo $get_all_img_tag_pro_cre; ?>','<?php echo $thumbPath_tag_pro_cre; ?>','<?php echo $orgPath_tag_pro_cre; ?>','<?php echo $galleryImagestitle_tag_pro_cre; ?>','<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>')"/></a>
												<!--<a href="javascript:void(0);"><img src="images/profile/add.gif"  onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'"/></a>-->
												<?php
													$get_all_img_tag_pro_cre ="";
													$galleryImagestitle_tag_pro_cre="";
													$mediaSrc_tag_pro_cre ="";
												?>
												<?php
												if(($combine_arr[$test_count][$newtest_count]['sharing_preference']==5 || $combine_arr[$test_count][$newtest_count]['sharing_preference']==2) && $combine_arr[$test_count][$newtest_count]['media_type'] == 114){
													?>
													<a onclick="download_directly_song('<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['title']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['creator']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['from']);?>')" href="javascript:void(0);">
													<img title="Add media to your cart for free download or purchase." onmouseout="this.src='../images/profile/download_down.gif'" onmouseover="this.src='../images/profile/download-over_down.gif'" src="../images/profile/download_down.gif"></a>
												<?php
												}/* else{
												?>
												<img src="images/profile/download-over_down.gif" />
												<?php
												} */
												?>
												</div>
												</div>
											<?php
												}else if($combine_arr[$test_count][$newtest_count]['media_type'] == 116){
													$get_channel_media = $new_profile_media_classobj ->get_all_media_selected_channel($combine_arr[$test_count][$newtest_count]['id']);
												?>
													<div class="blkplayer"><div class="player">
														<input type="hidden" id="channel_media_song<?php echo $newtest_count;?>" value="<?php echo $get_channel_media['songs']; ?>"/>
														<input type="hidden" id="channel_media_video<?php echo $newtest_count;?>" value="<?php echo $get_channel_media['videos']; ?>"/>
														<a href="javascript:void(0);"><img title="Play file now." src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" onclick="PlayAll('channel_media_video<?php echo $newtest_count;?>','channel_media_song<?php echo $newtest_count;?>')"/></a>
														<a href="javascript:void(0);"><img title="Add file to the bottom of your playlist without stopping current media." src="images/profile/add.gif"  onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'" onclick="showPlayer('v','<?php if($combine_arr[$test_count][$newtest_count]['id']!=""){echo $combine_arr[$test_count][$newtest_count]['id'];}else{ echo $combine_arr[$test_count][$newtest_count]['id'];}?>','addvi');"/></a>
														<?php
														if(($combine_arr[$test_count][$newtest_count]['sharing_preference']==5 || $combine_arr[$test_count][$newtest_count]['sharing_preference']==2) && $combine_arr[$test_count][$newtest_count]['media_type'] == 114){
															?>
															<a onclick="download_directly_song('<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['title']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['creator']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['from']);?>')" href="javascript:void(0);">
															<img title="Add media to your cart for free download or purchase." onmouseout="this.src='../images/profile/download_down.gif'" onmouseover="this.src='../images/profile/download-over_down.gif'" src="../images/profile/download_down.gif"></a>
														<?php
														}/* else{
														?>
														<img src="images/profile/download-over_down.gif" />
														<?php
														} */
														?>
													</div>
													</div>
												<?php
												}
												else
												{
												?>
												<div class="blkplayer"><div class="player">
													<a href="javascript:void(0);"><img src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" /></a>
													<a href="javascript:void(0);"><img src="images/profile/add.gif"  onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'" /></a>
													<?php
													if(($combine_arr[$test_count][$newtest_count]['sharing_preference']==5 || $combine_arr[$test_count][$newtest_count]['sharing_preference']==2) && $combine_arr[$test_count][$newtest_count]['media_type'] == 114){
														?>
														<a onclick="download_directly_song('<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['title']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['creator']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['from']);?>')" href="javascript:void(0);">
														<img title="Add media to your cart for free download or purchase." onmouseout="this.src='../images/profile/download_down.gif'" onmouseover="this.src='../images/profile/download-over_down.gif'" src="../images/profile/download_down.gif"></a>
													<?php
													}/* else{
													?>
													<img src="images/profile/download-over_down.gif" />
													<?php
													} */
													?>
												</div>
												</div>
												<?php
												}
											?>
											</div>
											<?php
										}
									}
								}
								
							}
							}
							?>
							<script type="text/javascript">             
								$(document).ready(function() {
									$("#gallery_images<?php echo $newtest_count; ?>").fancybox({
				helpers : {
					media : {},
					buttons : {},
					title : null
				}
			});

								
							});
							</script>

							<script type="text/javascript">             
							$(document).ready(function() {
										$("#Add_gallery_images<?php echo $newtest_count; ?>").fancybox({
										helpers : {
											media : {},
											buttons : {},
											title : null
										}
									});
								});
								</script>
							<?php	
							}
								?>
								<!--Code for displaying creator Project Ends Here-->
						</div>
					<?php
					}
					else if(isset($_GET['sel_val']))
					{
					?>
						<div id="mediaContentscrol" style="width:685px; height:400px; overflow:auto;">
							<?php
							$my_media_arr = array();
							//if(!isset($_GET['edit_gal']))
							//{
								$k=1;
								$getmedia=$new_profile_media_classobj->sel_all_media();
								if($getmedia!="" && $getmedia!=NULL)
								{
									while($res=mysql_fetch_assoc($getmedia))
									{
										if($res['media_type'] == $_GET['sel_val'])
										{
											if($res['media_type'] == 114)
											{
												$get_media_track = $new_profile_media_classobj->get_media_track($res['id']); 
												$res["track"] = $get_media_track['track'];
											}
											$res["media_from"] = "my_media";
											$my_media_arr[] = $res;
										}
									}
								}
								$getgeneral=$new_profile_media_classobj->sel_general();
								$exp_media_id =explode(',',$getgeneral['accepted_media_id']);
								$new_tag_media_arr = array();
								for($i=0;$i<count($exp_media_id);$i++)
								{
									$get_tagged_media=$new_profile_media_classobj->sel_all_tagged_media($exp_media_id[$i]);
									if($get_tagged_media!="" && $get_tagged_media!=NULL)
									{
										while($res1=mysql_fetch_assoc($get_tagged_media))
										{
											if($res1['media_type'] == $_GET['sel_val'])
											{
												if($res1['media_type'] == 114)
												{
													$get_media_track = $new_profile_media_classobj->get_media_track($res1['id']); 
													$res1["track"] = $get_media_track['track'];
												}
												$res1['media_from'] = "accepted_media";
												$get_type_of_tagged_media = $new_profile_media_classobj->sel_type_of_tagged_media($res1['media_type']);
												$res1['name'] = $get_type_of_tagged_media['name'];
												$new_tag_media_arr[] = $res1;
											}
										}
									}
								}
							//}
							?>
							<!--code for displaying Edit gallery-->
							<?php
							$new_edi_gal_media_arr = array();
							if(isset($_GET['edit_gal']) && $_GET['edit_gal']==1)
							{
								$k=1;
								if($getmedia!="" && $getmedia!=NULL)
								{
									while($res=mysql_fetch_assoc($getmedia))
									{
										if($res['media_type'] == $_GET['sel_val'])
										{
											if($res['media_type'] == 114)
											{
												$get_media_track = $new_profile_media_classobj->get_media_track($res['id']); 
												$res["track"] = $get_media_track['track'];
											}
											$res['media_from'] = "newedit_media";
											$new_edi_gal_media_arr[] = $res;
										}
									}
								}
							}
							?>
							
							
							<!-- Code For Displaying media From Fan --->
							<?php						
							$chk_fan = $new_profile_media_classobj->chk_fan();
							$new_fan_media_array = array();
							if($chk_fan!="" && $chk_fan!=NULL)
							{
								while($row_fan = mysql_fetch_assoc($chk_fan))
								{
									$new_fan_arr[] = $row_fan;
									$get_fan_media = $new_profile_media_classobj->get_fan_media($row_fan['buyer_id'],$row_fan['seller_id'],$row_fan['table_name'],$row_fan['type_id']);
									if($get_fan_media!="" && $get_fan_media!=NULL)
									{
										if(mysql_num_rows($get_fan_media)>0)
										{
											while($row_fan_media = mysql_fetch_assoc($get_fan_media))
											{
												$get_fan_generalmedia = $new_profile_media_classobj->get_all_fan_mediadetail($row_fan_media['media_id']);
												if($get_fan_generalmedia['media_type'] == $_GET['sel_val'])
												{
													if($get_fan_generalmedia['media_type'] == 114)
													{
														$get_media_track = $new_profile_media_classobj->get_media_track($get_fan_generalmedia['id']); 
														$get_fan_generalmedia["track"] = $get_media_track['track'];
													}
													$get_type_of_tagged_media = $new_profile_media_classobj->sel_type_of_tagged_media($get_fan_generalmedia['media_type']);
													$get_fan_generalmedia['name'] = $get_type_of_tagged_media['name'];
													$get_fan_generalmedia['media_from'] = "media_coming_fan";
													$new_fan_media_array[] = $get_fan_generalmedia;
												}	
											}
										}
									}
								}
							}
							
							$get_buyed_media = $new_profile_media_classobj->get_sale_media();
							$new_buyed_media_array = array();
							if($get_buyed_media!="" && $get_buyed_media!=NULL)
							{
								if(mysql_num_rows($get_buyed_media)>0){
									while($row_buyed_media = mysql_fetch_assoc($get_buyed_media)){
										$get_buyed_generalmedia = $new_profile_media_classobj->get_all_fan_mediadetail($row_buyed_media['media_id']);
										if($get_buyed_generalmedia['media_type'] == $_GET['sel_val'])
										{
											if($get_buyed_generalmedia['media_type'] == 114)
											{
												$get_media_track = $new_profile_media_classobj->get_media_track($get_buyed_generalmedia['id']); 
												$get_buyed_generalmedia["track"] = $get_media_track['track'];
											}
											$get_type_of_tagged_media = $new_profile_media_classobj->sel_type_of_tagged_media($get_buyed_generalmedia['media_type']);
											$get_buyed_generalmedia['name'] = $get_type_of_tagged_media['name'];
											$get_buyed_generalmedia['media_from'] = "media_coming_buyed";
											$new_buyed_media_array[] = $get_buyed_generalmedia;
										}
									}
								}
							}
							
							$get_media_tag_art_pro = $new_profile_media_classobj->get_artist_project_tag($_SESSION['login_email']);
							if($get_media_tag_art_pro!="" && $get_media_tag_art_pro!=NULL)
							{
								if(mysql_num_rows($get_media_tag_art_pro)>0)
								{
									$get_songs_new = array();
									$get_video_new = array();
									$get_gallery_new = array();
									$is = 0;
									$iv = 0;
									$ig = 0;
									while($art_pro_tag = mysql_fetch_assoc($get_media_tag_art_pro))
									{
										if($_GET['sel_val']==114)
										{
											$get_songs = explode(",",$art_pro_tag['tagged_songs']);
											for($count_art_tag_song=0;$count_art_tag_song<count($get_songs);$count_art_tag_song++)
											{
												if($get_songs[$count_art_tag_song]!="")
												{
													$get_songs_new[$is] = $get_songs[$count_art_tag_song];
													$is = $is + 1;
												}
											}
											if($get_songs_new!=NULL)
											{
												$get_songs_n = array_unique($get_songs_new);
											}
										}
										if($_GET['sel_val']==115)
										{
											$get_video_new = array();
											$get_video = explode(",",$art_pro_tag['tagged_videos']);
											for($count_art_tag_video=0;$count_art_tag_video<count($get_video);$count_art_tag_video++)
											{
												if($get_video[$count_art_tag_video]!="")
												{
													$get_video_new[$iv] = $get_video[$count_art_tag_video];
													$iv = $iv + 1;
												}
											}
											if($get_video_new!=NULL)
											{
												$get_video_n = array_unique($get_video_new);
											}
										}
										if($_GET['sel_val']==113)
										{
											$get_gallery = explode(",",$art_pro_tag['tagged_galleries']);
											for($count_art_tag_gal=0;$count_art_tag_gal<count($get_gallery);$count_art_tag_gal++)
											{
												if($get_gallery[$count_art_tag_gal]!="")
												{
													$get_gallery_new[$ig] = $get_gallery[$count_art_tag_gal];
													$ig = $ig + 1;
												}
											}
											if($get_gallery_new!=NULL)
											{
												$get_gallery_n = array_unique($get_gallery_new);
											}
										}
									}
								}
							}
							
							$get_media_tag_art_eve = $new_profile_media_classobj->get_artist_event_tag($_SESSION['login_email']);
							if($get_media_tag_art_eve!="" && $get_media_tag_art_eve!=NULL)
							{
								if(mysql_num_rows($get_media_tag_art_eve)>0)
								{
									$get_songs_newe = array();
									$get_video_newe = array();
									$get_gallery_newe = array();
									$is = 0;
									$iv = 0;
									$ig = 0;
									while($art_pro_tag = mysql_fetch_assoc($get_media_tag_art_eve))
									{
										if($_GET['sel_val']==114)
										{
											$get_songs = explode(",",$art_pro_tag['tagged_songs']);
											for($count_art_tag_song=0;$count_art_tag_song<count($get_songs);$count_art_tag_song++)
											{
												if($get_songs[$count_art_tag_song]!="")
												{
													$get_songs_newe[$is] = $get_songs[$count_art_tag_song];
													$is = $is + 1;
												}
											}
											if($get_songs_newe!=NULL)
											{
												$get_songs_ne = array_unique($get_songs_newe);
											}
										}
										if($_GET['sel_val']==115)
										{
											$get_video = explode(",",$art_pro_tag['tagged_videos']);
											for($count_art_tag_video=0;$count_art_tag_video<count($get_video);$count_art_tag_video++)
											{
												if($get_video[$count_art_tag_video]!="")
												{
													$get_video_newe[$iv] = $get_video[$count_art_tag_video];
													$iv = $iv + 1;
												}
											}
											if($get_video_newe!=NULL)
											{
												$get_video_ne = array_unique($get_video_newe);
											}
										}
										if($_GET['sel_val']==113)
										{
											$get_gallery = explode(",",$art_pro_tag['tagged_galleries']);
											for($count_art_tag_gallery=0;$count_art_tag_gallery<count($get_gallery);$count_art_tag_gallery++)
											{
												if($get_gallery[$count_art_tag_gallery]!="")
												{
													$get_gallery_newe[$ig] = $get_gallery[$count_art_tag_gallery];
													$ig = $ig + 1;
												}
											}
											if($get_gallery_newe!=NULL)
											{
												$get_gallery_ne = array_unique($get_gallery_newe);
											}
										}
									}
								}
							}
							
							$get_media_tag_com_pro = $new_profile_media_classobj->get_community_project_tag($_SESSION['login_email']);
							if($get_media_tag_com_pro!="" && $get_media_tag_com_pro!=NULL)
							{
								if(mysql_num_rows($get_media_tag_com_pro)>0)
								{
									$get_songs_new_c = array();
									$get_video_new_c = array();
									$get_gallery_new_c = array();
									$is_c = 0;
									$iv_c = 0;
									$ig_c = 0;
									
									while($com_pro_tag = mysql_fetch_assoc($get_media_tag_com_pro))
									{
										if($_GET['sel_val']==114)
										{
											$get_com_songs = explode(",",$com_pro_tag['tagged_songs']);
											for($count_com_tag_song=0;$count_com_tag_song<count($get_com_songs);$count_com_tag_song++)
											{
												if($get_com_songs[$count_com_tag_song]!="")
												{
													$get_songs_new_c[$is_c] = $get_com_songs[$count_com_tag_song];
													$is_c = $is_c + 1;
												}
											}
											if($get_songs_new_c!=NULL)
											{
												$get_songs_n_c = array_unique($get_songs_new_c);
											}
										}
										if($_GET['sel_val']==115)
										{
											$get_com_video = explode(",",$com_pro_tag['tagged_videos']);
											for($count_com_tag_video=0;$count_com_tag_video<count($get_com_video);$count_com_tag_video++)
											{
												if($get_com_video[$count_com_tag_video]!="")
												{
													$get_video_new_c[$iv_c] = $get_com_video[$count_com_tag_video];
													$iv_c = $iv_c + 1;
												}
											}
											if($get_video_new_c!=NULL)
											{
												$get_video_n_c = array_unique($get_video_new_c);
											}
										}
										if($_GET['sel_val']==113)
										{
											$get_com_gallery = explode(",",$com_pro_tag['tagged_galleries']);
											for($count_com_tag_gallery=0;$count_com_tag_gallery<count($get_com_gallery);$count_com_tag_gallery++)
											{
												if($get_com_gallery[$count_com_tag_gallery]!="")
												{
													$get_gallery_new_c[$ig_c] = $get_com_gallery[$count_com_tag_gallery];
													$ig_c = $ig_c + 1;
												}
											}
											if($get_gallery_new_c!=NULL)
											{
												$get_gallery_n_c = array_unique($get_gallery_new_c);
											}
										}
									}
								}
							}
							
							$get_media_tag_com_eve = $new_profile_media_classobj->get_community_event_tag($_SESSION['login_email']);
							if($get_media_tag_com_eve!="" && $get_media_tag_com_eve!=NULL)
							{
								if(mysql_num_rows($get_media_tag_com_eve)>0)
								{
									$get_songs_new_ce = array();
									$get_video_new_ce = array();
									$get_gallery_new_ce = array();
									$is_c = 0;
									$iv_c = 0;
									$ig_c = 0;
									while($com_pro_tag = mysql_fetch_assoc($get_media_tag_com_eve))
									{
										if($_GET['sel_val']==114)
										{
											$get_com_songs = explode(",",$com_pro_tag['tagged_songs']);
											for($count_com_tag_song=0;$count_com_tag_song<count($get_com_songs);$count_com_tag_song++)
											{
												if($get_com_songs[$count_com_tag_song]!="")
												{
													$get_songs_new_ce[$is_c] = $get_com_songs[$count_com_tag_song];
													$is_c = $is_c + 1;
												}
											}
											if($get_songs_new_ce!=NULL)
											{
												$get_songs_n_ec = array_unique($get_songs_new_ce);
											}
										}
										if($_GET['sel_val']==115)
										{
											$get_com_video = explode(",",$com_pro_tag['tagged_videos']);
											for($count_com_tag_video=0;$count_com_tag_video<count($get_com_video);$count_com_tag_video++)
											{
												if($get_com_video[$count_com_tag_video]!="")
												{
													$get_video_new_ce[$iv_c] = $get_com_video[$count_com_tag_video];
													$iv_c = $iv_c + 1;
												}
											}
											if($get_video_new_ce!=NULL)
											{
												$get_video_n_ec = array_unique($get_video_new_ce);
											}
										}
										if($_GET['sel_val']==113)
										{
											$get_com_gallery = explode(",",$com_pro_tag['tagged_galleries']);
											for($count_com_tag_gallery=0;$count_com_tag_gallery<count($get_com_gallery);$count_com_tag_gallery++)
											{
												if($get_com_gallery[$count_com_tag_gallery]!="")
												{
													$get_gallery_new_ce[$ig_c] = $get_com_gallery[$count_com_tag_gallery];
													$ig_c = $ig_c + 1;
												}
											}
											if($get_gallery_new_ce!=NULL)
											{
												$get_gallery_n_ec = array_unique($get_gallery_new_ce);
											}
										}
									}
								}
							}
							
							$get_media_cre_art_pro = $new_profile_media_classobj->get_project_create();
							if(!empty($get_media_cre_art_pro))
							{
								$get_songs_pn = array();
								$get_video_pn = array();
								$get_gallery_pn = array();
								$is_p = 0;
								$iv_p = 0;
								$ig_p = 0;
								
								//while($art_pro_tag = mysql_fetch_assoc($get_media_cre_art_pro))
								for($cr_i=0;$cr_i<count($get_media_cre_art_pro);$cr_i++)
								{
									if($_GET['sel_val']==114)
									{
										$get_csongs = explode(",",$get_media_cre_art_pro[$cr_i]['tagged_songs']);
										for($count_art_cr_song=0;$count_art_cr_song<count($get_csongs);$count_art_cr_song++)
										{
											if($get_csongs[$count_art_cr_song]!="")
											{
												$get_songs_pn[$is_p] = $get_csongs[$count_art_cr_song];
												$is_p = $is_p + 1;
											}
										}
										if($get_songs_pn!=NULL)
										{
											$get_songs_p_new = array_unique($get_songs_pn);
										}
									}
									if($_GET['sel_val']==115)
									{
										$get_video_pn = array();
										$get_cvideo = explode(",",$get_media_cre_art_pro[$cr_i]['tagged_videos']);
										for($count_art_cr_video=0;$count_art_cr_video<count($get_cvideo);$count_art_cr_video++)
										{
											if($get_cvideo[$count_art_cr_video]!="")
											{
												$get_video_pn[$iv_p] = $get_cvideo[$count_art_cr_video];
												$iv_p = $iv_p + 1;
											}
										}
										if($get_video_pn!=NULL)
										{
											$get_video_p_new = array_unique($get_video_pn);
										}
									}
									if($_GET['sel_val']==113)
									{
										$get_cgallery = explode(",",$get_media_cre_art_pro[$cr_i]['tagged_galleries']);
										for($count_art_cr_gallery=0;$count_art_cr_gallery<count($get_cgallery);$count_art_cr_gallery++)
										{
											if($get_cgallery[$count_art_cr_gallery]!="")
											{
												$get_gallery_pn[$ig_p] = $get_cgallery[$count_art_cr_gallery];
												$ig_p = $ig_p + 1;
											}
										}
										if($get_gallery_pn!=NULL)
										{
											$get_gallery_p_new = array_unique($get_gallery_pn);
										}
									}
								}
							}
							
							if(!isset($get_songs_n)){
								$get_songs_n = array();}
							if(!isset($get_video_n)){
								$get_video_n = array();}
							if(!isset($get_gallery_n)){
								$get_gallery_n = array();}
							
							if(!isset($get_songs_n_ec)){
								$get_songs_n_ec = array();}
							if(!isset($get_video_n_ec)){
								$get_video_n_ec = array();}
							if(!isset($get_gallery_n_ec)){
								$get_gallery_n_ec = array();}
							
							if(!isset($get_songs_ne)){
								$get_songs_ne = array();}
							if(!isset($get_video_ne)){
								$get_video_ne = array();}
							if(!isset($get_gallery_ne)){
								$get_gallery_ne = array();}
							
							if(!isset($get_songs_n_c)){
								$get_songs_n_c = array();}
							if(!isset($get_video_n_c)){
								$get_video_n_c = array();}
							if(!isset($get_gallery_n_c)){
								$get_gallery_n_c = array();}
							
							if(!isset($get_songs_p_new)){
								$get_songs_p_new = array();}
							if(!isset($get_video_p_new)){
								$get_video_p_new = array();}
							if(!isset($get_gallery_p_new)){
								$get_gallery_p_new = array();}

							$get_songs_pnew_w_c = array_merge($get_songs_n,$get_video_n,$get_gallery_n,$get_songs_n_c,$get_video_n_c,$get_gallery_n_c,$get_songs_p_new,$get_video_p_new,$get_gallery_p_new,$get_songs_ne,$get_video_ne,$get_gallery_ne,$get_songs_n_ec,$get_video_n_ec,$get_gallery_n_ec);
							if($get_songs_pnew_w_c!=NULL)
							{
								$get_songs_pnew_ct = array_unique($get_songs_pnew_w_c);
							}
							$get_songs_final_ct = array();
							
							for($count_art_cr_song=0;$count_art_cr_song<count($get_songs_pnew_ct);$count_art_cr_song++)
							{
								if($get_songs_pnew_ct[$count_art_cr_song]!="")
								{
									$get_media_tag_art_pro_song = $new_profile_media_classobj->get_tag_media_info($get_songs_pnew_ct[$count_art_cr_song]);
									if(!empty($get_media_tag_art_pro_song)){
										if($get_media_tag_art_pro_song['media_type'] == 114)
										{
											$get_media_track = $new_profile_media_classobj->get_media_track($get_media_tag_art_pro_song['id']); 
											$get_media_tag_art_pro_song["track"] = $get_media_track['track'];
										}
										$get_media_tag_art_pro_song['media_from'] ="new_tagging_media";
										$get_songs_final_ct[] = $get_media_tag_art_pro_song;
									}
									
								}
							}
							$get_songs_final_ct = array();
							$new_medi = $new_profile_media_classobj->get_creator_heis();
							$new_creator_creator_media_array = array();
							if(!empty($new_medi))
							{
								for($count_cre_m=0;$count_cre_m<count($new_medi);$count_cre_m++)
								{
									if($_GET['sel_val']== $new_medi[$count_cre_m]['media_type'])
									{
										$get_media_type = $new_profile_media_classobj->sel_type_of_tagged_media($new_medi[$count_cre_m]['media_type']);
										if($new_medi[$count_cre_m]['media_type'] == 114)
										{
											$get_media_track = $new_profile_media_classobj->get_media_track($new_medi[$count_cre_m]['id']); 
											$new_medi[$count_cre_m]["track"] = $get_media_track['track'];
										}
										$new_medi[$count_cre_m]['name'] = $get_media_type['name'];
										$new_medi[$count_cre_m]['media_from'] = "media_creator_creator";
										$new_creator_creator_media_array[] = $new_medi[$count_cre_m];
									}
								}
							}

							/*This function will display the user in which profile he is creator and display media where that profile was creator Ends here*/

							/*This function will display the user in which profile he is from and display media where that profile was creator*/
							$new_medi_from = $new_profile_media_classobj->get_from_heis();
							$new_from_from_media_array = array();
							if(!empty($new_medi_from))
							{
								for($count_cre_m=0;$count_cre_m<count($new_medi_from);$count_cre_m++)
								{
									if($_GET['sel_val']== $new_medi_from[$count_cre_m]['media_type'])
									{
										$get_media_type = $new_profile_media_classobj->sel_type_of_tagged_media($new_medi_from[$count_cre_m]['media_type']);
										if($new_medi_from[$count_cre_m]['media_type'] == 114)
										{
											$get_media_track = $new_profile_media_classobj->get_media_track($new_medi_from[$count_cre_m]['id']); 
											$new_medi_from[$count_cre_m]["track"] = $get_media_track['track'];
										}
										$new_medi_from[$count_cre_m]['name'] = $get_media_type['name'];
										$new_medi_from[$count_cre_m]['media_from'] = "media_from_from";
										$new_from_from_media_array[] = $new_medi_from[$count_cre_m];
									}
								}
							}
							/*This function will display the user in which profile he is from and display media where that profile was creator Ends here*/
							
							/*This function will display the user in which profile he is from or creator and tagged and display media where that profile was creator*/
							$new_medi_tagged = $new_profile_media_classobj->get_the_user_tagin_project($_SESSION['login_email']);
							$new_tagged_from_media_array = array();
							if(!empty($new_medi_tagged))
							{
								for($count_cre_m=0;$count_cre_m<count($new_medi_tagged);$count_cre_m++)
								{
									if($_GET['sel_val']== $new_medi_tagged[$count_cre_m]['media_type'])
									{
										$get_media_type = $new_profile_media_classobj->sel_type_of_tagged_media($new_medi_tagged[$count_cre_m]['media_type']);
										if($new_medi_tagged[$count_cre_m]['media_type'] == 114)
										{
											$get_media_track = $new_profile_media_classobj->get_media_track($new_medi_tagged[$count_cre_m]['id']); 
											$new_medi_tagged[$count_cre_m]["track"] = $get_media_track['track'];
										}
										$new_medi_tagged[$count_cre_m]['name'] = $get_media_type['name'];
										$new_medi_tagged[$count_cre_m]['media_from'] = "media_creator_from_tag";
										$new_tagged_from_media_array[] = $new_medi_tagged[$count_cre_m];
									}
								}
							}
							/*This function will display the user in which profile he is from or creator and tagged and display media where that profile was creator Ends here*/
							
							/*This function will display in which media the curent user was creator*/
							$get_where_creator_media = $new_profile_media_classobj->get_media_where_creator_or_from();
							$new_creator_media_array = array();
							if(!empty($get_where_creator_media))
							{
								for($count_tag_m=0;$count_tag_m<count($get_where_creator_media);$count_tag_m++)
								{
									if($_GET['sel_val']== $get_where_creator_media[$count_tag_m]['media_type'])
									{
										$get_media_type = $new_profile_media_classobj->sel_type_of_tagged_media($get_where_creator_media[$count_tag_m]['media_type']);
										if($get_where_creator_media[$count_tag_m]['media_type'] == 114)
										{
											$get_media_track = $new_profile_media_classobj->get_media_track($get_where_creator_media[$count_tag_m]['id']); 
											$get_where_creator_media[$count_tag_m]["track"] = $get_media_track['track'];
										}
										$get_where_creator_media[$count_tag_m]['name'] = $get_media_type['name'];
										$get_where_creator_media[$count_tag_m]['media_from'] = "media_creator";
										$new_creator_media_array[] = $get_where_creator_media[$count_tag_m];
									}
								}
							}
							/*This function will display in which media the curent user was creator ends here*/
							$cre_frm_med = array();
							$cre_frm_med = $new_profile_media_classobj->get_media_create_with_type($GET['sel_val']);
							
							
							/*Get Subscription media*/
							$new_subscription_media = array();
							$get_subscription_media_res = $new_profile_media_classobj->get_subscription_media();
							for($count_sub=0;$count_sub<=count($get_subscription_media_res);$count_sub++)
							{
								if($get_subscription_media_res[$count_sub]!="")
								{
									if($get_subscription_media_res[$count_sub]['media_type'] == $_GET['sel_val'])
									{
										$get_media_type = $new_profile_media_classobj->sel_type_of_tagged_media($get_subscription_media_res[$count_sub]['media_type']);
										if($get_subscription_media_res[$count_sub]['media_type']==114)
										{
											$get_media_track_res = $new_profile_media_classobj->get_media_track($get_subscription_media_res[$count_sub]['id']); 
											$get_subscription_media_res[$count_sub]['track'] = $get_media_track_res['track'];
										}
										$get_subscription_media_res[$count_sub]['name'] = $get_media_type['name'];
										$get_subscription_media_res[$count_sub]['media_from'] = "email_subscription";
										$new_subscription_media[] = $get_subscription_media_res[$count_sub];
									}
								}
							}
							if($member_var==0){
								$new_subscription_media = array();
							}
							$combine_arr = array();
							$combine_arr[] = array_merge ($my_media_arr,$new_tag_media_arr,$new_edi_gal_media_arr,$new_fan_media_array,$new_creator_media_array,$new_creator_creator_media_array,$new_from_from_media_array,$new_tagged_from_media_array,$cre_frm_med,$get_songs_final_ct,$new_subscription_media,$new_buyed_media_array);
							$sort = array();
							foreach($combine_arr[0] as $k=>$v) {
							
								//$create_c = strpos($v['creator'],'(');
								//$end_c1 = substr($v['creator'],0,$create_c);
								$end_c1 = $v['creator'];
								$end_c = trim($end_c1);
								
								//$create_f = strpos($v['from'],'(');
								//$end_f1 = substr($v['from'],0,$create_f);
								$end_f1 = $v['from'];
								$end_f = trim($end_f1);
								
								$sort['creator'][$k] = strtolower($end_c);
								
								$sort['from'][$k] = strtolower($end_f);
								if($v['track']!=NULL){								
									$sort['track'][$k] = $v['track'];
								}else{									
									$sort['track'][$k] = " ";
								}
								$sort['title'][$k] = strtolower($v['title']);
								$sort['name'][$k] = strtolower($v['name']);
							}
							if(isset($_GET['disp_val']) && isset($_GET['disp_type']))
							{
								if($_GET['disp_val']=="DESC"){
									if($_GET['disp_type']=="type")
									{
										array_multisort($sort['name'], SORT_DESC , $combine_arr[0]);
									}else{
										array_multisort($sort[$_GET['disp_type']], SORT_DESC , $combine_arr[0]);
									}
								}if($_GET['disp_val']=="ASC"){
									if($_GET['disp_type']=="type")
									{
									//var_dump($sort['name']);
										array_multisort($sort['name'], SORT_ASC , $combine_arr[0]);
									}else{
										array_multisort($sort[$_GET['disp_type']], SORT_ASC , $combine_arr[0]);
									}
								}
							}else{
								if(!empty($sort))
								{
									array_multisort($sort['creator'], SORT_ASC, $sort['from'], SORT_ASC, $sort['track'], SORT_NUMERIC ,SORT_ASC,$sort['title'], SORT_ASC,$combine_arr[0]);
								}
							}
							$dummy_pr = array();
							$test_count = 0;
							$get_all_del_id = $new_profile_media_classobj->get_all_del_media_id();
							$exp_all_del_id = explode(",",$get_all_del_id);
							for($count_all=0;$count_all<=count($combine_arr[$test_count]);$count_all++)
							{
								for($count_all_del=0;$count_all_del<=count($exp_all_del_id);$count_all_del++)
								{
									if($exp_all_del_id[$count_all_del]==""){continue;}
									else{
										if($exp_all_del_id[$count_all_del]==$combine_arr[$test_count][$count_all]['id'])
										{
											$combine_arr[$test_count][$count_all]="";
										}
									}
								}
							}
							for($newtest_count = 0;$newtest_count<=count($combine_arr[$test_count]);$newtest_count++){
								if(in_array($combine_arr[$test_count][$newtest_count]['id'],$dummy_pr))
							    {
								
							    }
							    else
							    {
									$dummy_pr[] = $combine_arr[$test_count][$newtest_count]['id'];
								
								//if($combine_arr[$test_count][$newtest_count]['name'] !="")
								//{
								//echo $combine_arr[$test_count][$newtest_count]['media_from'];
								if($combine_arr[$test_count][$newtest_count]['media_from']=="my_media")
								{
								?>
									<div class="tableCont" id="AccordionContainer">
									<div class="blkD" style="width:60px;"><?php if($combine_arr[$test_count][$newtest_count]['name']=="Galleries/Images"){echo "Images";}else{echo $combine_arr[$test_count][$newtest_count]['name'];} ?></div>
									<div class="blkI" style="width:127px;"><?php
									$exp_creator_info = explode("|",$combine_arr[$test_count][$newtest_count]['creator_info']);
									$pro_url = $new_profile_media_classobj->get_creator_url($exp_creator_info[0],$exp_creator_info[1]);
									?>
									<a href="<?php if($pro_url['profile_url'] !=""){ echo "/".$pro_url['profile_url']; }else { echo "javascript:void(0)"; }?>">
									<?php
									if($combine_arr[$test_count][$newtest_count]['creator']!="")
									{
										//$end_pos = strpos($combine_arr[$test_count][$newtest_count]['creator'],'(');
										//if($end_pos !="" || $end_pos !=false){
										//echo substr($combine_arr[$test_count][$newtest_count]['creator'],0,$end_pos);}else{
										echo $combine_arr[$test_count][$newtest_count]['creator'];
										//}
									}else{
									?>&nbsp;<?php
									}
										?></a></div>
										
									<div class="blkK"><?php
									$exp_from_info = explode("|",$combine_arr[$test_count][$newtest_count]['from_info']);
									$pro_url_from = $new_profile_media_classobj->get_from_url($exp_from_info[0],$exp_from_info[1]);
									?>
									<a href="<?php if($pro_url_from['profile_url'] !=""){ echo "/".$pro_url_from['profile_url']; }else { echo "javascript:void(0)"; }?>"><?php 
									if($combine_arr[$test_count][$newtest_count]['from'] !="")
									{
										//$end_pos_from = strpos($combine_arr[$test_count][$newtest_count]['from'],'(');
										//if($end_pos_from !="" || $end_pos_from!= false){
										//echo substr($combine_arr[$test_count][$newtest_count]['from'],0,$end_pos_from);}else{
										echo $combine_arr[$test_count][$newtest_count]['from'];
										//}
									}else{
									?>&nbsp;<?php
									}
										?></a></div>
									<div class="blkB"><?php
									if($combine_arr[$test_count][$newtest_count]['title']!="")
									{
										if($combine_arr[$test_count][$newtest_count]['media_type'] == 114)
										{
											//$get_media_track = $new_profile_media_classobj->get_media_track($combine_arr[$test_count][$newtest_count]['id']); 
											if($combine_arr[$test_count][$newtest_count]['track'] != "" && $combine_arr[$test_count][$newtest_count]['track'] != NULL && $combine_arr[$test_count][$newtest_count]['track'] !=" "){
												echo $combine_arr[$test_count][$newtest_count]['track'].".".stripslashes($combine_arr[$test_count][$newtest_count]['title']);
											}else{
												echo stripslashes($combine_arr[$test_count][$newtest_count]['title']);
											}
										}
										else{
										echo stripslashes($combine_arr[$test_count][$newtest_count]['title']); }
									}else{
									?>&nbsp;<?php
									}
										?></div>
									<div class="blkplayer"><div class="player">
									<?php
										if($combine_arr[$test_count][$newtest_count]['media_type'] == 114){
										?>
										<a href="javascript:void(0);"><img title="Play file now." src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" onclick="showPlayer('a','<?php if($combine_arr[$test_count][$newtest_count]['id']!=""){echo $combine_arr[$test_count][$newtest_count]['id'];}else{echo $combine_arr[$test_count][$newtest_count]['id'];}?>','play');"/></a>
										<a href="javascript:void(0);"><img title="Add file to the bottom of your playlist without stopping current media." src="images/profile/add.gif"  onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'" onclick="showPlayer('a','<?php if($combine_arr[$test_count][$newtest_count]['id']!=""){echo $combine_arr[$test_count][$newtest_count]['id'];}else{echo $combine_arr[$test_count][$newtest_count]['id'];}?>','add');"/></a>
										<?php
										}
										else if($combine_arr[$test_count][$newtest_count]['media_type'] == 113){
											$get_images = $new_profile_media_classobj ->get_all_gallery_images($combine_arr[$test_count][$newtest_count]['id']);
											for($count_img=0;$count_img<=count($get_images);$count_img++){
												if($get_all_img ==""){
													$get_all_img = $get_images[$count_img]['image_name'];
												}else{
													$get_all_img = $get_all_img.",".$get_images[$count_img]['image_name'];
												}
												if($galleryImagestitle ==""){
													$galleryImagestitle = $get_images[$count_img]['image_title'];
												}else{
													$galleryImagestitle = $galleryImagestitle.",".$get_images[$count_img]['image_title'];
												}
												$mediaSrc = $get_images[0]['image_name'];
											}
											$gal_id = $get_images[$count_img]['media_id'];
											$get_other_user_id = $user_keys_data->get_general_user_idby_mediaid($combine_arr[$test_count][$newtest_count]['id']);
											$orgPath = $s3->getNewURI_otheruser("medgallery",$get_other_user_id);//"https://medgallery.s3.amazonaws.com/";
											$thumbPath = $s3->getNewURI_otheruser("medgalthumb",$get_other_user_id);//"https://medgalthumb.s3.amazonaws.com/";
										?>
										<a href="javascript:void(0);"><img title="Play file now." src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" onclick="checkload('<?php echo $mediaSrc;?>','<?php echo $get_all_img; ?>','<?php echo $thumbPath; ?>','<?php echo $orgPath; ?>','<?php echo $galleryImagestitle; ?>','<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>')"/></a>
										<!--<a href="javascript:void(0);"><img title="Add file to the bottom of your playlist without stopping current media." src="images/profile/add.gif"  onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'"/></a>-->
										<?php
											$get_all_img ="";
											$galleryImagestitle="";
											$mediaSrc="";
										}
										else if($combine_arr[$test_count][$newtest_count]['media_type'] == 115){
										?>
										<a href="javascript:void(0);"><img title="Play file now." src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" onclick="showPlayer('v','<?php if($combine_arr[$test_count][$newtest_count]['id']!=""){echo $combine_arr[$test_count][$newtest_count]['id'];}else{ echo $combine_arr[$test_count][$newtest_count]['id'];}?>','playlist');"/></a>
										<a href="javascript:void(0);"><img title="Add file to the bottom of your playlist without stopping current media." src="images/profile/add.gif"  onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'" onclick="showPlayer('v','<?php if($combine_arr[$test_count][$newtest_count]['id']!=""){echo $combine_arr[$test_count][$newtest_count]['id'];}else{ echo $combine_arr[$test_count][$newtest_count]['id'];}?>','addvi');"/></a>
										<?php
										}else if($combine_arr[$test_count][$newtest_count]['media_type'] == 116){
											$get_channel_media = $new_profile_media_classobj ->get_all_media_selected_channel($combine_arr[$test_count][$newtest_count]['id']);
											
										?>
										<input type="hidden" id="channel_media_song<?php echo $newtest_count;?>" value="<?php echo $get_channel_media['songs']; ?>"/>
										<input type="hidden" id="channel_media_video<?php echo $newtest_count;?>" value="<?php echo $get_channel_media['videos']; ?>"/>
										<a href="javascript:void(0);"><img title="Play file now." src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" onclick="PlayAll('channel_media_video<?php echo $newtest_count;?>','channel_media_song<?php echo $newtest_count;?>')"/></a>
										<a href="javascript:void(0);"><img title="Add file to the bottom of your playlist without stopping current media." src="images/profile/add.gif"  onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'" onclick="showPlayer('v','<?php if($combine_arr[$test_count][$newtest_count]['id']!=""){echo $combine_arr[$test_count][$newtest_count]['id'];}else{ echo $combine_arr[$test_count][$newtest_count]['id'];}?>','addvi');"/></a>
										<?php
										}else{
										?>
										<a href="javascript:void(0);"><img title="Play file now." src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" /></a>
										<a href="javascript:void(0);"><img title="Add file to the bottom of your playlist without stopping current media." src="images/profile/add.gif"  onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'"/></a>
										<?php
										}
										if(($combine_arr[$test_count][$newtest_count]['sharing_preference']==5 || $combine_arr[$test_count][$newtest_count]['sharing_preference']==2) && $combine_arr[$test_count][$newtest_count]['media_type'] == 114){
											?>
											<a onclick="download_directly_song('<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['title']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['creator']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['from']);?>')" href="javascript:void(0);">
											<img title="Add media to your cart for free download or purchase." onmouseout="this.src='../images/profile/download_down.gif'" onmouseover="this.src='../images/profile/download-over_down.gif'" src="../images/profile/download_down.gif"></a>
										<?php
										}/* else{
										?>
										<img src="images/profile/download-over_down.gif" />
										<?php
										} */
										?>
										</div></div>
									<div class="icon" onclick="runAccordion(<?php echo $combine_arr[$test_count][$newtest_count]['id']; ?>);">
										<div onselectstart="return false;"><img title="View details for media." src="images/profile/view.jpg" /></div>
									</div>
									<div class="icon"><a href="add_media.php?edit=<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>" onclick="save_cookie()"><img title="Edit media." src="images/profile/edit.png" /></a></div>
									<div class="icon"><a onclick="return confirmdelete('<?php echo $combine_arr[$test_count][$newtest_count]['title'];?>','<?php if($combine_arr[$test_count][$newtest_count]['name']=="Galleries/Images"){echo "Images";}else{echo $combine_arr[$test_count][$newtest_count]['name'];} ?>')" href="delete_media.php?id=<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>&whos_media=my_media"><img title="Remove media from your library." src="images/profile/delete.png" /></a></div>
									<div class="expandable" id="Accordion<?php echo $combine_arr[$test_count][$newtest_count]['id']; ?>Content">
										<div class="affiBlock">
											<div class="RowCont">
												<div class="sideL">Plays :</div>
												<div class="sideR"><a href="" target="_blank"><?php if($combine_arr[$test_count][$newtest_count]['play_count']!=""){ echo $combine_arr[$test_count][$newtest_count]['play_count'];}else{ echo "0";}?></a></div>
											</div>
											<?php
												if($combine_arr[$test_count][$newtest_count]['sharing_preference']==5 || $combine_arr[$test_count][$newtest_count]['sharing_preference']==2){
												?>
												<div class="RowCont">
													<div class="sideL">Downloads :</div>
													<div class="sideR"><a href="javascript:void(0);"><?php echo $combine_arr[$test_count][$newtest_count]['download_count'];?></a></div>
												</div>
												<?php
												}
												if($combine_arr[$test_count][$newtest_count]['media_type']=='113')
												{
												
											?>
											<div class="RowCont">
												<div class="sideL">Image :</div>
												<div class="sideR"><a id="gallery_images<?php echo $newtest_count; ?>" class="fancybox fancybox.ajax" href="gallery_images.php?gal_id=<?php echo $combine_arr[$test_count][$newtest_count]['id']; ?>">Edit Image</a></div>
											</div>	
											<div class="RowCont">
												<div class="sideL"></div>
												<?php 
												$find_gal = $new_profile_media_classobj->get_gallery_editable($combine_arr[$test_count][$newtest_count]['id']);
												if($find_gal['gallery_id'] !=0)
												{
												?>
													<div class="sideR"><a id="Add_gallery_images<?php echo $newtest_count; ?>" class="fancybox fancybox.ajax" href="add_gallery_images.php?gal_id=<?php echo $combine_arr[$test_count][$newtest_count]['id']; ?>">Add Image</a></div>
												<?php
												}
												?>
											</div>
											<?php
												
												}
												else
												{
											?>	
											<!--<div class="RowCont">
												<div class="sideL">Media URL :</div>
												<div class="sideR"><a href="" target="_blank">www.<?php echo $domainname; ?></a></div>
											</div>-->
											<?php
												}
											?>
										</div>
									</div>
								</div>
									<?php
									$k = $k+1;
								}
								
								else if($combine_arr[$test_count][$newtest_count]['media_from']=="accepted_media")
								{
									if(isset($_GET['sel_val']) && $_GET['sel_val']!='all media')
									{
										$chk_tag_user = $new_profile_media_classobj->get_loggedin_info($_SESSION['login_email']);
										if($chk_tag_user['general_user_id']!=$combine_arr[$test_count][$newtest_count]['general_user_id'])
										{
											if($_GET['sel_val']==$combine_arr[$test_count][$newtest_count]['media_type'])
											{
												$get_type_of_tagged_media=$new_profile_media_classobj->sel_type_of_tagged_media($combine_arr[$test_count][$newtest_count]['media_type']);
									?>
											<div class="tableCont" id="AccordionContainer">
												<div class="blkD" style="width:60px;"><?php if($get_type_of_tagged_media['name']!=""){if($get_type_of_tagged_media['name'] =="Galleries/Images"){echo "Images";}else{echo $get_type_of_tagged_media['name'];}}else{?>&nbsp;<?php } ?></div>
												<div class="blkI" style="width:127px;"><?php
												$exp_creator_info = explode("|",$combine_arr[$test_count][$newtest_count]['creator_info']);
												$pro_url = $new_profile_media_classobj->get_creator_url($exp_creator_info[0],$exp_creator_info[1]);
												?>
												<a href="<?php if($pro_url['profile_url'] !=""){ echo "/".$pro_url['profile_url']; }else { echo "javascript:void(0)"; }?>">
												<?php if($combine_arr[$test_count][$newtest_count]['creator']!=""){
												//$endpos = strpos($combine_arr[$test_count][$newtest_count]['creator'],"(");
												//if($endpos !="" || $endpos !=false) {
												//echo substr($combine_arr[$test_count][$newtest_count]['creator'],0,$endpos);} else {
												echo $combine_arr[$test_count][$newtest_count]['creator']; }
												else{?>&nbsp;<?php } ?></a></div>
												<div class="blkK"><?php
												$exp_from_info = explode("|",$combine_arr[$test_count][$newtest_count]['from_info']);
												$pro_url_from = $new_profile_media_classobj->get_from_url($exp_from_info[0],$exp_from_info[1]);
												?>
												<a href="<?php if($pro_url_from['profile_url'] !=""){ echo "/".$pro_url_from['profile_url']; }else { echo "javascript:void(0)"; }?>">
												<?php if($combine_arr[$test_count][$newtest_count]['from']!=""){
												//$endpos = strpos($combine_arr[$test_count][$newtest_count]['from'],"(");
												//if($endpos !="" || $endpos != false) {
												//echo substr($combine_arr[$test_count][$newtest_count]['from'],0,$endpos);} else {
												echo $combine_arr[$test_count][$newtest_count]['from']; }
												else{?>&nbsp;<?php } ?></a></div>
												<div class="blkB"><?php
												if($combine_arr[$test_count][$newtest_count]['title']!="")
												{
													if($combine_arr[$test_count][$newtest_count]['media_type'] == 114)
													{
														//$get_media_track = $new_profile_media_classobj->get_media_track($combine_arr[$test_count][$newtest_count]['id']); 
														if($combine_arr[$test_count][$newtest_count]['track'] != "" && $combine_arr[$test_count][$newtest_count]['track'] != NULL && $combine_arr[$test_count][$newtest_count]['track'] !=" "){
															echo $combine_arr[$test_count][$newtest_count]['track'].".".stripslashes($combine_arr[$test_count][$newtest_count]['title']);
														}else{
															echo stripslashes($combine_arr[$test_count][$newtest_count]['title']);
														}
													}
													else{
													echo stripslashes($combine_arr[$test_count][$newtest_count]['title']); }
												}else{
												?>&nbsp;<?php
												}
													?></div>
												<div class="blkplayer"><div class="player">
												<?php 
													if($combine_arr[$test_count][$newtest_count]['media_type']==113){
														$get_images_tag = $new_profile_media_classobj ->get_all_gallery_images($combine_arr[$test_count][$newtest_count]['id']);
														for($count_img=0;$count_img<=count($get_images_tag);$count_img++){
															if($get_all_img_tag == ""){
																$get_all_img_tag = $get_images_tag[$count_img]['image_name'];
															}else{
																$get_all_img_tag = $get_all_img_tag.",".$get_images_tag[$count_img]['image_name'];
															}
															if($galleryImagestitle_tag==""){
																$galleryImagestitle_tag = $get_images_tag[$count_img]['image_title'];
															}else{
																$galleryImagestitle_tag = $galleryImagestitle_tag.",".$get_images_tag[$count_img]['image_title'];
															}
															$mediaSrc_tag = $get_images_tag[0]['image_name'];
														}
														$gal_id_tag = $get_images_tag[$count_img]['media_id'];
														$get_other_user_id = $user_keys_data->get_general_user_idby_mediaid($combine_arr[$test_count][$newtest_count]['id']);
														$orgPath_tag = $s3->getNewURI_otheruser("medgallery",$get_other_user_id);//"https://medgallery.s3.amazonaws.com/";
														$thumbPath_tag = $s3->getNewURI_otheruser("medgalthumb",$get_other_user_id);//"https://medgalthumb.s3.amazonaws.com/";
													?>
													<a href="javascript:void(0);"><img title="Play file now." src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" onclick="checkload('<?php echo $mediaSrc_tag;?>','<?php echo $get_all_img_tag; ?>','<?php echo $thumbPath_tag; ?>','<?php echo $orgPath_tag; ?>','<?php echo $galleryImagestitle_tag; ?>','<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>')"/></a>
													<!--<a href="javascript:void(0);"><img title="Add file to the bottom of your playlist without stopping current media." src="images/profile/add.gif"  onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'"/></a>-->
													<?php
														$get_all_img_tag ="";
														$galleryImagestitle_tag="";
														$mediaSrc_tag="";
													}else if($combine_arr[$test_count][$newtest_count]['media_type']==114){
													?>
													<a href="javascript:void(0);"><img title="Play file now." src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" onclick="showPlayer('a','<?php if($combine_arr[$test_count][$newtest_count]['id']!=""){echo $combine_arr[$test_count][$newtest_count]['id'];}else{echo $combine_arr[$test_count][$newtest_count]['id'];}?>','play');"/></a>
													<a href="javascript:void(0);"><img title="Add file to the bottom of your playlist without stopping current media." src="images/profile/add.gif"  onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'" onclick="showPlayer('a','<?php if($combine_arr[$test_count][$newtest_count]['id']!=""){echo $combine_arr[$test_count][$newtest_count]['id'];}else{echo $combine_arr[$test_count][$newtest_count]['id'];}?>','add');"/></a>
													<?php
													}else if($combine_arr[$test_count][$newtest_count]['media_type']==115){
													?>
													<a href="javascript:void(0);"><img title="Play file now." src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" onclick="showPlayer('v','<?php if($combine_arr[$test_count][$newtest_count]['id']!=""){echo $combine_arr[$test_count][$newtest_count]['id'];}else{ echo $combine_arr[$test_count][$newtest_count]['id'];}?>','playlist');"/></a>
													<a href="javascript:void(0);"><img title="Add file to the bottom of your playlist without stopping current media." src="images/profile/add.gif"  onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'" onclick="showPlayer('v','<?php if($combine_arr[$test_count][$newtest_count]['id']!=""){echo $combine_arr[$test_count][$newtest_count]['id'];}else{ echo $combine_arr[$test_count][$newtest_count]['id'];}?>','addvi');"/></a>
													<?php
													}else if($combine_arr[$test_count][$newtest_count]['media_type'] == 116){
														$get_channel_media = $new_profile_media_classobj ->get_all_media_selected_channel($combine_arr[$test_count][$newtest_count]['id']);
													?>
													<input type="hidden" id="channel_media_song<?php echo $newtest_count;?>" value="<?php echo $get_channel_media['songs']; ?>"/>
													<input type="hidden" id="channel_media_video<?php echo $newtest_count;?>" value="<?php echo $get_channel_media['videos']; ?>"/>
													<a href="javascript:void(0);"><img title="Play file now." src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" onclick="PlayAll('channel_media_video<?php echo $newtest_count;?>','channel_media_song<?php echo $newtest_count;?>')"/></a>
													<a href="javascript:void(0);"><img title="Add file to the bottom of your playlist without stopping current media." src="images/profile/add.gif"  onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'" onclick="showPlayer('v','<?php if($combine_arr[$test_count][$newtest_count]['id']!=""){echo $combine_arr[$test_count][$newtest_count]['id'];}else{ echo $combine_arr[$test_count][$newtest_count]['id'];}?>','addvi');"/></a>
													<?php
													}else{
													?>
													<a href="javascript:void(0);"><img title="Play file now." src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" /></a>
													<a href="javascript:void(0);"><img title="Add file to the bottom of your playlist without stopping current media." src="images/profile/add.gif"  onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'"/></a>
													<?php
													}
												if(($combine_arr[$test_count][$newtest_count]['sharing_preference']==5 || $combine_arr[$test_count][$newtest_count]['sharing_preference']==2) && $combine_arr[$test_count][$newtest_count]['media_type'] == 114){
													?>
													<a onclick="download_directly_song('<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['title']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['creator']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['from']);?>')" href="javascript:void(0);">
													<img title="Add media to your cart for free download or purchase." onmouseout="this.src='../images/profile/download_down.gif'" onmouseover="this.src='../images/profile/download-over_down.gif'" src="../images/profile/download_down.gif"></a>
												<?php
												}/* else{
												?>
												<img src="images/profile/download-over_down.gif" />
												<?php
												} */
												?>
												</div>
												</div>
												<div class="icon" onclick="runAccordion(<?php echo $combine_arr[$test_count][$newtest_count]['id']; ?>);">
													<div onselectstart="return false;"><img title="View details for media." src="images/profile/view.jpg" /></div>
												</div>
												<div class="icon">&nbsp;</div>
												<div class="icon"><a href="delete_media.php?id=<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>&type=<?php if($get_type_of_tagged_media['name'] =="Galleries/Images"){echo "Images";}else{echo $get_type_of_tagged_media['name'];}?>" onclick="return confirmdelete('<?php echo $combine_arr[$test_count][$newtest_count]['title'];?>','<?php if($combine_arr[$test_count][$newtest_count]['name']=="Galleries/Images"){echo "Images";}else{echo $combine_arr[$test_count][$newtest_count]['name'];} ?>')"><img title="Remove media from your library." src="images/profile/delete.png" /></a></div>
												<div class="expandable" id="Accordion<?php echo $combine_arr[$test_count][$newtest_count]['id']; ?>Content">
													<div class="affiBlock">
														<div class="RowCont">
															<div class="sideL">Plays :</div>
															<div class="sideR"><a href="" target="_blank"><?php if($combine_arr[$test_count][$newtest_count]['play_count']!=""){ echo $combine_arr[$test_count][$newtest_count]['play_count'];}else{ echo "0";}?></a></div>
														</div>
														<?php
															if($combine_arr[$test_count][$newtest_count]['sharing_preference']==5 || $combine_arr[$test_count][$newtest_count]['sharing_preference']==2){
															?>
															<div class="RowCont">
																<div class="sideL">Downloads :</div>
																<div class="sideR"><a href="javascript:void(0);"><?php echo $combine_arr[$test_count][$newtest_count]['download_count'];?></a></div>
															</div>
															<?php
															}

															if($combine_arr[$test_count][$newtest_count]['media_type']=='113')
															{
															
															}
															else
															{
														?>	
														<!--<div class="RowCont">
															<div class="sideL">Media URL :</div>
															<div class="sideR"><a href="" target="_blank">www.<?php echo $domainname; ?></a></div>
														</div>-->
														<?php
															}
														?>
													</div>
												</div>
											</div>
									<?php
											}
										}
									}
									else
									{
										$chk_tag_user = $new_profile_media_classobj->get_loggedin_info($_SESSION['login_email']);
										if($chk_tag_user['general_user_id']!=$combine_arr[$test_count][$newtest_count]['general_user_id'])
										{
											$get_type_of_tagged_media=$new_profile_media_classobj->sel_type_of_tagged_media($combine_arr[$test_count][$newtest_count]['media_type']);
									?>
											<div class="tableCont" id="AccordionContainer">
												<div class="blkD" style="width:60px;"><?php if($get_type_of_tagged_media['name']!=""){if($get_type_of_tagged_media['name'] =="Galleries/Images"){echo "Images";}else{echo $get_type_of_tagged_media['name'];}}else{?>&nbsp;<?php } ?></div>
												<div class="blkI" style="width:127px;"><?php
												$exp_creator_info = explode("|",$combine_arr[$test_count][$newtest_count]['creator_info']);
												$pro_url = $new_profile_media_classobj->get_creator_url($exp_creator_info[0],$exp_creator_info[1]);
												?>
												<a href="<?php if($pro_url['profile_url'] !=""){ echo "/".$pro_url['profile_url']; }else { echo "javascript:void(0)"; }?>">
												<?php if($combine_arr[$test_count][$newtest_count]['creator']!=""){
												//$endpos = strpos($combine_arr[$test_count][$newtest_count]['creator'],"(");
												//if($endpos !="" || $endpos !=false) {
												//echo substr($combine_arr[$test_count][$newtest_count]['creator'],0,$endpos);} else {
												echo $combine_arr[$test_count][$newtest_count]['creator']; }
												else{?>&nbsp;<?php } ?></a></div>
												<div class="blkK"><?php
												$exp_from_info = explode("|",$combine_arr[$test_count][$newtest_count]['from_info']);
												$pro_url_from = $new_profile_media_classobj->get_from_url($exp_from_info[0],$exp_from_info[1]);
												?>
												<a href="<?php if($pro_url_from['profile_url'] !=""){ echo "/".$pro_url_from['profile_url']; }else { echo "javascript:void(0)"; }?>">
												<?php if($combine_arr[$test_count][$newtest_count]['from']!=""){
												//$endpos = strpos($combine_arr[$test_count][$newtest_count]['from'],"(");
												//if($endpos !="" || $endpos != false) {
												//echo substr($combine_arr[$test_count][$newtest_count]['from'],0,$endpos);} else {
												echo $combine_arr[$test_count][$newtest_count]['from']; }
												else{?>&nbsp;<?php } ?></a></div>
												<div class="blkB"><?php
												if($combine_arr[$test_count][$newtest_count]['title']!="")
												{
													if($combine_arr[$test_count][$newtest_count]['media_type'] == 114)
													{
														//$get_media_track = $new_profile_media_classobj->get_media_track($combine_arr[$test_count][$newtest_count]['id']); 
														if($combine_arr[$test_count][$newtest_count]['track'] != "" && $combine_arr[$test_count][$newtest_count]['track'] != NULL && $combine_arr[$test_count][$newtest_count]['track'] !=" "){
															echo $combine_arr[$test_count][$newtest_count]['track'].".".stripslashes($combine_arr[$test_count][$newtest_count]['title']);
														}else{
															echo stripslashes($combine_arr[$test_count][$newtest_count]['title']);
														}
													}
													else{
													echo stripslashes($combine_arr[$test_count][$newtest_count]['title']); }
												}else{
												?>&nbsp;<?php
												}
													?></div>
												<div class="blkplayer"><div class="player">
												<?php 
													if($combine_arr[$test_count][$newtest_count]['media_type']==113){
														$get_images_tag = $new_profile_media_classobj ->get_all_gallery_images($combine_arr[$test_count][$newtest_count]['id']);
														for($count_img=0;$count_img<=count($get_images_tag);$count_img++){
															if($get_all_img_tag ==""){
																$get_all_img_tag = $get_images_tag[$count_img]['image_name'];
															}else{
																$get_all_img_tag = $get_all_img_tag.",".$get_images_tag[$count_img]['image_name'];
															}
															if($galleryImagestitle_tag ==""){
																$galleryImagestitle_tag = $get_images_tag[$count_img]['image_title'];
															}else{
																$galleryImagestitle_tag = $galleryImagestitle_tag.",".$get_images_tag[$count_img]['image_title'];
															}
															$mediaSrc_tag = $get_images_tag[0]['image_name'];
														}
														$gal_id_tag = $get_images_tag[$count_img]['media_id'];
														$get_other_user_id = $user_keys_data->get_general_user_idby_mediaid($combine_arr[$test_count][$newtest_count]['id']);
														$orgPath_tag = $s3->getNewURI_otheruser("medgallery",$get_other_user_id);//"https://medgallery.s3.amazonaws.com/";
														$thumbPath_tag = $s3->getNewURI_otheruser("medgalthumb",$get_other_user_id);//"https://medgalthumb.s3.amazonaws.com/";
														
													?>
													<a href="javascript:void(0);"><img title="Play file now." src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" onclick="checkload('<?php echo $mediaSrc_tag;?>','<?php echo $get_all_img_tag; ?>','<?php echo $thumbPath_tag; ?>','<?php echo $orgPath_tag; ?>','<?php echo $galleryImagestitle_tag; ?>','<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>')"/></a>
													<!--<a href="javascript:void(0);"><img title="Add file to the bottom of your playlist without stopping current media." src="images/profile/add.gif"  onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'"/></a>-->
													<?php
														$get_all_img_tag ="";
														$galleryImagestitle_tag="";
														$mediaSrc_tag="";
													}else if($combine_arr[$test_count][$newtest_count]['media_type']==114){
													?>
													<a href="javascript:void(0);"><img title="Play file now." src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" onclick="showPlayer('a','<?php if($combine_arr[$test_count][$newtest_count]['id']!=""){echo $combine_arr[$test_count][$newtest_count]['id'];}else{echo $combine_arr[$test_count][$newtest_count]['id'];}?>','play');"/></a>
													<a href="javascript:void(0);"><img title="Add file to the bottom of your playlist without stopping current media." src="images/profile/add.gif"  onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'" onclick="showPlayer('a','<?php if($combine_arr[$test_count][$newtest_count]['id']!=""){echo $combine_arr[$test_count][$newtest_count]['id'];}else{echo $combine_arr[$test_count][$newtest_count]['id'];}?>','add');"/></a>
													<?php
													}else if($combine_arr[$test_count][$newtest_count]['media_type']==115){
													?>
													<a href="javascript:void(0);"><img title="Play file now." src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" onclick="showPlayer('v','<?php if($combine_arr[$test_count][$newtest_count]['id']!=""){echo $combine_arr[$test_count][$newtest_count]['id'];}else{ echo $combine_arr[$test_count][$newtest_count]['id'];}?>','playlist');"/></a>
													<a href="javascript:void(0);"><img title="Add file to the bottom of your playlist without stopping current media." src="images/profile/add.gif"  onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'" onclick="showPlayer('v','<?php if($combine_arr[$test_count][$newtest_count]['id']!=""){echo $combine_arr[$test_count][$newtest_count]['id'];}else{ echo $combine_arr[$test_count][$newtest_count]['id'];}?>','addvi');"/></a>
													<?php
													}else if($combine_arr[$test_count][$newtest_count]['media_type'] == 116){
														$get_channel_media = $new_profile_media_classobj ->get_all_media_selected_channel($combine_arr[$test_count][$newtest_count]['id']);
													?>
													<input type="hidden" id="channel_media_song<?php echo $newtest_count;?>" value="<?php echo $get_channel_media['songs']; ?>"/>
													<input type="hidden" id="channel_media_video<?php echo $newtest_count;?>" value="<?php echo $get_channel_media['videos']; ?>"/>
													<a href="javascript:void(0);"><img title="Play file now." src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" onclick="PlayAll('channel_media_video<?php echo $newtest_count;?>','channel_media_song<?php echo $newtest_count;?>')"/></a>
													<a href="javascript:void(0);"><img title="Add file to the bottom of your playlist without stopping current media." src="images/profile/add.gif"  onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'" onclick="showPlayer('v','<?php if($combine_arr[$test_count][$newtest_count]['id']!=""){echo $combine_arr[$test_count][$newtest_count]['id'];}else{ echo $combine_arr[$test_count][$newtest_count]['id'];}?>','addvi');"/></a>
													<?php
													}
													else{
													?>
													<a href="javascript:void(0);"><img title="Play file now." src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" /></a>
													<a href="javascript:void(0);"><img title="Add file to the bottom of your playlist without stopping current media." src="images/profile/add.gif"  onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'"/></a>
													<?php
													}
													
												if(($combine_arr[$test_count][$newtest_count]['sharing_preference']==5 || $combine_arr[$test_count][$newtest_count]['sharing_preference']==2) && $combine_arr[$test_count][$newtest_count]['media_type'] == 114){
													?>
													<a onclick="download_directly_song('<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['title']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['creator']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['from']);?>')" href="javascript:void(0);">
													<img title="Add media to your cart for free download or purchase." onmouseout="this.src='../images/profile/download_down.gif'" onmouseover="this.src='../images/profile/download-over_down.gif'" src="../images/profile/download_down.gif"></a>
												<?php
												}/* else{
												?>
												<img src="images/profile/download-over_down.gif" />
												<?php
												} */
												?>
												</div>
												</div>
												<div class="icon" onclick="runAccordion(<?php echo $combine_arr[$test_count][$newtest_count]['id']; ?>);">
													<div onselectstart="return false;"><img title="View details for media." src="images/profile/view.jpg" /></div>
												</div>
												<div class="icon">&nbsp;</div>
												<div class="icon"><a href="delete_media.php?id=<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>&type=<?php if($get_type_of_tagged_media['name'] =="Galleries/Images"){echo "Images";}else{echo $get_type_of_tagged_media['name'];}?>" onclick="return confirmdelete('<?php echo $combine_arr[$test_count][$newtest_count]['title'];?>','<?php if($combine_arr[$test_count][$newtest_count]['name']=="Galleries/Images"){echo "Images";}else{echo $combine_arr[$test_count][$newtest_count]['name'];} ?>')"><img title="Remove media from your library." src="images/profile/delete.png" /></a></div>
												<div class="expandable" id="Accordion<?php echo $combine_arr[$test_count][$newtest_count]['id']; ?>Content">
													<div class="affiBlock">
														<div class="RowCont">
															<div class="sideL">Plays :</div>
															<div class="sideR"><a href="" target="_blank"><?php if($combine_arr[$test_count][$newtest_count]['play_count']!=""){ echo $combine_arr[$test_count][$newtest_count]['play_count'];}else{ echo "0";}?></a></div>
														</div>
														<?php
															if($combine_arr[$test_count][$newtest_count]['sharing_preference']==5 || $combine_arr[$test_count][$newtest_count]['sharing_preference']==2){
															?>
															<div class="RowCont">
																<div class="sideL">Downloads :</div>
																<div class="sideR"><a href="javascript:void(0);"><?php echo $combine_arr[$test_count][$newtest_count]['download_count'];?></a></div>
															</div>
															<?php
															}

															if($combine_arr[$test_count][$newtest_count]['media_type']=='113')
															{
															
															}
															else
															{
														?>	
														<!--<div class="RowCont">
															<div class="sideL">Media URL :</div>
															<div class="sideR"><a href="" target="_blank">www.<?php echo $domainname; ?></a></div>
														</div>-->
														<?php
															}
														?>
													</div>
												</div>
											</div>
									<?php
										}
									}
								}
								else if($combine_arr[$test_count][$newtest_count]['media_from']=="newedit_media")
								{
									$chk_tag_user = $new_profile_media_classobj->get_loggedin_info($_SESSION['login_email']);
									if($chk_tag_user['general_user_id']!=$combine_arr[$test_count][$newtest_count]['general_user_id'])
									{
									?>
										<div class="tableCont" id="AccordionContainer">
										<div class="blkD" style="width:60px;"><?php if($combine_arr[$test_count][$newtest_count]['name']!=""){ if($combine_arr[$test_count][$newtest_count]['name']=="Galleries/Images"){ echo "Images";}else {echo $combine_arr[$test_count][$newtest_count]['name'];}}else{?>&nbsp;<?php } ?></div>
										<div class="blkI" style="width:127px;"><?php
										$exp_creator_info = explode("|",$combine_arr[$test_count][$newtest_count]['creator_info']);
										$pro_url = $new_profile_media_classobj->get_creator_url($exp_creator_info[0],$exp_creator_info[1]);
										?>
										<a href="<?php if($pro_url['profile_url'] !=""){ echo "/".$pro_url['profile_url']; }else { echo "javascript:void(0)"; }?>">
										<?php if($combine_arr[$test_count][$newtest_count]['creator']!=""){
										//$endpos = strpos($combine_arr[$test_count][$newtest_count]['creator'],"(");
										//if($endpos !="" || $endpos !=false) {
										//echo substr($combine_arr[$test_count][$newtest_count]['creator'],0,$endpos);} else {
										echo $combine_arr[$test_count][$newtest_count]['creator']; 
										}else{?>&nbsp;<?php } ?></a></div>
										<div class="blkK"><?php
										$exp_from_info = explode("|",$combine_arr[$test_count][$newtest_count]['from_info']);
										$pro_url_from = $new_profile_media_classobj->get_from_url($exp_from_info[0],$exp_from_info[1]);
										?>
										<a href="<?php if($pro_url_from['profile_url'] !=""){ echo "/".$pro_url_from['profile_url']; }else { echo "javascript:void(0)"; }?>">
										<?php if($combine_arr[$test_count][$newtest_count]['from']!=""){
										//$endpos = strpos($combine_arr[$test_count][$newtest_count]['from'],"(");
										//if($endpos !="" || $endpos != false) {
										//echo substr($combine_arr[$test_count][$newtest_count]['from'],0,$endpos);} else {
										echo $combine_arr[$test_count][$newtest_count]['from']; }
										else{?>&nbsp;<?php } ?></a></div>
										<!--<div class="blkK"><a href="gallery_images.php?gal_id=<?php //echo $res['id'];?>" id="gallery_images<?php //echo $k;?>">Edit Image</a></div>
										<div class="blkB"><a href="add_gallery_images.php?gal_id=<?php echo $res['id'];?>" id="Add_gallery_images<?php echo $k;?>">Add Image</a></div>-->
										<!--<div class="blkJ"><a href=""><img src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" /></a><a href=""><img src="images/profile/add.gif"  onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'"/></a><a href=""><img src="images/profile/download_down.gif" onmouseover="this.src='images/profile/download-over_down.gif'" onmouseout="this.src='images/profile/download_down.gif'" /></a></div>-->
										<div class="icon" onclick="runAccordion(<?php echo $combine_arr[$test_count][$newtest_count]['id']; ?>);">
											<div onselectstart="return false;"><img title="View details for media." src="images/profile/view.jpg" /></div>
										</div>
										<div class="icon"><a href="add_media.php?edit=<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>" onclick="save_cookie()"><img title="Edit media." src="images/profile/edit.png" /></a></div>
										<div class="icon"><a onclick="return confirmdelete('<?php echo $combine_arr[$test_count][$newtest_count]['title'];?>','<?php if($combine_arr[$test_count][$newtest_count]['name']=="Galleries/Images"){echo "Images";}else{echo $combine_arr[$test_count][$newtest_count]['name'];} ?>')" href="delete_media.php?id=<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>&whos_media=newedit_media"><img title="Remove media from your library." src="images/profile/delete.png" /></a></div>
										<div class="expandable" id="Accordion<?php echo $combine_arr[$test_count][$newtest_count]['id']; ?>Content">
											<div class="affiBlock">
												<div class="RowCont">
													<div class="sideL">Plays :</div>
													<div class="sideR"><a href="" target="_blank"><?php if($combine_arr[$test_count][$newtest_count]['play_count']!=""){ echo $combine_arr[$test_count][$newtest_count]['play_count'];}else{ echo "0";}?></a></div>
												</div>
												<?php
													if($combine_arr[$test_count][$newtest_count]['sharing_preference']==5 || $combine_arr[$test_count][$newtest_count]['sharing_preference']==2){
													?>
													<div class="RowCont">
														<div class="sideL">Downloads :</div>
														<div class="sideR"><a href="javascript:void(0);"><?php echo $combine_arr[$test_count][$newtest_count]['download_count'];?></a></div>
													</div>
													<?php
													}

													if($combine_arr[$test_count][$newtest_count]['media_type']=='113')
													{
													
													}
													else
													{
												?>	
												<!--<div class="RowCont">
													<div class="sideL">Media URL :</div>
													<div class="sideR"><a href="" target="_blank">www.<?php echo $domainname; ?></a></div>
												</div>-->
												<?php
													}
												?>
											</div>
										</div>
									</div>
									<?php
										$k=$k+1;
									}
								}
								else if($combine_arr[$test_count][$newtest_count]['media_from']=="media_creator")
								{
									?>
										<div class="tableCont" id="AccordionContainer">
										<div class="blkD" style="width:60px;"><?php if($combine_arr[$test_count][$newtest_count]['name']=="Galleries/Images"){echo "Images";}else{echo $combine_arr[$test_count][$newtest_count]['name'];} ?></div>
										<div class="blkI" style="width:127px;"><?php
										$exp_creator_info = explode("|",$combine_arr[$test_count][$newtest_count]['creator_info']);
										$pro_url = $new_profile_media_classobj->get_creator_url($exp_creator_info[0],$exp_creator_info[1]);
										?>
										<a href="<?php if($pro_url['profile_url'] !=""){ echo "/".$pro_url['profile_url']; }else { echo "javascript:void(0)"; }?>">
										<?php if($combine_arr[$test_count][$newtest_count]['creator']!=""){
										//$endpos = strpos($combine_arr[$test_count][$newtest_count]['creator'],"(");
										//if($endpos !="" || $endpos !=false) {
										//echo substr($combine_arr[$test_count][$newtest_count]['creator'],0,$endpos);} else {
										echo $combine_arr[$test_count][$newtest_count]['creator']; }
										else{?>&nbsp;<?php } ?></a></div>
										<div class="blkK"><?php
										$exp_from_info = explode("|",$combine_arr[$test_count][$newtest_count]['from_info']);
										$pro_url_from = $new_profile_media_classobj->get_from_url($exp_from_info[0],$exp_from_info[1]);
										?>
										<a href="<?php if($pro_url_from['profile_url'] !=""){ echo "/".$pro_url_from['profile_url']; }else { echo "javascript:void(0)"; }?>">
										<?php if($combine_arr[$test_count][$newtest_count]['from']!=""){
										//$endpos = strpos($combine_arr[$test_count][$newtest_count]['from'],"(");
										//if($endpos !="" || $endpos != false) {
										//echo substr($combine_arr[$test_count][$newtest_count]['from'],0,$endpos);} else {
										echo $combine_arr[$test_count][$newtest_count]['from']; }
										else{?>&nbsp;<?php } ?></a></div>
										<div class="blkB"><?php
												if($combine_arr[$test_count][$newtest_count]['title']!="")
												{
													if($combine_arr[$test_count][$newtest_count]['media_type'] == 114)
													{
														//$get_media_track = $new_profile_media_classobj->get_media_track($combine_arr[$test_count][$newtest_count]['id']); 
														if($combine_arr[$test_count][$newtest_count]['track'] != "" && $combine_arr[$test_count][$newtest_count]['track'] != NULL && $combine_arr[$test_count][$newtest_count]['track'] !=" "){
															echo $combine_arr[$test_count][$newtest_count]['track'].".".stripslashes($combine_arr[$test_count][$newtest_count]['title']);
														}else{
															echo stripslashes($combine_arr[$test_count][$newtest_count]['title']);
														}
													}
													else{
													echo stripslashes($combine_arr[$test_count][$newtest_count]['title']); }
												}else{
												?>&nbsp;<?php
												}
													?></div>
										<?php
										if($combine_arr[$test_count][$newtest_count]['media_type']==114)
										{
										?>
											<div class="blkplayer"><div class="player">
												<a href="javascript:void(0);"><img onclick="showPlayer('a','<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','play');" onmouseout="this.src='images/profile/play.gif'" onmouseover="this.src='images/profile/play-over.gif'" src="images/profile/play.gif" title="Play file now."></a>
												<a href="javascript:void(0);"><img onclick="showPlayer('a','<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','add');" onmouseout="this.src='images/profile/add.gif'" onmouseover="this.src='images/profile/add-over.gif'" src="images/profile/add.gif" title="Add file to the bottom of your playlist without stopping current media."></a>
												<?php
												if(($combine_arr[$test_count][$newtest_count]['sharing_preference']==5 || $combine_arr[$test_count][$newtest_count]['sharing_preference']==2) && $combine_arr[$test_count][$newtest_count]['media_type'] == 114){
													?>
													<a onclick="download_directly_song('<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['title']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['creator']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['from']);?>')" href="javascript:void(0);">
													<img title="Add media to your cart for free download or purchase." onmouseout="this.src='../images/profile/download_down.gif'" onmouseover="this.src='../images/profile/download-over_down.gif'" src="../images/profile/download_down.gif"></a>
												<?php
												}/* else{
												?>
												<img src="images/profile/download-over_down.gif" />
												<?php
												} */
												?>
											</div>
											</div>
										<?php
										}else if($combine_arr[$test_count][$newtest_count]['media_type']==115)
										{
										?>
											<div class="blkplayer"><div class="player">
												<a href="javascript:void(0);"><img onclick="showPlayer('v','<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','playlist');" onmouseout="this.src='images/profile/play.gif'" onmouseover="this.src='images/profile/play-over.gif'" src="images/profile/play.gif" title="Play file now."></a>
												<a href="javascript:void(0);"><img onclick="showPlayer('v','<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','addvi');" onmouseout="this.src='images/profile/add.gif'" onmouseover="this.src='images/profile/add-over.gif'" src="images/profile/add.gif" title="Add file to the bottom of your playlist without stopping current media."></a>
												<?php
												if(($combine_arr[$test_count][$newtest_count]['sharing_preference']==5 || $combine_arr[$test_count][$newtest_count]['sharing_preference']==2) && $combine_arr[$test_count][$newtest_count]['media_type'] == 114){
													?>
													<a onclick="download_directly_song('<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['title']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['creator']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['from']);?>')" href="javascript:void(0);">
													<img title="Add media to your cart for free download or purchase." onmouseout="this.src='../images/profile/download_down.gif'" onmouseover="this.src='../images/profile/download-over_down.gif'" src="../images/profile/download_down.gif"></a>
												<?php
												}/* else{
												?>
												<img src="images/profile/download-over_down.gif" />
												<?php
												} */
												?>
											</div>
											</div>
										<?php
										}else if($combine_arr[$test_count][$newtest_count]['media_type']==113){
											$get_images_fan = $new_profile_media_classobj ->get_all_gallery_images($combine_arr[$test_count][$newtest_count]['id']);
											for($count_img=0;$count_img<=count($get_images_fan);$count_img++){
												if($get_all_img_fan ==""){
													$get_all_img_fan = $get_images_fan[$count_img]['image_name'];
												}else{
													$get_all_img_fan = $get_all_img_fan.",".$get_images_fan[$count_img]['image_name'];
												}
												if($galleryImagestitle_fan==""){
													$galleryImagestitle_fan = $get_images_fan[$count_img]['image_title'];
												}else{
													$galleryImagestitle_fan = $galleryImagestitle_fan.",".$get_images_fan[$count_img]['image_title'];
												}
												$mediaSrc_fan = $get_images_fan[0]['image_name'];
											}
											$gal_id_fan = $get_images_fan[$count_img]['media_id'];
											$get_other_user_id = $user_keys_data->get_general_user_idby_mediaid($combine_arr[$test_count][$newtest_count]['id']);
											$orgPath_fan = $s3->getNewURI_otheruser("medgallery",$get_other_user_id);//"https://medgallery.s3.amazonaws.com/";
											$thumbPath_fan = $s3->getNewURI_otheruser("medgalthumb",$get_other_user_id);//"https://medgalthumb.s3.amazonaws.com/";
											
										?>
											<div class="blkplayer"><div class="player">
											<a href="javascript:void(0);"><img title="Play file now." src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" onclick="checkload('<?php echo $mediaSrc_fan;?>','<?php echo $get_all_img_fan; ?>','<?php echo $thumbPath_fan; ?>','<?php echo $orgPath_fan; ?>','<?php echo $galleryImagestitle_fan; ?>','<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>')"/></a>
											<!--<a href="javascript:void(0);"><img title="Add file to the bottom of your playlist without stopping current media." src="images/profile/add.gif"  onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'"/></a>-->
											<?php
												if(($combine_arr[$test_count][$newtest_count]['sharing_preference']==5 || $combine_arr[$test_count][$newtest_count]['sharing_preference']==2) && $combine_arr[$test_count][$newtest_count]['media_type'] == 114){
													?>
													<a onclick="download_directly_song('<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['title']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['creator']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['from']);?>')" href="javascript:void(0);">
													<img title="Add media to your cart for free download or purchase." onmouseout="this.src='../images/profile/download_down.gif'" onmouseover="this.src='../images/profile/download-over_down.gif'" src="../images/profile/download_down.gif"></a>
												<?php
												}/* else{
												?>
												<img src="images/profile/download-over_down.gif" />
												<?php
												} */
												?>
											</div>
											</div>
										<?php
											$get_all_img_fan ="";
											$galleryImagestitle_fan="";
											$mediaSrc_fan ="";
										}else if($combine_arr[$test_count][$newtest_count]['media_type'] == 116){
											$get_channel_media = $new_profile_media_classobj ->get_all_media_selected_channel($combine_arr[$test_count][$newtest_count]['id']);
										?>
										<div class="blkplayer"><div class="player">
											<input type="hidden" id="channel_media_song<?php echo $newtest_count;?>" value="<?php echo $get_channel_media['songs']; ?>"/>
											<input type="hidden" id="channel_media_video<?php echo $newtest_count;?>" value="<?php echo $get_channel_media['videos']; ?>"/>
											<a href="javascript:void(0);"><img title="Play file now." src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" onclick="PlayAll('channel_media_video<?php echo $newtest_count;?>','channel_media_song<?php echo $newtest_count;?>')"/></a>
											<a href="javascript:void(0);"><img title="Add file to the bottom of your playlist without stopping current media." src="images/profile/add.gif"  onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'" onclick="showPlayer('v','<?php if($combine_arr[$test_count][$newtest_count]['id']!=""){echo $combine_arr[$test_count][$newtest_count]['id'];}else{ echo $combine_arr[$test_count][$newtest_count]['id'];}?>','addvi');"/></a>
											<?php
												if(($combine_arr[$test_count][$newtest_count]['sharing_preference']==5 || $combine_arr[$test_count][$newtest_count]['sharing_preference']==2) && $combine_arr[$test_count][$newtest_count]['media_type'] == 114){
													?>
													<a onclick="download_directly_song('<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['title']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['creator']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['from']);?>')" href="javascript:void(0);">
													<img title="Add media to your cart for free download or purchase." onmouseout="this.src='../images/profile/download_down.gif'" onmouseover="this.src='../images/profile/download-over_down.gif'" src="../images/profile/download_down.gif"></a>
												<?php
												}/* else{
												?>
												<img src="images/profile/download-over_down.gif" />
												<?php
												} */
												?>
										</div>
										</div>
										<?php
										}
										else{
										?>
											<div class="blkplayer"><div class="player">
											<a href="javascript:void(0);"><img title="Play file now." src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" /></a>
											<a href="javascript:void(0);"><img title="Add file to the bottom of your playlist without stopping current media." src="images/profile/add.gif"  onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'"/></a>
											<?php
												if(($combine_arr[$test_count][$newtest_count]['sharing_preference']==5 || $combine_arr[$test_count][$newtest_count]['sharing_preference']==2) && $combine_arr[$test_count][$newtest_count]['media_type'] == 114){
													?>
													<a onclick="download_directly_song('<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['title']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['creator']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['from']);?>')" href="javascript:void(0);">
													<img title="Add media to your cart for free download or purchase." onmouseout="this.src='../images/profile/download_down.gif'" onmouseover="this.src='../images/profile/download-over_down.gif'" src="../images/profile/download_down.gif"></a>
												<?php
												}/* else{
												?>
												<img src="images/profile/download-over_down.gif" />
												<?php
												} */
												?>
											</div>
											</div>
										<?php
										}
										?>
										<div class="icon" onclick="runAccordion(<?php echo $combine_arr[$test_count][$newtest_count]['id']; ?>);">
											<div onselectstart="return false;"><img title="View details for media." src="images/profile/view.jpg" /></div>
										</div>
										<div class="icon"><!--<a href="add_media.php?edit=<?php// echo $combine_arr[$test_count][$newtest_count]['id'];?>"><img title="Edit media." src="images/profile/edit.png" /></a>--></div>
										<div class="icon"><a onclick="return confirmdelete('<?php echo $combine_arr[$test_count][$newtest_count]['title'];?>','<?php if($combine_arr[$test_count][$newtest_count]['name']=="Galleries/Images"){echo "Images";}else{echo $combine_arr[$test_count][$newtest_count]['name'];} ?>')" href="delete_media.php?id=<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>&whos_media=media_creator"><img title="Remove media from your library." src="images/profile/delete.png" /></a></div>
										<div class="expandable" id="Accordion<?php echo $combine_arr[$test_count][$newtest_count]['id']; ?>Content">
											<div class="affiBlock">
												<div class="RowCont">
													<div class="sideL">Plays :</div>
													<div class="sideR"><a href="" target="_blank"><?php if($combine_arr[$test_count][$newtest_count]['play_count']!=""){ echo $combine_arr[$test_count][$newtest_count]['play_count'];}else{ echo "0";}?></a></div>
												</div>
												<?php
													if($combine_arr[$test_count][$newtest_count]['sharing_preference']==5 || $combine_arr[$test_count][$newtest_count]['sharing_preference']==2){
													?>
													<div class="RowCont">
														<div class="sideL">Downloads :</div>
														<div class="sideR"><a href="javascript:void(0);"><?php echo $combine_arr[$test_count][$newtest_count]['download_count'];?></a></div>
													</div>
													<?php
													}

													if($combine_arr[$test_count][$newtest_count]['media_type']=='113')
													{
													
													}
													else
													{
												?>	
												<!--<div class="RowCont">
													<div class="sideL">Media URL :</div>
													<div class="sideR"><a href="" target="_blank">www.<?php echo $domainname; ?></a></div>
												</div>-->
												<?php
													}
												?>
											</div>
										</div>
									</div>
									<?php
								}
								else if($combine_arr[$test_count][$newtest_count]['media_from']=="email_subscription")
								{
									?>
										<div class="tableCont" id="AccordionContainer">
										<div class="blkD" style="width:60px;"><?php if($combine_arr[$test_count][$newtest_count]['name']=="Galleries/Images"){echo "Images";}else{echo $combine_arr[$test_count][$newtest_count]['name'];} ?></div>
										<div class="blkI" style="width:127px;"><?php
										$exp_creator_info = explode("|",$combine_arr[$test_count][$newtest_count]['creator_info']);
										$pro_url = $new_profile_media_classobj->get_creator_url($exp_creator_info[0],$exp_creator_info[1]);
										?>
										<a href="<?php if($pro_url['profile_url'] !=""){ echo "/".$pro_url['profile_url']; }else { echo "javascript:void(0)"; }?>">
										<?php if($combine_arr[$test_count][$newtest_count]['creator']!=""){
										//$endpos = strpos($combine_arr[$test_count][$newtest_count]['creator'],"(");
										//if($endpos !="" || $endpos !=false) {
										//echo substr($combine_arr[$test_count][$newtest_count]['creator'],0,$endpos);} else {
										echo $combine_arr[$test_count][$newtest_count]['creator']; }
										else{?>&nbsp;<?php } ?></a></div>
										<div class="blkK"><?php
										$exp_from_info = explode("|",$combine_arr[$test_count][$newtest_count]['from_info']);
										$pro_url_from = $new_profile_media_classobj->get_from_url($exp_from_info[0],$exp_from_info[1]);
										?>
										<a href="<?php if($pro_url_from['profile_url'] !=""){ echo "/".$pro_url_from['profile_url']; }else { echo "javascript:void(0)"; }?>">
										<?php if($combine_arr[$test_count][$newtest_count]['from']!=""){
										//$endpos = strpos($combine_arr[$test_count][$newtest_count]['from'],"(");
										//if($endpos !="" || $endpos != false) {
										//echo substr($combine_arr[$test_count][$newtest_count]['from'],0,$endpos);} else {
										echo $combine_arr[$test_count][$newtest_count]['from']; }
										else{?>&nbsp;<?php } ?></a></div>
										<div class="blkB"><?php
												if($combine_arr[$test_count][$newtest_count]['title']!="")
												{
													if($combine_arr[$test_count][$newtest_count]['media_type'] == 114)
													{
														//$get_media_track = $new_profile_media_classobj->get_media_track($combine_arr[$test_count][$newtest_count]['id']); 
														if($combine_arr[$test_count][$newtest_count]['track'] != "" && $combine_arr[$test_count][$newtest_count]['track'] != NULL && $combine_arr[$test_count][$newtest_count]['track'] !=" "){
															echo $combine_arr[$test_count][$newtest_count]['track'].".".stripslashes($combine_arr[$test_count][$newtest_count]['title']);
														}else{
															echo stripslashes($combine_arr[$test_count][$newtest_count]['title']);
														}
													}
													else{
													echo stripslashes($combine_arr[$test_count][$newtest_count]['title']); }
												}else{
												?>&nbsp;<?php
												}
													?></div>
										<?php
										if($combine_arr[$test_count][$newtest_count]['media_type']==114)
										{
										?>
											<div class="blkplayer"><div class="player">
												<a href="javascript:void(0);"><img onclick="showPlayer('a','<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','play');" onmouseout="this.src='images/profile/play.gif'" onmouseover="this.src='images/profile/play-over.gif'" src="images/profile/play.gif" title="Play file now."></a>
												<a href="javascript:void(0);"><img onclick="showPlayer('a','<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','add');" onmouseout="this.src='images/profile/add.gif'" onmouseover="this.src='images/profile/add-over.gif'" src="images/profile/add.gif" title="Add file to the bottom of your playlist without stopping current media."></a>
												<?php
												if(($combine_arr[$test_count][$newtest_count]['sharing_preference']==5 || $combine_arr[$test_count][$newtest_count]['sharing_preference']==2) && $combine_arr[$test_count][$newtest_count]['media_type'] == 114){
													$song_image_name = $new_profile_media_classobj->get_audio_img($combine_arr[$test_count][$newtest_count]['id']);
													if($combine_arr[$test_count][$newtest_count]['sharing_preference']==5)
													{
													?>
													<a href="javascript:void(0);" onclick="add_tocart(<?php echo $newtest_count;?>)"><img src="../images/profile/download_down.gif" onmouseover="this.src='../images/profile/download-over_down.gif'" onmouseout="this.src='../images/profile/download_down.gif'" title="Add media to your cart for free download or purchase." /></a>
													<?php
													}
													else if($combine_arr[$test_count][$newtest_count]['sharing_preference']==2)
													{
														$tip_add_aftr_detils = $display->get_tip_details_for_every($combine_arr[$test_count][$newtest_count]['id'],'media');
													?>
													<a href="javascript:void(0);" onclick="ask_to_donate('<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['title']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['creator']);?>','<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','add_to_cart_media<?php echo $newtest_count;?>','<?php echo $combine_arr[$test_count][$newtest_count]['general_user_id']; ?>','<?php echo $tip_add_aftr_detils; ?>')"><img src="../images/profile/download_down.gif" onmouseover="this.src='../images/profile/download-over_down.gif'" onmouseout="this.src='../images/profile/download_down.gif'" title="Add media to your cart for free download or purchase." /></a>
													<?php
													}
												}
												/* else{
												?>
												<img src="images/profile/download-over_down.gif" />
												<?php
												} */
												?>
												<a href="addtocart.php?seller_id=<?php echo $combine_arr[$test_count][$newtest_count]['general_user_id'];?>&media_id=<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>&buyer_id=<?php echo $_SESSION['gen_user_id'];?>&title=<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['title']);?>&image_name=<?php echo $song_image_name;?>" id="add_to_cart_media<?php echo $newtest_count;?>" style="display:none;"></a>
												<script type="text/javascript">
												$(document).ready(function() {
													$("#add_to_cart_media<?php echo $newtest_count;?>").fancybox({
													'height'			: '75%',
													'width'				: '69%',
													'transitionIn'		: 'none',
													'transitionOut'		: 'none',
													'type'				: 'iframe'
													});
												});
												</script>
											</div>
											</div>
										<?php
										}else if($combine_arr[$test_count][$newtest_count]['media_type']==115)
										{
										?>
											<div class="blkplayer"><div class="player">
												<a href="javascript:void(0);"><img onclick="showPlayer('v','<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','playlist');" onmouseout="this.src='images/profile/play.gif'" onmouseover="this.src='images/profile/play-over.gif'" src="images/profile/play.gif" title="Play file now."></a>
												<a href="javascript:void(0);"><img onclick="showPlayer('v','<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','addvi');" onmouseout="this.src='images/profile/add.gif'" onmouseover="this.src='images/profile/add-over.gif'" src="images/profile/add.gif" title="Add file to the bottom of your playlist without stopping current media."></a>
												<?php
												if(($combine_arr[$test_count][$newtest_count]['sharing_preference']==5 || $combine_arr[$test_count][$newtest_count]['sharing_preference']==2) && $combine_arr[$test_count][$newtest_count]['media_type'] == 114){
													$get_video_image = $new_profile_media_classobj->getvideoimage($combine_arr[$test_count][$newtest_count]['id']);
													$video_image_name = "https://img.youtube.com/vi/".$get_video_image."/default.jpg";
													if($combine_arr[$test_count][$newtest_count]['sharing_preference']==5)
													{
													?>
													<a href="javascript:void(0);" onclick="add_tocart(<?php echo $newtest_count;?>)"><img src="../images/profile/download_down.gif" onmouseover="this.src='../images/profile/download-over_down.gif'" onmouseout="this.src='../images/profile/download_down.gif'" title="Add media to your cart for free download or purchase." /></a>
													<?php
													}
													else if($combine_arr[$test_count][$newtest_count]['sharing_preference']==2)
													{
														$tip_add_aftr_detils = $display->get_tip_details_for_every($combine_arr[$test_count][$newtest_count]['id'],'media');
													?>
													<a href="javascript:void(0);" onclick="ask_to_donate('<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['title']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['creator']);?>','<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','add_to_cart_media<?php echo $newtest_count;?>','<?php echo $combine_arr[$test_count][$newtest_count]['general_user_id']; ?>','<?php echo $tip_add_aftr_detils; ?>')"><img src="../images/profile/download_down.gif" onmouseover="this.src='../images/profile/download-over_down.gif'" onmouseout="this.src='../images/profile/download_down.gif'" title="Add media to your cart for free download or purchase." /></a>
													<?php
													}
												}/* else{
												?>
												<img src="images/profile/download-over_down.gif" />
												<?php
												} */
												?>
												<a href="addtocart.php?seller_id=<?php echo $combine_arr[$test_count][$newtest_count]['general_user_id'];?>&media_id=<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>&buyer_id=<?php echo $_SESSION['gen_user_id'];?>&title=<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['title']);?>&image_name=<?php echo $video_image_name;?>" id="add_to_cart_media<?php echo $newtest_count;?>" style="display:none;"></a>
												<script type="text/javascript">
												$(document).ready(function() {
													$("#add_to_cart_media<?php echo $newtest_count;?>").fancybox({
													'height'			: '75%',
													'width'				: '69%',
													'transitionIn'		: 'none',
													'transitionOut'		: 'none',
													'type'				: 'iframe'
													});
												});
												</script>
											</div>
											</div>
										<?php
										}else if($combine_arr[$test_count][$newtest_count]['media_type']==113){
											$get_images_fan = $new_profile_media_classobj ->get_all_gallery_images($combine_arr[$test_count][$newtest_count]['id']);
											for($count_img=0;$count_img<=count($get_images_fan);$count_img++){
												if($get_all_img_fan ==""){
													$get_all_img_fan = $get_images_fan[$count_img]['image_name'];
												}else{
													$get_all_img_fan = $get_all_img_fan.",".$get_images_fan[$count_img]['image_name'];
												}
												if($galleryImagestitle_fan ==""){
													$galleryImagestitle_fan = $get_images_fan[$count_img]['image_title'];
												}else{
													$galleryImagestitle_fan = $galleryImagestitle_fan.",".$get_images_fan[$count_img]['image_title'];
												}
												$mediaSrc_fan = $get_images_fan[0]['image_name'];
											}
											$gal_id_fan = $get_images_fan[$count_img]['media_id'];
											$get_other_user_id = $user_keys_data->get_general_user_idby_mediaid($combine_arr[$test_count][$newtest_count]['id']);
											$orgPath_fan = $s3->getNewURI_otheruser("medgallery",$get_other_user_id);//"https://medgallery.s3.amazonaws.com/";
											$thumbPath_fan = $s3->getNewURI_otheruser("medgalthumb",$get_other_user_id);//"https://medgalthumb.s3.amazonaws.com/";
											
										?>
											<div class="blkplayer"><div class="player">
											<a href="javascript:void(0);"><img title="Play file now." src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" onclick="checkload('<?php echo $mediaSrc_fan;?>','<?php echo $get_all_img_fan; ?>','<?php echo $thumbPath_fan; ?>','<?php echo $orgPath_fan; ?>','<?php echo $galleryImagestitle_fan; ?>','<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>')"/></a>
											<!--<a href="javascript:void(0);"><img title="Add file to the bottom of your playlist without stopping current media." src="images/profile/add.gif"  onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'"/></a>-->
											<?php
												$get_gal_image_name  = $new_profile_media_classobj ->get_gallery_info2($combine_arr[$test_count][$newtest_count]['id']);
												if(($combine_arr[$test_count][$newtest_count]['sharing_preference']==2) && $combine_arr[$test_count][$newtest_count]['media_type'] == 114)
												{
													$tip_add_aftr_detils = $display->get_tip_details_for_every($combine_arr[$test_count][$newtest_count]['id'],'media');
													?>
													<a href="javascript:void(0);" onclick="ask_to_donate('<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['title']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['creator']);?>','<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','add_to_cart_media<?php echo $newtest_count;?>','<?php echo $combine_arr[$test_count][$newtest_count]['general_user_id']; ?>','<?php echo $tip_add_aftr_detils; ?>')"><img src="../images/profile/download_down.gif" onmouseover="this.src='../images/profile/download-over_down.gif'" onmouseout="this.src='../images/profile/download_down.gif'" title="Add media to your cart for free download or purchase." /></a>
													<?php
												}/* else{
												?>
												<img src="images/profile/download-over_down.gif" />
												<?php
												} */
												?>
												<a href="addtocart.php?seller_id=<?php echo $combine_arr[$test_count][$newtest_count]['general_user_id'];?>&media_id=<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>&buyer_id=<?php echo $_SESSION['gen_user_id'];?>&title=<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['title']);?>&image_name=<?php echo $get_gal_image_name;?>" id="add_to_cart_media<?php echo $newtest_count;?>" style="display:none;"></a>
												<script type="text/javascript">
												$(document).ready(function() {
													$("#add_to_cart_media<?php echo $newtest_count;?>").fancybox({
													'height'			: '75%',
													'width'				: '69%',
													'transitionIn'		: 'none',
													'transitionOut'		: 'none',
													'type'				: 'iframe'
													});
												});
												</script>
											</div>
											</div>
										<?php
											$get_all_img_fan ="";
											$galleryImagestitle_fan="";
											$mediaSrc_fan ="";
										}else if($combine_arr[$test_count][$newtest_count]['media_type'] == 116){
											$get_channel_media = $new_profile_media_classobj ->get_all_media_selected_channel($combine_arr[$test_count][$newtest_count]['id']);
										?>
										<div class="blkplayer"><div class="player">
											<input type="hidden" id="channel_media_song<?php echo $newtest_count;?>" value="<?php echo $get_channel_media['songs']; ?>"/>
											<input type="hidden" id="channel_media_video<?php echo $newtest_count;?>" value="<?php echo $get_channel_media['videos']; ?>"/>
											<a href="javascript:void(0);"><img title="Play file now." src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" onclick="PlayAll('channel_media_video<?php echo $newtest_count;?>','channel_media_song<?php echo $newtest_count;?>')"/></a>
											<a href="javascript:void(0);"><img title="Add file to the bottom of your playlist without stopping current media." src="images/profile/add.gif"  onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'" onclick="showPlayer('v','<?php if($combine_arr[$test_count][$newtest_count]['id']!=""){echo $combine_arr[$test_count][$newtest_count]['id'];}else{ echo $combine_arr[$test_count][$newtest_count]['id'];}?>','addvi');"/></a>
											<?php
												if(($combine_arr[$test_count][$newtest_count]['sharing_preference']==5 || $combine_arr[$test_count][$newtest_count]['sharing_preference']==2) && $combine_arr[$test_count][$newtest_count]['media_type'] == 114){
													?>
													<a onclick="download_directly_song('<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['title']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['creator']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['from']);?>')" href="javascript:void(0);">
													<img title="Add media to your cart for free download or purchase." onmouseout="this.src='../images/profile/download_down.gif'" onmouseover="this.src='../images/profile/download-over_down.gif'" src="../images/profile/download_down.gif"></a>
												<?php
												}/* else{
												?>
												<img src="images/profile/download-over_down.gif" />
												<?php
												} */
												?>
										</div>
										</div>
										<?php
										}
										else{
										?>
											<div class="blkplayer"><div class="player">
											<a href="javascript:void(0);"><img title="Play file now." src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" /></a>
											<a href="javascript:void(0);"><img title="Add file to the bottom of your playlist without stopping current media." src="images/profile/add.gif"  onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'"/></a>
											<?php
												if(($combine_arr[$test_count][$newtest_count]['sharing_preference']==5 || $combine_arr[$test_count][$newtest_count]['sharing_preference']==2) && $combine_arr[$test_count][$newtest_count]['media_type'] == 114){
													?>
													<a onclick="download_directly_song('<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['title']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['creator']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['from']);?>')" href="javascript:void(0);">
													<img title="Add media to your cart for free download or purchase." onmouseout="this.src='../images/profile/download_down.gif'" onmouseover="this.src='../images/profile/download-over_down.gif'" src="../images/profile/download_down.gif"></a>
												<?php
												}/* else{
												?>
												<img src="images/profile/download-over_down.gif" />
												<?php
												} */
												?>
											</div>
											</div>
										<?php
										}
										?>
										<div class="icon" onclick="runAccordion(<?php echo $combine_arr[$test_count][$newtest_count]['id']; ?>);">
											<div onselectstart="return false;"><img title="View details for media." src="images/profile/view.jpg" /></div>
										</div>
										<div class="icon"><!--<a href="add_media.php?edit=<?php// echo $combine_arr[$test_count][$newtest_count]['id'];?>"><img title="Edit media." src="images/profile/edit.png" /></a>--></div>
										<div class="icon"><a onclick="return confirmdelete('<?php echo $combine_arr[$test_count][$newtest_count]['title'];?>','<?php if($combine_arr[$test_count][$newtest_count]['name']=="Galleries/Images"){echo "Images";}else{echo $combine_arr[$test_count][$newtest_count]['name'];} ?>')" href="delete_media.php?id=<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>&whos_media=email_subscription"><img title="Remove media from your library." src="images/profile/delete.png" /></a></div>
										<div class="expandable" id="Accordion<?php echo $combine_arr[$test_count][$newtest_count]['id']; ?>Content">
											<div class="affiBlock">
												<div class="RowCont">
													<div class="sideL">Plays :</div>
													<div class="sideR"><a href="" target="_blank"><?php if($combine_arr[$test_count][$newtest_count]['play_count']!=""){ echo $combine_arr[$test_count][$newtest_count]['play_count'];}else{ echo "0";}?></a></div>
												</div>
													<?php
													if($combine_arr[$test_count][$newtest_count]['sharing_preference']==5 || $combine_arr[$test_count][$newtest_count]['sharing_preference']==2){
													?>
													<div class="RowCont">
														<div class="sideL">Downloads :</div>
														<div class="sideR"><a href="javascript:void(0);"><?php echo $combine_arr[$test_count][$newtest_count]['download_count'];?></a></div>
													</div>
													<?php
													}

													if($combine_arr[$test_count][$newtest_count]['media_type']=='113')
													{
													
													}
													else
													{
												?>	
												<!--<div class="RowCont">
													<div class="sideL">Media URL :</div>
													<div class="sideR"><a href="" target="_blank">www.<?php echo $domainname; ?></a></div>
												</div>-->
												<?php
													}
												?>
											</div>
										</div>
									</div>
									<?php
								}
								else if($combine_arr[$test_count][$newtest_count]['media_from']=="new_tagging_media")
								{
									?>
										<div class="tableCont" id="AccordionContainer">
										<div class="blkD" style="width:60px;"><?php if($combine_arr[$test_count][$newtest_count]['name']=="Galleries/Images"){echo "Images";}else{echo $combine_arr[$test_count][$newtest_count]['name'];} ?></div>
										<div class="blkI" style="width:127px;"><?php
										$exp_creator_info = explode("|",$combine_arr[$test_count][$newtest_count]['creator_info']);
										$pro_url = $new_profile_media_classobj->get_creator_url($exp_creator_info[0],$exp_creator_info[1]);
										?>
										<a href="<?php if($pro_url['profile_url'] !=""){ echo "/".$pro_url['profile_url']; }else { echo "javascript:void(0)"; }?>">
										<?php if($combine_arr[$test_count][$newtest_count]['creator']!=""){
										//$endpos = strpos($combine_arr[$test_count][$newtest_count]['creator'],"(");
										//if($endpos !="" || $endpos !=false) {
										//echo substr($combine_arr[$test_count][$newtest_count]['creator'],0,$endpos);} else {
										echo $combine_arr[$test_count][$newtest_count]['creator']; }
										else{?>&nbsp;<?php } ?></a></div>
										<div class="blkK"><?php
										$exp_from_info = explode("|",$combine_arr[$test_count][$newtest_count]['from_info']);
										$pro_url_from = $new_profile_media_classobj->get_from_url($exp_from_info[0],$exp_from_info[1]);
										?>
										<a href="<?php if($pro_url_from['profile_url'] !=""){ echo "/".$pro_url_from['profile_url']; }else { echo "javascript:void(0)"; }?>">
										<?php if($combine_arr[$test_count][$newtest_count]['from']!=""){
										//$endpos = strpos($combine_arr[$test_count][$newtest_count]['from'],"(");
										//if($endpos !="" || $endpos != false) {
										//echo substr($combine_arr[$test_count][$newtest_count]['from'],0,$endpos);} else {
										echo $combine_arr[$test_count][$newtest_count]['from']; }
										else{?>&nbsp;<?php } ?></a></div>
										<div class="blkB"><?php
												if($combine_arr[$test_count][$newtest_count]['title']!="")
												{
													if($combine_arr[$test_count][$newtest_count]['media_type'] == 114)
													{
														//$get_media_track = $new_profile_media_classobj->get_media_track($combine_arr[$test_count][$newtest_count]['id']); 
														if($combine_arr[$test_count][$newtest_count]['track'] != "" && $combine_arr[$test_count][$newtest_count]['track'] != NULL && $combine_arr[$test_count][$newtest_count]['track'] !=" "){
															echo $combine_arr[$test_count][$newtest_count]['track'].".".stripslashes($combine_arr[$test_count][$newtest_count]['title']);
														}else{
															echo stripslashes($combine_arr[$test_count][$newtest_count]['title']);
														}
													}
													else{
													echo stripslashes($combine_arr[$test_count][$newtest_count]['title']); }
												}else{
												?>&nbsp;<?php
												}
													?></div>
										<?php
										if($combine_arr[$test_count][$newtest_count]['media_type']==114)
										{
										?>
											<div class="blkplayer"><div class="player">
												<a href="javascript:void(0);"><img onclick="showPlayer('a','<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','play');" onmouseout="this.src='images/profile/play.gif'" onmouseover="this.src='images/profile/play-over.gif'" src="images/profile/play.gif" title="Play file now."></a>
												<a href="javascript:void(0);"><img onclick="showPlayer('a','<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','add');" onmouseout="this.src='images/profile/add.gif'" onmouseover="this.src='images/profile/add-over.gif'" src="images/profile/add.gif" title="Add file to the bottom of your playlist without stopping current media."></a>
												<?php
												if(($combine_arr[$test_count][$newtest_count]['sharing_preference']==5 || $combine_arr[$test_count][$newtest_count]['sharing_preference']==2) && $combine_arr[$test_count][$newtest_count]['media_type'] == 114){
													?>
													<a onclick="download_directly_song('<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['title']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['creator']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['from']);?>')" href="javascript:void(0);">
													<img title="Add media to your cart for free download or purchase." onmouseout="this.src='../images/profile/download_down.gif'" onmouseover="this.src='../images/profile/download-over_down.gif'" src="../images/profile/download_down.gif"></a>
												<?php
												}/* else{
												?>
												<img src="images/profile/download-over_down.gif" />
												<?php
												} */
												?>
											</div>
											</div>
										<?php
										}else if($combine_arr[$test_count][$newtest_count]['media_type']==115)
										{
										?>
											<div class="blkplayer"><div class="player">
												<a href="javascript:void(0);"><img onclick="showPlayer('v','<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','playlist');" onmouseout="this.src='images/profile/play.gif'" onmouseover="this.src='images/profile/play-over.gif'" src="images/profile/play.gif" title="Play file now."></a>
												<a href="javascript:void(0);"><img onclick="showPlayer('v','<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','addvi');" onmouseout="this.src='images/profile/add.gif'" onmouseover="this.src='images/profile/add-over.gif'" src="images/profile/add.gif" title="Add file to the bottom of your playlist without stopping current media."></a>
												<?php
												if(($combine_arr[$test_count][$newtest_count]['sharing_preference']==5 || $combine_arr[$test_count][$newtest_count]['sharing_preference']==2) && $combine_arr[$test_count][$newtest_count]['media_type'] == 114){
													?>
													<a onclick="download_directly_song('<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['title']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['creator']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['from']);?>')" href="javascript:void(0);">
													<img title="Add media to your cart for free download or purchase." onmouseout="this.src='../images/profile/download_down.gif'" onmouseover="this.src='../images/profile/download-over_down.gif'" src="../images/profile/download_down.gif"></a>
												<?php
												}/* else{
												?>
												<img src="images/profile/download-over_down.gif" />
												<?php
												} */
												?>
											</div>
											</div>
										<?php
										}else if($combine_arr[$test_count][$newtest_count]['media_type']==113){
											$get_images_fan = $new_profile_media_classobj ->get_all_gallery_images($combine_arr[$test_count][$newtest_count]['id']);
											for($count_img=0;$count_img<=count($get_images_fan);$count_img++){
												if($get_all_img_fan ==""){
													$get_all_img_fan = $get_images_fan[$count_img]['image_name'];
												}else{
													$get_all_img_fan = $get_all_img_fan.",".$get_images_fan[$count_img]['image_name'];
												}
												if($galleryImagestitle_fan ==""){
													$galleryImagestitle_fan = $get_images_fan[$count_img]['image_title'];
												}else{
													$galleryImagestitle_fan = $galleryImagestitle_fan.",".$get_images_fan[$count_img]['image_title'];
												}
												$mediaSrc_fan = $get_images_fan[0]['image_name'];
											}
											$gal_id_fan = $get_images_fan[$count_img]['media_id'];
											$get_other_user_id = $user_keys_data->get_general_user_idby_mediaid($combine_arr[$test_count][$newtest_count]['id']);
											$orgPath_fan = $s3->getNewURI_otheruser("medgallery",$get_other_user_id);//"https://medgallery.s3.amazonaws.com/";
											$thumbPath_fan = $s3->getNewURI_otheruser("medgalthumb",$get_other_user_id);//"https://medgalthumb.s3.amazonaws.com/";
											
										?>
											<div class="blkplayer"><div class="player">
											<a href="javascript:void(0);"><img title="Play file now." src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" onclick="checkload('<?php echo $mediaSrc_fan;?>','<?php echo $get_all_img_fan; ?>','<?php echo $thumbPath_fan; ?>','<?php echo $orgPath_fan; ?>','<?php echo $galleryImagestitle_fan; ?>','<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>')"/></a>
											<!--<a href="javascript:void(0);"><img title="Add file to the bottom of your playlist without stopping current media." src="images/profile/add.gif"  onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'"/></a>-->
											<?php
												if(($combine_arr[$test_count][$newtest_count]['sharing_preference']==5 || $combine_arr[$test_count][$newtest_count]['sharing_preference']==2) && $combine_arr[$test_count][$newtest_count]['media_type'] == 114){
													?>
													<a onclick="download_directly_song('<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['title']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['creator']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['from']);?>')" href="javascript:void(0);">
													<img title="Add media to your cart for free download or purchase." onmouseout="this.src='../images/profile/download_down.gif'" onmouseover="this.src='../images/profile/download-over_down.gif'" src="../images/profile/download_down.gif"></a>
												<?php
												}/* else{
												?>
												<img src="images/profile/download-over_down.gif" />
												<?php
												} */
												?>
											</div>
											</div>
										<?php
											$get_all_img_fan ="";
											$galleryImagestitle_fan="";
											$mediaSrc_fan ="";
										}else if($combine_arr[$test_count][$newtest_count]['media_type'] == 116){
											$get_channel_media = $new_profile_media_classobj ->get_all_media_selected_channel($combine_arr[$test_count][$newtest_count]['id']);
										?>
										<div class="blkplayer"><div class="player">
											<input type="hidden" id="channel_media_song<?php echo $newtest_count;?>" value="<?php echo $get_channel_media['songs']; ?>"/>
											<input type="hidden" id="channel_media_video<?php echo $newtest_count;?>" value="<?php echo $get_channel_media['videos']; ?>"/>
											<a href="javascript:void(0);"><img title="Play file now." src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" onclick="PlayAll('channel_media_video<?php echo $newtest_count;?>','channel_media_song<?php echo $newtest_count;?>')"/></a>
											<a href="javascript:void(0);"><img title="Add file to the bottom of your playlist without stopping current media." src="images/profile/add.gif"  onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'" onclick="showPlayer('v','<?php if($combine_arr[$test_count][$newtest_count]['id']!=""){echo $combine_arr[$test_count][$newtest_count]['id'];}else{ echo $combine_arr[$test_count][$newtest_count]['id'];}?>','addvi');"/></a>
											<?php
												if(($combine_arr[$test_count][$newtest_count]['sharing_preference']==5 || $combine_arr[$test_count][$newtest_count]['sharing_preference']==2) && $combine_arr[$test_count][$newtest_count]['media_type'] == 114){
													?>
													<a onclick="download_directly_song('<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['title']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['creator']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['from']);?>')" href="javascript:void(0);">
													<img title="Add media to your cart for free download or purchase." onmouseout="this.src='../images/profile/download_down.gif'" onmouseover="this.src='../images/profile/download-over_down.gif'" src="../images/profile/download_down.gif"></a>
												<?php
												}/* else{
												?>
												<img src="images/profile/download-over_down.gif" />
												<?php
												} */
												?>
										</div>
										</div>
										<?php
										}else{
										?>
											<div class="blkplayer"><div class="player">
											<a href="javascript:void(0);"><img title="Play file now." src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" /></a>
											<a href="javascript:void(0);"><img title="Add file to the bottom of your playlist without stopping current media." src="images/profile/add.gif"  onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'"/></a>
											<?php
												if(($combine_arr[$test_count][$newtest_count]['sharing_preference']==5 || $combine_arr[$test_count][$newtest_count]['sharing_preference']==2) && $combine_arr[$test_count][$newtest_count]['media_type'] == 114){
													?>
													<a onclick="download_directly_song('<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['title']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['creator']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['from']);?>')" href="javascript:void(0);">
													<img title="Add media to your cart for free download or purchase." onmouseout="this.src='../images/profile/download_down.gif'" onmouseover="this.src='../images/profile/download-over_down.gif'" src="../images/profile/download_down.gif"></a>
												<?php
												}/* else{
												?>
												<img src="images/profile/download-over_down.gif" />
												<?php
												} */
												?>
											</div>
											</div>
										<?php
										}
										?>
										<div class="icon" onclick="runAccordion(<?php echo $combine_arr[$test_count][$newtest_count]['id']; ?>);">
											<div onselectstart="return false;"><img title="View details for media." src="images/profile/view.jpg" /></div>
										</div>
										<div class="icon"><!--<a href="add_media.php?edit=<?php// echo $combine_arr[$test_count][$newtest_count]['id'];?>"><img title="Edit media." src="images/profile/edit.png" /></a>--></div>
										<div class="icon"><a onclick="return confirmdelete('<?php echo $combine_arr[$test_count][$newtest_count]['title'];?>','<?php if($combine_arr[$test_count][$newtest_count]['name']=="Galleries/Images"){echo "Images";}else{echo $combine_arr[$test_count][$newtest_count]['name'];} ?>')" href="delete_media.php?id=<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>&whos_media=new_tagging_media"><img title="Remove media from your library." src="images/profile/delete.png" /></a></div>
										<div class="expandable" id="Accordion<?php echo $combine_arr[$test_count][$newtest_count]['id']; ?>Content">
											<div class="affiBlock">
												<div class="RowCont">
													<div class="sideL">Plays :</div>
													<div class="sideR"><a href="" target="_blank"><?php if($combine_arr[$test_count][$newtest_count]['play_count']!=""){ echo $combine_arr[$test_count][$newtest_count]['play_count'];}else{ echo "0";}?></a></div>
												</div>
												<?php
													if($combine_arr[$test_count][$newtest_count]['sharing_preference']==5 || $combine_arr[$test_count][$newtest_count]['sharing_preference']==2){
													?>
													<div class="RowCont">
														<div class="sideL">Downloads :</div>
														<div class="sideR"><a href="javascript:void(0);"><?php echo $combine_arr[$test_count][$newtest_count]['download_count'];?></a></div>
													</div>
													<?php
													}

													if($combine_arr[$test_count][$newtest_count]['media_type']=='113')
													{
													
													}
													else
													{
												?>	
												<!--<div class="RowCont">
													<div class="sideL">Media URL :</div>
													<div class="sideR"><a href="" target="_blank">www.<?php echo $domainname; ?></a></div>
												</div>-->
												<?php
													}
												?>
											</div>
										</div>
									</div>
									<?php
								}
								
								else if($combine_arr[$test_count][$newtest_count]['media_from']=="media_creator_creator")
								{
									?>
										<div class="tableCont" id="AccordionContainer">
										<div class="blkD" style="width:60px;"><?php if($combine_arr[$test_count][$newtest_count]['name']=="Galleries/Images"){echo "Images";}else{echo $combine_arr[$test_count][$newtest_count]['name'];} ?></div>
										<div class="blkI" style="width:127px;"><?php
										$exp_creator_info = explode("|",$combine_arr[$test_count][$newtest_count]['creator_info']);
										$pro_url = $new_profile_media_classobj->get_creator_url($exp_creator_info[0],$exp_creator_info[1]);
										?>
										<a href="<?php if($pro_url['profile_url'] !=""){ echo "/".$pro_url['profile_url']; }else { echo "javascript:void(0)"; }?>">
										<?php if($combine_arr[$test_count][$newtest_count]['creator']!=""){
										//$endpos = strpos($combine_arr[$test_count][$newtest_count]['creator'],"(");
										//if($endpos !="" || $endpos !=false) {
										//echo substr($combine_arr[$test_count][$newtest_count]['creator'],0,$endpos);} else {
										echo $combine_arr[$test_count][$newtest_count]['creator']; }
										else{?>&nbsp;<?php } ?></a></div>
										<div class="blkK"><?php
										$exp_from_info = explode("|",$combine_arr[$test_count][$newtest_count]['from_info']);
										$pro_url_from = $new_profile_media_classobj->get_from_url($exp_from_info[0],$exp_from_info[1]);
										?>
										<a href="<?php if($pro_url_from['profile_url'] !=""){ echo "/".$pro_url_from['profile_url']; }else { echo "javascript:void(0)"; }?>">
										<?php if($combine_arr[$test_count][$newtest_count]['from']!=""){
										//$endpos = strpos($combine_arr[$test_count][$newtest_count]['from'],"(");
										//if($endpos !="" || $endpos != false) {
										//echo substr($combine_arr[$test_count][$newtest_count]['from'],0,$endpos);} else {
										echo $combine_arr[$test_count][$newtest_count]['from']; }
										else{?>&nbsp;<?php } ?></a></div>
										<div class="blkB"><?php
												if($combine_arr[$test_count][$newtest_count]['title']!="")
												{
													if($combine_arr[$test_count][$newtest_count]['media_type'] == 114)
													{
														//$get_media_track = $new_profile_media_classobj->get_media_track($combine_arr[$test_count][$newtest_count]['id']); 
														if($combine_arr[$test_count][$newtest_count]['track'] != "" && $combine_arr[$test_count][$newtest_count]['track'] != NULL && $combine_arr[$test_count][$newtest_count]['track'] !=" "){
															echo $combine_arr[$test_count][$newtest_count]['track'].".".stripslashes($combine_arr[$test_count][$newtest_count]['title']);
														}else{
															echo stripslashes($combine_arr[$test_count][$newtest_count]['title']);
														}
													}
													else{
													echo stripslashes($combine_arr[$test_count][$newtest_count]['title']); }
												}else{
												?>&nbsp;<?php
												}
													?></div>
										<?php
										if($combine_arr[$test_count][$newtest_count]['media_type']==114)
										{
										?>
											<div class="blkplayer"><div class="player">
												<a href="javascript:void(0);"><img onclick="showPlayer('a','<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','play');" onmouseout="this.src='images/profile/play.gif'" onmouseover="this.src='images/profile/play-over.gif'" src="images/profile/play.gif" title="Play file now."></a>
												<a href="javascript:void(0);"><img onclick="showPlayer('a','<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','add');" onmouseout="this.src='images/profile/add.gif'" onmouseover="this.src='images/profile/add-over.gif'" src="images/profile/add.gif" title="Add file to the bottom of your playlist without stopping current media."></a>
												<?php
												if(($combine_arr[$test_count][$newtest_count]['sharing_preference']==5 || $combine_arr[$test_count][$newtest_count]['sharing_preference']==2) && $combine_arr[$test_count][$newtest_count]['media_type'] == 114){
													?>
													<a onclick="download_directly_song('<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['title']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['creator']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['from']);?>')" href="javascript:void(0);">
													<img title="Add media to your cart for free download or purchase." onmouseout="this.src='../images/profile/download_down.gif'" onmouseover="this.src='../images/profile/download-over_down.gif'" src="../images/profile/download_down.gif"></a>
												<?php
												}/* else{
												?>
												<img src="images/profile/download-over_down.gif" />
												<?php
												} */
												?>
											</div>
											</div>
										<?php
										}else if($combine_arr[$test_count][$newtest_count]['media_type']==115)
										{
										?>
											<div class="blkplayer"><div class="player">
												<a href="javascript:void(0);"><img onclick="showPlayer('v','<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','playlist');" onmouseout="this.src='images/profile/play.gif'" onmouseover="this.src='images/profile/play-over.gif'" src="images/profile/play.gif" title="Play file now."></a>
												<a href="javascript:void(0);"><img onclick="showPlayer('v','<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','addvi');" onmouseout="this.src='images/profile/add.gif'" onmouseover="this.src='images/profile/add-over.gif'" src="images/profile/add.gif" title="Add file to the bottom of your playlist without stopping current media."></a>
												<?php
												if(($combine_arr[$test_count][$newtest_count]['sharing_preference']==5 || $combine_arr[$test_count][$newtest_count]['sharing_preference']==2) && $combine_arr[$test_count][$newtest_count]['media_type'] == 114){
													?>
													<a onclick="download_directly_song('<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['title']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['creator']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['from']);?>')" href="javascript:void(0);">
													<img title="Add media to your cart for free download or purchase." onmouseout="this.src='../images/profile/download_down.gif'" onmouseover="this.src='../images/profile/download-over_down.gif'" src="../images/profile/download_down.gif"></a>
												<?php
												}/* else{
												?>
												<img src="images/profile/download-over_down.gif" />
												<?php
												} */
												?>
											</div>
											</div>
										<?php
										}else if($combine_arr[$test_count][$newtest_count]['media_type']==113){
											$get_images_fan = $new_profile_media_classobj ->get_all_gallery_images($combine_arr[$test_count][$newtest_count]['id']);
											for($count_img=0;$count_img<=count($get_images_fan);$count_img++){
												if($get_all_img_fan == ""){
													$get_all_img_fan = $get_images_fan[$count_img]['image_name'];
												}else{
													$get_all_img_fan = $get_all_img_fan.",".$get_images_fan[$count_img]['image_name'];
												}
												if($galleryImagestitle_fan == ""){
													$galleryImagestitle_fan = $get_images_fan[$count_img]['image_title'];
												}else{
													$galleryImagestitle_fan = $galleryImagestitle_fan.",".$get_images_fan[$count_img]['image_title'];
												}
												$mediaSrc_fan = $get_images_fan[0]['image_name'];
											}
											$gal_id_fan = $get_images_fan[$count_img]['media_id'];
											$get_other_user_id = $user_keys_data->get_general_user_idby_mediaid($combine_arr[$test_count][$newtest_count]['id']);
											$orgPath_fan = $s3->getNewURI_otheruser("medgallery",$get_other_user_id);//"https://medgallery.s3.amazonaws.com/";
											$thumbPath_fan = $s3->getNewURI_otheruser("medgalthumb",$get_other_user_id);//"https://medgalthumb.s3.amazonaws.com/";
											
										?>
											<div class="blkplayer"><div class="player">
											<a href="javascript:void(0);"><img title="Play file now." src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" onclick="checkload('<?php echo $mediaSrc_fan;?>','<?php echo $get_all_img_fan; ?>','<?php echo $thumbPath_fan; ?>','<?php echo $orgPath_fan; ?>','<?php echo $galleryImagestitle_fan; ?>','<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>')"/></a>
											<!--<a href="javascript:void(0);"><img title="Add file to the bottom of your playlist without stopping current media." src="images/profile/add.gif"  onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'"/></a>-->
											<?php
												if(($combine_arr[$test_count][$newtest_count]['sharing_preference']==5 || $combine_arr[$test_count][$newtest_count]['sharing_preference']==2) && $combine_arr[$test_count][$newtest_count]['media_type'] == 114){
													?>
													<a onclick="download_directly_song('<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['title']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['creator']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['from']);?>')" href="javascript:void(0);">
													<img title="Add media to your cart for free download or purchase." onmouseout="this.src='../images/profile/download_down.gif'" onmouseover="this.src='../images/profile/download-over_down.gif'" src="../images/profile/download_down.gif"></a>
												<?php
												}/* else{
												?>
												<img src="images/profile/download-over_down.gif" />
												<?php
												} */
												?>
											</div>
											</div>
										<?php
											$get_all_img_fan ="";
											$galleryImagestitle_fan="";
											$mediaSrc_fan ="";
										}else if($combine_arr[$test_count][$newtest_count]['media_type'] == 116){
											$get_channel_media = $new_profile_media_classobj ->get_all_media_selected_channel($combine_arr[$test_count][$newtest_count]['id']);
										?>
										<div class="blkplayer"><div class="player">
											<input type="hidden" id="channel_media_song<?php echo $newtest_count;?>" value="<?php echo $get_channel_media['songs']; ?>"/>
											<input type="hidden" id="channel_media_video<?php echo $newtest_count;?>" value="<?php echo $get_channel_media['videos']; ?>"/>
											<a href="javascript:void(0);"><img title="Play file now." src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" onclick="PlayAll('channel_media_video<?php echo $newtest_count;?>','channel_media_song<?php echo $newtest_count;?>')"/></a>
											<a href="javascript:void(0);"><img title="Add file to the bottom of your playlist without stopping current media." src="images/profile/add.gif"  onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'" onclick="showPlayer('v','<?php if($combine_arr[$test_count][$newtest_count]['id']!=""){echo $combine_arr[$test_count][$newtest_count]['id'];}else{ echo $combine_arr[$test_count][$newtest_count]['id'];}?>','addvi');"/></a>
											<?php
												if(($combine_arr[$test_count][$newtest_count]['sharing_preference']==5 || $combine_arr[$test_count][$newtest_count]['sharing_preference']==2) && $combine_arr[$test_count][$newtest_count]['media_type'] == 114){
													?>
													<a onclick="download_directly_song('<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['title']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['creator']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['from']);?>')" href="javascript:void(0);">
													<img title="Add media to your cart for free download or purchase." onmouseout="this.src='../images/profile/download_down.gif'" onmouseover="this.src='../images/profile/download-over_down.gif'" src="../images/profile/download_down.gif"></a>
												<?php
												}/* else{
												?>
												<img src="images/profile/download-over_down.gif" />
												<?php
												} */
												?>
										</div>
										</div>
										<?php
										}
										else{
										?>
											<div class="blkplayer"><div class="player">
											<a href="javascript:void(0);"><img title="Play file now." src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" /></a>
											<a href="javascript:void(0);"><img title="Add file to the bottom of your playlist without stopping current media." src="images/profile/add.gif"  onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'"/></a>
											<?php
												if(($combine_arr[$test_count][$newtest_count]['sharing_preference']==5 || $combine_arr[$test_count][$newtest_count]['sharing_preference']==2) && $combine_arr[$test_count][$newtest_count]['media_type'] == 114){
													?>
													<a onclick="download_directly_song('<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['title']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['creator']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['from']);?>')" href="javascript:void(0);">
													<img title="Add media to your cart for free download or purchase." onmouseout="this.src='../images/profile/download_down.gif'" onmouseover="this.src='../images/profile/download-over_down.gif'" src="../images/profile/download_down.gif"></a>
												<?php
												}/* else{
												?>
												<img src="images/profile/download-over_down.gif" />
												<?php
												} */
												?>
											</div>
											</div>
										<?php
										}
										?>
										<div class="icon" onclick="runAccordion(<?php echo $combine_arr[$test_count][$newtest_count]['id']; ?>);">
											<div onselectstart="return false;"><img title="View details for media." src="images/profile/view.jpg" /></div>
										</div>
										<div class="icon">&nbsp;<!--<a href="add_media.php?edit=<?php// echo $combine_arr[$test_count][$newtest_count]['id'];?>"><img title="Edit media." src="images/profile/edit.png" /></a>--></div>
										<div class="icon"><a onclick="return confirmdelete('<?php echo $combine_arr[$test_count][$newtest_count]['title'];?>','<?php if($combine_arr[$test_count][$newtest_count]['name']=="Galleries/Images"){echo "Images";}else{echo $combine_arr[$test_count][$newtest_count]['name'];} ?>')" href="delete_media.php?id=<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>&whos_media=media_creator_creator"><img title="Remove media from your library." src="images/profile/delete.png" /></a></div>
										<div class="expandable" id="Accordion<?php echo $combine_arr[$test_count][$newtest_count]['id']; ?>Content">
											<div class="affiBlock">
												<div class="RowCont">
													<div class="sideL">Plays :</div>
													<div class="sideR"><a href="" target="_blank"><?php if($combine_arr[$test_count][$newtest_count]['play_count']!=""){ echo $combine_arr[$test_count][$newtest_count]['play_count'];}else{ echo "0";}?></a></div>
												</div>
												<?php
													if($combine_arr[$test_count][$newtest_count]['sharing_preference']==5 || $combine_arr[$test_count][$newtest_count]['sharing_preference']==2){
													?>
													<div class="RowCont">
														<div class="sideL">Downloads :</div>
														<div class="sideR"><a href="javascript:void(0);"><?php echo $combine_arr[$test_count][$newtest_count]['download_count'];?></a></div>
													</div>
													<?php
													}

													if($combine_arr[$test_count][$newtest_count]['media_type']=='113')
													{
												
													}
													else
													{
												?>	
												<!--<div class="RowCont">
													<div class="sideL">Media URL :</div>
													<div class="sideR"><a href="" target="_blank">www.<?php echo $domainname; ?></a></div>
												</div>-->
												<?php
													}
												?>
											</div>
										</div>
									</div>
									<?php
								}
								else if($combine_arr[$test_count][$newtest_count]['media_from']=="media_create_new")
								{
									?>
										<div class="tableCont" id="AccordionContainer">
										<div class="blkD" style="width:60px;"><?php if($combine_arr[$test_count][$newtest_count]['name']=="Galleries/Images"){echo "Images";}else{echo $combine_arr[$test_count][$newtest_count]['name'];} ?></div>
										<div class="blkI" style="width:127px;"><?php
										$exp_creator_info = explode("|",$combine_arr[$test_count][$newtest_count]['creator_info']);
										$pro_url = $new_profile_media_classobj->get_creator_url($exp_creator_info[0],$exp_creator_info[1]);
										?>
										<a href="<?php if($pro_url['profile_url'] !=""){ echo "/".$pro_url['profile_url']; }else { echo "javascript:void(0)"; }?>">
										<?php if($combine_arr[$test_count][$newtest_count]['creator']!=""){
										//$endpos = strpos($combine_arr[$test_count][$newtest_count]['creator'],"(");
										//if($endpos !="" || $endpos !=false) {
										//echo substr($combine_arr[$test_count][$newtest_count]['creator'],0,$endpos);} else {
										echo $combine_arr[$test_count][$newtest_count]['creator']; }
										else{?>&nbsp;<?php } ?></a></div>
										<div class="blkK"><?php
										$exp_from_info = explode("|",$combine_arr[$test_count][$newtest_count]['from_info']);
										$pro_url_from = $new_profile_media_classobj->get_from_url($exp_from_info[0],$exp_from_info[1]);
										?>
										<a href="<?php if($pro_url_from['profile_url'] !=""){ echo "/".$pro_url_from['profile_url']; }else { echo "javascript:void(0)"; }?>">
										<?php if($combine_arr[$test_count][$newtest_count]['from']!=""){
										///$endpos = strpos($combine_arr[$test_count][$newtest_count]['from'],"(");
										//if($endpos !="" || $endpos != false) {
										//echo substr($combine_arr[$test_count][$newtest_count]['from'],0,$endpos);} else {
										echo $combine_arr[$test_count][$newtest_count]['from']; }
										else{?>&nbsp;<?php } ?></a></div>
										<div class="blkB"><?php
												if($combine_arr[$test_count][$newtest_count]['title']!="")
												{
													if($combine_arr[$test_count][$newtest_count]['media_type'] == 114)
													{
														//$get_media_track = $new_profile_media_classobj->get_media_track($combine_arr[$test_count][$newtest_count]['id']); 
														if($combine_arr[$test_count][$newtest_count]['track'] != "" && $combine_arr[$test_count][$newtest_count]['track'] != NULL && $combine_arr[$test_count][$newtest_count]['track'] !=" "){
															echo $combine_arr[$test_count][$newtest_count]['track'].".".stripslashes($combine_arr[$test_count][$newtest_count]['title']);
														}else{
															echo stripslashes($combine_arr[$test_count][$newtest_count]['title']);
														}
													}
													else{
													echo stripslashes($combine_arr[$test_count][$newtest_count]['title']); }
												}else{
												?>&nbsp;<?php
												}
													?></div>
										<?php
										if($combine_arr[$test_count][$newtest_count]['media_type']==114)
										{
										?>
											<div class="blkplayer"><div class="player">
												<a href="javascript:void(0);"><img onclick="showPlayer('a','<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','play');" onmouseout="this.src='images/profile/play.gif'" onmouseover="this.src='images/profile/play-over.gif'" src="images/profile/play.gif" title="Play file now."></a>
												<a href="javascript:void(0);"><img onclick="showPlayer('a','<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','add');" onmouseout="this.src='images/profile/add.gif'" onmouseover="this.src='images/profile/add-over.gif'" src="images/profile/add.gif" title="Add file to the bottom of your playlist without stopping current media."></a>
												<?php
												if(($combine_arr[$test_count][$newtest_count]['sharing_preference']==5 || $combine_arr[$test_count][$newtest_count]['sharing_preference']==2) && $combine_arr[$test_count][$newtest_count]['media_type'] == 114){
													?>
													<a onclick="download_directly_song('<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['title']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['creator']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['from']);?>')" href="javascript:void(0);">
													<img title="Add media to your cart for free download or purchase." onmouseout="this.src='../images/profile/download_down.gif'" onmouseover="this.src='../images/profile/download-over_down.gif'" src="../images/profile/download_down.gif"></a>
												<?php
												}/* else{
												?>
												<img src="images/profile/download-over_down.gif" />
												<?php
												} */
												?>
											</div>
											</div>
										<?php
										}else if($combine_arr[$test_count][$newtest_count]['media_type']==115)
										{
										?>
											<div class="blkplayer"><div class="player">
												<a href="javascript:void(0);"><img onclick="showPlayer('v','<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','playlist');" onmouseout="this.src='images/profile/play.gif'" onmouseover="this.src='images/profile/play-over.gif'" src="images/profile/play.gif" title="Play file now."></a>
												<a href="javascript:void(0);"><img onclick="showPlayer('v','<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','addvi');" onmouseout="this.src='images/profile/add.gif'" onmouseover="this.src='images/profile/add-over.gif'" src="images/profile/add.gif" title="Add file to the bottom of your playlist without stopping current media."></a>
												<?php
												if(($combine_arr[$test_count][$newtest_count]['sharing_preference']==5 || $combine_arr[$test_count][$newtest_count]['sharing_preference']==2) && $combine_arr[$test_count][$newtest_count]['media_type'] == 114){
													?>
													<a onclick="download_directly_song('<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['title']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['creator']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['from']);?>')" href="javascript:void(0);">
													<img title="Add media to your cart for free download or purchase." onmouseout="this.src='../images/profile/download_down.gif'" onmouseover="this.src='../images/profile/download-over_down.gif'" src="../images/profile/download_down.gif"></a>
												<?php
												}/* else{
												?>
												<img src="images/profile/download-over_down.gif" />
												<?php
												} */
												?>
											</div>
											</div>
										<?php
										}else if($combine_arr[$test_count][$newtest_count]['media_type']==113){
											$get_images_fan = $new_profile_media_classobj ->get_all_gallery_images($combine_arr[$test_count][$newtest_count]['id']);
											for($count_img=0;$count_img<=count($get_images_fan);$count_img++){
												if($get_all_img_fan == ""){
													$get_all_img_fan = $get_images_fan[$count_img]['image_name'];
												}else{
													$get_all_img_fan = $get_all_img_fan.",".$get_images_fan[$count_img]['image_name'];
												}
												if($galleryImagestitle_fan==""){
													$galleryImagestitle_fan = $get_images_fan[$count_img]['image_title'];
												}else{
													$galleryImagestitle_fan = $galleryImagestitle_fan.",".$get_images_fan[$count_img]['image_title'];
												}
												$mediaSrc_fan = $get_images_fan[0]['image_name'];
											}
											$gal_id_fan = $get_images_fan[$count_img]['media_id'];
											$get_other_user_id = $user_keys_data->get_general_user_idby_mediaid($combine_arr[$test_count][$newtest_count]['id']);
											$orgPath_fan = $s3->getNewURI_otheruser("medgallery",$get_other_user_id);//"https://medgallery.s3.amazonaws.com/";
											$thumbPath_fan = $s3->getNewURI_otheruser("medgalthumb",$get_other_user_id);//"https://medgalthumb.s3.amazonaws.com/";
											
										?>
											<div class="blkplayer"><div class="player">
											<a href="javascript:void(0);"><img title="Play file now." src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" onclick="checkload('<?php echo $mediaSrc_fan;?>','<?php echo $get_all_img_fan; ?>','<?php echo $thumbPath_fan; ?>','<?php echo $orgPath_fan; ?>','<?php echo $galleryImagestitle_fan; ?>','<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>')"/></a>
											<!--<a href="javascript:void(0);"><img title="Add file to the bottom of your playlist without stopping current media." src="images/profile/add.gif"  onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'"/></a>-->
											<?php
												if(($combine_arr[$test_count][$newtest_count]['sharing_preference']==5 || $combine_arr[$test_count][$newtest_count]['sharing_preference']==2) && $combine_arr[$test_count][$newtest_count]['media_type'] == 114){
													?>
													<a onclick="download_directly_song('<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['title']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['creator']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['from']);?>')" href="javascript:void(0);">
													<img title="Add media to your cart for free download or purchase." onmouseout="this.src='../images/profile/download_down.gif'" onmouseover="this.src='../images/profile/download-over_down.gif'" src="../images/profile/download_down.gif"></a>
												<?php
												}/* else{
												?>
												<img src="images/profile/download-over_down.gif" />
												<?php
												} */
												?>
											</div>
											</div>
										<?php
											$get_all_img_fan ="";
											$galleryImagestitle_fan="";
											$mediaSrc_fan ="";
										}else if($combine_arr[$test_count][$newtest_count]['media_type'] == 116){
											$get_channel_media = $new_profile_media_classobj ->get_all_media_selected_channel($combine_arr[$test_count][$newtest_count]['id']);
										?>
										<div class="blkplayer"><div class="player">
											<input type="hidden" id="channel_media_song<?php echo $newtest_count;?>" value="<?php echo $get_channel_media['songs']; ?>"/>
											<input type="hidden" id="channel_media_video<?php echo $newtest_count;?>" value="<?php echo $get_channel_media['videos']; ?>"/>
											<a href="javascript:void(0);"><img title="Play file now." src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" onclick="PlayAll('channel_media_video<?php echo $newtest_count;?>','channel_media_song<?php echo $newtest_count;?>')"/></a>
											<a href="javascript:void(0);"><img title="Add file to the bottom of your playlist without stopping current media." src="images/profile/add.gif"  onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'" onclick="showPlayer('v','<?php if($combine_arr[$test_count][$newtest_count]['id']!=""){echo $combine_arr[$test_count][$newtest_count]['id'];}else{ echo $combine_arr[$test_count][$newtest_count]['id'];}?>','addvi');"/></a>
											<?php
												if(($combine_arr[$test_count][$newtest_count]['sharing_preference']==5 || $combine_arr[$test_count][$newtest_count]['sharing_preference']==2) && $combine_arr[$test_count][$newtest_count]['media_type'] == 114){
													?>
													<a onclick="download_directly_song('<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['title']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['creator']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['from']);?>')" href="javascript:void(0);">
													<img title="Add media to your cart for free download or purchase." onmouseout="this.src='../images/profile/download_down.gif'" onmouseover="this.src='../images/profile/download-over_down.gif'" src="../images/profile/download_down.gif"></a>
												<?php
												}/* else{
												?>
												<img src="images/profile/download-over_down.gif" />
												<?php
												} */
												?>
										</div>
										</div>
										<?php
										}
										else{
										?>
											<div class="blkplayer"><div class="player">
											<a href="javascript:void(0);"><img title="Play file now." src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" /></a>
											<a href="javascript:void(0);"><img title="Add file to the bottom of your playlist without stopping current media." src="images/profile/add.gif"  onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'"/></a>
											<?php
												if(($combine_arr[$test_count][$newtest_count]['sharing_preference']==5 || $combine_arr[$test_count][$newtest_count]['sharing_preference']==2) && $combine_arr[$test_count][$newtest_count]['media_type'] == 114){
													?>
													<a onclick="download_directly_song('<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['title']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['creator']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['from']);?>')" href="javascript:void(0);">
													<img title="Add media to your cart for free download or purchase." onmouseout="this.src='../images/profile/download_down.gif'" onmouseover="this.src='../images/profile/download-over_down.gif'" src="../images/profile/download_down.gif"></a>
												<?php
												}/* else{
												?>
												<img src="images/profile/download-over_down.gif" />
												<?php
												} */
												?>
											</div>
											</div>
										<?php
										}
										?>
										<div class="icon" onclick="runAccordion(<?php echo $combine_arr[$test_count][$newtest_count]['id']; ?>);">
											<div onselectstart="return false;"><img title="View details for media." src="images/profile/view.jpg" /></div>
										</div>
										<div class="icon">&nbsp;<!--<a href="add_media.php?edit=<?php// echo $combine_arr[$test_count][$newtest_count]['id'];?>"><img title="Edit media." src="images/profile/edit.png" /></a>--></div>
										<div class="icon"><a onclick="return confirmdelete('<?php echo $combine_arr[$test_count][$newtest_count]['title'];?>','<?php if($combine_arr[$test_count][$newtest_count]['name']=="Galleries/Images"){echo "Images";}else{echo $combine_arr[$test_count][$newtest_count]['name'];} ?>')" href="delete_media.php?id=<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>&whos_media=media_create_new"><img title="Remove media from your library." src="images/profile/delete.png" /></a></div>
										<div class="expandable" id="Accordion<?php echo $combine_arr[$test_count][$newtest_count]['id']; ?>Content">
											<div class="affiBlock">
												<div class="RowCont">
													<div class="sideL">Plays :</div>
													<div class="sideR"><a href="" target="_blank"><?php if($combine_arr[$test_count][$newtest_count]['play_count']!=""){ echo $combine_arr[$test_count][$newtest_count]['play_count'];}else{ echo "0";}?></a></div>
												</div>
												<?php
													if($combine_arr[$test_count][$newtest_count]['sharing_preference']==5 || $combine_arr[$test_count][$newtest_count]['sharing_preference']==2){
													?>
													<div class="RowCont">
														<div class="sideL">Downloads :</div>
														<div class="sideR"><a href="javascript:void(0);"><?php echo $combine_arr[$test_count][$newtest_count]['download_count'];?></a></div>
													</div>
													<?php
													}

													if($combine_arr[$test_count][$newtest_count]['media_type']=='113')
													{
													
													}
													else
													{
												?>	
												<!--<div class="RowCont">
													<div class="sideL">Media URL :</div>
													<div class="sideR"><a href="" target="_blank">www.<?php echo $domainname; ?></a></div>
												</div>-->
												<?php
													}
												?>
											</div>
										</div>
									</div>
									<?php
								}
								else if($combine_arr[$test_count][$newtest_count]['media_from']=="media_from_from")
								{
									?>
										<div class="tableCont" id="AccordionContainer">
										<div class="blkD" style="width:60px;"><?php if($combine_arr[$test_count][$newtest_count]['name']=="Galleries/Images"){echo "Images";}else{echo $combine_arr[$test_count][$newtest_count]['name'];} ?></div>
										<div class="blkI" style="width:127px;"><?php
										$exp_creator_info = explode("|",$combine_arr[$test_count][$newtest_count]['creator_info']);
										$pro_url = $new_profile_media_classobj->get_creator_url($exp_creator_info[0],$exp_creator_info[1]);
										?>
										<a href="<?php if($pro_url['profile_url'] !=""){ echo "/".$pro_url['profile_url']; }else { echo "javascript:void(0)"; }?>">
										<?php if($combine_arr[$test_count][$newtest_count]['creator']!=""){
										//$endpos = strpos($combine_arr[$test_count][$newtest_count]['creator'],"(");
										//if($endpos !="" || $endpos !=false) {
										//echo substr($combine_arr[$test_count][$newtest_count]['creator'],0,$endpos);} else {
										echo $combine_arr[$test_count][$newtest_count]['creator']; }
										else{?>&nbsp;<?php } ?></a></div>
										<div class="blkK"><?php
										$exp_from_info = explode("|",$combine_arr[$test_count][$newtest_count]['from_info']);
										$pro_url_from = $new_profile_media_classobj->get_from_url($exp_from_info[0],$exp_from_info[1]);
										?>
										<a href="<?php if($pro_url_from['profile_url'] !=""){ echo "/".$pro_url_from['profile_url']; }else { echo "javascript:void(0)"; }?>">
										<?php if($combine_arr[$test_count][$newtest_count]['from']!=""){
										//$endpos = strpos($combine_arr[$test_count][$newtest_count]['from'],"(");
										//if($endpos !="" || $endpos != false) {
										//echo substr($combine_arr[$test_count][$newtest_count]['from'],0,$endpos);} else {
										echo $combine_arr[$test_count][$newtest_count]['from']; }
										else{?>&nbsp;<?php } ?></a></div>
										<div class="blkB"><?php
												if($combine_arr[$test_count][$newtest_count]['title']!="")
												{
													if($combine_arr[$test_count][$newtest_count]['media_type'] == 114)
													{
														//$get_media_track = $new_profile_media_classobj->get_media_track($combine_arr[$test_count][$newtest_count]['id']); 
														if($combine_arr[$test_count][$newtest_count]['track'] != "" && $combine_arr[$test_count][$newtest_count]['track'] != NULL && $combine_arr[$test_count][$newtest_count]['track'] !=" "){
															echo $combine_arr[$test_count][$newtest_count]['track'].".".stripslashes($combine_arr[$test_count][$newtest_count]['title']);
														}else{
															echo stripslashes($combine_arr[$test_count][$newtest_count]['title']);
														}
													}
													else{
													echo stripslashes($combine_arr[$test_count][$newtest_count]['title']); }
												}else{
												?>&nbsp;<?php
												}
													?></div>
										<?php
										if($combine_arr[$test_count][$newtest_count]['media_type']==114)
										{
										?>
											<div class="blkplayer"><div class="player">
												<a href="javascript:void(0);"><img onclick="showPlayer('a','<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','play');" onmouseout="this.src='images/profile/play.gif'" onmouseover="this.src='images/profile/play-over.gif'" src="images/profile/play.gif" title="Play file now."></a>
												<a href="javascript:void(0);"><img onclick="showPlayer('a','<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','add');" onmouseout="this.src='images/profile/add.gif'" onmouseover="this.src='images/profile/add-over.gif'" src="images/profile/add.gif" title="Add file to the bottom of your playlist without stopping current media."></a>
												<?php
												if(($combine_arr[$test_count][$newtest_count]['sharing_preference']==5 || $combine_arr[$test_count][$newtest_count]['sharing_preference']==2) && $combine_arr[$test_count][$newtest_count]['media_type'] == 114){
													?>
													<a onclick="download_directly_song('<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['title']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['creator']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['from']);?>')" href="javascript:void(0);">
													<img title="Add media to your cart for free download or purchase." onmouseout="this.src='../images/profile/download_down.gif'" onmouseover="this.src='../images/profile/download-over_down.gif'" src="../images/profile/download_down.gif"></a>
												<?php
												}/* else{
												?>
												<img src="images/profile/download-over_down.gif" />
												<?php
												} */
												?>
											</div>
											</div>
										<?php
										}else if($combine_arr[$test_count][$newtest_count]['media_type']==115)
										{
										?>
											<div class="blkplayer"><div class="player">
												<a href="javascript:void(0);"><img onclick="showPlayer('v','<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','playlist');" onmouseout="this.src='images/profile/play.gif'" onmouseover="this.src='images/profile/play-over.gif'" src="images/profile/play.gif" title="Play file now."></a>
												<a href="javascript:void(0);"><img onclick="showPlayer('v','<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','addvi');" onmouseout="this.src='images/profile/add.gif'" onmouseover="this.src='images/profile/add-over.gif'" src="images/profile/add.gif" title="Add file to the bottom of your playlist without stopping current media."></a>
												<?php
												if(($combine_arr[$test_count][$newtest_count]['sharing_preference']==5 || $combine_arr[$test_count][$newtest_count]['sharing_preference']==2) && $combine_arr[$test_count][$newtest_count]['media_type'] == 114){
													?>
													<a onclick="download_directly_song('<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['title']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['creator']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['from']);?>')" href="javascript:void(0);">
													<img title="Add media to your cart for free download or purchase." onmouseout="this.src='../images/profile/download_down.gif'" onmouseover="this.src='../images/profile/download-over_down.gif'" src="../images/profile/download_down.gif"></a>
												<?php
												}/* else{
												?>
												<img src="images/profile/download-over_down.gif" />
												<?php
												} */
												?>
											</div>
											</div>
										<?php
										}else if($combine_arr[$test_count][$newtest_count]['media_type']==113){
											$get_images_fan = $new_profile_media_classobj ->get_all_gallery_images($combine_arr[$test_count][$newtest_count]['id']);
											for($count_img=0;$count_img<=count($get_images_fan);$count_img++){
												if($get_all_img_fan ==""){
													$get_all_img_fan = $get_images_fan[$count_img]['image_name'];
												}else{
													$get_all_img_fan = $get_all_img_fan.",".$get_images_fan[$count_img]['image_name'];
												}
												if($galleryImagestitle_fan ==""){
													$galleryImagestitle_fan = $get_images_fan[$count_img]['image_title'];
												}else{
													$galleryImagestitle_fan = $galleryImagestitle_fan.",".$get_images_fan[$count_img]['image_title'];
												}
												$mediaSrc_fan = $get_images_fan[0]['image_name'];
											}
											$gal_id_fan = $get_images_fan[$count_img]['media_id'];
											$get_other_user_id = $user_keys_data->get_general_user_idby_mediaid($combine_arr[$test_count][$newtest_count]['id']);
											$orgPath_fan = $s3->getNewURI_otheruser("medgallery",$get_other_user_id);//"https://medgallery.s3.amazonaws.com/";
											$thumbPath_fan = $s3->getNewURI_otheruser("medgalthumb",$get_other_user_id);//"https://medgalthumb.s3.amazonaws.com/";
											
										?>
											<div class="blkplayer"><div class="player">
											<a href="javascript:void(0);"><img title="Play file now." src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" onclick="checkload('<?php echo $mediaSrc_fan;?>','<?php echo $get_all_img_fan; ?>','<?php echo $thumbPath_fan; ?>','<?php echo $orgPath_fan; ?>','<?php echo $galleryImagestitle_fan; ?>','<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>')"/></a>
											<!--<a href="javascript:void(0);"><img title="Add file to the bottom of your playlist without stopping current media." src="images/profile/add.gif"  onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'"/></a>-->
											<?php
												if(($combine_arr[$test_count][$newtest_count]['sharing_preference']==5 || $combine_arr[$test_count][$newtest_count]['sharing_preference']==2) && $combine_arr[$test_count][$newtest_count]['media_type'] == 114){
													?>
													<a onclick="download_directly_song('<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['title']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['creator']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['from']);?>')" href="javascript:void(0);">
													<img title="Add media to your cart for free download or purchase." onmouseout="this.src='../images/profile/download_down.gif'" onmouseover="this.src='../images/profile/download-over_down.gif'" src="../images/profile/download_down.gif"></a>
												<?php
												}/* else{
												?>
												<img src="images/profile/download-over_down.gif" />
												<?php
												} */
												?>
											</div>
											</div>
										<?php
											$get_all_img_fan ="";
											$galleryImagestitle_fan="";
											$mediaSrc_fan ="";
										}else if($combine_arr[$test_count][$newtest_count]['media_type'] == 116){
											$get_channel_media = $new_profile_media_classobj ->get_all_media_selected_channel($combine_arr[$test_count][$newtest_count]['id']);
										?>
										<div class="blkplayer"><div class="player">
											<input type="hidden" id="channel_media_song<?php echo $newtest_count;?>" value="<?php echo $get_channel_media['songs']; ?>"/>
											<input type="hidden" id="channel_media_video<?php echo $newtest_count;?>" value="<?php echo $get_channel_media['videos']; ?>"/>
											<a href="javascript:void(0);"><img title="Play file now." src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" onclick="PlayAll('channel_media_video<?php echo $newtest_count;?>','channel_media_song<?php echo $newtest_count;?>')"/></a>
											<a href="javascript:void(0);"><img title="Add file to the bottom of your playlist without stopping current media." src="images/profile/add.gif"  onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'" onclick="showPlayer('v','<?php if($combine_arr[$test_count][$newtest_count]['id']!=""){echo $combine_arr[$test_count][$newtest_count]['id'];}else{ echo $combine_arr[$test_count][$newtest_count]['id'];}?>','addvi');"/></a>
											<?php
												if(($combine_arr[$test_count][$newtest_count]['sharing_preference']==5 || $combine_arr[$test_count][$newtest_count]['sharing_preference']==2) && $combine_arr[$test_count][$newtest_count]['media_type'] == 114){
													?>
													<a onclick="download_directly_song('<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['title']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['creator']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['from']);?>')" href="javascript:void(0);">
													<img title="Add media to your cart for free download or purchase." onmouseout="this.src='../images/profile/download_down.gif'" onmouseover="this.src='../images/profile/download-over_down.gif'" src="../images/profile/download_down.gif"></a>
												<?php
												}/* else{
												?>
												<img src="images/profile/download-over_down.gif" />
												<?php
												} */
												?>
										</div>
										</div>
										<?php
										}
										else{
										?>
											<div class="blkplayer"><div class="player">
											<a href="javascript:void(0);"><img title="Play file now." src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" /></a>
											<a href="javascript:void(0);"><img title="Add file to the bottom of your playlist without stopping current media." src="images/profile/add.gif"  onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'"/></a>
											<?php
												if(($combine_arr[$test_count][$newtest_count]['sharing_preference']==5 || $combine_arr[$test_count][$newtest_count]['sharing_preference']==2) && $combine_arr[$test_count][$newtest_count]['media_type'] == 114){
													?>
													<a onclick="download_directly_song('<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['title']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['creator']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['from']);?>')" href="javascript:void(0);">
													<img title="Add media to your cart for free download or purchase." onmouseout="this.src='../images/profile/download_down.gif'" onmouseover="this.src='../images/profile/download-over_down.gif'" src="../images/profile/download_down.gif"></a>
												<?php
												}/* else{
												?>
												<img src="images/profile/download-over_down.gif" />
												<?php
												} */
												?>
											</div>
											</div>
										<?php
										}
										?>
										<div class="icon" onclick="runAccordion(<?php echo $combine_arr[$test_count][$newtest_count]['id']; ?>);">
											<div onselectstart="return false;"><img title="View details for media." src="images/profile/view.jpg" /></div>
										</div>
										<div class="icon">&nbsp;<!--<a href="add_media.php?edit=<?php// echo $combine_arr[$test_count][$newtest_count]['id'];?>"><img title="Edit media." src="images/profile/edit.png" /></a>--></div>
										<div class="icon"><a onclick="return confirmdelete('<?php echo $combine_arr[$test_count][$newtest_count]['title'];?>','<?php if($combine_arr[$test_count][$newtest_count]['name']=="Galleries/Images"){echo "Images";}else{echo $combine_arr[$test_count][$newtest_count]['name'];} ?>')" href="delete_media.php?id=<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>&whos_media=media_from_from"><img title="Remove media from your library." src="images/profile/delete.png" /></a></div>
										<div class="expandable" id="Accordion<?php echo $combine_arr[$test_count][$newtest_count]['id']; ?>Content">
											<div class="affiBlock">
												<div class="RowCont">
													<div class="sideL">Plays :</div>
													<div class="sideR"><a href="" target="_blank"><?php if($combine_arr[$test_count][$newtest_count]['play_count']!=""){ echo $combine_arr[$test_count][$newtest_count]['play_count'];}else{ echo "0";}?></a></div>
												</div>
												<?php
													if($combine_arr[$test_count][$newtest_count]['sharing_preference']==5 || $combine_arr[$test_count][$newtest_count]['sharing_preference']==2){
													?>
													<div class="RowCont">
														<div class="sideL">Downloads :</div>
														<div class="sideR"><a href="javascript:void(0);"><?php echo $combine_arr[$test_count][$newtest_count]['download_count'];?></a></div>
													</div>
													<?php
													}

													if($combine_arr[$test_count][$newtest_count]['media_type']=='113')
													{
													
													}
													else
													{
												?>	
												<!--<div class="RowCont">
													<div class="sideL">Media URL :</div>
													<div class="sideR"><a href="" target="_blank">www.<?php echo $domainname; ?></a></div>
												</div>-->
												<?php
													}
												?>
											</div>
										</div>
									</div>
									<?php
								}
								else if($combine_arr[$test_count][$newtest_count]['media_from']=="media_creator_from_tag")
								{
									$exp_deleted_id = explode(",",$getgeneral['deleted_media_by_tag_user']);
									if($newtest_count==0)
									{
										$newdel_count=1;
									}else{
									$newdel_count=$newtest_count;
									}
									if($exp_deleted_id[$newdel_count] != $combine_arr[$test_count][$newtest_count]['id'])
									{
									?>
										<div class="tableCont" id="AccordionContainer">
										<div class="blkD" style="width:60px;"><?php if($combine_arr[$test_count][$newtest_count]['name']=="Galleries/Images"){echo "Images";}else{echo $combine_arr[$test_count][$newtest_count]['name'];} ?></div>
										<div class="blkI" style="width:127px;"><?php
										$exp_creator_info = explode("|",$combine_arr[$test_count][$newtest_count]['creator_info']);
										$pro_url = $new_profile_media_classobj->get_creator_url($exp_creator_info[0],$exp_creator_info[1]);
										?>
										<a href="<?php if($pro_url['profile_url'] !=""){ echo "/".$pro_url['profile_url']; }else { echo "javascript:void(0)"; }?>">
										<?php if($combine_arr[$test_count][$newtest_count]['creator']!=""){
										//$endpos = strpos($combine_arr[$test_count][$newtest_count]['creator'],"(");
										//if($endpos !="" || $endpos !=false) {
										//echo substr($combine_arr[$test_count][$newtest_count]['creator'],0,$endpos);} else {
										echo $combine_arr[$test_count][$newtest_count]['creator']; }
										else{?>&nbsp;<?php } ?></a></div>
										<div class="blkK"><?php
										$exp_from_info = explode("|",$combine_arr[$test_count][$newtest_count]['from_info']);
										$pro_url_from = $new_profile_media_classobj->get_from_url($exp_from_info[0],$exp_from_info[1]);
										?>
										<a href="<?php if($pro_url_from['profile_url'] !=""){ echo "/".$pro_url_from['profile_url']; }else { echo "javascript:void(0)"; }?>">
										<?php if($combine_arr[$test_count][$newtest_count]['from']!=""){
										//$endpos = strpos($combine_arr[$test_count][$newtest_count]['from'],"(");
										//if($endpos !="" || $endpos != false) {
										//echo substr($combine_arr[$test_count][$newtest_count]['from'],0,$endpos);}else {
										echo $combine_arr[$test_count][$newtest_count]['from']; }
										else{?>&nbsp;<?php } ?></a></div>
										<div class="blkB"><?php
												if($combine_arr[$test_count][$newtest_count]['title']!="")
												{
													if($combine_arr[$test_count][$newtest_count]['media_type'] == 114)
													{
														//$get_media_track = $new_profile_media_classobj->get_media_track($combine_arr[$test_count][$newtest_count]['id']); 
														if($combine_arr[$test_count][$newtest_count]['track'] != "" && $combine_arr[$test_count][$newtest_count]['track'] != NULL && $combine_arr[$test_count][$newtest_count]['track'] !=" "){
															echo $combine_arr[$test_count][$newtest_count]['track'].".".stripslashes($combine_arr[$test_count][$newtest_count]['title']);
														}else{
															echo stripslashes($combine_arr[$test_count][$newtest_count]['title']);
														}
													}
													else{
													echo stripslashes($combine_arr[$test_count][$newtest_count]['title']); }
												}else{
												?>&nbsp;<?php
												}
													?></div>
										<?php
										if($combine_arr[$test_count][$newtest_count]['media_type']==114)
										{
										?>
											<div class="blkplayer"><div class="player">
												<a href="javascript:void(0);"><img onclick="showPlayer('a','<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','play');" onmouseout="this.src='images/profile/play.gif'" onmouseover="this.src='images/profile/play-over.gif'" src="images/profile/play.gif" title="Play file now."></a>
												<a href="javascript:void(0);"><img onclick="showPlayer('a','<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','add');" onmouseout="this.src='images/profile/add.gif'" onmouseover="this.src='images/profile/add-over.gif'" src="images/profile/add.gif" title="Add file to the bottom of your playlist without stopping current media."></a>
												<?php
												if(($combine_arr[$test_count][$newtest_count]['sharing_preference']==5 || $combine_arr[$test_count][$newtest_count]['sharing_preference']==2) && $combine_arr[$test_count][$newtest_count]['media_type'] == 114){
													?>
													<a onclick="download_directly_song('<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['title']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['creator']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['from']);?>')" href="javascript:void(0);">
													<img title="Add media to your cart for free download or purchase." onmouseout="this.src='../images/profile/download_down.gif'" onmouseover="this.src='../images/profile/download-over_down.gif'" src="../images/profile/download_down.gif"></a>
												<?php
												}/* else{
												?>
												<img src="images/profile/download-over_down.gif" />
												<?php
												} */
												?>
											</div>
											</div>
										<?php
										}else if($combine_arr[$test_count][$newtest_count]['media_type']==115)
										{
										?>
											<div class="blkplayer"><div class="player">
												<a href="javascript:void(0);"><img onclick="showPlayer('v','<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','playlist');" onmouseout="this.src='images/profile/play.gif'" onmouseover="this.src='images/profile/play-over.gif'" src="images/profile/play.gif" title="Play file now."></a>
												<a href="javascript:void(0);"><img onclick="showPlayer('v','<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','addvi');" onmouseout="this.src='images/profile/add.gif'" onmouseover="this.src='images/profile/add-over.gif'" src="images/profile/add.gif" title="Add file to the bottom of your playlist without stopping current media."></a>
												<?php
												if(($combine_arr[$test_count][$newtest_count]['sharing_preference']==5 || $combine_arr[$test_count][$newtest_count]['sharing_preference']==2) && $combine_arr[$test_count][$newtest_count]['media_type'] == 114){
													?>
													<a onclick="download_directly_song('<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['title']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['creator']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['from']);?>')" href="javascript:void(0);">
													<img title="Add media to your cart for free download or purchase." onmouseout="this.src='../images/profile/download_down.gif'" onmouseover="this.src='../images/profile/download-over_down.gif'" src="../images/profile/download_down.gif"></a>
												<?php
												}/* else{
												?>
												<img src="images/profile/download-over_down.gif" />
												<?php
												} */
												?>
											</div>
											</div>
										<?php
										}else if($combine_arr[$test_count][$newtest_count]['media_type']==113){
											$get_images_fan = $new_profile_media_classobj ->get_all_gallery_images($combine_arr[$test_count][$newtest_count]['id']);
											for($count_img=0;$count_img<=count($get_images_fan);$count_img++){
												if($get_all_img_fan ==""){
													$get_all_img_fan = $get_images_fan[$count_img]['image_name'];
												}else{
													$get_all_img_fan = $get_all_img_fan.",".$get_images_fan[$count_img]['image_name'];
												}
												if($galleryImagestitle_fan ==""){
													$galleryImagestitle_fan = $get_images_fan[$count_img]['image_title'];
												}else{
													$galleryImagestitle_fan = $galleryImagestitle_fan.",".$get_images_fan[$count_img]['image_title'];
												}
												$mediaSrc_fan = $get_images_fan[0]['image_name'];
											}
											$gal_id_fan = $get_images_fan[$count_img]['media_id'];
											$get_other_user_id = $user_keys_data->get_general_user_idby_mediaid($combine_arr[$test_count][$newtest_count]['id']);
											$orgPath_fan = $s3->getNewURI_otheruser("medgallery",$get_other_user_id);//"https://medgallery.s3.amazonaws.com/";
											$thumbPath_fan = $s3->getNewURI_otheruser("medgalthumb",$get_other_user_id);//"https://medgalthumb.s3.amazonaws.com/";
											
										?>
											<div class="blkplayer"><div class="player">
											<a href="javascript:void(0);"><img title="Play file now." src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" onclick="checkload('<?php echo $mediaSrc_fan;?>','<?php echo $get_all_img_fan; ?>','<?php echo $thumbPath_fan; ?>','<?php echo $orgPath_fan; ?>','<?php echo $galleryImagestitle_fan; ?>','<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>')"/></a>
											<!--<a href="javascript:void(0);"><img title="Add file to the bottom of your playlist without stopping current media." src="images/profile/add.gif"  onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'"/></a>-->
											<?php
												if(($combine_arr[$test_count][$newtest_count]['sharing_preference']==5 || $combine_arr[$test_count][$newtest_count]['sharing_preference']==2) && $combine_arr[$test_count][$newtest_count]['media_type'] == 114){
													?>
													<a onclick="download_directly_song('<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['title']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['creator']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['from']);?>')" href="javascript:void(0);">
													<img title="Add media to your cart for free download or purchase." onmouseout="this.src='../images/profile/download_down.gif'" onmouseover="this.src='../images/profile/download-over_down.gif'" src="../images/profile/download_down.gif"></a>
												<?php
												}/* else{
												?>
												<img src="images/profile/download-over_down.gif" />
												<?php
												} */
												?>
											</div>
											</div>
										<?php
											$get_all_img_fan ="";
											$galleryImagestitle_fan="";
											$mediaSrc_fan ="";
										}else if($combine_arr[$test_count][$newtest_count]['media_type'] == 116){
											$get_channel_media = $new_profile_media_classobj ->get_all_media_selected_channel($combine_arr[$test_count][$newtest_count]['id']);
										?>
										<div class="blkplayer"><div class="player">
											<input type="hidden" id="channel_media_song<?php echo $newtest_count;?>" value="<?php echo $get_channel_media['songs']; ?>"/>
											<input type="hidden" id="channel_media_video<?php echo $newtest_count;?>" value="<?php echo $get_channel_media['videos']; ?>"/>
											<a href="javascript:void(0);"><img title="Play file now." src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" onclick="PlayAll('channel_media_video<?php echo $newtest_count;?>','channel_media_song<?php echo $newtest_count;?>')"/></a>
											<a href="javascript:void(0);"><img title="Add file to the bottom of your playlist without stopping current media." src="images/profile/add.gif"  onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'" onclick="showPlayer('v','<?php if($combine_arr[$test_count][$newtest_count]['id']!=""){echo $combine_arr[$test_count][$newtest_count]['id'];}else{ echo $combine_arr[$test_count][$newtest_count]['id'];}?>','addvi');"/></a>
											<?php
												if(($combine_arr[$test_count][$newtest_count]['sharing_preference']==5 || $combine_arr[$test_count][$newtest_count]['sharing_preference']==2) && $combine_arr[$test_count][$newtest_count]['media_type'] == 114){
													?>
													<a onclick="download_directly_song('<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['title']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['creator']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['from']);?>')" href="javascript:void(0);">
													<img title="Add media to your cart for free download or purchase." onmouseout="this.src='../images/profile/download_down.gif'" onmouseover="this.src='../images/profile/download-over_down.gif'" src="../images/profile/download_down.gif"></a>
												<?php
												}/* else{
												?>
												<img src="images/profile/download-over_down.gif" />
												<?php
												} */
												?>
										</div>
										</div>
										<?php
										}
										else{
										?>
											<div class="blkplayer"><div class="player">
											<a href="javascript:void(0);"><img title="Play file now." src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" /></a>
											<a href="javascript:void(0);"><img title="Add file to the bottom of your playlist without stopping current media." src="images/profile/add.gif"  onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'"/></a>
											<?php
												if(($combine_arr[$test_count][$newtest_count]['sharing_preference']==5 || $combine_arr[$test_count][$newtest_count]['sharing_preference']==2) && $combine_arr[$test_count][$newtest_count]['media_type'] == 114){
													?>
													<a onclick="download_directly_song('<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['title']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['creator']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['from']);?>')" href="javascript:void(0);">
													<img title="Add media to your cart for free download or purchase." onmouseout="this.src='../images/profile/download_down.gif'" onmouseover="this.src='../images/profile/download-over_down.gif'" src="../images/profile/download_down.gif"></a>
												<?php
												}/* else{
												?>
												<img src="images/profile/download-over_down.gif" />
												<?php
												} */
												?>
											</div>
											</div>
										<?php
										}
										?>
										<div class="icon" onclick="runAccordion(<?php echo $combine_arr[$test_count][$newtest_count]['id']; ?>);">
											<div onselectstart="return false;"><img title="View details for media." src="images/profile/view.jpg" /></div>
										</div>
										<div class="icon">&nbsp;<!--<a href="add_media.php?edit=<?php// echo $combine_arr[$test_count][$newtest_count]['id'];?>"><img title="Edit media." src="images/profile/edit.png" /></a>--></div>
										<div class="icon"><a onclick="return confirmdelete('<?php echo $combine_arr[$test_count][$newtest_count]['title'];?>','<?php if($combine_arr[$test_count][$newtest_count]['name']=="Galleries/Images"){echo "Images";}else{echo $combine_arr[$test_count][$newtest_count]['name'];} ?>')" href="delete_media.php?id=<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>&whos_media=media_creator_from_tag"><img title="Remove media from your library." src="images/profile/delete.png" /></a></div>
										<div class="expandable" id="Accordion<?php echo $combine_arr[$test_count][$newtest_count]['id']; ?>Content">
											<div class="affiBlock">
												<div class="RowCont">
													<div class="sideL">Plays :</div>
													<div class="sideR"><a href="" target="_blank"><?php if($combine_arr[$test_count][$newtest_count]['play_count']!=""){ echo $combine_arr[$test_count][$newtest_count]['play_count'];}else{ echo "0";}?></a></div>
												</div>
													<?php
													if($combine_arr[$test_count][$newtest_count]['sharing_preference']==5 || $combine_arr[$test_count][$newtest_count]['sharing_preference']==2){
													?>
													<div class="RowCont">
														<div class="sideL">Downloads :</div>
														<div class="sideR"><a href="javascript:void(0);"><?php echo $combine_arr[$test_count][$newtest_count]['download_count'];?></a></div>
													</div>
													<?php
													}

													if($combine_arr[$test_count][$newtest_count]['media_type']=='113')
													{
																										
													}
													else
													{
												?>	
												<!--<div class="RowCont">
													<div class="sideL">Media URL :</div>
													<div class="sideR"><a href="" target="_blank">www.<?php echo $domainname; ?></a></div>
												</div>-->
												<?php
													}
												?>
											</div>
										</div>
									</div>
									<?php
									}
								}
								else if($combine_arr[$test_count][$newtest_count]['media_from']=="media_coming_fan")
								{
									$chk_tag_user = $new_profile_media_classobj->get_loggedin_info($_SESSION['login_email']);
									if($chk_tag_user['general_user_id']!=$combine_arr[$test_count][$newtest_count]['general_user_id'])
									{
										//$get_media_data = $new_profile_media_classobj->get_media_data($combine_arr[$test_count][$newtest_count]['media_id']);
										//if($get_media_data!="")
										//{
							?>
										<div class="tableCont" id="AccordionContainer">
											<div class="blkD" style="width:60px;"><?php if($combine_arr[$test_count][$newtest_count]['name']!=""){if($combine_arr[$test_count][$newtest_count]['name'] =="Galleries/Images"){echo "Images";}else{echo $combine_arr[$test_count][$newtest_count]['name'];}}else{?>&nbsp;<?php } ?></div>
											<div class="blkI" style="width:127px;"><?php
											$exp_creator_info = explode("|",$combine_arr[$test_count][$newtest_count]['creator_info']);
											$pro_url = $new_profile_media_classobj->get_creator_url($exp_creator_info[0],$exp_creator_info[1]);
											?>
											<a href="<?php if($pro_url['profile_url'] !=""){ echo "/".$pro_url['profile_url']; }else { echo "javascript:void(0)"; }?>">
											<?php if($combine_arr[$test_count][$newtest_count]['creator']!=""){
											//$endpos = strpos($combine_arr[$test_count][$newtest_count]['creator'],"(");
											//if($endpos !="" || $endpos !=false) {
											//echo substr($combine_arr[$test_count][$newtest_count]['creator'],0,$endpos);} else {
											echo $combine_arr[$test_count][$newtest_count]['creator']; }
											else{?>&nbsp;<?php } ?></a></div>
											<div class="blkK"><?php
											$exp_from_info = explode("|",$combine_arr[$test_count][$newtest_count]['from_info']);
											$pro_url_from = $new_profile_media_classobj->get_from_url($exp_from_info[0],$exp_from_info[1]);
											?>
											<a href="<?php if($pro_url_from['profile_url'] !=""){ echo "/".$pro_url_from['profile_url']; }else { echo "javascript:void(0)"; }?>">
											<?php if($combine_arr[$test_count][$newtest_count]['from']!=""){
											//$endpos = strpos($combine_arr[$test_count][$newtest_count]['from'],"(");
											//if($endpos !="" || $endpos != false) {
											//echo substr($combine_arr[$test_count][$newtest_count]['from'],0,$endpos);} else {
											echo $combine_arr[$test_count][$newtest_count]['from']; }
											else{?>&nbsp;<?php } ?></a></div>
											<div class="blkB"><?php
												if($combine_arr[$test_count][$newtest_count]['title']!="")
												{
													if($combine_arr[$test_count][$newtest_count]['media_type'] == 114)
													{
														//$get_media_track = $new_profile_media_classobj->get_media_track($combine_arr[$test_count][$newtest_count]['id']); 
														if($combine_arr[$test_count][$newtest_count]['track'] != "" && $combine_arr[$test_count][$newtest_count]['track'] != NULL && $combine_arr[$test_count][$newtest_count]['track'] !=" "){
															echo $combine_arr[$test_count][$newtest_count]['track'].".".stripslashes($combine_arr[$test_count][$newtest_count]['title']);
														}else{
															echo stripslashes($combine_arr[$test_count][$newtest_count]['title']);
														}
													}
													else{
													echo stripslashes($combine_arr[$test_count][$newtest_count]['title']); }
												}else{
												?>&nbsp;<?php
												}
													?></div>
											<div class="blkplayer"><div class="player">
											<?php 
												if($combine_arr[$test_count][$newtest_count]['media_type']==113){
													$get_images_fan = $new_profile_media_classobj ->get_all_gallery_images($combine_arr[$test_count][$newtest_count]['id']);
													for($count_img=0;$count_img<=count($get_images_fan);$count_img++){
														if($get_all_img_fan==""){
															$get_all_img_fan = $get_images_fan[$count_img]['image_name'];
														}else{
															$get_all_img_fan = $get_all_img_fan.",".$get_images_fan[$count_img]['image_name'];
														}
														if($galleryImagestitle_fan==""){
															$galleryImagestitle_fan = $get_images_fan[$count_img]['image_title'];
														}else{
															$galleryImagestitle_fan = $galleryImagestitle_fan.",".$get_images_fan[$count_img]['image_title'];
														}
														$mediaSrc_fan = $get_images_fan[0]['image_name'];
													}
													$gal_id_fan = $get_images_fan[$count_img]['media_id'];
													$get_other_user_id = $user_keys_data->get_general_user_idby_mediaid($combine_arr[$test_count][$newtest_count]['id']);
													$orgPath_fan = $s3->getNewURI_otheruser("medgallery",$get_other_user_id);//"https://medgallery.s3.amazonaws.com/";
													$thumbPath_fan = $s3->getNewURI_otheruser("medgalthumb",$get_other_user_id);//"https://medgalthumb.s3.amazonaws.com/";
											
												?>
												<a href="javascript:void(0);"><img title="Play file now." src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" onclick="checkload('<?php echo $mediaSrc_fan;?>','<?php echo $get_all_img_fan; ?>','<?php echo $thumbPath_fan; ?>','<?php echo $orgPath_fan; ?>','<?php echo $galleryImagestitle_fan; ?>','<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>')"/></a>
												<!--<a href="javascript:void(0);"><img title="Add file to the bottom of your playlist without stopping current media." src="images/profile/add.gif"  onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'"/></a>-->
												<?php
													$get_all_img_fan ="";
													$galleryImagestitle_fan="";
													$mediaSrc_fan ="";
												}else if($combine_arr[$test_count][$newtest_count]['media_type']==114){
												?>
												<a href="javascript:void(0);"><img title="Play file now." src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" onclick="showPlayer('a','<?php if($combine_arr[$test_count][$newtest_count]['id']!=""){echo $combine_arr[$test_count][$newtest_count]['id'];}else{echo $combine_arr[$test_count][$newtest_count]['id'];}?>','play');"/></a>
												<a href="javascript:void(0);"><img title="Add file to the bottom of your playlist without stopping current media." src="images/profile/add.gif"  onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'" onclick="showPlayer('a','<?php if($combine_arr[$test_count][$newtest_count]['id']!=""){echo $combine_arr[$test_count][$newtest_count]['id'];}else{echo $combine_arr[$test_count][$newtest_count]['id'];}?>','add');"/></a>
												<?php
												}else if($combine_arr[$test_count][$newtest_count]['media_type']==115){
												?>
												<a href="javascript:void(0);"><img title="Play file now." src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" onclick="showPlayer('v','<?php if($combine_arr[$test_count][$newtest_count]['id']!=""){echo $combine_arr[$test_count][$newtest_count]['id'];}else{ echo $combine_arr[$test_count][$newtest_count]['id'];}?>','playlist');"/></a>
												<a href="javascript:void(0);"><img title="Add file to the bottom of your playlist without stopping current media." src="images/profile/add.gif"  onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'" onclick="showPlayer('v','<?php if($combine_arr[$test_count][$newtest_count]['id']!=""){echo $combine_arr[$test_count][$newtest_count]['id'];}else{ echo $combine_arr[$test_count][$newtest_count]['id'];}?>','addvi');"/></a>
												<?php
												}else if($combine_arr[$test_count][$newtest_count]['media_type'] == 116){
												$get_channel_media = $new_profile_media_classobj ->get_all_media_selected_channel($combine_arr[$test_count][$newtest_count]['id']);
												?>
													<input type="hidden" id="channel_media_song<?php echo $newtest_count;?>" value="<?php echo $get_channel_media['songs']; ?>"/>
													<input type="hidden" id="channel_media_video<?php echo $newtest_count;?>" value="<?php echo $get_channel_media['videos']; ?>"/>
													<a href="javascript:void(0);"><img title="Play file now." src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" onclick="PlayAll('channel_media_video<?php echo $newtest_count;?>','channel_media_song<?php echo $newtest_count;?>')"/></a>
													<a href="javascript:void(0);"><img title="Add file to the bottom of your playlist without stopping current media." src="images/profile/add.gif"  onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'" onclick="showPlayer('v','<?php if($combine_arr[$test_count][$newtest_count]['id']!=""){echo $combine_arr[$test_count][$newtest_count]['id'];}else{ echo $combine_arr[$test_count][$newtest_count]['id'];}?>','addvi');"/></a>
												<?php
												}
												else{
												?>
												<a href="javascript:void(0);"><img title="Play file now." src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" /></a>
												<a href="javascript:void(0);"><img title="Add file to the bottom of your playlist without stopping current media." src="images/profile/add.gif"  onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'"/></a>
												<?php
												}
												if(($combine_arr[$test_count][$newtest_count]['sharing_preference']==5 || $combine_arr[$test_count][$newtest_count]['sharing_preference']==2) && $combine_arr[$test_count][$newtest_count]['media_type'] == 114){
													?>
													<a onclick="download_directly_song('<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['title']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['creator']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['from']);?>')" href="javascript:void(0);">
													<img title="Add media to your cart for free download or purchase." onmouseout="this.src='../images/profile/download_down.gif'" onmouseover="this.src='../images/profile/download-over_down.gif'" src="../images/profile/download_down.gif"></a>
												<?php
												}/* else{
												?>
												<img src="images/profile/download-over_down.gif" />
												<?php
												} */
												?>
											</div>
											</div>
											<div class="icon" onclick="runAccordion(<?php echo $combine_arr[$test_count][$newtest_count]['id']; ?>);">
											<div onselectstart="return false;"><img title="View details for media." src="images/profile/view.jpg" /></div>
										</div>
										<div class="icon">&nbsp;<!--<a href="add_media.php?edit=<?php// echo $combine_arr[$test_count][$newtest_count]['id'];?>"><img title="Edit media." src="images/profile/edit.png" /></a>--></div>
										<div class="icon"><a onclick="return confirmdelete('<?php echo $combine_arr[$test_count][$newtest_count]['title'];?>','<?php if($combine_arr[$test_count][$newtest_count]['name']=="Galleries/Images"){echo "Images";}else{echo $combine_arr[$test_count][$newtest_count]['name'];} ?>')" href="delete_media.php?id=<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>&whos_media=media_coming_fan"><img title="Remove media from your library." src="images/profile/delete.png" /></a></div>
											<div class="expandable" id="Accordion<?php echo $combine_arr[$test_count][$newtest_count]['id']; ?>Content">
												<div class="affiBlock">
													<div class="RowCont">
														<div class="sideL">Plays :</div>
														<div class="sideR"><a href="" target="_blank"><?php if($combine_arr[$test_count][$newtest_count]['play_count']!=""){ echo $combine_arr[$test_count][$newtest_count]['play_count'];}else{ echo "0";}?></a></div>
													</div>
													<?php
														if($combine_arr[$test_count][$newtest_count]['sharing_preference']==5 || $combine_arr[$test_count][$newtest_count]['sharing_preference']==2){
														?>
														<div class="RowCont">
															<div class="sideL">Downloads :</div>
															<div class="sideR"><a href="javascript:void(0);"><?php echo $combine_arr[$test_count][$newtest_count]['download_count'];?></a></div>
														</div>
														<?php
														}
														if($combine_arr[$test_count][$newtest_count]['media_type']=='113')
														{
																											
														}
														else
														{
													?>	
													<!--<div class="RowCont">
														<div class="sideL">Media URL :</div>
														<div class="sideR"><a href="" target="_blank">www.<?php echo $domainname; ?></a></div>
													</div>-->
													<?php
														}
													?>
												</div>
											</div>
										</div>
							<?php
										//}
									}
								}
								else if($combine_arr[$test_count][$newtest_count]['media_from']=="media_coming_buyed")
								{
									$chk_tag_user = $new_profile_media_classobj->get_loggedin_info($_SESSION['login_email']);
									if($chk_tag_user['general_user_id']!=$combine_arr[$test_count][$newtest_count]['general_user_id'])
									{
							?>
										<div class="tableCont" id="AccordionContainer">
											<div class="blkD" style="width:60px;"><?php if($combine_arr[$test_count][$newtest_count]['name']!=""){if($combine_arr[$test_count][$newtest_count]['name'] =="Galleries/Images"){echo "Images";}else{echo $combine_arr[$test_count][$newtest_count]['name'];}}else{?>&nbsp;<?php } ?></div>
											<div class="blkI" style="width:127px;"><?php
											$exp_creator_info = explode("|",$combine_arr[$test_count][$newtest_count]['creator_info']);
											$pro_url = $new_profile_media_classobj->get_creator_url($exp_creator_info[0],$exp_creator_info[1]);
											?>
											<a href="<?php if($pro_url['profile_url'] !=""){ echo "/".$pro_url['profile_url']; }else { echo "javascript:void(0)"; }?>">
											<?php if($combine_arr[$test_count][$newtest_count]['creator']!=""){
											//$endpos = strpos($combine_arr[$test_count][$newtest_count]['creator'],"(");
											//if($endpos !="" || $endpos !=false) {
											//echo substr($combine_arr[$test_count][$newtest_count]['creator'],0,$endpos);} else {
											echo $combine_arr[$test_count][$newtest_count]['creator']; }
											else{?>&nbsp;<?php } ?></a></div>
											<div class="blkK"><?php
											$exp_from_info = explode("|",$combine_arr[$test_count][$newtest_count]['from_info']);
											$pro_url_from = $new_profile_media_classobj->get_from_url($exp_from_info[0],$exp_from_info[1]);
											?>
											<a href="<?php if($pro_url_from['profile_url'] !=""){ echo "/".$pro_url_from['profile_url']; }else { echo "javascript:void(0)"; }?>">
											<?php if($combine_arr[$test_count][$newtest_count]['from']!=""){
											//$endpos = strpos($combine_arr[$test_count][$newtest_count]['from'],"(");
											//if($endpos !="" || $endpos != false) {
											//echo substr($combine_arr[$test_count][$newtest_count]['from'],0,$endpos);} else {
											echo $combine_arr[$test_count][$newtest_count]['from']; }
											else{?>&nbsp;<?php } ?></a></div>
											<div class="blkB"><?php
												if($combine_arr[$test_count][$newtest_count]['title']!="")
												{
													if($combine_arr[$test_count][$newtest_count]['media_type'] == 114)
													{
														//$get_media_track = $new_profile_media_classobj->get_media_track($combine_arr[$test_count][$newtest_count]['id']); 
														if($combine_arr[$test_count][$newtest_count]['track'] != "" && $combine_arr[$test_count][$newtest_count]['track'] != NULL && $combine_arr[$test_count][$newtest_count]['track'] !=" "){
															echo $combine_arr[$test_count][$newtest_count]['track'].".".stripslashes($combine_arr[$test_count][$newtest_count]['title']);
														}else{
															echo stripslashes($combine_arr[$test_count][$newtest_count]['title']);
														}
													}
													else{
													echo stripslashes($combine_arr[$test_count][$newtest_count]['title']); }
												}else{
												?>&nbsp;<?php
												}
													?></div>
											<div class="blkplayer"><div class="player">
											<?php 
												if($combine_arr[$test_count][$newtest_count]['media_type']==113){
													$get_images_fan = $new_profile_media_classobj ->get_all_gallery_images($combine_arr[$test_count][$newtest_count]['id']);
													for($count_img=0;$count_img<=count($get_images_fan);$count_img++){
														if($get_all_img_fan ==""){
															$get_all_img_fan = $get_images_fan[$count_img]['image_name'];
														}else{
															$get_all_img_fan = $get_all_img_fan.",".$get_images_fan[$count_img]['image_name'];
														}
														if($galleryImagestitle_fan ==""){
															$galleryImagestitle_fan = $get_images_fan[$count_img]['image_title'];
														}else{
															$galleryImagestitle_fan = $galleryImagestitle_fan.",".$get_images_fan[$count_img]['image_title'];
														}
														$mediaSrc_fan = $get_images_fan[0]['image_name'];
													}
													$gal_id_fan = $get_images_fan[$count_img]['media_id'];
													$get_other_user_id = $user_keys_data->get_general_user_idby_mediaid($combine_arr[$test_count][$newtest_count]['id']);
													$orgPath_fan = $s3->getNewURI_otheruser("medgallery",$get_other_user_id);//"https://medgallery.s3.amazonaws.com/";
													$thumbPath_fan = $s3->getNewURI_otheruser("medgalthumb",$get_other_user_id);//"https://medgalthumb.s3.amazonaws.com/";
													
												?>
												<a href="javascript:void(0);"><img title="Play file now." src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" onclick="checkload('<?php echo $mediaSrc_fan;?>','<?php echo $get_all_img_fan; ?>','<?php echo $thumbPath_fan; ?>','<?php echo $orgPath_fan; ?>','<?php echo $galleryImagestitle_fan; ?>','<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>')"/></a>
												<!--<a href="javascript:void(0);"><img title="Add file to the bottom of your playlist without stopping current media." src="images/profile/add.gif"  onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'"/></a>-->
												<?php
													$get_all_img_fan ="";
													$galleryImagestitle_fan="";
													$mediaSrc_fan ="";
												}else if($combine_arr[$test_count][$newtest_count]['media_type']==114){
												?>
												<a href="javascript:void(0);"><img title="Play file now." src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" onclick="showPlayer('a','<?php if($combine_arr[$test_count][$newtest_count]['id']!=""){echo $combine_arr[$test_count][$newtest_count]['id'];}else{echo $combine_arr[$test_count][$newtest_count]['id'];}?>','play');"/></a>
												<a href="javascript:void(0);"><img title="Add file to the bottom of your playlist without stopping current media." src="images/profile/add.gif"  onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'" onclick="showPlayer('a','<?php if($combine_arr[$test_count][$newtest_count]['id']!=""){echo $combine_arr[$test_count][$newtest_count]['id'];}else{echo $combine_arr[$test_count][$newtest_count]['id'];}?>','add');"/></a>
												<?php
												}else if($combine_arr[$test_count][$newtest_count]['media_type']==115){
												?>
												<a href="javascript:void(0);"><img title="Play file now." src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" onclick="showPlayer('v','<?php if($combine_arr[$test_count][$newtest_count]['id']!=""){echo $combine_arr[$test_count][$newtest_count]['id'];}else{ echo $combine_arr[$test_count][$newtest_count]['id'];}?>','playlist');"/></a>
												<a href="javascript:void(0);"><img title="Add file to the bottom of your playlist without stopping current media." src="images/profile/add.gif"  onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'" onclick="showPlayer('v','<?php if($combine_arr[$test_count][$newtest_count]['id']!=""){echo $combine_arr[$test_count][$newtest_count]['id'];}else{ echo $combine_arr[$test_count][$newtest_count]['id'];}?>','addvi');"/></a>
												<?php
												}else if($combine_arr[$test_count][$newtest_count]['media_type'] == 116){
													$get_channel_media = $new_profile_media_classobj ->get_all_media_selected_channel($combine_arr[$test_count][$newtest_count]['id']);
												?>
													<input type="hidden" id="channel_media_song<?php echo $newtest_count;?>" value="<?php echo $get_channel_media['songs']; ?>"/>
													<input type="hidden" id="channel_media_video<?php echo $newtest_count;?>" value="<?php echo $get_channel_media['videos']; ?>"/>
													<a href="javascript:void(0);"><img title="Play file now." src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" onclick="PlayAll('channel_media_video<?php echo $newtest_count;?>','channel_media_song<?php echo $newtest_count;?>')"/></a>
													<a href="javascript:void(0);"><img title="Add file to the bottom of your playlist without stopping current media." src="images/profile/add.gif"  onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'" onclick="showPlayer('v','<?php if($combine_arr[$test_count][$newtest_count]['id']!=""){echo $combine_arr[$test_count][$newtest_count]['id'];}else{ echo $combine_arr[$test_count][$newtest_count]['id'];}?>','addvi');"/></a>
												<?php
												}
												else{
												?>
												<a href="javascript:void(0);"><img title="Play file now." src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" /></a>
												<a href="javascript:void(0);"><img title="Add file to the bottom of your playlist without stopping current media." src="images/profile/add.gif"  onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'"/></a>
												<?php
												}
												
												if(($combine_arr[$test_count][$newtest_count]['sharing_preference']==5 || $combine_arr[$test_count][$newtest_count]['sharing_preference']==2) && $combine_arr[$test_count][$newtest_count]['media_type'] == 114){
													?>
													<a onclick="download_directly_song('<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['title']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['creator']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['from']);?>')" href="javascript:void(0);">
													<img title="Add media to your cart for free download or purchase." onmouseout="this.src='../images/profile/download_down.gif'" onmouseover="this.src='../images/profile/download-over_down.gif'" src="../images/profile/download_down.gif"></a>
												<?php
												}/* else{
												?>
												<img src="images/profile/download-over_down.gif" />
												<?php
												} */
												?>
											</div>
											</div>
										<div class="icon" onclick="runAccordion(<?php echo $combine_arr[$test_count][$newtest_count]['id']; ?>);">
											<div onselectstart="return false;"><img title="View details for media." src="images/profile/view.jpg" /></div>
										</div>
										<div class="icon">&nbsp;<!--<a href="add_media.php?edit=<?php// echo $combine_arr[$test_count][$newtest_count]['id'];?>"><img title="Edit media." src="images/profile/edit.png" /></a>--></div>
										<div class="icon"><a onclick="return confirmdelete('<?php echo $combine_arr[$test_count][$newtest_count]['title'];?>','<?php if($combine_arr[$test_count][$newtest_count]['name']=="Galleries/Images"){echo "Images";}else{echo $combine_arr[$test_count][$newtest_count]['name'];} ?>')" href="delete_media.php?id=<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>&whos_media=media_coming_buyed"><img title="Remove media from your library." src="images/profile/delete.png" /></a></div>
											<div class="expandable" id="Accordion<?php echo $combine_arr[$test_count][$newtest_count]['id']; ?>Content">
												<div class="affiBlock">
													<div class="RowCont">
														<div class="sideL">Plays :</div>
														<div class="sideR"><a href="" target="_blank"><?php if($combine_arr[$test_count][$newtest_count]['play_count']!=""){ echo $combine_arr[$test_count][$newtest_count]['play_count'];}else{ echo "0";}?></a></div>
													</div>
													<?php
														if($combine_arr[$test_count][$newtest_count]['sharing_preference']==5 || $combine_arr[$test_count][$newtest_count]['sharing_preference']==2){
														?>
														<div class="RowCont">
															<div class="sideL">Downloads :</div>
															<div class="sideR"><a href="javascript:void(0);"><?php echo $combine_arr[$test_count][$newtest_count]['download_count'];?></a></div>
														</div>
														<?php
														}
														if($combine_arr[$test_count][$newtest_count]['media_type']=='113')
														{
																											
														}
														else
														{
													?>	
													<!--<div class="RowCont">
														<div class="sideL">Media URL :</div>
														<div class="sideR"><a href="" target="_blank">www.<?php echo $domainname; ?></a></div>
													</div>-->
													<?php
														}
													?>
												</div>
											</div>
										</div>
							<?php
									}
								}
								
								else if($combine_arr[$test_count][$newtest_count]['media_from']=="all_tagging_media")
								{
									//$get_media_tag_g_art = $new_profile_media_classobj->get_tag_media_info($combine_arr[$test_count][$newtest_count]);
									$chk_tag_user = $new_profile_media_classobj->get_loggedin_info($_SESSION['login_email']);
									if($chk_tag_user['general_user_id']!=$combine_arr[$test_count][$newtest_count]['general_user_id'])
									{
										if($combine_arr[$test_count][$newtest_count]['id']!=""){
											?>
											<div class="tableCont" id="AccordionContainer">
												<div class="blkD" style="width:60px;"><?php if($combine_arr[$test_count][$newtest_count]['name']!=""){if($combine_arr[$test_count][$newtest_count]['name'] =="Galleries/Images"){echo "Images";}else{echo $combine_arr[$test_count][$newtest_count]['name'];}}else{?>&nbsp;<?php }?></div>
												<div class="blkI" style="width:127px;"><?php
												$exp_creator_info = explode("|",$combine_arr[$test_count][$newtest_count]['creator_info']);
												$pro_url = $new_profile_media_classobj->get_creator_url($exp_creator_info[0],$exp_creator_info[1]);
												?>
												<a href="<?php if($pro_url['profile_url'] !=""){ echo "/".$pro_url['profile_url']; }else { echo "javascript:void(0)"; }?>">
												<?php if($combine_arr[$test_count][$newtest_count]['creator']!=""){
												//$endpos = strpos($combine_arr[$test_count][$newtest_count]['creator'],"(");
												//if($endpos !="" || $endpos !=false) {
												//echo substr($combine_arr[$test_count][$newtest_count]['creator'],0,$endpos);} else {
												echo $combine_arr[$test_count][$newtest_count]['creator']; }
												else{?>&nbsp;<?php } ?></a></div>
												<div class="blkK"><?php
												$exp_from_info = explode("|",$combine_arr[$test_count][$newtest_count]['from_info']);
												$pro_url_from = $new_profile_media_classobj->get_from_url($exp_from_info[0],$exp_from_info[1]);
												?>
												<a href="<?php if($pro_url_from['profile_url'] !=""){ echo "/".$pro_url_from['profile_url']; }else { echo "javascript:void(0)"; }?>">
												<?php if($combine_arr[$test_count][$newtest_count]['from']!=""){
												//$endpos = strpos($combine_arr[$test_count][$newtest_count]['from'],"(");
												//if($endpos !="" || $endpos != false) {
												//echo substr($combine_arr[$test_count][$newtest_count]['from'],0,$endpos);} else {
												echo $combine_arr[$test_count][$newtest_count]['from']; }
												else{?>&nbsp;<?php } ?></a></div>
												<div class="blkB"><?php
												if($combine_arr[$test_count][$newtest_count]['title']!="")
												{
													if($combine_arr[$test_count][$newtest_count]['media_type'] == 114)
													{
														if($combine_arr[$test_count][$newtest_count]['track'] == ""){
														echo stripslashes($combine_arr[$test_count][$newtest_count]['title']);}else{
														echo $combine_arr[$test_count][$newtest_count]['track'].".".stripslashes($combine_arr[$test_count][$newtest_count]['title']);
														}
													}
													else{
													echo stripslashes($combine_arr[$test_count][$newtest_count]['title']); }
												}else{
												?>&nbsp;<?php
												}
													?>
												</div>
												<?php
												if($combine_arr[$test_count][$newtest_count]['media_type']==114){
												?>
												<div class="blkplayer"><div class="player">
												<a href="javascript:void(0);"><img title="Play file now." src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" onclick="showPlayer('a','<?php if($combine_arr[$test_count][$newtest_count]['id']!=""){echo $combine_arr[$test_count][$newtest_count]['id'];}else{echo $combine_arr[$test_count][$newtest_count]['id'];}?>','play');"/></a>
												<a href="javascript:void(0);"><img title="Add file to the bottom of your playlist without stopping current media." src="images/profile/add.gif"  onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'" onclick="showPlayer('a','<?php if($combine_arr[$test_count][$newtest_count]['id']!=""){echo $combine_arr[$test_count][$newtest_count]['id'];}else{echo $combine_arr[$test_count][$newtest_count]['id'];}?>','add');"/></a>
												<?php
												if(($combine_arr[$test_count][$newtest_count]['sharing_preference']==5 || $combine_arr[$test_count][$newtest_count]['sharing_preference']==2) && $combine_arr[$test_count][$newtest_count]['media_type'] == 114){
													?>
													<a onclick="download_directly_song('<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['title']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['creator']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['from']);?>')" href="javascript:void(0);">
													<img title="Add media to your cart for free download or purchase." onmouseout="this.src='../images/profile/download_down.gif'" onmouseover="this.src='../images/profile/download-over_down.gif'" src="../images/profile/download_down.gif"></a>
												<?php
												}/* else{
												?>
												<img src="images/profile/download-over_down.gif" />
												<?php
												} */
												?>
												</div>
												</div>
												<?php
												}else if($combine_arr[$test_count][$newtest_count]['media_type']==115){
												?>
												<div class="blkplayer"><div class="player">
												<a href="javascript:void(0);"><img title="Play file now." src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" onclick="showPlayer('v','<?php if($combine_arr[$test_count][$newtest_count]['id']!=""){echo $combine_arr[$test_count][$newtest_count]['id'];}else{echo $combine_arr[$test_count][$newtest_count]['id'];}?>','playlist');"/></a>
												<a href="javascript:void(0);"><img title="Add file to the bottom of your playlist without stopping current media." src="images/profile/add.gif"  onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'" onclick="showPlayer('v','<?php if($combine_arr[$test_count][$newtest_count]['id']!=""){echo $combine_arr[$test_count][$newtest_count]['id'];}else{echo $combine_arr[$test_count][$newtest_count]['id'];}?>','addvi');"/></a>
												<?php
												if(($combine_arr[$test_count][$newtest_count]['sharing_preference']==5 || $combine_arr[$test_count][$newtest_count]['sharing_preference']==2) && $combine_arr[$test_count][$newtest_count]['media_type'] == 114){
													?>
													<a onclick="download_directly_song('<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['title']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['creator']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['from']);?>')" href="javascript:void(0);">
													<img title="Add media to your cart for free download or purchase." onmouseout="this.src='../images/profile/download_down.gif'" onmouseover="this.src='../images/profile/download-over_down.gif'" src="../images/profile/download_down.gif"></a>
												<?php
												}/* else{
												?>
												<img src="images/profile/download-over_down.gif" />
												<?php
												} */
												?>
												</div>
												</div>
												<?php
												}else if($combine_arr[$test_count][$newtest_count]['media_type']==113){
												?>
												<div class="blkplayer"><div class="player">
												<?php 
													$get_images_tag_pro_cre = $new_profile_media_classobj ->get_all_gallery_images($combine_arr[$test_count][$newtest_count]['id']);
													for($count_img_tag_pro_cre=0;$count_img_tag_pro_cre<=count($get_images_tag_pro_cre);$count_img_tag_pro_cre++){
														if($get_all_img_tag_pro_cre ==""){	
															$get_all_img_tag_pro_cre = $get_images_tag_pro_cre[$count_img_tag_pro_cre]['image_name'];
														}else{
															$get_all_img_tag_pro_cre = $get_all_img_tag_pro_cre.",".$get_images_tag_pro_cre[$count_img_tag_pro_cre]['image_name'];
														}
														if($galleryImagestitle_tag_pro_cre ==""){
															$galleryImagestitle_tag_pro_cre= $get_images_tag_pro_cre[$count_img_tag_pro_cre]['image_title'];
														}else{
															$galleryImagestitle_tag_pro_cre= $galleryImagestitle_tag_pro_cre.",".$get_images_tag_pro_cre[$count_img_tag_pro_cre]['image_title'];
														}
														$mediaSrc_tag_pro_cre = $get_images_tag_pro_cre[0]['image_name'];
													}
													$gal_id_tag_pro_cre = $get_images_tag_pro_cre[$count_img_tag_pro_cre]['media_id'];
													$get_other_user_id = $user_keys_data->get_general_user_idby_mediaid($combine_arr[$test_count][$newtest_count]['id']);
													$orgPath_tag_pro_cre = $s3->getNewURI_otheruser("medgallery",$get_other_user_id);//"https://medgallery.s3.amazonaws.com/";
													$thumbPath_tag_pro_cre = $s3->getNewURI_otheruser("medgalthumb",$get_other_user_id);//"https://medgalthumb.s3.amazonaws.com/";
													
												?>
												<a href="javascript:void(0);"><img src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" onclick="checkload('<?php echo $mediaSrc_tag_pro_cre;?>','<?php echo $get_all_img_tag_pro_cre; ?>','<?php echo $thumbPath_tag_pro_cre; ?>','<?php echo $orgPath_tag_pro_cre; ?>','<?php echo $galleryImagestitle_tag_pro_cre; ?>','<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>')"/></a>
												<!--<a href="javascript:void(0);"><img src="images/profile/add.gif"  onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'"/></a>-->
												<?php
													$get_all_img_tag_pro_cre ="";
													$galleryImagestitle_tag_pro_cre="";
													$mediaSrc_tag_pro_cre ="";
												?>
												<?php
												if(($combine_arr[$test_count][$newtest_count]['sharing_preference']==5 || $combine_arr[$test_count][$newtest_count]['sharing_preference']==2) && $combine_arr[$test_count][$newtest_count]['media_type'] == 114){
													?>
													<a onclick="download_directly_song('<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['title']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['creator']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['from']);?>')" href="javascript:void(0);">
													<img title="Add media to your cart for free download or purchase." onmouseout="this.src='../images/profile/download_down.gif'" onmouseover="this.src='../images/profile/download-over_down.gif'" src="../images/profile/download_down.gif"></a>
												<?php
												}/* else{
												?>
												<img src="images/profile/download-over_down.gif" />
												<?php
												} */
												?>
												</div>
												</div>
											<?php
												}else if($combine_arr[$test_count][$newtest_count]['media_type'] == 116){
													$get_channel_media = $new_profile_media_classobj ->get_all_media_selected_channel($combine_arr[$test_count][$newtest_count]['id']);
												?>
												<div class="blkplayer"><div class="player">
													<input type="hidden" id="channel_media_song<?php echo $newtest_count;?>" value="<?php echo $get_channel_media['songs']; ?>"/>
													<input type="hidden" id="channel_media_video<?php echo $newtest_count;?>" value="<?php echo $get_channel_media['videos']; ?>"/>
													<a href="javascript:void(0);"><img title="Play file now." src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" onclick="PlayAll('channel_media_video<?php echo $newtest_count;?>','channel_media_song<?php echo $newtest_count;?>')"/></a>
													<a href="javascript:void(0);"><img title="Add file to the bottom of your playlist without stopping current media." src="images/profile/add.gif"  onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'" onclick="showPlayer('v','<?php if($combine_arr[$test_count][$newtest_count]['id']!=""){echo $combine_arr[$test_count][$newtest_count]['id'];}else{ echo $combine_arr[$test_count][$newtest_count]['id'];}?>','addvi');"/></a>
													<?php
												if(($combine_arr[$test_count][$newtest_count]['sharing_preference']==5 || $combine_arr[$test_count][$newtest_count]['sharing_preference']==2) && $combine_arr[$test_count][$newtest_count]['media_type'] == 114){
													?>
													<a onclick="download_directly_song('<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['title']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['creator']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['from']);?>')" href="javascript:void(0);">
													<img title="Add media to your cart for free download or purchase." onmouseout="this.src='../images/profile/download_down.gif'" onmouseover="this.src='../images/profile/download-over_down.gif'" src="../images/profile/download_down.gif"></a>
												<?php
												}/* else{
												?>
												<img src="images/profile/download-over_down.gif" />
												<?php
												} */
												?>
												</div>
												</div>
												<?php
												}
												else
												{
												?>
												<div class="blkplayer"><div class="player">
													<a href="javascript:void(0);"><img src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" /></a>
													<a href="javascript:void(0);"><img src="images/profile/add.gif"  onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'" /></a>
													<?php
												if(($combine_arr[$test_count][$newtest_count]['sharing_preference']==5 || $combine_arr[$test_count][$newtest_count]['sharing_preference']==2) && $combine_arr[$test_count][$newtest_count]['media_type'] == 114){
													?>
													<a onclick="download_directly_song('<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['title']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['creator']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['from']);?>')" href="javascript:void(0);">
													<img title="Add media to your cart for free download or purchase." onmouseout="this.src='../images/profile/download_down.gif'" onmouseover="this.src='../images/profile/download-over_down.gif'" src="../images/profile/download_down.gif"></a>
												<?php
												}/* else{
												?>
												<img src="images/profile/download-over_down.gif" />
												<?php
												} */
												?>
												</div>
												</div>
												<?php
												}
											?>
											</div>
											<?php
										}
									}
								}
								
							//}
							}
							?>
							<script type="text/javascript">             
								$(document).ready(function() {
									$("#gallery_images<?php echo $newtest_count; ?>").fancybox({
				helpers : {
					media : {},
					buttons : {},
					title : null
				}
			});

								
							});
							</script>

							<script type="text/javascript">             
							$(document).ready(function() {
										$("#Add_gallery_images<?php echo $newtest_count; ?>").fancybox({
															helpers : {
											media : {},
											buttons : {},
											title : null
										}
									});

									
								});
								</script>
							<?php
							}
								?>
								<!--Code for displaying creator Project Ends Here-->
						</div>
						<?php
					
					}
					
					/*else if(isset($_GET['disp_val']) && !isset($_GET['sel_val']))
					{
					echo "aaaaaaaaa";
					?>
					<div id="mediaContent" style="width:685px; height:400px; overflow:auto;">
							<?php
							if(!isset($_GET['edit_gal']))
							{
								$k=1;
								for($count_arr = 0;$count_arr<count($newarray);$count_arr++)
								{
									if($newarray[$count_arr]['own_tag']=="own")
									{
								?>
								<div class="tableCont" id="AccordionContainer">
									<div class="blkD" style="width:60px;"><?php if($newarray[$count_arr]['name']=="Galleries/Images"){echo "Gallery";}else{echo $newarray[$count_arr]['name'];} ?></div>
									<div class="blkI" style="width:127px;"><a href=""><?php
									if($newarray[$count_arr]['creator']!="")
									{
										$end_pos = strpos($newarray[$count_arr]['creator'],'(');
										echo substr($newarray[$count_arr]['creator'],0,$end_pos);
									}else{
									?>&nbsp;<?php
									}
										?></a></div>
										
									<div class="blkK"><a href=""><?php 
									if($newarray[$count_arr]['from'] !="")
									{
										$end_pos_from = strpos($newarray[$count_arr]['from'],'(');
										echo substr($newarray[$count_arr]['from'],0,$end_pos_from);
									}else{
									?>&nbsp;<?php
									}
										?></a></div>
									<div class="blkB"><?php
									if($newarray[$count_arr]['title']!="")
									{
										if($newarray[$count_arr]['media_type'] == 114)
										{
											$get_media_track = $new_profile_media_classobj->get_media_track($newarray[$count_arr]['id']); 
											if($get_media_track['track'] == ""){
											echo $newarray[$count_arr]['title'];}else{
											echo $get_media_track['track'].".".$newarray[$count_arr]['title'];
											}
										}
										else{
										echo $newarray[$count_arr]['title']; }
									}else{
									?>&nbsp;<?php
									}
										?></div>
									<div class="blkplayer"><div class="player">
										<?php 
											if($newarray[$count_arr]['media_type']==113){
												$get_images1 = $new_profile_media_classobj ->get_all_gallery_images($newarray[$count_arr]['id']);
												for($count_img1=0;$count_img1<=count($get_images1);$count_img1++){
													$get_all_img1 = $get_all_img1.",".$get_images1[$count_img1]['image_name'];
													$galleryImagestitle1 = $galleryImagestitle1.",".$get_images1[$count_img1]['image_title'];
													$mediaSrc1 = $get_images1[0]['image_name'];
												}
												$gal_id1 = $get_images1[$count_img1]['media_id'];
												$orgPath1 = "https://medgallery.s3.amazonaws.com/";
												$thumbPath1 = "https://medgalthumb.s3.amazonaws.com/";
											?>
											<a href="javascript:void(0);"><img title="Play file now." src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" onclick="checkload('<?php echo $mediaSrc1;?>','<?php echo $get_all_img1; ?>','<?php echo $thumbPath1; ?>','<?php echo $orgPath1; ?>','<?php echo $galleryImagestitle1; ?>','<?php echo $gal_id1;?>')"/></a>
											<a href="javascript:void(0);"><img title="Add file to the bottom of your playlist without stopping current media." src="images/profile/add.gif"  onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'"/></a>
											<?php
												$get_all_img1 ="";
												$galleryImagestitle1="";
												$mediaSrc1 ="";
											}else if($newarray[$count_arr]['media_type']==114){
											?>
											<a href="javascript:void(0);"><img title="Play file now." src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" onclick="showPlayer('a','<?php if($newarray[$count_arr]['id']!=""){echo $newarray[$count_arr]['id'];}else{echo $newarray[$count_arr]['id'];}?>','play');"/></a>
											<a href="javascript:void(0);"><img title="Add file to the bottom of your playlist without stopping current media." src="images/profile/add.gif"  onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'" onclick="showPlayer('a','<?php if($newarray[$count_arr]['id']!=""){echo $newarray[$count_arr]['id'];}else{echo $newarray[$count_arr]['id'];}?>','add');"/></a>
											<?php
											}else if($newarray[$count_arr]['media_type']==115){
											?>
											<a href="javascript:void(0);"><img title="Play file now." src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" onclick="showPlayer('v','<?php if($newarray[$count_arr]['id']!=""){echo $newarray[$count_arr]['id'];}else{ echo $newarray[$count_arr]['id'];}?>','playlist');"/></a>
											<a href="javascript:void(0);"><img title="Add file to the bottom of your playlist without stopping current media." src="images/profile/add.gif"  onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'" onclick="showPlayer('v','<?php if($newarray[$count_arr]['id']!=""){echo $newarray[$count_arr]['id'];}else{ echo $newarray[$count_arr]['id'];}?>','addvi');"/></a>
											<?php
											}else{
											?>
											<a href="javascript:void(0);"><img title="Play file now." src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" /></a>
											<a href="javascript:void(0);"><img title="Add file to the bottom of your playlist without stopping current media." src="images/profile/add.gif"  onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'"/></a>
											<?php
											}
											
										?>
										<a href="javascript:void(0);"><img src="images/profile/download-over_down.gif" /></a>
									</div>
									<div class="icon" onclick="runAccordion(<?php echo $newarray[$count_arr]['id']; ?>);">
										<div onselectstart="return false;"><img title="View details for media." src="images/profile/view.jpg" /></div>
									</div>
									<div class="icon"><a href="add_media.php?edit=<?php echo $newarray[$count_arr]['id'];?>"><img title="Edit media." src="images/profile/edit.png" /></a></div>
									<div class="icon"><a onclick="return confirmdelete('<?php echo $newarray[$count_arr]['title'];?>')" href="delete_media.php?id=<?php echo $newarray[$count_arr]['id'];?>"><img title="Remove media from your library." src="images/profile/delete.png" /></a></div>
									<div class="expandable" id="Accordion<?php echo $newarray[$count_arr]['id']; ?>Content">
										<div class="affiBlock">
											<div class="RowCont">
												<div class="sideL">Plays :</div>
												<div class="sideR"><a href="" target="_blank">2</a></div>
											</div>
											<div class="RowCont">
												<div class="sideL">Downloads :</div>
												<div class="sideR"><a href="" target="_blank">2</a></div>
											</div>
											<?php
												if($newarray[$count_arr]['media_type']=='113')
												{
												
											?>
											<div class="RowCont">
												<div class="sideL">Image :</div>
												<div class="sideR"><a id="gallery_images<?php echo $k; ?>" href="gallery_images.php?gal_id=<?php echo $newarray[$count_arr]['id']; ?>">Edit Image</a></div>
											</div>	
											<div class="RowCont">
												<div class="sideL"></div>
												<?php 
												$find_gal = $new_profile_media_classobj->get_gallery_editable($newarray[$count_arr]['id']);
												if($find_gal['gallery_id'] !=0)
												{
												?>
													<div class="sideR"><a id="Add_gallery_images<?php echo $k; ?>" href="add_gallery_images.php?gal_id=<?php echo $newarray[$count_arr]['id']; ?>">Add Image</a></div>
												<?php
												}
												?>
											</div>
											<?php
												
												}
												else
												{
											?>	
											<div class="RowCont">
												<div class="sideL">Media URL :</div>
												<div class="sideR"><a href="" target="_blank">www.<?php echo $domainname; ?></a></div>
											</div>
											<?php
												}
											?>
										</div>
									</div>
								</div>
								<?php
								$k = $k+1;
								}
								else
								{
									if(isset($_GET['sel_val']) && $_GET['sel_val']!='all media')
									{
										if($_GET['sel_val']==$newarray[$count_arr]['media_type'])
										{
								?>
										<div class="tableCont" id="AccordionContainer">
											<div class="blkD" style="width:60px;"><?php if($newarray[$count_arr]['name']!=""){if($newarray[$count_arr]['name'] =="Galleries/Images"){echo "Gallery";}else{echo $newarray[$count_arr]['name'];}}else{?>&nbsp;<?php } ?></div>
											<div class="blkI" style="width:127px;"><a href=""><?php if($newarray[$count_arr]['creator']!=""){ echo $newarray[$count_arr]['creator'];}else{?>&nbsp;<?php } ?></a></div>
											<div class="blkK"><a href=""><?php if($newarray[$count_arr]['from']!=""){echo $newarray[$count_arr]['from'];}else{?>&nbsp;<?php } ?></a></div>
											<div class="blkB"><?php if($newarray[$count_arr]['title']!=""){echo $newarray[$count_arr]['title'];}else{?>&nbsp;<?php } ?></div>
											<div class="blkplayer"><div class="player">
											<?php 
												if($newarray[$count_arr]['media_type']==113){
													$get_images2 = $new_profile_media_classobj ->get_all_gallery_images($newarray[$count_arr]['id']);
													for($count_img2=0;$count_img2<=count($get_images2);$count_img2++){
														$get_all_img2 = $get_all_img2.",".$get_images2[$count_img2]['image_name'];
														$galleryImagestitle2 = $galleryImagestitle2.",".$get_images2[$count_img2]['image_title'];
														$mediaSrc2 = $get_images2[0]['image_name'];
													}
													$gal_id2 = $get_images2[$count_img2]['media_id'];
													$orgPath2 = "https://medgallery.s3.amazonaws.com/";
													$thumbPath2 = "https://medgalthumb.s3.amazonaws.com/";
												?>
												<a href="javascript:void(0);"><img title="Play file now." src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" onclick="checkload('<?php echo $mediaSrc2;?>','<?php echo $get_all_img2; ?>','<?php echo $thumbPath2; ?>','<?php echo $orgPath2; ?>','<?php echo $galleryImagestitle2; ?>','<?php echo $gal_id2;?>')"/></a>
												<a href="javascript:void(0);"><img title="Add file to the bottom of your playlist without stopping current media." src="images/profile/add.gif"  onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'"/></a>
												<?php
													$get_all_img2 ="";
													$galleryImagestitle2="";
													$mediaSrc2 ="";
												}else if($newarray[$count_arr]['media_type']==114){
												?>
												<a href="javascript:void(0);"><img title="Play file now." src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" onclick="showPlayer('a','<?php if($newarray[$count_arr]['id']!=""){echo $newarray[$count_arr]['id'];}else{echo $newarray[$count_arr]['id'];}?>','play');"/></a>
												<a href="javascript:void(0);"><img title="Add file to the bottom of your playlist without stopping current media." src="images/profile/add.gif"  onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'" onclick="showPlayer('a','<?php if($newarray[$count_arr]['id']!=""){echo $newarray[$count_arr]['id'];}else{echo $newarray[$count_arr]['id'];}?>','add');"/></a>
												<?php
												}else if($newarray[$count_arr]['media_type']==115){
												?>
												<a href="javascript:void(0);"><img title="Play file now." src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" onclick="showPlayer('v','<?php if($newarray[$count_arr]['id']!=""){echo $newarray[$count_arr]['id'];}else{ echo $newarray[$count_arr]['id'];}?>','playlist');"/></a>
												<a href="javascript:void(0);"><img title="Add file to the bottom of your playlist without stopping current media." src="images/profile/add.gif"  onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'" onclick="showPlayer('v','<?php if($newarray[$count_arr]['id']!=""){echo $newarray[$count_arr]['id'];}else{ echo $newarray[$count_arr]['id'];}?>','addvi');"/></a>
												<?php
												}else{
												?>
												<a href="javascript:void(0);"><img title="Play file now." src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" /></a>
												<a href="javascript:void(0);"><img title="Add file to the bottom of your playlist without stopping current media." src="images/profile/add.gif"  onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'"/></a>
												<?php
												}
												
											?>
											<a href="javascript:void(0);"><img src="images/profile/download-over_down.gif" /></a>
										</div>
											<!--<div class="icon" onclick="runAccordion(1);">
												<div onselectstart="return false;"><img src="images/profile/view.jpg" /></div>
											</div>
											<div class="icon"><a href="add_media.php?edit=<?php //echo $res1['id'];?>"><img src="images/profile/edit.png" /></a></div>
											<div class="icon"><a href="profileedit_media.php?id=<?php //echo $res1['id'];?>"><img src="images/profile/delete.png" /></a></div>-->
											<div class="expandable" id="Accordion1Content">
												<div class="affiBlock">
													<div class="RowCont">
														<div class="sideL">Plays :</div>
														<div class="sideR"><a href="" target="_blank">2</a></div>
													</div>
													<div class="RowCont">
														<div class="sideL">Downloads :</div>
														<div class="sideR"><a href="" target="_blank">2</a></div>
													</div>
													<div class="RowCont">
														<div class="sideL">Add Image :</div>
														<div class="sideR"><input type="file" class="fieldUpload" /></div>
													</div>
												</div>
											</div>
										</div>
								<?php
										}
									}
									else
									{
								?>
										<div class="tableCont" id="AccordionContainer">
											<div class="blkD" style="width:60px;"><?php if($all_sorting_media[$count_arr]['name']!=""){if($all_sorting_media[$count_arr]['name'] =="Galleries/Images"){echo "Gallery";}else{echo $all_sorting_media[$count_arr]['name'];}}else{?>&nbsp;<?php } ?></div>
											<div class="blkI" style="width:127px;"><a href=""><?php if($all_sorting_media[$count_arr]['creator']!=""){ echo $all_sorting_media[$count_arr]['creator'];}else{?>&nbsp;<?php } ?></a></div>
											<div class="blkK"><a href=""><?php if($all_sorting_media[$count_arr]['from']!=""){echo $all_sorting_media[$count_arr]['from'];}else{?>&nbsp;<?php } ?></a></div>
											<div class="blkB"><?php if($all_sorting_media[$count_arr]['title']!=""){echo $all_sorting_media[$count_arr]['title'];}else{?>&nbsp;<?php } ?></div>
											<div class="blkplayer"><div class="player">
											<?php 
												if($all_sorting_media[$count_arr]['media_type']==113){
													$get_images3 = $new_profile_media_classobj ->get_all_gallery_images($all_sorting_media[$count_arr]['id']);
													for($count_img3=0;$count_img3<=count($get_images3);$count_img3++){
														$get_all_img3 = $get_all_img3.",".$get_images3[$count_img3]['image_name'];
														$galleryImagestitle3 = $galleryImagestitle3.",".$get_images3[$count_img3]['image_title'];
														$mediaSrc3= $get_images3[0]['image_name'];
													}
													$gal_id3 = $get_images3[$count_img3]['media_id'];
													$orgPath3 = "https://medgallery.s3.amazonaws.com/";
													$thumbPath3 = "https://medgalthumb.s3.amazonaws.com/";
												?>
												<a href="javascript:void(0);"><img title="Play file now." src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" onclick="checkload('<?php echo $mediaSrc3;?>','<?php echo $get_all_img3; ?>','<?php echo $thumbPath3; ?>','<?php echo $orgPath3; ?>','<?php echo $galleryImagestitle3; ?>','<?php echo $gal_id3;?>')"/></a>
												<a href="javascript:void(0);"><img title="Add file to the bottom of your playlist without stopping current media." src="images/profile/add.gif"  onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'"/></a>
												<?php
													$get_all_img3 ="";
													$galleryImagestitle3="";
													$mediaSrc3 ="";
												}else if($all_sorting_media[$count_arr]['media_type']==114){
												?>
												<a href="javascript:void(0);"><img title="Play file now." src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" onclick="showPlayer('a','<?php if($all_sorting_media[$count_arr]['id']!=""){echo $all_sorting_media[$count_arr]['id'];}else{echo $all_sorting_media[$count_arr]['id'];}?>','play');"/></a>
												<a href="javascript:void(0);"><img title="Add file to the bottom of your playlist without stopping current media." src="images/profile/add.gif"  onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'" onclick="showPlayer('a','<?php if($all_sorting_media[$count_arr]['id']!=""){echo $all_sorting_media[$count_arr]['id'];}else{echo $all_sorting_media[$count_arr]['id'];}?>','add');"/></a>
												<?php
												}else if($all_sorting_media[$count_arr]['media_type']==115){
												?>
												<a href="javascript:void(0);"><img title="Play file now." src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" onclick="showPlayer('v','<?php if($all_sorting_media[$count_arr]['id']!=""){echo $all_sorting_media[$count_arr]['id'];}else{ echo $all_sorting_media[$count_arr]['id'];}?>','playlist');"/></a>
												<a href="javascript:void(0);"><img title="Add file to the bottom of your playlist without stopping current media." src="images/profile/add.gif"  onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'" onclick="showPlayer('v','<?php if($all_sorting_media[$count_arr]['id']!=""){echo $all_sorting_media[$count_arr]['id'];}else{ echo $all_sorting_media[$count_arr]['id'];}?>','addvi');"/></a>
												<?php
												}else{
												?>
												<a href="javascript:void(0);"><img title="Play file now." src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" /></a>
												<a href="javascript:void(0);"><img title="Add file to the bottom of your playlist without stopping current media." src="images/profile/add.gif"  onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'"/></a>
												<?php
												}
												
											?>
											<a href="javascript:void(0);"><img src="images/profile/download-over_down.gif" /></a>
										</div>
											<!--<div class="icon" onclick="runAccordion(1);">
												<div onselectstart="return false;"><img src="images/profile/view.jpg" /></div>
											</div>
											<div class="icon"><a href="add_media.php?edit=<?php //echo $res1['id'];?>"><img src="images/profile/edit.png" /></a></div>
											<div class="icon"><a href="profileedit_media.php?id=<?php //echo $res1['id'];?>"><img src="images/profile/delete.png" /></a></div>-->
											<div class="expandable" id="Accordion1Content">
												<div class="affiBlock">
													<div class="RowCont">
														<div class="sideL">Plays :</div>
														<div class="sideR"><a href="" target="_blank">2</a></div>
													</div>
													<div class="RowCont">
														<div class="sideL">Downloads :</div>
														<div class="sideR"><a href="" target="_blank">2</a></div>
													</div>
													<div class="RowCont">
														<div class="sideL">Add Image :</div>
														<div class="sideR"><input type="file" class="fieldUpload" /></div>
													</div>
												</div>
											</div>
										</div>
								<?php
									}
								}
								}
								
							?>
                          <?php
						  	
							
							//while($res=mysql_fetch_assoc($getmedia_regis))
								//{
								if($getmedia_regis['audio_id']!="" || $getmedia_regis['video_id']!="" || $getmedia_regis['img_id']!="")
								{
								?>
								<!--Display media uploaded at registration of artist--->
								<div class="tableCont" id="AccordionContainer">
									<div class="blkD" style="width:60px;"><?php if($getmedia_regis['audio_id']!=""){echo "Songs";}else{?>&nbsp;<?php }
															if($getmedia_regis['video_id']!=""){echo "Videos";}else{?>&nbsp;<?php }
															if($getmedia_regis['img_id']!=""){echo "Gallery";}else{?>&nbsp;<?php }
									?></div>
									<div class="blkI" style="width:127px;"><a href=""><?php if($getgeneral['fname']!=""){echo $getgeneral['fname'];}else{?>&nbsp;<?php } ?></a></div>
									<div class="blkK" >&nbsp;</div>
									<div class="blkB"><?php if($getmedia_regis['audio_id']!=""){echo $getmedia_regis['audio_name'];}else{?>&nbsp;<?php }
															if($getmedia_regis['video_id']!=""){echo $getmedia_regis['video_name'];}else{?>&nbsp;<?php }
															if($getmedia_regis['img_id']!=""){
															$get_gal_name=$new_profile_media_classobj->get_gallery_info();
															echo $get_gal_name['gallery_title'];
															}else{?>&nbsp;<?php }

									?></div>
									<div class="blkplayer"><div class="player">
										<?php 
										if($getmedia_regis['audio_id']!=""){
										?>
										<a href="javascript:void(0);"><img title="Play file now." src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" onclick="showPlayer_reg('a','<?php echo $getmedia_regis['audio_id'];?>','play_reg','general_artist_audio');"/></a>
										<a href="javascript:void(0);"><img title="Add file to the bottom of your playlist without stopping current media." src="images/profile/add.gif"  onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'" onclick="showPlayer_reg('a','<?php echo $getmedia_regis['audio_id'];?>','add_reg','general_artist_audio');"/></a>
										<?php
										}
										else if($getmedia_regis['video_id']!=""){
										?>
										<a href="javascript:void(0);"><img title="Play file now." src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" onclick="showPlayer_reg('v','<?php echo $getmedia_regis['video_id'];?>','playlist_reg','general_artist_video');"/></a>
										<a href="javascript:void(0);"><img title="Add file to the bottom of your playlist without stopping current media." src="images/profile/add.gif"  onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'" onclick="showPlayer_reg('v','<?php echo $getmedia_regis['video_id'];?>','addvi_reg','general_artist_video');"/></a>
										<?php
										}
										else if($getmedia_regis['img_id']!=""){
										$get_reg_gal_image1=$new_profile_media_classobj->get_gallery_reg_image($get_gal_name['gallery_id']);
										for($count_img_reg1=0;$count_img_reg1<=count($get_reg_gal_image1);$count_img_reg1++){
											$get_all_img_reg1 = $get_all_img_reg1.",".$get_reg_gal_image1[$count_img_reg1]['image_name'];
											$galleryImagestitle_reg1 = $galleryImagestitle_reg1.",".$get_reg_gal_image1[$count_img_reg1]['image_title'];
											$mediaSrc_reg1 = $get_reg_gal_image1[0]['image_name'];
										}
										$gal_id_reg1 = $get_reg_gal_image1[$count_img_reg1]['media_id'];
										$orgPath_reg1 = "https://reggallery.s3.amazonaws.com/";
										$thumbPath_reg1 = "https://reggalthumb.s3.amazonaws.com/";
										
										?>
										<a href="javascript:void(0);"><img title="Play file now." src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" onclick="checkload('<?php echo $mediaSrc_reg1;?>','<?php echo $get_all_img_reg1; ?>','<?php echo $thumbPath_reg1; ?>','<?php echo $orgPath_reg1; ?>','<?php echo $galleryImagestitle_reg1; ?>','<?php echo $gal_id_reg1;?>')"/></a>
										<a href="javascript:void(0);"><img title="Add file to the bottom of your playlist without stopping current media." src="images/profile/add.gif"  onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'"/></a>
										<?php
										$get_all_img_reg1 ="";
										$galleryImagestitle_reg1="";
										$mediaSrc_reg1="";
										}else{
										?>
										<a href="javascript:void(0);"><img title="Play file now." src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" /></a>
										<a href="javascript:void(0);"><img title="Add file to the bottom of your playlist without stopping current media." src="images/profile/add.gif"  onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'"/></a>
										<?php
										}
										?>
										
										<a href="javascript:void(0);"><img src="images/profile/download-over_down.gif" /></a>
									</div>
									<div class="icon" onclick="runAccordion(<?php echo $get_gal_name['gallery_id']; ?>);">
										<div onselectstart="return false;"><img title="View details for media." src="images/profile/view.jpg" /></div>
									</div>
									<!--<div class="icon"><a href="add_media.php?edit=<?php// echo $getmedia_regis['id'];?>"><img src="images/profile/edit.png" /></a></div>-->
									<div class="icon"><a onclick="return confirmdelete()" href="profileedit_media.php?id=<?php //echo $getmedia_regis['id'];?>"><img title="Remove media from your library." src="images/profile/delete.png" /></a></div>
									<div class="expandable" id="Accordion<?php echo $get_gal_name['gallery_id']; ?>Content">
										<div class="affiBlock">
											<div class="RowCont">
												<div class="sideL">Plays :</div>
												<div class="sideR"><a href="" target="_blank">2</a></div>
											</div>
											<div class="RowCont">
												<div class="sideL">Downloads :</div>
												<div class="sideR"><a href="" target="_blank">2</a></div>
											</div>
											<?php
												if($get_gal_name['gallery_title']!="" || $get_gal_name['gallery_title']!=null)
												{
												
											?>
											<div class="RowCont">
												<div class="sideL">Image :</div>
												<div class="sideR"><a id="artist_gallery_images" href="gallery_images_reg.php?gal_id=<?php echo $getmedia_regis['gallery_id']; ?>&artist=artist">Edit Image</a> <p> &nbsp &nbsp </p>
												<a id="Add_artist_gallery_images" href="add_gallery_images_reg.php?gal_id=<?php echo $getmedia_regis['gallery_id']; ?>&artist=artist">Add Image</a></div>
											</div>
											<?php
												$getmedia_regis['img_id']="";
												
												}
												else
												{
											?>	
											<div class="RowCont">
												<div class="sideL">Media URL :</div>
												<div class="sideR"><a href="" target="_blank">www.<?php echo $domainname; ?></a></div>
											</div>
											<?php
												}
											?>
										</div>
									</div>
								</div>
								<?php
								}
								?>
								<!--Display media uploaded at registration of artist Ends Here--->
								
								<?php
								if($getmedia_regis_com['audio_id']!="" || $getmedia_regis_com['video_id']!="" || $getmedia_regis_com['img_id']!="")
								{
								?>
								<!--Display media uploaded at registration of commuinty--->
								<div class="tableCont" id="AccordionContainer">
									<div class="blkD" style="width:60px;"><?php if($getmedia_regis_com['audio_id']!=""){echo "Songs";}else{?>&nbsp;<?php }
															if($getmedia_regis_com['video_id']!=""){echo "Videos";}else{?>&nbsp;<?php }
															if($getmedia_regis_com['img_id']!=""){echo "Gallery";}else{?>&nbsp;<?php }
									?></div>
									<div class="blkI" style="width:127px;"><a href=""><?php if($getgeneral['fname']!=""){echo $getgeneral['fname'];}else{?>&nbsp;<?php } ?></a></div>
									<div class="blkK">&nbsp;</div>
									<div class="blkB"><?php if($getmedia_regis_com['audio_id']!=""){echo $getmedia_regis_com['audio_name'];}else{?>&nbsp;<?php }
															if($getmedia_regis_com['video_id']!=""){echo $getmedia_regis_com['video_name'];}else{?>&nbsp;<?php }
															if($getmedia_regis_com['img_id']!=""){
															$get_gal_name=$new_profile_media_classobj->get_gallery_info_community();
															echo $get_gal_name['gallery_title'];
															}else{?>&nbsp;<?php }
									?></div>
									<div class="blkplayer"><div class="player">
										<?php 
										if($getmedia_regis_com['audio_id']!=""){
										?>
										<a href="javascript:void(0);"><img title="Play file now." src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" onclick="showPlayer_reg('a','<?php echo $getmedia_regis_com['audio_id'];?>','play_reg','general_community_audio');"/></a>
										<a href="javascript:void(0);"><img title="Add file to the bottom of your playlist without stopping current media." src="images/profile/add.gif"  onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'" onclick="showPlayer_reg('a','<?php echo $getmedia_regis_com['audio_id'];?>','add_reg','general_community_audio');"/></a>
										<?php
										}
										else if($getmedia_regis_com['video_id']!=""){
										?>
										<a href="javascript:void(0);"><img title="Play file now." src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" onclick="showPlayer_reg('v','<?php echo $getmedia_regis_com['video_id'];?>','playlist_reg','general_community_video');"/></a>
										<a href="javascript:void(0);"><img title="Add file to the bottom of your playlist without stopping current media." src="images/profile/add.gif"  onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'" onclick="showPlayer_reg('v','<?php echo $getmedia_regis_com['video_id'];?>','addvi_reg','general_community_video');"/></a>
										<?php
										}
										else if($getmedia_regis_com['img_id']!=""){
										$get_reg_gal_image_com1=$new_profile_media_classobj->get_gallery_reg_image_com($get_gal_name['gallery_id']);
										for($count_img_reg_com1=0;$count_img_reg_com1<=count($get_reg_gal_image_com1);$count_img_reg_com1++){
											$get_all_img_regcom1 = $get_all_img_regcom1.",".$get_reg_gal_image_com1[$count_img_reg_com1]['image_name'];
											$galleryImagestitle_regcom1 = $galleryImagestitle_regcom1.",".$get_reg_gal_image_com1[$count_img_reg_com1]['image_title'];
											$mediaSrc_regcom1 = $get_reg_gal_image_com1[0]['image_name'];
										}
										$gal_id_regcom1 = $get_reg_gal_image_com1[$count_img_reg_com1]['media_id'];
										$orgPath_regcom1 = "https://reggallery.s3.amazonaws.com/";
										$thumbPath_regcom1 = "https://reggalthumb.s3.amazonaws.com/";
										
										?>
										<a href="javascript:void(0);"><img title="Play file now." src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" onclick="checkload('<?php echo $mediaSrc_regcom1;?>','<?php echo $get_all_img_regcom1; ?>','<?php echo $thumbPath_regcom1; ?>','<?php echo $orgPath_regcom1; ?>','<?php echo $galleryImagestitle_regcom1; ?>','<?php echo $gal_id_regcom1;?>')"/></a>
										<a href="javascript:void(0);"><img title="Add file to the bottom of your playlist without stopping current media." src="images/profile/add.gif"  onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'"/></a>
										<?php
										$get_all_img_regcom1 ="";
										$galleryImagestitle_regcom1="";
										$mediaSrc_regcom1="";
										}else{
										?>
										<a href="javascript:void(0);"><img title="Play file now." src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" /></a>
										<a href="javascript:void(0);"><img title="Add file to the bottom of your playlist without stopping current media." src="images/profile/add.gif"  onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'"/></a>
										<?php
										}
										?>
										
										<a href="javascript:void(0);"><img src="images/profile/download-over_down.gif" /></a>
									</div>
									<div class="icon" onclick="runAccordion(<?php echo $get_gal_name['gallery_id']; ?>);">
										<div onselectstart="return false;"><img title="View details for media." src="images/profile/view.jpg" /></div>
									</div>
								<!--	<div class="icon"><a href="add_media.php?edit=<?php// echo $getmedia_regis['id'];?>"><img src="images/profile/edit.png" /></a></div>-->
									<div class="icon"><a onclick="return confirmdelete()" href="profileedit_media.php?id=<?php //echo $getmedia_regis['id'];?>"><img title="Remove media from your library." src="images/profile/delete.png" /></a></div>
									<div class="expandable" id="Accordion<?php echo $get_gal_name['gallery_id']; ?>Content">
										<div class="affiBlock">
											<div class="RowCont">
												<div class="sideL">Plays :</div>
												<div class="sideR"><a href="" target="_blank">2</a></div>
											</div>
											<div class="RowCont">
												<div class="sideL">Downloads :</div>
												<div class="sideR"><a href="" target="_blank">2</a></div>
											</div>
											<?php
												if($get_gal_name['gallery_title']!="" || $get_gal_name['gallery_title']!=null)
												{
												
											?>
											<div class="RowCont">
												<div class="sideL">Image :</div>
												<div class="sideR"><a id="community_gallery_images" href="gallery_images_reg.php?gal_id=<?php echo $getmedia_regis_com['gallery_id']; ?>&community=community">Edit Image</a> <p> &nbsp &nbsp </p>
												<a id="Add_community_gallery_images" href="add_gallery_images_reg.php?gal_id=<?php echo $getmedia_regis_com['gallery_id']; ?>&community=community">Add Image</a></div>
											</div>
											<?php
											
												}
												else
												{
											?>	
											<div class="RowCont">
												<div class="sideL">Media URL :</div>
												<div class="sideR"><a href="" target="_blank">www.<?php echo $domainname; ?></a></div>
											</div>
											<?php
												}
											?>
										</div>
									</div>
								</div>
								<?php
								 }
								 ?>
								<!--Display media uploaded at registration of commuinty Ends Here--->
								
								<?php
								//}
							
							}
							?>
							<!--code for displaying Edit gallery-->
							<?php
							if(isset($_GET['edit_gal']) && $_GET['edit_gal']==1)
							{
								$k=1;
								while($res=mysql_fetch_assoc($getmedia))
								{
									
								?>
								<div class="tableCont" id="AccordionContainer">
									<div class="blkD" style="width:60px;"><?php if($res['name']!=""){echo $res['name'];}else{?>&nbsp;<?php } ?></div>
									<div class="blkI" style="width:127px;"><a href=""><?php if($res['creator']!=""){echo $res['creator'];}else{?>&nbsp;<?php } ?></a></div>
									<!--<div class="blkK"><a href="gallery_images.php?gal_id=<?php //echo $res['id'];?>" id="gallery_images<?php //echo $k;?>">Edit Image</a></div>
									<div class="blkB"><a href="add_gallery_images.php?gal_id=<?php echo $res['id'];?>" id="Add_gallery_images<?php echo $k;?>">Add Image</a></div>-->
									<!--<div class="blkJ"><a href=""><img src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" /></a><a href=""><img src="images/profile/add.gif"  onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'"/></a><a href=""><img src="images/profile/download_down.gif" onmouseover="this.src='images/profile/download-over_down.gif'" onmouseout="this.src='images/profile/download_down.gif'" /></a></div>-->
									<div class="icon" onclick="runAccordion(1);">
										<div onselectstart="return false;"><img title="View details for media." src="images/profile/view.jpg" /></div>
									</div>
									<div class="icon"><a href="add_media.php?edit=<?php echo $res['id'];?>"><img title="Edit media." src="images/profile/edit.png" /></a></div>
									<div class="icon"><a onclick="return confirmdelete()" href="profileedit_media.php?id=<?php echo $res['id'];?>"><img title="Remove media from your library." src="images/profile/delete.png" /></a></div>
									<div class="expandable" id="Accordion1Content">
										<div class="affiBlock">
											<div class="RowCont">
												<div class="sideL">Plays :</div>
												<div class="sideR"><a href="" target="_blank">2</a></div>
											</div>
											<div class="RowCont">
												<div class="sideL">Downloads :</div>
												<div class="sideR"><a href="" target="_blank">2</a></div>
											</div>
											<div class="RowCont">
												<div class="sideL">Add Image :</div>
												<div class="sideR"><input type="file" class="fieldUpload" /></div>
											</div>
										</div>
									</div>
								</div>
								<?php
									$k=$k+1;
								}
							}
							?>
							
							
							<!-- Code For Displaying media From Fan --->
							<?php
								$chk_fan = $new_profile_media_classobj->chk_fan();
								while($row_fan = mysql_fetch_assoc($chk_fan))
								{
									$get_fan_media = $new_profile_media_classobj->get_fan_media($row_fan['buyer_id'],$row_fan['seller_id'],$row_fan['table_name'],$row_fan['type_id']);
									if(mysql_num_rows($get_fan_media)>0)
									{
										while($row_fan_media = mysql_fetch_assoc($get_fan_media))
										{
											//echo $row_fan_media['media_id']."</br>";
											$get_media_data = $new_profile_media_classobj->get_media_data($row_fan_media['media_id']);
											if($get_media_data!="")
											{
								?>
											<div class="tableCont" id="AccordionContainer">
												<div class="blkD" style="width:60px;"><?php if($get_media_data['name']!=""){if($get_media_data['name'] =="Galleries/Images"){echo "Gallery";}else{echo $get_media_data['name'];}}else{?>&nbsp;<?php }?></div>
												<div class="blkI" style="width:127px;"><a href=""><?php if($get_media_data['creator']!=""){echo $get_media_data['creator'];}else{?>&nbsp;<?php }?></a></div>
												<div class="blkK"><a href=""><?php if($get_media_data['from']!=""){echo $get_media_data['from'];}else{?>&nbsp;<?php }?></a></div>
												<div class="blkB"><?php if($get_media_data['title']!=""){echo $get_media_data['title'];}else{?>&nbsp;<?php }?></div>
												<div class="blkplayer"><div class="player">
												<?php 
													if($get_media_data['media_type']==113){
														$get_images_fan1 = $new_profile_media_classobj ->get_all_gallery_images($get_media_data['id']);
														for($count_img=0;$count_img<=count($get_images_fan1);$count_img++){
															$get_all_img_fan1 = $get_all_img_fan1.",".$get_images_fan1[$count_img]['image_name'];
															$galleryImagestitle_fan1 = $galleryImagestitle_fan1.",".$get_images_fan1[$count_img]['image_title'];
															$mediaSrc_fan1 = $get_images_fan1[0]['image_name'];
														}
														$gal_id_fan1 = $get_images_fan1[$count_img]['media_id'];
														$orgPath_fan1 = "https://medgallery.s3.amazonaws.com/";
														$thumbPath_fan1 = "https://medgalthumb.s3.amazonaws.com/";
													?>
													<a href="javascript:void(0);"><img title="Play file now." src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" onclick="checkload('<?php echo $mediaSrc_fan1;?>','<?php echo $get_all_img_fan1; ?>','<?php echo $thumbPath_fan1; ?>','<?php echo $orgPath_fan1; ?>','<?php echo $galleryImagestitle_fan1; ?>','<?php echo $gal_id_fan1;?>')"/></a>
													<a href="javascript:void(0);"><img title="Add file to the bottom of your playlist without stopping current media." src="images/profile/add.gif"  onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'"/></a>
													<?php
														$get_all_img_fan1 ="";
														$galleryImagestitle_fan1="";
														$mediaSrc_fan1 ="";
													}else if($get_media_data['media_type']==114){
													?>
													<a href="javascript:void(0);"><img title="Play file now." src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" onclick="showPlayer('a','<?php if($get_media_data['id']!=""){echo $get_media_data['id'];}else{echo $get_media_data['id'];}?>','play');"/></a>
													<a href="javascript:void(0);"><img title="Add file to the bottom of your playlist without stopping current media." src="images/profile/add.gif"  onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'" onclick="showPlayer('a','<?php if($get_media_data['id']!=""){echo $get_media_data['id'];}else{echo $get_media_data['id'];}?>','add');"/></a>
													<?php
													}else if($get_media_data['media_type']==115){
													?>
													<a href="javascript:void(0);"><img title="Play file now." src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" onclick="showPlayer('v','<?php if($get_media_data['id']!=""){echo $get_media_data['id'];}else{ echo $get_media_data['id'];}?>','playlist');"/></a>
													<a href="javascript:void(0);"><img title="Add file to the bottom of your playlist without stopping current media." src="images/profile/add.gif"  onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'" onclick="showPlayer('v','<?php if($get_media_data['id']!=""){echo $get_media_data['id'];}else{ echo $get_media_data['id'];}?>','addvi');"/></a>
													<?php
													}else{
													?>
													<a href="javascript:void(0);"><img title="Play file now." src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" /></a>
													<a href="javascript:void(0);"><img title="Add file to the bottom of your playlist without stopping current media." src="images/profile/add.gif"  onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'"/></a>
													<?php
													}
													
												?>
												
												<a href="javascript:void(0);"><img src="images/profile/download-over_down.gif" /></a>
												</div>
											</div>
								<?php
											}
										}
									}
								}
							?>
								<!--Code For Displaying media From Fan Ends Here--->
							
								
							
						</div>
					<?php
					}*/
					else{
					?>
					<div id="mediaContent" style="width:685px; height:400px; overflow:auto;">
							<?php
							$my_media_arr = array();
							if(!isset($_GET['edit_gal']))
							{
								$k=1;
								$getmedia=$new_profile_media_classobj->sel_all_media();
								if($getmedia!="" && $getmedia!=NULL)
								{
									while($res=mysql_fetch_assoc($getmedia))
									{
										if($res['media_type'] == 114)
										{
											$get_media_track = $new_profile_media_classobj->get_media_track($res['id']); 
												$res["track"] = $get_media_track['track'];
										}
									
										$res["media_from"] = "my_media";
										$my_media_arr[] = $res;
									}
								}
								$getgeneral=$new_profile_media_classobj->sel_general();
								$exp_media_id =explode(',',$getgeneral['accepted_media_id']);
								$new_tag_media_arr = array();
								for($i=0;$i<count($exp_media_id);$i++)
								{
									$get_tagged_media=$new_profile_media_classobj->sel_all_tagged_media($exp_media_id[$i]);
									if($get_tagged_media!="" && $get_tagged_media!=NULL)
									{
										while($res1=mysql_fetch_assoc($get_tagged_media))
										{
											if($res1['media_type'] == 114)
											{
												$get_media_track = $new_profile_media_classobj->get_media_track($res1['id']); 
												$res1["track"] = $get_media_track['track'];
											}
											$res1['media_from'] = "accepted_media";
											$get_type_of_tagged_media=$new_profile_media_classobj->sel_type_of_tagged_media($res1['media_type']);
											$res1['name'] = $get_type_of_tagged_media['name'];
											$new_tag_media_arr[] = $res1;
										}
									}
								}
							}
							$new_edi_gal_media_arr = array();
							if(isset($_GET['edit_gal']) && $_GET['edit_gal']==1)
							{
								$k=1;
								if($getmedia!="" && $getmedia!=NULL)
								{
									while($res=mysql_fetch_assoc($getmedia))
									{
										if($res['media_type'] == 114)
										{
											$get_media_track = $new_profile_media_classobj->get_media_track($res['id']); 
											$res["track"] = $get_media_track['track'];
										}
										$res['media_from'] = "newedit_media";
										$new_edi_gal_media_arr[] = $res;
									}
								}
							}
											
							$chk_fan = $new_profile_media_classobj->chk_fan();
							$new_fan_media_array = array();
							if($chk_fan!="" && $chk_fan!=NULL)
							{
								while($row_fan = mysql_fetch_assoc($chk_fan))
								{
									$new_fan_arr[] = $row_fan;
									$get_fan_media = $new_profile_media_classobj->get_fan_media($row_fan['buyer_id'],$row_fan['seller_id'],$row_fan['table_name'],$row_fan['type_id']);
									if($get_fan_media!="" && $get_fan_media!=NULL)
									{
										if(mysql_num_rows($get_fan_media)>0)
										{
											while($row_fan_media = mysql_fetch_assoc($get_fan_media))
											{
												$get_fan_generalmedia = $new_profile_media_classobj->get_all_fan_mediadetail($row_fan_media['media_id']);
												if($get_fan_generalmedia['media_type'] == 114)
												{
													$get_media_track = $new_profile_media_classobj->get_media_track($get_fan_generalmedia['id']); 
													$get_fan_generalmedia["track"] = $get_media_track['track'];
												}
												$get_type_of_tagged_media = $new_profile_media_classobj->sel_type_of_tagged_media($get_fan_generalmedia['media_type']);
												$get_fan_generalmedia['name'] = $get_type_of_tagged_media['name'];
												$get_fan_generalmedia['media_from'] = "media_coming_fan";
												$new_fan_media_array[] = $get_fan_generalmedia;
											}
										}
									}
								}
							}
							
							$get_buyed_media = $new_profile_media_classobj->get_sale_media();
							$new_buyed_media_array = array();
							if($get_buyed_media!="" && $get_buyed_media!=NULL)
							{
								if(mysql_num_rows($get_buyed_media)>0){
									while($row_buyed_media = mysql_fetch_assoc($get_buyed_media)){
										$get_buyed_generalmedia = $new_profile_media_classobj->get_all_fan_mediadetail($row_buyed_media['media_id']);
										if($get_buyed_generalmedia['media_type'] == 114)
										{
											$get_media_track = $new_profile_media_classobj->get_media_track($get_buyed_generalmedia['id']); 
											$get_buyed_generalmedia["track"] = $get_media_track['track'];
										}
										$get_type_of_tagged_media = $new_profile_media_classobj->sel_type_of_tagged_media($get_buyed_generalmedia['media_type']);
										$get_buyed_generalmedia['name'] = $get_type_of_tagged_media['name'];
										$get_buyed_generalmedia['media_from'] = "media_coming_buyed";
										$new_buyed_media_array[] = $get_buyed_generalmedia;
									}
								}
							}
							
							$get_media_tag_art_pro = $new_profile_media_classobj->get_artist_project_tag($_SESSION['login_email']);
							if($get_media_tag_art_pro!="" && $get_media_tag_art_pro!=NULL)
							{
								if(mysql_num_rows($get_media_tag_art_pro)>0)
								{
									$get_songs_new = array();
									$get_video_new = array();
									$get_gallery_new = array();
									$is = 0;
									$iv = 0;
									$ig = 0;
									while($art_pro_tag = mysql_fetch_assoc($get_media_tag_art_pro))
									{
										$get_songs = explode(",",$art_pro_tag['tagged_songs']);
										for($count_art_tag_song=0;$count_art_tag_song<count($get_songs);$count_art_tag_song++)
										{
											if($get_songs[$count_art_tag_song]!="")
											{
												$get_songs_new[$is] = $get_songs[$count_art_tag_song];
												$is = $is + 1;
											}
										}
										$get_video = explode(",",$art_pro_tag['tagged_videos']);
										for($count_art_tag_video=0;$count_art_tag_video<count($get_video);$count_art_tag_video++)
										{
											if($get_video[$count_art_tag_video]!="")
											{
												$get_video_new[$iv] = $get_video[$count_art_tag_video];
												$iv = $iv + 1;
											}
										}
										$get_gallery = explode(",",$art_pro_tag['tagged_galleries']);
										for($count_art_tag_gal=0;$count_art_tag_gal<count($get_gallery);$count_art_tag_gal++)
										{
											if($get_gallery[$count_art_tag_gal]!="")
											{
												$get_gallery_new[$ig] = $get_gallery[$count_art_tag_gal];
												$ig = $ig + 1;
											}
										}
									}
									if($get_songs_new!=NULL)
									{
										$get_songs_n = array_unique($get_songs_new);
									}
									if($get_video_new!=NULL)
									{
										$get_video_n = array_unique($get_video_new);
									}
									if($get_gallery_new!=NULL)
									{
										$get_gallery_n = array_unique($get_gallery_new);
									}
								}
							}
							
							$get_media_tag_art_eve = $new_profile_media_classobj->get_artist_event_tag($_SESSION['login_email']);
							if($get_media_tag_art_eve!="" && $get_media_tag_art_eve!=NULL){
								if(mysql_num_rows($get_media_tag_art_eve)>0)
								{
									$get_songs_newe = array();
									$get_video_newe = array();
									$get_gallery_newe = array();
									$is = 0;
									$iv = 0;
									$ig = 0;
									while($art_pro_tag = mysql_fetch_assoc($get_media_tag_art_eve))
									{
										$get_songs = explode(",",$art_pro_tag['tagged_songs']);
										for($count_art_tag_song=0;$count_art_tag_song<count($get_songs);$count_art_tag_song++)
										{
											if($get_songs[$count_art_tag_song]!="")
											{
												$get_songs_newe[$is] = $get_songs[$count_art_tag_song];
												$is = $is + 1;
											}
										}
										$get_video = explode(",",$art_pro_tag['tagged_videos']);
										for($count_art_tag_video=0;$count_art_tag_video<count($get_video);$count_art_tag_video++)
										{
											if($get_video[$count_art_tag_video]!="")
											{
												$get_video_newe[$iv] = $get_video[$count_art_tag_video];
												$iv = $iv + 1;
											}
										}
										$get_gallery = explode(",",$art_pro_tag['tagged_galleries']);
										for($count_art_tag_gallery=0;$count_art_tag_gallery<count($get_gallery);$count_art_tag_gallery++)
										{
											if($get_gallery[$count_art_tag_gallery]!="")
											{
												$get_gallery_newe[$ig] = $get_gallery[$count_art_tag_gallery];
												$ig = $ig + 1;
											}
										}
									}
									if($get_songs_newe!=NULL)
									{
										$get_songs_ne = array_unique($get_songs_newe);
									}
									if($get_video_newe!=NULL)
									{
										$get_video_ne = array_unique($get_video_newe);
									}
									if($get_gallery_newe!=NULL)
									{
										$get_gallery_ne = array_unique($get_gallery_newe);
									}
								}
							}
							
							$get_media_tag_com_pro = $new_profile_media_classobj->get_community_project_tag($_SESSION['login_email']);
							if($get_media_tag_com_pro!="" && $get_media_tag_com_pro!=NULL)
							{
								if(mysql_num_rows($get_media_tag_com_pro)>0)
								{
									$get_songs_new_c = array();
									$get_video_new_c = array();
									$get_gallery_new_c = array();
									$is_c = 0;
									$iv_c = 0;
									$ig_c = 0;
									
									while($com_pro_tag = mysql_fetch_assoc($get_media_tag_com_pro))
									{
										$get_com_songs = explode(",",$com_pro_tag['tagged_songs']);
										for($count_com_tag_song=0;$count_com_tag_song<count($get_com_songs);$count_com_tag_song++)
										{
											if($get_com_songs[$count_com_tag_song]!="")
											{
												$get_songs_new_c[$is_c] = $get_com_songs[$count_com_tag_song];
												$is_c = $is_c + 1;
											}
										}
										$get_com_video = explode(",",$com_pro_tag['tagged_videos']);
										for($count_com_tag_video=0;$count_com_tag_video<count($get_com_video);$count_com_tag_video++)
										{
											if($get_com_video[$count_com_tag_video]!="")
											{
												$get_video_new_c[$iv_c] = $get_com_video[$count_com_tag_video];
												$iv_c = $iv_c + 1;
											}
										}
										$get_com_gallery = explode(",",$com_pro_tag['tagged_galleries']);
										for($count_com_tag_gallery=0;$count_com_tag_gallery<count($get_com_gallery);$count_com_tag_gallery++)
										{
											if($get_com_gallery[$count_com_tag_gallery]!="")
											{
												$get_gallery_new_c[$ig_c] = $get_com_gallery[$count_com_tag_gallery];
												$ig_c = $ig_c + 1;
											}
										}
									}
									if($get_songs_new_c!=NULL)
									{
										$get_songs_n_c = array_unique($get_songs_new_c);
									}
									if($get_video_new_c!=NULL)
									{
										$get_video_n_c = array_unique($get_video_new_c);
									}
									if($get_gallery_new_c!=NULL)
									{
										$get_gallery_n_c = array_unique($get_gallery_new_c);
									}
								}
							}
							
							$get_media_tag_com_eve = $new_profile_media_classobj->get_community_event_tag($_SESSION['login_email']);
							if($get_media_tag_com_eve!="" && $get_media_tag_com_eve!=NULL)
							{
								if(mysql_num_rows($get_media_tag_com_eve)>0)
								{
									$get_songs_new_ce = array();
									$get_video_new_ce = array();
									$get_gallery_new_ce = array();
									$is_c = 0;
									$iv_c = 0;
									$ig_c = 0;
									while($com_pro_tag = mysql_fetch_assoc($get_media_tag_com_eve))
									{
										$get_com_songs = explode(",",$com_pro_tag['tagged_songs']);
										for($count_com_tag_song=0;$count_com_tag_song<count($get_com_songs);$count_com_tag_song++)
										{
											if($get_com_songs[$count_com_tag_song]!="")
											{
												$get_songs_new_ce[$is_c] = $get_com_songs[$count_com_tag_song];
												$is_c = $is_c + 1;
											}
										}
										$get_com_video = explode(",",$com_pro_tag['tagged_videos']);
										for($count_com_tag_video=0;$count_com_tag_video<count($get_com_video);$count_com_tag_video++)
										{
											if($get_com_video[$count_com_tag_video]!="")
											{
												$get_video_new_ce[$iv_c] = $get_com_video[$count_com_tag_video];
												$iv_c = $iv_c + 1;
											}
										}
										$get_com_gallery = explode(",",$com_pro_tag['tagged_galleries']);
										for($count_com_tag_gallery=0;$count_com_tag_gallery<count($get_com_gallery);$count_com_tag_gallery++)
										{
											if($get_com_gallery[$count_com_tag_gallery]!="")
											{
												$get_gallery_new_ce[$ig_c] = $get_com_gallery[$count_com_tag_gallery];
												$ig_c = $ig_c + 1;
											}
										}
									}
									if($get_songs_new_ce!=NULL)
									{
										$get_songs_n_ec = array_unique($get_songs_new_ce);
									}
									if($get_video_new_ce!=NULL)
									{
										$get_video_n_ec = array_unique($get_video_new_ce);
									}
									if($get_gallery_new_ce!=NULL)
									{
										$get_gallery_n_ec = array_unique($get_gallery_new_ce);
									}
								}
							}
							
							$get_media_cre_art_pro = $new_profile_media_classobj->get_project_create();
							if(!empty($get_media_cre_art_pro))
							{
								$get_songs_pn = array();
								$get_video_pn = array();
								$get_gallery_pn = array();
								$is_p = 0;
								$iv_p = 0;
								$ig_p = 0;
								
								//while($art_pro_tag = mysql_fetch_assoc($get_media_cre_art_pro))
								for($cr_i=0;$cr_i<count($get_media_cre_art_pro);$cr_i++)
								{
									$get_csongs = explode(",",$get_media_cre_art_pro[$cr_i]['tagged_songs']);
									for($count_art_cr_song=0;$count_art_cr_song<count($get_csongs);$count_art_cr_song++)
									{
										if($get_csongs[$count_art_cr_song]!="")
										{
											$get_songs_pn[$is_p] = $get_csongs[$count_art_cr_song];
											$is_p = $is_p + 1;
										}
									}
									$get_cvideo = explode(",",$get_media_cre_art_pro[$cr_i]['tagged_videos']);
									for($count_art_cr_video=0;$count_art_cr_video<count($get_cvideo);$count_art_cr_video++)
									{
										if($get_cvideo[$count_art_cr_video]!="")
										{
											$get_video_pn[$iv_p] = $get_cvideo[$count_art_cr_video];
											$iv_p = $iv_p + 1;
										}
									}
									$get_cgallery = explode(",",$get_media_cre_art_pro[$cr_i]['tagged_galleries']);
									for($count_art_cr_gallery=0;$count_art_cr_gallery<count($get_cgallery);$count_art_cr_gallery++)
									{
										if($get_cgallery[$count_art_cr_gallery]!="")
										{
											$get_gallery_pn[$ig_p] = $get_cgallery[$count_art_cr_gallery];
											$ig_p = $ig_p + 1;
										}
									}
								}
								if($get_songs_pn!=NULL)
								{
									$get_songs_p_new = array_unique($get_songs_pn);
								}
								if($get_video_pn!=NULL)
								{
									$get_video_p_new = array_unique($get_video_pn);
								}
								if($get_gallery_pn!=NULL)
								{
									$get_gallery_p_new = array_unique($get_gallery_pn);
								}
							}
							
							if(!isset($get_songs_n)){
								$get_songs_n = array();}
							if(!isset($get_video_n)){
								$get_video_n = array();}
							if(!isset($get_gallery_n)){
								$get_gallery_n = array();}
							
							if(!isset($get_songs_n_ec)){
								$get_songs_n_ec = array();}
							if(!isset($get_video_n_ec)){
								$get_video_n_ec = array();}
							if(!isset($get_gallery_n_ec)){
								$get_gallery_n_ec = array();}
							
							if(!isset($get_songs_ne)){
								$get_songs_ne = array();}
							if(!isset($get_video_ne)){
								$get_video_ne = array();}
							if(!isset($get_gallery_ne)){
								$get_gallery_ne = array();}
							
							if(!isset($get_songs_n_c)){
								$get_songs_n_c = array();}
							if(!isset($get_video_n_c)){
								$get_video_n_c = array();}
							if(!isset($get_gallery_n_c)){
								$get_gallery_n_c = array();}
							
							if(!isset($get_songs_p_new)){
								$get_songs_p_new = array();}
							if(!isset($get_video_p_new)){
								$get_video_p_new = array();}
							if(!isset($get_gallery_p_new)){
								$get_gallery_p_new = array();}

							$get_songs_pnew_w_c = array_merge($get_songs_n,$get_video_n,$get_gallery_n,$get_songs_n_c,$get_video_n_c,$get_gallery_n_c,$get_songs_p_new,$get_video_p_new,$get_gallery_p_new,$get_songs_ne,$get_video_ne,$get_gallery_ne,$get_songs_n_ec,$get_video_n_ec,$get_gallery_n_ec);
							if($get_songs_pnew_w_c!=NULL)
							{
								$get_songs_pnew_ct = array_unique($get_songs_pnew_w_c);
							}
							$get_songs_final_ct = array();
							
							for($count_art_cr_song=0;$count_art_cr_song<count($get_songs_pnew_ct);$count_art_cr_song++)
							{
								if($get_songs_pnew_ct[$count_art_cr_song]!="")
								{
									$get_media_tag_art_pro_song = $new_profile_media_classobj->get_tag_media_info($get_songs_pnew_ct[$count_art_cr_song]);
									if(!empty($get_media_tag_art_pro_song)){
										if($get_media_tag_art_pro_song['media_type'] == 114)
										{
											$get_media_track = $new_profile_media_classobj->get_media_track($get_media_tag_art_pro_song['id']); 
											$get_media_tag_art_pro_song["track"] = $get_media_track['track'];
										}
										$get_media_tag_art_pro_song['media_from'] ="new_tagging_media";
										$get_songs_final_ct[] = $get_media_tag_art_pro_song;
									}
									
								}
							}
							$get_songs_final_ct = array();
							$new_medi = $new_profile_media_classobj->get_creator_heis();
							$new_creator_creator_media_array = array();
							if(!empty($new_medi))
							{
								for($count_cre_m=0;$count_cre_m<count($new_medi);$count_cre_m++)
								{
									$get_media_type = $new_profile_media_classobj->sel_type_of_tagged_media($new_medi[$count_cre_m]['media_type']);
									if($new_medi[$count_cre_m]['media_type'] == 114)
									{
										$get_media_track = $new_profile_media_classobj->get_media_track($new_medi[$count_cre_m]['id']); 
										$new_medi[$count_cre_m]["track"] = $get_media_track['track'];
									}
									$new_medi[$count_cre_m]['name'] = $get_media_type['name'];
									$new_medi[$count_cre_m]['media_from'] = "media_creator_creator";
									$new_creator_creator_media_array[] = $new_medi[$count_cre_m];
								}
							}

							/*This function will display the user in which profile he is creator and display media where that profile was creator Ends here*/

							/*This function will display the user in which profile he is from and display media where that profile was creator*/
							$new_medi_from = $new_profile_media_classobj->get_from_heis();
							$new_from_from_media_array = array();
							if(!empty($new_medi_from))
							{
								for($count_cre_m=0;$count_cre_m<count($new_medi_from);$count_cre_m++)
								{
									$get_media_type = $new_profile_media_classobj->sel_type_of_tagged_media($new_medi_from[$count_cre_m]['media_type']);
									if($new_medi_from[$count_cre_m]['media_type'] == 114)
									{
										$get_media_track = $new_profile_media_classobj->get_media_track($new_medi_from[$count_cre_m]['id']); 
										$new_medi_from[$count_cre_m]["track"] = $get_media_track['track'];
									}
									$new_medi_from[$count_cre_m]['name'] = $get_media_type['name'];
									$new_medi_from[$count_cre_m]['media_from'] = "media_from_from";
									$new_from_from_media_array[] = $new_medi_from[$count_cre_m];
								}
							}
							/*This function will display the user in which profile he is from and display media where that profile was creator Ends here*/
							
							/*This function will display the user in which profile he is from or creator and tagged and display media where that profile was creator*/
							$new_medi_tagged = $new_profile_media_classobj->get_the_user_tagin_project($_SESSION['login_email']);
							$new_tagged_from_media_array = array();
							if(!empty($new_medi_tagged))
							{
								for($count_cre_m=0;$count_cre_m<count($new_medi_tagged);$count_cre_m++)
								{
									$get_media_type = $new_profile_media_classobj->sel_type_of_tagged_media($new_medi_tagged[$count_cre_m]['media_type']);
									if($new_medi_tagged[$count_cre_m]['media_type'] == 114)
									{
										$get_media_track = $new_profile_media_classobj->get_media_track($new_medi_tagged[$count_cre_m]['id']); 
										$new_medi_tagged[$count_cre_m]["track"] = $get_media_track['track'];
									}
									$new_medi_tagged[$count_cre_m]['name'] = $get_media_type['name'];
									$new_medi_tagged[$count_cre_m]['media_from'] = "media_creator_from_tag";
									$new_tagged_from_media_array[] = $new_medi_tagged[$count_cre_m];
								}
							}
							/*This function will display the user in which profile he is from or creator and tagged and display media where that profile was creator Ends here*/
							
							/*This function will display in which media the curent user was creator*/
							$get_where_creator_media = $new_profile_media_classobj->get_media_where_creator_or_from();
							$new_creator_media_array = array();
							if(!empty($get_where_creator_media))
							{
								for($count_tag_m=0;$count_tag_m<count($get_where_creator_media);$count_tag_m++)
								{
									$get_media_type = $new_profile_media_classobj->sel_type_of_tagged_media($get_where_creator_media[$count_tag_m]['media_type']);
									if($get_where_creator_media[$count_tag_m]['media_type'] == 114)
									{
										$get_media_track = $new_profile_media_classobj->get_media_track($get_where_creator_media[$count_tag_m]['id']); 
										$get_where_creator_media[$count_tag_m]["track"] = $get_media_track['track'];
									}
									$get_where_creator_media[$count_tag_m]['name'] = $get_media_type['name'];
									$get_where_creator_media[$count_tag_m]['media_from'] = "media_creator";
									$new_creator_media_array[] = $get_where_creator_media[$count_tag_m];
								}
							}
							/*This function will display in which media the curent user was creator ends here*/
							$cre_frm_med = array();
							$cre_frm_med = $new_profile_media_classobj->get_media_create();
							
							$new_subscription_media = array();
							$get_subscription_media_res = $new_profile_media_classobj->get_subscription_media();
							for($count_sub=0;$count_sub<=count($get_subscription_media_res);$count_sub++)
							{
								if($get_subscription_media_res[$count_sub]!="")
								{
									$get_media_type = $new_profile_media_classobj->sel_type_of_tagged_media($get_subscription_media_res[$count_sub]['media_type']);
									if($get_subscription_media_res[$count_sub]['media_type']==114)
									{
										$get_media_track_res = $new_profile_media_classobj->get_media_track($get_subscription_media_res[$count_sub]['id']); 
										$get_subscription_media_res[$count_sub]['track'] = $get_media_track_res['track'];
									}
									$get_subscription_media_res[$count_sub]['name'] = $get_media_type['name'];
									$get_subscription_media_res[$count_sub]['media_from'] = "email_subscription";
									$new_subscription_media[] = $get_subscription_media_res[$count_sub];
								}
							}
							if($member_var==0){
								$new_subscription_media = array();
							}
							$combine_arr[] = array_merge ($my_media_arr,$new_tag_media_arr,$new_edi_gal_media_arr,$new_fan_media_array,$new_creator_media_array,$new_creator_creator_media_array,$new_from_from_media_array,$new_tagged_from_media_array,$cre_frm_med,$get_songs_final_ct,$new_subscription_media,$new_buyed_media_array);
							$sort = array();
							foreach($combine_arr[0] as $k=>$v) {
							
								//$create_c = strpos($v['creator'],'(');
								//$end_c1 = substr($v['creator'],0,$create_c);
								$end_c1 = $v['creator'];
								$end_c = trim($end_c1);
								
								//$create_f = strpos($v['from'],'(');
								//$end_f1 = substr($v['from'],0,$create_f);
								$end_f1 = $v['from'];
								$end_f = trim($end_f1);
								
								$sort['creator'][$k] = strtolower($end_c);
								
								$sort['from'][$k] = strtolower($end_f);
								$sort['title'][$k] = strtolower($v['title']);
								if($v['name']=="Galleries/Images")
								{
									$sort['name'][$k] = "images";
								}
								else{
									$sort['name'][$k] = strtolower($v['name']);
								} 
								
							}
							//array_multisort($sort['creator'], SORT_ASC, $sort['from'], SORT_ASC, $sort['track'], SORT_NUMERIC ,SORT_ASC,$sort['title'], SORT_ASC,$combine_arr[0]);
							
							if($_GET['disp_val']=="DESC"){
								if($_GET['disp_type']=="type")
								{
									array_multisort($sort['name'], SORT_DESC , $combine_arr[0]);
								}else{
									array_multisort($sort[$_GET['disp_type']], SORT_DESC , $combine_arr[0]);
								}
							}if($_GET['disp_val']=="ASC"){
								if($_GET['disp_type']=="type")
								{
									//var_dump($sort['name']);
									array_multisort($sort['name'], SORT_ASC , $combine_arr[0]);
								}else{
									array_multisort($sort[$_GET['disp_type']], SORT_ASC , $combine_arr[0]);
								}
							}
							$dummy_pr = array();
							$test_count = 0;
							$get_all_del_id = $new_profile_media_classobj->get_all_del_media_id();
							$exp_all_del_id = explode(",",$get_all_del_id);
							for($count_all=0;$count_all<=count($combine_arr[$test_count]);$count_all++)
							{
								for($count_all_del=0;$count_all_del<=count($exp_all_del_id);$count_all_del++)
								{
									if($exp_all_del_id[$count_all_del]==""){continue;}
									else{
										if($exp_all_del_id[$count_all_del]==$combine_arr[$test_count][$count_all]['id'])
										{
											$combine_arr[$test_count][$count_all]="";
										}
									}
								}
							}
							for($newtest_count = 0;$newtest_count<=count($combine_arr[$test_count]);$newtest_count++){
								if(in_array($combine_arr[$test_count][$newtest_count]['id'],$dummy_pr))
							    {
								
							    }
							    else
							    {
									$dummy_pr[] = $combine_arr[$test_count][$newtest_count]['id'];
								
								//if($combine_arr[$test_count][$newtest_count]['name'] !="")
								//{
								//echo $combine_arr[$test_count][$newtest_count]['media_from'];
								if($combine_arr[$test_count][$newtest_count]['media_from']=="my_media")
								{
								?>
									<div class="tableCont" id="AccordionContainer">
									<div class="blkD" style="width:60px;"><?php if($combine_arr[$test_count][$newtest_count]['name']=="Galleries/Images"){echo "Images";}else{echo $combine_arr[$test_count][$newtest_count]['name'];} ?></div>
									<div class="blkI" style="width:127px;"><?php
									$exp_creator_info = explode("|",$combine_arr[$test_count][$newtest_count]['creator_info']);
									$pro_url = $new_profile_media_classobj->get_creator_url($exp_creator_info[0],$exp_creator_info[1]);
									?>
									<a href="<?php if($pro_url['profile_url'] !=""){ echo "/".$pro_url['profile_url']; }else { echo "javascript:void(0)"; }?>"><?php
									if($combine_arr[$test_count][$newtest_count]['creator']!="")
									{
										//$end_pos = strpos($combine_arr[$test_count][$newtest_count]['creator'],'(');
										//if($end_pos !="" || $end_pos != false){
										//echo substr($combine_arr[$test_count][$newtest_count]['creator'],0,$end_pos);}else{
										echo $combine_arr[$test_count][$newtest_count]['creator'];
										//}
									}else{
									?>&nbsp;<?php
									}
										?></a></div>
										
									<div class="blkK"><?php
									$exp_from_info = explode("|",$combine_arr[$test_count][$newtest_count]['from_info']);
									$pro_url_from = $new_profile_media_classobj->get_from_url($exp_from_info[0],$exp_from_info[1]);
									?>
									<a href="<?php if($pro_url_from['profile_url'] !=""){ echo "/".$pro_url_from['profile_url']; }else { echo "javascript:void(0)"; }?>"><?php 
									if($combine_arr[$test_count][$newtest_count]['from'] !="")
									{
										//$end_pos_from = strpos($combine_arr[$test_count][$newtest_count]['from'],'(');
										//if($end_pos_from != "" || $end_pos_from !=false){
										//echo substr($combine_arr[$test_count][$newtest_count]['from'],0,$end_pos_from);}else{
										echo $combine_arr[$test_count][$newtest_count]['from'];
										//}
									}else{
									?>&nbsp;<?php
									}
										?></a></div>
									<div class="blkB"><?php
									if($combine_arr[$test_count][$newtest_count]['title']!="")
									{
										if($combine_arr[$test_count][$newtest_count]['media_type'] == 114)
										{
											//$get_media_track = $new_profile_media_classobj->get_media_track($combine_arr[$test_count][$newtest_count]['id']); 
											if($combine_arr[$test_count][$newtest_count]['track'] != "" && $combine_arr[$test_count][$newtest_count]['track'] != NULL && $combine_arr[$test_count][$newtest_count]['track'] !=" "){
												echo $combine_arr[$test_count][$newtest_count]['track'].".".stripslashes($combine_arr[$test_count][$newtest_count]['title']);
											}else{
												echo stripslashes($combine_arr[$test_count][$newtest_count]['title']);
											}
										}
										else{
										echo stripslashes($combine_arr[$test_count][$newtest_count]['title']); }
									}else{
									?>&nbsp;<?php
									}
										?></div>
									<div class="blkplayer"><div class="player">
									<?php
										if($combine_arr[$test_count][$newtest_count]['media_type'] == 114){
										?>
										<a href="javascript:void(0);"><img title="Play file now." src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" onclick="showPlayer('a','<?php if($combine_arr[$test_count][$newtest_count]['id']!=""){echo $combine_arr[$test_count][$newtest_count]['id'];}else{echo $combine_arr[$test_count][$newtest_count]['id'];}?>','play');"/></a>
										<a href="javascript:void(0);"><img title="Add file to the bottom of your playlist without stopping current media." src="images/profile/add.gif"  onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'" onclick="showPlayer('a','<?php if($combine_arr[$test_count][$newtest_count]['id']!=""){echo $combine_arr[$test_count][$newtest_count]['id'];}else{echo $combine_arr[$test_count][$newtest_count]['id'];}?>','add');"/></a>
										<?php
										}
										else if($combine_arr[$test_count][$newtest_count]['media_type'] == 113){
											$get_images = $new_profile_media_classobj ->get_all_gallery_images($combine_arr[$test_count][$newtest_count]['id']);
											for($count_img=0;$count_img<=count($get_images);$count_img++){
												if($get_all_img==""){
													$get_all_img = $get_images[$count_img]['image_name'];
												}else{
													$get_all_img = $get_all_img.",".$get_images[$count_img]['image_name'];
												}
												if($galleryImagestitle==""){
													$galleryImagestitle = $get_images[$count_img]['image_title'];
												}else{
													$galleryImagestitle = $galleryImagestitle.",".$get_images[$count_img]['image_title'];
												}
												$mediaSrc = $get_images[0]['image_name'];
											}
											$gal_id = $get_images[$count_img]['media_id'];
											$get_other_user_id = $user_keys_data->get_general_user_idby_mediaid($combine_arr[$test_count][$newtest_count]['id']);
											$orgPath = $s3->getNewURI_otheruser("medgallery",$get_other_user_id);//"https://medgallery.s3.amazonaws.com/";
											$thumbPath = $s3->getNewURI_otheruser("medgalthumb",$get_other_user_id);//"https://medgalthumb.s3.amazonaws.com/";
										?>
										<a href="javascript:void(0);"><img title="Play file now." src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" onclick="checkload('<?php echo $mediaSrc;?>','<?php echo $get_all_img; ?>','<?php echo $thumbPath; ?>','<?php echo $orgPath; ?>','<?php echo $galleryImagestitle; ?>','<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>')"/></a>
										<!--<a href="javascript:void(0);"><img title="Add file to the bottom of your playlist without stopping current media." src="images/profile/add.gif"  onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'"/></a>-->
										<?php
											$get_all_img ="";
											$galleryImagestitle="";
											$mediaSrc="";
										}
										else if($combine_arr[$test_count][$newtest_count]['media_type'] == 115){
										?>
										<a href="javascript:void(0);"><img title="Play file now." src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" onclick="showPlayer('v','<?php if($combine_arr[$test_count][$newtest_count]['id']!=""){echo $combine_arr[$test_count][$newtest_count]['id'];}else{ echo $combine_arr[$test_count][$newtest_count]['id'];}?>','playlist');"/></a>
										<a href="javascript:void(0);"><img title="Add file to the bottom of your playlist without stopping current media." src="images/profile/add.gif"  onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'" onclick="showPlayer('v','<?php if($combine_arr[$test_count][$newtest_count]['id']!=""){echo $combine_arr[$test_count][$newtest_count]['id'];}else{ echo $combine_arr[$test_count][$newtest_count]['id'];}?>','addvi');"/></a>
										<?php
										}else if($combine_arr[$test_count][$newtest_count]['media_type'] == 116){
											$get_channel_media = $new_profile_media_classobj ->get_all_media_selected_channel($combine_arr[$test_count][$newtest_count]['id']);
										?>
											<input type="hidden" id="channel_media_song<?php echo $newtest_count;?>" value="<?php echo $get_channel_media['songs']; ?>"/>
											<input type="hidden" id="channel_media_video<?php echo $newtest_count;?>" value="<?php echo $get_channel_media['videos']; ?>"/>
											<a href="javascript:void(0);"><img title="Play file now." src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" onclick="PlayAll('channel_media_video<?php echo $newtest_count;?>','channel_media_song<?php echo $newtest_count;?>')"/></a>
											<a href="javascript:void(0);"><img title="Add file to the bottom of your playlist without stopping current media." src="images/profile/add.gif"  onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'" onclick="showPlayer('v','<?php if($combine_arr[$test_count][$newtest_count]['id']!=""){echo $combine_arr[$test_count][$newtest_count]['id'];}else{ echo $combine_arr[$test_count][$newtest_count]['id'];}?>','addvi');"/></a>
										<?php
										}
										else{
										?>
										<a href="javascript:void(0);"><img title="Play file now." src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" /></a>
										<a href="javascript:void(0);"><img title="Add file to the bottom of your playlist without stopping current media." src="images/profile/add.gif"  onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'"/></a>
										<?php
										}
										if(($combine_arr[$test_count][$newtest_count]['sharing_preference']==5 || $combine_arr[$test_count][$newtest_count]['sharing_preference']==2) && $combine_arr[$test_count][$newtest_count]['media_type'] == 114){
											?>
											<a onclick="download_directly_song('<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['title']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['creator']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['from']);?>')" href="javascript:void(0);">
											<img title="Add media to your cart for free download or purchase." onmouseout="this.src='../images/profile/download_down.gif'" onmouseover="this.src='../images/profile/download-over_down.gif'" src="../images/profile/download_down.gif"></a>
										<?php
										}/* else{
										?>
										<img src="images/profile/download-over_down.gif" />
										<?php
										} */
										?>
									</div>
									</div>
									<div class="icon" onclick="runAccordion(<?php echo $combine_arr[$test_count][$newtest_count]['id']; ?>);">
										<div onselectstart="return false;"><img title="View details for media." src="images/profile/view.jpg" /></div>
									</div>
									<div class="icon"><a href="add_media.php?edit=<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>" onclick="save_cookie()"><img title="Edit media." src="images/profile/edit.png" /></a></div>
									<div class="icon"><a onclick="return confirmdelete('<?php echo $combine_arr[$test_count][$newtest_count]['title'];?>','<?php if($combine_arr[$test_count][$newtest_count]['name']=="Galleries/Images"){echo "Images";}else{echo $combine_arr[$test_count][$newtest_count]['name'];} ?>')" href="delete_media.php?id=<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>&whos_media=my_media"><img title="Remove media from your library." src="images/profile/delete.png" /></a></div>
									<div class="expandable" id="Accordion<?php echo $combine_arr[$test_count][$newtest_count]['id']; ?>Content">
										<div class="affiBlock">
											<div class="RowCont">
												<div class="sideL">Plays :</div>
												<div class="sideR"><a href="" target="_blank"><?php if($combine_arr[$test_count][$newtest_count]['play_count']!=""){ echo $combine_arr[$test_count][$newtest_count]['play_count'];}else{ echo "0";}?></a></div>
											</div>
											<?php
												if($combine_arr[$test_count][$newtest_count]['sharing_preference']==5 || $combine_arr[$test_count][$newtest_count]['sharing_preference']==2){
												?>
												<div class="RowCont">
													<div class="sideL">Downloads :</div>
													<div class="sideR"><a href="javascript:void(0);"><?php echo $combine_arr[$test_count][$newtest_count]['download_count'];?></a></div>
												</div>
												<?php
												}

												if($combine_arr[$test_count][$newtest_count]['media_type']=='113')
												{
												
											?>
											<div class="RowCont">
												<div class="sideL">Image :</div>
												<div class="sideR"><a id="gallery_images<?php echo $newtest_count; ?>" class="fancybox fancybox.ajax" href="gallery_images.php?gal_id=<?php echo $combine_arr[$test_count][$newtest_count]['id']; ?>">Edit Image</a></div>
											</div>	
											<div class="RowCont">
												<div class="sideL"></div>
												<?php 
												$find_gal = $new_profile_media_classobj->get_gallery_editable($combine_arr[$test_count][$newtest_count]['id']);
												if($find_gal['gallery_id'] !=0)
												{
												?>
													<div class="sideR"><a id="Add_gallery_images<?php echo $newtest_count; ?>" class="fancybox fancybox.ajax" href="add_gallery_images.php?gal_id=<?php echo $combine_arr[$test_count][$newtest_count]['id']; ?>">Add Image</a></div>
												<?php
												}
												?>
											</div>
											<?php
												
												}
												else
												{
											?>	
											<!--<div class="RowCont">
												<div class="sideL">Media URL :</div>
												<div class="sideR"><a href="" target="_blank">www.<?php echo $domainname; ?></a></div>
											</div>-->
											<?php
												}
											?>
										</div>
									</div>
								</div>
									<?php
									$k = $k+1;
								}
								
								else if($combine_arr[$test_count][$newtest_count]['media_from']=="accepted_media")
								{
									if(isset($_GET['sel_val']) && $_GET['sel_val']!='all media')
									{
										$chk_tag_user = $new_profile_media_classobj->get_loggedin_info($_SESSION['login_email']);
										if($chk_tag_user['general_user_id']!=$combine_arr[$test_count][$newtest_count]['general_user_id'])
										{
											if($_GET['sel_val']==$combine_arr[$test_count][$newtest_count]['media_type'])
											{
												$get_type_of_tagged_media=$new_profile_media_classobj->sel_type_of_tagged_media($combine_arr[$test_count][$newtest_count]['media_type']);
									?>
											<div class="tableCont" id="AccordionContainer">
												<div class="blkD" style="width:60px;"><?php if($get_type_of_tagged_media['name']!=""){if($get_type_of_tagged_media['name'] =="Galleries/Images"){echo "Images";}else{echo $get_type_of_tagged_media['name'];}}else{?>&nbsp;<?php } ?></div>
												<div class="blkI" style="width:127px;"><?php
												$exp_creator_info = explode("|",$combine_arr[$test_count][$newtest_count]['creator_info']);
												$pro_url = $new_profile_media_classobj->get_creator_url($exp_creator_info[0],$exp_creator_info[1]);
												?>
												<a href="<?php if($pro_url['profile_url'] !=""){ echo "/".$pro_url['profile_url']; }else { echo "javascript:void(0)"; }?>">
												<?php if($combine_arr[$test_count][$newtest_count]['creator']!=""){
												//$endpos = strpos($combine_arr[$test_count][$newtest_count]['creator'],"(");
												//if($endpos !="" || $endpos !=false) {
												//echo substr($combine_arr[$test_count][$newtest_count]['creator'],0,$endpos);} else {
												echo $combine_arr[$test_count][$newtest_count]['creator']; }
												else{?>&nbsp;<?php } ?></a></div>
												<div class="blkK"><?php
												$exp_from_info = explode("|",$combine_arr[$test_count][$newtest_count]['from_info']);
												$pro_url_from = $new_profile_media_classobj->get_from_url($exp_from_info[0],$exp_from_info[1]);
												?>
												<a href="<?php if($pro_url_from['profile_url'] !=""){ echo "/".$pro_url_from['profile_url']; }else { echo "javascript:void(0)"; }?>">
												<?php if($combine_arr[$test_count][$newtest_count]['from']!=""){
												//$endpos = strpos($combine_arr[$test_count][$newtest_count]['from'],"(");
												//if($endpos !="" || $endpos != false) {
												//echo substr($combine_arr[$test_count][$newtest_count]['from'],0,$endpos);} else {
												echo $combine_arr[$test_count][$newtest_count]['from']; }
												else{?>&nbsp;<?php } ?></a></div>
												<div class="blkB"><?php
												if($combine_arr[$test_count][$newtest_count]['title']!="")
												{
													if($combine_arr[$test_count][$newtest_count]['media_type'] == 114)
													{
														//$get_media_track = $new_profile_media_classobj->get_media_track($combine_arr[$test_count][$newtest_count]['id']); 
														if($combine_arr[$test_count][$newtest_count]['track'] != "" && $combine_arr[$test_count][$newtest_count]['track'] != NULL && $combine_arr[$test_count][$newtest_count]['track'] !=" "){
															echo $combine_arr[$test_count][$newtest_count]['track'].".".stripslashes($combine_arr[$test_count][$newtest_count]['title']);
														}else{
															echo stripslashes($combine_arr[$test_count][$newtest_count]['title']);
														}
													}
													else{
													echo stripslashes($combine_arr[$test_count][$newtest_count]['title']); }
												}else{
												?>&nbsp;<?php
												}
													?></div>
												<div class="blkplayer"><div class="player">
												<?php 
													if($combine_arr[$test_count][$newtest_count]['media_type']==113){
														$get_images_tag = $new_profile_media_classobj ->get_all_gallery_images($combine_arr[$test_count][$newtest_count]['id']);
														for($count_img=0;$count_img<=count($get_images_tag);$count_img++){
															if($get_all_img_tag ==""){
																$get_all_img_tag = $get_images_tag[$count_img]['image_name'];
															}else{
																$get_all_img_tag = $get_all_img_tag.",".$get_images_tag[$count_img]['image_name'];
															}
															if($galleryImagestitle_tag ==""){
																$galleryImagestitle_tag = $get_images_tag[$count_img]['image_title'];
															}else{
																$galleryImagestitle_tag = $galleryImagestitle_tag.",".$get_images_tag[$count_img]['image_title'];
															}
															$mediaSrc_tag = $get_images_tag[0]['image_name'];
														}
														$gal_id_tag = $get_images_tag[$count_img]['media_id'];
														$get_other_user_id = $user_keys_data->get_general_user_idby_mediaid($combine_arr[$test_count][$newtest_count]['id']);
														$orgPath_tag = $s3->getNewURI_otheruser("medgallery",$get_other_user_id);//"https://medgallery.s3.amazonaws.com/";
														$thumbPath_tag = $s3->getNewURI_otheruser("medgalthumb",$get_other_user_id);//"https://medgalthumb.s3.amazonaws.com/";
											
													?>
													<a href="javascript:void(0);"><img title="Play file now." src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" onclick="checkload('<?php echo $mediaSrc_tag;?>','<?php echo $get_all_img_tag; ?>','<?php echo $thumbPath_tag; ?>','<?php echo $orgPath_tag; ?>','<?php echo $galleryImagestitle_tag; ?>','<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>')"/></a>
													<!--<a href="javascript:void(0);"><img title="Add file to the bottom of your playlist without stopping current media." src="images/profile/add.gif"  onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'"/></a>-->
													<?php
														$get_all_img_tag ="";
														$galleryImagestitle_tag="";
														$mediaSrc_tag="";
													}else if($combine_arr[$test_count][$newtest_count]['media_type']==114){
													?>
													<a href="javascript:void(0);"><img title="Play file now." src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" onclick="showPlayer('a','<?php if($combine_arr[$test_count][$newtest_count]['id']!=""){echo $combine_arr[$test_count][$newtest_count]['id'];}else{echo $combine_arr[$test_count][$newtest_count]['id'];}?>','play');"/></a>
													<a href="javascript:void(0);"><img title="Add file to the bottom of your playlist without stopping current media." src="images/profile/add.gif"  onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'" onclick="showPlayer('a','<?php if($combine_arr[$test_count][$newtest_count]['id']!=""){echo $combine_arr[$test_count][$newtest_count]['id'];}else{echo $combine_arr[$test_count][$newtest_count]['id'];}?>','add');"/></a>
													<?php
													}else if($combine_arr[$test_count][$newtest_count]['media_type']==115){
													?>
													<a href="javascript:void(0);"><img title="Play file now." src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" onclick="showPlayer('v','<?php if($combine_arr[$test_count][$newtest_count]['id']!=""){echo $combine_arr[$test_count][$newtest_count]['id'];}else{ echo $combine_arr[$test_count][$newtest_count]['id'];}?>','playlist');"/></a>
													<a href="javascript:void(0);"><img title="Add file to the bottom of your playlist without stopping current media." src="images/profile/add.gif"  onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'" onclick="showPlayer('v','<?php if($combine_arr[$test_count][$newtest_count]['id']!=""){echo $combine_arr[$test_count][$newtest_count]['id'];}else{ echo $combine_arr[$test_count][$newtest_count]['id'];}?>','addvi');"/></a>
													<?php
													}else if($combine_arr[$test_count][$newtest_count]['media_type'] == 116){
														$get_channel_media = $new_profile_media_classobj ->get_all_media_selected_channel($combine_arr[$test_count][$newtest_count]['id']);
													?>
														<input type="hidden" id="channel_media_song<?php echo $newtest_count;?>" value="<?php echo $get_channel_media['songs']; ?>"/>
														<input type="hidden" id="channel_media_video<?php echo $newtest_count;?>" value="<?php echo $get_channel_media['videos']; ?>"/>
														<a href="javascript:void(0);"><img title="Play file now." src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" onclick="PlayAll('channel_media_video<?php echo $newtest_count;?>','channel_media_song<?php echo $newtest_count;?>')"/></a>
														<a href="javascript:void(0);"><img title="Add file to the bottom of your playlist without stopping current media." src="images/profile/add.gif"  onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'" onclick="showPlayer('v','<?php if($combine_arr[$test_count][$newtest_count]['id']!=""){echo $combine_arr[$test_count][$newtest_count]['id'];}else{ echo $combine_arr[$test_count][$newtest_count]['id'];}?>','addvi');"/></a>
													<?php
													}
													else{
													?>
													<a href="javascript:void(0);"><img title="Play file now." src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" /></a>
													<a href="javascript:void(0);"><img title="Add file to the bottom of your playlist without stopping current media." src="images/profile/add.gif"  onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'"/></a>
													<?php
													}
													
													if(($combine_arr[$test_count][$newtest_count]['sharing_preference']==5 || $combine_arr[$test_count][$newtest_count]['sharing_preference']==2) && $combine_arr[$test_count][$newtest_count]['media_type'] == 114){
														?>
														<a onclick="download_directly_song('<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['title']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['creator']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['from']);?>')" href="javascript:void(0);">
														<img title="Add media to your cart for free download or purchase." onmouseout="this.src='../images/profile/download_down.gif'" onmouseover="this.src='../images/profile/download-over_down.gif'" src="../images/profile/download_down.gif"></a>
													<?php
													}/* else{
													?>
													<img src="images/profile/download-over_down.gif" />
													<?php
													} */
													?>

												</div>
												</div>
												<div class="icon" onclick="runAccordion(<?php echo $combine_arr[$test_count][$newtest_count]['id']; ?>);">
													<div onselectstart="return false;"><img title="View details for media." src="images/profile/view.jpg" /></div>
												</div>
												<div class="icon">&nbsp;</div>
												<div class="icon"><a href="delete_media.php?id=<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>&type=<?php if($get_type_of_tagged_media['name'] =="Galleries/Images"){echo "Images";}else{echo $get_type_of_tagged_media['name'];}?>" onclick="return confirmdelete('<?php echo $combine_arr[$test_count][$newtest_count]['title'];?>','<?php if($combine_arr[$test_count][$newtest_count]['name']=="Galleries/Images"){echo "Images";}else{echo $combine_arr[$test_count][$newtest_count]['name'];} ?>')"><img title="Remove media from your library." src="images/profile/delete.png" /></a></div>
												<div class="expandable" id="Accordion<?php echo $combine_arr[$test_count][$newtest_count]['id']; ?>Content">
													<div class="affiBlock">
														<div class="RowCont">
															<div class="sideL">Plays :</div>
															<div class="sideR"><a href="" target="_blank"><?php if($combine_arr[$test_count][$newtest_count]['play_count']!=""){ echo $combine_arr[$test_count][$newtest_count]['play_count'];}else{ echo "0";}?></a></div>
														</div>
														<?php
															if($combine_arr[$test_count][$newtest_count]['sharing_preference']==5 || $combine_arr[$test_count][$newtest_count]['sharing_preference']==2){
															?>
															<div class="RowCont">
																<div class="sideL">Downloads :</div>
																<div class="sideR"><a href="javascript:void(0);"><?php echo $combine_arr[$test_count][$newtest_count]['download_count'];?></a></div>
															</div>
															<?php
															}
															if($combine_arr[$test_count][$newtest_count]['media_type']=='113')
															{
															
															}
															else
															{
														?>	
														<!--<div class="RowCont">
															<div class="sideL">Media URL :</div>
															<div class="sideR"><a href="" target="_blank">www.<?php echo $domainname; ?></a></div>
														</div>-->
														<?php
															}
														?>
													</div>
												</div>
											</div>
									<?php
											}
										}
									}
									else
									{
										$chk_tag_user = $new_profile_media_classobj->get_loggedin_info($_SESSION['login_email']);
										if($chk_tag_user['general_user_id']!=$combine_arr[$test_count][$newtest_count]['general_user_id'])
										{
											$get_type_of_tagged_media=$new_profile_media_classobj->sel_type_of_tagged_media($combine_arr[$test_count][$newtest_count]['media_type']);
									?>
											<div class="tableCont" id="AccordionContainer">
												<div class="blkD" style="width:60px;"><?php if($get_type_of_tagged_media['name']!=""){if($get_type_of_tagged_media['name'] =="Galleries/Images"){echo "Images";}else{echo $get_type_of_tagged_media['name'];}}else{?>&nbsp;<?php } ?></div>
												<div class="blkI" style="width:127px;"><?php
												$exp_creator_info = explode("|",$combine_arr[$test_count][$newtest_count]['creator_info']);
												$pro_url = $new_profile_media_classobj->get_creator_url($exp_creator_info[0],$exp_creator_info[1]);
												?>
												<a href="<?php if($pro_url['profile_url'] !=""){ echo "/".$pro_url['profile_url']; }else { echo "javascript:void(0)"; }?>">
												<?php if($combine_arr[$test_count][$newtest_count]['creator']!=""){
												//$endpos = strpos($combine_arr[$test_count][$newtest_count]['creator'],"(");
												//if($endpos !="" || $endpos !=false) {
												//echo substr($combine_arr[$test_count][$newtest_count]['creator'],0,$endpos);} else {
												echo $combine_arr[$test_count][$newtest_count]['creator']; }
												else{?>&nbsp;<?php } ?></a></div>
												<div class="blkK"><?php
												$exp_from_info = explode("|",$combine_arr[$test_count][$newtest_count]['from_info']);
												$pro_url_from = $new_profile_media_classobj->get_from_url($exp_from_info[0],$exp_from_info[1]);
												?>
												<a href="<?php if($pro_url_from['profile_url'] !=""){ echo "/".$pro_url_from['profile_url']; }else { echo "javascript:void(0)"; }?>">
												<?php if($combine_arr[$test_count][$newtest_count]['from']!=""){
												//$endpos = strpos($combine_arr[$test_count][$newtest_count]['from'],"(");
												//if($endpos !="" || $endpos != false) {
												//echo substr($combine_arr[$test_count][$newtest_count]['from'],0,$endpos);} else {
												echo $combine_arr[$test_count][$newtest_count]['from']; }
												else{?>&nbsp;<?php } ?></a></div>
												<div class="blkB"><?php
												if($combine_arr[$test_count][$newtest_count]['title']!="")
												{
													if($combine_arr[$test_count][$newtest_count]['media_type'] == 114)
													{
														//$get_media_track = $new_profile_media_classobj->get_media_track($combine_arr[$test_count][$newtest_count]['id']); 
														if($combine_arr[$test_count][$newtest_count]['track'] != "" && $combine_arr[$test_count][$newtest_count]['track'] != NULL && $combine_arr[$test_count][$newtest_count]['track'] !=" "){
															echo $combine_arr[$test_count][$newtest_count]['track'].".".stripslashes($combine_arr[$test_count][$newtest_count]['title']);
														}else{
															echo stripslashes($combine_arr[$test_count][$newtest_count]['title']);
														}
													}
													else{
													echo stripslashes($combine_arr[$test_count][$newtest_count]['title']); }
												}else{
												?>&nbsp;<?php
												}
													?></div>
												<div class="blkplayer"><div class="player">
												<?php 
													if($combine_arr[$test_count][$newtest_count]['media_type']==113){
														$get_images_tag = $new_profile_media_classobj ->get_all_gallery_images($combine_arr[$test_count][$newtest_count]['id']);
														for($count_img=0;$count_img<=count($get_images_tag);$count_img++){
															if($get_all_img_tag ==""){
																$get_all_img_tag = $get_images_tag[$count_img]['image_name'];
															}else{
																$get_all_img_tag = $get_all_img_tag.",".$get_images_tag[$count_img]['image_name'];
															}
															if($galleryImagestitle_tag ==""){
																$galleryImagestitle_tag = $get_images_tag[$count_img]['image_title'];
															}else{
																$galleryImagestitle_tag = $galleryImagestitle_tag.",".$get_images_tag[$count_img]['image_title'];
															}
															$mediaSrc_tag = $get_images_tag[0]['image_name'];
														}
														$gal_id_tag = $get_images_tag[$count_img]['media_id'];
														$get_other_user_id = $user_keys_data->get_general_user_idby_mediaid($combine_arr[$test_count][$newtest_count]['id']);
														$orgPath_tag = $s3->getNewURI_otheruser("medgallery",$get_other_user_id);//"https://medgallery.s3.amazonaws.com/";
														$thumbPath_tag = $s3->getNewURI_otheruser("medgalthumb",$get_other_user_id);//"https://medgalthumb.s3.amazonaws.com/";
														
													?>
													<a href="javascript:void(0);"><img title="Play file now." src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" onclick="checkload('<?php echo $mediaSrc_tag;?>','<?php echo $get_all_img_tag; ?>','<?php echo $thumbPath_tag; ?>','<?php echo $orgPath_tag; ?>','<?php echo $galleryImagestitle_tag; ?>','<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>')"/></a>
													<!--<a href="javascript:void(0);"><img title="Add file to the bottom of your playlist without stopping current media." src="images/profile/add.gif"  onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'"/></a>-->
													<?php
														$get_all_img_tag ="";
														$galleryImagestitle_tag="";
														$mediaSrc_tag="";
													}else if($combine_arr[$test_count][$newtest_count]['media_type']==114){
													?>
													<a href="javascript:void(0);"><img title="Play file now." src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" onclick="showPlayer('a','<?php if($combine_arr[$test_count][$newtest_count]['id']!=""){echo $combine_arr[$test_count][$newtest_count]['id'];}else{echo $combine_arr[$test_count][$newtest_count]['id'];}?>','play');"/></a>
													<a href="javascript:void(0);"><img title="Add file to the bottom of your playlist without stopping current media." src="images/profile/add.gif"  onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'" onclick="showPlayer('a','<?php if($combine_arr[$test_count][$newtest_count]['id']!=""){echo $combine_arr[$test_count][$newtest_count]['id'];}else{echo $combine_arr[$test_count][$newtest_count]['id'];}?>','add');"/></a>
													<?php
													}else if($combine_arr[$test_count][$newtest_count]['media_type']==115){
													?>
													<a href="javascript:void(0);"><img title="Play file now." src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" onclick="showPlayer('v','<?php if($combine_arr[$test_count][$newtest_count]['id']!=""){echo $combine_arr[$test_count][$newtest_count]['id'];}else{ echo $combine_arr[$test_count][$newtest_count]['id'];}?>','playlist');"/></a>
													<a href="javascript:void(0);"><img title="Add file to the bottom of your playlist without stopping current media." src="images/profile/add.gif"  onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'" onclick="showPlayer('v','<?php if($combine_arr[$test_count][$newtest_count]['id']!=""){echo $combine_arr[$test_count][$newtest_count]['id'];}else{ echo $combine_arr[$test_count][$newtest_count]['id'];}?>','addvi');"/></a>
													<?php
													}else if($combine_arr[$test_count][$newtest_count]['media_type'] == 116){
														$get_channel_media = $new_profile_media_classobj ->get_all_media_selected_channel($combine_arr[$test_count][$newtest_count]['id']);
													?>
														<input type="hidden" id="channel_media_song<?php echo $newtest_count;?>" value="<?php echo $get_channel_media['songs']; ?>"/>
														<input type="hidden" id="channel_media_video<?php echo $newtest_count;?>" value="<?php echo $get_channel_media['videos']; ?>"/>
														<a href="javascript:void(0);"><img title="Play file now." src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" onclick="PlayAll('channel_media_video<?php echo $newtest_count;?>','channel_media_song<?php echo $newtest_count;?>')"/></a>
														<a href="javascript:void(0);"><img title="Add file to the bottom of your playlist without stopping current media." src="images/profile/add.gif"  onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'" onclick="showPlayer('v','<?php if($combine_arr[$test_count][$newtest_count]['id']!=""){echo $combine_arr[$test_count][$newtest_count]['id'];}else{ echo $combine_arr[$test_count][$newtest_count]['id'];}?>','addvi');"/></a>
													<?php
													}
													else{
													?>
													<a href="javascript:void(0);"><img title="Play file now." src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" /></a>
													<a href="javascript:void(0);"><img title="Add file to the bottom of your playlist without stopping current media." src="images/profile/add.gif"  onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'"/></a>
													<?php
													}
													
													if(($combine_arr[$test_count][$newtest_count]['sharing_preference']==5 || $combine_arr[$test_count][$newtest_count]['sharing_preference']==2) && $combine_arr[$test_count][$newtest_count]['media_type'] == 114){
														?>
														<a onclick="download_directly_song('<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['title']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['creator']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['from']);?>')" href="javascript:void(0);">
														<img title="Add media to your cart for free download or purchase." onmouseout="this.src='../images/profile/download_down.gif'" onmouseover="this.src='../images/profile/download-over_down.gif'" src="../images/profile/download_down.gif"></a>
													<?php
													}/* else{
													?>
													<img src="images/profile/download-over_down.gif" />
													<?php
													} */
													?>
												</div>
												</div>
												<div class="icon" onclick="runAccordion(<?php echo $combine_arr[$test_count][$newtest_count]['id']; ?>);">
													<div onselectstart="return false;"><img title="View details for media." src="images/profile/view.jpg" /></div>
												</div>
												<div class="icon">&nbsp;</div>
												<div class="icon"><a href="delete_media.php?id=<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>&type=<?php if($get_type_of_tagged_media['name'] =="Galleries/Images"){echo "Images";}else{echo $get_type_of_tagged_media['name'];}?>" onclick="return confirmdelete('<?php echo $combine_arr[$test_count][$newtest_count]['title'];?>','<?php if($combine_arr[$test_count][$newtest_count]['name']=="Galleries/Images"){echo "Images";}else{echo $combine_arr[$test_count][$newtest_count]['name'];} ?>')"><img title="Remove media from your library." src="images/profile/delete.png" /></a></div>
												<div class="expandable" id="Accordion<?php echo $combine_arr[$test_count][$newtest_count]['id']; ?>Content">
													<div class="affiBlock">
														<div class="RowCont">
															<div class="sideL">Plays :</div>
															<div class="sideR"><a href="" target="_blank"><?php if($combine_arr[$test_count][$newtest_count]['play_count']!=""){ echo $combine_arr[$test_count][$newtest_count]['play_count'];}else{ echo "0";}?></a></div>
														</div>
														<?php
															if($combine_arr[$test_count][$newtest_count]['sharing_preference']==5 || $combine_arr[$test_count][$newtest_count]['sharing_preference']==2){
															?>
															<div class="RowCont">
																<div class="sideL">Downloads :</div>
																<div class="sideR"><a href="javascript:void(0);"><?php echo $combine_arr[$test_count][$newtest_count]['download_count'];?></a></div>
															</div>
															<?php
															}
															if($combine_arr[$test_count][$newtest_count]['media_type']=='113')
															{
															
															}
															else
															{
														?>	
														<!--<div class="RowCont">
															<div class="sideL">Media URL :</div>
															<div class="sideR"><a href="" target="_blank">www.<?php echo $domainname; ?></a></div>
														</div>-->
														<?php
															}
														?>
													</div>
												</div>
											</div>
									<?php
										}
									}
								}
								else if($combine_arr[$test_count][$newtest_count]['media_from']=="newedit_media")
								{
									$chk_tag_user = $new_profile_media_classobj->get_loggedin_info($_SESSION['login_email']);
									if($chk_tag_user['general_user_id']!=$combine_arr[$test_count][$newtest_count]['general_user_id'])
									{
									?>
										<div class="tableCont" id="AccordionContainer">
										<div class="blkD" style="width:60px;"><?php if($combine_arr[$test_count][$newtest_count]['name']!=""){ if($combine_arr[$test_count][$newtest_count]['name']=="Galleries/Images"){ echo "Images";}else {echo $combine_arr[$test_count][$newtest_count]['name'];}}else{?>&nbsp;<?php } ?></div>
										<div class="blkI" style="width:127px;"><?php
										$exp_creator_info = explode("|",$combine_arr[$test_count][$newtest_count]['creator_info']);
										$pro_url = $new_profile_media_classobj->get_creator_url($exp_creator_info[0],$exp_creator_info[1]);
										?>
										<a href="<?php if($pro_url['profile_url'] !=""){ echo "/".$pro_url['profile_url']; }else { echo "javascript:void(0)"; }?>">
										<?php if($combine_arr[$test_count][$newtest_count]['creator']!=""){
										//$endpos = strpos($combine_arr[$test_count][$newtest_count]['creator'],"(");
										//if($endpos !="" || $endpos !=false) {
										//echo substr($combine_arr[$test_count][$newtest_count]['creator'],0,$endpos);} else {
										echo $combine_arr[$test_count][$newtest_count]['creator']; }
										else{?>&nbsp;<?php } ?></a></div>
										<div class="blkK"><?php
										$exp_from_info = explode("|",$combine_arr[$test_count][$newtest_count]['from_info']);
										$pro_url_from = $new_profile_media_classobj->get_from_url($exp_from_info[0],$exp_from_info[1]);
										?>
										<a href="<?php if($pro_url_from['profile_url'] !=""){ echo "/".$pro_url_from['profile_url']; }else { echo "javascript:void(0)"; }?>">
										<?php if($combine_arr[$test_count][$newtest_count]['from']!=""){
										//$endpos = strpos($combine_arr[$test_count][$newtest_count]['from'],"(");
										//if($endpos !="" || $endpos != false) {
										//echo substr($combine_arr[$test_count][$newtest_count]['from'],0,$endpos);} else {
										echo $combine_arr[$test_count][$newtest_count]['from']; }
										else{?>&nbsp;<?php } ?></a></div>
										<!--<div class="blkK"><a href="gallery_images.php?gal_id=<?php //echo $res['id'];?>" id="gallery_images<?php //echo $k;?>">Edit Image</a></div>
										<div class="blkB"><a href="add_gallery_images.php?gal_id=<?php echo $res['id'];?>" id="Add_gallery_images<?php echo $k;?>">Add Image</a></div>-->
										<!--<div class="blkJ"><a href=""><img src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" /></a><a href=""><img src="images/profile/add.gif"  onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'"/></a><a href=""><img src="images/profile/download_down.gif" onmouseover="this.src='images/profile/download-over_down.gif'" onmouseout="this.src='images/profile/download_down.gif'" /></a></div>-->
										<div class="icon" onclick="runAccordion(<?php echo $combine_arr[$test_count][$newtest_count]['id']; ?>);">
											<div onselectstart="return false;"><img title="View details for media." src="images/profile/view.jpg" /></div>
										</div>
										<div class="icon"><a href="add_media.php?edit=<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>" onclick="save_cookie()"><img title="Edit media." src="images/profile/edit.png" /></a></div>
										<div class="icon"><a onclick="return confirmdelete('<?php echo $combine_arr[$test_count][$newtest_count]['title'];?>','<?php if($combine_arr[$test_count][$newtest_count]['name']=="Galleries/Images"){echo "Images";}else{echo $combine_arr[$test_count][$newtest_count]['name'];} ?>')" href="delete_media.php?id=<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>&whos_media=newedit_media"><img title="Remove media from your library." src="images/profile/delete.png" /></a></div>
										<div class="expandable" id="Accordion<?php echo $combine_arr[$test_count][$newtest_count]['id']; ?>Content">
											<div class="affiBlock">
												<div class="RowCont">
													<div class="sideL">Plays :</div>
													<div class="sideR"><a href="" target="_blank"><?php if($combine_arr[$test_count][$newtest_count]['play_count']!=""){ echo $combine_arr[$test_count][$newtest_count]['play_count'];}else{ echo "0";}?></a></div>
												</div>
												<?php
													if($combine_arr[$test_count][$newtest_count]['sharing_preference']==5 || $combine_arr[$test_count][$newtest_count]['sharing_preference']==2){
													?>
													<div class="RowCont">
														<div class="sideL">Downloads :</div>
														<div class="sideR"><a href="javascript:void(0);"><?php echo $combine_arr[$test_count][$newtest_count]['download_count'];?></a></div>
													</div>
													<?php
													}
													if($combine_arr[$test_count][$newtest_count]['media_type']=='113')
													{
													
													}
													else
													{
												?>	
												<!--<div class="RowCont">
													<div class="sideL">Media URL :</div>
													<div class="sideR"><a href="" target="_blank">www.<?php echo $domainname; ?></a></div>
												</div>-->
												<?php
													}
												?>
											</div>
										</div>
									</div>
									<?php
										$k=$k+1;
									}
								}
								else if($combine_arr[$test_count][$newtest_count]['media_from']=="media_creator")
								{
									?>
										<div class="tableCont" id="AccordionContainer">
										<div class="blkD" style="width:60px;"><?php if($combine_arr[$test_count][$newtest_count]['name']=="Galleries/Images"){echo "Images";}else{echo $combine_arr[$test_count][$newtest_count]['name'];} ?></div>
										<div class="blkI" style="width:127px;"><?php
										$exp_creator_info = explode("|",$combine_arr[$test_count][$newtest_count]['creator_info']);
										$pro_url = $new_profile_media_classobj->get_creator_url($exp_creator_info[0],$exp_creator_info[1]);
										?>
										<a href="<?php if($pro_url['profile_url'] !=""){ echo "/".$pro_url['profile_url']; }else { echo "javascript:void(0)"; }?>">
										<?php if($combine_arr[$test_count][$newtest_count]['creator']!=""){
										//$endpos = strpos($combine_arr[$test_count][$newtest_count]['creator'],"(");
										//if($endpos !="" || $endpos !=false) {
										//echo substr($combine_arr[$test_count][$newtest_count]['creator'],0,$endpos);} else {
										echo $combine_arr[$test_count][$newtest_count]['creator']; }
										else{?>&nbsp;<?php } ?></a></div>
										<div class="blkK"><?php
										$exp_from_info = explode("|",$combine_arr[$test_count][$newtest_count]['from_info']);
										$pro_url_from = $new_profile_media_classobj->get_from_url($exp_from_info[0],$exp_from_info[1]);
										?>
										<a href="<?php if($pro_url_from['profile_url'] !=""){ echo "/".$pro_url_from['profile_url']; }else { echo "javascript:void(0)"; }?>">
										<?php if($combine_arr[$test_count][$newtest_count]['from']!=""){
										//$endpos = strpos($combine_arr[$test_count][$newtest_count]['from'],"(");
										//if($endpos !="" || $endpos != false) {
										//echo substr($combine_arr[$test_count][$newtest_count]['from'],0,$endpos);} else {
										echo $combine_arr[$test_count][$newtest_count]['from']; }
										else{?>&nbsp;<?php } ?></a></div>
										<div class="blkB"><?php
												if($combine_arr[$test_count][$newtest_count]['title']!="")
												{
													if($combine_arr[$test_count][$newtest_count]['media_type'] == 114)
													{
														//$get_media_track = $new_profile_media_classobj->get_media_track($combine_arr[$test_count][$newtest_count]['id']); 
														if($combine_arr[$test_count][$newtest_count]['track'] != "" && $combine_arr[$test_count][$newtest_count]['track'] != NULL && $combine_arr[$test_count][$newtest_count]['track'] !=" "){
															echo $combine_arr[$test_count][$newtest_count]['track'].".".stripslashes($combine_arr[$test_count][$newtest_count]['title']);
														}else{
															echo stripslashes($combine_arr[$test_count][$newtest_count]['title']);
														}
													}
													else{
													echo stripslashes($combine_arr[$test_count][$newtest_count]['title']); }
												}else{
												?>&nbsp;<?php
												}
													?></div>
										<?php
										if($combine_arr[$test_count][$newtest_count]['media_type']==114)
										{
										?>
											<div class="blkplayer"><div class="player">
												<a href="javascript:void(0);"><img onclick="showPlayer('a','<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','play');" onmouseout="this.src='images/profile/play.gif'" onmouseover="this.src='images/profile/play-over.gif'" src="images/profile/play.gif" title="Play file now."></a>
												<a href="javascript:void(0);"><img onclick="showPlayer('a','<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','add');" onmouseout="this.src='images/profile/add.gif'" onmouseover="this.src='images/profile/add-over.gif'" src="images/profile/add.gif" title="Add file to the bottom of your playlist without stopping current media."></a>
												<?php
												if(($combine_arr[$test_count][$newtest_count]['sharing_preference']==5 || $combine_arr[$test_count][$newtest_count]['sharing_preference']==2) && $combine_arr[$test_count][$newtest_count]['media_type'] == 114){
													?>
													<a onclick="download_directly_song('<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['title']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['creator']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['from']);?>')" href="javascript:void(0);">
													<img title="Add media to your cart for free download or purchase." onmouseout="this.src='../images/profile/download_down.gif'" onmouseover="this.src='../images/profile/download-over_down.gif'" src="../images/profile/download_down.gif"></a>
												<?php
												}/* else{
												?>
												<img src="images/profile/download-over_down.gif" />
												<?php
												} */
												?>
											</div>
											</div>
										<?php
										}else if($combine_arr[$test_count][$newtest_count]['media_type']==115)
										{
										?>
											<div class="blkplayer"><div class="player">
												<a href="javascript:void(0);"><img onclick="showPlayer('v','<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','playlist');" onmouseout="this.src='images/profile/play.gif'" onmouseover="this.src='images/profile/play-over.gif'" src="images/profile/play.gif" title="Play file now."></a>
												<a href="javascript:void(0);"><img onclick="showPlayer('v','<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','addvi');" onmouseout="this.src='images/profile/add.gif'" onmouseover="this.src='images/profile/add-over.gif'" src="images/profile/add.gif" title="Add file to the bottom of your playlist without stopping current media."></a>
												<?php
												if(($combine_arr[$test_count][$newtest_count]['sharing_preference']==5 || $combine_arr[$test_count][$newtest_count]['sharing_preference']==2) && $combine_arr[$test_count][$newtest_count]['media_type'] == 114){
													?>
													<a onclick="download_directly_song('<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['title']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['creator']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['from']);?>')" href="javascript:void(0);">
													<img title="Add media to your cart for free download or purchase." onmouseout="this.src='../images/profile/download_down.gif'" onmouseover="this.src='../images/profile/download-over_down.gif'" src="../images/profile/download_down.gif"></a>
												<?php
												}/* else{
												?>
												<img src="images/profile/download-over_down.gif" />
												<?php
												} */
												?>
											</div>
											</div>
										<?php
										}else if($combine_arr[$test_count][$newtest_count]['media_type']==113){
											$get_images_fan = $new_profile_media_classobj ->get_all_gallery_images($combine_arr[$test_count][$newtest_count]['id']);
											for($count_img=0;$count_img<=count($get_images_fan);$count_img++){
												if($get_all_img_fan ==""){
													$get_all_img_fan = $get_images_fan[$count_img]['image_name'];
												}else{
													$get_all_img_fan = $get_all_img_fan.",".$get_images_fan[$count_img]['image_name'];
												}
												if($galleryImagestitle_fan ==""){
													$galleryImagestitle_fan = $get_images_fan[$count_img]['image_title'];
												}else{
													$galleryImagestitle_fan = $galleryImagestitle_fan.",".$get_images_fan[$count_img]['image_title'];
												}
												
												$mediaSrc_fan = $get_images_fan[0]['image_name'];
											}
											$gal_id_fan = $get_images_fan[$count_img]['media_id'];
											$get_other_user_id = $user_keys_data->get_general_user_idby_mediaid($combine_arr[$test_count][$newtest_count]['id']);
											$orgPath_fan = $s3->getNewURI_otheruser("medgallery",$get_other_user_id);//"https://medgallery.s3.amazonaws.com/";
											$thumbPath_fan = $s3->getNewURI_otheruser("medgalthumb",$get_other_user_id);//"https://medgalthumb.s3.amazonaws.com/";
											
										?>
											<div class="blkplayer"><div class="player">
											<a href="javascript:void(0);"><img title="Play file now." src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" onclick="checkload('<?php echo $mediaSrc_fan;?>','<?php echo $get_all_img_fan; ?>','<?php echo $thumbPath_fan; ?>','<?php echo $orgPath_fan; ?>','<?php echo $galleryImagestitle_fan; ?>','<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>')"/></a>
											<!--<a href="javascript:void(0);"><img title="Add file to the bottom of your playlist without stopping current media." src="images/profile/add.gif"  onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'"/></a>-->
											<?php
												if(($combine_arr[$test_count][$newtest_count]['sharing_preference']==5 || $combine_arr[$test_count][$newtest_count]['sharing_preference']==2) && $combine_arr[$test_count][$newtest_count]['media_type'] == 114){
													?>
													<a onclick="download_directly_song('<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['title']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['creator']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['from']);?>')" href="javascript:void(0);">
													<img title="Add media to your cart for free download or purchase." onmouseout="this.src='../images/profile/download_down.gif'" onmouseover="this.src='../images/profile/download-over_down.gif'" src="../images/profile/download_down.gif"></a>
												<?php
												}/* else{
												?>
												<img src="images/profile/download-over_down.gif" />
												<?php
												} */
												?>
											</div>
											</div>
										<?php
											$get_all_img_fan ="";
											$galleryImagestitle_fan="";
											$mediaSrc_fan ="";
										}else if($combine_arr[$test_count][$newtest_count]['media_type'] == 116){
											$get_channel_media = $new_profile_media_classobj ->get_all_media_selected_channel($combine_arr[$test_count][$newtest_count]['id']);
										?>
											<div class="blkplayer"><div class="player">
												<input type="hidden" id="channel_media_song<?php echo $newtest_count;?>" value="<?php echo $get_channel_media['songs']; ?>"/>
												<input type="hidden" id="channel_media_video<?php echo $newtest_count;?>" value="<?php echo $get_channel_media['videos']; ?>"/>
												<a href="javascript:void(0);"><img title="Play file now." src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" onclick="PlayAll('channel_media_video<?php echo $newtest_count;?>','channel_media_song<?php echo $newtest_count;?>')"/></a>
												<a href="javascript:void(0);"><img title="Add file to the bottom of your playlist without stopping current media." src="images/profile/add.gif"  onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'" onclick="showPlayer('v','<?php if($combine_arr[$test_count][$newtest_count]['id']!=""){echo $combine_arr[$test_count][$newtest_count]['id'];}else{ echo $combine_arr[$test_count][$newtest_count]['id'];}?>','addvi');"/></a>
												<?php
												if(($combine_arr[$test_count][$newtest_count]['sharing_preference']==5 || $combine_arr[$test_count][$newtest_count]['sharing_preference']==2) && $combine_arr[$test_count][$newtest_count]['media_type'] == 114){
													?>
													<a onclick="download_directly_song('<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['title']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['creator']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['from']);?>')" href="javascript:void(0);">
													<img title="Add media to your cart for free download or purchase." onmouseout="this.src='../images/profile/download_down.gif'" onmouseover="this.src='../images/profile/download-over_down.gif'" src="../images/profile/download_down.gif"></a>
												<?php
												}/* else{
												?>
												<img src="images/profile/download-over_down.gif" />
												<?php
												} */
												?>
											</div>
											</div>
										<?php
										}
										else{
										?>
											<div class="blkplayer"><div class="player">
											<a href="javascript:void(0);"><img title="Play file now." src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" /></a>
											<a href="javascript:void(0);"><img title="Add file to the bottom of your playlist without stopping current media." src="images/profile/add.gif"  onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'"/></a>
											<?php
												if(($combine_arr[$test_count][$newtest_count]['sharing_preference']==5 || $combine_arr[$test_count][$newtest_count]['sharing_preference']==2) &&$combine_arr[$test_count][$newtest_count]['media_type'] == 114){
													?>
													<a onclick="download_directly_song('<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['title']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['creator']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['from']);?>')" href="javascript:void(0);">
													<img title="Add media to your cart for free download or purchase." onmouseout="this.src='../images/profile/download_down.gif'" onmouseover="this.src='../images/profile/download-over_down.gif'" src="../images/profile/download_down.gif"></a>
												<?php
												}/* else{
												?>
												<img src="images/profile/download-over_down.gif" />
												<?php
												} */
												?>
											</div>
											</div>
										<?php
										}
										?>
										<div class="icon" onclick="runAccordion(<?php echo $combine_arr[$test_count][$newtest_count]['id']; ?>);">
											<div onselectstart="return false;"><img title="View details for media." src="images/profile/view.jpg" /></div>
										</div>
										<div class="icon"><!--<a href="add_media.php?edit=<?php// echo $combine_arr[$test_count][$newtest_count]['id'];?>"><img title="Edit media." src="images/profile/edit.png" /></a>--></div>
										<div class="icon"><a onclick="return confirmdelete('<?php echo $combine_arr[$test_count][$newtest_count]['title'];?>','<?php if($combine_arr[$test_count][$newtest_count]['name']=="Galleries/Images"){echo "Images";}else{echo $combine_arr[$test_count][$newtest_count]['name'];} ?>')" href="delete_media.php?id=<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>&whos_media=media_creator"><img title="Remove media from your library." src="images/profile/delete.png" /></a></div>
										<div class="expandable" id="Accordion<?php echo $combine_arr[$test_count][$newtest_count]['id']; ?>Content">
											<div class="affiBlock">
												<div class="RowCont">
													<div class="sideL">Plays :</div>
													<div class="sideR"><a href="" target="_blank"><?php if($combine_arr[$test_count][$newtest_count]['play_count']!=""){ echo $combine_arr[$test_count][$newtest_count]['play_count'];}else{ echo "0";}?></a></div>
												</div>
												<?php
													if($combine_arr[$test_count][$newtest_count]['sharing_preference']==5 || $combine_arr[$test_count][$newtest_count]['sharing_preference']==2){
													?>
													<div class="RowCont">
														<div class="sideL">Downloads :</div>
														<div class="sideR"><a href="javascript:void(0);"><?php echo $combine_arr[$test_count][$newtest_count]['download_count'];?></a></div>
													</div>
													<?php
													}
													if($combine_arr[$test_count][$newtest_count]['media_type']=='113')
													{
													
													}
													else
													{
												?>	
												<!--<div class="RowCont">
													<div class="sideL">Media URL :</div>
													<div class="sideR"><a href="" target="_blank">www.<?php echo $domainname; ?></a></div>
												</div>-->
												<?php
													}
												?>
											</div>
										</div>
									</div>
									<?php
								}
								else if($combine_arr[$test_count][$newtest_count]['media_from']=="email_subscription")
								{
									?>
										<div class="tableCont" id="AccordionContainer">
										<div class="blkD" style="width:60px;"><?php if($combine_arr[$test_count][$newtest_count]['name']=="Galleries/Images"){echo "Images";}else{echo $combine_arr[$test_count][$newtest_count]['name'];} ?></div>
										<div class="blkI" style="width:127px;"><?php
										$exp_creator_info = explode("|",$combine_arr[$test_count][$newtest_count]['creator_info']);
										$pro_url = $new_profile_media_classobj->get_creator_url($exp_creator_info[0],$exp_creator_info[1]);
										?>
										<a href="<?php if($pro_url['profile_url'] !=""){ echo "/".$pro_url['profile_url']; }else { echo "javascript:void(0)"; }?>">
										<?php if($combine_arr[$test_count][$newtest_count]['creator']!=""){
										//$endpos = strpos($combine_arr[$test_count][$newtest_count]['creator'],"(");
										//if($endpos !="" || $endpos !=false) {
										//echo substr($combine_arr[$test_count][$newtest_count]['creator'],0,$endpos);} else {
										echo $combine_arr[$test_count][$newtest_count]['creator']; }
										else{?>&nbsp;<?php } ?></a></div>
										<div class="blkK"><?php
										$exp_from_info = explode("|",$combine_arr[$test_count][$newtest_count]['from_info']);
										$pro_url_from = $new_profile_media_classobj->get_from_url($exp_from_info[0],$exp_from_info[1]);
										?>
										<a href="<?php if($pro_url_from['profile_url'] !=""){ echo "/".$pro_url_from['profile_url']; }else { echo "javascript:void(0)"; }?>">
										<?php if($combine_arr[$test_count][$newtest_count]['from']!=""){
										//$endpos = strpos($combine_arr[$test_count][$newtest_count]['from'],"(");
										//if($endpos !="" || $endpos != false) {
										//echo substr($combine_arr[$test_count][$newtest_count]['from'],0,$endpos);} else {
										echo $combine_arr[$test_count][$newtest_count]['from']; }
										else{?>&nbsp;<?php } ?></a></div>
										<div class="blkB"><?php
												if($combine_arr[$test_count][$newtest_count]['title']!="")
												{
													if($combine_arr[$test_count][$newtest_count]['media_type'] == 114)
													{
														//$get_media_track = $new_profile_media_classobj->get_media_track($combine_arr[$test_count][$newtest_count]['id']); 
														if($combine_arr[$test_count][$newtest_count]['track'] != "" && $combine_arr[$test_count][$newtest_count]['track'] != NULL && $combine_arr[$test_count][$newtest_count]['track'] !=" "){
															echo $combine_arr[$test_count][$newtest_count]['track'].".".stripslashes($combine_arr[$test_count][$newtest_count]['title']);
														}else{
															echo stripslashes($combine_arr[$test_count][$newtest_count]['title']);
														}
													}
													else{
													echo stripslashes($combine_arr[$test_count][$newtest_count]['title']); }
												}else{
												?>&nbsp;<?php
												}
													?></div>
										<?php
										if($combine_arr[$test_count][$newtest_count]['media_type']==114)
										{
										?>
											<div class="blkplayer"><div class="player">
												<a href="javascript:void(0);"><img onclick="showPlayer('a','<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','play');" onmouseout="this.src='images/profile/play.gif'" onmouseover="this.src='images/profile/play-over.gif'" src="images/profile/play.gif" title="Play file now."></a>
												<a href="javascript:void(0);"><img onclick="showPlayer('a','<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','add');" onmouseout="this.src='images/profile/add.gif'" onmouseover="this.src='images/profile/add-over.gif'" src="images/profile/add.gif" title="Add file to the bottom of your playlist without stopping current media."></a>
												<?php
												if(($combine_arr[$test_count][$newtest_count]['sharing_preference']==5 || $combine_arr[$test_count][$newtest_count]['sharing_preference']==2) &&$combine_arr[$test_count][$newtest_count]['media_type'] == 114){
													$song_image_name = $new_profile_media_classobj->get_audio_img($combine_arr[$test_count][$newtest_count]['id']);
													if($combine_arr[$test_count][$newtest_count]['sharing_preference']==5)
													{
													?>
													<a href="javascript:void(0);" onclick="add_tocart(<?php echo $newtest_count;?>)"><img src="../images/profile/download_down.gif" onmouseover="this.src='../images/profile/download-over_down.gif'" onmouseout="this.src='../images/profile/download_down.gif'" title="Add media to your cart for free download or purchase." /></a>
													<?php
													}
													else if($combine_arr[$test_count][$newtest_count]['sharing_preference']==2)
													{
														$tip_add_aftr_detils = $display->get_tip_details_for_every($combine_arr[$test_count][$newtest_count]['id'],'media');
													?>
													<a href="javascript:void(0);" onclick="ask_to_donate('<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['title']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['creator']);?>','<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','add_to_cart_media<?php echo $newtest_count;?>','<?php echo $combine_arr[$test_count][$newtest_count]['general_user_id']; ?>','<?php echo $tip_add_aftr_detils; ?>')"><img src="../images/profile/download_down.gif" onmouseover="this.src='../images/profile/download-over_down.gif'" onmouseout="this.src='../images/profile/download_down.gif'" title="Add media to your cart for free download or purchase." /></a>
													<?php
													}
												}/* else{
												?>
												<img src="images/profile/download-over_down.gif" />
												<?php
												} */
												?>
												<a href="addtocart.php?seller_id=<?php echo $combine_arr[$test_count][$newtest_count]['general_user_id'];?>&media_id=<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>&buyer_id=<?php echo $_SESSION['gen_user_id'];?>&title=<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['title']);?>&image_name=<?php echo $song_image_name;?>" id="add_to_cart_media<?php echo $newtest_count;?>" style="display:none;"></a>
												<script type="text/javascript">
												$(document).ready(function() {
													$("#add_to_cart_media<?php echo $newtest_count;?>").fancybox({
													'height'			: '75%',
													'width'				: '69%',
													'transitionIn'		: 'none',
													'transitionOut'		: 'none',
													'type'				: 'iframe'
													});
												});
												</script>
											</div>
											</div>
										<?php
										}else if($combine_arr[$test_count][$newtest_count]['media_type']==115)
										{
										?>
											<div class="blkplayer"><div class="player">
												<a href="javascript:void(0);"><img onclick="showPlayer('v','<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','playlist');" onmouseout="this.src='images/profile/play.gif'" onmouseover="this.src='images/profile/play-over.gif'" src="images/profile/play.gif" title="Play file now."></a>
												<a href="javascript:void(0);"><img onclick="showPlayer('v','<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','addvi');" onmouseout="this.src='images/profile/add.gif'" onmouseover="this.src='images/profile/add-over.gif'" src="images/profile/add.gif" title="Add file to the bottom of your playlist without stopping current media."></a>
												<?php
												if(($combine_arr[$test_count][$newtest_count]['sharing_preference']==5 || $combine_arr[$test_count][$newtest_count]['sharing_preference']==2) &&$combine_arr[$test_count][$newtest_count]['media_type'] == 114){
													$get_video_image = $new_profile_media_classobj->getvideoimage($combine_arr[$test_count][$newtest_count]['id']);
													$video_image_name = "https://img.youtube.com/vi/".$get_video_image."/default.jpg";
													if($combine_arr[$test_count][$newtest_count]['sharing_preference']==5)
													{
													?>
													<a href="javascript:void(0);" onclick="add_tocart(<?php echo $newtest_count;?>)"><img src="../images/profile/download_down.gif" onmouseover="this.src='../images/profile/download-over_down.gif'" onmouseout="this.src='../images/profile/download_down.gif'" title="Add media to your cart for free download or purchase." /></a>
													<?php
													}
													else if($combine_arr[$test_count][$newtest_count]['sharing_preference']==2)
													{
														$tip_add_aftr_detils = $display->get_tip_details_for_every($combine_arr[$test_count][$newtest_count]['id'],'media');
													?>
													<a href="javascript:void(0);" onclick="ask_to_donate('<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['title']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['creator']);?>','<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','add_to_cart_media<?php echo $newtest_count;?>','<?php echo $combine_arr[$test_count][$newtest_count]['general_user_id']; ?>','<?php echo $tip_add_aftr_detils; ?>')"><img src="../images/profile/download_down.gif" onmouseover="this.src='../images/profile/download-over_down.gif'" onmouseout="this.src='../images/profile/download_down.gif'" title="Add media to your cart for free download or purchase." /></a>
													<?php
													}
												}/* else{
												?>
												<img src="images/profile/download-over_down.gif" />
												<?php
												} */
												?>
												<a href="addtocart.php?seller_id=<?php echo $combine_arr[$test_count][$newtest_count]['general_user_id'];?>&media_id=<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>&buyer_id=<?php echo $_SESSION['gen_user_id'];?>&title=<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['title']);?>&image_name=<?php echo $video_image_name;?>" id="add_to_cart_media<?php echo $newtest_count;?>" style="display:none;"></a>
												<script type="text/javascript">
												$(document).ready(function() {
													$("#add_to_cart_media<?php echo $newtest_count;?>").fancybox({
													'height'			: '75%',
													'width'				: '69%',
													'transitionIn'		: 'none',
													'transitionOut'		: 'none',
													'type'				: 'iframe'
													});
												});
												</script>
											</div>
											</div>
										<?php
										}else if($combine_arr[$test_count][$newtest_count]['media_type']==113){
											$get_images_fan = $new_profile_media_classobj ->get_all_gallery_images($combine_arr[$test_count][$newtest_count]['id']);
											for($count_img=0;$count_img<=count($get_images_fan);$count_img++){
												if($get_all_img_fan ==""){
													$get_all_img_fan = $get_images_fan[$count_img]['image_name'];
												}else{
													$get_all_img_fan = $get_all_img_fan.",".$get_images_fan[$count_img]['image_name'];
												}
												if($galleryImagestitle_fan==""){
													$galleryImagestitle_fan = $get_images_fan[$count_img]['image_title'];
												}else{
													$galleryImagestitle_fan = $galleryImagestitle_fan.",".$get_images_fan[$count_img]['image_title'];
												}
												$mediaSrc_fan = $get_images_fan[0]['image_name'];
											}
											$gal_id_fan = $get_images_fan[$count_img]['media_id'];
											$get_other_user_id = $user_keys_data->get_general_user_idby_mediaid($combine_arr[$test_count][$newtest_count]['id']);
											$orgPath_fan = $s3->getNewURI_otheruser("medgallery",$get_other_user_id);//"https://medgallery.s3.amazonaws.com/";
											$thumbPath_fan = $s3->getNewURI_otheruser("medgalthumb",$get_other_user_id);//"https://medgalthumb.s3.amazonaws.com/";
											
										?>
											<div class="blkplayer"><div class="player">
											<a href="javascript:void(0);"><img title="Play file now." src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" onclick="checkload('<?php echo $mediaSrc_fan;?>','<?php echo $get_all_img_fan; ?>','<?php echo $thumbPath_fan; ?>','<?php echo $orgPath_fan; ?>','<?php echo $galleryImagestitle_fan; ?>','<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>')"/></a>
											<!--<a href="javascript:void(0);"><img title="Add file to the bottom of your playlist without stopping current media." src="images/profile/add.gif"  onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'"/></a>-->
											<?php
												$get_gal_image_name  = $new_profile_media_classobj ->get_gallery_info2($combine_arr[$test_count][$newtest_count]['id']);
												if(($combine_arr[$test_count][$newtest_count]['sharing_preference']==2) && $combine_arr[$test_count][$newtest_count]['media_type'] == 114)
												{
													$tip_add_aftr_detils = $display->get_tip_details_for_every($combine_arr[$test_count][$newtest_count]['id'],'media');
													?>
													<a href="javascript:void(0);" onclick="ask_to_donate('<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['title']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['creator']);?>','<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','add_to_cart_media<?php echo $newtest_count;?>','<?php echo $combine_arr[$test_count][$newtest_count]['general_user_id']; ?>','<?php echo $tip_add_aftr_detils; ?>')"><img src="../images/profile/download_down.gif" onmouseover="this.src='../images/profile/download-over_down.gif'" onmouseout="this.src='../images/profile/download_down.gif'" title="Add media to your cart for free download or purchase." /></a>
													<?php
												}/* else{
												?>
												<img src="images/profile/download-over_down.gif" />
												<?php
												} */
												?>
												<a href="addtocart.php?seller_id=<?php echo $combine_arr[$test_count][$newtest_count]['general_user_id'];?>&media_id=<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>&buyer_id=<?php echo $_SESSION['gen_user_id'];?>&title=<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['title']);?>&image_name=<?php echo $get_gal_image_name;?>" id="add_to_cart_media<?php echo $newtest_count;?>" style="display:none;"></a>
												<script type="text/javascript">
												$(document).ready(function() {
													$("#add_to_cart_media<?php echo $newtest_count;?>").fancybox({
													'height'			: '75%',
													'width'				: '69%',
													'transitionIn'		: 'none',
													'transitionOut'		: 'none',
													'type'				: 'iframe'
													});
												});
												</script>
											</div>
											</div>
										<?php
											$get_all_img_fan ="";
											$galleryImagestitle_fan="";
											$mediaSrc_fan ="";
										}else if($combine_arr[$test_count][$newtest_count]['media_type'] == 116){
											$get_channel_media = $new_profile_media_classobj ->get_all_media_selected_channel($combine_arr[$test_count][$newtest_count]['id']);
										?>
											<div class="blkplayer"><div class="player">
												<input type="hidden" id="channel_media_song<?php echo $newtest_count;?>" value="<?php echo $get_channel_media['songs']; ?>"/>
												<input type="hidden" id="channel_media_video<?php echo $newtest_count;?>" value="<?php echo $get_channel_media['videos']; ?>"/>
												<a href="javascript:void(0);"><img title="Play file now." src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" onclick="PlayAll('channel_media_video<?php echo $newtest_count;?>','channel_media_song<?php echo $newtest_count;?>')"/></a>
												<a href="javascript:void(0);"><img title="Add file to the bottom of your playlist without stopping current media." src="images/profile/add.gif"  onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'" onclick="showPlayer('v','<?php if($combine_arr[$test_count][$newtest_count]['id']!=""){echo $combine_arr[$test_count][$newtest_count]['id'];}else{ echo $combine_arr[$test_count][$newtest_count]['id'];}?>','addvi');"/></a>
												<?php
												if(($combine_arr[$test_count][$newtest_count]['sharing_preference']==5 || $combine_arr[$test_count][$newtest_count]['sharing_preference']==2) && $combine_arr[$test_count][$newtest_count]['media_type'] == 114){
													?>
													<a onclick="download_directly_song('<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['title']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['creator']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['from']);?>')" href="javascript:void(0);">
													<img title="Add media to your cart for free download or purchase." onmouseout="this.src='../images/profile/download_down.gif'" onmouseover="this.src='../images/profile/download-over_down.gif'" src="../images/profile/download_down.gif"></a>
												<?php
												}/* else{
												?>
												<img src="images/profile/download-over_down.gif" />
												<?php
												} */
												?>
											</div>
											</div>
										<?php
										}
										else{
										?>
											<div class="blkplayer"><div class="player">
											<a href="javascript:void(0);"><img title="Play file now." src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" /></a>
											<a href="javascript:void(0);"><img title="Add file to the bottom of your playlist without stopping current media." src="images/profile/add.gif"  onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'"/></a>
											<?php
												if(($combine_arr[$test_count][$newtest_count]['sharing_preference']==5 || $combine_arr[$test_count][$newtest_count]['sharing_preference']==2) && $combine_arr[$test_count][$newtest_count]['media_type'] == 114){
													?>
													<a onclick="download_directly_song('<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['title']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['creator']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['from']);?>')" href="javascript:void(0);">
													<img title="Add media to your cart for free download or purchase." onmouseout="this.src='../images/profile/download_down.gif'" onmouseover="this.src='../images/profile/download-over_down.gif'" src="../images/profile/download_down.gif"></a>
												<?php
												}/* else{
												?>
												<img src="images/profile/download-over_down.gif" />
												<?php
												} */
												?>
											</div>
											</div>
										<?php
										}
										?>
										<div class="icon" onclick="runAccordion(<?php echo $combine_arr[$test_count][$newtest_count]['id']; ?>);">
											<div onselectstart="return false;"><img title="View details for media." src="images/profile/view.jpg" /></div>
										</div>
										<div class="icon"><!--<a href="add_media.php?edit=<?php// echo $combine_arr[$test_count][$newtest_count]['id'];?>"><img title="Edit media." src="images/profile/edit.png" /></a>--></div>
										<div class="icon"><a onclick="return confirmdelete('<?php echo $combine_arr[$test_count][$newtest_count]['title'];?>','<?php if($combine_arr[$test_count][$newtest_count]['name']=="Galleries/Images"){echo "Images";}else{echo $combine_arr[$test_count][$newtest_count]['name'];} ?>')" href="delete_media.php?id=<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>&whos_media=email_subscription"><img title="Remove media from your library." src="images/profile/delete.png" /></a></div>
										<div class="expandable" id="Accordion<?php echo $combine_arr[$test_count][$newtest_count]['id']; ?>Content">
											<div class="affiBlock">
												<div class="RowCont">
													<div class="sideL">Plays :</div>
													<div class="sideR"><a href="" target="_blank"><?php if($combine_arr[$test_count][$newtest_count]['play_count']!=""){ echo $combine_arr[$test_count][$newtest_count]['play_count'];}else{ echo "0";}?></a></div>
												</div>
											<?php
													if($combine_arr[$test_count][$newtest_count]['sharing_preference']==5 || $combine_arr[$test_count][$newtest_count]['sharing_preference']==2){
													?>
													<div class="RowCont">
														<div class="sideL">Downloads :</div>
														<div class="sideR"><a href="javascript:void(0);"><?php echo $combine_arr[$test_count][$newtest_count]['download_count'];?></a></div>
													</div>
													<?php
													}
													if($combine_arr[$test_count][$newtest_count]['media_type']=='113')
													{
													
													}
													else
													{
												?>	
												<!--<div class="RowCont">
													<div class="sideL">Media URL :</div>
													<div class="sideR"><a href="" target="_blank">www.<?php echo $domainname; ?></a></div>
												</div>-->
												<?php
													}
												?>
											</div>
										</div>
									</div>
									<?php
								}
								else if($combine_arr[$test_count][$newtest_count]['media_from']=="new_tagging_media")
								{
									?>
										<div class="tableCont" id="AccordionContainer">
										<div class="blkD" style="width:60px;"><?php if($combine_arr[$test_count][$newtest_count]['name']=="Galleries/Images"){echo "Images";}else{echo $combine_arr[$test_count][$newtest_count]['name'];} ?></div>
										<div class="blkI" style="width:127px;"><?php
										$exp_creator_info = explode("|",$combine_arr[$test_count][$newtest_count]['creator_info']);
										$pro_url = $new_profile_media_classobj->get_creator_url($exp_creator_info[0],$exp_creator_info[1]);
										?>
										<a href="<?php if($pro_url['profile_url'] !=""){ echo "/".$pro_url['profile_url']; }else { echo "javascript:void(0)"; }?>">
										<?php if($combine_arr[$test_count][$newtest_count]['creator']!=""){
										//$endpos = strpos($combine_arr[$test_count][$newtest_count]['creator'],"(");
										//if($endpos !="" || $endpos !=false) {
										//echo substr($combine_arr[$test_count][$newtest_count]['creator'],0,$endpos);} else {
										echo $combine_arr[$test_count][$newtest_count]['creator']; }
										else{?>&nbsp;<?php } ?></a></div>
										<div class="blkK"><?php
										$exp_from_info = explode("|",$combine_arr[$test_count][$newtest_count]['from_info']);
										$pro_url_from = $new_profile_media_classobj->get_from_url($exp_from_info[0],$exp_from_info[1]);
										?>
										<a href="<?php if($pro_url_from['profile_url'] !=""){ echo "/".$pro_url_from['profile_url']; }else { echo "javascript:void(0)"; }?>">
										<?php if($combine_arr[$test_count][$newtest_count]['from']!=""){
										//$endpos = strpos($combine_arr[$test_count][$newtest_count]['from'],"(");
										//if($endpos !="" || $endpos != false) {
										//echo substr($combine_arr[$test_count][$newtest_count]['from'],0,$endpos);} else {
										echo $combine_arr[$test_count][$newtest_count]['from']; }
										else{?>&nbsp;<?php } ?></a></div>
										<div class="blkB"><?php
												if($combine_arr[$test_count][$newtest_count]['title']!="")
												{
													if($combine_arr[$test_count][$newtest_count]['media_type'] == 114)
													{
														//$get_media_track = $new_profile_media_classobj->get_media_track($combine_arr[$test_count][$newtest_count]['id']); 
														if($combine_arr[$test_count][$newtest_count]['track'] != "" && $combine_arr[$test_count][$newtest_count]['track'] != NULL && $combine_arr[$test_count][$newtest_count]['track'] !=" "){
															echo $combine_arr[$test_count][$newtest_count]['track'].".".stripslashes($combine_arr[$test_count][$newtest_count]['title']);
														}else{
															echo stripslashes($combine_arr[$test_count][$newtest_count]['title']);
														}
													}
													else{
													echo stripslashes($combine_arr[$test_count][$newtest_count]['title']); }
												}else{
												?>&nbsp;<?php
												}
													?></div>
										<?php
										if($combine_arr[$test_count][$newtest_count]['media_type']==114)
										{
										?>
											<div class="blkplayer"><div class="player">
												<a href="javascript:void(0);"><img onclick="showPlayer('a','<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','play');" onmouseout="this.src='images/profile/play.gif'" onmouseover="this.src='images/profile/play-over.gif'" src="images/profile/play.gif" title="Play file now."></a>
												<a href="javascript:void(0);"><img onclick="showPlayer('a','<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','add');" onmouseout="this.src='images/profile/add.gif'" onmouseover="this.src='images/profile/add-over.gif'" src="images/profile/add.gif" title="Add file to the bottom of your playlist without stopping current media."></a>
												<?php
												if(($combine_arr[$test_count][$newtest_count]['sharing_preference']==5 || $combine_arr[$test_count][$newtest_count]['sharing_preference']==2) && $combine_arr[$test_count][$newtest_count]['media_type'] == 114){
													?>
													<a onclick="download_directly_song('<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['title']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['creator']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['from']);?>')" href="javascript:void(0);">
													<img title="Add media to your cart for free download or purchase." onmouseout="this.src='../images/profile/download_down.gif'" onmouseover="this.src='../images/profile/download-over_down.gif'" src="../images/profile/download_down.gif"></a>
												<?php
												}/* else{
												?>
												<img src="images/profile/download-over_down.gif" />
												<?php
												} */
												?>
											</div>
											</div>
										<?php
										}else if($combine_arr[$test_count][$newtest_count]['media_type']==115)
										{
										?>
											<div class="blkplayer"><div class="player">
												<a href="javascript:void(0);"><img onclick="showPlayer('v','<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','playlist');" onmouseout="this.src='images/profile/play.gif'" onmouseover="this.src='images/profile/play-over.gif'" src="images/profile/play.gif" title="Play file now."></a>
												<a href="javascript:void(0);"><img onclick="showPlayer('v','<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','addvi');" onmouseout="this.src='images/profile/add.gif'" onmouseover="this.src='images/profile/add-over.gif'" src="images/profile/add.gif" title="Add file to the bottom of your playlist without stopping current media."></a>
												<?php
												if(($combine_arr[$test_count][$newtest_count]['sharing_preference']==5 || $combine_arr[$test_count][$newtest_count]['sharing_preference']==2) && $combine_arr[$test_count][$newtest_count]['media_type'] == 114){
													?>
													<a onclick="download_directly_song('<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['title']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['creator']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['from']);?>')" href="javascript:void(0);">
													<img title="Add media to your cart for free download or purchase." onmouseout="this.src='../images/profile/download_down.gif'" onmouseover="this.src='../images/profile/download-over_down.gif'" src="../images/profile/download_down.gif"></a>
												<?php
												}/* else{
												?>
												<img src="images/profile/download-over_down.gif" />
												<?php
												} */
												?>
											</div>
											</div>
										<?php
										}else if($combine_arr[$test_count][$newtest_count]['media_type']==113){
											$get_images_fan = $new_profile_media_classobj ->get_all_gallery_images($combine_arr[$test_count][$newtest_count]['id']);
											for($count_img=0;$count_img<=count($get_images_fan);$count_img++){
												if($get_all_img_fan ==""){
													$get_all_img_fan = $get_images_fan[$count_img]['image_name'];
												}else{
													$get_all_img_fan = $get_all_img_fan.",".$get_images_fan[$count_img]['image_name'];
												}
												if($galleryImagestitle_fan ==""){
													$galleryImagestitle_fan = $get_images_fan[$count_img]['image_title'];
												}else{
													$galleryImagestitle_fan = $galleryImagestitle_fan.",".$get_images_fan[$count_img]['image_title'];
												}
												$mediaSrc_fan = $get_images_fan[0]['image_name'];
											}
											$gal_id_fan = $get_images_fan[$count_img]['media_id'];
											$get_other_user_id = $user_keys_data->get_general_user_idby_mediaid($combine_arr[$test_count][$newtest_count]['id']);
											$orgPath_fan = $s3->getNewURI_otheruser("medgallery",$get_other_user_id);//"https://medgallery.s3.amazonaws.com/";
											$thumbPath_fan = $s3->getNewURI_otheruser("medgalthumb",$get_other_user_id);//"https://medgalthumb.s3.amazonaws.com/";
											
										?>
											<div class="blkplayer"><div class="player">
											<a href="javascript:void(0);"><img title="Play file now." src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" onclick="checkload('<?php echo $mediaSrc_fan;?>','<?php echo $get_all_img_fan; ?>','<?php echo $thumbPath_fan; ?>','<?php echo $orgPath_fan; ?>','<?php echo $galleryImagestitle_fan; ?>','<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>')"/></a>
											<!--<a href="javascript:void(0);"><img title="Add file to the bottom of your playlist without stopping current media." src="images/profile/add.gif"  onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'"/></a>-->
											<?php
												if(($combine_arr[$test_count][$newtest_count]['sharing_preference']==5 || $combine_arr[$test_count][$newtest_count]['sharing_preference']==2) && $combine_arr[$test_count][$newtest_count]['media_type'] == 114){
													?>
													<a onclick="download_directly_song('<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['title']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['creator']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['from']);?>')" href="javascript:void(0);">
													<img title="Add media to your cart for free download or purchase." onmouseout="this.src='../images/profile/download_down.gif'" onmouseover="this.src='../images/profile/download-over_down.gif'" src="../images/profile/download_down.gif"></a>
												<?php
												}/* else{
												?>
												<img src="images/profile/download-over_down.gif" />
												<?php
												} */
												?>
											</div>
											</div>
										<?php
											$get_all_img_fan ="";
											$galleryImagestitle_fan="";
											$mediaSrc_fan ="";
										}else if($combine_arr[$test_count][$newtest_count]['media_type'] == 116){
											$get_channel_media = $new_profile_media_classobj ->get_all_media_selected_channel($combine_arr[$test_count][$newtest_count]['id']);
										?>
											<div class="blkplayer"><div class="player">
												<input type="hidden" id="channel_media_song<?php echo $newtest_count;?>" value="<?php echo $get_channel_media['songs']; ?>"/>
												<input type="hidden" id="channel_media_video<?php echo $newtest_count;?>" value="<?php echo $get_channel_media['videos']; ?>"/>
												<a href="javascript:void(0);"><img title="Play file now." src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" onclick="PlayAll('channel_media_video<?php echo $newtest_count;?>','channel_media_song<?php echo $newtest_count;?>')"/></a>
												<a href="javascript:void(0);"><img title="Add file to the bottom of your playlist without stopping current media." src="images/profile/add.gif"  onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'" onclick="showPlayer('v','<?php if($combine_arr[$test_count][$newtest_count]['id']!=""){echo $combine_arr[$test_count][$newtest_count]['id'];}else{ echo $combine_arr[$test_count][$newtest_count]['id'];}?>','addvi');"/></a>
												<?php
												if(($combine_arr[$test_count][$newtest_count]['sharing_preference']==5 || $combine_arr[$test_count][$newtest_count]['sharing_preference']==2) && $combine_arr[$test_count][$newtest_count]['media_type'] == 114){
													?>
													<a onclick="download_directly_song('<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['title']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['creator']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['from']);?>')" href="javascript:void(0);">
													<img title="Add media to your cart for free download or purchase." onmouseout="this.src='../images/profile/download_down.gif'" onmouseover="this.src='../images/profile/download-over_down.gif'" src="../images/profile/download_down.gif"></a>
												<?php
												}/* else{
												?>
												<img src="images/profile/download-over_down.gif" />
												<?php
												} */
												?>
											</div>
											</div>
										<?php
										}
										else{
										?>
											<div class="blkplayer"><div class="player">
											<a href="javascript:void(0);"><img title="Play file now." src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" /></a>
											<a href="javascript:void(0);"><img title="Add file to the bottom of your playlist without stopping current media." src="images/profile/add.gif"  onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'"/></a>
											<?php
												if(($combine_arr[$test_count][$newtest_count]['sharing_preference']==5 || $combine_arr[$test_count][$newtest_count]['sharing_preference']==2) && $combine_arr[$test_count][$newtest_count]['media_type'] == 114){
													?>
													<a onclick="download_directly_song('<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['title']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['creator']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['from']);?>')" href="javascript:void(0);">
													<img title="Add media to your cart for free download or purchase." onmouseout="this.src='../images/profile/download_down.gif'" onmouseover="this.src='../images/profile/download-over_down.gif'" src="../images/profile/download_down.gif"></a>
												<?php
												}/* else{
												?>
												<img src="images/profile/download-over_down.gif" />
												<?php
												} */
												?>
											</div>
											</div>
										<?php
										}
										?>
										<div class="icon" onclick="runAccordion(<?php echo $combine_arr[$test_count][$newtest_count]['id']; ?>);">
											<div onselectstart="return false;"><img title="View details for media." src="images/profile/view.jpg" /></div>
										</div>
										<div class="icon"><!--<a href="add_media.php?edit=<?php// echo $combine_arr[$test_count][$newtest_count]['id'];?>"><img title="Edit media." src="images/profile/edit.png" /></a>--></div>
										<div class="icon"><a onclick="return confirmdelete('<?php echo $combine_arr[$test_count][$newtest_count]['title'];?>','<?php if($combine_arr[$test_count][$newtest_count]['name']=="Galleries/Images"){echo "Images";}else{echo $combine_arr[$test_count][$newtest_count]['name'];} ?>')" href="delete_media.php?id=<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>&whos_media=new_tagging_media"><img title="Remove media from your library." src="images/profile/delete.png" /></a></div>
										<div class="expandable" id="Accordion<?php echo $combine_arr[$test_count][$newtest_count]['id']; ?>Content">
											<div class="affiBlock">
												<div class="RowCont">
													<div class="sideL">Plays :</div>
													<div class="sideR"><a href="" target="_blank"><?php if($combine_arr[$test_count][$newtest_count]['play_count']!=""){ echo $combine_arr[$test_count][$newtest_count]['play_count'];}else{ echo "0";}?></a></div>
												</div>
												<?php
													if($combine_arr[$test_count][$newtest_count]['sharing_preference']==5 || $combine_arr[$test_count][$newtest_count]['sharing_preference']==2){
													?>
													<div class="RowCont">
														<div class="sideL">Downloads :</div>
														<div class="sideR"><a href="javascript:void(0);"><?php echo $combine_arr[$test_count][$newtest_count]['download_count'];?></a></div>
													</div>
													<?php
													}
													if($combine_arr[$test_count][$newtest_count]['media_type']=='113')
													{
													
													}
													else
													{
												?>	
												<!--<div class="RowCont">
													<div class="sideL">Media URL :</div>
													<div class="sideR"><a href="" target="_blank">www.<?php echo $domainname; ?></a></div>
												</div>-->
												<?php
													}
												?>
											</div>
										</div>
									</div>
									<?php
								}
								
								else if($combine_arr[$test_count][$newtest_count]['media_from']=="media_creator_creator")
								{
									?>
										<div class="tableCont" id="AccordionContainer">
										<div class="blkD" style="width:60px;"><?php if($combine_arr[$test_count][$newtest_count]['name']=="Galleries/Images"){echo "Images";}else{echo $combine_arr[$test_count][$newtest_count]['name'];} ?></div>
										<div class="blkI" style="width:127px;"><?php
										$exp_creator_info = explode("|",$combine_arr[$test_count][$newtest_count]['creator_info']);
										$pro_url = $new_profile_media_classobj->get_creator_url($exp_creator_info[0],$exp_creator_info[1]);
										?>
										<a href="<?php if($pro_url['profile_url'] !=""){ echo "/".$pro_url['profile_url']; }else { echo "javascript:void(0)"; }?>">
										<?php if($combine_arr[$test_count][$newtest_count]['creator']!=""){
										//$endpos = strpos($combine_arr[$test_count][$newtest_count]['creator'],"(");
										//if($endpos !="" || $endpos !=false) {
										//echo substr($combine_arr[$test_count][$newtest_count]['creator'],0,$endpos);} else {
										echo $combine_arr[$test_count][$newtest_count]['creator']; }
										else{?>&nbsp;<?php } ?></a></div>
										<div class="blkK"><?php
										$exp_from_info = explode("|",$combine_arr[$test_count][$newtest_count]['from_info']);
										$pro_url_from = $new_profile_media_classobj->get_from_url($exp_from_info[0],$exp_from_info[1]);
										?>
										<a href="<?php if($pro_url_from['profile_url'] !=""){ echo "/".$pro_url_from['profile_url']; }else { echo "javascript:void(0)"; }?>">
										<?php if($combine_arr[$test_count][$newtest_count]['from']!=""){
										//$endpos = strpos($combine_arr[$test_count][$newtest_count]['from'],"(");
										//if($endpos !="" || $endpos != false) {
										//echo substr($combine_arr[$test_count][$newtest_count]['from'],0,$endpos);} else {
										echo $combine_arr[$test_count][$newtest_count]['from']; }
										else{?>&nbsp;<?php } ?></a></div>
										<div class="blkB"><?php
												if($combine_arr[$test_count][$newtest_count]['title']!="")
												{
													if($combine_arr[$test_count][$newtest_count]['media_type'] == 114)
													{
														//$get_media_track = $new_profile_media_classobj->get_media_track($combine_arr[$test_count][$newtest_count]['id']); 
														if($combine_arr[$test_count][$newtest_count]['track'] != "" && $combine_arr[$test_count][$newtest_count]['track'] != NULL && $combine_arr[$test_count][$newtest_count]['track'] !=" "){
															echo $combine_arr[$test_count][$newtest_count]['track'].".".stripslashes($combine_arr[$test_count][$newtest_count]['title']);
														}else{
															echo stripslashes($combine_arr[$test_count][$newtest_count]['title']);
														}
													}
													else{
													echo stripslashes($combine_arr[$test_count][$newtest_count]['title']); }
												}else{
												?>&nbsp;<?php
												}
													?></div>
										<?php
										if($combine_arr[$test_count][$newtest_count]['media_type']==114)
										{
										?>
											<div class="blkplayer"><div class="player">
												<a href="javascript:void(0);"><img onclick="showPlayer('a','<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','play');" onmouseout="this.src='images/profile/play.gif'" onmouseover="this.src='images/profile/play-over.gif'" src="images/profile/play.gif" title="Play file now."></a>
												<a href="javascript:void(0);"><img onclick="showPlayer('a','<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','add');" onmouseout="this.src='images/profile/add.gif'" onmouseover="this.src='images/profile/add-over.gif'" src="images/profile/add.gif" title="Add file to the bottom of your playlist without stopping current media."></a>
												<?php
												if(($combine_arr[$test_count][$newtest_count]['sharing_preference']==5 || $combine_arr[$test_count][$newtest_count]['sharing_preference']==2) && $combine_arr[$test_count][$newtest_count]['media_type'] == 114){
													?>
													<a onclick="download_directly_song('<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['title']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['creator']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['from']);?>')" href="javascript:void(0);">
													<img title="Add media to your cart for free download or purchase." onmouseout="this.src='../images/profile/download_down.gif'" onmouseover="this.src='../images/profile/download-over_down.gif'" src="../images/profile/download_down.gif"></a>
												<?php
												}/* else{
												?>
												<img src="images/profile/download-over_down.gif" />
												<?php
												} */
												?>
											</div>
											</div>
										<?php
										}else if($combine_arr[$test_count][$newtest_count]['media_type']==115)
										{
										?>
											<div class="blkplayer"><div class="player">
												<a href="javascript:void(0);"><img onclick="showPlayer('v','<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','playlist');" onmouseout="this.src='images/profile/play.gif'" onmouseover="this.src='images/profile/play-over.gif'" src="images/profile/play.gif" title="Play file now."></a>
												<a href="javascript:void(0);"><img onclick="showPlayer('v','<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','addvi');" onmouseout="this.src='images/profile/add.gif'" onmouseover="this.src='images/profile/add-over.gif'" src="images/profile/add.gif" title="Add file to the bottom of your playlist without stopping current media."></a>
												<?php
												if(($combine_arr[$test_count][$newtest_count]['sharing_preference']==5 || $combine_arr[$test_count][$newtest_count]['sharing_preference']==2) && $combine_arr[$test_count][$newtest_count]['media_type'] == 114){
													?>
													<a onclick="download_directly_song('<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['title']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['creator']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['from']);?>')" href="javascript:void(0);">
													<img title="Add media to your cart for free download or purchase." onmouseout="this.src='../images/profile/download_down.gif'" onmouseover="this.src='../images/profile/download-over_down.gif'" src="../images/profile/download_down.gif"></a>
												<?php
												}/* else{
												?>
												<img src="images/profile/download-over_down.gif" />
												<?php
												} */
												?>
											</div>
											</div>
										<?php
										}else if($combine_arr[$test_count][$newtest_count]['media_type']==113){
											$get_images_fan = $new_profile_media_classobj ->get_all_gallery_images($combine_arr[$test_count][$newtest_count]['id']);
											for($count_img=0;$count_img<=count($get_images_fan);$count_img++){
												if($get_all_img_fan ==""){
													$get_all_img_fan = $get_images_fan[$count_img]['image_name'];
												}else{
													$get_all_img_fan = $get_all_img_fan.",".$get_images_fan[$count_img]['image_name'];
												}
												if($galleryImagestitle_fan ==""){
													$galleryImagestitle_fan = $get_images_fan[$count_img]['image_title'];
												}else{
													$galleryImagestitle_fan = $galleryImagestitle_fan.",".$get_images_fan[$count_img]['image_title'];
												}
												$mediaSrc_fan = $get_images_fan[0]['image_name'];
											}
											$gal_id_fan = $get_images_fan[$count_img]['media_id'];
											$get_other_user_id = $user_keys_data->get_general_user_idby_mediaid($combine_arr[$test_count][$newtest_count]['id']);
											$orgPath_fan = $s3->getNewURI_otheruser("medgallery",$get_other_user_id);//"https://medgallery.s3.amazonaws.com/";
											$thumbPath_fan = $s3->getNewURI_otheruser("medgalthumb",$get_other_user_id);//"https://medgalthumb.s3.amazonaws.com/";
											
										?>
											<div class="blkplayer"><div class="player">
											<a href="javascript:void(0);"><img title="Play file now." src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" onclick="checkload('<?php echo $mediaSrc_fan;?>','<?php echo $get_all_img_fan; ?>','<?php echo $thumbPath_fan; ?>','<?php echo $orgPath_fan; ?>','<?php echo $galleryImagestitle_fan; ?>','<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>')"/></a>
											<!--<a href="javascript:void(0);"><img title="Add file to the bottom of your playlist without stopping current media." src="images/profile/add.gif"  onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'"/></a>-->
											<?php
												if(($combine_arr[$test_count][$newtest_count]['sharing_preference']==5 || $combine_arr[$test_count][$newtest_count]['sharing_preference']==2) && $combine_arr[$test_count][$newtest_count]['media_type'] == 114){
													?>
													<a onclick="download_directly_song('<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['title']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['creator']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['from']);?>')" href="javascript:void(0);">
													<img title="Add media to your cart for free download or purchase." onmouseout="this.src='../images/profile/download_down.gif'" onmouseover="this.src='../images/profile/download-over_down.gif'" src="../images/profile/download_down.gif"></a>
												<?php
												}/* else{
												?>
												<img src="images/profile/download-over_down.gif" />
												<?php
												} */
												?>
											</div>
											</div>
										<?php
											$get_all_img_fan ="";
											$galleryImagestitle_fan="";
											$mediaSrc_fan ="";
										}else if($combine_arr[$test_count][$newtest_count]['media_type'] == 116){
											$get_channel_media = $new_profile_media_classobj ->get_all_media_selected_channel($combine_arr[$test_count][$newtest_count]['id']);
										?>
											<div class="blkplayer"><div class="player">
												<input type="hidden" id="channel_media_song<?php echo $newtest_count;?>" value="<?php echo $get_channel_media['songs']; ?>"/>
												<input type="hidden" id="channel_media_video<?php echo $newtest_count;?>" value="<?php echo $get_channel_media['videos']; ?>"/>
												<a href="javascript:void(0);"><img title="Play file now." src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" onclick="PlayAll('channel_media_video<?php echo $newtest_count;?>','channel_media_song<?php echo $newtest_count;?>')"/></a>
												<a href="javascript:void(0);"><img title="Add file to the bottom of your playlist without stopping current media." src="images/profile/add.gif"  onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'" onclick="showPlayer('v','<?php if($combine_arr[$test_count][$newtest_count]['id']!=""){echo $combine_arr[$test_count][$newtest_count]['id'];}else{ echo $combine_arr[$test_count][$newtest_count]['id'];}?>','addvi');"/></a>
												<?php
												if(($combine_arr[$test_count][$newtest_count]['sharing_preference']==5 || $combine_arr[$test_count][$newtest_count]['sharing_preference']==2) && $combine_arr[$test_count][$newtest_count]['media_type'] == 114){
													?>
													<a onclick="download_directly_song('<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['title']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['creator']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['from']);?>')" href="javascript:void(0);">
													<img title="Add media to your cart for free download or purchase." onmouseout="this.src='../images/profile/download_down.gif'" onmouseover="this.src='../images/profile/download-over_down.gif'" src="../images/profile/download_down.gif"></a>
												<?php
												}/* else{
												?>
												<img src="images/profile/download-over_down.gif" />
												<?php
												} */
												?>
											</div>
											</div>
										<?php
										}
										else{
										?>
											<div class="blkplayer"><div class="player">
											<a href="javascript:void(0);"><img title="Play file now." src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" /></a>
											<a href="javascript:void(0);"><img title="Add file to the bottom of your playlist without stopping current media." src="images/profile/add.gif"  onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'"/></a>
											<?php
												if(($combine_arr[$test_count][$newtest_count]['sharing_preference']==5 || $combine_arr[$test_count][$newtest_count]['sharing_preference']==2) && $combine_arr[$test_count][$newtest_count]['media_type'] == 114){
													?>
													<a onclick="download_directly_song('<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['title']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['creator']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['from']);?>')" href="javascript:void(0);">
													<img title="Add media to your cart for free download or purchase." onmouseout="this.src='../images/profile/download_down.gif'" onmouseover="this.src='../images/profile/download-over_down.gif'" src="../images/profile/download_down.gif"></a>
												<?php
												}/* else{
												?>
												<img src="images/profile/download-over_down.gif" />
												<?php
												} */
												?>
											</div>
											</div>
										<?php
										}
										?>
										<div class="icon" onclick="runAccordion(<?php echo $combine_arr[$test_count][$newtest_count]['id']; ?>);">
											<div onselectstart="return false;"><img title="View details for media." src="images/profile/view.jpg" /></div>
										</div>
										<div class="icon">&nbsp;<!--<a href="add_media.php?edit=<?php// echo $combine_arr[$test_count][$newtest_count]['id'];?>"><img title="Edit media." src="images/profile/edit.png" /></a>--></div>
										<div class="icon"><a onclick="return confirmdelete('<?php echo $combine_arr[$test_count][$newtest_count]['title'];?>','<?php if($combine_arr[$test_count][$newtest_count]['name']=="Galleries/Images"){echo "Images";}else{echo $combine_arr[$test_count][$newtest_count]['name'];} ?>')" href="delete_media.php?id=<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>&whos_media=media_creator_creator"><img title="Remove media from your library." src="images/profile/delete.png" /></a></div>
										<div class="expandable" id="Accordion<?php echo $combine_arr[$test_count][$newtest_count]['id']; ?>Content">
											<div class="affiBlock">
												<div class="RowCont">
													<div class="sideL">Plays :</div>
													<div class="sideR"><a href="" target="_blank"><?php if($combine_arr[$test_count][$newtest_count]['play_count']!=""){ echo $combine_arr[$test_count][$newtest_count]['play_count'];}else{ echo "0";}?></a></div>
												</div>
												<?php
													if($combine_arr[$test_count][$newtest_count]['sharing_preference']==5 || $combine_arr[$test_count][$newtest_count]['sharing_preference']==2){
													?>
													<div class="RowCont">
														<div class="sideL">Downloads :</div>
														<div class="sideR"><a href="javascript:void(0);"><?php echo $combine_arr[$test_count][$newtest_count]['download_count'];?></a></div>
													</div>
													<?php
													}
													if($combine_arr[$test_count][$newtest_count]['media_type']=='113')
													{
												
													}
													else
													{
												?>	
												<!--<div class="RowCont">
													<div class="sideL">Media URL :</div>
													<div class="sideR"><a href="" target="_blank">www.<?php echo $domainname; ?></a></div>
												</div>-->
												<?php
													}
												?>
											</div>
										</div>
									</div>
									<?php
								}
								else if($combine_arr[$test_count][$newtest_count]['media_from']=="media_create_new")
								{
									?>
										<div class="tableCont" id="AccordionContainer">
										<div class="blkD" style="width:60px;"><?php if($combine_arr[$test_count][$newtest_count]['name']=="Galleries/Images"){echo "Images";}else{echo $combine_arr[$test_count][$newtest_count]['name'];} ?></div>
										<div class="blkI" style="width:127px;"><?php
										$exp_creator_info = explode("|",$combine_arr[$test_count][$newtest_count]['creator_info']);
										$pro_url = $new_profile_media_classobj->get_creator_url($exp_creator_info[0],$exp_creator_info[1]);
										?>
										<a href="<?php if($pro_url['profile_url'] !=""){ echo "/".$pro_url['profile_url']; }else { echo "javascript:void(0)"; }?>">
										<?php if($combine_arr[$test_count][$newtest_count]['creator']!=""){
										//$endpos = strpos($combine_arr[$test_count][$newtest_count]['creator'],"(");
										//if($endpos !="" || $endpos !=false) {
										//echo substr($combine_arr[$test_count][$newtest_count]['creator'],0,$endpos);} else {
										echo $combine_arr[$test_count][$newtest_count]['creator']; }
										else{?>&nbsp;<?php } ?></a></div>
										<div class="blkK"><?php
										$exp_from_info = explode("|",$combine_arr[$test_count][$newtest_count]['from_info']);
										$pro_url_from = $new_profile_media_classobj->get_from_url($exp_from_info[0],$exp_from_info[1]);
										?>
										<a href="<?php if($pro_url_from['profile_url'] !=""){ echo "/".$pro_url_from['profile_url']; }else { echo "javascript:void(0)"; }?>">
										<?php if($combine_arr[$test_count][$newtest_count]['from']!=""){
										//$endpos = strpos($combine_arr[$test_count][$newtest_count]['from'],"(");
										//if($endpos !="" || $endpos != false) {
										//echo substr($combine_arr[$test_count][$newtest_count]['from'],0,$endpos);} else {
										echo $combine_arr[$test_count][$newtest_count]['from']; }
										else{?>&nbsp;<?php } ?></a></div>
										<div class="blkB"><?php
												if($combine_arr[$test_count][$newtest_count]['title']!="")
												{
													if($combine_arr[$test_count][$newtest_count]['media_type'] == 114)
													{
														//$get_media_track = $new_profile_media_classobj->get_media_track($combine_arr[$test_count][$newtest_count]['id']); 
														if($combine_arr[$test_count][$newtest_count]['track'] != "" && $combine_arr[$test_count][$newtest_count]['track'] != NULL && $combine_arr[$test_count][$newtest_count]['track'] !=" "){
															echo $combine_arr[$test_count][$newtest_count]['track'].".".stripslashes($combine_arr[$test_count][$newtest_count]['title']);
														}else{
															echo stripslashes($combine_arr[$test_count][$newtest_count]['title']);
														}
													}
													else{
													echo stripslashes($combine_arr[$test_count][$newtest_count]['title']); }
												}else{
												?>&nbsp;<?php
												}
													?></div>
										<?php
										if($combine_arr[$test_count][$newtest_count]['media_type']==114)
										{
										?>
											<div class="blkplayer"><div class="player">
												<a href="javascript:void(0);"><img onclick="showPlayer('a','<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','play');" onmouseout="this.src='images/profile/play.gif'" onmouseover="this.src='images/profile/play-over.gif'" src="images/profile/play.gif" title="Play file now."></a>
												<a href="javascript:void(0);"><img onclick="showPlayer('a','<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','add');" onmouseout="this.src='images/profile/add.gif'" onmouseover="this.src='images/profile/add-over.gif'" src="images/profile/add.gif" title="Add file to the bottom of your playlist without stopping current media."></a>
												<?php
												if(($combine_arr[$test_count][$newtest_count]['sharing_preference']==5 || $combine_arr[$test_count][$newtest_count]['sharing_preference']==2) && $combine_arr[$test_count][$newtest_count]['media_type'] == 114){
													?>
													<a onclick="download_directly_song('<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['title']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['creator']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['from']);?>')" href="javascript:void(0);">
													<img title="Add media to your cart for free download or purchase." onmouseout="this.src='../images/profile/download_down.gif'" onmouseover="this.src='../images/profile/download-over_down.gif'" src="../images/profile/download_down.gif"></a>
												<?php
												}/* else{
												?>
												<img src="images/profile/download-over_down.gif" />
												<?php
												} */
												?>
											</div>
											</div>
										<?php
										}else if($combine_arr[$test_count][$newtest_count]['media_type']==115)
										{
										?>
											<div class="blkplayer"><div class="player">
												<a href="javascript:void(0);"><img onclick="showPlayer('v','<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','playlist');" onmouseout="this.src='images/profile/play.gif'" onmouseover="this.src='images/profile/play-over.gif'" src="images/profile/play.gif" title="Play file now."></a>
												<a href="javascript:void(0);"><img onclick="showPlayer('v','<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','addvi');" onmouseout="this.src='images/profile/add.gif'" onmouseover="this.src='images/profile/add-over.gif'" src="images/profile/add.gif" title="Add file to the bottom of your playlist without stopping current media."></a>
												<?php
												if(($combine_arr[$test_count][$newtest_count]['sharing_preference']==5 || $combine_arr[$test_count][$newtest_count]['sharing_preference']==2) && $combine_arr[$test_count][$newtest_count]['media_type'] == 114){
													?>
													<a onclick="download_directly_song('<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['title']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['creator']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['from']);?>')" href="javascript:void(0);">
													<img title="Add media to your cart for free download or purchase." onmouseout="this.src='../images/profile/download_down.gif'" onmouseover="this.src='../images/profile/download-over_down.gif'" src="../images/profile/download_down.gif"></a>
												<?php
												}/* else{
												?>
												<img src="images/profile/download-over_down.gif" />
												<?php
												} */
												?>
											</div>
											</div>
										<?php
										}else if($combine_arr[$test_count][$newtest_count]['media_type']==113){
											$get_images_fan = $new_profile_media_classobj ->get_all_gallery_images($combine_arr[$test_count][$newtest_count]['id']);
											for($count_img=0;$count_img<=count($get_images_fan);$count_img++){
												if($get_all_img_fan ==""){
													$get_all_img_fan = $get_images_fan[$count_img]['image_name'];
												}else{
													$get_all_img_fan = $get_all_img_fan.",".$get_images_fan[$count_img]['image_name'];
												}
												if($galleryImagestitle_fan ==""){
													$galleryImagestitle_fan = $get_images_fan[$count_img]['image_title'];
												}else{
													$galleryImagestitle_fan = $galleryImagestitle_fan.",".$get_images_fan[$count_img]['image_title'];
												}
												$mediaSrc_fan = $get_images_fan[0]['image_name'];
											}
											$gal_id_fan = $get_images_fan[$count_img]['media_id'];
											$get_other_user_id = $user_keys_data->get_general_user_idby_mediaid($combine_arr[$test_count][$newtest_count]['id']);
											$orgPath_fan = $s3->getNewURI_otheruser("medgallery",$get_other_user_id);//"https://medgallery.s3.amazonaws.com/";
											$thumbPath_fan = $s3->getNewURI_otheruser("medgalthumb",$get_other_user_id);//"https://medgalthumb.s3.amazonaws.com/";
											
										?>
											<div class="blkplayer"><div class="player">
											<a href="javascript:void(0);"><img title="Play file now." src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" onclick="checkload('<?php echo $mediaSrc_fan;?>','<?php echo $get_all_img_fan; ?>','<?php echo $thumbPath_fan; ?>','<?php echo $orgPath_fan; ?>','<?php echo $galleryImagestitle_fan; ?>','<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>')"/></a>
											<!--<a href="javascript:void(0);"><img title="Add file to the bottom of your playlist without stopping current media." src="images/profile/add.gif"  onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'"/></a>-->
											<?php
												if(($combine_arr[$test_count][$newtest_count]['sharing_preference']==5 || $combine_arr[$test_count][$newtest_count]['sharing_preference']==2) && $combine_arr[$test_count][$newtest_count]['media_type'] == 114){
													?>
													<a onclick="download_directly_song('<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['title']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['creator']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['from']);?>')" href="javascript:void(0);">
													<img title="Add media to your cart for free download or purchase." onmouseout="this.src='../images/profile/download_down.gif'" onmouseover="this.src='../images/profile/download-over_down.gif'" src="../images/profile/download_down.gif"></a>
												<?php
												}/* else{
												?>
												<img src="images/profile/download-over_down.gif" />
												<?php
												} */
												?>
											</div>
											</div>
										<?php
											$get_all_img_fan ="";
											$galleryImagestitle_fan="";
											$mediaSrc_fan ="";
										}else if($combine_arr[$test_count][$newtest_count]['media_type'] == 116){
											$get_channel_media = $new_profile_media_classobj ->get_all_media_selected_channel($combine_arr[$test_count][$newtest_count]['id']);
										?>
											<div class="blkplayer"><div class="player">
												<input type="hidden" id="channel_media_song<?php echo $newtest_count;?>" value="<?php echo $get_channel_media['songs']; ?>"/>
												<input type="hidden" id="channel_media_video<?php echo $newtest_count;?>" value="<?php echo $get_channel_media['videos']; ?>"/>
												<a href="javascript:void(0);"><img title="Play file now." src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" onclick="PlayAll('channel_media_video<?php echo $newtest_count;?>','channel_media_song<?php echo $newtest_count;?>')"/></a>
												<a href="javascript:void(0);"><img title="Add file to the bottom of your playlist without stopping current media." src="images/profile/add.gif"  onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'" onclick="showPlayer('v','<?php if($combine_arr[$test_count][$newtest_count]['id']!=""){echo $combine_arr[$test_count][$newtest_count]['id'];}else{ echo $combine_arr[$test_count][$newtest_count]['id'];}?>','addvi');"/></a>
												<?php
												if(($combine_arr[$test_count][$newtest_count]['sharing_preference']==5 || $combine_arr[$test_count][$newtest_count]['sharing_preference']==2) && $combine_arr[$test_count][$newtest_count]['media_type'] == 114){
													?>
													<a onclick="download_directly_song('<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['title']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['creator']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['from']);?>')" href="javascript:void(0);">
													<img title="Add media to your cart for free download or purchase." onmouseout="this.src='../images/profile/download_down.gif'" onmouseover="this.src='../images/profile/download-over_down.gif'" src="../images/profile/download_down.gif"></a>
												<?php
												}/* else{
												?>
												<img src="images/profile/download-over_down.gif" />
												<?php
												} */
												?>
											</div>
											</div>
										<?php
										}
										else{
										?>
											<div class="blkplayer"><div class="player">
											<a href="javascript:void(0);"><img title="Play file now." src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" /></a>
											<a href="javascript:void(0);"><img title="Add file to the bottom of your playlist without stopping current media." src="images/profile/add.gif"  onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'"/></a>
											<?php
												if(($combine_arr[$test_count][$newtest_count]['sharing_preference']==5 || $combine_arr[$test_count][$newtest_count]['sharing_preference']==2) && $combine_arr[$test_count][$newtest_count]['media_type'] == 114){
													?>
													<a onclick="download_directly_song('<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['title']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['creator']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['from']);?>')" href="javascript:void(0);">
													<img title="Add media to your cart for free download or purchase." onmouseout="this.src='../images/profile/download_down.gif'" onmouseover="this.src='../images/profile/download-over_down.gif'" src="../images/profile/download_down.gif"></a>
												<?php
												}/* else{
												?>
												<img src="images/profile/download-over_down.gif" />
												<?php
												} */
												?>
											</div>
											</div>
										<?php
										}
										?>
										<div class="icon" onclick="runAccordion(<?php echo $combine_arr[$test_count][$newtest_count]['id']; ?>);">
											<div onselectstart="return false;"><img title="View details for media." src="images/profile/view.jpg" /></div>
										</div>
										<div class="icon">&nbsp;<!--<a href="add_media.php?edit=<?php// echo $combine_arr[$test_count][$newtest_count]['id'];?>"><img title="Edit media." src="images/profile/edit.png" /></a>--></div>
										<div class="icon"><a onclick="return confirmdelete('<?php echo $combine_arr[$test_count][$newtest_count]['title'];?>','<?php if($combine_arr[$test_count][$newtest_count]['name']=="Galleries/Images"){echo "Images";}else{echo $combine_arr[$test_count][$newtest_count]['name'];} ?>')" href="delete_media.php?id=<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>&whos_media=media_create_new"><img title="Remove media from your library." src="images/profile/delete.png" /></a></div>
										<div class="expandable" id="Accordion<?php echo $combine_arr[$test_count][$newtest_count]['id']; ?>Content">
											<div class="affiBlock">
												<div class="RowCont">
													<div class="sideL">Plays :</div>
													<div class="sideR"><a href="" target="_blank"><?php if($combine_arr[$test_count][$newtest_count]['play_count']!=""){ echo $combine_arr[$test_count][$newtest_count]['play_count'];}else{ echo "0";}?></a></div>
												</div>
												<?php
													if($combine_arr[$test_count][$newtest_count]['sharing_preference']==5 || $combine_arr[$test_count][$newtest_count]['sharing_preference']==2){
													?>
													<div class="RowCont">
														<div class="sideL">Downloads :</div>
														<div class="sideR"><a href="javascript:void(0);"><?php echo $combine_arr[$test_count][$newtest_count]['download_count'];?></a></div>
													</div>
													<?php
													}
													if($combine_arr[$test_count][$newtest_count]['media_type']=='113')
													{
													
													}
													else
													{
												?>	
												<!--<div class="RowCont">
													<div class="sideL">Media URL :</div>
													<div class="sideR"><a href="" target="_blank">www.<?php echo $domainname; ?></a></div>
												</div>-->
												<?php
													}
												?>
											</div>
										</div>
									</div>
									<?php
								}
								else if($combine_arr[$test_count][$newtest_count]['media_from']=="media_from_from")
								{
									?>
										<div class="tableCont" id="AccordionContainer">
										<div class="blkD" style="width:60px;"><?php if($combine_arr[$test_count][$newtest_count]['name']=="Galleries/Images"){echo "Images";}else{echo $combine_arr[$test_count][$newtest_count]['name'];} ?></div>
										<div class="blkI" style="width:127px;"><?php
										$exp_creator_info = explode("|",$combine_arr[$test_count][$newtest_count]['creator_info']);
										$pro_url = $new_profile_media_classobj->get_creator_url($exp_creator_info[0],$exp_creator_info[1]);
										?>
										<a href="<?php if($pro_url['profile_url'] !=""){ echo "/".$pro_url['profile_url']; }else { echo "javascript:void(0)"; }?>">
										<?php if($combine_arr[$test_count][$newtest_count]['creator']!=""){
										//$endpos = strpos($combine_arr[$test_count][$newtest_count]['creator'],"(");
										//if($endpos !="" || $endpos !=false) {
										//echo substr($combine_arr[$test_count][$newtest_count]['creator'],0,$endpos);} else {
										echo $combine_arr[$test_count][$newtest_count]['creator']; }
										else{?>&nbsp;<?php } ?></a></div>
										<div class="blkK"><?php
										$exp_from_info = explode("|",$combine_arr[$test_count][$newtest_count]['from_info']);
										$pro_url_from = $new_profile_media_classobj->get_from_url($exp_from_info[0],$exp_from_info[1]);
										?>
										<a href="<?php if($pro_url_from['profile_url'] !=""){ echo "/".$pro_url_from['profile_url']; }else { echo "javascript:void(0)"; }?>">
										<?php if($combine_arr[$test_count][$newtest_count]['from']!=""){
										//$endpos = strpos($combine_arr[$test_count][$newtest_count]['from'],"(");
										//if($endpos !="" || $endpos != false) {
										//echo substr($combine_arr[$test_count][$newtest_count]['from'],0,$endpos);} else {
										echo $combine_arr[$test_count][$newtest_count]['from']; }
										else{?>&nbsp;<?php } ?></a></div>
										<div class="blkB"><?php
												if($combine_arr[$test_count][$newtest_count]['title']!="")
												{
													if($combine_arr[$test_count][$newtest_count]['media_type'] == 114)
													{
														//$get_media_track = $new_profile_media_classobj->get_media_track($combine_arr[$test_count][$newtest_count]['id']); 
														if($combine_arr[$test_count][$newtest_count]['track'] != "" && $combine_arr[$test_count][$newtest_count]['track'] != NULL && $combine_arr[$test_count][$newtest_count]['track'] !=" "){
															echo $combine_arr[$test_count][$newtest_count]['track'].".".stripslashes($combine_arr[$test_count][$newtest_count]['title']);
														}else{
															echo stripslashes($combine_arr[$test_count][$newtest_count]['title']);
														}
													}
													else{
													echo stripslashes($combine_arr[$test_count][$newtest_count]['title']); }
												}else{
												?>&nbsp;<?php
												}
													?></div>
										<?php
										if($combine_arr[$test_count][$newtest_count]['media_type']==114)
										{
										?>
											<div class="blkplayer"><div class="player">
												<a href="javascript:void(0);"><img onclick="showPlayer('a','<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','play');" onmouseout="this.src='images/profile/play.gif'" onmouseover="this.src='images/profile/play-over.gif'" src="images/profile/play.gif" title="Play file now."></a>
												<a href="javascript:void(0);"><img onclick="showPlayer('a','<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','add');" onmouseout="this.src='images/profile/add.gif'" onmouseover="this.src='images/profile/add-over.gif'" src="images/profile/add.gif" title="Add file to the bottom of your playlist without stopping current media."></a>
												<?php
												if(($combine_arr[$test_count][$newtest_count]['sharing_preference']==5 || $combine_arr[$test_count][$newtest_count]['sharing_preference']==2) && $combine_arr[$test_count][$newtest_count]['media_type'] == 114){
													?>
													<a onclick="download_directly_song('<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['title']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['creator']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['from']);?>')" href="javascript:void(0);">
													<img title="Add media to your cart for free download or purchase." onmouseout="this.src='../images/profile/download_down.gif'" onmouseover="this.src='../images/profile/download-over_down.gif'" src="../images/profile/download_down.gif"></a>
												<?php
												}/* else{
												?>
												<img src="images/profile/download-over_down.gif" />
												<?php
												} */
												?>
											</div>
											</div>
										<?php
										}else if($combine_arr[$test_count][$newtest_count]['media_type']==115)
										{
										?>
											<div class="blkplayer"><div class="player">
												<a href="javascript:void(0);"><img onclick="showPlayer('v','<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','playlist');" onmouseout="this.src='images/profile/play.gif'" onmouseover="this.src='images/profile/play-over.gif'" src="images/profile/play.gif" title="Play file now."></a>
												<a href="javascript:void(0);"><img onclick="showPlayer('v','<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','addvi');" onmouseout="this.src='images/profile/add.gif'" onmouseover="this.src='images/profile/add-over.gif'" src="images/profile/add.gif" title="Add file to the bottom of your playlist without stopping current media."></a>
												<?php
												if(($combine_arr[$test_count][$newtest_count]['sharing_preference']==5 || $combine_arr[$test_count][$newtest_count]['sharing_preference']==2) && $combine_arr[$test_count][$newtest_count]['media_type'] == 114){
													?>
													<a onclick="download_directly_song('<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['title']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['creator']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['from']);?>')" href="javascript:void(0);">
													<img title="Add media to your cart for free download or purchase." onmouseout="this.src='../images/profile/download_down.gif'" onmouseover="this.src='../images/profile/download-over_down.gif'" src="../images/profile/download_down.gif"></a>
												<?php
												}/* else{
												?>
												<img src="images/profile/download-over_down.gif" />
												<?php
												} */
												?>
											</div>
											</div>
										<?php
										}else if($combine_arr[$test_count][$newtest_count]['media_type']==113){
											$get_images_fan = $new_profile_media_classobj ->get_all_gallery_images($combine_arr[$test_count][$newtest_count]['id']);
											for($count_img=0;$count_img<=count($get_images_fan);$count_img++){
												if($get_all_img_fan ==""){
													$get_all_img_fan = $get_images_fan[$count_img]['image_name'];
												}else{
													$get_all_img_fan = $get_all_img_fan.",".$get_images_fan[$count_img]['image_name'];
												}
												if($galleryImagestitle_fan ==""){
													$galleryImagestitle_fan = $get_images_fan[$count_img]['image_title'];
												}else{
													$galleryImagestitle_fan = $galleryImagestitle_fan.",".$get_images_fan[$count_img]['image_title'];
												}
												$mediaSrc_fan = $get_images_fan[0]['image_name'];
											}
											$gal_id_fan = $get_images_fan[$count_img]['media_id'];
											$get_other_user_id = $user_keys_data->get_general_user_idby_mediaid($combine_arr[$test_count][$newtest_count]['id']);
											$orgPath_fan = $s3->getNewURI_otheruser("medgallery",$get_other_user_id);//"https://medgallery.s3.amazonaws.com/";
											$thumbPath_fan = $s3->getNewURI_otheruser("medgalthumb",$get_other_user_id);//"https://medgalthumb.s3.amazonaws.com/";
											
										?>
											<div class="blkplayer"><div class="player">
											<a href="javascript:void(0);"><img title="Play file now." src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" onclick="checkload('<?php echo $mediaSrc_fan;?>','<?php echo $get_all_img_fan; ?>','<?php echo $thumbPath_fan; ?>','<?php echo $orgPath_fan; ?>','<?php echo $galleryImagestitle_fan; ?>','<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>')"/></a>
											<!--<a href="javascript:void(0);"><img title="Add file to the bottom of your playlist without stopping current media." src="images/profile/add.gif"  onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'"/></a>-->
											<?php
												if(($combine_arr[$test_count][$newtest_count]['sharing_preference']==5 || $combine_arr[$test_count][$newtest_count]['sharing_preference']==2) && $combine_arr[$test_count][$newtest_count]['media_type'] == 114){
													?>
													<a onclick="download_directly_song('<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['title']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['creator']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['from']);?>')" href="javascript:void(0);">
													<img title="Add media to your cart for free download or purchase." onmouseout="this.src='../images/profile/download_down.gif'" onmouseover="this.src='../images/profile/download-over_down.gif'" src="../images/profile/download_down.gif"></a>
												<?php
												}/* else{
												?>
												<img src="images/profile/download-over_down.gif" />
												<?php
												} */
												?>
											</div>
											</div>
										<?php
											$get_all_img_fan ="";
											$galleryImagestitle_fan="";
											$mediaSrc_fan ="";
										}else if($combine_arr[$test_count][$newtest_count]['media_type'] == 116){
											$get_channel_media = $new_profile_media_classobj ->get_all_media_selected_channel($combine_arr[$test_count][$newtest_count]['id']);
										?>
											<div class="blkplayer"><div class="player">
												<input type="hidden" id="channel_media_song<?php echo $newtest_count;?>" value="<?php echo $get_channel_media['songs']; ?>"/>
												<input type="hidden" id="channel_media_video<?php echo $newtest_count;?>" value="<?php echo $get_channel_media['videos']; ?>"/>
												<a href="javascript:void(0);"><img title="Play file now." src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" onclick="PlayAll('channel_media_video<?php echo $newtest_count;?>','channel_media_song<?php echo $newtest_count;?>')"/></a>
												<a href="javascript:void(0);"><img title="Add file to the bottom of your playlist without stopping current media." src="images/profile/add.gif"  onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'" onclick="showPlayer('v','<?php if($combine_arr[$test_count][$newtest_count]['id']!=""){echo $combine_arr[$test_count][$newtest_count]['id'];}else{ echo $combine_arr[$test_count][$newtest_count]['id'];}?>','addvi');"/></a>
												<?php
												if(($combine_arr[$test_count][$newtest_count]['sharing_preference']==5 || $combine_arr[$test_count][$newtest_count]['sharing_preference']==2) && $combine_arr[$test_count][$newtest_count]['media_type'] == 114){
													?>
													<a onclick="download_directly_song('<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['title']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['creator']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['from']);?>')" href="javascript:void(0);">
													<img title="Add media to your cart for free download or purchase." onmouseout="this.src='../images/profile/download_down.gif'" onmouseover="this.src='../images/profile/download-over_down.gif'" src="../images/profile/download_down.gif"></a>
												<?php
												}/* else{
												?>
												<img src="images/profile/download-over_down.gif" />
												<?php
												} */
												?>
											</div>
											</div>
										<?php
										}
										else{
										?>
											<div class="blkplayer"><div class="player">
											<a href="javascript:void(0);"><img title="Play file now." src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" /></a>
											<a href="javascript:void(0);"><img title="Add file to the bottom of your playlist without stopping current media." src="images/profile/add.gif"  onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'"/></a>
											<?php
												if(($combine_arr[$test_count][$newtest_count]['sharing_preference']==5 || $combine_arr[$test_count][$newtest_count]['sharing_preference']==2) && $combine_arr[$test_count][$newtest_count]['media_type'] == 114){
													?>
													<a onclick="download_directly_song('<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['title']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['creator']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['from']);?>')" href="javascript:void(0);">
													<img title="Add media to your cart for free download or purchase." onmouseout="this.src='../images/profile/download_down.gif'" onmouseover="this.src='../images/profile/download-over_down.gif'" src="../images/profile/download_down.gif"></a>
												<?php
												}/* else{
												?>
												<img src="images/profile/download-over_down.gif" />
												<?php
												} */
												?>
											</div>
											</div>
										<?php
										}
										?>
										<div class="icon" onclick="runAccordion(<?php echo $combine_arr[$test_count][$newtest_count]['id']; ?>);">
											<div onselectstart="return false;"><img title="View details for media." src="images/profile/view.jpg" /></div>
										</div>
										<div class="icon">&nbsp;<!--<a href="add_media.php?edit=<?php// echo $combine_arr[$test_count][$newtest_count]['id'];?>"><img title="Edit media." src="images/profile/edit.png" /></a>--></div>
										<div class="icon"><a onclick="return confirmdelete('<?php echo $combine_arr[$test_count][$newtest_count]['title'];?>','<?php if($combine_arr[$test_count][$newtest_count]['name']=="Galleries/Images"){echo "Images";}else{echo $combine_arr[$test_count][$newtest_count]['name'];} ?>')" href="delete_media.php?id=<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>&whos_media=media_from_from"><img title="Remove media from your library." src="images/profile/delete.png" /></a></div>
										<div class="expandable" id="Accordion<?php echo $combine_arr[$test_count][$newtest_count]['id']; ?>Content">
											<div class="affiBlock">
												<div class="RowCont">
													<div class="sideL">Plays :</div>
													<div class="sideR"><a href="" target="_blank"><?php if($combine_arr[$test_count][$newtest_count]['play_count']!=""){ echo $combine_arr[$test_count][$newtest_count]['play_count'];}else{ echo "0";}?></a></div>
												</div>
												<?php
													if($combine_arr[$test_count][$newtest_count]['sharing_preference']==5 || $combine_arr[$test_count][$newtest_count]['sharing_preference']==2){
													?>
													<div class="RowCont">
														<div class="sideL">Downloads :</div>
														<div class="sideR"><a href="javascript:void(0);"><?php echo $combine_arr[$test_count][$newtest_count]['download_count'];?></a></div>
													</div>
													<?php
													}
													if($combine_arr[$test_count][$newtest_count]['media_type']=='113')
													{
													
													}
													else
													{
												?>	
												<!--<div class="RowCont">
													<div class="sideL">Media URL :</div>
													<div class="sideR"><a href="" target="_blank">www.<?php echo $domainname; ?></a></div>
												</div>-->
												<?php
													}
												?>
											</div>
										</div>
									</div>
									<?php
								}
								else if($combine_arr[$test_count][$newtest_count]['media_from']=="media_creator_from_tag")
								{
									$exp_deleted_id = explode(",",$getgeneral['deleted_media_by_tag_user']);
									if($newtest_count==0)
									{
										$newdel_count=1;
									}else{
									$newdel_count=$newtest_count;
									}
									if($exp_deleted_id[$newdel_count] != $combine_arr[$test_count][$newtest_count]['id'])
									{
									?>
										<div class="tableCont" id="AccordionContainer">
										<div class="blkD" style="width:60px;"><?php if($combine_arr[$test_count][$newtest_count]['name']=="Galleries/Images"){echo "Images";}else{echo $combine_arr[$test_count][$newtest_count]['name'];} ?></div>
										<div class="blkI" style="width:127px;"><?php
										$exp_creator_info = explode("|",$combine_arr[$test_count][$newtest_count]['creator_info']);
										$pro_url = $new_profile_media_classobj->get_creator_url($exp_creator_info[0],$exp_creator_info[1]);
										?>
										<a href="<?php if($pro_url['profile_url'] !=""){ echo "/".$pro_url['profile_url']; }else { echo "javascript:void(0)"; }?>">
										<?php if($combine_arr[$test_count][$newtest_count]['creator']!=""){
										//$endpos = strpos($combine_arr[$test_count][$newtest_count]['creator'],"(");
										//if($endpos !="" || $endpos !=false) {
										//echo substr($combine_arr[$test_count][$newtest_count]['creator'],0,$endpos);} else {
										echo $combine_arr[$test_count][$newtest_count]['creator']; }
										else{?>&nbsp;<?php } ?></a></div>
										<div class="blkK"><?php
										$exp_from_info = explode("|",$combine_arr[$test_count][$newtest_count]['from_info']);
										$pro_url_from = $new_profile_media_classobj->get_from_url($exp_from_info[0],$exp_from_info[1]);
										?>
										<a href="<?php if($pro_url_from['profile_url'] !=""){ echo "/".$pro_url_from['profile_url']; }else { echo "javascript:void(0)"; }?>">
										<?php if($combine_arr[$test_count][$newtest_count]['from']!=""){
										//$endpos = strpos($combine_arr[$test_count][$newtest_count]['from'],"(");
										//if($endpos !="" || $endpos != false) {
										//echo substr($combine_arr[$test_count][$newtest_count]['from'],0,$endpos);} else {
										echo $combine_arr[$test_count][$newtest_count]['from']; }
										else{?>&nbsp;<?php } ?></a></div>
										<div class="blkB"><?php
												if($combine_arr[$test_count][$newtest_count]['title']!="")
												{
													if($combine_arr[$test_count][$newtest_count]['media_type'] == 114)
													{
														//$get_media_track = $new_profile_media_classobj->get_media_track($combine_arr[$test_count][$newtest_count]['id']); 
														if($combine_arr[$test_count][$newtest_count]['track'] != "" && $combine_arr[$test_count][$newtest_count]['track'] != NULL && $combine_arr[$test_count][$newtest_count]['track'] !=" "){
															echo $combine_arr[$test_count][$newtest_count]['track'].".".stripslashes($combine_arr[$test_count][$newtest_count]['title']);
														}else{
															echo stripslashes($combine_arr[$test_count][$newtest_count]['title']);
														}
													}
													else{
													echo stripslashes($combine_arr[$test_count][$newtest_count]['title']); }
												}else{
												?>&nbsp;<?php
												}
													?></div>
										<?php
										if($combine_arr[$test_count][$newtest_count]['media_type']==114)
										{
										?>
											<div class="blkplayer"><div class="player">
												<a href="javascript:void(0);"><img onclick="showPlayer('a','<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','play');" onmouseout="this.src='images/profile/play.gif'" onmouseover="this.src='images/profile/play-over.gif'" src="images/profile/play.gif" title="Play file now."></a>
												<a href="javascript:void(0);"><img onclick="showPlayer('a','<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','add');" onmouseout="this.src='images/profile/add.gif'" onmouseover="this.src='images/profile/add-over.gif'" src="images/profile/add.gif" title="Add file to the bottom of your playlist without stopping current media."></a>
												<?php
												if(($combine_arr[$test_count][$newtest_count]['sharing_preference']==5 || $combine_arr[$test_count][$newtest_count]['sharing_preference']==2) && $combine_arr[$test_count][$newtest_count]['media_type'] == 114){
													?>
													<a onclick="download_directly_song('<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['title']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['creator']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['from']);?>')" href="javascript:void(0);">
													<img title="Add media to your cart for free download or purchase." onmouseout="this.src='../images/profile/download_down.gif'" onmouseover="this.src='../images/profile/download-over_down.gif'" src="../images/profile/download_down.gif"></a>
												<?php
												}/* else{
												?>
												<img src="images/profile/download-over_down.gif" />
												<?php
												} */
												?>
											</div>
											</div>
										<?php
										}else if($combine_arr[$test_count][$newtest_count]['media_type']==115)
										{
										?>
											<div class="blkplayer"><div class="player">
												<a href="javascript:void(0);"><img onclick="showPlayer('v','<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','playlist');" onmouseout="this.src='images/profile/play.gif'" onmouseover="this.src='images/profile/play-over.gif'" src="images/profile/play.gif" title="Play file now."></a>
												<a href="javascript:void(0);"><img onclick="showPlayer('v','<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','addvi');" onmouseout="this.src='images/profile/add.gif'" onmouseover="this.src='images/profile/add-over.gif'" src="images/profile/add.gif" title="Add file to the bottom of your playlist without stopping current media."></a>
												<?php
												if(($combine_arr[$test_count][$newtest_count]['sharing_preference']==5 || $combine_arr[$test_count][$newtest_count]['sharing_preference']==2) && $combine_arr[$test_count][$newtest_count]['media_type'] == 114){
													?>
													<a onclick="download_directly_song('<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['title']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['creator']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['from']);?>')" href="javascript:void(0);">
													<img title="Add media to your cart for free download or purchase." onmouseout="this.src='../images/profile/download_down.gif'" onmouseover="this.src='../images/profile/download-over_down.gif'" src="../images/profile/download_down.gif"></a>
												<?php
												}/* else{
												?>
												<img src="images/profile/download-over_down.gif" />
												<?php
												} */
												?>
											</div>
											</div>
										<?php
										}else if($combine_arr[$test_count][$newtest_count]['media_type']==113){
											$get_images_fan = $new_profile_media_classobj ->get_all_gallery_images($combine_arr[$test_count][$newtest_count]['id']);
											for($count_img=0;$count_img<=count($get_images_fan);$count_img++){
												if($get_all_img_fan ==""){
													$get_all_img_fan = $get_images_fan[$count_img]['image_name'];
												}else{
													$get_all_img_fan = $get_all_img_fan.",".$get_images_fan[$count_img]['image_name'];
												}
												if($galleryImagestitle_fan ==""){
													$galleryImagestitle_fan = $get_images_fan[$count_img]['image_title'];
												}else{
													$galleryImagestitle_fan = $galleryImagestitle_fan.",".$get_images_fan[$count_img]['image_title'];
												}
												$mediaSrc_fan = $get_images_fan[0]['image_name'];
											}
											$gal_id_fan = $get_images_fan[$count_img]['media_id'];
											$get_other_user_id = $user_keys_data->get_general_user_idby_mediaid($combine_arr[$test_count][$newtest_count]['id']);
											$orgPath_fan = $s3->getNewURI_otheruser("medgallery",$get_other_user_id);//"https://medgallery.s3.amazonaws.com/";
											$thumbPath_fan = $s3->getNewURI_otheruser("medgalthumb",$get_other_user_id);//"https://medgalthumb.s3.amazonaws.com/";
											
										?>
											<div class="blkplayer"><div class="player">
											<a href="javascript:void(0);"><img title="Play file now." src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" onclick="checkload('<?php echo $mediaSrc_fan;?>','<?php echo $get_all_img_fan; ?>','<?php echo $thumbPath_fan; ?>','<?php echo $orgPath_fan; ?>','<?php echo $galleryImagestitle_fan; ?>','<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>')"/></a>
											<!--<a href="javascript:void(0);"><img title="Add file to the bottom of your playlist without stopping current media." src="images/profile/add.gif"  onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'"/></a>-->
											<?php
												if(($combine_arr[$test_count][$newtest_count]['sharing_preference']==5 || $combine_arr[$test_count][$newtest_count]['sharing_preference']==2) && $combine_arr[$test_count][$newtest_count]['media_type'] == 114){
													?>
													<a onclick="download_directly_song('<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['title']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['creator']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['from']);?>')" href="javascript:void(0);">
													<img title="Add media to your cart for free download or purchase." onmouseout="this.src='../images/profile/download_down.gif'" onmouseover="this.src='../images/profile/download-over_down.gif'" src="../images/profile/download_down.gif"></a>
												<?php
												}/* else{
												?>
												<img src="images/profile/download-over_down.gif" />
												<?php
												} */
												?>
											</div>
											</div>
										<?php
											$get_all_img_fan ="";
											$galleryImagestitle_fan="";
											$mediaSrc_fan ="";
										}else if($combine_arr[$test_count][$newtest_count]['media_type'] == 116){
											$get_channel_media = $new_profile_media_classobj ->get_all_media_selected_channel($combine_arr[$test_count][$newtest_count]['id']);
										?>
											<div class="blkplayer"><div class="player">
												<input type="hidden" id="channel_media_song<?php echo $newtest_count;?>" value="<?php echo $get_channel_media['songs']; ?>"/>
												<input type="hidden" id="channel_media_video<?php echo $newtest_count;?>" value="<?php echo $get_channel_media['videos']; ?>"/>
												<a href="javascript:void(0);"><img title="Play file now." src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" onclick="PlayAll('channel_media_video<?php echo $newtest_count;?>','channel_media_song<?php echo $newtest_count;?>')"/></a>
												<a href="javascript:void(0);"><img title="Add file to the bottom of your playlist without stopping current media." src="images/profile/add.gif"  onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'" onclick="showPlayer('v','<?php if($combine_arr[$test_count][$newtest_count]['id']!=""){echo $combine_arr[$test_count][$newtest_count]['id'];}else{ echo $combine_arr[$test_count][$newtest_count]['id'];}?>','addvi');"/></a>
												<?php
												if(($combine_arr[$test_count][$newtest_count]['sharing_preference']==5 || $combine_arr[$test_count][$newtest_count]['sharing_preference']==2) && $combine_arr[$test_count][$newtest_count]['media_type'] == 114){
													?>
													<a onclick="download_directly_song('<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['title']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['creator']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['from']);?>')" href="javascript:void(0);">
													<img title="Add media to your cart for free download or purchase." onmouseout="this.src='../images/profile/download_down.gif'" onmouseover="this.src='../images/profile/download-over_down.gif'" src="../images/profile/download_down.gif"></a>
												<?php
												}/* else{
												?>
												<img src="images/profile/download-over_down.gif" />
												<?php
												} */
												?>
											</div>
											</div>
										<?php
										}
										else{
										?>
											<div class="blkplayer"><div class="player">
											<a href="javascript:void(0);"><img title="Play file now." src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" /></a>
											<a href="javascript:void(0);"><img title="Add file to the bottom of your playlist without stopping current media." src="images/profile/add.gif"  onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'"/></a>
											<?php
												if(($combine_arr[$test_count][$newtest_count]['sharing_preference']==5 || $combine_arr[$test_count][$newtest_count]['sharing_preference']==2) && $combine_arr[$test_count][$newtest_count]['media_type'] == 114){
													?>
													<a onclick="download_directly_song('<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['title']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['creator']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['from']);?>')" href="javascript:void(0);">
													<img title="Add media to your cart for free download or purchase." onmouseout="this.src='../images/profile/download_down.gif'" onmouseover="this.src='../images/profile/download-over_down.gif'" src="../images/profile/download_down.gif"></a>
												<?php
												}/* else{
												?>
												<img src="images/profile/download-over_down.gif" />
												<?php
												} */
												?>
											</div>
											</div>
										<?php
										}
										?>
										<div class="icon" onclick="runAccordion(<?php echo $combine_arr[$test_count][$newtest_count]['id']; ?>);">
											<div onselectstart="return false;"><img title="View details for media." src="images/profile/view.jpg" /></div>
										</div>
										<div class="icon">&nbsp;<!--<a href="add_media.php?edit=<?php// echo $combine_arr[$test_count][$newtest_count]['id'];?>"><img title="Edit media." src="images/profile/edit.png" /></a>--></div>
										<div class="icon"><a onclick="return confirmdelete('<?php echo $combine_arr[$test_count][$newtest_count]['title'];?>','<?php if($combine_arr[$test_count][$newtest_count]['name']=="Galleries/Images"){echo "Images";}else{echo $combine_arr[$test_count][$newtest_count]['name'];} ?>')" href="delete_media.php?id=<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>&whos_media=media_creator_from_tag"><img title="Remove media from your library." src="images/profile/delete.png" /></a></div>
										<div class="expandable" id="Accordion<?php echo $combine_arr[$test_count][$newtest_count]['id']; ?>Content">
											<div class="affiBlock">
												<div class="RowCont">
													<div class="sideL">Plays :</div>
													<div class="sideR"><a href="" target="_blank"><?php if($combine_arr[$test_count][$newtest_count]['play_count']!=""){ echo $combine_arr[$test_count][$newtest_count]['play_count'];}else{ echo "0";}?></a></div>
												</div>
												<?php
													if($combine_arr[$test_count][$newtest_count]['sharing_preference']==5 || $combine_arr[$test_count][$newtest_count]['sharing_preference']==2){
													?>
													<div class="RowCont">
														<div class="sideL">Downloads :</div>
														<div class="sideR"><a href="javascript:void(0);"><?php echo $combine_arr[$test_count][$newtest_count]['download_count'];?></a></div>
													</div>
													<?php
													}
													if($combine_arr[$test_count][$newtest_count]['media_type']=='113')
													{
																										
													}
													else
													{
												?>	
												<!--<div class="RowCont">
													<div class="sideL">Media URL :</div>
													<div class="sideR"><a href="" target="_blank">www.<?php echo $domainname; ?></a></div>
												</div>-->
												<?php
													}
												?>
											</div>
										</div>
									</div>
									<?php
									}
								}
								else if($combine_arr[$test_count][$newtest_count]['media_from']=="media_coming_fan")
								{
									$chk_tag_user = $new_profile_media_classobj->get_loggedin_info($_SESSION['login_email']);
									if($chk_tag_user['general_user_id']!=$combine_arr[$test_count][$newtest_count]['general_user_id'])
									{
										//$get_media_data = $new_profile_media_classobj->get_media_data($combine_arr[$test_count][$newtest_count]['media_id']);
										//if($get_media_data!="")
										//{
							?>
										<div class="tableCont" id="AccordionContainer">
											<div class="blkD" style="width:60px;"><?php if($combine_arr[$test_count][$newtest_count]['name']!=""){if($combine_arr[$test_count][$newtest_count]['name'] =="Galleries/Images"){echo "Images";}else{echo $combine_arr[$test_count][$newtest_count]['name'];}}else{?>&nbsp;<?php } ?></div>
											<div class="blkI" style="width:127px;"><?php
											$exp_creator_info = explode("|",$combine_arr[$test_count][$newtest_count]['creator_info']);
											$pro_url = $new_profile_media_classobj->get_creator_url($exp_creator_info[0],$exp_creator_info[1]);
											?>
											<a href="<?php if($pro_url['profile_url'] !=""){ echo "/".$pro_url['profile_url']; }else { echo "javascript:void(0)"; }?>">
											<?php if($combine_arr[$test_count][$newtest_count]['creator']!=""){
											//$endpos = strpos($combine_arr[$test_count][$newtest_count]['creator'],"(");
											//if($endpos !="" || $endpos !=false) {
											//echo substr($combine_arr[$test_count][$newtest_count]['creator'],0,$endpos);} else {
											echo $combine_arr[$test_count][$newtest_count]['creator']; }
											else{?>&nbsp;<?php } ?></a></div>
											<div class="blkK"><?php
											$exp_from_info = explode("|",$combine_arr[$test_count][$newtest_count]['from_info']);
											$pro_url_from = $new_profile_media_classobj->get_from_url($exp_from_info[0],$exp_from_info[1]);
											?>
											<a href="<?php if($pro_url_from['profile_url'] !=""){ echo "/".$pro_url_from['profile_url']; }else { echo "javascript:void(0)"; }?>">
											<?php if($combine_arr[$test_count][$newtest_count]['from']!=""){
											//$endpos = strpos($combine_arr[$test_count][$newtest_count]['from'],"(");
											//if($endpos !="" || $endpos != false) {
											//echo substr($combine_arr[$test_count][$newtest_count]['from'],0,$endpos);} else {
											echo $combine_arr[$test_count][$newtest_count]['from']; }
											else{?>&nbsp;<?php } ?></a></div>
											<div class="blkB"><?php
												if($combine_arr[$test_count][$newtest_count]['title']!="")
												{
													if($combine_arr[$test_count][$newtest_count]['media_type'] == 114)
													{
														//$get_media_track = $new_profile_media_classobj->get_media_track($combine_arr[$test_count][$newtest_count]['id']); 
														if($combine_arr[$test_count][$newtest_count]['track'] != "" && $combine_arr[$test_count][$newtest_count]['track'] != NULL && $combine_arr[$test_count][$newtest_count]['track'] !=" "){
															echo $combine_arr[$test_count][$newtest_count]['track'].".".stripslashes($combine_arr[$test_count][$newtest_count]['title']);
														}else{
															echo stripslashes($combine_arr[$test_count][$newtest_count]['title']);
														}
													}
													else{
													echo stripslashes($combine_arr[$test_count][$newtest_count]['title']); }
												}else{
												?>&nbsp;<?php
												}
													?></div>
											<div class="blkplayer"><div class="player">
											<?php 
												if($combine_arr[$test_count][$newtest_count]['media_type']==113){
													$get_images_fan = $new_profile_media_classobj ->get_all_gallery_images($combine_arr[$test_count][$newtest_count]['id']);
													for($count_img=0;$count_img<=count($get_images_fan);$count_img++){
														if($get_all_img_fan==""){
															$get_all_img_fan = $get_images_fan[$count_img]['image_name'];
														}else{
															$get_all_img_fan = $get_all_img_fan.",".$get_images_fan[$count_img]['image_name'];
														}
														if($galleryImagestitle_fan ==""){
															$galleryImagestitle_fan = $get_images_fan[$count_img]['image_title'];
														}else{
															$galleryImagestitle_fan = $galleryImagestitle_fan.",".$get_images_fan[$count_img]['image_title'];
														}
														$mediaSrc_fan = $get_images_fan[0]['image_name'];
													}
													$gal_id_fan = $get_images_fan[$count_img]['media_id'];
													$get_other_user_id = $user_keys_data->get_general_user_idby_mediaid($combine_arr[$test_count][$newtest_count]['id']);
													$orgPath_fan = $s3->getNewURI_otheruser("medgallery",$get_other_user_id);//"https://medgallery.s3.amazonaws.com/";
													$thumbPath_fan = $s3->getNewURI_otheruser("medgalthumb",$get_other_user_id);//"https://medgalthumb.s3.amazonaws.com/";
												?>
												<a href="javascript:void(0);"><img title="Play file now." src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" onclick="checkload('<?php echo $mediaSrc_fan;?>','<?php echo $get_all_img_fan; ?>','<?php echo $thumbPath_fan; ?>','<?php echo $orgPath_fan; ?>','<?php echo $galleryImagestitle_fan; ?>','<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>')"/></a>
												<!--<a href="javascript:void(0);"><img title="Add file to the bottom of your playlist without stopping current media." src="images/profile/add.gif"  onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'"/></a>-->
												<?php
													$get_all_img_fan ="";
													$galleryImagestitle_fan="";
													$mediaSrc_fan ="";
												}else if($combine_arr[$test_count][$newtest_count]['media_type']==114){
												?>
												<a href="javascript:void(0);"><img title="Play file now." src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" onclick="showPlayer('a','<?php if($combine_arr[$test_count][$newtest_count]['id']!=""){echo $combine_arr[$test_count][$newtest_count]['id'];}else{echo $combine_arr[$test_count][$newtest_count]['id'];}?>','play');"/></a>
												<a href="javascript:void(0);"><img title="Add file to the bottom of your playlist without stopping current media." src="images/profile/add.gif"  onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'" onclick="showPlayer('a','<?php if($combine_arr[$test_count][$newtest_count]['id']!=""){echo $combine_arr[$test_count][$newtest_count]['id'];}else{echo $combine_arr[$test_count][$newtest_count]['id'];}?>','add');"/></a>
												<?php
												}else if($combine_arr[$test_count][$newtest_count]['media_type']==115){
												?>
												<a href="javascript:void(0);"><img title="Play file now." src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" onclick="showPlayer('v','<?php if($combine_arr[$test_count][$newtest_count]['id']!=""){echo $combine_arr[$test_count][$newtest_count]['id'];}else{ echo $combine_arr[$test_count][$newtest_count]['id'];}?>','playlist');"/></a>
												<a href="javascript:void(0);"><img title="Add file to the bottom of your playlist without stopping current media." src="images/profile/add.gif"  onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'" onclick="showPlayer('v','<?php if($combine_arr[$test_count][$newtest_count]['id']!=""){echo $combine_arr[$test_count][$newtest_count]['id'];}else{ echo $combine_arr[$test_count][$newtest_count]['id'];}?>','addvi');"/></a>
												<?php
												}else if($combine_arr[$test_count][$newtest_count]['media_type'] == 116){
													$get_channel_media = $new_profile_media_classobj ->get_all_media_selected_channel($combine_arr[$test_count][$newtest_count]['id']);
												?>
													<input type="hidden" id="channel_media_song<?php echo $newtest_count;?>" value="<?php echo $get_channel_media['songs']; ?>"/>
													<input type="hidden" id="channel_media_video<?php echo $newtest_count;?>" value="<?php echo $get_channel_media['videos']; ?>"/>
													<a href="javascript:void(0);"><img title="Play file now." src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" onclick="PlayAll('channel_media_video<?php echo $newtest_count;?>','channel_media_song<?php echo $newtest_count;?>')"/></a>
													<a href="javascript:void(0);"><img title="Add file to the bottom of your playlist without stopping current media." src="images/profile/add.gif"  onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'" onclick="showPlayer('v','<?php if($combine_arr[$test_count][$newtest_count]['id']!=""){echo $combine_arr[$test_count][$newtest_count]['id'];}else{ echo $combine_arr[$test_count][$newtest_count]['id'];}?>','addvi');"/></a>
												<?php
												}
												else{
												?>
												<a href="javascript:void(0);"><img title="Play file now." src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" /></a>
												<a href="javascript:void(0);"><img title="Add file to the bottom of your playlist without stopping current media." src="images/profile/add.gif"  onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'"/></a>
												<?php
												}
												if(($combine_arr[$test_count][$newtest_count]['sharing_preference']==5 || $combine_arr[$test_count][$newtest_count]['sharing_preference']==2) && $combine_arr[$test_count][$newtest_count]['media_type'] == 114){
													?>
													<a onclick="download_directly_song('<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['title']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['creator']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['from']);?>')" href="javascript:void(0);">
													<img title="Add media to your cart for free download or purchase." onmouseout="this.src='../images/profile/download_down.gif'" onmouseover="this.src='../images/profile/download-over_down.gif'" src="../images/profile/download_down.gif"></a>
												<?php
												}/* else{
												?>
												<img src="images/profile/download-over_down.gif" />
												<?php
												} */
												?>
											</div>
											</div>
											<div class="icon" onclick="runAccordion(<?php echo $combine_arr[$test_count][$newtest_count]['id']; ?>);">
											<div onselectstart="return false;"><img title="View details for media." src="images/profile/view.jpg" /></div>
										</div>
										<div class="icon">&nbsp;<!--<a href="add_media.php?edit=<?php// echo $combine_arr[$test_count][$newtest_count]['id'];?>"><img title="Edit media." src="images/profile/edit.png" /></a>--></div>
										<div class="icon"><a onclick="return confirmdelete('<?php echo $combine_arr[$test_count][$newtest_count]['title'];?>','<?php if($combine_arr[$test_count][$newtest_count]['name']=="Galleries/Images"){echo "Images";}else{echo $combine_arr[$test_count][$newtest_count]['name'];} ?>')" href="delete_media.php?id=<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>&whos_media=media_coming_fan"><img title="Remove media from your library." src="images/profile/delete.png" /></a></div>
											<div class="expandable" id="Accordion<?php echo $combine_arr[$test_count][$newtest_count]['id']; ?>Content">
												<div class="affiBlock">
													<div class="RowCont">
														<div class="sideL">Plays :</div>
														<div class="sideR"><a href="" target="_blank"><?php if($combine_arr[$test_count][$newtest_count]['play_count']!=""){ echo $combine_arr[$test_count][$newtest_count]['play_count'];}else{ echo "0";}?></a></div>
													</div>
													<?php
														if($combine_arr[$test_count][$newtest_count]['sharing_preference']==5 || $combine_arr[$test_count][$newtest_count]['sharing_preference']==2){
														?>
														<div class="RowCont">
															<div class="sideL">Downloads :</div>
															<div class="sideR"><a href="javascript:void(0);"><?php echo $combine_arr[$test_count][$newtest_count]['download_count'];?></a></div>
														</div>
														<?php
														}
														if($combine_arr[$test_count][$newtest_count]['media_type']=='113')
														{
																											
														}
														else
														{
													?>	
													<!--<div class="RowCont">
														<div class="sideL">Media URL :</div>
														<div class="sideR"><a href="" target="_blank">www.<?php echo $domainname; ?></a></div>
													</div>-->
													<?php
														}
													?>
												</div>
											</div>
										</div>
							<?php
										//}
									}
								}
								else if($combine_arr[$test_count][$newtest_count]['media_from']=="media_coming_buyed")
								{
									$chk_tag_user = $new_profile_media_classobj->get_loggedin_info($_SESSION['login_email']);
									if($chk_tag_user['general_user_id']!=$combine_arr[$test_count][$newtest_count]['general_user_id'])
									{
							?>
										<div class="tableCont" id="AccordionContainer">
											<div class="blkD" style="width:60px;"><?php if($combine_arr[$test_count][$newtest_count]['name']!=""){if($combine_arr[$test_count][$newtest_count]['name'] =="Galleries/Images"){echo "Images";}else{echo $combine_arr[$test_count][$newtest_count]['name'];}}else{?>&nbsp;<?php } ?></div>
											<div class="blkI" style="width:127px;"><?php
											$exp_creator_info = explode("|",$combine_arr[$test_count][$newtest_count]['creator_info']);
											$pro_url = $new_profile_media_classobj->get_creator_url($exp_creator_info[0],$exp_creator_info[1]);
											?>
											<a href="<?php if($pro_url['profile_url'] !=""){ echo "/".$pro_url['profile_url']; }else { echo "javascript:void(0)"; }?>">
											<?php if($combine_arr[$test_count][$newtest_count]['creator']!=""){
											//$endpos = strpos($combine_arr[$test_count][$newtest_count]['creator'],"(");
											//if($endpos !="" || $endpos !=false) {
											//echo substr($combine_arr[$test_count][$newtest_count]['creator'],0,$endpos);} else {
											echo $combine_arr[$test_count][$newtest_count]['creator']; }
											else{?>&nbsp;<?php } ?></a></div>
											<div class="blkK"><?php
											$exp_from_info = explode("|",$combine_arr[$test_count][$newtest_count]['from_info']);
											$pro_url_from = $new_profile_media_classobj->get_from_url($exp_from_info[0],$exp_from_info[1]);
											?>
											<a href="<?php if($pro_url_from['profile_url'] !=""){ echo "/".$pro_url_from['profile_url']; }else { echo "javascript:void(0)"; }?>">
											<?php if($combine_arr[$test_count][$newtest_count]['from']!=""){
											//$endpos = strpos($combine_arr[$test_count][$newtest_count]['from'],"(");
											//if($endpos !="" || $endpos != false) {
											//echo substr($combine_arr[$test_count][$newtest_count]['from'],0,$endpos);} else {
											echo $combine_arr[$test_count][$newtest_count]['from']; }
											else{?>&nbsp;<?php } ?></a></div>
											<div class="blkB"><?php
												if($combine_arr[$test_count][$newtest_count]['title']!="")
												{
													if($combine_arr[$test_count][$newtest_count]['media_type'] == 114)
													{
														//$get_media_track = $new_profile_media_classobj->get_media_track($combine_arr[$test_count][$newtest_count]['id']); 
														if($combine_arr[$test_count][$newtest_count]['track'] != "" && $combine_arr[$test_count][$newtest_count]['track'] != NULL && $combine_arr[$test_count][$newtest_count]['track'] !=" "){
															echo $combine_arr[$test_count][$newtest_count]['track'].".".stripslashes($combine_arr[$test_count][$newtest_count]['title']);
														}else{
															echo stripslashes($combine_arr[$test_count][$newtest_count]['title']);
														}
													}
													else{
													echo stripslashes($combine_arr[$test_count][$newtest_count]['title']); }
												}else{
												?>&nbsp;<?php
												}
													?></div>
											<div class="blkplayer"><div class="player">
											<?php 
												if($combine_arr[$test_count][$newtest_count]['media_type']==113){
													$get_images_fan = $new_profile_media_classobj ->get_all_gallery_images($combine_arr[$test_count][$newtest_count]['id']);
													for($count_img=0;$count_img<=count($get_images_fan);$count_img++){
														if($get_all_img_fan ==""){
															$get_all_img_fan = $get_images_fan[$count_img]['image_name'];
														}else{
															$get_all_img_fan = $get_all_img_fan.",".$get_images_fan[$count_img]['image_name'];
														}
														if($galleryImagestitle_fan ==""){
															$galleryImagestitle_fan = $get_images_fan[$count_img]['image_title'];
														}else{
															$galleryImagestitle_fan = $galleryImagestitle_fan.",".$get_images_fan[$count_img]['image_title'];
														}
														$mediaSrc_fan = $get_images_fan[0]['image_name'];
													}
													$gal_id_fan = $get_images_fan[$count_img]['media_id'];
													$get_other_user_id = $user_keys_data->get_general_user_idby_mediaid($combine_arr[$test_count][$newtest_count]['id']);
													$orgPath_fan = $s3->getNewURI_otheruser("medgallery",$get_other_user_id);//"https://medgallery.s3.amazonaws.com/";
													$thumbPath_fan = $s3->getNewURI_otheruser("medgalthumb",$get_other_user_id);//"https://medgalthumb.s3.amazonaws.com/";
													
												?>
												<a href="javascript:void(0);"><img title="Play file now." src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" onclick="checkload('<?php echo $mediaSrc_fan;?>','<?php echo $get_all_img_fan; ?>','<?php echo $thumbPath_fan; ?>','<?php echo $orgPath_fan; ?>','<?php echo $galleryImagestitle_fan; ?>','<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>')"/></a>
												<!--<a href="javascript:void(0);"><img title="Add file to the bottom of your playlist without stopping current media." src="images/profile/add.gif"  onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'"/></a>-->
												<?php
													$get_all_img_fan ="";
													$galleryImagestitle_fan="";
													$mediaSrc_fan ="";
												}else if($combine_arr[$test_count][$newtest_count]['media_type']==114){
												?>
												<a href="javascript:void(0);"><img title="Play file now." src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" onclick="showPlayer('a','<?php if($combine_arr[$test_count][$newtest_count]['id']!=""){echo $combine_arr[$test_count][$newtest_count]['id'];}else{echo $combine_arr[$test_count][$newtest_count]['id'];}?>','play');"/></a>
												<a href="javascript:void(0);"><img title="Add file to the bottom of your playlist without stopping current media." src="images/profile/add.gif"  onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'" onclick="showPlayer('a','<?php if($combine_arr[$test_count][$newtest_count]['id']!=""){echo $combine_arr[$test_count][$newtest_count]['id'];}else{echo $combine_arr[$test_count][$newtest_count]['id'];}?>','add');"/></a>
												<?php
												}else if($combine_arr[$test_count][$newtest_count]['media_type']==115){
												?>
												<a href="javascript:void(0);"><img title="Play file now." src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" onclick="showPlayer('v','<?php if($combine_arr[$test_count][$newtest_count]['id']!=""){echo $combine_arr[$test_count][$newtest_count]['id'];}else{ echo $combine_arr[$test_count][$newtest_count]['id'];}?>','playlist');"/></a>
												<a href="javascript:void(0);"><img title="Add file to the bottom of your playlist without stopping current media." src="images/profile/add.gif"  onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'" onclick="showPlayer('v','<?php if($combine_arr[$test_count][$newtest_count]['id']!=""){echo $combine_arr[$test_count][$newtest_count]['id'];}else{ echo $combine_arr[$test_count][$newtest_count]['id'];}?>','addvi');"/></a>
												<?php
												}else if($combine_arr[$test_count][$newtest_count]['media_type'] == 116){
													$get_channel_media = $new_profile_media_classobj ->get_all_media_selected_channel($combine_arr[$test_count][$newtest_count]['id']);
												?>
													<input type="hidden" id="channel_media_song<?php echo $newtest_count;?>" value="<?php echo $get_channel_media['songs']; ?>"/>
													<input type="hidden" id="channel_media_video<?php echo $newtest_count;?>" value="<?php echo $get_channel_media['videos']; ?>"/>
													<a href="javascript:void(0);"><img title="Play file now." src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" onclick="PlayAll('channel_media_video<?php echo $newtest_count;?>','channel_media_song<?php echo $newtest_count;?>')"/></a>
													<a href="javascript:void(0);"><img title="Add file to the bottom of your playlist without stopping current media." src="images/profile/add.gif"  onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'" onclick="showPlayer('v','<?php if($combine_arr[$test_count][$newtest_count]['id']!=""){echo $combine_arr[$test_count][$newtest_count]['id'];}else{ echo $combine_arr[$test_count][$newtest_count]['id'];}?>','addvi');"/></a>
												<?php
												}
												else{
												?>
												<a href="javascript:void(0);"><img title="Play file now." src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" /></a>
												<a href="javascript:void(0);"><img title="Add file to the bottom of your playlist without stopping current media." src="images/profile/add.gif"  onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'"/></a>
												<?php
												}
												
												if(($combine_arr[$test_count][$newtest_count]['sharing_preference']==5 || $combine_arr[$test_count][$newtest_count]['sharing_preference']==2) && $combine_arr[$test_count][$newtest_count]['media_type'] == 114){
													?>
													<a onclick="download_directly_song('<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['title']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['creator']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['from']);?>')" href="javascript:void(0);">
													<img title="Add media to your cart for free download or purchase." onmouseout="this.src='../images/profile/download_down.gif'" onmouseover="this.src='../images/profile/download-over_down.gif'" src="../images/profile/download_down.gif"></a>
												<?php
												}/* else{
												?>
												<img src="images/profile/download-over_down.gif" />
												<?php
												} */
												?>
											</div>
											</div>
										<div class="icon" onclick="runAccordion(<?php echo $combine_arr[$test_count][$newtest_count]['id']; ?>);">
											<div onselectstart="return false;"><img title="View details for media." src="images/profile/view.jpg" /></div>
										</div>
										<div class="icon">&nbsp;<!--<a href="add_media.php?edit=<?php// echo $combine_arr[$test_count][$newtest_count]['id'];?>"><img title="Edit media." src="images/profile/edit.png" /></a>--></div>
										<div class="icon"><a onclick="return confirmdelete('<?php echo $combine_arr[$test_count][$newtest_count]['title'];?>','<?php if($combine_arr[$test_count][$newtest_count]['name']=="Galleries/Images"){echo "Images";}else{echo $combine_arr[$test_count][$newtest_count]['name'];} ?>')" href="delete_media.php?id=<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>&whos_media=media_coming_buyed"><img title="Remove media from your library." src="images/profile/delete.png" /></a></div>
											<div class="expandable" id="Accordion<?php echo $combine_arr[$test_count][$newtest_count]['id']; ?>Content">
												<div class="affiBlock">
													<div class="RowCont">
														<div class="sideL">Plays :</div>
														<div class="sideR"><a href="" target="_blank"><?php if($combine_arr[$test_count][$newtest_count]['play_count']!=""){ echo $combine_arr[$test_count][$newtest_count]['play_count'];}else{ echo "0";}?></a></div>
													</div>
													<?php
														if($combine_arr[$test_count][$newtest_count]['sharing_preference']==5 || $combine_arr[$test_count][$newtest_count]['sharing_preference']==2){
														?>
														<div class="RowCont">
															<div class="sideL">Downloads :</div>
															<div class="sideR"><a href="javascript:void(0);"><?php echo $combine_arr[$test_count][$newtest_count]['download_count'];?></a></div>
														</div>
														<?php
														}
														if($combine_arr[$test_count][$newtest_count]['media_type']=='113')
														{
																											
														}
														else
														{
													?>	
													<!--<div class="RowCont">
														<div class="sideL">Media URL :</div>
														<div class="sideR"><a href="" target="_blank">www.<?php echo $domainname; ?></a></div>
													</div>-->
													<?php
														}
													?>
												</div>
											</div>
										</div>
							<?php
									}
								}
								else if($combine_arr[$test_count][$newtest_count]['media_from']=="all_tagging_media")
								{
									//$get_media_tag_g_art = $new_profile_media_classobj->get_tag_media_info($combine_arr[$test_count][$newtest_count]);
									$chk_tag_user = $new_profile_media_classobj->get_loggedin_info($_SESSION['login_email']);
									if($chk_tag_user['general_user_id']!=$combine_arr[$test_count][$newtest_count]['general_user_id'])
									{
										if($combine_arr[$test_count][$newtest_count]['id']!=""){
											?>
											<div class="tableCont" id="AccordionContainer">
												<div class="blkD" style="width:60px;"><?php if($combine_arr[$test_count][$newtest_count]['name']!=""){if($combine_arr[$test_count][$newtest_count]['name'] =="Galleries/Images"){echo "Images";}else{echo $combine_arr[$test_count][$newtest_count]['name'];}}else{?>&nbsp;<?php }?></div>
												<div class="blkI" style="width:127px;"><?php
												$exp_creator_info = explode("|",$combine_arr[$test_count][$newtest_count]['creator_info']);
												$pro_url = $new_profile_media_classobj->get_creator_url($exp_creator_info[0],$exp_creator_info[1]);
												?>
												<a href="<?php if($pro_url['profile_url'] !=""){ echo "/".$pro_url['profile_url']; }else { echo "javascript:void(0)"; }?>">
												<?php if($combine_arr[$test_count][$newtest_count]['creator']!=""){
												//$endpos = strpos($combine_arr[$test_count][$newtest_count]['creator'],"(");
												//if($endpos !="" || $endpos !=false) {
												//echo substr($combine_arr[$test_count][$newtest_count]['creator'],0,$endpos);} else {
												echo $combine_arr[$test_count][$newtest_count]['creator']; }
												else{?>&nbsp;<?php } ?></a></div>
												<div class="blkK"><?php
												$exp_from_info = explode("|",$combine_arr[$test_count][$newtest_count]['from_info']);
												$pro_url_from = $new_profile_media_classobj->get_from_url($exp_from_info[0],$exp_from_info[1]);
												?>
												<a href="<?php if($pro_url_from['profile_url'] !=""){ echo "/".$pro_url_from['profile_url']; }else { echo "javascript:void(0)"; }?>">
												<?php if($combine_arr[$test_count][$newtest_count]['from']!=""){
												//$endpos = strpos($combine_arr[$test_count][$newtest_count]['from'],"(");
												//if($endpos !="" || $endpos != false) {
												//echo substr($combine_arr[$test_count][$newtest_count]['from'],0,$endpos);} else {
												echo $combine_arr[$test_count][$newtest_count]['from']; }
												else{?>&nbsp;<?php } ?></a></div>
												<div class="blkB"><?php
												if($combine_arr[$test_count][$newtest_count]['title']!="")
												{
													if($combine_arr[$test_count][$newtest_count]['media_type'] == 114)
													{
														if($combine_arr[$test_count][$newtest_count]['track'] == ""){
														echo stripslashes($combine_arr[$test_count][$newtest_count]['title']);}else{
														echo $combine_arr[$test_count][$newtest_count]['track'].".".stripslashes($combine_arr[$test_count][$newtest_count]['title']);
														}
													}
													else{
													echo stripslashes($combine_arr[$test_count][$newtest_count]['title']); }
												}else{
												?>&nbsp;<?php
												}
													?>
												</div>
												<?php
												if($combine_arr[$test_count][$newtest_count]['media_type']==114){
												?>
												<div class="blkplayer"><div class="player">
												<a href="javascript:void(0);"><img title="Play file now." src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" onclick="showPlayer('a','<?php if($combine_arr[$test_count][$newtest_count]['id']!=""){echo $combine_arr[$test_count][$newtest_count]['id'];}else{echo $combine_arr[$test_count][$newtest_count]['id'];}?>','play');"/></a>
												<a href="javascript:void(0);"><img title="Add file to the bottom of your playlist without stopping current media." src="images/profile/add.gif"  onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'" onclick="showPlayer('a','<?php if($combine_arr[$test_count][$newtest_count]['id']!=""){echo $combine_arr[$test_count][$newtest_count]['id'];}else{echo $combine_arr[$test_count][$newtest_count]['id'];}?>','add');"/></a>
												<?php
												if(($combine_arr[$test_count][$newtest_count]['sharing_preference']==5 || $combine_arr[$test_count][$newtest_count]['sharing_preference']==2) && $combine_arr[$test_count][$newtest_count]['media_type'] == 114){
													?>
													<a onclick="download_directly_song('<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['title']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['creator']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['from']);?>')" href="javascript:void(0);">
													<img title="Add media to your cart for free download or purchase." onmouseout="this.src='../images/profile/download_down.gif'" onmouseover="this.src='../images/profile/download-over_down.gif'" src="../images/profile/download_down.gif"></a>
												<?php
												}/* else{
												?>
												<img src="images/profile/download-over_down.gif" />
												<?php
												} */
												?>
												</div>
												</div>
												<?php
												}else if($combine_arr[$test_count][$newtest_count]['media_type']==115){
												?>
												<div class="blkplayer"><div class="player">
												<a href="javascript:void(0);"><img title="Play file now." src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" onclick="showPlayer('v','<?php if($combine_arr[$test_count][$newtest_count]['id']!=""){echo $combine_arr[$test_count][$newtest_count]['id'];}else{echo $combine_arr[$test_count][$newtest_count]['id'];}?>','playlist');"/></a>
												<!--<a href="javascript:void(0);"><img title="Add file to the bottom of your playlist without stopping current media." src="images/profile/add.gif"  onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'" onclick="showPlayer('v','<?php if($combine_arr[$test_count][$newtest_count]['id']!=""){echo $combine_arr[$test_count][$newtest_count]['id'];}else{echo $combine_arr[$test_count][$newtest_count]['id'];}?>','addvi');"/></a>-->
												<?php
												if(($combine_arr[$test_count][$newtest_count]['sharing_preference']==5 || $combine_arr[$test_count][$newtest_count]['sharing_preference']==2) && $combine_arr[$test_count][$newtest_count]['media_type'] == 114){
													?>
													<a onclick="download_directly_song('<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['title']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['creator']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['from']);?>')" href="javascript:void(0);">
													<img title="Add media to your cart for free download or purchase." onmouseout="this.src='../images/profile/download_down.gif'" onmouseover="this.src='../images/profile/download-over_down.gif'" src="../images/profile/download_down.gif"></a>
												<?php
												}/* else{
												?>
												<img src="images/profile/download-over_down.gif" />
												<?php
												} */
												?>
												</div>
												</div>
												<?php
												}else if($combine_arr[$test_count][$newtest_count]['media_type']==113){
												?>
												<div class="blkplayer"><div class="player">
												<?php 
													$get_images_tag_pro_cre = $new_profile_media_classobj ->get_all_gallery_images($combine_arr[$test_count][$newtest_count]['id']);
													for($count_img_tag_pro_cre=0;$count_img_tag_pro_cre<=count($get_images_tag_pro_cre);$count_img_tag_pro_cre++){
														if($get_all_img_tag_pro_cre ==""){
															$get_all_img_tag_pro_cre = $get_images_tag_pro_cre[$count_img_tag_pro_cre]['image_name'];
														}else{
															$get_all_img_tag_pro_cre = $get_all_img_tag_pro_cre.",".$get_images_tag_pro_cre[$count_img_tag_pro_cre]['image_name'];
														}
														if($galleryImagestitle_tag_pro_cre ==""){
															$galleryImagestitle_tag_pro_cre= $get_images_tag_pro_cre[$count_img_tag_pro_cre]['image_title'];
														}else{
															$galleryImagestitle_tag_pro_cre= $galleryImagestitle_tag_pro_cre.",".$get_images_tag_pro_cre[$count_img_tag_pro_cre]['image_title'];
														}
														$mediaSrc_tag_pro_cre = $get_images_tag_pro_cre[0]['image_name'];
													}
													$gal_id_tag_pro_cre = $get_images_tag_pro_cre[$count_img_tag_pro_cre]['media_id'];
													$get_other_user_id = $user_keys_data->get_general_user_idby_mediaid($combine_arr[$test_count][$newtest_count]['id']);
													$orgPath_tag_pro_cre = $s3->getNewURI_otheruser("medgallery",$get_other_user_id);//"https://medgallery.s3.amazonaws.com/";
													$thumbPath_tag_pro_cre = $s3->getNewURI_otheruser("medgalthumb",$get_other_user_id);//"https://medgalthumb.s3.amazonaws.com/";
												
												?>
												<a href="javascript:void(0);"><img src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" onclick="checkload('<?php echo $mediaSrc_tag_pro_cre;?>','<?php echo $get_all_img_tag_pro_cre; ?>','<?php echo $thumbPath_tag_pro_cre; ?>','<?php echo $orgPath_tag_pro_cre; ?>','<?php echo $galleryImagestitle_tag_pro_cre; ?>','<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>')"/></a>
												<!--<a href="javascript:void(0);"><img src="images/profile/add.gif"  onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'"/></a>-->
												<?php
													$get_all_img_tag_pro_cre ="";
													$galleryImagestitle_tag_pro_cre="";
													$mediaSrc_tag_pro_cre ="";
												?>
												<?php
												if(($combine_arr[$test_count][$newtest_count]['sharing_preference']==5 || $combine_arr[$test_count][$newtest_count]['sharing_preference']==2) && $combine_arr[$test_count][$newtest_count]['media_type'] == 114){
													?>
													<a onclick="download_directly_song('<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['title']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['creator']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['from']);?>')" href="javascript:void(0);">
													<img title="Add media to your cart for free download or purchase." onmouseout="this.src='../images/profile/download_down.gif'" onmouseover="this.src='../images/profile/download-over_down.gif'" src="../images/profile/download_down.gif"></a>
												<?php
												}/* else{
												?>
												<img src="images/profile/download-over_down.gif" />
												<?php
												} */
												?>
												</div>
												</div>
											<?php
												}else if($combine_arr[$test_count][$newtest_count]['media_type'] == 116){
													$get_channel_media = $new_profile_media_classobj ->get_all_media_selected_channel($combine_arr[$test_count][$newtest_count]['id']);
												?>
													<div class="blkplayer"><div class="player">
														<input type="hidden" id="channel_media_song<?php echo $newtest_count;?>" value="<?php echo $get_channel_media['songs']; ?>"/>
														<input type="hidden" id="channel_media_video<?php echo $newtest_count;?>" value="<?php echo $get_channel_media['videos']; ?>"/>
														<a href="javascript:void(0);"><img title="Play file now." src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" onclick="PlayAll('channel_media_video<?php echo $newtest_count;?>','channel_media_song<?php echo $newtest_count;?>')"/></a>
														<a href="javascript:void(0);"><img title="Add file to the bottom of your playlist without stopping current media." src="images/profile/add.gif"  onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'" onclick="showPlayer('v','<?php if($combine_arr[$test_count][$newtest_count]['id']!=""){echo $combine_arr[$test_count][$newtest_count]['id'];}else{ echo $combine_arr[$test_count][$newtest_count]['id'];}?>','addvi');"/></a>
														<?php
														if(($combine_arr[$test_count][$newtest_count]['sharing_preference']==5 || $combine_arr[$test_count][$newtest_count]['sharing_preference']==2) && $combine_arr[$test_count][$newtest_count]['media_type'] == 114){
															?>
															<a onclick="download_directly_song('<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['title']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['creator']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['from']);?>')" href="javascript:void(0);">
															<img title="Add media to your cart for free download or purchase." onmouseout="this.src='../images/profile/download_down.gif'" onmouseover="this.src='../images/profile/download-over_down.gif'" src="../images/profile/download_down.gif"></a>
														<?php
														}/* else{
														?>
														<img src="images/profile/download-over_down.gif" />
														<?php
														} */
														?>
													</div>
													</div>
												<?php
												}
												else
												{
												?>
												<div class="blkplayer"><div class="player">
													<a href="javascript:void(0);"><img src="images/profile/play.gif" onmouseover="this.src='images/profile/play-over.gif'" onmouseout="this.src='images/profile/play.gif'" /></a>
													<a href="javascript:void(0);"><img src="images/profile/add.gif"  onmouseover="this.src='images/profile/add-over.gif'" onmouseout="this.src='images/profile/add.gif'" /></a>
													<?php
													if(($combine_arr[$test_count][$newtest_count]['sharing_preference']==5 || $combine_arr[$test_count][$newtest_count]['sharing_preference']==2) && $combine_arr[$test_count][$newtest_count]['media_type'] == 114){
														?>
														<a onclick="download_directly_song('<?php echo $combine_arr[$test_count][$newtest_count]['id'];?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['title']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['creator']);?>','<?php echo str_replace("'","\'",$combine_arr[$test_count][$newtest_count]['from']);?>')" href="javascript:void(0);">
														<img title="Add media to your cart for free download or purchase." onmouseout="this.src='../images/profile/download_down.gif'" onmouseover="this.src='../images/profile/download-over_down.gif'" src="../images/profile/download_down.gif"></a>
													<?php
													}/* else{
													?>
													<img src="images/profile/download-over_down.gif" />
													<?php
													} */
													?>
												</div>
												</div>
												<?php
												}
											?>
											</div>
											<?php
										}
									}
								}
								
							//}
							}
							?>
							<script type="text/javascript">             
								$(document).ready(function() {
									$("#gallery_images<?php echo $newtest_count; ?>").fancybox({
				helpers : {
					media : {},
					buttons : {},
					title : null
				}
			});

								
							});
							</script>

							<script type="text/javascript">             
							$(document).ready(function() {
										$("#Add_gallery_images<?php echo $newtest_count; ?>").fancybox({
										helpers : {
											media : {},
											buttons : {},
											title : null
										}
									});
								});
								</script>
							<?php
							}
								?>
								<!--Code for displaying creator Project Ends Here-->
						</div>
					<?php
					}
					?>
						
						
					</div>
				</div>
                
                
                
                
                <div class="subTabs" id="storage" style="display:none;"><h1>Storage</h1><br /><p><strong>Add File: </strong><em>If you are uploading multiple files, compress them into a single file using zip or rar compression.</em></p>
      <p><strong>Add User who may access file: </strong><em>List registered user </em></p>
      <p><strong> List of Users who may access file: </strong>Kaz, Granny Beats </p>
      <p>UPLOAD / DELETE </p>
      <p><strong>Total Library Space: </strong>5,000 MB </p>
      <p><strong>Library Space Left: </strong>3,054 MB </p>
      <p><strong>Stored Files</strong></p>
      <p><a href="#">02.12.09, 12:43, 243 MB, LeeRick_Untitiled_01.zip</a> (edit/download) <br />
        <a href="#">02.14.09, 12:45, 192 MB, LeeRick_Untitiled_02.zip</a> (edit/download) <br />
        <a href="#">02.19.09, 12:47, 262 MB, LeeRick_Untitiled_03.zip</a> (edit/download) <br />
        <a href="#">02.22.09, 12:49, 302 MB, LeeRick_Untitiled_04.zip</a> (edit/download) <br />
        <a href="#">03.02.09, 12:52, 212 MB, LeeRick_Untitiled_05.zip</a> (edit/download) </p></div>
               
                
                <div class="subTabs" id="s3" style="display:none;">
                	<h1>S3 Library</h1>
                    <div id="mediaContent" style="width:685px; height:400px; overflow:auto;">
                    	<div class="topLinks">
                            <div class="links" style="float:left;">
                                <ul>
                                    <li><a href="add_user_s3.php" id="add_new_s3" ><input type="button" value="Add S3" style="background-color: #000000;border: 0 none;color: #FFFFFF;float: left;font-family: futura-pt,sans-serif;font-size: 0.825em;font-weight: bold;margin-right: 5px;padding: 8px 15px;text-decoration: none;" /></a></li>
                                </ul>
                            </div>
                        </div>
                    	<div class="titleCont">
                            <div class="blkA">Title</div>
                            <div class="blkB">Edit</div>
                            <div class="blkB">Delete</div>
                        </div>
                        <div class="tableCont">
						<?php
							$get_all_user = $new_profile_media_classobj->get_all_user_s3();
							if($get_all_user!="" && $get_all_user!=NULL)
							{
								if(mysql_num_rows($get_all_user)>0)
								{
									while($row = mysql_fetch_assoc($get_all_user))
									{
								?>
									<div class="blkA"><?php echo $row['user_name'];?></div>
									<div class="blkB"><a href="">Edit</a></div>
									<div class="blkB"><a href="">Delete</a></div>
								<?php
									}
								}
							}
						?>
                            
                        </div>
                    </div>
                </div>
                
               </div>
         
		 </div>  
          
           
          
          <p></p>
   </div>
</div>
<?php
$sql_general = mysql_query("SELECT * FROM general_user WHERE email='".$_SESSION['login_email']."'");
$ans_sql = mysql_fetch_assoc($sql_general);

?>
<a href="asktodonate.php" class="fancybox fancybox.ajax" id="get_donationpop" style="display:none">fs</a>
<div id="light-gal" style="z-index:99999;" class="gallery-wrapper">
	<div id="bg">
		<a href="#" class="nextImageBtn" title="next"></a>
		<a href="#" class="prevImageBtn" title="previous"></a>
		<img width="1680" src="" height="1050" alt="" title="" id="bgimg" />
	</div>
	<div id="preloader"><img src=".././galleryfiles/ajax-loader_dark.gif" width="32" height="32" /></div>
	<div id="img_title"></div>
	<div id="toolbar">
		<a href="#" title="Pause" id="pauseImg" onClick="setimage('pause')"><img src=".././galleryfiles/pause.png" width="50" height="50"  /></a>
		<a href="#" title="Maximize" id="maximizeImg" onClick="ImageViewMode('full');return false"><img src=".././galleryfiles/toolbar_fs_icon.png" width="50" height="50"  /></a>
		<script type="text/javascript">
		<!--[if IE]>
		$("#maximizeImg").css({"display":"none"});
		<![endif]-->
		</script>
		<a href="#" title="Close" id="closeGal" onClick="CloseGallery();return false"><img src=".././galleryfiles/close.png" width="50" height="50"  /></a>
	</div>
	<div id="thumbnails_wrapper">
	<div id="outer_container">
	<div class="thumbScroller">
		<div class="container" id="gallery-container">
			
		</div>
	</div>
	</div>
	</div>
</div>
  <!-- end of main container -->
</div><?php include_once("displayfooter.php"); ?>
<style>
	#miniFooter { margin : 15px auto 0; }
</style>

<input type="hidden" id="curr_playing_playlist" name="curr_playing_playlist"/>
</body>
<link href="../galleryfiles/gallery.css" rel="stylesheet"/>
<script src="../galleryfiles/jquery.easing.1.3.js"></script>
<script src="../includes/functionsmedia.js" type="text/javascript"></script>
<script src="../includes/functionsURL.js" type="text/javascript"></script>
<script src="../includes/mediaelement-and-player.min.js"></script>
<link rel="stylesheet" href="../includes/mediaelementplayer.min.css" />

<link rel="stylesheet" href="../lightbox/prettyPhoto.css" type="text/css" media="screen" title="prettyPhoto main stylesheet" charset="utf-8" />
<script src="../lightbox/jquery.prettyPhoto.js" type="text/javascript" charset="utf-8"></script>
<script src="../javascripts/popup.js" type="text/javascript" charset="utf-8"></script>
<script type="text/javascript" src="/galleryfiles/jqgal.js"></script>
<script type="text/javascript">
$(document).ready(function() {
	$("#add_new_s3").fancybox({
	'width'				: '75%',
	'height'			: '75%',
	'transitionIn'		: 'none',
	'transitionOut'		: 'none',
	'type'				: 'iframe'
	});
});
</script>

<script type="text/javascript">
/*Code For Playing Channel All media*/
function PlayAll(videoid,songid)
{
	var get_song_id = document.getElementById(songid).value;
	var get_video_id = document.getElementById(videoid).value;
	var ex_song_id =get_song_id.split(",");
	var ex_video_id =get_video_id.split(",");
	var type;
	var counter_song =0;
	var counter_video =0;
	function next()
	{
		if (counter_video < ex_video_id.length)
		{
			if($("#curr_playing_playlist").val()!="")
			{
				type = "addvi";
			}else{
				if(counter_video ==1)
				{
					type = "playlist";
				}
				else
				{
					type = "addvi";
				}
			}
			what = "v";
			showPlayer_channel(what,ex_video_id[counter_video],type);
			setTimeout(next, 2000);
			counter_video++;
		}
		if(counter_video == ex_video_id.length)
		{
			if(counter_song < ex_song_id.length)
			{
				if($("#curr_playing_playlist").val()!="")
				{
					type = "add";
				}else{
					if(counter_song ==1)
					{
						type = "play";
					}
					else
					{
						type = "add";
					}
				}
				what = "a";
				showPlayer_channel(what,ex_song_id[counter_song],type);
				setTimeout(next, 2000);
				counter_song++;
			}
		}
	}
	next();
}
/*Code For Playing Channel All media Ends Here*/


function checkload(defImg,Images,orgpath,thumbpath,title,media_id){
	var arr_sep = Images.split(',');
	var title_sep = null;
	title_sep = title.split(',');
	//alert(title_sep);
	var k;
	var data = new Array();
	for(k=0;k<arr_sep.length;k++)
	{
		if(arr_sep[k]!="" && arr_sep[k]!=" "){
			//arr_sep[k] = arr_sep[k].replace("\"","");
			arr_sep[k] = arr_sep[k].replace(/"/g, '');
			arr_sep[k] = arr_sep[k].replace("[","");
			arr_sep[k] = arr_sep[k].replace("]","");
			data[k] = arr_sep[k];
		}
	}
	
	var q;
	var data1 = new Array();
	for(q=0;q<title_sep.length;q++)
	{
		//arr_sep[k] = arr_sep[k].replace("\"","");
		title_sep[q] = title_sep[q].replace(/"/g, '');
		title_sep[q] = title_sep[q].replace("[","");
		title_sep[q] = title_sep[q].replace("]","");
		data1[q] = title_sep[q];
	}
	if(data.length>0){
		$("#gallery-container").html('');
	}
	//$("#bgimg").attr('src','<?php echo $orgPath;?>'+defImg);
	//alert(orgpath+defImg);
	$("#bgimg").attr('src',thumbpath+defImg);
	

	for(var i=0;i<data.length;i++){
		$("#gallery-container").append("<div class='content'><div><a href='"+thumbpath+data[i]+"'><img  src='"+orgpath+data[i]+"' title='"+data1[i]+"' alt='"+data1[i]+"' class='thumb' /></a></div></div>");
	}
	//setTimeout(function(){	loadGallery();	$("#light-gal").css('visibility','visible');	},500);
	loadGallery();
	$("#light-gal").css('visibility','visible');
	$(".accountSettings").css('z-index','1');
	//$("#outerContainer").css({"display":"none"});
	document.getElementById("gallery-container").style.left = "0px";
	$.ajax({
			type: "POST",
			url: 'update_media.php',
			data: { "media_id":media_id },
			success: function(data){
			}
	});
}




/////// Related Player	
//var win_obj = $("#curr_playing_playlist").val();
//if(win_obj==""){
var windowRef=false;
//}
var profileName = "Mithesh";
function showPlayer(tp,ref,act){
	var pl = $("#curr_playing_playlist").val();
	$.ajax({
		type: 'POST',
		url: '/player/getMediaInfo.php',
		data: { "tp":tp, "ref":ref, "source":"<?php echo $ans_sql['general_user_id']; ?>", "act":act, "pl":pl, "creator":profileName, "update":"update" },
		success: function(data){
			get_cookie(data);
			var is_chrome = navigator.userAgent.toLowerCase().indexOf('chrome') > -1;
			if(is_chrome ==true){
				WinHeight = 579;
			}else{
				WinHeight = 580;
			}
			windowRef = window.open('', 'formpopup', 'height='+WinHeight+',width=587,resizable=no,scrollbars');
			if(windowRef == null || typeof(windowRef) == "undefined") {
			   alert("The media player is a pop up player to give you uninterrupted streaming while browsing the site. To view it you must allow pop ups from Purify Art in the settings of your browser.");
			   return false;
			}
			if(windowRef.location == "about:blank" || (act!="add" && act!="addvi")){
				windowRef.location.href='combinePlayer/index.php#tp='+tp+'&ref='+ref+'&pl='+data+'&buyer_id='+'<?php echo $_SESSION['gen_user_id'];?>'+'&act=play';
				//jQuery(window).blur();
				windowRef.focus();
			}
			else{
				windowRef.location.href='combinePlayer/index.php#tp='+tp+'&ref='+ref+'&pl='+data+'&buyer_id='+'<?php echo $_SESSION['gen_user_id'];?>';
				windowRef.focus();
			}
		}, 
		error: function(jqXHR, textStatus, errorThrown) 
          {
              alert(errorThrown);
           }
	});
}
function showPlayer_channel(tp,ref,act){
	var pl = $("#curr_playing_playlist").val();
	$.ajax({
		type: 'POST',
		url: '/player/getMediaInfo.php',
		data: { "tp":tp, "ref":ref, "source":"<?php echo $ans_sql['general_user_id']; ?>", "act":act, "pl":pl, "creator":profileName, "update":"update" },
		success: function(data){
			get_cookie(data);
			var is_chrome = navigator.userAgent.toLowerCase().indexOf('chrome') > -1;
			if(is_chrome ==true){
				WinHeight = 579;
			}else{
				WinHeight = 580;
			}
			windowRef = window.open('', 'formpopup', 'height='+WinHeight+',width=587,resizable=no,scrollbars');
			if(windowRef == null || typeof(windowRef) == "undefined") {
			   alert("The media player is a pop up player to give you uninterrupted streaming while browsing the site. To view it you must allow pop ups from Purify Art in the settings of your browser.");
			   return false;
			}
			if(windowRef.location == "about:blank" || (act!="add" && act!="addvi")){
				windowRef.location.href='combinePlayer/index.php#tp='+tp+'&ref='+ref+'&pl='+data+'&buyer_id='+'<?php echo $_SESSION['gen_user_id'];?>'+'&act=play'+'&chan=channel';
				//jQuery(window).blur();
				windowRef.focus();
			}
			else{
				windowRef.location.href='combinePlayer/index.php#tp='+tp+'&ref='+ref+'&pl='+data+'&buyer_id='+'<?php echo $_SESSION['gen_user_id'];?>'+'&chan=channel';
				//windowRef.focus();
			}
		}, 
		error: function(jqXHR, textStatus, errorThrown) 
          {
              //alert(errorThrown);
           }
	});
}

function showPlayer_reg(tp,ref,act,tbl){
	var pl = $("#curr_playing_playlist").val();
	$.ajax({
		type: 'POST',
		url: 'http://www.<?php echo $domainname; ?>/player/getMediaInfo.php',
		data: { "tp":tp, "ref":ref, "tbl":tbl, "source":"<?php echo $ans_sql['general_user_id']; ?>", "act":act, "pl":pl, "creator":profileName, "update":"update" },
		success: function(data){
		//alert(data);
			//$("#curr_playing_playlist").val(data);
			get_cookie(data);
			var is_chrome = navigator.userAgent.toLowerCase().indexOf('chrome') > -1;
			if(is_chrome ==true){
				WinHeight = 579;
			}else{
				WinHeight = 580;
			}
			windowRef = window.open('', 'formpopup', 'height='+WinHeight+',width=587,resizeable,scrollbars');
			if(windowRef == null || typeof(windowRef) == "undefined") {
			   alert("The media player is a pop up player to give you uninterrupted streaming while browsing the site. To view it you must allow pop ups from Purify Art in the settings of your browser.");
			   return false;
			}
			if(windowRef.location == "about:blank" || (act!="add" && act!="addvi" && act!="add_reg" && act!="addvi_reg")){
				windowRef.location.href='combinePlayer/index.php#tp='+tp+'&ref='+ref+'&pl='+data+'&buyer_id='+'<?php echo $_SESSION['gen_user_id'];?>'+'&act=play';
				//jQuery(window).blur();
				windowRef.focus();
			}
			else{
				windowRef.location.href='combinePlayer/index.php#tp='+tp+'&ref='+ref+'&pl='+data+'&buyer_id='+'<?php echo $_SESSION['gen_user_id'];?>';
				//jQuery(window).blur();
				windowRef.focus();
			}
		}
	});
}

function get_cookie(data)
{
	document.cookie="filename=" + data;
	getCookieValue("filename");
}

function getCookieValue(key)
{
	currentcookie = document.cookie;
	if (currentcookie.length > 0)
	{
		firstidx = currentcookie.indexOf(key + "=");
		if (firstidx != -1)
		{
			firstidx = firstidx + key.length + 1;
			lastidx = currentcookie.indexOf(";",firstidx);
			if (lastidx == -1)
			{
				lastidx = currentcookie.length;
			}
			getdata = unescape(currentcookie.substring(firstidx, lastidx));
			$("#curr_playing_playlist").val(unescape(currentcookie.substring(firstidx, lastidx)));
			//return unescape(currentcookie.substring(firstidx, lastidx));
		}
		
	}
	return "";
}

$("document").ready(function(){
	window.setInterval("getCookieValue('filename')",500);
});

function download_directly_song(id,t,c,f){
	var val = confirm("Are you sure you want to download "+t+" by "+c+" from "+f+"?");
	if(val==true){
	window.location = "downloads_ok_zip.php?download_meds_zip="+id+"&single_pocess=true";
	}
}

function add_tocart(id)
{
	var Dvideolink = document.getElementById("add_to_cart_media"+id);
	Dvideolink.click();
}

var $_returnvalue = "";
var $_returnvalue_tip_user = 0;
var $_returnclicktext ="";
var $_returnsong_id ="";

var $_returnvalue_pro = "";
var $_returnvalue_tip_user_pro = 0;
var $_returnclicktext_pro ="";
var $_returnsong_id_pro ="";

var $_retval_pro_tip_gen_id_don = 0;
var $_retval_single_tip_gen_id_don = 0;
var $_retval_single_tip_gen_ids_aldon = "";
var $_retval_single_tip_gen_ids_aldon_pro = "";

var $_retval_pro_name_aftr_don = "";
var $_retval_pro_email_aftr_don = "";
var $_returnsong_id_pro_type = "";
var $_returnsong_id_pro_ids_zip = "";

var $_retval_single_name_aftr_don = "";
var $_retval_single_email_aftr_don = "";

function ask_to_donate(name,creator,song_id,clicktext,down_tip_gen_id_sin,get_all_detils_tip_fr)
{
	$("#get_donationpop").attr("href", "asktodonate.php?name="+encodeURIComponent(name)+"&creator="+encodeURIComponent(creator)+"&clicktext="+encodeURIComponent(clicktext)+"&song_id="+song_id+"&gen_user_down_tip_id="+down_tip_gen_id_sin+"&simple_detals_fr_tip="+get_all_detils_tip_fr);
	$("#get_donationpop").click();
}
/*function ask_to_donate(name,creator,song_id,clicktext)
{
	var val = confirm("would you like to donate :"+name+" ("+creator+")");
	if(val== true){
		var Dvideolink = document.getElementById(clicktext);
		Dvideolink.click();
	}
	else{
		window.location = "zipes_1.php?download_meds="+song_id;
	}
}*/
function ask_to_donate_result()
{
	if($_returnvalue == 1){
		$_returnvalue ="";
		var Dvideolink = document.getElementById($_returnclicktext);
		setTimeout(function(){
		$("#"+$_returnclicktext).attr("href", Dvideolink.href + "&don_tips_al="+$_returnvalue_tip_user);
		Dvideolink.click();
		}
		,2000);
	}
	else if($_returnvalue == 2){
		setTimeout(function(){
			$_returnvalue ="";
				window.location = "downloads_ok_zip.php?download_meds_zip="+$_returnsong_id+"&single_pocess=true&gen_down_tip_id="+$_retval_single_tip_gen_id_don+"&gen_tip_al_ids="+$_retval_single_tip_gen_ids_aldon;
			}
			,1000);
	}
}
$(document).ready(function() {
	$("#get_donationpop").fancybox({
		helpers : {
			media : {},
			buttons : {},
			title : null,
		},
		afterClose : function(){
								ask_to_donate_result();	
							}
	});
});

$(window).load(function() {
 $("#outerContainer").css({"display":"block"});
 $("#loaders").css({"display":"none"});
});

$("document").ready(function(){
	key = "scroll_position";
	currentcookie = document.cookie;
	if (currentcookie.length > 0)
	{
		firstidx = currentcookie.indexOf(key + "=");
		if (firstidx != -1)
		{
			firstidx = firstidx + key.length + 1;
			lastidx = currentcookie.indexOf(";",firstidx);
			if (lastidx == -1)
			{
				lastidx = currentcookie.length;
			}
			getdata = unescape(currentcookie.substring(firstidx, lastidx));
			var scro_pos = unescape(currentcookie.substring(firstidx, lastidx));
			setTimeout(function(){
				document.getElementById('scroll_act_one').scrollTop = scro_pos;
			},1000);
		}
	}
	document.cookie = key + '=;expires=Thu, 01 Jan 1970 00:00:01 GMT;';
	return "";
});



/**Hash change code **/
$(document).ready(function(){
	var url_check = document.URL;
	
	if(location.hash == "" || url_check.indexOf("#library")>0)
	{
		$("#library").show();
	}
	$(window).hashchange(
		function()
		{
			$("a").each(
			function()
			{    
				if($(this).attr("href") == location.hash)
				{
					$(this).click();
				}
			});
		}
	)

});

$(document).ready(function(){
	if(location.hash == "#s3account")
	{
		$("#s3account").show();
	}
});


function validate_amazon_account(){
	if($("#username").val().trim()==""){
		alert("Please enter name");
		return false;
	}
	else if($("#secret_key").val().trim()==""){
		alert("Please enter secret key");
		return false;
	}
	else if($("#access_key").val().trim()==""){
		alert("Please enter access key");
		return false;
	}else{
		$("#loaders").css({"display":"inline-block"});
		$("#outerContainer").css({"display":"none"});
	}
}
function show_loader(){
	$("#loaders").css({"display":"inline-block"});
	$("#outerContainer").css({"display":"none"});
}
</script>

</html>