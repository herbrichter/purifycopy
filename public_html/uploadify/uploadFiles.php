<?php
include_once('../commons/db.php');
session_start();
error_reporting(0);

if (!empty($_FILES)) {
	//include_once('classes/Gallery.php');

	// Image Upload Code for Amazon S3
	//if(strpos($_REQUEST['fileDesc'],"image")!==false){
		$tempFile = $_FILES['Filedata']['tmp_name'];
		$fileName = time().'-'.str_replace(" ","_",$_FILES['Filedata']['name']);
		$folderName = $_REQUEST['folder'];

		//include  S3 class
        $root = realpath($_SERVER["DOCUMENT_ROOT"]);
        if (!class_exists('S3')) require_once ($root.'/S3.php');
		//s3 credential setup
		if (!defined('awsAccessKey')) define('awsAccessKey', '');
		if (!defined('awsSecretKey')) define('awsSecretKey', '');
		//instantiate the class
		$s3 = new S3(awsAccessKey, awsSecretKey);
		//check whether form is submitted
		$sql_get_bucket = mysql_query("SELECT * FROM `general_user` WHERE `email`='".$_POST['bucket_name']."'");
		$run_get_bucket = mysql_fetch_assoc($sql_get_bucket);
		$bucket_name = "reggallery";
		$targetPath = "http://reggallery.s3.amazonaws.com/";
		$targetPathThumb = $_SERVER['DOCUMENT_ROOT'] .'/uploads/thumb/';
		//$bucket_name = '';

		//create a new bucket
		$s3->putBucket($bucket_name, S3::ACL_PUBLIC_READ);
		
		//put object
	//move the file
	if($s3->putObject(S3::inputFile($tempFile),$bucket_name,$fileName, S3::ACL_PUBLIC_READ))
	{ 
		//echo  $run_get_bucket['bucket_name'].$fileName;
	}
	createThumbnail($fileName, $targetPath, $targetPathThumb);
		/*
		
		//move the file
		if ($s3->putObjectFile($tempFile, $bucket_name, $run_get_bucket['bucket_name'].'/gallery/'.$fileName, S3::ACL_PUBLIC_READ)) {
			//echo "<strong>We successfully uploaded your file.</strong>";
		}
		
		*/
		
		/*Code for uploading images on server*/
		
			/*$tempFile = $_FILES['Filedata']['tmp_name'];
			$targetPath = $_SERVER['DOCUMENT_ROOT'] . $folderName . 'gallery/';
			$targetPathThumb = $_SERVER['DOCUMENT_ROOT'] . $folderName . 'thumb/';

			$targetFile =  str_replace('//','/',$targetPath) . $fileName;
			
			move_uploaded_file($tempFile,$targetFile);

			createThumbnail($fileName, $targetPath, $targetPathThumb);
			//echo $fileName;*/
	   /*Code for uploading images on server ends Here*/
		
		
	//}		
}

function createThumbnail($filename, $path_to_image_directory, $path_to_thumbs_directory) {

	$final_width_of_image = 200;
	//$path_to_image_directory = 'images/fullsized/';
	//$path_to_thumbs_directory = 'images/thumbs/';

	if(preg_match('/[.](jpg)$/', $filename)) {
		$im = imagecreatefromjpeg($path_to_image_directory . $filename);
	} else if (preg_match('/[.](gif)$/', $filename)) {
		$im = imagecreatefromgif($path_to_image_directory . $filename);
	} else if (preg_match('/[.](png)$/', $filename)) {
		$im = imagecreatefrompng($path_to_image_directory . $filename);
	}

	$ox = imagesx($im);
	$oy = imagesy($im);

	$nx = $final_width_of_image;
	$ny = floor($oy * ($final_width_of_image / $ox));

	$nm = imagecreatetruecolor($nx, $ny);

	imagecopyresized($nm, $im, 0,0,0,0,$nx,$ny,$ox,$oy);

	if(!file_exists($path_to_thumbs_directory)) {
	  if(!mkdir($path_to_thumbs_directory)) {
           die("There was a problem. Please try again!");
	  }
       }

	imagejpeg($nm, $path_to_thumbs_directory . $filename);
	
	$bucket_name = "reggalthumb";
    $root = realpath($_SERVER["DOCUMENT_ROOT"]);
    if (!class_exists('S3')) require_once ($root.'/S3.php');
	if (!defined('awsAccessKey')) define('awsAccessKey', '');
	if (!defined('awsSecretKey')) define('awsSecretKey', '');
	$s3 = new S3(awsAccessKey, awsSecretKey);
	$s3->putBucket($bucket_name, S3::ACL_PUBLIC_READ);
	$s3->putObject(S3::inputFile($path_to_thumbs_directory.$filename),$bucket_name,$filename,S3::ACL_PUBLIC_READ);
	unlink($path_to_thumbs_directory.$filename);
	echo $filename;
	//echo $tn;
}


?>