<?php
	include_once('commons/session_check.php');
	include_once('classes/Quote.php');
	
	session_start();
	$username = $_SESSION['username'];
	if(!$username == '')
	{
		$login_flag=1;
	}
	
	if($login_flag) include_once('loggedin_includes.php');
	else include_once('login_includes.php');
	
	if(isset($_POST['postquote']))
	{
		$quote_para=$_POST['quote_para'];
		$source=$_POST['source'];
		$source_link=$_POST['source_link'];
		$author=$_POST['author'];
		$author_link=$_POST['author_link'];
		
		$obj=new Quote();
		if($obj->addQuote($quote_para,$source,$source_link,$author,$author_link))
			$msg="<div style='background:#CCCCCC; padding:5px; float:right; text-align:center;' class='hintWhite'>Quote has been submitted.</div>";
		else
			$msg="<div style='background:#CC0000; padding:5px; float:right; text-align:center;' class='hintWhite'>Error in submiting quote.</div>";
	}	
	
	$objQuote=new Quote();
	$rs=$objQuote->getQuote();
?>

<?php include_once('includes/header.php'); ?>
<title>Purify Entertainment: About</title>
<script type="text/javascript" language="javascript">
function suggsubmit()
{
	$name=document.getElementById('name');
	$email=document.getElementById('email');
	$sugg=document.getElementById('sugg');
	$sugg_form=document.getElementById('sugg_form');
	
	if($name == '' || $email == '' || $sugg == '')
	{
		alert('Please fill all the fields.');
	}
	else
	{
		$sugg_form.submit();
	}			
}
</script>

  <div id="contentContainer">
	<?php include_once('includes/subnavigation_aboutpages.php'); ?>
    <div id="actualContent">
    <?php echo $msg; ?>    
	<h1>Quotes</h1>
		<div style="width:100%; float:left;">
        	<p>Post a quote here with the following details.</p>
            <form action="" method="post" id="news_post">
            <table width="100%" border="0" cellpadding="0" cellspacing="0" align="center">
                <tr>
                  <td width="100" valign="top">Quote</td>
                  <td width="402">
                    <textarea name="quote_para" class="regularField" id="quote_para"></textarea>
                  </td>
                  <td width="148" align="left"><div class="hint">Upto 1000 characters</div></td>
                </tr>
                <tr>
                  <td colspan="3"><img src="images/spacer.gif" alt=" " width="10" height="10" /></td>
                </tr>
                <tr>
                  <td width="100">Source</td>
                  <td width="402">
                    <input name="source" type="text" class="regularField" id="source" />
                  </td>
                  <td width="148" align="left"><div class="hint"></div></td>
                </tr>
                <tr>
                  <td colspan="3"><img src="images/spacer.gif" alt=" " width="10" height="10" /></td>
                </tr>
                <tr>
                  <td width="100">Source Link</td>
                  <td width="402">
                    <input name="source_link" type="text" class="regularField" id="source_link" value="http://" />
                  </td>
                  <td width="148" align="left"><div class="hint"></div></td>
                </tr>
                <tr>
                  <td colspan="3"><img src="images/spacer.gif" alt=" " width="10" height="10" /></td>
                </tr>
                <tr>
                  <td width="100">Author</td>
                  <td width="402">
                    <input name="author" type="text" class="regularField" id="author" />
                  </td>
                  <td width="148" align="left"><div class="hint"></div></td>
                </tr>
                <tr>
                  <td colspan="3"><img src="images/spacer.gif" alt=" " width="10" height="10" /></td>
                </tr> 
                <tr>
                  <td width="100">Author Link</td>
                  <td width="402">
                    <input name="author_link" type="text" class="regularField" id="author_link" value="http://" />
                  </td>
                  <td width="148" align="left"><div class="hint"></div></td>
                </tr>
                <tr>
                  <td colspan="3"><img src="images/spacer.gif" alt=" " width="10" height="10" /></td>
                </tr>                                         
                <tr>
                  <td width="100" valign="top"></td>
                  <td width="402" align="right">
                    <input type="submit" name="postquote" id="postquote" value=" Submit " />
                  </td>
                  <td width="148" align="left"><div class="hint"></div></td>
                </tr>
              </table>
              </form>        
        </div>
       	<div id="area-separator"></div>    
        <h3>Published Quotes</h3>
        <?php 
			while($row=mysql_fetch_assoc($rs))
			{
		?>
        <div id="quote-box">
	        <div id="quote-separator" class="hintGray"></div>         
        	<div id="quote-line"><?php echo $row['quote_para'] ?></div>
        	<div id="quote-line" class="hint"><a href="<?php echo $row['source_link'] ?>" target="_blank"><?php echo $row['source'] ?></a> | <a href="<?php echo $row['author_link'] ?>" target="_blank"><?php echo $row['author'] ?></a></div>            
        </div>
        <?php
			}
		?>
    </div>
    
    <div class="clearMe"></div>
    
  </div>
  
<?php include_once('includes/footer.php'); ?>