<?php
$result = array();
$errorflag='no';

$errormsg=array();
$inputparams=array();
$username='nouser';
$userpass='nopass';
$listartists='leerick,kaz,hadley';
$format='json';

$data = file_get_contents("php://input");
$dataary=json_decode($data,true);

if (isset($dataary['format'])){    
    $format = strtolower($dataary['format']);
}
if(isset($dataary['listartists'])) {
    $listartists=$dataary['listartists'];
}else {
    $errormessage='missing profile';
    $errormsg[]=array('errormsg'=>$errormessage);
    $result[] = array('error'=>$errormsg);
    $inputparams[] = array('listartists'=>$listartists);
    $inputparams[] = array('format'=>$format);
    $inputparams[] = array('post'=>$dataary);
    $result[] = array('inputparams'=>$inputparams);
    $errorflag='yes';
}

if ($errorflag='no'){
    include_once("commons/db.php");    
    include_once('classes/Artist.php');
    include_once('classes/Media.php');
    include_once('classes/CountryState.php');
    include_once('classes/Type.php');
    include_once('classes/SubType.php');
    include_once('classes/MetaType.php');
    include_once('classes/ProfileDisplay.php');    
    include_once("classes/ViewArtistProfileURL.php");
    require_once('vimeo-vimeo-php-lib/vimeo.php');
    
    $key = '6b55800e7503e2cb8d69f93d2f7bda0df9fac434';
    $secret = 'f924407f6edf2e3398092516363de7b562a9d4a5';
    $vimeo = new phpVimeo($key, $secret);    
   
    $new_profile_class_obj = new ViewArtistProfileURL();
    
    $foundurls=",";
    $foundvideos=",";
    
    $listartists_exp = explode(',',$listartists);
    
    $outputvideoindex=0;
    if(isset($listartists_exp) && count($listartists_exp)>0 && !empty($listartists_exp))
    {
        $tagged_profileurls="";
        for($artistindex=0;$artistindex<count($listartists_exp);$artistindex++)
        {               
            //echo '<br>getting profileurl='.$listartists_exp[$artistindex].' <br>';     
            $pos=strpos($foundurls,$listartists_exp[$artistindex]);
            if ($pos===false)
            {                            
                $foundurls=$foundurls.','.$listartists_exp[$artistindex];                           
                if($listartists_exp[$artistindex]!="")
                { 
                
                    $artistprofileurl=$listartists_exp[$artistindex];
                    
                    If($artistindex==0)
                    {
                        $objArtist=new Artist();
                        $objMedia=new Media();
                        $display = new ProfileDisplay();
                        $artist_check = $display->artistCheck($artistprofileurl);
    
                        //$new_profile_class_obj = new ViewArtistProfileURL();
                        $getgeneral =  $new_profile_class_obj->get_user_info($artist_check['artist_id']);
	                    $result[] = array('generaluser'=>$getgeneral);
    
                        //$get_user_delete_or_not = $new_profile_class_obj->chk_user_delete($artist_check['artist_id']);
                        //$get_common_id=$artist_check['artist_id'];
    
    
                        ////var_dump($getgeneral);
                        $getuser = $new_profile_class_obj->get_user_artist_profile($artist_check['artist_id']);
                        $result[] = array('generalartist'=>$getuser);
                        $getcountry = $new_profile_class_obj->get_user_country($artist_check['artist_id']);
                        $result[] = array('country'=>$getcountry);
                        $getstate = $new_profile_class_obj->get_user_state($artist_check['artist_id']);
                        $result[] = array('state'=>$getstate);
                        $gettype = $new_profile_class_obj->get_artist_user_type($artist_check['artist_id']);
                        $result[] = array('type'=>$gettype);
                        ////$get_type_dis = $new_profile_class_obj->get_artist_user($artist_check['artist_id']);
                        ////$result[] = array('get_type_dis'=>$get_type_dis);
	
                        $get_subtype = $new_profile_class_obj->get_artist_user_subtype($artist_check['artist_id']);
                        $result[] = array('subtype'=>$get_subtype);
                        ////$get_subtype_dis = $new_profile_class_obj->get_artist_user_subs($artist_check['artist_id']);
                        ////$result[] = array('get_subtype_dis'=>$get_subtype_dis);
	
                        ////var_dump($get_subtype);
                        $get_metatype = $new_profile_class_obj->get_artist_user_metatype($artist_check['artist_id']);
                        $result[] = array('metatype'=>$get_metatype);
                        //$src = "http://artjcropprofile.s3.amazonaws.com/";
                        //$src_list = "http://artjcropthumb.s3.amazonaws.com/";
                        //$src1 = "../";
    
                        //echo '<br> about to get all projects <br>';
                        
                    
                    
                    
                    
                    
                    
                                        //echo '<br>getting user artist for artisturl='.$listartists_exp[$artistindex].' <br>'; 
                    } 
                
                    //echo '<br>getting artists for profileurl='.$listartists_exp[$artistindex].' <br>'; 
                    
                    $userartist =$new_profile_class_obj->get_user_artist_profileurl($listartists_exp[$artistindex]);
                    //echo '<br> userartist <br>';
                    //print_r($userartist);                   
                    
                    $exp_tagged_videos=explode(',',$userartist['taggedvideos']);                    

                    //echo '<br> tagged videos <br>';
                    //print_r($exp_tagged_videos);
                    //print_r('<br> Count='.count($exp_tagged_videos));

                    if(isset($exp_tagged_videos) && count($exp_tagged_videos)>0)
                    {
                        for($tagvideoindex=0;$tagvideoindex<count($exp_tagged_videos);$tagvideoindex++)
                        {
                            if($exp_tagged_videos[$tagvideoindex]!="")
                            {
                                $pos=strpos($foundvideos,','.$exp_tagged_videos[$tagvideoindex]);
                                //print_r('foundvideo='.$foundvideos.' taggedvideo='.$exp_tagged_videos[$tagvideoindex].' pos='.$pos);
                                if ($pos==false)
                                {                            
                                    $foundvideos=$foundvideos.','.$exp_tagged_videos[$tagvideoindex];                      
                        
                                   // echo '<br> tagvideoindex '.$tagvideoindex.' tagged video ' .$exp_tagged_videos[$tagvideoindex].'<br>';
                                    if($exp_tagged_videos[$tagvideoindex]==""||$exp_tagged_videos[$tagvideoindex]==" "||$exp_tagged_videos[$tagvideoindex]==Null){continue;}
                                    else
                                    {                
                                        $exp_tagged_videos_info[$outputvideoindex] = $new_profile_class_obj->get_Media_video($exp_tagged_videos[$tagvideoindex]);
                                        //$imagename= $new_profile_class_obj->get_audio_img($exp_tagged_videos[$tagvideoindex]);
                                    
			                            $get_video_image = $new_profile_class_obj->getvideoimage($exp_tagged_videos[$tagvideoindex]);
			                        
                                        //echo '<br>Called getvideoimage <br>';
                                        //print_r($get_video_image);
                                    
                                    
			                            if(!empty($get_video_image))
			                            {
				                            if($get_video_image[1]=='youtube')
				                            {
                                                $imagename="https://img.youtube.com/vi/".$get_video_image[0]."/default.jpg";
                                            }                                     
				                            elseif($get_video_image[1]=='vimeo' && $get_video_image[0]!=NULL)
				                            {					
					                            $video = $vimeo->call('vimeo.videos.getInfo', array('video_id' => $get_video_image[0]));
					                            $imagename = $video->video[0]->thumbnails->thumbnail[1]->_content; 
                                            }
                                        }else
                                        {
                                            $imagename ="https://comjcropprofile.s3.amazonaws.com/Noimage.png";
                                        }
                                        //echo '<br>imagename = '.$imagename.' <br>';                                    
                                    
                                    
                                        $videoimagefile[$exp_tagged_videos[$tagvideoindex]]=$imagename;
                                    
                                        $profileurlforvideo[$exp_tagged_videos[$tagvideoindex]]=$listartists_exp[$artistindex];                                     
                                        //echo '<br> output videoindex '.$outputvideoindex.'<br>';
                                        //echo '<br> for key '.$exp_tagged_videos[$tagvideoindex].' value is '.$videoimagefile[$exp_tagged_videos[$tagvideoindex]].' <br>';                
                                        //print_r($exp_tagged_videos_info[$outputvideoindex]);
                                        $outputvideoindex++;
                                    }
                                } // skip video - duplicate
                            } // no videos
                        } // end of video loop
                    } // no videos
                    
                    //print_r($get_all_user_artists_for_profileurl); 
                    //echo '<br>emails in project index ='.$j.' <br>';
                } // no artists - should not have this error
            } // skipping this profileurl already processed
        } // end of get videos for profileurl
    }    

    //print_r($songimagefile);
    //die;
        
    // Obtain a list of columns
    foreach ($exp_tagged_videos_info as $key => $row) {
        $from[$key]  = $row['creator'];
        $track[$key] = $row['title'];
    }

    // Sort the data with volume descending, edition ascending
    // Add $data as the last parameter, to sort by the common key
    array_multisort($from, SORT_ASC, $track, SORT_ASC, $exp_tagged_videos_info);
                        
    $result[] = array('mediavideo'=>$exp_tagged_videos_info); 
    $result[] = array('videoimagefile'=>$videoimagefile);
    $result[] = array('profileurlforvideo'=>$profileurlforvideo);
    
    
    
//print ( '<pre>' );
//print_r($exp_tagged_videos_info);
//print_r($videoimagefile);
//print_r($profileurlforvideo);
//print ( '</pre>' );    
//die;

  
    // add alluserartistsforproject to result array
  
    

} //end of if error == no

/* output in necessary format */
if($format == 'json') {
    header('Content-type: application/json');
    echo json_encode($result);
    //echo json_encode(array('artistprofile'=>$result));
    //echo array_to_json($result);
}
else {
    if ($format == 'xml'){
        header('Content-type: text/xml');
        echo '<result>';
        foreach($result as $index => $resultentry) {
            if(is_array($resultentry)) {
                foreach($resultentry as $key => $value) {
                    echo '<',$key,'>';
                    if(is_array($value)) {
                        foreach($value as $tag => $val) {
                            echo '<',$tag,'>',htmlentities($val),'</',$tag,'>';
                        }
                    }
                    echo '</',$key,'>';
                }
            }
        }
        echo '</result>';
    }
}

function array_to_json( $array ){

    if( !is_array( $array ) ){
        return false;
    }

    $associative = count( array_diff( array_keys($array), array_keys( array_keys( $array )) ));
    if( $associative ){

        $construct = array();
        foreach( $array as $key => $value ){

            // We first copy each key/value pair into a staging array,
            // formatting each key and value properly as we go.

            // Format the key:
            if( is_numeric($key) ){
                $key = "key_$key";
            }
            $key = "\"".addslashes($key)."\"";

            // Format the value:
            if( is_array( $value )){
                $value = array_to_json( $value );
            } else if( !is_numeric( $value ) || is_string( $value ) ){
                $value = "\"".addslashes($value)."\"";
            }

            // Add to staging array:
            $construct[] = "$key: $value";
        }

        // Then we collapse the staging array into the JSON form:
        $result = "{ " . implode( ", ", $construct ) . " }";

    } else { // If the array is a vector (not associative):

        $construct = array();
        foreach( $array as $value ){

            // Format the value:
            if( is_array( $value )){
                $value = array_to_json( $value );
            } else if( !is_numeric( $value ) || is_string( $value ) ){
                $value = "'".addslashes($value)."'";
            }

            // Add to staging array:
            $construct[] = $value;
        }

        // Then we collapse the staging array into the JSON form:
        $result = "[ " . implode( ", ", $construct ) . " ]";
    }
	
    return $result;
}

?>