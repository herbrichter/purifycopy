<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "//www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<?php
session_start();
?>
<html xmlns="//www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Purify Art</title>
<link rel="stylesheet" type="text/css" media="screen" href="includes/purify.css" />
<link rel="stylesheet" href="engine/css/vlightbox.css" type="text/css" />
<link rel="stylesheet" href="engine/css/visuallightbox.css" type="text/css" media="screen" />
<link rel="stylesheet" href="css/jquery.autocomplete.css" type="text/css" />
<link rel="stylesheet" href="includes/jquery.ui.all.css">
<link  rel="stylesheet" type="text/css" media="screen" href="css/jquery.ui.autocomplete.css"></link>


 <script type="text/javascript" src="javascripts/country_state.js"></script>
<script src="javascripts/jquery-1.2.6.js"></script>
<script type="text/javascript" src="javascripts/flowplayer-3.1.4.min.js"></script>
<script type="text/javascript" src="javascripts/audio-player.js"></script>
<style type="text/css">#vlightbox a#vlb{display:none}</style>
<script src="engine/js/jquery.min.js" type="text/javascript"></script>
<script src="javascripts/jquery-1.2.6.js"></script>
<script src="ui/jquery-1.7.2.js"></script>
<script src="includes/jquery.ui.core.js"></script>
<script src="includes/jquery.ui.widget.js"></script>
<script src="includes/jquery.ui.datepicker.js"></script>
<script type="text/javascript" src="javascripts/jquery.ui.autocomplete.js"></script>
<script type="text/javascript" src="javascripts/jquery.ui.position.js"></script>
<script type="text/javascript" src="javascripts/checkForm.js"></script>
<style>
	.ui-autocomplete-loading { background: white url('css/ui-anim_basic_16x16.gif') right center no-repeat; }
</style>


</head>
<body>
<div id="outerContainer">
<div id="purifyMasthead">
	<div id="purifyLogo">
		<a href="index.php"><img src="images/purelogo.gif" alt="Purify Entertainment" width="302" height="60" border="0"/></a>
	</div>
<?php
	include("login_area.php"); 	


?>


		<script>
			!window.jQuery && document.write('<script src="fancybox/jquery-1.4.3.min.js"><\/script>');
		</script>
		<script type="text/javascript" src="fancybox/jquery.fancybox-1.3.4.pack.js"></script>
		<link rel="stylesheet" type="text/css" href="fancybox/jquery.fancybox-1.3.4.css" media="screen" />
	
</div>