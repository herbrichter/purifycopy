<?php
    ob_start();
	include_once('login_includes.php');
	include_once('classes/UserSubscription.php');

	$user_id=$_GET['uid'];
	$subscription_id=$_GET['sid'];
?>

<?php include_once('includes/header.php'); ?>
<title>Purify Entertainment: Six Feature</title>

  <div id="contentContainer">
    <h1>Complete your registration</h1>
    <p>Dear user, you have been directed to this page either because you have not yet completed the payment for your subscription or your request is still pending approval.</p>
    <p>Please <a href='registration_payment_gc.php?uid=<?php echo $user_id; ?>&&sid=<?php echo $subscription_id ?>' title="Pay using Google Checkout">click here</a> to complete your payment or ignore this message if you have already paid. Your account will be activated once approved.</p>
  </div>
  
<?php include_once('includes/footer.php'); ?>